package login.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.AgreementsActivity;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.ForgetPasswordActivity;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.background.VolleyRequest.ForgetPasswordRequest.ForgetPasswordSendOTPApi;
import com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse.ForgetPasswordSendOTPResponse;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.ResendOtpTFAResponse;
import com.mphrx.fisike.background.sendForgetPassOTP;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.ContactAdminActivity;
import com.mphrx.fisike.createUserProfile.fragment.SetPasswordFragment;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.gson.request.UploadPicture;
import com.mphrx.fisike.gson.response.ForgetPassOtpVerifyResponse;
import com.mphrx.fisike.gson.response.ForgetPasswordResponseGson.ForgetPasswordSendOTPResponseGson;
import com.mphrx.fisike.gson.response.GenericStatusMsgResponse;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.persistence.AgreementFileDBAdapter;
import com.mphrx.fisike.persistence.PhysicianDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowRequest;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowResponse;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ErrorMessage;
import com.mphrx.fisike_physician.activity.ContactActivity;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.fragment.OTPVerificationFragment;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.FetchAllAgreementsRequest;
import com.mphrx.fisike_physician.network.request.RegisterUserDeviceRequest;
import com.mphrx.fisike_physician.network.request.UserDetailRequest;
import com.mphrx.fisike_physician.network.response.FetchAllAgreementsResponse;
import com.mphrx.fisike_physician.network.response.LoginResponse;
import com.mphrx.fisike_physician.network.response.RegisterUserDeviceResponse;
import com.mphrx.fisike_physician.network.response.UserDetailResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;

import appointment.model.LinkedPatientInfoMO;
import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;
import appointment.request.FetchSelfAndAssociatedPatients;
import appointment.request.FetchSelfAndAssociatedPatientsResponse;
import login.fragment.LoginFragment;
import searchpatient.activity.SearchPatientFilterActivity;
import signup.ResponseCodesEnum;
import signup.activity.LoginSignupActivity;
import signup.fragment.EnterPassword;
import signup.fragment.OTPFragment;
import signup.response.ResetPasswordResponse;
import signup.response.SendOtpResponse;
import signup.response.VerifyOtpSignupResponse;

public class LoginActivity extends LoginSignupActivity implements LoginFragment.OnGetTransactionIdListener, FragmentManager.OnBackStackChangedListener {


    private boolean mIsUserExisted;
    private UserMO userMO;
    private boolean mIsadminCreatedPasswordNeverChanged;
    private String mDefaultPassword;
    private String tempPassword;
    private String usernameHeader;
    private LoginResponse loginResponse;
    private String passWord;
    private boolean isInvalidUser;
    private AlertDialog alert;

    private String loginId;
    public Toolbar mToolbar;
    public TextView toolbar_title;
    public IconTextView bt_toolbar_right;
    public ImageButton btnBack;
    private boolean popToLoginScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_login);
        isInvalidUser = getIntent().getBooleanExtra(VariableConstants.REVOKE_USER, false);
        initToolbar();
        if (savedInstanceState == null) {
            LoginFragment loginFragment = LoginFragment.getInstance(null);
            addFragment(loginFragment, R.id.fragment_container, false);
        } else
            mIsUserExisted = savedInstanceState.getBoolean(TextConstants.USER_EXISTED, false);


        if (isInvalidUser)
            showAlertDialog(this, getResources().getString(R.string.invalid_login), getResources().getString(R.string.unable_find_user_login_details), getResources().getString(R.string.btn_ok));
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        mToolbar.setBackgroundColor(getResources().getColor(R.color.theme_btn_normal));
    }

    public void setToolbar(int mode) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        mToolbar.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switch (mode) {
            case OTP_VERIFICATION:
                toolbar_title.setText(getResources().getString(R.string.lets_verify));
                break;
            case ENTER_PASSWORD:
                toolbar_title.setText(getResources().getString(R.string.password_header));
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void onClick(View view) {
        Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragmentById == null) {
            return;
        }
        if (fragmentById instanceof LoginFragment) {
            ((LoginFragment) fragmentById).onClick(view);
        } else if (fragmentById instanceof OTPFragment) {
            ((OTPFragment) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterPassword) {
            ((EnterPassword) fragmentById).onClick(view);
            return;
        }
    }

    @Override
    public void fetchInitialDataFromServer() {
        // Do Nothing
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }


    private void handleErrorResponse(String status, LoginResponse response) {

        switch (status) {

            case TextConstants.STATUS_CODE_460:
                showDialogForNoContact(getResources().getString(R.string.cannot_establish_contact), getResources().getString(R.string.email_does_not_exist));
                break;

            case TextConstants.STATUS_CODE_461:
                showDialogForNoContact(getResources().getString(R.string.cannot_establish_contact), getResources().getString(R.string.phone_number_not_exist));
                break;

            case TextConstants.STATUS_CODE_IN02:
                showDialogForNoContact(getResources().getString(R.string.cannot_establish_contact), getResources().getString(R.string.email_phone_number_needed));
                break;

            case TextConstants.HTTP_STATUS_UV14:
                showDialogToResetPassword(true);
                break;

            case TextConstants.HTTP_STATUS_423:
                AppLog.d("AppLogs", "Logging Error " + TextConstants.HTTP_STATUS_423);

                break;
            case TextConstants.HTTP_STATUS_425:
            case TextConstants.HTTP_STATUS_UV15:
                showDialogToResetPassword(false);
                break;

            case TextConstants.HTTP_STATUS_401:
            case TextConstants.HTTP_STATUS_L01:
                //signInSignUpFragment.setError(TextConstants.INVALID_LOGIN);
                String attempt_remaining = "";
                attempt_remaining = ((response.getNetworkResponse().headers != null
                        && Utils.isValueAvailable(response.getNetworkResponse().headers.get("incorrectPasswordRetriesRemaining"))
                        && response.getNetworkResponse().headers.get("incorrectPasswordRetriesRemaining").equals("1"))
                        ? ("1 attempt") : (response.getNetworkResponse().headers.get("incorrectPasswordRetriesRemaining") + " attempts"));
                String msg;

                if (!Utils.isValueAvailable(response.getNetworkResponse().headers.get("incorrectPasswordRetriesRemaining")))
                    msg = getResources().getString(R.string.Incorrect_username_or_password);
                else
                    msg = getString(R.string.incorrect_pass_n_attempts_Remaining, attempt_remaining);
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                break;

            default:
                Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showDialogForNoContact(String message, String title) {
        DialogUtils.showAlertDialog(this, title, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    openActivity(ContactAdminActivity.class, null, true);
                } else if (which == Dialog.BUTTON_NEGATIVE) {

                }
            }
        });
    }

    @Subscribe
    public void LoginResponse(LoginResponse response) throws Exception {

        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            passWord = response.getPassword();
            if (response.isOtpSent()) {
                twoFactorAuthenticationFlow(response);
            } else {
                /**edited by AASTHA */
                adminFlow(response);
            }
        } else {
            dismissProgressDialog();
            String responseStatus = response.getErrorResponseStatus();

            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            else if (Utils.isValueAvailable(responseStatus) && Integer.parseInt(responseStatus) == 401) {
                int error;
                if (SharedPref.getIsEmailEnabled()) {
                    error = R.string.Incorrect_email_or_password;
                } else if (SharedPref.getIsMobileEnabled()) {
                    error = R.string.Incorrect_mobile_or_password;
                } else {
                    error = R.string.Incorrect_username_or_password;
                }
                Toast.makeText(MyApplication.getAppContext(), getResources().getString(error), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                    /*TODO - specific cases for code not handled(not required) as of now @@aastha for phloober*/
            //code = errors.optJSONObject(0).optString("code");
        }
    }

    private void adminFlow(LoginResponse response) {
        boolean isadminCreatedPasswordNeverChanged = SharedPref.getAdminCreatedPasswordNeverChanged();
        if (isadminCreatedPasswordNeverChanged) {
            dismissProgressDialog();
            Bundle args = new Bundle();
            args.putString("defaultpass", mDefaultPassword);
            SetPasswordFragment setPasswordFragment = SetPasswordFragment.getInstance(args);
            replaceFragment(setPasswordFragment, R.id.fragment_container, false);
        } else if (response.isLoginPossible())
            ThreadManager.getDefaultExecutorService().submit(new UserDetailRequest(passWord, getTransactionId(), this));
        else {
            Toast.makeText(this, "Password Expired", Toast.LENGTH_SHORT).show();
        }
    }

    private void twoFactorAuthenticationFlow(LoginResponse response) {

        String mobileNumber = "";
        String email = "";
        dismissProgressDialog();
        Bundle args = new Bundle();
        args.putString("username", response.getUsernameHeader());
        usernameHeader = response.getUsernameHeader();
        mobileNumber = response.getMobile();
        email = response.getEmail();
        int otpSentDevices = Utils.checkOtpSentDevices(mobileNumber, email);
        args.putString("mobile", mobileNumber);
        args.putString("email", email);
        args.putInt("otpSentOnDevices", otpSentDevices);
        OTPVerificationFragment otpVerificationFragment = OTPVerificationFragment.getInstance(args);
        replaceFragment(otpVerificationFragment, R.id.fragment_container, false);
    }


    @Subscribe
    public void onResendOtpTFAResponse(ResendOtpTFAResponse response) {
        dismissProgressDialog();
        if (response.getTransactionId() != mTransactionId)
            return;

        if (response.isSuccessful()) {
            String mobileNumber;
            String email;

            Bundle args = new Bundle();
            args.putString("username", response.getUsernameHeader());
            usernameHeader = response.getUsernameHeader();
            mobileNumber = response.getMobile();
            email = response.getEmail();
            int otpSentDevices = Utils.checkOtpSentDevices(mobileNumber, email);
            args.putString("mobile", mobileNumber);
            args.putString("email", email);
            args.putInt("otpSentOnDevices", otpSentDevices);
            OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
            if (fragment == null || !fragment.isVisible()) {
                replaceFragment(OTPVerificationFragment.getInstance(args), R.id.fragment_container, false);
            } else {
                //fragment.initView();
                fragment.readArguments(args);
                if (mobileNumber != null)
                    fragment.startTimer();
            }
        } else {

            dismissProgressDialog();
            Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onUserProfileResponse(UserDetailResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            UserMO userMO = SettingManager.getInstance().getUserMO();

            //Below check is for checking if the agreements are available in language requested by user MNV-8921(aastha)
            if (!response.isCorrectAgreementStatus()) {
                dismissProgressDialog();
                //TODO-text to be taken
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.language_no_info), getResources().getString(R.string.language_not_available), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            //TODO extra text
                            intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(LoginActivity.this, null) + "\n" + getString(R.string.regards));
                            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                            }
                        } else if (which == Dialog.BUTTON_NEGATIVE) {
                            dialog.dismiss();
                        }
                    }
                });
            }
            /*Check to see if user is logged in on another device MNV-8921(aastha)*/
            else if (response.isAlreadyLoggedIn()) {
                dismissProgressDialog();
                DialogUtils.showAlertDialog(this, MyApplication.getAppContext().getResources().getString(R.string.confirm_device_switch),
                        MyApplication.getAppContext().getResources().getString(R.string.existing_sessions_terminated), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == Dialog.BUTTON_POSITIVE) {
                                    showProgressDialog();
                                    ThreadManager.getDefaultExecutorService().submit(new RegisterUserDeviceRequest(getTransactionId()));
                                } else if (which == Dialog.BUTTON_NEGATIVE) {
                                    SharedPref.clearAllData();
                                }
                            }
                        });
            } else {
                fetchAllAgreementsRequest(userMO);
            }
        } else {
            dismissProgressDialog();
            SharedPref.clearAllData();
            Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }

    }

    private void fetchAllAgreementsRequest(UserMO userMO) {
        ArrayList<AgreementModel> agreementModels = userMO.getAgreements();
        if (agreementModels != null && agreementModels.size() > 0) {
            dismissProgressDialog();
            startAgreementActivity(userMO);
        }
        /* if agreements array is null or size is 0,we still populate our DB as it is required in support activity MNV-8921(aastha)*/
        else {
            ThreadManager.getDefaultExecutorService().submit(new FetchAllAgreementsRequest(getTransactionId(), this));
        }
    }


    @Subscribe
    public void onRegisterUserDeviceResponse(RegisterUserDeviceResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful()) {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            fetchAllAgreementsRequest(userMO);

        } else {
            dismissProgressDialog();
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void startAgreementActivity(UserMO userMO) {
        // UserMO userMO = SettingManager.getInstance().getUserMO();
        if (userMO == null) {
            return;
        }
        Intent agreementsIntent = new Intent(this, AgreementsActivity.class);
        agreementsIntent.putExtra(TextConstants.FROM_LOGIN, true);
        startActivity(agreementsIntent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(TextConstants.USER_EXISTED, mIsUserExisted);
    }

    @Subscribe
    public void onVerifyOtpSignup(VerifyOtpSignupResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessScenario()) {
            Bundle bundle = new Bundle();
            bundle.putString(MphRxUrl.K.OTP, getPhoneOTP());
            addFragment(EnterPassword.getInstance(bundle));
        } else {
            String responseStatus = response.getErrorResponseStatus();
            String code = response.getErrorCode();

            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();

            else if (Utils.isValueAvailable(code)) {
                String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
                Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onResetPasswordResponse(ResetPasswordResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            showDialogforPassword();
        } else {
            String responseStatus = response.getErrorResponseStatus();
            String code = response.getErrorCode();

            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();

            else if (Utils.isValueAvailable(code)) {
                String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
                Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onPhysicianDetailResponse(PractionerShowResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();

        userMO = SettingManager.getInstance().getUserMO();
        try {
            ContentValues contentValues = new ContentValues();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(this);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(response.getPractitioneMOFromResponse().getAddress());
            contentValues.put(userDBAdapter.getADDRESS(), bytes.toByteArray());
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response.isSuccessful() && response.getPractitioneMOFromResponse() != null) {
            try {
                PhysicianDBAdapter.getInstance(this).createOrUpdatePhysicianMo(response.getPractitioneMOFromResponse());
            } catch (Exception e) {
                e.printStackTrace();
            }
            lauchHomeScreen();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            SharedPref.clearAllData();
        }
    }


    @Override
    protected void onDestroy() {
        unregisterSMSReceiver();
        super.onDestroy();
    }

    public void lauchHomeScreen() {
        if (userMO != null) {
            UserDBAdapter.getInstance(this).updateSalutation(userMO.getPersistenceKey(), TextConstants.YES);
        } else {
            userMO = SettingManager.getInstance().getUserMO();
            UserDBAdapter.getInstance(this).updateSalutation(userMO.getPersistenceKey(), TextConstants.YES);
        }
        Utils.revertForgetPasswordInitiation();
        SharedPref.setLoginAttempts(0);
        SharedPref.setUserLogout(false);
        Utils.setUserLoggedOut(false);
        SharedPref.setLastInteractionTime(System.currentTimeMillis());
        Utils.checkAlternateContactDetails(SettingManager.getInstance().getUserMO().getAlternateContact());
        UserMO userMo = SettingManager.getInstance().getUserMO();
       /* new UserProfilePic(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        dismissProgressDialog();*/
        if (BuildConfig.isPatientApp) {
            Intent i = new Intent(this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (userMo.getHasSignedUp() != null && userMo.getHasSignedUp())
                i.putExtra(VariableConstants.LAUNCH_UPLOAD_SCREEN, true);
            startActivity(i);
        } else if (BuildConfig.isSearchPatient) {
            openActivity(SearchPatientFilterActivity.class, null, true);

        } else {
            openActivity(ContactActivity.class, null, true);
        }
    }


    public void uploadProfilePic(String jsonString, byte[] bitmapdata, UploadPicture uploadGson, boolean isTostopProgress) {
        UserMO userdata = SettingManager.getInstance().getUserMO();
        UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(this);
        ContentValues values = new ContentValues();
        if (jsonString != null && uploadGson.getStatus().equals("SC200")) {
//            userdata.setProfilePic(bitmapdata);
            userdata.setProfilePicUpdateStatus(true);
        } else {
            userdata.setProfilePic(null);
            Toast.makeText(this, getResources().getString(R.string.failed_update_profile_pic), Toast.LENGTH_SHORT).show();
        }
        try {
            values.put(userDBAdapter.getPROFILEPIC(), userdata.getProfilePic());
            values.put(userDBAdapter.getPROFILEPICUPDATESTATUS(), userdata.getProfilePicUpdateStatus());
//            SettingManager.getInstance().updateUserMO(userdata);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showAlertDialog(final Context context, String title, String message, String btnname) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(btnname, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                isInvalidUser = false;
            }

        });
        if (alert != null && alert.isShowing())
            alert.dismiss();
        alert = alertDialog.show();
    }


    public void showDialogToResetPassword(final boolean isPasswordExpired) {

        Resources res = getResources();
        userMO = SettingManager.getInstance().getUserMO();
        String text;
        String title;
        if (isPasswordExpired) {
            title = getResources().getString(R.string.Password_expired);
            text = res.getString(R.string.dialog_expired_password_message);
        } else {
            title = getResources().getString(R.string.Password_attempts_exceeded);
            text = res.getString(R.string.dialog_reset_password_message);
        }

        DialogUtils.showAlertDialog(this, title, text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    if (BuildConfig.isForgetPassword) {
                        SharedPref.setResetPassword(true);
                        resetPassword();
                    } else {
                        openActivity(ContactAdminActivity.class, null, false);
                    }
                } else if (which == Dialog.BUTTON_NEGATIVE) {
                    SharedPref.setResetPassword(false);
                }
            }
        });
    }


    private void sendOTP() {
        new sendForgetPassOTP(this, SharedPref.getEmailAddress()).execute();
    }

    private void resetPassword() {

        String number = "";
        showProgressDialog();
        if (SharedPref.getIsUserNameEnabled())
            number = SharedPref.getLoginUserName();
        else if (SharedPref.getIsMobileEnabled())
            number = SharedPref.getMobileNumber();
        else if (!SharedPref.getIsMobileEnabled())
            number = SharedPref.getEmailAddress();
        ThreadManager.getDefaultExecutorService().submit(new ForgetPasswordSendOTPApi(number, SharedPref.getDeviceUid(), getTransactionId(), SharedPref.getCountryCode()));

    }

    public void executeForgetPassOTPSendAPI(GenericStatusMsgResponse response, String result_status) {
        Intent i = new Intent(this, ForgetPasswordActivity.class);
        i.putExtra(TextConstants.RESET_PASS, true);
        startActivity(i);
    }


    @Subscribe
    public void onForgetPasswordSendOTPResponse(ForgetPasswordSendOTPResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            ForgetPasswordSendOTPResponseGson objectResponse = response.getForgetPasswordSendOTPResponse();
            if (objectResponse.getAlternateContact() != null)
                SharedPref.setForgetPasswordContactAltenate(objectResponse.getAlternateContact());
            else if (objectResponse.getStatus().equals("SC200") && objectResponse.getAlternateContact() == null) {
                if (SharedPref.getIsMobileEnabled())
                    SharedPref.setForgetPasswordContactAltenate(SharedPref.getMobileNumber());
                else if (!SharedPref.getIsMobileEnabled())
                    SharedPref.setForgetPasswordContactAltenate(SharedPref.getEmailAddress());

            }
            Intent i = new Intent(this, ForgetPasswordActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(i);
//            this.finish();
        } else if (response != null
                && response.getMsg() != null
                && (response.getMsg().equals(TextConstants.USER_DEVICE_TOKEN_NOT_FOUND)
                || response.getMsg().equals(TextConstants.ACTIVE_DEVICE_AND_ALTERNATE_CONTACT_NOT_FOUND))) {
            SharedPref.setForgetPasswordContactAltenate(null);
            Intent i = new Intent(this, ForgetPasswordActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            //  this.finish();
        } else {
            SharedPref.setResetPassword(false);
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void setIdInActivity(long id) {
        mTransactionId = id;
    }

    @Override
    public void setDefaultPassword(String password) {
        mDefaultPassword = password;
    }

    public void setOTPText(String code) {
        OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
        if (code.length() != 4)
            return;
        if (fragment == null || !fragment.isVisible()) {
            return;
        } else {
            fragment.displayOtpCode(code);
        }
    }


    /* Edited by Aastha */
    public void executeChangePassword(ForgetPassOtpVerifyResponse responseJson, String msg) {
        if (msg.equals(TextConstants.SUCESSFULL_API_CALL)) {
            userMO = SettingManager.getInstance().getUserMO();
            try {
                String temp_Pass = URLEncoder.encode(tempPassword, "UTF-8");
                userMO.setPassword(temp_Pass);
            } catch (Exception e) {
            }
            try {

                SettingManager.getInstance().updateUserMO(userMO);
            } catch (Exception e) {
                e.printStackTrace();
            }
            showDialogforPassword();
        } else {
            if (responseJson != null && (responseJson.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_422)) {
                if (responseJson.getMsg() != null) {
                    if (responseJson.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_LENGTH))
                        msg = getString(R.string.password_with_length_error, (SharedPref.getPasswordLength() + ""));
                    else if (responseJson.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_PATTERN))
                        msg = getString(R.string.password_pattern_error);
                    else if (responseJson.getMsg().equals(TextConstants.OLD_PASSWORD_INVALID))
                        msg = getString(R.string.old_password_incorrect);
                    else if (responseJson.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() == 0)
                        msg = getString(R.string.last_password_mismatch);
                    else if (responseJson.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() != 0)
                        msg = getString(R.string.last_n_password_mismatch, SharedPref.getPasswordHistoryCount() + "");
                    else
                        msg = TextConstants.UNEXPECTED_ERROR;
                } else
                    msg = TextConstants.UNEXPECTED_ERROR;
                showSnackBar(msg);
            } else
                showSnackBar(msg);
        }
    }

    private void showDialogforPassword() {
        //new design Dialog
        DialogUtils.showAlertDialogCommon(this, getString(R.string.password_updated), MyApplication.getAppContext().getResources().getString(R.string.password_changed_successful), MyApplication.getAppContext().getResources().getString(R.string.ok_got_it), null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popToLoginScreen = true;
                getFragmentManager().popBackStack();

            }
        });
    }

    private void showSnackBar(String error) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.WHITE);
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.RED);
        if (!error.equals(""))
            snackbar.show();
    }
    /*end of edited*/

    public void connectionPostExecute(int connectionState) {
        ErrorMessage localErrorMessage = new ErrorMessage(this, R.id.layoutParent);
        String message = MyApplication.getAppContext().getResources().getString(R.string.connection_not_available);
        switch (connectionState) {
            case DefaultConnection.STATE_NOT_CONNECTED:
                message = MyApplication.getAppContext().getResources().getString(R.string.connection_not_available);
                break;
            case DefaultConnection.STATE_NOT_AUTHENTICATED:
                message = TextConstants.INVALID_LOGIN;
                break;
            case DefaultConnection.STATE_AUTHENTICATED:
                try {
                    Utils.userLoggedinStatus(this, true);
                    startService(new Intent(this, MessengerService.class));
                    Utils.sendPresenceAvailable();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            default:
                break;
        }
        localErrorMessage.showMessage(message);
    }

    /*    handle patient show response and will not allow login if patient show fails*/
    public void executePatientProfileResponse(PatientMO patientMO) {

        userMO = SettingManager.getInstance().getUserMO();
        if (patientMO != null) {
            lauchHomeScreen();
            if (BuildConfig.isMRNFlowEnabled)
                checkMRNNumber(patientMO);
        } else {
            UserDBAdapter.getInstance(this).updateSalutation(userMO.getPersistenceKey(), TextConstants.NO);
            dismissProgressDialog();
            Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
        }

    }

    public void checkMRNNumber(PatientMO patientMO) {
        String mrnNumber = patientMO.getMRNNumber();
        if (mrnNumber != null && !mrnNumber.trim().equals("") && isValidMRNNumber(mrnNumber.trim())) {
            // the patient has a registered mrnNumber
            SharedPref.setIsMRNAvailable(true);
        } else {
            // we need to show the notification
            SharedPref.setIsMRNAvailable(false);
            NotificationCenter notificationObject = new NotificationCenter();
            notificationObject.setNotificationType(VariableConstants.MRN_NOTIFICATION);
            new DatabaseAsyncTask(notificationObject, DatabaseTables.NOTIFICATION_CENTER_TABLE)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    private boolean isValidMRNNumber(String mrnNumber) {
        if (mrnNumber.matches("[0-9]+") && mrnNumber.length() == 14 &&
                !mrnNumber.toLowerCase().contains(getString(R.string.mphrx))) {
            return true;
        }
        return false;
    }

    @Subscribe
    public void onFetchAgreementsResponse(FetchAllAgreementsResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            ArrayList<AgreementModel> arrayList = response.getAgreementModelsList();
            for (int i = 0; i < arrayList.size(); i++) {
                AgreementFileDBAdapter.getInstance(this).insertAgreementData(arrayList.get(i));
            }
            if (BuildConfig.isPatientApp) {
                /*Patient show call not used from Phloober onwards @Aastha */
                ThreadManager.getDefaultExecutorService().submit(new FetchSelfAndAssociatedPatients(getTransactionId(), TextConstants.FETCH_SELF));
            } else {
                ThreadManager.getDefaultExecutorService().submit(new PractionerShowRequest(getTransactionId(), null));
            }
        } else {
            //fetchALl agreements is unsuccessful ,hence blocking the user MNV-8921(aastha)
            dismissProgressDialog();
            showDialogforError();

        }
    }

    @Subscribe
    public void onGetLinkedPatientsResponse(FetchSelfAndAssociatedPatientsResponse fetchSelfAndAssociatedPatientsResponse) {
        if (mTransactionId != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;

        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful() && fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().size() > 0) {
            ArrayList<LinkedPatientInfoMO> linkedPatients = fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList();
            ArrayList<Coverage> coverages = linkedPatients.get(0).getCoverage();
            ArrayList<FetchUpdateTelecom> phoneList = linkedPatients.get(0).getPhoneList();
            ArrayList<FetchUpdateTelecom> emailList = linkedPatients.get(0).getEmailList();
            ArrayList<FetchUpdateIdentifier> identifiers = linkedPatients.get(0).getIdentifier();

            ContentValues contentValues = new ContentValues();
            if (coverages != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(coverages);
                    contentValues.put(UserDBAdapter.getCOVERAGE(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing phone list in UserMo
            if (phoneList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(phoneList);
                    contentValues.put(UserDBAdapter.getPHONELIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing emailList in usermo
            if (emailList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(emailList);
                    contentValues.put(UserDBAdapter.getEMAILLIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }
            //writing identifierlist in usermo
            if (identifiers != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(identifiers);
                    contentValues.put(UserDBAdapter.getIDENTIFIER(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            String maritalStaus = linkedPatients.get(0).getMaritalStatus();
            if (Utils.isValueAvailable(maritalStaus))
                contentValues.put(UserDBAdapter.getMARITALSTATUS(), maritalStaus);

            UserDBAdapter.getInstance(this).updateUserInfo(userMO.getPersistenceKey(), contentValues);

            if (Utils.isValueAvailable(linkedPatients.get(0).getProfilePic())) {
                Bitmap profile = Utils.convertBase64ToBitmap(linkedPatients.get(0).getProfilePic());
                if (profile != null)
                    UserProfilePic.saveToInternalStorage(profile, MyApplication.getAppContext());
            }

            if (SharedPref.getIsCoverageUpdated()) {

                dismissProgressDialog();
                lauchHomeScreen();
            } else {
                failAPI(userMO);
            }
        } else {

            failAPI(userMO);
        }
    }

    private void showDialogforError() {
        DialogUtils.showAlertDialogCommon(this, getString(R.string.success), getResources().getString(R.string.fetch_fail), getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void failAPI(UserMO userMO1) {
        if (userMO1 != null)
            UserDBAdapter.getInstance(this).updateSalutation(userMO1.getPersistenceKey(), TextConstants.NO);
        dismissProgressDialog();
        Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onSendOtpResponse(SendOtpResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            popToLoginScreen = false;
            if (response.isInfoStatus()) {
                if (!isResendOtpCode()) {
                    if (SharedPref.getIsMobileEnabled() && ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                        registerSMSReceiver();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString(TextConstants.LOGIN_ID, getLoginId());
                    addFragment(OTPFragment.getInstance(bundle));
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.otp_sent_successfully), Toast.LENGTH_SHORT).show();
                    Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof OTPFragment && SharedPref.getIsMobileEnabled()) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                            registerSMSReceiver();
                        }
                        ((OTPFragment) fragment).startTimerPatient();
                    } else
                        ((OTPFragment) fragment).setTextAndTimerVisibility(getResources().getString(R.string.sent_sms_non_mobile));
                }
            } else {
                dismissAndShowInfoAlert(response);
            }
        } else {
            dismissAndShowErrorAlert(response);
        }
    }

    private void dismissAndShowInfoAlert(SendOtpResponse response) {
        dismissProgressDialog();
        String responseStatus = response.getInfoResponseStatus();
        String code = response.getInfoCode();

        if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
            Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        else if (Utils.isValueAvailable(code)) {
            openActivity(ContactAdminActivity.class, null, true);
        } else
            Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
    }

    private void dismissAndShowErrorAlert(SendOtpResponse response) {
        dismissProgressDialog();
        String responseStatus = response.getErrorResponseStatus();
        String code = response.getErrorCode();

        if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
            Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        else if (Utils.isValueAvailable(code)) {
            String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
            Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
    }

    public void addFragment(BaseFragment fragment) {
        addFragment(fragment, R.id.fragment_container, true);
        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (popToLoginScreen && !(fragment instanceof LoginFragment)) {
            getFragmentManager().popBackStack();
        } else
            updateTitle(fragment);
    }

    private void updateTitle(Fragment f) {
        String className = f.getClass().getName();
        initToolbar();

        if (className.equals(LoginFragment.class.getName())) {
            mToolbar.setVisibility(View.GONE);
        } else if (className.equals(EnterPassword.class.getName())) {
            setToolbar(ENTER_PASSWORD);
        } else if (className.equals(OTPFragment.class.getName())) {
            setToolbar(OTP_VERIFICATION);
        }
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        popToLoginScreen = false;

        //  getFragmentManager().popBackStack();
    }

}
