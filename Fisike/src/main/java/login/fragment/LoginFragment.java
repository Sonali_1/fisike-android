package login.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.LoginSettings;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.utils.CountryCodeUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.CountryAdapter;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.network.request.LoginRequest;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import login.activity.LoginActivity;
import signup.activity.LoginSignupActivity;
import signup.activity.SignUpActivity;
import signup.request.SendOtpRequest;

/**
 * Created by Kailash Khurana on 6/19/2017.
 */

public class LoginFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {

    private CustomEditTextWhite edPassword;

    private String loginId;
    public LoginFragment.OnGetTransactionIdListener mListener;
    private long mTransactionId;
    private RelativeLayout layout_mobile_number;
    private Spinner spinner_change_country_code;
    private CustomEditTextWhite etEmailId;
    private CustomEditTextWhite etMobileNumber;
    private CustomEditTextWhite etUserName;
    private CountryAdapter adapter_country;
    private int position;
    private String number;
    private String countryCode,countryCodePlus;
    private CustomFontButton btnForgetPassword;
    private RadioGroup rgloginMenu;
    private RadioButton rbMobile, rbEmail, rbUsername;
    private int loginMenuVisibility;
    private int MOBILE = 0, EMAIL = 1, USERNAME = 2;
    private int DEFAULT_LOGIN = USERNAME;
    private boolean mobileNumberValid;
    private boolean usernameValid;
    private boolean emailValid;
    private String password;
    private static final int LOGIN_FLOW = 4;
    private static final int FORGET_PASSWORD = 5;
    private int currentFlow;
    int country_code = 0;
    String countryId = "";

    public boolean isMobileNumberValid(int flow) {
        mobileNumberValid = true;
        if (!Utils.isValueAvailable(loginId)) {
            etMobileNumber.setError(getActivity().getResources().getString(R.string.enter_mobile_number_sign_in));
            mobileNumberValid = false;
        }
        else if (Integer.parseInt(countryCode) == TextConstants.INDIA_COUNTRYCODE && (loginId.length() != TextConstants.INDIA_PHONENO_LENGTH)) {
            etMobileNumber.setError(getActivity().getResources().getString(R.string.error_invalid_mobile_no));
            mobileNumberValid = false;
        } else if ((countryCodePlus.length() + loginId.length()) <= TextConstants.MIN_PHONENO_LENGTH) {
            etMobileNumber.setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            mobileNumberValid = false;
        } else if ((countryCodePlus.length() + loginId.length()) > TextConstants.MAX_PHONENO_LENGTH) {
            etMobileNumber.setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            mobileNumberValid = false;
        }
        if (loginId.equals("0000000000")) {
            etMobileNumber.setError("");
            startActivity(new Intent(getActivity(), LoginSettings.class).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            mobileNumberValid = false;
        }
        if(isPasswordBlank(password,flow))
            mobileNumberValid =false;

        return mobileNumberValid;
    }

    public boolean isEmailValid(int flow) {
        emailValid = true;
        if (!Utils.isValueAvailable(loginId)) {
            etEmailId.setError(getActivity().getResources().getString(R.string.enter_email_address));
            emailValid = false;
        } else if (!Utils.isEmailValid(loginId)) {
            etEmailId.setError(getActivity().getResources().getString(R.string.enter_valid_email_address));
            emailValid = false;
        } else if (loginId.equals("mphrx") && edPassword.getText().toString().equals("MphRxP0C")) {
            etEmailId.setError("");
            startActivity(new Intent(getActivity(), LoginSettings.class).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            emailValid = false;
        }
        if(isPasswordBlank(password,flow))
            emailValid =false;
        return emailValid;
    }

    public boolean isUsernameValid(int flow) {
        usernameValid = true;
        if (!Utils.isValueAvailable(loginId)) {
            etUserName.setError(getActivity().getResources().getString(R.string.enter_user_name));
            usernameValid = false;
        } else if (loginId.equals("mphrx") && edPassword.getText().toString().equals("MphRxP0C")) {
            etUserName.setError("");
            startActivity(new Intent(getActivity(), LoginSettings.class).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            usernameValid = false;
        }
        if(isPasswordBlank(password,flow))
            usernameValid =false;
        return usernameValid;
    }


    public interface OnGetTransactionIdListener {
        public void setIdInActivity(long id);

        public void setDefaultPassword(String password);
    }

    public static LoginFragment getInstance(Bundle bundle) {
        LoginFragment fragment = new LoginFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_login;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mListener = (LoginFragment.OnGetTransactionIdListener) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");
        else {
            layout_mobile_number = (RelativeLayout) getView().findViewById(R.id.mobile_number_flow);
            spinner_change_country_code = (Spinner) getView().findViewById(R.id.spinner_change_country_code);
            etMobileNumber = (CustomEditTextWhite) getView().findViewById(R.id.et_mobile_number);
            etEmailId = (CustomEditTextWhite) getView().findViewById(R.id.et_email);
            etUserName = (CustomEditTextWhite) getView().findViewById(R.id.et_username);
            edPassword = (CustomEditTextWhite) getView().findViewById(R.id.et_pass);
            btnForgetPassword = (CustomFontButton) getView().findViewById(R.id.btn_forget_password);
            rgloginMenu = (RadioGroup) getView().findViewById(R.id.rb_login_menu);
            rbMobile = (RadioButton) getView().findViewById(R.id.rb_mobile);
            rbEmail = (RadioButton) getView().findViewById(R.id.rb_email);
            rbUsername = (RadioButton) getView().findViewById(R.id.rb_username);
            btnForgetPassword.setVisibility(BuildConfig.isForgetPassword ? View.VISIBLE : View.GONE);
            getView().findViewById(R.id.btn_sign_up).setVisibility(BuildConfig.isPatientApp && SharedPref.getIsSelfSignUpEnabled() ? View.VISIBLE : View.GONE);
            rgloginMenu.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void initView() {
        ((LoginActivity)getActivity()).mToolbar.setVisibility(View.GONE);
        SharedPref.setMobileEnabled(false);
        SharedPref.setUserNameEnabled(false);
        SharedPref.setIsEmailEnabled(false);
        initRadioGroupView();
        manageRadioButtonsVisibility();
        if (SharedPref.getAdminCreatedPasswordNeverChanged()) {
            edPassword.setHint(SharedPref.getDefaultPasswordType());
        } else if (SharedPref.getUserType().equals(SharedPref.UserType.PARTIALLY_REGISTERED.getTypeString
                ()) && !BuildConfig.isPatientApp) {
            edPassword.setHint(R.string.forgot_password_new_password);
        } else if (SharedPref.getUserType().equals(SharedPref.UserType.REGISTERED.getTypeString())) {
            edPassword.setHint(getActivity().getResources().getString(R.string.password));
        } else if ((SharedPref.isSignedUpUser() || !SharedPref.isVerifiedUser()) && BuildConfig.isPatientApp) {
            edPassword.setHint(getActivity().getResources().getString(R.string.password));
        }

        if (DEFAULT_LOGIN == MOBILE) {

            initCountryCode();

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE);

            if (sharedPreferences.contains(VariableConstants.ERROR_MESSAGE_LOGIN) && sharedPreferences.getString(VariableConstants.ERROR_MESSAGE_LOGIN, "").equals(VariableConstants.REVOKE_USER)) {
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getResources().getString(R.string.device_deactive_title), getActivity().getResources().getString(R.string.device_deactive_msg), getActivity().getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                sharedPreferences.edit().putString(VariableConstants.ERROR_MESSAGE_LOGIN, "").commit();
            }

        }

        Utils.hideKeyboard(getActivity());
    }

    private void initCountryCode() {
        TelephonyManager telMgr = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_READY:
                setCountryCode(true);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                setCountryCode(false);
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int radiobutton) {
        edPassword.setError("");
        edPassword.setText("");
        switch (radiobutton) {
            case R.id.rb_mobile:
                etMobileNumber.setError("");
                etMobileNumber.requestFocus();
                layout_mobile_number.setVisibility(View.VISIBLE);
                initCountryCode();
                etEmailId.setVisibility(View.GONE);
                etUserName.setVisibility(View.GONE);
                DEFAULT_LOGIN = MOBILE;
                break;
            case R.id.rb_email:
                etEmailId.setError("");
                etEmailId.requestFocus();
                layout_mobile_number.setVisibility(View.GONE);
                etEmailId.setVisibility(View.VISIBLE);
                etUserName.setVisibility(View.GONE);
                DEFAULT_LOGIN = EMAIL;
                break;
            case R.id.rb_username:
                etUserName.setError("");
                etUserName.requestFocus();
                layout_mobile_number.setVisibility(View.GONE);
                etEmailId.setVisibility(View.GONE);
                etUserName.setVisibility(View.VISIBLE);
                DEFAULT_LOGIN = USERNAME;
                break;
        }
    }

    private void manageRadioButtonsVisibility() {
        if (loginMenuVisibility == View.VISIBLE) {
            if (BuildConfig.isPhoneEnabled) {
                rbMobile.setVisibility(View.VISIBLE);
                if (DEFAULT_LOGIN == MOBILE) {
                    rbMobile.setChecked(true);
                }
            }
            if (BuildConfig.isUserNameEnabled) {
                rbUsername.setVisibility(View.VISIBLE);
                if (DEFAULT_LOGIN == USERNAME) {
                    rbUsername.setChecked(true);
                }
            }
            if (BuildConfig.isEmailEnabled) {
                rbEmail.setVisibility(View.VISIBLE);
                if (DEFAULT_LOGIN == EMAIL) {
                    rbEmail.setChecked(true);
                }
            }
        } else if (loginMenuVisibility == View.GONE) {
            if (BuildConfig.isPhoneEnabled) {
                rbMobile.setVisibility(View.VISIBLE);
                layout_mobile_number.setVisibility(View.VISIBLE);
                DEFAULT_LOGIN = MOBILE;
            } else if (BuildConfig.isEmailEnabled) {
                rbEmail.setVisibility(View.VISIBLE);
                etEmailId.setVisibility(View.VISIBLE);
                DEFAULT_LOGIN = EMAIL;
            } else if (BuildConfig.isUserNameEnabled) {
                rbUsername.setVisibility(View.VISIBLE);
                etUserName.setVisibility(View.VISIBLE);
                DEFAULT_LOGIN = USERNAME;
            }
        }
    }

    private void initRadioGroupView() {
        int count = 0;
        if (BuildConfig.isPhoneEnabled)
            count++;
        if (BuildConfig.isUserNameEnabled)
            count++;
        if (BuildConfig.isEmailEnabled)
            count++;
        if (count > 1)
            loginMenuVisibility = View.VISIBLE;
        else
            loginMenuVisibility = View.GONE;
        rgloginMenu.setVisibility(loginMenuVisibility);
    }

    public void setCountryCode(boolean on) {
        if (on) {
            try {
                countryId = CountryCodeUtils.getCountryId(getActivity());
                country_code = Integer.parseInt(CountryCodeUtils.getCountryZipCode(getActivity(), countryId).toString().trim());
            } catch (Exception e) {
                setDefaultCountryCodes();
            }
        } else if (!on) {
            setDefaultCountryCodes();
        }
        if(BuildConfig.isShowMultipleCountryCodes)
            position = CountryCodeUtils.getCountryPosition(getActivity(), country_code, countryId);
        else
            position = 0;

        spinner_change_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                countryCode =  stringTokenizer.nextToken().toString().trim();
                countryCodePlus = "+" + countryCode;
                if (Integer.parseInt(countryCode)== TextConstants.INDIA_COUNTRYCODE) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    etMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    /*[  .... Moblile length set on   [ max_length- countrycode(including+) ]   ........  ]*/
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countryCodePlus.length()));
                    etMobileNumber.setFilters(FilterArray);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        if(BuildConfig.isShowMultipleCountryCodes)
          countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        else
        {
            if(TextConstants.DEFAULT_COUNTRY_INDIA)
                countrylist.add(String.valueOf(getResources().getString(R.string.india_country)));
            else
                countrylist.add(String.valueOf(getResources().getString(R.string.us_country)));
        }
        adapter_country = new CountryAdapter(getActivity(), countrylist, "white");
        spinner_change_country_code.setAdapter(adapter_country);
        spinner_change_country_code.setSelection(position);
    }

    private void setDefaultCountryCodes() {
        if(TextConstants.DEFAULT_COUNTRY_INDIA)
        {
            countryId = TextConstants.IN_COUNTRYNAME;
            country_code = TextConstants.INDIA_COUNTRYCODE;
        }
        else
        {
            countryId = TextConstants.US_COUNTRYNAME;
            country_code = TextConstants.US_COUNTRYCODE;
        }
    }

    @Override
    public void bindView() {
        spinner_change_country_code.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.hideKeyboard(getActivity());
                return false;
            }
        });

        edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        edPassword.setPasswordKeyListener();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:
                Intent newAccount = new Intent(getActivity(), SignUpActivity.class);
                startActivity(newAccount);
                break;
            case R.id.btn_proceed:
                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                    currentFlow = LOGIN_FLOW;
                    onClickSubmit(LOGIN_FLOW);
                }
                break;
            case R.id.btn_forget_password:
                currentFlow = FORGET_PASSWORD;
                onClickSubmit(FORGET_PASSWORD);
                break;
        }
    }

    private void forgetPassword() {
        if(Utils.showDialogForNoNetwork(getActivity(),false)){
            showProgressDialog(getResources().getString(R.string.loading_txt));
            if(SharedPref.getIsMobileEnabled())
                ((LoginActivity)getActivity()).setLoginId(countryCodePlus+loginId);
            else
                ((LoginActivity)getActivity()).setLoginId(loginId);
            mTransactionId = System.currentTimeMillis();
            mListener.setIdInActivity(mTransactionId);
            ((LoginSignupActivity)getActivity()).setResendOTPCode(false);
            ThreadManager.getDefaultExecutorService().submit(new SendOtpRequest(loginId,"",TextConstants.FORGOT_PASSWORD,false,mTransactionId,SharedPref.getDeviceUid()));
        }
    }

    private void onClickSubmit(int loginFlow) {
           setSharedPrefsAndValidate(loginFlow);
    }

    private void setSharedPrefsAndValidate(int loginFlow) {
        hideKeyboard();
        password = edPassword.getText().toString().trim();
        if (DEFAULT_LOGIN == MOBILE) {
            loginId = etMobileNumber.getText().toString();
            SharedPref.setMobileEnabled(true);
            SharedPref.setIsEmailEnabled(false);
            SharedPref.setUserNameEnabled(false);
            SharedPref.setMobileNumber(loginId);
            SharedPref.setCountryCode(countryCodePlus);
            if (!isMobileNumberValid(loginFlow)) {
                return;
            }
        } else if (DEFAULT_LOGIN == EMAIL) {
            loginId = etEmailId.getText().toString();
            SharedPref.setMobileEnabled(false);
            SharedPref.setIsEmailEnabled(true);
            SharedPref.setUserNameEnabled(false);
            SharedPref.setEmailAddress(loginId);
            if (!isEmailValid(loginFlow)) {
                return;
            }
        } else if (DEFAULT_LOGIN == USERNAME) {
            loginId = etUserName.getText().toString().trim();
            SharedPref.setMobileEnabled(false);
            SharedPref.setIsEmailEnabled(false);
            SharedPref.setUserNameEnabled(true);
            SharedPref.setLoginUserName(loginId);
            if (!isUsernameValid(loginFlow)) {
                return;
            }
        }
        CheckMultiplePermission(loginFlow);
    }

    private boolean isPasswordBlank(String password,int loginflow) {
        if (!Utils.isValueAvailable(password) && loginflow == LOGIN_FLOW) {
            edPassword.setError(MyApplication.getAppContext().getResources().getString(R.string.enter_yourpassword));
            return true;
        }else if(loginflow == FORGET_PASSWORD)
            return false;
        return false;
    }

    private boolean isUsernameBlank() {
        if (etMobileNumber.getText().toString().trim().equals("")) {
            etMobileNumber.setError(getActivity().getResources().getString(R.string.enter_mobile_number));
            return true;
        }
        return false;

    }

    private void CheckMultiplePermission(int loginFlow) {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add(getActivity().getString(R.string.permission_phone_state));

        /*We dont need SMS permission for Login @Aastha for Phloober*/
        if(loginFlow == FORGET_PASSWORD)
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add(getActivity().getString(R.string.permission_sms_recieve));

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = getActivity().getString(R.string.permission_msg) + " " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    FragmentCompat.requestPermissions(LoginFragment.this, permissionsList.toArray(new String[permissionsList.size()]),
                                            TextConstants.PERMISSION_REQUEST_CODE);
                                }
                            }
                        });
                return;
            }
            //System dialogs generated for each permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                FragmentCompat.requestPermissions(LoginFragment.this, permissionsList.toArray(new String[permissionsList.size()]),
                        TextConstants.PERMISSION_REQUEST_CODE);
            }
            return;
        }
        if(loginFlow == LOGIN_FLOW)
            loginWithNumberAndPassword();
        else
            forgetPassword();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                permissionsList.add(permission);
                return false;
            } else if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && !FragmentCompat.shouldShowRequestPermissionRationale(LoginFragment.this, permission)) {
                permissionsList.add(permission);
                return false;
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.allow), okListener)
                .setNegativeButton(getResources().getString(R.string.deny), null)
                .create()
                .show();
    }

    /*called for change mobile number and login flow*/
    /*private void getOtp() {

        if (Utils.showDialogForNoNetwork(getActivity(), false)) {

            if (SharedPref.getIsMobileEnabled()) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                countryCode = "+" + stringTokenizer.nextToken().toString().trim();
                number = countryCode + etMobileNumber.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            } else if (SharedPref.getIsUserNameEnabled()) {
                number = etUserName.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            } else {
                number = etEmailId.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            }
            //TODO need to add userType in change phone number flow

            if (isChangeNumberMode) { //to be checked in case of email
                if (SharedPref.getIsMobileEnabled()) {
                    SharedPref.setOldMobileNumber(SharedPref.getMobileNumber());
                    SharedPref.setOldCountryCode(SharedPref.getCountryCode());
                    SharedPref.setMobileNumber(number);
                    SharedPref.setCountryCode(countryCode);
                    disableAccount = SharedPref.isDisableAccount();
                } else {
                    SharedPref.setoldEmailAddress(SharedPref.getEmailAddress());
                    SharedPref.setEmailAddress(number);
                    disableAccount = false;
                }

                mTransactionId = getTransactionId();
                mListener.setIdInActivity(mTransactionId);
                ThreadManager.getDefaultExecutorService().submit(new CheckPhoneNoExistRequest(number, mTransactionId, disableAccount));
            } else {
                loginWithNumberAndPassword();
            }
        }
    }*/


    private void loginWithNumberAndPassword() {
        mListener.setDefaultPassword(password);
        showProgressDialog(getResources().getString(R.string.progress_loading));
        mTransactionId = getTransactionId();
        mListener.setIdInActivity(mTransactionId);
        ThreadManager.getDefaultExecutorService().submit(new LoginRequest(loginId, password,
                mTransactionId));

    }

    @Override
    public void onPermissionGranted(String permission) {
        if(currentFlow ==LOGIN_FLOW)
          loginWithNumberAndPassword();
        else
            forgetPassword();
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                //denied
                DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.deny_permission, getActivity().getResources().getString(R.string.app_name)), getActivity().getResources().getString(R.string.btn_ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                return;
            }
            DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.permission_require_message), getActivity().getResources().getString(R.string.grant_permission), getActivity().getResources().getString(R.string.exit_app), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (which == AlertDialog.BUTTON_POSITIVE) {
                        FragmentCompat.requestPermissions(LoginFragment.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                                TextConstants.PERMISSION_REQUEST_CODE);
                    } else if (which == AlertDialog.BUTTON_NEGATIVE) {
                        LoginFragment.this.getActivity().onBackPressed();
                    }
                }
            });
        }
    }

    public void setError(String error) {
        etMobileNumber.setError(error);
    }

    /*private class InternalTextWatcher implements TextWatcher {
        View view;

        public InternalTextWatcher(CustomEditTextWhite view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    etEmailId.setErrorEnabled(false);
                    etEmailId.setError(getActivity().getResources().getString(R.string.no_error));
                    etEmailId.setError(null);
                    break;
                case R.id.et_mobile_number:
                    etMobileNumber.setErrorEnabled(false);
                    etMobileNumber.setError(getActivity().getResources().getString(R.string.no_error));
                    etMobileNumber.setError(null);
                    break;
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    etEmailId.setErrorEnabled(false);
                    etEmailId.setError(getActivity().getResources().getString(R.string.no_error));
                    etEmailId.setError(null);
                    break;
                case R.id.et_mobile_number:
                    etMobileNumber.setErrorEnabled(false);
                    etMobileNumber.setError(getActivity().getResources().getString(R.string.no_error));
                    etMobileNumber.setError(null);
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    if (SharedPref.getEmailAndMobile()) {
                        if (!etMobileNumber.getText().equals(""))
                            etMobileNumber.setText("");
                        SharedPref.setMobileEnabled(false);
                    }
                    etEmailId.setError("");
                    etMobileNumber.setError("");
                    break;
                case R.id.et_mobile_number:
                    if (SharedPref.getEmailAndMobile()) {
                        if (!etEmailId.getText().equals(""))
                            etEmailId.setText("");
                        SharedPref.setMobileEnabled(true);
                        etMobileNumber.setErrorEnabled(false);
                    }
                    etMobileNumber.setError("");
                    etEmailId.setError("");
                    break;
            }
        }*/

}
