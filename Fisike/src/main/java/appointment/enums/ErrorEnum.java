package appointment.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.enums.DesignationEnum;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by laxmansingh on 6/6/2017.
 */

public enum ErrorEnum {
    SOMETHING_WENT_WRONG(MyApplication.getAppContext().getString(R.string.Something_went_wrong_key), R.string.Something_went_wrong),
    CHECK_INTERNET_CONNECTION(MyApplication.getAppContext().getString(R.string.check_Internet_Connection_key), R.string.check_Internet_Connection),
    SOMETHING_WENT_WRON(MyApplication.getAppContext().getString(R.string.Something_went_wron_key), R.string.Something_went_wron),
    CHECK_INTERNET_CONNECTIO(MyApplication.getAppContext().getString(R.string.check_Internet_Connectio_key), R.string.check_Internet_Connectio),
    UNEXPECTED_ERROR(MyApplication.getAppContext().getString(R.string.unexpected_error_key), R.string.unexpected_error),
    ERR_NETWORK(MyApplication.getAppContext().getString(R.string.err_network_key), R.string.err_network),;


    private String code;
    private int value;
    private static LinkedHashMap<String, ErrorEnum> errorEnumLinkedHashMap = null;


    private ErrorEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ErrorEnum> getDesignationEnumLinkedHashMap() {
        if (errorEnumLinkedHashMap == null) {
            errorEnumLinkedHashMap = new LinkedHashMap<String, ErrorEnum>();
            for (ErrorEnum vitals : ErrorEnum.values()) {
                errorEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return errorEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getDesignationEnumLinkedHashMap();
        for (Map.Entry<String, ErrorEnum> enumMap : errorEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getDesignationEnumLinkedHashMap();
        if (errorEnumLinkedHashMap != null) {
            for (Map.Entry<String, ErrorEnum> enumMap : errorEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }

}

