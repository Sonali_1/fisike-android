package appointment.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by laxmansingh on 5/4/2017.
 */

public enum AppointmentTypeEnum {

    DOCTORS(MyApplication.getAppContext().getString(R.string.doctors_key), R.string.doctors_appointment),
    SERVICES(MyApplication.getAppContext().getString(R.string.services_key), R.string.services_appointment),
    SPECIALITIES(MyApplication.getAppContext().getString(R.string.specialities_key), R.string.specialities_appointment),
    HOMESAMPLECOLLECTION(MyApplication.getAppContext().getString(R.string.homesamplecollection_key), R.string.homesamplecollection_appointment),
    LABTESTS(MyApplication.getAppContext().getString(R.string.labtests_key), R.string.labtests_appointment),
    DOCTORS_SUBTITLE(MyApplication.getAppContext().getString(R.string.doctors_subtitle_key), R.string.doctors_subtitle_appointment),
    SERVICES_SUBTITLE(MyApplication.getAppContext().getString(R.string.services_subtitle_key), R.string.services_subtitle_appointment),
    SPECIALITIES_SUBTITLE(MyApplication.getAppContext().getString(R.string.specialities_subtitle_key), R.string.specialities_subtitle_appointment),
    HOMESAMPLECOLLECTION_SUBTITLE(MyApplication.getAppContext().getString(R.string.homesamplecollection_subtitle_key), R.string.homesamplecollection_subtitle_appointment),
    LABTESTS_SUBTITLE(MyApplication.getAppContext().getString(R.string.labtests_subtitle_key), R.string.labtests_subtitle_appointment),
    BOOKHOMEDRAW(MyApplication.getAppContext().getString(R.string.book_homedraw_key), R.string.book_homedraw_value),
    VISITPATIENTSERVICE(MyApplication.getAppContext().getString(R.string.visit_patient_center_key), R.string.visit_patient_center_value);

    private String code;
    private int value;
    private static LinkedHashMap<String, AppointmentTypeEnum> AppointmentTypeEnumLinkedHashMap = null;


    private AppointmentTypeEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }


    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, AppointmentTypeEnum> getAppointmentTypeEnumLinkedHashMap() {
        if (AppointmentTypeEnumLinkedHashMap == null) {
            AppointmentTypeEnumLinkedHashMap = new LinkedHashMap<String, AppointmentTypeEnum>();
            for (AppointmentTypeEnum vitals : AppointmentTypeEnum.values()) {
                AppointmentTypeEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return AppointmentTypeEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getAppointmentTypeEnumLinkedHashMap();
        for (Map.Entry<String, AppointmentTypeEnum> enumMap : AppointmentTypeEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getAppointmentTypeEnumLinkedHashMap();
        if (AppointmentTypeEnumLinkedHashMap != null) {
            for (Map.Entry<String, AppointmentTypeEnum> enumMap : AppointmentTypeEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
