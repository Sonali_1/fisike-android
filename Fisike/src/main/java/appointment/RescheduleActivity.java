package appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import appointment.adapter.SpinAdapter;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.appointmodel.MinutesDuration;
import appointment.request.AppointUpdateRequest;
import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 2/25/2016.
 */
public class RescheduleActivity extends BaseActivity implements View.OnClickListener, AppointmentResponseCallback {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private Context mContext;
    private Button bt_next;
    private Spinner spDate, spTime;
    private String starttime, endtime;
    private ProgressDialog pdialog;
    private String position = "";
    private String cancel_or_reschedule = "";
    private AppointmentResponseCallback responseCallback;

    /*[  Adapter variables   ]*/
    private SpinAdapter day_adapter;
    private SpinAdapter time_adapter;
    private List<String> day_list = new ArrayList<String>();
    private List<String> time_list = new ArrayList<String>();
    private appointment.model.appointmodel.List appointmentmodel;
    private FrameLayout frameLayout;
    /*[  Adapter variables   ]*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.reschedule_activity);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.reschedule_activity, frameLayout);
        mContext = this;
        responseCallback = this;
        findView();
        initView();
        bindView();

        Bundle bundle = getIntent().getExtras();
        position = bundle.getString("position");
        cancel_or_reschedule = bundle.getString("rebook_or_reschedule");
        appointmentmodel = bundle.getParcelable("reschedule_appointmentmodel");

    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        spDate = (Spinner) findViewById(R.id.spinner_date);
        spTime = (Spinner) findViewById(R.id.spinner_time);
        bt_next = (Button) findViewById(R.id.bt_next);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_reschedule));

        bt_next.setText(getResources().getString(R.string.txt_save));

        day_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, day_list, TextConstants.DATE_TEXT);
        spDate.setAdapter(day_adapter);

        time_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, time_list, TextConstants.TIME_TEXT);
        spTime.setAdapter(time_adapter);
    }

    private void bindView() {
        bt_next.setOnClickListener(this);


        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  OrderSummary.getInstance().setTime(spTime.getSelectedItem().toString().trim());
                Calendar calendar = Calendar.getInstance();
                // get a date to represent "today"
                Date today = calendar.getTime();
                // add one day to the date/calendar
                calendar.add(Calendar.DAY_OF_YEAR, position);
                // now get "tomorrow"
                Date formatted = calendar.getTime();
                //   OrderSummary.getInstance().setDate(Utils.getFormattedDate(formatted));

                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_today));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_tomorrow));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

//............... Changeing time on changeing date.....
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, dateInString);
                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, enddateInString);
                //..........End of finding end date time..............

                // ............... End of Changeing time on changeing date.....


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  OrderSummary.getInstance().setTime(parent.getItemAtPosition(position).toString());

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, dateInString);
                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, enddateInString);
                //..........End of finding end date time..............
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_next:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    return;
                } else {
                    pdialog = new ProgressDialog(mContext);
                    pdialog.setMessage(getResources().getString(R.string.Rescheduling));
                    pdialog.setCanceledOnTouchOutside(false);
                    pdialog.show();
                    long transactionId = getTransactionId();


                    int minsDuration = (int) new CommonTasks().findDateDifferenceinMins(starttime, endtime, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);

                    /*[   ......  important code to change the start time and end time to reschedule......  ]*/
                    if (appointmentmodel.getMinutesDuration() == null) {
                        List<Object> extension = new ArrayList<Object>();
                        MinutesDuration minutesDuration = new MinutesDuration();
                        minutesDuration.setExtension(extension);
                        minutesDuration.setValue(minsDuration);
                        appointmentmodel.setMinutesDuration(minutesDuration);
                    } else {
                        appointmentmodel.getMinutesDuration().setValue(minsDuration);
                    }
                    appointmentmodel.getStart().setValue(starttime);
                    appointmentmodel.getEnd().setValue(endtime);
                    /*[   ...... End of important code to change the start time and end time to reschedule......  ]*/
                    ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, responseCallback, appointmentmodel, cancel_or_reschedule, "", ""));
                }
                break;

            default:
                break;

        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public void setTimeAdapter(String whichday) throws ParseException {
        if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
        /*[   Checking time must shown on spinner greater then current time  ]*/
            String lasttime = null;
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);
            String strdate = ft.format(date);
            Date newdate = null;
            try {
                newdate = ft.parse(strdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            time_list.clear();
            int length = getResources().getStringArray(R.array.time_units).length;
            int i = 0;
            for (String time : getResources().getStringArray(R.array.time_units)) {
                Date tempdate = null;
                try {
                    tempdate = ft.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (newdate.before(tempdate)) {
                    String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    time_list.add(new_time);
                } else {
                }
                if (i == length - 1) {
                    lasttime = time;
                    if (newdate.after(ft.parse(lasttime))) {
                        time_list.clear();
                        for (int j = 0; j < getResources().getStringArray(R.array.time_units).length; j++) {
                            String new_time = DateTimeUtil.convertSourceDestinationDateLocale(getResources().getStringArray(R.array.time_units)[j], DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                            time_list.add(new_time);
                        }
                    }
                }
                i++;
            }

            //.......... checking for days last time and set it to next day......
            if (newdate.after(ft.parse(lasttime))) {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            } else {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_today));
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            }
            //.......... End of checking for days last time and set it to next day......

            day_adapter.notifyDataSetChanged();
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        } else if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
            time_list.clear();
            for (String time : getResources().getStringArray(R.array.time_units)) {
                String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                time_list.add(new_time);
            }
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        }
/*[   End of Checking time must shown on spinner greater then current time  ]*/
    }

    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pdialog.isShowing()) {
                        pdialog.dismiss();
                        pdialog = null;
                    }
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        Toast.makeText(RescheduleActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                    } else {

                        JSONObject mainJasonObject = null;
                        try {
                            mainJasonObject = new JSONObject(response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1001")) {
                            DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.time_slot_unavailable), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1002")) {
                            DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.patient_not_register), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else if (mainJasonObject.optString("httpStatusCode").equals("500") && mainJasonObject.optString("status").equals("1004")) {
                            DialogUtils.showErrorDialog(mContext, "Alert", mContext.getResources().getString(R.string.appointment_cant_rescheduled_contact_admin), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
                                if (position.length() > 0 && cancel_or_reschedule.length() > 0) {
                                    Gson gson = new Gson();
                                    appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
                                    Intent intent = new Intent();
                                    intent.putExtra("position", position + "");
                                    intent.putExtra("cancel_or_reschedule", TextConstants.RESCHEDULE);
                                    intent.putExtra("appointment_model", appointmentmodel);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {
    }
}


