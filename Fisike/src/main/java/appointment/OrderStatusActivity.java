package appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import appointment.adapter.OrderStatusAdapter;
import appointment.interfaces.ResponseCallback;
import appointment.model.appointmodel.Extension;
import appointment.model.appointmodel.Value;
import appointment.request.AppointmentShowRequest;
import appointment.utils.CommonTasks;
import appointment.utils.CustomTypefaceSpan;
import appointment.utils.SimpleDividerItemDecorationHeight;

/**
 * Created by laxmansingh on 3/2/2016.
 */

public class OrderStatusActivity extends BaseActivity implements View.OnClickListener, ResponseCallback {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private RecyclerView rv_order_status;
    private OrderStatusAdapter adapterOrderStatus;
    private SpannableString instruction_styledString;
    private Typeface font_bold, font_regular;


    private TextView tv_timing, tv_desc, tv_price, tv_type, tv_ref_id;
    private IconTextView iv_ic_menu_overflow;

    private List<appointment.model.appointmodel.List> list = new ArrayList<appointment.model.appointmodel.List>();
    private List<Extension> statusList = new ArrayList<Extension>();

    private Bundle bundle;
    private String date, status, desc, ref_id;
    private int appointmentID;
    private ProgressDialog pdialog;
    private ResponseCallback responseCallback;
    private List<Value> valueList = new ArrayList<Value>();
    private RelativeLayout rlLayoutRecentPopular;
    private View rlMyOrder;
    private FrameLayout frameLayout;
    private IconTextView tv_currency;

    public static void newInstance(Context context, Bundle bundle) {
        Intent intent = new Intent(context, OrderStatusActivity.class);
        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.orderstatus_activity, frameLayout);
        mContext = this;
        findView();
        initView();
        bindView();
    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rv_order_status = (RecyclerView) findViewById(R.id.rv_order_status);

        tv_timing = (TextView) findViewById(R.id.tv_timing);
        tv_desc = (TextView) findViewById(R.id.tv_desc);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_ref_id = (TextView) findViewById(R.id.tv_ref_id);
        tv_currency = (IconTextView) findViewById(R.id.img_currency);

        iv_ic_menu_overflow = (IconTextView) findViewById(R.id.iv_ic_menu_overflow);
        iv_ic_menu_overflow.setVisibility(View.GONE);
        tv_price.setVisibility(View.GONE);
        tv_currency.setVisibility(View.GONE);

        rlMyOrder = (View) findViewById(R.id.my_order_lyt);
        rlLayoutRecentPopular = (RelativeLayout) findViewById(R.id.rel_lyt_recent_popular);

        font_bold = Typeface.createFromAsset(mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_bold));
        font_regular = Typeface.createFromAsset(mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_regular));

    }

    private void initView() {

        responseCallback = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_appointment_header));


        if (getIntent().getBundleExtra("bundle").getLong("appointmentID", 0) == 0) {
            bundle = getIntent().getBundleExtra("bundle");
            date = bundle.getString("date");
            status = bundle.getString("status");
            desc = bundle.getString("desc");
            ref_id = bundle.getString("ref_id");

            list = bundle.getParcelableArrayList("appointment_list");
            statusList = list.get(0).getExtension();
            for (int j = 0; j < statusList.size(); j++) {
                if (statusList.get(j).getUrl().trim().toString().equalsIgnoreCase("audit")) {
                    valueList = statusList.get(j).getValue();
                    break;
                }
            }
            setAdapter(valueList);
            tv_timing.setText(date);
            tv_desc.setText(desc);
            tv_type.setText(status);
            if (ref_id != null && !ref_id.equalsIgnoreCase("")) {
                instruction_styledString = new SpannableString(MyApplication.getAppContext().getResources().getString(R.string.ref_id) + ref_id);
                instruction_styledString.setSpan(new CustomTypefaceSpan("", font_bold), 0, 7, 0);
                instruction_styledString.setSpan(new CustomTypefaceSpan("", font_regular), 8, instruction_styledString.length(), 0);
                tv_ref_id.setText(instruction_styledString);
                tv_ref_id.setVisibility(View.VISIBLE);
            }

            appointment.model.appointmodel.List list_model = list.get(0);

        } else {
            appointmentID = (int) getIntent().getBundleExtra("bundle").getLong("appointmentID", 0);
            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                return;
            } else {
                rlMyOrder.setVisibility(View.GONE);
                rlLayoutRecentPopular.setVisibility(View.GONE);
                pdialog = new ProgressDialog(mContext);
                pdialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.loading_txt));
                pdialog.setCanceledOnTouchOutside(false);
                pdialog.show();
                long transactionId = System.currentTimeMillis();
                ThreadManager.getDefaultExecutorService().submit(new AppointmentShowRequest(transactionId, responseCallback, appointmentID));
            }
        }
    }

    private void bindView() {
        iv_ic_menu_overflow.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_ic_menu_overflow:
                showPopup(iv_ic_menu_overflow);
                break;

            default:
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(mContext, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_upcoming, popup.getMenu());

        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.view_instruction:
                        Intent intent = new Intent(mContext, ViewInstructionsActivity.class);
                        mContext.startActivity(intent);

                        break;
                    case R.id.reschedule:
                        Intent intents = new Intent(mContext, RescheduleActivity.class);
                        mContext.startActivity(intents);
                        break;

                    case R.id.cancel:
                        break;

                    default:
                        break;
                }

                return true;
            }
        });

    }


    @Override
    public void passResponse(final String response, boolean mIsSuccessful) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pdialog.isShowing()) {
                        pdialog.dismiss();
                        pdialog = null;
                    }
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        DialogUtils.showErrorDialog(mContext,
                                mContext.getResources().getString(R.string.Unsuccessful),
                                response, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    } else {

                        rlMyOrder.setVisibility(View.VISIBLE);
                        rlLayoutRecentPopular.setVisibility(View.VISIBLE);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List>() {
                        }.getType();
                        try {
                            appointment.model.appointmodel.List appointmentDataModel = gson.fromJson(response.toString(), appointment.model.appointmodel.List.class);
                            statusList = appointmentDataModel.getExtension();
                            for (int j = 0; j < statusList.size(); j++) {
                                if (statusList.get(j).getUrl().trim().toString().equalsIgnoreCase("audit")) {
                                    valueList = statusList.get(j).getValue();
                                    break;
                                }
                            }
                            status = appointmentDataModel.getStatus();
                            date = appointmentDataModel.getStart().getValue();
                            try {
                                ref_id = appointmentDataModel.getIdentifier().get(0).getValue();
                            } catch (Exception e) {
                                ref_id = null;
                            }


                            setAdapter(valueList);
                            String startdate = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy, date);
                            String starttime = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa, date);

                            int valuePostiton = 0;
                            for (int i = 0; i < statusList.size(); i++) {
                                if (statusList.get(i).getUrl().equals("appointmentDetails")) {
                                    valuePostiton = i;
                                    break;
                                }
                            }
                            tv_desc.setText(statusList.get(valuePostiton).getValue().get(0).getServiceName().toString());
                            tv_timing.setText(starttime + ", " + startdate);
                            tv_type.setText(statusList.get(valuePostiton).getValue().get(0).getLocationDetails().toString());
                            if (ref_id != null && !ref_id.equalsIgnoreCase("")) {
                                instruction_styledString = new SpannableString(MyApplication.getAppContext().getResources().getString(R.string.Ref_id) + ref_id);
                                instruction_styledString.setSpan(new CustomTypefaceSpan("", font_bold), 0, 7, 0);
                                instruction_styledString.setSpan(new CustomTypefaceSpan("", font_regular), 8, instruction_styledString.length(), 0);
                                tv_ref_id.setText(instruction_styledString);
                                tv_ref_id.setVisibility(View.VISIBLE);
                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    private void setAdapter(List<Value> valueList) {
        rv_order_status.setNestedScrollingEnabled(false);
        adapterOrderStatus = new OrderStatusAdapter(mContext, valueList);
        rv_order_status.setLayoutManager(new LinearLayoutManager(mContext));
        rv_order_status.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        rv_order_status.setAdapter(adapterOrderStatus);
    }
}

