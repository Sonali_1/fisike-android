package appointment.backgroundAsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UpdatePatientRequest;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike.update_user_profile.UpdateProfileBaseActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import appointment.FragmentBaseActivity;
import appointment.fragment.PatientInformationFragment;

/**
 * Created by Neha on 14-02-2017.
 */

public class UpdateLinkedPatientBG extends AsyncTask<Void, Void, String> {

    PatientMO request;
    Context context;
    String response;
    Fragment fragment;

    public UpdateLinkedPatientBG(Context context, Fragment fragment, PatientMO request) {
        // TODO Auto-generated constructor stub
        this.request = request;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    protected String doInBackground(Void... arg0)
    {
        try {
            updateLinkedPatient();
            return TextConstants.SUCESSFULL_API_CALL;
        } catch (Exception e) {
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else if (e.getMessage().equals(TextConstants.PHONE_NO_NOT_UNIQUE))
                return TextConstants.PHONE_NO_NOT_UNIQUE;
            else if (e.getMessage().equals(TextConstants.EMAIL_NOT_UNIQUE))
                return TextConstants.EMAIL_NOT_UNIQUE;
            else
                return TextConstants.UNEXPECTED_ERROR;
        }

    }

    protected void onPostExecute(String result) {
        if (context instanceof FragmentBaseActivity && fragment != null && fragment instanceof PatientInformationFragment)
            ((PatientInformationFragment) fragment).updateLinkedPatientProfileResponse();
    }


    private void updateLinkedPatient() throws Exception {
        APIManager apimanager = APIManager.getInstance();
        String url = apimanager.getLinkedPatientUpdateProfileURL(request.getId());
        String json = new Gson().toJson(request);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        String apiResult = apimanager.executeHttpPut(url, jsonObj, SharedPref.getAccessToken(),context);
        response =apiResult;
        if (response != null) {
            if (apiResult.contains("503"))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else {

            }
        } else {
            throw new Exception(TextConstants.UNEXPECTED_ERROR);
        }
    }

}
