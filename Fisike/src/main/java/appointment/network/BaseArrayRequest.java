package appointment.network;

import com.android.volley.Response;

import org.json.JSONArray;

import java.util.concurrent.Callable;

/**
 * Created by laxmansingh on 3/7/2016.
 */
public abstract class BaseArrayRequest implements Callable<Void>, Response.Listener<JSONArray>, Response.ErrorListener {

    @Override
    public Void call() throws Exception {
        doInBackground();
        return null;
    }

    public abstract void doInBackground();

}