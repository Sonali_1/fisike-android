package appointment.network;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by laxmansingh on 3/7/2016.
 */
public class BaseResponse {

    private long mTransactionId;
    private String mResponse;
    protected String mMessage;
    protected boolean mIsSuccessful;

    public BaseResponse(JSONArray response, long transactionId) {
        mTransactionId = transactionId;
        mIsSuccessful = true;
        mResponse = response.toString();
        mMessage = "";

    }

    public BaseResponse(Exception exception, long transactionId) {
        mTransactionId = transactionId;
        mIsSuccessful = false;
        if (exception instanceof VolleyError) {
            if (((VolleyError) exception).networkResponse != null) {
                NetworkResponse response = ((VolleyError) exception).networkResponse;
                int statusCode = response.statusCode;
                if (statusCode == 401) {
                    SharedPref.setAccessToken(null);
                }
                else if ((statusCode+"").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                    Utils.launchLocationChangeActivity(MyApplication.getAppContext());
                }
                try {
                    String responseBody = new String(response.data, "utf-8");
                    parseErrorResponse(new JSONArray(responseBody));
                }
                catch (Exception e) {
                    mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
                }
            } else if (exception instanceof NetworkError) {
                mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_network);
            } else {
                mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
            }
        } else {
            mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
        }
    }

    private void parseErrorResponse(JSONArray response) throws JSONException {
    //    String statusCode = response.getString("status");
    //    String httpStatusCode = response.getString("httpStatusCode");
     //   mMessage = ErrorUtils.getErrorMessage(statusCode, httpStatusCode);
    }


    public long getTransactionId() {
        return mTransactionId;
    }

    public String getResponse() {
        return mResponse;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean isSuccessful() {
        return mIsSuccessful;
    }

}
