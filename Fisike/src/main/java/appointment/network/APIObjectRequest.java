package appointment.network;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by brijesh on 15/10/15.
 */
public class APIObjectRequest extends JsonArrayRequest {


    private String url;
    private CharSequence FETCH_POPULAR_TESTS = "MoAppointmentEntity/fetchPopularTests";
    private CharSequence FETCH_POPULAR_SPECIALITIES = "MoAppointmentEntity/fetchAllBookableSpecialties";
    private CharSequence FETCH_RESOURCES_SPECIALITIES = "MoAppointmentEntity/fetchResourcesForSpecialty";

    public APIObjectRequest(int method, String url, String requestBody, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.url = url;
        setRetryPolicy(new DefaultRetryPolicy(MphRxUrl.TIMEOUT, MphRxUrl.RETRY, MphRxUrl.BACKOFF));
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        //   if (SharedPref.getAccessToken() != null )
        params.put("x-auth-token", SharedPref.getAccessToken());
        AppLog.showInfo("xauthtoken", params.toString());
        if (url.contains(FETCH_POPULAR_TESTS) || url.contains(FETCH_POPULAR_SPECIALITIES) || url.contains(FETCH_RESOURCES_SPECIALITIES)) {
            params.put("api-info", APIManager.getInstance().getHeader(MyApplication.getAppContext(), "V1"));
        }

        return params;
    }

    @Override
    public String getBodyContentType() {
        return "application/json;charset=UTF-8";
    }
}
