package appointment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.fragment.SelectDoctorFragment;
import careplan.activity.CareTaskAppointment;

/**
 * Created by Aastha on 11/03/2016.
 */
public class BookDoctorActivity extends BaseActivity {

    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_book_order, frameLayout);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentBookOrder, new SelectDoctorFragment(), "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
