
package appointment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.fragment.HomeSampleCollectionFragment;
import appointment.model.OrderSummary;
import careplan.activity.CareTaskAppointment;

/**
 * Created by laxmansingh on 2/24/2016.
 */
public class HomeSampleCollectionActivity extends BaseActivity {

    private FrameLayout frameLayout;
    //ImageButton bt_toolbar_cross;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.homesamplecollectionactivity, frameLayout);

        Bundle bundle = getIntent().getExtras();
        HomeSampleCollectionFragment fragment = new HomeSampleCollectionFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentParentView, fragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        OrderSummary.setInstance();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
