package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.SpecialityTypeAdapter;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.request.FetchSelectedSpecialityRequest;
import appointment.request.FetchSelectedSpecialityResponse;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 1/30/2017.
 */

public class SelectSpecialityTypeFragment extends SupportBaseFragment implements View.OnClickListener, SpecialityTypeAdapter.SpecialityOnClick {

    private Context mContext;
    private List<String> list = new ArrayList<String>();
    private List<AppointmentModel> list_specialityModel = new ArrayList<AppointmentModel>();
    private String from;
    private SpecialityTypeAdapter.SpecialityOnClick specialityOnClick;
    private RecyclerView mRv_speciality_type;
    private SpecialityTypeAdapter mSpecialityTypeAdapter;
    private CustomFontTextView tv_speciality_name, txt_select;
    private SpecialityDataModel selctedSpecialityDataModel;
    private long mTransactionId;


    private RelativeLayout mData_lyt, mProgress_error_lyt, mProgress_lyt;
    private CustomFontTextView mError_txt_view;
    private ProgressBar mProgress;
    private LinearLayout lyn_lyt_panel;

    public SelectSpecialityTypeFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.specialities));
        BusProvider.getInstance().register(this);
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.select_speciality_type_fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void findView() {
        specialityOnClick = this;
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        if (bundle.containsKey("specialitymodel")) {
            selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
        }

        txt_select = (CustomFontTextView) getView().findViewById(R.id.txt_select);
        mRv_speciality_type = (RecyclerView) getView().findViewById(R.id.rv_speciality_type);


        mData_lyt = (RelativeLayout) getView().findViewById(R.id.data_lyt);
        mProgress_error_lyt = (RelativeLayout) getView().findViewById(R.id.progress_error_lyt);
        mProgress_lyt = (RelativeLayout) getView().findViewById(R.id.progress_lyt);
        mError_txt_view = (CustomFontTextView) getView().findViewById(R.id.error_txt_view);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        setHeaderInfo();// Adding Selected Speciality Info dynamically...

    }


    @Override
    public void initView() {

        mRv_speciality_type.setLayoutManager(new LinearLayoutManager(mContext));
        //  mRv_speciality_type.setNestedScrollingEnabled(false);
        mSpecialityTypeAdapter = new SpecialityTypeAdapter(getActivity(), list, from, specialityOnClick);
        mRv_speciality_type.setAdapter(mSpecialityTypeAdapter);
    }

    @Override
    public void bindView() {
        txt_select.setText(mContext.getResources().getString(R.string.txt_select));
        if (!Utils.showDialogForNoNetwork(getActivity(), false)) {
            return;
        } else {
            // lyn_lyt.setVisibility(View.VISIBLE);
            // progress_bar.setVisibility(View.VISIBLE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new FetchSelectedSpecialityRequest(transactionId, mContext, getPayload()));
        }
        mProgress.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }


    public void setHeaderInfo() {
        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
        tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);



        /*[**** Add View***]*/
        lyn_lyt_panel.addView(speciality_info);
        /*[**** Add View***]*/

        /*[**** Set Data ***]*/
        tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());
        /*[**** Set Data ***]*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                break;
        }
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onSpecialityTypeClick(int adapterPosition) {

        Bundle bundle = new Bundle();

        if (list.get(adapterPosition).equals(TextConstants.SPECIALITY_DOCTORS)) {
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_specialityModel);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", list.get(adapterPosition));
            SpecialityDoctorFragment specialityTypeFragment = new SpecialityDoctorFragment();
            specialityTypeFragment.setArguments(bundle);
            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentParentView, specialityTypeFragment, "specialitydoctorfragment")
                    .addToBackStack(null)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        } else if (list.get(adapterPosition).equals(TextConstants.SPECIALITY_SERVICES)) {
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_specialityModel);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", list.get(adapterPosition));
            SpecialityServicesFragment specialityTypeFragment = new SpecialityServicesFragment();
            specialityTypeFragment.setArguments(bundle);
            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentParentView, specialityTypeFragment, "specialityServicesfragment")
                    .addToBackStack(null)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }

    }


    private String getPayload() {
        JSONObject maiJsonObject = new JSONObject();
        try {
            maiJsonObject.put("constraints", new JSONObject()
                    .put("serviceType", Integer.parseInt(selctedSpecialityDataModel.getServiceTypeId()))
                    .put("_count", 50)
                    .put("_skip", 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return maiJsonObject.toString();
    }


    @Subscribe
    public void onFetchSelectedSpecialityResponse(final FetchSelectedSpecialityResponse response) {
        try {
            if (mTransactionId != response.getTransactionId())
                return;
            if (response.isSuccessful() && response.getSpecialityModelArrayList() != null && response.getSpecialityModelArrayList().size() > 0) {
                list_specialityModel.clear();
                list_specialityModel.addAll(response.getSpecialityModelArrayList());
                if (list_specialityModel.size() == 0) {
                    showErrorView(true, mContext.getResources().getString(R.string.Something_went_wrong));
                } else {
                    showErrorView(false, null);
                    for (int i = 0; i < list_specialityModel.size(); i++) {
                        /*[****** If a Single doctor is available in list then show the Doctors Option else hide the option *********]*/
                        if (list_specialityModel.get(i).getSpecialtyForAppointment().getPractitionerForAppointment() != null) {
                            list.add(TextConstants.SPECIALITY_DOCTORS);
                            break;
                        }
                    }

                    list.add(TextConstants.SPECIALITY_SERVICES);
                    mSpecialityTypeAdapter.notifyDataSetChanged();
                }
            } else if (!response.isSuccessful() || response.getSpecialityModelArrayList() == null || response.getSpecialityModelArrayList().size() == 0) {
                Toast.makeText(getActivity(), getResources().getString(R.string.api_error), Toast.LENGTH_SHORT).show();
                if (response.getSpecialityModelArrayList().size() == 0) {
                    showErrorView(true, getResources().getString(R.string.api_error));
                } else {
                    showErrorView(true, response.getMessage());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void showErrorView(boolean isshowError, String errorMsg) {
        if (!isshowError) {
            mData_lyt.setVisibility(View.VISIBLE);
            mProgress_error_lyt.setVisibility(View.GONE);
        } else {
            mData_lyt.setVisibility(View.GONE);
            mProgress_lyt.setVisibility(View.GONE);
            mProgress_error_lyt.setVisibility(View.VISIBLE);
            mError_txt_view.setVisibility(View.VISIBLE);
            mError_txt_view.setText(errorMsg != null ? errorMsg : mContext.getResources().getString(R.string.Something_went_wrong));
        }
    }

}