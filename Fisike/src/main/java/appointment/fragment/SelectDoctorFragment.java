package appointment.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.NotifyingScrollView;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import appointment.MyOrdersActivity;
import appointment.SearchingActivity;
import appointment.adapter.PractitionerAdapter;
import appointment.interfaces.Animable;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.utils.SupportBaseFragment;

/**
 * Created by Aastha on 14/03/2016.
 */
public class SelectDoctorFragment extends SupportBaseFragment implements View.OnClickListener, Animable, NotifyingScrollView.OnScrollChangedListener {
    private Context mContext;
    private Animable animable;
    private CustomFontTextView mErrorInfo;
    private TextView toolbar_title;
    private Toolbar mToolbar;
    private EditText mEtSearch;
    private RecyclerView rv_doctors;
    private PractitionerAdapter adapterdoctors;
    private ProgressBar progress_bar;
    private long mTransactionId;
    private String toolbar_title_text;
    private LinearLayout lyn_lyt;

    private ColorDrawable mActionBarBackgroundDrawable;
    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener;
    private LinearLayout layoutToolbar;
    private IconTextView bt_toolbar_right;
    private ImageButton btnBack;

    private List<AppointmentModel> doctorslist = new ArrayList<>();
    private RelativeLayout rl_empty_view;
    private String from;
    private String searchedString;


    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        from = TextConstants.DOCTORS;
        toolbar_title_text = getResources().getString(R.string.doctor);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(toolbar_title_text);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        mOnScrollChangedListener = this;
        BusProvider.getInstance().register(this);
        return R.layout.select_doctor_fragment;
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void findView() {
        mActionBarBackgroundDrawable = new ColorDrawable(getResources().getColor(R.color.action_bar_bg));
        mActionBarBackgroundDrawable.setAlpha(0);

        animable = this;


        mToolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(toolbar_title_text);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setOnClickListener(this);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        ((NotifyingScrollView) getView().findViewById(R.id.scroll_view)).setOnScrollChangedListener(mOnScrollChangedListener);

        mEtSearch = (EditText) getView().findViewById(R.id.etSearch);
        mEtSearch.setText(R.string.search_for_doctors);

        lyn_lyt = (LinearLayout) getView().findViewById(R.id.lyn_lyt);
        progress_bar = (ProgressBar) getView().findViewById(R.id.progress_bar);
        rv_doctors = (RecyclerView) getView().findViewById(R.id.rv_doctors);
        rl_empty_view = (RelativeLayout) getView().findViewById(R.id.rl_empty_view);
        lyn_lyt = (LinearLayout) getView().findViewById(R.id.lyn_lyt);
        mErrorInfo = (CustomFontTextView) getView().findViewById(R.id.tv_error_info);
        layoutToolbar = (LinearLayout) getView().findViewById(R.id.toolbar_lyt);


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
        rv_doctors.setVisibility(View.GONE);
        lyn_lyt.setVisibility(View.GONE);
        mErrorInfo.setVisibility(View.VISIBLE);
        rl_empty_view.setVisibility(View.VISIBLE);
//            }
//        }, 2000);

    }

    @Override
    public void initView() {

        Drawable.Callback mDrawableCallback = new Drawable.Callback() {
            @Override
            public void invalidateDrawable(Drawable who) {
                ((AppCompatActivity) getActivity()).getActionBar().setBackgroundDrawable(who);
            }

            @Override
            public void scheduleDrawable(Drawable who, Runnable what, long when) {
            }

            @Override
            public void unscheduleDrawable(Drawable who, Runnable what) {
            }
        };
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }


        OrderSummary.getInstance().setTestType(TextConstants.DOCTORS);

        progress_bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);

        mEtSearch.setHint(getResources().getString(R.string.search_by_doctor));
    }


    public String loadJSONFromAsset() {
        String json = null;
        InputStream is;
        try {
            is = mContext.getAssets().open("appointment.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    @Override
    public void bindView() {
        mEtSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchingActivity.class).putExtra("from", TextConstants.DOCTORS));
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_toolbar_cross:
                OrderSummary.setInstance();
                getActivity().finish();
                break;

            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(getActivity());
                break;

            default:
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }


    @Override
    public void onResume() {
        super.onResume();
        OrderSummary.getInstance().setTestType(TextConstants.DOCTORS);
    }


    @Override
    public void onScrollChanged(NestedScrollView who, int l, int t, int oldl, int oldt) {
        final int headerHeight = layoutToolbar.getHeight() - mToolbar.getHeight();
        final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
        final int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }

/*
                        Type listType = new TypeToken<List<Practitioner>>() {
                        }.getType();
                        List<Practitioner> temp_list = gson.fromJson(response.toString(), listType);*/


    @Override
    public void animate(String what) {
    }
}
