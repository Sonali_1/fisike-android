package appointment.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

import appointment.FragmentBaseActivity;
import appointment.ViewInstructionsActivity;
import appointment.model.OrderSummary;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;
import careplan.activity.CareTaskAppointment;

/**
 * Created by laxmansingh on 4/6/2016.
 */
public class SuccessFragment extends SupportBaseFragment implements View.OnClickListener {
    private Context mContext;
    private CustomFontTextView tvSpeciality;
    private CustomFontTextView tv_all_test_names;
    private CustomFontTextView tv_total_price;
    private TextView tv_patient_name, tv_patient_emailaddress, tv_mobile_number;
    private Button btn_view_instruction, btn_view_all_order;
    private View rltestInfo, rlDoctorInfo, rlSpecialityInfo;
    private String from;
    private ArrayList<String> testNames = new ArrayList<String>();
    private String starttime;
    private appointment.model.appointmodel.List appointmentmodel;
    private CustomFontTextView tvDate, tvTime;
    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality, mTv_service_names;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    private View mServices_dotted_line, mLocations_dotted_line, mDatetime_dotted_line;
    private CustomFontTextView mTv_location1, mTv_location2, mTv_location3;
    private CustomFontTextView mTv_date, mTv_time;
    private CircularImageView mPractitionerPic;
    /* currently hardcoding for rsna */
    private boolean isSkipped = false;
    private CustomFontTextView tv_reason, tv_comment, tv_gender, tv_dob;
    private int instructionPosition = 0;

    private boolean isDoctorsAvailable = true;
    private String selectedSpeciality;


    /*[......For Speciality......]*/
    private LinearLayout lyn_lyt_panel;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;
    private CustomFontTextView tv_service_names;
    private FrameLayout frame_container;
/*[......For Speciality......]*/

    /*[ Header View Instances ]*/


    public SuccessFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.txt_summary));
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.success_fragment;
    }

    @Override
    public void findView() {

        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        appointmentmodel = bundle.getParcelable("appointmentmodel");
        starttime = appointmentmodel.getStart().getValue();
        tvDate = (CustomFontTextView) getView().findViewById(R.id.tv_date_success);
        tvTime = (CustomFontTextView) getView().findViewById(R.id.tv_time_success);
        tv_total_price = (CustomFontTextView) getView().findViewById(R.id.tv_total_price);


        if (from.equals(TextConstants.SPECIALITY)) {
            selectedSpeciality = bundle.getString("selectedSpeciality");
            isDoctorsAvailable = bundle.getBoolean("isDoctorsAvailable");
        }


        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.isSuccessful = true;
        }

        initializeAndSetHeaderView();
        MyAppointmentFragment.isAppointmentSuccessful = true;
         /*[....... Speciality info.............]*/
        tvSpeciality = (CustomFontTextView) getView().findViewById(R.id.tv_selected_speciality);
        /*[....... Speciality info.............]*/


        tv_patient_name = (CustomFontTextView) getView().findViewById(R.id.tv_patient_name);
        tv_patient_emailaddress = (CustomFontTextView) getView().findViewById(R.id.tv_patient_email);
        tv_mobile_number = (CustomFontTextView) getView().findViewById(R.id.tv_mobile_number);
        tv_reason = (CustomFontTextView) getView().findViewById(R.id.tv_reason);
        tv_comment = (CustomFontTextView) getView().findViewById(R.id.tv_comment);
        tv_gender = (CustomFontTextView) getView().findViewById(R.id.tv_gender);
        tv_dob = (CustomFontTextView) getView().findViewById(R.id.tv_dob);
        btn_view_instruction = (Button) getView().findViewById(R.id.btn_view_instruction);
        btn_view_all_order = (Button) getView().findViewById(R.id.btn_view_all_order);

       /* rltestInfo = (View) getView().findViewById(R.id.total_test_lyt);
        rlDoctorInfo = (View) getView().findViewById(R.id.doc_info_layout);*/
        rlSpecialityInfo = (View) getView().findViewById(R.id.rel_lyt_speciality);


        for (int i = 0; i < appointmentmodel.getExtension().size(); i++) {
            if (appointmentmodel.getExtension().get(i).getUrl().equals("appointmentDetails")) {
                String instruction = (appointmentmodel.getExtension().get(i).getValue().get(0).getCharacteristic() == null || appointmentmodel.getExtension().get(i).getValue().get(0).getCharacteristic().size() == 0) ? "" : appointmentmodel.getExtension().get(i).getValue().get(0).getCharacteristic().get(0).getCoding().get(0).getDisplay();
                instructionPosition = i;
                if (instruction != null && !instruction.equals("") && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) {
                    btn_view_instruction.setVisibility(View.VISIBLE);
                } else {
                    btn_view_instruction.setVisibility(View.GONE);
                }
                break;
            } else {
                btn_view_instruction.setVisibility(View.GONE);
            }
        }
    }

    private void initializeAndSetHeaderView() {

        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);
        frame_container = (FrameLayout) getView().findViewById(R.id.frame_container);

        if (from.equals(TextConstants.SPECIALITY)) {
            lyn_lyt_panel.setVisibility(View.VISIBLE);
            frame_container.setVisibility(View.GONE);


            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
            tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

            ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
            mPractitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
            tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
            tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

            ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
            tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

            ViewGroup locations_info = (ViewGroup) inflater.inflate(R.layout.locations_info_layout, lyn_lyt_panel, false);
            mTv_location1 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location3);



        /*[**** Add View***]*/
            lyn_lyt_panel.addView(speciality_info);
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(doc_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                if (isDoctorsAvailable) {
                    lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                    lyn_lyt_panel.addView(doc_info);
                }
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
            }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
            tv_speciality_name.setText(OrderSummary.getInstance().getSpeciality());

            String practionerName = null, practionerSpeciality = null, practionerProfilePic = null;
            String selectedservice = null, location1 = null, location3 = null;
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                practionerName = OrderSummary.getInstance().getDoctorName();
                practionerProfilePic = OrderSummary.getInstance().getPractionerProfilePic();
                practionerSpeciality = OrderSummary.getInstance().getDoctorSpeciality();
                selectedservice = OrderSummary.getInstance().getServiceNames();
                location1 = OrderSummary.getInstance().getLocation1();
                location3 = OrderSummary.getInstance().getLocation3();
                new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                if (isDoctorsAvailable) {
                    practionerName = OrderSummary.getInstance().getDoctorName();
                    practionerProfilePic = OrderSummary.getInstance().getPractionerProfilePic();
                    practionerSpeciality = OrderSummary.getInstance().getDoctorSpeciality();
                    location1 = OrderSummary.getInstance().getLocation1();
                    location3 = OrderSummary.getInstance().getLocation3();
                    new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
                } else {
                    location1 = OrderSummary.getInstance().getLocation1();
                    location3 = OrderSummary.getInstance().getLocation3();
                }
                selectedservice = OrderSummary.getInstance().getServiceNames();
            }

            mTv_location1.setText(location1);
            mTv_location2.setVisibility(View.GONE);
            if (Utils.isValueAvailable(location3))
                mTv_location3.setText(location3.toString().trim());
            else {
                mTv_location3.setText("");
                mTv_location3.setVisibility(View.GONE);
            }

            tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
            tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");

            tv_service_names.setText((selectedservice != null && !selectedservice.equals(mContext.getResources().getString(R.string.txt_null))) ? selectedservice : "");

        /*[**** Set Data ***]*/
        } else {

            lyn_lyt_panel.setVisibility(View.GONE);
            frame_container.setVisibility(View.VISIBLE);
            mTvdoctor_name = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_name);
            mTvdoctor_speciality = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_speciality);
            mTv_service_names = (CustomFontTextView) getView().findViewById(R.id.tv_service_names);

            mPractitionerPic = (CircularImageView) getView().findViewById(R.id.practitionerPic);

            mInner_doc_info_lyt = (RelativeLayout) getView().findViewById(R.id.inner_doc_info_lyt);
            mDoc_services_lyt = (RelativeLayout) getView().findViewById(R.id.doc_services_lyt);
            mDoc_locations_lyt = (RelativeLayout) getView().findViewById(R.id.doc_locations_lyt);
            mDoc_datetime_lyt = (RelativeLayout) getView().findViewById(R.id.doc_datetime_lyt);

            mServices_dotted_line = (View) getView().findViewById(R.id.services_dotted_line);
            mLocations_dotted_line = (View) getView().findViewById(R.id.locations_dotted_line);
            mDatetime_dotted_line = (View) getView().findViewById(R.id.datetime_dotted_line);

            mTv_location1 = (CustomFontTextView) getView().findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) getView().findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) getView().findViewById(R.id.tv_location3);

            mTv_date = (CustomFontTextView) getView().findViewById(R.id.tv_date);
            mTv_time = (CustomFontTextView) getView().findViewById(R.id.tv_time);

            mDoc_services_lyt.setVisibility(View.VISIBLE);
            mDoc_locations_lyt.setVisibility(View.VISIBLE);
            mDoc_datetime_lyt.setVisibility(View.GONE);
            mServices_dotted_line.setVisibility(View.GONE);
            mLocations_dotted_line.setVisibility(View.GONE);

            if (from.equals(TextConstants.DOCTORS)) {
                mInner_doc_info_lyt.setVisibility(View.VISIBLE);
                if (!isSkipped) {
                    mDoc_services_lyt.setVisibility(View.VISIBLE);
                    mTv_location2.setVisibility(View.GONE);
                } else {
                    mDoc_services_lyt.setVisibility(View.GONE);
                    mTv_location2.setVisibility(View.VISIBLE);
                }
                new CommonControls().setProfilePic(mContext, mPractitionerPic, OrderSummary.getInstance().getPractionerProfilePic());

            } else if (from.equals(TextConstants.SPECIALITY)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
            } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
                mServices_dotted_line.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mDoc_locations_lyt.setVisibility(View.VISIBLE);
                mTv_location2.setVisibility(View.GONE);
            }
        }

    }


    @Override
    public void initView() {
        {
          /*  if (from.equals(TextConstants.DOCTORS)) {

                rltestInfo.setVisibility(View.GONE);
                rlDoctorInfo.setVisibility(View.VISIBLE);
                rlSpecialityInfo.setVisibility(View.GONE);

                tvdoctor_name.setText(OrderSummary.getInstance().getDoctorName());
                tvdoctorspeciality.setText(OrderSummary.getInstance().getDoctorSpeciality());
                tvdoctorspeciality.setVisibility(View.VISIBLE);
            } else if (from.equals(TextConstants.SPECIALITY)) {

                rltestInfo.setVisibility(View.GONE);
                rlDoctorInfo.setVisibility(View.GONE);
                rlSpecialityInfo.setVisibility(View.VISIBLE);

                rlDoctorInfo.setVisibility(View.GONE);
                tvdoctorspeciality.setVisibility(View.GONE);
                rlSpecialityInfo.setVisibility(View.VISIBLE);
                tvSpeciality.setText(OrderSummary.getInstance().getDoctorSpeciality().toString());
            } else if (from.equals(TextConstants.LAB_TEST)) {

                rltestInfo.setVisibility(View.VISIBLE);
                rlDoctorInfo.setVisibility(View.GONE);
                rlSpecialityInfo.setVisibility(View.GONE);

                rlDoctorInfo.setVisibility(View.GONE);
                tvdoctorspeciality.setVisibility(View.GONE);
                rltestInfo.setVisibility(View.VISIBLE);
                tv_total_price.setText(OrderSummary.getInstance().getTotalPrice() + "");
                ((RelativeLayout) getView().findViewById(R.id.rel_lyt_total_test)).setVisibility(View.VISIBLE);
                // tv_total_test_number.setText(OrderSummary.getInstance().getTestNumber() + "");
                List<TestsDataModel> testMOList = OrderSummary.getInstance().getOrderist();
                for (int i = 0; i < testMOList.size(); i++)
                    testNames.add(testMOList.get(i).getName());

                String name = "";
                int size = testNames.size();
        *//*if (size > 0) {*//*
                for (int i = 0; i < size; i++) {
                    if (i != size - 1) {
                        name += testNames.get(i) + "/";
                    } else
                        name += testNames.get(i);
                }
                tv_all_test_names.setText(name);
            }*/

            //   if (!OrderSummary.getInstance().getDoctorLocationAddress().equals(""))
            //      tv_location_address.setText(OrderSummary.getInstance().getDoctorLocationAddress());
            //    if (!OrderSummary.getInstance().getDoctorContact().equals(""))
            //     tv_location_contact.setText(OrderSummary.getInstance().getDoctorContact().toString());
        }

        try {
            tvDate.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy, starttime));

            if (DateFormat.is24HourFormat(mContext)) {
                tvTime.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.HH_mm, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.HH_mm, appointmentmodel.getEnd().getValue()));
            } else {
                tvTime.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa, appointmentmodel.getEnd().getValue()));
            }
        } catch (Exception ex) {
        }

        tv_patient_name.setText(OrderSummary.getInstance().getPatientName());
        tv_patient_emailaddress.setText(OrderSummary.getInstance().getEmailAddress());
        tv_mobile_number.setText(OrderSummary.getInstance().getPatientMobile());

        if (!from.equals(TextConstants.SPECIALITY)) {
            mTvdoctor_name.setText(OrderSummary.getInstance().getDoctorName());
            mTvdoctor_speciality.setText(OrderSummary.getInstance().getDoctorSpeciality());
            mTv_service_names.setText(OrderSummary.getInstance().getServiceNames());
            mTv_location1.setText(OrderSummary.getInstance().getLocation1());
            mTv_location2.setText(OrderSummary.getInstance().getLocation2());
            if (Utils.isValueAvailable(OrderSummary.getInstance().getLocation3()))
                mTv_location3.setText(OrderSummary.getInstance().getLocation3());
            else
                mTv_location3.setVisibility(View.GONE);
        }

        tv_dob.setText(OrderSummary.getInstance().getDateOfBirth());
        tv_gender.setText(OrderSummary.getInstance().getGender());

        if (Utils.isValueAvailable(OrderSummary.getInstance().getReason()))
            tv_reason.setText(OrderSummary.getInstance().getReason());

        if (Utils.isValueAvailable(OrderSummary.getInstance().getComment()))
            tv_comment.setText(OrderSummary.getInstance().getComment());

    }

    @Override
    public void bindView() {
        btn_view_instruction.setOnClickListener(this);
        btn_view_all_order.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_view_instruction:
                Intent intent = new Intent(mContext, ViewInstructionsActivity.class);
                intent.putExtra("instructions", appointmentmodel.getExtension().get(instructionPosition).getValue().get(0).getCharacteristic().get(0).getCoding().get(0).getDisplay());
                intent.putExtra("title", appointmentmodel.getExtension().get(instructionPosition).getValue().get(0).getServiceName());
                mContext.startActivity(intent);
                break;

            case R.id.btn_view_all_order:
                goToAppointmentScreen();
                break;
            default:
                break;
        }
    }


    public void goToAppointmentScreen() {
        if (CareTaskAppointment.careTaskAppointmentActivity != null && CareTaskAppointment.isSuccessful) {
            for (int i = 0; i < CareTaskAppointment.listAppointmentActivity.size(); i++) {
                CareTaskAppointment.listAppointmentActivity.get(i).finish();
            }
            CareTaskAppointment.listAppointmentActivity.clear();

        } else {
            startActivity(new Intent(getActivity(), HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
        getActivity().finish();
    }


}
