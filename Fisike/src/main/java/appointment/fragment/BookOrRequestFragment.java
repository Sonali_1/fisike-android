package appointment.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.BaseActivity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import appointment.MyOrdersActivity;
import appointment.adapter.RequestAdapter;
import appointment.model.OfferingModel;
import appointment.utils.SupportBaseFragment;

/**
 * Created by Aastha on 11/03/2016.
 */
public class BookOrRequestFragment extends SupportBaseFragment implements View.OnClickListener {

    private RecyclerView lv_options;
    private RequestAdapter requestAdapter;
    private ArrayList<OfferingModel> requestTypes = new ArrayList<OfferingModel>();
    private Context mContext;

    public BookOrRequestFragment() {
        mContext = getActivity();

    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        BaseActivity.bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        BaseActivity.bt_toolbar_right.setTextSize(44);
        BaseActivity.bt_toolbar_right.setVisibility(View.GONE);
        BaseActivity.bt_toolbar_right.setOnClickListener(this);
        return R.layout.book_request_fragment;
    }

    @Override
    public void findView() {

        lv_options = (RecyclerView) getView().findViewById(R.id.request_options);

    }

    @Override
    public void initView() {
        String jsonstring = loadJSONFromAsset();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<OfferingModel>>() {
        }.getType();

        requestTypes.clear();
        requestTypes.addAll((Collection<? extends OfferingModel>) gson.fromJson(jsonstring.toString(), listType));

        requestAdapter = new RequestAdapter(getActivity(), requestTypes, TextConstants.FROM_APPOINTMENT);
        lv_options.setLayoutManager(new LinearLayoutManager(getActivity()));
        lv_options.setAdapter(requestAdapter);
        requestAdapter.notifyDataSetChanged();

    }

    @Override
    public void bindView() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(getActivity());
                break;
        }
    }


    public String loadJSONFromAsset() {
        String json = null;
        InputStream is;
        try {
            is = mContext.getAssets().open("offering.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}

