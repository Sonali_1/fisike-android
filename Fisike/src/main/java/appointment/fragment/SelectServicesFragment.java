package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.ServicesAdapter;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class SelectServicesFragment extends SupportBaseFragment implements View.OnClickListener {

    private Context mContext;
    private List<AppointmentModel> list = new ArrayList<AppointmentModel>();
    private String from;


    private RecyclerView mRv_services;
    private ServicesAdapter mServicesAdapter;


    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality;
    private CircularImageView mPractitionerPic;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    /*[ Header View Instances ]*/


    public SelectServicesFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.services));
        return R.layout.selectservicesfragment;
    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        if (bundle.containsKey("appointmentmodel")) {
            list.clear();
            list.add((AppointmentModel) bundle.getParcelable("appointmentmodel"));
        }
        initializeAndSetHeaderView();
        mRv_services = (RecyclerView) getView().findViewById(R.id.rv_services);

    }


    @Override
    public void initView() {
        //  img_speciality_delete.setOnClickListener(this);

        mRv_services.setLayoutManager(new LinearLayoutManager(mContext));
        mRv_services.setNestedScrollingEnabled(false);
        mServicesAdapter = new ServicesAdapter(getActivity(), list, from);
        mRv_services.setAdapter(mServicesAdapter);


//        mTvdoctor_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                list.addAll(list);
//                mServicesAdapter.notifyDataSetChanged();
//            }
//        });
    }

    @Override
    public void bindView() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                break;

        }
    }


    private void initializeAndSetHeaderView() {

        mTvdoctor_name = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_name);
        mTvdoctor_speciality = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_speciality);
        mPractitionerPic = (CircularImageView) getView().findViewById(R.id.practitionerPic);


        mInner_doc_info_lyt = (RelativeLayout) getView().findViewById(R.id.inner_doc_info_lyt);
        mDoc_services_lyt = (RelativeLayout) getView().findViewById(R.id.doc_services_lyt);
        mDoc_locations_lyt = (RelativeLayout) getView().findViewById(R.id.doc_locations_lyt);
        mDoc_datetime_lyt = (RelativeLayout) getView().findViewById(R.id.doc_datetime_lyt);

        mDoc_services_lyt.setVisibility(View.GONE);
        mDoc_locations_lyt.setVisibility(View.GONE);
        mDoc_datetime_lyt.setVisibility(View.GONE);

        if (from.equals(TextConstants.DOCTORS)) {
            mInner_doc_info_lyt.setVisibility(View.VISIBLE);
            mTvdoctor_name.setText(list.get(0).getPractitionerForAppointment().getPractitionerName().toString().trim());
            mTvdoctor_speciality.setText(list.get(0).getPractitionerForAppointment().getSpecialty().toString().trim());

            PractitionerForAppointment practitioner = list.get(0).getPractitionerForAppointment();
            new CommonControls().setProfilePic(mContext, mPractitionerPic, practitioner.getPractionerPic());

        } else if (from.equals(TextConstants.SPECIALITY)) {
            mInner_doc_info_lyt.setVisibility(View.GONE);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }

}