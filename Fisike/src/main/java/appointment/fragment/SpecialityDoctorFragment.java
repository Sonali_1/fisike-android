package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.SpecialityDoctorAdapter;
import appointment.interfaces.Animable;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 1/31/2017.
 */

public class SpecialityDoctorFragment extends SupportBaseFragment implements View.OnClickListener, SpecialityDoctorAdapter.SpecialityDoctorOnClick {

    private Context mContext;
    private Animable animable;
    private String from;
    private SpecialityDoctorAdapter.SpecialityDoctorOnClick specialityDoctorOnClick;
    private RecyclerView mRv_speciality_type;
    private SpecialityDoctorAdapter mSpecialityDoctorAdapter;
    private CustomFontTextView tv_speciality_name, txt_select, tv_service_names;
    private SpecialityDataModel selctedSpecialityDataModel;
    private String selectedSpeciality;
    private List<AppointmentModel> list_specialityModel;
    private List<ServiceGroup> list_ServicesGroup;
    private List<PractitionerForAppointment> practionerList = new ArrayList<PractitionerForAppointment>();


    private RelativeLayout mData_lyt, mProgress_error_lyt, mProgress_lyt;
    private CustomFontTextView mError_txt_view;
    private ProgressBar mProgress;
    private LinearLayout lyn_lyt_panel;

    public SpecialityDoctorFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.doctor));
        specialityDoctorOnClick = this;
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.select_speciality_type_fragment;
    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        selectedSpeciality = bundle.getString("selectedSpeciality");
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            list_specialityModel = bundle.getParcelableArrayList("alldata");
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            list_ServicesGroup = bundle.getParcelableArrayList("alldata");
        }
        if (bundle.containsKey("specialitymodel")) {
            selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
        }


        mRv_speciality_type = (RecyclerView) getView().findViewById(R.id.rv_speciality_type);

        mData_lyt = (RelativeLayout) getView().findViewById(R.id.data_lyt);
        mProgress_error_lyt = (RelativeLayout) getView().findViewById(R.id.progress_error_lyt);
        mProgress_lyt = (RelativeLayout) getView().findViewById(R.id.progress_lyt);
        mError_txt_view = (CustomFontTextView) getView().findViewById(R.id.error_txt_view);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        txt_select = (CustomFontTextView) getView().findViewById(R.id.txt_select);

        mProgress_error_lyt.setVisibility(View.GONE);
        mData_lyt.setVisibility(View.VISIBLE);
        setHeaderInfo();// Adding Selected Speciality Info dynamically...

    }


    @Override
    public void initView() {
        mRv_speciality_type.setLayoutManager(new LinearLayoutManager(mContext));
        //  mRv_speciality_type.setNestedScrollingEnabled(false);
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            mSpecialityDoctorAdapter = new SpecialityDoctorAdapter(getActivity(), practionerList, from, specialityDoctorOnClick, selectedSpeciality);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            mSpecialityDoctorAdapter = new SpecialityDoctorAdapter(list_ServicesGroup, getActivity(), from, specialityDoctorOnClick, selectedSpeciality);
        }
        mRv_speciality_type.setAdapter(mSpecialityDoctorAdapter);


    }

    @Override
    public void bindView() {

        txt_select.setText(mContext.getResources().getString(R.string.doctor));
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            practionerList.clear();
            for (int i = 0; i < list_specialityModel.size(); i++) {
                if (list_specialityModel.get(i).getSpecialtyForAppointment().getPractitionerForAppointment() != null) {
                    practionerList.add(list_specialityModel.get(i).getSpecialtyForAppointment().getPractitionerForAppointment());
                }
            }
        }
        mSpecialityDoctorAdapter.notifyDataSetChanged();
    }


    public void setHeaderInfo() {
        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
        tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);


        /*[**** Add View***]*/
        lyn_lyt_panel.addView(speciality_info);

        if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
            tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(services_info);
        }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
        tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());

        if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            tv_service_names.setText(list_ServicesGroup.get(0).getServiceName() != null ? list_ServicesGroup.get(0).getServiceName().toString().trim() : "");
        }
        /*[**** Set Data ***]*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                break;

        }
    }

    @Override
    public void onSpecialityDoctorClick(int adapterPosition) {


        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            AppointmentModel selectedAppointmentModel = null;
            for (int i = 0; i < list_specialityModel.size(); i++) {
                if (list_specialityModel.get(i).getSpecialtyForAppointment().getPractitionerForAppointment() != null) {
                    if (list_specialityModel.get(i).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerId().equals(practionerList.get(adapterPosition).getPractitionerId())) {
                        selectedAppointmentModel = list_specialityModel.get(i);
                        break;
                    }
                }
            }

            List<AppointmentModel> selectedlist = new ArrayList<AppointmentModel>();
            selectedlist.add(selectedAppointmentModel);
            Bundle bundle = new Bundle();
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) selectedlist);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            SpecialityServicesFragment specialityServicesFragment = new SpecialityServicesFragment();
            specialityServicesFragment.setArguments(bundle);
            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentParentView, specialityServicesFragment, "specialityServicesfragment")
                    .addToBackStack(null)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_ServicesGroup);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            bundle.putInt("selectedDoctorServices", adapterPosition);
            SpecialityLocationsFragment specialityLocationsFragment = new SpecialityLocationsFragment();
            specialityLocationsFragment.setArguments(bundle);
            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentParentView, specialityLocationsFragment, "specialityLocationsfragment")
                    .addToBackStack(null)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }
}