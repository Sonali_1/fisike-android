package appointment.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.background.updateUserProfileBG;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.UpdatePatientRequest;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.PatientDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.PatientNameAdapter;
import appointment.adapter.ReasonsAdapter;
import appointment.backgroundAsyncTask.UpdateLinkedPatientBG;
import appointment.constants.AppointmentConstants;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.interfaces.ResponseCallback;
import appointment.model.LinkedPatientInfoMO;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.request.AppointUpdateRequest;
import appointment.request.FetchSelfAndAssociatedPatients;
import appointment.request.FetchSelfAndAssociatedPatientsResponse;
import appointment.request.GetLinkedPatientsRequest;
import appointment.request.GetLinkedPatientsResponse;
import appointment.request.PlaceTestOrderRequest;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 4/1/2016.
 */
public class PatientInformationFragment extends SupportBaseFragment implements View.OnClickListener, ResponseCallback, AppointmentResponseCallback {
    private Toolbar toolbar;
    private ResponseCallback responseCallback;
    private AppointmentResponseCallback appointmentResponseCallback;
    private Context mContext;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private String starttime, endtime;
    private int hcsID;
    private long patientId;
    private PatientMO patientMO;
    private String bookRequestApiResponse;

    private List<String> nameList = new ArrayList<String>();
    private List<String> reasonList = new ArrayList<String>();
    private PatientNameAdapter adapter;
    private ReasonsAdapter reasonAdapter;


    private CustomFontTextView patientGender, patientDob;

    private Spinner patientName;

    private CardView mCard_patient_info;
    private NestedScrollView mScrollView;

    private Button btPayNow;
    private String rebook_or_reschedule = "";
    private appointment.model.appointmodel.List appointment_model = null;
    private String position = "";
    private boolean REBOOK;
    private IconTextView iv_ic_menu_overflow;
    private String type = null;

    /*[..... new address fields ......... ]*/
    private CustomFontEditTextView editTextComment;
    private RelativeLayout relativeLayoutPatientInfo;
    private Spinner tvReason;
    private ArrayList<LinkedPatientInfoMO> linkedPatientInfoMOArrayList = new ArrayList<LinkedPatientInfoMO>();
    private boolean isSelfProfileUpdate, isToHitUpdateProfileCall;
    /*[..... new address fields ......... ]*/



    /*[Edited by Laxman with new header]*/

    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality, mTv_service_names;
    private CircularImageView mPractitionerPic;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    private View mServices_dotted_line, mLocations_dotted_line, mDatetime_dotted_line;
    private CustomFontTextView mTv_location1, mTv_location2, mTv_location3;
    private CustomFontTextView mTv_date, mTv_time;

    /*[......For Speciality......]*/
    private LinearLayout lyn_lyt_panel;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;
    private CustomFontTextView tv_service_names;
    private FrameLayout frame_container;
/*[......For Speciality......]*/

    /*[ Header View Instances ]*/

    private List<AppointmentModel> list_appointment = new ArrayList<AppointmentModel>();
    private String from;
    private boolean isSkipped;
    private int selectedServicePosition;
    private int selectedLocation;
    private boolean isBookable;
    private PopupWindow mPopup;
    private CustomFontTextView patientNameLabel, reasonLabel;
    private CustomFontEditTextView patientMobile;
    private CustomFontEditTextView patientEmail;

    /*[Edited by Laxman with new header]*/


    /*[**** Variables for Speciality ****]*/
    private boolean isDoctorsAvailable = true;
    private int selectedDoctorServices;
    private List<ServiceGroup> listServiceGroup;
    private String selectedSpeciality;
    private SpecialityDataModel selctedSpecialityDataModel;
    private int selectedService;
    private UserMO userMO;
    private String originalEmail, originalPhone;
    private int width;
    /*[**** Variables for Speciality ****]*/

    public PatientInformationFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.txt_appointment_summary));
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.patientinformation_fragment;
    }


    @Override
    public void findView() {

        Bundle bundle = getArguments();
        starttime = bundle.getString("start_date_time", "");
        endtime = bundle.getString("end_date_time", "");
        hcsID = bundle.getInt("hcsid", 0);
        from = bundle.getString(TextConstants.TEST_TYPE);
        isBookable = bundle.getBoolean("isBookable");

        if (from.equals(TextConstants.DOCTORS)) {
            isSkipped = bundle.getBoolean("isskipped");
            selectedServicePosition = bundle.getInt("selectedservice");
            selectedLocation = bundle.getInt("selectedlocation");
        }

        if (bundle.containsKey("appointmentmodel")) {
            list_appointment.clear();
            list_appointment.add((AppointmentModel) bundle.getParcelable("appointmentmodel"));
        }

        if (from.equals(TextConstants.DOCTORS)) {
            type = TextConstants.TYPE_DOCTOR;
        } else if (from.equals(TextConstants.SPECIALITY)) {
            type = TextConstants.TYPE_SPECIALITY;

            selectedSpeciality = bundle.getString("selectedSpeciality");
            selectedLocation = bundle.getInt("selectedlocation");
            if (bundle.containsKey("selectedService")) {
                selectedService = bundle.getInt("selectedService");
            }
            if (bundle.containsKey("specialitymodel")) {
                selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
            }


            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                list_appointment = bundle.getParcelableArrayList("alldata");
                hcsID = Integer.parseInt(list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getHcsId());
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                listServiceGroup = bundle.getParcelableArrayList("alldata");
                selectedDoctorServices = bundle.getInt("selectedDoctorServices");
                if (bundle.containsKey("isDoctorsAvailable")) {
                    isDoctorsAvailable = bundle.getBoolean("isDoctorsAvailable");
                }

                if (isDoctorsAvailable) {
                    hcsID = Integer.parseInt(listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getHcsId());
                } else {
                    hcsID = Integer.parseInt(listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getHcsId());
                }
            }
        } else if (from.equals(TextConstants.LAB_TEST)) {
            type = TextConstants.TYPE_TESTS;
        }

        rebook_or_reschedule = bundle.containsKey("rebook_or_reschedule") ? bundle.getString("rebook_or_reschedule") : "";
        position = bundle.containsKey("position") ? bundle.getString("position") : "";
        if (bundle.containsKey("appointment_model")) {
            appointment_model = new Gson().fromJson(bundle.getString("appointment_model"), appointment.model.appointmodel.List.class);
        } else {
            appointment_model = null;
        }
        REBOOK = bundle.getBoolean(TextConstants.REBOOK);

        responseCallback = this;
        appointmentResponseCallback = this;

        iv_ic_menu_overflow = (IconTextView) getView().findViewById(R.id.iv_ic_menu_overflow);

        initializeAndSetHeaderView();

        btPayNow = (Button) getView().findViewById(R.id.bt_pay_now);
        btPayNow.setText(isBookable ? this.getResources().getString(R.string.txt_book_now) : this.getResources().getString(R.string.txt_request_now));
        mCard_patient_info = (CardView) getView().findViewById(R.id.card_patient_info);
        mScrollView = (NestedScrollView) getView().findViewById(R.id.scrollView);


        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.smoothScrollTo(0, mCard_patient_info.getTop());
            }
        });

        findViewPatientInformation();
    }

    private void findViewPatientInformation() {

        relativeLayoutPatientInfo = (RelativeLayout) getView().findViewById(R.id.relative_patientdetails);
        patientNameLabel = (CustomFontTextView) getView().findViewById(R.id.tv_static_name);
        patientName = (Spinner) getView().findViewById(R.id.spinnerPatientName);
        patientDob = (CustomFontTextView) getView().findViewById(R.id.tv_dob);
        patientGender = (CustomFontTextView) getView().findViewById(R.id.tv_gender);
        patientMobile = (CustomFontEditTextView) getView().findViewById(R.id.ed_phone);
        patientEmail = (CustomFontEditTextView) getView().findViewById(R.id.tv_email);
        reasonLabel = (CustomFontTextView) getView().findViewById(R.id.tv_static_reason);
        tvReason = (Spinner) getView().findViewById(R.id.spinnerReasons);
        editTextComment = (CustomFontEditTextView) getView().findViewById(R.id.ed_comments);
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);

        adapter = new PatientNameAdapter(getActivity(), nameList);
        patientName.setAdapter(adapter);

        reasonAdapter = new ReasonsAdapter(getActivity(), reasonList);
        tvReason.setAdapter(reasonAdapter);

        if (rebook_or_reschedule.equals(TextConstants.RESCHEDULE)) {
            btPayNow.setText(mContext.getResources().getString(R.string.txt_save));
        }

        if (Utils.showDialogForNoNetwork(getActivity(), false) && !BuildConfig.isStartWithPatientInfo) {
            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();
            ThreadManager.getDefaultExecutorService().submit(new GetLinkedPatientsRequest(getTransactionId()));
        } else {
            if (!BuildConfig.isStartWithPatientInfo) {
                UserMO userMO = SettingManager.getInstance().getUserMO();
                try {
                    patientMO = PatientDBAdapter.getInstance(getActivity()).fetchPatientInfo(userMO.getPatientId() + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (patientMO != null) {
                    linkedPatientInfoMOArrayList.clear();
                    LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
                    linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
                    populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
                    showPopupNames();
                }
            } else {
                relativeLayoutPatientInfo.setVisibility(View.GONE);
                getView().findViewById(R.id.card_patient_info).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void bindView() {
        btPayNow.setOnClickListener(this);
        iv_ic_menu_overflow.setOnClickListener(this);
        showPopupReasons();
        patientName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                populatePatientInformationUI(linkedPatientInfoMOArrayList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void showPopupReasons() {
        ArrayList<String> bookingReason = OrderSummary.getInstance().getBookingReason();
        reasonList.clear();

        if (bookingReason.size() > 0) {
            reasonList.addAll(bookingReason);
        } else {
            String[] arrayPopup = getResources().getStringArray(R.array.appointment_reasons);
            for (int i = 0; i < arrayPopup.length; i++) {
                reasonList.add(arrayPopup[i].toString());
            }
        }
        reasonList.add(0, mContext.getResources().getString(R.string.select_one));
        reasonAdapter.notifyDataSetChanged();
        tvReason.setSelection(0);
    }

    private void showPopupNames() {
        int size = linkedPatientInfoMOArrayList.size();
        for (int i = 0; i < size; i++) {
            nameList.add(linkedPatientInfoMOArrayList.get(i).getName());
        }
        adapter.notifyDataSetChanged();
        patientName.setSelection(0);
    }


    private void initializeAndSetHeaderView() {

        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);
        frame_container = (FrameLayout) getView().findViewById(R.id.frame_containers);

        if (from.equals(TextConstants.SPECIALITY)) {
            lyn_lyt_panel.setVisibility(View.VISIBLE);
            frame_container.setVisibility(View.GONE);


            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
            tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

            ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
            mPractitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
            tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
            tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

            ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
            tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

            ViewGroup locations_info = (ViewGroup) inflater.inflate(R.layout.locations_info_layout, lyn_lyt_panel, false);
            mTv_location1 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location3);


            ViewGroup datetime_info = (ViewGroup) inflater.inflate(R.layout.datetime_info_layout, lyn_lyt_panel, false);
            mTv_date = (CustomFontTextView) datetime_info.findViewById(R.id.tv_date);
            mTv_time = (CustomFontTextView) datetime_info.findViewById(R.id.tv_time);

        /*[**** Add View***]*/
            lyn_lyt_panel.addView(speciality_info);
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(doc_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(datetime_info);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                if (isDoctorsAvailable) {
                    lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                    lyn_lyt_panel.addView(doc_info);
                }
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(datetime_info);
            }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
            tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());

            String practionerName = null, practionerSpeciality = null, practionerProfilePic = null;
            String selectedservice = null, location1 = null, location3 = null;
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                practionerName = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerName();
                practionerSpeciality = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getSpecialty();
                practionerProfilePic = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractionerPic();
                selectedservice = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getServiceName();
                location1 = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getLocationName();
                location3 = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getLocationAddress();
                new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                if (isDoctorsAvailable) {
                    practionerName = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerName();
                    practionerProfilePic = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractionerPic();
                    practionerSpeciality = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getSpecialty();
                    location1 = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getLocationName();
                    location3 = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getLocationAddres();
                    new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
                } else {
                    location1 = listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getLocationName();
                    location3 = listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getLocationAddress();
                }
                selectedservice = listServiceGroup.get(0).getServiceName();
            }

            mTv_location1.setText(location1);
            mTv_location2.setVisibility(View.GONE);
            if (Utils.isValueAvailable(location3))
                mTv_location3.setText(location3.toString().trim());
            else {
                mTv_location3.setText("");
                mTv_location3.setVisibility(View.GONE);
            }

            tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
            tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");

            tv_service_names.setText((selectedservice != null && !selectedservice.equals(mContext.getResources().getString(R.string.txt_null))) ? selectedservice : "");

            mTv_date.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.dd_MMM_yyyy, starttime));

            if (DateFormat.is24HourFormat(mContext)) {
                mTv_time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.HH_mm, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.HH_mm, endtime));
            } else {
                mTv_time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.hh_mm_aaa, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.hh_mm_aaa, endtime));
            }
        /*[**** Set Data ***]*/


            OrderSummary.getInstance().setDoctorName(practionerName);
            OrderSummary.getInstance().setPractionerProfilePic(practionerProfilePic);
            OrderSummary.getInstance().setDoctorSpeciality(practionerSpeciality);
            OrderSummary.getInstance().setSpeciality(selctedSpecialityDataModel.getServiceTypeName().toString().trim());
            OrderSummary.getInstance().setServiceNames(selectedservice);
            OrderSummary.getInstance().setLocation1(mTv_location1.getText().toString());
            OrderSummary.getInstance().setLocation2(mTv_location2.getText().toString());
            OrderSummary.getInstance().setLocation3(mTv_location3.getText().toString());
        } else {

            lyn_lyt_panel.setVisibility(View.GONE);
            frame_container.setVisibility(View.VISIBLE);

            mTvdoctor_name = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_name);
            mTvdoctor_speciality = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_speciality);
            mTv_service_names = (CustomFontTextView) getView().findViewById(R.id.tv_service_names);

            mPractitionerPic = (CircularImageView) getView().findViewById(R.id.practitionerPic);

            mInner_doc_info_lyt = (RelativeLayout) getView().findViewById(R.id.inner_doc_info_lyt);
            mDoc_services_lyt = (RelativeLayout) getView().findViewById(R.id.doc_services_lyt);
            mDoc_locations_lyt = (RelativeLayout) getView().findViewById(R.id.doc_locations_lyt);
            mDoc_datetime_lyt = (RelativeLayout) getView().findViewById(R.id.doc_datetime_lyt);

            mServices_dotted_line = (View) getView().findViewById(R.id.services_dotted_line);
            mLocations_dotted_line = (View) getView().findViewById(R.id.locations_dotted_line);
            mDatetime_dotted_line = (View) getView().findViewById(R.id.datetime_dotted_line);

            mTv_location1 = (CustomFontTextView) getView().findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) getView().findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) getView().findViewById(R.id.tv_location3);

            mTv_date = (CustomFontTextView) getView().findViewById(R.id.tv_date);
            mTv_time = (CustomFontTextView) getView().findViewById(R.id.tv_time);

            mDoc_services_lyt.setVisibility(View.VISIBLE);
            mDoc_locations_lyt.setVisibility(View.VISIBLE);
            mDoc_datetime_lyt.setVisibility(View.VISIBLE);

            if (from.equals(TextConstants.DOCTORS)) {
                mInner_doc_info_lyt.setVisibility(View.VISIBLE);
                mTvdoctor_name.setText(list_appointment.get(0).getPractitionerForAppointment().getPractitionerName().toString().trim());
                mTvdoctor_speciality.setText(list_appointment.get(0).getPractitionerForAppointment().getSpecialty().toString().trim());
                if (!isSkipped) {
                    mDoc_services_lyt.setVisibility(View.VISIBLE);
                    mTv_service_names.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getServiceName().toString().trim());
                    mTv_location1.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationName().toString().trim());
                    mTv_location2.setVisibility(View.GONE);
                    if (Utils.isValueAvailable(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationAddress().toString().trim()))
                        mTv_location3.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationAddress().toString().trim());
                    else {
                        mTv_location3.setText("");
                        mTv_location3.setVisibility(View.GONE);
                    }
                } else {
                    mDoc_services_lyt.setVisibility(View.GONE);
                    mTv_location1.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getServiceName().toString().trim());
                    mTv_location2.setVisibility(View.VISIBLE);
                    mTv_location2.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationName().toString().trim());
                    mTv_location3.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim());
                }

                PractitionerForAppointment practitioner = list_appointment.get(0).getPractitionerForAppointment();
                new CommonControls().setProfilePic(mContext, mPractitionerPic, practitioner.getPractionerPic());

            } else if (from.equals(TextConstants.SPECIALITY)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
            } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
                mServices_dotted_line.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mDoc_locations_lyt.setVisibility(View.VISIBLE);
                mTv_service_names.setText(list_appointment.get(0).getTestForAppointment().getTestName().toString().trim());
                mTv_location1.setText(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationName().toString().trim());
                mTv_location2.setVisibility(View.GONE);
                if (Utils.isValueAvailable(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim()))
                    mTv_location3.setText(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim());
                else {
                    mTv_location3.setText("");
                    mTv_location3.setVisibility(View.GONE);
                }
            }
            mTv_date.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.dd_MMM_yyyy, starttime));

            if (DateFormat.is24HourFormat(mContext)) {
                mTv_time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.HH_mm, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.HH_mm, endtime));
            } else {
                mTv_time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.hh_mm_aaa, starttime) + " - " + CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.hh_mm_aaa, endtime));
            }

            OrderSummary.getInstance().setDoctorName(mTvdoctor_name.getText().toString());
            if (from.equals(TextConstants.DOCTORS)) {
                OrderSummary.getInstance().setPractionerProfilePic(list_appointment.get(0).getPractitionerForAppointment().getPractionerPic());
            }
            OrderSummary.getInstance().setDoctorSpeciality(mTvdoctor_speciality.getText().toString());
            OrderSummary.getInstance().setServiceNames(mTv_service_names.getText().toString());
            OrderSummary.getInstance().setLocation1(mTv_location1.getText().toString());
            OrderSummary.getInstance().setLocation2(mTv_location2.getText().toString());
            OrderSummary.getInstance().setLocation3(mTv_location3.getText().toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;


            case R.id.img_labtest_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.img_speciality_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.iv_ic_menu_overflow:
                showPopup(iv_ic_menu_overflow);
                break;

            case R.id.bt_pay_now:
                Utils.hideKeyboard(getActivity());
                if (!checkConstraints() && !BuildConfig.isStartWithPatientInfo)
                    return;

                if (rebook_or_reschedule.equals(TextConstants.RESCHEDULE)) {

                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                        Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        pdialog = new ProgressDialog(mContext);
                        pdialog.setMessage(mContext.getResources().getString(R.string.Rescheduling));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                        long transactionId = getTransactionId();

                    /*[   ......  important code to change the start time and end time to reschedule......  ]*/
                        appointment_model.getStart().setValue(starttime);
                        appointment_model.getEnd().setValue(endtime);
                    /*[   ...... End of important code to change the start time and end time to reschedule......  ]*/
                        ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, appointmentResponseCallback, appointment_model, rebook_or_reschedule, "reason", "comment"));
                    }
                } else {
                    String title, msg;
                    if (from.equals(TextConstants.DOCTORS) || from.equals(TextConstants.SPECIALITY) || from.equals(TextConstants.LAB_TEST)) {
                        title = this.getResources().getString(R.string.confirmation);
                        msg = this.getResources().getString(R.string.txt_dialog_doc_info_msg);
                    } else {
                        title = this.getResources().getString(R.string.txt_dialog_patient_info_title);
                        msg = this.getResources().getString(R.string.txt_dialog_patient_info_msg);
                    }

                    DialogUtils.showAlertDialogCommon(mContext, title, msg, mContext.getResources().getString(R.string.ok), mContext.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case Dialog.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;

                                case Dialog.BUTTON_POSITIVE:
                                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                        Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    dialog.dismiss();
                                    pdialog = new ProgressDialog(mContext);
                                    pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                                    pdialog.setCanceledOnTouchOutside(false);
                                    pdialog.show();
                                    long transactionId = getTransactionId();
                                    String type = null;
                                    if (from.equals(TextConstants.DOCTORS)) {
                                        type = TextConstants.TYPE_DOCTOR;
                                    } else if (from.equals(TextConstants.SPECIALITY)) {
                                        type = TextConstants.TYPE_SPECIALITY;
                                    } else if (from.equals(TextConstants.LAB_TEST)) {
                                        type = TextConstants.TYPE_TESTS;
                                    }
                                    ThreadManager.getDefaultExecutorService().submit(new PlaceTestOrderRequest(transactionId, getPayload(), responseCallback));
                                    break;
                            }
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onGetLinkedPatientsResponse(GetLinkedPatientsResponse
                                                    fetchSelfAndAssociatedPatientsResponse) {
        if (mTransactionId != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;

        dismissProgressDialog();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        try {
            patientMO = PatientDBAdapter.getInstance(getActivity()).fetchPatientInfo(userMO.getPatientId() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful()) {
            linkedPatientInfoMOArrayList.clear();
            LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
            linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);

            int j = 0;
            if (linkedPatientInfoMOArrayList.size() > 0) {
                j = linkedPatientInfoMOArrayList.size();
            }

            for (int i = 0; i < fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().size(); i++) {
                linkedPatientInfoMOArrayList.add(j, fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().get(i));
                j++;
            }
            if (linkedPatientInfoMOArrayList.size() > 0) {
                populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
            }
            showPopupNames();
        } else {
            linkedPatientInfoMOArrayList.clear();
            LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
            linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
            populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
            showPopupNames();
        }
    }

    private void populatePatientInformationUI(LinkedPatientInfoMO linkedPatientInfoMO) {

        // patientName.setText(linkedPatientInfoMO.getName());
        patientId = linkedPatientInfoMO.getPatientId();
        patientMO = linkedPatientInfoMO.getPatientMO();

        String date = ((linkedPatientInfoMO.getDateOfBirth() == null) ? getResources().getString(R.string.not_available) : (DateTimeUtil.calculateDateLocle(linkedPatientInfoMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy_HiphenSepertedDate)));
        if (date == null) {

            date = DateTimeUtil.calculateDateLocle(linkedPatientInfoMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.dd_MMM_yyyy_HiphenSepertedDate);

        }
        patientDob.setText(date);
        patientEmail.setText("");
        patientMobile.setText("");

        if (Utils.isValueAvailable(linkedPatientInfoMO.getEmail()))
            patientEmail.setText(linkedPatientInfoMO.getEmail());
        //displaying phone number properly
        if (Utils.isValueAvailable(linkedPatientInfoMO.getPhoneNumber()))
            patientMobile.setText(linkedPatientInfoMO.getPhoneNumber());

        String gender = linkedPatientInfoMO.getGender();

        if (Utils.isValueAvailable(gender)) {
            if (gender.equals("M") || gender.equalsIgnoreCase("Male")) {
                gender = mContext.getResources().getString(R.string.Male);
            } else {
                gender = mContext.getResources().getString(R.string.Female);
            }
            patientGender.setText(gender);
        }
    }

    private boolean checkConstraints() {
        /*if (TextUtils.isEmpty(patientFirstName.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter first name.", Toast.LENGTH_SHORT).show();
            patientFirstNam e.requestFocus();
            return false;
        }
*/
        /*if (TextUtils.isEmpty(patientLastName.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter last name.", Toast.LENGTH_SHORT).show();
            patientLastName.requestFocus();
            return false;
        }*/

        if (BuildConfig.isStartWithPatientInfo)
            return false;

        if (TextUtils.isEmpty(patientEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_email_address), Toast.LENGTH_SHORT).show();
            patientEmail.requestFocus();
            return false;
        }

        if (!Utils.isEmailValid(patientEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_valid_email), Toast.LENGTH_SHORT).show();
            patientEmail.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(patientMobile.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_your_mobile), Toast.LENGTH_SHORT).show();
            patientMobile.requestFocus();
            return false;
        }
        return true;
    }


    private void initSuccessView(appointment.model.appointmodel.List appointmentmodel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("appointmentmodel", appointmentmodel);
        bundle.putString(TextConstants.TEST_TYPE, from);

        if (from.equals(TextConstants.SPECIALITY)) {
            bundle.putString("selectedSpeciality", selectedSpeciality);
            bundle.putBoolean("isDoctorsAvailable", isDoctorsAvailable);
        }
        SuccessFragment successFragment = new SuccessFragment();
        successFragment.setArguments(bundle);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, successFragment, "successfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


    @Override
    public void passResponse(final String response, boolean mIsSuccessful) {
        bookRequestApiResponse = response;
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissProgressDialog();

                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        DialogUtils.showErrorDialog(mContext,
                                mContext.getResources().getString(R.string.Unsuccessful),
                                response, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    } else {
                        JSONObject mainJasonObject = null;
                        try {
                            mainJasonObject = new JSONObject(response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1001")) {
                            DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.time_slot_unavailable), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1002")) {
                            DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.patient_not_register), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            setDataInOrderSummary();
//                            checkIsToHitUpdateUserProfileCall();
                            if (isToHitUpdateProfileCall) {
                                OrderSummary summary = OrderSummary.getInstance();
                                String email = Utils.isValueAvailable(summary.getEmailAddress()) ? summary.getEmailAddress() : originalEmail;
                                String phone = Utils.isValueAvailable(summary.getPatientMobile()) ? summary.getPatientMobile() : originalPhone;
                                if (isSelfProfileUpdate)
                                    getSelfProfileUpdateRequest(email, phone);
                                else
                                    linkedProfileUpdateRequest(email, phone);
                            } else
                                handleAppointmentBookSuccess(response);

                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }


    private void setDataInOrderSummary() {

        if (!BuildConfig.isStartWithPatientInfo) {
            OrderSummary.getInstance().setPatientName(patientName.getSelectedItem().toString().trim());
            //  OrderSummary.getInstance().setPatientAddress(patientAddress.getText().toString());
            OrderSummary.getInstance().setPatientMobile(patientMobile.getText().toString());
            OrderSummary.getInstance().setEmailAddress(patientEmail.getText().toString());
            OrderSummary.getInstance().setGender(patientGender.getText().toString());
            OrderSummary.getInstance().setDateOfBirth(patientDob.getText().toString());
            OrderSummary.getInstance().setPatientId(patientId);
            OrderSummary.getInstance().setPatientMO(patientMO);
        }
        OrderSummary.getInstance().setComment(editTextComment.getText().toString());

        if (!tvReason.getSelectedItem().toString().equals(getResources().getString(R.string.select_one)))
            OrderSummary.getInstance().setReason(tvReason.getSelectedItem().toString());
        else
            OrderSummary.getInstance().setReason("");

    }


    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissProgressDialog();

                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                        Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
                            if (position.length() > 0 && rebook_or_reschedule.length() > 0) {
                                Gson gson = new Gson();
                                appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
                                Intent intent = new Intent();
                                intent.putExtra("position", position + "");
                                intent.putExtra("cancel_or_reschedule", TextConstants.RESCHEDULE);
                                intent.putExtra("appointment_model", appointmentmodel);
                                getActivity().setResult(Activity.RESULT_OK, intent);
                                getActivity().finish();
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {
    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(mContext, v);
        MenuInflater inflater = popup.getMenuInflater();

        inflater.inflate(R.menu.menu_back, popup.getMenu());

        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.change_time:
                        if (((AppCompatActivity) getActivity()).getSupportFragmentManager().getBackStackEntryCount() > 1) {
                            Fragment patientinformationfragment = ((AppCompatActivity) getActivity()).getSupportFragmentManager().findFragmentByTag("patientinformationfragment");
                            if (patientinformationfragment instanceof PatientInformationFragment) {
                                ((FragmentBaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.title_select_time));
                            }
                            ((AppCompatActivity) getActivity()).getSupportFragmentManager().popBackStack();
                        }
                        break;

                    default:
                        break;
                }

                return true;
            }
        });
    }


    private String getPayload() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.US);
        String formattedDate = df.format(c.getTime());

        UserMO userMO = SettingManager.getInstance().getUserMO();
        long patientid = OrderSummary.getInstance().getPatientId();
        if (patientid == 0)
            patientid = userMO.getPatientId();

        String start, end;

        start = isBookable ? starttime : CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, starttime);
        end = isBookable ? endtime : CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, endtime);

        int minsDuration;
        if (isBookable) {
            minsDuration = (int) new CommonTasks().findDateDifferenceinMins(start, end, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated);
        } else {
            minsDuration = (int) new CommonTasks().findDateDifferenceinMins(start, end, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        }

        String reason;
        if (tvReason.getSelectedItem().toString().equals(getResources().getString(R.string.select_one)))
            reason = "";
        else
            reason = tvReason.getSelectedItem().toString();
        String status = isBookable ? AppointmentConstants.STATUS_BOOKED : AppointmentConstants.STATUS_PROPOSED;

        String payLoad = null;
        JSONObject jsonObject = null;
        try {

            payLoad = "{\"type\":{\"text\":\"" + type + "\"}," +
                    "\"end\":{\"value\":\"" + end + "\"}," +
                    "\"minutesDuration\":{\"value\"" + ": " + minsDuration + "}," +
                    "\"extension\":[{\"url\":\"Audit\",\"value\":[{\"status\":\"" + status + "\"," +
                    "\"actionBy\":\"" + OrderSummary.getInstance().getPatientName() + "\"," +
                    "\"actionReason\":\"" + reason + "\"," +
                    "\"actionComment\":\"" + editTextComment.getText().toString().trim() + "\"," +
                    "\"actionOn\":\"" + formattedDate + "\"}]}]," +
                    "\"participant\":[{\"actorPatient\":{\"id\":" + patientid + ",\"class\":\"com.mphrx.consus.resources.Patient\"},\"type\":[{\"text\":{\"value\":\"SBJ\"}}]}]," +
                    "\"priority\":{\"value\":3}," +
                    "\"start\":{\"value\":\"" + start + "\"}," +
                    "\"status\":{\"value\":\"" + status + "\"}}";

            jsonObject = new JSONObject(payLoad);

            /*[....... Inserting address in extension array...........]*/
            JSONObject j = new JSONObject();
            j.put("url", "SampleCollectionLocation");
            JSONArray ja = new JSONArray();
            ja.put(0, new JSONObject().put("addr", ""));
            j.put("value", ja);
            jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), j);
            /*[....... End of Inserting address in extension array...........]*/


              /*[....... Inserting hcs in participant array...........]*/
            JSONObject hcs_object = new JSONObject();

            JSONObject actorHealthcareService_object = new JSONObject();
            actorHealthcareService_object.put("id", hcsID);
            actorHealthcareService_object.put("class", "com.mphrx.consus.resources.HealthcareService");

            JSONArray type_array = new JSONArray();
            JSONObject type_object = new JSONObject();
            type_object.put("text", new JSONObject().put("value", "HCS"));
            type_array.put(0, type_object);

            hcs_object.put("actorHealthcareService", actorHealthcareService_object);
            hcs_object.put("type", type_array);

            jsonObject.getJSONArray("participant").put(jsonObject.getJSONArray("participant").length(), hcs_object);
              /*[....... End of Inserting hcs in participant array...........]*/




             /*[....... Inserting appointment details in extension array...........]*/

            if (rebook_or_reschedule.equals("")) {
                JSONObject appointmentdetail = new JSONObject();
                appointmentdetail.put("url", "appointmentDetails");

                JSONArray value_array = new JSONArray();
                if (from.equals(TextConstants.DOCTORS)) {
                    String instruction = list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(0).getInstructions();
                    value_array.put(0, new JSONObject().put("useAppointmentDetailsFromExtension", true)
                            .put("serviceName", mTv_service_names.getText().toString().trim())
                            .put("practitionerName", mTvdoctor_name.getText().toString().trim())
                            .put("specialityName", "")
                            .put("locationDetails", mTv_location1.getText().toString().trim() + "\n" + mTv_location3.getText().toString().trim())
                            .put("roomDetails", "")
                            .put("email", OrderSummary.getInstance().getEmailAddress() != null ? OrderSummary.getInstance().getEmailAddress() : "")
                            .put("phoneNo", OrderSummary.getInstance().getPatientMobile() != null ? OrderSummary.getInstance().getPatientMobile() : "")
                            .put("timeZoneId", TimeZone.getDefault().getID().toString().trim())
                            .put("department", list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getDepartmentName().toString().trim())
                            .put("characteristic", new JSONArray().put(0, new JSONObject().put("coding", new JSONArray().put(0, new JSONObject().put("code", "ST-1").put("display", (instruction != null && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) ? instruction : ""))).put("text", "instructions")))
                            .put("cost", "0"));

                    JSONObject practitionerDetails = new JSONObject();
                    practitionerDetails.put("url", "practitionerDetails");
                    JSONArray value_array_practitioner = new JSONArray();
                    value_array_practitioner.put(0, new JSONObject().put("practitionerId", list_appointment.get(0).getPractitionerForAppointment().getPractitionerId().toString().trim()));
                    practitionerDetails.put("value", value_array_practitioner);
                    jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), practitionerDetails);
                } else if (from.equals(TextConstants.SPECIALITY)) {

                    String practitionerName = "", serviceName = "", specialityName = "", locationDetails = "", department = "", instruction = "", practitionerId = "";

                    if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                        instruction = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(0).getInstructions();
                        practitionerName = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerName();
                        serviceName = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getServiceName();
                        specialityName = selctedSpecialityDataModel.getServiceTypeName();
                        locationDetails = mTv_location1.getText().toString().trim() + "\n" + mTv_location3.getText().toString().trim();
                        department = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getDepartmentName();
                        practitionerId = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerId();
                    } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                        if (isDoctorsAvailable) {
                            instruction = listServiceGroup.get(0).getHcsWithLocationList().get(0).getInstructions();
                            practitionerName = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerName();
                            serviceName = listServiceGroup.get(0).getServiceName();
                            specialityName = selctedSpecialityDataModel.getServiceTypeName();
                            locationDetails = mTv_location1.getText().toString().trim() + "\n" + mTv_location3.getText().toString().trim();
                            department = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getDepartmentName();
                            practitionerId = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerId();
                        } else {
                            instruction = listServiceGroup.get(0).getHcsWithLocationList().get(0).getInstructions();
                            practitionerName = "";
                            serviceName = listServiceGroup.get(0).getServiceName();
                            specialityName = selctedSpecialityDataModel.getServiceTypeName();
                            locationDetails = mTv_location1.getText().toString().trim() + "\n" + mTv_location3.getText().toString().trim();
                            department = listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getDepartmentName();
                        }
                    }
                    value_array.put(0, new JSONObject().put("useAppointmentDetailsFromExtension", true)
                            .put("practitionerName", practitionerName)
                            .put("serviceName", serviceName)
                            .put("specialityName", specialityName)
                            .put("locationDetails", locationDetails)
                            .put("roomDetails", "")
                            .put("email", OrderSummary.getInstance().getEmailAddress() != null ? OrderSummary.getInstance().getEmailAddress() : "")
                            .put("phoneNo", OrderSummary.getInstance().getPatientMobile() != null ? OrderSummary.getInstance().getPatientMobile() : "")
                            .put("timeZoneId", TimeZone.getDefault().getID().toString().trim())
                            .put("department", department)
                            .put("characteristic", new JSONArray().put(0, new JSONObject().put("coding", new JSONArray().put(0, new JSONObject().put("code", "ST-1").put("display", (instruction != null && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) ? instruction : ""))).put("text", "instructions")))
                            .put("cost", "0"));

                    if (isDoctorsAvailable) {
                        JSONObject practitionerDetails = new JSONObject();
                        practitionerDetails.put("url", "practitionerDetails");
                        JSONArray value_array_practitioner = new JSONArray();
                        value_array_practitioner.put(0, new JSONObject().put("practitionerId", practitionerId));
                        practitionerDetails.put("value", value_array_practitioner);
                        jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), practitionerDetails);
                    }

                } else if (from.equals(TextConstants.LAB_TEST)) {
                    String instruction = list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getInstructions();
                    value_array.put(0, new JSONObject().put("useAppointmentDetailsFromExtension", true)
                            .put("serviceName", mTv_service_names.getText().toString().trim())
                            .put("practitionerName", "")
                            .put("specialityName", "")
                            .put("locationDetails", mTv_location1.getText().toString().trim() + "\n" + mTv_location3.getText().toString().trim())
                            .put("roomDetails", "")
                            .put("email", OrderSummary.getInstance().getEmailAddress() != null ? OrderSummary.getInstance().getEmailAddress() : "")
                            .put("phoneNo", OrderSummary.getInstance().getPatientMobile() != null ? OrderSummary.getInstance().getPatientMobile() : "")
                            .put("timeZoneId", TimeZone.getDefault().getID().toString().trim())
                            .put("department", list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getDepartmentName().toString().trim())
                            .put("characteristic", new JSONArray().put(0, new JSONObject().put("coding", new JSONArray().put(0, new JSONObject().put("code", "ST-1").put("display", (instruction != null && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) ? instruction : ""))).put("text", "instructions")))
                            .put("cost", "0"));
                }
                appointmentdetail.put("value", value_array);
                jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), appointmentdetail);
            }
            /*[....... End of Inserting appointment details in extension array...........]*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }


    private void checkIsToHitUpdateUserProfileCall() {
        userMO = SettingManager.getInstance().getUserMO();
        isToHitUpdateProfileCall = false;
        String email = "", phone = "";
        OrderSummary summary = OrderSummary.getInstance();
        email = summary.getEmailAddress();
        phone = summary.getPatientMobile();
        patientMO = summary.getPatientMO();
        if (summary.getPatientId() == userMO.getPatientId())//initializing actual values send by user.
        {
            isSelfProfileUpdate = true;
        } else //initializing actual values send for linked patient
        {
            isSelfProfileUpdate = false;
        }

        originalEmail = Utils.isValueAvailable(patientMO.getPrimaryEmail()) ? patientMO.getPrimaryEmail() : "";
        originalPhone = Utils.isValueAvailable(patientMO.getPrimaryPhone()) ? patientMO.getPrimaryPhone() : "";

        if (!email.equalsIgnoreCase(originalEmail))
            isToHitUpdateProfileCall = true;
        else if (!phone.endsWith(originalPhone))
            isToHitUpdateProfileCall = true;
    }

    private void getSelfProfileUpdateRequest(String email, String phoneNo) {
        User userdata = new User();
        userdata.setId(userMO.getId());
        userdata.setFirstName(userMO.getFirstName());
        userdata.setLastName(userMO.getLastName());
        String gender = userMO.getGender();
        if (BuildConfig.isPatientApp) {
            if (gender != null && !gender.equals(GenderEnum.getGenderEnumLinkedHashMap().get(getString(R.string.gender_other_key)).getValue())) {
                userdata.setGender(gender.length() == 1 ? gender.equalsIgnoreCase(mContext.getResources().getString(R.string.gender_male_initial)) ? mContext.getResources().getString(R.string.gender_male_key) :
                        mContext.getResources().getString(R.string.gender_female_key) : gender);
            }
            Weight wt = new Weight();
            wt.setValue(userMO.getWeight().getValue());
            wt.setUnit(userMO.getWeight().getUnit());
            userdata.setWeight(wt);
            Height ht = new Height();
            ht.setValue(userMO.getHeight().getValue());
            ht.setUnit(userMO.getHeight().getUnit());
            userdata.setHeight(ht);
            String dob = userMO.getDob();
            try {
                if (Utils.isValueAvailable(dob)) {
                    userdata.setDob(DateTimeUtil.convertSourceDestinationDate(dob, DateTimeUtil
                            .destinationDateFormatWithoutTime, DateTimeUtil
                            .yyyy_MM_dd_T_HH_mm_ss_z_appended));
                } else {
                    dob = Utils.getFormattedDate(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime);
                    userdata.setDob(dob);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                userdata.setDob("");
            }
        }

        userdata.setEmail(email);
        userdata.setPhoneNo(phoneNo);
        userdata.setAlternateContact(userMO.getAlternateContact());
        UpdatePatientRequest req = new UpdatePatientRequest();
        req.setUser(userdata);
        req.setUserType(Utils.getUserType());
        patientMO = updatePatientPhoneAndEmail(patientMO, email, phoneNo);
        req.setPatient(patientMO);
        new updateUserProfileBG(getActivity(), this, req, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private PatientMO updatePatientPhoneAndEmail(PatientMO patientMo, String email, String phoneNo) {
        if (Utils.isValueAvailable(email))
            patientMo.setPrimaryEmail(email, patientMo.getPrimaryEmail());
        if (Utils.isValueAvailable(phoneNo))
            patientMo.setPrimaryPhone(phoneNo, patientMo.getPrimaryPhone());

        return patientMo;
    }

    public void dismissProgressDialog() {
        if (pdialog != null && pdialog.isShowing()) {
            pdialog.dismiss();
            pdialog = null;
        }
    }

    public void handleAppointmentBookSuccess(String response) {
        dismissProgressDialog();
                    /*[......... Parsing the appointment model...........]*/
        Gson gson = new Gson();
        appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
            /*[......... End of Parsing the appointment model...........]*/

        if (appointmentmodel.getStatus().equalsIgnoreCase("Booked") || appointmentmodel.getStatus().equalsIgnoreCase("Proposed")) {
            initSuccessView(appointmentmodel);

            if (rebook_or_reschedule.equals(TextConstants.REBOOK)) {
                Intent intent = new Intent();
                intent.putExtra("appointment_model", appointmentmodel);
                intent.putExtra("position", position);
                getActivity().setResult(TextConstants.REBOOK_CONSTANT, intent);
            }

        } else {
            DialogUtils.showErrorDialog(mContext,
                    mContext.getResources().getString(R.string.Appointment_not_booked),
                    appointmentmodel.getStatus(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        }

    }


    public void updateProfileResponse(updatePatientResponse responseGson, String result) {
        UserDBAdapter adapter = UserDBAdapter.getInstance(getActivity());
        if (responseGson != null && result.equals(TextConstants.SUCESSFULL_API_CALL)) {
            SharedPref.setMobileNumber(responseGson.getUser().getPhoneNumber());
            SharedPref.setEmailAddress(responseGson.getUser().getEmail());
            userMO.setPhoneNumber(SharedPref.getMobileNumber());
            userMO.setEmail(SharedPref.getEmailAddress());
            ContentValues contentValues = new ContentValues();
            contentValues.put(adapter.getPHONENUMBER(), responseGson.getUser().getPhoneNumber());
            contentValues.put(adapter.getEMAILADDRESS(), responseGson.getUser().getEmail());
            adapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
            try {
                PatientDBAdapter.getInstance(getActivity()).createOrUpdatePatientMo(responseGson.getPatient());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        handleAppointmentBookSuccess(bookRequestApiResponse);

    }

    public void updateLinkedPatientProfileResponse() {
        //weather sucess of failure of UpdatePatient Profile Response redirection to
        handleAppointmentBookSuccess(bookRequestApiResponse);
    }


    private void linkedProfileUpdateRequest(String email, String phoneNo) {
        patientMO = updatePatientPhoneAndEmail(patientMO, email, phoneNo);
        new UpdateLinkedPatientBG(getActivity(), this, patientMO).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
