package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.adapter.SpecialityLocationsAdapter;
import appointment.interfaces.Animable;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 2/7/2017.
 */

public class SpecialityLocationsFragment extends SupportBaseFragment implements View.OnClickListener, SpecialityLocationsAdapter.SpecialityLocationsOnClick {

    private Context mContext;
    private Animable animable;
    private List<AppointmentModel> list;
    private List<ServiceGroup> listServiceGroup;
    private String from;
    private SpecialityLocationsAdapter.SpecialityLocationsOnClick specialityLocationsOnClick;
    private RecyclerView mRv_speciality_type;
    private SpecialityLocationsAdapter mSpecialityLocationsAdapter;
    private CustomFontTextView txt_select;
    private SpecialityDataModel selctedSpecialityDataModel;
    private String selectedSpeciality;
    private int selectedService, selectedDoctorServices;
    private boolean isBookable = false;


    private RelativeLayout mData_lyt, mProgress_error_lyt, mProgress_lyt;
    private CustomFontTextView mError_txt_view;
    private ProgressBar mProgress;

    private LinearLayout lyn_lyt_panel;
    private CircularImageView practitionerPic;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;
    private CustomFontTextView tv_service_names;
    private boolean isDoctorsAvailable = true;


    public SpecialityLocationsFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.locations));
        specialityLocationsOnClick = this;
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.select_speciality_type_fragment;

    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);

        selectedSpeciality = bundle.getString("selectedSpeciality");
        if (bundle.containsKey("selectedService")) {
            selectedService = bundle.getInt("selectedService");
        }
        if (bundle.containsKey("specialitymodel")) {
            selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
        }


        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            list = bundle.getParcelableArrayList("alldata");
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            listServiceGroup = bundle.getParcelableArrayList("alldata");
            selectedDoctorServices = bundle.getInt("selectedDoctorServices");
            if (bundle.containsKey("isDoctorsAvailable")) {
                isDoctorsAvailable = bundle.getBoolean("isDoctorsAvailable");
            }
        }

        txt_select = (CustomFontTextView) getView().findViewById(R.id.txt_select);

        mRv_speciality_type = (RecyclerView) getView().findViewById(R.id.rv_speciality_type);

        mData_lyt = (RelativeLayout) getView().findViewById(R.id.data_lyt);
        mProgress_error_lyt = (RelativeLayout) getView().findViewById(R.id.progress_error_lyt);
        mProgress_lyt = (RelativeLayout) getView().findViewById(R.id.progress_lyt);
        mError_txt_view = (CustomFontTextView) getView().findViewById(R.id.error_txt_view);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);


        setHeaderInfo();// Adding Selected Speciality Info dynamically...


        mProgress_error_lyt.setVisibility(View.GONE);
        mData_lyt.setVisibility(View.VISIBLE);

    }


    @Override
    public void initView() {
        mRv_speciality_type.setLayoutManager(new LinearLayoutManager(mContext));
        //  mRv_speciality_type.setNestedScrollingEnabled(false);
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            mSpecialityLocationsAdapter = new SpecialityLocationsAdapter(getActivity(), list, from, specialityLocationsOnClick, selectedSpeciality, selectedService, isDoctorsAvailable);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            mSpecialityLocationsAdapter = new SpecialityLocationsAdapter(listServiceGroup, getActivity(), from, specialityLocationsOnClick, selectedSpeciality, selectedDoctorServices, isDoctorsAvailable);
        }
        mRv_speciality_type.setAdapter(mSpecialityLocationsAdapter);


    }

    @Override
    public void bindView() {
        txt_select.setText(mContext.getResources().getString(R.string.locations));

    }


    public void setHeaderInfo() {
        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
        tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

        ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
        practitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
        tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
        tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

        ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
        tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

        /*[**** Add View***]*/
        lyn_lyt_panel.addView(speciality_info);
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(doc_info);
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(services_info);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(services_info);
            if (isDoctorsAvailable) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(doc_info);
            }
        }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
        tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());

        String practionerName = null, practionerSpeciality = null, practitionerProfilePic = null;
        String selectedservice = null;
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            practionerName = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerName();
            practionerSpeciality = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getSpecialty();
            practitionerProfilePic = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractionerPic();
            selectedservice = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getServiceName();
            tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
            tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");
            new CommonControls().setProfilePic(mContext, practitionerPic, practitionerProfilePic);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            if (isDoctorsAvailable) {
                practionerName = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerName();
                practionerSpeciality = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getSpecialty();
                practitionerProfilePic = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractionerPic();
                tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
                tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");
                new CommonControls().setProfilePic(mContext, practitionerPic, practitionerProfilePic);
            }
            selectedservice = listServiceGroup.get(0).getServiceName();
        }


        tv_service_names.setText((selectedservice != null && !selectedservice.equals(mContext.getResources().getString(R.string.txt_null))) ? selectedservice : "");
        /*[**** Set Data ***]*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }


    @Override
    public void onSpecialityLocationsClick(int adapterPosition) {
        Bundle bundle = new Bundle();
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            bundle.putInt("selectedService", selectedService);
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            try {
                isBookable = new CommonTasks().checkBookableOrRequestable(list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(adapterPosition).getRoles());
            } catch (Exception ex) {
                isBookable = false;
            }
            bundle.putBoolean("isBookable", isBookable);
            bundle.putBoolean("isskipped", false);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            bundle.putBoolean("isDoctorsAvailable", isDoctorsAvailable);
            bundle.putInt("selectedDoctorServices", selectedDoctorServices);
            bundle.putInt("selectedLocation", adapterPosition);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) listServiceGroup);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putBoolean("isskipped", false);

            if (isDoctorsAvailable) {
                try {
                    isBookable = new CommonTasks().checkBookableOrRequestable(listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(adapterPosition).getRoles());
                } catch (Exception ex) {
                    isBookable = false;
                }
            } else {
                try {
                    isBookable = new CommonTasks().checkBookableOrRequestable(listServiceGroup.get(0).getHcsWithLocationList().get(adapterPosition).getRoles());
                } catch (Exception ex) {
                    isBookable = false;
                }
            }
            bundle.putBoolean("isBookable", isBookable);
        }

        SelectTimeFragment timeFragment = new SelectTimeFragment();
        timeFragment.setArguments(bundle);
        ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, timeFragment, "selecttimefragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }
}