package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.LocationsAdapter;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.model.appointmentcommonmodel.TestForAppointment;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 11/11/2016.
 */

public class SelectLocationsFragment extends SupportBaseFragment implements View.OnClickListener {

    private Context mContext;
    private List<AppointmentModel> listAppointmentModel = new ArrayList<AppointmentModel>();
    private List<TestForAppointment> listTestAppointment = new ArrayList<TestForAppointment>();
    private String from;


    private RecyclerView mRv_locations;
    private LocationsAdapter mLocationsAdapter;


    private CustomFontTextView mTxt_select_location;

    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality, mTv_service_names;
    private CircularImageView mPractitionerPic;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    private View mServices_dotted_line;
    /*[ Header View Instances ]*/

    private boolean isSkipped;
    private int selectedServicePosition;


    public SelectLocationsFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(mContext.getResources().getString(R.string.title_select_location));
        return R.layout.selectlocationsfragment;
    }


    @Override
    public void findView() {

        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);

        if (from.equals(TextConstants.DOCTORS)) {
            isSkipped = bundle.getBoolean("isskipped");
            if (!isSkipped) {
                selectedServicePosition = bundle.getInt("selectedservice");
            }
        }
        //this is the case of lab test - Aastha
        if (bundle.containsKey("appointmentmodel")) {
            listAppointmentModel.clear();
            listAppointmentModel.add((AppointmentModel) bundle.getParcelable("appointmentmodel"));
        }

        mRv_locations = (RecyclerView) getView().findViewById(R.id.rv_locations);
        mTxt_select_location = (CustomFontTextView) getView().findViewById(R.id.txt_select_location);
        initializeAndSetHeaderView();

    }


    @Override
    public void initView() {
        //  img_speciality_delete.setOnClickListener(this);
        mRv_locations.setLayoutManager(new LinearLayoutManager(mContext));
        //  mRv_services.setNestedScrollingEnabled(false);
        mLocationsAdapter = new LocationsAdapter(getActivity(), listAppointmentModel, from, selectedServicePosition, isSkipped);
        mRv_locations.setAdapter(mLocationsAdapter);
    }

    @Override
    public void bindView() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }


    private void initializeAndSetHeaderView() {
        mTvdoctor_name = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_name);
        mTvdoctor_speciality = (CustomFontTextView) getView().findViewById(R.id.tvdoctor_speciality);
        mTv_service_names = (CustomFontTextView) getView().findViewById(R.id.tv_service_names);

        mPractitionerPic = (CircularImageView) getView().findViewById(R.id.practitionerPic);

        mInner_doc_info_lyt = (RelativeLayout) getView().findViewById(R.id.inner_doc_info_lyt);
        mDoc_services_lyt = (RelativeLayout) getView().findViewById(R.id.doc_services_lyt);
        mDoc_locations_lyt = (RelativeLayout) getView().findViewById(R.id.doc_locations_lyt);
        mDoc_datetime_lyt = (RelativeLayout) getView().findViewById(R.id.doc_datetime_lyt);

        mServices_dotted_line = (View) getView().findViewById(R.id.services_dotted_line);


        mDoc_services_lyt.setVisibility(View.VISIBLE);
        mDoc_locations_lyt.setVisibility(View.GONE);
        mDoc_datetime_lyt.setVisibility(View.GONE);

        if (from.equals(TextConstants.DOCTORS)) {
            mInner_doc_info_lyt.setVisibility(View.VISIBLE);
            mTvdoctor_name.setText(listAppointmentModel.get(0).getPractitionerForAppointment().getPractitionerName().toString().trim());
            mTvdoctor_speciality.setText(listAppointmentModel.get(0).getPractitionerForAppointment().getSpecialty().toString().trim());
            if (!isSkipped) {
                mTxt_select_location.setText(mContext.getResources().getString(R.string.locations));
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mTv_service_names.setText(listAppointmentModel.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getServiceName().toString().trim());
            } else {
                mDoc_services_lyt.setVisibility(View.GONE);
                mTxt_select_location.setText(mContext.getResources().getString(R.string.locations_and_services));
            }

            PractitionerForAppointment practitioner = listAppointmentModel.get(0).getPractitionerForAppointment();
            new CommonControls().setProfilePic(mContext, mPractitionerPic, practitioner.getPractionerPic());


        } else if (from.equals(TextConstants.SPECIALITY)) {
            mTxt_select_location.setText(mContext.getResources().getString(R.string.locations));
        } else {
            if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                mTxt_select_location.setText(mContext.getResources().getString(R.string.locations));
                mInner_doc_info_lyt.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mServices_dotted_line.setVisibility(View.GONE);
                mTv_service_names.setText(listAppointmentModel.get(0).getTestForAppointment().getTestName().toString().trim());

            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                break;

        }
    }
}