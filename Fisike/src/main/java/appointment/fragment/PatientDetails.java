package appointment.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.PatientDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import appointment.BookDoctorActivity;
import appointment.BookSpecialityActivity;
import appointment.HomeSampleCollectionActivity;
import appointment.adapter.PatientNameAdapter;
import appointment.model.LinkedPatientInfoMO;
import appointment.model.OrderSummary;
import appointment.request.GetLinkedPatientsRequest;
import appointment.request.GetLinkedPatientsResponse;
import appointment.utils.SupportBaseFragment;

/**
 * Created by aastha on 11/30/2016.
 */
public class PatientDetails extends SupportBaseFragment implements View.OnClickListener {

    private Context mContext;
    private List<String> nameList = new ArrayList<String>();
    private String toolbar_title_text;
    private CustomFontTextView patientNameLabel, patientDob, patientGender;
    private Spinner patientName;
    private CustomFontEditTextView patientMobile, patientEmail, countryCode;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private ArrayList<LinkedPatientInfoMO> linkedPatientInfoMOArrayList = new ArrayList<LinkedPatientInfoMO>();
    private CustomFontButton btNext;
    private int width;
    private PopupWindow mPopup;
    private String testType;
    private long patientId;
    private PatientMO patientMO;
    private PatientNameAdapter adapter;

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        return R.layout.fragment_patientdetails;
    }

    @Override
    public void findView() {
        patientNameLabel = (CustomFontTextView) getView().findViewById(R.id.tv_static_name);
        patientName = (Spinner) getView().findViewById(R.id.spinnerPatientName);
        patientDob = (CustomFontTextView) getView().findViewById(R.id.tv_dob);
        patientGender = (CustomFontTextView) getView().findViewById(R.id.tv_gender);
        patientMobile = (CustomFontEditTextView) getView().findViewById(R.id.ed_phone);
        patientEmail = (CustomFontEditTextView) getView().findViewById(R.id.tv_email);
        // countryCode = (CustomFontEditTextView) getView().findViewById(R.id.ed_country_code);
        btNext = (CustomFontButton) getView().findViewById(R.id.bt_pay_now);
    }

    @Override
    public void initView() {

        adapter = new PatientNameAdapter(getActivity(), nameList);
        patientName.setAdapter(adapter);

        testType = getArguments().getString(TextConstants.FROM);
        BusProvider.getInstance().register(this);

        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();
            ThreadManager.getDefaultExecutorService().submit(new GetLinkedPatientsRequest(getTransactionId()));
        } else {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            try {
                patientMO = PatientDBAdapter.getInstance(getActivity()).fetchPatientInfo(userMO.getPatientId() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }


            showPopupNames();

            linkedPatientInfoMOArrayList.clear();
            if (patientMO != null) {
                LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
                linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
                populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
            }

        }

        patientMobile.setKeyListener(DigitsKeyListener.getInstance("0123456789+"));
    }

    private void populatePatientInformationUI(LinkedPatientInfoMO linkedPatientInfoMO) {
        //  patientName.setText(linkedPatientInfoMO.getName());
        patientId = linkedPatientInfoMO.getPatientId();
        patientMO = linkedPatientInfoMO.getPatientMO();


        String date = ((linkedPatientInfoMO.getDateOfBirth() == null || linkedPatientInfoMO.getDateOfBirth().equals(getResources().getString(R.string.txt_null))) ? getResources().getString(R.string.not_available) : (DateTimeUtil.calculateDateLocle(linkedPatientInfoMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy_HiphenSepertedDate)));
        if (date == null) {
            date = DateTimeUtil.calculateDateLocle(linkedPatientInfoMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.dd_MMM_yyyy_HiphenSepertedDate);
        }
        patientDob.setText(date);
        patientEmail.setText("");
        patientMobile.setText("");
        if (Utils.isValueAvailable(linkedPatientInfoMO.getEmail()))
            patientEmail.setText(linkedPatientInfoMO.getEmail());

        if (Utils.isValueAvailable(linkedPatientInfoMO.getPhoneNumber()))
            patientMobile.setText(linkedPatientInfoMO.getPhoneNumber());

        String gender = linkedPatientInfoMO.getGender();

        if (Utils.isValueAvailable(gender)) {
            if (gender.equals("M") || gender.equalsIgnoreCase("Male")) {
                gender = mContext.getResources().getString(R.string.Male);
            } else {
                gender = mContext.getResources().getString(R.string.Female);
            }
            patientGender.setText(gender);
        }

        //  countryCode.setText(linkedPatientInfoMO.getPhoneNumber().substring(0, 3));
    }

    private long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void bindView() {
        btNext.setOnClickListener(this);
        patientName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                populatePatientInformationUI(linkedPatientInfoMOArrayList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showPopupNames() {
        int size = linkedPatientInfoMOArrayList.size();
        for (int i = 0; i < size; i++) {
            nameList.add(linkedPatientInfoMOArrayList.get(i).getName());
        }
        adapter.notifyDataSetChanged();
        patientName.setSelection(0);
    }

    @Override
    public void onClick(View v) {
        if (!checkConstraints())
            return;
        //       ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.LE_THEME);
        Utils.hideKeyboard(getActivity());
        Intent intent = null;
        if (testType.equals(TextConstants.DOCTORS))
            intent = new Intent(getActivity(), BookDoctorActivity.class);
        else if (testType.equals(TextConstants.SERVICES)) {
            Bundle bundle = new Bundle();
            bundle.putString(TextConstants.TEST_TYPE, TextConstants.SERVICES);
            intent = new Intent(getActivity(), HomeSampleCollectionActivity.class);
            intent.putExtras(bundle);
        } else if (testType.equals(TextConstants.LAB_TEST)) {
            Bundle bundle = new Bundle();
            bundle.putString(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
            intent = new Intent(getActivity(), HomeSampleCollectionActivity.class);
            intent.putExtras(bundle);
        } else if (testType.equals(TextConstants.SPECIALITY)) {
            intent = new Intent(getActivity(), BookSpecialityActivity.class);
        }
        setDataInOrderSummary();
        getActivity().startActivity(intent);
    }

    private void setDataInOrderSummary() {
        OrderSummary.getInstance().setPatientName(patientName.getSelectedItem().toString().trim());
        //  OrderSummary.getInstance().setPatientAddress(patientAddress.getText().toString());
        OrderSummary.getInstance().setPatientMobile(patientMobile.getText().toString());
        OrderSummary.getInstance().setEmailAddress(patientEmail.getText().toString());
        OrderSummary.getInstance().setGender(patientGender.getText().toString());
        OrderSummary.getInstance().setDateOfBirth(patientDob.getText().toString());
        OrderSummary.getInstance().setPatientId(patientId);
        OrderSummary.getInstance().setPatientMO(patientMO);
    }


    private boolean checkConstraints() {
        if (TextUtils.isEmpty(patientEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_email_address), Toast.LENGTH_SHORT).show();
            patientEmail.requestFocus();
            return false;
        }

        if (!Utils.isEmailValid(patientEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_valid_email), Toast.LENGTH_SHORT).show();
            patientEmail.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(patientMobile.getText().toString().trim())) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.enter_your_mobile), Toast.LENGTH_SHORT).show();
            patientMobile.requestFocus();
            return false;
        }


        return true;
    }

    @Subscribe
    public void onGetLinkedPatientsResponse(GetLinkedPatientsResponse fetchSelfAndAssociatedPatientsResponse) {


        if (mTransactionId != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;

        pdialog.dismiss();
        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful()) {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            linkedPatientInfoMOArrayList.clear();
            try {
                patientMO = PatientDBAdapter.getInstance(getActivity()).fetchPatientInfo(userMO.getPatientId() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
            linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
            int j = 0;
            if (linkedPatientInfoMOArrayList.size() > 0) {
                j = linkedPatientInfoMOArrayList.size();
            }
            for (int i = 0; i < fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().size(); i++) {
                linkedPatientInfoMOArrayList.add(j, fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().get(i));
                j++;
            }

            if (linkedPatientInfoMOArrayList.size() > 0) {
                populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
            }
            showPopupNames();
            /*String [] arrayPopupString;
            if(OrderSummary.getInstance().getBookingReason().size()>0){
                ArrayList<String> bookingReason = OrderSummary.getInstance().getBookingReason();
                arrayPopupString = bookingReason.toArray(new String[bookingReason.size()]);
            }
            else
            { CharSequence[] arrayPopup = getResources().getTextArray(R.array.appointment_reasons);
              arrayPopupString = new String[arrayPopup.length];
            for (int i = 0; i < arrayPopup.length; i++) {
                arrayPopupString[i] = arrayPopup[i].toString();
            }
            }
            tvReason.setText(arrayPopupString[0].toString());*/
        } else {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            try {
                patientMO = PatientDBAdapter.getInstance(getActivity()).fetchPatientInfo(userMO.getPatientId() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            linkedPatientInfoMOArrayList.clear();
            LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), patientMO, userMO.getCoverage());
            linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
            populatePatientInformationUI(linkedPatientInfoMOArrayList.get(0));
            showPopupNames();
            /*CharSequence[] arrayPopup = getResources().getTextArray(R.array.appointment_reasons);
            String[] arrayPopupString = new String[arrayPopup.length];
            for (int i = 0; i < arrayPopup.length; i++) {
                arrayPopupString[i] = arrayPopup[i].toString();
            }
            tvReason.setText(arrayPopupString[0].toString());*/
        }
    }
}
