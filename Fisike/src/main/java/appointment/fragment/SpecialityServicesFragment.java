package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.adapter.SpecialityServicesAdapter;
import appointment.interfaces.Animable;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 2/6/2017.
 */

public class SpecialityServicesFragment extends SupportBaseFragment implements View.OnClickListener, SpecialityServicesAdapter.SpecialityServicesOnClick {

    private Context mContext;
    private Animable animable;
    private List<AppointmentModel> list;
    private List<ServiceGroup> listSeviceGroup = new ArrayList<ServiceGroup>();
    private String from;
    private SpecialityServicesAdapter.SpecialityServicesOnClick specialityServicesOnClick;
    private RecyclerView mRv_speciality_type;
    private SpecialityServicesAdapter mSpecialityServicesAdapter;
    private CustomFontTextView txt_select;
    private SpecialityDataModel selctedSpecialityDataModel;
    private String selectedSpeciality;


    private RelativeLayout mData_lyt, mProgress_error_lyt, mProgress_lyt;
    private CustomFontTextView mError_txt_view;
    private ProgressBar mProgress;

    private LinearLayout lyn_lyt_panel;
    private CircularImageView practitionerPic;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;

    public SpecialityServicesFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.Services));
        specialityServicesOnClick = this;
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.select_speciality_type_fragment;
    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        list = bundle.getParcelableArrayList("alldata");
        selectedSpeciality = bundle.getString("selectedSpeciality");
        if (bundle.containsKey("specialitymodel")) {
            selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
        }


        txt_select = (CustomFontTextView) getView().findViewById(R.id.txt_select);
        mRv_speciality_type = (RecyclerView) getView().findViewById(R.id.rv_speciality_type);

        mData_lyt = (RelativeLayout) getView().findViewById(R.id.data_lyt);
        mProgress_error_lyt = (RelativeLayout) getView().findViewById(R.id.progress_error_lyt);
        mProgress_lyt = (RelativeLayout) getView().findViewById(R.id.progress_lyt);
        mError_txt_view = (CustomFontTextView) getView().findViewById(R.id.error_txt_view);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);

        setHeaderInfo();// Adding Selected Speciality Info dynamically...

        mProgress_error_lyt.setVisibility(View.GONE);
        mData_lyt.setVisibility(View.VISIBLE);



        /*[**** Fetching ServiceGroup Values ****]*/
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            listSeviceGroup.clear();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getSpecialtyForAppointment().getServiceGroup() != null) {
                    listSeviceGroup.add(list.get(i).getSpecialtyForAppointment().getServiceGroup());
                }
            }
        }
       /*[**** Fetching ServiceGroup Values ****]*/


    }


    @Override
    public void initView() {
        mRv_speciality_type.setLayoutManager(new LinearLayoutManager(mContext));
        //  mRv_speciality_type.setNestedScrollingEnabled(false);
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            mSpecialityServicesAdapter = new SpecialityServicesAdapter(getActivity(), list, from, specialityServicesOnClick, selectedSpeciality);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            mSpecialityServicesAdapter = new SpecialityServicesAdapter(listSeviceGroup, getActivity(), from, specialityServicesOnClick, selectedSpeciality);
        }
        mRv_speciality_type.setAdapter(mSpecialityServicesAdapter);


    }

    @Override
    public void bindView() {
        txt_select.setText(mContext.getResources().getString(R.string.Services));

    }


    public void setHeaderInfo() {
        lyn_lyt_panel = (LinearLayout) getView().findViewById(R.id.lyn_lyt_panel);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
        tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

        ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
        practitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
        tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
        tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

        /*[**** Add View***]*/
        lyn_lyt_panel.addView(speciality_info);
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(doc_info);
        }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
        tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            String practionerName = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerName();
            String practionerSpeciality = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getSpecialty();
            String practionerProfilePic = list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractionerPic();
            tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
            tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");
            new CommonControls().setProfilePic(mContext, practitionerPic, practionerProfilePic);
        }
        /*[**** Set Data ***]*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }


    @Override
    public void onSpecialityServicesClick(int adapterPosition) {
        Bundle bundle = new Bundle();
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            bundle.putInt("selectedService", adapterPosition);
            SpecialityLocationsFragment specialityLocationsFragment = new SpecialityLocationsFragment();
            specialityLocationsFragment.setArguments(bundle);
            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentParentView, specialityLocationsFragment, "specialityLocationsfragment")
                    .addToBackStack(null)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {

            List<ServiceGroup> tempList = new ArrayList<ServiceGroup>();
            tempList.clear();
            tempList.add(listSeviceGroup.get(adapterPosition));

            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) tempList);
            bundle.putString(TextConstants.TEST_TYPE, from);
            bundle.putString("selectedSpeciality", selectedSpeciality);
            if (listSeviceGroup.get(adapterPosition).getMoPractitionerInfoList().size() > 0) {
                SpecialityDoctorFragment specialityDoctorFragment = new SpecialityDoctorFragment();
                specialityDoctorFragment.setArguments(bundle);
                ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, specialityDoctorFragment, "specialitydoctorfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            } else {
                bundle.putBoolean("isDoctorsAvailable", false);
                SpecialityLocationsFragment specialityLocationsFragment = new SpecialityLocationsFragment();
                specialityLocationsFragment.setArguments(bundle);
                ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, specialityLocationsFragment, "specialityLocationsfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        }
    }
}