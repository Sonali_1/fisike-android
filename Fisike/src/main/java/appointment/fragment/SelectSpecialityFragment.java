package appointment.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.NotifyingScrollView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import appointment.SearchingActivity;
import appointment.adapter.SpecialityAdapter;
import appointment.interfaces.Animable;
import appointment.model.OrderSummary;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.request.PopularSpecialityRequest;
import appointment.request.PopularSpecialityResponse;
import appointment.utils.SupportBaseFragment;

/**
 * Created by Aastha on 14/03/2016.
 */
public class SelectSpecialityFragment extends SupportBaseFragment implements View.OnClickListener, Animable, NotifyingScrollView.OnScrollChangedListener {

    private Context mContext;
    private Animable animable;
    private CustomFontTextView mErrorInfo;
    private TextView toolbar_title;
    private Toolbar mToolbar;
    private EditText mEtSearch;
    private RecyclerView rv_speciality;
    private SpecialityAdapter adapterSpeciality;
    private ProgressBar progress_bar;
    private String toolbar_title_text;
    private LinearLayout lyn_lyt;

    private ColorDrawable mActionBarBackgroundDrawable;
    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener;
    private LinearLayout layoutToolbar;
    private IconTextView bt_toolbar_right;
    private ImageButton btnBack;
    private long mTransactionId;

    private List<SpecialityDataModel> specialitylist = new ArrayList<SpecialityDataModel>();
    private RelativeLayout rl_empty_view;

    private String from;


    public SelectSpecialityFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        from = TextConstants.SPECIALITY;
        toolbar_title_text =getActivity().getResources().getString(R.string.speciality);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(toolbar_title_text);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        mOnScrollChangedListener = this;
        BusProvider.getInstance().register(this);
        return R.layout.select_speciality_fragment;

    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void findView() {

        mActionBarBackgroundDrawable = new ColorDrawable(getResources().getColor(R.color.action_bar_bg));
        mActionBarBackgroundDrawable.setAlpha(0);
        animable = this;

        mToolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(toolbar_title_text);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setOnClickListener(this);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        ((NotifyingScrollView) getView().findViewById(R.id.scroll_view)).setOnScrollChangedListener(mOnScrollChangedListener);

        mEtSearch = (EditText) getView().findViewById(R.id.etSearch);

        lyn_lyt = (LinearLayout) getView().findViewById(R.id.lyn_lyt);
        progress_bar = (ProgressBar) getView().findViewById(R.id.progress_bar);
        rv_speciality = (RecyclerView) getView().findViewById(R.id.rv_speciality);
        rl_empty_view = (RelativeLayout) getView().findViewById(R.id.rl_empty_view);
        lyn_lyt = (LinearLayout) getView().findViewById(R.id.lyn_lyt);
        mErrorInfo = (CustomFontTextView) getView().findViewById(R.id.tv_error_info);
        layoutToolbar = (LinearLayout) getView().findViewById(R.id.toolbar_lyt);


        if (!Utils.showDialogForNoNetwork(getActivity(), false)) {
            showErrorView(true, mContext.getResources().getString(R.string.search_by_three_char));
            return;
        } else {
            lyn_lyt.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new PopularSpecialityRequest(transactionId, mContext));
        }
    }

    @Override
    public void initView() {
        Drawable.Callback mDrawableCallback = new Drawable.Callback() {
            @Override
            public void invalidateDrawable(Drawable who) {
                ((AppCompatActivity) getActivity()).getActionBar().setBackgroundDrawable(who);
            }

            @Override
            public void scheduleDrawable(Drawable who, Runnable what, long when) {
            }

            @Override
            public void unscheduleDrawable(Drawable who, Runnable what) {
            }
        };
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }


        rv_speciality.setLayoutManager(new LinearLayoutManager(mContext));
        rv_speciality.setNestedScrollingEnabled(false);
        adapterSpeciality = new SpecialityAdapter(getActivity(), specialitylist, animable, from, false);
        rv_speciality.setAdapter(adapterSpeciality);


        OrderSummary.getInstance().setTestType(TextConstants.SPECIALITY);

        progress_bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);
        mEtSearch.setHint(getResources().getString(R.string.search_for_specialities));
    }

    @Override
    public void bindView() {

        mEtSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchingActivity.class).putExtra(TextConstants.FROM, TextConstants.SPECIALITY));
            }

        });
    }


    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_toolbar_cross:
                OrderSummary.setInstance();
                getActivity().finish();
                break;


            case R.id.bt_toolbar_right:
                break;

            default:
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }


    @Override
    public void onResume() {
        super.onResume();
        OrderSummary.getInstance().setTestType(TextConstants.SPECIALITY);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mErrorInfo.setVisibility(View.VISIBLE);
                lyn_lyt.setVisibility(View.GONE);
            }
        }.execute();
    }


    @Override
    public void onScrollChanged(NestedScrollView who, int l, int t, int oldl, int oldt) {
        final int headerHeight = layoutToolbar.getHeight() - mToolbar.getHeight();
        final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
        final int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }

    @Override
    public void animate(String what) {
    }


    @Subscribe
    public void onPopularSpecialityResponse(final PopularSpecialityResponse response) {
        try {

            if (mTransactionId != response.getTransactionId())
                return;
            if (response.isSuccessful() && response.getSpecialityDataModelArrayList() != null && response.getSpecialityDataModelArrayList().size() > 0) {
                specialitylist.clear();
                specialitylist.addAll(response.getSpecialityDataModelArrayList());
                if (specialitylist.size() == 0) {
                    showErrorView(true, mContext.getResources().getString(R.string.search_by_three_char));
                } else {
                    showErrorView(false, null);
                }
                adapterSpeciality.notifyDataSetChanged();
            } else if (!response.isSuccessful() || response.getSpecialityDataModelArrayList() == null) {
                Toast.makeText(getActivity(), getResources().getString(R.string.api_error), Toast.LENGTH_SHORT).show();
                showErrorView(true, response.getMessage());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showErrorView(boolean isError, String errorMsg) {
        if (isError) {
            mErrorInfo.setText(errorMsg == null ? mContext.getResources().getString(R.string.Something_went_wrong) : errorMsg);
            lyn_lyt.setVisibility(View.VISIBLE);
            rv_speciality.setVisibility(View.GONE);
            mErrorInfo.setVisibility(View.VISIBLE);
            rl_empty_view.setVisibility(View.VISIBLE);
        } else {
            lyn_lyt.setVisibility(View.GONE);
            rv_speciality.setVisibility(View.VISIBLE);
            mErrorInfo.setVisibility(View.GONE);
            rl_empty_view.setVisibility(View.GONE);
        }
    }
}
