package appointment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.BaseActivity;

import java.util.ArrayList;

import appointment.MyOrdersActivity;
import appointment.adapter.RequestAdapter;
import appointment.model.OfferingModel;
import appointment.utils.SupportBaseFragment;

/**
 * Created by Aastha on 11/03/2016.
 */
public class SelectServiceFragment extends SupportBaseFragment implements View.OnClickListener {
    private RecyclerView lv_options;
    private RequestAdapter requestAdapter;
    private ArrayList<OfferingModel> requestTypes = new ArrayList<OfferingModel>();
    private Context mContext;

    public SelectServiceFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.services_title));
        return R.layout.select_service_fragment;
    }

    @Override
    public void findView() {
        lv_options = (RecyclerView) getView().findViewById(R.id.service_options);
    }

    @Override
    public void initView() {
        BaseActivity.bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        BaseActivity.bt_toolbar_right.setTextSize(44);
        BaseActivity.bt_toolbar_right.setVisibility(View.VISIBLE);
        BaseActivity.bt_toolbar_right.setOnClickListener(this);

        Bundle bundle = getArguments();
       // String from = bundle.getString(TextConstants.TEST_TYPE);

        if (bundle.containsKey(TextConstants.CHILD_OFFERING_MODEL)) {
            requestTypes = bundle.getParcelableArrayList(TextConstants.CHILD_OFFERING_MODEL);
        }

        requestAdapter = new RequestAdapter(getActivity(), requestTypes, TextConstants.FROM_SERVICES);
        lv_options.setLayoutManager(new LinearLayoutManager(getActivity()));
        lv_options.setAdapter(requestAdapter);
        requestAdapter.notifyDataSetChanged();
    }

    @Override
    public void bindView() {
    /*    lv_options.setOnItemClickListener
                (new AdapterView.OnItemClickListener() {

                     @Override
                     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                         switch (position){
                             case 0:
                                 bundle.putString(TextConstants.TEST_TYPE,TextConstants.HOME_SAMPLE);
                                 Intent homeSampleIntent= new Intent(getActivity(),HomeSampleCollectionActivity.class);
                                 homeSampleIntent.putExtras(bundle);
                                 startActivity(homeSampleIntent);
                                 break;
                             case 1:
                                 bundle.putString(TextConstants.TEST_TYPE,TextConstants.LAB_TEST);
                                 Intent labtestIntent=new Intent(getActivity(), HomeSampleCollectionActivity.class);
                                 labtestIntent.putExtras(bundle);
                                 startActivity(labtestIntent);
                                 break;

                             default:
                                 break;
                         }

                     }
                 }
                );*/

    }

/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
      //  menu.clear();
            super.onCreateOptionsMenu(menu, inflater);
            menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "My Orders").setIcon(R.drawable.my_orders).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 1) {
            MyOrdersActivity.newInstance(getActivity());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(getActivity());
                break;
        }
    }
}

