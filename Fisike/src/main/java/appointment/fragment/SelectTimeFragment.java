package appointment.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.DateExpandAdapter;
import appointment.adapter.GridAdapter;
import appointment.adapter.SpinAdapter;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.model.datetimeslotmodel.DateModel;
import appointment.model.slotmodel.SlotDataModel;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.request.GetReasonRequest;
import appointment.request.SearchReasonResponse;
import appointment.request.SelectBookingTimeRequest;
import appointment.utils.AnimatedExpandableListView;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 4/1/2016.
 */
public class SelectTimeFragment extends SupportBaseFragment implements AppointmentResponseCallback, View.OnClickListener, DatePickerDialog.OnDateSetListener, GridAdapter.OnGridChildClickedListener {


    private DatePickerDialog datePickerDialog;
    private String selected_date;
    private AppointmentResponseCallback responseCallback;
    private AnimatedExpandableListView expandablelistView;
    private Context mContext;
    private List<DateModel> list = new ArrayList<DateModel>();
    private List<String> testNames = new ArrayList<String>();
    private DateExpandAdapter dateExpandAdapter;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private int hcsID;
    private RelativeLayout rl_error_view;
    private TextView tv_error_view;
    private String rebook_or_reschedule = "";
    private String appointment_model = null;
    private String position = "";
    private boolean REBOOK;


    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality, mTv_service_names;
    private CircularImageView mPractitionerPic;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    private View mServices_dotted_line, mLocations_dotted_line, mDatetime_dotted_line;
    private CustomFontTextView mTv_location1, mTv_location2, mTv_location3;

    /*[......For Speciality......]*/
    private LinearLayout lyn_lyt_panel;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;
    private CustomFontTextView tv_service_names;
    private FrameLayout frame_container;
    private RelativeLayout doc_info_lyt;
/*[......For Speciality......]*/

    /*[ Header View Instances ]*/


    private IconTextView iv_previous_button, iv_calender_button, iv_next_button;

    private List<AppointmentModel> list_appointment = new ArrayList<AppointmentModel>();

    private String from, dateCached = "";
    private boolean isSkipped;
    private int selectedServicePosition;
    private int selectedLocation;
    private boolean isBookable;


    /*[Request Flow variables]*/
    ViewGroup mRequest_time_lyt, mHeaderView;
    NestedScrollView mRequest_flow_scrollView;
    CardView mDynamic_userInfoLyt;
    CustomFontButton mBt_next;


    private RelativeLayout mProgressView;
    private ProgressBar mProgressLoading;

    private Spinner spDate, spTime;
    /* Adapter variables*/
    private SpinAdapter day_adapter;
    private SpinAdapter time_adapter;
    private String starttime, endtime;
    private List<String> day_list = new ArrayList<String>();
    private List<String> time_list = new ArrayList<String>();
    private long transactionIdReason;
    private ProgressDialog progressdialog;
    /*Adapter variables*/

    /*[Request Flow variables]*/


    /*[**** Variables for Speciality ****]*/
    private boolean isDoctorsAvailable = true;
    private int selectedDoctorServices;
    private List<ServiceGroup> listServiceGroup;
    private String selectedSpeciality;
    private SpecialityDataModel selctedSpecialityDataModel;
    private int selectedService;
    /*[**** Variables for Speciality ****]*/


    public SelectTimeFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        FragmentBaseActivity.list_fragments.add(this);
        return R.layout.selecttimefragment;
    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);
        isBookable = bundle.getBoolean("isBookable");

        if (from.equals(TextConstants.DOCTORS)) {
            isSkipped = bundle.getBoolean("isskipped");
            selectedServicePosition = bundle.getInt("selectedservice");
            selectedLocation = bundle.getInt("selectedlocation");
        }

        if (bundle.containsKey("appointmentmodel")) {
            list_appointment.clear();
            list_appointment.add((AppointmentModel) bundle.getParcelable("appointmentmodel"));
        }

        if (from.equals(TextConstants.DOCTORS)) {
            hcsID = Integer.parseInt(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getHcsId().toString().trim());
        } else if (from.equals(TextConstants.SPECIALITY)) {
            selectedSpeciality = bundle.getString("selectedSpeciality");
            selectedLocation = bundle.getInt("selectedlocation");
            if (bundle.containsKey("selectedService")) {
                selectedService = bundle.getInt("selectedService");
            }
            if (bundle.containsKey("specialitymodel")) {
                selctedSpecialityDataModel = bundle.getParcelable("specialitymodel");
            }


            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                list_appointment = bundle.getParcelableArrayList("alldata");
                hcsID = Integer.parseInt(list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getHcsId());
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                listServiceGroup = bundle.getParcelableArrayList("alldata");
                selectedDoctorServices = bundle.getInt("selectedDoctorServices");
                if (bundle.containsKey("isDoctorsAvailable")) {
                    isDoctorsAvailable = bundle.getBoolean("isDoctorsAvailable");
                }

                if (isDoctorsAvailable) {
                    hcsID = Integer.parseInt(listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getHcsId());
                } else {
                    hcsID = Integer.parseInt(listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getHcsId());
                }
            }

        } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
            hcsID = Integer.parseInt(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getHcsId().toString().trim());
        }


        rebook_or_reschedule = bundle.containsKey("rebook_or_reschedule") ? bundle.getString("rebook_or_reschedule") : "";
        position = bundle.containsKey("position") ? bundle.getString("position") : "";

        if (bundle.containsKey("appointment_model")) {
            appointment_model = bundle.getString("appointment_model");
        } else {
            appointment_model = null;
        }


        REBOOK = bundle.getBoolean(TextConstants.REBOOK, false);
        responseCallback = this;

        mRequest_flow_scrollView = (NestedScrollView) getView().findViewById(R.id.request_flow_scrollView);
        expandablelistView = (AnimatedExpandableListView) getView().findViewById(R.id.expandablelistView);
        mProgressView = (RelativeLayout) getView().findViewById(R.id.progressView);

        if (isBookable) {
            mProgressView.setVisibility(View.GONE);
            mRequest_flow_scrollView.setVisibility(View.GONE);
            expandablelistView.setVisibility(View.VISIBLE);
            initializeBookableFlow();
        } else {
            mProgressView.setVisibility(View.GONE);
            mRequest_flow_scrollView.setVisibility(View.VISIBLE);
            expandablelistView.setVisibility(View.GONE);
            initializeRequestableFlow();
        }
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);

        if (from.equals(TextConstants.DOCTORS)) {

            if (rebook_or_reschedule.equals("")) {
            } else {
            }
        } else if (from.equals(TextConstants.SPECIALITY)) {
            if (rebook_or_reschedule.equals("")) {
            } else {
            }
        } else if (from.equals(TextConstants.LAB_TEST)) {

        }
    }

    @Override
    public void bindView() {

    }


    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (recent_or_upcoming.equals(TextConstants.SCHEDULE)) {

                        if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                            if (pdialog.isShowing()) {
                                pdialog.dismiss();
                                pdialog = null;
                            }
                            rl_error_view.setVisibility(View.VISIBLE);
                            tv_error_view.setText(response.toString());
                            disableAllButtons();
                        } else {
                            String schedule_id = null;
                            JSONObject jsonObject = null;
                            int totalcount = 0;

                            try {
                                jsonObject = new JSONObject(response.toString());
                                totalcount = jsonObject.getInt("totalCount");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (totalcount == 0) {
                                if (pdialog.isShowing()) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                rl_error_view.setVisibility(View.VISIBLE);
                                tv_error_view.setText(mContext.getResources().getString(R.string.No_Slots_Available));
                                disableAllButtons();
                            } else {
                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                                    schedule_id = "" + jsonArray.getJSONObject(0).getInt("id");
                                    ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_BOOK));
                                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                        if (pdialog.isShowing()) {
                                            pdialog.dismiss();
                                            pdialog = null;
                                        }
                                        Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                                        return;
                                    } else {
                                        long transactionId = getTransactionId();
                                        ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, schedule_id, null, true, null));
                                    }
                                } catch (Exception ex) {
                                }
                            }
                        }
                    } else if (recent_or_upcoming.equals(TextConstants.SLOT)) {
                        if (pdialog.isShowing()) {
                            pdialog.dismiss();
                            pdialog = null;
                        }
                        if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                            rl_error_view.setVisibility(View.VISIBLE);
                            tv_error_view.setText(response.toString());
                            disableAllButtons();
                        } else {

                            JSONObject mainJasonObject = null;
                            try {
                                mainJasonObject = new JSONObject(response.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1001")) {
                                disableAllButtons();
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.time_slot_unavailable), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1002")) {
                                disableAllButtons();
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.patient_not_register), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else {

                                list.removeAll(list);
                                dateExpandAdapter.notifyDataSetChanged();
                                Gson gson = new Gson();
                                SlotDataModel slotDataModel = new SlotDataModel();

                                try {
                                    slotDataModel = gson.fromJson(response.toString(), SlotDataModel.class);
                                    if (slotDataModel.getList().size() == 0) {
                                        rl_error_view.setVisibility(View.VISIBLE);
                                        tv_error_view.setText(mContext.getResources().getString(R.string.No_Slots_Available));
                                        disableAllButtons();
                                    } else {
                                        rl_error_view.setVisibility(View.GONE);
                                        expandablelistView.setVisibility(View.VISIBLE);
                                        list.clear();
                                        /*[....Parsing the DateTime Model...]*/
                                        try {
                                            new CommonTasks().parseDateTimeModel(list, slotDataModel);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                        /*[....End of Parsing the DateTime Model...]*/
                                        dateExpandAdapter.notifyDataSetChanged();


                                       /*[ Previous Button Enabled or Disabled ]*/
                                        SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.getDefault());
                                        Date date1 = null;
                                        try {
                                            date1 = ft.parse(list.get(0).getDatre().toString().trim());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        String todayDate = "";
                                        Calendar c = Calendar.getInstance();
                                        if (!dateCached.equalsIgnoreCase("")) {
                                            c.setTime(ft.parse(dateCached));
                                            todayDate = ft.format(c.getTime());
                                        }

                                        c.setTime(date1);
                                        String laterDate = ft.format(c.getTime());
                                        if (!dateCached.equalsIgnoreCase("") && !todayDate.equals(laterDate)) {
                                            iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.blue_grey));
                                            iv_previous_button.setEnabled(true);
                                        } else {
                                            iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
                                            iv_previous_button.setEnabled(false);
                                            dateCached = ft.format(date1);
                                        }

                                        iv_calender_button.setTextColor(mContext.getResources().getColor(R.color.overflow_color));
                                        iv_calender_button.setEnabled(true);

                                        iv_next_button.setTextColor(mContext.getResources().getColor(R.color.blue_grey));
                                        iv_next_button.setEnabled(true);
                                        /*[ End of Previous Button Enabled or Disabled ]*/
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    dateExpandAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }


    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {
    }

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_next:
                if (!isBookable) {
                    Bundle bundle = new Bundle();
                    if (from.equals(TextConstants.SPECIALITY)) {
                        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_appointment);
                            bundle.putString(TextConstants.TEST_TYPE, from);
                            bundle.putString("selectedSpeciality", selectedSpeciality);
                            bundle.putInt("selectedService", selectedService);
                            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                            bundle.putBoolean("isskipped", false);
                        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                            bundle.putBoolean("isDoctorsAvailable", isDoctorsAvailable);
                            bundle.putInt("selectedDoctorServices", selectedDoctorServices);
                            bundle.putInt("selectedLocation", selectedLocation);
                            bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) listServiceGroup);
                            bundle.putString(TextConstants.TEST_TYPE, from);
                            bundle.putString("selectedSpeciality", selectedSpeciality);
                            bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                            bundle.putBoolean("isskipped", false);
                        }

                    } else {
                        bundle.putParcelable("appointmentmodel", list_appointment.get(0));
                        bundle.putInt("selectedservice", selectedServicePosition);
                        bundle.putInt("selectedlocation", selectedLocation);
                    }

                    bundle.putString("start_date_time", starttime);
                    bundle.putString("end_date_time", endtime);
                    bundle.putInt("hcsid", hcsID);
                    bundle.putString(TextConstants.TEST_TYPE, from);
                    bundle.putBoolean("isBookable", isBookable);
                    if (rebook_or_reschedule.equals(TextConstants.RESCHEDULE)) {
                        bundle.putString("rebook_or_reschedule", rebook_or_reschedule);
                        bundle.putString("position", position + "");
                        bundle.putString("appointment_model", appointment_model);
                    }
                    if (rebook_or_reschedule.equals(TextConstants.REBOOK)) {
                        bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
                    }
                    PatientInformationFragment patientInformationFragment = new PatientInformationFragment();
                    patientInformationFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentParentView, patientInformationFragment, "patientinformationfragment")
                            .addToBackStack(null)
                            .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit();
                }
                break;


            case R.id.iv_previous_button:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pdialog = new ProgressDialog(getActivity());
                            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                            pdialog.setCanceledOnTouchOutside(false);
                            pdialog.show();
                        }
                    });
                    long transactionId = getTransactionId();
                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.getDefault());
                    Date date1 = null;
                    try {
                        date1 = ft.parse(list.get(0).getDatre().toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar c = Calendar.getInstance();
                    String todayDate = ft.format(c.getTime());
                    c.setTime(date1);
                    c.add(Calendar.DATE, -3);
                    String startDate, laterDate = ft.format(c.getTime());
                    if (todayDate.equals(laterDate)) {
                        startDate = null;
                    } else {
                        c.add(Calendar.DATE, -1);
                        startDate = ft.format(c.getTime());
                    }
                    ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", null, false, startDate));
                }
                break;


            case R.id.iv_next_button:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pdialog = new ProgressDialog(getActivity());
                            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                            pdialog.setCanceledOnTouchOutside(false);
                            pdialog.show();
                        }
                    });
                    long transactionId = getTransactionId();
                    ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", null, true, list.get(list.size() - 1).getDatre()));
                }
                break;


            case R.id.iv_calender_button:
                setDOBDataListener();
                break;

            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.img_labtest_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.img_speciality_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;
        }
    }


    @Subscribe
    public void onGetReasonResponse(SearchReasonResponse searchReasonResponse) {
        if (transactionIdReason != searchReasonResponse.getTransactionId())
            return;

        if (searchReasonResponse.isSuccessful()) {
          /*  if (progressdialog!=null && progressdialog.isShowing()) {
                progressdialog.dismiss();
                progressdialog = null;
            }*/
            OrderSummary.getInstance().setBookingReason(searchReasonResponse.getReasonList());
        }
    }

    private void initializeAndSetHeaderView(ViewGroup expandableListViewHeader) {

        lyn_lyt_panel = (LinearLayout) expandableListViewHeader.findViewById(R.id.lyn_lyt_panel);


        if (isBookable) {
            frame_container = (FrameLayout) expandableListViewHeader.findViewById(R.id.frame_container);
        } else {
            doc_info_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_info_lyt);
        }

        if (from.equals(TextConstants.SPECIALITY)) {
            lyn_lyt_panel.setVisibility(View.VISIBLE);
            if (isBookable) {
                frame_container.setVisibility(View.GONE);
            } else {
                doc_info_lyt.setVisibility(View.GONE);
            }


            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
            tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

            ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
            mPractitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
            tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
            tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

            ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
            tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

            ViewGroup locations_info = (ViewGroup) inflater.inflate(R.layout.locations_info_layout, lyn_lyt_panel, false);
            mTv_location1 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location3);

        /*[**** Add View***]*/
            lyn_lyt_panel.addView(speciality_info);
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(doc_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(services_info);
                if (isDoctorsAvailable) {
                    lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                    lyn_lyt_panel.addView(doc_info);
                }
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(locations_info);
            }
        /*[**** Add View***]*/



        /*[**** Set Data ***]*/
            tv_speciality_name.setText(selctedSpecialityDataModel.getServiceTypeName().toString().trim());

            String practionerName = null, practionerSpeciality = null, practionerProfilePic = null;
            String selectedservice = null, location1 = null, location3 = null;
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                practionerName = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractitionerName();
                practionerSpeciality = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getSpecialty();
                practionerProfilePic = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getPractionerPic();
                selectedservice = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getServiceName();
                location1 = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getLocationName();
                location3 = list_appointment.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedService).getMoHcsList().get(selectedLocation).getLocationAddress();
                new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                if (isDoctorsAvailable) {
                    practionerName = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerName();
                    practionerProfilePic = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractionerPic();
                    practionerSpeciality = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getSpecialty();
                    location1 = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getLocationName();
                    location3 = listServiceGroup.get(0).getMoPractitionerInfoList().get(selectedDoctorServices).getPractitionerWithLocationList().get(selectedLocation).getLocationAddres();
                    new CommonControls().setProfilePic(mContext, mPractitionerPic, practionerProfilePic);
                } else {
                    location1 = listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getLocationName();
                    location3 = listServiceGroup.get(0).getHcsWithLocationList().get(selectedLocation).getLocationAddress();
                }
                selectedservice = listServiceGroup.get(0).getServiceName();
            }

            mTv_location1.setText(location1);
            mTv_location2.setVisibility(View.GONE);
            if (Utils.isValueAvailable(location3))
                mTv_location3.setText(location3.toString().trim());
            else {
                mTv_location3.setText("");
                mTv_location3.setVisibility(View.GONE);
            }

            tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
            tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");

            tv_service_names.setText((selectedservice != null && !selectedservice.equals(mContext.getResources().getString(R.string.txt_null))) ? selectedservice : "");
        /*[**** Set Data ***]*/


        } else {

            lyn_lyt_panel.setVisibility(View.GONE);
            if (isBookable) {
                frame_container.setVisibility(View.VISIBLE);
            } else {
                doc_info_lyt.setVisibility(View.VISIBLE);
            }

            mTvdoctor_name = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tvdoctor_name);
            mTvdoctor_speciality = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tvdoctor_speciality);
            mTv_service_names = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_service_names);

            mPractitionerPic = (CircularImageView) expandableListViewHeader.findViewById(R.id.practitionerPic);

            mInner_doc_info_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.inner_doc_info_lyt);
            mDoc_services_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_services_lyt);
            mDoc_locations_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_locations_lyt);
            mDoc_datetime_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_datetime_lyt);

            mServices_dotted_line = (View) expandableListViewHeader.findViewById(R.id.services_dotted_line);
            mLocations_dotted_line = (View) expandableListViewHeader.findViewById(R.id.locations_dotted_line);
            mDatetime_dotted_line = (View) expandableListViewHeader.findViewById(R.id.datetime_dotted_line);

            mTv_location1 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location3);

            mDoc_services_lyt.setVisibility(View.VISIBLE);
            mDoc_locations_lyt.setVisibility(View.VISIBLE);
            mDoc_datetime_lyt.setVisibility(View.GONE);

            if (from.equals(TextConstants.DOCTORS)) {
                mInner_doc_info_lyt.setVisibility(View.VISIBLE);
                mTvdoctor_name.setText(list_appointment.get(0).getPractitionerForAppointment().getPractitionerName().toString().trim());
                mTvdoctor_speciality.setText(list_appointment.get(0).getPractitionerForAppointment().getSpecialty().toString().trim());
                if (!isSkipped) {
                    mDoc_services_lyt.setVisibility(View.VISIBLE);
                    mTv_service_names.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getServiceName().toString().trim());
                    mTv_location1.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationName().toString().trim());
                    mTv_location2.setVisibility(View.GONE);

                    if (Utils.isValueAvailable(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationAddress().toString().trim()))
                        mTv_location3.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(selectedLocation).getLocationAddress().toString().trim());
                    else {
                        mTv_location3.setText("");
                        mTv_location3.setVisibility(View.GONE);
                    }
                } else {
                    mDoc_services_lyt.setVisibility(View.GONE);
                    mTv_location1.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getServiceName().toString().trim());
                    mTv_location2.setVisibility(View.VISIBLE);
                    mTv_location2.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationName().toString().trim());
                    mTv_location3.setText(list_appointment.get(0).getPractitionerForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim());
                }

                PractitionerForAppointment practitioner = list_appointment.get(0).getPractitionerForAppointment();
                new CommonControls().setProfilePic(mContext, mPractitionerPic, practitioner.getPractionerPic());
            } else if (from.equals(TextConstants.SPECIALITY)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
            } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
                mServices_dotted_line.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mDoc_locations_lyt.setVisibility(View.VISIBLE);
                mTv_service_names.setText(list_appointment.get(0).getTestForAppointment().getTestName().toString().trim());
                mTv_location1.setText(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationName().toString().trim());
                mTv_location2.setVisibility(View.GONE);

                if (Utils.isValueAvailable(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim()))
                    mTv_location3.setText(list_appointment.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0).getLocationAddress().toString().trim());
                else {
                    mTv_location3.setText("");
                    mTv_location3.setVisibility(View.GONE);
                }

            }
        }
    }


    public void initializeBookableFlow() {

        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.title_select_time));
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        //this code for adjusting the group indicator into right side of the view

        if (Utils.isRTL(mContext)) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                expandablelistView.setIndicatorBounds(width - GetDipsFromPixel(10), width - GetDipsFromPixel(40));
            } else {
                expandablelistView.setIndicatorBoundsRelative(width - GetDipsFromPixel(10), width - GetDipsFromPixel(40));
            }
        } else {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                expandablelistView.setIndicatorBounds(width - GetDipsFromPixel(40), width - GetDipsFromPixel(10));
            } else {
                expandablelistView.setIndicatorBoundsRelative(width - GetDipsFromPixel(40), width - GetDipsFromPixel(10));
            }
        }
        // End of this code for adjusting the group indicator into right side of the view

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_bookingtime, expandablelistView, false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.footer_selecttime, expandablelistView, false);

        dateExpandAdapter = new DateExpandAdapter(getActivity(), list, expandablelistView, this);
        expandablelistView.setAdapter(dateExpandAdapter);
        expandablelistView.addHeaderView(header, null, false);
        expandablelistView.addFooterView(footer, null, false);

        iv_previous_button = (IconTextView) getView().findViewById(R.id.iv_previous_button);
        iv_calender_button = (IconTextView) getView().findViewById(R.id.iv_calender_button);
        iv_next_button = (IconTextView) getView().findViewById(R.id.iv_next_button);


        iv_previous_button.setOnClickListener(this);
        iv_calender_button.setOnClickListener(this);
        iv_next_button.setOnClickListener(this);


        initializeAndSetHeaderView(header);

        rl_error_view = (RelativeLayout) footer.findViewById(R.id.rl_error_view);
        tv_error_view = (TextView) footer.findViewById(R.id.tv_error_view);

        // In order to show animations, we need to use a custom click handler
        // for our ExpandableListView.

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
            return;
        } else {
            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_BOOK));
            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    long transactionId = getTransactionId();
                    ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", null, true, null));
                }
            }, 100);

        }

        (new Handler()).post(new Runnable() {
            @Override
            public void run() {
                expandablelistView.setIndicatorBounds(expandablelistView.getRight() - 40, expandablelistView.getWidth());
            }
        });


        expandablelistView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (expandablelistView.isGroupExpanded(groupPosition)) {
                    expandablelistView.collapseGroupWithAnimation(groupPosition);
                } else {
                    dateExpandAdapter.notifyDataSetChanged();
                    expandablelistView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }

        });
        expandablelistView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //Nothing here ever fires
                Bundle bundle = new Bundle();
                if (from.equals(TextConstants.SPECIALITY)) {
                    if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                        bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_appointment);
                        bundle.putString(TextConstants.TEST_TYPE, from);
                        bundle.putString("selectedSpeciality", selectedSpeciality);
                        bundle.putInt("selectedService", selectedService);
                        bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                        bundle.putBoolean("isskipped", false);
                    } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                        bundle.putBoolean("isDoctorsAvailable", isDoctorsAvailable);
                        bundle.putInt("selectedDoctorServices", selectedDoctorServices);
                        bundle.putInt("selectedLocation", selectedLocation);
                        bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) listServiceGroup);
                        bundle.putString(TextConstants.TEST_TYPE, from);
                        bundle.putString("selectedSpeciality", selectedSpeciality);
                        bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                        bundle.putBoolean("isskipped", false);
                    }

                } else {
                    bundle.putParcelable("appointmentmodel", list_appointment.get(0));
                    bundle.putInt("selectedservice", selectedServicePosition);
                    bundle.putInt("selectedlocation", selectedLocation);
                }

                bundle.putString("start_date_time", list.get(groupPosition).getTimeModelList().get(childPosition).getStartdate().toString().trim());
                bundle.putString("end_date_time", list.get(groupPosition).getTimeModelList().get(childPosition).getEnddate().toString().trim());
                bundle.putInt("hcsid", hcsID);
                bundle.putString(TextConstants.TEST_TYPE, from);

                bundle.putBoolean("isBookable", isBookable);
                if (rebook_or_reschedule.equals(TextConstants.RESCHEDULE)) {
                    bundle.putString("rebook_or_reschedule", rebook_or_reschedule);
                    bundle.putString("position", position + "");
                    bundle.putString("appointment_model", appointment_model);
                }
                if (rebook_or_reschedule.equals(TextConstants.REBOOK)) {
                    bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
                }
                PatientInformationFragment patientInformationFragment = new PatientInformationFragment();
                patientInformationFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, patientInformationFragment, "patientinformationfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();

                return true;
            }
        });

    }


    public void initializeRequestableFlow() {

        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.requestable_select_time));
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDynamic_userInfoLyt = (CardView) getView().findViewById(R.id.dynamic_userInfoLyt);
        mHeaderView = (ViewGroup) mInflater.inflate(R.layout.doctor_info, mDynamic_userInfoLyt, false);
        mDynamic_userInfoLyt.addView(mHeaderView);

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
            return;
        } else {
           /* progressdialog = new ProgressDialog(mContext);
            progressdialog.setMessage("Loading...");
            progressdialog.setCanceledOnTouchOutside(false);
            progressdialog.show();*/
            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_BOOK));
        }
        initializeAndSetHeaderView(mHeaderView);

        mBt_next = (CustomFontButton) getView().findViewById(R.id.bt_next);
        mBt_next.setOnClickListener(this);
        spDate = (Spinner) getView().findViewById(R.id.spinner_date);
        spTime = (Spinner) getView().findViewById(R.id.spinner_time);

        day_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, day_list, TextConstants.DATE_TEXT);
        spDate.setAdapter(day_adapter);

        time_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, time_list, TextConstants.TIME_TEXT);
        spTime.setAdapter(time_adapter);

        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Calendar calendar = Calendar.getInstance();
                // get a date to represent "today"
                Date today = calendar.getTime();
                // add one day to the date/calendar
                calendar.add(Calendar.DAY_OF_YEAR, position);
                // now get "tomorrow"
                Date formatted = calendar.getTime();

                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_today));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_tomorrow));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                //............... Changeing time on changeing date.....
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, dateInString);


                    /*[   ....set new time after setting date and time adapter...     ]*/
                OrderSummary.getInstance().setDate(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.MMM_dd_comma_yyyy, dateInString));
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(spTime.getSelectedItem().toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
               /*[   ....End of set new time after setting date and time adapter...     ]*/

                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, enddateInString);
                //..........End of finding end date time..............

                // ............... End of Changeing time on changeing date.....

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(parent.getItemAtPosition(position).toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);

                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, dateInString);
                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, enddateInString);
                //..........End of finding end date time..............
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    public void setTimeAdapter(String whichday) throws ParseException {
        if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
        /*[   Checking time must shown on spinner greater then current time  ]*/
            String lasttime = null;
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);

            String strdate = ft.format(date);
            Date newdate = null;
            try {
                newdate = ft.parse(strdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            time_list.clear();
            int length = getResources().getStringArray(R.array.time_units).length;
            int i = 0;
            for (String time : getResources().getStringArray(R.array.time_units)) {
                Date tempdate = null;
                try {
                    tempdate = ft.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (newdate.before(tempdate)) {
                    String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    time_list.add(new_time);
                } else {
                }
                if (i == length - 1) {
                    lasttime = time;
                    if (newdate.after(ft.parse(lasttime))) {
                        time_list.clear();
                        for (int j = 0; j < getResources().getStringArray(R.array.time_units).length; j++) {
                            String new_time = DateTimeUtil.convertSourceDestinationDateLocale(getResources().getStringArray(R.array.time_units)[j], DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                            time_list.add(new_time);
                        }
                    }
                }
                i++;
            }

            //.......... checking for days last time and set it to next day......
            if (newdate.after(ft.parse(lasttime))) {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            } else {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_today));
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            }
            //.......... End of checking for days last time and set it to next day......

            day_adapter.notifyDataSetChanged();
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        } else if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
            time_list.clear();
            for (String time : getResources().getStringArray(R.array.time_units)) {
                String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                time_list.add(new_time);
            }
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        }
/*[   End of Checking time must shown on spinner greater then current time  ]*/
    }

    public long getTransactionIdReason() {
        transactionIdReason = System.currentTimeMillis();
        return transactionIdReason;
    }


    private void disableAllButtons() {
        iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
        iv_previous_button.setEnabled(false);

        iv_next_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
        iv_next_button.setEnabled(false);

        list.removeAll(list);
        dateExpandAdapter.notifyDataSetChanged();

        iv_calender_button.setTextColor(mContext.getResources().getColor(R.color.overflow_color));
        iv_calender_button.setEnabled(true);
    }

    private void setDOBDataListener() {
        Calendar c = Calendar.getInstance();

        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(mContext, this, cyear, cmonth, cday);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);

        /*c.add(Calendar.DAY_OF_YEAR, 60);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());*/
        datePickerDialog.show();

    }


    @Override
    public void onDateSet(DatePicker view, int year, final int monthOfYear, int dayOfMonth) {
        if (view.isShown()) {
            selected_date = DateTimeUtil.getFormattedDateWithoutUTC(((monthOfYear + 1) + "-" + (dayOfMonth) + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);

            int diffInDays = (int) new CommonTasks().findDateDifferenceinDays(selected_date, DateTimeUtil.dd_MMM_yyyy);
            int reminderOFDiffInDays = diffInDays % 3;

            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
            Date date1 = null;
            try {
                date1 = ft.parse(selected_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar c = Calendar.getInstance();


            String todayDate = ft.format(c.getTime());


            c.setTime(date1);

            if (reminderOFDiffInDays == 0)
                c.add(Calendar.DATE, 0);
            if (reminderOFDiffInDays == 1)
                c.add(Calendar.DATE, -1);
            else if (reminderOFDiffInDays == 2)
                c.add(Calendar.DATE, -2);

            String startDate, laterDate = ft.format(c.getTime());
            if (todayDate.equals(laterDate)) {
                selected_date = null;
            } else {
                c.add(Calendar.DATE, -1);
                selected_date = ft.format(c.getTime());
            }

//        selected_date = ft.format(c.getTime());

            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                return;
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pdialog = new ProgressDialog(getActivity());
                        pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                    }
                });
                long transactionId = getTransactionId();
                ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", null, true, selected_date));
            }
        }
    }

    @Override
    public void onChildClicked(View view, int groupPosition, int childPosition) {

        Bundle bundle = new Bundle();
        if (from.equals(TextConstants.SPECIALITY)) {
            if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
                bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) list_appointment);
                bundle.putString(TextConstants.TEST_TYPE, from);
                bundle.putString("selectedSpeciality", selectedSpeciality);
                bundle.putInt("selectedService", selectedService);
                bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                bundle.putBoolean("isskipped", false);
            } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
                bundle.putBoolean("isDoctorsAvailable", isDoctorsAvailable);
                bundle.putInt("selectedDoctorServices", selectedDoctorServices);
                bundle.putInt("selectedLocation", selectedLocation);
                bundle.putParcelableArrayList("alldata", (ArrayList<? extends Parcelable>) listServiceGroup);
                bundle.putString(TextConstants.TEST_TYPE, from);
                bundle.putString("selectedSpeciality", selectedSpeciality);
                bundle.putParcelable("specialitymodel", selctedSpecialityDataModel);
                bundle.putBoolean("isskipped", false);
            }

        } else {
            bundle.putParcelable("appointmentmodel", list_appointment.get(0));
            bundle.putInt("selectedservice", selectedServicePosition);
            bundle.putInt("selectedlocation", selectedLocation);
        }

        bundle.putString("start_date_time", list.get(groupPosition).getTimeModelList().get(childPosition).getStartdate().toString().trim());
        bundle.putString("end_date_time", list.get(groupPosition).getTimeModelList().get(childPosition).getEnddate().toString().trim());
        bundle.putInt("hcsid", hcsID);
        bundle.putString(TextConstants.TEST_TYPE, from);
        bundle.putBoolean("isBookable", isBookable);
        if (rebook_or_reschedule.equals(TextConstants.RESCHEDULE)) {
            bundle.putString("rebook_or_reschedule", rebook_or_reschedule);
            bundle.putString("position", position + "");
            bundle.putString("appointment_model", appointment_model);
        }
        if (rebook_or_reschedule.equals(TextConstants.REBOOK)) {
            bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
        }
        PatientInformationFragment patientInformationFragment = new PatientInformationFragment();
        patientInformationFragment.setArguments(bundle);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, patientInformationFragment, "patientinformationfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}