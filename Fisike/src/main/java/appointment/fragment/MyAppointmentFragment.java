package appointment.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.CustomViewPager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.SlidingTabLayout;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import appointment.adapter.MyAppointmentAdapter;
import appointment.phlooba.fragment.HomeDrawListingFragment;
import appointment.request.AppointmentSearchRequestCommon;

/**
 * Created by Raj on 12/2/2016.
 */

public class MyAppointmentFragment extends Fragment implements ViewPager.OnPageChangeListener {
    private View myFragmentView;
    public static CustomViewPager mViewPager;
    private AppCompatActivity activity;
    private MyAppointmentAdapter myAppointmentAdapter;
    private SlidingTabLayout tabs;
    public static boolean isAppointmentSuccessful = false;

    public MyAppointmentFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = (AppCompatActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (isAppointmentSuccessful) {
            myAppointmentAdapter.getItem(1);
            setPage(1);
        }


        super.onResume();
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.fragment_my_health, container, false);

        if (!Utils.showDialogForNoNetwork(getActivity(), false)) {
        } else {
            new FetchRenderConfig().execute();
        }
        if (BuildConfig.isPatientApp) {
            ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.appointments));
        }
        addFabs();

        initView();

        setAdapter();

        setHasOptionsMenu(true);

        return myFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    private void initView() {


        tabs = (SlidingTabLayout) myFragmentView.findViewById(R.id.sliding_tabs);
        mViewPager = (CustomViewPager) myFragmentView.findViewById(R.id.pager);

        setPage(0);
        mViewPager.setOffscreenPageLimit(1);


        // setting indicator and divider color
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white); // define any color in xml resources and set it here, I have used white
            }

            @Override
            public int getDividerColor(int position) {
                // return getResources().getColor(R.color.white);
                return 0;
            }
        });

        mViewPager.addOnPageChangeListener(this);
    }

    private void setAdapter() {
        myAppointmentAdapter = new MyAppointmentAdapter(getActivity(), getChildFragmentManager(), tabs, this);
        mViewPager.setAdapter(myAppointmentAdapter);


        /*if (getArguments() != null && getArguments().getBoolean(VariableConstants.DEEP_LINK_FOR_RECORDS_CLICKED, false)) {
            setPage(1);
        } else if (getArguments() != null && getArguments().getBoolean(VariableConstants.DEEP_LINK_FOR_MEDICATION_CLICKED, false)) {
            mViewPager.setCurrentItem(2);
            setPage(2);
        }*/

        tabs.setViewPager(mViewPager);

       /* if (getArguments() != null && getArguments().containsKey(VariableConstants.LAUNCH_UPLOAD_SCREEN)) {
            if (getArguments().getBoolean(VariableConstants.LAUNCH_UPLOAD_SCREEN)) {
                mViewPager.setCurrentItem(3);

            }
        }*/
    }

    public CustomViewPager getmViewPager() {
        return mViewPager;
    }

    public void launchUploadScreen() {
        mViewPager.setCurrentItem(1);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        int currentItem = mViewPager.getCurrentItem();
        Fragment fragment = myAppointmentAdapter.getRegisteredFragment(currentItem);
        if (fragment instanceof MyOrderFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // @Override
    // public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    // if(mViewPager.getCurrentItem()==0){
    // myHelthAdapter.getItem(0).onCreateOptionsMenu(menu, inflater);
    // }
    // super.onCreateOptionsMenu(menu, inflater);
    // }


    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int page) {
        setPage(page);
    }

    public static void setPage(int page) {

        mViewPager.setCurrentItem(page);

    }

    private void addFabs() {

       /* if (BuildConfig.isPatientApp) {
            fabMenu = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);
            fabMenu.setVisibility(View.GONE);
        }*/
    }


    public MyAppointmentAdapter getMyAppointmentAdapter() {
        return myAppointmentAdapter;
    }

    public int getCurrentFragmentVisible() {
        return mViewPager.getCurrentItem();
    }


    class FetchRenderConfig extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            APIManager apiManager = APIManager.getInstance();
            AppointmentSearchRequestCommon appointmentSearchRequestJson = new AppointmentSearchRequestCommon();

            String json = new Gson().toJson(appointmentSearchRequestJson);
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();
                                /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
            String authToken = SharedPref.getAccessToken();
            String executeHttpPost = apiManager.executeHttpPost(MphRxUrl.getFetchRenderConfigUrl(), jsonObj, authToken, getActivity());
            return executeHttpPost;
        }

        protected void onPostExecute(String response) {
            if (response != null) {
                SharedPref.setRenderConfig(response);
            }
            AppLog.showInfo("response", SharedPref.getRenderConfig());
        }
    }


}