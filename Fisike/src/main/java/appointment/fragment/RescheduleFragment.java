package appointment.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import appointment.FragmentBaseActivity;
import appointment.SelectLocationActivity;
import appointment.adapter.DateExpandAdapter;
import appointment.adapter.GridAdapter;
import appointment.adapter.PatientNameAdapter;
import appointment.adapter.ReasonsAdapter;
import appointment.adapter.SpinAdapter;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.Roles;
import appointment.model.appointmodel.MinutesDuration;
import appointment.model.datetimeslotmodel.DateModel;
import appointment.model.slotmodel.SlotDataModel;
import appointment.request.AppointUpdateRequest;
import appointment.request.GetReasonRequest;
import appointment.request.SearchReasonResponse;
import appointment.request.SelectBookingTimeRequest;
import appointment.utils.AnimatedExpandableListView;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 11/17/2016.
 */

public class RescheduleFragment extends SupportBaseFragment implements AppointmentResponseCallback, View.OnClickListener, DatePickerDialog.OnDateSetListener, GridAdapter.OnGridChildClickedListener {

    private DatePickerDialog datePickerDialog;
    private String selected_date;
    private AppointmentResponseCallback responseCallback;
    private AnimatedExpandableListView expandablelistView;
    private Context mContext;
    private List<DateModel> list = new ArrayList<DateModel>();
    private DateExpandAdapter dateExpandAdapter;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private int hcsID;
    private RelativeLayout rl_error_view;
    private TextView tv_error_view;
    private String rebook_or_reschedule = "";
    private String position = "";
    private Roles rolesModel;


    /*[ Header View Instances ]*/
    private CustomFontTextView mTvdoctor_name, mTvdoctor_speciality, mTv_service_names;
    private CircularImageView mPractitionerPic;
    private RelativeLayout mDoc_services_lyt, mInner_doc_info_lyt, mDoc_locations_lyt, mDoc_datetime_lyt;
    private View mServices_dotted_line, mLocations_dotted_line, mDatetime_dotted_line;
    private CustomFontTextView mTv_location1, mTv_location2, mTv_location3;


    /*[......For Speciality......]*/
    private LinearLayout lyn_lyt_panel;
    private CustomFontTextView tv_speciality_name;
    private CustomFontTextView tvdoctor_name, tvdoctor_speciality;
    private CustomFontTextView tv_service_names;
    private FrameLayout frame_container;
    private RelativeLayout doc_info_lyt;
/*[......For Speciality......]*/

    /*[ Header View Instances ]*/

    private IconTextView iv_previous_button, iv_calender_button, iv_next_button;


    private PopupWindow mPopup;
    private CustomFontTextView reasonLabel;
    private Spinner tvReason;
    private CustomFontEditTextView ed_comments;

    private RelativeLayout mProgressView;
    private ProgressBar mProgressLoading;

    private appointment.model.appointmodel.List appointmentmodel;
    private String from, dateCached = "";
    private boolean isBookable = false;

    List<String> reasonList = new ArrayList<String>();
    private ReasonsAdapter reasonAdapter;

    /*[Request Flow variables]*/
    ViewGroup mRequest_time_lyt, mHeaderView;
    NestedScrollView mRequest_flow_scrollView;
    CardView mDynamic_userInfoLyt;
    CustomFontButton mBt_next;


    private Spinner spDate, spTime;
    /* Adapter variables*/
    private SpinAdapter day_adapter;
    private SpinAdapter time_adapter;
    private String starttime, endtime;
    private List<String> day_list = new ArrayList<String>();
    private List<String> time_list = new ArrayList<String>();
    private long transactionIdReason;
    /*Adapter variables*/

    // 9711496077asa

    /*[Request Flow variables]*/


    public RescheduleFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        ((FragmentBaseActivity) getActivity()).setToolbarTitleText(getResources().getString(R.string.txt_menu_reschedule));
        return R.layout.selecttimefragment;
    }


    @Override
    public void findView() {
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);

        if (bundle.containsKey("reschedule_appointmentmodel")) {
            appointmentmodel = bundle.getParcelable("reschedule_appointmentmodel");
        }

        if (from.equals(TextConstants.DOCTORS)) {
            hcsID = appointmentmodel.getParticipant().get(1).getActorHealthcareService().getId();
        } else if (from.equals(TextConstants.SPECIALITY)) {
            hcsID = appointmentmodel.getParticipant().get(1).getActorHealthcareService().getId();
        } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
            hcsID = appointmentmodel.getParticipant().get(1).getActorHealthcareService().getId();
        }

        new AsyncTaskForHCSSHOW(String.valueOf(hcsID)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        rebook_or_reschedule = bundle.containsKey("rebook_or_reschedule") ? bundle.getString("rebook_or_reschedule") : "";
        position = bundle.containsKey("position") ? bundle.getString("position") : "";

        responseCallback = this;

        mRequest_flow_scrollView = (NestedScrollView) getView().findViewById(R.id.request_flow_scrollView);
        expandablelistView = (AnimatedExpandableListView) getView().findViewById(R.id.expandablelistView);
        mProgressView = (RelativeLayout) getView().findViewById(R.id.progressView);
        mProgressLoading = (ProgressBar) getView().findViewById(R.id.progressLoading);
        mProgressLoading.getIndeterminateDrawable()
                .setColorFilter(mContext.getResources().getColor(R.color.action_bar_bg), PorterDuff.Mode.SRC_IN);

        mProgressView.setVisibility(View.VISIBLE);
        mRequest_flow_scrollView.setVisibility(View.GONE);
        expandablelistView.setVisibility(View.GONE);

    }


    private void initializeWhichFlow() {
        if (isBookable) {
            mProgressView.setVisibility(View.GONE);
            mRequest_flow_scrollView.setVisibility(View.GONE);
            expandablelistView.setVisibility(View.VISIBLE);
            initializeBookableFlow();
        } else {
            mProgressView.setVisibility(View.GONE);
            mRequest_flow_scrollView.setVisibility(View.VISIBLE);
            expandablelistView.setVisibility(View.GONE);
            initializeRequestableFlow();
        }
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void initView() {

        BusProvider.getInstance().register(this);

        if (from.equals(TextConstants.DOCTORS)) {

            if (rebook_or_reschedule.equals("")) {
            } else {
            }
        } else if (from.equals(TextConstants.SPECIALITY)) {
            if (rebook_or_reschedule.equals("")) {
            } else {
            }
        } else if (from.equals(TextConstants.LAB_TEST)) {

        }
    }

    @Override
    public void bindView() {

    }


    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (recent_or_upcoming.equals(TextConstants.SCHEDULE)) {

                        if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                            if (pdialog != null && pdialog.isShowing()) {
                                pdialog.dismiss();
                                pdialog = null;
                            }
                            rl_error_view.setVisibility(View.VISIBLE);
                            tv_error_view.setText(response.toString());
                            disableAllButtons();
                        } else {
                            String schedule_id = null;
                            JSONObject jsonObject = null;
                            int totalcount = 0;

                            try {
                                jsonObject = new JSONObject(response.toString());
                                totalcount = jsonObject.getInt("totalCount");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (totalcount == 0) {
                                if (pdialog != null && pdialog.isShowing()) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                rl_error_view.setVisibility(View.VISIBLE);
                                tv_error_view.setText(mContext.getResources().getString(R.string.No_Slots_Available));
                                disableAllButtons();
                            } else {
                                try {
                                    ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_RESCHEDULE));

                                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                                    schedule_id = "" + jsonArray.getJSONObject(0).getInt("id");
                                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                        if (pdialog != null && pdialog.isShowing()) {
                                            pdialog.dismiss();
                                            pdialog = null;
                                        }
                                        Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                                        return;
                                    } else {
                                        long transactionId = getTransactionId();
                                        ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, String.valueOf(schedule_id), String.valueOf(appointmentmodel.getId()), true, null));
                                    }
                                } catch (Exception ex) {

                                }
                            }
                        }

                    } else if (recent_or_upcoming.equals(TextConstants.SLOT)) {
                        if (pdialog != null && pdialog.isShowing()) {
                            pdialog.dismiss();
                            pdialog = null;
                        }
                        if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                            rl_error_view.setVisibility(View.VISIBLE);
                            tv_error_view.setText(response.toString());
                            disableAllButtons();
                        } else {

                            JSONObject mainJasonObject = null;
                            try {
                                mainJasonObject = new JSONObject(response.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1001")) {
                                disableAllButtons();
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.time_slot_unavailable), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1002")) {
                                disableAllButtons();
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.patient_not_register), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else if (mainJasonObject.optString("httpStatusCode").equals("500") && mainJasonObject.optString("status").equals("1004")) {
                                DialogUtils.showErrorDialog(mContext, "Alert", mContext.getResources().getString(R.string.appointment_cant_rescheduled_contact_admin), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else {

                                list.removeAll(list);
                                dateExpandAdapter.notifyDataSetChanged();
                                Gson gson = new Gson();
                                SlotDataModel slotDataModel = new SlotDataModel();

                                try {
                                    slotDataModel = gson.fromJson(response.toString(), SlotDataModel.class);
                                    if (slotDataModel.getList().size() == 0) {
                                        rl_error_view.setVisibility(View.VISIBLE);
                                        tv_error_view.setText(mContext.getString(R.string.No_Slots_Available));
                                        disableAllButtons();
                                    } else {
                                        rl_error_view.setVisibility(View.GONE);
                                        expandablelistView.setVisibility(View.VISIBLE);
                                        list.clear();
                                        /*[....Parsing the DateTime Model...]*/
                                        try {
                                            new CommonTasks().parseDateTimeModel(list, slotDataModel);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                        /*[....End of Parsing the DateTime Model...]*/
                                        dateExpandAdapter.notifyDataSetChanged();

                                          /*[ Previous Button Enabled or Disabled ]*/
                                        SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.getDefault());
                                        Date date1 = null;
                                        try {
                                            date1 = ft.parse(list.get(0).getDatre().toString().trim());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        String todayDate = "";
                                        Calendar c = Calendar.getInstance();
                                        if (!dateCached.equalsIgnoreCase("")) {
                                            c.setTime(ft.parse(dateCached));
                                            todayDate = ft.format(c.getTime());
                                        }

                                        c.setTime(date1);
                                        String laterDate = ft.format(c.getTime());
                                        if (!dateCached.equalsIgnoreCase("") && !todayDate.equals(laterDate)) {
                                            iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.blue_grey));
                                            iv_previous_button.setEnabled(true);
                                        } else {
                                            iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
                                            iv_previous_button.setEnabled(false);
                                            dateCached = ft.format(date1);
                                        }

                                        iv_calender_button.setTextColor(mContext.getResources().getColor(R.color.overflow_color));
                                        iv_calender_button.setEnabled(true);

                                        iv_next_button.setTextColor(mContext.getResources().getColor(R.color.blue_grey));
                                        iv_next_button.setEnabled(true);
                                        /*[ End of Previous Button Enabled or Disabled ]*/
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    dateExpandAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    } else if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {

                        if (pdialog != null && pdialog.isShowing()) {
                            pdialog.dismiss();
                            pdialog = null;
                        }

                        if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                                || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                                || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                            Toast.makeText(mContext, response.toString(), Toast.LENGTH_SHORT).show();
                        } else {

                            JSONObject mainJasonObject = null;
                            try {
                                mainJasonObject = new JSONObject(response.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1001")) {
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.time_slot_unavailable), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else if (mainJasonObject.optString("httpStatusCode").equals("200") && mainJasonObject.optString("status").equals("1002")) {
                                DialogUtils.showErrorDialog(mContext, mContext.getResources().getString(R.string.Alert), mContext.getResources().getString(R.string.patient_not_register), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else if (mainJasonObject.optString("httpStatusCode").equals("500") && mainJasonObject.optString("status").equals("1004")) {
                                DialogUtils.showErrorDialog(mContext, "Alert", mContext.getResources().getString(R.string.appointment_cant_rescheduled_contact_admin), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                            } else {
                                if (position.length() > 0) {
                                    Gson gson = new Gson();
                                    appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
                                    Intent intent = new Intent();
                                    intent.putExtra("position", position + "");
                                    intent.putExtra("cancel_or_reschedule", TextConstants.RESCHEDULE);
                                    intent.putExtra("appointment_model", appointmentmodel);
                                    getActivity().setResult(Activity.RESULT_OK, intent);
                                    getActivity().finish();
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }


    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {
    }

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.bt_next:
                /*reschedule*/
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    return;
                } else {
                    showReasonCommentDialogCommon();
                }
                break;


            case R.id.iv_previous_button:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pdialog = new ProgressDialog(getActivity());
                            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                            pdialog.setCanceledOnTouchOutside(false);
                            pdialog.show();
                        }
                    });
                    long transactionId = getTransactionId();
                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.getDefault());
                    Date date1 = null;
                    try {
                        date1 = ft.parse(list.get(0).getDatre().toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar c = Calendar.getInstance();
                    String todayDate = ft.format(c.getTime());
                    c.setTime(date1);
                    c.add(Calendar.DATE, -3);
                    String startDate, laterDate = ft.format(c.getTime());
                    if (todayDate.equals(laterDate)) {
                        startDate = null;
                    } else {
                        c.add(Calendar.DATE, -1);
                        startDate = ft.format(c.getTime());
                    }
                    ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", String.valueOf(appointmentmodel.getId()), false, startDate));
                }
                break;


            case R.id.iv_next_button:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pdialog = new ProgressDialog(getActivity());
                            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                            pdialog.setCanceledOnTouchOutside(false);
                            pdialog.show();
                        }
                    });
                    long transactionId = getTransactionId();
                    ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", String.valueOf(appointmentmodel.getId()), true, list.get(list.size() - 1).getDatre()));
                }
                break;


            case R.id.iv_calender_button:
                setDOBDataListener();
//                Toast.makeText(mContext, "Calendar", Toast.LENGTH_SHORT).show();
                break;


            case R.id.img_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.img_labtest_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;

            case R.id.img_speciality_delete:
                if (SelectLocationActivity.act != null) {
                    SelectLocationActivity.act.finish();
                }
                getActivity().finish();
                break;
        }
    }


    private void initializeAndSetHeaderView(ViewGroup expandableListViewHeader) {

        int valuePostiton = 0;
        for (int i = 0; i < appointmentmodel.getExtension().size(); i++) {
            if (appointmentmodel.getExtension().get(i).getUrl().equals("appointmentDetails")) {
                valuePostiton = i;
                break;
            }
        }


        lyn_lyt_panel = (LinearLayout) expandableListViewHeader.findViewById(R.id.lyn_lyt_panel);
        if (isBookable) {
            frame_container = (FrameLayout) expandableListViewHeader.findViewById(R.id.frame_container);
        } else {
            doc_info_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_info_lyt);
        }

        if (from.equals(TextConstants.SPECIALITY)) {
            lyn_lyt_panel.setVisibility(View.VISIBLE);
            if (isBookable) {
                frame_container.setVisibility(View.GONE);
            } else {
                doc_info_lyt.setVisibility(View.GONE);
            }

            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup speciality_info = (ViewGroup) inflater.inflate(R.layout.speciality_info_layout, lyn_lyt_panel, false);
            tv_speciality_name = (CustomFontTextView) speciality_info.findViewById(R.id.tv_speciality_name);

            ViewGroup doc_info = (ViewGroup) inflater.inflate(R.layout.doctor_info_layout, lyn_lyt_panel, false);
            mPractitionerPic = (CircularImageView) doc_info.findViewById(R.id.practitionerPic);
            tvdoctor_name = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_name);
            tvdoctor_speciality = (CustomFontTextView) doc_info.findViewById(R.id.tvdoctor_speciality);

            ViewGroup services_info = (ViewGroup) inflater.inflate(R.layout.services_info_layout, lyn_lyt_panel, false);
            tv_service_names = (CustomFontTextView) services_info.findViewById(R.id.tv_service_names);

            ViewGroup locations_info = (ViewGroup) inflater.inflate(R.layout.locations_info_layout, lyn_lyt_panel, false);
            mTv_location1 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) locations_info.findViewById(R.id.tv_location3);



        /*[**** Set Data ***]*/
            String practionerName = null, practionerSpeciality = null, speciality = null;
            String selectedservice = null, location1 = null, location3 = null;

            practionerName = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getPractitionerName();
            practionerSpeciality = "";
            selectedservice = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getServiceName();
            speciality = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getSpecialityName();


                /*[**** Add View***]*/
            lyn_lyt_panel.addView(speciality_info);
            if (practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null)) && !practionerName.equals("")) {
                lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
                lyn_lyt_panel.addView(doc_info);
            }
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(services_info);
            lyn_lyt_panel.addView(CommonTasks.getLine(mContext));
            lyn_lyt_panel.addView(locations_info);

        /*[**** Add View***]*/

            String[] location_arr = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getLocationDetails().split("\n");
            try {
                mTv_location1.setText(location_arr[0]);
            } catch (Exception Ex) {
            }
            mTv_location2.setVisibility(View.GONE);
            try {
                mTv_location3.setText(location_arr[1]);
            } catch (Exception Ex) {
                mTv_location3.setVisibility(View.GONE);
            }


            tv_speciality_name.setText((speciality != null && !speciality.equals(mContext.getResources().getString(R.string.txt_null))) ? speciality : "");
            if (practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null)) && !practionerName.equals("")) {
                tvdoctor_name.setText((practionerName != null && !practionerName.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerName : "");
                new CommonControls().setProfilePic(mContext, mPractitionerPic, null);
                tvdoctor_speciality.setText((practionerSpeciality != null && !practionerSpeciality.equals(mContext.getResources().getString(R.string.txt_null))) ? practionerSpeciality : "");
                tvdoctor_speciality.setVisibility(View.GONE);
            }
            tv_service_names.setText((selectedservice != null && !selectedservice.equals(mContext.getResources().getString(R.string.txt_null))) ? selectedservice : "");
        /*[**** Set Data ***]*/


        } else {

            lyn_lyt_panel.setVisibility(View.GONE);
            if (isBookable) {
                frame_container.setVisibility(View.VISIBLE);
            } else {
                doc_info_lyt.setVisibility(View.VISIBLE);
            }

            mTvdoctor_name = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tvdoctor_name);
            mTvdoctor_speciality = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tvdoctor_speciality);
            mTv_service_names = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_service_names);

            mPractitionerPic = (CircularImageView) expandableListViewHeader.findViewById(R.id.practitionerPic);

            mInner_doc_info_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.inner_doc_info_lyt);
            mDoc_services_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_services_lyt);
            mDoc_locations_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_locations_lyt);
            mDoc_datetime_lyt = (RelativeLayout) expandableListViewHeader.findViewById(R.id.doc_datetime_lyt);

            mServices_dotted_line = (View) expandableListViewHeader.findViewById(R.id.services_dotted_line);
            mLocations_dotted_line = (View) expandableListViewHeader.findViewById(R.id.locations_dotted_line);
            mDatetime_dotted_line = (View) expandableListViewHeader.findViewById(R.id.datetime_dotted_line);

            mTv_location1 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location1);
            mTv_location2 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location2);
            mTv_location3 = (CustomFontTextView) expandableListViewHeader.findViewById(R.id.tv_location3);

            mDoc_services_lyt.setVisibility(View.VISIBLE);
            mDoc_locations_lyt.setVisibility(View.VISIBLE);
            mDoc_datetime_lyt.setVisibility(View.GONE);


            if (from.equals(TextConstants.DOCTORS)) {
                mInner_doc_info_lyt.setVisibility(View.VISIBLE);
                mTvdoctor_name.setText(appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getPractitionerName());
                new CommonControls().setProfilePic(mContext, mPractitionerPic, null);
                mTvdoctor_speciality.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);

                mTv_service_names.setText(appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getServiceName());
                String[] location_arr = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getLocationDetails().split("\n");
                try {
                    mTv_location1.setText(location_arr[0]);
                } catch (Exception Ex) {
                }
                mTv_location2.setVisibility(View.GONE);
                try {
                    mTv_location3.setText(location_arr[1]);
                } catch (Exception Ex) {
                    mTv_location3.setVisibility(View.GONE);
                }
            } else if (from.equals(TextConstants.SPECIALITY)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
            } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                mInner_doc_info_lyt.setVisibility(View.GONE);
                mServices_dotted_line.setVisibility(View.GONE);
                mDoc_services_lyt.setVisibility(View.VISIBLE);
                mDoc_locations_lyt.setVisibility(View.VISIBLE);
                mTv_service_names.setText(appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getServiceName());
                String[] location_arr = appointmentmodel.getExtension().get(valuePostiton).getValue().get(0).getLocationDetails().split("\n");
                try {
                    mTv_location1.setText(location_arr[0]);
                } catch (Exception Ex) {
                }
                mTv_location2.setVisibility(View.GONE);
                try {
                    mTv_location3.setText(location_arr[1]);
                } catch (Exception Ex) {
                    mTv_location3.setVisibility(View.GONE);
                }
            }

        }


    }


    public void initializeBookableFlow() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        //this code for adjusting the group indicator into right side of the view
        if (Utils.isRTL(mContext)) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                expandablelistView.setIndicatorBounds(width - GetDipsFromPixel(10), width - GetDipsFromPixel(40));
            } else {
                expandablelistView.setIndicatorBoundsRelative(width - GetDipsFromPixel(10), width - GetDipsFromPixel(40));
            }
        } else {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                expandablelistView.setIndicatorBounds(width - GetDipsFromPixel(40), width - GetDipsFromPixel(10));
            } else {
                expandablelistView.setIndicatorBoundsRelative(width - GetDipsFromPixel(40), width - GetDipsFromPixel(10));
            }
        }
        // End of this code for adjusting the group indicator into right side of the view

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_bookingtime, expandablelistView, false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.footer_selecttime, expandablelistView, false);

        dateExpandAdapter = new DateExpandAdapter(getActivity(), list, expandablelistView, this);
        expandablelistView.setAdapter(dateExpandAdapter);
        expandablelistView.addHeaderView(header, null, false);
        expandablelistView.addFooterView(footer, null, false);


        iv_previous_button = (IconTextView) getView().findViewById(R.id.iv_previous_button);
        iv_calender_button = (IconTextView) getView().findViewById(R.id.iv_calender_button);
        iv_next_button = (IconTextView) getView().findViewById(R.id.iv_next_button);
        iv_previous_button.setOnClickListener(this);
        iv_calender_button.setOnClickListener(this);
        iv_next_button.setOnClickListener(this);


        initializeAndSetHeaderView(header);

        rl_error_view = (RelativeLayout) footer.findViewById(R.id.rl_error_view);
        tv_error_view = (TextView) footer.findViewById(R.id.tv_error_view);

        // In order to show animations, we need to use a custom click handler
        // for our ExpandableListView.


        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
            return;
        } else {

            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_RESCHEDULE));

            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, String.valueOf(hcsID), String.valueOf(appointmentmodel.getId()), true, null));
        }

        (new Handler()).post(new Runnable() {
            @Override
            public void run() {
                expandablelistView.setIndicatorBounds(expandablelistView.getRight() - 40, expandablelistView.getWidth());
            }
        });


        expandablelistView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (expandablelistView.isGroupExpanded(groupPosition)) {
                    expandablelistView.collapseGroupWithAnimation(groupPosition);
                } else {
                    dateExpandAdapter.notifyDataSetChanged();
                    expandablelistView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }

        });
        expandablelistView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //Nothing here ever fires
                starttime = list.get(groupPosition).getTimeModelList().get(childPosition).getStartdate().toString().trim();
                endtime = list.get(groupPosition).getTimeModelList().get(childPosition).getEnddate().toString().trim();
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    return false;
                } else {
                    showReasonCommentDialogCommon();
                }

                return true;
            }
        });

    }


    public void initializeRequestableFlow() {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDynamic_userInfoLyt = (CardView) getView().findViewById(R.id.dynamic_userInfoLyt);
        mHeaderView = (ViewGroup) mInflater.inflate(R.layout.doctor_info, mDynamic_userInfoLyt, false);
        mDynamic_userInfoLyt.addView(mHeaderView);

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
            return;
        } else {
           /* progressdialog = new ProgressDialog(mContext);
            progressdialog.setMessage("Loading...");
            progressdialog.setCanceledOnTouchOutside(false);
            progressdialog.show();*/
            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_RESCHEDULE));
        }
        initializeAndSetHeaderView(mHeaderView);

        mBt_next = (CustomFontButton) getView().findViewById(R.id.bt_next);
        mBt_next.setText(getResources().getString(R.string.txt_save));
        mBt_next.setOnClickListener(this);
        spDate = (Spinner) getView().findViewById(R.id.spinner_date);
        spTime = (Spinner) getView().findViewById(R.id.spinner_time);

        day_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, day_list, TextConstants.DATE_TEXT);
        spDate.setAdapter(day_adapter);

        time_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, time_list, TextConstants.TIME_TEXT);
        spTime.setAdapter(time_adapter);

        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Calendar calendar = Calendar.getInstance();
                // get a date to represent "today"
                Date today = calendar.getTime();
                // add one day to the date/calendar
                calendar.add(Calendar.DAY_OF_YEAR, position);
                // now get "tomorrow"
                Date formatted = calendar.getTime();

                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_today));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_tomorrow));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                //............... Changeing time on changeing date.....
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, dateInString);


                    /*[   ....set new time after setting date and time adapter...     ]*/
                OrderSummary.getInstance().setDate(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.MMM_dd_comma_yyyy, dateInString));
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(spTime.getSelectedItem().toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
               /*[   ....End of set new time after setting date and time adapter...     ]*/

                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, enddateInString);
                //..........End of finding end date time..............

                // ............... End of Changeing time on changeing date.....

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(parent.getItemAtPosition(position).toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);

                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, dateInString);
                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, enddateInString);
                //..........End of finding end date time..............
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    @Subscribe
    public void onGetReasonResponse(SearchReasonResponse searchReasonResponse) {

        if (transactionIdReason != searchReasonResponse.getTransactionId())
            return;

        if (searchReasonResponse.isSuccessful()) {
            OrderSummary.getInstance().setRescheduleReason(searchReasonResponse.getReasonList());
        }
    }

    public void setTimeAdapter(String whichday) throws ParseException {
        if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
        /*[   Checking time must shown on spinner greater then current time  ]*/
            String lasttime = null;
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);

            String strdate = ft.format(date);
            Date newdate = null;
            try {
                newdate = ft.parse(strdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            time_list.clear();
            int length = getResources().getStringArray(R.array.time_units).length;
            int i = 0;
            for (String time : getResources().getStringArray(R.array.time_units)) {
                Date tempdate = null;
                try {
                    tempdate = ft.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (newdate.before(tempdate)) {
                    String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    time_list.add(new_time);
                } else {
                }
                if (i == length - 1) {
                    lasttime = time;
                    if (newdate.after(ft.parse(lasttime))) {
                        time_list.clear();
                        for (int j = 0; j < getResources().getStringArray(R.array.time_units).length; j++) {
                            String new_time = DateTimeUtil.convertSourceDestinationDateLocale(getResources().getStringArray(R.array.time_units)[j], DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                            time_list.add(new_time);
                        }
                    }
                }
                i++;
            }

            //.......... checking for days last time and set it to next day......
            if (newdate.after(ft.parse(lasttime))) {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            } else {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_today));
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            }
            //.......... End of checking for days last time and set it to next day......

            day_adapter.notifyDataSetChanged();
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        } else if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
            time_list.clear();
            for (String time : getResources().getStringArray(R.array.time_units)) {
                String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                time_list.add(new_time);
            }
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        }
/*[   End of Checking time must shown on spinner greater then current time  ]*/
    }


    public void showReasonCommentDialogCommon() {
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.reason_comment_dialog, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(mContext.getResources().getString(R.string.txt_reschedule_appointment));
        ed_comments = (CustomFontEditTextView) v.findViewById(R.id.ed_comments);
        reasonLabel = (CustomFontTextView) v.findViewById(R.id.tv_static_reason);
        tvReason = (Spinner) v.findViewById(R.id.spinnerReasons);

        reasonAdapter = new ReasonsAdapter(getActivity(), reasonList);
        tvReason.setAdapter(reasonAdapter);

       /* CharSequence[] arrayPopup = getResources().getTextArray(R.array.appointment_reasons);
        String[] arrayPopupString = new String[arrayPopup.length];
        for (int i = 0; i < arrayPopup.length; i++) {
            arrayPopupString[i] = arrayPopup[i].toString();
        }
        tvReason.setText(arrayPopupString[0].toString());*/

        showPopupReasons();

        ((CustomFontButton) v.findViewById(R.id.cancel_action)).setText(mContext.getResources().getString(R.string.cancel));
        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        ((CustomFontButton) v.findViewById(R.id.continue_action)).setText(mContext.getResources().getString(R.string.txt_ok));
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    alertDialog.dismiss();
                    return;
                } else {
                    alertDialog.dismiss();
                    pdialog = new ProgressDialog(mContext);
                    pdialog.setMessage(mContext.getResources().getString(R.string.Rescheduling));
                    pdialog.setCanceledOnTouchOutside(false);
                    pdialog.show();
                    long transactionId = getTransactionId();

                    String start, end;

                    start = isBookable ? starttime : CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, starttime);
                    end = isBookable ? endtime : CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, endtime);


                    int minsDuration;
                    if (isBookable) {
                        minsDuration = (int) new CommonTasks().findDateDifferenceinMins(start, end, DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated);
                    } else {
                        minsDuration = (int) new CommonTasks().findDateDifferenceinMins(start, end, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                    }

                    /*[   ......  important code to change the start time and end time to reschedule......  ]*/

                    if (appointmentmodel.getMinutesDuration() == null) {
                        List<Object> extension = new ArrayList<Object>();
                        MinutesDuration minutesDuration = new MinutesDuration();
                        minutesDuration.setExtension(extension);
                        minutesDuration.setValue(minsDuration);
                        appointmentmodel.setMinutesDuration(minutesDuration);
                    } else {
                        appointmentmodel.getMinutesDuration().setValue(minsDuration);
                    }
                    appointmentmodel.getStart().setValue(start);
                    appointmentmodel.getEnd().setValue(end);

                    /*[   ...... End of important code to change the start time and end time to reschedule......  ]*/

                    String reason;
                    /*Code to update reason and comment start-Aastha*/
                    if (tvReason.getSelectedItem().toString().equals(getResources().getString(R.string.select_one)))
                        reason = "";
                    else
                        reason = tvReason.getSelectedItem().toString();

                    /*Code to update reason and comment end- Aastha */
                    ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, responseCallback, appointmentmodel, TextConstants.RESCHEDULE, reason, ed_comments.getText().toString().trim()));
                }
            }
        });
        alertDialog.show();
    }


    private void showPopupReasons() {
        ArrayList<String> bookingReason = OrderSummary.getInstance().getRescheduleReason();
        reasonList.clear();

        if (bookingReason.size() > 0) {
            reasonList.addAll(bookingReason);
        } else {
            String[] arrayPopup = getResources().getStringArray(R.array.appointment_reasons);
            for (int i = 0; i < arrayPopup.length; i++) {
                reasonList.add(arrayPopup[i].toString());
            }
        }
        reasonList.add(0, mContext.getResources().getString(R.string.select_one));
        reasonAdapter.notifyDataSetChanged();
        tvReason.setSelection(0);
    }


    public long getTransactionIdReason() {
        transactionIdReason = System.currentTimeMillis();
        return transactionIdReason;
    }

    @Override
    public void onChildClicked(View view, int groupPosition, int childPosition) {
        starttime = list.get(groupPosition).getTimeModelList().get(childPosition).getStartdate().toString().trim();
        endtime = list.get(groupPosition).getTimeModelList().get(childPosition).getEnddate().toString().trim();
        if (!Utils.showDialogForNoNetwork(mContext, false)) {
        } else {
            showReasonCommentDialogCommon();
        }

    }


    public class AsyncTaskForHCSSHOW extends AsyncTask<Void, Void, String> {
        String hcsId;

        public AsyncTaskForHCSSHOW(String hcsId) {
            this.hcsId = hcsId;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                getActivity().finish();
                return;
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            String jsonString = "";
            String arr = "";
            try {
                String authToken = SharedPref.getAccessToken();
                jsonString = APIManager.getInstance().executeHttpGet(MphRxUrl.getHCSShow() + hcsID, authToken);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonString;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            try {

                if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                        || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                        || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                    Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else {
                    try {
                        JSONObject mainJason = new JSONObject(response);
                        if (mainJason.has("extension")) {
                            JSONArray jsonExtensionArray = mainJason.getJSONArray("extension");
                            for (int i = 0; i < jsonExtensionArray.length(); i++) {
                                if (jsonExtensionArray.getJSONObject(i).has("url")) {
                                    if (jsonExtensionArray.getJSONObject(i).getString("url").equals("accessControl")) {
                                        JSONObject jsonRolesObject = jsonExtensionArray.getJSONObject(i).getJSONArray("value").getJSONObject(0);
                                        rolesModel = new Roles();
                                        ArrayList<String> bookableRolesList = new ArrayList();
                                        ArrayList<String> requestableRolesList = new ArrayList();
                                        for (int m = 0; m < jsonRolesObject.getJSONArray("bookableRoles").length(); m++) {
                                            bookableRolesList.add(jsonRolesObject.getJSONArray("bookableRoles").getString(m));
                                        }
                                        for (int n = 0; n < jsonRolesObject.getJSONArray("requestableRoles").length(); n++) {
                                            requestableRolesList.add(jsonRolesObject.getJSONArray("requestableRoles").getString(n));
                                        }
                                        rolesModel.setBookableRoles(bookableRolesList);
                                        rolesModel.setRequestableRoles(requestableRolesList);
                                    }
                                }
                            }
                        }

                        try {
                            isBookable = new CommonTasks().checkBookableOrRequestable(rolesModel);
                        } catch (Exception ex) {
                            isBookable = false;
                        }
                        initializeWhichFlow();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.Something_went_wrong), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void disableAllButtons() {
        iv_previous_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
        iv_previous_button.setEnabled(false);

        iv_next_button.setTextColor(mContext.getResources().getColor(R.color.grey_opacity_50));
        iv_next_button.setEnabled(false);

        list.removeAll(list);
        dateExpandAdapter.notifyDataSetChanged();

        iv_calender_button.setTextColor(mContext.getResources().getColor(R.color.overflow_color));
        iv_calender_button.setEnabled(true);
    }


    private void setDOBDataListener() {
        Calendar c = Calendar.getInstance();

//        if (setnextdate) {
//            c.add(Calendar.DATE, 1);
//        }

        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(mContext, this, cyear, cmonth, cday);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);

        c.add(Calendar.DAY_OF_YEAR, 60);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();

    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (view.isShown()) {
            selected_date = DateTimeUtil.getFormattedDateWithoutUTC(((monthOfYear + 1) + "-" + (dayOfMonth) + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);

            int diffInDays = (int) new CommonTasks().findDateDifferenceinDays(selected_date, DateTimeUtil.dd_MMM_yyyy);
            int reminderOFDiffInDays = diffInDays % 3;

            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
            Date date1 = null;
            try {
                date1 = ft.parse(selected_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar c = Calendar.getInstance();
            String todayDate = ft.format(c.getTime());
            c.setTime(date1);

            if (reminderOFDiffInDays == 0)
                c.add(Calendar.DATE, 0);
            if (reminderOFDiffInDays == 1)
                c.add(Calendar.DATE, -1);
            else if (reminderOFDiffInDays == 2)
                c.add(Calendar.DATE, -2);

            String startDate, laterDate = ft.format(c.getTime());
            if (todayDate.equals(laterDate)) {
                selected_date = null;
            } else {
                c.add(Calendar.DATE, -1);
                selected_date = ft.format(c.getTime());
            }
            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                Toast.makeText(getActivity(), mContext.getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_SHORT).show();
                return;
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pdialog = new ProgressDialog(getActivity());
                        pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                    }
                });
                long transactionId = getTransactionId();
                ThreadManager.getDefaultExecutorService().submit(new SelectBookingTimeRequest(transactionId, responseCallback, TextConstants.SLOT, hcsID + "", String.valueOf(appointmentmodel.getId()), true, selected_date));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }
}