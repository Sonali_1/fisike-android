package appointment.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.NotifyingScrollView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.MyOrdersActivity;
import appointment.SearchingActivity;
import appointment.SelectTimeActivity;
import appointment.adapter.HomeSampleCollectionAdapter;
import appointment.enums.AppointmentTypeEnum;
import appointment.interfaces.Animable;
import appointment.interfaces.ResponseCallback;
import appointment.model.HomesampleModel.TestsDataModel;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.request.PopularTestsRequest;
import appointment.request.SearchTestResponse;
import appointment.utils.AnimUtils;
import appointment.utils.OnListItemSelected;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 2/24/2016.
 */
public class HomeSampleCollectionFragment extends SupportBaseFragment implements OnListItemSelected, ResponseCallback, View.OnClickListener, Animable, NotifyingScrollView.OnScrollChangedListener {


    private Toolbar mToolbar;
    private Context mContext;
    private Animable animable;
    private ResponseCallback responseCallback;
    private RecyclerView mRecyclerview_Recent;
    private RelativeLayout rl_empty_view;
    private CustomFontTextView mErrorInfo;
    private EditText mEtSearch;
    private HomeSampleCollectionAdapter adapterHomeSampleCollection_recent;
    private String from;

    private View footerView;
    private TextView tv_numberOfTests, tvTotalCost, toolbar_title;
    private int number, totalCost;
    private Button btProceed;
    private RelativeLayout bottomLayout;
    private LinearLayout lyn_lyt;
    private ProgressBar progress_bar;
    private TextView text_loading;
    private long mTransactionId;
    private String toolbar_title_text;


    private ColorDrawable mActionBarBackgroundDrawable;
    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener;
    private LinearLayout layoutToolbar;
    private IconTextView bt_toolbar_right;
    private ImageButton btnBack;

    private List<AppointmentModel> homesamplecollection_list = new ArrayList<AppointmentModel>();
    private List<AppointmentModel> appointmentModel;
    private String isShowingSnackbar = "hide";

    public HomeSampleCollectionFragment() {
        mContext = getActivity();
    }


    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        toolbar_title_text = getArguments().getString(TextConstants.TEST_TYPE);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(toolbar_title_text);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        mOnScrollChangedListener = this;
        BusProvider.getInstance().register(this);
        return R.layout.homesamplecollectionfragment;
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }


    @Override
    public void findView() {
        mActionBarBackgroundDrawable = new ColorDrawable(getResources().getColor(R.color.action_bar_bg));
        mActionBarBackgroundDrawable.setAlpha(0);

        animable = this;
        responseCallback = this;
        footerView = getView().findViewById(R.id.snackbar);
        Bundle bundle = getArguments();
        from = bundle.getString(TextConstants.TEST_TYPE);

        mToolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(AppointmentTypeEnum.getDisplayedValuefromCode(toolbar_title_text));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setOnClickListener(this);

        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        ((NotifyingScrollView) getView().findViewById(R.id.scroll_view)).setOnScrollChangedListener(mOnScrollChangedListener);

        mEtSearch = (EditText) getView().findViewById(R.id.etSearch);

        mEtSearch.setText(mContext.getResources().getString(R.string.txt_search_for_test));


        tv_numberOfTests = (TextView) footerView.findViewById(R.id.tv_total_number);
        tvTotalCost = (TextView) footerView.findViewById(R.id.tv_total_bill);
        lyn_lyt = (LinearLayout) getView().findViewById(R.id.lyn_lyt);
        progress_bar = (ProgressBar) getView().findViewById(R.id.progress_bar);

        text_loading = (TextView) getView().findViewById(R.id.text_loading);
//        text_loading.setText("Fetching popular " + from.toLowerCase()+"...");
        mRecyclerview_Recent = (RecyclerView) getView().findViewById(R.id.rv_homesamplecollection);
        rl_empty_view = (RelativeLayout) getView().findViewById(R.id.rl_empty_view);
        bottomLayout = (RelativeLayout) getView().findViewById(R.id.snackbar);
        btProceed = (Button) bottomLayout.findViewById(R.id.btn_proceed);
        mErrorInfo = (CustomFontTextView) getView().findViewById(R.id.tv_error_info);
        layoutToolbar = (LinearLayout) getView().findViewById(R.id.toolbar_lyt);

        if (!Utils.showDialogForNoNetwork(getActivity(), false)) {
            return;
        } else {
            lyn_lyt.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            text_loading.setVisibility(View.VISIBLE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new PopularTestsRequest(transactionId, responseCallback));
        }
    }

    @Override
    public void initView() {


        Drawable.Callback mDrawableCallback = new Drawable.Callback() {
            @Override
            public void invalidateDrawable(Drawable who) {
                ((AppCompatActivity) getActivity()).getActionBar().setBackgroundDrawable(who);
            }

            @Override
            public void scheduleDrawable(Drawable who, Runnable what, long when) {
            }

            @Override
            public void unscheduleDrawable(Drawable who, Runnable what) {
            }
        };
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }

        //  btnBack = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_cross);
        //  btnSubmit = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);

        mRecyclerview_Recent.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerview_Recent.setNestedScrollingEnabled(false);
        if (from.equalsIgnoreCase(TextConstants.SERVICES))
            from = TextConstants.LAB_TEST;
        adapterHomeSampleCollection_recent = new HomeSampleCollectionAdapter(getActivity(), homesamplecollection_list, animable, from, false);
        mRecyclerview_Recent.setAdapter(adapterHomeSampleCollection_recent);

        OrderSummary.getInstance().setTestType(from);
        progress_bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);

//        mEtSearch.setText(getResources().getString(R.string.search_by_name));
    }

    @Override
    public void bindView() {
        btProceed.setOnClickListener(this);
        if (BuildConfig.isMultipleSelectionEnabled)
            MyApplication.getInstance().registerForListSelection(this);

        mEtSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchingActivity.class).putExtra("from", from));
            }

        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.isMultipleSelectionEnabled) {
            if (OrderSummary.getInstance().getOrderist() == null) {
                footerView.setVisibility(View.GONE);
                adapterHomeSampleCollection_recent.notifyDataSetChanged();
            } else if (OrderSummary.getInstance().getOrderist().size() >= 1) {
                adapterHomeSampleCollection_recent.notifyDataSetChanged();
                animable.animate("show");
            } else if (OrderSummary.getInstance().getOrderist().size() == 0) {
                if (from.equals(TextConstants.LAB_TEST) && appointmentModel != null) {
                    homesamplecollection_list.clear();
                    homesamplecollection_list.addAll(appointmentModel);
                    adapterHomeSampleCollection_recent = new HomeSampleCollectionAdapter(getActivity(), homesamplecollection_list, animable, from, false);
                    mRecyclerview_Recent.setAdapter(adapterHomeSampleCollection_recent);
                } else {
                    adapterHomeSampleCollection_recent.notifyDataSetChanged();
                    animable.animate("hide");
                }
            }
            OrderSummary.getInstance().setTestType(toolbar_title_text);
            if (from.equals(TextConstants.HOME_SAMPLE))
                handleSelection(from, false);   //first time , after idle, back from search/select time ,adapter set
        } else {
            if (appointmentModel != null) {
                homesamplecollection_list.clear();
                homesamplecollection_list.addAll(appointmentModel);
                adapterHomeSampleCollection_recent = new HomeSampleCollectionAdapter(getActivity(), homesamplecollection_list, animable, from, false);
                mRecyclerview_Recent.setAdapter(adapterHomeSampleCollection_recent);
                //  adapterHomeSampleCollection_recent.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void handleSelection(String from, boolean selected) {

        if (selected && OrderSummary.getInstance().getOrderist().size() == 1) {
            animable.animate("show");
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (!selected && OrderSummary.getInstance().getOrderist().size() == 1) {
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (OrderSummary.getInstance().getOrderist().size() > 1) {
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (OrderSummary.getInstance().getOrderist().size() == 0) {
            animable.animate("hide");
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_proceed:
                if (from.equals(TextConstants.HOME_SAMPLE)) {
                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    } else {
                        Intent intent1 = new Intent(getActivity(), SelectTimeActivity.class);
                        mContext.startActivity(intent1);
                    }
                } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.SERVICES)) {
                    toolbar_title_text = from;
                    Bundle bundle = new Bundle();
                    bundle.putString(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                    Intent intent2 = new Intent(mContext, FragmentBaseActivity.class);
                    intent2.putExtras(bundle);
                    mContext.startActivity(intent2);
                }

                break;

            case R.id.bt_toolbar_cross:
                OrderSummary.setInstance();
                getActivity().finish();
                break;

            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(getActivity());
                break;

            default:
                break;
        }
    }

    @Override
    public void animate(String what) {
        if (!isShowingSnackbar.equals("show") && what.equals("show")) {
            isShowingSnackbar = "show";
            AnimUtils.setLayoutAnim_slideupfrombottom((ViewGroup) footerView, getActivity());
        } else if (!isShowingSnackbar.equals("hide") && what.equals("hide")) {
            isShowingSnackbar = "hide";
            AnimUtils.setLayoutAnim_slidedownfromtop((ViewGroup) footerView, getActivity());
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        //    super.onCreateOptionsMenu(menu, inflater);
        //    menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "My Orders").setIcon(R.drawable.my_orders).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 1) {
            MyOrdersActivity.newInstance(getActivity());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public String loadJSONFromAsset() {
        String json = null;
        InputStream is;
        try {
            is = mContext.getAssets().open("appointment.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void passResponse(final String response, boolean mIsSuccessful) {

        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                        mErrorInfo.setVisibility(View.VISIBLE);
                        mErrorInfo.setText(response);
                        progress_bar.setVisibility(View.GONE);
                        text_loading.setVisibility(View.GONE);
                        mRecyclerview_Recent.setVisibility(View.GONE);
                        mErrorInfo.setVisibility(View.VISIBLE);
                        rl_empty_view.setVisibility(View.VISIBLE);

                    } else {
                        Gson gson = new Gson();

                        lyn_lyt.setVisibility(View.GONE);
                        progress_bar.setVisibility(View.GONE);
                        text_loading.setVisibility(View.GONE);

                        Gson gson1 = new Gson();
                        Type listType = null;
                        listType = new TypeToken<List<AppointmentModel>>() {
                        }.getType();

                        try {
                            appointmentModel = gson1.fromJson(response, listType);
                            homesamplecollection_list.clear();
                            homesamplecollection_list.addAll(appointmentModel);
                        } catch (Exception ex) {
                            homesamplecollection_list.clear();
                        }
                        adapterHomeSampleCollection_recent.notifyDataSetChanged();

                        if (homesamplecollection_list.size() == 0) {
                            mErrorInfo.setVisibility(View.VISIBLE);
                            mRecyclerview_Recent.setVisibility(View.GONE);
                            mRecyclerview_Recent.setVisibility(View.GONE);
                            rl_empty_view.setVisibility(View.VISIBLE);
                        } else {
                            mRecyclerview_Recent.setVisibility(View.VISIBLE);
                            rl_empty_view.setVisibility(View.GONE);
                            mErrorInfo.setVisibility(View.GONE);
                        }

                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (BuildConfig.isMultipleSelectionEnabled)
            MyApplication.getInstance().unRegisterListSelection(this);
    }

    @Override
    public void onScrollChanged(NestedScrollView who, int l, int t, int oldl, int oldt) {
        final int headerHeight = layoutToolbar.getHeight() - mToolbar.getHeight();
        final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
        final int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }


    @Subscribe
    public void onSearchTestResponse(final SearchTestResponse searchresponse) {
        if (mTransactionId != searchresponse.getTransactionId())
            return;

        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String response = searchresponse.getSearchresponse().toString();
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                        mErrorInfo.setVisibility(View.VISIBLE);
                        mErrorInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.no_test_found, 0, 0);
                        mErrorInfo.setText(response);
                    } else {
                        Gson gson = new Gson();


                        Type listType = new TypeToken<List<TestsDataModel>>() {
                        }.getType();
                        List<TestsDataModel> temp_list = gson.fromJson(response.toString(), listType);
                        if (temp_list.size() == 0) {
                            mRecyclerview_Recent.setVisibility(View.VISIBLE);
                            rl_empty_view.setVisibility(View.GONE);

                        } else {
                            mRecyclerview_Recent.setVisibility(View.VISIBLE);
                            rl_empty_view.setVisibility(View.GONE);

                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }
}