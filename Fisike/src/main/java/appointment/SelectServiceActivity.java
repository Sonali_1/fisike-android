package appointment;

import android.os.Build;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.fragment.SelectServiceFragment;
import appointment.model.OrderSummary;
import careplan.activity.CareTaskAppointment;

/**
 * Created by Aastha on 14/03/2016.
 */

public class SelectServiceActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        //setContentView(R.layout.activity_select_service);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_select_service, frameLayout);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setOnClickListener(this);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.title_services));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setOnClickListener(this);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setVisibility(View.GONE);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        Bundle bundle = getIntent().getExtras();
        SelectServiceFragment selectServiceFragment = new SelectServiceFragment();
        selectServiceFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentSelectService, selectServiceFragment, "selectservicefragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_cross:
                OrderSummary.setInstance();
                finish();
                break;


            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(this);
                break;

        }
    }
}

