package appointment.model;

import java.util.List;

import appointment.model.HomesampleModel.TestsDataModel;

/**
 * Created by Aastha on 24/02/2016.
 */
public class TestMO {

    private String id;
    private String testName;
    private String testDescription;
    private String testCost;
    private String testInstruction;
    private String testDiscountedCost;
    private boolean isSelected;
    private boolean isExpanded;
    private TestsDataModel listTestsDataModel;


    public TestMO(String id, String s, String s1, String s2, String s3, String s4, boolean b, boolean isExpanded) {
        this.id = id;
        testName = s;
        testDescription = s1;
        testCost = s2;
        testInstruction = s3;
        testDiscountedCost = s4;
        isSelected = b;
        this.isExpanded = isExpanded;
    }

    public TestMO(TestsDataModel testsDataModel) {
        listTestsDataModel=testsDataModel;
    }


    public TestsDataModel getListTestsDataModel() {
        return listTestsDataModel;
    }

    public void setListTestsDataModel(List<TestsDataModel> listTestsDataModel) {
        this.listTestsDataModel = (TestsDataModel) listTestsDataModel;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestDescription() {
        return testDescription;
    }

    public void setTestDescription(String testDescription) {
        this.testDescription = testDescription;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public String getTestInstruction() {
        return testInstruction;
    }

    public void setTestInstruction(String testInstruction) {
        this.testInstruction = testInstruction;
    }

    public String getTestCost() {
        return testCost;
    }

    public void setTestCost(String testCost) {
        this.testCost = testCost;
    }

    public String getTestDiscountedCost() {
        return testDiscountedCost;
    }

    public void setTestDiscountedCost(String testDiscountedCost) {
        this.testDiscountedCost = testDiscountedCost;
    }


}
