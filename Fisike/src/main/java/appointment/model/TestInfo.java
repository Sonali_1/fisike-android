package appointment.model;

import java.util.ArrayList;
import java.util.List;

import appointment.model.HomesampleModel.TestsDataModel;

/**
 * Created by Aastha on 04/03/2016.
 */
public class TestInfo {
    private List<TestsDataModel> testParameterList=new ArrayList<TestsDataModel>();

    public List<TestsDataModel> getTestParameterList() {
        return testParameterList;
    }

    public void setTestParameterList(List<TestsDataModel> testParameterList) {
        this.testParameterList = testParameterList;
    }
}
