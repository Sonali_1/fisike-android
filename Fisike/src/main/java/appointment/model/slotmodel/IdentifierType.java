package appointment.model.slotmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;

/**
 * Created by laxmansingh on 4/4/2016.
 */
public class IdentifierType {

    @SerializedName("extension")
    @Expose
    private java.util.List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private java.util.List<Object> coding = new ArrayList<Object>();

    /**
     * @return The extension
     */
    public java.util.List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The coding
     */
    public java.util.List<Object> getCoding() {
        return coding;
    }

    /**
     * @param coding The coding
     */
    public void setCoding(java.util.List<Object> coding) {
        this.coding = coding;
    }

}
