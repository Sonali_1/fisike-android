package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiagnosticOrder {

    @SerializedName("Other")
    @Expose
    private String Other;
    @SerializedName("Lab")
    @Expose
    private String Lab;
    @SerializedName("Radiology")
    @Expose
    private String Radiology;

    /**
     * @return The Other
     */
    public String getOther() {
        return Other;
    }

    /**
     * @param Other The Other
     */
    public void setOther(String Other) {
        this.Other = Other;
    }

    /**
     * @return The Lab
     */
    public String getLab() {
        return Lab;
    }

    /**
     * @param Lab The Lab
     */
    public void setLab(String Lab) {
        this.Lab = Lab;
    }

    /**
     * @return The Radiology
     */
    public String getRadiology() {
        return Radiology;
    }

    /**
     * @param Radiology The Radiology
     */
    public void setRadiology(String Radiology) {
        this.Radiology = Radiology;
    }

}
