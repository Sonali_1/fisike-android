package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("documentReference")
    @Expose
    private DocumentReference documentReference;
    @SerializedName("diagnosticOrder")
    @Expose
    private DiagnosticOrder diagnosticOrder;
    @SerializedName("medicationPrescription")
    @Expose
    private MedicationPrescription medicationPrescription;

    /**
     * @return The documentReference
     */
    public DocumentReference getDocumentReference() {
        return documentReference;
    }

    /**
     * @param documentReference The documentReference
     */
    public void setDocumentReference(DocumentReference documentReference) {
        this.documentReference = documentReference;
    }

    /**
     * @return The diagnosticOrder
     */
    public DiagnosticOrder getDiagnosticOrder() {
        return diagnosticOrder;
    }

    /**
     * @param diagnosticOrder The diagnosticOrder
     */
    public void setDiagnosticOrder(DiagnosticOrder diagnosticOrder) {
        this.diagnosticOrder = diagnosticOrder;
    }

    /**
     * @return The medicationPrescription
     */
    public MedicationPrescription getMedicationPrescription() {
        return medicationPrescription;
    }

    /**
     * @param medicationPrescription The medicationPrescription
     */
    public void setMedicationPrescription(MedicationPrescription medicationPrescription) {
        this.medicationPrescription = medicationPrescription;
    }

}
