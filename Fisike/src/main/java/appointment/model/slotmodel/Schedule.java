package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("ref")
    @Expose
    private String ref;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref The ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

}

