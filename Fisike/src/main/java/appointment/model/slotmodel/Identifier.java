package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */



import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Identifier {

    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("system")
    @Expose
    private Object system;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("useCode")
    @Expose
    private String useCode;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("type")
    @Expose
    private IdentifierType type;

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The system
     */
    public Object getSystem() {
        return system;
    }

    /**
     *
     * @param system
     * The system
     */
    public void setSystem(Object system) {
        this.system = system;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The useCode
     */
    public String getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     *
     * @return
     * The type
     */
    public IdentifierType getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(IdentifierType type) {
        this.type = type;
    }

}