package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Extension {

    @SerializedName("value")
    @Expose
    private List<Value> value = new ArrayList<Value>();
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * @return The value
     */
    public List<Value> getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(List<Value> value) {
        this.value = value;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

}