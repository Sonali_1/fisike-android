package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("extension")
    @Expose
    private java.util.List<Extension> extension = new ArrayList<Extension>();
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("schedule")
    @Expose
    private Schedule schedule;
    @SerializedName("freeBusyType")
    @Expose
    private String freeBusyType;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("overbooked")
    @Expose
    private Overbooked overbooked;
    @SerializedName("actionTriggerService")
    @Expose
    private Object actionTriggerService;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("identifier")
    @Expose
    private java.util.List<Identifier> identifier = new ArrayList<Identifier>();
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("resourceType")
    @Expose
    private String resourceType;

    /**
     *
     * @return
     * The extension
     */
    public java.util.List<Extension> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(java.util.List<Extension> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The schedule
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     *
     * @param schedule
     * The schedule
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     *
     * @return
     * The freeBusyType
     */
    public String getFreeBusyType() {
        return freeBusyType;
    }

    /**
     *
     * @param freeBusyType
     * The freeBusyType
     */
    public void setFreeBusyType(String freeBusyType) {
        this.freeBusyType = freeBusyType;
    }

    /**
     *
     * @return
     * The start
     */
    public String getStart() {
        return start;
    }

    /**
     *
     * @param start
     * The start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     *
     * @return
     * The overbooked
     */
    public Overbooked getOverbooked() {
        return overbooked;
    }

    /**
     *
     * @param overbooked
     * The overbooked
     */
    public void setOverbooked(Overbooked overbooked) {
        this.overbooked = overbooked;
    }

    /**
     *
     * @return
     * The actionTriggerService
     */
    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    /**
     *
     * @param actionTriggerService
     * The actionTriggerService
     */
    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The type
     */
    public Type getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The identifier
     */
    public java.util.List<Identifier> getIdentifier() {
        return identifier;
    }

    /**
     *
     * @param identifier
     * The identifier
     */
    public void setIdentifier(java.util.List<Identifier> identifier) {
        this.identifier = identifier;
    }

    /**
     *
     * @return
     * The end
     */
    public String getEnd() {
        return end;
    }

    /**
     *
     * @param end
     * The end
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     *
     * @return
     * The resourceType
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     *
     * @param resourceType
     * The resourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }


    
}
