package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotDataModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("list")
    @Expose
    private java.util.List<appointment.model.slotmodel.List> list = new ArrayList<appointment.model.slotmodel.List>();

    /**
     * @return The totalCount
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount The totalCount
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return The list
     */
    public java.util.List<appointment.model.slotmodel.List> getList() {
        return list;
    }

    /**
     * @param list The list
     */
    public void setList(java.util.List<appointment.model.slotmodel.List> list) {
        this.list = list;
    }

}