package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Type {

    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private List<Coding> coding = new ArrayList<Coding>();

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The coding
     */
    public List<Coding> getCoding() {
        return coding;
    }

    /**
     *
     * @param coding
     * The coding
     */
    public void setCoding(List<Coding> coding) {
        this.coding = coding;
    }

}
