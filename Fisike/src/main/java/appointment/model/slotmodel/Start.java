package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Start {

    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * @return The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
