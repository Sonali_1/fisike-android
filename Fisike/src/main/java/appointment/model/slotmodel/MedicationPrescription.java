package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicationPrescription {

    @SerializedName("medicationPrescription")
    @Expose
    private String medicationPrescription;

    /**
     * @return The medicationPrescription
     */
    public String getMedicationPrescription() {
        return medicationPrescription;
    }

    /**
     * @param medicationPrescription The medicationPrescription
     */
    public void setMedicationPrescription(String medicationPrescription) {
        this.medicationPrescription = medicationPrescription;
    }

}