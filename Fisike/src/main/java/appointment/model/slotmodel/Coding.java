package appointment.model.slotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */


import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coding {

    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("system")
    @Expose
    private Object system;
    @SerializedName("userSelected")
    @Expose
    private Object userSelected;
    @SerializedName("display")
    @Expose
    private String display;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("version")
    @Expose
    private Object version;

    /**
     * @return The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return The system
     */
    public Object getSystem() {
        return system;
    }

    /**
     * @param system The system
     */
    public void setSystem(Object system) {
        this.system = system;
    }

    /**
     * @return The userSelected
     */
    public Object getUserSelected() {
        return userSelected;
    }

    /**
     * @param userSelected The userSelected
     */
    public void setUserSelected(Object userSelected) {
        this.userSelected = userSelected;
    }

    /**
     * @return The display
     */
    public String getDisplay() {
        return display;
    }

    /**
     * @param display The display
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The version
     */
    public Object getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    public void setVersion(Object version) {
        this.version = version;
    }

}
