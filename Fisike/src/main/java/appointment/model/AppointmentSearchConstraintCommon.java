package appointment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class AppointmentSearchConstraintCommon {

    @SerializedName("_count")
    @Expose
    private int count;

    @SerializedName("_skip")
    @Expose
    private int skip;

    @SerializedName("searchString")
    @Expose
    private String searchString;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
