package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Extension {
    public List<Value> getValue() {
        return value;
    }

    public void setValue(List<Value> value) {
        this.value = value;
    }

    @SerializedName("value")
    @Expose
    private List<Value> value;
    @SerializedName("url")
    @Expose
    private String url;

    public Extension() {
        value = new ArrayList<Value>();
    }

    /**
     *
     * @return
     * The value
     */

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
