package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Address {
    public java.util.List<AddressExtension> getExtension() {
        return extension;
    }

    public void setExtension(java.util.List<AddressExtension> extension) {
        this.extension = extension;
    }

    public void setLine(java.util.List<String> line) {
        this.line = line;
    }

    @SerializedName("extension")

    @Expose
    private java.util.List<AddressExtension> extension = new ArrayList<AddressExtension>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("line")
    @Expose
    private java.util.List<String> line = new ArrayList<String>();
    @SerializedName("useCode")
    @Expose
    private Object useCode;
    @SerializedName("district")
    @Expose
    private Object district;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private Object country;

    /**
     *
     * @return
     * The extension
     */

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    public java.util.List<String> getLine() {
        return line;
    }

    /**
     *
     * @return
     * The line

     */

    /**
     *
     * @return
     * The useCode
     */
    public Object getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(Object useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The district
     */
    public Object getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     * The district
     */
    public void setDistrict(Object district) {
        this.district = district;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     *
     * @return
     * The type
     */
    public Object getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Object type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The country
     */
    public Object getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(Object country) {
        this.country = country;
    }
}