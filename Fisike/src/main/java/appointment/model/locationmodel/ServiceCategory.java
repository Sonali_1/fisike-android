package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class ServiceCategory {

    @SerializedName("extension")
    @Expose
    private List<AddressExtension> extension = new ArrayList<AddressExtension>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private List<Object> coding = new ArrayList<Object>();

    /**
     *
     * @return
     * The extension
     */

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    public List<AddressExtension> getExtension() {
        return extension;
    }

    public void setExtension(List<AddressExtension> extension) {
        this.extension = extension;
    }

    public List<Object> getCoding() {
        return coding;
    }

    public void setCoding(List<Object> coding) {
        this.coding = coding;
    }
/**
     *
     * @return
     * The coding
     */


}
