package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Telecom {
    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("rank")
    @Expose
    private Object rank;
    @SerializedName("system")
    @Expose
    private String system;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("useCode")
    @Expose
    private String useCode;
    @SerializedName("period")
    @Expose
    private Object period;

    /**
     *
     * @return
     * The extension
     */


    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The id
     */

    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The rank
     */
    public Object getRank() {
        return rank;
    }

    /**
     *
     * @param rank
     * The rank
     */
    public void setRank(Object rank) {
        this.rank = rank;
    }

    /**
     *
     * @return
     * The system
     */
    public String getSystem() {
        return system;
    }

    /**
     *
     * @param system
     * The system
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The useCode
     */
    public String getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

}
