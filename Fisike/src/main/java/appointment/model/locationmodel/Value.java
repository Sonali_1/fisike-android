package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Value {
    @SerializedName("practitionerId")
    @Expose
    private String practitionerId;
    @SerializedName("value")
    @Expose
    private List<Value_> value = new ArrayList<Value_>();
    @SerializedName("url")
    @Expose
    private String url;

    /**
     *
     * @return
     * The practitionerId
     */
    public String getPractitionerId() {
        return practitionerId;
    }

    /**
     *
     * @param practitionerId
     * The practitionerId
     */
    public void setPractitionerId(String practitionerId) {
        this.practitionerId = practitionerId;
    }

    /**
     *
     * @return
     * The value
     */


    public List<Value_> getValue() {
        return value;
    }

    public void setValue(List<Value_> value) {
        this.value = value;
    }

    /**

     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}