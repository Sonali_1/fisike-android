package appointment.model.locationmodel;

/**
 * Created by Aastha on 01/04/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class List {

    @SerializedName("publicKey")
    @Expose
    private Object publicKey;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("actionTriggerService")
    @Expose
    private Object actionTriggerService;
    @SerializedName("serviceName")
    @Expose
    private Object serviceName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("providedBy")
    @Expose
    private Object providedBy;
    @SerializedName("eligibilityNote")
    @Expose
    private Object eligibilityNote;
    @SerializedName("referralMethod")
    @Expose
    private java.util.List<Object> referralMethod = new ArrayList<Object>();
    @SerializedName("notAvailable")
    @Expose
    private java.util.List<Object> notAvailable = new ArrayList<Object>();
    @SerializedName("telecom")
    @Expose
    private java.util.List<Object> telecom = new ArrayList<Object>();
    @SerializedName("appointmentRequired")
    @Expose
    private Object appointmentRequired;
    @SerializedName("coverageArea")
    @Expose
    private java.util.List<Object> coverageArea = new ArrayList<Object>();
    @SerializedName("programName")
    @Expose
    private java.util.List<Object> programName = new ArrayList<Object>();
    @SerializedName("extraDetails")
    @Expose
    private String extraDetails;
    @SerializedName("serviceProvisionCode")
    @Expose
    private java.util.List<Object> serviceProvisionCode = new ArrayList<Object>();
    @SerializedName("availableTime")
    @Expose
    private java.util.List<Object> availableTime = new ArrayList<Object>();
    @SerializedName("serviceType")
    @Expose
    private java.util.List<ServiceType> serviceType = new ArrayList<ServiceType>();
    @SerializedName("photo")
    @Expose
    private Object photo;
    @SerializedName("extension")
    @Expose
    private java.util.List<Extension> extension = new ArrayList<Extension>();
    @SerializedName("eligibility")
    @Expose
    private Object eligibility;
    @SerializedName("serviceCategory")
    @Expose
    private ServiceCategory serviceCategory;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("characteristic")
    @Expose
    private java.util.List<Characteristic> characteristic = new ArrayList<Characteristic>();
    @SerializedName("identifier")
    @Expose
    private java.util.List<Object> identifier = new ArrayList<Object>();
    @SerializedName("availabilityExceptions")
    @Expose
    private Object availabilityExceptions;

    /**
     *
     * @return
     * The publicKey
     */
    public Object getPublicKey() {
        return publicKey;
    }

    /**
     *
     * @param publicKey
     * The publicKey
     */
    public void setPublicKey(Object publicKey) {
        this.publicKey = publicKey;
    }

    /**
     *
     * @return
     * The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The actionTriggerService
     */
    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    /**
     *
     * @param actionTriggerService
     * The actionTriggerService
     */
    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    /**
     *
     * @return
     * The serviceName
     */
    public Object getServiceName() {
        return serviceName;
    }

    /**
     *
     * @param serviceName
     * The serviceName
     */
    public void setServiceName(Object serviceName) {
        this.serviceName = serviceName;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The providedBy
     */
    public Object getProvidedBy() {
        return providedBy;
    }

    /**
     *
     * @param providedBy
     * The providedBy
     */
    public void setProvidedBy(Object providedBy) {
        this.providedBy = providedBy;
    }

    /**
     *
     * @return
     * The eligibilityNote
     */
    public Object getEligibilityNote() {
        return eligibilityNote;
    }

    /**
     *
     * @param eligibilityNote
     * The eligibilityNote
     */
    public void setEligibilityNote(Object eligibilityNote) {
        this.eligibilityNote = eligibilityNote;
    }

    /**
     *
     * @return
     * The referralMethod
     */
    public java.util.List<Object> getReferralMethod() {
        return referralMethod;
    }

    /**
     *
     * @param referralMethod
     * The referralMethod
     */
    public void setReferralMethod(java.util.List<Object> referralMethod) {
        this.referralMethod = referralMethod;
    }

    /**
     *
     * @return
     * The notAvailable
     */
    public java.util.List<Object> getNotAvailable() {
        return notAvailable;
    }

    /**
     *
     * @param notAvailable
     * The notAvailable
     */
    public void setNotAvailable(java.util.List<Object> notAvailable) {
        this.notAvailable = notAvailable;
    }

    /**
     *
     * @return
     * The telecom
     */
    public java.util.List<Object> getTelecom() {
        return telecom;
    }

    /**
     *
     * @param telecom
     * The telecom
     */
    public void setTelecom(java.util.List<Object> telecom) {
        this.telecom = telecom;
    }

    /**
     *
     * @return
     * The appointmentRequired
     */
    public Object getAppointmentRequired() {
        return appointmentRequired;
    }

    /**
     *
     * @param appointmentRequired
     * The appointmentRequired
     */
    public void setAppointmentRequired(Object appointmentRequired) {
        this.appointmentRequired = appointmentRequired;
    }

    /**
     *
     * @return
     * The coverageArea
     */
    public java.util.List<Object> getCoverageArea() {
        return coverageArea;
    }

    /**
     *
     * @param coverageArea
     * The coverageArea
     */
    public void setCoverageArea(java.util.List<Object> coverageArea) {
        this.coverageArea = coverageArea;
    }

    /**
     *
     * @return
     * The programName
     */
    public java.util.List<Object> getProgramName() {
        return programName;
    }

    /**
     *
     * @param programName
     * The programName
     */
    public void setProgramName(java.util.List<Object> programName) {
        this.programName = programName;
    }

    /**
     *
     * @return
     * The extraDetails
     */
    public String getExtraDetails() {
        return extraDetails;
    }

    /**
     *
     * @param extraDetails
     * The extraDetails
     */
    public void setExtraDetails(String extraDetails) {
        this.extraDetails = extraDetails;
    }

    /**
     *
     * @return
     * The serviceProvisionCode
     */
    public java.util.List<Object> getServiceProvisionCode() {
        return serviceProvisionCode;
    }

    /**
     *
     * @param serviceProvisionCode
     * The serviceProvisionCode
     */
    public void setServiceProvisionCode(java.util.List<Object> serviceProvisionCode) {
        this.serviceProvisionCode = serviceProvisionCode;
    }

    /**
     *
     * @return
     * The availableTime
     */
    public java.util.List<Object> getAvailableTime() {
        return availableTime;
    }

    /**
     *
     * @param availableTime
     * The availableTime
     */
    public void setAvailableTime(java.util.List<Object> availableTime) {
        this.availableTime = availableTime;
    }

    /**
     *
     * @return
     * The serviceType
     */
    public java.util.List<ServiceType> getServiceType() {
        return serviceType;
    }

    /**
     *
     * @param serviceType
     * The serviceType
     */
    public void setServiceType(java.util.List<ServiceType> serviceType) {
        this.serviceType = serviceType;
    }

    /**
     *
     * @return
     * The photo
     */
    public Object getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The extension
     */
    public java.util.List<Extension> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(java.util.List<Extension> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The eligibility
     */
    public Object getEligibility() {
        return eligibility;
    }

    /**
     *
     * @param eligibility
     * The eligibility
     */
    public void setEligibility(Object eligibility) {
        this.eligibility = eligibility;
    }

    /**
     *
     * @return
     * The serviceCategory
     */
    public ServiceCategory getServiceCategory() {
        return serviceCategory;
    }

    /**
     *
     * @param serviceCategory
     * The serviceCategory
     */
    public void setServiceCategory(ServiceCategory serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The characteristic
     */
    public java.util.List<Characteristic> getCharacteristic() {
        return characteristic;
    }

    /**
     *
     * @param characteristic
     * The characteristic
     */
    public void setCharacteristic(java.util.List<Characteristic> characteristic) {
        this.characteristic = characteristic;
    }

    /**
     *
     * @return
     * The identifier
     */
    public java.util.List<Object> getIdentifier() {
        return identifier;
    }

    /**
     *
     * @param identifier
     * The identifier
     */
    public void setIdentifier(java.util.List<Object> identifier) {
        this.identifier = identifier;
    }

    /**
     *
     * @return
     * The availabilityExceptions
     */
    public Object getAvailabilityExceptions() {
        return availabilityExceptions;
    }

    /**
     *
     * @param availabilityExceptions
     * The availabilityExceptions
     */
    public void setAvailabilityExceptions(Object availabilityExceptions) {
        this.availabilityExceptions = availabilityExceptions;
    }
}
