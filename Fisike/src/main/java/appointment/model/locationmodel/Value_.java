package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Value_ {

    @SerializedName("requestableRoles")
    @Expose
    private List<String> requestableRoles = new ArrayList<String>();
    @SerializedName("bookableRoles")
    @Expose
    private List<String> bookableRoles = new ArrayList<String>();

    /**
     *
     * @return
     * The requestableRoles
     */
    public List<String> getRequestableRoles() {
        return requestableRoles;
    }

    public void setRequestableRoles(List<String> requestableRoles) {
        this.requestableRoles = requestableRoles;
    }

    public List<String> getBookableRoles() {
        return bookableRoles;
    }

    public void setBookableRoles(List<String> bookableRoles) {
        this.bookableRoles = bookableRoles;
    }
/**
     *
     * @param requestableRoles
     * The requestableRoles
     */

}

