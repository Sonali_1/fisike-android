package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Aastha on 01/04/2016.
 */
public class LocationModel {

    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("list")
    @Expose
    private java.util.List<appointment.model.locationmodel.List> list = new ArrayList<appointment.model.locationmodel.List>();

    /**
     *
     * @return
     * The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     * The totalCount
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     *
     * @return
     * The list
     */
    public java.util.List<appointment.model.locationmodel.List> getList() {
        return list;
    }

    /**
     *
     * @param list
     * The list
     */
    public void setList(java.util.List<appointment.model.locationmodel.List> list) {
        this.list = list;
    }


}
