package appointment.model.locationmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by Aastha on 01/04/2016.
 */
public class Type {
    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public List<Object> getCoding() {
        return coding;
    }

    public void setCoding(List<Object> coding) {
        this.coding = coding;
    }

    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private List<Object> coding = new ArrayList<Object>();

    /**
     *
     * @return
     * The extension
     */


    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The coding
     */

}