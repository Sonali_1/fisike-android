package appointment.model.locationmodel;

/**
 * Created by Aastha on 01/04/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Location {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("position")
    @Expose
    private Object position;
    @SerializedName("managingOrganization")
    @Expose
    private ManagingOrganization managingOrganization;
    @SerializedName("address")
    @Expose
    private List<Address> address = new ArrayList<Address>();
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("name")
    @Expose
    private Object name;
    @SerializedName("physicalType")
    @Expose
    private Object physicalType;
    @SerializedName("telecom")
    @Expose
    private List<Telecom> telecom = new ArrayList<Telecom>();
    @SerializedName("partOf")
    @Expose
    private Object partOf;
    @SerializedName("actionTriggerService")
    @Expose
    private Object actionTriggerService;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("identifier")
    @Expose
    private List<Identifier> identifier = new ArrayList<Identifier>();
    @SerializedName("mode")
    @Expose
    private Object mode;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The position
     */
    public Object getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Object position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The managingOrganization
     */
    public ManagingOrganization getManagingOrganization() {
        return managingOrganization;
    }

    /**
     *
     * @param managingOrganization
     * The managingOrganization
     */
    public void setManagingOrganization(ManagingOrganization managingOrganization) {
        this.managingOrganization = managingOrganization;
    }

    /**
     *
     * @return
     * The address
     */
    public List<Address> getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(List<Address> address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The status
     */
    public Object getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Object status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The name
     */
    public Object getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(Object name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The physicalType
     */
    public Object getPhysicalType() {
        return physicalType;
    }

    /**
     *
     * @param physicalType
     * The physicalType
     */
    public void setPhysicalType(Object physicalType) {
        this.physicalType = physicalType;
    }

    /**
     *
     * @return
     * The telecom
     */
    public List<Telecom> getTelecom() {
        return telecom;
    }

    /**
     *
     * @param telecom
     * The telecom
     */
    public void setTelecom(List<Telecom> telecom) {
        this.telecom = telecom;
    }

    /**
     *
     * @return
     * The partOf
     */
    public Object getPartOf() {
        return partOf;
    }

    /**
     *
     * @param partOf
     * The partOf
     */
    public void setPartOf(Object partOf) {
        this.partOf = partOf;
    }

    /**
     *
     * @return
     * The actionTriggerService
     */
    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    /**
     *
     * @param actionTriggerService
     * The actionTriggerService
     */
    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    /**
     *
     * @return
     * The type
     */
    public Object getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Object type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The identifier
     */
    public List<Identifier> getIdentifier() {
        return identifier;
    }

    /**
     *
     * @param identifier
     * The identifier
     */
    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }

    /**
     *
     * @return
     * The mode
     */
    public Object getMode() {
        return mode;
    }

    /**
     *
     * @param mode
     * The mode
     */
    public void setMode(Object mode) {
        this.mode = mode;
    }


}