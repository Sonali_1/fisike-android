package appointment.model.datetimeslotmodel;

import java.util.List;

/**
 * Created by laxmansingh on 4/4/2016.
 */
public class DateModel {

    private String datre;
    private List<TimeModel> timeModelList;


    public String getDatre() {
        return datre;
    }

    public void setDatre(String datre) {
        this.datre = datre;
    }

    public List<TimeModel> getTimeModelList() {
        return timeModelList;
    }

    public void setTimeModelList(List<TimeModel> timeModelList) {
        this.timeModelList = timeModelList;
    }
}
