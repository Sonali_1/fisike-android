package appointment.model.datetimeslotmodel;

/**
 * Created by laxmansingh on 4/4/2016.
 */
public class TimeModel {
    private String startdate;
    private String enddate;

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }
}
