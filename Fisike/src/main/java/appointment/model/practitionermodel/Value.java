package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Value {

    @SerializedName("bookable")
    @Expose
    private String bookable;

    /**
     *
     * @return
     * The bookable
     */
    public String getBookable() {
        return bookable;
    }

    /**
     *
     * @param bookable
     * The bookable
     */
    public void setBookable(String bookable) {
        this.bookable = bookable;
    }

}