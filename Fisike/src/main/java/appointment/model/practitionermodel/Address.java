package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Address {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("text")
    @Expose
    private Object text;
    @SerializedName("postalCode")
    @Expose
    private Object postalCode;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("line")
    @Expose
    private List<Object> line = new ArrayList<Object>();
    @SerializedName("useCode")
    @Expose
    private Object useCode;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("district")
    @Expose
    private Object district;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("city")
    @Expose
    private Object city;

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The text
     */
    public Object getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(Object text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public Object getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postalCode
     */
    public void setPostalCode(Object postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The state
     */
    public Object getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(Object state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The line
     */
    public List<Object> getLine() {
        return line;
    }

    /**
     *
     * @param line
     * The line
     */
    public void setLine(List<Object> line) {
        this.line = line;
    }

    /**
     *
     * @return
     * The useCode
     */
    public Object getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(Object useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The type
     */
    public Object getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Object type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     *
     * @return
     * The district
     */
    public Object getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     * The district
     */
    public void setDistrict(Object district) {
        this.district = district;
    }

    /**
     *
     * @return
     * The country
     */
    public Object getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(Object country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The city
     */
    public Object getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(Object city) {
        this.city = city;
    }

}