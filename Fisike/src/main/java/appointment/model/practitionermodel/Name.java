package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Name {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("given")
    @Expose
    private List<String> given = new ArrayList<String>();
    @SerializedName("family")
    @Expose
    private List<String> family = new ArrayList<String>();
    @SerializedName("prefix")
    @Expose
    private List<Object> prefix = new ArrayList<Object>();
    @SerializedName("useCode")
    @Expose
    private String useCode;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("suffix")
    @Expose
    private List<Object> suffix = new ArrayList<Object>();

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The given
     */
    public List<String> getGiven() {
        return given;
    }

    /**
     *
     * @param given
     * The given
     */
    public void setGiven(List<String> given) {
        this.given = given;
    }

    /**
     *
     * @return
     * The family
     */
    public List<String> getFamily() {
        return family;
    }

    /**
     *
     * @param family
     * The family
     */
    public void setFamily(List<String> family) {
        this.family = family;
    }

    /**
     *
     * @return
     * The prefix
     */
    public List<Object> getPrefix() {
        return prefix;
    }

    /**
     *
     * @param prefix
     * The prefix
     */
    public void setPrefix(List<Object> prefix) {
        this.prefix = prefix;
    }

    /**
     *
     * @return
     * The useCode
     */
    public String getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     *
     * @return
     * The suffix
     */
    public List<Object> getSuffix() {
        return suffix;
    }

    /**
     *
     * @param suffix
     * The suffix
     */
    public void setSuffix(List<Object> suffix) {
        this.suffix = suffix;
    }

}