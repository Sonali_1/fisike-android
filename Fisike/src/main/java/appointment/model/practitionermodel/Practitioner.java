package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Practitioner {

    private String testCost = "1049";

    private String discounted="950";
    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("telecom")
    @Expose
    private List<Object> telecom = new ArrayList<Object>();
    @SerializedName("lowerCaseName")
    @Expose
    private Object lowerCaseName;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("birthDate")
    @Expose
    private Object birthDate;
    @SerializedName("identifier")
    @Expose
    private List<Identifier> identifier = new ArrayList<Identifier>();

    public String getTestCost() {
        return testCost;
    }

    public void setTestCost(String testCost) {
        this.testCost = testCost;
    }

    @SerializedName("practitionerRole")
    @Expose
    private List<PractitionerRole> practitionerRole = new ArrayList<PractitionerRole>();
    @SerializedName("communication")
    @Expose
    private List<Object> communication = new ArrayList<Object>();
    @SerializedName("location")
    @Expose
    private List<Location> location = new ArrayList<Location>();
    @SerializedName("actionTriggerService")
    @Expose
    private Object actionTriggerService;
    @SerializedName("photo")
    @Expose
    private List<Object> photo = new ArrayList<Object>();
    @SerializedName("extension")
    @Expose
    private List<Extension> extension = new ArrayList<Extension>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("qualification")
    @Expose
    private List<Object> qualification = new ArrayList<Object>();
    @SerializedName("address")
    @Expose
    private List<Address> address = new ArrayList<Address>();

    /**
     *
     * @return
     * The practitionerRole
     */
    public List<PractitionerRole> getPractitionerRole() {
        return practitionerRole;
    }

    /**
     *
     * @param practitionerRole
     * The practitionerRole
     */
    public void setPractitionerRole(List<PractitionerRole> practitionerRole) {
        this.practitionerRole = practitionerRole;
    }

    /**
     *
     * @return
     * The communication
     */
    public List<Object> getCommunication() {
        return communication;
    }

    /**
     *
     * @param communication
     * The communication
     */
    public void setCommunication(List<Object> communication) {
        this.communication = communication;
    }

    /**
     *
     * @return
     * The location
     */
    public List<Location> getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(List<Location> location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The actionTriggerService
     */
    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    /**
     *
     * @param actionTriggerService
     * The actionTriggerService
     */
    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    /**
     *
     * @return
     * The photo
     */
    public List<Object> getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(List<Object> photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The extension
     */
    public List<Extension> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Extension> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The qualification
     */
    public List<Object> getQualification() {
        return qualification;
    }

    /**
     *
     * @param qualification
     * The qualification
     */
    public void setQualification(List<Object> qualification) {
        this.qualification = qualification;
    }

    /**
     *
     * @return
     * The address
     */
    public List<Address> getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(List<Address> address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The name
     */
    public Name getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The telecom
     */
    public List<Object> getTelecom() {
        return telecom;
    }

    /**
     *
     * @param telecom
     * The telecom
     */
    public void setTelecom(List<Object> telecom) {
        this.telecom = telecom;
    }

    /**
     *
     * @return
     * The lowerCaseName
     */
    public Object getLowerCaseName() {
        return lowerCaseName;
    }

    /**
     *
     * @param lowerCaseName
     * The lowerCaseName
     */
    public void setLowerCaseName(Object lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    /**
     *
     * @return
     * The active
     */
    public Object getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     *
     * @return
     * The gender
     */
    public Object getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(Object gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The birthDate
     */
    public Object getBirthDate() {
        return birthDate;
    }

    /**
     *
     * @param birthDate
     * The birthDate
     */
    public void setBirthDate(Object birthDate) {
        this.birthDate = birthDate;
    }

    /**
     *
     * @return
     * The identifier
     */
    public List<Identifier> getIdentifier() {
        return identifier;
    }

    /**
     *
     * @param identifier
     * The identifier
     */
    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }
    public String getDiscounted() {
        return discounted;
    }


}