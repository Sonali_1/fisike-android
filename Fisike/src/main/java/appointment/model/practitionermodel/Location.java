package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Location {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ref")
    @Expose
    private String ref;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The ref
     */
    public String getRef() {
        return ref;
    }

    /**
     *
     * @param ref
     * The ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

}
