package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Type {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private List<Object> coding = new ArrayList<Object>();

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The coding
     */
    public List<Object> getCoding() {
        return coding;
    }

    /**
     *
     * @param coding
     * The coding
     */
    public void setCoding(List<Object> coding) {
        this.coding = coding;
    }

}
