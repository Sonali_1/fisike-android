package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class PractitionerRole {

    @SerializedName("managingOrganization")
    @Expose
    private Object managingOrganization;
    @SerializedName("healthcareService")
    @Expose
    private Object healthcareService;
    @SerializedName("role")
    @Expose
    private Object role;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("specialty")
    @Expose
    private List<Specialty> specialty = new ArrayList<Specialty>();

    /**
     *
     * @return
     * The managingOrganization
     */
    public Object getManagingOrganization() {
        return managingOrganization;
    }

    /**
     *
     * @param managingOrganization
     * The managingOrganization
     */
    public void setManagingOrganization(Object managingOrganization) {
        this.managingOrganization = managingOrganization;
    }

    /**
     *
     * @return
     * The healthcareService
     */
    public Object getHealthcareService() {
        return healthcareService;
    }

    /**
     *
     * @param healthcareService
     * The healthcareService
     */
    public void setHealthcareService(Object healthcareService) {
        this.healthcareService = healthcareService;
    }

    /**
     *
     * @return
     * The role
     */
    public Object getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(Object role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     *
     * @return
     * The specialty
     */
    public List<Specialty> getSpecialty() {
        return specialty;
    }

    /**
     *
     * @param specialty
     * The specialty
     */
    public void setSpecialty(List<Specialty> specialty) {
        this.specialty = specialty;
    }

}