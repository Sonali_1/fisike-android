package appointment.model.practitionermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 16/03/2016.
 */
public class Identifier {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("extension")
    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @SerializedName("system")
    @Expose
    private Object system;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("useCode")
    @Expose
    private String useCode;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("period")
    @Expose
    private Object period;

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The extension
     */
    public List<Object> getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The system
     */
    public Object getSystem() {
        return system;
    }

    /**
     *
     * @param system
     * The system
     */
    public void setSystem(Object system) {
        this.system = system;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The useCode
     */
    public String getUseCode() {
        return useCode;
    }

    /**
     *
     * @param useCode
     * The useCode
     */
    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    /**
     *
     * @return
     * The type
     */
    public Type getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     *
     * @param period
     * The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

}
