package appointment.model.renderconfigmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by laxmansingh on 2/21/2017.
 */

public class StateCityAndZipMap {


    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("cities")
    @Expose
    private List<City> cities = null;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
