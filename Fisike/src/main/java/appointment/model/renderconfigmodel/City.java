package appointment.model.renderconfigmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by laxmansingh on 2/21/2017.
 */

public class City {


    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("pinCode")
    @Expose
    private List<String> pinCode = null;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<String> getPinCode() {
        return pinCode;
    }

    public void setPinCode(List<String> pinCode) {
        this.pinCode = pinCode;
    }

}
