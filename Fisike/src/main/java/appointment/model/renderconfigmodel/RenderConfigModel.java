package appointment.model.renderconfigmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import appointment.model.appointmentcommonmodel.GroupPrice;
import appointment.model.appointmentcommonmodel.MoHcsList;

/**
 * Created by laxmansingh on 2/21/2017.
 */

public class RenderConfigModel {

    @SerializedName("testInfoEnabled")
    @Expose
    private Boolean testInfoEnabled;
    @SerializedName("reportHeader")
    @Expose
    private String reportHeader;
    @SerializedName("orderHeader")
    @Expose
    private String orderHeader;
    @SerializedName("reportFooter")
    @Expose
    private String reportFooter;
    @SerializedName("orderFooter")
    @Expose
    private String orderFooter;
    @SerializedName("availableModalities")
    @Expose
    private List<AvailableModality> availableModalities = null;
    @SerializedName("availableGenderList")
    @Expose
    private List<AvailableGenderList> availableGenderList = null;
    @SerializedName("viewDateFormat")
    @Expose
    private String viewDateFormat;
    @SerializedName("availableRecordTypes")
    @Expose
    private List<AvailableRecordType> availableRecordTypes = null;
    @SerializedName("availableRecordStatuses")
    @Expose
    private List<AvailableRecordStatus> availableRecordStatuses = null;
    @SerializedName("availableWidgetContexts")
    @Expose
    private List<AvailableWidgetContext> availableWidgetContexts = null;
    @SerializedName("availableUserTypeForWidget")
    @Expose
    private List<AvailableUserTypeForWidget> availableUserTypeForWidget = null;
    @SerializedName("availableWidgetSizes")
    @Expose
    private List<AvailableWidgetSize> availableWidgetSizes = null;
    @SerializedName("useAgfaXEROViewer")
    @Expose
    private String useAgfaXEROViewer;
  /*  @SerializedName("timeZone")
    @Expose
    private String timeZone;*/
    @SerializedName("roleList")
    @Expose
    private List<String> roleList = null;
    @SerializedName("sendToPacsDestinations")
    @Expose
    private SendToPacsDestinations sendToPacsDestinations;
    @SerializedName("phoneMask")
    @Expose
    private String phoneMask;
    @SerializedName("appointmentEnabled")
    @Expose
    private String appointmentEnabled;
    @SerializedName("appointmentEditableBefore")
    @Expose
    private String appointmentEditableBefore;
    @SerializedName("appointmentSlotDuration")
    @Expose
    private String appointmentSlotDuration;
    @SerializedName("showRescheduleFeatureInAppointment")
    @Expose
    private Boolean showRescheduleFeatureInAppointment;
    @SerializedName("showCancelFeatureInAppointment")
    @Expose
    private Boolean showCancelFeatureInAppointment;
    @SerializedName("showStaticTextOnListPage")
    @Expose
    private Boolean showStaticTextOnListPage;
    @SerializedName("redirectPageAfterAppointmentSave")
    @Expose
    private String redirectPageAfterAppointmentSave;
    @SerializedName("permissableHcsRoles")
    @Expose
    private List<String> permissableHcsRoles = null;
    @SerializedName("defaultRequestableRoleInHCS")
    @Expose
    private String defaultRequestableRoleInHCS;
    @SerializedName("defaultBookableRoleInHCS")
    @Expose
    private String defaultBookableRoleInHCS;
    @SerializedName("daysDurationForFirstAvailableSlot")
    @Expose
    private String daysDurationForFirstAvailableSlot;
    @SerializedName("stateCityAndZipMap")
    @Expose
    private List<StateCityAndZipMap> stateCityAndZipMap = null;
    @SerializedName("checkOrmStatusInReports")
    @Expose
    private Boolean checkOrmStatusInReports;
    @SerializedName("ormStatusValueInReports")
    @Expose
    private String ormStatusValueInReports;
    @SerializedName("fetchResultsCount")
    @Expose
    private String fetchResultsCount;
    @SerializedName("showPractitionerSpeciality")
    @Expose
    private Boolean showPractitionerSpeciality;
    @SerializedName("showTimeInEUFormat")
    @Expose
    private Boolean showTimeInEUFormat;
    @SerializedName("testCompendiumDepartments")
    @Expose
    private List<String> testCompendiumDepartments = null;
    @SerializedName("testCompendiumSystems")
    @Expose
    private List<String> testCompendiumSystems = null;
    @SerializedName("globalTestHeader")
    @Expose
    private String globalTestHeader;
    @SerializedName("globalTestFooter")
    @Expose
    private String globalTestFooter;
    @SerializedName("batteryTestComponent")
    @Expose
    private String batteryTestComponent;
    @SerializedName("historyValue")
    @Expose
    private Integer historyValue;
    @SerializedName("supportFormattedReports")
    @Expose
    private Boolean supportFormattedReports;
    @SerializedName("disabledUserTypeForChangePassword")
    @Expose
    private List<Object> disabledUserTypeForChangePassword = null;
    @SerializedName("displayOnlyLatestRadReport")
    @Expose
    private Boolean displayOnlyLatestRadReport;
    @SerializedName("patientSearchAction")
    @Expose
    private List<PatientSearchAction> patientSearchAction = null;
    @SerializedName("patientSearchSortParams")
    @Expose
    private List<PatientSearchSortParam> patientSearchSortParams = null;
    @SerializedName("patientSearchNameOrder")
    @Expose
    private String patientSearchNameOrder;
    @SerializedName("patientSearchGenderList")
    @Expose
    private List<PatientSearchGenderList> patientSearchGenderList = null;

    public Boolean getTestInfoEnabled() {
        return testInfoEnabled;
    }

    public void setTestInfoEnabled(Boolean testInfoEnabled) {
        this.testInfoEnabled = testInfoEnabled;
    }

    public String getReportHeader() {
        return reportHeader;
    }

    public void setReportHeader(String reportHeader) {
        this.reportHeader = reportHeader;
    }

    public String getOrderHeader() {
        return orderHeader;
    }

    public void setOrderHeader(String orderHeader) {
        this.orderHeader = orderHeader;
    }

    public String getReportFooter() {
        return reportFooter;
    }

    public void setReportFooter(String reportFooter) {
        this.reportFooter = reportFooter;
    }

    public String getOrderFooter() {
        return orderFooter;
    }

    public void setOrderFooter(String orderFooter) {
        this.orderFooter = orderFooter;
    }

    public List<AvailableModality> getAvailableModalities() {
        return availableModalities;
    }

    public void setAvailableModalities(List<AvailableModality> availableModalities) {
        this.availableModalities = availableModalities;
    }

    public List<AvailableGenderList> getAvailableGenderList() {
        return availableGenderList;
    }

    public void setAvailableGenderList(List<AvailableGenderList> availableGenderList) {
        this.availableGenderList = availableGenderList;
    }

    public String getViewDateFormat() {
        return viewDateFormat;
    }

    public void setViewDateFormat(String viewDateFormat) {
        this.viewDateFormat = viewDateFormat;
    }

    public List<AvailableRecordType> getAvailableRecordTypes() {
        return availableRecordTypes;
    }

    public void setAvailableRecordTypes(List<AvailableRecordType> availableRecordTypes) {
        this.availableRecordTypes = availableRecordTypes;
    }

    public List<AvailableRecordStatus> getAvailableRecordStatuses() {
        return availableRecordStatuses;
    }

    public void setAvailableRecordStatuses(List<AvailableRecordStatus> availableRecordStatuses) {
        this.availableRecordStatuses = availableRecordStatuses;
    }

    public List<AvailableWidgetContext> getAvailableWidgetContexts() {
        return availableWidgetContexts;
    }

    public void setAvailableWidgetContexts(List<AvailableWidgetContext> availableWidgetContexts) {
        this.availableWidgetContexts = availableWidgetContexts;
    }

    public List<AvailableUserTypeForWidget> getAvailableUserTypeForWidget() {
        return availableUserTypeForWidget;
    }

    public void setAvailableUserTypeForWidget(List<AvailableUserTypeForWidget> availableUserTypeForWidget) {
        this.availableUserTypeForWidget = availableUserTypeForWidget;
    }

    public List<AvailableWidgetSize> getAvailableWidgetSizes() {
        return availableWidgetSizes;
    }

    public void setAvailableWidgetSizes(List<AvailableWidgetSize> availableWidgetSizes) {
        this.availableWidgetSizes = availableWidgetSizes;
    }

    public String getUseAgfaXEROViewer() {
        return useAgfaXEROViewer;
    }

    public void setUseAgfaXEROViewer(String useAgfaXEROViewer) {
        this.useAgfaXEROViewer = useAgfaXEROViewer;
    }

   /* public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
*/
    public List<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }

    public SendToPacsDestinations getSendToPacsDestinations() {
        return sendToPacsDestinations;
    }

    public void setSendToPacsDestinations(SendToPacsDestinations sendToPacsDestinations) {
        this.sendToPacsDestinations = sendToPacsDestinations;
    }

    public String getPhoneMask() {
        return phoneMask;
    }

    public void setPhoneMask(String phoneMask) {
        this.phoneMask = phoneMask;
    }

    public String getAppointmentEnabled() {
        return appointmentEnabled;
    }

    public void setAppointmentEnabled(String appointmentEnabled) {
        this.appointmentEnabled = appointmentEnabled;
    }

    public String getAppointmentEditableBefore() {
        return appointmentEditableBefore;
    }

    public void setAppointmentEditableBefore(String appointmentEditableBefore) {
        this.appointmentEditableBefore = appointmentEditableBefore;
    }

    public String getAppointmentSlotDuration() {
        return appointmentSlotDuration;
    }

    public void setAppointmentSlotDuration(String appointmentSlotDuration) {
        this.appointmentSlotDuration = appointmentSlotDuration;
    }

    public Boolean getShowRescheduleFeatureInAppointment() {
        return showRescheduleFeatureInAppointment;
    }

    public void setShowRescheduleFeatureInAppointment(Boolean showRescheduleFeatureInAppointment) {
        this.showRescheduleFeatureInAppointment = showRescheduleFeatureInAppointment;
    }

    public Boolean getShowCancelFeatureInAppointment() {
        return showCancelFeatureInAppointment;
    }

    public void setShowCancelFeatureInAppointment(Boolean showCancelFeatureInAppointment) {
        this.showCancelFeatureInAppointment = showCancelFeatureInAppointment;
    }

    public Boolean getShowStaticTextOnListPage() {
        return showStaticTextOnListPage;
    }

    public void setShowStaticTextOnListPage(Boolean showStaticTextOnListPage) {
        this.showStaticTextOnListPage = showStaticTextOnListPage;
    }

    public String getRedirectPageAfterAppointmentSave() {
        return redirectPageAfterAppointmentSave;
    }

    public void setRedirectPageAfterAppointmentSave(String redirectPageAfterAppointmentSave) {
        this.redirectPageAfterAppointmentSave = redirectPageAfterAppointmentSave;
    }

    public List<String> getPermissableHcsRoles() {
        return permissableHcsRoles;
    }

    public void setPermissableHcsRoles(List<String> permissableHcsRoles) {
        this.permissableHcsRoles = permissableHcsRoles;
    }

    public String getDefaultRequestableRoleInHCS() {
        return defaultRequestableRoleInHCS;
    }

    public void setDefaultRequestableRoleInHCS(String defaultRequestableRoleInHCS) {
        this.defaultRequestableRoleInHCS = defaultRequestableRoleInHCS;
    }

    public String getDefaultBookableRoleInHCS() {
        return defaultBookableRoleInHCS;
    }

    public void setDefaultBookableRoleInHCS(String defaultBookableRoleInHCS) {
        this.defaultBookableRoleInHCS = defaultBookableRoleInHCS;
    }

    public String getDaysDurationForFirstAvailableSlot() {
        return daysDurationForFirstAvailableSlot;
    }

    public void setDaysDurationForFirstAvailableSlot(String daysDurationForFirstAvailableSlot) {
        this.daysDurationForFirstAvailableSlot = daysDurationForFirstAvailableSlot;
    }

    public List<StateCityAndZipMap> getStateCityAndZipMap() {
        return stateCityAndZipMap;
    }

    public void setStateCityAndZipMap(List<StateCityAndZipMap> stateCityAndZipMap) {
        this.stateCityAndZipMap = stateCityAndZipMap;
    }

    public Boolean getCheckOrmStatusInReports() {
        return checkOrmStatusInReports;
    }

    public void setCheckOrmStatusInReports(Boolean checkOrmStatusInReports) {
        this.checkOrmStatusInReports = checkOrmStatusInReports;
    }

    public String getOrmStatusValueInReports() {
        return ormStatusValueInReports;
    }

    public void setOrmStatusValueInReports(String ormStatusValueInReports) {
        this.ormStatusValueInReports = ormStatusValueInReports;
    }

    public String getFetchResultsCount() {
        return fetchResultsCount;
    }

    public void setFetchResultsCount(String fetchResultsCount) {
        this.fetchResultsCount = fetchResultsCount;
    }

    public Boolean getShowPractitionerSpeciality() {
        return showPractitionerSpeciality;
    }

    public void setShowPractitionerSpeciality(Boolean showPractitionerSpeciality) {
        this.showPractitionerSpeciality = showPractitionerSpeciality;
    }

    public Boolean getShowTimeInEUFormat() {
        return showTimeInEUFormat;
    }

    public void setShowTimeInEUFormat(Boolean showTimeInEUFormat) {
        this.showTimeInEUFormat = showTimeInEUFormat;
    }

    public List<String> getTestCompendiumDepartments() {
        return testCompendiumDepartments;
    }

    public void setTestCompendiumDepartments(List<String> testCompendiumDepartments) {
        this.testCompendiumDepartments = testCompendiumDepartments;
    }

    public List<String> getTestCompendiumSystems() {
        return testCompendiumSystems;
    }

    public void setTestCompendiumSystems(List<String> testCompendiumSystems) {
        this.testCompendiumSystems = testCompendiumSystems;
    }

    public String getGlobalTestHeader() {
        return globalTestHeader;
    }

    public void setGlobalTestHeader(String globalTestHeader) {
        this.globalTestHeader = globalTestHeader;
    }

    public String getGlobalTestFooter() {
        return globalTestFooter;
    }

    public void setGlobalTestFooter(String globalTestFooter) {
        this.globalTestFooter = globalTestFooter;
    }

    public String getBatteryTestComponent() {
        return batteryTestComponent;
    }

    public void setBatteryTestComponent(String batteryTestComponent) {
        this.batteryTestComponent = batteryTestComponent;
    }

    public Integer getHistoryValue() {
        return historyValue;
    }

    public void setHistoryValue(Integer historyValue) {
        this.historyValue = historyValue;
    }

    public Boolean getSupportFormattedReports() {
        return supportFormattedReports;
    }

    public void setSupportFormattedReports(Boolean supportFormattedReports) {
        this.supportFormattedReports = supportFormattedReports;
    }

    public List<Object> getDisabledUserTypeForChangePassword() {
        return disabledUserTypeForChangePassword;
    }

    public void setDisabledUserTypeForChangePassword(List<Object> disabledUserTypeForChangePassword) {
        this.disabledUserTypeForChangePassword = disabledUserTypeForChangePassword;
    }

    public Boolean getDisplayOnlyLatestRadReport() {
        return displayOnlyLatestRadReport;
    }

    public void setDisplayOnlyLatestRadReport(Boolean displayOnlyLatestRadReport) {
        this.displayOnlyLatestRadReport = displayOnlyLatestRadReport;
    }

    public List<PatientSearchAction> getPatientSearchAction() {
        return patientSearchAction;
    }

    public void setPatientSearchAction(List<PatientSearchAction> patientSearchAction) {
        this.patientSearchAction = patientSearchAction;
    }

    public List<PatientSearchSortParam> getPatientSearchSortParams() {
        return patientSearchSortParams;
    }

    public void setPatientSearchSortParams(List<PatientSearchSortParam> patientSearchSortParams) {
        this.patientSearchSortParams = patientSearchSortParams;
    }

    public String getPatientSearchNameOrder() {
        return patientSearchNameOrder;
    }

    public void setPatientSearchNameOrder(String patientSearchNameOrder) {
        this.patientSearchNameOrder = patientSearchNameOrder;
    }

    public List<PatientSearchGenderList> getPatientSearchGenderList() {
        return patientSearchGenderList;
    }

    public void setPatientSearchGenderList(List<PatientSearchGenderList> patientSearchGenderList) {
        this.patientSearchGenderList = patientSearchGenderList;
    }
}
