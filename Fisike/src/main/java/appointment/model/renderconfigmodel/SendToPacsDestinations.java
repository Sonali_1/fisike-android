package appointment.model.renderconfigmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 2/21/2017.
 */

public class SendToPacsDestinations {


    @SerializedName("PACS 1")
    @Expose
    private String pACS1;
    @SerializedName("PACS 2")
    @Expose
    private String pACS2;

    public String getPACS1() {
        return pACS1;
    }

    public void setPACS1(String pACS1) {
        this.pACS1 = pACS1;
    }

    public String getPACS2() {
        return pACS2;
    }

    public void setPACS2(String pACS2) {
        this.pACS2 = pACS2;
    }
}
