package appointment.model;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;

/**
 * Created by aastha on 11/10/2016.
 */
public class LinkedPatientInfoMO implements Serializable{
    private static final long serialVersionUID = -4086287806785675730L;
    String dateOfBirth;
    String gender;
    String phoneNumber;
    String name;
    String email;
    long patientId;
    ArrayList<Address>  addressList;
    ArrayList<FetchUpdateIdentifier> identifier;
    ArrayList<FetchUpdateTelecom> emailList;
    ArrayList<FetchUpdateTelecom> phoneList;
    String maritalStatus;

    private PatientMO patientMO;
    private String profilePic;
    private ArrayList<Coverage> coverage = new ArrayList<>();
    private String relationshipWithUser;
    public String getCardFrontImage() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getFrontImage();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getCardBackImage() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getBackImage();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getInsuranceType() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getInsuranceType();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getInsuranceStatus() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getStatus();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getPolicyNumber() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getPolicyNumber();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getPrimaryInsuranceCompany() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getPrimaryInsuranceCompany();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }


    public String getPolicyHolderName() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getPolicyHolderName();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }

    public int getCoverageId() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getId();
        }
        catch (Exception e)
        {
            return 0;
        }
        return 0;
    }


    public int getSubscriberRefId() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getSubscriberRefId();
        }
        catch (Exception e)
        {
            return 0;
        }
        return 0;
    }


    public LinkedPatientInfoMO() {
    }

    public LinkedPatientInfoMO(String birthdate, String name, String phone, String email, String gender, long id) {
        this.dateOfBirth = birthdate;
        this.gender = gender;
        this.phoneNumber = phone;
        this.name = name;
        this.email = email;
        this.patientId = id;
    }

    public LinkedPatientInfoMO(String birthdate, String name, String phone, String email, String gender, long id,PatientMO patientMO,ArrayList<Coverage> coverage) {
        this.dateOfBirth = birthdate;
        this.gender = gender;
        this.phoneNumber = phone;
        this.name = name;
        this.email = email;
        this.patientId = id;
        this.coverage=coverage;

    }


    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public ArrayList<Coverage> getCoverage() {
        return coverage;
    }

    public void setCoverage(ArrayList<Coverage> coverage) {
        this.coverage = coverage;
    }

    public PatientMO getPatientMO() {
        return patientMO;
    }

    public void setPatientMO(PatientMO patientMO) {
        this.patientMO = patientMO;
    }

    public String getGroupId() {
        try {
            if(coverage!=null && coverage.size()>0)
                return coverage.get(0).getGroupId();
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }

    public String getRelationshipWithUser() {
        return relationshipWithUser;
    }

    public void setRelationshipWithUser(String relationshipWithUser) {
        this.relationshipWithUser = relationshipWithUser;
    }

    public ArrayList<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<Address> addressList) {
        this.addressList = addressList;
    }

    public ArrayList<FetchUpdateIdentifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(ArrayList<FetchUpdateIdentifier> identifier) {
        this.identifier = identifier;
    }

    public ArrayList<FetchUpdateTelecom> getEmailList() {
        return emailList;
    }

    public void setEmailList(ArrayList<FetchUpdateTelecom> emailList) {
        this.emailList = emailList;
    }

    public ArrayList<FetchUpdateTelecom> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(ArrayList<FetchUpdateTelecom> phoneList) {
        this.phoneList = phoneList;
    }

    public String getAadharNumber() {
        String aadharNumber = "";

        try {
            if (identifier != null && identifier.size() > 0) {

                for (int i = 0; i < identifier.size(); i++) {
                    FetchUpdateIdentifier telecom = identifier.get(i);
                    if (Utils.isValueAvailable(telecom.getText()) &&
                            telecom.getText().equalsIgnoreCase(MyApplication.getAppContext().getString(R.string.aadhar_key))) {
                        aadharNumber = telecom.getValue();
                        return aadharNumber;
                    }

                }
            }
        } catch (Exception e) {

        }
        return aadharNumber;

    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
}
