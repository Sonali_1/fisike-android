package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class Type implements Parcelable {

    @SerializedName("extension")
    @Expose
    private java.util.List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("coding")
    @Expose
    private java.util.List<Object> coding = new ArrayList<Object>();

    /**
     * @return The extension
     */
    public java.util.List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The coding
     */
    public java.util.List<Object> getCoding() {
        return coding;
    }

    /**
     * @param coding The coding
     */
    public void setCoding(java.util.List<Object> coding) {
        this.coding = coding;
    }















        /*[    Parcelable code here   ]*/

    public Type() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeValue(id);
        dest.writeString(text);
        dest.writeList(extension);
        dest.writeList(coding);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        public Type[] newArray(int size) {
            return new Type[size];
        }
    };

    // "De-parcel object
    public Type(Parcel in) {
        id = in.readValue(Object.class.getClassLoader());
        text = in.readString();
        extension = in.readArrayList(Object.class.getClassLoader());
        coding = in.readArrayList(Object.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/


}