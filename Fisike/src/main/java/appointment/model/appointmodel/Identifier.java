package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import appointment.model.appointmodel.*;
import appointment.model.appointmodel.End;
import appointment.model.appointmodel.Start;


/**
 * Created by Raj on 1/13/2017.
 */

public class Identifier implements  Parcelable{


    @SerializedName("value")
    @Expose
    private String value;



    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }






    public Identifier() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(value);

    }


    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Identifier createFromParcel(Parcel in) {
            return new Identifier(in);
        }

        public Identifier[] newArray(int size) {
            return new Identifier[size];
        }
    };

    // "De-parcel object
    public Identifier(Parcel in) {

        value = in.readString();
    }

/*[    End of Parcelable code here   ]*/

}

