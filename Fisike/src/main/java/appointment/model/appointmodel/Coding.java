package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 12/23/2016.
 */

public class Coding implements Parcelable{
    @SerializedName("display")
    @Expose
    private String display;

    @SerializedName("code")
    @Expose
    private String code;


    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



    /*[    Parcelable code here   ]*/

    public Coding() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(display);
        dest.writeString(code);
    }


    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Coding createFromParcel(Parcel in) {
            return new Coding(in);
        }

        public Coding[] newArray(int size) {
            return new Coding[size];
        }
    };

    // "De-parcel object
    public Coding(Parcel in) {
        display = in.readString();
        code = in.readString();
    }

/*[    End of Parcelable code here   ]*/
}
