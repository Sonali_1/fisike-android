package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class Start implements Parcelable {

    @SerializedName("extension")
    @Expose
    private java.util.List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * @return The extension
     */
    public java.util.List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }









        /*[    Parcelable code here   ]*/

    public Start() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeValue(id);
        dest.writeString(value);
        dest.writeList(extension);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public Start createFromParcel(Parcel in) {
            return new Start(in);
        }

        public Start[] newArray(int size) {
            return new Start[size];
        }
    };

    // "De-parcel object
    public Start(Parcel in) {
        id = in.readValue(Object.class.getClassLoader());
        value = in.readString();
        extension = in.readArrayList(Object.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/

}