package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class AppointmentDataModel implements Parcelable {

    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("list")
    @Expose
    private java.util.List<List> list = new ArrayList<List>();

    /**
     * @return The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount The totalCount
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return The list
     */
    public java.util.List<List> getList() {
        return list;
    }

    /**
     * @param list The list
     */
    public void setList(java.util.List<List> list) {
        this.list = list;
    }





        /*[    Parcelable code here   ]*/

    public AppointmentDataModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(totalCount);
        dest.writeList(list);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public AppointmentDataModel createFromParcel(Parcel in) {
            return new AppointmentDataModel(in);
        }

        public AppointmentDataModel[] newArray(int size) {
            return new AppointmentDataModel[size];
        }
    };

    // "De-parcel object
    public AppointmentDataModel(Parcel in) {
        totalCount = in.readInt();
        list = in.readArrayList(List.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/

}