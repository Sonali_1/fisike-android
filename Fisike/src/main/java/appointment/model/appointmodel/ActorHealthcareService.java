package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class ActorHealthcareService implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ref")
    @Expose
    private String ref;

    @SerializedName("class")
    @Expose
    private String class_name="com.mphrx.consus.resources.HealthcareService";

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref The ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }


/*[    Parcelable code here   ]*/

    public ActorHealthcareService() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(ref);
        dest.writeString(class_name);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public ActorHealthcareService createFromParcel(Parcel in) {
            return new ActorHealthcareService(in);
        }

        public ActorHealthcareService[] newArray(int size) {
            return new ActorHealthcareService[size];
        }
    };

    // "De-parcel object
    public ActorHealthcareService(Parcel in) {
        id = in.readInt();
        ref = in.readString();
        class_name = in.readString();
    }

/*[    End of Parcelable code here   ]*/

}
