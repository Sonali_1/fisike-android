package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class Extension implements Parcelable {

    @SerializedName("value")
    @Expose
    private java.util.List<Value> value = new ArrayList<Value>();
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * @return The value
     */
    public java.util.List<Value> getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(java.util.List<Value> value) {
        this.value = value;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }






        /*[    Parcelable code here   ]*/

    public Extension() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeList(value);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public Extension createFromParcel(Parcel in) {
            return new Extension(in);
        }

        public Extension[] newArray(int size) {
            return new Extension[size];
        }
    };

    // "De-parcel object
    public Extension(Parcel in) {
        url = in.readString();
        in.readList(value, getClass().getClassLoader());
    }

/*[    End of Parcelable code here   ]*/

}

