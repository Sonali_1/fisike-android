package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class MinutesDuration implements Parcelable {

    @SerializedName("extension")
    @Expose
    private java.util.List<Object> extension = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("value")
    @Expose
    private Integer value;

    /**
     * @return The extension
     */
    public java.util.List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The value
     */
    public Integer getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(Integer value) {
        this.value = value;
    }










        /*[    Parcelable code here   ]*/

    public MinutesDuration() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeInt(value);
        dest.writeList(extension);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public MinutesDuration createFromParcel(Parcel in) {
            return new MinutesDuration(in);
        }

        public MinutesDuration[] newArray(int size) {
            return new MinutesDuration[size];
        }
    };

    // "De-parcel object
    public MinutesDuration(Parcel in) {
        id = in.readInt();
        value = in.readInt();
        extension = in.readArrayList(Object.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/


}

