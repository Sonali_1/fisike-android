package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by laxmansingh on 12/23/2016.
 */

public class Characteristic implements Parcelable{

    @SerializedName("coding")
    @Expose
    private java.util.List<Coding> coding = new ArrayList<Coding>();
    @SerializedName("text")
    @Expose
    private String text;


    public List<Coding> getCoding() {
        return coding;
    }

    public void setCoding(List<Coding> coding) {
        this.coding = coding;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    /*[    Parcelable code here   ]*/

    public Characteristic() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(coding);
        dest.writeString(text);
    }


    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Characteristic createFromParcel(Parcel in) {
            return new Characteristic(in);
        }

        public Characteristic[] newArray(int size) {
            return new Characteristic[size];
        }
    };

    // "De-parcel object
    public Characteristic(Parcel in) {
        in.readList(coding, getClass().getClassLoader());
        text = in.readString();
    }

/*[    End of Parcelable code here   ]*/
}
