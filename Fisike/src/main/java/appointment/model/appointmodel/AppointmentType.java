package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 4/8/2016.
 */
public class AppointmentType implements Parcelable {

    @SerializedName("extension")
    @Expose
    private java.util.List<Object> extension = new ArrayList<Object>();

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("coding")
    @Expose
    private java.util.List<Object> coding = new ArrayList<Object>();



    /**
     * @return The extension
     */
    public java.util.List<Object> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Object> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }



    /**
     * @return The coding
     */
    public java.util.List<Object> getCoding() {
        return coding;
    }

    /**
     * @param coding The coding
     */
    public void setCoding(java.util.List<Object> coding) {
        this.coding = coding;
    }


        /*[    Parcelable code here   ]*/

    public AppointmentType() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(extension);
        dest.writeInt(id);
        dest.writeString(text);
        dest.writeList(coding);
    }



    // Creator
    public static final Creator CREATOR = new Creator() {
        public AppointmentType createFromParcel(Parcel in) {
            return new AppointmentType(in);
        }

        public AppointmentType[] newArray(int size) {
            return new AppointmentType[size];
        }
    };

    // "De-parcel object
    public AppointmentType(Parcel in) {
        extension = in.readArrayList(Object.class.getClassLoader());
        id = in.readInt();
        text = in.readString();
        coding = in.readArrayList(Object.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/


}