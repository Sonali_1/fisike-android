package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class Participant implements Parcelable {

    @SerializedName("actorPatient")
    @Expose
    private ActorPatient actorPatient;
    @SerializedName("actorDevice")
    @Expose
    private Object actorDevice;
    @SerializedName("actorRelatedPerson")
    @Expose
    private Object actorRelatedPerson;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("actorLocation")
    @Expose
    private Object actorLocation;
    @SerializedName("actorPractitioner")
    @Expose
    private Object actorPractitioner;
    @SerializedName("required")
    @Expose
    private Object required;
    @SerializedName("type")
    @Expose
    private java.util.List<Type> type = new ArrayList<Type>();
    @SerializedName("actorHealthcareService")
    @Expose
    private ActorHealthcareService actorHealthcareService;

    /**
     * @return The actorPatient
     */
    public ActorPatient getActorPatient() {
        return actorPatient;
    }

    /**
     * @param actorPatient The actorPatient
     */
    public void setActorPatient(ActorPatient actorPatient) {
        this.actorPatient = actorPatient;
    }

    /**
     * @return The actorDevice
     */
    public Object getActorDevice() {
        return actorDevice;
    }

    /**
     * @param actorDevice The actorDevice
     */
    public void setActorDevice(Object actorDevice) {
        this.actorDevice = actorDevice;
    }

    /**
     * @return The actorRelatedPerson
     */
    public Object getActorRelatedPerson() {
        return actorRelatedPerson;
    }

    /**
     * @param actorRelatedPerson The actorRelatedPerson
     */
    public void setActorRelatedPerson(Object actorRelatedPerson) {
        this.actorRelatedPerson = actorRelatedPerson;
    }

    /**
     * @return The status
     */
    public Object getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Object status) {
        this.status = status;
    }

    /**
     * @return The actorLocation
     */
    public Object getActorLocation() {
        return actorLocation;
    }

    /**
     * @param actorLocation The actorLocation
     */
    public void setActorLocation(Object actorLocation) {
        this.actorLocation = actorLocation;
    }

    /**
     * @return The actorPractitioner
     */
    public Object getActorPractitioner() {
        return actorPractitioner;
    }

    /**
     * @param actorPractitioner The actorPractitioner
     */
    public void setActorPractitioner(Object actorPractitioner) {
        this.actorPractitioner = actorPractitioner;
    }

    /**
     * @return The required
     */
    public Object getRequired() {
        return required;
    }

    /**
     * @param required The required
     */
    public void setRequired(Object required) {
        this.required = required;
    }

    /**
     * @return The type
     */
    public java.util.List<Type> getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(java.util.List<Type> type) {
        this.type = type;
    }

    /**
     * @return The actorHealthcareService
     */
    public ActorHealthcareService getActorHealthcareService() {
        return actorHealthcareService;
    }

    /**
     * @param actorHealthcareService The actorHealthcareService
     */
    public void setActorHealthcareService(ActorHealthcareService actorHealthcareService) {
        this.actorHealthcareService = actorHealthcareService;
    }







        /*[    Parcelable code here   ]*/

    public Participant() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeValue(actorPatient);
        dest.writeValue(actorDevice);
        dest.writeValue(actorRelatedPerson);
        dest.writeValue(status);
        dest.writeValue(actorLocation);
        dest.writeValue(actorPractitioner);
        dest.writeValue(required);
        dest.writeValue(actorHealthcareService);
        dest.writeList(type);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public Participant createFromParcel(Parcel in) {
            return new Participant(in);
        }

        public Participant[] newArray(int size) {
            return new Participant[size];
        }
    };

    // "De-parcel object
    public Participant(Parcel in) {
        actorPatient = (ActorPatient) in.readValue(ActorPatient.class.getClassLoader());
        actorDevice = in.readValue(Object.class.getClassLoader());
        actorRelatedPerson = in.readValue(Object.class.getClassLoader());
        status = in.readValue(Object.class.getClassLoader());
        actorLocation = in.readValue(Object.class.getClassLoader());
        actorPractitioner = in.readValue(Object.class.getClassLoader());
        required = in.readValue(Object.class.getClassLoader());
        actorHealthcareService = (ActorHealthcareService) in.readValue(ActorHealthcareService.class.getClassLoader());
        type = in.readArrayList(Type.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/


}
