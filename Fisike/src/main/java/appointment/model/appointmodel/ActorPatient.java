package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 3/22/2016.
 */
public class ActorPatient implements Parcelable {


    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("ref")
    @Expose
    private String ref;

    @SerializedName("photo")
    @Expose
    private String photo;


    @SerializedName("class")
    @Expose
    private String class_name = "com.mphrx.consus.resources.Patient";


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }





        /*[    Parcelable code here   ]*/

    public ActorPatient() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(ref);
        dest.writeString(photo);
        dest.writeString(class_name);
    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public ActorPatient createFromParcel(Parcel in) {
            return new ActorPatient(in);
        }

        public ActorPatient[] newArray(int size) {
            return new ActorPatient[size];
        }
    };

    // "De-parcel object
    public ActorPatient(Parcel in) {
        id = in.readInt();
        ref = in.readString();
        photo = in.readString();
        class_name = in.readString();
    }

/*[    End of Parcelable code here   ]*/


}
