package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class Value implements Parcelable {


    /*[.........  Audit Fields.........]*/
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("actionBy")
    @Expose
    private String actionBy;
    @SerializedName("actionComment")
    @Expose
    private String actionComment;
    @SerializedName("actionReason")
    @Expose
    private String actionReason;
    @SerializedName("actionOn")
    @Expose
    private String actionOn;

    /*[......... End of Audit Fields.........]*/


    /*[.........  SampleCollectionLocation Fields.........]*/
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("addr")
    @Expose
    private String addr;
    @SerializedName("city")
    @Expose
    private String city;
    /*[......... End of SampleCollectionLocation Fields.........]*/


    /*[.........  appointmentDetails Fields.........]*/
    @SerializedName("useAppointmentDetailsFromExtension")
    @Expose
    private String useAppointmentDetailsFromExtension;
    @SerializedName("testName")
    @Expose
    private String testName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("instruction")
    @Expose
    private String instruction;
    @SerializedName("locationDetails")
    @Expose
    private String locationDetails;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("cost")
    @Expose
    private String cost;

    @SerializedName("practitionerName")
    @Expose
    private String practitionerName;

    @SerializedName("isBookable")
    @Expose
    private Boolean isBookable;

    @SerializedName("specialityName")
    @Expose
    private String specialityName;

    @SerializedName("practitionerId")
    @Expose
    private String practitionerId;

    @SerializedName("characteristic")
    @Expose
    private java.util.List<Characteristic> characteristic = new ArrayList<Characteristic>();

    @SerializedName("timeZoneId")
    @Expose
    private String timeZoneId;

    @SerializedName("phoneNo")
    @Expose
    private String phoneNo;

    @SerializedName("email")
    @Expose
    private String email;

//    phloober specific fields starts

    @SerializedName("selectedAddressZipCode")
    @Expose
    private String selectedAddressZipCode;

    @SerializedName("selectedAddress")
    @Expose
    private String selectedAddress;

    @SerializedName("subjectName")
    @Expose
    private String subjectName;

    @SerializedName("practitionerFirstName")
    @Expose
    private String practitionerFirstName;

    @SerializedName("practitionerLastName")
    @Expose
    private String practitionerLastName;

    @SerializedName("practitionerLongitude")
    @Expose
    private String practitionerLongitude;

    @SerializedName("practitionerLatitude")
    @Expose
    private String practitionerLatitude;

    @SerializedName("practitionerPhoneNo")
    @Expose
    private String practitionerPhoneNo;

    @SerializedName("referringPhysicianName")
    @Expose
    private String referringPhysicianName;

    @SerializedName("referringPhysicianReferenceId")
    @Expose
    private String referringPhysicianReferenceId;

    @SerializedName("scripts")
    @Expose
    private java.util.List<Scripts> scripts = new ArrayList<Scripts>();


    public List<Scripts> getScripts() {
        return scripts;
    }

    public void setScripts(List<Scripts> scripts) {
        this.scripts = scripts;
    }


    public String getSelectedAddressZipCode() {
        return selectedAddressZipCode;
    }

    public void setSelectedAddressZipCode(String selectedAddressZipCode) {
        this.selectedAddressZipCode = selectedAddressZipCode;
    }

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getPractitionerFirstName() {
        return practitionerFirstName;
    }

    public void setPractitionerFirstName(String practitionerFirstName) {
        this.practitionerFirstName = practitionerFirstName;
    }

    public String getPractitionerLastName() {
        return practitionerLastName;
    }

    public void setPractitionerLastName(String practitionerLastName) {
        this.practitionerLastName = practitionerLastName;
    }

    public String getPractitionerLongitude() {
        return practitionerLongitude;
    }

    public void setPractitionerLongitude(String practitionerLongitude) {
        this.practitionerLongitude = practitionerLongitude;
    }

    public String getPractitionerLatitude() {
        return practitionerLatitude;
    }

    public void setPractitionerLatitude(String practitionerLatitude) {
        this.practitionerLatitude = practitionerLatitude;
    }

    public String getPractitionerPhoneNo() {
        return practitionerPhoneNo;
    }

    public void setPractitionerPhoneNo(String practitionerPhoneNo) {
        this.practitionerPhoneNo = practitionerPhoneNo;
    }

    public String getReferringPhysicianName() {
        return referringPhysicianName;
    }

    public void setReferringPhysicianName(String referringPhysicianName) {
        this.referringPhysicianName = referringPhysicianName;
    }

    public String getReferringPhysicianReferenceId() {
        return referringPhysicianReferenceId;
    }

    public void setReferringPhysicianReferenceId(String referringPhysicianReferenceId) {
        this.referringPhysicianReferenceId = referringPhysicianReferenceId;
    }




    /*[......... End of appointmentDetails Fields.........]*/


    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The actionBy
     */
    public String getActionBy() {
        return actionBy;
    }

    /**
     * @param actionBy The actionBy
     */
    public void setActionBy(String actionBy) {
        this.actionBy = actionBy;
    }

    /**
     * @return The actionComment
     */
    public String getActionComment() {
        return actionComment;
    }

    /**
     * @param actionComment The actionComment
     */
    public void setActionComment(String actionComment) {
        this.actionComment = actionComment;
    }

    /**
     * @return The actionReason
     */
    public String getActionReason() {
        return actionReason;
    }

    /**
     * @param actionReason The actionReason
     */
    public void setActionReason(String actionReason) {
        this.actionReason = actionReason;
    }

    /**
     * @return The actionOn
     */
    public String getActionOn() {
        return actionOn;
    }

    /**
     * @param actionOn The actionOn
     */
    public void setActionOn(String actionOn) {
        this.actionOn = actionOn;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUseAppointmentDetailsFromExtension() {
        return useAppointmentDetailsFromExtension;
    }

    public void setUseAppointmentDetailsFromExtension(String useAppointmentDetailsFromExtension) {
        this.useAppointmentDetailsFromExtension = useAppointmentDetailsFromExtension;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getLocationDetails() {
        return locationDetails;
    }

    public void setLocationDetails(String locationDetails) {
        this.locationDetails = locationDetails;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPractitionerName() {
        return practitionerName;
    }

    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public Boolean getBookable() {
        return isBookable;
    }

    public void setBookable(Boolean bookable) {
        isBookable = bookable;
    }

    public String getPractitionerId() {
        return practitionerId;
    }

    public void setPractitionerId(String practitionerId) {
        this.practitionerId = practitionerId;
    }

    public java.util.List<Characteristic> getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(List<Characteristic> characteristic) {
        this.characteristic = characteristic;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

/*[    Parcelable code here   ]*/

    public Value() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(actionBy);
        dest.writeString(actionComment);
        dest.writeString(actionReason);
        dest.writeString(actionOn);
        dest.writeString(zip);
        dest.writeString(state);
        dest.writeString(addr);
        dest.writeString(city);
        dest.writeString(useAppointmentDetailsFromExtension);
        dest.writeString(testName);
        dest.writeString(description);
        dest.writeString(instruction);
        dest.writeString(locationDetails);
        dest.writeString(serviceName);
        dest.writeString(cost);
        dest.writeString(practitionerName);
        dest.writeValue(isBookable);
        dest.writeString(specialityName);
        dest.writeString(practitionerId);
        dest.writeList(characteristic);
        dest.writeString(timeZoneId);
        dest.writeString(phoneNo);
        dest.writeString(email);


        dest.writeString(selectedAddressZipCode);
        dest.writeString(selectedAddress);
        dest.writeString(practitionerFirstName);
        dest.writeString(practitionerLastName);
        dest.writeString(practitionerLatitude);
        dest.writeString(practitionerLongitude);
        dest.writeString(referringPhysicianName);
        dest.writeString(referringPhysicianReferenceId);
        dest.writeString(practitionerPhoneNo);
        dest.writeString(subjectName);
        dest.writeList(scripts);

        //  dest.writeList(list_CustomerHiredHelpModels);

    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public Value createFromParcel(Parcel in) {
            return new Value(in);
        }

        public Value[] newArray(int size) {
            return new Value[size];
        }
    };

    // "De-parcel object
    public Value(Parcel in) {
        status = in.readString();
        actionBy = in.readString();
        actionComment = in.readString();
        actionReason = in.readString();
        actionOn = in.readString();
        zip = in.readString();
        state = in.readString();
        addr = in.readString();
        city = in.readString();
        useAppointmentDetailsFromExtension = in.readString();
        testName = in.readString();
        description = in.readString();
        instruction = in.readString();
        locationDetails = in.readString();
        serviceName = in.readString();
        cost = in.readString();
        practitionerName = in.readString();
        isBookable = (Boolean) in.readValue(getClass().getClassLoader());
        specialityName = in.readString();
        practitionerId = in.readString();
        in.readList(characteristic, getClass().getClassLoader());
        timeZoneId = in.readString();
        phoneNo = in.readString();
        email = in.readString();

        selectedAddressZipCode = in.readString();
        selectedAddress = in.readString();
        practitionerFirstName = in.readString();
        practitionerLastName = in.readString();
        practitionerLatitude = in.readString();
        practitionerLongitude = in.readString();
        referringPhysicianName = in.readString();
        referringPhysicianReferenceId = in.readString();
        practitionerPhoneNo = in.readString();
        subjectName = in.readString();
        scripts = in.readArrayList(Scripts.class.getClassLoader());



        //  list_CustomerHiredHelpModels = in.readArrayList(CustomerHiredHelpModel.class.getClassLoader());
    }

/*[    End of Parcelable code here   ]*/

}