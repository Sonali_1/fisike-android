package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class List implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reason")
    @Expose
    private Object reason;
    @SerializedName("actionTriggerService")
    @Expose
    private Object actionTriggerService;
    @SerializedName("type")
    @Expose
    private AppointmentType type;

    @SerializedName("minutesDuration")
    @Expose
    private MinutesDuration minutesDuration;
    @SerializedName("resourceType")
    @Expose
    private String resourceType;
    @SerializedName("extension")
    @Expose
    private java.util.List<Extension> extension = new ArrayList<Extension>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("start")
    @Expose
    private Start start;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("priority")
    @Expose
    private Priority priority;
    @SerializedName("participant")
    @Expose
    private java.util.List<Participant> participant = new ArrayList<Participant>();
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("slot")
    @Expose
    private java.util.List<Object> slot = new ArrayList<Object>();
    @SerializedName("identifier")
    @Expose
    private java.util.List<Identifier> identifier = new ArrayList<Identifier>();
    @SerializedName("end")
    @Expose
    private End end;

    @SerializedName("lastUpdated")
    @Expose
    private String lastUpdated;



    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The reason
     */
    public Object getReason() {
        return reason;
    }

    /**
     * @param reason The reason
     */
    public void setReason(Object reason) {
        this.reason = reason;
    }

    /**
     * @return The actionTriggerService
     */
    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    /**
     * @param actionTriggerService The actionTriggerService
     */
    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    /**
     * @return The type
     */
    public AppointmentType getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(AppointmentType type) {
        this.type = type;
    }

    /**
     * @return The minutesDuration
     */
    public MinutesDuration getMinutesDuration() {
        return minutesDuration;
    }

    /**
     * @param minutesDuration The minutesDuration
     */
    public void setMinutesDuration(MinutesDuration minutesDuration) {
        this.minutesDuration = minutesDuration;
    }

    /**
     * @return The resourceType
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * @param resourceType The resourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * @return The extension
     */
    public java.util.List<Extension> getExtension() {
        return extension;
    }

    /**
     * @param extension The extension
     */
    public void setExtension(java.util.List<Extension> extension) {
        this.extension = extension;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The start
     */
    public Start getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(Start start) {
        this.start = start;
    }

    /**
     * @return The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     * @return The priority
     */
    public Priority getPriority() {
        return priority;
    }

    /**
     * @param priority The priority
     */
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    /**
     * @return The participant
     */
    public java.util.List<Participant> getParticipant() {
        return participant;
    }

    /**
     * @param participant The participant
     */
    public void setParticipant(java.util.List<Participant> participant) {
        this.participant = participant;
    }

    /**
     * @return The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * @return The slot
     */
    public java.util.List<Object> getSlot() {
        return slot;
    }

    /**
     * @param slot The slot
     */
    public void setSlot(java.util.List<Object> slot) {
        this.slot = slot;
    }

    /**
     * @return The identifier
     */
    public java.util.List<Identifier> getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier The identifier
     */
    public void setIdentifier(java.util.List<Identifier> identifier) {
        this.identifier = identifier;
    }

    /**
     * @return The end
     */
    public End getEnd() {
        return end;
    }

    /**
     * @param end The end
     */
    public void setEnd(End end) {
        this.end = end;
    }







        /*[    Parcelable code here   ]*/

    public List() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(status);
        dest.writeValue(reason);
        dest.writeValue(actionTriggerService);
        dest.writeValue(type);
        dest.writeValue(minutesDuration);
        dest.writeString(resourceType);
        dest.writeList(extension);
        dest.writeInt(id);
        dest.writeValue(start);
        dest.writeValue(description);
        dest.writeValue(priority);
        dest.writeList(participant);
        dest.writeValue(comment);
        dest.writeList(slot);
        dest.writeList(identifier);
        dest.writeValue(end);
        dest.writeString(lastUpdated);

    }


    // Creator
    public static final Creator CREATOR = new Creator() {
        public List createFromParcel(Parcel in) {
            return new List(in);
        }

        public List[] newArray(int size) {
            return new List[size];
        }
    };

    // "De-parcel object
    public List(Parcel in) {

        status = in.readString();
        reason = in.readValue(Object.class.getClassLoader());
        actionTriggerService = in.readValue(Object.class.getClassLoader());
        type = (AppointmentType) in.readValue(AppointmentType.class.getClassLoader());
        minutesDuration = (MinutesDuration) in.readValue(MinutesDuration.class.getClassLoader());
        resourceType = in.readString();
        in.readList(extension, getClass().getClassLoader());
        id = in.readInt();
        start = (Start) in.readValue(Start.class.getClassLoader());
        description = in.readValue(Object.class.getClassLoader());
        priority = (Priority) in.readValue(Priority.class.getClassLoader());
        participant = in.readArrayList(Participant.class.getClassLoader());
        comment = in.readValue(Object.class.getClassLoader());
        slot = in.readArrayList(Object.class.getClassLoader());
        identifier = in.readArrayList(Identifier.class.getClassLoader());
        end = (End) in.readValue(End.class.getClassLoader());
        lastUpdated = in.readString();
    }

/*[    End of Parcelable code here   ]*/

}