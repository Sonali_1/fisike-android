package appointment.model.appointmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by raj on 17/07/17.
 */
public class Scripts implements Parcelable {


    @SerializedName("scriptImageThumbnail")
    @Expose
    private String scriptImageThumbnail;

    @SerializedName("scriptImageMimeType")
    @Expose
    private String scriptImageMimeType;

//    @SerializedName("scriptImage")
//    @Expose
//    private String scriptImage;


    public Scripts()
    {

    }


    public String getScriptImageThumbnail() {
        return scriptImageThumbnail;
    }

    public void setScriptImageThumbnail(String scriptImageThumbnail) {
        this.scriptImageThumbnail = scriptImageThumbnail;
    }

    public String getScriptImageMimeType() {
        return scriptImageMimeType;
    }

    public void setScriptImageMimeType(String scriptImageMimeType) {
        this.scriptImageMimeType = scriptImageMimeType;
    }

//    public String getScriptImage() {
//        return scriptImage;
//    }
//
//    public void setScriptImage(String scriptImage) {
//        this.scriptImage = scriptImage;
//    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(scriptImageThumbnail);
        dest.writeString(scriptImageMimeType);
//        dest.writeString(scriptImage);

    }


    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Scripts createFromParcel(Parcel in) {
            return new Scripts(in);
        }

        public Scripts[] newArray(int size) {
            return new Scripts[size];
        }
    };

    // "De-parcel object
    public Scripts(Parcel in) {

        scriptImageThumbnail = in.readString();
        scriptImageMimeType = in.readString();
//        scriptImage = in.readString();

    }

/*[    End of Parcelable code here   ]*/

}

