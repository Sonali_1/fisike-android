package appointment.model;

import com.mphrx.fisike.mo.PatientMO;

import java.util.ArrayList;
import java.util.List;

import appointment.model.HomesampleModel.TestsDataModel;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.TestForAppointment;

/**
 * Created by Aastha on 24/02/2016.
 */
public class OrderSummary {

    private static OrderSummary myOrderSummary;

    /*[   Test OrderSummary data  variables ]*/
    private List<TestForAppointment> orderist = new ArrayList<TestForAppointment>();
    private List<AppointmentModel> appointmentModelOrderList = new ArrayList<AppointmentModel>();
    private List<String> hcsIdList = new ArrayList<>();
    private String date;
    private String hour;
    private String minute;
    private int testNumber = 0;
    private int totalPrice = 0;

    private long patientId;
    private String patientName;
    private String patientAddress;
    private String patientMobile;
    private String time;
    private String doctorName = "";
    private String doctorSpeciality = "";
    private int doctorFee;
    private String doctorLocationName = "";
    private String doctorLocationAddress = "";
    private String doctorContact = "";
    private String discounted = "950";
    private String testType;
    private String emailAddress;
    private String practionerProfilePic;
    private String gender;
    private String comment;
    private String reason;
    private String dateOfBirth;
    private String serviceNames;
    private String speciality;
    private String location1;
    private String location2;
    private String location3;
    private ArrayList<String> bookingReason = new ArrayList<>();
    private ArrayList<String> cancelReason = new ArrayList<>();
    private ArrayList<String> rescheduleReason = new ArrayList<>();

    public PatientMO getPatientMO() {
        return patientMO;
    }

    public void setPatientMO(PatientMO patientMO) {
        this.patientMO = patientMO;
    }

    private PatientMO patientMO;

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public static OrderSummary getInstance() {
        if (myOrderSummary == null) {
            myOrderSummary = new OrderSummary();
        }
        return myOrderSummary;
    }

    public static void setInstance() {
        if (myOrderSummary != null) {
            myOrderSummary = null;
        }
    }

    public String getPractionerProfilePic() {
        return practionerProfilePic;
    }

    public void setPractionerProfilePic(String practionerProfilePic) {
        this.practionerProfilePic = practionerProfilePic;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public int getDoctorFee() {
        return doctorFee;
    }

    public void setDoctorFee(int doctorFee) {
        this.doctorFee = doctorFee;
    }

    public String getDoctorLocationName() {
        return doctorLocationName;
    }

    public void setDoctorLocationName(String doctorLocationName) {
        this.doctorLocationName = doctorLocationName;
    }

    public String getDoctorLocationAddress() {
        return doctorLocationAddress;
    }

    public void setDoctorLocationAddress(String doctorLocationAddress) {
        this.doctorLocationAddress = doctorLocationAddress;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctorContact() {
        return doctorContact;
    }

    public void setDoctorContact(String doctorContact) {
        this.doctorContact = doctorContact;
    }


    public String getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public String getDiscounted() {
        return discounted;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }



    /*[   End of Test OrderSummary data  variables ]*/


    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientMobile() {
        return patientMobile;
    }

    public void setPatientMobile(String patientMobile) {
        this.patientMobile = patientMobile;
    }

    public List<TestForAppointment> getOrderist() {
        if(orderist == null){
            orderist = new ArrayList<TestForAppointment>();
        }
        return orderist;
    }

    public void setOrderist(List<TestForAppointment> orderist) {
        this.orderist = orderist;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public int getTestNumber() {
        return testNumber;
    }

    public void setTestNumber(int testNumber) {
        this.testNumber = testNumber;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLocation1() {
        return location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getServiceNames() {
        return serviceNames;
    }

    public void setServiceNames(String serviceNames) {
        this.serviceNames = serviceNames;
    }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getLocation3() {
        return location3;
    }

    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    public void clearOrderSummary() {
        this.orderist.clear();
        this.testNumber = 0;
        this.totalPrice = 0;
    }

    public ArrayList<String> getBookingReason() {
        return bookingReason;
    }

    public void setBookingReason(ArrayList<String> bookingReason) {
        this.bookingReason = bookingReason;
    }

    public ArrayList<String> getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(ArrayList<String> cancelReason) {
        this.cancelReason = cancelReason;
    }

    public ArrayList<String> getRescheduleReason() {
        return rescheduleReason;
    }

    public void setRescheduleReason(ArrayList<String> rescheduleReason) {
        this.rescheduleReason = rescheduleReason;
    }

    public List<AppointmentModel> getAppointmentModelOrderList() {
        if(appointmentModelOrderList == null){
            appointmentModelOrderList = new ArrayList<AppointmentModel>();
        }
        return appointmentModelOrderList;
    }

    public void setAppointmentModelOrderList(List<AppointmentModel> appointmentModelOrderList) {
        this.appointmentModelOrderList = appointmentModelOrderList;
    }

    public List<String> getHcsIdList() {
        return hcsIdList;
    }

    public void setHcsIdList(List<String> hcsIdList) {
        this.hcsIdList = hcsIdList;
    }
}
