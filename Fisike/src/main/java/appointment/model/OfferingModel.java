package appointment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jivesoftware.smackx.workgroup.agent.OfferConfirmation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LENOVO on 11/14/2016.
 */
public class OfferingModel implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("hasChild")
    @Expose
    private Boolean hasChild;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("resourceId")
    @Expose
    private int resourceId;
    @SerializedName("child")
    @Expose
    private List<OfferingModel> child = new ArrayList<OfferingModel>();


    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The hasChild
     */
    public Boolean getHasChild() {
        return hasChild;
    }

    /**
     * @param hasChild The hasChild
     */
    public void setHasChild(Boolean hasChild) {
        this.hasChild = hasChild;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The isActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The child
     */
    public List<OfferingModel> getChild() {
        return child;
    }

    /**
     * @param child The child
     */
    public void setChild(List<OfferingModel> child) {
        this.child = child;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

/*  [ Parcealable code ]*/

    public OfferingModel() {
        //default constructor
    }

    protected OfferingModel(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<OfferingModel> CREATOR = new Parcelable.Creator<OfferingModel>() {
        @Override
        public OfferingModel createFromParcel(Parcel in) {
            return new OfferingModel(in);
        }

        @Override
        public OfferingModel[] newArray(int size) {
            return new OfferingModel[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        title = in.readString();
        description = in.readString();
        isActive = (Boolean) in.readValue(getClass().getClassLoader());
        hasChild = (Boolean) in.readValue(getClass().getClassLoader());
        id = in.readString();
        resourceId = in.readInt();
        in.readList(child, getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeValue(isActive);
        dest.writeValue(hasChild);
        dest.writeString(id);
        dest.writeInt(resourceId);
        dest.writeList(child);
    }
     /*  [ End of Parcealable code ]*/
}

