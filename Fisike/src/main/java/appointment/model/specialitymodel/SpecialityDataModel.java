package appointment.model.specialitymodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import appointment.model.appointmodel.Start;

/**
 * Created by laxmansingh on 3/30/2016.
 */
public class SpecialityDataModel implements Parcelable {

    @SerializedName("serviceTypeName")
    @Expose
    private String serviceTypeName;

    @SerializedName("serviceTypeId")
    @Expose
    private String serviceTypeId;


    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }






        /*[    Parcelable code here   ]*/

    public SpecialityDataModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceTypeName);
        dest.writeString(serviceTypeId);
    }


    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public SpecialityDataModel createFromParcel(Parcel in) {
            return new SpecialityDataModel(in);
        }

        public SpecialityDataModel[] newArray(int size) {
            return new SpecialityDataModel[size];
        }
    };

    // "De-parcel object
    public SpecialityDataModel(Parcel in) {
        serviceTypeName = in.readString();
        serviceTypeId = in.readString();
    }

/*[    End of Parcelable code here   ]*/


}
