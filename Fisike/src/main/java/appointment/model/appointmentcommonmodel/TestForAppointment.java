package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ProgressBar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class TestForAppointment implements Parcelable {

    @SerializedName("testCompendiumId")
    @Expose
    private String testCompendiumId;
    @SerializedName("testName")
    @Expose
    private String testName;
    @SerializedName("testFullName")
    @Expose
    private String testFullName;
    @SerializedName("testDescription")
    @Expose
    private String testDescription;
    @SerializedName("testInstructions")
    @Expose
    private String testInstructions;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("hcsGroups")
    @Expose
    private List<HcsGroup> hcsGroups = new ArrayList<HcsGroup>();



    /**
     * @return The testCompendiumId
     */
    public String getTestCompendiumId() {
        return testCompendiumId;
    }

    /**
     * @param testCompendiumId The testCompendiumId
     */
    public void setTestCompendiumId(String testCompendiumId) {
        this.testCompendiumId = testCompendiumId;
    }

    /**
     * @return The testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * @param testName The testName
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }

    /**
     * @return The testFullName
     */
    public String getTestFullName() {
        return testFullName;
    }

    /**
     * @param testFullName The testFullName
     */
    public void setTestFullName(String testFullName) {
        this.testFullName = testFullName;
    }

    /**
     * @return The testDescription
     */
    public String getTestDescription() {
        return testDescription;
    }

    /**
     * @param testDescription The testDescription
     */
    public void setTestDescription(String testDescription) {
        this.testDescription = testDescription;
    }

    /**
     * @return The testInstructions
     */
    public String getTestInstructions() {
        return testInstructions;
    }

    /**
     * @param testInstructions The testInstructions
     */
    public void setTestInstructions(String testInstructions) {
        this.testInstructions = testInstructions;
    }

    /**
     * @return The price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return The hcsGroups
     */
    public List<HcsGroup> getHcsGroups() {
        return hcsGroups;
    }

    /**
     * @param hcsGroups The hcsGroups
     */
    public void setHcsGroups(List<HcsGroup> hcsGroups) {
        this.hcsGroups = hcsGroups;
    }










      /*  [ Parcealable code ]*/

    public TestForAppointment() {
        //default constructor
    }

    protected TestForAppointment(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<TestForAppointment> CREATOR = new Parcelable.Creator<TestForAppointment>() {
        @Override
        public TestForAppointment createFromParcel(Parcel in) {
            return new TestForAppointment(in);
        }

        @Override
        public TestForAppointment[] newArray(int size) {
            return new TestForAppointment[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        testCompendiumId = in.readString();
        testName = in.readString();
        testFullName = in.readString();
        testDescription = in.readString();
        testInstructions = in.readString();
        price = (Price) in.readValue(getClass().getClassLoader());
        in.readList(hcsGroups, getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(testCompendiumId);
        dest.writeString(testName);
        dest.writeString(testFullName);
        dest.writeString(testDescription);
        dest.writeString(testInstructions);
        dest.writeValue(price);
        dest.writeList(hcsGroups);
    }
     /*  [ End of Parcealable code ]*/


}
