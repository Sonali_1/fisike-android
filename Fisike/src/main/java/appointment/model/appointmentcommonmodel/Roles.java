package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class Roles implements Parcelable {
    @SerializedName("requestableRoles")
    @Expose
    private List<String> requestableRoles = new ArrayList<String>();
    @SerializedName("bookableRoles")
    @Expose
    private List<String> bookableRoles = new ArrayList<String>();

    /**
     * @return The requestableRoles
     */
    public List<String> getRequestableRoles() {
        return requestableRoles;
    }

    /**
     * @param requestableRoles The requestableRoles
     */
    public void setRequestableRoles(List<String> requestableRoles) {
        this.requestableRoles = requestableRoles;
    }

    /**
     * @return The bookableRoles
     */
    public List<String> getBookableRoles() {
        return bookableRoles;
    }

    /**
     * @param bookableRoles The bookableRoles
     */
    public void setBookableRoles(List<String> bookableRoles) {
        this.bookableRoles = bookableRoles;
    }





      /*  [ Parcealable code ]*/

    public Roles() {
        //default constructor
    }

    protected Roles(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Roles> CREATOR = new Parcelable.Creator<Roles>() {
        @Override
        public Roles createFromParcel(Parcel in) {
            return new Roles(in);
        }

        @Override
        public Roles[] newArray(int size) {
            return new Roles[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        in.readList(requestableRoles, getClass().getClassLoader());
        in.readList(bookableRoles, getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(requestableRoles);
        dest.writeList(bookableRoles);
    }
     /*  [ End of Parcealable code ]*/
}