package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.gson.profile.request.Role;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class HcsWithLocationList implements Parcelable {
    @SerializedName("departmentName")
    @Expose
    private String departmentName;

    @SerializedName("instructions")
    @Expose
    private String instructions;

    @SerializedName("departmentAddress")
    @Expose
    private String departmentAddress;

    @SerializedName("hcsName")
    @Expose
    private String hcsName;

    @SerializedName("locationName")
    @Expose
    private String locationName;

    @SerializedName("price")
    @Expose
    private Price price;

    @SerializedName("roles")
    @Expose
    private Roles roles;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("hcsId")
    @Expose
    private String hcsId;

    @SerializedName("locationAddress")
    @Expose
    private String locationAddress;
    @SerializedName("longitude")
    @Expose
    private Double longitude = 0.0d;
    @SerializedName("latitude")
    @Expose
    private Double latitude = 0.0d;
    @SerializedName("altitude")
    @Expose
    private Double altitude = 0.0d;


    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getHcsName() {
        return hcsName;
    }

    public void setHcsName(String hcsName) {
        this.hcsName = hcsName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHcsId() {
        return hcsId;
    }

    public void setHcsId(String hcsId) {
        this.hcsId = hcsId;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

/*  [ Parcealable code ]*/

    public HcsWithLocationList() {
        //default constructor
    }

    protected HcsWithLocationList(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<HcsWithLocationList> CREATOR = new Parcelable.Creator<HcsWithLocationList>() {
        @Override
        public HcsWithLocationList createFromParcel(Parcel in) {
            return new HcsWithLocationList(in);
        }

        @Override
        public HcsWithLocationList[] newArray(int size) {
            return new HcsWithLocationList[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        departmentName = in.readString();
        instructions = in.readString();
        departmentAddress = in.readString();
        hcsName = in.readString();
        locationName = in.readString();
        price = (Price) in.readValue(getClass().getClassLoader());
        roles = (Roles) in.readValue(getClass().getClassLoader());
        description = in.readString();
        hcsId = in.readString();
        locationAddress = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        altitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(departmentName);
        dest.writeString(instructions);
        dest.writeString(departmentAddress);
        dest.writeString(hcsName);
        dest.writeString(locationName);
        dest.writeValue(price);
        dest.writeValue(roles);
        dest.writeString(description);
        dest.writeString(hcsId);
        dest.writeString(locationAddress);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeDouble(altitude);
    }
     /*  [ End of Parcealable code ]*/
}
