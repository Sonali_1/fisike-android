package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class Price implements Parcelable {

    @SerializedName("currentRate")
    @Expose
    private String currentRate;
    @SerializedName("originalRate")
    @Expose
    private String originalRate;
    @SerializedName("currency")
    @Expose
    private String currency;


    /**
     * @return The currentRate
     */
    public String getCurrentRate() {
        return currentRate;
    }

    /**
     * @param currentRate The currentRate
     */
    public void setCurrentRate(String currentRate) {
        this.currentRate = currentRate;
    }

    /**
     * @return The originalRate
     */
    public String getOriginalRate() {
        return originalRate;
    }

    /**
     * @param originalRate The originalRate
     */
    public void setOriginalRate(String originalRate) {
        this.originalRate = originalRate;
    }

    /**
     * @return The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency The currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }






      /*  [ Parcealable code ]*/

    public Price() {
        //default constructor
    }

    protected Price(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<Price> CREATOR = new Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel in) {
            return new Price(in);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        currentRate = in.readString();
        originalRate = in.readString();
        currency = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentRate);
        dest.writeString(originalRate);
        dest.writeString(currency);
    }
     /*  [ End of Parcealable code ]*/

}