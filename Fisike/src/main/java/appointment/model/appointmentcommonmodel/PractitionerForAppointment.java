package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class PractitionerForAppointment implements Parcelable {

    @SerializedName("practitionerId")
    @Expose
    private String practitionerId;
    @SerializedName("practitionerName")
    @Expose
    private String practitionerName;
    @SerializedName("specialty")
    @Expose
    private String specialty;
    @SerializedName("hcsGroups")
    @Expose
    private List<HcsGroup> hcsGroups = new ArrayList<HcsGroup>();

    private String practionerPic ; //= "http://jerrysusa.com/wp-content/uploads/2014/11/doctor-profile-02.jpg";

    /**
     * @return The practitionerId
     */
    public String getPractitionerId() {
        return practitionerId;
    }

    /**
     * @param practitionerId The practitionerId
     */
    public void setPractitionerId(String practitionerId) {
        this.practitionerId = practitionerId;
    }

    /**
     * @return The practitionerName
     */
    public String getPractitionerName() {
        return practitionerName;
    }

    /**
     * @param practitionerName The practitionerName
     */
    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    /**
     * @return The specialty
     */
    public String getSpecialty() {
        return specialty;
    }

    /**
     * @param specialty The specialty
     */
    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    /**
     * @return The hcsGroups
     */
    public List<HcsGroup> getHcsGroups() {
        return hcsGroups;
    }

    /**
     * @param hcsGroups The hcsGroups
     */
    public void setHcsGroups(List<HcsGroup> hcsGroups) {
        this.hcsGroups = hcsGroups;
    }

    public String getPractionerPic() {
        return practionerPic;
    }

    public void setPractionerPic(String practionerPic) {
        this.practionerPic = practionerPic;
    }

    /*  [ Parcealable code ]*/

    public PractitionerForAppointment() {
        //default constructor
    }

    protected PractitionerForAppointment(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<PractitionerForAppointment> CREATOR = new Parcelable.Creator<PractitionerForAppointment>() {
        @Override
        public PractitionerForAppointment createFromParcel(Parcel in) {
            return new PractitionerForAppointment(in);
        }

        @Override
        public PractitionerForAppointment[] newArray(int size) {
            return new PractitionerForAppointment[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        practitionerId = in.readString();
        practitionerName = in.readString();
        specialty = in.readString();
        in.readList(hcsGroups, getClass().getClassLoader());
        practionerPic = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(practitionerId);
        dest.writeString(practitionerName);
        dest.writeString(specialty);
        dest.writeList(hcsGroups);
        dest.writeString(practionerPic);
    }
     /*  [ End of Parcealable code ]*/

}