package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class AppointmentModel implements Parcelable {

    @SerializedName("practitionerForAppointment")
    @Expose
    private PractitionerForAppointment practitionerForAppointment;
    @SerializedName("testForAppointment")
    @Expose
    private TestForAppointment testForAppointment;
    @SerializedName("specialtyForAppointment")
    @Expose
    private SpecialtyForAppointment specialtyForAppointment;


    /**
     * @return The practitionerForAppointment
     */
    public PractitionerForAppointment getPractitionerForAppointment() {
        return practitionerForAppointment;
    }

    /**
     * @param practitionerForAppointment The practitionerForAppointment
     */
    public void setPractitionerForAppointment(PractitionerForAppointment practitionerForAppointment) {
        this.practitionerForAppointment = practitionerForAppointment;
    }

    /**
     * @return The testForAppointment
     */
    public TestForAppointment getTestForAppointment() {
        return testForAppointment;
    }

    /**
     * @param testForAppointment The testForAppointment
     */
    public void setTestForAppointment(TestForAppointment testForAppointment) {
        this.testForAppointment = testForAppointment;
    }

    public SpecialtyForAppointment getSpecialtyForAppointment() {
        return specialtyForAppointment;
    }

    public void setSpecialtyForAppointment(SpecialtyForAppointment specialtyForAppointment) {
        this.specialtyForAppointment = specialtyForAppointment;
    }

    /*  [ Parcealable code ]*/

    public AppointmentModel() {
        //default constructor
    }

    protected AppointmentModel(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<AppointmentModel> CREATOR = new Creator<AppointmentModel>() {
        @Override
        public AppointmentModel createFromParcel(Parcel in) {
            return new AppointmentModel(in);
        }

        @Override
        public AppointmentModel[] newArray(int size) {
            return new AppointmentModel[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }


    private void readFromParcel(Parcel in) {
        practitionerForAppointment = in.readParcelable(getClass().getClassLoader());
        testForAppointment = in.readParcelable(getClass().getClassLoader());
        specialtyForAppointment = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) practitionerForAppointment, flags);
        dest.writeParcelable((Parcelable) testForAppointment, flags);
        dest.writeParcelable((Parcelable) specialtyForAppointment, flags);
    }
     /*  [ End of Parcealable code ]*/
}
