package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class PractitionerWithLocationList implements Parcelable {

    @SerializedName("departmentName")
    @Expose
    private String departmentName;

    @SerializedName("practitionerName")
    @Expose
    private String practitionerName;

    @SerializedName("departmentAddress")
    @Expose
    private String departmentAddress;

    @SerializedName("specialty")
    @Expose
    private String specialty;

    @SerializedName("locationName")
    @Expose
    private String locationName;

    @SerializedName("price")
    @Expose
    private Price price;

    @SerializedName("hcsId")
    @Expose
    private String hcsId;

    @SerializedName("locationAddres")
    @Expose
    private String locationAddres;

    @SerializedName("practitionerId")
    @Expose
    private String practitionerId;

    @SerializedName("roles")
    @Expose
    private Roles roles;

    @SerializedName("longitude")
    @Expose
    private Double longitude = 0.0d;
    @SerializedName("latitude")
    @Expose
    private Double latitude = 0.0d;
    @SerializedName("altitude")
    @Expose
    private Double altitude = 0.0d;


    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getPractitionerName() {
        return practitionerName;
    }

    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getHcsId() {
        return hcsId;
    }

    public void setHcsId(String hcsId) {
        this.hcsId = hcsId;
    }

    public String getLocationAddres() {
        return locationAddres;
    }

    public void setLocationAddres(String locationAddres) {
        this.locationAddres = locationAddres;
    }

    public String getPractitionerId() {
        return practitionerId;
    }

    public void setPractitionerId(String practitionerId) {
        this.practitionerId = practitionerId;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }


/*  [ Parcealable code ]*/

    public PractitionerWithLocationList() {
        //default constructor
    }

    protected PractitionerWithLocationList(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<PractitionerWithLocationList> CREATOR = new Parcelable.Creator<PractitionerWithLocationList>() {
        @Override
        public PractitionerWithLocationList createFromParcel(Parcel in) {
            return new PractitionerWithLocationList(in);
        }

        @Override
        public PractitionerWithLocationList[] newArray(int size) {
            return new PractitionerWithLocationList[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        departmentName = in.readString();
        practitionerName = in.readString();
        departmentAddress = in.readString();
        specialty = in.readString();
        locationName = in.readString();
        price = (Price) in.readValue(getClass().getClassLoader());
        hcsId = in.readString();
        locationAddres = in.readString();
        practitionerId = in.readString();
        roles = (Roles) in.readValue(getClass().getClassLoader());
        longitude = in.readDouble();
        latitude = in.readDouble();
        altitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(departmentName);
        dest.writeString(practitionerName);
        dest.writeString(departmentAddress);
        dest.writeString(specialty);
        dest.writeString(locationName);
        dest.writeValue(price);
        dest.writeString(hcsId);
        dest.writeString(locationAddres);
        dest.writeString(practitionerId);
        dest.writeValue(roles);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeDouble(altitude);
    }
     /*  [ End of Parcealable code ]*/
}
