package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class GroupPrice implements Parcelable {

    @SerializedName("currentRate")
    @Expose
    private String currentRate;

    /**
     * @return The currentRate
     */
    public String getCurrentRate() {
        return currentRate;
    }

    /**
     * @param currentRate The currentRate
     */
    public void setCurrentRate(String currentRate) {
        this.currentRate = currentRate;
    }









      /*  [ Parcealable code ]*/

    public GroupPrice() {
        //default constructor
    }

    protected GroupPrice(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<GroupPrice> CREATOR = new Parcelable.Creator<GroupPrice>() {
        @Override
        public GroupPrice createFromParcel(Parcel in) {
            return new GroupPrice(in);
        }

        @Override
        public GroupPrice[] newArray(int size) {
            return new GroupPrice[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }


    private void readFromParcel(Parcel in) {
        currentRate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentRate);
    }
     /*  [ End of Parcealable code ]*/
}
