package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class MoPractitionerInfoList implements Parcelable {

    @SerializedName("practitionerName")
    @Expose
    private String practitionerName;

    @SerializedName("practitionerWithLocationList")
    @Expose
    private List<PractitionerWithLocationList> practitionerWithLocationList = new ArrayList<PractitionerWithLocationList>();

    @SerializedName("specialty")
    @Expose
    private String specialty;

    @SerializedName("price")
    @Expose
    private Price price;

    @SerializedName("practitionerId")
    @Expose
    private String practitionerId;

    private String practionerPic;    //= "http://jerrysusa.com/wp-content/uploads/2014/11/doctor-profile-02.jpg";


    public String getPractitionerName() {
        return practitionerName;
    }

    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    public List<PractitionerWithLocationList> getPractitionerWithLocationList() {
        return practitionerWithLocationList;
    }

    public void setPractitionerWithLocationList(List<PractitionerWithLocationList> practitionerWithLocationList) {
        this.practitionerWithLocationList = practitionerWithLocationList;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getPractitionerId() {
        return practitionerId;
    }

    public void setPractitionerId(String practitionerId) {
        this.practitionerId = practitionerId;
    }

    public String getPractionerPic() {
        return practionerPic;
    }

    public void setPractionerPic(String practionerPic) {
        this.practionerPic = practionerPic;
    }

/*  [ Parcealable code ]*/

    public MoPractitionerInfoList() {
        //default constructor
    }

    protected MoPractitionerInfoList(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<MoPractitionerInfoList> CREATOR = new Parcelable.Creator<MoPractitionerInfoList>() {
        @Override
        public MoPractitionerInfoList createFromParcel(Parcel in) {
            return new MoPractitionerInfoList(in);
        }

        @Override
        public MoPractitionerInfoList[] newArray(int size) {
            return new MoPractitionerInfoList[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        practitionerName = in.readString();
        in.readList(practitionerWithLocationList, getClass().getClassLoader());
        specialty = in.readString();
        price = (Price) in.readValue(getClass().getClassLoader());
        practitionerId = in.readString();
        practionerPic = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(practitionerName);
        dest.writeList(practitionerWithLocationList);
        dest.writeString(specialty);
        dest.writeValue(price);
        dest.writeString(practitionerId);
        dest.writeString(practionerPic);
    }
     /*  [ End of Parcealable code ]*/
}
