package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class HcsGroup implements Parcelable {

    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("groupDescription")
    @Expose
    private String groupDescription;
    @SerializedName("groupInstructions")
    @Expose
    private String groupInstructions;
    @SerializedName("groupPrice")
    @Expose
    private GroupPrice groupPrice;
    @SerializedName("moHcsList")
    @Expose
    private List<MoHcsList> moHcsList = new ArrayList<MoHcsList>();

    /**
     * @return The serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName The serviceName
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return The groupDescription
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * @param groupDescription The groupDescription
     */
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * @return The groupInstructions
     */
    public String getGroupInstructions() {
        return groupInstructions;
    }

    /**
     * @param groupInstructions The groupInstructions
     */
    public void setGroupInstructions(String groupInstructions) {
        this.groupInstructions = groupInstructions;
    }

    /**
     * @return The groupPrice
     */
    public GroupPrice getGroupPrice() {
        return groupPrice;
    }

    /**
     * @param groupPrice The groupPrice
     */
    public void setGroupPrice(GroupPrice groupPrice) {
        this.groupPrice = groupPrice;
    }

    /**
     * @return The moHcsList
     */
    public List<MoHcsList> getMoHcsList() {
        return moHcsList;
    }

    /**
     * @param moHcsList The moHcsList
     */
    public void setMoHcsList(List<MoHcsList> moHcsList) {
        this.moHcsList = moHcsList;
    }











      /*  [ Parcealable code ]*/

    public HcsGroup() {
        //default constructor
    }

    protected HcsGroup(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<HcsGroup> CREATOR = new Parcelable.Creator<HcsGroup>() {
        @Override
        public HcsGroup createFromParcel(Parcel in) {
            return new HcsGroup(in);
        }

        @Override
        public HcsGroup[] newArray(int size) {
            return new HcsGroup[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        serviceName = in.readString();
        groupDescription = in.readString();
        groupInstructions = in.readString();
        groupPrice = (GroupPrice) in.readValue(getClass().getClassLoader());
        in.readList(moHcsList, getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceName);
        dest.writeString(groupDescription);
        dest.writeString(groupInstructions);
        dest.writeValue(groupPrice);
        dest.writeList(moHcsList);
    }
     /*  [ End of Parcealable code ]*/


}