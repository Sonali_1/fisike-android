package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class SpecialtyForAppointment implements Parcelable {

    @SerializedName("specialtyName")
    @Expose
    private String specialtyName;

    @SerializedName("practitionerForAppointment")
    @Expose
    private PractitionerForAppointment practitionerForAppointment;

    @SerializedName("serviceGroup")
    @Expose
    private ServiceGroup serviceGroup;

    @SerializedName("sericeTypeId")
    @Expose
    private String sericeTypeId;


    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public PractitionerForAppointment getPractitionerForAppointment() {
        return practitionerForAppointment;
    }

    public void setPractitionerForAppointment(PractitionerForAppointment practitionerForAppointment) {
        this.practitionerForAppointment = practitionerForAppointment;
    }

    public ServiceGroup getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(ServiceGroup serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public String getSericeTypeId() {
        return sericeTypeId;
    }

    public void setSericeTypeId(String sericeTypeId) {
        this.sericeTypeId = sericeTypeId;
    }

  /*  [ Parcealable code ]*/

    public SpecialtyForAppointment() {
        //default constructor
    }

    protected SpecialtyForAppointment(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<SpecialtyForAppointment> CREATOR = new Parcelable.Creator<SpecialtyForAppointment>() {
        @Override
        public SpecialtyForAppointment createFromParcel(Parcel in) {
            return new SpecialtyForAppointment(in);
        }

        @Override
        public SpecialtyForAppointment[] newArray(int size) {
            return new SpecialtyForAppointment[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        specialtyName = in.readString();
        practitionerForAppointment = (PractitionerForAppointment) in.readValue(getClass().getClassLoader());
        serviceGroup = (ServiceGroup) in.readValue(getClass().getClassLoader());
        sericeTypeId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(specialtyName);
        dest.writeValue(practitionerForAppointment);
        dest.writeValue(serviceGroup);
        dest.writeString(sericeTypeId);
    }
     /*  [ End of Parcealable code ]*/


}
