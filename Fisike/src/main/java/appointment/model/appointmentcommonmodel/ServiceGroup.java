package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class ServiceGroup implements Parcelable {

    @SerializedName("groupDescription")
    @Expose
    private String groupDescription;

    @SerializedName("groupInstructions")
    @Expose
    private String groupInstructions;

    @SerializedName("price")
    @Expose
    private Price price;

    @SerializedName("moPractitionerInfoList")
    @Expose
    private List<MoPractitionerInfoList> moPractitionerInfoList = new ArrayList<MoPractitionerInfoList>();

    @SerializedName("hcsWithLocationList")
    @Expose
    private List<HcsWithLocationList> hcsWithLocationList = new ArrayList<HcsWithLocationList>();

    @SerializedName("serviceName")
    @Expose
    private String serviceName;


    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupInstructions() {
        return groupInstructions;
    }

    public void setGroupInstructions(String groupInstructions) {
        this.groupInstructions = groupInstructions;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<MoPractitionerInfoList> getMoPractitionerInfoList() {
        return moPractitionerInfoList;
    }

    public void setMoPractitionerInfoList(List<MoPractitionerInfoList> moPractitionerInfoList) {
        this.moPractitionerInfoList = moPractitionerInfoList;
    }

    public List<HcsWithLocationList> getHcsWithLocationList() {
        return hcsWithLocationList;
    }

    public void setHcsWithLocationList(List<HcsWithLocationList> hcsWithLocationList) {
        this.hcsWithLocationList = hcsWithLocationList;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }







     /*  [ Parcealable code ]*/

    public ServiceGroup() {
        //default constructor
    }

    protected ServiceGroup(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<ServiceGroup> CREATOR = new Parcelable.Creator<ServiceGroup>() {
        @Override
        public ServiceGroup createFromParcel(Parcel in) {
            return new ServiceGroup(in);
        }

        @Override
        public ServiceGroup[] newArray(int size) {
            return new ServiceGroup[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        groupDescription = in.readString();
        groupInstructions = in.readString();
        price = (Price) in.readValue(getClass().getClassLoader());
        in.readList(moPractitionerInfoList, getClass().getClassLoader());
        in.readList(hcsWithLocationList, getClass().getClassLoader());
        serviceName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupDescription);
        dest.writeString(groupInstructions);
        dest.writeValue(price);
        dest.writeList(moPractitionerInfoList);
        dest.writeList(hcsWithLocationList);
        dest.writeString(serviceName);
    }
     /*  [ End of Parcealable code ]*/
}
