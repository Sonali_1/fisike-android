package appointment.model.appointmentcommonmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 11/9/2016.
 */

public class MoHcsList implements Parcelable {


    @SerializedName("hcsId")
    @Expose
    private String hcsId;
    @SerializedName("locationName")
    @Expose
    private String locationName;
    @SerializedName("locationAddress")
    @Expose
    private String locationAddress;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("instructions")
    @Expose
    private String instructions;
    @SerializedName("isBookable")
    @Expose
    private Boolean isBookable;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("roles")
    @Expose
    private Roles roles;
    @SerializedName("hcsName")
    @Expose
    private String hcsName;
    @SerializedName("departmentName")
    @Expose
    private String departmentName;
    @SerializedName("departmentAddress")
    @Expose
    private String departmentAddress;
    @SerializedName("longitude")
    @Expose
    private Double longitude = 0.0d;
    @SerializedName("latitude")
    @Expose
    private Double latitude = 0.0d;
    @SerializedName("altitude")
    @Expose
    private Double altitude = 0.0d;


    /**
     * @return The hcsId
     */
    public String getHcsId() {
        return hcsId;
    }

    /**
     * @param hcsId The hcsId
     */
    public void setHcsId(String hcsId) {
        this.hcsId = hcsId;
    }

    /**
     * @return The locationName
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * @param locationName The locationName
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * @return The locationAddress
     */
    public String getLocationAddress() {
        return locationAddress;
    }

    /**
     * @param locationAddress The locationAddress
     */
    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * @param instructions The instructions
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     * @return The isBookable
     */
    public Boolean getIsBookable() {
        return isBookable;
    }

    /**
     * @param isBookable The isBookable
     */
    public void setIsBookable(Boolean isBookable) {
        this.isBookable = isBookable;
    }

    /**
     * @return The price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return The roles
     */
    public Roles getRoles() {
        return roles;
    }

    /**
     * @param roles The roles
     */
    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    /**
     * @return The hcsName
     */
    public String getHcsName() {
        return hcsName;
    }

    /**
     * @param hcsName The hcsName
     */
    public void setHcsName(String hcsName) {
        this.hcsName = hcsName;
    }

    /**
     * @return The departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName The departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return The departmentAddress
     */
    public String getDepartmentAddress() {
        return departmentAddress;
    }

    /**
     * @param departmentAddress The departmentAddress
     */
    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

/*  [ Parcealable code ]*/

    public MoHcsList() {
        //default constructor
    }

    protected MoHcsList(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<MoHcsList> CREATOR = new Parcelable.Creator<MoHcsList>() {
        @Override
        public MoHcsList createFromParcel(Parcel in) {
            return new MoHcsList(in);
        }

        @Override
        public MoHcsList[] newArray(int size) {
            return new MoHcsList[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        hcsId = in.readString();
        locationName = in.readString();
        locationAddress = in.readString();
        description = in.readString();
        instructions = in.readString();
        isBookable = (Boolean) in.readValue(getClass().getClassLoader());
        price = (Price) in.readValue(getClass().getClassLoader());
        roles = (Roles) in.readValue(getClass().getClassLoader());
        hcsName = in.readString();
        departmentName = in.readString();
        departmentAddress = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        altitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(hcsId);
        dest.writeString(locationName);
        dest.writeString(locationAddress);
        dest.writeString(description);
        dest.writeString(instructions);
        dest.writeValue(isBookable);
        dest.writeValue(price);
        dest.writeValue(roles);
        dest.writeString(hcsName);
        dest.writeString(departmentName);
        dest.writeString(departmentAddress);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeDouble(altitude);
    }
     /*  [ End of Parcealable code ]*/


}