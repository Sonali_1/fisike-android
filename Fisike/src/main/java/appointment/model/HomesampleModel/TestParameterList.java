package appointment.model.HomesampleModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 7/20/2016.
 */
public class TestParameterList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("system")
    @Expose
    private String system;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("identifiers")
    @Expose
    private List<Identifier> identifiers = new ArrayList<Identifier>();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("referenceRange")
    @Expose
    private String referenceRange;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("otherNames")
    @Expose
    private List<String> otherNames = new ArrayList<String>();
    @SerializedName("version")
    @Expose
    private Integer version;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit The unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return The system
     */
    public String getSystem() {
        return system;
    }

    /**
     * @param system The system
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The identifiers
     */
    public List<Identifier> getIdentifiers() {
        return identifiers;
    }

    /**
     * @param identifiers The identifiers
     */
    public void setIdentifiers(List<Identifier> identifiers) {
        this.identifiers = identifiers;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The referenceRange
     */
    public String getReferenceRange() {
        return referenceRange;
    }

    /**
     * @param referenceRange The referenceRange
     */
    public void setReferenceRange(String referenceRange) {
        this.referenceRange = referenceRange;
    }

    /**
     * @return The fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The otherNames
     */
    public List<String> getOtherNames() {
        return otherNames;
    }

    /**
     * @param otherNames The otherNames
     */
    public void setOtherNames(List<String> otherNames) {
        this.otherNames = otherNames;
    }

    /**
     * @return The version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

}