package appointment.model.HomesampleModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 7/20/2016.
 */
public class TestsDataModel {


    private boolean isSelected = false;
    private boolean isExpanded = false;


    private int hcsId = 0;
    @SerializedName("testId")
    @Expose
    private String testId = "";

    @SerializedName("instructions")
    @Expose
    private String instructions = "";
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("description")
    @Expose
    private String description = "";

    @SerializedName("category")
    @Expose
    private String category;


    @SerializedName("price")
    @Expose
    private Price price;


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("testParameterList")
    @Expose
    private List<TestParameterList> testParameterList = new ArrayList<TestParameterList>();
    @SerializedName("popular")
    @Expose
    private Boolean popular;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastUpdated")
    @Expose
    private String lastUpdated;
    @SerializedName("dateCreated")
    @Expose
    private String dateCreated;

    @SerializedName("orderCodeSystemVersion")
    @Expose
    private String orderCodeSystemVersion;


    @SerializedName("isMaster")
    @Expose
    private Boolean isMaster;
    @SerializedName("isBookable")
    @Expose
    private Boolean isBookable;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("version")
    @Expose
    private Integer version;


    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public int getHcsId() {
        return hcsId;
    }

    public void setHcsId(int hcsId) {
        this.hcsId = hcsId;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The category
     */
    public String getCategory() {
        return category;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    /**
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The testParameterList
     */
    public List<TestParameterList> getTestParameterList() {
        return testParameterList;
    }

    /**
     * @param testParameterList The testParameterList
     */
    public void setTestParameterList(List<TestParameterList> testParameterList) {
        this.testParameterList = testParameterList;
    }

    /**
     * @return The popular
     */
    public Boolean getPopular() {
        return popular;
    }

    /**
     * @param popular The popular
     */
    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The lastUpdated
     */
    public String getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated The lastUpdated
     */
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated The dateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The isMaster
     */
    public Boolean getIsMaster() {
        return isMaster;
    }

    /**
     * @param isMaster The isMaster
     */
    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    /**
     * @return The isBookable
     */
    public Boolean getIsBookable() {
        return isBookable;
    }

    /**
     * @param isBookable The isBookable
     */
    public void setIsBookable(Boolean isBookable) {
        this.isBookable = isBookable;
    }

    /**
     * @return The fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted The isDeleted
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return The version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getOrderCodeSystemVersion() {
        return orderCodeSystemVersion;
    }

    public void setOrderCodeSystemVersion(String orderCodeSystemVersion) {
        this.orderCodeSystemVersion = orderCodeSystemVersion;
    }

    public Boolean getMaster() {
        return isMaster;
    }

    public void setMaster(Boolean master) {
        isMaster = master;
    }

    public Boolean getBookable() {
        return isBookable;
    }

    public void setBookable(Boolean bookable) {
        isBookable = bookable;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

}
