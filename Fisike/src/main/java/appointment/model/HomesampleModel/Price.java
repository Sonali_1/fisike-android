package appointment.model.HomesampleModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 7/20/2016.
 */
public class Price {

    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("currency")
    @Expose
    private String currency;

    public Price(String cost){
        this.cost = cost;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}