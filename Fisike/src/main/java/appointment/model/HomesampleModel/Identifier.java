package appointment.model.HomesampleModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by laxmansingh on 7/20/2016.
 */
public class Identifier {
    @SerializedName("system")
    @Expose
    private String system;
    @SerializedName("paramId")
    @Expose
    private String paramId;

    /**
     * @return The system
     */
    public String getSystem() {
        return system;
    }

    /**
     * @param system The system
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return The paramId
     */
    public String getParamId() {
        return paramId;
    }

    /**
     * @param paramId The paramId
     */
    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

}