package appointment.phlooba.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import appointment.fragment.MyAppointmentFragment;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.phlooba.model.BookedPatientModel;
import appointment.phlooba.model.HomeDrawModel;
import appointment.model.OrderSummary;
import appointment.phlooba.model.ScriptModel;
import appointment.phlooba.adapter.HomeDrawAdapter;
import appointment.phlooba.request.HomeDrawSearchRequest;
import appointment.phlooba.ViewAllHomeDrawActivity;
import appointment.request.AppointUpdateRequest;
import appointment.request.SearchReasonResponse;
import appointment.utils.SimpleDividerItemDecorationHeight;
import appointment.utils.SupportBaseFragment;

import static com.mphrx.fisike.R.id.search_test;

/**
 * Created by LENOVO on 12/1/2016.
 */

public class HomeDrawListingFragment extends SupportBaseFragment implements View.OnClickListener, AppointmentResponseCallback {

    private Context mContext;
    private ProgressDialog pdialog;
    private AppointmentResponseCallback responseCallback;
    private RecyclerView rv_upcoming, rv_recent;
    private TextView tv_viewAll_upcoming, tv_viewAll_recent;
    private String position = "", cancel_or_reschedule = "";

    private CustomFontTextView textEmptyError;
    private RelativeLayout listEmptyError;

    private HomeDrawAdapter adapterMyOrders_upcoming;
    private java.util.List<HomeDrawModel> myorder_upcoming_list = new ArrayList<HomeDrawModel>();

    private HomeDrawAdapter adapterMyOrders_recent;
    private java.util.List<HomeDrawModel> myorder_recent_list = new ArrayList<HomeDrawModel>();
    private RelativeLayout rel_lyt_myorder, rel_lyt_upcoming, rel_lyt_recent;
    private RelativeLayout frm_lyt_upcoming, frm_lyt_recent;
    private String from;
    private Button searchButton;
    private FrameLayout frameLayout;


    private PopupWindow mPopup;
    private CustomFontTextView reasonLabel;
    private CustomFontTextView tvReason;
    private CustomFontEditTextView ed_comments;
    private long transactionIdReason;


    public HomeDrawListingFragment() {
        mContext = getActivity();
    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        responseCallback = this;

        return R.layout.homedrawlisting_activity;
    }


    @Subscribe
    public void onGetReasonResponse(SearchReasonResponse searchReasonResponse) {

        if (transactionIdReason != searchReasonResponse.getTransactionId())
            return;

        if (searchReasonResponse.isSuccessful()) {
            OrderSummary.getInstance().setCancelReason(searchReasonResponse.getReasonList());
        }
    }

    @Override
    public void findView() {
        rv_upcoming = (RecyclerView) getView().findViewById(R.id.rv_upcoming);
        rv_recent = (RecyclerView) getView().findViewById(R.id.rv_recent);
        tv_viewAll_upcoming = (TextView) getView().findViewById(R.id.tv_viewAll_upcoming);
        tv_viewAll_recent = (TextView) getView().findViewById(R.id.tv_viewAll_recent);
        rel_lyt_myorder = (RelativeLayout) getView().findViewById(R.id.rel_lyt_myorder);
        textEmptyError = (CustomFontTextView) getView().findViewById(R.id.tv_error_view);
        listEmptyError = (RelativeLayout) getView().findViewById(R.id.rl_error_view);
        rel_lyt_recent = (RelativeLayout) getView().findViewById(R.id.rel_lyt_recent);
        rel_lyt_upcoming = (RelativeLayout) getView().findViewById(R.id.rel_lyt_upcoming);
        frm_lyt_recent = (RelativeLayout) getView().findViewById(R.id.frameRecent);
        frm_lyt_upcoming = (RelativeLayout) getView().findViewById(R.id.frameUpcoming);
        searchButton = (Button) getView().findViewById(search_test);
    }

    @Override
    public void initView() {

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            rel_lyt_myorder.setVisibility(View.GONE);
            return;
        } else {
//            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_CANCEL));
            /*pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.setCancelable(false);
            pdialog.show();*/

            showProgressDialog(getString(R.string.txt_loading));
            rel_lyt_myorder.setVisibility(View.VISIBLE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, TextConstants.BOTH, 0));
        }


        from = OrderSummary.getInstance().getTestType();
        rv_upcoming.setNestedScrollingEnabled(false);
        adapterMyOrders_upcoming = new HomeDrawAdapter(mContext, myorder_upcoming_list, responseCallback, TextConstants.UPCOMING, rv_upcoming);
        rv_upcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_upcoming.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        rv_upcoming.setAdapter(adapterMyOrders_upcoming);

        rv_recent.setNestedScrollingEnabled(false);
        adapterMyOrders_recent = new HomeDrawAdapter(mContext, myorder_recent_list, responseCallback, TextConstants.RECENT, rv_recent);
        rv_recent.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_recent.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        rv_recent.setAdapter(adapterMyOrders_recent);
    }

    @Override
    public void bindView() {
        tv_viewAll_upcoming.setOnClickListener(this);
        tv_viewAll_recent.setOnClickListener(this);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getActivity().onBackPressed();
//                MyAppointmentFragment.setPage(0);
            }
        });
    }


    @Override
    public void onResume() {
        if (MyAppointmentFragment.isAppointmentSuccessful) {
            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                return;
            } else {
               /* pdialog = new ProgressDialog(mContext);
                pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                pdialog.setCanceledOnTouchOutside(false);
                pdialog.show();*/
                showProgressDialog(getString(R.string.txt_loading));

                long transactionId = getTransactionId();
                ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, TextConstants.BOTH, 0));
            }
            MyAppointmentFragment.isAppointmentSuccessful = false;
        }
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_viewAll_upcoming:
                Intent intent = new Intent(mContext, ViewAllHomeDrawActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("recent_or_upcoming", TextConstants.UPCOMING);
                intent.putExtra("bundle", bundle);
                startActivityForResult(intent, TextConstants.INTENT_CONSTANT);
                break;

            case R.id.tv_viewAll_recent:
                Intent intent1 = new Intent(mContext, ViewAllHomeDrawActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("recent_or_upcoming", TextConstants.RECENT);
                intent1.putExtra("bundle", bundle1);
                startActivityForResult(intent1, TextConstants.INTENT_CONSTANT);
                break;

            default:
                break;
        }
    }


    @Override
    public void passResponse(final String response, final String recent_or_upcoming)
    {
        try
        {
            getActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                   /* if (pdialog != null && pdialog.isShowing())
                    {
                        pdialog.dismiss();
                        pdialog = null;
                    }*/
                    dismissProgressDialog();
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                        showErrorFullListEmpty(response, false);
                    }
                    else
                    {
                        if (recent_or_upcoming.equalsIgnoreCase(TextConstants.BOTH))
                        {
                            try
                            {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject bookedJsonObject = jsonObject.optJSONObject("bookedAppointments");
                                JSONObject fulfilledJsonObject = jsonObject.optJSONObject("fulfilledAppointments");

                                JSONArray bookedJsonArray = bookedJsonObject.optJSONArray("appointments");
                                JSONArray fulfilledJsonArray = fulfilledJsonObject.optJSONArray("appointments");

                                int totalCountForBooked = bookedJsonObject.optInt("totalCount");
                                int totalCountForFulfilled = fulfilledJsonObject.optInt("totalCount");


                                if(bookedJsonArray!=null && bookedJsonArray.length()>0)
                                {
                                    populateAppointmentList(bookedJsonArray, (ArrayList<HomeDrawModel>) myorder_upcoming_list);
                                }

                                if(fulfilledJsonArray!=null && fulfilledJsonArray.length()>0)
                                {
                                    populateAppointmentList(fulfilledJsonArray, (ArrayList<HomeDrawModel>) myorder_recent_list);
                                }

                                if(myorder_upcoming_list.size()<1 && myorder_recent_list.size() <1)
                                {
                                    showErrorFullListEmpty(getResources().getString(R.string.error_no_result), true);
                                }
                                else
                                {
                                    listEmptyError.setVisibility(View.GONE);
                                    rel_lyt_myorder.setVisibility(View.VISIBLE);

                                    adapterMyOrders_upcoming.notifyDataSetChanged();
                                    adapterMyOrders_recent.notifyDataSetChanged();

                                    if (totalCountForFulfilled == 0) {
                                        frm_lyt_recent.setVisibility(View.GONE);
                                    } else if (totalCountForFulfilled <= 2) {
                                        frm_lyt_recent.setVisibility(View.VISIBLE);
                                        tv_viewAll_recent.setVisibility(View.GONE);
                                    } else if (totalCountForFulfilled > 2) {
                                        frm_lyt_recent.setVisibility(View.VISIBLE);
                                        tv_viewAll_recent.setVisibility(View.VISIBLE);
                                    }

                                    if (totalCountForBooked == 0) {
                                        frm_lyt_upcoming.setVisibility(View.GONE);
                                    } else if (totalCountForBooked <= 2) {
                                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                        tv_viewAll_upcoming.setVisibility(View.GONE);
                                    } else if (totalCountForBooked > 2) {
                                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                        tv_viewAll_upcoming.setVisibility(View.VISIBLE);
                                    }
                                }


                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }






                           /* Gson gson = new Gson();

                            Type listType = new TypeToken<java.util.List<AppointmentDataModel>>() {}.getType();

                            AppointmentDataModel appointmentDataModel = new AppointmentDataModel();
                            try {
                                appointmentDataModel = gson.fromJson(response.toString(), AppointmentDataModel.class);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            if (appointmentDataModel.getList().size() == 0) {
                                showErrorFullListEmpty(getResources().getString(R.string.error_no_result), true);
                            } else {
                         *//*    [............. Splitting data into upcoming and recent...........   ]     *//*
                                listEmptyError.setVisibility(View.GONE);
                                rel_lyt_myorder.setVisibility(View.VISIBLE);
                                myorder_upcoming_list.clear();
                                myorder_recent_list.clear();
                                for (int i = 0; i < appointmentDataModel.getList().size(); i++) {
                                    appointment.model.appointmodel.List list_model = appointmentDataModel.getList().get(i);

                                    Date date = new Date();
                                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd_hh_mm_ss_a, Locale.US);
                                    String strdate = ft.format(date);
                                    Date todaydate = null, startdate = null;
                                    try {
                                        todaydate = ft.parse(strdate);
                                        startdate = ft.parse(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.YYYY_MM_dd_hh_mm_ss_a, list_model.getStart().getValue().toString().trim()));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (startdate.after(todaydate) && !list_model.getStatus().equalsIgnoreCase("cancelled")) {
                                        myorder_upcoming_list.add(list_model);
                                    } else if (startdate.before(todaydate) || list_model.getStatus().equalsIgnoreCase("cancelled")) {
                                        myorder_recent_list.add(list_model);
                                    }
                                }
                                Collections.sort(myorder_upcoming_list, new AppointmentComparator<List>());

                                try {
                                    adapterMyOrders_upcoming.notifyDataSetChanged();
                                    adapterMyOrders_recent.notifyDataSetChanged();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                                if (myorder_recent_list.size() == 0) {
                                    frm_lyt_recent.setVisibility(View.GONE);
                                } else if (myorder_recent_list.size() <= 3) {
                                    frm_lyt_recent.setVisibility(View.VISIBLE);
                                    tv_viewAll_recent.setVisibility(View.GONE);
                                } else if (myorder_recent_list.size() > 3) {
                                    frm_lyt_recent.setVisibility(View.VISIBLE);
                                    tv_viewAll_recent.setVisibility(View.VISIBLE);
                                }

                                if (myorder_upcoming_list.size() == 0) {
                                    frm_lyt_upcoming.setVisibility(View.GONE);
                                } else if (myorder_upcoming_list.size() <= 3) {
                                    frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                    tv_viewAll_upcoming.setVisibility(View.GONE);
                                } else if (myorder_upcoming_list.size() > 3) {
                                    frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                    tv_viewAll_upcoming.setVisibility(View.VISIBLE);
                                }*/


                        } else if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {

                            if (position.length() != 0 && cancel_or_reschedule.length() != 0) {
                                Gson gson = new Gson();
                                appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
                                myorder_upcoming_list.remove(Integer.parseInt(position));
//                                myorder_recent_list.add(appointmentmodel);

                                if (myorder_recent_list.size() == 0) {
                                    frm_lyt_recent.setVisibility(View.GONE);
                                } else if (myorder_recent_list.size() <= 3) {
                                    frm_lyt_recent.setVisibility(View.VISIBLE);
                                    tv_viewAll_recent.setVisibility(View.GONE);
                                } else if (myorder_recent_list.size() > 3) {
                                    frm_lyt_recent.setVisibility(View.VISIBLE);
                                    tv_viewAll_recent.setVisibility(View.VISIBLE);
                                }

                                if (myorder_upcoming_list.size() == 0) {
                                    frm_lyt_upcoming.setVisibility(View.GONE);
                                } else if (myorder_upcoming_list.size() <= 3) {
                                    frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                    tv_viewAll_upcoming.setVisibility(View.GONE);
                                } else if (myorder_upcoming_list.size() > 3) {
                                    frm_lyt_upcoming.setVisibility(View.VISIBLE);
                                    tv_viewAll_upcoming.setVisibility(View.VISIBLE);
                                }

//                                Collections.sort(myorder_recent_list, new AppointmentComparator<appointment.model.appointmodel.List>());
                                Collections.reverse(myorder_recent_list);
                                adapterMyOrders_recent.notifyDataSetChanged();
                                adapterMyOrders_upcoming.notifyDataSetChanged();

                                Toast.makeText(mContext, mContext.getResources().getString(R.string.appointment_cancelled), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }
        catch (Exception e)
        {
        }
    }

    private void populateAppointmentList(JSONArray jsonArray, ArrayList<HomeDrawModel> list)
    {

        for(int j=0; j<jsonArray.length(); j++)
        {
            try
            {
                JSONObject response = jsonArray.getJSONObject(j);

                HomeDrawModel homeDrawModel = new HomeDrawModel();

                homeDrawModel.setBookedPractitionerFirstName(response.optString("bookedPractitionerFirstName"));
                homeDrawModel.setBookedPractitionerLastName(response.optString("bookedPractitionerLastName"));
                homeDrawModel.setBookedPractitionerPhoneNo(response.optString("bookedPractitionerPhoneNo"));
                homeDrawModel.setAppointmentIdentifier(response.optString("appointmentIdentifier"));
                homeDrawModel.setStartTime(response.optString("startTime"));
                homeDrawModel.setEndDate(response.optString("endTime"));
                homeDrawModel.setAppointmentDate(response.optString("appointmentDate"));
                homeDrawModel.setTimezone(response.optString("timezone"));
                homeDrawModel.setAppointmentStatus(response.optString("appointmentStatus"));
                homeDrawModel.setAssignedPractitionerLatitude(response.optString("assignedPractitionerLatitude"));
                homeDrawModel.setAssignedPractitionerLongitude(response.optString("assignedPractitionerLongitude"));
                homeDrawModel.setId(response.optLong("id"));
                homeDrawModel.setSelectedAddress(response.optString("selectedAddress"));
                homeDrawModel.setSelectedAddressZipCode(response.optString("selectedAddressZipCode"));

                JSONObject bookedPatientJSON = response.optJSONObject("bookedPatient");
                BookedPatientModel bookedPatientModel = new BookedPatientModel();
                if (bookedPatientJSON != null)
                {
                    bookedPatientModel.setFirstName(bookedPatientJSON.optString("firstName"));
                    bookedPatientModel.setLastName(bookedPatientJSON.optString("lastName"));
                    bookedPatientModel.setDob(bookedPatientJSON.optString("dob"));
                    bookedPatientModel.setPhoneNo(bookedPatientJSON.optString("phoneNo"));
                    JSONArray uploadedScriptJSONARRAY = bookedPatientJSON.optJSONArray("bookedPatient");
                    ArrayList<ScriptModel> uploadedScriptList = new ArrayList<>();
                    for (int i = 0; uploadedScriptJSONARRAY != null && i < uploadedScriptJSONARRAY.length(); i++)
                    {
                        ScriptModel scriptModel = new ScriptModel();
                        JSONObject obj = uploadedScriptJSONARRAY.getJSONObject(i);
                        scriptModel.setImageBase64(obj.optString("scriptImage"));
                        scriptModel.setScriptImageMimeType(obj.optString("scriptImageMimeType"));
                        scriptModel.setScriptImageThumbnail(obj.optString("scriptImageThumbnail"));
                        uploadedScriptList.add(scriptModel);
                    }
                    bookedPatientModel.setUploadedScriptsList(uploadedScriptList);
                }
                homeDrawModel.setBookedPatientModel(bookedPatientModel);

                list.add(homeDrawModel);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }


    }

    private void showErrorFullListEmpty(String response, boolean toShowBtn) {
        rel_lyt_myorder.setVisibility(View.GONE);
        listEmptyError.setVisibility(View.VISIBLE);
        textEmptyError.setText(response);
        if (!toShowBtn) {
            searchButton.setVisibility(View.GONE);
        } else {
            searchButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {
        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            return;
        } else {
            this.position = position;
            this.cancel_or_reschedule = cancel_or_reschedule;
            showReasonCommentDialogCommon(appointmentmodel, position, cancel_or_reschedule);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        //Log.d("EquiMoves", requestCode + "  /  " + resultCode);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String position = bundle.getString("position");
                    String cancel_or_reschedule = bundle.getString("cancel_or_reschedule");
                    appointment.model.appointmodel.List appointmentmodel = bundle.getParcelable("appointment_model");
                    if (cancel_or_reschedule.equalsIgnoreCase(TextConstants.RESCHEDULE)) {

//                        myorder_upcoming_list.set(Integer.parseInt(position), appointmentmodel);
                        //    adapterMyOrders_upcoming.notifyItemChanged(Integer.parseInt(position));     //***.. uncomment if sorting not required......

                         /*[.... Sorting applied if cancel or reschedule......]*/
//                        Collections.sort(myorder_upcoming_list, new AppointmentComparator<appointment.model.appointmodel.List>());
                        adapterMyOrders_upcoming.notifyDataSetChanged();
                        adapterMyOrders_recent.notifyDataSetChanged();
                        Toast.makeText(getActivity(), getResources().getString(R.string.appointment_rescheduled), Toast.LENGTH_SHORT).show();
                        /*[.... End of Sorting applied if cancel or reschedule......]*/
                    }

                    if (myorder_recent_list.size() == 0) {
                        frm_lyt_recent.setVisibility(View.GONE);
                    } else if (myorder_recent_list.size() <= 3) {
                        frm_lyt_recent.setVisibility(View.VISIBLE);
                        tv_viewAll_recent.setVisibility(View.GONE);
                    } else if (myorder_recent_list.size() > 3) {
                        frm_lyt_recent.setVisibility(View.VISIBLE);
                        tv_viewAll_recent.setVisibility(View.VISIBLE);
                    }

                    if (myorder_upcoming_list.size() == 0) {
                        frm_lyt_upcoming.setVisibility(View.GONE);
                    } else if (myorder_upcoming_list.size() <= 3) {
                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                        tv_viewAll_upcoming.setVisibility(View.GONE);
                    } else if (myorder_upcoming_list.size() > 3) {
                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                        tv_viewAll_upcoming.setVisibility(View.VISIBLE);
                    }

                } else if (null != data && resultCode == TextConstants.REFERESH_CONSTANT) {
                    Bundle bundle = data.getExtras();
                    String str = bundle.getString("data_refresh");

                    if (str.equals(TextConstants.REFRESH_SMALLCASE)) {
                        if (!Utils.showDialogForNoNetwork(mContext, false)) {
                            listEmptyError.setVisibility(View.VISIBLE);
                            rel_lyt_myorder.setVisibility(View.GONE);
                            return;
                        } else {
                            myorder_upcoming_list.clear();
                            myorder_recent_list.clear();
                            adapterMyOrders_upcoming.notifyDataSetChanged();
                            adapterMyOrders_recent.notifyDataSetChanged();

                            if (pdialog != null) {
                                pdialog = null;
                            }
                          /*  pdialog = new ProgressDialog(mContext);
                            pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                            pdialog.setCanceledOnTouchOutside(false);
                            pdialog.show();*/

                            showProgressDialog(getString(R.string.txt_loading));

                            listEmptyError.setVisibility(View.GONE);
                            rel_lyt_myorder.setVisibility(View.VISIBLE);
                            long transactionId = getTransactionId();
                            ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, TextConstants.BOTH, 0));
                        }
                    }

                } else if (null != data && resultCode == TextConstants.REBOOK_CONSTANT) {
                    Bundle bundle = data.getExtras();
                    appointment.model.appointmodel.List appointmentmodel = bundle.getParcelable("appointment_model");
                    //list to be sorted
//                    myorder_upcoming_list.add(appointmentmodel);
//                    Collections.sort(myorder_upcoming_list, new AppointmentComparator<appointment.model.appointmodel.List>());
                    adapterMyOrders_upcoming.notifyDataSetChanged();
                    adapterMyOrders_recent.notifyDataSetChanged();

                    if (myorder_recent_list.size() == 0) {
                        frm_lyt_recent.setVisibility(View.GONE);
                    } else if (myorder_recent_list.size() <= 3) {
                        frm_lyt_recent.setVisibility(View.VISIBLE);
                        tv_viewAll_recent.setVisibility(View.GONE);
                    } else if (myorder_recent_list.size() > 3) {
                        frm_lyt_recent.setVisibility(View.VISIBLE);
                        tv_viewAll_recent.setVisibility(View.VISIBLE);
                    }

                    if (myorder_upcoming_list.size() == 0) {
                        frm_lyt_upcoming.setVisibility(View.GONE);
                    } else if (myorder_upcoming_list.size() <= 3) {
                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                        tv_viewAll_upcoming.setVisibility(View.GONE);
                    } else if (myorder_upcoming_list.size() > 3) {
                        frm_lyt_upcoming.setVisibility(View.VISIBLE);
                        tv_viewAll_upcoming.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }


    public void showReasonCommentDialogCommon(final appointment.model.appointmodel.List appointmentmodel, final String position, final String cancel_or_reschedule) {
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.reason_comment_dialog, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(mContext.getResources().getString(R.string.txt_cancel_appointment));
        ed_comments = (CustomFontEditTextView) v.findViewById(R.id.ed_comments);
        reasonLabel = (CustomFontTextView) v.findViewById(R.id.tv_static_reason);
        tvReason = (CustomFontTextView) v.findViewById(R.id.spinnerReasons);


        reasonLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupNames(tvReason);
            }
        });

        tvReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupNames(tvReason);
            }
        });


        ((CustomFontButton) v.findViewById(R.id.cancel_action)).setText(mContext.getResources().getString(R.string.cancel));
        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        ((CustomFontButton) v.findViewById(R.id.continue_action)).setText(mContext.getResources().getString(R.string.txt_ok));
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    alertDialog.dismiss();
                    return;
                } else {


                    String reason;
                    if (tvReason.getText().toString().equals(mContext.getResources().getString(R.string.select_one)))
                        Toast.makeText(getActivity(), getResources().getString(R.string.reason_for_cancellation), Toast.LENGTH_LONG).show();
                    else {
                        alertDialog.dismiss();
                        reason = tvReason.getText().toString();
                        pdialog = new ProgressDialog(mContext);
                        pdialog.setMessage(getResources().getString(R.string.updating));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                        long transactionId = getTransactionId();
                        ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, responseCallback, appointmentmodel, cancel_or_reschedule, reason, ed_comments.getText().toString().trim()));
                    }
                }
            }
        });
        alertDialog.show();
    }


    private void showPopupNames(final View view) {
        int idView = -1;
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        String[] arrayPopupString = null;
        if (view.getId() == R.id.spinnerReasons) {
            idView = R.id.spinnerReasons;
            ArrayList<String> cancelReason = OrderSummary.getInstance().getCancelReason();
            if (cancelReason.size() > 0) {

                arrayPopupString = cancelReason.toArray(new String[cancelReason.size()]);

            } else {
                CharSequence[] arrayPopup = getResources().getTextArray(R.array.appointment_reasons);
                arrayPopupString = new String[arrayPopup.length];
                for (int i = 0; i < arrayPopup.length; i++) {
                    arrayPopupString[i] = arrayPopup[i].toString();
                }
            }
        }
        final StatusAdapter adapter = new StatusAdapter(getActivity(), R.layout.text_status, arrayPopupString);

        list.setAdapter(adapter);
        final int finalIdView = idView;
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (finalIdView == R.id.spinnerReasons)
                    tvReason.setText(adapter.getItem(position));

                if (mPopup != null && mPopup.isShowing()) {
                    mPopup.dismiss();
                }
            }
        });
        int width = tvReason.getWidth();
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setFocusable(true);
        mPopup.setOutsideTouchable(true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.showAsDropDown(view, (int) Utils.convertDpToPixel(0, getActivity()), -(int) Utils.convertDpToPixel(0, getActivity()));
    }

    public long getTransactionIdReason() {
        transactionIdReason = System.currentTimeMillis();
        return transactionIdReason;
    }

    public long getTransactionId() {
        return System.currentTimeMillis();
    }
}
