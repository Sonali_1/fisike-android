package appointment.phlooba.fragment;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import appointment.MyOrdersActivity;
import appointment.phlooba.adapter.FeedBackAdapter;
import appointment.phlooba.adapter.GridAppointmentAdapter;
import appointment.utils.SupportBaseFragment;

/**
 * Created by Aastha on 11/03/2016.
 */
public class PhloobaHomeFragment extends SupportBaseFragment implements View.OnClickListener {

    private RecyclerView lv_options;
    private GridAppointmentAdapter requestAdapter;
    private ArrayList<String> requestTypes = new ArrayList<String>();
    private Context mContext;

    private FeedBackAdapter mFeedBackAdapter;
    private List<String> list_feedback = new ArrayList<String>();
    private RecyclerView mRv_feedback;


    public PhloobaHomeFragment() {
        mContext = getActivity();

    }

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        BaseActivity.bt_toolbar_right.setText(getResources().getString(R.string.fa_northwell));
        BaseActivity.bt_toolbar_right.setTextSize(44);
        BaseActivity.bt_toolbar_right.setVisibility(View.GONE);
        BaseActivity.bt_toolbar_right.setOnClickListener(this);
        return R.layout.phlooba_home_fragment;
    }

    @Override
    public void findView() {

        lv_options = (RecyclerView) getView().findViewById(R.id.request_options);
        mRv_feedback = (RecyclerView) getView().findViewById(R.id.rv_feedback);

    }

    @Override
    public void initView() {
        requestTypes.clear();
        requestTypes.add("Book a Home Draw Request");
        requestTypes.add("Visit a Patient Service Center");

        int numberOfColumns = 2;
        lv_options.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        requestAdapter = new GridAppointmentAdapter(getActivity(), requestTypes, TextConstants.FROM_APPOINTMENT);
        lv_options.setAdapter(requestAdapter);

        mFeedBackAdapter = new FeedBackAdapter(getActivity(), list_feedback);
        mRv_feedback.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRv_feedback.setAdapter(mFeedBackAdapter);
        mFeedBackAdapter.notifyDataSetChanged();

    }

    @Override
    public void bindView() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(getActivity());
                break;
        }
    }

}

