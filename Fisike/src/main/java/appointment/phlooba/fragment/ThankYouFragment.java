package appointment.phlooba.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.util.ArrayList;

import appointment.phlooba.model.HomeDrawModel;
import appointment.phlooba.model.ImageDataType;
import appointment.phlooba.model.TimeSlotModel;
import appointment.phlooba.adapter.ImageGridAdapter;
import appointment.phlooba.WhosPatientActivity;
import appointment.utils.CommonTasks;
import appointment.utils.SelectableRoundedImageView;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 6/22/2017.
 */

public class ThankYouFragment extends SupportBaseFragment implements View.OnClickListener {

    public static String IS_BOOKED = "IS_BOOKED";
    private Context mContext;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private SelectableRoundedImageView mImg_user;
    private CustomFontTextView mTv_success_status, mTv_name, mTv_info, mTv_refid, mTv_time, mTv_username,
            mTv_location_address, mTv_view_in_pdf,static_tv_uploaded_script, tv_request_title, tv_request_subtite;
    private RelativeLayout mRel_lyt_img_call_msg, mRel_lyt_iconcall, mRel_lyt_iconmsg,
            mRel_lyt_appointment_info, mRel_lyt_uploaded_script, rlBookableFlow,
            rlRequestableFlow;

    private CardView phlebotomistCardView;
    private IconTextView mIv_icon_call, mIv_icon_msg;
    private CustomFontButton mBtn_ok;
    boolean isBooked = false;
    private HomeDrawModel model;
    private TimeSlotModel slot;
    private Address address;
    private String timeZone;
    private RecyclerView rv_script;
    private ArrayList<ImageDataType> listElement;
    private ImageGridAdapter uploadedScriptGridAdapter;
    private String phlebotomistMobile;

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.thankyou_title));
        WhosPatientActivity.showHelplineToolbar();
        return R.layout.thankyou_fragment;
    }

    @Override
    public void findView() {
        mImg_user = (SelectableRoundedImageView) getView().findViewById(R.id.img_user);

        mTv_success_status = (CustomFontTextView) getView().findViewById(R.id.tv_success_status);
        mTv_name = (CustomFontTextView) getView().findViewById(R.id.tv_name);
        mTv_info = (CustomFontTextView) getView().findViewById(R.id.tv_info);
        mTv_refid = (CustomFontTextView) getView().findViewById(R.id.tv_refid);
        mTv_time = (CustomFontTextView) getView().findViewById(R.id.tv_time);
        mTv_username = (CustomFontTextView) getView().findViewById(R.id.tv_username);
        mTv_location_address = (CustomFontTextView) getView().findViewById(R.id.tv_location_address);
        mTv_view_in_pdf = (CustomFontTextView) getView().findViewById(R.id.tv_view_in_pdf);

        tv_request_subtite = (CustomFontTextView) getView().findViewById(R.id.tv_request_success_msg);
        tv_request_title = (CustomFontTextView) getView().findViewById(R.id.tv_bookable_request_details);


        mRel_lyt_img_call_msg = (RelativeLayout) getView().findViewById(R.id.rel_lyt_img_call_msg);
        mRel_lyt_iconcall = (RelativeLayout) getView().findViewById(R.id.rel_lyt_iconcall);
        mRel_lyt_iconmsg = (RelativeLayout) getView().findViewById(R.id.rel_lyt_iconmsg);
        mRel_lyt_appointment_info = (RelativeLayout) getView().findViewById(R.id.rel_lyt_appointment_info);
        mRel_lyt_uploaded_script = (RelativeLayout) getView().findViewById(R.id.rel_lyt_uploaded_script);

        mIv_icon_call = (IconTextView) getView().findViewById(R.id.iv_icon_call);
        mIv_icon_msg = (IconTextView) getView().findViewById(R.id.iv_icon_msg);

        mBtn_ok = (CustomFontButton) getView().findViewById(R.id.btn_ok);
        mBtn_ok.setOnClickListener(this);
        WhosPatientActivity.ivToolbarCall.setOnClickListener(this);

        rlBookableFlow = (RelativeLayout) getView().findViewById(R.id.rl_bookable_flow);
        rlRequestableFlow = (RelativeLayout) getView().findViewById(R.id.rl_requestable_flow);

        phlebotomistCardView = (CardView) getView().findViewById(R.id.card_phlebotomist);

        rv_script= (RecyclerView) getView().findViewById(R.id.rv_script);
        static_tv_uploaded_script= (CustomFontTextView) getView().findViewById(R.id.static_tv_uploaded_script);
    }

    private void showRequestableFlow() {
        phlebotomistCardView.setVisibility(View.GONE);
        mTv_success_status.setVisibility(View.GONE);
        mRel_lyt_uploaded_script.setVisibility(View.GONE);
    }

    @Override
    public void initView() {

        BusProvider.getInstance().register(this);

        model = ((WhosPatientActivity) getActivity()).getHomeDrawModel();
        slot = ((WhosPatientActivity) getActivity()).getSelectedTimeSlotModel();
        address = ((WhosPatientActivity) getActivity()).getSavedAddress();
        timeZone = ((WhosPatientActivity) getActivity()).getDateTimeZone();
        mRel_lyt_iconcall.setOnClickListener(this);
        mRel_lyt_iconmsg.setOnClickListener(this);
        mTv_view_in_pdf.setOnClickListener(this);
        if (getArguments() != null)
            isBooked = getArguments().getBoolean(IS_BOOKED);


        setCommonAppointmentData();

        if (isBooked)
            setBookedAppointmentDetails();
        else
            showRequestableFlow();

        listElement = new ArrayList<>();
        ArrayList<Bitmap> scriptList = ((WhosPatientActivity)getActivity()).uploadedScriptList;
        if(scriptList!=null && scriptList.size() > 0){
            rv_script.setVisibility(View.VISIBLE);
            mTv_view_in_pdf.setVisibility(View.VISIBLE);
            static_tv_uploaded_script.setVisibility(View.VISIBLE);
            for(Bitmap bitmap : scriptList){
                addNewElement(bitmap);
            }

        }

        uploadedScriptGridAdapter = new ImageGridAdapter(listElement, getActivity(), true);
        rv_script.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_script.setAdapter(uploadedScriptGridAdapter);
    }

    private void setBookedAppointmentDetails() {


        tv_request_title.setText(getResources().getText(R.string.home_draw_info));
        tv_request_subtite.setVisibility(View.GONE);


        mImg_user.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImg_user.setOval(true);
        mImg_user.setImageResource(R.drawable.pro);
        if (Utils.isValueAvailable(model.getPractitionerName())) {
            mTv_name.setText(model.getPractitionerName());
            mTv_success_status.setText(getString(R.string.phalbotmist_visit, model.getPractitionerName()));
        }

        if(Utils.isValueAvailable(model.getBookedPractitionerPhoneNo()))
        {
            phlebotomistMobile=model.getBookedPractitionerPhoneNo();
            mRel_lyt_iconcall.setVisibility(View.VISIBLE);
            mRel_lyt_iconmsg.setVisibility(View.VISIBLE);
        }

    }

    private void setCommonAppointmentData() {
        if (Utils.isValueAvailable(model.getAppointmentIdentifier())) {
            mTv_refid.setText(getString(R.string.booking_id) + " " + model.getAppointmentIdentifier());
        }
        if (Utils.isValueAvailable(model.getAppointmentDate())) {
//            String date = DateTimeUtil.convertDateFromOneFormatToAnother(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy, model.getAppointmentDate(), timeZone);

            String[]  strings = model.getAppointmentDate().split("T");
            String date = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.mm_dd_yyyy_slashSeperated, strings[0]);

            date = Utils.isValueAvailable(date) ? date + ", " : "";
            mTv_time.setText(date + slot.getDisplayedTime(getActivity()));
        }
        if (Utils.isValueAvailable(model.getBookedPatientModel().getPatientName())) {
            mTv_username.setText(model.getBookedPatientModel().getPatientName());
        }
        if (Utils.isValueAvailable(Utils.getAddressTextString(address))) {
            mTv_location_address.setText(Utils.getAddressTextString(address));
        }

    }

    private long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void bindView() {
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_lyt_iconcall:
                callHelpline();
                break;
            case R.id.rel_lyt_iconmsg:
                if (Utils.isValueAvailable(phlebotomistMobile)) {
                    Uri smsUri = Uri.parse("smsto:"+ phlebotomistMobile);
                    Intent intent = new Intent(Intent.ACTION_SENDTO, smsUri);
                    intent.putExtra("sms_body", "Where are you?");
//                    intent.setType("vnd.android-dir/mms-sms");
                    mContext.startActivity(intent);

                }
                break;
            case R.id.btn_ok:
                getActivity().finish();
                break;

            case R.id.tv_view_in_pdf:

                break;

            case R.id.bt_toolbar_call:
                if (!checkPermission(Manifest.permission.CALL_PHONE, ""))
                    return;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + TextConstants.PHLOOBER_HELP_LINE_NO));
                startActivity(intent);

                break;
        }
    }

    private  void addNewElement (Bitmap bitmap) {
        listElement.add(new ImageDataType(getString(R.string.add_image),bitmap));
    }

    private void callHelpline(){
        if(Utils.isValueAvailable(phlebotomistMobile)){
            Intent intentCall = new Intent(Intent.ACTION_DIAL);
            intentCall.setData(Uri.parse("tel:" + phlebotomistMobile));
            mContext.startActivity(intentCall);}
    }

}
