package appointment.phlooba.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.CameraFragment;
import com.mphrx.fisike.fragments.ImageUploadUIFragment;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.CircleImageView;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import appointment.adapter.SpinAdapter;
import appointment.adapter.SpinnerDropDownAdapter;
import appointment.phlooba.model.ImageDataType;
import appointment.phlooba.adapter.ImageGridAdapter;
import appointment.phlooba.request.RegisterAssociatedPatientRequest;
import appointment.phlooba.response.RegisterAssociatedPatientResponse;

/**
 * Created by rajantalwar on 7/18/17.
 */

public class AddProfileFragment extends ImageUploadUIFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener,
        ImageGridAdapter.clickListener, RadioGroup.OnCheckedChangeListener,
        CameraFragment.CameraIntegration {
    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;

    private CircleImageView user_img;
    private CustomFontEditTextView mEt_FirstName, mEt_LastName;

    private RelativeLayout mRel_lyt_add_manually;

    private CustomFontTextView mTv_dob;
    private CustomFontButton mBtn_save;
    private DatePickerDialog datePickerDialog;
    private String dateType;
    private SpinnerDropDownAdapter mGenderAdapter, mRelationshipAdapter;
    private Spinner mSpinner_gender, mSpinner_relationship;
    private List<String> mGenderlist = new ArrayList<String>();
    private List<String> mRelationshiplist = new ArrayList<String>();
    private RadioButton mRb_enter_manually, mRb_take_photo;
    ArrayList<ImageDataType> listElement;
    private RecyclerView gridImageList;
    private ImageGridAdapter gridImageAdapter;

    private SpinAdapter mMedicareAdapter;
    private List<String> mMedicarelist = new ArrayList<String>();
    private Spinner mSpinner_medicare;
    private CustomFontEditTextView mEt_MiddleName, mEt_medicare, mEt_policy_holdername, mEt_policy_number, mEt_group_number, mEt_primary_insurance_company;
    String fname, mname, lastname, gender, dob, relationship, cardFrontImage, cardBackImage, policyNo, groupNo, policyHolderName, policyType, policyInsuraceCompany, insuranceType;
    private String profilePic;
    private RadioGroup rgpChangeInsuranceType;
    private String medicareNo;
    private long mTransactionId;
    private int position=-1;

    @Override
    protected int getLayoutId() {
        return R.layout.addprofile_fragment_layout;
    }

    @Override
    public void findView() {
        user_img = (CircleImageView) getView().findViewById(R.id.user_img);
        user_img.setOnClickListener(this);
        mTv_dob = (CustomFontTextView) getView().findViewById(R.id.tv_dob);
        mSpinner_gender = (Spinner) getView().findViewById(R.id.spinner_gender);
        mSpinner_relationship = (Spinner) getView().findViewById(R.id.spinner_relationship);
        mBtn_save = (CustomFontButton) getView().findViewById(R.id.btn_save);
        mEt_FirstName = (CustomFontEditTextView) getView().findViewById(R.id.et_FirstName);
        mEt_LastName = (CustomFontEditTextView) getView().findViewById(R.id.et_LastName);
        mRel_lyt_add_manually = (RelativeLayout) getView().findViewById(R.id.rel_lyt_add_manually);
        mRb_enter_manually = (RadioButton) getView().findViewById(R.id.rb_enter_manually);
        mRb_take_photo = (RadioButton) getView().findViewById(R.id.rb_take_photo);
        mRb_take_photo.setChecked(true);
        gridImageList = (RecyclerView) getView().findViewById(R.id.grid_image_list);

        mSpinner_medicare = (Spinner) getView().findViewById(R.id.spinner_medicare);

        mEt_medicare = (CustomFontEditTextView) getView().findViewById(R.id.et_medicare);
        mEt_policy_holdername = (CustomFontEditTextView) getView().findViewById(R.id.et_policy_holdername);
        mEt_policy_number = (CustomFontEditTextView) getView().findViewById(R.id.et_policy_number);
        mEt_group_number = (CustomFontEditTextView) getView().findViewById(R.id.et_group_number);
        mEt_primary_insurance_company = (CustomFontEditTextView) getView().findViewById(R.id.et_primary_insurance_company);
        mEt_MiddleName = (CustomFontEditTextView) getView().findViewById(R.id.et_MiddleName);
        rgpChangeInsuranceType = (RadioGroup) getView().findViewById(R.id.rg_add_option);
        rgpChangeInsuranceType.setOnCheckedChangeListener(this);

    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);
        mContext = getActivity();
        createGenderList();
        createRelationshipList();
        createMedicareList();

        mGenderAdapter = new SpinnerDropDownAdapter(mContext, R.layout.spinner_text_view, mGenderlist);
        mRelationshipAdapter = new SpinnerDropDownAdapter(mContext, R.layout.spinner_text_view, mRelationshiplist);
        mMedicareAdapter = new SpinAdapter(mContext, R.layout.spinner_text_view, mMedicarelist, TextConstants.MEDICARE);


        mSpinner_gender.setAdapter(mGenderAdapter);
        mSpinner_relationship.setAdapter(mRelationshipAdapter);
        mSpinner_medicare.setAdapter(mMedicareAdapter);
        mSpinner_medicare.setSelection(0);


        addInitialImageViews();
        gridImageAdapter = new ImageGridAdapter(listElement, getActivity(), false);
        gridImageAdapter.setClickListener(this);
        gridImageList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        gridImageList.setAdapter(gridImageAdapter);
        mRb_take_photo.setChecked(true);
        insuranceType = mRb_take_photo.getText().toString();
        cameraIntegrationListener = this;
    }

    private void addInitialImageViews() {

        if (listElement == null)
            listElement = new ArrayList<>();
        listElement.clear();
        listElement.add(new ImageDataType(getResources().getString(R.string.front_image), null));
        listElement.add(new ImageDataType(getResources().getString(R.string.back_image), null));
    }

    @Override
    public void bindView() {
        mTv_dob.setOnClickListener(this);
        mBtn_save.setOnClickListener(this);

        mSpinner_medicare.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mSpinner_medicare.getSelectedItemPosition() == 2 && mSpinner_medicare.getSelectedItem().equals("Others")) {
                    mEt_medicare.setVisibility(View.GONE);
                    mEt_policy_holdername.setVisibility(View.VISIBLE);
                    mEt_policy_number.setVisibility(View.VISIBLE);
                    mEt_group_number.setVisibility(View.VISIBLE);
                    mEt_primary_insurance_company.setVisibility(View.VISIBLE);
                } else {
                    mEt_medicare.setVisibility(View.VISIBLE);
                    mEt_policy_holdername.setVisibility(View.GONE);
                    mEt_policy_number.setVisibility(View.GONE);
                    mEt_group_number.setVisibility(View.GONE);
                    mEt_primary_insurance_company.setVisibility(View.GONE);

                    if(mSpinner_medicare.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.medicare)))
                    {
                        mEt_medicare.setHint(getResources().getString(R.string.medicare_number));
                    }
                    else
                        mEt_medicare.setHint(getResources().getString(R.string.medicaid_number));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void createGenderList() {
        mGenderlist.clear();
        mGenderlist.add(mContext.getResources().getString(R.string.gender));
        mGenderlist.add(mContext.getResources().getString(R.string.gender_male_key));
        mGenderlist.add(mContext.getResources().getString(R.string.gender_female_key));
        mGenderlist.add(mContext.getResources().getString(R.string.gender_other_key));
    }

    private void createRelationshipList() {
        mRelationshiplist.clear();
        String[] relationship = mContext.getResources().getStringArray(R.array.relationshiparray);
        for (String relation : relationship) {
            mRelationshiplist.add(relation);
        }
    }

    private void createMedicareList() {
        mMedicarelist.clear();
        String[] medicare = mContext.getResources().getStringArray(R.array.medicarearray);
        for (String medicares : medicare) {
            mMedicarelist.add(medicares);
        }
    }



    public void showDateDialog() {
        if (datePickerDialog == null) {
            Calendar calendar = Calendar.getInstance();// Aug28, 2015

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            Calendar minDate = Calendar.getInstance();
            minDate.add(Calendar.YEAR, -100);
            Calendar maxDate = Calendar.getInstance();
            maxDate.add(Calendar.YEAR, -1);
            maxDate.set(Calendar.HOUR_OF_DAY, 23);
            maxDate.set(Calendar.MINUTE, 59);
            maxDate.set(Calendar.SECOND, 59);

            datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                LinearLayout layout = new LinearLayout(getActivity());
                layout.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
                datePickerDialog.setCustomTitle(layout);
            }
            datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
            datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());

        }
        datePickerDialog.show();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String date = null;
        if(Utils.getAge(year, month, day) >=1) {
            if (view.isShown()) {
                date = DateTimeUtil.getFormattedDateWithoutUTC(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
                mTv_dob.setText(date);
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.age_greater_than_one_year),
                           Toast.LENGTH_LONG).show();
            if(datePickerDialog != null && !datePickerDialog.isShowing())
            showDateDialog();
        }
    }


    @Override
    public void itemClicked(View view, int position) {

        switch (view.getId()) {
            default:
                this.position=position;
               showBottomSheet();
                break;
            case R.id.itv_cross:
                addBitmapToPositionAndRefreshAdapter(position,null);
                break;

        }
    }

    private void resetInsuranceValue() {
        addInitialImageViews();
        clearEditText(mEt_medicare, medicareNo);
        clearEditText(mEt_group_number, groupNo);
        clearEditText(mEt_policy_holdername, policyHolderName);
        clearEditText(mEt_policy_number, policyNo);
        clearEditText(mEt_primary_insurance_company, policyInsuraceCompany);
        mSpinner_medicare.setSelection(0);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        resetInsuranceValue();
        switch (i) {
            case R.id.rb_take_photo:
                insuranceType = mRb_take_photo.getText().toString();
                gridImageList.setVisibility(View.VISIBLE);
                mRel_lyt_add_manually.setVisibility(View.GONE);
                break;
            case R.id.rb_enter_manually:
                insuranceType = mRb_enter_manually.getText().toString();
                gridImageList.setVisibility(View.GONE);
                mRel_lyt_add_manually.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void clearEditText(CustomFontEditTextView editTextView, String field) {
        editTextView.setText("");
        field="";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_dob:
                showDateDialog();
                break;
            case R.id.btn_save:
                savePatient();
                break;
            case R.id.user_img:
                position=-1;
                showBottomSheet();
                break;
            default:
                break;
        }
    }

    private void savePatient() {
        fname = mEt_FirstName.getText().toString();
        mname = mEt_MiddleName.getText().toString();
        lastname = mEt_LastName.getText().toString();
        dob = mTv_dob.getText().toString().trim();
        gender = mSpinner_gender.getSelectedItem().toString();
        relationship = mSpinner_relationship.getSelectedItem().toString();
        policyNo = mEt_policy_number.getText().toString();
        policyHolderName = mEt_policy_holdername.getText().toString();
        groupNo = mEt_group_number.getText().toString();
        policyInsuraceCompany = mEt_primary_insurance_company.getText().toString();
        policyType = mSpinner_medicare.getSelectedItem().toString();
        boolean isValidFIelds = validateUserFields();
        if(isValidFIelds)
            sendApiToRegisterPatient();

    }

    private void sendApiToRegisterPatient() {
        if(getProfilePic() != null){
            profilePic = Utils.convertBitmapToBase64(getProfilePic());
        }

        showProgressDialog(getResources().getString(R.string.loading_txt));
        ThreadManager.getDefaultExecutorService().submit(new RegisterAssociatedPatientRequest(mContext,getTransactionId()
                , dob,fname,lastname,mname,gender,"SELF_SIGN_UP_MOBILE",relationship, SettingManager.getInstance().getUserMO().getId()+"",
                Utils.getDeviceUUid(mContext),createInsuranceJsonObject(), profilePic));
    }

    private long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    //return false if valid fields entered
    private boolean validateUserFields() {

        if (!Utils.isValueAvailable(fname))
            mEt_FirstName.setError(getString(R.string.blank_error_generic, mEt_FirstName.getHint().toString().trim()));
        else if(!Utils.isValueAvailable(lastname))
            mEt_LastName.setError(getString(R.string.blank_error_generic, mEt_LastName.getHint().toString().trim()));
        else if (!Utils.isValueAvailable(dob) || dob.equalsIgnoreCase(getString(R.string.dob)))
            showError(getString(R.string.blank_error_generic, getString(R.string.dob)));
        else if (!Utils.isValueAvailable(gender) || gender.equalsIgnoreCase(getString(R.string.gender)))
            showError(getString(R.string.blank_error_generic, getString(R.string.gender)));
        else if (!Utils.isValueAvailable(relationship) || relationship.equalsIgnoreCase(getString(R.string.relationship)))
            showError(getString(R.string.blank_error_generic, getString(R.string.relationship)));
        else {
            if (validateInsurance())
                return true;
            else return false;
        }

        return false;

    }

    private boolean validateInsurance() {
        if (insuranceType.equalsIgnoreCase(getString(R.string.enter_manually)))
            return validateManualInsuranceField();
        else
            return validateInsuranceImageAdded();
    }

    private Bitmap getProfilePic(){
        Bitmap bitmap = null;
        if(user_img.getDrawable() != null) {
            bitmap = ((BitmapDrawable) user_img.getDrawable()).getBitmap();
        }
        return bitmap;
    }

    //return true if valid image insurance added
    private boolean validateInsuranceImageAdded() {
        cardFrontImage = "null";
        cardBackImage = "null";
        for (int i = 0; i < listElement.size(); i++) {
            if (listElement.get(i).getTitle().equalsIgnoreCase(getString(R.string.front_image)) && listElement.get(i).getImage()!=null)
                cardFrontImage = Utils.convertBitmapToBase64(listElement.get(i).getImage());
            if (listElement.get(i).getTitle().equalsIgnoreCase(getString(R.string.back_image))&& listElement.get(i).getImage()!=null)
                cardBackImage = Utils.convertBitmapToBase64(listElement.get(i).getImage());
        }

        if (!Utils.isValueAvailable(cardFrontImage))
            showError(getString(R.string.upload_frontimage));
        else if (!Utils.isValueAvailable(cardBackImage))
            showError(getString(R.string.upload_backimage));
        else return true;

        return false;

    }

    //return true if all mandatory manual fields entered
    private boolean validateManualInsuranceField() {

        if (policyType.equalsIgnoreCase(getString(R.string.insurance_type_other))) {
            policyNo = mEt_policy_number.getText().toString();
            groupNo = mEt_group_number.getText().toString();
            policyHolderName = mEt_policy_holdername.getText().toString();
            policyInsuraceCompany = mEt_primary_insurance_company.getText().toString();
            if (!Utils.isValueAvailable(policyNo))
                mEt_policy_number.setError(getString(R.string.blank_error_generic, mEt_policy_number.getHint().toString()));
            else if (!Utils.isValueAvailable(groupNo))
                mEt_policy_number.setError(getString(R.string.blank_error_generic, mEt_group_number.getHint().toString()));
            else if (!Utils.isValueAvailable(policyHolderName))
                mEt_policy_number.setError(getString(R.string.blank_error_generic, mEt_policy_holdername.getHint().toString()));
            else if (!Utils.isValueAvailable(policyInsuraceCompany))
                mEt_policy_number.setError(getString(R.string.blank_error_generic, mEt_primary_insurance_company.getHint().toString()));
            else
                return true;
        } else {
            medicareNo = mEt_medicare.getText().toString();
            if (!Utils.isValueAvailable(medicareNo))
                mEt_medicare.setError(getString(R.string.blank_error_generic, mEt_medicare.getHint().toString()));
            else
                return true;
        }

        return false;
    }

    private void showError(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setExtractedBitmap(Bitmap bitmap) {
        Bitmap bitmap1 = bitmap;
        if(position==-1){
        user_img.setImageBitmap(bitmap1);
        getView().findViewById(R.id.iv_icon1).setVisibility(View.GONE);
        }
        else
        {
            addBitmapToPositionAndRefreshAdapter(position,bitmap1);
        }

    }

    private void addBitmapToPositionAndRefreshAdapter(int position,Bitmap bitmap) {
        if(position!=-1) {
            ImageDataType dataType = listElement.get(position);
            dataType.setImage(bitmap);
            listElement.set(position,dataType);
            gridImageAdapter.notifyDataSetChanged();
        }
    }

    private JSONObject createInsuranceJsonObject() {
        JSONObject coverage = new JSONObject();
        try {
            if (insuranceType.equalsIgnoreCase(getString(R.string.take_photo))) {
                coverage.put("insuranceType", getString(R.string.insurance_type_picture));
                if (Utils.isValueAvailable(cardFrontImage))
                    coverage.put("frontImage", cardFrontImage);
                else
                    coverage.put("frontImage", "");
                if (Utils.isValueAvailable(cardBackImage))
                    coverage.put("backImage", cardBackImage);
                else
                    coverage.put("backImage", "");
                coverage.put("frontImageMimeType", "image/jpg");
                coverage.put("backImageMimeType", "image/jpg");

            } else {
                if (policyType.equalsIgnoreCase(getString(R.string.insurance_type_other))) {
                    coverage.put("insuranceType", "private");
                    coverage.put("primaryInsuranceCompany", policyInsuraceCompany);
                    coverage.put("policyHolderName", policyHolderName);
                    coverage.put("groupId", groupNo);
                    coverage.put("policyNumber", policyNo);
                } else {
                    coverage.put("insuranceType", policyType);
                    coverage.put("policyNumber", medicareNo);
                }
            }
        } catch (Exception e) {

        }
        return coverage;
    }


    @Subscribe
    public void onAddProfileResponse(RegisterAssociatedPatientResponse response)
    {
     if(mTransactionId!=response.getTransactionId())
         return;
     else
     {
         dismissProgressDialog();
         if(response.isSuccessful())
         {
             if(Utils.isValueAvailable(response.getCode()) && response.getCode().equalsIgnoreCase(TextConstants.CODE_64))
                 ((ExitFromActivity)getActivity()).patientSuccessfullyLinked();
             else if(Utils.isValueAvailable(response.getInfoCode()) && response.getInfoCode().equalsIgnoreCase(TextConstants.CODE_65))
                 showError(getString(R.string.patient_already_linked));
             else
                 showError(getString(R.string.unexpected_error));
         }
         else
           showError(getString(R.string.unexpected_error));
     }
    }

  public interface ExitFromActivity{
      public void patientSuccessfullyLinked();
  }


}
