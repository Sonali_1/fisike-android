package appointment.phlooba.fragment;

import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import appointment.phlooba.model.TimeSlotModel;
import appointment.phlooba.adapter.SlotStatusAdapter;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.request.TimeSlotRequest;
import appointment.phlooba.response.TImeSlotResponse;
import appointment.utils.CommonTasks;
import appointment.utils.SupportBaseFragment;


public class ScheduleFragment extends SupportBaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, SlotStatusAdapter.clickListener {


    CustomFontTextView tvDateSlots, tvSelectedDateTime;
    CustomFontButton btnIncrementDate, btnSave, btnDecrementDate;
    RecyclerView rvGridSlots;
    Context context;
    ArrayList<TimeSlotModel> listData;
    SlotStatusAdapter adapter;
    private long transactionId;
    private long mMinDate, mMaxDate;
    private static final int APPOINTMENT_SLOT_DAY_LIMIT = 6;//no of days after current day
    String timeZone;
    private RelativeLayout rlDateHeader;
    private int selectedYear, selectedMonth, selectedDay;
    private int lastSelectedYear, lastSelectedMonth, lastSelectedDay;

    boolean isToday = false;


    @Override
    protected int getLayoutId() {

        context = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(context.getResources().getString(R.string.what_is_your_schedule));
        return R.layout.fragment_schedule;
    }

    @Override
    public void findView() {

        tvDateSlots = (CustomFontTextView) getView().findViewById(R.id.tv_date_slots);
        btnIncrementDate = (CustomFontButton) getView().findViewById(R.id.btn_increment_date);
        btnDecrementDate = (CustomFontButton) getView().findViewById(R.id.btn_decrement_date);
        rvGridSlots = (RecyclerView) getView().findViewById(R.id.rv_grid_slots);
        tvSelectedDateTime = (CustomFontTextView) getView().findViewById(R.id.tv_selected_date_time_slot);
        btnSave = (CustomFontButton) getView().findViewById(R.id.btn_save);
        rlDateHeader = (RelativeLayout) getView().findViewById(R.id.rl_date_header);
        rlDateHeader.setOnClickListener(this);
        btnDecrementDate.setOnClickListener(this);
    }

    public String getTime(String timezone, Calendar calendar) {
        if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            return DateTimeUtil.getFormattedDateTimeInZfFormatWithTimeZone(calendar, timezone);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            return DateTimeUtil.getFormattedDateTimeInZfFormatWithTimeZone(calendar, timezone);
        }
    }


    @Override
    public void initView() {
        BusProvider.getInstance().register(this);
        timeZone = ((WhosPatientActivity) context).getDateTimeZone();
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone(timeZone));
        selectedYear = c.get(Calendar.YEAR);
        selectedMonth = c.get(Calendar.MONTH);
        selectedDay = c.get(Calendar.DAY_OF_MONTH);
        lastSelectedYear = selectedYear;
        lastSelectedMonth = selectedMonth;
        lastSelectedDay = selectedDay;
        String date = DateTimeUtil.getFormattedDateWithoutUTC(((selectedMonth + 1) + "-" + selectedDay + "-" + selectedYear),
                DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
        date = CommonTasks.formatDateFromstring(DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.mm_dd_yyyy_slashSeperated, date);
        setText(date, true, tvDateSlots);
        btnDecrementDate.setVisibility(View.GONE);
        fetchSlotApiCall(getTime(timeZone, Calendar.getInstance()));
    }

    private void createSlotsList() {
        if (listData == null)
            listData = new ArrayList<>();
        listData.clear();
    }

    @Override
    public void bindView() {

        btnSave.setOnClickListener(this);
        btnIncrementDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_save:
                saveSlot();
                break;

            case R.id.btn_increment_date:
                if(Utils.showDialogForNoNetwork(getActivity(),false))
                    toggleDate(1);
                break;

            case R.id.rl_date_header:
//                showDateDialog();
                break;
            case R.id.btn_decrement_date:
                if(Utils.showDialogForNoNetwork(getActivity(),false))
                toggleDate(-1);
                break;

        }
    }

    private void toggleDate(int numberOfDays) {
        // seven days and current date logic needs to be implemented
        Calendar toggleCalendar = Calendar.getInstance();
        toggleCalendar.set(selectedYear, selectedMonth, selectedDay);
        toggleCalendar.add(Calendar.DATE, numberOfDays);
        boolean isOperationAllowed = false;
        if (numberOfDays > 0) {
            isOperationAllowed = isIncrementAllowed(toggleCalendar);
        } else {
            isOperationAllowed = isDecrementAllowed(toggleCalendar);

        }

        if (isOperationAllowed) {
            lastSelectedDay=selectedDay;
            lastSelectedMonth=selectedMonth;
            lastSelectedYear=selectedYear;
            selectedYear = toggleCalendar.get(Calendar.YEAR);
            selectedMonth = toggleCalendar.get(Calendar.MONTH);
            selectedDay = toggleCalendar.get(Calendar.DAY_OF_MONTH);
            String date = DateTimeUtil.getFormattedDateWithoutUTC(((selectedMonth + 1) + "-" + selectedDay + "-" +
                    selectedYear), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);

            date = CommonTasks.formatDateFromstring(DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.mm_dd_yyyy_slashSeperated, date);
//            setText(date, true, tvDateSlots);
            Calendar slotsCalendar = Calendar.getInstance();
            slotsCalendar.set(selectedYear, selectedMonth, selectedDay);
            fetchSlotApiCall(getTime(timeZone, slotsCalendar));
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.valid_date_range), Toast.LENGTH_LONG).show();
        }
    }

    private boolean isIncrementAllowed(Calendar calendar) {
        Calendar compare = Calendar.getInstance();
        compare.setTimeZone(TimeZone.getTimeZone(timeZone));
        compare.add(Calendar.DATE, APPOINTMENT_SLOT_DAY_LIMIT);
        return isDateWithInTheLimits(compare, calendar);
    }

    private boolean isDecrementAllowed(Calendar calendar) {
        Calendar decrement = Calendar.getInstance();
        decrement.setTimeZone(TimeZone.getTimeZone(timeZone));
        return isDateWithInTheLimits(calendar, decrement);
    }

    private boolean isDateWithInTheLimits(Calendar startCalendar, Calendar endCalendar) {
        int value = DateTimeUtil.compare(startCalendar,endCalendar);
        if (value >= 0) {
            return true;
        }
        return false;
    }

    private void saveSlot() {
        TimeSlotModel selectedTimeSlotModel = null;

        if (listData == null) {
            Toast.makeText(getActivity(), getString(R.string.please_select_slot), Toast.LENGTH_SHORT).show();
            return;
        }

        for (TimeSlotModel model : listData) {
            if (model.isSelected()) {
                selectedTimeSlotModel = model;
                break;
            }
        }
        if (selectedTimeSlotModel == null) {
            Toast.makeText(getActivity(), getString(R.string.please_select_slot), Toast.LENGTH_SHORT).show();
            return;
        }
        Bundle bundle = new Bundle();
        SummaryFragment summaryFragment = new SummaryFragment();
        summaryFragment.setArguments(bundle);
        ((SaveSlotInterface) (getActivity())).saveTimeSlotModel(selectedTimeSlotModel);
        ((WhosPatientActivity) context).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, summaryFragment, "summaryfragment")
                .addToBackStack("null")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    public void showDateDialog() {
        // Use the current date as the default date in the picker
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone(timeZone));
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        mMinDate = c.getTimeInMillis();
        c.add(Calendar.DATE, APPOINTMENT_SLOT_DAY_LIMIT);
        mMaxDate = c.getTimeInMillis();
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(mMinDate);
        datePickerDialog.getDatePicker().setMaxDate(mMaxDate);
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        String date = null;
     /*   if(getCurrentDate().compareTo(getSelectedDate(year, month, day)) < 0 ||
                getSelectedDate(year, month, day).compareTo(getCurrentDate()) > 0){
            Toast.makeText(getActivity(), getResources().getString(R.string.valid_date_range), Toast.LENGTH_LONG).show();
        } else{*/
        selectedYear = year;
        selectedMonth = month;
        selectedDay = day;
        if (view.isShown()) {
            date = DateTimeUtil.getFormattedDateWithoutUTC(((selectedMonth + 1) + "-" + selectedDay + "-" + selectedYear), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
            date = CommonTasks.formatDateFromstring(DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.mm_dd_yyyy_slashSeperated, date);

            setText(date, true, tvDateSlots);
            setSelectedDateTimeSlot();
            Calendar slotsCalendar = Calendar.getInstance();
            slotsCalendar.set(selectedYear, selectedMonth, selectedDay);
            fetchSlotApiCall(getTime(timeZone, slotsCalendar));
        }
        /*}*/
    }

    private Calendar getSelectedDate(int year, int month, int day) {
        Calendar selDate = Calendar.getInstance();
        selDate.set(year, month, day);
        return selDate;
    }

    private Calendar getCurrentDate() {
        Calendar curDate = Calendar.getInstance();
        curDate.setTimeZone(TimeZone.getTimeZone(timeZone));
        return curDate;
    }

    private Calendar getMaxAllowedDate() {
        Calendar compare = Calendar.getInstance();
        compare.setTimeZone(TimeZone.getTimeZone(timeZone));
        compare.add(Calendar.DATE, APPOINTMENT_SLOT_DAY_LIMIT);
        return compare;
    }

    @Override
    public void itemCLicked(View view, int position) {

        switch (view.getId()) {
            case R.id.ll_parent:
                int lastSelectedIndex = adapter.getLastSelectedIndex();
                if (lastSelectedIndex == position)
                    return;
                try {
                    TimeSlotModel model = adapter.getSlotModelAtPosition(position);
                    int index = listData.indexOf(model);
                    if (model.isSlotAvailable()) {
                        model.setIsSelected(true);
                        TimeSlotModel model1 = adapter.getSlotModelAtPosition(lastSelectedIndex);
                        int index1 = listData.indexOf(model1);
                        model1.setIsSelected(false);
                        listData.set(index1, model1);
                        listData.set(index, model);
                        adapter.notifyDataSetChanged();
                        setSelectedDateTimeSlot();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void fetchSlotApiCall(String dateTime) {
        showProgressDialog(getString(R.string.txt_loading));
        ThreadManager.getDefaultExecutorService().submit(new TimeSlotRequest(getActivity(), getTransactionId(), ((WhosPatientActivity) context).getAddressPostalCode(), dateTime, ((WhosPatientActivity) context).selectedPatientInfoMo.getDateOfBirth()));
    }

    public long getTransactionId() {
        transactionId = System.currentTimeMillis();
        return transactionId;
    }

    @Subscribe
    public void onSlotsRecieved(TImeSlotResponse response) {
        if (transactionId != response.getTransactionId())
            return;
        else {
            dismissProgressDialog();
            if (response.isSuccessful()) {
                setDate();
                if (response.getTimeSlotModels() != null && response.getTimeSlotModels().size() > 0) {
                    listData = response.getTimeSlotModels();
                    for (int i = 0; i < listData.size(); i++) {
                        if (listData.get(i).isSlotAvailable()) {
                            listData.get(i).setIsSelected(true);
                            break;
                        }
                    }
                    if (adapter == null)
                        setAdapter();
                    else
                        updateDataSet();
                } else if (Utils.isValueAvailable(response.getErrorMessage()) && response.getErrorMessage().equalsIgnoreCase("zipNotRegistered")) {
                    revertDateChanged();
                    Toast.makeText(getActivity(), getString(R.string.postal_not_serviceable), Toast.LENGTH_SHORT).show();
                } else {
                    revertDateChanged();
                    Toast.makeText(getActivity(), getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                revertDateChanged();
                Toast.makeText(getActivity(), getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void setDate()
    {
        String date = DateTimeUtil.getFormattedDateWithoutUTC(((selectedMonth + 1) + "-" + selectedDay + "-" +
                selectedYear), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
        date = CommonTasks.formatDateFromstring(DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.mm_dd_yyyy_slashSeperated, date);
        setText(date, true, tvDateSlots);
    }

    private void revertDateChanged() {
        selectedDay = lastSelectedDay;
        selectedMonth = lastSelectedMonth;
        selectedYear = lastSelectedYear;
        setDate();
        setSelectedDateTimeSlot();
    }

    private void updateDataSet() {
        adapter.setList(listData);
        adapter.notifyDataSetChanged();
        setDate();
        setSelectedDateTimeSlot();
    }

    private void setAdapter() {
        adapter = new SlotStatusAdapter(context, listData, this);
        rvGridSlots.setLayoutManager(new GridLayoutManager(context, 2));
        rvGridSlots.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        setSelectedDateTimeSlot();
    }

    private void setSelectedDateTimeSlot() {
        try {
            String date = tvDateSlots.getText().toString();
            String time = "";
            for (int i = 0; i < listData.size(); i++) {
                if (listData.get(i).isSelected())
                    time = listData.get(i).getDisplayedTime(getActivity());
            }

            date = date + (Utils.isValueAvailable(time) ? " , " + time : " ");
            try {
                if (date.contains(getString(R.string.today))) {
                    date = date.replace(getString(R.string.today), "").replace("(", "").replace(")", "");
                }
            } catch (Exception e) {
            }
            setText(date, false, tvSelectedDateTime);

        } catch (Exception e) {

        }
    }

    private void setText(String text, boolean isToShowDateWithToday, TextView textViewDate) {
        Calendar curDate = Calendar.getInstance();
        curDate.setTimeZone(TimeZone.getTimeZone(timeZone));

        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(selectedYear, selectedMonth, selectedDay);

        Calendar endDate=Calendar.getInstance();
        endDate.setTimeZone(TimeZone.getTimeZone(timeZone));
        endDate.add(Calendar.DAY_OF_MONTH,APPOINTMENT_SLOT_DAY_LIMIT);

        //setting visibility of back button
        if ((DateTimeUtil.compare(curDate,selectedDate)== 0)) {
            btnDecrementDate.setVisibility(View.GONE);
            setTodayDate(text, isToShowDateWithToday, textViewDate);
        } else {
            btnDecrementDate.setVisibility(View.VISIBLE);
            textViewDate.setText(text);
        }

        //setting visibility of next button
        if(DateTimeUtil.compare(endDate,selectedDate)==0)
            btnIncrementDate.setVisibility(View.GONE);
        else
            btnIncrementDate.setVisibility(View.VISIBLE);
    }

    private void setTodayDate(String text, boolean isToShowDateWithToday, TextView textView) {
        if (isToShowDateWithToday) {
            textView.setText(getResources().getString(R.string.today) + "(" + text + ")");
        } else {
            textView.setText(text);
        }
    }

    public interface SaveSlotInterface {
        public void saveTimeSlotModel(TimeSlotModel model);
    }


}

