package appointment.phlooba.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.AddProfileActivity;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.adapter.WhosPatientAdapter;
import appointment.request.FetchSelfAndAssociatedPatients;
import appointment.request.FetchSelfAndAssociatedPatientsResponse;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 6/19/2017.
 */

public class WhosPatientFragment extends SupportBaseFragment implements View.OnClickListener {

    private Context mContext;
    private String toolbar_title_text;
    private long mTransactionId;
    private ArrayList<LinkedPatientInfoMO> linkedPatientInfoMOArrayList = new ArrayList<LinkedPatientInfoMO>();
    private PatientMO patientMO;
    private RecyclerView rv_whospatient;
    private String testType;
    private WhosPatientAdapter whosPatientAdapter;
    private FloatingActionButton mFab_add;
    private boolean isFrom_patient_profile_view = false;

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        if (getArguments() != null) {
            isFrom_patient_profile_view = getArguments().getBoolean(VariableConstants.FROM_PATIENT_PROFILE_VIEW, false);
        }

        if (!isFrom_patient_profile_view) {
            WhosPatientActivity.list_fragments.add(this);
            WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.whospatient_title));
        }
        return R.layout.whospatient;
    }

    @Override
    public void findView() {
        rv_whospatient = (RecyclerView) getView().findViewById(R.id.rv_whospatient);
        mFab_add = (FloatingActionButton) getView().findViewById(R.id.fab_add);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void initView() {

        testType = getArguments().getString(TextConstants.FROM);
        BusProvider.getInstance().register(this);

        rv_whospatient.setLayoutManager(new LinearLayoutManager(mContext));
        whosPatientAdapter = new WhosPatientAdapter(mContext, linkedPatientInfoMOArrayList, isFrom_patient_profile_view);
        rv_whospatient.setAdapter(whosPatientAdapter);
        fetchLinkedPatient();

    }

    private void fetchLinkedPatient() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            showProgressDialog(getString(R.string.txt_loading));
            ThreadManager.getDefaultExecutorService().submit(new FetchSelfAndAssociatedPatients(getTransactionId(), TextConstants.FETCH_ALL));

        } else {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            try {

            if (userMO != null) {
                linkedPatientInfoMOArrayList.clear();
                LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), null,userMO.getCoverage());
                linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
            }
            } catch (Exception e) {
                e.printStackTrace();
            }
            whosPatientAdapter.notifyDataSetChanged();
        }
    }


    private long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void bindView() {
        mFab_add.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add:
//                Toast.makeText(getActivity(), "Under development...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, AddProfileActivity.class);
                getActivity().startActivityForResult(intent, TextConstants.INTENT_CONSTANT);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    fetchLinkedPatient();

                }
        }
    }


    @Subscribe
    public void onGetLinkedPatientsResponse(FetchSelfAndAssociatedPatientsResponse fetchSelfAndAssociatedPatientsResponse) {


        if (mTransactionId != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;
        dismissProgressDialog();
        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful()) {

            if (linkedPatientInfoMOArrayList.size() > 0)
                linkedPatientInfoMOArrayList.clear();

            for (int i = 0; i < fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().size(); i++) {
                linkedPatientInfoMOArrayList.add(fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList().get(i));

            }
            whosPatientAdapter.notifyDataSetChanged();
        } else {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            try {
                if (userMO != null) {
                    linkedPatientInfoMOArrayList.clear();
                    LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO(userMO.getDateOfBirth(), userMO.getFirstName() + " " + userMO.getLastName(), userMO.getPhoneNumber(), userMO.getEmail(), userMO.getGender(), userMO.getPatientId(), null,userMO.getCoverage());
                    linkedPatientInfoMOArrayList.add(0, linkedPatientInfoMO);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            whosPatientAdapter.notifyDataSetChanged();
        }
    }

}
