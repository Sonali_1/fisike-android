package appointment.phlooba.fragment;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.CameraFragment;
import com.mphrx.fisike.fragments.ImageUploadUIFragment;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import appointment.adapter.RecommendedBySpinnerAdapter;
import appointment.phlooba.model.ImageDataType;
import appointment.phlooba.model.InstructionListModel;
import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.model.ObservationAppointmentMo;
import appointment.phlooba.AddProfileActivity;
import appointment.phlooba.adapter.ImageGridAdapter;
import appointment.phlooba.adapter.InstructionsAdapter;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.request.ObservationApointmentRequest;
import appointment.phlooba.response.ObservationAppointmentResponse;
import appointment.utils.SelectableRoundedImageView;

/**
 * Created by rajandroid on 23/06/17.
 */


public class UploadScriptFragment extends ImageUploadUIFragment implements View.OnClickListener ,ImageGridAdapter.clickListener,
                                                                           CameraFragment.CameraIntegration,InstructionsAdapter.clickListener
{

    private Context mContext;
    private String toolbar_title_text;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private SelectableRoundedImageView imageView_user;
    private CustomFontTextView textView_user;
    private CustomFontButton proceedButton;
    Bitmap script_bitmap;
    private RecyclerView gridImageView;
    private RecyclerView rvInstructionList;
    private ImageGridAdapter adapter;
    private ArrayList<ImageDataType> listElement;
    private Spinner mSpinnerRecommendedBy;
    private ArrayList<ObservationAppointmentMo> mRecommendedByList;
    ArrayList<InstructionListModel> instructionData;
    private RecommendedBySpinnerAdapter recommendedBy_adapter;
    private CardView cardRecommendedBy;
    private RelativeLayout rlUploadedScript;
    private int selectedScriptindex = -1;
    InstructionsAdapter instructionsAdapter;
    private long transactionId;
    private String patientName;
    private int refferedBySelectedIndex;
    private ArrayList<Bitmap> bitmapList = new ArrayList<Bitmap>();
    private AlertDialog dialog;
    private AlertDialog testListDialog;
    private LinkedPatientInfoMO patientInfoMO;


    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.do_you_have_script));
        return R.layout.uploadscript_layout;
    }

    @Override
    public void findView() {
        imageView_user = (SelectableRoundedImageView) getView().findViewById(R.id.img_user);
        textView_user = (CustomFontTextView) getView().findViewById(R.id.tv_user_name);
        proceedButton = (CustomFontButton) getView().findViewById(R.id.btn_save);
        gridImageView = (RecyclerView) getView().findViewById(R.id.grid_image_list);
        mSpinnerRecommendedBy = (Spinner) getView().findViewById(R.id.sp_recommended_by);
        cardRecommendedBy=(CardView) getView().findViewById(R.id.card_recommended_by);
        rvInstructionList= (RecyclerView) getView().findViewById(R.id.rv_instructions);

    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);
        patientName= Utils.isValueAvailable(((WhosPatientActivity)getActivity()).getSelectedPatientName())?((WhosPatientActivity)getActivity()).getSelectedPatientName():"";
        textView_user.setText(patientName);
        imageView_user.setOval(true);

        patientInfoMO=((WhosPatientActivity)getActivity()).selectedPatientInfoMo;

        if(patientInfoMO.getProfilePic()!=null && !patientInfoMO.getProfilePic().equalsIgnoreCase("null")) {
            imageView_user.setImageBitmap(Utils.convertBase64ToBitmap(patientInfoMO.getProfilePic().toString().trim()));
        }
        listElement = new ArrayList<>();
        addNewElement();
        adapter = new ImageGridAdapter(listElement, getActivity(),false);
        adapter.setClickListener(this);
        gridImageView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        gridImageView.setAdapter(adapter);
        cameraIntegrationListener = this;
        observationAppointmentApi();
        initInstructionAdapter();

    }

    private void createRecommendedByList(ArrayList<ObservationAppointmentMo> refferingPhlysicianList) {

        if(refferingPhlysicianList.size()>0){
        ObservationAppointmentMo selectModel=new ObservationAppointmentMo();
        selectModel.setOrderingPhysicianName("Select");
        mRecommendedByList=refferingPhlysicianList;
            mRecommendedByList.add(0,selectModel);
        }

    }

    @Override
    public void bindView() {
        proceedButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add:
                Intent intent = new Intent(mContext, AddProfileActivity.class);
                getActivity().startActivityForResult(intent, TextConstants.INTENT_CONSTANT);
                break;
            case R.id.btn_save:

                if(dialog==null) {
                    dialog = DialogUtils.showOrangeThemeDialog(getActivity(), getString(R.string.confirmation_title), getString(R.string.not_uploaded_script_message),
                            getString(R.string.btn_continue), getString(R.string.cancel), null, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == Dialog.BUTTON_POSITIVE) {
                                       saveUploadedScriptAndMoveToNextScreen();
                                    } else if (i == Dialog.BUTTON_NEGATIVE) {
                                        dialogInterface.dismiss();
                                    }
                                }
                            });
                }

                if(bitmapList!=null && bitmapList.size()>0)
                    saveUploadedScriptAndMoveToNextScreen();
                else
                    dialog.show();

                break;

            default:
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {
                }
        }
    }


    @Override
    public void itemClicked(View view, int position) {

        switch (view.getId()) {
            case R.id.itv_cross:
                removeImage(position);
                break;
            default:
                rlUploadedScript = (RelativeLayout) view;
                captureImage(position);
        }
    }

    private void toggleView(View view){
        // hide camera // hide camera text // show cross button
        view.findViewById(R.id.tv_txt_cameraoption).setVisibility(View.GONE);
        view.findViewById(R.id.itv_icon_camera).setVisibility(View.GONE);
        view.findViewById(R.id.itv_cross).setVisibility(View.VISIBLE);
    }

    private void captureImage(int position) {
        selectedScriptindex = position;
        showBottomSheet();
    }

    void  addNewElement() {
        listElement.add(new ImageDataType(getString(R.string.add_image),null));

    }

    private void removeImage(int position) {
        listElement.remove(position);
        bitmapList.remove(position);
        if (listElement.size() == 0) {
            addNewElement();
        }
        adapter.notifyDataSetChanged();
    }


    private void setThumbnail(Bitmap backgroundBitmap){
        if(selectedScriptindex != -1 && listElement.size() >= selectedScriptindex){
            listElement.get(selectedScriptindex).setImage(backgroundBitmap);
        }
        toggleView(rlUploadedScript);
        addNewElement();
        if(rlUploadedScript != null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                rlUploadedScript.setBackground(new BitmapDrawable(getResources(), backgroundBitmap));
            else
                rlUploadedScript.setBackgroundDrawable(new BitmapDrawable(getResources(), backgroundBitmap));
    }

    @Override
    public void setExtractedBitmap(Bitmap bitmap) {
        setThumbnail(bitmap);
        bitmapList.add(bitmap);
    }

    private void initInstructionAdapter()
    {
        instructionData=new ArrayList<>();
        instructionData.add(new InstructionListModel(getString(R.string.instruction1),""));
        instructionData.add(new InstructionListModel(getString(R.string.instruction2),""));
        instructionData.add(new InstructionListModel(getString(R.string.instruction3,getString(R.string.app_name)),getString(R.string.txt_test)));
        instructionData.add(new InstructionListModel(getString(R.string.instruction4),getString(R.string.txt_psc)));
        instructionsAdapter=new InstructionsAdapter(getActivity(),instructionData,this);
        rvInstructionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvInstructionList.setAdapter(instructionsAdapter);
    }

    public long getTransactionId() {
        transactionId = System.currentTimeMillis();
        return transactionId;
    }

    private void observationAppointmentApi() {
        showProgressDialog(getString(R.string.txt_loading));
        if(patientInfoMO!=null){
        ThreadManager.getDefaultExecutorService().submit(new ObservationApointmentRequest(getTransactionId(),patientInfoMO.getPatientId()));
        }
    }


    @Subscribe
    public void ObservationAppointment(ObservationAppointmentResponse response) {
        if (transactionId != response.getTransactionId())
            return;
        else
        {
            dismissProgressDialog();
            if(response.isSuccessful())
            {

                createRecommendedByList(response.getObservations());
                if(mRecommendedByList != null && mRecommendedByList.size()>0){
                    recommendedBy_adapter = new RecommendedBySpinnerAdapter(mContext, R.layout.spinner_text_view, mRecommendedByList);
                    mSpinnerRecommendedBy.setAdapter(recommendedBy_adapter);
                    cardRecommendedBy.setVisibility(View.VISIBLE);
                    if(mRecommendedByList.size()==2)
                    {
                        mSpinnerRecommendedBy.setSelection(1);
                    }

                }
            }
        }
    }

    @Override
    public void itemClicked(int position, String text) {

        if(text.equalsIgnoreCase(getString(R.string.txt_test)))
        {
            showDialogForTestList(position);
        }
        else if(text.equalsIgnoreCase(getString(R.string.txt_psc)))
        {
            getActivity().finish();
        }
    }

    private void showDialogForTestList(int position)
    {

        if(testListDialog==null){
        testListDialog=DialogUtils.showOrangeThemeDialog(mContext,Utils.toCamelCase(getString(R.string.txt_test)),getString(R.string.test_list),getString(R.string.ok_got_it),null,null,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i==DialogInterface.BUTTON_POSITIVE)
                {
                    dialogInterface.dismiss();
                }
            }
        });
        }
        testListDialog.show();


    }

    public interface UploadedScript{
        void saveUploadedScript(ArrayList<Bitmap> uploadedScriptList);
}

    private void saveUploadedScriptAndMoveToNextScreen()
    {
        Bundle bundle = new Bundle();
        ((UploadedScript)getActivity()).saveUploadedScript(bitmapList);
        SelectInsuranceFragment insuranceFragment = new SelectInsuranceFragment();
        insuranceFragment.setArguments(bundle);
        if(recommendedBy_adapter!=null && mRecommendedByList!=null && mRecommendedByList.size()>0)
            ((WhosPatientActivity) mContext).setSelectedRefferingPhysicianMo(recommendedBy_adapter.getItem(mSpinnerRecommendedBy.getSelectedItemPosition()));

        ((WhosPatientActivity) mContext).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, insuranceFragment, "insuranceFragment")
                .addToBackStack("insurance")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
