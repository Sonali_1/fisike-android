package appointment.phlooba.fragment;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import appointment.phlooba.model.HomeDrawModel;
import appointment.phlooba.model.ImageDataType;
import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.model.ObservationAppointmentMo;
import appointment.phlooba.model.ScriptModel;
import appointment.phlooba.model.TimeSlotModel;
import appointment.phlooba.AddProfileActivity;
import appointment.phlooba.adapter.ImageGridAdapter;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.request.HomeDrawRequest;
import appointment.phlooba.response.HomeDrawResponse;
import appointment.utils.SelectableRoundedImageView;
import appointment.utils.SupportBaseFragment;

/**
 * Created by LENOVO on 6/27/2017.
 */

public class SummaryFragment extends SupportBaseFragment implements View.OnClickListener {

    private Context mContext;
    private String toolbar_title_text;
    private ProgressDialog pdialog;
    private long mTransactionId;
    private SelectableRoundedImageView imageView_user;
    private CustomFontTextView textView_user, tvMobileNumber, tvAddress, tvDate, tvTime;
    private CustomFontButton proceedButton;
    private RecyclerView uploadedScriptGridView, insuranceGridView;
    private ImageGridAdapter uploadedScriptGridAdapter;
    private LinearLayout llManualInsurance;
    Bitmap script_bitmap;
    private ArrayList<ImageDataType> listElement;
    private LinkedPatientInfoMO selectedPatientInfoMo;
    private CardView cardUploadScript, cardInsurance;
    private Address address;
    private TimeSlotModel selectedTimeSlotModel;
    private String timeZone;
    private ArrayList<ImageDataType> listElementInsurance;
    private ImageGridAdapter insuranceAdapter;
    private long transactionId;
    private CustomFontTextView insuranceType, insurancetypeCompanyName, lblPolicyNumber, txtPolicyNo,
            lblGroupNo, txtGroupNo, lblPolicyHolderName, txtPolicyHolderName;
    private CustomFontButton btnDelete;
    private  LinearLayout llPhoneNumber;
    private Coverage coverage;


    private IconTextView iv_icon_edit_script, iv_icon_edit_slot;


    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.booking_summary));
        return R.layout.summary_layout;
    }

    @Override
    public void findView() {
        imageView_user = (SelectableRoundedImageView) getView().findViewById(R.id.img_user);
        textView_user = (CustomFontTextView) getView().findViewById(R.id.tv_user_name);
        proceedButton = (CustomFontButton) getView().findViewById(R.id.btn_save);
        uploadedScriptGridView = (RecyclerView) getView().findViewById(R.id.gridview);
        tvMobileNumber = (CustomFontTextView) getView().findViewById(R.id.tv_mobile);
        cardUploadScript = (CardView) getView().findViewById(R.id.card_upload_script);
        tvAddress = (CustomFontTextView) getView().findViewById(R.id.tv_address);
        tvDate = (CustomFontTextView) getView().findViewById(R.id.tv_date);
        tvTime = (CustomFontTextView) getView().findViewById(R.id.tv_time);
        cardInsurance = (CardView) getView().findViewById(R.id.card_insurance);
        insuranceGridView = (RecyclerView) getView().findViewById(R.id.rv_insurance_image);
        iv_icon_edit_script = (IconTextView) getView().findViewById(R.id.edit_script);
        iv_icon_edit_slot = (IconTextView) getView().findViewById(R.id.edit_time);
        llPhoneNumber= (LinearLayout) getView().findViewById(R.id.ll_phone_number);
        findInsuranceCardElements();
    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);
        timeZone = ((WhosPatientActivity) getActivity()).getDateTimeZone();
        imageView_user.setOval(true);
        listElement = new ArrayList<>();
        uploadedScriptGridAdapter = new ImageGridAdapter(listElement, getActivity(), true);
        uploadedScriptGridView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        uploadedScriptGridView.setAdapter(uploadedScriptGridAdapter);
        cardUploadScript.setVisibility(View.GONE);
        cardInsurance.setVisibility(View.GONE);
        listElementInsurance = new ArrayList<>();
        insuranceAdapter = new ImageGridAdapter(listElementInsurance, getActivity(), true);
        insuranceGridView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        insuranceGridView.setAdapter(insuranceAdapter);
        coverage=((WhosPatientActivity) getActivity()).getCoverage();

    }

    private void addNewElement(Bitmap bitmap) {
        listElement.add(new ImageDataType(getString(R.string.add_image), bitmap));
    }

    private void addNewInsuranceElement(Bitmap bitmap, String tag) {

        listElementInsurance.add(new ImageDataType(tag, bitmap));
    }

    private void findInsuranceCardElements() {
        llManualInsurance = (LinearLayout) getView().findViewById(R.id.ll_manual_insurance);
        insuranceType = (CustomFontTextView) getView().findViewById(R.id.insuranceType);
        insurancetypeCompanyName = (CustomFontTextView) getView().findViewById(R.id.insurancetype_company_name);
        lblPolicyNumber = (CustomFontTextView) getView().findViewById(R.id.lbl_policy_number);
        txtPolicyNo = (CustomFontTextView) getView().findViewById(R.id.txt_policy_no);
        lblGroupNo = (CustomFontTextView) getView().findViewById(R.id.lbl_group_number);
        txtGroupNo = (CustomFontTextView) getView().findViewById(R.id.txt_group_no);
        lblPolicyHolderName = (CustomFontTextView) getView().findViewById(R.id.lbl_policy_holder_name);
        txtPolicyHolderName = (CustomFontTextView) getView().findViewById(R.id.txt_policy_holder_name);
        btnDelete = (CustomFontButton) getView().findViewById(R.id.btn_delete);
        btnDelete.setVisibility(View.GONE);

    }


    @Override
    public void bindView() {
        proceedButton.setOnClickListener(this);
        iv_icon_edit_slot.setOnClickListener(this);
        iv_icon_edit_script.setOnClickListener(this);
        this.selectedPatientInfoMo = ((WhosPatientActivity) getActivity()).selectedPatientInfoMo;
        ArrayList<Bitmap> scriptList = ((WhosPatientActivity) getActivity()).uploadedScriptList;
        if (scriptList != null && scriptList.size() > 0) {
            cardUploadScript.setVisibility(View.VISIBLE);
            for (Bitmap bitmap : scriptList) {
                addNewElement(bitmap);
            }

        }

        if(selectedPatientInfoMo.getProfilePic()!=null && !selectedPatientInfoMo.getProfilePic().equalsIgnoreCase("null")) {
            imageView_user.setImageBitmap(Utils.convertBase64ToBitmap(selectedPatientInfoMo.getProfilePic().toString().trim()));
        }

        if (selectedPatientInfoMo != null) {
            if (Utils.isValueAvailable(selectedPatientInfoMo.getPhoneNumber()))
                tvMobileNumber.setText(selectedPatientInfoMo.getPhoneNumber().trim());
            else {
                llPhoneNumber.setVisibility(View.GONE);
            }

            if (Utils.isValueAvailable(selectedPatientInfoMo.getName()))
                textView_user.setText(selectedPatientInfoMo.getName().trim());
        }

        initInsuranceData();

        this.address = ((WhosPatientActivity) getActivity()).getSavedAddress();
        if (address != null) {
            tvAddress.setText(Utils.getAddressTextString(address));
        }
        this.selectedTimeSlotModel = ((WhosPatientActivity) getActivity()).getSelectedTimeSlotModel();
        if (selectedTimeSlotModel != null) {

            tvDate.setText(DateTimeUtil.convertDateFromOneFormatToAnother
                    (DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.mm_dd_yyyy_slashSeperated,
                            selectedTimeSlotModel.getSlotRequestedDate(), timeZone));
            tvTime.setText(selectedTimeSlotModel.getDisplayedTime(getActivity()));
        }
    }

    private void initInsuranceData() {

        if (Utils.isValueAvailable(coverage.getInsuranceType()) && coverage.getInsuranceType().
                equalsIgnoreCase(getString(R.string.insurance_type_picture))) {
            llManualInsurance.setVisibility(View.GONE);
            insuranceGridView.setVisibility(View.VISIBLE);
            cardInsurance.setVisibility(View.VISIBLE);
            if (Utils.isValueAvailable(coverage.getBackImage()) || Utils.isValueAvailable(coverage.getFrontImage())) {
                cardInsurance.setVisibility(View.VISIBLE);
                if (Utils.isValueAvailable(coverage.getFrontImage()))
                    addNewInsuranceElement(Utils.convertBase64ToBitmap(coverage.getFrontImage()), getString(R.string.front_image));
                if (Utils.isValueAvailable(coverage.getBackImage()))
                    addNewInsuranceElement(Utils.convertBase64ToBitmap(coverage.getBackImage()), getString(R.string.back_image));
                insuranceAdapter.notifyDataSetChanged();
            }
        } else if (Utils.isValueAvailable(coverage.getInsuranceType())) {
            llManualInsurance.setVisibility(View.VISIBLE);
            insuranceGridView.setVisibility(View.GONE);
            cardInsurance.setVisibility(View.VISIBLE);
            if (Utils.isValueAvailable(coverage.getInsuranceType())) {
                if (coverage.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicare)) &&
                        Utils.isValueAvailable(coverage.getPolicyNumber())) {
                    insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicare)));
                    lblPolicyNumber.setText(getString(R.string.medicare_number));
                    txtPolicyNo.setText(coverage.getPolicyNumber());
                } else if (coverage.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicaid)) &&
                        Utils.isValueAvailable(coverage.getPolicyNumber())) {
                    insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicaid)));
                    lblPolicyNumber.setText(getString(R.string.medicaid_number));
                    txtPolicyNo.setText(coverage.getPolicyNumber());
                } else {
                    insuranceType.setText(coverage.getPrimaryInsuranceCompany());
                    txtPolicyNo.setText(coverage.getPolicyNumber());
                    txtGroupNo.setText(coverage.getGroupId());
                    txtPolicyHolderName.setText(coverage.getPolicyHolderName());
                    lblGroupNo.setVisibility(View.VISIBLE);
                    txtGroupNo.setVisibility(View.VISIBLE);
                    lblPolicyHolderName.setVisibility(View.VISIBLE);
                    txtPolicyHolderName.setVisibility(View.VISIBLE);
                    insurancetypeCompanyName.setVisibility(View.VISIBLE);

                }

            }

        }

    }

    public long getTransactionId() {
        transactionId = System.currentTimeMillis();
        return transactionId;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add:
                Intent intent = new Intent(mContext, AddProfileActivity.class);
                getActivity().startActivityForResult(intent, TextConstants.INTENT_CONSTANT);
                break;
            case R.id.btn_save:
                //api call integration
                showConfirmationDialog();
                break;

            case R.id.edit_script:

                WhosPatientActivity.list_fragments.remove(WhosPatientActivity.list_fragments.size() - 1);
                WhosPatientActivity.list_fragments.remove(WhosPatientActivity.list_fragments.size() - 1);

                getActivity().getSupportFragmentManager().popBackStack("schedule", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                WhosPatientActivity.setToolbarTitleText(getResources().getString(R.string.do_you_have_script));

                break;

            case R.id.edit_time:

                getActivity().onBackPressed();

                break;


            default:
                break;
        }
    }


    private ArrayList<ScriptModel> getScriptModelList() {
        ArrayList<ScriptModel> scriptModelList = new ArrayList<>();
        ArrayList<Bitmap> scriptList = ((WhosPatientActivity) getActivity()).uploadedScriptList;
        for (Bitmap scriptBitmap : scriptList) {
            ScriptModel scriptModel = new ScriptModel(Utils.convertBitmapToBase64(scriptBitmap), "image/jpg", null);
            scriptModelList.add(scriptModel);
        }

        return scriptModelList;
    }


    private void showConfirmationDialog() {

        DialogUtils.showOrangeThemeDialog(mContext, getString(R.string.confirmation_title), getString(R.string.homedraw_fee_confirmation_msg),
                getString(R.string.yes_sure), getString(R.string.no_wait), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            saveAppointment();
                        } else if (which == Dialog.BUTTON_NEGATIVE) {
                            dialog.dismiss();
                        }
                    }
                }).show();
    }

    private void saveAppointment() {
        ObservationAppointmentMo referringPhysicianMo = ((WhosPatientActivity) getActivity()).getSelectedRefferingPhysicianMo();
        String referringPhysicianName = "";
        long referringPhysicianId = -1;
        if (referringPhysicianMo != null) {
            referringPhysicianName = referringPhysicianMo.getOrderingPhysicianName();
            referringPhysicianId = referringPhysicianMo.getId();
        }

        showProgressDialog(getString(R.string.txt_loading));
        ThreadManager.getDefaultExecutorService().submit(new HomeDrawRequest(getActivity(), getTransactionId(),
                getScriptModelList(), selectedTimeSlotModel.getStartTime(),
                selectedTimeSlotModel.getEndTime(),
                selectedTimeSlotModel.getSlotRequestedDate(),
                selectedTimeSlotModel.getSlotIdentifier(),
                Utils.getAddressTextString(address),
                TextConstants.SCRIPT_ORIGINAL, selectedPatientInfoMo.getPatientId() + "",
                referringPhysicianName, referringPhysicianId, address.getPostalCode(),
                address.getLatitude(),
                address.getLongitude(),
                timeZone,coverage.getId()));

    }


    private void launchThankYouScreen(HomeDrawModel homeDrawModel) {
        ((HomeDrawDataSet) mContext).setHomeDrawMode(homeDrawModel);
        boolean isBooked = false;
        if (Utils.isValueAvailable(homeDrawModel.getAppointmentStatus())) {
            if (homeDrawModel.getAppointmentStatus().equalsIgnoreCase(getString(R.string.booked_status)))
                isBooked = true;
            else if (homeDrawModel.getAppointmentStatus().equalsIgnoreCase(getString(R.string.proposed_status)))
                isBooked = false;
        }

        Bundle bundle = new Bundle();
        bundle.putBoolean(ThankYouFragment.IS_BOOKED, isBooked);
        ThankYouFragment thankYouFragment = new ThankYouFragment();
        thankYouFragment.setArguments(bundle);
        ((WhosPatientActivity) mContext).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, thankYouFragment, "thankyoufragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {


                }
        }
    }

    @Subscribe
    public void onHomeDrawResponse(HomeDrawResponse response) {
        if (transactionId != response.getTransactionId())
            return;
        else {
            dismissProgressDialog();
            if (response.isSuccessful() && response.getHomeDrawModel() != null) {
                launchThankYouScreen(response.getHomeDrawModel());
            } else {
                Toast.makeText(getActivity(), getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public interface HomeDrawDataSet {
        public void setHomeDrawMode(HomeDrawModel model);

        public HomeDrawModel getHomeDrawModel();
    }
}

