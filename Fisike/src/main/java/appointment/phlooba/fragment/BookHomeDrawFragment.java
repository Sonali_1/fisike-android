package appointment.phlooba.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import appointment.phlooba.AddAddressActivity;
import appointment.phlooba.adapter.HorizontalAddressAdapter;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.request.HealthCareServiceTimeZoneRequest;
import appointment.phlooba.response.HealthCareServiceTimeZoneResponse;
import appointment.phlooba.snaphelper.GravitySnapHelper;
import appointment.phlooba.snaphelper.HorizontalSpaceItemDecoration;
import appointment.utils.SupportBaseFragment;

/**
 * Created by laxmansingh on 6/19/2017.
 */


public class BookHomeDrawFragment extends SupportBaseFragment implements View.OnClickListener, OnMapReadyCallback, HorizontalAddressAdapter.clickListener {

    private Context mContext;
    private List<Address> mListAddress = new ArrayList<Address>();
    private RecyclerView mRecyclerview_horizontal;
    private HorizontalAddressAdapter mHorizontalAddressAdapter;
    private GoogleMap mMap;
    private LinearLayoutManager HorizontalLayout;
    private View ChildView;
    private int RecyclerViewItemPosition;
    private SupportMapFragment mapFragment;
    private FloatingActionButton mFab_add;
    private long transactionId;
    private Address selectedAddress;
    private SnapHelper helper;

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.book_home_draw_title));

        BusProvider.getInstance().register(this);
        return R.layout.bookhomedraw_layout;
    }

    @Override
    public void findView() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mRecyclerview_horizontal = (RecyclerView) getView().findViewById(R.id.recyclerview_horizontal);
        mRecyclerview_horizontal.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        mFab_add = (FloatingActionButton) getView().findViewById(R.id.fab_add);
    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    public void initView() {
        mapFragment.getMapAsync(this);
    }


    @Override
    public void bindView() {

        mFab_add.setOnClickListener(this);
        AddItemsToRecyclerViewArrayList();

        mHorizontalAddressAdapter = new HorizontalAddressAdapter(mContext, mListAddress);
        mHorizontalAddressAdapter.setClickListener(this);
        HorizontalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerview_horizontal.setLayoutManager(HorizontalLayout);

        mRecyclerview_horizontal.setOnFlingListener(null);

        helper = new GravitySnapHelper(Gravity.START);
        helper.attachToRecyclerView(mRecyclerview_horizontal);

        mRecyclerview_horizontal.setAdapter(mHorizontalAddressAdapter);
        HorizontalSpaceItemDecoration decoration = new HorizontalSpaceItemDecoration(10);
        mRecyclerview_horizontal.addItemDecoration(decoration);


        // Adding on item click listener to RecyclerView.
        mRecyclerview_horizontal.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent motionEvent) {

                    return true;
                }


            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    // Toast.makeText(mContext, Recyclerview.getChildAdapterPosition(ChildView) + "", Toast.LENGTH_SHORT).show();
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    Recyclerview.smoothScrollToPosition(RecyclerViewItemPosition);

                    LatLng latLng = new LatLng(mListAddress.get(RecyclerViewItemPosition).getLatitude(), mListAddress.get(RecyclerViewItemPosition).getLongitude());

                     if(mMap != null)
                     {
                         mMap.clear();
                         mMap.addMarker(new MarkerOptions().position(latLng).title("Selected Address"));
                         CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 11.0f);
                         mMap.animateCamera(yourLocation);

                     }

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        mRecyclerview_horizontal.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);



                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE)
                {
                    ChildView = helper.findSnapView(HorizontalLayout);

                    if(ChildView!=null)
                        RecyclerViewItemPosition = recyclerView.getChildAdapterPosition(ChildView);
                    else
                        RecyclerViewItemPosition = mListAddress.size()-1;

                    LatLng latLng = new LatLng(mListAddress.get(RecyclerViewItemPosition).getLatitude(), mListAddress.get(RecyclerViewItemPosition).getLongitude());

                    if(mMap != null) {
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(latLng).title("Selected Address"));
                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 11.0f);
                        mMap.animateCamera(yourLocation);
                    }

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                if(Utils.showDialogForNoNetwork(getActivity(),false)){
                Intent intent = new Intent(mContext, AddAddressActivity.class);
                getActivity().startActivityForResult(intent, TextConstants.INTENT_CONSTANT);}
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    Address addressModel = new Address();
                    addressModel.setText(Utils.createAddressTextString(bundle.getString("text1"), bundle.getString("text2"), bundle.getString("country"),bundle.getString("state"), bundle.getString("city"), bundle.getString("postalcode")));
                    addressModel.setCity(bundle.getString("city"));
                    addressModel.setState(bundle.getString("state"));
                    addressModel.setPostalCode(bundle.getString("postalcode"));
                    addressModel.setCountry(bundle.getString("country"));
                    mListAddress.add(addressModel);
                    mHorizontalAddressAdapter.notifyDataSetChanged();
                }
                
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if(mListAddress!=null && mListAddress.size()>0  && googleMap!= null)
        {
            LatLng latLng = new LatLng(mListAddress.get(0).getLatitude(), mListAddress.get(0).getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title("Selected Address"));
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 11.0f);
            googleMap.animateCamera(yourLocation);
        }

    }


    // function to add items in RecyclerView.
    public void AddItemsToRecyclerViewArrayList() {

        UserMO userMO = SettingManager.getInstance().getUserMO();
        ArrayList<Address> addresses = userMO.getAddressUserMos();
        if (addresses != null) {
            for (Address address : addresses) {
                mListAddress.add(address);
            }
        }
    }

    public long getTransactionId() {
        transactionId = System.currentTimeMillis();
        return transactionId;
    }

    @Override
    public void itemClicked(int position) {
        selectedAddress = mHorizontalAddressAdapter.getElementAtPosition(position);
        // add the api call here
        showProgressDialog(getResources().getString(R.string.Processing_loading));
        ThreadManager.getDefaultExecutorService().submit
                                                (new HealthCareServiceTimeZoneRequest(getActivity(),
                                                                                      getTransactionId(),
                                                                                      selectedAddress.getPostalCode()));
    }

    @Subscribe
    public void OperationAllowedInZipCode(HealthCareServiceTimeZoneResponse healthCareServiceTimeZoneResponse){
       // onSuccess call navigateToUploadScriptScreen
        if(healthCareServiceTimeZoneResponse.isSuccess()){
            // save in shared pref
            ((WhosPatientActivity)getActivity()).setTimeZone(healthCareServiceTimeZoneResponse.getTimeZone());
            navigateToUploadScriptScreen();
        } else if(!healthCareServiceTimeZoneResponse.isAddressServicable()){
            Toast.makeText(getActivity(), getResources().getString(R.string.address_not_servicable),
                           Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.something_wrong_try_again),Toast.LENGTH_LONG).show();
        }
        dismissProgressDialog();
    }

    private void navigateToUploadScriptScreen(){
        Bundle bundle = new Bundle();
        UploadScriptFragment uploadScriptFragment = new UploadScriptFragment();
        uploadScriptFragment.setArguments(bundle);
        ((WhosPatientActivity) mContext).saveAddress(selectedAddress);
        ((WhosPatientActivity) mContext).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, uploadScriptFragment, "uploadscriptfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
