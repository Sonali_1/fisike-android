package appointment.phlooba.fragment;

import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.utils.Utils;

import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.WhosPatientActivity;
import appointment.profile.Adapter.InsuranceListAdapter;
import appointment.utils.SelectableRoundedImageView;
import appointment.utils.SupportBaseFragment;

/**
 * Created by neharathore on 28/08/17.
 */

public class SelectInsuranceFragment extends SupportBaseFragment implements InsuranceListAdapter.CheckChangeListener{
    private Context mContext;
    private RecyclerView rvInsuranceList;
    private LinkedPatientInfoMO model;
    InsuranceListAdapter adapter;
    SelectableRoundedImageView imgUser;
    CustomFontTextView tvUserFullName;

    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        WhosPatientActivity.list_fragments.add(this);
        WhosPatientActivity.setToolbarTitleText(mContext.getResources().getString(R.string.insurance_card));
        return R.layout.fragment_select_insurance;
    }

    @Override
    public void findView() {

        rvInsuranceList = (RecyclerView) getView().findViewById(R.id.rv_insurance);
        model = ((WhosPatientActivity) getActivity()).getSelectedPatientInfoMo();
        imgUser = (SelectableRoundedImageView) getView().findViewById(R.id.img_user);
        tvUserFullName = (CustomFontTextView) getView().findViewById(R.id.tv_user_name);

    }

    @Override
    public void initView() {
        if (model == null)
            return;

        imgUser.setImageBitmap(Utils.convertBase64ToBitmap(model.getProfilePic()));
        if (Utils.isValueAvailable(model.getName()))
            tvUserFullName.setText(model.getName());
        adapter = new InsuranceListAdapter(getActivity(), model.getCoverage(), false,model.getCoverage().size()>0?0:-1,this);
        rvInsuranceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvInsuranceList.setAdapter(adapter);
    }

    @Override
    public void bindView() {

        getView().findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInsuranceAndLaunchNextScreen();
            }
        });
    }

    private void saveInsuranceAndLaunchNextScreen()
    {
        ((saveSelectedCoverage)getActivity()).saveCoverage(adapter.getInsuranceAtPosition(adapter.getInsuranceSelectedPosition()));
        ScheduleFragment scheduleFragment = new ScheduleFragment();
        scheduleFragment.setArguments(null);
        ((WhosPatientActivity) mContext).getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, scheduleFragment, "scheduleFragment")
                .addToBackStack("schedule")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void checkChanged(int position) {
        if(adapter!=null)
        {
            adapter.setRadioButtonSelectedIndex(position);
            adapter.notifyDataSetChanged();
        }
    }

    public interface saveSelectedCoverage{
        public void saveCoverage(Coverage coverage);
    }


}
