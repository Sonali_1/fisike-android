package appointment.phlooba;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.interfaces.OnLoadMoreListener;
import appointment.phlooba.model.BookedPatientModel;
import appointment.phlooba.model.HomeDrawModel;
import appointment.model.OrderSummary;
import appointment.phlooba.model.ScriptModel;
import appointment.model.appointmodel.List;
import appointment.phlooba.adapter.HomeDrawAdapter;
import appointment.phlooba.request.HomeDrawSearchRequest;
import appointment.request.AppointUpdateRequest;
import appointment.request.SearchReasonResponse;
import appointment.utils.SimpleDividerItemDecorationHeight;

/**
 * Created by laxmansingh on 3/16/2016.
 */
public class ViewAllHomeDrawActivity extends BaseActivity implements View.OnClickListener, AppointmentResponseCallback {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private ProgressDialog pdialog;
    private AppointmentResponseCallback responseCallback;
    private RecyclerView rv_viewall;
    private String recent_or_upcoming;
    private HomeDrawAdapter adapterMyOrders;
    private java.util.List<HomeDrawModel> myorder_list = new ArrayList<HomeDrawModel>();
    private Bundle bundle;
    private TextView tvErrorInfo;
    private boolean loadingmore = false;
    private String position = "", cancel_or_reschedule = "";
    private boolean isRefreshrequired = false;
    private FrameLayout frameLayout;


    private PopupWindow mPopup;
    private CustomFontTextView reasonLabel;
    private CustomFontTextView tvReason;
    private CustomFontEditTextView ed_comments;
    private long transactionIdReason;


    public static void newInstance(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ViewAllHomeDrawActivity.class);
        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.viewallhomedraw_activity, frameLayout);
        mContext = this;
        responseCallback = this;
        bundle = getIntent().getBundleExtra("bundle");
        recent_or_upcoming = bundle.getString("recent_or_upcoming");
        findView();
        initView();
        bindView();

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            tvErrorInfo.setVisibility(View.VISIBLE);
            return;
        } else {
//            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_CANCEL));
            /*pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();*/

            showProgressDialog();
            tvErrorInfo.setVisibility(View.GONE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, recent_or_upcoming, myorder_list.size()));
        }


/*[     .....Load More listener on adapter.....     ]*/
        adapterMyOrders.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    if (loadingmore) {
                        myorder_list.add(null);
                        adapterMyOrders.notifyItemInserted(myorder_list.size() - 1);
                        long transactionId = getTransactionId();
                        ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, recent_or_upcoming, myorder_list.size() - 1));
                    }
                } catch (Exception ex) {

                }
            }
        });
/*[     .....End of Load More listener on adapter.....     ]*/
    }


    @Override
    public void onBackPressed() {
        /*[   sets previous activity needs to be refreshed with new data available on server  ]*/
        if (isRefreshrequired) {
            Intent intent = new Intent();
            intent.putExtra("data_refresh", TextConstants.REFRESH_SMALLCASE);
            setResult(TextConstants.REFERESH_CONSTANT, intent);
        }
        /*[   End of sets previous activity needs to be refreshed with new data available on server  ]*/
        super.onBackPressed();
    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rv_viewall = (RecyclerView) findViewById(R.id.rv_viewall);
        tvErrorInfo = (TextView) findViewById(R.id.tv_error_info);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        String title = getResources().getString(R.string.title_view_all);
        if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
            title = getResources().getString(R.string.title_view_all_upcoming);
        } else if (recent_or_upcoming.equals(TextConstants.RECENT)) {
            title = getResources().getString(R.string.title_view_all_recent);
        }

        toolbar_title.setText(title);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));

        myorder_list.clear();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_viewall.setLayoutManager(layoutManager);
        rv_viewall.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        adapterMyOrders = new HomeDrawAdapter(mContext, myorder_list, responseCallback, recent_or_upcoming, rv_viewall);
        rv_viewall.setAdapter(adapterMyOrders);
    }

    private void bindView() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   /* if (pdialog.isShowing()) {
                        pdialog.dismiss();
                    }*/

                    dismissProgressDialog();
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        if (myorder_list.size() > 0) {
                        /*[  ... Loading more handeled if comes an error...  ]*/
                            Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                            if (loadingmore) {
                                if (myorder_list.get(myorder_list.size() - 1) == null) {
                                    myorder_list.remove(myorder_list.size() - 1);
                                    adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                }
                                loadingmore = true;
                                adapterMyOrders.notifyDataSetChanged();
                                adapterMyOrders.setLoaded();
                            } else {
                            }
                        /*[  ... End of Loading more handeled if comes an error...  ]*/
                        } else {
                            tvErrorInfo.setVisibility(View.VISIBLE);
                            tvErrorInfo.setText(response);
                            //mProgressbar.setVisibility(View.GONE);
                        }

                    } else {

                        if (recent_or_upcoming.equals(TextConstants.RECENT) || recent_or_upcoming.equals(TextConstants.UPCOMING)) {

                           /* Gson gson = new Gson();
                            Type listType = new TypeToken<java.util.List<AppointmentDataModel>>() {}.getType();

                            AppointmentDataModel appointmentDataModel = new AppointmentDataModel();
                            try {
                                appointmentDataModel = gson.fromJson(response.toString(), AppointmentDataModel.class);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }*/





                            if (loadingmore) {
                                if (myorder_list.get(myorder_list.size() - 1) == null) {
                                    myorder_list.remove(myorder_list.size() - 1);
                                    adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                }
                            } else {
                            }

                            JSONArray viewAllJsonArray = null ;
                            try
                            {
                                JSONObject jsonObject = new JSONObject(response);
                                viewAllJsonArray = jsonObject.optJSONArray("appointments");

                                if(viewAllJsonArray!=null && viewAllJsonArray.length()>0)
                                {
                                    populateAppointmentList(viewAllJsonArray, (ArrayList<HomeDrawModel>) myorder_list);
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                           /* for (int i = 0; i < appointmentDataModel.getList().size(); i++) {
                                appointment.model.appointmodel.List list_model = appointmentDataModel.getList().get(i);
                                myorder_list.add(list_model);
                            }*/

                           /* if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                                Collections.sort(myorder_list, new AppointmentComparator<appointment.model.appointmodel.List>());
                            }*/

                            if (myorder_list.size() == 0) {
                                tvErrorInfo.setVisibility(View.VISIBLE);
                            } else {
                                tvErrorInfo.setVisibility(View.GONE);
                            }

                             if(viewAllJsonArray!=null && viewAllJsonArray.length()==0)
                             {
                                loadingmore = false;
                             } else {
                                loadingmore = true;
                                adapterMyOrders.notifyDataSetChanged();
                                adapterMyOrders.setLoaded();
                            }
                            //..Keep in mind must to do, to avoid crash of illegal state on recyclerview.....
                        }
                        if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
                            if (position.length() != 0 && cancel_or_reschedule.length() != 0) {
                                Gson gson = new Gson();
                                appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
                                myorder_list.remove(Integer.parseInt(position));
                                adapterMyOrders.notifyDataSetChanged();
                                isRefreshrequired = true;// on cancelling the appointment this variable must be set to true

                                if (loadingmore) {
                                    if (myorder_list.get(myorder_list.size() - 1) == null) {
                                        myorder_list.remove(myorder_list.size() - 1);
                                        adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                    }
                                } else {
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    private void populateAppointmentList(JSONArray jsonArray, ArrayList<HomeDrawModel> list)
    {

        for(int j=0; j<jsonArray.length(); j++)
        {
            try
            {
                JSONObject response = jsonArray.getJSONObject(j);

                HomeDrawModel homeDrawModel = new HomeDrawModel();

                homeDrawModel.setBookedPractitionerFirstName(response.optString("bookedPractitionerFirstName"));
                homeDrawModel.setBookedPractitionerLastName(response.optString("bookedPractitionerLastName"));
                homeDrawModel.setBookedPractitionerPhoneNo(response.optString("bookedPractitionerPhoneNo"));
                homeDrawModel.setAppointmentIdentifier(response.optString("appointmentIdentifier"));
                homeDrawModel.setStartTime(response.optString("startTime"));
                homeDrawModel.setEndDate(response.optString("endTime"));
                homeDrawModel.setAppointmentDate(response.optString("appointmentDate"));
                homeDrawModel.setTimezone(response.optString("timezone"));
                homeDrawModel.setAppointmentStatus(response.optString("appointmentStatus"));
                homeDrawModel.setAssignedPractitionerLatitude(response.optString("assignedPractitionerLatitude"));
                homeDrawModel.setAssignedPractitionerLongitude(response.optString("assignedPractitionerLongitude"));
                homeDrawModel.setId(response.optLong("id"));
                homeDrawModel.setSelectedAddress(response.optString("selectedAddress"));
                homeDrawModel.setSelectedAddressZipCode(response.optString("selectedAddressZipCode"));

                JSONObject bookedPatientJSON = response.optJSONObject("bookedPatient");
                BookedPatientModel bookedPatientModel = new BookedPatientModel();
                if (bookedPatientJSON != null)
                {
                    bookedPatientModel.setFirstName(bookedPatientJSON.optString("firstName"));
                    bookedPatientModel.setLastName(bookedPatientJSON.optString("lastName"));
                    bookedPatientModel.setDob(bookedPatientJSON.optString("dob"));
                    bookedPatientModel.setPhoneNo(bookedPatientJSON.optString("phoneNo"));
                    JSONArray uploadedScriptJSONARRAY = bookedPatientJSON.optJSONArray("bookedPatient");
                    ArrayList<ScriptModel> uploadedScriptList = new ArrayList<>();
                    for (int i = 0; uploadedScriptJSONARRAY != null && i < uploadedScriptJSONARRAY.length(); i++)
                    {
                        ScriptModel scriptModel = new ScriptModel();
                        JSONObject obj = uploadedScriptJSONARRAY.getJSONObject(i);
                        scriptModel.setImageBase64(obj.optString("scriptImage"));
                        scriptModel.setScriptImageMimeType(obj.optString("scriptImageMimeType"));
                        scriptModel.setScriptImageThumbnail(obj.optString("scriptImageThumbnail"));
                        uploadedScriptList.add(scriptModel);
                    }
                    bookedPatientModel.setUploadedScriptsList(uploadedScriptList);
                }
                homeDrawModel.setBookedPatientModel(bookedPatientModel);

                list.add(homeDrawModel);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void cancelOrReschedule(List appointmentmodel, String position, String cancel_or_reschedule) {
        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            return;
        } else {
            this.position = position;
            this.cancel_or_reschedule = cancel_or_reschedule;
            showReasonCommentDialogCommon(appointmentmodel, position, cancel_or_reschedule);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String position = bundle.getString("position");
                    String cancel_or_reschedule = bundle.getString("cancel_or_reschedule");
                    appointment.model.appointmodel.List appointmentmodel = (appointment.model.appointmodel.List) bundle.getParcelable("appointment_model");
                    if (cancel_or_reschedule.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
//                        myorder_list.set(Integer.parseInt(position), appointmentmodel);

                        if (loadingmore) {
                            if (myorder_list.get(myorder_list.size() - 1) == null) {
                                myorder_list.remove(myorder_list.size() - 1);
                                adapterMyOrders.notifyItemRemoved(myorder_list.size());
                            }
                        } else {
                        }
                        /*[.... Sorting applied if cancel or reschedule......]*/
//                        Collections.sort(myorder_list, new AppointmentComparator<appointment.model.appointmodel.List>());
                        adapterMyOrders.notifyDataSetChanged();
                        /*[.... End of Sorting applied if cancel or reschedule......]*/
                        isRefreshrequired = true;// on rescheduling the appointment this variable must be set to true
                    }
                } else if (null != data && resultCode == TextConstants.REFERESH_CONSTANT) {
                    Bundle bundle = data.getExtras();
                    HomeDrawModel appointmentmodel = myorder_list.get(bundle.getInt("position"));
                    myorder_list.remove(appointmentmodel);
//                    Collections.sort(myorder_list, new AppointmentComparator<quest.model.appointmodel.List>());
                    adapterMyOrders.notifyDataSetChanged();
                    isRefreshrequired = true;
                }
                break;
        }
    }


    public void showReasonCommentDialogCommon(final appointment.model.appointmodel.List appointmentmodel, final String position, final String cancel_or_reschedule) {
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.reason_comment_dialog, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(mContext.getResources().getString(R.string.txt_cancel_appointment));
        ed_comments = (CustomFontEditTextView) v.findViewById(R.id.ed_comments);
        reasonLabel = (CustomFontTextView) v.findViewById(R.id.tv_static_reason);
        tvReason = (CustomFontTextView) v.findViewById(R.id.spinnerReasons);


        reasonLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupNames(tvReason);
            }
        });

        tvReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupNames(tvReason);
            }
        });


        ((CustomFontButton) v.findViewById(R.id.cancel_action)).setText(mContext.getResources().getString(R.string.cancel));
        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        ((CustomFontButton) v.findViewById(R.id.continue_action)).setText(mContext.getResources().getString(R.string.txt_ok));
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    alertDialog.dismiss();
                    return;
                } else {
                    String reason;
                    if (tvReason.getText().toString().equals(getResources().getString(R.string.select_one)))
                        Toast.makeText(ViewAllHomeDrawActivity.this, getResources().getString(R.string.reason_for_cancellation), Toast.LENGTH_LONG).show();
                    else {
                        alertDialog.dismiss();
                        reason = tvReason.getText().toString();
                        pdialog = new ProgressDialog(mContext);
                        pdialog.setMessage(getResources().getString(R.string.updating));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                        long transactionId = getTransactionId();
                        ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, responseCallback, appointmentmodel, cancel_or_reschedule, reason, ed_comments.getText().toString().trim()));
                    }
                }
            }
        });
        alertDialog.show();
    }


    @Subscribe
    public void onGetReasonResponse(SearchReasonResponse searchReasonResponse) {

        if (transactionIdReason != searchReasonResponse.getTransactionId())
            return;

        if (searchReasonResponse.isSuccessful()) {
            OrderSummary.getInstance().setCancelReason(searchReasonResponse.getReasonList());
        }
    }

    private void showPopupNames(final View view) {
        int idView = -1;
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        String[] arrayPopupString = null;
        if (view.getId() == R.id.spinnerReasons) {
            idView = R.id.spinnerReasons;
            ArrayList<String> cancelReason = OrderSummary.getInstance().getCancelReason();
            if (cancelReason.size() > 0) {

                arrayPopupString = cancelReason.toArray(new String[cancelReason.size()]);

            } else {
                CharSequence[] arrayPopup = getResources().getTextArray(R.array.appointment_reasons);
                arrayPopupString = new String[arrayPopup.length];
                for (int i = 0; i < arrayPopup.length; i++) {
                    arrayPopupString[i] = arrayPopup[i].toString();
                }
            }
        }
        final StatusAdapter adapter = new StatusAdapter(this, R.layout.text_status, arrayPopupString);

        list.setAdapter(adapter);
        final int finalIdView = idView;
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (finalIdView == R.id.spinnerReasons)
                    tvReason.setText(adapter.getItem(position));

                if (mPopup != null && mPopup.isShowing()) {
                    mPopup.dismiss();
                }
            }
        });
        int width = tvReason.getWidth();
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setFocusable(true);
        mPopup.setOutsideTouchable(true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.showAsDropDown(view, (int) Utils.convertDpToPixel(0, this), -(int) Utils.convertDpToPixel(0, this));
    }

    public long getTransactionIdReason() {
        transactionIdReason = System.currentTimeMillis();
        return transactionIdReason;
    }
}
