package appointment.phlooba.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.phlooba.model.BookedPatientModel;
import appointment.phlooba.model.HomeDrawModel;
import appointment.phlooba.model.ScriptModel;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class HomeDrawResponse extends BaseResponse {

    private HomeDrawModel homeDrawModel;


    public HomeDrawResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        doParse(response);
    }

    public HomeDrawResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }

    private void doParse(JSONObject response) {
        try {
            homeDrawModel = new HomeDrawModel();

            homeDrawModel.setBookedPractitionerFirstName(response.optString("bookedPractitionerFirstName"));
            homeDrawModel.setBookedPractitionerLastName(response.optString("bookedPractitionerLastName"));
            homeDrawModel.setBookedPractitionerPhoneNo(response.optString("bookedPractitionerPhoneNo"));
            homeDrawModel.setAppointmentIdentifier(response.optString("appointmentIdentifier"));
            homeDrawModel.setStartTime(response.optString("startTime"));
            homeDrawModel.setEndDate(response.optString("endTime"));
            homeDrawModel.setAppointmentDate(response.optString("appointmentDate"));
            homeDrawModel.setTimezone(response.optString("timezone"));
            homeDrawModel.setAppointmentStatus(response.optString("appointmentStatus"));
            homeDrawModel.setAssignedPractitionerLatitude(response.optString("assignedPractitionerLatitude"));
            homeDrawModel.setAssignedPractitionerLongitude(response.optString("assignedPractitionerLongitude"));
            homeDrawModel.setId(response.optLong("id"));

            JSONObject booledPatientJSON = response.optJSONObject("bookedPatient");
            BookedPatientModel bookedPatientModel = new BookedPatientModel();
            if (booledPatientJSON != null) {
                bookedPatientModel.setFirstName(booledPatientJSON.optString("firstName"));
                bookedPatientModel.setLastName(booledPatientJSON.optString("lastName"));
                bookedPatientModel.setDob(booledPatientJSON.optString("dob"));
                bookedPatientModel.setPhoneNo(booledPatientJSON.optString("phoneNo"));
                JSONArray uploadedScriptJSONARRAY = booledPatientJSON.optJSONArray("bookedPatient");
                ArrayList<ScriptModel> uploadedScriptList = new ArrayList<>();
                for (int i = 0; uploadedScriptJSONARRAY != null && i < uploadedScriptJSONARRAY.length(); i++) {
                    ScriptModel scriptModel = new ScriptModel();
                    JSONObject obj = uploadedScriptJSONARRAY.getJSONObject(i);
                    scriptModel.setImageBase64(obj.optString("scriptImage"));
                    scriptModel.setScriptImageMimeType(obj.optString("scriptImageMimeType"));
                    scriptModel.setScriptImageThumbnail(obj.optString("scriptImageThumbnail"));
                    uploadedScriptList.add(scriptModel);
                }
                bookedPatientModel.setUploadedScriptsList(uploadedScriptList);
            }
            homeDrawModel.setBookedPatientModel(bookedPatientModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HomeDrawModel getHomeDrawModel() {
        return homeDrawModel;
    }
}