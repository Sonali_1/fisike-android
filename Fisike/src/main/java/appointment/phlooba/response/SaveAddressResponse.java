package appointment.phlooba.response;

import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by neharathore on 11/07/17.
 */

public class SaveAddressResponse extends BaseResponse {

    ArrayList<Address> arrayList;
    public SaveAddressResponse(JSONObject response, long transactionID) {
        super(response, transactionID);
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {

        try {
            JSONArray addressArray = response.optJSONArray("address");
            if (addressArray != null)
                arrayList = new ArrayList<>();
            for (int i = 0; (addressArray != null && i < addressArray.length()); i++) {
                JSONObject addressObject = addressArray.getJSONObject(i);
                Address address = new Address();
                address.setCity(addressObject.optString("city"));
                address.setCountry(addressObject.optString("country"));
                JSONArray lineArray = addressObject.optJSONArray("line");
                ArrayList<String> line = null;
                if (lineArray != null) {
                    line = new ArrayList<>();
                    for (int j = 0; j < lineArray.length(); j++) {
                        line.add(lineArray.optString(j));
                    }
                }

                address.setLine(line);
                address.setPostalCode(addressObject.optString("postalCode"));
                address.setUseCode(addressObject.optString("useCode"));
                address.setState(addressObject.optString("state"));
                address.setText(addressObject.optString("text"));
                address.setLatitude(Double.isNaN(addressObject.optDouble("latitude"))?0.0:addressObject.optDouble("latitude"));
                address.setLongitude(Double.isNaN(addressObject.optDouble("longitude"))?0.0:addressObject.optDouble("longitude"));
                arrayList.add(address);
            }
          } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SaveAddressResponse(Exception exception, long transactionID) {
        super(exception, transactionID);
    }

    public ArrayList<Address> getAddressList()
    {
        return arrayList;
    }
}



