package appointment.phlooba.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.phlooba.model.ObservationAppointmentMo;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class ObservationAppointmentResponse extends BaseResponse {

    private final ArrayList<ObservationAppointmentMo> observations = new ArrayList<>();
    private String syncDate;
    private long totalCount;


    public ObservationAppointmentResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        doParse(response);
    }

    public ObservationAppointmentResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }


    private void doParse(JSONObject response) {
        try {
            JSONArray observationList = response.optJSONArray("list");
            for (int i = 0; observationList != null && i < observationList.length(); i++) {
                JSONObject observationListJSONObject = observationList.getJSONObject(i);
                ObservationAppointmentMo observation = new ObservationAppointmentMo();
                observation.setAccessionNumber(observationListJSONObject.optString("accessionNumber"));
                observation.setCategory(observationListJSONObject.optString("category"));
                observation.setId(observationListJSONObject.optLong("id"));
                observation.setLabName(observationListJSONObject.optString("labName"));
                observation.setOrderId(observationListJSONObject.optString("orderId"));
                observation.setOrderNumber(observationListJSONObject.optString("orderNumber"));
                observation.setOrderingPhysicianName(observationListJSONObject.optString("orderingPhysicianName"));
                observation.setOrganizationId(observationListJSONObject.optString("organizationId"));
                observation.setPatientDob(observationListJSONObject.optString("patientDob"));
                observation.setPatientId(observationListJSONObject.optString("patientId"));
                observation.setPatientName(observationListJSONObject.optString("patientName"));
                observation.setPatientSex(observationListJSONObject.optString("patientSex"));
                observation.setPerformingLocationId(observationListJSONObject.optString("performingLocationId"));
                observation.setPerformingLocationName(observationListJSONObject.optString("performingLocationName"));
                observation.setScheduleDate(observationListJSONObject.optString("scheduleDate"));
                observation.setStatus(observationListJSONObject.optString("status"));
                observation.setOrderingPhysicianReferenceId(observationListJSONObject.optLong("orderingPhysicianReferenceId"));

                observations.add(observation);
            }
            syncDate = response.optString("syncDate");
            totalCount = response.optLong("totalCount");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ObservationAppointmentMo> getObservations() {
        return observations;
    }

    public String getSyncDate() {
        return syncDate;
    }

    public long getTotalCount() {
        return totalCount;
    }
}