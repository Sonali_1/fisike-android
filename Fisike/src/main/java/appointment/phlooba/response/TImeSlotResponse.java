package appointment.phlooba.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.phlooba.model.TimeSlotModel;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class TImeSlotResponse extends BaseResponse {

    private final ArrayList<TimeSlotModel> timeSlotModels = new ArrayList<>();
    private boolean status;
    private String errorMessage;


    public TImeSlotResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        doParse(response);
    }

    public TImeSlotResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }


    private void doParse(JSONObject response) {
        try {
            JSONArray timeSlotList = response.optJSONArray("slotList");
            for (int i = 0; timeSlotList != null && i < timeSlotList.length(); i++) {
                JSONObject observationListJSONObject = timeSlotList.getJSONObject(i);
                TimeSlotModel timeSlotModel = new TimeSlotModel();
                timeSlotModel.setStartTime(observationListJSONObject.optString("startTime"));
                timeSlotModel.setEndTime(observationListJSONObject.optString("endTime"));
                JSONArray array = observationListJSONObject.optJSONArray("slotIdentifier");
                ArrayList<Object> identifierArray = new ArrayList<>();
                for (int j = 0; array != null && j < array.length(); j++) {
                    identifierArray.add(array.get(j));
                }
                timeSlotModel.setSlotIdentifier(identifierArray);
                timeSlotModel.setSlotRequestedDate(observationListJSONObject.optString("slotRequestedDate"));
                timeSlotModel.setStatus(observationListJSONObject.optString("status"));
                timeSlotModels.add(timeSlotModel);
            }

            JSONArray info=response.optJSONArray("info");
            for(int j=0;info!=null && j<info.length();j++)
            {
                JSONObject object=info.getJSONObject(j);
                errorMessage=object.optString("message");
                status=object.optBoolean("status");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<TimeSlotModel> getTimeSlotModels() {
        return timeSlotModels;
    }

    public boolean getStatus() {
        return status;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
}