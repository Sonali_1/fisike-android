package appointment.phlooba.response;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by neharathore on 18/07/17.
 */

public class RegisterAssociatedPatientResponse extends BaseResponse {

    boolean statusResponse=false;
    String messageResponse="";
    String code="";

    public boolean isStatusResponse() {
        return statusResponse;
    }

    public void setStatusResponse(boolean statusResponse) {
        this.statusResponse = statusResponse;
    }

    public String getMessageResponse() {
        return messageResponse;
    }

    public void setMessageResponse(String messageResponse) {
        this.messageResponse = messageResponse;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RegisterAssociatedPatientResponse(JSONObject response, long transactionID) {
        super(response, transactionID);
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {
        try {
            statusResponse=response.optBoolean("status");
            messageResponse=response.optString("message");
            code=response.optString("code");

        } catch (Exception e) {

        }
    }

    public RegisterAssociatedPatientResponse(Exception exception, long transactionID) {
        super(exception, transactionID);
    }

}
