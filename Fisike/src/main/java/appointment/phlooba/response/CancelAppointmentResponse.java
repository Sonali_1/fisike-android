package appointment.phlooba.response;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by raj on 14/07/17.
 */
public class CancelAppointmentResponse extends BaseResponse {

    boolean success;
    public CancelAppointmentResponse(JSONObject response, long transactionID) {
        super(response, transactionID);
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {

        try
        {
            response.optBoolean("success");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public CancelAppointmentResponse(Exception exception, long transactionID) {
        super(exception, transactionID);
    }

    public boolean getSuccessStatus()
    {
        return success;
    }
}



