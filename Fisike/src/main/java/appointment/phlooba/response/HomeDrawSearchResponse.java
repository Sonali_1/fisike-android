package appointment.phlooba.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

import appointment.interfaces.AppointmentResponseCallback;

/**
 * Created by laxmansingh on 8/3/2017.
 */

public class HomeDrawSearchResponse extends BaseResponse {

    public JSONObject response;
    AppointmentResponseCallback responseCallback;
    private  String recent_or_upcoming;


    public HomeDrawSearchResponse(JSONObject response, long transactionId, AppointmentResponseCallback responseCallback,String recent_or_upcoming) {
        super(response, transactionId);
        try {
            this.responseCallback = responseCallback;
            this.response = response;
            this.recent_or_upcoming=recent_or_upcoming;
            mIsSuccessful = true;
            this.responseCallback.passResponse(response.toString(), recent_or_upcoming);
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    public HomeDrawSearchResponse(Exception exception, long transactionId, AppointmentResponseCallback responseCallback,String recent_or_upcoming) {
        super(exception, transactionId);
        this.responseCallback = responseCallback;
        this.recent_or_upcoming=recent_or_upcoming;
        this.responseCallback.passResponse(getMessage(), recent_or_upcoming);

    }

}
