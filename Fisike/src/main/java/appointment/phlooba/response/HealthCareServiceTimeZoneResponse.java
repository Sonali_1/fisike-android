package appointment.phlooba.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONObject;


/**
 * Created by rajantalwar on 7/18/17.
 */

public class HealthCareServiceTimeZoneResponse extends BaseResponse {
    private String timeZone;
    private boolean isSuccess = false;
    private boolean isAddressServicable = true;
    public HealthCareServiceTimeZoneResponse(JSONObject response,
                                             long transactionId) {
        super(response, transactionId);
        doParse(response);
    }

    public HealthCareServiceTimeZoneResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }

    private void doParse(JSONObject response){
        // parse the json response
        try {
            // check whether success or failure
            if (response.toString().contains("success")) {
                if (response.optString("success").equals("true")) {
                    isSuccess = true;
                    timeZone = response.optString("timeZone");
                } else if (response.toString().contains("info")) {
                    isSuccess = false;
                    JSONObject object = response.optJSONArray("info").getJSONObject(0);
                    if (object != null && object.optString("responseStatus").equals("200") &&
                            object.optString("code").equals("A477")) {
                        isAddressServicable = false;
                        AppLog.d("navya", "service not available");
                    }
                } else {
                    isSuccess = false;
                    AppLog.d("navya", "navya bad request");
                }
            }
        } catch(Exception e){

        }
    }

    public String getTimeZone() {
        return timeZone;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isAddressServicable(){
        return isAddressServicable;
    }

}
