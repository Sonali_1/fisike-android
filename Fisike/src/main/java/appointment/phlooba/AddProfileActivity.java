package appointment.phlooba;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.phlooba.fragment.AddProfileFragment;

/**
 * Created by laxmansingh on 6/20/2017.
 */

public class AddProfileActivity extends BaseActivity implements AddProfileFragment.ExitFromActivity{

    private Context mContext;
    private Toolbar mToolbar;
    private FrameLayout frameLayout;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.addprofile_activity_layout, frameLayout);
        mContext = this;
        findView();
        initView();
        bindView();
        AddProfileFragment addProfileFragment = new AddProfileFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_add_profile, addProfileFragment, "addProfileFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.add_profile_title));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
    }

    private void bindView() {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("addProfileFragment");
        if(fragment != null && fragment instanceof AddProfileFragment){
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void patientSuccessfullyLinked() {
        Intent intent = new Intent();
        intent.putExtra("isToReload", true);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}



