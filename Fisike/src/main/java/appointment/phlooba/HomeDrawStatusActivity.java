package appointment.phlooba;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.phlooba.model.BookedPatientModel;
import appointment.phlooba.model.HomeDrawModel;
import appointment.phlooba.model.ScriptModel;
import appointment.model.appointmodel.Extension;
import appointment.model.appointmodel.Value;
import appointment.phlooba.adapter.HomeDrawStatusAdapter;
import appointment.phlooba.request.HomeDrawSearchRequest;
import appointment.phlooba.slidinguppanel.SlidingUpPanelLayout;
import appointment.utils.CommonTasks;
import appointment.utils.DirectionsJSONParser;
import appointment.utils.SimpleDividerItemDecorationHeight;

/**
 * Created by laxmansingh on 3/2/2016.
 */

public class HomeDrawStatusActivity extends BaseActivity implements View.OnClickListener, AppointmentResponseCallback , OnMapReadyCallback {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right,ivToolbarCall;
    private CustomFontTextView tvHelpline;
    private RecyclerView rv_order_status;
    private HomeDrawStatusAdapter adapterOrderStatus;
    private SpannableString instruction_styledString;
    private Typeface font_bold, font_regular;


    private List<HomeDrawModel> list = new ArrayList<HomeDrawModel>();
    private List<Extension> statusList = new ArrayList<Extension>();

    private Bundle bundle;
    public static String scriptImage;
    private String date, status, desc, ref_id, patient = "", phlebotomist = "", phlebotomistMobile, phlebotomistLat, phlebotomistLong, timezone = "";
    private long appointmentID;
    private ProgressDialog pdialog;
    private AppointmentResponseCallback responseCallback;
    private List<Value> valueList = new ArrayList<Value>();;
    private RelativeLayout rlLayoutRecentPopular;
    private View rlMyOrder;
    private FrameLayout frameLayout;

    private SlidingUpPanelLayout mSlidingUpPanelLayout;

    private View mTransparentView;
    private View mWhiteSpaceView;
    private LatLng mLocation;
    private GoogleMap mMap;
    private ArrayList markerPoints = new ArrayList();
    private Marker marker;
    private boolean isAppointmentShowCall;


    public static void newInstance(Context context, Bundle bundle) {
        Intent intent = new Intent(context, HomeDrawStatusActivity.class);
        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.homedrawstatus_activity, frameLayout);
        mContext = this;
        scriptImage = null;
        findView();
        initView();
    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rv_order_status = (RecyclerView) findViewById(R.id.rv_order_status);
        rv_order_status.setOverScrollMode(ListView.OVER_SCROLL_NEVER);


        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // rlMyOrder = (View) findViewById(R.id.my_order_lyt);
        rlLayoutRecentPopular = (RelativeLayout) findViewById(R.id.rel_lyt_recent_popular);

        font_bold = Typeface.createFromAsset(mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_bold));
        font_regular = Typeface.createFromAsset(mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_regular));


        mSlidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.slidingLayout);
        mSlidingUpPanelLayout.setAnchorPoint(0.7f);
        int mapHeight = getResources().getDimensionPixelSize(R.dimen.map_height);
        mSlidingUpPanelLayout.setPanelHeight(mapHeight); // you can use different height here
      //  mSlidingUpPanelLayout.setScrollableView(rv_order_status, mapHeight);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingUpPanelLayout.setElevation(5.0f);
        }

       // mSlidingUpPanelLayout.setPanelSlideListener(this);

        // transparent view at the top of ListView

        // init header view for ListView
        // mTransparentHeaderView = inflater.inflate(R.layout.transparent_header_view, mListView, false);
        // mSpaceView = mTransparentHeaderView.findViewById(R.id.space);




    }

    private void initView() {

        responseCallback = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        ivToolbarCall = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_call);
        tvHelpline = (CustomFontTextView)mToolbar.findViewById(R.id.toolbar_sub_helpline);
        bt_toolbar_right.setVisibility(View.GONE);
        ivToolbarCall.setVisibility(View.VISIBLE);
        tvHelpline.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.track));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));

        rv_order_status.setItemAnimator(null);
        ivToolbarCall.setOnClickListener(this);

        setUpMapIfNeeded();


        if (getIntent().getBundleExtra("bundle").getLong("appointmentID", 0) == 0)
        {
            bundle = getIntent().getBundleExtra("bundle");
            date = bundle.getString("date");
            status = bundle.getString("status");
            desc = bundle.getString("desc");
            ref_id = bundle.getString("ref_id");
            patient = bundle.getString("patient");
            phlebotomist = bundle.getString("phlebotomist");
            phlebotomistMobile = bundle.getString("phlebotomist_phone");
            appointmentID = bundle.getLong("_id");
            timezone = bundle.getString("timezone");


           /* list = bundle.getParcelableArrayList("appointment_list");
            statusList = list.get(0).getExtension();
            for (int j = 0; j < statusList.size(); j++) {
                if (statusList.get(j).getUrl().trim().toString().equalsIgnoreCase("audit")) {
                    valueList = statusList.get(j).getValue();
                    break;
                }
            }

            int valuePostiton = 0;
            for (int j = 0; j < statusList.size(); j++) {
                if (statusList.get(j).getUrl().trim().toString().equalsIgnoreCase("appointmentDetails")) {
                    valuePostiton = j;
                    break;
                }
            }

            phlebotomistMobile = statusList.get(valuePostiton).getValue().get(0).getPractitionerPhoneNo();
            phlebotomistLat = statusList.get(valuePostiton).getValue().get(0).getPractitionerLatitude();
            phlebotomistLong = statusList.get(valuePostiton).getValue().get(0).getPractitionerLongitude();

            if(statusList.get(valuePostiton).getValue().get(0).getScripts()!=null && statusList.get(valuePostiton).getValue().get(0).getScripts().size()!=0)
            {
                scriptImage = statusList.get(valuePostiton).getValue().get(0).getScripts().get(0).getScriptImage();
                scriptThumbImage = statusList.get(valuePostiton).getValue().get(0).getScripts().get(0).getScriptImageThumbnail();
                scriptMimeType = statusList.get(valuePostiton).getValue().get(0).getScripts().get(0).getScriptImageMimeType();
            }

*/


            Value val = new Value();

            if(status.equalsIgnoreCase("confirmed"))
            {
                val.setStatus("Confirmed");

            }
            else if(status.equalsIgnoreCase("pending confirmation"))
            {
                val.setStatus("Pending Confirmation");
            }
            else
                val.setStatus(status);

            val.setActionOn("");
            valueList.add(val);

            if(status.equalsIgnoreCase("confirmed"))
            {
                Value valOnTheWay = new Value();
                valOnTheWay.setStatus("On the way");
                valOnTheWay.setActionOn("");

                Value valArrived = new Value();
                valArrived.setStatus("Arrived");
                valArrived.setActionOn("");

                Value valCompleted = new Value();
                valCompleted.setStatus("Completed");
                valCompleted.setActionOn("");

                valueList.add(valOnTheWay);
                valueList.add(valArrived);
                valueList.add(valCompleted);

            }
            else if(status.equalsIgnoreCase("pending confirmation"))
            {
                Value valConfirmed = new Value();
                valConfirmed.setStatus("Confirmed");
                valConfirmed.setActionOn("");

                Value valOnTheWay = new Value();
                valOnTheWay.setStatus("On the way");
                valOnTheWay.setActionOn("");

                Value valArrived = new Value();
                valArrived.setStatus("Arrived");
                valArrived.setActionOn("");

                Value valCompleted = new Value();
                valCompleted.setStatus("Completed");
                valCompleted.setActionOn("");

                valueList.add(valConfirmed);
                valueList.add(valOnTheWay);
                valueList.add(valArrived);
                valueList.add(valCompleted);


            }

            setAdapter(valueList, date, status, desc, ref_id, phlebotomist, patient, phlebotomistMobile, appointmentID, timezone);

            if(status.equalsIgnoreCase("confirmed") || status.equalsIgnoreCase("on the way"))
                mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            else
                mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

            mSlidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
                @Override
                public void onPanelSlide(View panel, float slideOffset) {
                    Log.i("rag", "onPanelSlide, offset " + slideOffset);
                }

                @Override
                public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                    Log.i("tag", "onPanelStateChanged " + newState);
                }
            });
            mSlidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            });

        } else {
            appointmentID = getIntent().getBundleExtra("bundle").getLong("appointmentID", 0);
            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                return;
            } else {
                //rlMyOrder.setVisibility(View.GONE);
                //rlLayoutRecentPopular.setVisibility(View.GONE);
               /* pdialog = new ProgressDialog(mContext);
                pdialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.loading_txt));
                pdialog.setCanceledOnTouchOutside(false);
                pdialog.show();
*/              isAppointmentShowCall = true;

                showProgressDialog();

                long transactionId = System.currentTimeMillis();
                ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, responseCallback, TextConstants.SINGLE, (int)appointmentID));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_ic_menu_overflow:
                // showPopup(iv_ic_menu_overflow);
                break;

            case R.id.bt_toolbar_call:
                if (!checkPermission(Manifest.permission.CALL_PHONE, ""))
                    return;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + TextConstants.PHLOOBER_HELP_LINE_NO));
                startActivity(intent);

//                animateMarker(mMap, marker, markerPoints, false);
                break;
            default:
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void passResponse(final String response, String recent_or_upcoming) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   /* if (pdialog.isShowing()) {
                        pdialog.dismiss();
                        pdialog = null;
                    }*/

                    dismissProgressDialog();
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(MyApplication.getAppContext().getResources().getString(R.string.unable_load_try_after_sometime));
                        builder.setMessage(response);
                        builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    else
                    {
                        try
                        {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray viewJsonArray = jsonObject.optJSONArray("appointments");

                            if(viewJsonArray!=null && viewJsonArray.length()>0)
                            {
                                populateAppointmentList(viewJsonArray, (ArrayList<HomeDrawModel>) list);
                            }
                            else
                            {
                                if(isAppointmentShowCall)
                                    Toast.makeText(mContext, "Detail not available", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "Script not available", Toast.LENGTH_SHORT).show();

                                return;
                            }

                            if(isAppointmentShowCall)
                            {
                                HomeDrawModel  list_model = list.get(0);

                                status = list.get(0).getAppointmentStatus();

                                Value val = new Value();

                                if(status.equalsIgnoreCase("booked"))
                                {
                                    val.setStatus("Confirmed");

                                }
                                else if(status.equalsIgnoreCase("proposed"))
                                {
                                    val.setStatus("Pending Confirmation");
                                }
                                else
                                    val.setStatus(status);

                                val.setActionOn("");
                                valueList.add(val);

                                if(status.equalsIgnoreCase("booked"))
                                {
                                    Value valOnTheWay = new Value();
                                    valOnTheWay.setStatus("On the way");
                                    valOnTheWay.setActionOn("");

                                    Value valArrived = new Value();
                                    valArrived.setStatus("Arrived");
                                    valArrived.setActionOn("");

                                    Value valCompleted = new Value();
                                    valCompleted.setStatus("Completed");
                                    valCompleted.setActionOn("");

                                    valueList.add(valOnTheWay);
                                    valueList.add(valArrived);
                                    valueList.add(valCompleted);

                                    status = "Confirmed";

                                }
                                else if(status.equalsIgnoreCase("proposed"))
                                {
                                    Value valConfirmed = new Value();
                                    valConfirmed.setStatus("Confirmed");
                                    valConfirmed.setActionOn("");

                                    Value valOnTheWay = new Value();
                                    valOnTheWay.setStatus("On the way");
                                    valOnTheWay.setActionOn("");

                                    Value valArrived = new Value();
                                    valArrived.setStatus("Arrived");
                                    valArrived.setActionOn("");

                                    Value valCompleted = new Value();
                                    valCompleted.setStatus("Completed");
                                    valCompleted.setActionOn("");

                                    valueList.add(valConfirmed);
                                    valueList.add(valOnTheWay);
                                    valueList.add(valArrived);
                                    valueList.add(valCompleted);
                                    status = "Pending Confirmation";
                                }


                                String[]  strings = list_model.getAppointmentDate().toString().trim().split("T");
                                String appointmentDateString = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.mm_dd_yyyy_slashSeperated, strings[0]);                                String startTimeString = CommonTasks.formateDateFromstring(DateTimeUtil.sourceTimeFormat, DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat, list_model.getStartTime().toString().trim());
                                String endTimeString = CommonTasks.formateDateFromstring(DateTimeUtil.sourceTimeFormat, DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat, list_model.getEndDate().toString().trim());

                                SimpleDateFormat ft = new SimpleDateFormat(DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat);
                                Date appointmentTime = null;
                                try
                                {
                                    if(Utils.isValueAvailable(startTimeString))
                                    {
                                        appointmentTime = ft.parse(startTimeString);
                                        appointmentTime.setMinutes(appointmentTime.getMinutes()+30);
                                        startTimeString = ft.format(appointmentTime);
                                    }
                                }
                                catch (ParseException e)
                                {
                                    e.printStackTrace();
                                }

                                date = appointmentDateString+", "+startTimeString+"-"+endTimeString;

                                phlebotomist = list_model.getPractitionerName();

                                desc = list_model.getSelectedAddress();

                                ref_id = list_model.getAppointmentIdentifier();

                                patient = list_model.getBookedPatientModel().getPatientName();

                                phlebotomistMobile = Utils.isValueAvailable(list_model.getBookedPractitionerPhoneNo())?list_model.getBookedPractitionerPhoneNo():"";

                                appointmentID = list_model.getId();

                                timezone = list_model.getTimezone();

                                try
                                {
                                scriptImage = list_model.getBookedPatientModel().getUploadedScriptsList().get(0).getImageBase64();
                                } catch (Exception ex) {
                                    Toast.makeText(mContext, "Script not available", Toast.LENGTH_SHORT).show();
                                }

                                setAdapter(valueList, date, status, desc, ref_id, phlebotomist, patient, phlebotomistMobile, appointmentID, timezone);

                            }
                            else
                            {
                                scriptImage = list.get(0).getBookedPatientModel().getUploadedScriptsList().get(0).getImageBase64();

                                Intent viewScriptIntent = new Intent(mContext, OpenImageFragment.class);
                                viewScriptIntent.putExtra(VariableConstants.DOCUMENT_TYPE, "Script");

                                mContext.startActivity(viewScriptIntent);

                            }

                           /*
                        rlMyOrder.setVisibility(View.VISIBLE);
                        rlLayoutRecentPopular.setVisibility(View.VISIBLE);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List>() {
                        }.getType();

                            appointment.model.appointmodel.List appointmentDataModel = gson.fromJson(response.toString(), appointment.model.appointmodel.List.class);
                            statusList = appointmentDataModel.getExtension();
                            for (int j = 0; j < statusList.size(); j++) {
                                if (statusList.get(j).getUrl().trim().toString().equalsIgnoreCase("audit")) {
                                    valueList = statusList.get(j).getValue();
                                    break;
                                }
                            }
                            status = appointmentDataModel.getStatus();
                            date = appointmentDataModel.getStart().getValue();
                            try {
                                ref_id = appointmentDataModel.getIdentifier().get(0).getValue();
                            } catch (Exception e) {
                                ref_id = null;
                            }


                            String startdate = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy, date);
                            String starttime = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa, date);

                            int valuePostiton = 0;
                            for (int i = 0; i < statusList.size(); i++) {
                                if (statusList.get(i).getUrl().equals("appointmentDetails")) {
                                    valuePostiton = i;
                                    break;
                                }
                            }
                            setAdapter(valueList, starttime + ", " + startdate,
                                    statusList.get(valuePostiton).getValue().get(0).getLocationDetails().toString()
                                    , statusList.get(valuePostiton).getValue().get(0).getServiceName().toString()
                                    , ref_id, phlebotomist, patient, phlebotomistMobile, scriptThumbImage);*/


                        } catch (Exception ex) {
                            if(isAppointmentShowCall)
                                Toast.makeText(mContext, "Detail not available", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(mContext, "Script not available", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }


    private void populateAppointmentList(JSONArray jsonArray, ArrayList<HomeDrawModel> list)
    {

        for(int j=0; j<jsonArray.length(); j++)
        {
            try
            {
                JSONObject response = jsonArray.getJSONObject(j);

                HomeDrawModel homeDrawModel = new HomeDrawModel();

                homeDrawModel.setBookedPractitionerFirstName(response.optString("bookedPractitionerFirstName"));
                homeDrawModel.setBookedPractitionerLastName(response.optString("bookedPractitionerLastName"));
                homeDrawModel.setBookedPractitionerPhoneNo(response.optString("bookedPractitionerPhoneNo"));
                homeDrawModel.setAppointmentIdentifier(response.optString("appointmentIdentifier"));
                homeDrawModel.setStartTime(response.optString("startTime"));
                homeDrawModel.setEndDate(response.optString("endTime"));
                homeDrawModel.setAppointmentDate(response.optString("appointmentDate"));
                homeDrawModel.setTimezone(response.optString("timezone"));
                homeDrawModel.setAppointmentStatus(response.optString("appointmentStatus"));
                homeDrawModel.setAssignedPractitionerLatitude(response.optString("assignedPractitionerLatitude"));
                homeDrawModel.setAssignedPractitionerLongitude(response.optString("assignedPractitionerLongitude"));
                homeDrawModel.setId(response.optLong("id"));
                homeDrawModel.setSelectedAddress(response.optString("selectedAddress"));
                homeDrawModel.setSelectedAddressZipCode(response.optString("selectedAddressZipCode"));

                JSONObject bookedPatientJSON = response.optJSONObject("bookedPatient");
                BookedPatientModel bookedPatientModel = new BookedPatientModel();
                if (bookedPatientJSON != null)
                {
                    bookedPatientModel.setFirstName(bookedPatientJSON.optString("firstName"));
                    bookedPatientModel.setLastName(bookedPatientJSON.optString("lastName"));
                    bookedPatientModel.setDob(bookedPatientJSON.optString("dob"));
                    bookedPatientModel.setPhoneNo(bookedPatientJSON.optString("phoneNo"));
                    JSONArray uploadedScriptJSONARRAY = bookedPatientJSON.optJSONArray("uploadedScripts");
                    ArrayList<ScriptModel> uploadedScriptList = new ArrayList<>();
                    for (int i = 0; uploadedScriptJSONARRAY != null && i < uploadedScriptJSONARRAY.length(); i++)
                    {
                        ScriptModel scriptModel = new ScriptModel();
                        JSONObject obj = uploadedScriptJSONARRAY.getJSONObject(i);
                        scriptModel.setImageBase64(obj.optString("scriptImage"));
                        scriptModel.setScriptImageMimeType(obj.optString("scriptImageMimeType"));
                        scriptModel.setScriptImageThumbnail(obj.optString("scriptImageThumbnail"));
                        uploadedScriptList.add(scriptModel);
                    }
                    bookedPatientModel.setUploadedScriptsList(uploadedScriptList);
                }
                homeDrawModel.setBookedPatientModel(bookedPatientModel);

                list.add(homeDrawModel);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scriptImage = null;
    }

    private void setAdapter(List<Value> valueList, String date, String status, String desc, String ref_id, String phlebotomist, String patient, String phoneNo, long _id, String timezone) {
        adapterOrderStatus = new HomeDrawStatusAdapter(mContext, valueList, date, status, desc, ref_id, phlebotomist, patient, phoneNo, _id, timezone);
        rv_order_status.setLayoutManager(new LinearLayoutManager(mContext));
        rv_order_status.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        rv_order_status.setAdapter(adapterOrderStatus);
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        LatLng latLngSource = new LatLng(40.775045, -73.700924);
        LatLng latLngDestination = new LatLng(40.811304, -73.701267);

        // Adding latlng to the ArrayList
        markerPoints.add(latLngSource);
        markerPoints.add(latLngDestination);

        googleMap.addMarker((new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin)).position(latLngDestination).title("You")));

        marker = googleMap.addMarker((new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.car)).position(latLngSource).title("Phlebotomist")));

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLngSource, 12.5f);
        googleMap.animateCamera(yourLocation);

//      VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();
//      LatLngBounds latLngBounds = new LatLngBounds(latLngSource, latLngDestination);
//      mMap.setLatLngBoundsForCameraTarget(latLngBounds);


        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2)
        {
            LatLng origin = (LatLng) markerPoints.get(0);
            LatLng dest = (LatLng) markerPoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }


//        if(Utils.isValueAvailable(phlebotomistLat) && Utils.isValueAvailable(phlebotomistLong))
//            addLines(latLngSource, latLngDestination);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        // Disconnecting the client invalidates it.
        super.onStop();
    }

    private void addLines(LatLng latLngSource, LatLng latLngDestination) {

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.add(latLngSource);
        polylineOptions.add(latLngDestination);
        mMap.addPolyline(polylineOptions.width(12).color(Color.BLUE).geodesic(true));

    }



    @Override
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule) {

    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }


    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String,String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String,String>>> routes = null;

            try
            {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String,String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String,String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap point = path.get(j);

                    double lat = Double.parseDouble((String)point.get("lat"));
                    double lng = Double.parseDouble((String)point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);

            }

            if(lineOptions != null && mMap!=null)
            {
                // Drawing polyline in the Google Map for the i-th route
                mMap.addPolyline(lineOptions);
                animateMarker(mMap, marker, points,false);
            }
            else
                Toast.makeText(mContext, "Erroe while plotting route on map...", Toast.LENGTH_SHORT).show();

        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException
    {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try
        {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }
        catch (Exception e)
        {
            Log.d("Exception", e.toString());
        }
        finally
        {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public void setAnimation(GoogleMap myMap, final List<LatLng> directionPoint, final Bitmap bitmap) {


        Marker marker = myMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                .position(directionPoint.get(0))
                .flat(true));

        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(0), 10));

        animateMarker(myMap, marker, directionPoint, false);
    }


    private void animateMarker(GoogleMap myMap, final Marker marker, final List<LatLng> directionPoint, final boolean hideMarker)
    {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = myMap.getProjection();
        final long duration = 35000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable()
        {
            int i = 0;

            @Override
            public void run()
            {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                if (i < directionPoint.size())
                {
                    marker.setPosition(directionPoint.get(i));
                    rotateMarker(marker, (float) bearingBetweenLocations(directionPoint.get(i),directionPoint.get(i+1)));

                }
                i++;


                if (t < 1.0)
                {
                    handler.postDelayed(this, 160);
                }
                else
                {
                    if (hideMarker)
                    {
                        marker.setVisible(false);
                    }
                    else
                    {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private double bearingBetweenLocations(LatLng latLng1,LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private boolean isMarkerRotating;

    private void rotateMarker(final Marker marker, final float toRotation) {
        if(!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0)
                    {
                        handler.postDelayed(this, 16);
                    }
                    else
                    {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

}





