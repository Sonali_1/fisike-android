package appointment.phlooba.model;

/**
 * Created by kailashkhurana on 12/07/17.
 */


import com.mphrx.fisike.utils.Utils;

public class HomeDrawModel {

    private String appointmentStatus;
    private String assignedPractitionerLatitude;
    private String assignedPractitionerLongitude;
    private long id;
    private BookedPatientModel bookedPatientModel;

    private String bookedPractitionerFirstName;
    private String bookedPractitionerLastName;
    private String bookedPractitionerPhoneNo;
    private String appointmentIdentifier;
    private String startTime;
    private String endDate;
    private String appointmentDate;
    private String timezone;
    private String bookedPractitionerDescription;

    private String selectedAddress;
    private String selectedAddressZipCode;

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public String getSelectedAddressZipCode() {
        return selectedAddressZipCode;
    }

    public void setSelectedAddressZipCode(String selectedAddressZipCode) {
        this.selectedAddressZipCode = selectedAddressZipCode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getBookedPractitionerFirstName() {
        return bookedPractitionerFirstName;
    }

    public void setBookedPractitionerFirstName(String bookedPractitionerFirstName) {
        this.bookedPractitionerFirstName = bookedPractitionerFirstName;
    }

    public String getBookedPractitionerLastName() {
        return bookedPractitionerLastName;
    }

    public void setBookedPractitionerLastName(String bookedPractitionerLastName) {
        this.bookedPractitionerLastName = bookedPractitionerLastName;
    }

    public String getBookedPractitionerDescription() {
        return bookedPractitionerDescription;
    }

    public void setBookedPractitionerDescription(String bookedPractitionerDescription) {
        this.bookedPractitionerDescription = bookedPractitionerDescription;
    }

    public String getBookedPractitionerPhoneNo() {
        return bookedPractitionerPhoneNo;
    }

    public void setBookedPractitionerPhoneNo(String bookedPractitionerPhoneNo) {
        this.bookedPractitionerPhoneNo = bookedPractitionerPhoneNo;
    }

    public String getAppointmentIdentifier() {
        return appointmentIdentifier;
    }

    public void setAppointmentIdentifier(String appointmentIdentifier) {
        this.appointmentIdentifier = appointmentIdentifier;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPractitionerName() {
        try {
            String name = "";
            if (Utils.isValueAvailable(bookedPractitionerFirstName))
                name = name + bookedPractitionerFirstName;

            if (Utils.isValueAvailable(bookedPractitionerLastName))
                name = name +" "+ bookedPractitionerLastName;

            return name;
        } catch (Exception exception) {
            return "";
        }
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getAssignedPractitionerLatitude() {
        return assignedPractitionerLatitude;
    }

    public void setAssignedPractitionerLatitude(String assignedPractitionerLatitude) {
        this.assignedPractitionerLatitude = assignedPractitionerLatitude;
    }

    public String getAssignedPractitionerLongitude() {
        return assignedPractitionerLongitude;
    }

    public void setAssignedPractitionerLongitude(String assignedPractitionerLongitude) {
        this.assignedPractitionerLongitude = assignedPractitionerLongitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BookedPatientModel getBookedPatientModel() {
        return bookedPatientModel;
    }

    public void setBookedPatientModel(BookedPatientModel bookedPatientModel) {
        this.bookedPatientModel = bookedPatientModel;
    }

    public AppointmentDetailsModel getAppointmentDetailsModel() {
        return appointmentDetailsModel;
    }

    public void setAppointmentDetailsModel(AppointmentDetailsModel appointmentDetailsModel) {
        this.appointmentDetailsModel = appointmentDetailsModel;
    }

    private AppointmentDetailsModel appointmentDetailsModel;

}