package appointment.phlooba.model;

import android.content.Context;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class TimeSlotModel {

    String startTime;
    String endTime;
    String slotRequestedDate;
    String status;
    ArrayList<Object> slotIdentifier;
    boolean isSelected;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSlotRequestedDate() {
        return slotRequestedDate;
    }

    public void setSlotRequestedDate(String slotRequestedDate) {
        this.slotRequestedDate = slotRequestedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Object> getSlotIdentifier() {
        return slotIdentifier;
    }

    public void setSlotIdentifier(ArrayList<Object> slotIdentifier) {
        this.slotIdentifier = slotIdentifier;
    }

    public String getDisplayedTime(Context context)
    {
        try{
        String text="";
        if(Utils.isValueAvailable(startTime))
            text=text+ Utils.formatTimeAccordingToDeviceTimeFormat(tweakSlots(context,startTime), context);
        if(Utils.isValueAvailable(endTime))
            text=text+" - "+Utils.formatTimeAccordingToDeviceTimeFormat(endTime, context);

        return text;}
        catch (Exception e)
        {
            return "";
        }
    }



    public boolean isSlotAvailable()
    {
        if(Utils.isValueAvailable(status) && status.equalsIgnoreCase(TextConstants.FREE_SLOT_TEXT))
            return true;
        else
            return false;
    }

    public boolean isSelected()
    {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected)
    {
        this.isSelected=isSelected;
    }


    public String tweakSlots(Context context, String startTime) {
        String tweakedTime = null;
        SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat);
        Date appointmentTime = null;
        try {
            if (Utils.isValueAvailable(startTime)) {
                appointmentTime = ft.parse(startTime);
                appointmentTime.setMinutes(appointmentTime.getMinutes() + 30);
                tweakedTime = ft.format(appointmentTime);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tweakedTime;
    }
}
