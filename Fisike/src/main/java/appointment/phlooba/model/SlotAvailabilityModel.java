package appointment.phlooba.model;

/**
 * Created by neharathore on 08/07/17.
 */

public class SlotAvailabilityModel {

    String slotTitle;
    boolean isSlotAvailable;
    boolean isSelected;

    public SlotAvailabilityModel(String slotTitle,boolean isSlotAvailable,boolean isSelected)
    {
        this.slotTitle=slotTitle;
        this.isSelected=isSelected;
        this.isSlotAvailable=isSlotAvailable;
    }

    public String getSlotTitle() {
        return slotTitle;
    }

    public void setSlotTitle(String slotTitle) {
        this.slotTitle = slotTitle;
    }

    public boolean getIsSlotAvailable() {
        return isSlotAvailable;
    }

    public void setIsSlotAvailable(boolean isSlotAvailable) {
        this.isSlotAvailable = isSlotAvailable;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
