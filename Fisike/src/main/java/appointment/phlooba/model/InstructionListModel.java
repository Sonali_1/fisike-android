package appointment.phlooba.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neharathore on 11/07/17.
 */

public class InstructionListModel {

    String data;
    ArrayList<String> testName;
    String key;

    public InstructionListModel(String data,String key) {
        this.data = data;
        this.key=key;
    }

   public InstructionListModel(String data, ArrayList<String> testName)
    {
        this.data=data;
        this.testName=testName;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<String> getTestName() {
        return testName;
    }

    public void setTestName(ArrayList<String> testName) {
        this.testName = testName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
