package appointment.phlooba.model;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class ObservationAppointmentMo {
    private String accessionNumber;
    private String category;
    private long id;
    private String labName;
    private String orderId;
    private String orderNumber;
    private String orderingPhysicianName;
    private String organizationId;
    private String patientDob;
    private String patientId;
    private String patientName;
    private String patientSex;
    private String performingLocationId;
    private String performingLocationName;
    //            "reports": [],
    private String scheduleDate;
    private String status;
    private long orderingPhysicianReferenceId;

    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderingPhysicianName() {
        return orderingPhysicianName;
    }

    public void setOrderingPhysicianName(String orderingPhysicianName) {
        this.orderingPhysicianName = orderingPhysicianName;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPerformingLocationId() {
        return performingLocationId;
    }

    public void setPerformingLocationId(String performingLocationId) {
        this.performingLocationId = performingLocationId;
    }

    public String getPerformingLocationName() {
        return performingLocationName;
    }

    public void setPerformingLocationName(String performingLocationName) {
        this.performingLocationName = performingLocationName;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getOrderingPhysicianReferenceId() {
        return orderingPhysicianReferenceId;
    }

    public void setOrderingPhysicianReferenceId(long orderingPhysicianReferenceId) {
        this.orderingPhysicianReferenceId = orderingPhysicianReferenceId;
    }
}
