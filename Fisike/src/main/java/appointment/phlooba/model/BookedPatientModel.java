package appointment.phlooba.model;

import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

/**
 * Created by kailashkhurana on 13/07/17.
 */

public class BookedPatientModel {

    private String firstName;
    private String lastName;
    private String dob;
    private ArrayList<ScriptModel> uploadedScriptsList;
    private String phoneNo;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public ArrayList<ScriptModel> getUploadedScriptsList() {
        return uploadedScriptsList;
    }

    public void setUploadedScriptsList(ArrayList<ScriptModel> uploadedScriptsList) {
        this.uploadedScriptsList = uploadedScriptsList;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPatientName()
    {
        try {
            String name = "";
            if (Utils.isValueAvailable(firstName))
                name = name + firstName;

            if (Utils.isValueAvailable(lastName))
                name = name +" "+lastName;

            return name;
        } catch (Exception exception) {
            return "";
        }
    }
}
