package appointment.phlooba.model;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class ScriptModel {
    private String imageBase64;
    private String scriptImageMimeType;
    private String scriptImageThumbnail;

    public ScriptModel(){

    }

    public ScriptModel(String imageBase64, String scriptImageMimeType, String scriptImageThumbnail){
       this.imageBase64 = imageBase64;
        this.scriptImageMimeType = scriptImageMimeType;
        this.scriptImageThumbnail = scriptImageThumbnail;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String getScriptImageMimeType() {
        return scriptImageMimeType;
    }

    public void setScriptImageMimeType(String scriptImageMimeType) {
        this.scriptImageMimeType = scriptImageMimeType;
    }

    public String getScriptImageThumbnail() {
        return scriptImageThumbnail;
    }

    public void setScriptImageThumbnail(String scriptImageThumbnail) {
        this.scriptImageThumbnail = scriptImageThumbnail;
    }
}
