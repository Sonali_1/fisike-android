package appointment.phlooba.model;

import android.graphics.Bitmap;

/**
 * Created by neharathore on 04/07/17.
 */

public class ImageDataType {
    String title;
    Bitmap image;

    public ImageDataType(String title,Bitmap bitmap){
        this.image=bitmap;
        this.title=title;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }


}
