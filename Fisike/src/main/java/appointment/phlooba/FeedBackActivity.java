package appointment.phlooba;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;

import appointment.phlooba.adapter.RatingResponseListAdapter;
import appointment.utils.SelectableRoundedImageView;
import appointment.phlooba.request.CancelAppointmentRequest;
import appointment.phlooba.response.CancelAppointmentResponse;

/**
 * Created by laxmansingh on 6/23/2017.
 */

public class FeedBackActivity extends BaseActivity implements View.OnClickListener,RatingResponseListAdapter.clickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title, title_header_text;
    private IconTextView bt_toolbar_right;

    RatingBar mRatingBar;
    private CustomFontEditTextView mEt_comment;
    private CustomFontButton mBtn_submit;
    private CustomFontTextView mTv_name;
    private SelectableRoundedImageView mImg_profile;
    private FrameLayout frameLayout;
    private ArrayList<String> listFeedBackButton, listReasonCode;
    private RecyclerView listFeedback;
    RatingResponseListAdapter adapter;
    private String selectedReason="", selectedReasonCode = "", appointmentId, ref_id, landingFrom;
    private int position;
    boolean isOtherSelected=false;
    private AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.feedback_activity, frameLayout);
        mContext = this;

        landingFrom = getIntent().getStringExtra("from");
        position = getIntent().getIntExtra("position",0);

        findView();
        initView();
        bindView();
    }

    public void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);

        mEt_comment = (CustomFontEditTextView) findViewById(R.id.et_comment);
        mBtn_submit = (CustomFontButton) findViewById(R.id.btn_submit);
        mTv_name = (CustomFontTextView) findViewById(R.id.tv_name);
        title_header_text = (CustomFontTextView) findViewById(R.id.tv_static_rate_ur_exp);

        mImg_profile = (SelectableRoundedImageView) findViewById(R.id.img_profile);
        listFeedback= (RecyclerView) findViewById(R.id.comment_list);


    }

    public void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(landingFrom.equalsIgnoreCase("feedback"))
        {
            toolbar_title.setText(getResources().getString(R.string.feedback_title));
            listFeedback.setVisibility(View.GONE);
            listFeedBackButton=new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.feedback_comments)));

        }
        else
        {
            title_header_text.setText(getResources().getString(R.string.select_reason));
            toolbar_title.setText(getResources().getString(R.string.cancel_title));
            mImg_profile.setVisibility(View.GONE);
            mRatingBar.setVisibility(View.GONE);
            mTv_name.setVisibility(View.GONE);
            listFeedBackButton=new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.appointment_reasons)));
            listReasonCode=new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.appointment_reasons_code)));

            appointmentId = getIntent().getStringExtra("_id");
            ref_id = getIntent().getStringExtra("ref_id");

        }

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));

        mImg_profile.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImg_profile.setOval(true);
        mImg_profile.setBackgroundDrawable(getResources().getDrawable(R.drawable.image_practitioner));

        listFeedback.setLayoutManager(new LinearLayoutManager(this));
        adapter=new RatingResponseListAdapter(listFeedBackButton,this);
        adapter.setClickListener(this);
        listFeedback.setAdapter(adapter);

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                if(v>=1)
                {
                    listFeedback.setVisibility(View.VISIBLE);
                }
            }
        });

    }


    public void bindView() {
        mBtn_submit.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_submit:
                if(isOtherSelected && !Utils.isValueAvailable(mEt_comment.getText().toString().trim()))
                    mEt_comment.setError(getString(R.string.enter_feedback_comment));
                else if(!Utils.isValueAvailable(selectedReason))
                    Toast.makeText(mContext, getResources().getString(R.string.select_reason), Toast.LENGTH_SHORT).show();
                else
                {
                    if(landingFrom.equalsIgnoreCase("feedback"))
                        finish();
                    else
                    {
                            dialog = DialogUtils.showOrangeThemeDialog(FeedBackActivity.this, getString(R.string.confirmation_title), getString(R.string.appointment_cancelling_message), getString(R.string.yes), getString(R.string.btn_no), null, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == Dialog.BUTTON_POSITIVE)
                                            {
                                                showProgressDialog();
                                                ThreadManager.getDefaultExecutorService().submit(new CancelAppointmentRequest(getTransactionId(), ref_id, appointmentId , mEt_comment.getText().toString().trim(), selectedReason, selectedReasonCode ));
                                            }
                                            else if (i == Dialog.BUTTON_NEGATIVE)
                                            {
                                                dialogInterface.dismiss();
                                            }
                                        }
                                    });

                            dialog.show();;
                        }
                }
                break;
        }
    }


    @Override
    public void itemClicked(View view, int position)
    {
       switch (view.getId())
       {
           case R.id.title:

           adapter.setSelectedIndex(position);
           selectedReason=adapter.getItemAtPosition(position);
           if(listReasonCode!=null)
             selectedReasonCode = listReasonCode.get(position);

           if (adapter.getItemAtPosition(position).equals(getString(R.string.feedback_other_value)))
           {
               isOtherSelected=true;
               mEt_comment.setText("");
               mEt_comment.setVisibility(View.VISIBLE);
               mEt_comment.requestFocus();
           }
           else
           {
                isOtherSelected=false;
               mEt_comment.setVisibility(View.GONE);
           }

           adapter.notifyDataSetChanged();

        }
    }

    @Subscribe
    public void onCancelAppointmentResponse(CancelAppointmentResponse response)
    {
        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful())
        {
            dismissProgressDialog();
            Intent intent = new Intent();
            intent.putExtra("data_refresh", TextConstants.REFRESH_SMALLCASE);
            intent.putExtra("position", position);
            setResult(TextConstants.REFERESH_CONSTANT, intent);
            finish();

        }
        else
        {
            dismissProgressDialog();
            Toast.makeText(mContext, getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();

        }
    }
}
