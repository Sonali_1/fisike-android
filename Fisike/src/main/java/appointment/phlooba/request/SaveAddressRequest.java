package appointment.phlooba.request;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import java.util.ArrayList;

import appointment.request.SaveAddressRequestJson;
import appointment.phlooba.response.SaveAddressResponse;

/**
 * Created by neharathore on 11/07/17.
 */

public class SaveAddressRequest extends BaseObjectRequest {
    private final long transactionID;
    ArrayList<Address> addressList;
    private long patientId;
    private String json;

    public SaveAddressRequest(long mTransactionId, ArrayList<Address> address,long patientId) {
        this.transactionID = mTransactionId;
        this.addressList=address;
        this.patientId=patientId;

    }

    @Override
    public void doInBackground() {
        String url= MphRxUrl.getAddressSaveUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url,getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SaveAddressResponse(error, transactionID));
    }


    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SaveAddressResponse(response, transactionID));
    }

    public String getPayload() {
        String  json="";

        try {
            SaveAddressRequestJson jsonRequest=new SaveAddressRequestJson();
            jsonRequest.setAddress(addressList);
            jsonRequest.setPatientId(patientId);
             json = new Gson().toJson(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}