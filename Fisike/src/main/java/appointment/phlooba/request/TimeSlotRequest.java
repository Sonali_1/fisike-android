package appointment.phlooba.request;

import android.content.Context;

import com.android.volley.VolleyError;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import appointment.phlooba.response.TImeSlotResponse;
import appointment.utils.CommonTasks;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class TimeSlotRequest extends BaseObjectRequest {
    private Context context;
    private final long mTransactionId;
    private String startDate, dob;
    private String zipCode;

    public TimeSlotRequest(Context context, long transactionId,String zipCode,String startDate, String dob) {
        this.context = context;
        mTransactionId = transactionId;
        this.zipCode = zipCode;
        this.startDate = startDate;
        this.dob = dob;
    }

    public long getmTransactionId() {
        return mTransactionId;
    }

    @Override
    public void doInBackground() {
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                MphRxUrl.getTimeSlotUrl(), getPrimaryPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(reportRequest);
    }

    private String getPrimaryPayLoad() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("slotDate", startDate);
            payload.put("zipCode", zipCode);
            payload.put("patientDob", CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS, dob)+("Z"));
        } catch (Exception ignore) {
        }
        return payload.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new TImeSlotResponse(error, mTransactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new TImeSlotResponse(response, mTransactionId));
    }
}
