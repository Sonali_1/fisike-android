package appointment.phlooba.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.phlooba.response.HealthCareServiceTimeZoneResponse;

/**
 * Created by rajantalwar on 7/18/17.
 */


public class HealthCareServiceTimeZoneRequest extends BaseObjectRequest {
    private long transactionId;
    private String zipCode;
    private Context context;

    public HealthCareServiceTimeZoneRequest(Context context, long transactionId, String zipCode){
        this.context = context;
        this.transactionId = transactionId;
        this.zipCode = zipCode;
    }

    @Override
    public void doInBackground() {
        // api call
        try {
            String baseUrl = MphRxUrl.getHealthCareServiceTimezoneUrl();
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayload(), this, this);
            Network.getGeneralRequestQueue().add(request);
        } catch (Exception JSONException) {

        }
    }

    private String getPayload() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("zipCode", zipCode);
        return jsonObject.toString();
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        BusProvider.getInstance().post(new HealthCareServiceTimeZoneResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        BusProvider.getInstance().post(new HealthCareServiceTimeZoneResponse(response, transactionId));

    }
}

