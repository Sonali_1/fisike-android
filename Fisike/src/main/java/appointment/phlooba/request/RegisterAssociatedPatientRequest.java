package appointment.phlooba.request;

import android.content.Context;

import com.android.volley.VolleyError;
import com.mphrx.fisike.enums.RelationShipEnum;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.phlooba.response.RegisterAssociatedPatientResponse;

/**
 * Created by neharathore on 18/07/17.
 */

public class RegisterAssociatedPatientRequest extends BaseObjectRequest {

    private String dob, firstName, middleName, lastName, gender, signUpSource, relationShip, userId,
                   deviceUId, profilePic;
    private Context context;
    private long transactionId;
    private JSONObject coverage;

    public RegisterAssociatedPatientRequest(Context context, long transactionId, String dob, String firstName, String lastName, String middleName,
                                            String gender,String signUpSource, String relationShip, String userId, String deviceUId, JSONObject coverage,
                                            String profilePic) {
        this.context = context;
        this.transactionId = transactionId;
        this.dob = dob;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.signUpSource = signUpSource;
        this.relationShip = relationShip;
        this.userId = userId;
        this.deviceUId = deviceUId;
        this.coverage = coverage;
        this.gender=gender;
        this.profilePic = profilePic;
    }

    @Override
    public void doInBackground() {

        String url = MphRxUrl.getRegisterAssoiciatedPatientUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new RegisterAssociatedPatientResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new RegisterAssociatedPatientResponse(response, transactionId));

    }


    public String getPayload() {
        JSONObject payload = new JSONObject();
        JSONObject customFields = new JSONObject();
        try {
            payload.put("dob", Utils.getFormattedDate(dob,DateTimeUtil.destinationDateFormatWithoutTime,DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            payload.put("firstName", firstName);
            payload.put("lastName", lastName);
            if(Utils.isValueAvailable(middleName)){
                payload.put("middleName", middleName);
            }
            payload.put("middleName", middleName);
            payload.put("gender", gender);
            payload.put("signUpSource", signUpSource);
            payload.put("relationship", RelationShipEnum.getCodeFromValue(relationShip));
            payload.put("userId", userId);
            customFields.put("deviceUID", deviceUId);
            if(Utils.isValueAvailable(profilePic)){
                customFields.put("userProfilePicture", profilePic);
                customFields.put("userProfilePictureMimeType", "image/jpg");
            }
            //TODO need to add insurance fields
            customFields.put("coverage", coverage);
            payload.put("customFields", customFields);


        } catch (JSONException ignore) {
        }
        return payload.toString();
    }
}
