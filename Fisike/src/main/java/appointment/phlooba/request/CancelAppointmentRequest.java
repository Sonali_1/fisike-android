package appointment.phlooba.request;

import com.android.volley.VolleyError;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.phlooba.response.CancelAppointmentResponse;

/**
 * Created by raj on 14/07/17.
 */

public class CancelAppointmentRequest extends BaseObjectRequest {

    private final long transactionID;

    private String appointmentIdentifier, appointmentReferenceId, comment, reason, reasonCode;


    public CancelAppointmentRequest(long mTransactionId, String appointmentIdentifier, String appointmentReferenceId, String comment, String reason, String reasonCode) {
        this.transactionID = mTransactionId;
        this.appointmentIdentifier = appointmentIdentifier;
        this.appointmentReferenceId = appointmentReferenceId;
        this.comment = comment;
        this.reason = reason;
        this.reasonCode = reasonCode;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getCancelAppointmentUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CancelAppointmentResponse(error, transactionID));
    }


    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new CancelAppointmentResponse(response, transactionID));
    }

    public String getPayload() {
        JSONObject payload = new JSONObject();
        JSONObject reasonJsonObject = new JSONObject();
        try
        {
            reasonJsonObject.put("code", reasonCode);
            reasonJsonObject.put("text", reason);
            payload.put("reason", reasonJsonObject);
            payload.put("comment", comment);
            payload.put("appointmentIdentifier", appointmentIdentifier);
            payload.put("appointmentReferenceId", appointmentReferenceId);
            payload.put("appointmentType","GENERAL_PATHOLOGY");
        }
        catch (JSONException ignore)
        {
        }
        return payload.toString();
    }
}
