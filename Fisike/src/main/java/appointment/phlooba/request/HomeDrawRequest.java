package appointment.phlooba.request;

import android.content.Context;

import com.android.volley.VolleyError;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.phlooba.model.ScriptModel;
import appointment.phlooba.response.HomeDrawResponse;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class HomeDrawRequest extends BaseObjectRequest {
    private final long mTransactionId;
    private ArrayList<ScriptModel> scriptList;
    private String startTime;
    private String endTime;
    private String appointmentDate;
    private ArrayList<Object> bookedSlotIdentifierList;
    private String scriptImageType;
    private String bookedPatientId;
    private String referringPhysicianName;
    private String address;
    private String postalCode;
    private long referringPhysicianReferenceId;
    private Context mContext;

    private String timezone;
    private double lat, lng;
    private long coverageId;


    public HomeDrawRequest(Context context, long transactionId, ArrayList<ScriptModel> scriptList, String startTime,
                           String endTime, String appointmentDate, ArrayList<Object> bookedSlotIdentifierList,
                           String address, String scriptImageType, String bookedPatientId, String referringPhysicianName,
                           long referringPhysicianReferenceId, String postalCode, double lat, double lng, String timezone, long coverageId) {
        this.mContext = context;
        mTransactionId = transactionId;
        this.scriptList = scriptList;
        this.startTime = startTime;
        this.endTime = endTime;
        this.appointmentDate = appointmentDate;
        this.bookedSlotIdentifierList = bookedSlotIdentifierList;
        this.address = address;
        this.scriptImageType = scriptImageType;
        this.bookedPatientId = bookedPatientId;
        this.referringPhysicianName = referringPhysicianName;
        this.referringPhysicianReferenceId = referringPhysicianReferenceId;
        this.postalCode = postalCode;
        this.lat = lat;
        this.lng = lng;
        this.timezone = timezone;
        this.coverageId = coverageId;
    }

    public long getmTransactionId() {
        return mTransactionId;
    }

    @Override
    public void doInBackground() {
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                MphRxUrl.getBookAppointmentUrl(), getPrimaryPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(reportRequest);

    }

    private String getPrimaryPayLoad() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("startTime", startTime);
            payload.put("endTime", endTime);
            payload.put("appointmentDate", appointmentDate);
            JSONArray bookedSlotIdentifierArray = new JSONArray();
            for (int i = 0; bookedSlotIdentifierList != null && i < bookedSlotIdentifierList.size(); i++) {
                bookedSlotIdentifierArray.put(i, bookedSlotIdentifierList.get(i));
            }
            payload.put("bookedSlotIdentifier", bookedSlotIdentifierArray);
            payload.put("scriptImageType", scriptImageType);
            payload.put("bookedPatientId", bookedPatientId);
            payload.put("selectedAddress", address);
            payload.put("selectedAddressZipCode", postalCode);

            JSONArray scriptArray = new JSONArray();
            for (int i = 0; scriptList != null && i < scriptList.size(); i++) {
                JSONObject scriptModel = new JSONObject();
                scriptModel.put("scriptImage", scriptList.get(i).getImageBase64());
                scriptModel.put("scriptImageMimeType", scriptList.get(i).getScriptImageMimeType());
                scriptArray.put(scriptModel);
            }
            payload.put("scripts", scriptArray);
            if (Utils.isValueAvailable(referringPhysicianName)) {
                payload.put("referringPhysicianName", referringPhysicianName);
            }
            if (referringPhysicianReferenceId >= 0) {
                payload.put("referringPhysicianReferenceId", referringPhysicianReferenceId);
            }

            //Newly added fiels
            payload.put("appointmentType", "GENERAL PATHOLOGY");
            payload.put("timezone", timezone);

            JSONObject latLngJsonObject = new JSONObject();
            latLngJsonObject.put("latitude", lat);
            latLngJsonObject.put("longitude", lng);

            payload.put("selectedAddressCoordinates", latLngJsonObject);
            JSONArray coverageIds = new JSONArray();
            coverageIds.put(coverageId + "");
            payload.put("coverageIds", coverageIds);
            payload.put("assignedPractitionerProfilePictureType", "THUMBNAIL");
        } catch (Exception ignore) {
        }
        return payload.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new HomeDrawResponse(error, mTransactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new HomeDrawResponse(response, mTransactionId));
    }
}
