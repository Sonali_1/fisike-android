package appointment.phlooba.request;

/**
 * Created by kailashkhurana on 12/07/17.
 */

import com.android.volley.VolleyError;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.phlooba.response.ObservationAppointmentResponse;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class ObservationApointmentRequest extends BaseObjectRequest {

    private final long mTransactionId;
    private long patientId;
    public ObservationApointmentRequest(long transactionId,long patientId) {
        mTransactionId = transactionId;
        this.patientId=patientId;
    }

    public long getmTransactionId() {
        return mTransactionId;
    }

    @Override
    public void doInBackground() {
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                MphRxUrl.getObservationAppointment(), getPrimaryPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(reportRequest);
    }

    private String getPrimaryPayLoad() {
        JSONObject payload = new JSONObject();
        try {
            JSONObject constraints = new JSONObject();
            constraints.put("_include", "DiagnosticOrder:orderer");
            constraints.put("_sort:desc", "dateCreated");
            constraints.put("status", "Pending");
            constraints.put("_count", 3);
            constraints.put("authParams", "showSelf");
            constraints.put("subject",patientId);
            payload.put("constraints", constraints);

        } catch (JSONException ignore) {
        }
        return payload.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new ObservationAppointmentResponse(error, mTransactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new ObservationAppointmentResponse(response, mTransactionId));
    }

}
