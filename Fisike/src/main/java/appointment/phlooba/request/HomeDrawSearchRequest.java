package appointment.phlooba.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.phlooba.response.HomeDrawSearchResponse;

/**
 * Created by laxmansingh on 3/11/2016.
 */
public class HomeDrawSearchRequest extends BaseObjectRequest {

    private long mTransactionId;
    AppointmentResponseCallback responseCallback;
    private UserMO userMO;
    private String recent_or_upcoming;
    private int skip_count;


    public HomeDrawSearchRequest(long transactionId, AppointmentResponseCallback responseCallback, String recent_or_upcoming, int skip_count) {
        mTransactionId = transactionId;
        this.responseCallback = responseCallback;
        this.recent_or_upcoming = recent_or_upcoming;
        this.skip_count = skip_count;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getfetchAppointmentsUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayload() {

        userMO = SettingManager.getInstance().getUserMO();
        long patientid = userMO.getPatientId();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String formattedDate = df.format(c.getTime());

        JSONObject payLoad = new JSONObject();
        JSONObject constraint = new JSONObject();

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray();
            jsonArray.put(0, patientid);
            List linkedPatientsList = userMO.getLinkedPatients();
            for (int i = 0; i < linkedPatientsList.size(); i++) {
                jsonArray.put(i + 1, linkedPatientsList.get(i));
            }
        } catch (Exception ex) {
        }

        try {
            AppLog.showInfo("allpatients", jsonArray.toString());
            /*[   key part for fetching recent,upcoming and both for getting appointmentsearch data  ]*/
            if (recent_or_upcoming.equals(TextConstants.RECENT)) {
                constraint.put("_query", "{\"participant\":{$elemMatch:{\"actorPatient\":"+ patientid +",\"type.text.value\":\"AUT\"}}}");
                constraint.put("status", "fulfilled,cancelled");
                constraint.put("_count", 10);
                constraint.put("_skip", skip_count);
                constraint.put("_sort:desc", "start.value");
            } else if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                constraint.put("_query", "{\"participant\":{$elemMatch:{\"actorPatient\":"+ patientid +",\"type.text.value\":\"AUT\"}}}");
                constraint.put("status", "booked,proposed");
                constraint.put("_count", 10);
                constraint.put("_sort:desc", "start.value");
                constraint.put("_skip", skip_count);
            } else if (recent_or_upcoming.equals(TextConstants.BOTH)) {
                constraint.put("_query", "{\"participant\":{$elemMatch:{\"actorPatient\":"+ patientid +",\"type.text.value\":\"AUT\"}}}");
                constraint.put("_count", "2");
                constraint.put("source", "APPOINTMENT_USER_BOOKER");
                constraint.put("_sort", "start.value");
            }
            else if (recent_or_upcoming.equals(TextConstants.SINGLE)) {
                constraint.put("_id", skip_count);
                constraint.put("scriptImageType","ORIGINAL");
            }

            /*[  End of key part for fetching recent,upcoming and both for getting appointmentsearch data  ]*/
            payLoad.put("constraints", constraint);
//            payLoad.put("totalCount", 0);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
        }

        return payLoad.toString();
    }



    private String getPayloadLegacy() {
        userMO = SettingManager.getInstance().getUserMO();
        long patientid = userMO.getPatientId();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String formattedDate = df.format(c.getTime());

        JSONObject payLoad = new JSONObject();
        JSONObject constraint = new JSONObject();

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray();
            jsonArray.put(0, patientid);
            List linkedPatientsList = userMO.getLinkedPatients();
            for (int i = 0; i < linkedPatientsList.size(); i++) {
                jsonArray.put(i + 1, linkedPatientsList.get(i));
            }
        } catch (Exception ex) {
        }

        try {
            AppLog.showInfo("allpatients", jsonArray.toString());
            /*[   key part for fetching recent,upcoming and both for getting appointmentsearch data  ]*/
            if (recent_or_upcoming.equals(TextConstants.RECENT)) {
                try {
                    constraint.put("_query", "{\"$or\":[{\"status.value\":\"cancelled\"},{\"start.value:date\":{$lt:\"" + formattedDate + "\"}}],\"participant.actorPatient\":{\"$in\":" + jsonArray + "}}");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                constraint.put("_count", 10);
                constraint.put("_skip", skip_count);
                constraint.put("_sort:desc", "start.value");
            } else if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                constraint.put("_query", "{\"participant.actorPatient\":{\"$in\":" + jsonArray + "},\"status.value\":{\"$nin\":[\"cancelled\"]}}");
                try {
                    constraint.put("date", URLEncoder.encode(">" + formattedDate, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                constraint.put("_count", 10);
                constraint.put("_sort:asc", "start.value");
                constraint.put("_skip", skip_count);
            } else if (recent_or_upcoming.equals(TextConstants.BOTH)) {
                constraint.put("_query", "{\"participant.actorPatient\":{\"$in\":" + jsonArray + "}}");
                constraint.put("_skip", 0);
                constraint.put("_sort:desc", "start.value");
            }
            /*[  End of key part for fetching recent,upcoming and both for getting appointmentsearch data  ]*/
            payLoad.put("constraints", constraint);
            payLoad.put("totalCount", 0);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
        }

        return payLoad.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new HomeDrawSearchResponse(error, mTransactionId, responseCallback, recent_or_upcoming));
    }

    @Override
    public void onResponse(JSONObject response) {
        BusProvider.getInstance().post(new HomeDrawSearchResponse(response, mTransactionId, responseCallback, recent_or_upcoming));
    }
}