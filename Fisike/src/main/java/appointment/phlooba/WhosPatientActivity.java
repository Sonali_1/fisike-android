package appointment.phlooba;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import appointment.phlooba.fragment.SelectInsuranceFragment;
import appointment.phlooba.model.HomeDrawModel;
import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.model.ObservationAppointmentMo;
import appointment.phlooba.model.TimeSlotModel;
import appointment.phlooba.fragment.BookHomeDrawFragment;
import appointment.phlooba.fragment.ScheduleFragment;
import appointment.phlooba.fragment.SummaryFragment;
import appointment.phlooba.fragment.ThankYouFragment;
import appointment.phlooba.fragment.UploadScriptFragment;
import appointment.phlooba.fragment.WhosPatientFragment;

/**
 * Created by laxmansingh on 6/19/2017.
 */

public class WhosPatientActivity extends BaseActivity implements UploadScriptFragment.UploadedScript, ScheduleFragment.SaveSlotInterface, SummaryFragment.HomeDrawDataSet ,SelectInsuranceFragment.saveSelectedCoverage{

    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private static TextView toolbar_title;
    private ImageButton buttonBack;
    public static IconTextView buttonright, ivToolbarCall;
    private static CustomFontTextView tvHelpline;
    public static List<Fragment> list_fragments = new ArrayList<Fragment>();
    private Address address;
    public LinkedPatientInfoMO selectedPatientInfoMo;
    ObservationAppointmentMo selectedPhysicianMo; //selected physician Mo can be null if notselected any or do not have any in the list
    // add null checks during the implementation
    public ArrayList<Bitmap> uploadedScriptList = new ArrayList<Bitmap>();
    private TimeSlotModel timeSlotModel;
    private HomeDrawModel bookedAppointmentHomeDrawModel;
    private String timeZone;
    private Coverage coverage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list_fragments.clear();
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_patient_info, frameLayout);
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            toolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ivToolbarCall = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_call);
        tvHelpline = (CustomFontTextView)toolbar.findViewById(R.id.toolbar_sub_helpline);
        toolbar_title.setText(getResources().getString(R.string.whospatient_title));
        buttonBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        buttonright = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        buttonBack.setVisibility(View.GONE);
        buttonright.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        showHideToolbar(false);
        Bundle bundle = getIntent().getExtras();
        bundle.putBoolean(VariableConstants.FROM_PATIENT_PROFILE_VIEW, false);
        WhosPatientFragment whosPatientFragment = new WhosPatientFragment();
        whosPatientFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentPatientInfo, whosPatientFragment, "whosfatientfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


    @Override
    public void onBackPressed() {
        Utils.hideKeyboard(WhosPatientActivity.this);
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            /*[   Think if the fragment is successfragment we hav to finish otherwise traverse backstack of fragments on backpressed  ]*/
            Fragment currFragment = null;
            if (list_fragments.size() > 0) {
                list_fragments.remove(list_fragments.size() - 1);
                if (list_fragments.size() > 0) {
                    currFragment = list_fragments.get(list_fragments.size() - 1);
                }
            }

            if (currFragment != null && currFragment instanceof WhosPatientFragment) {
                setToolbarTitleText(getResources().getString(R.string.whospatient_title));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            if (currFragment != null && currFragment instanceof BookHomeDrawFragment) {
                setToolbarTitleText(getResources().getString(R.string.book_home_draw_title));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            if (currFragment != null && currFragment instanceof ThankYouFragment) {
                setToolbarTitleText(getResources().getString(R.string.thankyou_title));
                ivToolbarCall.setVisibility(View.VISIBLE);
                tvHelpline.setVisibility(View.VISIBLE);
            }

            if (currFragment != null && currFragment instanceof UploadScriptFragment) {
                setToolbarTitleText(getResources().getString(R.string.do_you_have_script));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            if (currFragment != null && currFragment instanceof ScheduleFragment) {
                setToolbarTitleText(getResources().getString(R.string.what_is_your_schedule));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            if (currFragment != null && currFragment instanceof SummaryFragment) {
                finish();
                setToolbarTitleText(getResources().getString(R.string.booking_summary));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            if (currFragment != null && currFragment instanceof SelectInsuranceFragment) {
                setToolbarTitleText(getResources().getString(R.string.insurance_card));
                tvHelpline.setVisibility(View.GONE);
                ivToolbarCall.setVisibility(View.GONE);
            }

            getSupportFragmentManager().popBackStack();

/*[   End of Thinking if the fragment is successfragment we have to finish otherwise traverse backstack of fragments on backpressed  ]*/
        } else {
            list_fragments.clear();
            finish();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment currFragment = null;
        
        if (list_fragments.size() > 0) {
            if (list_fragments.size() > 0) {
                currFragment = list_fragments.get(list_fragments.size() - 1);
            }

            if (currFragment != null && currFragment instanceof WhosPatientFragment) {
                currFragment.onActivityResult(requestCode, resultCode, data);
            }

            if (currFragment != null && currFragment instanceof BookHomeDrawFragment) {
                currFragment.onActivityResult(requestCode, resultCode, data);
            }

            if (currFragment != null && currFragment instanceof UploadScriptFragment) {
                currFragment.onActivityResult(requestCode, resultCode, data);
            }

            if (currFragment != null && currFragment instanceof SummaryFragment) {
                currFragment.onActivityResult(requestCode, resultCode, data);
            }


        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public static void setToolbarTitleText(String title) {
        toolbar_title.setText(title.trim());
    }

    public static void showHelplineToolbar(){
        ivToolbarCall.setVisibility(View.VISIBLE);
        tvHelpline.setVisibility(View.VISIBLE);
    }

    public void saveAddress(Address address) {
        this.address = address;
    }

    public void setSelectedPatient(LinkedPatientInfoMO linkedPatientInfoMO) {
        selectedPatientInfoMo = linkedPatientInfoMO;
    }

    public String getSelectedPatientName() {
        if (selectedPatientInfoMo != null)
            return selectedPatientInfoMo.getName();
        return "";
    }

    public void setSelectedRefferingPhysicianMo(ObservationAppointmentMo observationMo) {
        this.selectedPhysicianMo = observationMo;
    }

    public ObservationAppointmentMo getSelectedRefferingPhysicianMo() {
        return this.selectedPhysicianMo;
    }


    @Override
    public void saveUploadedScript(ArrayList<Bitmap> uploadedScriptList) {
        this.uploadedScriptList = uploadedScriptList;
    }

    public String getDateTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone){
        this.timeZone = timeZone;
    }

    public String getAddressPostalCode() {
        if (address != null && address.getPostalCode() != null)
            return address.getPostalCode();

        return "";
    }


    @Override
    public void saveTimeSlotModel(TimeSlotModel model) {
        this.timeSlotModel = model;
    }

    public Address getSavedAddress() {
        return address;
    }

    public TimeSlotModel getSelectedTimeSlotModel() {
        return timeSlotModel;
    }

    @Override
    public void setHomeDrawMode(HomeDrawModel model) {
        this.bookedAppointmentHomeDrawModel = model;
    }

    @Override
    public HomeDrawModel getHomeDrawModel() {
        return bookedAppointmentHomeDrawModel;
    }

    public LinkedPatientInfoMO getSelectedPatientInfoMo() {
        return selectedPatientInfoMo;
    }


    @Override
    public void saveCoverage(Coverage coverage) {
        this.coverage=coverage;
    }

    public Coverage getCoverage() {
        return coverage;
    }
}
