package appointment.phlooba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;

import java.util.ArrayList;

/**
 * Created by neharathore on 06/07/17.
 */

public class RatingResponseListAdapter  extends RecyclerView.Adapter<RatingResponseListAdapter.supportViewHolder> {
    Context context;
    private String title;
    private ArrayList<String> ListData;
    public RatingResponseListAdapter.clickListener listener;
    private int selectedIndex=-1;

    public RatingResponseListAdapter(ArrayList<String> listData,Context context) {
        // TODO Auto-generated constructor stub
        this.ListData = listData;
        this.context=context;

    }

    //view holder for RatingResponseListAdapter
    public class supportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontButton tvTitle;

        public supportViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontButton) v.findViewById(R.id.title);
            this.tvTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                //handling Item Click
                listener.itemClicked(v, getAdapterPosition());
            }
        }
    }

    @Override
    public int getItemCount() {
        return ListData.size();
    }

    @Override
    public void onBindViewHolder(RatingResponseListAdapter.supportViewHolder arg0, int position) {
        arg0.tvTitle.setText(ListData.get(position));

        if(position==selectedIndex)
        {
            arg0.tvTitle.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rating_selected));
            arg0.tvTitle.setTextColor(context.getResources().getColor(R.color.white));
        }
        else
        {

            arg0.tvTitle.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.btn_stroke_cancel));
            arg0.tvTitle.setTextColor(context.getResources().getColor(R.color.grey_blue));

        }

    }

    @Override
    public RatingResponseListAdapter.supportViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.feeback_row, arg0, false);
        RatingResponseListAdapter.supportViewHolder settingHolder = new RatingResponseListAdapter.supportViewHolder(row);
        return settingHolder;
    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }


    public void setClickListener(RatingResponseListAdapter.clickListener listener) {
        // TODO Auto-generated method stub
        this.listener = listener;

    }

    public String getItemAtPosition(int position)
    {
        return  ListData.get(position);
    }

    public void setSelectedIndex(int position)
    {
        selectedIndex=position;
    }
}
