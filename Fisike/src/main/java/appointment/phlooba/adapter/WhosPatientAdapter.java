package appointment.phlooba.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;

import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.ViewProfileActivity;
import appointment.phlooba.WhosPatientActivity;
import appointment.phlooba.fragment.BookHomeDrawFragment;
import appointment.profile.ViewAssociatedPatientActivity;
import appointment.profile.ViewUpdateSelfProfileActivity;
import appointment.utils.SelectableRoundedImageView;

/**
 * Created by laxmansingh on 6/19/2017.
 */

public class WhosPatientAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private java.util.List<LinkedPatientInfoMO> list;
    private boolean from;

    /*[  .....loadmore variables needed.....   ]*/
    private final int VIEW_TYPE_DOC_SERVICES = 0;
    private final int VIEW_TYPE_ITEM = 1;
    /*[  .....End of loadmore variables needed.....   ]*/


    public WhosPatientAdapter(Context context, final java.util.List<LinkedPatientInfoMO> list, boolean isFromProfiles) {
        this.list = list;
        this.mContext = context;
        this.from = isFromProfiles;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.whospatient_list_item, null);
            WhosPatientAdapter.WhosPatientViewHolder whosPatientViewHolder = new WhosPatientAdapter.WhosPatientViewHolder(view, mContext);
            return whosPatientViewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int position) {

        if (position == 0) {
            ((WhosPatientViewHolder) customViewHolder).temp_view.setVisibility(View.VISIBLE);
        } else {
            ((WhosPatientViewHolder) customViewHolder).temp_view.setVisibility(View.GONE);
        }


        if (customViewHolder instanceof WhosPatientAdapter.WhosPatientViewHolder) {
            LinkedPatientInfoMO linkedPatientInfoMO = list.get(position);
            ((WhosPatientViewHolder) customViewHolder).tv_name.setText(linkedPatientInfoMO.getName());

            if (linkedPatientInfoMO.getProfilePic() != null && !linkedPatientInfoMO.getProfilePic().equalsIgnoreCase("null")) {
                byte[] imageBytes = Base64.decode(linkedPatientInfoMO.getProfilePic(), Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ((WhosPatientViewHolder) customViewHolder).img_profile.setImageBitmap(decodedImage);
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class WhosPatientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_name;
        SelectableRoundedImageView img_profile;
        FrameLayout temp_view;

        public WhosPatientViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setOnClickListener(this);
            tv_name = (CustomFontTextView) view.findViewById(R.id.tv_name);
            img_profile = (SelectableRoundedImageView) view.findViewById(R.id.img_profile);
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            if (!from) {
                Bundle bundle = new Bundle();
                BookHomeDrawFragment bookHomeDrawFragment = new BookHomeDrawFragment();
                bookHomeDrawFragment.setArguments(bundle);
                ((WhosPatientActivity) mContext).setSelectedPatient(list.get(adapter_position));
                ((WhosPatientActivity) mContext).getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentPatientInfo, bookHomeDrawFragment, "bookhomedrawfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            } else {
                Bundle bundle = new Bundle();
                Intent i;
                LinkedPatientInfoMO model = list.get(adapter_position);
                if (!(Utils.isValueAvailable(model.getRelationshipWithUser()) &&
                        model.getRelationshipWithUser().equalsIgnoreCase(mContext.getString(R.string.relationship_self_key)))) {
                    bundle.putSerializable("linkedPatientInfo", model);
                    i = new Intent(mContext, ViewAssociatedPatientActivity.class);
                    i.putExtras(bundle);
                } else {
                    i = new Intent(mContext, ViewUpdateSelfProfileActivity.class);
                    i.putExtra("isFromProfile",true);
                }
                mContext.startActivity(i);
            }


        }


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }


    public static Bitmap getRoundedCornerBitmap(Context context, Bitmap bitmap, float left, float top, float right, float bottom) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);


        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return output;
    }
}
