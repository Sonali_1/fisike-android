package appointment.phlooba.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

import appointment.phlooba.model.InstructionListModel;

/**
 * Created by neharathore on 11/07/17.
 */

public class InstructionsAdapter extends RecyclerView.Adapter<InstructionsAdapter.InstructionViewHolder> {

    ArrayList<InstructionListModel> data;
    Context context;
    clickListener listener;

    public InstructionsAdapter(Context context, ArrayList<InstructionListModel> data, clickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public InstructionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(context).inflate(R.layout.instruction_row, parent, false);
        InstructionsAdapter.InstructionViewHolder viewHolder = new InstructionsAdapter.InstructionViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(InstructionViewHolder holder, final int position) {


        String text = data.get(position).getData();
        final String key = data.get(position).getKey();
        if (!Utils.isValueAvailable(key))
            holder.txtTitle.setText(text);
        else {
            if (Utils.isValueAvailable(text)) {
                try {
                    Spannable wordtoSpan = new SpannableString(text);
                    wordtoSpan.setSpan(new ClickableSpan() {
                        @Override
                        public void onClick(View v) {

                            if (listener != null) {
                                listener.itemClicked(position, key);
                            }

                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            super.updateDrawState(ds);
                            ds.setColor(Color.BLUE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                ds.setColor(context.getColor(R.color.ui_theme_color));
                            }

                            ds.setUnderlineText(true);
                        }
                    }, text.indexOf(key), text.indexOf(key) + key.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    holder.txtTitle.setText(wordtoSpan);
                    holder.txtTitle.setMovementMethod(LinkMovementMethod.getInstance());
                } catch (Exception e) {
                    holder.txtTitle.setText(text);
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class InstructionViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextView txtTitle;

        public InstructionViewHolder(View itemView) {
            super(itemView);
            txtTitle = (CustomFontTextView) itemView.findViewById(R.id.txt_title);
        }
    }

    public interface clickListener {
        public void itemClicked(int position, String text);
    }
}
