package appointment.phlooba.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.appointmodel.Value;
import appointment.phlooba.request.HomeDrawSearchRequest;
import appointment.phlooba.HomeDrawStatusActivity;
import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 3/2/2016.
 */
public class HomeDrawStatusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Value> list;
    private SpannableString instruction_styledString;
    private String date, status, desc, ref_id, phlebotomist, patient, phlebotomistMobile, timezone;
    private long _id;

    private static final int TYPE_BLANK_HEADER = 0;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ITEM = 2;

    private boolean mIsSpaceVisible = true;
    private Typeface font_bold, font_regular;

    private ProgressDialog pdialog;


    public HomeDrawStatusAdapter(Context context, List<Value> list, String date, String status, String desc, String ref_id, String phlebotomist, String patient, String phlebotomistMobile, long _id, String timezone) {
        this.list = list;
        this.mContext = context;
        this.date = date;
        this.status = status;
        this.desc = desc;
        this.ref_id = ref_id;
        this.phlebotomist = phlebotomist;
        this.phlebotomistMobile = phlebotomistMobile;
        this.patient = patient;
        this._id = _id;
        this.timezone = timezone;
        font_bold = Typeface.createFromAsset(this.mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_bold));
        font_regular = Typeface.createFromAsset(this.mContext.getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_regular));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_BLANK_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transparent_header_view, null);
            HomeDrawStatusAdapter.OrderStatusBlankHeaderViewHolder viewHolder = new HomeDrawStatusAdapter.OrderStatusBlankHeaderViewHolder(view, mContext);
            return viewHolder;
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.orderstatus_header, null);
            HomeDrawStatusAdapter.OrderStatusHeaderViewHolder viewHolder = new HomeDrawStatusAdapter.OrderStatusHeaderViewHolder(view, mContext);
            return viewHolder;
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.homedraw_status_list_item, null);
            OrderStatusAdapterViewHolder viewHolder = new OrderStatusAdapterViewHolder(view, mContext);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder customViewHolder, final int i) {


        if (customViewHolder instanceof OrderStatusAdapterViewHolder) {
            String startdate = "";
            int position = i - 1;

            if (position == list.size() - 1)
                ((OrderStatusAdapterViewHolder) customViewHolder).vertical_line.setVisibility(View.GONE);

            if(position!=0)
                ((OrderStatusAdapterViewHolder) customViewHolder).img_tick.setTextColor(mContext.getResources().getColor(R.color.blue_grey));


            ((OrderStatusAdapterViewHolder) customViewHolder).tv_order_status.setText(Utils.capitalizeFirstLetter(list.get(position).getStatus()));
            int length = list.get(position).getActionOn().length();
            String inputDate = list.get(position).getActionOn();

            if ((inputDate.toString().trim().contains("z") || inputDate.toString().trim().contains("Z"))) {
                try {
                    Date ds = CommonTasks.fromIsoUtcString(inputDate.toString().trim(), DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS);

                    SimpleDateFormat simpleDateFormat;
                    if (DateFormat.is24HourFormat(mContext)) {
                        simpleDateFormat = new SimpleDateFormat(DateTimeUtil.HH_mm_comma_d_MMM_yyyy);
                    } else {
                        simpleDateFormat = new SimpleDateFormat(DateTimeUtil.hh_mm_aaa_comma_d_MMM_yyyy);
                    }

                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    startdate = simpleDateFormat.format(ds);
                } catch (Exception ex) {
                }
            } else {
                try {
                    if (DateFormat.is24HourFormat(mContext)) {
                        startdate = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS, DateTimeUtil.HH_mm_comma_d_MMM_yyyy, inputDate);
                    } else {
                        startdate = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS, DateTimeUtil.hh_mm_aaa_comma_d_MMM_yyyy, inputDate);
                    }
                } catch (Exception ex) {
                }
            }
            ((OrderStatusAdapterViewHolder) customViewHolder).tv_date.setText(startdate);
        } else if (customViewHolder instanceof OrderStatusHeaderViewHolder) {

            if(Utils.isValueAvailable(phlebotomist))
                ((OrderStatusHeaderViewHolder) customViewHolder).tv_name.setText(phlebotomist);
            else
                ((OrderStatusHeaderViewHolder) customViewHolder).tv_name.setVisibility(View.GONE);

            if(Utils.isValueAvailable(timezone))
                ((OrderStatusHeaderViewHolder) customViewHolder).timezone.setText(timezone);
            else
                ((OrderStatusHeaderViewHolder) customViewHolder).timezone.setVisibility(View.GONE);


            ((OrderStatusHeaderViewHolder) customViewHolder).tv_time.setText(date);
            ((OrderStatusHeaderViewHolder) customViewHolder).tv_refid.setText(mContext.getString(R.string.booking_id)+" "+ref_id);
            ((OrderStatusHeaderViewHolder) customViewHolder).tv_status.setText(status);
                if(status.equalsIgnoreCase("Confirmed"))
                    ((OrderStatusHeaderViewHolder) customViewHolder).tv_status.setTextColor(mContext.getResources().getColor(R.color.green_btn_bg));

            ((OrderStatusHeaderViewHolder) customViewHolder).tv_username.setText(patient);
            ((OrderStatusHeaderViewHolder) customViewHolder).tv_location_address.setText(desc);

            if(status.equalsIgnoreCase("Pending Confirmation") || status.equalsIgnoreCase("Cancelled"))
            {
                ((OrderStatusHeaderViewHolder) customViewHolder).tv_view_script.setVisibility(View.GONE);
                ((OrderStatusHeaderViewHolder) customViewHolder).rel_lyt_icon1.setVisibility(View.GONE);
                ((OrderStatusHeaderViewHolder) customViewHolder).rel_lyt_icon2.setVisibility(View.GONE);
            }


        } else if (customViewHolder instanceof OrderStatusBlankHeaderViewHolder) {
            ((OrderStatusBlankHeaderViewHolder) customViewHolder).mSpaceView.setVisibility(mIsSpaceVisible ? View.VISIBLE : View.GONE);
            ((OrderStatusBlankHeaderViewHolder) customViewHolder).mSpaceView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
            ((OrderStatusBlankHeaderViewHolder) customViewHolder).mPosition = i;
        }


    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
       /* if (position == 0) {
            return TYPE_BLANK_HEADER;
        }*/
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }


    public class OrderStatusAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        IconTextView img_tick;
        FrameLayout vertical_line;
        TextView tv_order_status, tv_date;

        public OrderStatusAdapterViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_order_status = (TextView) view.findViewById(R.id.tv_order_status);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            //tv_time= (TextView) view.findViewById(R.id.tv_time);
            vertical_line = (FrameLayout) view.findViewById(R.id.vertical_line);
            img_tick = (IconTextView) view.findViewById(R.id.img_tick);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {


                default:
                    break;
            }
        }
    }


    public class OrderStatusHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        private CustomFontTextView tv_name, tv_time, tv_refid, tv_status, tv_username, tv_location_address, tv_view_script, timezone;
        private ImageView iv_icon1, iv_icon2;
        private IconTextView iv_icon3;

        private RelativeLayout rel_lyt_icon1, rel_lyt_icon2, rel_lyt_icon3;
        private LinearLayout lyn_lyt_icons;
        private RelativeLayout rel_lyt_user_name, rel_lyt_location_address, rel_lyt_time, rel_lyt_refid, rel_lyt_appointment_status;

        public OrderStatusHeaderViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);



            tv_name = (CustomFontTextView) view.findViewById(R.id.tv_name);
            tv_time = (CustomFontTextView) view.findViewById(R.id.tv_time);
            timezone = (CustomFontTextView) view.findViewById(R.id.timezone);

            tv_refid = (CustomFontTextView) view.findViewById(R.id.tv_refid);
            tv_status = (CustomFontTextView) view.findViewById(R.id.tv_status);
            tv_username = (CustomFontTextView) view.findViewById(R.id.tv_username);
            tv_location_address = (CustomFontTextView) view.findViewById(R.id.tv_location_address);
            tv_view_script = (CustomFontTextView) view.findViewById(R.id.tv_view_script);

            iv_icon1 = (ImageView) view.findViewById(R.id.iv_icon1);
            iv_icon2 = (ImageView) view.findViewById(R.id.iv_icon2);
            iv_icon3 = (IconTextView) view.findViewById(R.id.iv_icon3);

            rel_lyt_icon1 = (RelativeLayout) view.findViewById(R.id.rel_lyt_icon1);
            rel_lyt_icon2 = (RelativeLayout) view.findViewById(R.id.rel_lyt_icon2);
            rel_lyt_icon3 = (RelativeLayout) view.findViewById(R.id.rel_lyt_icon3);

            lyn_lyt_icons = (LinearLayout) view.findViewById(R.id.lyn_lyt_icons);

            rel_lyt_user_name = (RelativeLayout) view.findViewById(R.id.rel_lyt_user_name);
            rel_lyt_location_address = (RelativeLayout) view.findViewById(R.id.rel_lyt_location_address);
            rel_lyt_time = (RelativeLayout) view.findViewById(R.id.rel_lyt_time);
            rel_lyt_refid = (RelativeLayout) view.findViewById(R.id.rel_lyt_refid);
            rel_lyt_appointment_status = (RelativeLayout) view.findViewById(R.id.rel_lyt_appointment_status);

            rel_lyt_icon1.setOnClickListener(this);
            rel_lyt_icon2.setOnClickListener(this);
            tv_view_script.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {

                case R.id.rel_lyt_icon1:

                    Intent intentCall = new Intent(Intent.ACTION_DIAL);
                    intentCall.setData(Uri.parse("tel:" + phlebotomistMobile));
                    mContext.startActivity(intentCall);

                    break;

                case R.id.rel_lyt_icon2:
                    Uri smsUri = Uri.parse("smsto:"+ phlebotomistMobile);
                    Intent intentMessage = new Intent(Intent.ACTION_SENDTO, smsUri);
                    intentMessage.putExtra("sms_body", "Where are you?");
//                  intent.setType("vnd.android-dir/mms-sms");
                    mContext.startActivity(intentMessage);

                    break;

                case R.id.tv_view_script:

                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                        return;
                    } else {
                       /* pdialog = new ProgressDialog(mContext);
                        pdialog.setMessage(mContext.getResources().getString(R.string.loading_txt));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.setCancelable(false);
                        pdialog.show();*/

                        if(Utils.isValueAvailable(HomeDrawStatusActivity.scriptImage))
                        {
                            Intent viewScriptIntent = new Intent(mContext, OpenImageFragment.class);
                            viewScriptIntent.putExtra(VariableConstants.DOCUMENT_TYPE, "Script");
                            mContext.startActivity(viewScriptIntent);
                        }
                        else
                        {
                            ((HomeDrawStatusActivity)mContext).showProgressDialog();
                            long transactionId = System.currentTimeMillis();
                            ThreadManager.getDefaultExecutorService().submit(new HomeDrawSearchRequest(transactionId, (AppointmentResponseCallback) context, TextConstants.SINGLE, (int)_id));

                        }

                    }


                    break;

                default:
                    break;
            }
        }
    }


    public class OrderStatusBlankHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        View mSpaceView;
        int mPosition;

        public OrderStatusBlankHeaderViewHolder(View view, Context context) {
            super(view);
            mSpaceView = view.findViewById(R.id.space);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {


                default:
                    break;
            }
        }
    }


}
