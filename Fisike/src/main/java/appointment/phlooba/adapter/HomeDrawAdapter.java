package appointment.phlooba.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import appointment.MyOrdersActivity;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.interfaces.OnLoadMoreListener;
import appointment.phlooba.model.HomeDrawModel;
import appointment.model.renderconfigmodel.RenderConfigModel;
import appointment.phlooba.FeedBackActivity;
import appointment.phlooba.HomeDrawStatusActivity;
import appointment.phlooba.ViewAllHomeDrawActivity;
import appointment.utils.CommonTasks;


/**
 * Created by laxmansingh on 3/1/2016.
 */
public class HomeDrawAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<HomeDrawModel> list;
    Resources resources;
    AppointmentResponseCallback responseCallback;
    String recent_or_upcoming;
    private int editableMinutes = 60;
    private RenderConfigModel renderConfigModel;

    private int instructionPosition = 0;
    /*[  .....loadmore variables needed.....   ]*/
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    /*[  .....End of loadmore variables needed.....   ]*/


    public HomeDrawAdapter(Context context, final List<HomeDrawModel> list, AppointmentResponseCallback responseCallback, String recent_or_upcoming, RecyclerView mRecyclerView) {
        this.list = list;
        this.mContext = context;
        resources = mContext.getResources();
        this.responseCallback = responseCallback;
        this.recent_or_upcoming = recent_or_upcoming;


       /*[    .......ScrollListener on recyclerview for loadmore items.......   ]*/
        if (mContext instanceof ViewAllHomeDrawActivity) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
        /*[    .......End of ScrollListener on recyclerview for loadmore items.......   ]*/

        try {
            Gson gson1 = new Gson();
            Type listType = null;
            listType = new TypeToken<List<RenderConfigModel>>() {
            }.getType();
            renderConfigModel = gson1.fromJson(SharedPref.getRenderConfig().toString(), RenderConfigModel.class);
            editableMinutes = Integer.parseInt(renderConfigModel.getAppointmentEditableBefore());
            renderConfigModel = null;
        } catch (Exception ex) {
            renderConfigModel = null;
            editableMinutes = 60;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.homedraw_list_item, null);
            MyOrdersAdapterViewHolder viewHolder = new MyOrdersAdapterViewHolder(view, mContext);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View lodingView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_loading_item, null);
            LoadingViewHolder loadingViewHolder = new LoadingViewHolder(lodingView, mContext);
            return loadingViewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int i) {
        if (customViewHolder instanceof MyOrdersAdapterViewHolder) {
            HomeDrawModel list_model = list.get(i);

            MyOrdersAdapterViewHolder myOrdersAdapterViewHolder = (MyOrdersAdapterViewHolder) customViewHolder;

        /*[............. all date logic on basis of today, previous and after date...........   ]*/

            String[]  strings = list_model.getAppointmentDate().toString().trim().split("T");
            String appointmentDateString = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.mm_dd_yyyy_slashSeperated, strings[0]);
            String startTimeString = CommonTasks.formateDateFromstring(DateTimeUtil.sourceTimeFormat, DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat, list_model.getStartTime().toString().trim());
            String endTimeString = CommonTasks.formateDateFromstring(DateTimeUtil.sourceTimeFormat, DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat, list_model.getEndDate().toString().trim());



            SimpleDateFormat ft = new SimpleDateFormat(DateFormat.is24HourFormat(mContext)?DateTimeUtil.sourceTimeFormat:DateTimeUtil.destinationTimeFormat);
            Date appointmentTime = null;
            try
            {
                if(Utils.isValueAvailable(startTimeString))
                {
                    appointmentTime = ft.parse(startTimeString);
                    appointmentTime.setMinutes(appointmentTime.getMinutes()+30);
                    startTimeString = ft.format(appointmentTime);
                }
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }


            myOrdersAdapterViewHolder.tv_timing.setText(appointmentDateString+", "+startTimeString+"-"+endTimeString);


           /* int valuePostiton = 0, sampleLocationPosition = -1;
            for (int j = 0; j < list.get(i).getExtension().size(); j++) {
                if (list.get(i).getExtension().get(j).getUrl().equals("appointmentDetails")) {
                    valuePostiton = j;
                    break;
                }
            }

            for (int j = 0; j < list.get(i).getExtension().size(); j++) {
                if (list.get(i).getExtension().get(j).getUrl().equals("SampleCollectionLocation")) {
                    sampleLocationPosition = j;
                    break;
                }
            }

            boolean isHSC = false;

            if (sampleLocationPosition >= 0) {
                if (list.get(i).getExtension().get(sampleLocationPosition).getValue().size() > 0) {
                    String addr = list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getAddr();
                    if (!CommonTasks.isTextEmptyOrNull(addr)) {
                        isHSC = true;
                    }
                }
            }

            try {
                String address = list_model.getExtension().get(valuePostiton).getValue().get(0).getLocationDetails().toString().trim();
                myOrdersAdapterViewHolder.tv_type.setText(address);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

*/










//            Value value = list_model.getExtension().get(valuePostiton).getValue().get(0);

            if(Utils.isValueAvailable(list_model.getPractitionerName()))
            {
                myOrdersAdapterViewHolder.tv_phlabotmist_name.setText(list_model.getPractitionerName());
            }
            else
            {
                myOrdersAdapterViewHolder.tv_phlabotmist_name.setVisibility(View.GONE);
            }


            if(Utils.isValueAvailable(list_model.getTimezone()))
            {
                myOrdersAdapterViewHolder.timezone.setText(list_model.getTimezone());
            }
            else
            {
                myOrdersAdapterViewHolder.timezone.setVisibility(View.GONE);
            }


            try
            {
                myOrdersAdapterViewHolder.tv_ref_id.setText(mContext.getString(R.string.booking_id)+" "+list_model.getAppointmentIdentifier().toString());
            }
            catch (Exception ex)
            {
                myOrdersAdapterViewHolder.tv_ref_id.setVisibility(View.GONE);
                ex.printStackTrace();
            }



            myOrdersAdapterViewHolder.tv_patient_name.setText(list_model.getBookedPatientModel().getPatientName());
            myOrdersAdapterViewHolder.tv_desc.setText(list_model.getSelectedAddress());

            if(list_model.getAppointmentStatus().toString().equalsIgnoreCase("booked"))
            {
                myOrdersAdapterViewHolder.tv_type.setText("Confirmed");
                myOrdersAdapterViewHolder.tv_type.setTextColor(mContext.getResources().getColor(R.color.green_btn_bg));

            }else if(list_model.getAppointmentStatus().toString().equalsIgnoreCase("proposed"))
            {
                myOrdersAdapterViewHolder.tv_type.setText("Pending Confirmation");
                myOrdersAdapterViewHolder.tv_type.setTextColor(mContext.getResources().getColor(R.color.dusky_blue_70));
            }
            else
            {
                myOrdersAdapterViewHolder.tv_type.setText(Utils.capitalizeFirstLetter(list_model.getAppointmentStatus().toString()));
                myOrdersAdapterViewHolder.tv_type.setTextColor(mContext.getResources().getColor(R.color.dusky_blue_70));
            }









            /*String doctor = list.get(i).getExtension().get(valuePostiton).getValue().get(0).getPractitionerName();
            String speciality = list.get(i).getExtension().get(valuePostiton).getValue().get(0).getSpecialityName();
            String tests = list.get(i).getExtension().get(valuePostiton).getValue().get(0).getServiceName();

            if ((tests != null && !tests.equalsIgnoreCase("")) && !(doctor != null && !doctor.equalsIgnoreCase("")) && !(speciality != null && !speciality.equalsIgnoreCase(""))) {

                StringBuilder testnames_string = new StringBuilder();
                int price = 0;
                String address = "";
                for (int j = 0; j < list_model.getExtension().size(); j++) {
                    if (list_model.getExtension().get(j).getUrl().trim().toString().equals("appointmentDetails")) {
                        for (int k = 0; k < list_model.getExtension().get(j).getValue().size(); k++) {
                            if (k > 0 && k < list_model.getExtension().get(j).getValue().size()) {
                                testnames_string.append(", ");
                            }
                            try {
                                testnames_string.append(list_model.getExtension().get(j).getValue().get(k).getServiceName().toString().trim());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
//                                price = price + Integer.parseInt(list_model.getExtension().get(j).getValue().get(k).getCost().toString().trim());
                            try {
                                address = address + list_model.getExtension().get(j).getValue().get(k).getLocationDetails().toString().trim();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        break;
                    }
                }
                myOrdersAdapterViewHolder.tv_type.setText(address);

                myOrdersAdapterViewHolder.tv_price.setText("" + price);
                myOrdersAdapterViewHolder.img_currency.setVisibility(View.INVISIBLE);
                myOrdersAdapterViewHolder.tv_desc.setText(testnames_string.toString());
            } else if ((tests != null && !tests.equalsIgnoreCase("")) && (doctor != null && !doctor.equalsIgnoreCase("")) && !(speciality != null && !speciality.equalsIgnoreCase(""))) {
                StringBuilder doc_name = new StringBuilder();
                String address = "";
                for (int j = 0; j < list_model.getExtension().size(); j++) {
                    if (list_model.getExtension().get(j).getUrl().trim().toString().equals("appointmentDetails")) {
                        for (int k = 0; k < list_model.getExtension().get(j).getValue().size(); k++) {
                            try {
                                doc_name.append(list_model.getExtension().get(j).getValue().get(k).getPractitionerName().toString().trim());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            try {
                                address = address + list_model.getExtension().get(j).getValue().get(k).getLocationDetails().toString().trim();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        break;
                    }
                }
                myOrdersAdapterViewHolder.tv_type.setText(address);
                myOrdersAdapterViewHolder.tv_desc.setText(doc_name);
            } else if (speciality != null && !speciality.equalsIgnoreCase("")) {
                StringBuilder speciality_name = new StringBuilder();
                String address = "";
                for (int j = 0; j < list_model.getExtension().size(); j++) {
                    if (list_model.getExtension().get(j).getUrl().trim().toString().equals("appointmentDetails")) {
                        for (int k = 0; k < list_model.getExtension().get(j).getValue().size(); k++) {
                            try {
                                speciality_name.append(list_model.getExtension().get(j).getValue().get(k).getSpecialityName().toString().trim());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            try {
                                address = address + list_model.getExtension().get(j).getValue().get(k).getLocationDetails().toString().trim();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        break;
                    }
                }
//                myOrdersAdapterViewHolder.tv_type.setText(Utils.capitalizeFirstLetter(list_model.getStatus().toString()));
                myOrdersAdapterViewHolder.tv_desc.setText(speciality_name);
                myOrdersAdapterViewHolder.tv_type.setText(address);

            } else if (isHSC) {
                int price = 0;
                StringBuilder address = new StringBuilder();

                if (!CommonTasks.isTextEmptyOrNull(list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getAddr())) {
                    address.append(list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getAddr());
                    if (!CommonTasks.isTextEmptyOrNull(list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getCity())) {
                        address.append(", " + list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getCity());
                    }
                    if (!CommonTasks.isTextEmptyOrNull(list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getState())) {
                        address.append(", " + list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getState());
                    }
                    if (!CommonTasks.isTextEmptyOrNull(list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getZip())) {
                        address.append(", " + list.get(i).getExtension().get(sampleLocationPosition).getValue().get(0).getZip());
                    }
                }

                myOrdersAdapterViewHolder.tv_type.setText(address.toString());
                myOrdersAdapterViewHolder.tv_price.setText("" + price);
                myOrdersAdapterViewHolder.img_currency.setVisibility(View.INVISIBLE);
            }

*/
            if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                myOrdersAdapterViewHolder.tv_timing.setTextColor(mContext.getResources().getColor(R.color.ui_theme_color));


               /* for (int j = 0; j < list_model.getExtension().size(); j++) {
                    if (list_model.getExtension().get(j).getUrl().trim().toString().equals("Audit")) {
                        for (int k = 0; k < list_model.getExtension().get(j).getValue().size(); k++) {
                            try {
                                if (list_model.getExtension().get(j).getValue().get(k).getActionBy().equals("SIU")) {
                                    if (isMenuOverFlowVisible(i)) {
                                        myOrdersAdapterViewHolder.iv_ic_menu_overflow.setVisibility(View.VISIBLE);
                                    } else {
                                        myOrdersAdapterViewHolder.iv_ic_menu_overflow.setVisibility(View.GONE);
                                    }
                                    break;
                                } else {
                                    myOrdersAdapterViewHolder.iv_ic_menu_overflow.setVisibility(View.VISIBLE);
                                }


                                *//*[This code is because there is speciality enabled in web but disabled in mobile...]*//*
                                if (speciality != null && !speciality.equalsIgnoreCase("")) {
                                    myOrdersAdapterViewHolder.iv_ic_menu_overflow.setVisibility(View.GONE);
                                }
                                *//*[This code is because there is speciality enabled in web but disabled in mobile...]*//*

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        break;
                    }
                }*/


            }
            else if (recent_or_upcoming.equals(TextConstants.RECENT))
            {
                myOrdersAdapterViewHolder.tv_timing.setTextColor(mContext.getResources().getColor(R.color.orange));
                myOrdersAdapterViewHolder.iv_ic_menu_overflow.setVisibility(View.GONE);
                myOrdersAdapterViewHolder.relativeLayoutCancel.setVisibility(View.GONE);
                myOrdersAdapterViewHolder.relativeLayoutReschedule.setVisibility(View.GONE);

            }




            /*if (startdate.after(todaydate)) {
                if(android.text.format.DateFormat.is24HourFormat(mContext)) {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.HH_mm_comma_dd_MMM_yyyy, list_model.getStartTime().toString().trim()));
                }else {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, list_model.getStartTime().toString().trim()));
                }
            } else if (startdate.before(todaydate)) {
                if(android.text.format.DateFormat.is24HourFormat(mContext)) {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.HH_mm_comma_dd_MMM_yyyy, list_model.getStartTime().toString().trim()));
                }else {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, list_model.getStartTime().toString().trim()));
                }
            } else if (startdate.equals(todaydate)) {
                if(android.text.format.DateFormat.is24HourFormat(mContext)) {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.HH_mm_comma, list_model.getStartTime().toString().trim()) + " " + mContext.getResources().getString(R.string.txt_today));
                }else {
                    myOrdersAdapterViewHolder.tv_timing.setText(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa_comma, list_model.getStartTime().toString().trim()) + " " + mContext.getResources().getString(R.string.txt_today));
                }
            }*/
        /*[............. End of all date logic on basis of today,previous and after date...........   ]*/

        } else if (customViewHolder instanceof LoadingViewHolder)

        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) customViewHolder;
        }
    }

    @Override
    public int getItemCount() {

        if ((recent_or_upcoming.equals(TextConstants.UPCOMING) || recent_or_upcoming.equals(TextConstants.RECENT)) && (mContext instanceof MyOrdersActivity || mContext instanceof HomeActivity)) {
            if (list.size() > TextConstants.NO_OF_UPCOMING_RECENT_ELEMENTS_TO_SHOW)// manage number of recent/upcoming events to show
                return TextConstants.NO_OF_UPCOMING_RECENT_ELEMENTS_TO_SHOW;
            else
                return list.size();
        }
        return (null != list ? list.size() : 0);
    }


    public class MyOrdersAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView tv_phlabotmist_name, tv_ref_id, tv_patient_name, tv_timing, tv_desc, tv_price, tv_type, timezone;
        IconTextView iv_ic_menu_overflow, img_currency;
        View temp_line;
        RelativeLayout clicked_view, relativeLayoutCancel, relativeLayoutReschedule;

        public MyOrdersAdapterViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_phlabotmist_name = (TextView) view.findViewById(R.id.tv_phlabotmist_name);
            tv_ref_id = (TextView) view.findViewById(R.id.tv_ref_id);
            tv_patient_name = (TextView) view.findViewById(R.id.tv_patient_name);

            tv_timing = (TextView) view.findViewById(R.id.tv_timing);
            timezone = (TextView) view.findViewById(R.id.timezone);

            tv_type = (TextView) view.findViewById(R.id.tv_type);
            tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            img_currency = (IconTextView) view.findViewById(R.id.img_currency);
            iv_ic_menu_overflow = (IconTextView) view.findViewById(R.id.iv_ic_menu_overflow);
            iv_ic_menu_overflow.setOnClickListener(this);
            clicked_view = (RelativeLayout) view.findViewById(R.id.clicked_view);
            clicked_view.setOnClickListener(this);

            relativeLayoutCancel = (RelativeLayout) view.findViewById(R.id.rel_lyt_cancel);
            relativeLayoutReschedule = (RelativeLayout) view.findViewById(R.id.rel_lyt_icon_reschedule);

            relativeLayoutCancel.setOnClickListener(this);
            relativeLayoutReschedule.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {

                case R.id.iv_ic_menu_overflow:
                    showPopup(iv_ic_menu_overflow, adapter_position);
                    break;

                case R.id.rel_lyt_cancel:

                    /*String appointmentDateString = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.dd_MMM_yyyy, list.get(adapter_position).getAppointmentDate().toString().trim());

                    long diffcancelmins = CommonTasks.findDateDifferenceinMins(CommonTasks.formateDateFromstring(DateTimeUtil.DateFormatWithTime24hr, DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, appointmentDateString+" "+list.get(adapter_position).getStartTime().toString().trim()));

                    if (diffcancelmins >= editableMinutes) {*/
                        HomeDrawModel appointModel = list.get(adapter_position);

                        Intent intent = new Intent(mContext, FeedBackActivity.class);
                        intent.putExtra("from", "cancel");
                        intent.putExtra("ref_id",appointModel.getAppointmentIdentifier().toString());
                        intent.putExtra("_id", Long.toString(appointModel.getId()));
                        intent.putExtra("position", adapter_position);
                        ((Activity) mContext).startActivityForResult(intent, TextConstants.INTENT_CONSTANT);

                        /*if (!Utils.showDialogForNoNetwork(mContext, false)) {
                        } else {
                            responseCallback.cancelOrReschedule(appointModel, adapter_position + "", TextConstants.CANCEL_TEXT);
                        }*/
                  /*  } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.cannot_cancel), Toast.LENGTH_SHORT).show();
                    }*/
                    break;

                case R.id.rel_lyt_icon_reschedule:
                    Toast.makeText(context, "Under development...", Toast.LENGTH_SHORT).show();
                    break;


                case R.id.clicked_view:
                    Bundle bundle = new Bundle();

                   /* ArrayList<appointment.model.appointmodel.List> temp_list = new ArrayList<appointment.model.appointmodel.List>();
                    temp_list.clear();
                    temp_list.add(list.get(adapter_position));
                    bundle.putParcelableArrayList("appointment_list", (ArrayList<? extends Parcelable>) temp_list);*/

                    bundle.putString("date", tv_timing.getText().toString());
                    bundle.putString("desc", tv_desc.getText().toString());
                    bundle.putString("status", tv_type.getText().toString());
                    bundle.putString("patient", tv_patient_name.getText().toString());
                    bundle.putString("phlebotomist", tv_phlabotmist_name.getText().toString());
                    bundle.putString("phlebotomist_latitude", tv_phlabotmist_name.getText().toString());
                    bundle.putString("phlebotomist_longitude", tv_phlabotmist_name.getText().toString());
                    bundle.putLong("_id", list.get(adapter_position).getId());
                    bundle.putString("timezone", timezone.getText().toString());

                    try {
                        bundle.putString("ref_id", list.get(adapter_position).getAppointmentIdentifier().toString());
                    } catch (Exception e) {
                        bundle.putString("ref_id", "");
                    }

                    try {
                        bundle.putString("phlebotomist_phone", list.get(adapter_position).getBookedPractitionerPhoneNo().toString());
                    } catch (Exception e) {
                        bundle.putString("phlebotomist_phone", "");
                    }



                    HomeDrawStatusActivity.newInstance(mContext, bundle);
                    break;
            }
        }


        public void showPopup(View v, final int position) {}
        /*
            PopupMenu popup = new PopupMenu(mContext, v);
            MenuInflater inflater = popup.getMenuInflater();

            if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                inflater.inflate(R.menu.menu_upcoming, popup.getMenu());
            } else if (recent_or_upcoming.equals(TextConstants.RECENT)) {
                inflater.inflate(R.menu.menu_recent, popup.getMenu());
            }


            for (int i = 0; i < list.get(position).getExtension().size(); i++) {
                if (list.get(position).getExtension().get(i).getUrl().equals("appointmentDetails")) {
                    String instruction = (list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic() == null || list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic().size() == 0) ? "" : list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic().get(0).getCoding().get(0).getDisplay();
                    instructionPosition = i;
                    if (instruction != null && !instruction.equals("") && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) {
                        popup.getMenu().getItem(0).setVisible(true);
                    } else {
                        popup.getMenu().getItem(0).setVisible(false);
                    }
                    break;
                } else {
                    popup.getMenu().getItem(0).setVisible(false);
                }
            }


            for (int j = 0; j < list.get(position).getExtension().size(); j++) {
                if (list.get(position).getExtension().get(j).getUrl().trim().toString().equals("Audit")) {
                    for (int k = 0; k < list.get(position).getExtension().get(j).getValue().size(); k++) {
                        try {
                            if (list.get(position).getExtension().get(j).getValue().get(k).getActionBy().equals("SIU")) {
                                popup.getMenu().getItem(1).setVisible(false);
                                popup.getMenu().getItem(2).setVisible(false);
                                break;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    break;
                }
            }

            popup.show();
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.view_instruction:
                            Intent intent = new Intent(mContext, ViewInstructionsActivity.class);
                            intent.putExtra("instructions", list.get(position).getExtension().get(instructionPosition).getValue().get(0).getCharacteristic().get(0).getCoding().get(0).getDisplay());
                            intent.putExtra("title", list.get(position).getExtension().get(instructionPosition).getValue().get(0).getServiceName());
                            mContext.startActivity(intent);
                            break;

                        case R.id.reschedule:
                            long diffreschedulemins = CommonTasks.findDateDifferenceinMins(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, list.get(position).getStart().getValue().toString().trim()));
                            if (diffreschedulemins >= editableMinutes) {
                                int valuePostiton = 0, sampleLocationPosition = -1;
                                for (int i = 0; i < list.get(position).getExtension().size(); i++) {
                                    if (list.get(position).getExtension().get(i).getUrl().equals("appointmentDetails")) {
                                        valuePostiton = i;
                                        break;
                                    }
                                }

                                for (int j = 0; j < list.get(position).getExtension().size(); j++) {
                                    if (list.get(position).getExtension().get(j).getUrl().equals("SampleCollectionLocation")) {
                                        sampleLocationPosition = j;
                                        break;
                                    }
                                }

                                boolean isHSC = false;

                                if (sampleLocationPosition >= 0) {
                                    if (list.get(position).getExtension().get(sampleLocationPosition).getValue().size() > 0) {
                                        String addr = list.get(position).getExtension().get(sampleLocationPosition).getValue().get(0).getAddr();
                                        if (!CommonTasks.isTextEmptyOrNull(addr)) {
                                            isHSC = true;
                                        }
                                    }
                                }

                                Bundle bundle = new Bundle();
                                bundle.putString("rebook_or_reschedule", TextConstants.RESCHEDULE);
                                bundle.putParcelable("reschedule_appointmentmodel", list.get(position));
                                //     bundle.putBoolean("isBookable", list.get(position).getExtension().get(valuePostiton).getValue().get(0).getBookable());
                                bundle.putString("position", position + "");
                                Intent intents;
                                if (isHSC) {
                                    intents = new Intent(mContext, RescheduleActivity.class);
                                } else {
                                    intents = new Intent(mContext, FragmentBaseActivity.class);
                                }

                                String doctor = list.get(position).getExtension().get(valuePostiton).getValue().get(0).getPractitionerName();
                                String speciality = list.get(position).getExtension().get(valuePostiton).getValue().get(0).getSpecialityName();
                                String tests = list.get(position).getExtension().get(valuePostiton).getValue().get(0).getServiceName();


                                if ((tests != null && !tests.equalsIgnoreCase("")) && !(doctor != null && !doctor.equalsIgnoreCase("")) && !(speciality != null && !speciality.equalsIgnoreCase(""))) {
                                    intents.putExtra(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                                } else if ((tests != null && !tests.equalsIgnoreCase("")) && (doctor != null && !doctor.equalsIgnoreCase("")) && !(speciality != null && !speciality.equalsIgnoreCase(""))) {
                                    intents.putExtra(TextConstants.TEST_TYPE, TextConstants.DOCTORS);
                                } else if (speciality != null && !speciality.equalsIgnoreCase("")) {
                                    intents.putExtra(TextConstants.TEST_TYPE, TextConstants.SPECIALITY);
                                } else {
                                    intents.putExtra(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                                }
                                intents.putExtras(bundle);
                                ((Activity) mContext).startActivityForResult(intents, TextConstants.INTENT_CONSTANT);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.cannot_rechedule), Toast.LENGTH_SHORT).show();
                            }
                            break;

                        case R.id.cancel:
                            long diffcancelmins = CommonTasks.findDateDifferenceinMins(CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, list.get(position).getStart().getValue().toString().trim()));
                            if (diffcancelmins >= editableMinutes) {
                                appointment.model.appointmodel.List appointModel = list.get(position);
                                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                } else {
                                    responseCallback.cancelOrReschedule(appointModel, position + "", TextConstants.CANCEL_TEXT);
                                }
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.cannot_cancel), Toast.LENGTH_SHORT).show();
                            }
                            break;

                        case R.id.reebok:
                            DialogUtils.showAlertDialogCommon(mContext, mContext.getResources().getString(R.string.txt_rebook_test), mContext.getResources().getString(R.string.txt_test_rebook), mContext.getResources().getString(R.string.ok), mContext.getResources().getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case Dialog.BUTTON_NEGATIVE:
                                            dialog.dismiss();
                                            break;

                                        case Dialog.BUTTON_POSITIVE:
                                            if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                                return;
                                            }
                                            dialog.dismiss();
                                            appointment.model.appointmodel.List appointModel = list.get(position);
                                            if (list.get(position).getType().getText().equals(TextConstants.TYPE_HSC)) {
                                                Intent rebookIntent = new Intent(mContext, SelectTimeActivity.class);
                                                rebookIntent.putExtra("appointment_listId", appointModel.getParticipant().get(1).getActorHealthcareService().getId());
                                                rebookIntent.putExtra("position", position + "");
                                                ((Activity) mContext).startActivityForResult(rebookIntent, TextConstants.INTENT_CONSTANT);
                                            } else if (list.get(position).getType().getText().equals(TextConstants.TYPE_DOCTOR)) {
                                                Intent rebookDoctor = new Intent(mContext, FragmentBaseActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putString(TextConstants.TEST_TYPE, TextConstants.DOCTORS);
                                                bundle.putString("position", position + "");
                                                bundle.putInt("HCS", list.get(position).getParticipant().get(1).getActorHealthcareService().getId());
                                                bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
                                                rebookDoctor.putExtras(bundle);
                                                ((Activity) mContext).startActivityForResult(rebookDoctor, TextConstants.INTENT_CONSTANT);
                                            } else if (list.get(position).getType().getText().equals(TextConstants.TYPE_SPECIALITY)) {
                                                Intent rebookSpeciality = new Intent(mContext, FragmentBaseActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putString(TextConstants.TEST_TYPE, TextConstants.SPECIALITY);
                                                bundle.putString("position", position + "");
                                                bundle.putInt("HCS", list.get(position).getParticipant().get(1).getActorHealthcareService().getId());
                                                bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
                                                rebookSpeciality.putExtras(bundle);
                                                ((Activity) mContext).startActivityForResult(rebookSpeciality, TextConstants.INTENT_CONSTANT);
                                            } else if (list.get(position).getType().getText().equals(TextConstants.TYPE_TESTS)) {
                                                Intent rebooklabtest = new Intent(mContext, FragmentBaseActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putString(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                                                bundle.putString("position", position + "");
                                                bundle.putInt("HCS", list.get(position).getParticipant().get(1).getActorHealthcareService().getId());
                                                bundle.putString("rebook_or_reschedule", TextConstants.REBOOK);
                                                rebooklabtest.putExtras(bundle);
                                                ((Activity) mContext).startActivityForResult(rebooklabtest, TextConstants.INTENT_CONSTANT);
                                            }
                                    }
                                }
                            });

                            break;

                        default:
                            break;
                    }

                    return true;
                }
            });
        }*/
    }


    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        Context context;

        public LoadingViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarLoading);
            progressBar.getIndeterminateDrawable().setColorFilter(
                    mContext.getResources().getColor(R.color.color_to_change_theme),
                    android.graphics.PorterDuff.Mode.SRC_IN);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }


    /*public boolean isMenuOverFlowVisible(int position) {
        for (int i = 0; i < list.get(position).getExtension().size(); i++) {
            if (list.get(position).getExtension().get(i).getUrl().equals("appointmentDetails")) {
                String instruction = (list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic() == null || list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic().size() == 0) ? "" : list.get(position).getExtension().get(i).getValue().get(0).getCharacteristic().get(0).getCoding().get(0).getDisplay();
                if (instruction != null && !instruction.equals("") && !instruction.equals(mContext.getResources().getString(R.string.txt_null))) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }*/

}
