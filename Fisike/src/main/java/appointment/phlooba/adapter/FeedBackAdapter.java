package appointment.phlooba.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

import appointment.phlooba.FeedBackActivity;
import appointment.utils.SelectableRoundedImageView;

/**
 * Created by laxmansingh on 6/23/2017.
 */

public class FeedBackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<String> list;

    private static final int TYPE_ITEM = 2;


    public FeedBackAdapter(Context context, List<String> list) {
        this.list = list;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feedback_list_item, null);
            FeedBackAdapter.FeedBackViewHolder viewHolder = new FeedBackAdapter.FeedBackViewHolder(view, mContext);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder customViewHolder, final int i) {
        if (customViewHolder instanceof FeedBackAdapter.FeedBackViewHolder) {
            ((FeedBackAdapter.FeedBackViewHolder) customViewHolder).img_profile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ((FeedBackAdapter.FeedBackViewHolder) customViewHolder).img_profile.setOval(true);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    public class FeedBackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        SelectableRoundedImageView img_profile;
        CustomFontTextView tv_name, tv_static_rate_exp;
        RatingBar ratingBar;

        public FeedBackViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_name = (CustomFontTextView) view.findViewById(R.id.tv_name);
            tv_static_rate_exp = (CustomFontTextView) view.findViewById(R.id.tv_static_rate_exp);
            img_profile = (SelectableRoundedImageView) view.findViewById(R.id.img_profile);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            Intent intent = new Intent(mContext, FeedBackActivity.class);
            intent.putExtra("from", "feedback");
            mContext.startActivity(intent);
        }

    }
}
