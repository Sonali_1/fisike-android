package appointment.phlooba.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;

import appointment.AppointmentActivity;
import appointment.enums.AppointmentTypeEnum;
import appointment.phlooba.WhosPatientActivity;

/**
 * Created by laxmansingh on 6/23/2017.
 */

public class GridAppointmentAdapter extends RecyclerView.Adapter<GridAppointmentAdapter.GridAppointmentViewHolder> {

    private Context context;
    ArrayList<String> requestList;
    String from;


    public GridAppointmentAdapter(FragmentActivity activity, ArrayList<String> requestTypes, String from) {
        context = activity;
        requestList = requestTypes;
        this.from = from;
    }

    @Override
    public GridAppointmentAdapter.GridAppointmentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.appointment_grid_list_item, null);
        GridAppointmentAdapter.GridAppointmentViewHolder viewHolder = new GridAppointmentAdapter.GridAppointmentViewHolder(view, context);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return (null != requestList ? requestList.size() : 0);
    }


    @Override
    public void onBindViewHolder(final GridAppointmentViewHolder customViewHolder, final int position) {
        String requestType = requestList.get(position);

        customViewHolder.tv_title.setText(AppointmentTypeEnum.getDisplayedValuefromCode(requestType) + "");

        if (position == 0) {
            customViewHolder.iconHome.setImageResource(R.drawable.home_appointment);
        } else if (position == 1) {
            customViewHolder.iconHome.setImageResource(R.drawable.hospital_appointment);
        }

    }


    public class GridAppointmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        private CustomFontTextView tv_title;
        private RelativeLayout clicked_view, rl_header;
        private ImageView iconHome;

        public GridAppointmentViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_title = (CustomFontTextView) view.findViewById(R.id.tv_title);
            clicked_view = (RelativeLayout) view.findViewById(R.id.clicked_view);
            rl_header = (RelativeLayout) view.findViewById(R.id.rl_header);
            iconHome = (ImageView) view.findViewById(R.id.icon_home);
            clicked_view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            onClickRequest(v);
        }

        public void onClickRequest(View v) {
            int adapter_position = getAdapterPosition();
            if (BuildConfig.isPhysicianApp) {
                ((AppointmentActivity) context).setCurrentItem(-1);
            }

            if(adapter_position == 0)
                openPatientInfoActivity(TextConstants.HOME_SAMPLE);
            else
                Toast.makeText(context, "Under development...", Toast.LENGTH_SHORT).show();
        }
    }


    private void openPatientInfoActivity(String from) {
        Bundle bundle = new Bundle();
        bundle.putString(TextConstants.FROM, from);
        Intent nextIntent = new Intent(context, WhosPatientActivity.class);
        //   Intent nextIntent = new Intent(context, PatientInfoActivity.class);
        nextIntent.putExtras(bundle);
        context.startActivity(nextIntent);
    }


}
