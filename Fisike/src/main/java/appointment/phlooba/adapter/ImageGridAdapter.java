package appointment.phlooba.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconButton;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.ArrayList;

import appointment.phlooba.model.ImageDataType;

/**
 * Created by neharathore on 04/07/17.
 */

public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.ImageViewHolder> {

    Context context;
    private String title;
    private ArrayList<ImageDataType> ListData;
    public clickListener listener;
    private boolean isReadMode;

    public ImageGridAdapter(ArrayList<ImageDataType> listData, Context context, boolean isReadMode) {
        // TODO Auto-generated constructor stub
        this.ListData = listData;
        this.context = context;
        this.isReadMode = isReadMode;
    }

    @Override
    public int getItemCount() {
        return ListData.size();
    }


    public interface clickListener {
        public void itemClicked(View view, int position);
    }


    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(context).inflate(R.layout.image_drawable_with_dotted_border, parent, false);
        ImageViewHolder settingHolder = new ImageViewHolder(row);
        return settingHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.tvTitle.setText(ListData.get(position).getTitle());
        if (ListData.get(position).getImage() != null) {
            holder.tvTitle.setVisibility(View.GONE);
            holder.itvCamera.setVisibility(View.GONE);
            if(isReadMode) {
                holder.ivIcon.setVisibility(View.GONE);
            } else {
                holder.ivIcon.setVisibility(View.VISIBLE);
            }
            holder.tvTitle.setVisibility(View.GONE);
            BitmapDrawable drawable = new BitmapDrawable(context.getResources(), ListData.get(position).getImage());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                holder.rl_image_view.setBackground(context.getResources().getDrawable(R.drawable.dashed_stroke_rectangle));
                holder.rl_parent.setBackground(drawable);
            }
            else{
                holder.rl_image_view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.dashed_stroke_rectangle));
                holder.rl_parent.setBackgroundDrawable(drawable);

            }
        }
        else {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.itvCamera.setVisibility(View.VISIBLE);
            holder.ivIcon.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                holder.rl_image_view.setBackground(context.getResources().getDrawable(R.drawable.dashed_stroke_rectangle));
                holder.rl_parent.setBackground(context.getResources().getDrawable(R.drawable.greydrawableimage));
            }
            else{
                holder.rl_image_view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.dashed_stroke_rectangle));
                holder.rl_parent.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.greydrawableimage));

            }

        }


    }


    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView tvTitle;
        IconTextView itvCamera;
        IconButton ivIcon;
        RelativeLayout rl_image_view;
        RelativeLayout rl_parent;

        public ImageViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.tv_txt_cameraoption);
            this.ivIcon = (IconButton) v.findViewById(R.id.itv_cross);
            this.rl_image_view = (RelativeLayout) v.findViewById(R.id.rl_image_view);
            this.itvCamera= (IconTextView) v.findViewById(R.id.itv_icon_camera);
            this.rl_parent= (RelativeLayout) v.findViewById(R.id.rl_parent);
            v.setOnClickListener(this);
            ivIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                //handling Item Click
                listener.itemClicked(v, getAdapterPosition());
            }
        }
    }
}
