package appointment.phlooba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;

import appointment.phlooba.model.TimeSlotModel;

/**
 * Created by neharathore on 08/07/17.
 */
public class SlotStatusAdapter extends RecyclerView.Adapter<SlotStatusAdapter.SlotViewHolder>{

    Context context;
    ArrayList<TimeSlotModel> listData;
    public clickListener listener;
    public  int lastSelectedIndex;

    public SlotStatusAdapter(Context context, ArrayList<TimeSlotModel> listData, clickListener listener)
    {
        this.context=context;
        this.listData=listData;
        this.listener=listener;

    }

    public void setList(ArrayList<TimeSlotModel> list){
        listData = list;
    }
    @Override
    public SlotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(context).inflate(R.layout.slot_item, parent, false);
        SlotStatusAdapter.SlotViewHolder viewHolder = new SlotStatusAdapter.SlotViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SlotViewHolder holder, int position) {

        TimeSlotModel model=listData.get(position);
        holder.tvSlot.setText(model.getDisplayedTime(context));
        if(position%2==0)
        { holder.tvSlot.setGravity(Gravity.LEFT|Gravity.CENTER);
        }
        else
        {
           holder.tvSlot.setGravity(Gravity.RIGHT|Gravity.CENTER);

        }
         if(!model.isSlotAvailable())
            holder.tvSlot.setTextColor(context.getResources().getColor(R.color.black_opacity_30));
         else if(model.isSlotAvailable())
         {
             if(model.isSelected()){
                 lastSelectedIndex=position;
                 holder.tvSlot.setTextColor(context.getResources().getColor(R.color.ui_theme_color));
             }
             else
                 holder.tvSlot.setTextColor(context.getResources().getColor(R.color.black_opacity_60));
         }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }



    public class SlotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CustomFontTextView tvSlot;
        public SlotViewHolder(View itemView) {
            super(itemView);
            this.tvSlot= (CustomFontTextView) itemView.findViewById(R.id.tv_slot);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
        if(listener!=null)
            listener.itemCLicked(view,getAdapterPosition());
        }
    }

    public interface clickListener{
        public void itemCLicked(View view, int position);
    }

    public TimeSlotModel getSlotModelAtPosition(int position)
    {
        return  listData.get(position);
    }

    public int getLastSelectedIndex()
    {
        return lastSelectedIndex;
    }
}
