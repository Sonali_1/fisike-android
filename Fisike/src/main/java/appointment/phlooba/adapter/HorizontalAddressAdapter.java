package appointment.phlooba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;

/**
 * Created by laxmansingh on 6/19/2017.
 */

public class HorizontalAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private java.util.List<Address> list;
    private String from;
    private clickListener listener;

    /*[  .....loadmore variables needed.....   ]*/
    private final int VIEW_TYPE_ITEM = 1;
    /*[  .....End of loadmore variables needed.....   ]*/


    public HorizontalAddressAdapter(Context context, final java.util.List<Address> list) {
        this.list = list;
        this.mContext = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.horizontaladdress_list_item, null);
            HorizontalAddressAdapter.HorizontalAddressViewHolder horizontalAddressViewHolder = new HorizontalAddressAdapter.HorizontalAddressViewHolder(view, mContext);
            return horizontalAddressViewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int position) {


        if (customViewHolder instanceof HorizontalAddressAdapter.HorizontalAddressViewHolder) {


            String address = getAddress(list.get(position));
            ((HorizontalAddressViewHolder) customViewHolder).tv_address.setText(address);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public Address getElementAtPosition(int position) {
        return list.get(position);
    }


    public class HorizontalAddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_address;
        CustomFontButton btn_proceed;

        public HorizontalAddressViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setOnClickListener(this);
            tv_address = (CustomFontTextView) view.findViewById(R.id.tv_address);
            btn_proceed = (CustomFontButton) view.findViewById(R.id.btn_proceed);
            btn_proceed.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {
                case R.id.btn_proceed:
                    if(listener!=null)
                        listener.itemClicked(adapter_position);
                    break;

                default:break;
            }

        }


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }


    public String getAddress(Address addressModel) {
        String address = "";
        if (addressModel != null) {
            address = addressModel.getText();
            String city = addressModel.getCity();
            String state = addressModel.getState();
            String postalcode = addressModel.getPostalCode();
            String country = addressModel.getCountry();

           /* address = (Utils.isValueAvailable(text) ? text + " " : "") +
                    ((Utils.isValueAvailable(city) ? city : "") + "\n") +
                    (Utils.isValueAvailable(state) ? state + " " : "") +
                    (Utils.isValueAvailable(postalcode) ? postalcode : "") +
                    (Utils.isValueAvailable(country) ? "\n" + country : "");*/
        }
        return address;

    }

    public interface clickListener{
        public void itemClicked(int position);
    }

    public void setClickListener(clickListener listener){
        this.listener=listener;
    }

}
