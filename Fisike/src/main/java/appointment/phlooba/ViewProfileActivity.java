package appointment.phlooba;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.AddressTypeEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.views.CircleImageView;

import java.util.ArrayList;

import appointment.phlooba.model.ImageDataType;
import appointment.model.LinkedPatientInfoMO;
import appointment.phlooba.adapter.ImageGridAdapter;


/**
 * Created by neharathore on 20/07/17.
 */

public class ViewProfileActivity extends BaseActivity implements View.OnClickListener {


    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private CustomFontTextView txtUserStatus, txtStatusHeader, txtNameHeader;
    private CustomFontTextView txtUserName;
    private CustomFontTextView txtUserGender;
    private CustomFontTextView txtUserDOB;
    private FrameLayout frameLayout;
    private CardView cardViewUserAddress;
    private CustomFontTextView txtAadharHeader, txtAadhar;
    private CardView cardPastConditions, cardAlternateContact, cardEmail;
    private LinkedPatientInfoMO patientDetails;
    private CustomFontTextView tvUserNumber, txtEmailAddress;
    private LinearLayout layoutAddAddressFields;
    private Context context;
    private RelativeLayout rlContainerUserImage;
    private ImageView imgProfile;
    private CircleImageView civUserProfilePic;
    private RelativeLayout btnChangeProfilePic;
    CustomFontButton btnVerifyEmail, btnUpdateEmail;
    private CardView cardinsurance;
    private RecyclerView rvInsuranceImage;
    private LinearLayout llManualInsurance;
    private ImageGridAdapter adapter;
    private ArrayList<ImageDataType> insuranceList;
    private CustomFontButton addInsuranceButton;
    private CustomFontTextView insuranceType, insurancetypeCompanyName, lblPolicyNumber, txtPolicyNo, lblGroupNo, txtGroupNo, lblPolicyHolderName, txtPolicyHolderName;
    private CustomFontButton btnDelete;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        showHideToolbar(false);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_user_profile, frameLayout);
        findView();
        initView();
        setData();
        setclickListeners();
    }

    private void setclickListeners() {


    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        txtUserName = (CustomFontTextView) findViewById(R.id.txtUserName);
        cardViewUserAddress = (CardView) findViewById(R.id.cardViewUserAddress);
        cardPastConditions = (CardView) findViewById(R.id.cardpastconditions);
        cardAlternateContact = (CardView) findViewById(R.id.cardViewUserContact);
        txtUserDOB = (CustomFontTextView) findViewById(R.id.txtUserDOB);
        txtUserStatus = (CustomFontTextView) findViewById(R.id.txtUserStatus);
        txtStatusHeader = (CustomFontTextView) findViewById(R.id.txtStatusHeader);
        //AadharView
        txtAadharHeader = (CustomFontTextView) findViewById(R.id.txtAadharHeader);
        txtAadhar = (CustomFontTextView) findViewById(R.id.txtAadhar);
        tvUserNumber = (CustomFontTextView) findViewById(R.id.tv_user_number);
        layoutAddAddressFields = (LinearLayout) findViewById(R.id.layoutAddAdressFields);
        rlContainerUserImage = (RelativeLayout) findViewById(R.id.rl_container_userImage);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        civUserProfilePic = (CircleImageView) findViewById(R.id.civ_user_profile_pic);
        btnChangeProfilePic = (RelativeLayout) findViewById(R.id.btn_change_profile_pic);
        txtUserGender = (CustomFontTextView) findViewById(R.id.txtUserGender);
        txtNameHeader = (CustomFontTextView) findViewById(R.id.txtNameHeader);

        findViewById(R.id.btn_change_profile_pic).setOnClickListener(this);
        findEmailCardElements();
        findInsuranceCardElements();

    }

    private void findInsuranceCardElements() {
        cardinsurance = (CardView) findViewById(R.id.card_insurance_parent);
        rvInsuranceImage = (RecyclerView) findViewById(R.id.rv_insurance);
        llManualInsurance = (LinearLayout) findViewById(R.id.ll_manual_insurance);
        addInsuranceButton = (CustomFontButton) findViewById(R.id.add_insurance_button);
        insuranceType = (CustomFontTextView) findViewById(R.id.insuranceType);
        insurancetypeCompanyName = (CustomFontTextView) findViewById(R.id.insurancetype_company_name);
        lblPolicyNumber = (CustomFontTextView) findViewById(R.id.lbl_policy_number);
        txtPolicyNo = (CustomFontTextView) findViewById(R.id.txt_policy_no);
        lblGroupNo = (CustomFontTextView) findViewById(R.id.lbl_group_number);
        txtGroupNo = (CustomFontTextView) findViewById(R.id.txt_group_no);
        lblPolicyHolderName = (CustomFontTextView) findViewById(R.id.lbl_policy_holder_name);
        txtPolicyHolderName = (CustomFontTextView) findViewById(R.id.txt_policy_holder_name);
        btnDelete = (CustomFontButton) findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(this);

    }

    private void findEmailCardElements() {
        cardEmail = (CardView) findViewById(R.id.parent_card_email);
        txtEmailAddress = (CustomFontTextView) findViewById(R.id.txt_email_text);
        btnVerifyEmail = (CustomFontButton) findViewById(R.id.btn_verify_email);
        btnUpdateEmail = (CustomFontButton) findViewById(R.id.btn_update_email);
        btnVerifyEmail.setOnClickListener(this);
        btnUpdateEmail.setOnClickListener(this);
        
    }

    private void initView() {
        initToolbar();
        //hiding past conditions
        cardPastConditions.setVisibility(View.GONE);
        //hiding cardAlternateContact
        cardAlternateContact.setVisibility(View.GONE);
        //hiding aadhar
        txtAadhar.setVisibility(View.GONE);
        txtAadharHeader.setVisibility(View.GONE);
        //hiding marital status
        txtUserStatus.setVisibility(View.GONE);
        txtStatusHeader.setVisibility(View.GONE);
        cardViewUserAddress.setVisibility(View.GONE);

    }

    private void setData() {

        try {
            patientDetails = (LinkedPatientInfoMO) getIntent().getExtras().getSerializable("linkedPatientInfo");
        } catch (Exception e) {
        }
        txtNameHeader.setText(getString(R.string.mobile_number));
        if (patientDetails != null) {
            if (Utils.isValueAvailable(patientDetails.getName()))
                tvUserNumber.setText(patientDetails.getName());
            if (Utils.isValueAvailable(patientDetails.getPhoneNumber())) {
                txtUserName.setText(patientDetails.getPhoneNumber());
            }
            else
            {
                txtNameHeader.setVisibility(View.GONE);
                txtUserName.setVisibility(View.GONE);
            }
            if (Utils.isValueAvailable(patientDetails.getGender())) {
                String gender = patientDetails.getGender();
                if (gender.equalsIgnoreCase(getString(R.string.gender_male_initial)))
                    gender = GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_male_key));
                else if (gender.equalsIgnoreCase(getString(R.string.gender_female_initial)))
                    gender = GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_female_key));
                else
                    gender = GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_other_key));

                txtUserGender.setText(gender);
            }
            if (Utils.isValueAvailable(patientDetails.getDateOfBirth()))
                txtUserDOB.setText(DateTimeUtil.getFormattedDateWithoutTime(patientDetails.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            if (Utils.isValueAvailable(patientDetails.getEmail())) {
                setEmailData(patientDetails.getEmail());
            }

            setUserProfilePic(Utils.convertBase64ToBitmap(patientDetails.getProfilePic()));
            initInsuranceData();
        }
    }

    private void setEmailData(String emailAddress) {
        cardEmail.setVisibility(View.VISIBLE);
        txtEmailAddress.setText(emailAddress);
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar_title.setText(getResources().getString(R.string.profile));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewProfileActivity.this.finish();
            }
        });
    }

    private void setAddress() {
        ArrayList<Address> addresses = null;
        if (addresses == null || addresses.size() == 0) {
            findViewById(R.id.txtAddressSubHeader).setVisibility(View.VISIBLE);
            layoutAddAddressFields.removeAllViews();
            cardViewUserAddress.setVisibility(View.GONE);
            return;
        }
        layoutAddAddressFields.removeAllViews();
        findViewById(R.id.txtAddressSubHeader).setVisibility(View.GONE);
        for (int i = 0; userMO.getAddressUserMos() != null && i < userMO.getAddressUserMos().size(); i++) {
            Address address = userMO.getAddressUserMos().get(i);
            addAddress(address, i);
        }
    }

    /**
     * Add address to be shown in the UI
     *
     * @param address
     * @param index
     */
    private void addAddress(final Address address, int index) {
        int dp29 = (int) Utils.convertDpToPixel(29, context);
        int dp12 = (int) Utils.convertDpToPixel(12, context);
        int dp7 = (int) Utils.convertDpToPixel(7, context);
        int dp20 = (int) Utils.convertDpToPixel(20, context);
        CustomFontTextView txtAddressHeader = new CustomFontTextView(this);
        if (address.getUseCode() == null || address.getUseCode().equals("")) {
            txtAddressHeader.setText("");
        } else {
            String useCode = address.getUseCode();
            useCode = AddressTypeEnum.getDisplayedValuefromCode(useCode);
            txtAddressHeader.setText(useCode.contains(getResources().getString(R.string.Address)) ? useCode : (useCode + " " + getResources().getString(R.string._Address)));
        }
        txtAddressHeader.setTextColor(getResources().getColor(R.color.dusky_blue_50));
        txtAddressHeader.setTextSize(12);
        CustomFontTextView txtAddress = new CustomFontTextView(this);

        String postal = address.getPostalCode();
        String city = address.getCity();
        String state = address.getState();
        String country = address.getCountry();
        ArrayList<String> line = address.getLine();
        String addLine = new String();
        for (int i = 0; i < line.size(); i++)
            if (!line.get(i).trim().equals(""))
                addLine += line.get(i) + ", ";

        String tempAddress = new String();
        if (addLine != null)
            tempAddress += addLine;
        if (city != null && !city.equals(getResources().getString(R.string.txt_null)) && !city.equals(""))
            tempAddress += city + ", ";
        if (state != null && !state.equals(getResources().getString(R.string.txt_null)) && !state.equals(""))
            tempAddress += state + ", ";
        if (postal != null && !postal.equals(getResources().getString(R.string.txt_null)) && !postal.equals(""))
            tempAddress += postal + ", ";
        if (country != null && !country.equals(getResources().getString(R.string.txt_null)) && !country.equals(""))
            tempAddress += country;
        txtAddress.setText(tempAddress);
        txtAddress.setTextColor(getResources().getColor(R.color.dusky_blue_70));
        txtAddress.setTextSize(14);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(dp29, 0, dp29, 0);
        txtAddress.setLayoutParams(lp);
        txtAddress.setGravity(Gravity.CENTER_VERTICAL);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        txtAddressHeader.setLayoutParams(lp);
        txtAddressHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_menu_overflow, 0);
        txtAddressHeader.setGravity(Gravity.CENTER_VERTICAL);
        relativeLayout.addView(txtAddressHeader);
        relativeLayout.setGravity(Gravity.CENTER_VERTICAL);
        RelativeLayout.LayoutParams layoutRule = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutRule.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        LinearLayout.LayoutParams relativeLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        relativeLayoutParams.setMargins(dp29, index > 0 ? 30 : 0, dp7, 0);
        relativeLayout.setLayoutParams(relativeLayoutParams);
        if (index > 0) {
            View view = new View(context);
            LinearLayout.LayoutParams lineView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 3);
            lineView.setMargins(0, dp12, 0, 0);
            view.setLayoutParams(lineView);
            view.setBackgroundColor(getResources().getColor(R.color.blue_grey_20));
            layoutAddAddressFields.addView(view);
        }
        layoutAddAddressFields.addView(relativeLayout);
        layoutAddAddressFields.addView(txtAddress);
    }

    private void setUserProfilePic(final Bitmap bitmap) {
        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap blurBitmap;

                if (bitmap != null) {
                    civUserProfilePic.setImageBitmap(bitmap);
                    byte[] profilePictureByte = Utils.convertBitmapToByteArray(bitmap);
                    try {
                        blurBitmap = Utils.blur(context, BitmapFactory.decodeByteArray(profilePictureByte, 0, profilePictureByte.length), 20f);
                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        blurBitmap = Utils.scaleBitmapAndKeepRation(blurBitmap, (int) Utils.convertDpToPixel(187, context), metrics.widthPixels);

                    } catch (Exception e) {
                        civUserProfilePic.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.default_profile_pic));
                        blurBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_default_profile_bg);
                    }
                } else {

                    civUserProfilePic.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.default_profile_pic));
                    blurBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_default_profile_bg);
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    imgProfile.setImageBitmap(blurBitmap);
                } else {
                    imgProfile.setImageBitmap(blurBitmap);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {

        Toast.makeText(context, "Under development", Toast.LENGTH_SHORT).show();
    }


    private void initInsuranceData() {
        addInsuranceButton.setOnClickListener(this);
        if (Utils.isValueAvailable(patientDetails.getInsuranceType()) && patientDetails.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_picture))) {
            insuranceList = new ArrayList<>();
            if (Utils.isValueAvailable(patientDetails.getCardFrontImage()))
                addElementInList(insuranceList, Utils.convertBase64ToBitmap(patientDetails.getCardFrontImage()), getString(R.string.front_image));
            adapter = new ImageGridAdapter(insuranceList, this, true);
            rvInsuranceImage.setLayoutManager(new GridLayoutManager(this, 2));
            rvInsuranceImage.setAdapter(adapter);
            llManualInsurance.setVisibility(View.GONE);

            if (insuranceList.size() == 0)
                cardinsurance.setVisibility(View.GONE);
            else
                cardinsurance.setVisibility(View.VISIBLE);
        } else if(Utils.isValueAvailable(patientDetails.getInsuranceType())){
            llManualInsurance.setVisibility(View.VISIBLE);
            rvInsuranceImage.setVisibility(View.GONE);
            cardinsurance.setVisibility(View.VISIBLE);
                if (Utils.isValueAvailable(patientDetails.getInsuranceType())) {
                    if (patientDetails.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicare)) && Utils.isValueAvailable(patientDetails.getPolicyNumber())) {
                        insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicare)));
                        lblPolicyNumber.setText(getString(R.string.medicare_number));
                        txtPolicyNo.setText(patientDetails.getPolicyNumber());
                    } else if (patientDetails.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicaid)) && Utils.isValueAvailable(patientDetails.getPolicyNumber())) {
                        insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicaid)));
                        lblPolicyNumber.setText(getString(R.string.medicaid_number));
                        txtPolicyNo.setText(patientDetails.getPolicyNumber());
                    } else {
                        insuranceType.setText(patientDetails.getPrimaryInsuranceCompany());
                        txtPolicyNo.setText(patientDetails.getPolicyNumber());
                        txtGroupNo.setText(patientDetails.getGroupId());
                        txtPolicyHolderName.setText(patientDetails.getPolicyHolderName());
                        lblGroupNo.setVisibility(View.VISIBLE);
                        txtGroupNo.setVisibility(View.VISIBLE);
                        lblPolicyHolderName.setVisibility(View.VISIBLE);
                        txtPolicyHolderName.setVisibility(View.VISIBLE);
                        insurancetypeCompanyName.setVisibility(View.VISIBLE);

                    }

                }

        }

    }

    private void addElementInList(ArrayList<ImageDataType> listData, Bitmap bitmap, String tag) {
        ImageDataType data = new ImageDataType(tag, bitmap);
        listData.add(data);
    }

}

