package appointment.phlooba;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import appointment.phlooba.request.SaveAddressRequest;
import appointment.phlooba.response.SaveAddressResponse;
import signup.request.GetCityMapRequest;
import signup.request.GetCountryStateMapRequest;
import signup.response.GetCityMapResponse;
import signup.response.GetCountryStateMapResponse;

/**
 * Created by laxmansingh on 6/19/2017.
 */

public class AddAddressActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;

    private Spinner mSpinner_city, mSpinner_state, mSpinner_Country;
    private List<String> countryList, stateList, cityList;
    // private AddressAdapter state_adapter, city_adapter,country_adapter;
    private ArrayAdapter state_adapter, city_adapter, country_adapter;
    private View line_city;
    private CustomFontEditTextView mEt_add_line1, mEt_add_line2, mEt_postal_code;
    private CustomFontButton mBtn_save;
    private FrameLayout frameLayout;
    private String addressLine1,addressLine2,state,city,country,postalCode;
    private boolean isToUpdateCityList = false;
    private boolean isCityFetchCallRunning = false;
    private boolean isCityFetchedFirstTime = false;


    private static HashMap<String,ArrayList<String>> countryStateMap = new HashMap<String, ArrayList<String>>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.addaddress_activity, frameLayout);
        mContext = this;
        findView();
        initView();
        countryList = new ArrayList<String>();
        stateList = new ArrayList<String>();
        cityList = new ArrayList<String>();
        fetchAndDisplayTheCountryAndState();
        bindView();
    }

    private void fetchAndDisplayTheCountryAndState() {
        showProgressDialog(getResources().getString(R.string.loading_txt));
        ThreadManager.getDefaultExecutorService().submit(new GetCountryStateMapRequest(getTransactionId(), true));
    }

    public void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        line_city = (View) findViewById(R.id.line_city);
        mSpinner_city = (Spinner) findViewById(R.id.spinner_city);
        mSpinner_state = (Spinner) findViewById(R.id.spinner_state);
        mEt_add_line1 = (CustomFontEditTextView) findViewById(R.id.et_add_line1);
        mEt_add_line2 = (CustomFontEditTextView) findViewById(R.id.et_add_line2);
        mEt_postal_code = (CustomFontEditTextView) findViewById(R.id.et_postal_code);
        mBtn_save = (CustomFontButton) findViewById(R.id.btn_save);
        mSpinner_Country= (Spinner) findViewById(R.id.spinner_country);



    }

    public void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.add_address_title));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        userMO = SettingManager.getInstance().getUserMO();
    }


    public void bindView() {
        mBtn_save.setOnClickListener(this);
    }


    private void setSpinners(Spinner spinner, ArrayAdapter spinnerAdapter, List<String> list ){
        if(spinner == null || list == null){
            return;
        }

        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(this);
    }

    private void updateSpinnerAdapter(Spinner spinner, ArrayAdapter spinnerAdapter, List<String> list){
        if(spinnerAdapter == null){
            return;
        }

        spinnerAdapter.clear();
        spinnerAdapter.addAll(list);
        spinnerAdapter.notifyDataSetChanged();
    }

    private ArrayList<String> getDeepCopyofArrayList(ArrayList<String> source){
        ArrayList<String> b=(ArrayList<String>)source.clone();
        return b;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                Toast.makeText(mContext, "fab", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn_save:
                if(Utils.showDialogForNoNetwork(this,false))
                    saveAddress();
                break;
        }
    }

    private void saveAddress() {
        addressLine1 = mEt_add_line1.getText().toString().trim();
        addressLine2 = mEt_add_line2.getText().toString().trim();
        try {
            country = mSpinner_Country.getSelectedItem().toString();
            state = mSpinner_state.getSelectedItem().toString();
            city = mSpinner_city.getSelectedItem().toString();
        } catch (Exception e) {
        }
        postalCode = mEt_postal_code.getText().toString();
        if (!Utils.isValueAvailable(addressLine1))
            mEt_add_line1.setError(getString(R.string.blank_address_error,mEt_add_line1.getHint().toString()));

        else if (!Utils.isValueAvailable(country))
            Toast.makeText(this,getString(R.string.blank_address_error,getString(R.string.country_hint)),Toast.LENGTH_SHORT).show();
        else if(!Utils.isValueAvailable(state))
            Toast.makeText(this,getString(R.string.blank_address_error,getString(R.string.state)),Toast.LENGTH_SHORT).show();
        else if(!Utils.isValueAvailable(city))
            Toast.makeText(this,getString(R.string.blank_address_error,getString(R.string.city)),Toast.LENGTH_SHORT).show();
        else if (!Utils.isValueAvailable(postalCode))
            mEt_postal_code.setError(getString(R.string.blank_address_error,mEt_postal_code.getHint().toString()));
        else
        {
            Address address = new Address();
            ArrayList<String> line = new ArrayList<>();
            line.add(addressLine1);
            if (Utils.isValueAvailable(addressLine2))
                line.add(addressLine2);
            address.setLine(line);
            address.setCountry(country);
            address.setState(state);
            address.setCity(city);
            address.setPostalCode(postalCode);
            address.setUseCode("home");
            address.setText(Utils.createAddressTextString(addressLine1, addressLine2, country, state, city, postalCode));
            ArrayList<Address> addressList = new ArrayList<>();
            addressList.add(address);
            showProgressDialog();
            ThreadManager.getDefaultExecutorService().submit(new SaveAddressRequest(getTransactionId(), addressList, userMO.getPatientId()));
        }


    }


    @Subscribe
    public void onGetCountryStateMap(GetCountryStateMapResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        if (response.isSuccessful()) {
            dismissProgressDialog();
            countryList = response.getKeysList();

            if (response.getStatesAndCitiesList() != null && countryList.size() > 0) {
                countryStateMap = response.getStatesAndCitiesList();
                stateList = getListofStates(countryStateMap.get(countryList.get(0)));
            }
            country_adapter = new ArrayAdapter<String>(this,   R.layout.spinner_address, countryList);
            state_adapter =  new ArrayAdapter<String>(this,   R.layout.spinner_address, stateList);
            setSpinners(mSpinner_Country, country_adapter, countryList);
            setSpinners(mSpinner_state, state_adapter, stateList);
            if (countryList.size() > 0 && stateList.size() > 0) {
                showProgressDialog(getResources().getString(R.string.loading_txt));
                fetchCityList(countryList.get(0), stateList.get(0));

            }

        } else {
            dismissProgressDialog();
        }
    }

    private ArrayList<String> getListofStates(ArrayList<String> input){
      return getDeepCopyofArrayList(input);
    }


    private void fetchCityList(String country, String state) {
        if (isCityFetchCallRunning) {
            return;
        } else {
            ThreadManager.getDefaultExecutorService().submit(new GetCityMapRequest(getTransactionId(),
                    country,
                    state));
            isCityFetchCallRunning = true;
        }
    }



    @Subscribe
    public void onGetCityMapResponse(GetCityMapResponse response){
        isCityFetchCallRunning = false;
        if(getTransactionId()!= response.getTransactionId())

            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            if (response.getCityList().size() > 0) {
                cityList = getListofStates(response.getCityList());
                if(city_adapter == null)
                    city_adapter = new ArrayAdapter<String>(this,   R.layout.spinner_address, cityList);
                if(isToUpdateCityList && isCityFetchedFirstTime){
                    updateSpinnerAdapter(mSpinner_city, city_adapter, cityList);
                } else {

                    setSpinners(mSpinner_city, city_adapter, cityList);
                }
                isCityFetchedFirstTime = true;
            }
        } else {
            isCityFetchedFirstTime = true;
            Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
        }
    }


    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        switch (parent.getId()) {
            case R.id.spinner_country:
                // change the state
               String country = mSpinner_Country.getSelectedItem().toString();
                if(country != null && countryStateMap != null){
                    stateList = getListofStates(countryStateMap.get(country));
                    String state = stateList.get(0);
                    mSpinner_state.setSelection(0);
                    updateSpinnerAdapter(mSpinner_state, state_adapter, stateList);
                        if (country != null && state != null && isCityFetchedFirstTime) {
                            isToUpdateCityList = true;
                            fetchCityList(country, state);
                        }
                    }
                break;
            case R.id.spinner_state:
                // change the city
                String selectedCountry = mSpinner_Country.getSelectedItem().toString();
                String selectedState = mSpinner_state.getSelectedItem().toString();
                if(selectedCountry != null && selectedState != null) {
                    isToUpdateCityList = true;
                    fetchCityList(selectedCountry, selectedState);
                }
                break;
            case R.id.spinner_city:
                // do nothing
                break;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Subscribe
    public void onSaveAddressResponse(SaveAddressResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            ArrayList<Address> addressList = userMO.getAddressUserMos();
            if (response.getAddressList() != null && response.getAddressList().size() > 0) {
                if (addressList == null)
                    addressList = new ArrayList<>();

                addressList.addAll(response.getAddressList());
            }

            userMO.setAddressUserMos(addressList);
            UserDBAdapter.getInstance(this).updateAddress(addressList, userMO);
            Intent intent = new Intent();
            intent.putExtra("text1", addressLine1);
            intent.putExtra("text2", addressLine2);
            intent.putExtra("city", city);
            intent.putExtra("state", state);
            intent.putExtra("postalcode", postalCode);
            intent.putExtra("country", country);
            setResult(Activity.RESULT_OK, intent);
            finish();

        } else {
            Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
        }
    }


}
