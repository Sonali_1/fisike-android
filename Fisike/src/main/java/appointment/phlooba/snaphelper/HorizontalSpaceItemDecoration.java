package appointment.phlooba.snaphelper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by laxmansingh on 6/15/2017.
 */

public class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;

    public HorizontalSpaceItemDecoration(int space) {
        this.mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.right = mSpace;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = mSpace;
        }
    }
}