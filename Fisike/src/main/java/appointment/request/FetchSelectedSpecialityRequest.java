package appointment.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import appointment.network.BaseArrayRequest;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class FetchSelectedSpecialityRequest extends BaseArrayRequest {

    private long mTransactionId;
    private Context mContext;
    private String payload;

    public FetchSelectedSpecialityRequest(long transactionId, Context mContext, String payload) {
        mTransactionId = transactionId;
        this.mContext = mContext;
        this.payload = payload;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchResourcesForSpecialtyUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        appointment.network.APIObjectRequest request = new appointment.network.APIObjectRequest(Request.Method.POST, url, payload, this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new FetchSelectedSpecialityResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new FetchSelectedSpecialityResponse(response, mTransactionId, mContext));
    }
}