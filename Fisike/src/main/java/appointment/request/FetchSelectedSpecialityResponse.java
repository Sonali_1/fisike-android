package appointment.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.constant.TextConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.network.BaseResponse;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class FetchSelectedSpecialityResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;
    private Context context;
    ArrayList<AppointmentModel> SpecialityModelArrayList = new ArrayList<AppointmentModel>();


    public FetchSelectedSpecialityResponse(JSONArray response, long transactionId,
                                           Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public FetchSelectedSpecialityResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONArray response) {
        try {
            SpecialityModelArrayList.clear();
            if (response != null && response.length() > 0) {
                Gson gson1 = new Gson();
                Type listType = null;
                listType = new TypeToken<List<AppointmentModel>>() {
                }.getType();
                SpecialityModelArrayList.addAll((Collection<? extends AppointmentModel>) gson1.fromJson(response.toString(), listType));
            }
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }


    public ArrayList<AppointmentModel> getSpecialityModelArrayList() {
        return SpecialityModelArrayList;
    }

    public void setSpecialityModelArrayList(ArrayList<AppointmentModel> specialityModelArrayList) {
        SpecialityModelArrayList = specialityModelArrayList;
    }
}
