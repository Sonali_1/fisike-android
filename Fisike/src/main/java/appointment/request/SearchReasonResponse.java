package appointment.request;

import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by aastha on 11/21/2016.
 */
public class SearchReasonResponse extends BaseResponse {
    ArrayList<String> reasonList = new ArrayList<>();
    public SearchReasonResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        try {
            parseResponse(response.toString());
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    private void parseResponse(String response) throws JSONException {
        JSONObject reasonObject= new JSONObject(response);
        JSONArray reasonListArray= reasonObject.getJSONArray("list");
        for (int i=0;i<reasonListArray.length();i++){
            JSONObject jsonObject = reasonListArray.getJSONObject(i);
            String displayReason = jsonObject.getString("display");
            reasonList.add(i,displayReason);
        }
    }

    public SearchReasonResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }

    public ArrayList<String> getReasonList() {
        return reasonList;
    }
}
