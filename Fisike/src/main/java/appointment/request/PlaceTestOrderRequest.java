package appointment.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import appointment.interfaces.ResponseCallback;

/**
 * Created by laxmansingh on 3/9/2016.
 */
public class PlaceTestOrderRequest extends BaseObjectRequest {

    private long mTransactionId;
    ResponseCallback responseCallback;
    private UserMO userMO;
    private String payload;

    public PlaceTestOrderRequest(long transactionId, String payload, ResponseCallback responseCallback) {
        mTransactionId = transactionId;
        this.responseCallback = responseCallback;
        try {
            this.payload = payload;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getPlaceTestOrderUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, payload, this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new PlaceTestOrderResponse(error, mTransactionId, responseCallback));
    }

    @Override
    public void onResponse(JSONObject response) {
        BusProvider.getInstance().post(new PlaceTestOrderResponse(response, mTransactionId, responseCallback));

    }
}