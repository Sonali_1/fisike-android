package appointment.request;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;
import appointment.network.APIObjectRequest;
import appointment.network.BaseArrayRequest;

/**
 * Created by Aastha on 16/03/2016.
 */
public class SearchDoctorRequest extends BaseArrayRequest {
    private long mTransactionId;
    ResponseCallback responseCallback;
    private String mSearchString;
    public SearchDoctorRequest(long transactionId, String s, ResponseCallback responseCallback) {
        mTransactionId = transactionId;
        mSearchString=s;
        this.responseCallback=responseCallback;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchBookablePractitionerUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if (TextUtils.isEmpty(mSearchString)) {
            BusProvider.getInstance().post(new SearchDoctorResponse(error, mTransactionId,responseCallback));
        } else {
            BusProvider.getInstance().post(new SearchDoctorResponse(error, mTransactionId,responseCallback));
        }
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SearchDoctorResponse(response,mSearchString,mTransactionId,responseCallback));

    }

    public String getPayload() {
        JSONObject payLoad = new JSONObject();
        JSONObject constraint= new JSONObject();
        try {
            constraint.put("_count", 10);
            constraint.put("_skip", 0);
            constraint.put("searchString", mSearchString);
            payLoad.put("constraints",constraint);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
