package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.model.LinkedPatientInfoMO;
import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;

/**
 * Created by aastha on 11/9/2016.
 */
public class FetchSelfAndAssociatedPatientsResponse extends BaseResponse {

    public JSONObject response;

    ArrayList<LinkedPatientInfoMO> patientInfoMOArrayList = new ArrayList<>();
/*

    public FetchSelfAndAssociatedPatientsResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        try {
            this.response = response;
            parseResponse(this.response.toString());
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }
*/

    public FetchSelfAndAssociatedPatientsResponse(JSONObject response, long mTransactionId) {

        super(response, mTransactionId);
        try {
            doParse(response);
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }


    private boolean isPrimaryEmailorPhone(JSONObject jsonObject) {
        String key;
        boolean value;
        JSONArray extension = jsonObject.optJSONArray("extension");
        if (extension == null)
            return false;
        for (int i = 0; i < extension.length(); i++) {
            try {
                JSONObject primary = extension.getJSONObject(i);
                if (primary.has("url") && primary.has("value")) {
                    key = primary.getString("url");
                    value = primary.optBoolean("value");
                    if (key.equalsIgnoreCase("primary") && value == true)
                        return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    private void parseResponse(JSONArray response) throws JSONException {
        JSONArray jsonArray = new JSONArray(response.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            String email = "", mobileno = "";

            try {
                JSONObject dependentPatient = jsonArray.getJSONObject(i).getJSONObject("dependentPatient");
                String birthdate = dependentPatient.optJSONObject("birthDate").getString("value");
                String name = dependentPatient.optJSONArray("name").getJSONObject(0).getString("text");
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(dependentPatient.toString(), PatientMO.class);
                PatientMO patientMO = (PatientMO) jsonToObjectMapper;
                String gender = dependentPatient.optString("gender");
                int id = dependentPatient.optInt("id");
/*
                LinkedPatientInfoMO patientInfoMO = new LinkedPatientInfoMO(birthdate, name, mobileno, email, gender, id, patientMO);

                patientInfoMOArrayList.add(i, patientInfoMO);
*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void doParse(JSONObject response) {
        try {
            JSONArray patients = response.optJSONArray("patients");
            for (int i = 0; patients != null && i < patients.length(); i++) {
                JSONObject patientJSONObject = patients.getJSONObject(i);
                LinkedPatientInfoMO linkedPatientInfoMO = new LinkedPatientInfoMO();
                String firstName = patientJSONObject.optString("firstName");

                String middleName = patientJSONObject.optString("middleName");

                String lastName = patientJSONObject.optString("lastName");
                String name = (Utils.isValueAvailable(firstName) ? Utils.capitalizeFirstLetter(firstName) + " " : "") +
                        (Utils.isValueAvailable(middleName) ? Utils.capitalizeFirstLetter(middleName) + " " : "") +
                        (Utils.isValueAvailable(lastName) ? Utils.capitalizeFirstLetter(lastName) : "");

                linkedPatientInfoMO.setName(name);
                linkedPatientInfoMO.setGender(patientJSONObject.optString("gender"));
                linkedPatientInfoMO.setPatientId(patientJSONObject.optLong("id"));
                linkedPatientInfoMO.setDateOfBirth(patientJSONObject.optString("dob"));
                linkedPatientInfoMO.setEmail(patientJSONObject.optString("email"));
                linkedPatientInfoMO.setPhoneNumber(patientJSONObject.optString("phoneNo"));
                linkedPatientInfoMO.setProfilePic(patientJSONObject.optString("profilePicture"));
                linkedPatientInfoMO.setRelationshipWithUser(patientJSONObject.optString("relationshipWithUser"));
                linkedPatientInfoMO.setMaritalStatus(patientJSONObject.optString("maritalStatus"));
                JSONArray jsonArrayCoverages = patientJSONObject.optJSONArray("coverage");

                ArrayList<Coverage> coverages = new ArrayList<>();
                if (jsonArrayCoverages != null) {
                    try {
                        for (int j = 0; i < jsonArrayCoverages.length(); j++) {
                            JSONObject jsonObjectCoverage = jsonArrayCoverages.optJSONObject(j);

                            Coverage coverage = new Coverage();
                            coverage.setFrontImage(jsonObjectCoverage.optString("frontImage"));
                            coverage.setBackImage(jsonObjectCoverage.optString("backImage"));
                            coverage.setInsuranceType(jsonObjectCoverage.optString("insuranceType"));

                            coverage.setId(jsonObjectCoverage.optInt("id"));
                            coverage.setSubscriberRefId(jsonObjectCoverage.optInt("subscriberRefId"));

                            coverage.setPolicyHolderName(jsonObjectCoverage.optString("policyHolderName"));
                            coverage.setPolicyNumber(jsonObjectCoverage.optString("policyNumber"));
                            coverage.setPrimaryInsuranceCompany(jsonObjectCoverage.optString("primaryInsuranceCompany"));
                            coverage.setStatus(jsonObjectCoverage.optString("status"));
                            coverage.setBackImageMimeType(jsonObjectCoverage.optString("frontImageMimeType"));
                            coverage.setFrontImageMimeType(jsonObjectCoverage.optString("backImageMimeType"));
                            coverage.setGroupId(jsonObjectCoverage.optString("groupId"));
                            coverages.add(coverage);
                        }
                    } catch (Exception e) {
                    }
                }
                linkedPatientInfoMO.setCoverage(coverages);

                //parsing address
                JSONArray addressArray = patientJSONObject.optJSONArray("addressList");
                ArrayList<Address> addressArrayList = new ArrayList<>();
                for (int j = 0; addressArray != null && addressArray.length() > 0 && j < addressArray.length(); j++) {
                    Address address;
                    try {
                        JSONObject addressObject = addressArray.getJSONObject(j);
                        address = new Address();
                        address.setCountry(addressObject.optString("country"));
                        address.setCity(addressObject.optString("city"));
                        address.setState(addressObject.optString("state"));
                        address.setPostalCode(addressObject.optString("postalCode"));
                        address.setDistrict(addressObject.optString("district"));
                        address.setUseCode(addressObject.optString("useCode"));
                        address.setText(addressObject.optString("text"));
                        address.setType(addressObject.optString("type"));
                        address.setPrimary(addressObject.optBoolean("primary"));
                        JSONArray lineArray = addressObject.optJSONArray("line");
                        if (lineArray != null && lineArray.length() > 0) {
                            ArrayList<String> line = new ArrayList<>();
                            for (int k = 0; k < lineArray.length(); k++) {
                                line.add(lineArray.optString(k));

                            }
                            address.setLine(line);
                        }

                        addressArrayList.add(address);
                    } catch (Exception e) {
                    }
                }
                linkedPatientInfoMO.setAddressList(addressArrayList);

                //parsing identifier list
                JSONArray identifierJsonArray = patientJSONObject.optJSONArray("identifier");
                ArrayList<FetchUpdateIdentifier> identifierArrayList = null;
                for (int j = 0; identifierJsonArray != null && j < identifierJsonArray.length(); j++) {
                    try {
                        JSONObject identifierObject = identifierJsonArray.getJSONObject(j);
                        FetchUpdateIdentifier identifier = new FetchUpdateIdentifier();
                        identifier.setSystem(identifierObject.optString("system"));
                        identifier.setText(identifierObject.optString("text"));
                        identifier.setUsecode(identifierObject.optString("useCode"));
                        identifier.setValue(identifierObject.optString("value"));
                        if (identifierArrayList == null)
                            identifierArrayList = new ArrayList<>();
                        identifierArrayList.add(identifier);
                    } catch (Exception e) {

                    }

                }
                linkedPatientInfoMO.setIdentifier(identifierArrayList);

                //parsing Email list
                JSONArray emailArrayList = patientJSONObject.optJSONArray("emailList");
                ArrayList<FetchUpdateTelecom> emailList = null;
                for (int j = 0; emailArrayList != null && j < emailArrayList.length(); j++) {
                    try {
                        JSONObject emailObject = emailArrayList.getJSONObject(j);
                        FetchUpdateTelecom telecom = new FetchUpdateTelecom();
                        telecom.setSystem(emailObject.optString("system"));
                        telecom.setValue(emailObject.optString("value"));
                        telecom.setPrimary(emailObject.optBoolean("primary"));
                        telecom.setCountryCode(emailObject.optString("countryCode"));
                        telecom.setUseCode(emailObject.optString("useCode"));
                        telecom.setVerified(emailObject.optBoolean("verified"));
                        if (emailList == null)
                            emailList = new ArrayList<>();
                        emailList.add(telecom);
                    } catch (Exception e) {

                    }

                }
                linkedPatientInfoMO.setEmailList(emailList);


                //parsing Email list
                JSONArray phoneArrayList = patientJSONObject.optJSONArray("phoneList");
                ArrayList<FetchUpdateTelecom> phoneList = null;
                for (int j = 0; phoneArrayList != null && j < phoneArrayList.length(); j++) {
                    try {
                        JSONObject phoneObject = phoneArrayList.getJSONObject(j);
                        FetchUpdateTelecom telecom = new FetchUpdateTelecom();
                        telecom.setSystem(phoneObject.optString("system"));
                        telecom.setValue(phoneObject.optString("value"));
                        telecom.setPrimary(phoneObject.optBoolean("primary"));
                        telecom.setCountryCode(phoneObject.optString("countryCode"));
                        telecom.setUseCode(phoneObject.optString("useCode"));
                        telecom.setVerified(phoneObject.optBoolean("verified"));
                        if (phoneList == null)
                            phoneList = new ArrayList<>();
                        phoneList.add(telecom);
                    } catch (Exception e) {

                    }

                }
                linkedPatientInfoMO.setPhoneList(phoneList);

                patientInfoMOArrayList.add(linkedPatientInfoMO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public FetchSelfAndAssociatedPatientsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);

    }

    public ArrayList<LinkedPatientInfoMO> getPatientInfoMOArrayList() {
        return patientInfoMOArrayList;
    }
}
