package appointment.request;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;
import appointment.network.APIObjectRequest;
import appointment.network.BaseArrayRequest;

/**
 * Created by laxmansingh on 3/29/2016.
 */
public class SearchRequest extends BaseArrayRequest {

    private long mTransactionId;
    private String mSearchString;
    private String from;
    private Context mContext;

    public SearchRequest(Context mContext, long transactionId, String s, String from) {
        this.mContext = mContext;
        mTransactionId = transactionId;
        mSearchString = s;
        this.from = from;
    }

    @Override
    public void doInBackground() {
        String url = null;

        if (from.equals(TextConstants.DOCTORS)) {
            url = MphRxUrl.getFetchBookablePractitionerUrl();
        } else if (from.equals(TextConstants.SPECIALITY)) {
            url = MphRxUrl.getfetchBookableSpecialtiesUrl();
        } else if (from.equals(TextConstants.HOME_SAMPLE)||(from.equals(TextConstants.LAB_TEST))) {
            url = MphRxUrl.getFetchBookableUrl();
        }


        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayload() {
        JSONObject payLoad = new JSONObject();
        JSONObject constraint = new JSONObject();
        try {
            constraint.put("_count", 10);
            constraint.put("_skip", 0);
            constraint.put("searchString", mSearchString);
            payLoad.put("constraints", constraint);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if (TextUtils.isEmpty(mSearchString)) {
            BusProvider.getInstance().post(new SearchTestResponse(error, mTransactionId));
        } else {
            BusProvider.getInstance().post(new SearchTestResponse(error, mTransactionId));
        }
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SearchTestResponse(response, mSearchString, mTransactionId));
    }
}
