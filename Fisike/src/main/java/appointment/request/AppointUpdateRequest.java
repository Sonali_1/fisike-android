package appointment.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.appointmodel.Value;

/**
 * Created by laxmansingh on 3/16/2016.
 */
public class AppointUpdateRequest extends BaseObjectRequest {

    private long mTransactionId;
    private AppointmentResponseCallback responseCallback;
    private UserMO userMO;
    private String reschedule_or_cancel;
    private appointment.model.appointmodel.List appointmentmodel;
    private String reason = "", comment = "";


    public AppointUpdateRequest(long transactionId, AppointmentResponseCallback responseCallback, appointment.model.appointmodel.List appointmentmodel, String reschedule_or_cancel, String reason, String comment) {
        mTransactionId = transactionId;
        this.responseCallback = responseCallback;
        this.reschedule_or_cancel = reschedule_or_cancel;
        this.appointmentmodel = appointmentmodel;
        this.reason = reason;
        this.comment = comment;
    }

    @Override
    public void doInBackground() {
        String url = null;
        if (reschedule_or_cancel.equals(TextConstants.CANCEL_TEXT)) {
            url = MphRxUrl.getAppointmentCancelUrl() + appointmentmodel.getId();
        } else if (reschedule_or_cancel.equals(TextConstants.RESCHEDULE)) {
            url = MphRxUrl.getAppointmentRescheduleUrl() + appointmentmodel.getId();
        }

        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.PUT, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayload() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);

        String formattedDate = df.format(c.getTime());
        System.out.println("Currenttime => " + formattedDate);
        userMO = SettingManager.getInstance().getUserMO();


        /*[....    Creating  cancelled value for extension   ....]*/
        Value value = new Value();
        if (reschedule_or_cancel.equals(TextConstants.CANCEL_TEXT)) {
            value.setStatus("cancelled");
        } else if (reschedule_or_cancel.equals(TextConstants.RESCHEDULE)) {
            value.setStatus("rescheduled");

        }
        value.setActionBy(userMO.getFirstName() + " " + userMO.getLastName());
        value.setActionReason(reason);
        value.setActionComment(comment);
        try {
            value.setActionOn(formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*[....    End of Creating  cancelled value for extension   ....]*/


        /*[....    key area to inject status and value to json model  ....]*/
        if (reschedule_or_cancel.equals(TextConstants.CANCEL_TEXT)) {
            appointmentmodel.setStatus("cancelled");
        } /*else if (reschedule_or_cancel.equals(TextConstants.RESCHEDULE)) {
            appointmentmodel.setStatus("rescheduled");
        }
*/
        appointmentmodel.getExtension().get(0).getValue().add(value);
       /*[....    End of key area to inject status and value to json model  ....]*/

        Gson gson = new Gson();
        String payLoad = gson.toJson(appointmentmodel);
        return payLoad.toString();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new AppointmentSearchResponse(error, mTransactionId, responseCallback, reschedule_or_cancel));
    }

    @Override
    public void onResponse(JSONObject response) {
        BusProvider.getInstance().post(new AppointmentSearchResponse(response, mTransactionId, responseCallback, reschedule_or_cancel));
    }
}