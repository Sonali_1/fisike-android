package appointment.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.persistence.ConfigDBAdapter;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;
import appointment.network.APIObjectRequest;
import appointment.network.BaseArrayRequest;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class PopularSpecialityRequest extends BaseArrayRequest {

    private long mTransactionId;
    private Context mContext;

    public PopularSpecialityRequest(long transactionId, Context mContext) {
        mTransactionId = transactionId;
        this.mContext = mContext;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchPopularSpecialitiesUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url, null, this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new PopularSpecialityResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new PopularSpecialityResponse(response, mTransactionId, mContext));
    }
}
