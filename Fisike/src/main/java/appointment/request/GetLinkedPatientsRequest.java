package appointment.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;

import appointment.interfaces.ResponseCallback;
import appointment.network.APIObjectRequest;
import appointment.network.BaseArrayRequest;

/**
 * Created by laxmansingh on 9/23/2017.
 */

public class GetLinkedPatientsRequest extends BaseArrayRequest {
    private long mTransactionId;
    ResponseCallback responseCallback;
    private int mPatientid;

    public GetLinkedPatientsRequest(long transactionId) {
        this.mTransactionId = transactionId;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getLinkedPatientUrl();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        long patientId= userMO.getPatientId();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url+patientId, "", this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new GetLinkedPatientsResponse(error, mTransactionId));

    }

  /*  @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new GetLinkedPatientsResponse(response, mTransactionId));
    }*/

    public long getmTransactionId() {
        return mTransactionId;
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new GetLinkedPatientsResponse(response, mTransactionId));
    }
}