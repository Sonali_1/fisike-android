package appointment.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.gson.request.Constraints;

import appointment.model.AppointmentSearchConstraintCommon;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class AppointmentSearchRequestCommon {

    @SerializedName("constraints")
    @Expose
    private AppointmentSearchConstraintCommon constraints;

    /**
     * @return The constraints
     */
    public AppointmentSearchConstraintCommon getConstraints() {
        return constraints;
    }

    /**
     * @param constraints The constraints
     */
    public void setConstraints(AppointmentSearchConstraintCommon constraints) {
        this.constraints = constraints;
    }
}
