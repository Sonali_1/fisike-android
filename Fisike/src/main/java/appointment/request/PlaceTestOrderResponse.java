package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;

/**
 * Created by laxmansingh on 3/9/2016.
 */
public class PlaceTestOrderResponse extends BaseResponse {

    public JSONObject response;
    ResponseCallback responseCallback;


    public PlaceTestOrderResponse(JSONObject response, long transactionId, ResponseCallback responseCallback) {
        super(response, transactionId);
        try {
            this.responseCallback = responseCallback;
            this.response = response;

            if (response.has("httpStatusCode") && response.has("msg")) {
                mIsSuccessful = false;
                this.responseCallback.passResponse(MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong),mIsSuccessful);
            } else {
                mIsSuccessful = true;
                this.responseCallback.passResponse(response.toString(),mIsSuccessful);
            }


        } catch (Exception e) {
            mIsSuccessful = false;
            this.responseCallback.passResponse(getMessage(), mIsSuccessful);
        }
    }

    public PlaceTestOrderResponse(Exception exception, long transactionId, ResponseCallback responseCallback) {
        super(exception, transactionId);
        this.responseCallback = responseCallback;
        this.responseCallback.passResponse(getMessage(),mIsSuccessful);

    }

}

