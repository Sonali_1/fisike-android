package appointment.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;

/**
 * Created by Aastha on 21/03/2016.
 */
public class AppointmentShowRequest extends BaseObjectRequest {
    private long mTransactionId;
    ResponseCallback responseCallback;
    private int appointmentID;
    public AppointmentShowRequest(long transactionId, ResponseCallback responseCallback,int s) {
        mTransactionId = transactionId;
        appointmentID=s;
        this.responseCallback=responseCallback;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getAppointmentShowUrl(appointmentID);
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, "request", this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());

            BusProvider.getInstance().post(new AppointmentShowResponse(error, mTransactionId,responseCallback));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new AppointmentShowResponse(response,mTransactionId,responseCallback));

    }


}
