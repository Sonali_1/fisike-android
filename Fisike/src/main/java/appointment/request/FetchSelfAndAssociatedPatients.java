package appointment.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;

/**
 * Created by aastha on 11/9/2016.
 */
public class FetchSelfAndAssociatedPatients extends BaseObjectRequest {
    private long mTransactionId;
    ResponseCallback responseCallback;
    private int mPatientid;
    private String selfAll;

    public FetchSelfAndAssociatedPatients(long transactionId, String fetch) {
        this.mTransactionId = transactionId;
        selfAll = fetch;
    }

    @Override
    public void doInBackground() {
        /*String url = MphRxUrl.getLinkedPatientUrl();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        long patientId= userMO.getPatientId();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url+patientId, "", this, this);*/

        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, MphRxUrl.fetchSelfAndAssociatedPatientDetails(), getPrimaryPayLoad(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new FetchSelfAndAssociatedPatientsResponse(error, mTransactionId));
    }

    private String getPrimaryPayLoad() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("profilePictureType", "THUMBNAIL");
            payload.put("fetchPatients", selfAll);
        } catch (JSONException ignore) {
        }
        return payload.toString();
    }


    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new FetchSelfAndAssociatedPatientsResponse(response, mTransactionId));
    }

    public long getmTransactionId() {
        return mTransactionId;
    }

    /*@Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new FetchSelfAndAssociatedPatientsResponse(response, mTransactionId));
    }*/
}
