package appointment.request;

import android.content.Context;

import com.mphrx.fisike.models.VitalsHistoryModels;
import com.mphrx.fisike.models.VitalsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.network.BaseResponse;

/**
 * Created by laxmansingh on 2/3/2017.
 */

public class PopularSpecialityResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;
    private Context context;
    ArrayList<SpecialityDataModel> SpecialityDataModelArrayList = new ArrayList<SpecialityDataModel>();


    public PopularSpecialityResponse(JSONArray response, long transactionId,
                                     Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public PopularSpecialityResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONArray response) {
        try {
            SpecialityDataModelArrayList.clear();
            if (response != null && response.length() > 0) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject mainObject = response.getJSONObject(i);
                        SpecialityDataModel specialityDataModel = new SpecialityDataModel();
                        specialityDataModel.setServiceTypeId(mainObject.optString("serviceTypeId"));
                        specialityDataModel.setServiceTypeName(mainObject.optString("serviceTypeName"));
                        SpecialityDataModelArrayList.add(specialityDataModel);
                    } catch (Exception ex) {
                    }
                }
            }
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }


    public ArrayList<SpecialityDataModel> getSpecialityDataModelArrayList() {
        return SpecialityDataModelArrayList;
    }

    public void setSpecialityDataModelArrayList(ArrayList<SpecialityDataModel> specialityDataModelArrayList) {
        SpecialityDataModelArrayList = specialityDataModelArrayList;
    }
}
