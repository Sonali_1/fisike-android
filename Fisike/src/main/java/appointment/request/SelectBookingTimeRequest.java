package appointment.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import appointment.interfaces.AppointmentResponseCallback;
import appointment.model.OrderSummary;

/**
 * Created by laxmansingh on 4/4/2016.
 */
public class SelectBookingTimeRequest extends BaseObjectRequest {

    private long mTransactionId;
    AppointmentResponseCallback responseCallback;
    private UserMO userMO;
    private String slot_or_schedule;
    private String id = null;
    private String appointmentId = null;
    private boolean next;
    private String startDate;


    public SelectBookingTimeRequest(long transactionId, AppointmentResponseCallback responseCallback, String slot_or_schedule, String id, String appointmentId, boolean next, String startDate) {
        mTransactionId = transactionId;
        this.responseCallback = responseCallback;
        this.slot_or_schedule = slot_or_schedule;
        this.id = id;
        this.appointmentId = appointmentId;
        this.next = next;
        this.startDate = startDate;
    }

    @Override
    public void doInBackground() {
        String url = null;
        if (slot_or_schedule.equals(TextConstants.SCHEDULE)) {
            url = MphRxUrl.getScheduleSearchUrl();
        } else if (slot_or_schedule.equals(TextConstants.SLOT)) {
            url = MphRxUrl.getSlotSearchUrl();
        }
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayload() {

        userMO = SettingManager.getInstance().getUserMO();
        long patientid = OrderSummary.getInstance().getPatientId();
        if (patientid == 0)
            patientid = userMO.getPatientId();


        JSONObject payLoad = new JSONObject();
        JSONObject constraint = new JSONObject();
        try {
            /*[   key part for fetching schedule,slot  for getting  data  ]*/
            if (slot_or_schedule.equals(TextConstants.SCHEDULE)) {
                constraint.put("actor", Integer.parseInt(id));
                constraint.put("_skip", 0);
                payLoad.put("constraints", constraint);
                payLoad.put("totalCount", 0);
            } else if (slot_or_schedule.equals(TextConstants.SLOT)) {

/*[   Today's date and next 2 days after date   ]*/
                Calendar c = Calendar.getInstance();

                if (startDate != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                    try {
                        c.setTime(sdf.parse(startDate));
                        c.add(Calendar.DATE, 1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                TimeZone utc = TimeZone.getTimeZone("UTC");
                df.setTimeZone(utc);
                String startdate = df.format(c.getTime());
                c.add(Calendar.DATE, 2);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                String formattedDate = sdf.format(c.getTime());
                String enddate = formattedDate + "T23:59:59.000";
                try {
                    SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                    enddate = df.format(dfs.parse(enddate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
/*[   End of Today's date and next 2 days after date   ]*/

                JSONArray jsonArray = new JSONArray();
                try {

                    jsonArray.put(">" + startdate + "Z");
                    jsonArray.put("<" + enddate + "Z");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                constraint.put("actor", Integer.parseInt(id));
                constraint.put("patientId", patientid);
                constraint.put("schedule", 0);
                constraint.put("appointmentId", appointmentId == null ? JSONObject.NULL : appointmentId);
                constraint.put("start", jsonArray);
                constraint.put("_skip", 0);
                constraint.put("nextSlots", next);
                constraint.put("_sort:asc", "start.value");
                payLoad.put("constraints", constraint);
            }
            /*[  End of key part for fetching schedule,slot for getting  data  ]*/

            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SelectBookingTimeResponse(error, mTransactionId, responseCallback, slot_or_schedule));
    }

    @Override
    public void onResponse(JSONObject response) {
       /* try {
            response = new JSONObject("{\"totalCount\":3,\"list\":[{\"id\":\"7bbdb440-aed5-4082-b615-51575f87df8f\",\"freeBusyType\":\"free\"," +
                    "\"start\":\"2016-12-10T19:29:00+01:00\",\"end\":\"2016-12-10T20:30:00+01:00\",\"resourceType\":\"Slot\"},{\"id\":\"2262b4ec-9520-4789-8986-659c33331f72\",\"freeBusyType\":\"free\"," +
                    "\"start\":\"2016-12-10T19:30:00+01:00\",\"end\":\"2016-12-10T20:35:00+01:00\",\"resourceType\":\"Slot\"},{\"id\":\"d758d56a-62b4-470f-8b77-6859565907b3\",\"freeBusyType\":\"free\"," +
                    "\"start\":\"2016-12-10T19:31:00+01:00\",\"end\":\"2016-12-10T20:40:00+01:00\",\"resourceType\":\"Slot\"}]}");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        BusProvider.getInstance().post(new SelectBookingTimeResponse(response, mTransactionId, responseCallback, slot_or_schedule));
    }
}