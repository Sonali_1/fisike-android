package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import appointment.interfaces.ResponseCallback;
import appointment.network.BaseResponse;

import org.json.JSONArray;

/**
 * Created by Aastha on 04/03/2016.
 */
public class SearchTestResponse extends BaseResponse {

    private JSONArray searchresponse;
    private String message;


    public SearchTestResponse(JSONArray response, String mSearchString, long transactionId) {
        super(response, transactionId);
        try {
            this.searchresponse = response;
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    public SearchTestResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        message = getMessage();
    }

    public JSONArray getSearchresponse() {
        return searchresponse;
    }

    public void setSearchresponse(JSONArray searchresponse) {
        this.searchresponse = searchresponse;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
