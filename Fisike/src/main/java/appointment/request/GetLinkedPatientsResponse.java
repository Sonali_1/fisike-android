package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import appointment.model.LinkedPatientInfoMO;
import appointment.network.BaseResponse;

/**
 * Created by laxmansingh on 9/23/2017.
 */

public class GetLinkedPatientsResponse extends BaseResponse {

    public JSONObject response;

    ArrayList<LinkedPatientInfoMO> patientInfoMOArrayList = new ArrayList<>();


    public GetLinkedPatientsResponse(JSONArray response, long mTransactionId) {

        super(response, mTransactionId);
        try {
            parseResponse(response);
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    private void parseResponse(JSONArray response) throws JSONException {
        JSONArray jsonArray = new JSONArray(response.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            String email = "", mobileno = "";
            try {
                JSONObject dependentPatient = jsonArray.getJSONObject(i).getJSONObject("dependentPatient");
                String birthdate = dependentPatient.optJSONObject("birthDate").getString("value");
                String name = dependentPatient.optJSONArray("name").getJSONObject(0).getString("text");

                for (int j = 0; j < dependentPatient.optJSONArray("telecom").length(); j++) {
                    JSONObject jo = dependentPatient.optJSONArray("telecom").optJSONObject(j);
                    if (jo.optString("system").equals("email")) {
                        email = jo.optString("value");
                    }
                    if (jo.optString("system").equals("phone")) {
                        mobileno = jo.optString("value");
                    }
                }

                String gender = dependentPatient.optString("gender");
                int id = dependentPatient.optInt("id");
                LinkedPatientInfoMO patientInfoMO = new LinkedPatientInfoMO(birthdate, name, mobileno, email, gender, id);
                patientInfoMOArrayList.add(i, patientInfoMO);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public GetLinkedPatientsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        ;

    }

    public ArrayList<LinkedPatientInfoMO> getPatientInfoMOArrayList() {
        return patientInfoMOArrayList;
    }
}