package appointment.request;

import com.mphrx.fisike.gson.request.Address;

import java.util.ArrayList;

/**
 * Created by neharathore on 12/07/17.
 */

public class SaveAddressRequestJson {

    ArrayList<Address> address;
    long patientId;

    public ArrayList<Address> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<Address> address) {
        this.address = address;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }
}
