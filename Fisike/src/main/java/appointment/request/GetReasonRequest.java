package appointment.request;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 11/21/2016.
 */
public class GetReasonRequest extends BaseObjectRequest {

    private long mTransactionId;
    private String reasonSource;

    public  GetReasonRequest(long mTransactionId,String reasonSource){
        this.mTransactionId=mTransactionId;
        this.reasonSource=reasonSource;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getReasonListUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
            BusProvider.getInstance().post(new SearchReasonResponse(error, mTransactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SearchReasonResponse(response,mTransactionId));
    }

    public String getPayload() {
        JSONObject payLoad = new JSONObject();
        JSONObject constraint = new JSONObject();
        try {
            constraint.put("_query", "{'active':true,'type' :'" + reasonSource + "'}");
            constraint.put("count", 100);
            payLoad.put("constraints",constraint);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
