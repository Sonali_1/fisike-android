package appointment.request;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.mo.DiseaseMO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class AppointmentSearchResponseCommon {
    @Expose
    private java.util.List<?> list = new ArrayList<>();
    @Expose
    private Integer totalCount;

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
