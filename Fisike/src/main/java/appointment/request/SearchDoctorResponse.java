package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import org.json.JSONArray;

import appointment.interfaces.ResponseCallback;
import appointment.network.BaseResponse;

/**
 * Created by Aastha on 16/03/2016.
 */
public class SearchDoctorResponse extends BaseResponse {
    private JSONArray response;
    ResponseCallback responseCallback;
    public SearchDoctorResponse(JSONArray response, String mSearchString, long mTransactionId, ResponseCallback responseCallback) {
        super(response, mTransactionId);
        try {
            this.response = response;
            this.responseCallback = responseCallback;
            responseCallback.passResponse(response.toString(), mIsSuccessful);
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    public SearchDoctorResponse(Exception exception, long transactionId, ResponseCallback responseCallback) {
        super(exception, transactionId);
        responseCallback.passResponse(getMessage(), mIsSuccessful);
    }


}
