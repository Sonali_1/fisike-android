package appointment.request;

import org.json.JSONArray;

import appointment.interfaces.ResponseCallback;
import appointment.network.BaseResponse;

/**
 * Created by laxmansingh on 3/4/2016.
 */
public class PopularTestsResponse extends BaseResponse {

    public JSONArray response;
    ResponseCallback responseCallback;


    public PopularTestsResponse(JSONArray response, long transactionId, ResponseCallback responseCallback) {
        super(response, transactionId);
        try {
            this.responseCallback = responseCallback;
            this.response = response;
            mIsSuccessful = true;
            this.responseCallback.passResponse(response.toString(), mIsSuccessful);
        } catch (Exception e) {
            mIsSuccessful = false;

            this.responseCallback.passResponse(getMessage(), mIsSuccessful);
        }
    }

    public PopularTestsResponse(Exception exception, long transactionId, ResponseCallback responseCallback) {
        super(exception, transactionId);
        this.responseCallback = responseCallback;

        this.responseCallback.passResponse(getMessage(), mIsSuccessful);

    }

}
