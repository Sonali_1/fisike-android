package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;

/**
 * Created by Aastha on 21/03/2016.
 */
public class AppointmentShowResponse extends BaseResponse {
    private JSONObject response;
    ResponseCallback responseCallback;
    public AppointmentShowResponse(JSONObject response, long mTransactionId, ResponseCallback responseCallback) {
        super(response, mTransactionId);
        try {
            this.response = response;
            this.responseCallback = responseCallback;
            responseCallback.passResponse(response.toString(), mIsSuccessful);
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
            responseCallback.passResponse(mMessage, mIsSuccessful);
        }
    }

    public AppointmentShowResponse(Exception exception, long transactionId, ResponseCallback responseCallback) {
        super(exception, transactionId);
        responseCallback.passResponse(getMessage(), mIsSuccessful);
    }

}
