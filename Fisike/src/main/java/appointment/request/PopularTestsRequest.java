package appointment.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import appointment.interfaces.ResponseCallback;
import appointment.network.APIObjectRequest;
import appointment.network.BaseArrayRequest;

/**
 * Created by laxmansingh on 3/4/2016.
 */
public class PopularTestsRequest extends BaseArrayRequest {

    private long mTransactionId;
    ResponseCallback responseCallback;

    public PopularTestsRequest(long transactionId,ResponseCallback responseCallback) {
        mTransactionId = transactionId;
        this.responseCallback=responseCallback;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchPopularTestsUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url,null, this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayload() {
        JSONObject payLoad = new JSONObject();
        try {
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new PopularTestsResponse(error, mTransactionId,responseCallback));
    }

    @Override
    public void onResponse(JSONArray response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new PopularTestsResponse(response, mTransactionId,responseCallback));

    }
}
