package appointment.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

import appointment.interfaces.AppointmentResponseCallback;

/**
 * Created by laxmansingh on 4/4/2016.
 */
public class SelectBookingTimeResponse extends BaseResponse {

    public JSONObject response;
    AppointmentResponseCallback responseCallback;
    private String slot_or_schedule;


    public SelectBookingTimeResponse(JSONObject response, long transactionId, AppointmentResponseCallback responseCallback, String slot_or_schedule) {
        super(response, transactionId);
        try {
            this.responseCallback = responseCallback;
            this.response = response;
            this.slot_or_schedule = slot_or_schedule;
            mIsSuccessful = true;
            this.responseCallback.passResponse(response.toString(), slot_or_schedule);
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    public SelectBookingTimeResponse(Exception exception, long transactionId, AppointmentResponseCallback responseCallback, String slot_or_schedule) {
        super(exception, transactionId);
        this.responseCallback = responseCallback;
        this.slot_or_schedule = slot_or_schedule;
        this.responseCallback.passResponse(getMessage(), slot_or_schedule);
    }

}
