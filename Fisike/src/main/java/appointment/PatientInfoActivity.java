package appointment;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.fragment.PatientDetails;
import careplan.activity.CareTaskAppointment;

/**
 * Created by aastha on 11/30/2016.
 */
public class PatientInfoActivity extends BaseActivity {

    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private TextView toolbar_title;
    private ImageButton buttonBack;
    private IconTextView buttonright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_patient_info, frameLayout);
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            toolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.txt_static_patient_details));
        buttonBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        buttonright = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        buttonBack.setVisibility(View.GONE);
        buttonright.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        showHideToolbar(false);
        Bundle bundle = getIntent().getExtras();
        PatientDetails patientDetails = new PatientDetails();
        patientDetails.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentPatientInfo, patientDetails, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
