package appointment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.utils.CustomTypefaceSpan;

/**
 * Created by laxmansingh on 2/25/2016.
 */
public class ViewInstructionsActivity extends BaseActivity implements View.OnClickListener {

    Context mContext;
    protected TextView tv_name, tv_desc, tv_test_instruction;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    Typeface font_bold, font_semibold;
    SpannableString desc_styledString, instruction_styledString;
    private FrameLayout frameLayout;


    public static void newInstance(Context context) {
        Intent intent = new Intent(context, ViewInstructionsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.viewinstructions_activity);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.viewinstructions_activity, frameLayout);
        mContext = this;
        font_bold = Typeface.createFromAsset(mContext.getAssets(), getResources().getString(R.string.opensans_ttf_bold));
        font_semibold = Typeface.createFromAsset(mContext.getAssets(), getResources().getString(R.string.opensans_ttf_regular));

        findView();
        initView();
        bindView();
        setData();
    }

    private void setData() {
        String instructions = getIntent().getStringExtra("instructions");
        String name = getIntent().getStringExtra("title");

    /*    desc_styledString = new SpannableString("Test description " + "CPK-MB Mass is a cardiac marker which assists in the diagnosis of Acute Myocardial Infarction (AMI).This is a very sensitive marker less prone to artifactual elevations as compared to CPK-MB activity.Increase in serum levels occurs between 3-5 hours after the onset of infarction, peaks at 16-20 hours and returns to normal by 48-72 hours.");
        desc_styledString.setSpan(new CustomTypefaceSpan("", font_bold), 0, 12, 0);
        desc_styledString.setSpan(new CustomTypefaceSpan("", font_semibold), 13, desc_styledString.length(), 0);
        tv_desc.setText(desc_styledString);
*/

        instruction_styledString = new SpannableString(getResources().getString(R.string.Instructions) + Html.fromHtml(instructions));
        instruction_styledString.setSpan(new CustomTypefaceSpan("", font_bold), 0, 13, 0);
        instruction_styledString.setSpan(new CustomTypefaceSpan("", font_semibold), 14, instruction_styledString.length(), 0);
        tv_test_instruction.setText(Html.fromHtml("<font size=14 color=#445a8d>" + "<b>"
                + getResources().getString(R.string.Instructions) + "</b></font>" + instructions.replace("\n","<br />")));
        tv_test_instruction. setMovementMethod(LinkMovementMethod.getInstance());
        tv_name.setText(name);

    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_desc = (TextView) findViewById(R.id.tv_desc);
        tv_test_instruction = (TextView) findViewById(R.id.tv_test_instruction);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_instructions));
    }

    private void bindView() {
    }

    @Override
    public void onClick(View v) {
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
