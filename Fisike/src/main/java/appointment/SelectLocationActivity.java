package appointment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.views.RTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appointment.adapter.LocationAdapter;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.TestForAppointment;
import appointment.model.locationmodel.LocationModel;
import appointment.utils.keyboardvisibility.KeyboardVisibilityEvent;
import appointment.utils.keyboardvisibility.KeyboardVisibilityEventListener;

/**
 * Created by Aastha on 30/03/2016.
 */

public class SelectLocationActivity extends BaseActivity implements Animation.AnimationListener, View.OnClickListener {

    private TextView doctorName, doctorspeciality, doctorFee, tvAllTestNames, tvSpeciality;
    IconTextView img_delete, img_labtest_delete, img_speciality_delete;
    private CustomFontTextView tvTotalTest, tvTotalPrice;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private ProgressDialog pdialog;
    private LocationAdapter locationAdapter;
    private RecyclerView rv_doctor_locations;
    private int registrationId, hcsId;
    private RTextView listEmptyError;
    private ClearableEditText etsearch;
    private String from;
    private FrameLayout frame_container;
    private RelativeLayout rl_error_view;
    private RelativeLayout rlDoctorInfo, rlSpecialityInfo;
    private LinearLayout rlTestInfo;
    private List<appointment.model.locationmodel.List> locationList = new ArrayList<appointment.model.locationmodel.List>();
    private List<String> testNames = new ArrayList<String>();
    public static SelectLocationActivity act;
    private boolean REBOOK = false;


    // Animation
    Animation animSlideUp;
    Animation animSideDown;
    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_select_location);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_select_location, frameLayout);
        act = this;
        findView();
        initView();
        bindView();


        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                //    updateKeyboardStatusText(isOpen);
            }
        });

        updateKeyboardStatusText(KeyboardVisibilityEvent.isKeyboardVisible(this));

    }

    private void updateKeyboardStatusText(boolean isOpen) {
        if (isOpen) {
            slideToTop(frame_container);
        } else {
            slideToBottom(frame_container);
        }
    }

    // To animate view slide out from top to bottom
    public void slideToBottom(FrameLayout view) {
        view.setVisibility(View.VISIBLE);
        view.startAnimation(animSideDown);
    }

    // To animate view slide out from bottom to top
    public void slideToTop(FrameLayout view) {
        view.setVisibility(View.GONE);
        view.startAnimation(animSlideUp);
    }


    private void bindView() {
        // load the animation
        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        // set animation listener
        animSlideUp.setAnimationListener(this);

        // load the animation
        animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        // set animation listener
        animSideDown.setAnimationListener(this);

        etsearch.addTextChangedListener(null);
        etsearch.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
// TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                locationAdapter.getFilter().filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void initView() {

        img_delete.setOnClickListener(this);
        img_labtest_delete.setOnClickListener(this);
        img_speciality_delete.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.select_location));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
      /*  mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeDrawStatusActivity.this.finish();
            }
        });*/


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            registrationId = bundle.getInt(TextConstants.KEY, 0);
            from = bundle.getString(TextConstants.TEST_TYPE);
            REBOOK = bundle.getBoolean(TextConstants.REBOOK, false);
            new AsyncTaskForHCSID(registrationId, from).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        if (from.equals(TextConstants.DOCTORS)) {
            doctorName.setText(OrderSummary.getInstance().getDoctorName());
            doctorFee.setText(OrderSummary.getInstance().getDoctorFee() + "");
            doctorspeciality.setText(OrderSummary.getInstance().getDoctorSpeciality());
            doctorspeciality.setVisibility(View.VISIBLE);
            OrderSummary.getInstance().setDoctorLocationAddress("");
            OrderSummary.getInstance().setDoctorContact("");
        } else if (from.equals(TextConstants.LAB_TEST)) {
            doctorspeciality.setVisibility(View.GONE);
            initLabView();
        } else if (from.equals(TextConstants.SPECIALITY)) {
            doctorspeciality.setVisibility(View.GONE);
            initSpecialityView();
        }
        //   rv_doctor_locations.setNestedScrollingEnabled(false);
        rv_doctor_locations.setNestedScrollingEnabled(false);
        locationAdapter = new LocationAdapter(this, locationList, from, rv_doctor_locations, rl_error_view, listEmptyError);
        rv_doctor_locations.setLayoutManager(new LinearLayoutManager(this));
        rv_doctor_locations.setAdapter(locationAdapter);

    }

    private void initSpecialityView() {
        rlDoctorInfo.setVisibility(View.GONE);
        rlTestInfo.setVisibility(View.GONE);
        rlSpecialityInfo.setVisibility(View.VISIBLE);
        tvSpeciality.setText(OrderSummary.getInstance().getDoctorSpeciality().toString());

    }

    private void initLabView() {
        rlDoctorInfo.setVisibility(View.GONE);
        rlSpecialityInfo.setVisibility(View.GONE);
        rlTestInfo.setVisibility(View.VISIBLE);
        testNames = new ArrayList<String>();
        testNames.clear();
        tvTotalTest.setText(OrderSummary.getInstance().getTestNumber() + "");
        tvTotalPrice.setText(OrderSummary.getInstance().getTotalPrice() + "");
        List<TestForAppointment> testMOList = OrderSummary.getInstance().getOrderist();
        for (int i = 0; i < testMOList.size(); i++)
            testNames.add(testMOList.get(i).getTestName());

        String name = "";
        int size = testNames.size();
        /*if (size > 0) {*/
        for (int i = 0; i < size; i++) {
            if (i != size - 1) {
                name += testNames.get(i) + "/";
            } else
                name += testNames.get(i);
        }
        tvAllTestNames.setText(name);

    }

    private void findView() {
        frame_container = (FrameLayout) frameLayout.findViewById(R.id.frame_container);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rlDoctorInfo = (RelativeLayout) findViewById(R.id.doc_info_layout);
        rlTestInfo = (LinearLayout) findViewById(R.id.rel_lyt_total_test);
        rlSpecialityInfo = (RelativeLayout) findViewById(R.id.rel_lyt_speciality);
        doctorName = (TextView) findViewById(R.id.tvdoctor_name);

        img_delete = (IconTextView) findViewById(R.id.img_delete);
        img_labtest_delete = (IconTextView) findViewById(R.id.img_labtest_delete);
        img_speciality_delete = (IconTextView) findViewById(R.id.img_speciality_delete);

        doctorspeciality = (TextView) findViewById(R.id.tvdoctor_speciality);
        doctorFee = (TextView) findViewById(R.id.doc_fee);
        rl_error_view = (RelativeLayout) findViewById(R.id.rl_error_view);
        listEmptyError = (RTextView) findViewById(R.id.tv_error_view);
        rv_doctor_locations = (RecyclerView) findViewById(R.id.rv_doctor_locations);
        tvTotalTest = (CustomFontTextView) findViewById(R.id.tv_test_count);
        tvTotalPrice = (CustomFontTextView) findViewById(R.id.tv_price);
        tvAllTestNames = (TextView) findViewById(R.id.tv_test_name);
        tvSpeciality = (TextView) findViewById(R.id.tv_selected_speciality);

        etsearch = (ClearableEditText) findViewById(R.id.etsearch);
        etsearch.setIsAddTxtVisible(false);
        etsearch.setHint(getResources().getString(R.string.filter_location));
        //     etsearch.getEditText().setPadding(0, 10, 10, 22);
        etsearch.setSingleLine();
        etsearch.getclearTextButton().setVisibility(View.GONE);
        etsearch.setIsCrossVisible(false);
        etsearch.setMaxLength(50);
        etsearch.setHintColor(R.color.dusky_blue_70);
        etsearch.setTextColor(R.color.dusky_blue);
        etsearch.setTextSize(14);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_delete:
                onBackPressed();
                break;

            case R.id.img_labtest_delete:
                onBackPressed();
                break;

            case R.id.img_speciality_delete:
                onBackPressed();
                break;
        }
    }

    private class AsyncTaskForHCSID extends AsyncTask<Void, Void, String> {

        int key;
        String source;

        public AsyncTaskForHCSID(int key, String from) {
            this.key = key;
            source = from;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!Utils.showDialogForNoNetwork(SelectLocationActivity.this, false)) {
                return;
            } else {
                pdialog = new ProgressDialog(SelectLocationActivity.this);
                pdialog.setMessage(getResources().getString(R.string.Fetching_Locations));
                pdialog.setCanceledOnTouchOutside(false);
                pdialog.show();
            }
            //Toast.makeText(SelectLocationActivity.this, "Fetching HCS-ID...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            String jsonString = "";
            String arr = "";
            try {
                JSONObject jsonObject = new JSONObject();
                //practitioner
                if (source.equals(TextConstants.DOCTORS))
                    arr = "practitioner.practitionerId|" + key;
                else if (source.equals(TextConstants.LAB_TEST))
                    arr = "test.testCompendiumId|" + key;

                JSONArray ext = new JSONArray();
                ext.put(arr);

                if (source.equals(TextConstants.DOCTORS)) {
                    jsonObject.put("extension:url", ext);
                    jsonObject.put("serviceCategory", "practitioner");
                } else if (source.equals(TextConstants.LAB_TEST)) {
                    jsonObject.put("extension:url", ext);
                    jsonObject.put("serviceCategory", "test");
                } else if (source.equals(TextConstants.SPECIALITY)) {
                    jsonObject.put("serviceType", key);
                    jsonObject.put("serviceCategory", "specialty");
                }
                jsonObject.put("_include", "healthcareService%3Alocation");
                jsonObject.put("_count", 10);
                jsonObject.put("_skip", 0);
                JSONObject constraintsObject = new JSONObject();
                constraintsObject.put("constraints", jsonObject);
                constraintsObject.put("totalCount", 0);
                jsonString = APIManager.getInstance().executeHttpPost(MphRxUrl.getHCSSearchUrl(), "", constraintsObject, SelectLocationActivity.this);
                //Log.i("constin", constraintsObject.toString());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonString;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            try {
                if (pdialog.isShowing()) {
                    pdialog.dismiss();
                    pdialog = null;
                }
                if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                        || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                        || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                        || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {
                    showError(response);
                    rl_error_view.setVisibility(View.VISIBLE);
                } else {
                    Gson gson = new Gson();
                    LocationModel locationModel = new LocationModel();
                    try {
                        locationModel = gson.fromJson(response.toString(), LocationModel.class);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    locationList.clear();
                    for (int i = 0; i < locationModel.getList().size(); i++) {
                        if (locationModel.getList().get(i).getLocation() != null) {
                            locationList.add(locationModel.getList().get(i));
                        }
                    }
                    if (locationList.size() == 0) {
                        showError(getResources().getString(R.string.No_location_found));
                        rl_error_view.setVisibility(View.VISIBLE);
                    }   //Log.i("contiis",locationList.size()+"**"+locationModel.getList().size()+"//"+locationModel.getTotalCount());
                    else {
                        rl_error_view.setVisibility(View.GONE);
                        locationAdapter.notifyDataSetChanged();
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void showError(String response) {
        rv_doctor_locations.setVisibility(View.GONE);
        listEmptyError.setVisibility(View.VISIBLE);
        listEmptyError.setText(response);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // OrderSummary.setInstance();
    }
}
