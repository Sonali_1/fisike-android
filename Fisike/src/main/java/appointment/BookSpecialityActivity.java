package appointment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.fragment.SelectSpecialityFragment;
import careplan.activity.CareTaskAppointment;

/**
 * Created by Aastha on 11/03/2016.
 */
public class BookSpecialityActivity extends BaseActivity {

    Toolbar toolbar;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        frameLayout= (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_book_speciality, frameLayout);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentSelectSpeciality, new SelectSpecialityFragment(), "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
