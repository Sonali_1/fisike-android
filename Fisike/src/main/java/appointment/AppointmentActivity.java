package appointment;

import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.phlooba.fragment.PhloobaHomeFragment;

/**
 * Created by mphrx dec13 on 19-May-16.
 */
public class AppointmentActivity extends BaseActivity implements View.OnClickListener {

    Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    FrameLayout frameLayout;

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        showHideToolbar(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        showHideToolbar(true);
        getLayoutInflater().inflate(R.layout.homesamplecollectionactivity, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setTitle(getString(R.string.home_appointment));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
  /*  mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });
      */
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setOnClickListener(this);
        btnBack.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.hamburger));
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.home_appointment));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setOnClickListener(this);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setVisibility(View.VISIBLE);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        bt_toolbar_cross = (ImageButton) findViewById(R.id.bt_toolbar_cross);
//        bt_toolbar_cross.setOnClickListener(new View.OnClickListener() {
          /*  @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        PhloobaHomeFragment fragment = new PhloobaHomeFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentParentView, fragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentItem(NavigationDrawerActivity.APPOINTMENT);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_cross:
                openDrawer();
                //finish();
                break;


            case R.id.bt_toolbar_right:
                MyOrdersActivity.newInstance(this);
                break;

        }
    }
}
