package appointment.interfaces;

/**
 * Created by laxmansingh on 3/8/2016.
 */
public interface ResponseCallback {
    public void passResponse(String response, boolean mIsSuccessful);
}
