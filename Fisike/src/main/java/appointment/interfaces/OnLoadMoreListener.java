package appointment.interfaces;

/**
 * Created by laxmansingh on 18/03/2016.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
