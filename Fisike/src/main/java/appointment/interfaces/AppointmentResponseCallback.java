package appointment.interfaces;

/**
 * Created by laxmansingh on 3/15/2016.
 */
public interface AppointmentResponseCallback {
    public void passResponse(String response, String recent_or_upcoming);
    public void cancelOrReschedule(appointment.model.appointmodel.List appointmentmodel, String position, String cancel_or_reschedule);
}
