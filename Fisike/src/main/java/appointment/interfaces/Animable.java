package appointment.interfaces;

/**
 * Created by laxmansingh on 3/3/2016.
 */
public interface Animable {
    public void animate(String what);
}
