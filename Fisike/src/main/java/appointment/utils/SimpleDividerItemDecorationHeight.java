package appointment.utils;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;


/**
 * Created by Singh Laxman on 30/10/15.
 */
public class SimpleDividerItemDecorationHeight extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    public SimpleDividerItemDecorationHeight(Resources resources) {
        mDivider = resources.getDrawable(R.drawable.divider_menu_height);
    }

    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}