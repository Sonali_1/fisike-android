package appointment.utils;

import com.mphrx.fisike.utils.DateTimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by laxmansingh on 3/31/2016.
 */
public class AppointmentComparator<T> implements Comparator<T> {


    @Override
    public int compare(T object1, T object2) {
        SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd_hh_mm_ss_a, Locale.US);
        Date date1 = null, date2 = null;
        try {
            if (object1 instanceof appointment.model.appointmodel.List && object2 instanceof appointment.model.appointmodel.List) {
                date1 = ft.parse(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.YYYY_MM_dd_hh_mm_ss_a, ((appointment.model.appointmodel.List) object1).getStart().getValue().toString().trim()));
                date2 = ft.parse(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.YYYY_MM_dd_hh_mm_ss_a, ((appointment.model.appointmodel.List) object2).getStart().getValue().toString().trim()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1.compareTo(date2);
    }
}
