package appointment.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.toolbox.ImageLoader;
import com.mphrx.fisike.R;

/**
 * Created by laxmansingh on 6/21/2016.
 */
public class CommonControls {

    public static void closeKeyBoard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) context).getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setProfilePic(Context mContext, CircularImageView imageView, String imgURL) {
        if (!CommonTasks.isTextEmptyOrNull(imgURL)) {
            ImageLoader imageLoader = CustomVolleyRequest.getInstance(mContext).getImageLoader();
            imageLoader.get(imgURL.toString().trim(), ImageLoader.getImageListener(imageView, R.drawable.user_default, R.drawable.user_default));
            imageView.setImageUrl(imgURL.toString().trim(), imageLoader);
        } else {
            imageView.setDefaultImageResId(R.drawable.user_default);
        }
    }


}
