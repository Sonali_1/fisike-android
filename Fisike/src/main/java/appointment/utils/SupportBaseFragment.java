package appointment.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.HeaderActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.squareup.otto.Bus;

/**
 * Created by laxmansingh on 3/7/2016.
 */
public abstract class SupportBaseFragment extends LogFragment {

    public static final int PERMISSION_REQUEST_CODE = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(getLayoutId(), null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findView();
        initView();
        bindView();
    }

    protected abstract int getLayoutId();

    public abstract void findView();

    public abstract void initView();

    public abstract void bindView();


    public void openActivity(Class<? extends HeaderActivity> clazz, Bundle bundle, boolean finishThisActivity) {
        ((HeaderActivity) getActivity()).openActivity(clazz, bundle, finishThisActivity);
    }

    public void replaceFragment(SupportBaseFragment fragment, int containerId, boolean addToBackStack) {

        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
       FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(containerId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    public void showProgressDialog(String message) {
        if (getActivity() instanceof NetworkActivity)
            ((NetworkActivity) getActivity()).showProgressDialog(getString(R.string.Processing_loading));
    }

    public void dismissProgressDialog() {
        if (getActivity() instanceof NetworkActivity)
            ((NetworkActivity) getActivity()).dismissProgressDialog();
    }

    public void hideKeyboard() {
        ((HeaderActivity) getActivity()).hideKeyboard();
    }

    public boolean checkPermission(final String permissionType, String message) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED)
        {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), "Permission", message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions( new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionNotGranted(permissions[0]);
            }
        }
    }

    public void onPermissionGranted(String permission) {
        throw new RuntimeException("You must override this function if you use ask Runtime Permission.");
    }

    public void onPermissionNotGranted(String permission) {
        throw new RuntimeException("You must override this function if you use ask Runtime Permission.");
    }
}
