package appointment.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.mphrx.fisike_physician.utils.AppLog;

/**
 * Created by laxmansingh on 3/7/2016.
 */
public class LogFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLog.showInfo(getClass().getSimpleName(), "On Create");

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AppLog.showInfo(getClass().getSimpleName(), "On Activity Created");
    }

    @Override
    public void onStart() {
        super.onStart();
        AppLog.showInfo(getClass().getSimpleName(), "On Start");
    }

    @Override
    public void onResume() {
        super.onResume();
        AppLog.showInfo(getClass().getSimpleName(), "On Resume");
    }

    @Override
    public void onPause() {
        super.onPause();
        AppLog.showInfo(getClass().getSimpleName(), "On Pause");
    }

    @Override
    public void onStop() {
        super.onStop();
        AppLog.showInfo(getClass().getSimpleName(), "On Stop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppLog.showInfo(getClass().getSimpleName(), "On Destroy View");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppLog.showInfo(getClass().getSimpleName(), "On Destroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppLog.showInfo(getClass().getSimpleName(), "On Detach");
    }


}
