package appointment.utils;

/**
 * Created by Aastha on 26/02/2016.
 */
public interface OnListItemSelected {

    void handleSelection(String from, boolean selected);
}
