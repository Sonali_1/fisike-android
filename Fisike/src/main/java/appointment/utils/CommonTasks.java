package appointment.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.LinearLayout;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import appointment.model.appointmentcommonmodel.Roles;
import appointment.model.datetimeslotmodel.DateModel;
import appointment.model.datetimeslotmodel.TimeModel;
import appointment.model.slotmodel.SlotDataModel;

/**
 * Created by laxmansingh on 3/9/2016.
 */
public class CommonTasks {


    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDate;
    }


    public static String formateDateeFromstring(String inputFormat, String outputFormat, String inputDate) {
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.US);
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDate;
    }


    public static String formatDateFromstring(String inputFormat, String outputFormat, String inputDate) {
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.US);
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.US);

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDate;
    }


    public static boolean isTextEmptyOrNull(String str) {
        if (str == null || str.equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || str.length() == 0)
            return true;
        else
            return false;
    }

    public static long findDateDifferenceinMins(String appointment_date) {


        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.hh_mm_aaa_comma_dd_MMM_yyyy, Locale.US);


        String strdate = ft.format(date);
        Date newdate = null, currentdate = null;
        try {
            newdate = ft.parse(appointment_date);
            currentdate = ft.parse(strdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = newdate.getTime() - currentdate.getTime();
        return TimeUnit.MILLISECONDS.toMinutes(diff);
    }


    public boolean checkBookableOrRequestable(Roles roles) {
        String role = "[ROLE_PATIENT]";/*SharedPref.getDefinedRoles().toString().trim();*/
        JSONArray array = null;
        List<String> roleList = new ArrayList<String>();
        try {
            array = new JSONArray(role);
            for (int i = 0; i < array.length(); i++) {
                roleList.add(array.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return !Collections.disjoint(roleList, roles.getBookableRoles());
    }






    public long findDateDifferenceinDays(String appointment_date, String dateTimeFormat) {

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat(dateTimeFormat, Locale.US);

        String strdate = ft.format(date);
        Date newdate = null, currentdate = null;
        try {
            newdate = ft.parse(appointment_date);
            currentdate = ft.parse(strdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = newdate.getTime() - currentdate.getTime();

        return TimeUnit.MILLISECONDS.toDays(diff);
    }


    public long findDateDifferenceinMins(String startDate, String endDate, String dateTimeFormat) {
        SimpleDateFormat ft = new SimpleDateFormat(dateTimeFormat, Locale.getDefault());
        Date newdate = null, currentdate = null;
        try {
            newdate = ft.parse(endDate);
            currentdate = ft.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = newdate.getTime() - currentdate.getTime();

        return TimeUnit.MILLISECONDS.toMinutes(diff);
    }


    public long findDateDifferenceinDaysBetweenTwoDates(String firstDate, String secondDate, String dateTimeFormat) {

        SimpleDateFormat ft = new SimpleDateFormat(dateTimeFormat, Locale.getDefault());

        Date firstDatee = null, secondDatee = null;
        try {
            firstDatee = ft.parse(firstDate);
            secondDatee = ft.parse(secondDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = firstDatee.getTime() - secondDatee.getTime();

        return TimeUnit.MILLISECONDS.toDays(diff);
    }

    public long findDateDifferenceinDaysBetweenTwoDate(String firstDate, String secondDate, String dateTimeFormat) {

        SimpleDateFormat ft = new SimpleDateFormat(dateTimeFormat, Locale.US);

        Date firstDatee = null, secondDatee = null;
        try {
            firstDatee = ft.parse(firstDate);
            secondDatee = ft.parse(secondDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = firstDatee.getTime() - secondDatee.getTime();

        return TimeUnit.MILLISECONDS.toDays(diff);
    }


    public void parseDateTimeModel(List<DateModel> list, SlotDataModel slotDataModel) {

        Collections.sort(slotDataModel.getList(), new Comparator<appointment.model.slotmodel.List>() {
            @Override
            public int compare(appointment.model.slotmodel.List o1, appointment.model.slotmodel.List o2) {
                try {
                    if ((o1.getStart().equals("") || o1.getStart().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o1.getStart() == null) && (o2.getStart().equals("") || (o2.getStart().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o2.getStart() == null)))
                        return 0;

                    if ((o1.getStart().equals("") || o1.getStart().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o1.getStart() == null))
                        return 1;

                    if ((o2.getStart().equals("") || o2.getStart().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o2.getStart() == null))
                        return -1;

                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, Locale.US);
                    Date date1 = null, date2 = null;
                    try {
                        date1 = ft.parse(o1.getStart().toString().trim());
                        date2 = ft.parse(o2.getStart().toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return date1.compareTo(date2);
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });


        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.getDefault());

        String formattedDate1 = null, formattedDate2 = null, formattedDate3 = null;

        for (int i = 0; i < slotDataModel.getList().size(); i++) {
            String current_date = null;
            try {

                if ((slotDataModel.getList().get(i).getStart().toString().trim().contains("z") || slotDataModel.getList().get(i).getStart().toString().trim().contains("Z"))) {
                    Date ds = fromIsoUTCString(slotDataModel.getList().get(i).getStart().toString().trim(), null);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    current_date = simpleDateFormat.format(ds);
                } else {
                    current_date = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.dd_MMM_yyyy, slotDataModel.getList().get(i).getStart().toString().trim());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (formattedDate1 == null && formattedDate2 == null && formattedDate3 == null) {
                formattedDate1 = current_date;
                DateModel dateModel_0 = new DateModel();
                dateModel_0.setDatre(formattedDate1);
                dateModel_0.setTimeModelList(new ArrayList<TimeModel>());
                list.add(dateModel_0);
            } else if (!current_date.equals(formattedDate1) && formattedDate1 != null && formattedDate2 == null) {
                formattedDate2 = current_date;
                DateModel dateModel_1 = new DateModel();
                dateModel_1.setDatre(formattedDate2);
                dateModel_1.setTimeModelList(new ArrayList<TimeModel>());
                list.add(dateModel_1);
            } else if (!current_date.equals(formattedDate1) && !current_date.equals(formattedDate2) && formattedDate1 != null && formattedDate2 != null && formattedDate3 == null) {
                formattedDate3 = current_date;
                DateModel dateModel_2 = new DateModel();
                dateModel_2.setDatre(formattedDate3);
                dateModel_2.setTimeModelList(new ArrayList<TimeModel>());
                list.add(dateModel_2);
            }


            if (current_date.equals(formattedDate1)) {
                TimeModel timeModel = new TimeModel();
                timeModel.setStartdate(slotDataModel.getList().get(i).getStart().toString().trim());
                timeModel.setEnddate(slotDataModel.getList().get(i).getEnd().toString().trim());
                list.get(0).getTimeModelList().add(timeModel);
            } else if (current_date.equals(formattedDate2)) {
                TimeModel timeModel = new TimeModel();
                timeModel.setStartdate(slotDataModel.getList().get(i).getStart().toString().trim());
                timeModel.setEnddate(slotDataModel.getList().get(i).getEnd().toString().trim());
                list.get(1).getTimeModelList().add(timeModel);
            } else if (current_date.equals(formattedDate3)) {
                TimeModel timeModel = new TimeModel();
                timeModel.setStartdate(slotDataModel.getList().get(i).getStart().toString().trim());
                timeModel.setEnddate(slotDataModel.getList().get(i).getEnd().toString().trim());
                list.get(2).getTimeModelList().add(timeModel);
            }
        }


        formattedDate1 = null;
        formattedDate2 = null;
        formattedDate3 = null;

        Collections.sort(list, new Comparator<DateModel>() {
            @Override
            public int compare(DateModel o1, DateModel o2) {
                try {
                    if ((o1.getDatre().equals("") || o1.getDatre().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o1.getDatre() == null) && (o2.getDatre().equals("") || (o2.getDatre().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o2.getDatre() == null)))
                        return 0;

                    if ((o1.getDatre().equals("") || o1.getDatre().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o1.getDatre() == null))
                        return 1;

                    if ((o2.getDatre().equals("") || o2.getDatre().equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || o2.getDatre() == null))
                        return -1;

                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                    Date date1 = null, date2 = null;
                    try {
                        date1 = ft.parse(o1.getDatre().toString().trim());
                        date2 = ft.parse(o2.getDatre().toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return date1.compareTo(date2);
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });


        if (list.size() > 0 && list.size() < 3) {
            int diffInDays = (int) new CommonTasks().findDateDifferenceinDays(list.get(0).getDatre().toString().trim(), DateTimeUtil.dd_MMM_yyyy);
            int reminderOFDiffInDays = diffInDays % 3;
            if (reminderOFDiffInDays == 0) {
                if (list.size() == 1) {
                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                    Date date1 = null;
                    try {
                        date1 = ft.parse(list.get(0).getDatre().toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar c = Calendar.getInstance();
                    c.setTime(date1);


                    c.add(Calendar.DATE, 1);
                    DateModel dateModel = new DateModel();
                    dateModel.setDatre(ft.format(c.getTime()));
                    dateModel.setTimeModelList(new ArrayList<TimeModel>());
                    list.add(1, dateModel);


                    c.add(Calendar.DATE, 1);
                    DateModel dateModel_1 = new DateModel();
                    dateModel_1.setDatre(ft.format(c.getTime()));
                    dateModel_1.setTimeModelList(new ArrayList<TimeModel>());
                    list.add(2, dateModel_1);
                } else if (list.size() == 2) {
                    int diffInDayss = (int) new CommonTasks().findDateDifferenceinDaysBetweenTwoDate(list.get(1).getDatre().toString().trim(), list.get(0).getDatre().toString().trim(), DateTimeUtil.dd_MMM_yyyy);
                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                    Date date1 = null;

                    if (diffInDayss == 1) {
                        try {
                            date1 = ft.parse(list.get(1).getDatre().toString().trim());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar c = Calendar.getInstance();
                        c.setTime(date1);
                        c.add(Calendar.DATE, 1);
                        DateModel dateModel = new DateModel();
                        dateModel.setDatre(ft.format(c.getTime()));
                        dateModel.setTimeModelList(new ArrayList<TimeModel>());
                        list.add(2, dateModel);
                    } else if (diffInDayss == 2) {
                        try {
                            date1 = ft.parse(list.get(0).getDatre().toString().trim());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar c = Calendar.getInstance();
                        c.setTime(date1);
                        c.add(Calendar.DATE, 1);
                        DateModel dateModel = new DateModel();
                        dateModel.setDatre(ft.format(c.getTime()));
                        dateModel.setTimeModelList(new ArrayList<TimeModel>());
                        list.add(1, dateModel);
                    }
                }
            } else if (reminderOFDiffInDays == 1) {
                SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                Date date1 = null;
                try {
                    date1 = ft.parse(list.get(0).getDatre().toString().trim());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                c.setTime(date1);

                if (list.size() == 1) {
                    c.add(Calendar.DATE, 1);
                    DateModel dateModel_1 = new DateModel();
                    dateModel_1.setDatre(ft.format(c.getTime()));
                    dateModel_1.setTimeModelList(new ArrayList<TimeModel>());
                    list.add(1, dateModel_1);
                    c.add(Calendar.DATE, -1);
                }

                c.add(Calendar.DATE, -1);
                DateModel dateModel = new DateModel();
                dateModel.setDatre(ft.format(c.getTime()));
                dateModel.setTimeModelList(new ArrayList<TimeModel>());
                list.add(0, dateModel);


            } else if (reminderOFDiffInDays == 2) {
                SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy, Locale.US);
                Date date1 = null;
                try {
                    date1 = ft.parse(list.get(0).getDatre().toString().trim());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                c.setTime(date1);


                c.add(Calendar.DATE, -1);
                DateModel dateModel = new DateModel();
                dateModel.setDatre(ft.format(c.getTime()));
                dateModel.setTimeModelList(new ArrayList<TimeModel>());

                c.add(Calendar.DATE, -1);
                DateModel dateModel_1 = new DateModel();
                dateModel_1.setDatre(ft.format(c.getTime()));
                dateModel_1.setTimeModelList(new ArrayList<TimeModel>());

                list.add(0, dateModel_1);
                list.add(1, dateModel);
            }
        }
    }

    public static int getDensityPixel(Context context, int paddingPixel) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (paddingPixel * density);
    }


    public static LinearLayout getLine(Context mContext) {
        LinearLayout line1 = new LinearLayout(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) Utils.convertDpToPixel(2, mContext));
        params.setMargins(0, 10, 0, 0);
        line1.setLayoutParams(params);
        line1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.view_line_dotted));
        return line1;
    }


    @Nullable
    public static Date fromIsoUtcString(String isoUtcString, String dateFormat) {
        DateFormat isoUtcFormat;
        if (dateFormat == null || dateFormat.equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || dateFormat.equals("")) {
            isoUtcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        } else {
            isoUtcFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
        }
        isoUtcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return isoUtcFormat.parse(isoUtcString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date fromIsoUTCString(String isoUtcString, String dateFormat) {
        DateFormat isoUtcFormat;
        if (dateFormat == null || dateFormat.equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null)) || dateFormat.equals("")) {
            isoUtcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        } else {
            isoUtcFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
        }
        isoUtcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return isoUtcFormat.parse(isoUtcString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
