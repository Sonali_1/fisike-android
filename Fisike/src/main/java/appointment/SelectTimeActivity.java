package appointment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.CountryAdapter;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

import appointment.adapter.AddressAdapter;
import appointment.adapter.SelectedTestsAdapter;
import appointment.adapter.SpinAdapter;
import appointment.constants.AppointmentConstants;
import appointment.fragment.MyAppointmentFragment;
import appointment.interfaces.ResponseCallback;
import appointment.interfaces.UpdateUIData;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.TestForAppointment;
import appointment.model.renderconfigmodel.RenderConfigModel;
import appointment.request.PlaceTestOrderRequest;
import appointment.utils.CommonTasks;
import careplan.activity.CareTaskAppointment;

/**
 * Created by laxmansingh on 2/25/2016.
 */
public class SelectTimeActivity extends BaseActivity implements View.OnClickListener, ResponseCallback, UpdateUIData {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private Context mContext;
    private UpdateUIData updateUIData;
    ResponseCallback responseCallback;
    private NestedScrollView verticalScrollView;
    private TextView tvTotalTest, tvTotalPrice;
    private Button btSubmit, btPayNow;
    private RecyclerView rv_tests;
    private List<String> testNames;
    private View line_city;
    private RelativeLayout rlDateTimelayout, patientInformationLayout, dateInfoLayout;
    private RelativeLayout successlayout;
    private SelectedTestsAdapter testAdapter;
    private CustomFontEditTextView patientFristName, patientLastName, patientAddress, patientMobile, et_postal_code, et_email;
    private TextView date, time;
    private Spinner spDate, spTime;
    public String starttime, endtime, countryCode;
    private ProgressDialog pdialog;
    //Success screen
    private TextView tvTotalCost, tvDate, tvTime, tvAllTestNames, tv_success_status;
    private TextView tvPatientName, tvPatientAddress, tvPatientMobile;
    private Button btn_view_instruction, btn_view_all_order;
    private IconTextView iv_ic_menu_overflow;

    /*[  Adapter variables   ]*/
    SpinAdapter day_adapter;
    SpinAdapter time_adapter;
    List<String> day_list = new ArrayList<String>();
    List<String> time_list = new ArrayList<String>();
    private String position;
    /*[  Adapter variables   ]*/


    /*[..... new address fields ......... ]*/
    private Spinner spinner_city, spinner_state;
    private LinearLayout address_lyt;
    List<RenderConfigModel> list_states = new ArrayList<RenderConfigModel>();
    private AddressAdapter state_adapter, city_adapter;
    private CustomFontEditTextView et_state, et_city;
    private Spinner spinner_change_country_code;
    private int position_country_code = 0;
    private CustomFontEditTextView mCountryCodes, mMobileNumber;

    /*[..... new address fields ......... ]*/

    //TO BE REMOVED
    private int hcsId = 0;
    private FrameLayout frameLayout;

    public static void newInstance(Context context) {
        Intent intent = new Intent(context, SelectTimeActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.selecttime_activity, frameLayout);
        responseCallback = this;
        updateUIData = this;
        mContext = this;

        findView();
        try {
            initView();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        bindView();
    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        verticalScrollView = (NestedScrollView) findViewById(R.id.scroll_view_vertical);
        tvTotalTest = (TextView) findViewById(R.id.tv_test_count);
        tvTotalPrice = (TextView) findViewById(R.id.tv_price);
        rv_tests = (RecyclerView) findViewById(R.id.rv_tests);
        rlDateTimelayout = (RelativeLayout) findViewById(R.id.date_time_lyt);
        patientInformationLayout = (RelativeLayout) findViewById(R.id.patient_info_layout);
        dateInfoLayout = (RelativeLayout) findViewById(R.id.date_info_layout);
        successlayout = (RelativeLayout) findViewById(R.id.succcess_layout);
        btSubmit = (Button) rlDateTimelayout.findViewById(R.id.bt_next);
        date = (TextView) dateInfoLayout.findViewById(R.id.tv_date);
        iv_ic_menu_overflow = (IconTextView) dateInfoLayout.findViewById(R.id.iv_ic_menu_overflow);
        time = (TextView) dateInfoLayout.findViewById(R.id.tv_time);
        patientFristName = (CustomFontEditTextView) patientInformationLayout.findViewById(R.id.et_firstname);
        patientLastName = (CustomFontEditTextView) patientInformationLayout.findViewById(R.id.et_lastname);
        patientAddress = (CustomFontEditTextView) patientInformationLayout.findViewById(R.id.et_address);
        spinner_city = (Spinner) patientInformationLayout.findViewById(R.id.spinner_city);
        spinner_state = (Spinner) patientInformationLayout.findViewById(R.id.spinner_state);
        et_postal_code = (CustomFontEditTextView) patientInformationLayout.findViewById(R.id.et_postal_code);
        address_lyt = (LinearLayout) patientInformationLayout.findViewById(R.id.address_lyt);
        line_city = (View) patientInformationLayout.findViewById(R.id.line_city);
        et_email = (CustomFontEditTextView) patientInformationLayout.findViewById(R.id.et_email);


        mCountryCodes = (CustomFontEditTextView) findViewById(R.id.country_code);
        mMobileNumber = (CustomFontEditTextView) findViewById(R.id.et_mobile_number);
        spinner_change_country_code = (Spinner) findViewById(R.id.spinner_change_country_code);
        mCountryCodes.setFocusable(false);


        btPayNow = (Button) patientInformationLayout.findViewById(R.id.bt_pay_now);
        btPayNow.setText(getResources().getString(R.string.Request_Now));

        spDate = (Spinner) rlDateTimelayout.findViewById(R.id.spinner_date);
        spTime = (Spinner) rlDateTimelayout.findViewById(R.id.spinner_time);

        /*Success Screen variable*/
        tv_success_status = (TextView) findViewById(R.id.tv_success_status);
        tvTotalCost = (TextView) findViewById(R.id.tv_total_price);
        tvPatientName = (TextView) findViewById(R.id.tv_patient_name);
        tvPatientAddress = (TextView) findViewById(R.id.tv_patient_address);
        tvPatientMobile = (TextView) findViewById(R.id.tv_mobile_number);
        btn_view_instruction = (Button) findViewById(R.id.btn_view_instruction);
        btn_view_all_order = (Button) findViewById(R.id.btn_view_all_order);
        tvDate = (TextView) findViewById(R.id.tv_date_success);
        tvTime = (TextView) findViewById(R.id.tv_time_success);
        tvAllTestNames = (TextView) findViewById(R.id.tv_all_test_names);
        /*Success Screen variable*/
    }

    private void initView() throws ParseException {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_select_time));
        rlDateTimelayout.setVisibility(View.VISIBLE);
        patientInformationLayout.setVisibility(View.GONE);
        dateInfoLayout.setVisibility(View.GONE);

        UpdateUI();
        List<TestForAppointment> testMOList = OrderSummary.getInstance().getOrderist();
        rv_tests.setNestedScrollingEnabled(false);
        rv_tests.setLayoutManager(new LinearLayoutManager(this));

        testAdapter = new SelectedTestsAdapter(this, testMOList, updateUIData);
        rv_tests.setAdapter(testAdapter);

        day_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, day_list, TextConstants.DATE_TEXT);
        spDate.setAdapter(day_adapter);

        time_adapter = new SpinAdapter(mContext, R.layout.spinner_text_view, time_list, TextConstants.TIME_TEXT);
        spTime.setAdapter(time_adapter);

        setTimeAdapter(getResources().getString(R.string.txt_today));

        UserMO userMO = SettingManager.getInstance().getUserMO();
        patientFristName.setText(userMO.getFirstName());
        patientLastName.setText(userMO.getLastName());
        if (SharedPref.getMobileNumber() != null && !SharedPref.getMobileNumber().equals(getResources().getString(R.string.txt_null)) && !SharedPref.getMobileNumber().equals("")) {
            mMobileNumber.setText(SharedPref.getMobileNumber().substring(3));
        }

        if (Utils.isValueAvailable(userMO.getEmail())) {
            et_email.setText(userMO.getEmail());
        }

        try {
            list_states.clear();
            Gson gson1 = new Gson();
            Type listType = null;
            listType = new TypeToken<List<RenderConfigModel>>() {
            }.getType();
            RenderConfigModel renderConfigModel = gson1.fromJson(SharedPref.getRenderConfig().toString(), RenderConfigModel.class);
            list_states.add(renderConfigModel);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        state_adapter = new AddressAdapter(mContext, R.layout.spinner_address, list_states, TextConstants.STATE);
        city_adapter = new AddressAdapter(mContext, R.layout.spinner_address, list_states, TextConstants.CITY);

        spinner_state.setAdapter(state_adapter);
        spinner_city.setAdapter(city_adapter);
        spinner_city.setClickable(false);
        spinner_city.setEnabled(false);
        line_city.setBackgroundColor(getResources().getColor(R.color.dusky_blue_30));

        spinner_change_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                String countrycode =  stringTokenizer.nextToken().toString().trim();
                String countrycodeplus = "+" +countrycode;
                if (Integer.parseInt(countrycode)== TextConstants.INDIA_COUNTRYCODE) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    mMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    /*[  .... Moblile length set on   [ max_length- countrycode(including+) ]   ........  ]*/
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countrycodeplus.length()));
                    mMobileNumber.setFilters(FilterArray);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        TelephonyManager telMgr = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_READY:
                setCountryCode(true);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                setCountryCode(false);
                break;
            default:
                setCountryCode(false);
                break;

        }


        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    city_adapter.setSelectedStatePosition(-1);
                    spinner_city.setEnabled(false);
                    spinner_city.setClickable(false);
                    line_city.setBackgroundColor(getResources().getColor(R.color.dusky_blue_30));
                } else {
                    spinner_city.setEnabled(true);
                    spinner_city.setClickable(true);
                    line_city.setBackgroundColor(getResources().getColor(R.color.dusky_blue_50));
                    city_adapter.setSelectedStatePosition(position - 1);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });


    }

    private void bindView() {
        btSubmit.setOnClickListener(this);
        iv_ic_menu_overflow.setOnClickListener(this);
        btPayNow.setOnClickListener(this);
        btn_view_instruction.setOnClickListener(this);
        btn_view_all_order.setOnClickListener(this);
        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                Calendar calendar = Calendar.getInstance();
                // get a date to represent "today"
                Date today = calendar.getTime();
                // add one day to the date/calendar
                calendar.add(Calendar.DAY_OF_YEAR, position);
                // now get "tomorrow"
                Date formatted = calendar.getTime();

                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_today));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    try {
                        setTimeAdapter(getResources().getString(R.string.txt_tomorrow));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                //............... Changeing time on changeing date.....
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, dateInString);


                    /*[   ....set new time after setting date and time adapter...     ]*/
                OrderSummary.getInstance().setDate(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.MMM_dd_comma_yyyy, dateInString));
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(spTime.getSelectedItem().toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
               /*[   ....End of set new time after setting date and time adapter...     ]*/

                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, enddateInString);
                //..........End of finding end date time..............

                // ............... End of Changeing time on changeing date.....

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OrderSummary.getInstance().setTime(DateTimeUtil.convertSourceLocaleToDateEnglish(parent.getItemAtPosition(position).toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat));
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                String formattedDate = df.format(c.getTime()) + " " + spTime.getSelectedItem().toString();
                formattedDate = DateTimeUtil.convertSourceLocaleToDateEnglish(formattedDate, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated, Locale.US);
                try {
                    c.setTime(sdf.parse(formattedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // if selected tomorrow add 1 day to current day
                if (spDate.getSelectedItem().toString().trim().equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
                    c.add(Calendar.DATE, 1);
                }

                //.......... finding start date time..............
                Date resultdate = new Date(c.getTimeInMillis());
                String dateInString = sdf.format(resultdate);
                starttime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, dateInString);
                //.......... End of finding start date time..............

                //.......... finding end date time..............
                c.add(Calendar.MINUTE, 30);
                Date enddate = new Date(c.getTimeInMillis());
                String enddateInString = sdf.format(enddate);
                endtime = CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, enddateInString);
                //..........End of finding end date time..............
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        try {
            setTimeAdapter(getResources().getString(R.string.txt_today));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_ic_menu_overflow:
                showPopup(iv_ic_menu_overflow);
                break;

            case R.id.bt_next:
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                } else {
                    for (int i = 0; i < OrderSummary.getInstance().getOrderist().size(); i++) {
                        if (Integer.parseInt(OrderSummary.getInstance().getOrderist().get(i).getHcsGroups().get(0).getMoHcsList().get(0).getHcsId()) == 0) {
                            Toast.makeText(mContext, getResources().getString(R.string.went_wrong_please_retry), Toast.LENGTH_SHORT).show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 200);
                            return;
                        }
                    }
                    rlDateTimelayout.setVisibility(View.GONE);
                    dateInfoLayout.setVisibility(View.VISIBLE);
                    patientInformationLayout.setVisibility(View.VISIBLE);
                    toolbar_title.setText(R.string.txt_patient_information);
                    date.setText(CommonTasks.formateDateFromstring(DateTimeUtil.MMM_dd_comma_yyyy, DateTimeUtil.dd_MMM_yyyy, OrderSummary.getInstance().getDate().toString().trim()));

                    if (DateFormat.is24HourFormat(mContext)) {
                        time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.destinationTimeFormat, DateTimeUtil.HH_mm, OrderSummary.getInstance().getTime().toString().trim()));
                    } else {
                        time.setText(CommonTasks.formateDateFromstring(DateTimeUtil.destinationTimeFormat, DateTimeUtil.hh_mm_aaa, OrderSummary.getInstance().getTime().toString().trim()));
                    }
                }
                break;
            case R.id.bt_pay_now:
                if (!checkConstraints())
                    return;
                DialogUtils.showAlertDialogCommon(SelectTimeActivity.this,
                        mContext.getResources().getString(R.string.txt_dialog_patient_info_title),
                        mContext.getResources().getString(R.string.txt_dialog_patient_info_msg),
                        mContext.getResources().getString(R.string.ok), mContext.getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case Dialog.BUTTON_NEGATIVE:
                                        dialog.dismiss();
                                        break;

                                    case Dialog.BUTTON_POSITIVE:
                                        if (!Utils.showDialogForNoNetwork(mContext, false)) {
                                            return;
                                        }
                                        dialog.dismiss();
                                        pdialog = new ProgressDialog(mContext);
                                        pdialog.setMessage(getResources().getString(R.string.loading_txt));
                                        pdialog.setCanceledOnTouchOutside(false);
                                        pdialog.show();
                                        long transactionId = getTransactionId();
                                        ThreadManager.getDefaultExecutorService().submit(new PlaceTestOrderRequest(transactionId, getPayload(), responseCallback));
                                        break;
                                }
                            }
                        });
                break;

            case R.id.btn_view_instruction:
                ViewInstructionsActivity.newInstance(mContext);
                break;

            case R.id.btn_view_all_order:
                goToAppointListScreen();
                break;
            default:
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (successlayout.getVisibility() == View.VISIBLE) {
            OrderSummary.getInstance().setOrderist(null);
            OrderSummary.getInstance().setTotalPrice(0);
            OrderSummary.getInstance().setTestNumber(0);

            goToAppointListScreen();

        } else if (rlDateTimelayout.getVisibility() == View.VISIBLE) {
            if (!BuildConfig.isMultipleSelectionEnabled) {
                OrderSummary.getInstance().setOrderist(null);
                OrderSummary.getInstance().setTotalPrice(0);
                OrderSummary.getInstance().setTestNumber(0);
            }
            super.onBackPressed();

        } else if (patientInformationLayout
                .getVisibility() == View.VISIBLE) {
            patientInformationLayout.setVisibility(View.GONE);
            dateInfoLayout.setVisibility(View.GONE);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(patientInformationLayout.getWindowToken(), 0);
            rlDateTimelayout.setVisibility(View.VISIBLE);
            toolbar_title.setText(R.string.txt_select_time);
        }
    }


    public void setTimeAdapter(String whichday) throws ParseException {
        if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_today))) {
        /*[   Checking time must shown on spinner greater then current time  ]*/
            String lasttime = null;
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);

            String strdate = ft.format(date);
            Date newdate = null;
            try {
                newdate = ft.parse(strdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            time_list.clear();
            int length = getResources().getStringArray(R.array.time_units).length;
            int i = 0;
            for (String time : getResources().getStringArray(R.array.time_units)) {
                Date tempdate = null;
                try {
                    tempdate = ft.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (newdate.before(tempdate)) {
                    String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    time_list.add(new_time);
                } else {
                }
                if (i == length - 1) {
                    lasttime = time;
                    if (newdate.after(ft.parse(lasttime))) {
                        time_list.clear();
                        for (int j = 0; j < getResources().getStringArray(R.array.time_units).length; j++) {
                            String new_time = DateTimeUtil.convertSourceDestinationDateLocale(getResources().getStringArray(R.array.time_units)[j], DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                            time_list.add(new_time);
                        }
                    }
                }
                i++;
            }

            //.......... checking for days last time and set it to next day......
            if (newdate.after(ft.parse(lasttime))) {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            } else {
                day_list.clear();
                day_list.add(getResources().getString(R.string.txt_today));
                day_list.add(getResources().getString(R.string.txt_tomorrow));
            }
            //.......... End of checking for days last time and set it to next day......

            day_adapter.notifyDataSetChanged();
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        } else if (whichday.equalsIgnoreCase(getResources().getString(R.string.txt_tomorrow))) {
            time_list.clear();
            for (String time : getResources().getStringArray(R.array.time_units)) {
                String new_time = DateTimeUtil.convertSourceDestinationDateLocale(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                time_list.add(new_time);
            }
            time_adapter.notifyDataSetChanged();
            spTime.setSelection(0);
        }
/*[   End of Checking time must shown on spinner greater then current time  ]*/
    }

    @Override
    public void passResponse(final String response, final boolean mIsSuccessful) {

        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pdialog.isShowing()) {
                        pdialog.dismiss();
                    }
                    if (!mIsSuccessful) {

                        DialogUtils.showErrorDialog(mContext,
                                mContext.getResources().getString(R.string.Unsuccessful_Request),
                                response, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    } else {
                        OrderSummary.getInstance().setPatientName(patientFristName.getText().toString().trim() + " " + patientLastName.getText().toString().trim());
                        OrderSummary.getInstance().setPatientAddress(patientAddress.getText().toString().trim() + "\n" + spinner_city.getSelectedItem().toString().trim() + "\n" + spinner_state.getSelectedItem().toString().trim() + "\n" + et_postal_code.getText().toString().trim());
                        OrderSummary.getInstance().setPatientMobile(countryCode + mMobileNumber.getText().toString());

            /*[......... Parsing the appointment model...........]*/
                        Gson gson = new Gson();
                        Type listType = null;
                        listType = new TypeToken<List<AppointmentModel>>() {
                        }.getType();
                        appointment.model.appointmodel.List appointmentmodel = gson.fromJson(response, appointment.model.appointmodel.List.class);
            /*[......... End of Parsing the appointment model...........]*/
                        if (appointmentmodel.getStatus().equalsIgnoreCase("Booked") || appointmentmodel.getStatus().equalsIgnoreCase("proposed")) {
                            initSuccessView();
                /*[ calling broadcast to finish search activity       ]*/
                            Intent intent_finish = new Intent(TextConstants.FINISH_ACTIVITY);
                            sendBroadcast(intent_finish);
                /*[ End of calling broadcast to finish search activity       ]*/
                            /*if (REBOOK) {
                                Intent intent = new Intent();
                                intent.putExtra("appointment_model", appointmentmodel);
                                intent.putExtra("position", position);
                                setResult(TextConstants.REBOOK_CONSTANT, intent);
                            }*/
                        } else {
                            DialogUtils.showErrorDialog(mContext, getResources().getString(R.string.Test_not_Booked), appointmentmodel.getStatus(), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    private void initSuccessView() {
        MyAppointmentFragment.isAppointmentSuccessful = true;
        successlayout.setVisibility(View.VISIBLE);
        verticalScrollView.setVisibility(View.GONE);

        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.isSuccessful = true;
        }

        tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        toolbar_title.setText(getResources().getString(R.string.Successful_Request));

        tvPatientName.setText(OrderSummary.getInstance().getPatientName());
        tvPatientAddress.setText(OrderSummary.getInstance().getPatientAddress());
        tvPatientMobile.setText(OrderSummary.getInstance().getPatientMobile());
        tvDate.setText(OrderSummary.getInstance().getDate());

        if (DateFormat.is24HourFormat(mContext)) {
            tvTime.setText(CommonTasks.formateDateFromstring(DateTimeUtil.destinationTimeFormat, DateTimeUtil.HH_mm, OrderSummary.getInstance().getTime()));
        } else {
            tvTime.setText(OrderSummary.getInstance().getTime());
        }


        setTestNames();

        //....... If order is placed successful, good practice to clear the ordersummary...........
        OrderSummary.getInstance().clearOrderSummary();
    }

    private void setTestNames() {
        String name = "";
        int size = OrderSummary.getInstance().getOrderist().size();
        for (int i = 0; i < size; i++) {
            if (i != size - 1) {
                name += OrderSummary.getInstance().getOrderist().get(i).getTestName().trim() + ", ";
            } else
                name += OrderSummary.getInstance().getOrderist().get(i).getTestName().toString().trim();
        }
        tvAllTestNames.setText(name);
    }


    private boolean checkConstraints() {
        if (TextUtils.isEmpty(patientFristName.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.enter_first_name), Toast.LENGTH_SHORT).show();
            patientFristName.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(patientLastName.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            patientLastName.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(patientAddress.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.enter_address), Toast.LENGTH_SHORT).show();
            patientAddress.requestFocus();
            return false;
        }

        if (spinner_state.getSelectedItem().toString().equals(getResources().getString(R.string.Select_State))) {
            Toast.makeText(this, getResources().getString(R.string.Please_select_state), Toast.LENGTH_SHORT).show();
            spinner_state.requestFocus();
            return false;
        }

        if (spinner_city.getSelectedItem().toString().equals(getResources().getString(R.string.Select_City))) {
            Toast.makeText(this, getResources().getString(R.string.Please_select_city), Toast.LENGTH_SHORT).show();
            spinner_city.requestFocus();
            return false;
        }

        if (et_postal_code.getText().toString().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.Please_enter_zipcode), Toast.LENGTH_SHORT).show();
            et_postal_code.requestFocus();
            return false;
        }


        if (et_email.getText().toString().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_email_address), Toast.LENGTH_SHORT).show();
            et_email.requestFocus();
            return false;
        }


        if (!Utils.isEmailValid(et_email.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.enter_valid_email), Toast.LENGTH_SHORT).show();
            et_email.requestFocus();
            return false;
        }

        if (!validateFieldAndCaptureAlternateContact()) {
            mMobileNumber.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void UpdateUI() {
        tvTotalTest.setText(OrderSummary.getInstance().getOrderist().size() + "");
        tvTotalPrice.setText(OrderSummary.getInstance().getTotalPrice() + "");
    }


    private class AsyncTaskForHCSID extends AsyncTask<Void, Void, String> {

        int testId;
        int position;

        public AsyncTaskForHCSID(int testId, int position) {
            this.testId = testId;
            this.position = position;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Toast.makeText(SelectTimeActivity.this, "Fetching HCS-ID...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub

            String jsonString = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("_query", "{\"extension.value.testCompendiumId\":\"" + testId + "\"}");
                JSONObject constraintsObject = new JSONObject();
                constraintsObject.put("constraints", jsonObject);

                jsonString = APIManager.getInstance().executeHttpPost(MphRxUrl.getHCSSearchUrl(), "", constraintsObject, SelectTimeActivity.this);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonString;
        }

        @Override
        protected void onPostExecute(String jsonString) {
            super.onPostExecute(jsonString);

            try {

                JSONObject jsonObject = new JSONObject(jsonString);

                JSONArray jsonArray = jsonObject.getJSONArray("list");

                int hcsId = jsonArray.getJSONObject(0).getInt("id");
                OrderSummary.getInstance().getOrderist().get(position).getHcsGroups().get(0).getMoHcsList().get(0).setHcsId(String.valueOf(hcsId));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(mContext, v);
        MenuInflater inflater = popup.getMenuInflater();

        inflater.inflate(R.menu.menu_back, popup.getMenu());

        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.change_time:
                        if (patientInformationLayout.getVisibility() == View.VISIBLE) {
                            patientInformationLayout.setVisibility(View.GONE);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(patientInformationLayout.getWindowToken(), 0);
                            rlDateTimelayout.setVisibility(View.VISIBLE);
                            dateInfoLayout.setVisibility(View.GONE);
                            toolbar_title.setText(R.string.txt_select_time);
                        }
                        break;

                    default:
                        break;
                }

                return true;
            }
        });
    }

    private String getPayload() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.US);
        String formattedDate = df.format(c.getTime());
        int minsDuration = (int) new CommonTasks().findDateDifferenceinMins(starttime, endtime, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);

        UserMO userMO = SettingManager.getInstance().getUserMO();
        long patientid = OrderSummary.getInstance().getPatientId();
        if (patientid == 0) {
            patientid = userMO.getPatientId();
        }


        String reason = "";
        String status = AppointmentConstants.STATUS_PROPOSED;
        String type = TextConstants.TYPE_HSC;
        String payLoad = null;
        JSONObject jsonObject = null;
        try {

            payLoad = "{\"type\":{\"text\":\"" + type + "\"}," +
                    "\"end\":{\"value\":\"" + endtime + "\"}," +
                    "\"minutesDuration\":{\"value\"" + ": " + minsDuration + "}," +
                    "\"extension\":[{\"url\":\"Audit\",\"value\":[{\"status\":\"" + status + "\"," +
                    "\"actionBy\":\"" + (patientFristName.getText() + " " + patientLastName.getText()).toString().trim() + "\"," +
                    "\"actionReason\":\"" + reason + "\"," +
                    "\"actionComment\":\"" + "" + "\"," +
                    "\"actionOn\":\"" + formattedDate + "\"}]}]," +
                    "\"participant\":[{\"actorPatient\":{\"id\":" + patientid + ",\"class\":\"com.mphrx.consus.resources.Patient\"},\"type\":[{\"text\":{\"value\":\"SBJ\"}}]}]," +
                    "\"priority\":{\"value\":3}," +
                    "\"start\":{\"value\":\"" + starttime + "\"}," +
                    "\"status\":{\"value\":\"" + status + "\"}}";

            jsonObject = new JSONObject(payLoad);

              /*[....... Inserting address in extension array...........]*/
            JSONObject j = new JSONObject();
            j.put("url", "SampleCollectionLocation");
            JSONArray ja = new JSONArray();
            ja.put(0, new JSONObject().put("addr", patientAddress.getText().toString().trim()).put("city", spinner_city.getSelectedItem().toString().trim()).put("state", spinner_state.getSelectedItem().toString().trim()).put("zip", et_postal_code.getText().toString().trim()).put("phone", countryCode + mMobileNumber.getText().toString().trim()));
            j.put("value", ja);
            jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), j);
            /*[....... End of Inserting address in extension array...........]*/


              /*[....... Inserting hcs in participant array...........]*/

            for (int i = 0; i < OrderSummary.getInstance().getOrderist().size(); i++) {
                JSONObject hcs_object = new JSONObject();

                JSONObject actorHealthcareService_object = new JSONObject();
                actorHealthcareService_object.put("id", Long.parseLong(OrderSummary.getInstance().getOrderist().get(i).getHcsGroups().get(0).getMoHcsList().get(0).getHcsId()));
                actorHealthcareService_object.put("class", "com.mphrx.consus.resources.HealthcareService");

                JSONArray type_array = new JSONArray();
                JSONObject type_object = new JSONObject();
                type_object.put("text", new JSONObject().put("value", "HCS"));
                type_array.put(0, type_object);

                hcs_object.put("actorHealthcareService", actorHealthcareService_object);
                hcs_object.put("type", type_array);

                jsonObject.getJSONArray("participant").put(jsonObject.getJSONArray("participant").length(), hcs_object);
            }
              /*[....... End of Inserting hcs in participant array...........]*/



             /*[....... Inserting appointment details in extension array...........]*/
            JSONObject appointmentdetail = new JSONObject();
            appointmentdetail.put("url", "appointmentDetails");

            JSONArray value_array = new JSONArray();
            for (int k = 0; k < OrderSummary.getInstance().getOrderist().size(); k++) {
                String department = null, instruction = null, cost = null, locationdetails = "", servicename = null;
                try {
                    instruction = OrderSummary.getInstance().getOrderist().get(k).getTestInstructions();
                } catch (Exception ex) {
                }
                try {
                    department = OrderSummary.getInstance().getOrderist().get(k).getHcsGroups().get(0).getMoHcsList().get(0).getDepartmentName();
                } catch (Exception ex) {
                }

                try {
                    locationdetails = OrderSummary.getInstance().getOrderist().get(k).getHcsGroups().get(0).getMoHcsList().get(0).getLocationName();
                } catch (Exception ex) {
                }


                try {
                    servicename = OrderSummary.getInstance().getOrderist().get(k).getHcsGroups().get(0).getServiceName();
                } catch (Exception ex) {
                }

                try {
                    if (OrderSummary.getInstance().getOrderist().get(k).getPrice() != null) {
                        cost = OrderSummary.getInstance().getOrderist().get(k).getPrice().getCurrentRate();
                    }
                } catch (Exception ex) {
                }
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                String country_code = "+" + stringTokenizer.nextToken().trim().toString();

                value_array.put(k, new JSONObject().put("useAppointmentDetailsFromExtension", true)
                        .put("serviceName", OrderSummary.getInstance().getOrderist().get(k).getTestName().trim())
                        .put("specialityName", "")
                        .put("practitionerName", "")
                        .put("locationDetails", CommonTasks.isTextEmptyOrNull(locationdetails) ? "" : locationdetails)
                        .put("roomDetails", "")
                        .put("email", et_email.getText().toString().trim())
                        .put("phoneNo", country_code + mMobileNumber.getText().toString().trim())
                        .put("timeZoneId", TimeZone.getDefault().getID().toString().trim())
                        .put("department", CommonTasks.isTextEmptyOrNull(department) ? "" : department)
                        .put("characteristic", new JSONArray().put(0, new JSONObject().put("coding", new JSONArray().put(0, new JSONObject().put("code", "ST-1").put("display", (instruction != null && !instruction.equals("null")) ? instruction : ""))).put("text", "instructions")))
                        .put("cost", CommonTasks.isTextEmptyOrNull(cost) ? "0" : cost));
            }
            appointmentdetail.put("value", value_array);
            jsonObject.getJSONArray("extension").put(jsonObject.getJSONArray("extension").length(), appointmentdetail);
            /*[....... End of Inserting appointment details in extension array...........]*/


        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }


    /*[.................. Edited by laxman singh....................]*/
    public void setCountryCode(boolean on) {

        int country_code = 0;
        if (on) {
            try {
                country_code = Integer.parseInt(GetCountryZipCode(this).toString().trim());
            } catch (Exception e) {
                // TODO: handle exception
                country_code = 91;
            }
        } else if (!on) {
            country_code = 91;
        }
        String[] list = getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < list.length; i++) {
            StringTokenizer stringTokenizer = new StringTokenizer(list[i].toString().trim(), ",");
            int valuecode = Integer.parseInt(stringTokenizer.nextToken());
            if (valuecode == country_code) {
                position_country_code = i;
                break;
            }
        }
        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        CountryAdapter adapter_country = new CountryAdapter(this, countrylist, "black");
        spinner_change_country_code.setAdapter(adapter_country);
        spinner_change_country_code.setSelection(position_country_code);
    }

    public static String GetCountryZipCode(Context context) {
        String CountryZipCode = null;
        String CountryID = null;
        try {
            CountryID = getUserCountry(context).toString().toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
            CountryID = "IN";
        }

        try {
            String[] rl = context.getResources().getStringArray(
                    R.array.CountryCodes);
            for (int i = 0; i < rl.length; i++) {
                String[] g = rl[i].split(",");
                if (g[1].trim().equals(CountryID.trim())) {
                    CountryZipCode = g[0];
                    break;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return CountryZipCode;
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country
                // code is
                // available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }
    /*[.................. End of Edited by laxman singh....................]*/


    private boolean validateFieldAndCaptureAlternateContact() {

        String phone = mMobileNumber.getText().toString().trim();

        if (!phone.equals("")) {
            StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
            String country_code =  stringTokenizer.nextToken().trim().toString();
            countryCode = "+"+country_code;

            if (phone.trim().equals("")) {
                setError(getResources().getString(R.string.enter_mobile_number));
                return false;
            } else if (Integer.parseInt(country_code)==TextConstants.INDIA_COUNTRYCODE && (phone.length() != TextConstants.INDIA_PHONENO_LENGTH)) {
                setError(getResources().getString(R.string.error_invalid_mobile_no));
                return false;
            } else if ((countryCode.length() + phone.length()) <= TextConstants.MIN_PHONENO_LENGTH) {
                setError(getResources().getString(R.string.mobile_must_6digits_including));
                return false;
            } else if ((countryCode.length() + phone.length()) > TextConstants.MAX_PHONENO_LENGTH) {
                setError(getResources().getString(R.string.error_invalid_mobile_no));
                return false;
            } else if (phone.length() == 0) //to change server settings
            {
                setError(getResources().getString(R.string.error_invalid_mobile_no));
                return false;
            }

            return true;

        } else

            setError(getResources().getString(R.string.error_invalid_mobile_no));
        return false;

    }

    private void setError(CharSequence error) {
        mMobileNumber.setError(error);
    }


    public void goToAppointListScreen() {
        if (CareTaskAppointment.careTaskAppointmentActivity != null && CareTaskAppointment.isSuccessful) {
                /*[Method runs when user come from CARETASK]*/
            for (int i = 0; i < CareTaskAppointment.listAppointmentActivity.size(); i++) {
                CareTaskAppointment.listAppointmentActivity.get(i).finish();
            }
            CareTaskAppointment.listAppointmentActivity.clear();
        } else {
            startActivity(new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
        finish();
    }
}
