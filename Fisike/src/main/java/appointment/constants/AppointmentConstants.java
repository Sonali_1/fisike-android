package appointment.constants;

/**
 * Created by laxmansingh on 4/11/2017.
 */

public class AppointmentConstants {

    public static final String STATUS_BOOKED = "booked";
    public static final String STATUS_PROPOSED = "proposed";

}
