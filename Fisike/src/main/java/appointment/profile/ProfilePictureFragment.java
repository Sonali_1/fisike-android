package appointment.profile;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.CameraFragment;
import com.mphrx.fisike.fragments.ImageUploadUIFragment;
import com.mphrx.fisike.icomoon.IconButton;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.UpdateProfilePicActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.views.CircleImageView;

import java.util.ArrayList;

import appointment.profile.ProfileInterface.UpdateProfilePictureAndName;


/**
 * Created by neharathore on 09/08/17.
 */

public class ProfilePictureFragment extends ImageUploadUIFragment implements CameraFragment.CameraIntegration, View.OnClickListener, UpdateProfilePictureAndName {

    private Context mContext;
    private RelativeLayout btnUpdatePicture, btnAddPicture;
    private LinearLayout rlParent;
    private CircleImageView civUserProfilePicture;
    Bitmap userProfilePictureBitmap;
    private IconButton itvCamera;
    private byte[] profilePictureByte;
    CustomFontTextView tvFullName;
    public static final String ISUPDATEALLOWED = "isUpdateAllowed";
    private boolean isUpdateAllowed = true;


    @Override
    protected int getLayoutId() {
        mContext = getActivity();
        return R.layout.fragment_profile_picture;
    }

    @Override
    public void findView() {
        if(getArguments() != null){
            isUpdateAllowed = getArguments().getBoolean(ISUPDATEALLOWED);
        }
        btnAddPicture = (RelativeLayout) getView().findViewById(R.id.btn_add_pic);
        btnUpdatePicture = (RelativeLayout) getView().findViewById(R.id.btn_update_pic);
        civUserProfilePicture = (CircleImageView) getView().findViewById(R.id.civ_user_profile_pic);
        itvCamera = (IconButton) getView().findViewById(R.id.itv_icon_camera);
        rlParent = (LinearLayout) getView().findViewById(R.id.rl_parent_profile_picture);
        tvFullName = (CustomFontTextView) getView().findViewById(R.id.tv_full_name);
    }

    @Override
    public void initView() {
        BusProvider.getInstance().register(this);
        cameraIntegrationListener = this;
        btnAddPicture.setOnClickListener(this);
        btnUpdatePicture.setOnClickListener(this);
        userProfilePictureBitmap = ((setUpdateBitmap) mContext).getBitmap();
        String name = ((setUpdateBitmap) mContext).getDisplayedName();
        if (Utils.isValueAvailable(name))
            tvFullName.setText(name);
    }

    @Override
    public void bindView() {
        super.bindView();
    }

    @Override
    public void setExtractedBitmap(Bitmap bitmap) {
        if(userProfilePictureBitmap==null && bitmap ==null)
            return;

        ((setUpdateBitmap) mContext).sendApiToUpdateProfilePicture(bitmap);
    }

    private void updateBitMap(Bitmap bitmap) {
        userProfilePictureBitmap = bitmap;
        ((setUpdateBitmap) mContext).setBitmap(bitmap);
        setUserProfilePic(bitmap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_pic:
                if (!(btnUpdatePicture.getVisibility() == View.VISIBLE))
                {   if(userProfilePictureBitmap==null && isUpdateAllowed)
                        showBottomSheet(userProfilePictureBitmap);
                }
                break;
            case R.id.btn_update_pic:
                if(isUpdateAllowed) {
                    showBottomSheet(userProfilePictureBitmap);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserProfilePic(userProfilePictureBitmap);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity=Gravity.CENTER_HORIZONTAL;
        tvFullName.setLayoutParams(layoutParams);
    }


    public void setUserProfilePic(Bitmap bitmap) {

        try {
            Bitmap blurBitmap;

            if (bitmap != null) {
                civUserProfilePicture.setVisibility(View.VISIBLE);
                civUserProfilePicture.setImageBitmap(bitmap);
                profilePictureByte = Utils.convertBitmapToByteArray(bitmap);
                itvCamera.setVisibility(View.GONE);
                if (!((setUpdateBitmap) mContext).isViewOnly())
                    btnUpdatePicture.setVisibility(View.VISIBLE);
                try {
                    blurBitmap = Utils.blur(mContext, BitmapFactory.decodeByteArray(profilePictureByte, 0, profilePictureByte.length), 20f);
                    DisplayMetrics metrics = new DisplayMetrics();
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    rlParent.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    int height = 200;
                    height = rlParent.getHeight();
                    if(height == 0){
                        height = rlParent.getMeasuredHeight();
                    }
                    blurBitmap = Utils.scaleBitmapAndKeepRation(blurBitmap, height, metrics.widthPixels);
                    Drawable drawable = new BitmapDrawable(getResources(), blurBitmap);
                    rlParent.setBackgroundDrawable(drawable);

                } catch (Exception e) {
                    setDefaultView();

                }

            } else {
                setDefaultView();
            }

        } catch (Exception e) {
            setDefaultView();
        }
    }

    private void setDefaultView() {
        civUserProfilePicture.setImageDrawable(getResources().getDrawable(R.drawable.img_transparent));
        civUserProfilePicture.setVisibility(View.GONE);
        rlParent.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        itvCamera.setVisibility(View.VISIBLE);
        btnUpdatePicture.setVisibility(View.GONE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void updateUserName(String name) {
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity=Gravity.CENTER_HORIZONTAL;
        tvFullName.setLayoutParams(layoutParams);
        
        if (Utils.isValueAvailable(name))
            tvFullName.setText(name);
        else
            tvFullName.setText("");
    }

    @Override
    public void updateProfilePicture(Bitmap bitmap) {
        updateBitMap(bitmap);
    }

    interface setUpdateBitmap {
        public void setBitmap(Bitmap bitmap);

        public Bitmap getBitmap();

        public String getDisplayedName();

        public void sendApiToUpdateProfilePicture(Bitmap bitmap);

        public boolean isViewOnly();

    }
}

