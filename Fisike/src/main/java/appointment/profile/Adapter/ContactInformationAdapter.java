package appointment.profile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.ArrayList;

import appointment.profile.FetchUpdateTelecom;

/**
 * Created by neharathore on 16/08/17.
 */

public class ContactInformationAdapter extends RecyclerView.Adapter<ContactInformationAdapter.ContactInformationViewHolder> {
    ArrayList<FetchUpdateTelecom> phoneList;
    Context context;

    public ContactInformationAdapter(Context context,ArrayList<FetchUpdateTelecom> phoneList)
    {
        this.phoneList=phoneList;
        this.context=context;
    }

    @Override
    public ContactInformationAdapter.ContactInformationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_list_row, parent, false);
        ContactInformationViewHolder viewHolder = new ContactInformationViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ContactInformationViewHolder holder, int position)
    {
        FetchUpdateTelecom telecom=getPhoneInstanceAtPosition(position);
        holder.tvProfileRowLabel.setText(telecom.getUseCode());
        holder.tvProfileRowValue.setText(telecom.getValue());
        holder.ivProfileRowIcon.setBackground(context.getResources().getDrawable(R.drawable.contact_number));
        //setting default data
        if(!telecom.isPrimary())
        {
            holder.ivDefaultIndicator.setVisibility(View.GONE);
        }


        //setting is verified
        if(telecom.isVerified())
            holder.ivVerifiedIndicator.setTextColor(context.getResources().getColor(R.color.green_btn_bg));
        else
            holder.ivVerifiedIndicator.setTextColor(context.getResources().getColor(R.color.orange));


    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }


    public class ContactInformationViewHolder extends RecyclerView.ViewHolder{

        public IconTextView ivVerifiedIndicator,ivOverflow,ivDefaultIndicator;
        private ImageView ivProfileRowIcon;
        public  CustomFontTextView tvProfileRowLabel,tvProfileRowValue;

        public ContactInformationViewHolder(View itemView) {
            super(itemView);
            ivProfileRowIcon= (ImageView) itemView.findViewById(R.id.iv_profile_row_icon);
            ivDefaultIndicator= (IconTextView) itemView.findViewById(R.id.iv_default_indicator);
            ivVerifiedIndicator= (IconTextView) itemView.findViewById(R.id.iv_verified_indicator);
            ivOverflow= (IconTextView) itemView.findViewById(R.id.iv_overflow);
            tvProfileRowLabel= (CustomFontTextView) itemView.findViewById(R.id.tv_profile_row_label);
            tvProfileRowValue= (CustomFontTextView) itemView.findViewById(R.id.tv_profile_row_value);
        }
    }

    public FetchUpdateTelecom getPhoneInstanceAtPosition(int position)
    {
        if(phoneList!=null)
            return phoneList.get(position);
        return null;
    }


}
