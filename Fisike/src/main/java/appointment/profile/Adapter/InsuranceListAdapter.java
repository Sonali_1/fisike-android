package appointment.profile.Adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

/**
 * Created by neharathore on 24/08/17.
 */

public class InsuranceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Coverage> coverageList;
    Context mContext;
    private final int INSURANCE_TYPE_MANUAL = 0;
    private final int INSURANCE_TYPE_IMAGE = 1;
    boolean isFromProfile = true;
    int selectedPosition = -1;
    CheckChangeListener listener;

    public InsuranceListAdapter(Context context, ArrayList<Coverage> coverageList, boolean isFromProfile) {
        this.mContext = context;
        this.coverageList = coverageList;
        this.isFromProfile = isFromProfile;
    }

    public InsuranceListAdapter(Context context, ArrayList<Coverage> coverageList, boolean isFromProfile, int selectedPosition, CheckChangeListener listener) {
        this.mContext = context;
        this.coverageList = coverageList;
        this.isFromProfile = isFromProfile;
        this.selectedPosition = selectedPosition;
        this.listener = listener;
    }


    public class ManualInsuranceViewHolder extends RecyclerView.ViewHolder {
        public ImageView iconInsurance;
        public RadioButton rbIsSelected;
        public IconTextView iconDefaultIndicator, ivOverFlow;
        public CustomFontTextView insuranceType, insurancetypeCompanyName, lblPolicyNumber, txtPolicyNo, lblGroupNo, txtGroupNo, lblPolicyHolderName, txtPolicyHolderName;

        public ManualInsuranceViewHolder(View itemView) {
            super(itemView);
            iconInsurance = (ImageView) itemView.findViewById(R.id.icon_insurance);
            rbIsSelected = (RadioButton) itemView.findViewById(R.id.rb_isSelected);
            iconDefaultIndicator = (IconTextView) itemView.findViewById(R.id.iv_default_indicator);
            ivOverFlow = (IconTextView) itemView.findViewById(R.id.iv_overflow);
            insuranceType = (CustomFontTextView) itemView.findViewById(R.id.insuranceType);
            insurancetypeCompanyName = (CustomFontTextView) itemView.findViewById(R.id.insurancetype_company_name);
            lblPolicyNumber = (CustomFontTextView) itemView.findViewById(R.id.lbl_policy_number);
            txtPolicyNo = (CustomFontTextView) itemView.findViewById(R.id.txt_policy_no);
            lblGroupNo = (CustomFontTextView) itemView.findViewById(R.id.lbl_group_number);
            txtGroupNo = (CustomFontTextView) itemView.findViewById(R.id.txt_group_no);
            lblPolicyHolderName = (CustomFontTextView) itemView.findViewById(R.id.lbl_policy_holder_name);
            txtPolicyHolderName = (CustomFontTextView) itemView.findViewById(R.id.txt_policy_holder_name);
/*
            rbIsSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(listener!=null)
                    {
                        listener.checkChanged(getAdapterPosition());
                    }
                }
            });
*/
            rbIsSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.checkChanged(getAdapterPosition());
                }
            });
        }
    }

    public class ImageInsuranceViewHolder extends RecyclerView.ViewHolder {
        public ImageView iconInsurance, insuranceFrontImage, insuranceBackImage;
        public RadioButton rbIsSelected;
        public IconTextView iconDefaultIndicator, ivOverFlow;

        public ImageInsuranceViewHolder(View itemView) {
            super(itemView);
            iconInsurance = (ImageView) itemView.findViewById(R.id.icon_insurance);
            insuranceFrontImage = (ImageView) itemView.findViewById(R.id.insurance_front_image);
            insuranceBackImage = (ImageView) itemView.findViewById(R.id.insurance_back_image);
            rbIsSelected = (RadioButton) itemView.findViewById(R.id.rb_isSelected);
        /*    rbIsSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                           if(listener!=null)
                           {
                               listener.checkChanged(getAdapterPosition());
                           }
                }
            });*/

            rbIsSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.checkChanged(getAdapterPosition());
                }
            });
            iconDefaultIndicator = (IconTextView) itemView.findViewById(R.id.iv_default_indicator);
            ivOverFlow = (IconTextView) itemView.findViewById(R.id.iv_overflow);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == INSURANCE_TYPE_IMAGE) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.insurance_picture_row, null);
            InsuranceListAdapter.ImageInsuranceViewHolder imageInsuranceViewHolder = new InsuranceListAdapter.ImageInsuranceViewHolder(view);
            return imageInsuranceViewHolder;
        } else if (viewType == INSURANCE_TYPE_MANUAL) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manual_insurance_row, null);
            InsuranceListAdapter.ManualInsuranceViewHolder manualInsuranceViewHolder = new InsuranceListAdapter.ManualInsuranceViewHolder(view);
            return manualInsuranceViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Coverage coverage = getInsuranceAtPosition(position);
        if (holder instanceof ManualInsuranceViewHolder)
            setManualInsuranceData(coverage, position, (ManualInsuranceViewHolder) holder);

        else if (holder instanceof ImageInsuranceViewHolder)
            setImageInsuranceData(coverage, position, (ImageInsuranceViewHolder) holder);


    }

    private void setImageInsuranceData(Coverage coverage, int position, ImageInsuranceViewHolder holder) {
        try {

            holder.insuranceFrontImage.setBackground(new BitmapDrawable(Utils.convertBase64ToBitmap(coverage.getFrontImage())));
            holder.insuranceBackImage.setBackground(new BitmapDrawable(Utils.convertBase64ToBitmap(coverage.getBackImage())));

            if (!isFromProfile) {
                holder.iconInsurance.setVisibility(View.GONE);
                holder.rbIsSelected.setVisibility(View.VISIBLE);
                holder.ivOverFlow.setVisibility(View.GONE);
                holder.iconDefaultIndicator.setVisibility(View.GONE);
                if (selectedPosition != -1 && position == selectedPosition)
                    holder.rbIsSelected.setChecked(true);
                else
                    holder.rbIsSelected.setChecked(false);

            } else {
/*
               TODO setting default star if coverage has default
                if(coverage.isDefault())
                    holder.iconDefaultIndicator.setVisibility(View.VISIBLE);
*/
            }


        } catch (Exception e) {
        }
    }

    private void setManualInsuranceData(Coverage coverage, int position, ManualInsuranceViewHolder holder) {

        if (!isFromProfile) {
            holder.iconInsurance.setVisibility(View.GONE);
            holder.rbIsSelected.setVisibility(View.VISIBLE);
            holder.ivOverFlow.setVisibility(View.GONE);
            holder.iconDefaultIndicator.setVisibility(View.GONE);
            if (selectedPosition != -1 && position == selectedPosition)
                holder.rbIsSelected.setChecked(true);
            else
                holder.rbIsSelected.setChecked(false);

        } else {
/*
               TODO setting default star if coverage has default
                if(coverage.isDefault())
                    holder.iconDefaultIndicator.setVisibility(View.VISIBLE);
*/
        }
        if (coverage.isMedicareInsurance(mContext) || coverage.isMedicaidInsurance(mContext)) {
            holder.txtPolicyNo.setText(coverage.getInsuranceType());
            setValueIfAvailable(holder.lblPolicyNumber, holder.txtPolicyNo, coverage.getPolicyNumber());
        } else {
            setValueIfAvailable(holder.insuranceType, holder.insurancetypeCompanyName, coverage.getPrimaryInsuranceCompany());
            setValueIfAvailable(holder.lblPolicyNumber, holder.txtPolicyNo, coverage.getPolicyNumber());
            setValueIfAvailable(holder.lblGroupNo, holder.txtGroupNo, coverage.getGroupId());
            setValueIfAvailable(holder.lblPolicyHolderName, holder.txtPolicyHolderName, coverage.getPolicyHolderName());
        }
    }

    private void setValueIfAvailable(CustomFontTextView label, CustomFontTextView txtValue, String value) {
        if (Utils.isValueAvailable(value)) {
            if (label != null)
                label.setVisibility(View.VISIBLE);
            if (txtValue != null) {
                txtValue.setVisibility(View.VISIBLE);
                txtValue.setText(value);
            }
        }
    }

    public Coverage getInsuranceAtPosition(int position) {
        if (position != -1 && coverageList.size() > position)
            return coverageList.get(position);
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (coverageList.get(position).isManualInsurance(mContext))
                return INSURANCE_TYPE_MANUAL;
            else
                return INSURANCE_TYPE_IMAGE;
        } catch (Exception e) {
        }
        return INSURANCE_TYPE_MANUAL;
    }

    @Override
    public int getItemCount() {
        return coverageList.size();
    }

    public void setRadioButtonSelectedIndex(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getInsuranceSelectedPosition() {
        return selectedPosition;
    }

    public interface CheckChangeListener {
        void checkChanged(int position);
    }
}
