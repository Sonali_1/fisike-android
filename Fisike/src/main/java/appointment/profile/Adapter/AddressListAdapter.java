package appointment.profile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

/**
 * Created by neharathore on 23/08/17.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressViewHolder> {
    ArrayList<Address> addressList;
    Context context;

    public AddressListAdapter(Context context, ArrayList<Address> addressList) {
        this.addressList = addressList;
        this.context = context;
    }

    @Override
    public AddressListAdapter.AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_list_row, parent, false);
        AddressViewHolder viewHolder = new AddressViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        Address address = getAddressAtPosition(position);
        holder.tvProfileRowLabel.setText(address.getUseCode());
        holder.tvProfileRowValue.setText(Utils.getAddressTextString(address));
        holder.ivProfileRowIcon.setBackground(context.getResources().getDrawable(R.drawable.address_icon));
        //setting default data
        if (!address.isPrimary()) {
            holder.ivDefaultIndicator.setVisibility(View.GONE);
        }


        //setting is verified
        holder.ivVerifiedIndicator.setVisibility(View.GONE);


    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }


    public class AddressViewHolder extends RecyclerView.ViewHolder {

        public IconTextView ivVerifiedIndicator, ivOverflow, ivDefaultIndicator;
        private ImageView ivProfileRowIcon;
        public CustomFontTextView tvProfileRowLabel, tvProfileRowValue;

        public AddressViewHolder(View itemView) {
            super(itemView);
            ivProfileRowIcon = (ImageView) itemView.findViewById(R.id.iv_profile_row_icon);
            ivDefaultIndicator = (IconTextView) itemView.findViewById(R.id.iv_default_indicator);
            ivVerifiedIndicator = (IconTextView) itemView.findViewById(R.id.iv_verified_indicator);
            ivOverflow = (IconTextView) itemView.findViewById(R.id.iv_overflow);
            tvProfileRowLabel = (CustomFontTextView) itemView.findViewById(R.id.tv_profile_row_label);
            tvProfileRowValue = (CustomFontTextView) itemView.findViewById(R.id.tv_profile_row_value);
        }
    }

    public Address getAddressAtPosition(int position) {
        if (addressList != null)
            return addressList.get(position);
        return null;
    }

}