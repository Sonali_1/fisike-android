package appointment.profile;

import java.io.Serializable;

/**
 * Created by neharathore on 16/08/17.
 */

public class FetchUpdateIdentifier implements Serializable {
    private static final long serialVersionUID = -2605784989959625398L;
    private String system;
    private String text;
    private String useCode;
    private String value;

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsecode() {
        return useCode;
    }

    public void setUsecode(String usecode) {
        this.useCode = usecode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
