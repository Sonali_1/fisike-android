package appointment.profile;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import appointment.model.LinkedPatientInfoMO;
import appointment.profile.Adapter.AddressListAdapter;
import appointment.profile.Adapter.ContactEmailAdapter;
import appointment.profile.Adapter.ContactInformationAdapter;
import appointment.profile.Adapter.InsuranceListAdapter;

/**
 * Created by neharathore on 11/08/17.
 */

public class ViewAssociatedPatientActivity extends ViewProfileBaseActivity implements ProfilePictureFragment.setUpdateBitmap, ViewProfileBaseActivity.renderDataInterface {


    Bitmap userProfilePicture;
    private LinkedPatientInfoMO patientDetails;
    String dob, gender, maritalStatus, relationShip;
    private ContactInformationAdapter phoneListAdapter;
    private ContactEmailAdapter emailListAdapter;
    private AddressListAdapter addressListAdapter;
    private InsuranceListAdapter insuranceListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            patientDetails = (LinkedPatientInfoMO) getIntent().getExtras().getSerializable("linkedPatientInfo");
            userProfilePicture = Utils.convertBase64ToBitmap(patientDetails.getProfilePic());
        } catch (Exception e) {
        }
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        userProfilePicture = bitmap;
    }

    @Override
    public Bitmap getBitmap() {
        return userProfilePicture;
    }

    @Override
    public String getDisplayedName() {
        if (patientDetails != null)
            return patientDetails.getName();

        return null;
    }

    @Override
    public void renderPatientDemographics() {

        //setting dob
        if (Utils.isValueAvailable(patientDetails.getDateOfBirth()))
            dob = Utils.getFormattedDateLocale(patientDetails.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime);

        setTextIfAvailable(getLblDob(), getTxtDob(), dob);

        //setting gender
        if (patientDetails.getGender() != null && !patientDetails.getGender().trim().equals("") && GenderEnum.getGenderEnumLinkedHashMap().containsKey(patientDetails.getGender())) {
            gender = GenderEnum.getDisplayedValuefromCode(patientDetails.getGender());
        } else {
            gender = GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_other_key));
        }
        setTextIfAvailable(getLblGender(), getTxtGender(), gender);

/*
        //setting marital status
        String maritalStatusValue = patientDetails.getMaritalStatus();
        maritalStatus = (Utils.isValueAvailable(maritalStatusValue)) ?
                (MaritalStatusEnum.getMaritalStatusMap()!=null &&
                        MaritalStatusEnum.getMaritalStatusMap().containsKey(maritalStatusValue) ?
                        MaritalStatusEnum.getMaritalStatusMap().get(maritalStatusValue).getValue() : "")
                : "";
*/

        setTextIfAvailable(getLblMaritalStatus(), getTxtMaritalStatus(), maritalStatus);

        //setting RelationShip
        setTextIfAvailable(getLblRelationShip(), getTxtRelationShip(), patientDetails.getRelationshipWithUser());
    }

    @Override
    public void renderContactInformation() {
        if (patientDetails != null && patientDetails.getPhoneList() != null && patientDetails.getPhoneList().size() > 0) {
            phoneListAdapter = new ContactInformationAdapter(this, patientDetails.getPhoneList());
            getRvContactList().setLayoutManager(new LinearLayoutManager(this));
            getRvContactList().setAdapter(phoneListAdapter);
        } else {
            //set default view
            setNoDataViewForCard(getCvProfileContactlist(),getString(R.string.no_contact_found),R.drawable.call_icon);
        }
    }

    @Override
    public void renderEmailInformation() {
        if (patientDetails != null && patientDetails.getEmailList() != null && patientDetails.getEmailList().size() > 0) {
            emailListAdapter = new ContactEmailAdapter(this, patientDetails.getEmailList());
            getRvContactEmailList().setLayoutManager(new LinearLayoutManager(this));
            getRvContactEmailList().setAdapter(emailListAdapter);
        } else {
            //set default view
            setNoDataViewForCard(getCvProfileEmailList(),getString(R.string.no_email_found),R.drawable.contact_email);
        }
    }

    @Override
    public void renderAadharInformation() {

        if (!BuildConfig.isAadharEnabled)
            return;
        if (patientDetails != null && Utils.isValueAvailable(patientDetails.getAadharNumber())) {
            getTxtAadharNumber().setText(patientDetails.getAadharNumber());
        } else {
            getAadharOvererflow().setVisibility(View.GONE);
        }

    }

    @Override
    public Bundle initBundleToSendDataForUpdate() {
        return null;
    }

    @Override
    public void renderAddressInformation() {
        if (patientDetails != null && patientDetails.getAddressList() != null && patientDetails.getAddressList().size() > 0) {
            addressListAdapter = new AddressListAdapter(this, userMO.getAddressUserMos());
            getRvAddressList().setLayoutManager(new LinearLayoutManager(this));
            getRvAddressList().setAdapter(addressListAdapter);
        } else {
            setNoDataViewForCard(getCvProfileAddressList(),getString(R.string.no_address_message),R.drawable.address_icon);
        }
    }

    @Override
    public void sendApiToUpdateProfilePicture(Bitmap bitmap) {
        //TODO add profile picture update api
    }

    @Override
    public boolean isViewOnly() {
        return true;
    }

    @Override
    public void renderInsuranceDetails() {
        if(userMO!=null && userMO.getCoverage()!=null && userMO.getCoverage().size()>0)
        {
            insuranceListAdapter = new InsuranceListAdapter(this, userMO.getCoverage(),true);
            getRvInsuranceList().setLayoutManager(new LinearLayoutManager(this));
            getRvInsuranceList().setAdapter(insuranceListAdapter);
        }
        else
        {
            setNoDataViewForCard(getCvProfileInsuranceList(),getString(R.string.no_insurance_added),R.drawable.insurance_icon);
        }
    }

    @Override
    public void setNavigationDrawerBackOrHamburger() {
        setToolbarIcon(false);
    }
}
