package appointment.profile.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by neharathore on 21/08/17.
 */

public class UpdateSelfProfileResponse extends BaseResponse {

    boolean isProfilePictureUpdate;
    public UpdateSelfProfileResponse(JSONObject response, long transactionId,boolean isProfilePictureUpdate) {
        super(response, transactionId);
        parseResponse(response);
        this.isProfilePictureUpdate = isProfilePictureUpdate;
    }

    private void parseResponse(JSONObject response) {
    }

    public UpdateSelfProfileResponse(Exception exception, long transactionid,boolean isProfilePictureUpdate) {
        super(exception, transactionid);
        this.isProfilePictureUpdate=isProfilePictureUpdate;
    }

    public boolean isProfilePictureUpdate() {
        return isProfilePictureUpdate;
    }

    public void setProfilePictureUpdate(boolean profilePictureUpdate) {
        isProfilePictureUpdate = profilePictureUpdate;
    }
}
