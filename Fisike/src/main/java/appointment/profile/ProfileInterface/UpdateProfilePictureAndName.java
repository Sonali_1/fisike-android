package appointment.profile.ProfileInterface;

import android.graphics.Bitmap;

/**
 * Created by neharathore on 22/08/17.
 */

public interface UpdateProfilePictureAndName {


        public void updateUserName(String name);
        public void updateProfilePicture(Bitmap bitmap);

}
