package appointment.profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ProfilePicUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import appointment.profile.Adapter.AddressListAdapter;
import appointment.profile.Adapter.ContactEmailAdapter;
import appointment.profile.Adapter.ContactInformationAdapter;
import appointment.profile.Adapter.InsuranceListAdapter;
import appointment.profile.ProfileInterface.UpdateProfilePictureAndName;
import appointment.profile.request.UserSelfProfileUpdateRequest;
import appointment.profile.response.UpdateSelfProfileResponse;


public class ViewUpdateSelfProfileActivity extends ViewProfileBaseActivity implements ProfilePictureFragment.setUpdateBitmap,
        ViewProfileBaseActivity.renderDataInterface, View.OnClickListener {

    Bitmap userProfilePicture;
    String dob, gender, maritalStatus, relationShip;
    ContactInformationAdapter phoneListAdapter;
    private ContactEmailAdapter emailListAdapter;
    private static final int UPDATE_PROFILE = 501;
    public static final int CHANGE_PASSWORD_REQUEST_CODE = 103;
    public static final int UPDATE_ADD_AADHAR_CARD_NUMBER = 104;
    private Bitmap tempBitmap;
    private AddressListAdapter addressListAdapter;
    private InsuranceListAdapter insuranceListAdapter;
    private boolean isFromProfileList=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.userProfilePicture = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());
        userMO = SettingManager.getInstance().getUserMO();
        super.onCreate(savedInstanceState);
        txtAddorUpdateAadhar.setOnClickListener(this);
        isFromProfileList=getIntent().getBooleanExtra("isFromProfile",false);
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        this.userProfilePicture = bitmap;
    }

    @Override
    public Bitmap getBitmap() {
        return userProfilePicture;
    }

    @Override
    public String getDisplayedName() {
        if (userMO != null) {
            return userMO.getUserFullName();
        }
        return null;
    }

    @Override
    public void sendApiToUpdateProfilePicture(Bitmap bitmap) {
        this.tempBitmap = bitmap;
        if(Utils.showDialogForNoNetwork(this, false)) {
            showProgressDialog();
            ThreadManager.getDefaultExecutorService().submit(new UserSelfProfileUpdateRequest(getTransactionId(), createProfilePicturePayload(), true));
        }

    }

    @Override
    public boolean isViewOnly() {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = (MenuItem) menu.findItem(R.id.menu_change_mobile_number);
        if (!SharedPref.getIsMobileEnabled() && item != null) {
            item.setVisible(false);
        }
        MenuItem signout = (MenuItem) menu.findItem(R.id.menu_sign_out);
        if (BuildConfig.isDrawerSignOut && menu.findItem(R.id.menu_sign_out) != null) {
            signout.setVisible(false);
        }

        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_update_profile:
                ((renderDataInterface) (this)).initBundleToSendDataForUpdate();
                startActivityForResult(new Intent(this, UpdateProfileDemogrphicsActivity.class).putExtra("data", getUpdateDemographicsBundle()), UPDATE_PROFILE);
                break;
            case R.id.menu_change_password:
                Intent i = new Intent(this, ChangeDetails.class);
                i.putExtra(VariableConstants.IS_CHANGE_PASSWORD, true);
                startActivityForResult(i, CHANGE_PASSWORD_REQUEST_CODE);

                break;
            case R.id.menu_change_mobile_number:
//                i = new Intent(this, OtpActivity.class).putExtra(TextConstants.LAUNCH_MODE, TextConstants.CHANGE_NUMBER);
//                startActivity(i);
//                break;

            case R.id.menu_sign_out:
                logoutUser();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logoutUser() {
        DialogUtils.showAlertDialog(this, this.getResources().getString(R.string.confirm_signout), getLogoutMessageConfigBased(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    showProgressDialog();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logout(ViewUpdateSelfProfileActivity.this);
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    public String getLogoutMessageConfigBased() {
        if (BuildConfig.isFisike) {
            return this.getResources().getString(R.string.logout_msg);
        } else
            return this.getResources().getString(R.string.hard_logout_msg);

    }

    protected void logout(Context context) {
        MessengerService mService = ((BaseActivity) context).getmService();
        mService.logoutUser(context);
    }

    @Override
    public void renderPatientDemographics() {

        userMO = SettingManager.getInstance().getUserMO();
        //setting dob
        if (Utils.isValueAvailable(userMO.getDateOfBirth()))
            dob=Utils.getFormattedDateLocale(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime);

        setTextIfAvailable(getLblDob(), getTxtDob(), dob);

        //setting gender
        if (userMO.getGender() != null && !userMO.getGender().trim().equals("") && GenderEnum.getGenderEnumLinkedHashMap().containsKey(userMO.getGender())) {
            gender = GenderEnum.getDisplayedValuefromCode(userMO.getGender());
        } else {
            gender = GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_other_key));
        }
        setTextIfAvailable(getLblGender(), getTxtGender(), gender);

        //setting marital status
        String maritalStatusValue = userMO.getMaritalStatus();
        maritalStatus = (Utils.isValueAvailable(maritalStatusValue)) ?
                (MaritalStatusEnum.getMaritalStatusMap() != null &&
                        MaritalStatusEnum.getMaritalStatusMap().containsKey(maritalStatusValue) ?
                        MaritalStatusEnum.getMaritalStatusMap().get(maritalStatusValue).getValue() : "")
                : "";

        setTextIfAvailable(getLblMaritalStatus(), getTxtMaritalStatus(), maritalStatus);

        //setting RelationShip to gone in case of self
        getLblRelationShip().setVisibility(View.GONE);
        getTxtRelationShip().setVisibility(View.GONE);
    }

    @Override
    public void renderContactInformation() {
        if (userMO != null && userMO.getPhoneList() != null && userMO.getPhoneList().size() > 0) {
            phoneListAdapter = new ContactInformationAdapter(this, userMO.getPhoneList());
            getRvContactList().setLayoutManager(new LinearLayoutManager(this));
            getRvContactList().setNestedScrollingEnabled(false);
            getRvContactList().setAdapter(phoneListAdapter);
        } else {
             setNoDataViewForCard(getCvProfileContactlist(),getString(R.string.no_contact_found),R.drawable.contact_number);
        }
    }

    @Override
    public void renderEmailInformation() {

        if (userMO != null && userMO.getEmailList() != null && userMO.getEmailList().size() > 0) {
            emailListAdapter = new ContactEmailAdapter(this, userMO.getEmailList());
            getRvContactEmailList().setLayoutManager(new LinearLayoutManager(this));
            getRvContactEmailList().setNestedScrollingEnabled(false);
            getRvContactEmailList().setAdapter(emailListAdapter);
        } else {
            setNoDataViewForCard(getCvProfileEmailList(),getString(R.string.no_email_found),R.drawable.contact_email);
        }
    }

    @Override
    public void renderAadharInformation() {
        userMO = SettingManager.getInstance().getUserMO();

        if (!BuildConfig.isAadharEnabled)
            return;

        if (userMO != null && Utils.isValueAvailable(userMO.getAadharNumber())) {
            txtAddorUpdateAadhar.setText(getResources().getString(R.string.update_aadhar));
            getTxtAadharNumber().setText(userMO.getAadharNumber());
        } else {
            txtAddorUpdateAadhar.setText(getResources().getString(R.string.add_aadhar_profile));
            getTxtAadharNumber().setText(getResources().getString(R.string.tell_us_aadhar_number));
            getAadharOvererflow().setVisibility(View.GONE);
        }
    }

    @Override
    public Bundle initBundleToSendDataForUpdate() {
        try {
            Bundle bundle = getUpdateDemographicsBundle();
            if (bundle == null)
                bundle = new Bundle();
            bundle.putString(UpdateProfileDemogrphicsActivity.FIRSTNAME, userMO.getFirstName());
            bundle.putString(UpdateProfileDemogrphicsActivity.MIDDLENAME, userMO.getMiddleName());
            bundle.putString(UpdateProfileDemogrphicsActivity.LASTNAME, userMO.getLastName());
            bundle.putString(UpdateProfileDemogrphicsActivity.BIRTHDATE, userMO.getDateOfBirth());
            bundle.putString(UpdateProfileDemogrphicsActivity.MARITALSTATUS, userMO.getMaritalStatus());
            bundle.putString(UpdateProfileDemogrphicsActivity.RELATIONSHIP, "self");
            bundle.putString(UpdateProfileDemogrphicsActivity.GENDER, userMO.getGender());
            bundle.putString(UpdateProfileDemogrphicsActivity.PROFILE_PICTURE, Utils.convertBitmapToBase64(userProfilePicture));
            setUpdateDemographicsBundle(bundle);
            return bundle;
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void renderAddressInformation() {
        if (userMO != null && userMO.getAddressUserMos() != null && userMO.getAddressUserMos().size() > 0) {
            addressListAdapter = new AddressListAdapter(this, userMO.getAddressUserMos());
            getRvAddressList().setLayoutManager(new LinearLayoutManager(this));
            getRvAddressList().setNestedScrollingEnabled(false);
            getRvAddressList().setAdapter(addressListAdapter);
        } else {
            setNoDataViewForCard(getCvProfileAddressList(),getString(R.string.no_address_message),R.drawable.address_icon);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHANGE_PASSWORD_REQUEST_CODE:
                    // show toast for password successfully updated
                    Toast.makeText(this, R.string.password_changed_successfully,
                            Toast.LENGTH_LONG).show();
                    break;
                case UPDATE_PROFILE:
                    //render demographics details
                    renderPatientDemographics();
                    //render patient full name and profile picture
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_profile_picture);
                    if (fragment != null && fragment instanceof ProfilePictureFragment) {
                        ((UpdateProfilePictureAndName) (fragment)).updateUserName(userMO.getUserFullName());
                        ((UpdateProfilePictureAndName) (fragment)).updateProfilePicture(UserProfilePic.loadImageFromStorage(this));
                    }
                    if (data.getBooleanExtra(TextConstants.SUCCESS_MESSAGE, false))
                        Toast.makeText(this, getString(R.string.profile_update_success), Toast.LENGTH_SHORT).show();

                    break;
                case UPDATE_ADD_AADHAR_CARD_NUMBER:
                    renderAadharInformation();
                    Toast.makeText(this, getString(R.string.aadhar_number_added_successfully), Toast.LENGTH_LONG).show();

            }
        }
    }

    private String createProfilePicturePayload() {
        try {
            JSONObject payload = new JSONObject();
            if (tempBitmap != null) {
                payload.put("profilePicture", Utils.convertBitmapToBase64(tempBitmap));
                payload.put("profilePictureMimeType", "image/jpg");
            } else {
                payload.put("profilePicture", "");
                payload.put("profilePictureMimeType", "image/jpg");
                return payload.toString();
            }
        } catch (Exception e) {

        }

        return "";
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch(view.getId()){
            case R.id.btn_add_aadhar:
                Intent updateoraddAadhar = new Intent(this, UpdateAadharActivity.class);
                startActivityForResult(updateoraddAadhar, UPDATE_ADD_AADHAR_CARD_NUMBER);
        }
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateSelfProfileResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful() && Utils.isValueAvailable(response.getInfoResponseStatus()) &&
                response.getInfoResponseStatus().equalsIgnoreCase(TextConstants.STATUS_200)) {

            if (response.isProfilePictureUpdate()) {
                UserProfilePic.saveToInternalStorage(tempBitmap, this);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_profile_picture);
                if (fragment != null && fragment instanceof ProfilePictureFragment) {
                    ((UpdateProfilePictureAndName) (fragment)).updateProfilePicture(UserProfilePic.loadImageFromStorage(this));
                }
            }

        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void renderInsuranceDetails() {
        if(userMO!=null && userMO.getCoverage()!=null && userMO.getCoverage().size()>0)
        {
            insuranceListAdapter = new InsuranceListAdapter(this, userMO.getCoverage(),true);
            getRvInsuranceList().setLayoutManager(new LinearLayoutManager(this));
            getRvInsuranceList().setAdapter(insuranceListAdapter);
        }
        else
        {
            setNoDataViewForCard(getCvProfileInsuranceList(),getString(R.string.no_insurance_added),R.drawable.insurance_icon);
        }
    }

    @Override
    public void setNavigationDrawerBackOrHamburger() {
        if(isFromProfileList)
            setToolbarIcon(false);
        else
            setToolbarIcon(true);
    }

}
