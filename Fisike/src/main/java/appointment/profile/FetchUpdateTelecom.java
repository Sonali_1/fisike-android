package appointment.profile;

import java.io.Serializable;

/**
 * Created by neharathore on 16/08/17.
 */

public class FetchUpdateTelecom implements Serializable {
    private static final long serialVersionUID = 6148057198317907466L;
    String countryCode;
    boolean primary;
    String system;
    String useCode;
    String value;
    boolean verified;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
