package appointment.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;


public class ViewProfileBaseActivity extends BaseActivity {

    private FrameLayout frameLayout, frameProfilePicture;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private ImageButton buttonBack;
    private IconTextView buttonright, aadharOvererflow;
    private Context mContext;
    private CustomFontTextView lblGender, txtGender, lblDob, txtDob, lblMaritalStatus,
            txtMaritalStatus, lblRelationShip, txtRelationShip, txtAadharNumber;
    public CustomFontTextView txtAddorUpdateAadhar;

    private CardView cvProfileDemographics, cvProfileContactlist, cvProfileEmailList,
            cvProfileAddressList, cvProfileInsuranceList, cvProfileAadhar,
            cvProfileEmergencyContactList, cvProfileCareProvidersList;

    private RecyclerView rvContactList, rvContactEmailList, rvAddressList, rvInsuranceList;
    private Bundle updateDemographicsBundle;
    private boolean isHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_view_profile_base, frameLayout);
        mContext = this;
        findViews();
        initToolbar();
        addPatientDemographicsFragment();
        ((renderDataInterface) this).renderPatientDemographics();
        ((renderDataInterface) this).renderContactInformation();
        ((renderDataInterface) this).renderEmailInformation();
        ((renderDataInterface) this).renderAadharInformation();
        ((renderDataInterface) this).initBundleToSendDataForUpdate();
        ((renderDataInterface) this).renderAddressInformation();
        ((renderDataInterface) this).renderInsuranceDetails();
    }

    private void findViews() {
        findProfileCards();
        frameProfilePicture = (FrameLayout) findViewById(R.id.frame_profile_picture);
        lblGender = (CustomFontTextView) findViewById(R.id.lbl_gender);
        txtGender = (CustomFontTextView) findViewById(R.id.txt_gender);
        lblDob = (CustomFontTextView) findViewById(R.id.lbl_dob);
        txtDob = (CustomFontTextView) findViewById(R.id.txt_dob);
        lblMaritalStatus = (CustomFontTextView) findViewById(R.id.lbl_marital_status);
        txtMaritalStatus = (CustomFontTextView) findViewById(R.id.txt_marital_status);
        lblRelationShip = (CustomFontTextView) findViewById(R.id.lbl_relationship);
        txtRelationShip = (CustomFontTextView) findViewById(R.id.txt_relationship);
        txtAddorUpdateAadhar = (CustomFontTextView) findViewById(R.id.btn_add_aadhar);

    }

    private void findProfileCards() {
        cvProfileDemographics = (CardView) findViewById(R.id.card_patient_demographics);
        cvProfileContactlist = (CardView) findViewById(R.id.card_profile_contact_list);
        cvProfileEmailList = (CardView) findViewById(R.id.card_profile_email_list);
        cvProfileAddressList = (CardView) findViewById(R.id.card_profile_address_list);
        cvProfileInsuranceList = (CardView) findViewById(R.id.card_profile_insurance_list);
        cvProfileAadhar = (CardView) findViewById(R.id.card_profile_aadhar_number);
        cvProfileEmergencyContactList = (CardView) findViewById(R.id.card_profile_emergency_contact_list);
        cvProfileCareProvidersList = (CardView) findViewById(R.id.card_profile_care_providers_list);
        rvContactList = (RecyclerView) findViewById(R.id.rv_contact_list);
        rvContactEmailList = (RecyclerView) findViewById(R.id.rv_email_list);
        txtAadharNumber = (CustomFontTextView) cvProfileAadhar.findViewById(R.id.txt_aadhar);
        aadharOvererflow = (IconTextView) cvProfileAadhar.findViewById(R.id.aadhar_overflow);
        rvAddressList = (RecyclerView) cvProfileAddressList.findViewById(R.id.rv_address_list);
        rvInsuranceList = (RecyclerView) cvProfileInsuranceList.findViewById(R.id.rv_insurance_list);

        handleVisibilityofViewsBasedOnConfigs(BuildConfig.isToShowInsuranceCard, cvProfileInsuranceList);
        handleVisibilityofViewsBasedOnConfigs(BuildConfig.isAadharEnabled, cvProfileAadhar);
        handleVisibilityofViewsBasedOnConfigs(BuildConfig.isToShowCareProviderCard, cvProfileCareProvidersList);
        handleVisibilityofViewsBasedOnConfigs(BuildConfig.isToShowEmergencyContactCard, cvProfileEmergencyContactList);
    }

    public void setToolbarIcon(final boolean isHamburger) {
        this.isHamburger=isHamburger;
        if (isHamburger) {
            buttonBack.setImageDrawable(getResources().getDrawable(R.drawable.hamburger));
        } else {
            buttonBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        }

    }

    private void handleVisibilityofViewsBasedOnConfigs(boolean visibility, View view) {
        if (visibility) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    private void addPatientDemographicsFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle(1);
        bundle.putBoolean(ProfilePictureFragment.ISUPDATEALLOWED, true);
        ProfilePictureFragment profilePictureFragment = new ProfilePictureFragment();
        profilePictureFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.frame_profile_picture, profilePictureFragment);
        fragmentTransaction.addToBackStack(ProfilePictureFragment.class.getName());
        fragmentTransaction.commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_profile_picture);
        if (fragment != null && fragment instanceof ProfilePictureFragment)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    private void initToolbar() {
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.profile));
        buttonBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        buttonright = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        buttonBack.setVisibility(View.VISIBLE);
        buttonright.setVisibility(View.GONE);
        showHideToolbar(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((renderDataInterface)this).setNavigationDrawerBackOrHamburger();
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewProfileBaseActivity.this.isHamburger) {
                    openDrawer();
                } else {
                    ViewProfileBaseActivity.this.finish();
                }
            }
        });


    }

    public void setTextIfAvailable(CustomFontTextView tvLabel, CustomFontTextView tvValue, String value) {
        if (Utils.isValueAvailable(value)) {
            tvValue.setText(value);
            tvLabel.setVisibility(View.VISIBLE);
            tvValue.setVisibility(View.VISIBLE);
        } else {
            tvLabel.setVisibility(View.GONE);
            tvValue.setVisibility(View.GONE);
        }
    }

    public CustomFontTextView getLblGender() {
        return lblGender;
    }

    public CustomFontTextView getTxtGender() {
        return txtGender;
    }

    public CustomFontTextView getLblDob() {
        return lblDob;
    }

    public CustomFontTextView getTxtDob() {
        return txtDob;
    }

    public CustomFontTextView getLblMaritalStatus() {
        return lblMaritalStatus;
    }

    public CustomFontTextView getTxtMaritalStatus() {
        return txtMaritalStatus;
    }

    public CustomFontTextView getLblRelationShip() {
        return lblRelationShip;
    }

    public CustomFontTextView getTxtRelationShip() {
        return txtRelationShip;
    }

    public RecyclerView getRvContactList() {
        return rvContactList;
    }

    public RecyclerView getRvContactEmailList() {
        return rvContactEmailList;
    }

    interface renderDataInterface {
        public void renderPatientDemographics();

        public void renderContactInformation();

        public void renderEmailInformation();

        public void renderAadharInformation();

        public Bundle initBundleToSendDataForUpdate();

        public void renderAddressInformation();

        public void renderInsuranceDetails();

        public void setNavigationDrawerBackOrHamburger();
    }

    public IconTextView getAadharOvererflow() {
        return aadharOvererflow;
    }

    public CustomFontTextView getTxtAadharNumber() {
        return txtAadharNumber;
    }

    public Bundle getUpdateDemographicsBundle() {
        return updateDemographicsBundle;
    }

    public void setUpdateDemographicsBundle(Bundle bundle) {
        updateDemographicsBundle = bundle;
    }

    public RecyclerView getRvAddressList() {
        return rvAddressList;
    }

    //default_profile_layout should be included in the particular card
    public void setNoDataViewForCard(CardView card, String message, int iconId) {
 /*       card.findViewById(R.id.ll_defaultview).setVisibility(View.VISIBLE);
        ((CustomFontTextView)card.findViewById(R.id.ll_defaultview).findViewById(R.id.default_view_message)).setText(message);
        ((ImageView) card.findViewById(R.id.ll_defaultview).findViewById(R.id.iv_default_icon)).setBackgroundDrawable(getResources().getDrawable(iconId));
*/
        card.setVisibility(View.GONE);
    }

    public CardView getCvProfileContactlist() {
        return cvProfileContactlist;
    }

    public CardView getCvProfileEmailList() {
        return cvProfileEmailList;
    }

    public CardView getCvProfileAddressList() {
        return cvProfileAddressList;
    }

    public CardView getCvProfileInsuranceList() {
        return cvProfileInsuranceList;
    }

    public CardView getCvProfileAadhar() {
        return cvProfileAadhar;
    }

    public CardView getCvProfileEmergencyContactList() {
        return cvProfileEmergencyContactList;
    }

    public CardView getCvProfileCareProvidersList() {
        return cvProfileCareProvidersList;
    }

    public RecyclerView getRvInsuranceList() {
        return rvInsuranceList;
    }
}

