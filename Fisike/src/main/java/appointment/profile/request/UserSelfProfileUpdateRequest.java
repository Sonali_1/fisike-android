package appointment.profile.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import appointment.profile.response.UpdateSelfProfileResponse;

/**
 * Created by neharathore on 21/08/17.
 */

public class UserSelfProfileUpdateRequest extends BaseObjectRequest {

    long transactionId;
    String payload;
    boolean isProfilePictureUpdate=false;

    public UserSelfProfileUpdateRequest(long mTransactionId, String payload) {
        this.transactionId = mTransactionId;
        this.payload = payload;
    }

    public UserSelfProfileUpdateRequest(long mTransactionId, String payload,boolean isProfilePicture) {
        this.transactionId = mTransactionId;
        this.payload = payload;
        this.isProfilePictureUpdate=isProfilePicture;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getUpdateProfileRequest();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, payload, this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new UpdateSelfProfileResponse(error, transactionId,isProfilePictureUpdate));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new UpdateSelfProfileResponse(response, transactionId,isProfilePictureUpdate));
    }
}
