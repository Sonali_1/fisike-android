package appointment.profile;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import appointment.profile.request.UserSelfProfileUpdateRequest;
import appointment.profile.response.UpdateSelfProfileResponse;

public class UpdateAadharActivity extends BaseActivity implements OnClickListener {
    private FrameLayout frameLayout;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private ImageButton buttonBack;
    private IconTextView buttonright;
    private CustomFontEditTextView etEnterAadharNumber, etConfirmAadharNumber;
    private CustomFontButton btChangeAadharNumber;
    private String newAadharNumber, confirmedAadharNumber;
    ArrayList<FetchUpdateIdentifier> identifierArrayList;
    private UserMO userMO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_update_aadhar, frameLayout);
        findViews();
        initToolbar();
        userMO = SettingManager.getInstance().getUserMO();
    }

    private void findViews() {
        etEnterAadharNumber = (CustomFontEditTextView) findViewById(R.id.et_new_aadhar_card_number);
        etConfirmAadharNumber = (CustomFontEditTextView) findViewById(R.id.et_confirm_aadhar_card_number);
        btChangeAadharNumber = (CustomFontButton) findViewById(R.id.btn_change_aadhar_submit);
        btChangeAadharNumber.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_change_aadhar_submit:
                // update aadhar
                if (isDataValid() && Utils.showDialogForNoNetwork(this, false)) {
                    // api call flow
                    try {
                        showProgressDialog();
                        ThreadManager.getDefaultExecutorService().
                                submit(new UserSelfProfileUpdateRequest(getTransactionId(),
                                        createUpdateAadharPayload()));
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    setError();
                }
                break;
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.confirm_aadhar_number));
        buttonBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        buttonright = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        buttonBack.setVisibility(View.GONE);
        buttonright.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        showHideToolbar(false);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateAadharActivity.this.finish();
            }
        });
    }

    private boolean isDataValid() {
        newAadharNumber = etEnterAadharNumber.getText().toString().trim();
        confirmedAadharNumber = etConfirmAadharNumber.getText().toString().trim();
        if (Utils.isValueAvailable(newAadharNumber) && Utils.isValueAvailable(confirmedAadharNumber)) {
            if (newAadharNumber.length() == 10 && newAadharNumber.equals(confirmedAadharNumber)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void setError() {
        checkEditText(newAadharNumber, etEnterAadharNumber);
        checkEditText(confirmedAadharNumber, etConfirmAadharNumber);
        if (!newAadharNumber.equals(confirmedAadharNumber)) {
            // show toast
            Toast.makeText(this, getResources().getString(R.string.error_aadhar_number_not_matching), Toast.LENGTH_LONG).show();
        }
    }

    private void checkEditText(String value, CustomFontEditTextView editText) {
        if (value.length() != 10) {
            editText.setError(getResources().getString(R.string.enter_valid_aadhar_number));
        }
    }

    private String createUpdateAadharPayload() throws JSONException{
        JSONArray jsonArray = new JSONArray();
        JSONObject payload = new JSONObject();
        try {
            if (userMO != null && userMO.getIdentifierArrayList() != null && userMO.getIdentifierArrayList().size() > 0) {
                jsonArray = createUpdateAadharCardPayLoad(jsonArray);
            } else {
                createIdentifierList();
                jsonArray.put(createidentifierJsonObject("AADHAR", "aadharNumber", "usual",
                        etEnterAadharNumber.getText().toString().trim()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payload.put("identifier", jsonArray).toString();
    }

    private JSONArray createUpdateAadharCardPayLoad(JSONArray jsonArray) throws JSONException {
         identifierArrayList = userMO.getIdentifierArrayList();
        boolean aadharCardUpdate = false;
        for (int i = 0; i < identifierArrayList.size(); i++) {
            FetchUpdateIdentifier identifier = identifierArrayList.get(i);
            if (Utils.isValueAvailable(identifier.getText()) && identifier.getText().equalsIgnoreCase(MyApplication.getAppContext().
                                                                                     getString(R.string.aadhar_key))) {
                // if aadhar card is present in list already update it
                aadharCardUpdate = true;
                identifierArrayList.get(i).setValue(etConfirmAadharNumber.getText().toLowerCase().trim());
                jsonArray.put(createidentifierJsonObject("AADHAR", "aadharNumber", "usual",
                                                          etEnterAadharNumber.getText().toString().trim()));
            } else {
                // send the list as it is.
                jsonArray.put(createidentifierJsonObject(identifier.getSystem(),
                                                         identifier.getText(),
                                                         identifier.getUsecode(),
                                                         identifier.getValue()));
            }
            }
            // aadhar card is not present  create a new entry
            if(!aadharCardUpdate && userMO != null && !Utils.isValueAvailable(userMO.getAadharNumber())){
                createIdentifierList();
                jsonArray.put(createidentifierJsonObject("AADHAR", "aadharNumber", "usual",
                                                          etEnterAadharNumber.getText().toString().trim()));
            }

        return jsonArray;
    }

    private void createIdentifierList(){
        if(identifierArrayList == null) {
            identifierArrayList = new ArrayList<FetchUpdateIdentifier>();
        }
        FetchUpdateIdentifier identifier = new FetchUpdateIdentifier();
        identifier.setSystem("AADHAR");
        identifier.setText("aadharNumber");
        identifier.setUsecode("usual");
        identifier.setValue(etConfirmAadharNumber.getText().toString());
        identifierArrayList.add(identifier);

    }

    private JSONObject createidentifierJsonObject(String system, String text, String useCode, String value) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("system", system);
        jsonObject.put("text", text);
        jsonObject.put("useCode", useCode);
        jsonObject.put("value", value);
        return jsonObject;
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateSelfProfileResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;
        dismissProgressDialog();

        if (response.isSuccessful() && Utils.isValueAvailable(response.getInfoResponseStatus()) &&
                response.getInfoResponseStatus().equalsIgnoreCase(TextConstants.STATUS_200)) {
            updateDataInUserMo(identifierArrayList);
            finishActivityWithSucess();
        }
    }

    private void finishActivityWithSucess() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        UpdateAadharActivity.this.finish();
    }

    private void updateDataInUserMo(ArrayList<FetchUpdateIdentifier> identifier) {
        try {
            ContentValues value = new ContentValues();
            UserDBAdapter adapter = UserDBAdapter.getInstance(this);
            adapter.updateIdentifier(identifier, userMO.getPersistenceKey());
        } catch (Exception ec) {
            ec.printStackTrace();
        }
    }
}
