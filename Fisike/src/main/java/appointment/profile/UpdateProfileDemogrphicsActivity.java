package appointment.profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.R;

import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.enums.RelationShipEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.provider.ChatIQ;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.CheckEmailExistenceResponse;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ThreadPoolExecutor;

import appointment.profile.ProfileInterface.UpdateProfilePictureAndName;
import appointment.profile.request.UserSelfProfileUpdateRequest;
import appointment.profile.response.UpdateSelfProfileResponse;

public class UpdateProfileDemogrphicsActivity extends BaseActivity implements ProfilePictureFragment.setUpdateBitmap,
        View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private Toolbar toolbar;
    private TextView toolbar_title;
    private ImageButton buttonBack;
    private IconTextView buttonright;
    private CustomFontEditTextView etFirstName, etMiddleName, etLastName, etDOB,
            spinnerMaritalStatus, spinnerRelationShip, spinnerGender;
    private String fName, mName, lName, dob, maritalSatus, relationShip, gender, apiDob;
    public static final String FIRSTNAME = "FIRSTNAME", MIDDLENAME = "MIDDLENAME", LASTNAME = "LASTNAME",
            BIRTHDATE = "BIRTHDATE", RELATIONSHIP = "RELATIONSHIP", MARITALSTATUS = "MARITALSTATUS", GENDER = "GENDER", PROFILE_PICTURE = "PROFILE_PICTURE";
    private Bitmap userProfilePicture;
    CustomFontButton btnUpdate;
    private DatePickerDialog dobDialog;
    private Bitmap tempBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile_demogrphics);
        addPatientProfilePictureFragment();
        initToolbar();
        findViews();
        initDataFromBundle();
        setData();
    }


    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.update_profile));
        buttonBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        buttonright = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        buttonBack.setVisibility(View.GONE);
        buttonright.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        showHideToolbar(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivityWithSucess(false);
            }
        });
    }


    private void findViews() {
        etFirstName = (CustomFontEditTextView) findViewById(R.id.et_first_name);
        etMiddleName = (CustomFontEditTextView) findViewById(R.id.et_middle_name);
        etLastName = (CustomFontEditTextView) findViewById(R.id.et_last_name);
        etDOB = (CustomFontEditTextView) findViewById(R.id.et_date_of_birth);
        spinnerRelationShip = (CustomFontEditTextView) findViewById(R.id.spinner_relationship);
        spinnerMaritalStatus = (CustomFontEditTextView) findViewById(R.id.spinner_marital_status);
        spinnerGender = (CustomFontEditTextView) findViewById(R.id.spinner_gender);
        btnUpdate = (CustomFontButton) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);


        if (Utils.isRTL(this)) {
            etDOB.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_profile, 0, 0, 0);
            spinnerRelationShip.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_drop_down, 0, 0, 0);
            spinnerMaritalStatus.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_drop_down, 0, 0, 0);
            spinnerGender.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_drop_down, 0, 0, 0);
        } else {
            etDOB.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.calendar_profile, 0);
            spinnerRelationShip.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0);
            spinnerMaritalStatus.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0);
            spinnerGender.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0);
        }
        etDOB.setInputType(InputType.TYPE_NULL);
        spinnerRelationShip.setInputType(InputType.TYPE_NULL);
        spinnerMaritalStatus.setInputType(InputType.TYPE_NULL);
        spinnerGender.setInputType(InputType.TYPE_NULL);

    }

    private void initDataFromBundle() {
        try {
            Bundle bundle = getIntent().getBundleExtra("data");
            fName = bundle.getString(FIRSTNAME, "");
            mName = bundle.getString(MIDDLENAME, "");
            lName = bundle.getString(LASTNAME, "");
            dob = bundle.getString(BIRTHDATE, "");
            relationShip = bundle.getString(RELATIONSHIP, "");
            maritalSatus = MaritalStatusEnum.getDisplayedValuefromCode(bundle.getString(MARITALSTATUS, ""));
            gender = bundle.getString(GENDER, "");
            userProfilePicture = Utils.convertBase64ToBitmap(bundle.getString(PROFILE_PICTURE, null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initAndSetDatePicker() {
        Calendar minDate = Calendar.getInstance();
        minDate.add(Calendar.YEAR, -100);
        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.YEAR, -18);
        maxDate.set(Calendar.HOUR_OF_DAY,23);
        maxDate.set(Calendar.MINUTE,59);
        maxDate.set(Calendar.SECOND,59);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            if (Utils.isValueAvailable(etDOB.getText())) {
                calendar.setTime(sdf.parse(etDOB.getText()));
            } else {
                calendar.setTime(sdf.parse(new Date().toString()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);


        dobDialog = new DatePickerDialog(this, this, year, month, day);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            LinearLayout layout = new LinearLayout(this);
            layout.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            dobDialog.setCustomTitle(layout);
        }
        dobDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        dobDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());


        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                dobDialog.show();
            }
        });


        etDOB.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                    dobDialog.show();
                }
            }
        });
    }

    private void setData() {
        createGenderList();
        createMaritalStatusList();
        createRelationshipList();
        initializeData();
        initAndSetDatePicker();
    }

    private void initializeData() {
        setValueifAvailable(etFirstName, fName);
        setValueifAvailable(etMiddleName, mName);
        setValueifAvailable(etLastName, lName);
        dob = (Utils.getFormattedDate(dob, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated,
                DateTimeUtil.destinationDateFormatWithoutTime));
        setValueifAvailable(etDOB, dob);
        setValueifAvailable(spinnerGender, gender);
        setValueifAvailable(spinnerMaritalStatus, GenderEnum.getDisplayedValuefromCode(maritalSatus));
        setValueifAvailable(spinnerRelationShip, relationShip);

    }

    private void setValueifAvailable(CustomFontEditTextView etView, String value) {
        if (Utils.isValueAvailable(value))
            etView.setText(value);
    }

    private void setValueifAvailable(CustomFontTextView etView, String value) {
        if (Utils.isValueAvailable(value))
            etView.setText(value);
    }

    private void addPatientProfilePictureFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_profile_picture, new ProfilePictureFragment());
        fragmentTransaction.addToBackStack(ProfilePictureFragment.class.getName());
        fragmentTransaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_profile_picture);
        if (fragment != null && fragment instanceof ProfilePictureFragment)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        this.userProfilePicture = bitmap;
    }

    @Override
    public Bitmap getBitmap() {
        return userProfilePicture;
    }

    @Override
    public String getDisplayedName() {
        return null;
    }

    @Override
    public void sendApiToUpdateProfilePicture(Bitmap bitmap) {
        this.tempBitmap = bitmap;
        if (Utils.showDialogForNoNetwork(this, false)) {
            showProgressDialog();
            ThreadManager.getDefaultExecutorService().submit(new UserSelfProfileUpdateRequest(getTransactionId(),
                    createProfilePicturePayload(), true));
        }
    }

    @Override
    public boolean isViewOnly() {
        return false;
    }

    private String createProfilePicturePayload() {

        try {
            JSONObject payload = new JSONObject();
            if (tempBitmap != null) {
                payload.put("profilePicture", Utils.convertBitmapToBase64(tempBitmap));
            } else
                payload.put("profilePicture", "");
            payload.put("profilePictureMimeType", "image/jpg");

            return payload.toString();
        } catch (Exception e) {

        }

        return "";
    }

    private void createGenderList() {
        spinnerGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(R.menu.menu_gender, spinnerGender, spinnerGender);
            }
        });


        spinnerGender.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                    showPopupMenu(R.menu.menu_gender, spinnerGender, spinnerGender);
                }
            }
        });
    }

    private void createRelationshipList() {

        spinnerRelationShip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(R.menu.menu_relationship, spinnerRelationShip, spinnerRelationShip);
            }
        });

        spinnerRelationShip.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                    showPopupMenu(R.menu.menu_relationship, spinnerRelationShip, spinnerRelationShip);
                }
            }
        });
    }

    private void createMaritalStatusList() {
        spinnerMaritalStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(R.menu.menu_marital_status, spinnerMaritalStatus, spinnerMaritalStatus);
            }
        });

        spinnerMaritalStatus.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                    showPopupMenu(R.menu.menu_marital_status, spinnerMaritalStatus, spinnerMaritalStatus);
                }
            }
        });

    }

    public void showPopupMenu(final int menuId, View anchorView, final CustomFontEditTextView txtView) {
        final PopupMenu popup = new PopupMenu(UpdateProfileDemogrphicsActivity.this, anchorView);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.setText((String) item.getTitle());
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnUpdate:
                setParameters();
                if (isValidValuesEntered() && Utils.showDialogForNoNetwork(this, false)) {
                    showProgressDialog();
                    ThreadManager.getDefaultExecutorService().submit(new UserSelfProfileUpdateRequest(getTransactionId(), createPayload()));
                }
                break;
            case R.id.et_date_of_birth:
                Utils.hideKeyboard(UpdateProfileDemogrphicsActivity.this);
                dobDialog.show();
        }
    }

    private void setParameters() {
        fName = etFirstName.getText();
        mName = etMiddleName.getText();
        lName = etLastName.getText();
        dob = etDOB.getText();
        gender = spinnerGender.getText().toString().trim();
        maritalSatus = spinnerMaritalStatus.getText().toString().trim();
        relationShip = spinnerRelationShip.getText().toString().trim();
    }

    private boolean isValidValuesEntered() {
        if (!Utils.isValueAvailable(fName)) {
            etFirstName.setError(getString(R.string.please_enter_first_name));
            return false;
        } else if (Utils.isValueAvailable(gender) && gender.equalsIgnoreCase(getString(R.string.gender))) {
            spinnerGender.setError(getString(R.string.please_enter_gender));
            return false;
        } else if (!Utils.isValueAvailable(dob)) {
            etDOB.setError(getString(R.string.please_enter_dob));
            return false;
        }

        return true;
    }

    private void showError(String errorMsg) {
        Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
    }

    private String createPayload() {
        try {

            JSONObject payload = new JSONObject();
            payload.put("firstName", fName);
            payload.put("middleName", mName);
            payload.put("lastName", lName);
            if (Utils.isValueAvailable(gender) && !gender.equalsIgnoreCase(getString(R.string.gender)))
                payload.put("gender", GenderEnum.getCodeFromValue(gender).substring(0, 1));
            if (Utils.isValueAvailable(relationShip) && !relationShip.equalsIgnoreCase(getString(R.string.relationship)))
                payload.put("relationshipWithUser", RelationShipEnum.getCodeFromValue(relationShip));
            if (Utils.isValueAvailable(maritalSatus) && !maritalSatus.equalsIgnoreCase(getString(R.string.marital_status)))
                payload.put("maritalStatus", MaritalStatusEnum.getCodeFromValue(maritalSatus));


            if (Utils.isValueAvailable(dob)) {
                apiDob = DateTimeUtil.convertSourceLocaleToDateEnglish(dob, DateTimeUtil
                        .destinationDateFormatWithoutTime, DateTimeUtil
                        .yyyy_MM_dd_T_HH_mm_ss_z_appended);

                payload.put("dob", apiDob);
            }

            return payload.toString();
        } catch (Exception e) {

        }

        return "";
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateSelfProfileResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful() && Utils.isValueAvailable(response.getInfoResponseStatus()) &&
                response.getInfoResponseStatus().equalsIgnoreCase(TextConstants.STATUS_200)) {

            if (response.isProfilePictureUpdate()) {
                UserProfilePic.saveToInternalStorage(tempBitmap, this);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_profile_picture);
                if (fragment != null && fragment instanceof ProfilePictureFragment) {
                    ((UpdateProfilePictureAndName) (fragment)).updateProfilePicture(UserProfilePic.loadImageFromStorage(this));
                }
            } else {
                dob = DateTimeUtil.convertSourceDestinationDate(apiDob, DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_z_appended, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated);
                if (relationShip.equalsIgnoreCase(getString(R.string.relationship_self_key)))
                    updateDataInUserMo();
                finishActivityWithSucess(true);
            }

        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void updateDataInUserMo() {
        ContentValues value = new ContentValues();
        UserDBAdapter adapter = UserDBAdapter.getInstance(this);
        value.put(adapter.getFIRSTNAME(), fName);
        value.put(adapter.getLASTNAME(), lName);
        value.put(adapter.getMIDDLENAME(), mName);
        value.put(adapter.getDATEOFBIRTH(), dob);
        if (Utils.isValueAvailable(maritalSatus))
            value.put(adapter.getMARITALSTATUS(), MaritalStatusEnum.getCodeFromValue(maritalSatus));

        value.put(adapter.getGENDER(), gender);
        adapter.updateUserData(userMO.getPersistenceKey(), value);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        etDOB.setText(DateTimeUtil.getFormattedDateWithoutUTCLocale(((monthOfYear + 1) + "-" +
                        dayOfMonth + "-" + year),
                DateTimeUtil.mm_dd_yyyy_HiphenSeperated));

    }

    private void finishActivityWithSucess(boolean isToShowSuccessMessage) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(TextConstants.SUCCESS_MESSAGE, isToShowSuccessMessage);
        setResult(Activity.RESULT_OK, returnIntent);
        UpdateProfileDemogrphicsActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        finishActivityWithSucess(false);
    }
}

