package appointment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import appointment.phlooba.fragment.HomeDrawListingFragment;

/**
 * Created by laxmansingh on 3/1/2016.
 */
public class MyOrdersActivity extends BaseActivity implements View.OnClickListener {


    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    private Fragment myOrderFragment;

    public static void newInstance(Context context) {
        Intent intent = new Intent(context, MyOrdersActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.homesamplecollectionactivity, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);

        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.txt_my_appointments));

        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));

        myOrderFragment = new HomeDrawListingFragment();
        myOrderFragment.setArguments(new Bundle());
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, myOrderFragment, "myorderfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        myOrderFragment.onActivityResult(requestCode, resultCode, data);
    }
}
