package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

import appointment.model.appointmentcommonmodel.MoPractitionerInfoList;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 2/6/2017.
 */

public class SpecialityDoctorAdapter extends RecyclerView.Adapter<SpecialityDoctorAdapter.SpecialityDoctorViewHolder> {
    private Context mContext;
    private List<PractitionerForAppointment> list;
    private List<ServiceGroup> listServiceGroup;
    private String from;
    private SpecialityDoctorOnClick specialityDoctorOnClick;
    private String selectedSpeciality;


    public interface SpecialityDoctorOnClick {
        public void onSpecialityDoctorClick(int adapterPosition);
    }


    public SpecialityDoctorAdapter(Context context, List<PractitionerForAppointment> practitionerList, String from, SpecialityDoctorOnClick specialityDoctorOnClick, String selectedSpeciality) {
        this.list = practitionerList;
        mContext = context;
        this.from = from;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityDoctorOnClick = specialityDoctorOnClick;
    }

    public SpecialityDoctorAdapter(List<ServiceGroup> listServiceGroup, Context context, String from, SpecialityDoctorOnClick specialityDoctorOnClick, String selectedSpeciality) {
        this.listServiceGroup = listServiceGroup;
        mContext = context;
        this.from = from;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityDoctorOnClick = specialityDoctorOnClick;
    }

    @Override
    public SpecialityDoctorAdapter.SpecialityDoctorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.practioner_list_item, null);
        SpecialityDoctorAdapter.SpecialityDoctorViewHolder viewHolder = new SpecialityDoctorAdapter.SpecialityDoctorViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialityDoctorAdapter.SpecialityDoctorViewHolder practitionerViewHolder, int position) {

        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            PractitionerForAppointment practitioner = (PractitionerForAppointment) list.get(position);
            String practitionerName = practitioner.getPractitionerName();

            practitionerViewHolder.name.setText(practitionerName);


            if (practitioner.getSpecialty() != null) {
                practitionerViewHolder.speciality.setText(practitioner.getSpecialty());
            } else {
                practitionerViewHolder.speciality.setText("");
            }
            //  practitionerViewHolder.price.setText(practitioner.getTestCost());
            new CommonControls().setProfilePic(mContext, practitionerViewHolder.practitionerPic, practitioner.getPractionerPic());

        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            MoPractitionerInfoList moPractitionerInfoList = (MoPractitionerInfoList) listServiceGroup.get(0).getMoPractitionerInfoList().get(position);
            String practitionerName = moPractitionerInfoList.getPractitionerName();

            practitionerViewHolder.name.setText(practitionerName);

            if (moPractitionerInfoList.getSpecialty() != null) {
                practitionerViewHolder.speciality.setText(moPractitionerInfoList.getSpecialty());
            } else {
                practitionerViewHolder.speciality.setText("");
            }

            new CommonControls().setProfilePic(mContext, practitionerViewHolder.practitionerPic, moPractitionerInfoList.getPractionerPic());
        }

        practitionerViewHolder.rel_lyt_recent_popular.setVisibility(View.GONE);
        if (position == 0) {
            practitionerViewHolder.temp_view.setVisibility(View.VISIBLE);
        } else {
            practitionerViewHolder.temp_view.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            return (null != list ? list.size() : 0);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            return (null != listServiceGroup ? listServiceGroup.get(0).getMoPractitionerInfoList().size() : 0);
        }
        return 0;
    }


    public class SpecialityDoctorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView name, speciality, price;
        RelativeLayout rel_lyt_recent_popular;
        FrameLayout temp_view;
        CircularImageView practitionerPic;

        public SpecialityDoctorViewHolder(View itemView, Context mContext) {

            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            temp_view = (FrameLayout) itemView.findViewById(R.id.temp_view);
            name = (CustomFontTextView) itemView.findViewById(R.id.practitionerName);
            speciality = (CustomFontTextView) itemView.findViewById(R.id.practitionerDesc);
            price = (CustomFontTextView) itemView.findViewById(R.id.tv_price);
            rel_lyt_recent_popular = (RelativeLayout) itemView.findViewById(R.id.rel_lyt_recent_popular);
            practitionerPic = (CircularImageView) itemView.findViewById(R.id.practitionerPic);

        }

        @Override
        public void onClick(View v) {
            CommonControls.closeKeyBoard(mContext);
            specialityDoctorOnClick.onSpecialityDoctorClick(getAdapterPosition());
          /*  PractitionerForAppointment practitioner = (PractitionerForAppointment) (list.get(getAdapterPosition()).getPractitionerForAppointment());

            Bundle bundle = new Bundle();
            bundle.putParcelable("appointmentmodel", list.get(getAdapterPosition()));
            bundle.putString(TextConstants.TEST_TYPE, from);
            Intent intent = new Intent(mContext, FragmentBaseActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);*/

        }
    }
}
