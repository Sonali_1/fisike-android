package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;

import appointment.phlooba.model.ObservationAppointmentMo;

/**
 * Created by neharathore on 12/07/17.
 */

public class RecommendedBySpinnerAdapter extends ArrayAdapter<ObservationAppointmentMo> {

    private Context context;
    private ArrayList<ObservationAppointmentMo> list;
    LayoutInflater mInflater;

    public RecommendedBySpinnerAdapter(Context context, int textViewResourceId, ArrayList<ObservationAppointmentMo> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }

    public ObservationAppointmentMo getItem(int position) {

        if(position==0) //index at which select is stored
            return null;
        else
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SpinAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            // Creates a ViewHolder and store references to
            // the two children views we want to bind data to.
            holder = new SpinAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            holder = (SpinAdapter.ViewHolder) convertView.getTag();
        }

        String physicianName=list.get(position).getOrderingPhysicianName();
        holder.txt_item.setText(Utils.isValueAvailable(physicianName)?physicianName:"");
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    static class ViewHolder {
        TextView txt_item;
    }


}