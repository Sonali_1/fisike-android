package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.Iterator;
import java.util.List;

import appointment.interfaces.Animable;
import appointment.interfaces.UpdateUIData;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.TestForAppointment;

/**
 * Created by laxmansingh on 6/17/2016.
 */
public class SelectedTestsAdapter extends RecyclerView.Adapter<SelectedTestsAdapter.SelectedTestsAdapterViewHolder> {
    private Context mContext;
    private List<TestForAppointment> list;
    private SelectedTestsAdapterViewHolder viewHolder;
    boolean checked;
    private UpdateUIData updateUIData;
    private Animable animable;
    private String from;
    Typeface font_bold, font_semibold;
    SpannableString desc_styledString, instruction_styledString;
    // boolean search;

    public SelectedTestsAdapter(Context context, List<TestForAppointment> list, UpdateUIData updateUIData) {
        this.list = list;
        this.mContext = context;
        this.updateUIData = updateUIData;
    }

    @Override
    public SelectedTestsAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.selectected_tests_listitem, null);
        SelectedTestsAdapterViewHolder viewHolder = new SelectedTestsAdapterViewHolder(view, mContext);
        setViewHolder(viewHolder);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SelectedTestsAdapterViewHolder customViewHolder, final int i) {
        customViewHolder.tv_test_name.setText(list.get(i).getTestName().toString().trim());
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public void setViewHolder(SelectedTestsAdapterViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public class SelectedTestsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView tv_test_name;
        IconTextView img_delete;

        public SelectedTestsAdapterViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);
            tv_test_name = (TextView) view.findViewById(R.id.tv_test_name);
            img_delete = (IconTextView) view.findViewById(R.id.img_labtest_delete);
            img_delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            List<TestForAppointment> selectedTests = OrderSummary.getInstance().getOrderist() ;
            String testHcsId = list.get(adapter_position).getHcsGroups().get(0).getMoHcsList().get(0).getHcsId();
            switch (v.getId()) {

                case R.id.img_labtest_delete:

                    Iterator<TestForAppointment> iterator = selectedTests.iterator();
                    while (iterator.hasNext()){
                        TestForAppointment testForAppointment = iterator.next();
                        if(testHcsId.equals(testForAppointment.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId())){
                            OrderSummary.getInstance().setTestNumber(OrderSummary.getInstance().getTestNumber() - 1);
                            if(list.get(adapter_position).getPrice() != null)
                                OrderSummary.getInstance().setTotalPrice(OrderSummary.getInstance().getTotalPrice() - Integer.parseInt(list.get(adapter_position).getPrice().getCurrentRate().toString().trim()));
                            else
                                OrderSummary.getInstance().setTotalPrice(OrderSummary.getInstance().getTotalPrice()-0);
                            OrderSummary.getInstance().getOrderist().remove(testForAppointment);
                            OrderSummary.getInstance().getHcsIdList().remove(testHcsId);
                         //   OrderSummary.getInstance().getAppointmentModelOrderList().remove(list.get(adapter_position));
                            break;
                        }
                    }

                    updateUIData.UpdateUI();
                    notifyDataSetChanged();
                    if (list.size() == 0) {
                        ((Activity) context).finish();
                    }
                    break;

            }
        }
    }


}
