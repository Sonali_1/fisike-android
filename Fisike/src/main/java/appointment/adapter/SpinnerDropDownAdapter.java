package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike_physician.adapters.CountryAdapter;

import java.util.List;
import java.util.StringTokenizer;

import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 6/20/2017.
 */

public class SpinnerDropDownAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> list;
    LayoutInflater mInflater;
    private Typeface font_regular, font_semibold;

    public SpinnerDropDownAdapter(Context context, int textViewResourceId,
                                  List<String> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        font_semibold = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_semibold));
        font_regular = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SpinAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            // Creates a ViewHolder and store references to
            // the two children views we want to bind data to.
            holder = new SpinAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            holder = (SpinAdapter.ViewHolder) convertView.getTag();
        }


        if (position == 0) {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));

        } else {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        }


        holder.txt_item.setText(list.get(position));
        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {


        final SpinAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, parent, false);
            holder = new SpinAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (SpinAdapter.ViewHolder) convertView.getTag();
        }

        if (position == 0) {
            holder.txt_item.setTextSize(16);
            holder.txt_item.setTypeface(font_semibold);

            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        } else {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.navy_blue_opacity_69));
        }
        holder.txt_item.setText(list.get(position));
        return convertView;
    }

    static class ViewHolder {
        TextView txt_item;
    }
}

