package appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.ArrayList;

import appointment.AppointmentActivity;
import appointment.BookDoctorActivity;
import appointment.BookSpecialityActivity;
import appointment.HomeSampleCollectionActivity;
import appointment.PatientInfoActivity;
import appointment.SelectServiceActivity;
import appointment.enums.AppointmentTypeEnum;
import appointment.model.OfferingModel;

/**
 * Created by laxman on 16/06/2016.
 */
public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestViewHolder> {

    private Context context;
    ArrayList<OfferingModel> requestList;
    String from;


    public RequestAdapter(FragmentActivity activity, ArrayList<OfferingModel> requestTypes, String from) {
        context = activity;
        requestList = requestTypes;
        this.from = from;
    }

    @Override
    public RequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.request_layout, null);
        RequestViewHolder viewHolder = new RequestViewHolder(view, context);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return (null != requestList ? requestList.size() : 0);
    }


    @Override
    public void onBindViewHolder(final RequestViewHolder customViewHolder, final int position) {
        OfferingModel requestType = requestList.get(position);

        if (position == 0) {
            customViewHolder.temp_view.setVisibility(View.VISIBLE);
        } else {
            customViewHolder.temp_view.setVisibility(View.GONE);
        }

        customViewHolder.category.setText(AppointmentTypeEnum.getDisplayedValuefromCode(requestType.getTitle()) + "");
        customViewHolder.description.setText(AppointmentTypeEnum.getDisplayedValuefromCode(requestType.getDescription()) + "");

//        if (from.equals(TextConstants.FROM_APPOINTMENT)) {
//            customViewHolder.imageView.setVisibility(View.VISIBLE);
//            if(position==0)
//                customViewHolder.imageView.setText(R.string.fa_doctor_ico);
//            else
//                customViewHolder.imageView.setText(requestType.getResourceId());
//        } else if (from.equals(TextConstants.FROM_SERVICES)) {
//            customViewHolder.imageView.setVisibility(View.GONE);
//        }

        if (!requestType.getIsActive())
            customViewHolder.perm_view.setVisibility(View.GONE);


    }


    public class RequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView category, description;
        IconTextView imageView;
        FrameLayout temp_view;
        RelativeLayout perm_view, clicked_view;

        public RequestViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            category = (TextView) view.findViewById(R.id.request_category);
            description = (TextView) view.findViewById(R.id.request_description);
            imageView = (IconTextView) view.findViewById(R.id.request_image);
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);
            perm_view = (RelativeLayout) view.findViewById(R.id.perm_view);
            clicked_view = (RelativeLayout) view.findViewById(R.id.clicked_view);
            clicked_view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            onClickRequest(v);
        }

        public void onClickRequest(View v) {
            int adapter_position = getAdapterPosition();
            if (BuildConfig.isPhysicianApp) {
                ((AppointmentActivity) context).setCurrentItem(-1);
            }

            String id = requestList.get(adapter_position).getId();
            switch (id) {
                case "1":
                    Intent nextActivity = null;

                    if (BuildConfig.isStartWithPatientInfo) {
                        openPatientInfoActivity(TextConstants.DOCTORS);
                    } else {
                        nextActivity = new Intent(context, BookDoctorActivity.class);
                        context.startActivity(nextActivity);
                    }
                    break;
                case "2":
                    if (BuildConfig.isStartWithPatientInfo) {
                        openPatientInfoActivity(TextConstants.SPECIALITY);
                    } else {
                        nextActivity = new Intent(context, BookSpecialityActivity.class);
                        context.startActivity(nextActivity);
                    }
                    break;

                case "3":
                    Bundle bundle = new Bundle();
                    OfferingModel offeringModel = requestList.get(adapter_position);
                    if (offeringModel.getHasChild() && offeringModel.getChild().size() > 0) {
                        bundle.putString(TextConstants.TEST_TYPE, TextConstants.SERVICES);
                        bundle.putParcelableArrayList(TextConstants.CHILD_OFFERING_MODEL, (ArrayList<? extends Parcelable>) offeringModel.getChild());
                        Intent serviceIntent = new Intent(context, SelectServiceActivity.class);
                        serviceIntent.putExtras(bundle);
                        context.startActivity(serviceIntent);
                    } else {
                        if (BuildConfig.isStartWithPatientInfo) {
                            openPatientInfoActivity(TextConstants.SERVICES);
                        } else {
                            bundle.putString(TextConstants.TEST_TYPE, TextConstants.SERVICES);
                            Intent serviceIntent = new Intent(context, HomeSampleCollectionActivity.class);
                            serviceIntent.putExtras(bundle);
                            context.startActivity(serviceIntent);
                        }
                    }
                    break;

                case "4":
                    if (requestList.get(getAdapterPosition()).getTitle().equals(TextConstants.HOME_SAMPLE)) {
                        Bundle bundles = new Bundle();
                        bundles.putString(TextConstants.TEST_TYPE, TextConstants.HOME_SAMPLE);
                        Intent homesampleIntent = new Intent(context, HomeSampleCollectionActivity.class);
                        homesampleIntent.putExtras(bundles);
                        context.startActivity(homesampleIntent);
                    } else {
                        if (BuildConfig.isStartWithPatientInfo) {
                            openPatientInfoActivity(TextConstants.HOME_SAMPLE);
                        } else {
                            Bundle bundles = new Bundle();
                            bundles.putString(TextConstants.TEST_TYPE, TextConstants.HOME_SAMPLE);
                            Intent homesampleIntent = new Intent(context, HomeSampleCollectionActivity.class);
                            homesampleIntent.putExtras(bundles);
                            context.startActivity(homesampleIntent);
                        }
                    }
                    break;

                case "5":
                    if (BuildConfig.isStartWithPatientInfo) {
                        openPatientInfoActivity(TextConstants.LAB_TEST);
                    } else {
                        Bundle bundless = new Bundle();
                        bundless.putString(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                        Intent labtestIntent = new Intent(context, HomeSampleCollectionActivity.class);
                        labtestIntent.putExtras(bundless);
                        context.startActivity(labtestIntent);
                    }
                    break;

                default:
                    break;
            }

        }
    }


    private void openPatientInfoActivity(String from) {
        Bundle bundle = new Bundle();
        bundle.putString(TextConstants.FROM, from);
//        Intent nextIntent = new Intent(context, WhosPatientActivity.class);
           Intent nextIntent = new Intent(context, PatientInfoActivity.class);
        nextIntent.putExtras(bundle);
        context.startActivity(nextIntent);
    }


}
