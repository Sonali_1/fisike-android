package appointment.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;

import appointment.FragmentBaseActivity;
import appointment.fragment.SelectTimeFragment;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.MoHcsList;
import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 11/11/2016.
 */

public class LocationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private java.util.List<AppointmentModel> list;
    private String from;
    private int selectedServicePosition;
    private boolean isSkipped;
    boolean isBookable = false;

    /*[  .....loadmore variables needed.....   ]*/
    private final int VIEW_TYPE_ITEM = 0;
    /*[  .....End of loadmore variables needed.....   ]*/

    public LocationsAdapter(Context context, final java.util.List<AppointmentModel> list, String from, int selectedServicePosition, boolean isSkipped) {
        this.list = list;
        this.mContext = context;
        this.from = from;
        this.selectedServicePosition = selectedServicePosition;
        this.isSkipped = isSkipped;
        //   checkBookableOrRequestable();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.locations_list_item, null);
            LocationsAdapter.LocationsViewHolder viewHolder = new LocationsAdapter.LocationsViewHolder(view, mContext);
            return viewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int i) {
        if (customViewHolder instanceof LocationsAdapter.LocationsViewHolder) {

            MoHcsList moHcsList = null;
            LocationsAdapter.LocationsViewHolder viewHolder = (LocationsAdapter.LocationsViewHolder) customViewHolder;
            if (from.equals(TextConstants.DOCTORS)) {
                if (!isSkipped) {
                    moHcsList = (MoHcsList) list.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(0);
                    viewHolder.tv_location1.setText(moHcsList.getLocationName().toString().trim());
                    viewHolder.tv_location2.setVisibility(View.GONE);
                    viewHolder.tv_location3.setText(moHcsList.getLocationAddress().toString().trim());
                } else {
                    viewHolder.tv_location1.setText(moHcsList.getLocationName().toString().trim());
                    viewHolder.tv_location2.setVisibility(View.VISIBLE);
                    viewHolder.tv_location3.setText(moHcsList.getLocationAddress().toString().trim());
                }
            } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE)) {
                moHcsList = (MoHcsList) list.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(0);
                viewHolder.tv_location1.setText(moHcsList.getLocationName().toString().trim());
                viewHolder.tv_location2.setVisibility(View.GONE);
                viewHolder.tv_location3.setText(moHcsList.getLocationAddress().toString().trim());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (from.equals(TextConstants.DOCTORS)) {
            if (!isSkipped) {
                return (null != list ? list.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().size() : 0);
            } else {
                return 0;
            }
        } else {
            return list.size();
        }
    }


    public class LocationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_location1, tv_location2, tv_location3;
        CustomFontButton btnSelect;
        FrameLayout temp_view;

        public LocationsViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_location1 = (CustomFontTextView) view.findViewById(R.id.tv_location1);
            tv_location2 = (CustomFontTextView) view.findViewById(R.id.tv_location2);
            tv_location3 = (CustomFontTextView) view.findViewById(R.id.tv_location3);
            btnSelect = (CustomFontButton) view.findViewById(R.id.btnSelect);
            btnSelect.setOnClickListener(this);
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {

                case R.id.btnSelect:

                    if (from.equals(TextConstants.DOCTORS)) {
                        if (!isSkipped) {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("appointmentmodel", list.get(0));
                            bundle.putString(TextConstants.TEST_TYPE, from);
                            bundle.putInt("selectedservice", selectedServicePosition);
                            bundle.putInt("selectedlocation", adapter_position);
                            try {
                                isBookable = new CommonTasks().checkBookableOrRequestable(list.get(0).getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(adapter_position).getRoles());
                            } catch (Exception ex) {
                                isBookable = false;
                            }
                            bundle.putBoolean("isBookable", isBookable);
                            bundle.putBoolean("isskipped", isSkipped);
                            SelectTimeFragment timeFragment = new SelectTimeFragment();
                            timeFragment.setArguments(bundle);
                            ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                                    .beginTransaction()
                                    .add(R.id.fragmentParentView, timeFragment, "selecttimefragment")
                                    .addToBackStack(null)
                                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commit();
                        } else {
                        }
                    } else if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST)) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("appointmentmodel", list.get(0));
                        bundle.putString(TextConstants.TEST_TYPE, from);
                        try {
                            isBookable = new CommonTasks().checkBookableOrRequestable(list.get(0).getTestForAppointment().getHcsGroups().get(0).getMoHcsList().get(adapter_position).getRoles());
                        } catch (Exception ex) {
                            isBookable = false;
                        }
                        bundle.putBoolean("isBookable", isBookable);
                        SelectTimeFragment timeFragment = new SelectTimeFragment();
                        timeFragment.setArguments(bundle);
                        ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.fragmentParentView, timeFragment, "selecttimefragment")
                                .addToBackStack(null)
                                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .commit();
                    }

                    break;

                default:
                    break;
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }


}
