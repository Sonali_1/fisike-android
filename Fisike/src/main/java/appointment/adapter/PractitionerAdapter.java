package appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;
import java.util.Locale;

import appointment.FragmentBaseActivity;
import appointment.interfaces.Animable;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.PractitionerForAppointment;
import appointment.utils.CircularImageView;
import appointment.utils.CommonControls;

/**
 * Created by Aastha on 16/03/2016.
 */
public class PractitionerAdapter extends RecyclerView.Adapter<PractitionerAdapter.PractitionerViewHolder> {
    private Context mContext;
    private List<AppointmentModel> list;
    boolean isSearch;
    private String from;
    private String searchedString;

    public PractitionerAdapter(Context context, List<AppointmentModel> practitionerList, Animable animable, String from, boolean isSearch) {
        this.list = practitionerList;
        mContext = context;
        this.from = from;
        this.isSearch = isSearch;
    }

    @Override
    public PractitionerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.practioner_list_item, null);
        PractitionerViewHolder viewHolder = new PractitionerViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PractitionerViewHolder practitionerViewHolder, int position) {


        PractitionerForAppointment practitioner = (PractitionerForAppointment) list.get(position).getPractitionerForAppointment();
        String practitionerName = practitioner.getPractitionerName();
        practitionerViewHolder.name.setText(practitionerName);


        new CommonControls().setProfilePic(mContext, practitionerViewHolder.practitionerPic, practitioner.getPractionerPic());

        if (practitioner.getSpecialty() != null)
            practitionerViewHolder.speciality.setText(practitioner.getSpecialty());
        else
            practitionerViewHolder.speciality.setText("");
        //  practitionerViewHolder.price.setText(practitioner.getTestCost());

        if (isSearch && searchedString != null) {
            if (practitionerName.toLowerCase().matches("(.*)" + searchedString.toLowerCase() + "(.*)")) {

                int startPos = practitionerName.toLowerCase(Locale.US).indexOf(searchedString.toLowerCase(Locale.US));
                int endPos = startPos + searchedString.length();
                if (startPos != -1) // This should always be true, just a sanity check
                {
                    Spannable spannable = new SpannableString(practitionerName);
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{ContextCompat.getColor(mContext, R.color.dusky_blue)});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);

                    spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    practitionerViewHolder.name.setText(spannable);
                } else
                    practitionerViewHolder.name.setText(practitionerName);

            }
        }

        if (position == 0 && !isSearch) {

        } else {
            practitionerViewHolder.rel_lyt_recent_popular.setVisibility(View.GONE);
        }

        if (position == 0 && !isSearch) {
            practitionerViewHolder.rel_lyt_recent_popular.setVisibility(View.VISIBLE);
            practitionerViewHolder.temp_view.setVisibility(View.GONE);
        } else {
            if (position == 0) {
                practitionerViewHolder.temp_view.setVisibility(View.VISIBLE);
            } else {
                practitionerViewHolder.temp_view.setVisibility(View.GONE);
            }
            practitionerViewHolder.rel_lyt_recent_popular.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public void setSearchedString(String searchedString) {
        this.searchedString = searchedString;
        notifyDataSetChanged();
    }


    public class PractitionerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView name, speciality, price;
        RelativeLayout rel_lyt_recent_popular;
        FrameLayout temp_view;
        CircularImageView practitionerPic;

        public PractitionerViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            temp_view = (FrameLayout) itemView.findViewById(R.id.temp_view);
            name = (CustomFontTextView) itemView.findViewById(R.id.practitionerName);
            speciality = (CustomFontTextView) itemView.findViewById(R.id.practitionerDesc);
            price = (CustomFontTextView) itemView.findViewById(R.id.tv_price);
            rel_lyt_recent_popular = (RelativeLayout) itemView.findViewById(R.id.rel_lyt_recent_popular);
            practitionerPic = (CircularImageView) itemView.findViewById(R.id.practitionerPic);
        }

        @Override
        public void onClick(View v) {
            CommonControls.closeKeyBoard(mContext);
            PractitionerForAppointment practitioner = (PractitionerForAppointment) (list.get(getAdapterPosition()).getPractitionerForAppointment());

            Bundle bundle = new Bundle();
            bundle.putParcelable("appointmentmodel", list.get(getAdapterPosition()));
            bundle.putString(TextConstants.TEST_TYPE, from);
            Intent intent = new Intent(mContext, FragmentBaseActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);

        }
    }
}
