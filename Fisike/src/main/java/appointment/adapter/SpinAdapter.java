package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Text;
import com.mphrx.fisike.utils.DateTimeUtil;

import java.util.List;

import javax.xml.transform.sax.SAXTransformerFactory;

import appointment.utils.CommonTasks;


/**
 * Created by laxmansingh on 3/10/16.
 */
public class SpinAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> list;
    LayoutInflater mInflater;
    private String isDateOrTime;

    public SpinAdapter(Context context, int textViewResourceId, List<String> list, String isDateOrTime) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        this.isDateOrTime = isDateOrTime;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            // Creates a ViewHolder and store references to
            // the two children views we want to bind data to.
            holder = new ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            holder = (ViewHolder) convertView.getTag();
        }


        String time_text = "";


        if (DateFormat.is24HourFormat(context) && isDateOrTime != null && isDateOrTime.equals(TextConstants.TIME_TEXT)) {
            time_text = CommonTasks.formatDateFromstring(DateTimeUtil.destinationTimeFormat, DateTimeUtil.sourceTimeFormat, list.get(position).toString().trim());
        } else if (isDateOrTime != null && isDateOrTime.equals(TextConstants.MEDICARE)) {
            time_text = list.get(position).toString().trim();
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        } else {
            time_text = list.get(position).toString().trim();
        }

        holder.txt_item.setText(time_text);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    static class ViewHolder {
        TextView txt_item;
    }
}

