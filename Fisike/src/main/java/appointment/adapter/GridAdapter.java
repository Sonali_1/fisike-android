package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import appointment.model.datetimeslotmodel.TimeModel;
import appointment.utils.CommonTasks;

/**
 * Created by Raj on 12/20/2016.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder> {

    private Context mContext;
    List<TimeModel> timeModels;
    private GridAdapter.OnGridChildClickedListener gridChildClickedListener;
    int groupPosition;

    public interface OnGridChildClickedListener {
        void onChildClicked(View view, int groupPosition, int childPosition);
    }

    public GridAdapter(Context context, List<TimeModel> list, int groupPosition, GridAdapter.OnGridChildClickedListener gridChildClickedListener) {
        this.mContext = context;
        this.timeModels = list;
        this.groupPosition = groupPosition;
        this.gridChildClickedListener = gridChildClickedListener;
    }

    public interface OnItemClickedListener {
        public void itemClicked(View view, int position);
    }

    @Override
    public GridAdapter.GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_child_item, null);
        GridAdapter.GridViewHolder viewHolder = new GridAdapter.GridViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GridAdapter.GridViewHolder holder, int position) {
        String time = "";
        if ((timeModels.get(position).getStartdate().toString().trim().contains("z") || timeModels.get(position).getStartdate().toString().trim().contains("Z") && (timeModels.get(position).getEnddate().toString().trim().contains("z") || timeModels.get(position).getEnddate().toString().trim().contains("Z")))) {
            try {
                Date ds = CommonTasks.fromIsoUtcString(timeModels.get(position).getStartdate().toString().trim(), null);
                Date de = CommonTasks.fromIsoUtcString(timeModels.get(position).getEnddate().toString().trim(), null);


                SimpleDateFormat simpleDateFormat;
                if(DateFormat.is24HourFormat(mContext)) {
                     simpleDateFormat = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat);
                }else {
                     simpleDateFormat = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat);
                }

                simpleDateFormat.setTimeZone(TimeZone.getDefault());

                time = simpleDateFormat.format(ds) + " - " + simpleDateFormat.format(de);

                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated);
                simpleDateFormat1.setTimeZone(TimeZone.getDefault());

                timeModels.get(position).setStartdate(simpleDateFormat1.format(ds));
                timeModels.get(position).setEnddate(simpleDateFormat1.format(de));
            } catch (Exception ex) {
            }
        } else {
            try {
                if(DateFormat.is24HourFormat(mContext)) {
                    time = CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.sourceTimeFormat, timeModels.get(position).getStartdate().toString().trim()) + " - " + CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.sourceTimeFormat, timeModels.get(position).getEnddate().toString().trim());
                }else {
                    time = CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.destinationTimeFormat, timeModels.get(position).getStartdate().toString().trim()) + " - " + CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated, DateTimeUtil.destinationTimeFormat, timeModels.get(position).getEnddate().toString().trim());
                }
            } catch (Exception ex) {
            }
        }

        holder.tv_time_txt.setText(time);
    }

    @Override
    public int getItemCount() {
        return timeModels.size();
    }

    public class GridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView tv_time_txt;

        public GridViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            tv_time_txt = (CustomFontTextView) itemView.findViewById(R.id.tv_time_txt);
        }

        @Override
        public void onClick(View v) {
            int adapterPostion = getAdapterPosition();
            gridChildClickedListener.onChildClicked(v, groupPosition, adapterPostion);
        }
    }
}
