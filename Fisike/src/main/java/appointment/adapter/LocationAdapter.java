package appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.views.RTextView;

import java.util.ArrayList;
import java.util.List;

import appointment.FragmentBaseActivity;
import appointment.model.OrderSummary;

/**
 * Created by Aastha on 30/03/2016.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> implements
        Filterable {

    private Context mContext;
    private List<appointment.model.locationmodel.List> list;
    private String from;
    private List<appointment.model.locationmodel.List> list_FilterList;
    private LocationFilter locationFilter;
    private RelativeLayout rl_error_view;
    private RecyclerView rv_doctor_locations;
    private RTextView listEmptyError;


    public LocationAdapter(Context mContext, List<appointment.model.locationmodel.List> list, String from, RecyclerView rv_doctor_locations, RelativeLayout rl_error_view, RTextView listEmptyError) {
        this.list = list;
        this.list_FilterList = list;
        this.mContext = mContext;
        this.from = from;
        this.rv_doctor_locations = rv_doctor_locations;
        this.rl_error_view = rl_error_view;
        this.listEmptyError = listEmptyError;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item, null);
        LocationViewHolder viewHolder = new LocationViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        appointment.model.locationmodel.Location location = list.get(position).getLocation();
        if (location.getAddress().size() != 0 && location.getAddress().get(0).getText() != null) {
            holder.tv_address.setText(location.getAddress().get(0).getText().toString());
        } else {
            holder.tv_address.setText(mContext.getResources().getString(R.string.default_));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_location_name, tv_address;

        public LocationViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            tv_location_name = (TextView) itemView.findViewById(R.id.tv_location_name);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            appointment.model.locationmodel.Location location = list.get(position).getLocation();
            if (location.getAddress().size() != 0 && location.getAddress().get(0).getText() != null)
                OrderSummary.getInstance().setDoctorLocationAddress(location.getAddress().get(0).getText());
            else
                OrderSummary.getInstance().setDoctorLocationAddress("");
            if (location.getTelecom().size() != 0 && location.getTelecom().get(0).getValue() != null) {
                OrderSummary.getInstance().setDoctorContact(location.getTelecom().get(0).getValue().toString());
            } else {
                OrderSummary.getInstance().setDoctorContact("");
            }
            Bundle bundle = new Bundle();
            bundle.putInt("HCS", list.get(position).getId());
            bundle.putString(TextConstants.TEST_TYPE, from);
            Intent intent = new Intent(mContext, FragmentBaseActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);

        }
    }


    /*[       Filtering code here  ]*/
    @Override
    public Filter getFilter() {
        if (locationFilter == null) {
            locationFilter = new LocationFilter();
        }
        return locationFilter;
    }


    private class LocationFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<appointment.model.locationmodel.List> filterList = new ArrayList<appointment.model.locationmodel.List>();
                for (int i = 0; i < list_FilterList.size(); i++) {
                    if ((list_FilterList.get(i).getLocation().getAddress().get(0).getText().toString()
                            .toUpperCase()).contains(constraint.toString()
                            .toUpperCase())) {
                        appointment.model.locationmodel.List names = (appointment.model.locationmodel.List) list_FilterList
                                .get(i);
                        filterList.add(names);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = list_FilterList.size();
                results.values = list_FilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list = (ArrayList<appointment.model.locationmodel.List>) results.values;
            notifyDataSetChanged();
            if (list.size() == 0) {
                showError(mContext.getResources().getString(R.string.No_location_found));
            } else {
                rl_error_view.setVisibility(View.GONE);
                rv_doctor_locations.setVisibility(View.VISIBLE);
                listEmptyError.setVisibility(View.GONE);
            }
        }

    }


    private void showError(String response) {
        rv_doctor_locations.setVisibility(View.GONE);
        listEmptyError.setVisibility(View.VISIBLE);
        listEmptyError.setText(response);
        rl_error_view.setVisibility(View.VISIBLE);
    }

    /*[     End of  Filtering code here  ]*/
}
