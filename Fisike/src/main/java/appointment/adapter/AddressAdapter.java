package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;

import java.util.ArrayList;
import java.util.List;

import appointment.model.renderconfigmodel.RenderConfigModel;

/**
 * Created by laxmansingh on 7/5/2016.
 */
public class AddressAdapter extends ArrayAdapter<String> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<RenderConfigModel> list;
    LayoutInflater mInflater;
    private Typeface font_regular, font_semibold;
    private String region;
    private int selectedStatePosition = -1;

    public AddressAdapter(Context context, int textViewResourceId, List<RenderConfigModel> list, String region) {
        super(context, textViewResourceId, new ArrayList<String>());
        this.context = context;
        this.list = list;
        this.region = region;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        font_semibold = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
        font_regular = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
    }

    public int getCount() {
        if (region.equals(TextConstants.STATE)) {
            if (list.size() > 0 && list.get(0) != null && list.get(0).getStateCityAndZipMap() != null) {
                return list.get(0).getStateCityAndZipMap().size() + 1;
            } else {
                return 1;
            }
        } else if (region.equals(TextConstants.CITY)) {

            if (selectedStatePosition == -1) {
                return 1;
            } else {
                return list.get(0).getStateCityAndZipMap().get(selectedStatePosition).getCities().size() + 1;
            }
        }
        return list.size();
    }

    public String getItem(int position) {
        if (region.equals(TextConstants.STATE)) {
            if (position == 0) {
                return context.getResources().getString(R.string.Select_State);
            } else {
                return list.get(0).getStateCityAndZipMap().get(position - 1).getState();
            }

        } else if (region.equals(TextConstants.CITY)) {
            if (position == 0) {
                return context.getResources().getString(R.string.Select_City);
            } else {
                return list.get(0).getStateCityAndZipMap().get(selectedStatePosition).getCities().get(position - 1).getCity();
            }
        }
        return "";
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_address, null);
            holder = new ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == 0) {
            holder.txt_item.setTextSize(16);
            holder.txt_item.setTypeface(font_semibold);
            if (selectedStatePosition == -1 && region.equals(TextConstants.CITY)) {
                holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue_30));
            } else {
                holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
            }
        } else {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        }

        if (region.equals(TextConstants.STATE)) {
            if (position == 0) {
                holder.txt_item.setText(context.getResources().getString(R.string.Select_State));
            } else {
                holder.txt_item.setText(list.get(0).getStateCityAndZipMap().get(position - 1).getState());
            }

        } else if (region.equals(TextConstants.CITY)) {
            if (position == 0) {
                holder.txt_item.setText(context.getResources().getString(R.string.Select_City));
            } else {
                holder.txt_item.setText(list.get(0).getStateCityAndZipMap().get(selectedStatePosition).getCities().get(position - 1).getCity());
            }
        }
        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            holder = new ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == 0) {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            if (selectedStatePosition == -1 && region.equals(TextConstants.CITY)) {
                holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue_30));
            } else {
                holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
            }
        } else {
            holder.txt_item.setTextSize(14);
            holder.txt_item.setTypeface(font_regular);
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.navy_blue_opacity_69));
        }


        if (region.equals(TextConstants.STATE)) {
            if (position == 0) {
                holder.txt_item.setText(context.getResources().getString(R.string.Select_State));
            } else {
                holder.txt_item.setText(list.get(0).getStateCityAndZipMap().get(position - 1).getState());
            }

        } else if (region.equals(TextConstants.CITY)) {
            if (position == 0) {
                holder.txt_item.setText(context.getResources().getString(R.string.Select_City));
            } else {
                holder.txt_item.setText(list.get(0).getStateCityAndZipMap().get(selectedStatePosition).getCities().get(position - 1).getCity());
            }
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txt_item;
    }


    public void setSelectedStatePosition(int position) {
        if (region.equals(TextConstants.CITY)) {
            selectedStatePosition = position;
            notifyDataSetChanged();
        }
    }
}
