package appointment.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.view.FragmentStatePagerAdapter;
import com.mphrx.fisike.view.SlidingTabLayout;

import appointment.fragment.BookOrRequestFragment;
import appointment.fragment.MyAppointmentFragment;
import appointment.fragment.MyOrderFragment;

/**
 * Created by Raj on 12/2/2016.
 */

public class MyAppointmentAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private static final String Fragment = null;
    private static int NUM_ITEMS = 2;
    private SlidingTabLayout tabs;
    private MyAppointmentFragment myAppointmentFragment;

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public MyAppointmentAdapter(Context context, FragmentManager fragmentManager, SlidingTabLayout tabs, MyAppointmentFragment myAppointmentFragment) {
        super(fragmentManager);
        this.mContext = context;
        this.tabs = tabs;
        this.myAppointmentFragment = myAppointmentFragment;
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                BookOrRequestFragment bookOrRequestFragment = new BookOrRequestFragment();
                return bookOrRequestFragment;
            case 1: // Fragment # 1 - This will show SecondFragment
                MyOrderFragment myOrderFragment = new MyOrderFragment();
                return myOrderFragment;

            default:
                return null;
        }
    }


    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence pageTitle = null;
        switch (position) {
            case 0:
                pageTitle = mContext.getResources().getString(R.string.title_schedule_now);
                break;
            case 1:
                pageTitle = mContext.getResources().getString(R.string.title_my_appointments);
                break;


        }
        return pageTitle;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commitAllowingStateLoss();
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }


    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

}
