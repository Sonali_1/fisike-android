package appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.CustomLinkMovementMethod;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import appointment.FragmentBaseActivity;
import appointment.SelectTimeActivity;
import appointment.interfaces.Animable;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.TestForAppointment;
import appointment.utils.CustomTypefaceSpan;

/**
 * Created by Aastha on 24/02/2016.
 */
public class HomeSampleCollectionAdapter extends RecyclerView.Adapter<HomeSampleCollectionAdapter.HomeSampleCollectionViewHolder> {
    private Context mContext;
    private List<AppointmentModel> list;
    private HomeSampleCollectionViewHolder viewHolder;
    private boolean checked, issearchecd;
    private Animable animable;
    private String from;
    private Typeface font_bold, font_semibold;

    private String searchedString;
    // boolean search;

    SpannableString text, appendString;
    String appendContent;
    boolean isExpanded;
    private int MAX_LENGTH = 50;
    private List<String> hcsIds;
    private String more, less;


    public HomeSampleCollectionAdapter(Context context, List<AppointmentModel> list, Animable animable, String from, boolean issearchecd) {
        this.list = list;
        this.mContext = context;
        this.animable = animable;
        this.issearchecd = issearchecd;
        this.from = from;
        font_bold = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_bold));
        font_semibold = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_regular));
    }

    @Override
    public HomeSampleCollectionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_collection_list_item, null);
        HomeSampleCollectionViewHolder viewHolder = new HomeSampleCollectionViewHolder(view, mContext);
        setViewHolder(viewHolder);
        hcsIds = OrderSummary.getInstance().getHcsIdList();

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final HomeSampleCollectionViewHolder customViewHolder, final int i) {
        String desc_styledString = "", instruction_styledString = "";

        TestForAppointment test = list.get(i).getTestForAppointment();
        String insruction = test.getHcsGroups().get(0).getMoHcsList().get(0).getInstructions();
        String description = test.getHcsGroups().get(0).getMoHcsList().get(0).getDescription();
        String hcsId = test.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId();
        String testName = test.getTestName().toString().trim();
        customViewHolder.tv_name.setText(testName);

        // mainitaining state of selected tests
        int size = OrderSummary.getInstance().getOrderist().size();
        if (from.equals(TextConstants.LAB_TEST)) {
            unselectUIText(customViewHolder); //set select in text
        } else if (size != 0 && hcsIds.contains(hcsId) || from.equals(TextConstants.LAB_TEST)) {
            selectUIText(customViewHolder); //set unselect in text
        } else {
            unselectUIText(customViewHolder); //set select in text
        }

        if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
            instruction_styledString = new String(mContext.getResources().getString(R.string.Instructions) + Html.fromHtml(insruction.trim()));
        }

        if (description != null && !description.equals(mContext.getResources().getString(R.string.txt_null)) && !description.equals("")) {
            desc_styledString = new String(mContext.getResources().getString(R.string.Description) + Html.fromHtml(description.trim()));
        }

        String content = new String(instruction_styledString + (instruction_styledString.length() == 0 ? "" : "\n\n") + desc_styledString);

        if (content.length() < MAX_LENGTH) {
            SpannableString text = new SpannableString(content);
            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Instructions).length(), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Description).length() + 1, text.length(), 0);
            } else if (content.length() > 12) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Description).length(), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Description).length() + 1, text.length(), 0);
            }

            customViewHolder.tv_test_instruction.setText(text);
            isExpanded = false;
        } else {
            more = MyApplication.getAppContext().getResources().getString(R.string.more);
            less = MyApplication.getAppContext().getResources().getString(R.string.less);
            appendContent = less;
            isExpanded = false;
            styleAndAnimate((CustomFontTextView) customViewHolder.tv_name, (CustomFontTextView) customViewHolder.tv_test_instruction, content, insruction, instruction_styledString, description, desc_styledString, mContext);
        }

        if (i == 0 && !issearchecd) {
            customViewHolder.temp_view.setVisibility(View.GONE);
            customViewHolder.rel_lyt_recent_popular.setVisibility(View.VISIBLE);
            customViewHolder.tv_recent_popular.setText(mContext.getResources().getString(R.string.txt_popular));
            customViewHolder.tv_viewAll.setVisibility(View.GONE);
            customViewHolder.tv_recent_popular.setVisibility(View.VISIBLE);
        } else {
            if (i == 0) {
                customViewHolder.temp_view.setVisibility(View.VISIBLE);
            } else {
                customViewHolder.temp_view.setVisibility(View.GONE);
            }
            customViewHolder.tv_recent_popular.setVisibility(View.GONE);
            customViewHolder.tv_viewAll.setVisibility(View.GONE);
            customViewHolder.rel_lyt_recent_popular.setVisibility(View.GONE);
        }

        if (issearchecd && searchedString != null) {
            if (testName.toLowerCase().matches("(.*)" + searchedString.toLowerCase() + "(.*)")) {

                int startPos = testName.toLowerCase(Locale.US).indexOf(searchedString.toLowerCase(Locale.US));
                int endPos = startPos + searchedString.length();
                if (startPos != -1) // This should always be true, just a sanity check
                {
                    Spannable spannable = new SpannableString(testName);
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{ContextCompat.getColor(mContext, R.color.dusky_blue)});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);

                    spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    customViewHolder.tv_name.setText(spannable);
                } else {
                    customViewHolder.tv_name.setText(testName);
                }
            }
        }

    }

    private void unselectUIText(HomeSampleCollectionViewHolder customViewHolder) {
        customViewHolder.img_add_subtract.setText(mContext.getResources().getString(R.string.Select));
        customViewHolder.img_add_subtract.setTag("plus");
    }

    private void selectUIText(HomeSampleCollectionViewHolder customViewHolder) {
        customViewHolder.img_add_subtract.setText(mContext.getResources().getString(R.string.Unselect));
        customViewHolder.img_add_subtract.setTag("minus");
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public void setViewHolder(HomeSampleCollectionViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setSearchedString(String searchedString) {
        this.searchedString = searchedString;
        notifyDataSetChanged();
    }


    public class HomeSampleCollectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView tv_name, tv_price, tv_know_more_less;
        RelativeLayout rlOverlay;
        TextView tv_recent_popular, tv_viewAll;
        RelativeLayout rel_lyt_recent_popular;
        int view_id;
        FrameLayout temp_view;
        CustomFontButton img_add_subtract;
        FrameLayout card_view;
        TextView tv_desc, tv_test_instruction;
        IconTextView img_currency;


        public HomeSampleCollectionViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            card_view = (FrameLayout) view.findViewById(R.id.card_view);
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);
            tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            tv_test_instruction = (TextView) view.findViewById(R.id.tv_test_instruction);

            img_currency = (IconTextView) view.findViewById(R.id.img_currency);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            tv_know_more_less = (TextView) view.findViewById(R.id.tv_know_more_less);
            tv_know_more_less.setOnClickListener(this);
            rlOverlay = (RelativeLayout) view.findViewById(R.id.overlay);
            rlOverlay.setOnClickListener(this);
            tv_recent_popular = (TextView) view.findViewById(R.id.tv_recent_popular);
            tv_viewAll = (TextView) view.findViewById(R.id.tv_viewAll);
            rel_lyt_recent_popular = (RelativeLayout) view.findViewById(R.id.rel_lyt_recent_popular);

            img_add_subtract = (CustomFontButton) view.findViewById(R.id.img_add_subtract);
            img_add_subtract.setOnClickListener(this);
            card_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int adapter_position = getAdapterPosition();
            TestForAppointment test = list.get(adapter_position).getTestForAppointment();

            int price;
            if (test.getPrice() != null)
                price = Integer.parseInt(test.getPrice().getCurrentRate());
            else
                price = 0;

            String testHcsId = test.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId();

            switch (v.getId()) {

                case R.id.img_add_subtract:

                    if (img_add_subtract.getTag().equals("plus")) {
                        if (!from.equals(TextConstants.HOME_SAMPLE)) { //case of lab test ,doctor , speciality ,services
                            img_add_subtract.setTag("minus");
                            //img_add_subtract.setText("Unselect");
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("appointmentmodel", list.get(adapter_position));
                            bundle.putString(TextConstants.TEST_TYPE, from);
                            Intent intent = new Intent(mContext, FragmentBaseActivity.class);
                            intent.putExtras(bundle);
                            mContext.startActivity(intent);

                        } else if (from.equals(TextConstants.HOME_SAMPLE) && !BuildConfig.isMultipleSelectionEnabled) { //case of home sample with single selection
                            img_add_subtract.setTag("minus");
                            //img_add_subtract.setText("Unselect");
                            OrderSummary.getInstance().getOrderist().add(test);
                            OrderSummary.getInstance().getHcsIdList().add(test.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId());
                            int totalCost = OrderSummary.getInstance().getTotalPrice();
                            int totalNumber = OrderSummary.getInstance().getTestNumber();
                            totalNumber += 1;
                            totalCost = totalCost + price;
                            OrderSummary.getInstance().setTestNumber(totalNumber);
                            OrderSummary.getInstance().setTotalPrice(totalCost);
                            Intent intent1 = new Intent(mContext, SelectTimeActivity.class);
                            mContext.startActivity(intent1);

                        } else if (from.equals(TextConstants.HOME_SAMPLE) && BuildConfig.isMultipleSelectionEnabled) { //home sample with multiple selection
                            img_add_subtract.setTag("minus");
                            img_add_subtract.setText(mContext.getResources().getString(R.string.Unselect));
                            OrderSummary.getInstance().getOrderist().add(test);
                            //    OrderSummary.getInstance().getAppointmentModelOrderList().add(list.get(getAdapterPosition()));
                            OrderSummary.getInstance().getHcsIdList().add(test.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId());
                            if (from.equals(TextConstants.HOME_SAMPLE))
                                MyApplication.getInstance().updateSnackBarValues(price, true, TextConstants.HOME_SAMPLE);

                        }
                    } else if (img_add_subtract.getTag().equals("minus") && from.equals(TextConstants.HOME_SAMPLE)) { //test is unselected from homesample
                        img_add_subtract.setTag("plus");
                        img_add_subtract.setText(mContext.getResources().getString(R.string.Select));

                        List<TestForAppointment> testForAppointments = OrderSummary.getInstance().getOrderist();
                        Iterator<TestForAppointment> iterator = testForAppointments.iterator();
                        while (iterator.hasNext()) {
                            TestForAppointment testForAppointment = iterator.next();
                            if (testHcsId.equals(testForAppointment.getHcsGroups().get(0).getMoHcsList().get(0).getHcsId())) {
                                OrderSummary.getInstance().getOrderist().remove(testForAppointment);
                                OrderSummary.getInstance().getHcsIdList().remove(testHcsId);
                                //  OrderSummary.getInstance().getAppointmentModelOrderList().remove(list.get(adapter_position));
                                break;
                            }

                        }


                        if (from.equals(TextConstants.HOME_SAMPLE) && BuildConfig.isMultipleSelectionEnabled)
                            MyApplication.getInstance().updateSnackBarValues(price, false, TextConstants.HOME_SAMPLE);

                        if (OrderSummary.getInstance().getOrderist().size() == 0) {
                            animable.animate("hide");
                        } else {

                        }
                    }


                    break;

                case R.id.tv_know_more_less:

                  /*  if (tv_know_more_less.getText().equals(mContext.getResources().getString(R.string.txt_know_more))) {
                        //   list.get(adapter_position).setIsExpanded(true);
                        tv_know_more_less.setText(mContext.getResources().getString(R.string.txt_know_less));

                        tv_name.setSingleLine(false);
                        tv_name.setEllipsize(null);

                        tv_desc.setSingleLine(false);
                        tv_desc.setEllipsize(null);
                        tv_test_instruction.setSingleLine(false);
                        tv_test_instruction.setEllipsize(null);

                        if (description != null && !description.trim().equals("") && !description.trim().equals("null")) {
                            tv_desc.setVisibility(View.VISIBLE);
                        } else {
                            tv_desc.setVisibility(View.GONE);
                        }

                    } else if (tv_know_more_less.getText().equals(mContext.getResources().getString(R.string.txt_know_less))) {
                        //    list.get(adapter_position).setIsExpanded(false);
                        tv_know_more_less.setText(mContext.getResources().getString(R.string.txt_know_more));

                        tv_name.setSingleLine(true);
                        tv_name.setEllipsize(TextUtils.TruncateAt.END);

                        tv_desc.setSingleLine(true);
                        tv_desc.setEllipsize(TextUtils.TruncateAt.END);
                        tv_test_instruction.setSingleLine(true);
                        tv_test_instruction.setEllipsize(TextUtils.TruncateAt.END);

                        if (instruction != null && !instruction.trim().equals("") && !instruction.trim().equals("null")) {
                            tv_desc.setVisibility(View.GONE);
                        } else {
                            tv_desc.setVisibility(View.VISIBLE);
                        }
                    }*/

                    break;
            }
        }

    }

    private void updateList() {
        //  list = OrderSummary.getInstance().getOrderist();
        notifyDataSetChanged();
    }


    private void styleAndAnimate(final CustomFontTextView tv_name, final CustomFontTextView textView, final String content, final String insruction, final String instruction_styledString, final String description, final String desc_styledString, final Context mContext) {

        ClickableSpan clickableSpan = new ClickableSpan() {
            boolean isUnderline = false;

            @Override
            public void onClick(View view) {
                if (appendContent.equals(less)) {
                    isExpanded = false;
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                } else if (appendContent.equals(more)) {
                    isExpanded = true;
                    textView.setEllipsize(null);
                }
                styleAndAnimate(tv_name, textView, content, insruction, instruction_styledString, description, desc_styledString, mContext);
            }

            @Override

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(isUnderline);
                ds.setColor(mContext.getResources().getColor(R.color.dusky_blue));
                ds.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            }
        };

        if (isExpanded && content.length() > MAX_LENGTH) {
            tv_name.setSingleLine(false);
            tv_name.setEllipsize(null);

            appendContent = less;
            text = new SpannableString(instruction_styledString + (instruction_styledString.length() == 0 ? "" : "\n\n") + desc_styledString + appendContent);
            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Instructions).length(), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Instructions).length() + 1, instruction_styledString.length(), 0);
            }

            if (description != null && !description.equals(mContext.getResources().getString(R.string.txt_null)) && !description.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), instruction_styledString.length() == 0 ? 0 : instruction_styledString.length() + 2, instruction_styledString.length() == 0 ? mContext.getResources().getString(R.string.Description).length() : instruction_styledString.length() + mContext.getResources().getString(R.string.Description).length() + 2, 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), instruction_styledString.length() + mContext.getResources().getString(R.string.Description).length() + 5, content.length(), 0);
            }

            text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);

        } else if (!isExpanded && content.length() > MAX_LENGTH) {
            tv_name.setSingleLine(true);
            tv_name.setEllipsize(TextUtils.TruncateAt.END);

            String contentTrim = (String) content.substring(0, MAX_LENGTH);
            appendContent = more;
            text = new SpannableString(contentTrim + appendContent);
            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Instructions).length(), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Instructions).length() + 1, text.length(), 0);
            } else {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Description).length(), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Description).length() + 1, text.length(), 0);
            }

            text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);
        }
    }

}
