package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.HcsGroup;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 2/6/2017.
 */

public class SpecialityServicesAdapter extends RecyclerView.Adapter<SpecialityServicesAdapter.SpecialityServicesViewHolder> {
    private Context mContext;
    private List<AppointmentModel> list;
    private List<ServiceGroup> listServicesGroup;
    private String from;
    private String selectedSpeciality;
    private SpecialityServicesAdapter.SpecialityServicesOnClick specialityServicesOnClick;

    SpannableString text, appendString;
    String content;
    String appendContent;
    boolean isExpanded;
    private int MAX_LENGTH = 115;
    private String more,less;

    public interface SpecialityServicesOnClick {
        public void onSpecialityServicesClick(int adapterPosition);
    }

    public SpecialityServicesAdapter(Context context, List<AppointmentModel> practitionerList, String from, SpecialityServicesOnClick specialityServicesOnClick, String selectedSpeciality) {
        this.list = practitionerList;
        mContext = context;
        this.from = from;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityServicesOnClick = specialityServicesOnClick;
    }

    public SpecialityServicesAdapter(List<ServiceGroup> listServicesGroup, Context context, String from, SpecialityServicesOnClick specialityServicesOnClick, String selectedSpeciality) {
        this.listServicesGroup = listServicesGroup;
        mContext = context;
        this.from = from;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityServicesOnClick = specialityServicesOnClick;
    }

    @Override
    public SpecialityServicesAdapter.SpecialityServicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_list_item, null);
        SpecialityServicesAdapter.SpecialityServicesViewHolder viewHolder = new SpecialityServicesAdapter.SpecialityServicesViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialityServicesAdapter.SpecialityServicesViewHolder viewHolder, int position) {

        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            String content, con = "";
            HcsGroup hcsGroup = null;
            hcsGroup = (HcsGroup) list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(position);
            viewHolder.tv_service_title.setText(hcsGroup.getServiceName().toString().trim());

            if (hcsGroup.getMoHcsList().get(0).getInstructions() != null && !hcsGroup.getMoHcsList().get(0).getInstructions().equalsIgnoreCase("") && !hcsGroup.getMoHcsList().get(0).getInstructions().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_null))) {
                content = mContext.getResources().getString(R.string.Instructions_colon) + Html.fromHtml(hcsGroup.getMoHcsList().get(0).getInstructions().toString().trim());
                con = hcsGroup.getMoHcsList().get(0).getInstructions();
            }//viewHolder.tv_instructions.setText("Instructions: "+ Html.fromHtml(hcsGroup.getMoHcsList().get(0).getInstructions().toString().trim()));
            else {
                content = "";
                viewHolder.tv_instructions.setVisibility(View.GONE);
            }
            if (content.length() < MAX_LENGTH) {
                viewHolder.tv_instructions.setText(content);
                isExpanded = false;
                // styleAndAnimate(viewHolder.tv_instructions);
            } else {
                more = MyApplication.getAppContext().getResources().getString(R.string.more);
                less = MyApplication.getAppContext().getResources().getString(R.string.less);
                appendContent = less;
                isExpanded = false;
                styleAndAnimate(viewHolder.tv_instructions, content);
            }
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            String content, con = "";
            ServiceGroup serviceGroup = null;
            serviceGroup = listServicesGroup.get(position);

            viewHolder.tv_service_title.setText(serviceGroup.getServiceName().toString().trim());

            if (serviceGroup.getHcsWithLocationList().get(0).getInstructions() != null && !serviceGroup.getHcsWithLocationList().get(0).getInstructions().equals("") && !serviceGroup.getHcsWithLocationList().get(0).getInstructions().equals(mContext.getResources().getString(R.string.txt_null))) {
                content = mContext.getResources().getString(R.string.Instructions_colon) + Html.fromHtml(serviceGroup.getHcsWithLocationList().get(0).getInstructions().toString().trim());
                con = serviceGroup.getHcsWithLocationList().get(0).getInstructions();
            }//viewHolder.tv_instructions.setText("Instructions: "+ Html.fromHtml(hcsGroup.getMoHcsList().get(0).getInstructions().toString().trim()));
            else {
                content = "";
                viewHolder.tv_instructions.setVisibility(View.GONE);
            }
            if (content.length() < MAX_LENGTH) {
                viewHolder.tv_instructions.setText(content);
                isExpanded = false;
                // styleAndAnimate(viewHolder.tv_instructions);
            } else {
                appendContent = less;
                isExpanded = false;
                styleAndAnimate(viewHolder.tv_instructions, content);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            return (null != list ? list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().size() : 0);
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            return (null != listServicesGroup ? listServicesGroup.size() : 0);
        }
        return 0;
    }


    public class SpecialityServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        CustomFontTextView tv_service_title, tv_instructions;
        CustomFontButton btnSelect;
        FrameLayout temp_view;

        public SpecialityServicesViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            tv_service_title = (CustomFontTextView) view.findViewById(R.id.tv_service_title);
            tv_instructions = (CustomFontTextView) view.findViewById(R.id.tv_instructions);
            btnSelect = (CustomFontButton) view.findViewById(R.id.btnSelect);
            btnSelect.setOnClickListener(this);
            /*tv_instructions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    styleAndAnimate(tv_instructions);
                }
            });*/
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);

        }

        @Override
        public void onClick(View v) {
            CommonControls.closeKeyBoard(mContext);
            specialityServicesOnClick.onSpecialityServicesClick(getAdapterPosition());

        }
    }


    private void styleAndAnimate(final CustomFontTextView tv_instructions, final String content) {


        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (appendContent.equals(less)) {
                    isExpanded = false;
                    tv_instructions.setEllipsize(TextUtils.TruncateAt.END);

                } else if (appendContent.equals(more)) {
                    isExpanded = true;
                    tv_instructions.setEllipsize(null);
                }
                styleAndAnimate(tv_instructions, content);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        if (isExpanded && content.length() > MAX_LENGTH) {
            appendContent = less;
            text = new SpannableString(content + appendContent);
            // text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, content.length(), 0);
            text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dusky_blue)), content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_instructions.setMovementMethod(LinkMovementMethod.getInstance());
            tv_instructions.setText(text);

        } else if (!isExpanded && content.length() > MAX_LENGTH) {
            String contentTrim = content.substring(0, MAX_LENGTH);
            appendContent = more;
            text = new SpannableString(contentTrim + appendContent);
            //   text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, MAX_LENGTH, 0);
            text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dusky_blue)), MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_instructions.setMovementMethod(LinkMovementMethod.getInstance());
            tv_instructions.setText(text);
        }


    }
}
