package appointment.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.ItemOffsetDecoration;

import java.util.List;

import appointment.model.datetimeslotmodel.DateModel;
import appointment.model.datetimeslotmodel.TimeModel;
import appointment.utils.AnimatedExpandableListView;


public class DateExpandAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
    private LayoutInflater inflater;
    private Context mContext;
    private List<DateModel> items;
    private AnimatedExpandableListView expandablelistView;
    private GridAdapter.OnGridChildClickedListener gridChildClickedListener;

    public DateExpandAdapter(Context context, List<DateModel> items, AnimatedExpandableListView expandablelistView, GridAdapter.OnGridChildClickedListener gridChildClickedListener) {
        this.mContext = context;
        this.items = items;
        this.gridChildClickedListener = gridChildClickedListener;
        this.expandablelistView = expandablelistView;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    @Override
    public TimeModel getChild(int groupPosition, int childPosition) {
        return items.get(groupPosition).getTimeModelList().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getRealChildView(int groupstringPosition,
                                 int childPosition, boolean isLastChild, View convertView,
                                 ViewGroup parent) {
        ChildHolder holder;
        if (convertView == null) {
            holder = new ChildHolder();
            convertView = inflater.inflate(R.layout.expandable_list_row, parent, false);
            holder.recyclerGrid = (RecyclerView) convertView.findViewById(R.id.recyclerGrid);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        //   holder.gridView.setAdapter(new GridAdapter(parent.getContext(), items.get(groupstringPosition).getTimeModelList()));

        holder.recyclerGrid.setNestedScrollingEnabled(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, TextConstants.NUM_OF_COLS);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.item_offset);
        holder.recyclerGrid.setLayoutManager(gridLayoutManager);
        //  mRv_Dynamic_Buttons.addItemDecoration(itemDecoration);
        holder.recyclerGrid.setAdapter(new GridAdapter(mContext, items.get(groupstringPosition).getTimeModelList(), groupstringPosition, gridChildClickedListener));
        return convertView;
    }


    private ViewGroup getViewGroupChild(View convertView, ViewGroup parent) {
        if (convertView instanceof ViewGroup) {
            return (ViewGroup) convertView;
        }
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup item = (ViewGroup) inflater.inflate(R.layout.expandable_list_row, null);
        return item;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return items.get(groupPosition).getTimeModelList().size() == 0 ? 0 : 1;
    }

    @Override
    public DateModel getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return items.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        GroupHolder holder;

        // QuestionBean item = items.get(groupPosition);

        if (convertView == null) {
            holder = new GroupHolder();
            convertView = inflater.inflate(R.layout.dateexpand_group_item, parent, false);
            holder.tv_text = (CustomFontTextView) convertView.findViewById(R.id.tv_text);
            holder.tv_no_slot = (CustomFontTextView) convertView.findViewById(R.id.tv_no_slot);
            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }


        String dateText = DateTimeUtil.convertSourceDestinationDate(items.get(groupPosition).getDatre().toString().trim(), DateTimeUtil.dd_MMM_yyyy, DateTimeUtil.dd_MMM_yyyy);
        holder.tv_text.setText(dateText);

        if (items.get(groupPosition).getTimeModelList().size() == 0) {
            holder.tv_no_slot.setVisibility(View.VISIBLE);
        } else {
            holder.tv_no_slot.setVisibility(View.GONE);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }

    private static class ChildHolder {
        RecyclerView recyclerGrid;

    }

    private static class GroupHolder {
        CustomFontTextView tv_text, tv_no_slot;
    }
}
