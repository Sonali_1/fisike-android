package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import appointment.model.appointmodel.Value;
import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 3/2/2016.
 */
public class OrderStatusAdapter extends RecyclerView.Adapter<OrderStatusAdapter.OrderStatusAdapterViewHolder> {
    private Context mContext;
    private List<Value> list;

    public OrderStatusAdapter(Context context, List<Value> list) {
        this.list = list;
        this.mContext = context;
    }

    @Override
    public OrderStatusAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_status_list_item, null);
        OrderStatusAdapterViewHolder viewHolder = new OrderStatusAdapterViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final OrderStatusAdapterViewHolder customViewHolder, final int i) {

        String startdate = "";
        if (i == list.size() - 1)
            customViewHolder.vertical_line.setVisibility(View.GONE);
        //customViewHolder.tv_time.setVisibility(View.GONE);
        customViewHolder.tv_order_status.setText(Utils.capitalizeFirstLetter(list.get(i).getStatus()));
        int length = list.get(i).getActionOn().length();
        String inputDate = list.get(i).getActionOn();

        if ((inputDate.toString().trim().contains("z") || inputDate.toString().trim().contains("Z"))) {
            try {
                Date ds = CommonTasks.fromIsoUtcString(inputDate.toString().trim(), DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS);

                SimpleDateFormat simpleDateFormat;
                if (DateFormat.is24HourFormat(mContext)) {
                    simpleDateFormat = new SimpleDateFormat(DateTimeUtil.HH_mm_comma_d_MMM_yyyy);
                } else {
                    simpleDateFormat = new SimpleDateFormat(DateTimeUtil.hh_mm_aaa_comma_d_MMM_yyyy);
                }

                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                startdate = simpleDateFormat.format(ds);
            } catch (Exception ex) {
            }
        } else {
            try {
                if (DateFormat.is24HourFormat(mContext)) {
                    startdate = CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS, DateTimeUtil.HH_mm_comma_d_MMM_yyyy, inputDate);
                } else {
                    startdate = CommonTasks.formateDateeFromstring(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_SSS, DateTimeUtil.hh_mm_aaa_comma_d_MMM_yyyy, inputDate);
                }
            } catch (Exception ex) {
            }
        }
        customViewHolder.tv_date.setText(startdate);
            /*Date formattedDate=null;
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("d MMM yyyy,hh:mm aaa");
            String strdate = ft.format(date);*/

        //customViewHolder.tv_time.setText(starttime);

       /*
        if (i == 2) {
            Log.i("atlaxman","2");
            customViewHolder.img_status.setVisibility(View.GONE);
            customViewHolder.rel_lyt_status_mark.setBackgroundResource(R.drawable.yellow_circle);
            customViewHolder.vertical_line.setVisibility(View.GONE);
        } else {
            Log.i("atlaxman","other");
            customViewHolder.img_status.setVisibility(View.VISIBLE);
            customViewHolder.rel_lyt_status_mark.setBackgroundResource(R.drawable.green_circle);
            customViewHolder.vertical_line.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class OrderStatusAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        IconTextView img_tick;
        FrameLayout vertical_line;
        TextView tv_order_status, tv_date;

        public OrderStatusAdapterViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_order_status = (TextView) view.findViewById(R.id.tv_order_status);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            //tv_time= (TextView) view.findViewById(R.id.tv_time);
            vertical_line = (FrameLayout) view.findViewById(R.id.vertical_line);
            img_tick = (IconTextView) view.findViewById(R.id.img_tick);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {


                default:
                    break;
            }
        }
    }
}
