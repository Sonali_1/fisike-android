package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.HcsWithLocationList;
import appointment.model.appointmentcommonmodel.MoHcsList;
import appointment.model.appointmentcommonmodel.PractitionerWithLocationList;
import appointment.model.appointmentcommonmodel.ServiceGroup;
import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 2/7/2017.
 */

public class SpecialityLocationsAdapter extends RecyclerView.Adapter<SpecialityLocationsAdapter.SpecialityLocationsViewHolder> {
    private Context mContext;
    private List<AppointmentModel> list;
    private List<ServiceGroup> listServicesGroup;
    private String from;
    private String selectedSpeciality;
    private SpecialityLocationsAdapter.SpecialityLocationsOnClick specialityLocationsOnClick;
    private boolean isDoctorsAvailable;

    SpannableString text, appendString;
    String content;
    String appendContent;
    boolean isExpanded;
    private int MAX_LENGTH = 115;

    private boolean isSkipped = false;
    private int selectedServicePosition;


    public interface SpecialityLocationsOnClick {
        public void onSpecialityLocationsClick(int adapterPosition);
    }

    public SpecialityLocationsAdapter(Context context, List<AppointmentModel> practitionerList, String from, SpecialityLocationsAdapter.SpecialityLocationsOnClick specialityLocationsOnClick, String selectedSpeciality, int selectedServicePosition, boolean isDoctorsAvailable) {
        this.list = practitionerList;
        mContext = context;
        this.from = from;
        this.isDoctorsAvailable = isDoctorsAvailable;
        this.selectedServicePosition = selectedServicePosition;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityLocationsOnClick = specialityLocationsOnClick;
    }

    public SpecialityLocationsAdapter(List<ServiceGroup> listServicesGroup, Context context, String from, SpecialityLocationsAdapter.SpecialityLocationsOnClick specialityLocationsOnClick, String selectedSpeciality, int selectedServicePosition, boolean isDoctorsAvailable) {
        this.listServicesGroup = listServicesGroup;
        mContext = context;
        this.isDoctorsAvailable = isDoctorsAvailable;
        this.from = from;
        this.selectedServicePosition = selectedServicePosition;
        this.selectedSpeciality = selectedSpeciality;
        this.specialityLocationsOnClick = specialityLocationsOnClick;
    }

    @Override
    public SpecialityLocationsAdapter.SpecialityLocationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.locations_list_item, null);
        SpecialityLocationsAdapter.SpecialityLocationsViewHolder viewHolder = new SpecialityLocationsAdapter.SpecialityLocationsViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialityLocationsAdapter.SpecialityLocationsViewHolder viewHolder, int position) {

        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            MoHcsList moHcsList = null;
            if (!isSkipped) {
                moHcsList = (MoHcsList) list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().get(position);
                viewHolder.tv_location1.setText(moHcsList.getLocationName().toString().trim());
                viewHolder.tv_location2.setVisibility(View.GONE);
                viewHolder.tv_location3.setText(moHcsList.getLocationAddress().toString().trim());
            } else {
                viewHolder.tv_location1.setText(moHcsList.getLocationName().toString().trim());
                viewHolder.tv_location2.setVisibility(View.VISIBLE);
                viewHolder.tv_location3.setText(moHcsList.getLocationAddress().toString().trim());
            }
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            if (isDoctorsAvailable) {
                PractitionerWithLocationList practitionerWithLocationList = listServicesGroup.get(0).getMoPractitionerInfoList().get(selectedServicePosition).getPractitionerWithLocationList().get(position);
                viewHolder.tv_location1.setText(practitionerWithLocationList.getLocationName().toString().trim());
                viewHolder.tv_location2.setVisibility(View.GONE);

                if (practitionerWithLocationList.getLocationAddres() != null && !practitionerWithLocationList.getLocationAddres().equals(mContext.getResources().getString(R.string.txt_null)) && !practitionerWithLocationList.getLocationAddres().equals("")) {
                    viewHolder.tv_location3.setText(practitionerWithLocationList.getLocationAddres().toString().trim());
                } else {
                    viewHolder.tv_location3.setVisibility(View.GONE);
                }
            } else if (!isDoctorsAvailable) {
                HcsWithLocationList hcsWithLocationList = listServicesGroup.get(0).getHcsWithLocationList().get(position);
                viewHolder.tv_location1.setText(hcsWithLocationList.getLocationName().toString().trim());
                viewHolder.tv_location2.setVisibility(View.GONE);

                if (hcsWithLocationList.getLocationAddress() != null && !hcsWithLocationList.getLocationAddress().equals(mContext.getResources().getString(R.string.txt_null)) && !hcsWithLocationList.getLocationAddress().equals("")) {
                    viewHolder.tv_location3.setText(hcsWithLocationList.getLocationAddress().toString().trim());
                } else {
                    viewHolder.tv_location3.setVisibility(View.GONE);
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        if (selectedSpeciality.equals(TextConstants.SPECIALITY_DOCTORS)) {
            if (!isSkipped) {
                return (null != list ? list.get(0).getSpecialtyForAppointment().getPractitionerForAppointment().getHcsGroups().get(selectedServicePosition).getMoHcsList().size() : 0);
            } else {
                return 0;
            }
        } else if (selectedSpeciality.equals(TextConstants.SPECIALITY_SERVICES)) {
            if (isDoctorsAvailable) {
                return (null != listServicesGroup ? listServicesGroup.get(0).getMoPractitionerInfoList().get(selectedServicePosition).getPractitionerWithLocationList().size() : 0);
            } else if (!isDoctorsAvailable) {
                return (null != listServicesGroup ? listServicesGroup.get(0).getHcsWithLocationList().size() : 0);
            }
        }
        return 0;
    }


    class SpecialityLocationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        CustomFontTextView tv_location1, tv_location2, tv_location3;
        CustomFontButton btnSelect;
        FrameLayout temp_view;

        public SpecialityLocationsViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_location1 = (CustomFontTextView) view.findViewById(R.id.tv_location1);
            tv_location2 = (CustomFontTextView) view.findViewById(R.id.tv_location2);
            tv_location3 = (CustomFontTextView) view.findViewById(R.id.tv_location3);
            btnSelect = (CustomFontButton) view.findViewById(R.id.btnSelect);
            btnSelect.setOnClickListener(this);
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);

        }

        @Override
        public void onClick(View v) {
            CommonControls.closeKeyBoard(mContext);
            specialityLocationsOnClick.onSpecialityLocationsClick(getAdapterPosition());

        }
    }

}
