package appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;

import java.util.List;
import java.util.Locale;

import appointment.FragmentBaseActivity;
import appointment.interfaces.Animable;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 3/29/2016.
 */
public class SpecialityAdapter extends RecyclerView.Adapter<SpecialityAdapter.SpecialityViewHolder> {
    private Context mContext;
    private List<SpecialityDataModel> list;
    boolean isSearch;
    private String from;
    private String searchedString;

    public SpecialityAdapter(Context context, List<SpecialityDataModel> practitionerList, Animable animable, String from, boolean isSearch) {
        this.list = practitionerList;
        mContext = context;
        this.isSearch = isSearch;
        this.from = from;
    }

    @Override
    public SpecialityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.speciality_search_list_item, null);
        SpecialityViewHolder viewHolder = new SpecialityViewHolder(view, mContext);
        //setViewHolder(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialityViewHolder viewHolder, int position) {
        SpecialityDataModel specialityDataModel = (SpecialityDataModel) list.get(position);
        String specialityName = specialityDataModel.getServiceTypeName().toString().trim();
        viewHolder.tv_speciality_name.setText(specialityName);
        if (position == 0 && !isSearch) {
            viewHolder.rel_lyt_recent_popular.setVisibility(View.VISIBLE);
            viewHolder.temp_view.setVisibility(View.GONE);
        } else {
            if (position == 0) {
                viewHolder.temp_view.setVisibility(View.VISIBLE);
            } else {
                viewHolder.temp_view.setVisibility(View.GONE);
            }
            viewHolder.rel_lyt_recent_popular.setVisibility(View.GONE);
        }

        if (isSearch && searchedString != null) {
            if (specialityName.toLowerCase().matches("(.*)" + searchedString.toLowerCase() + "(.*)")) {

                int startPos = specialityName.toLowerCase(Locale.US).indexOf(searchedString.toLowerCase(Locale.US));
                int endPos = startPos + searchedString.length();
                if (startPos != -1) // This should always be true, just a sanity check
                {
                    Spannable spannable = new SpannableString(specialityName);
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{ContextCompat.getColor(mContext, R.color.dusky_blue)});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);

                    spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolder.tv_speciality_name.setText(spannable);
                } else {
                    viewHolder.tv_speciality_name.setText(specialityName);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public void setSearchedString(String searchedString) {
        this.searchedString = searchedString;
        notifyDataSetChanged();
    }


    public class SpecialityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_speciality_name;
        RelativeLayout rel_lyt_recent_popular;
        FrameLayout temp_view;

        public SpecialityViewHolder(View itemView, Context mContext) {

            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            temp_view = (FrameLayout) itemView.findViewById(R.id.temp_view);
            tv_speciality_name = (TextView) itemView.findViewById(R.id.tv_speciality_name);
            rel_lyt_recent_popular = (RelativeLayout) itemView.findViewById(R.id.rel_lyt_recent_popular);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            CommonControls.closeKeyBoard(mContext);

            Bundle bundle = new Bundle();
            bundle.putParcelable("specialitymodel", list.get(adapter_position));
            bundle.putString(TextConstants.TEST_TYPE, from);
            Intent intent = new Intent(mContext, FragmentBaseActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
        }
    }
}
