package appointment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 1/30/2017.
 */

public class SpecialityTypeAdapter extends RecyclerView.Adapter<SpecialityTypeAdapter.SpecialityTypeViewHolder> {
    private Context mContext;
    private List<String> list;
    boolean isSearch;
    private String from;
    private String searchedString;
    SpecialityOnClick specialityOnClick;


    public interface SpecialityOnClick {
        public void onSpecialityTypeClick(int adapterPosition);
    }

    public SpecialityTypeAdapter(Context context, List<String> list, String from, SpecialityOnClick specialityOnClick) {
        this.list = list;
        mContext = context;
        this.specialityOnClick = specialityOnClick;
        this.from = from;
    }

    @Override
    public SpecialityTypeAdapter.SpecialityTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.speciality_type_list_item, null);
        SpecialityTypeAdapter.SpecialityTypeViewHolder viewHolder = new SpecialityTypeAdapter.SpecialityTypeViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialityTypeAdapter.SpecialityTypeViewHolder viewHolder, int position) {
        String specialityType = list.get(position);

        String servicesType = "";

        if (specialityType.equals(TextConstants.SPECIALITY_DOCTORS)) {
            servicesType = mContext.getResources().getString(R.string.doctor);
        } else if (specialityType.equals(TextConstants.SPECIALITY_SERVICES)) {
            servicesType = mContext.getResources().getString(R.string.Services);
        }

        viewHolder.tv_speciality_type.setText(servicesType);


        switch (position) {
            case 0:
                viewHolder.tv_speciality_related_info.setText(mContext.getResources().getString(R.string.Find_physician_related_selected_specialty));
                viewHolder.temp_view.setVisibility(View.VISIBLE);
                break;


            case 1:
                viewHolder.tv_speciality_related_info.setText(mContext.getResources().getString(R.string.Find_service_related_the_selected_specialty));
                viewHolder.temp_view.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public void setSearchedString(String searchedString) {
        this.searchedString = searchedString;
    }


    public class SpecialityTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView tv_speciality_type, tv_speciality_related_info;
        FrameLayout temp_view;

        public SpecialityTypeViewHolder(View itemView, Context mContext) {

            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            tv_speciality_type = (CustomFontTextView) itemView.findViewById(R.id.tv_speciality_type);
            tv_speciality_related_info = (CustomFontTextView) itemView.findViewById(R.id.tv_speciality_related_info);
            temp_view = (FrameLayout) itemView.findViewById(R.id.temp_view);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            CommonControls.closeKeyBoard(mContext);
            specialityOnClick.onSpecialityTypeClick(adapter_position);
        }
    }
}
