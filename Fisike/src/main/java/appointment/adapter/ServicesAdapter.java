package appointment.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.CustomLinkMovementMethod;

import appointment.FragmentBaseActivity;
import appointment.fragment.SelectLocationsFragment;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.appointmentcommonmodel.HcsGroup;
import appointment.utils.CustomTypefaceSpan;

/**
 * Created by laxmansingh on 11/10/2016.
 */

public class ServicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private java.util.List<AppointmentModel> list;
    private String from;

    /*[  .....loadmore variables needed.....   ]*/
    private final int VIEW_TYPE_DOC_SERVICES = 0;
    private final int VIEW_TYPE = 1;
    /*[  .....End of loadmore variables needed.....   ]*/

    SpannableString text, appendString;
    String content;
    String appendContent;
    boolean isExpanded;
    private int MAX_LENGTH = 50;
    private String more, less;
    private Typeface font_bold, font_semibold;

    public ServicesAdapter(Context context, final java.util.List<AppointmentModel> list, String from) {
        this.list = list;
        this.mContext = context;
        this.from = from;
        font_bold = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_bold));
        font_semibold = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_regular));
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_DOC_SERVICES) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_list_item, null);
            ServicesAdapter.DocServicesViewHolder viewHolder = new ServicesAdapter.DocServicesViewHolder(view, mContext);
            return viewHolder;
        } else if (viewType == VIEW_TYPE) {
            View lodingView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_loading_item, null);
            ServicesAdapter.LoadingViewHolder loadingViewHolder = new ServicesAdapter.LoadingViewHolder(lodingView, mContext);
            return loadingViewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int i) {
        if (customViewHolder instanceof ServicesAdapter.DocServicesViewHolder) {
            String desc_styledString = "", instruction_styledString = "";
            HcsGroup hcsGroup = null;
            if (from.equals(TextConstants.DOCTORS)) {
                hcsGroup = (HcsGroup) list.get(0).getPractitionerForAppointment().getHcsGroups().get(i);
            }
            ServicesAdapter.DocServicesViewHolder viewHolder = (ServicesAdapter.DocServicesViewHolder) customViewHolder;
            viewHolder.tv_service_title.setText(hcsGroup.getServiceName().toString().trim());


            String insruction = hcsGroup.getMoHcsList().get(0).getInstructions();
            String description = hcsGroup.getMoHcsList().get(0).getDescription();

            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                instruction_styledString = new String(mContext.getResources().getString(R.string.Instructions) + Html.fromHtml(insruction.trim()));
            }

            if (description != null && !description.equals(mContext.getResources().getString(R.string.txt_null)) && !description.equals("")) {
                desc_styledString = new String(mContext.getResources().getString(R.string.Description) + Html.fromHtml(description.trim()));
            }

            String content = new String(instruction_styledString + (instruction_styledString.length() == 0 ? "" : "\n\n") + desc_styledString);

            if (content.length() < MAX_LENGTH) {
                SpannableString text = new SpannableString(content);
                if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                    text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Instructions).length() - 1, 0);
                    text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Instructions).length(), text.length(), 0);
                } else if (content.length() > 12) {
                    text.setSpan(new CustomTypefaceSpan("", font_bold), 0, mContext.getResources().getString(R.string.Description).length() - 1, 0);
                    text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Description).length(), text.length(), 0);
                }

                viewHolder.tv_instructions.setText(text);
                isExpanded = false;
            } else {
                more = MyApplication.getAppContext().getResources().getString(R.string.more);
                less = MyApplication.getAppContext().getResources().getString(R.string.less);
                appendContent = less;
                isExpanded = false;
                styleAndAnimate((CustomFontTextView) viewHolder.tv_instructions, content, insruction, instruction_styledString, description, desc_styledString, mContext);
            }




          /*  if (hcsGroup.getMoHcsList().get(0).getInstructions() != null && !hcsGroup.getMoHcsList().get(0).getInstructions().equalsIgnoreCase("")) {
                content = mContext.getResources().getString(R.string.Instructions_colon) + Html.fromHtml(hcsGroup.getMoHcsList().get(0).getInstructions().toString().trim());
                con = hcsGroup.getMoHcsList().get(0).getInstructions();
            }//viewHolder.tv_instructions.setText("Instructions: "+ Html.fromHtml(hcsGroup.getMoHcsList().get(0).getInstructions().toString().trim()));
            else {
                content = "";
                viewHolder.tv_instructions.setVisibility(View.GONE);
            }
            if (content.length() < MAX_LENGTH) {
                viewHolder.tv_instructions.setText(content);
                isExpanded = false;
                // styleAndAnimate(viewHolder.tv_instructions);
            } else {
                more = MyApplication.getAppContext().getResources().getString(R.string.more);
                less = MyApplication.getAppContext().getResources().getString(R.string.less);
                appendContent = less;
                isExpanded = false;
                styleAndAnimate(viewHolder.tv_instructions, content);
            }*/

        } else if (customViewHolder instanceof ServicesAdapter.LoadingViewHolder) {
            ServicesAdapter.LoadingViewHolder loadingViewHolder = (ServicesAdapter.LoadingViewHolder) customViewHolder;
        }
    }


    /*  private void styleAndAnimate(final CustomFontTextView tv_instructions, final String content) {
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (appendContent.equals(less)) {
                        isExpanded = false;
                        tv_instructions.setEllipsize(TextUtils.TruncateAt.END);

                    } else if (appendContent.equals(more)) {
                        isExpanded = true;
                        tv_instructions.setEllipsize(null);
                    }
                    styleAndAnimate(tv_instructions, content);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };

            if (isExpanded && content.length() > MAX_LENGTH) {
                appendContent = less;
                text = new SpannableString(content + appendContent);
                // text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, content.length(), 0);
                text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dusky_blue)), content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_instructions.setMovementMethod(LinkMovementMethod.getInstance());
                tv_instructions.setText(text);

            } else if (!isExpanded && content.length() > MAX_LENGTH) {
                String contentTrim = content.substring(0, MAX_LENGTH);
                appendContent = more;
                text = new SpannableString(contentTrim + appendContent);
                //   text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, MAX_LENGTH, 0);
                text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dusky_blue)), MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_instructions.setMovementMethod(LinkMovementMethod.getInstance());
                tv_instructions.setText(text);
            }


        }*/
    @Override
    public int getItemCount() {
        if (from.equals(TextConstants.DOCTORS)) {
            return (null != list ? list.get(0).getPractitionerForAppointment().getHcsGroups().size() : 0);
        } else {
            return list.size();
        }
    }


    public class DocServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_service_title, tv_instructions;
        CustomFontButton btnSelect;
        FrameLayout temp_view;

        public DocServicesViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            tv_service_title = (CustomFontTextView) view.findViewById(R.id.tv_service_title);
            tv_instructions = (CustomFontTextView) view.findViewById(R.id.tv_instructions);
            btnSelect = (CustomFontButton) view.findViewById(R.id.btnSelect);
            btnSelect.setOnClickListener(this);
            /*tv_instructions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    styleAndAnimate(tv_instructions);
                }
            });*/
            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);

        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {

                case R.id.btnSelect:
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("appointmentmodel", list.get(0));
                    bundle.putString(TextConstants.TEST_TYPE, from);
                    bundle.putInt("selectedservice", adapter_position);
                    bundle.putBoolean("isskipped", false);
                    SelectLocationsFragment locationsFragment = new SelectLocationsFragment();
                    locationsFragment.setArguments(bundle);
                    ((FragmentBaseActivity) mContext).getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentParentView, locationsFragment, "selectlocationsfragment")
                            .addToBackStack(null)
                            .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit();
                    break;

                default:
                    break;
            }
        }


    }


    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        Context context;

        public LoadingViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarLoading);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_DOC_SERVICES;
    }


    private void styleAndAnimate(final CustomFontTextView textView, final String content, final String insruction, final String instruction_styledString, final String description, final String desc_styledString, final Context mContext) {

        ClickableSpan clickableSpan = new ClickableSpan() {
            boolean isUnderline = false;

            @Override
            public void onClick(View view) {
                if (appendContent.equals(less)) {
                    isExpanded = false;
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                } else if (appendContent.equals(more)) {
                    isExpanded = true;
                    textView.setEllipsize(null);
                }
                styleAndAnimate(textView, content, insruction, instruction_styledString, description, desc_styledString, mContext);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(isUnderline);
                ds.setColor(mContext.getResources().getColor(R.color.dusky_blue));
                ds.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            }
        };

        if (isExpanded && content.length() > MAX_LENGTH) {

            appendContent = less;

            text = new SpannableString(instruction_styledString + (instruction_styledString.length() == 0 ? "" : "\n\n") + desc_styledString + appendContent);
            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, (mContext.getResources().getString(R.string.Instructions).length() - 1), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), (mContext.getResources().getString(R.string.Instructions).length()), instruction_styledString.length(), 0);
            }

            if (description != null && !description.equals(mContext.getResources().getString(R.string.txt_null)) && !description.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold),
                        instruction_styledString.length() == 0 ?
                                0 : instruction_styledString.length() + 2,
                        instruction_styledString.length() == 0 ?
                                (mContext.getResources().getString(R.string.Description).length() - 1)
                                : (instruction_styledString.length() + (mContext.getResources().getString(R.string.Description).length()) + 1), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), (instruction_styledString.length() + (mContext.getResources().getString(R.string.Description).length())), content.length(), 0);
            }

            text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);

        } else if (!isExpanded && content.length() > MAX_LENGTH) {

            String contentTrim = (String) content.substring(0, MAX_LENGTH);
            appendContent = more;
            text = new SpannableString(contentTrim + appendContent);
            if (insruction != null && !insruction.equals(mContext.getResources().getString(R.string.txt_null)) && !insruction.equals("")) {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, (mContext.getResources().getString(R.string.Instructions).length() - 1), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Instructions).length(), text.length(), 0);
            } else {
                text.setSpan(new CustomTypefaceSpan("", font_bold), 0, (mContext.getResources().getString(R.string.Description).length() - 1), 0);
                text.setSpan(new CustomTypefaceSpan("", font_semibold), mContext.getResources().getString(R.string.Description).length(), text.length(), 0);
            }

            text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);
        }
    }


}
