package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

/**
 * Created by laxmansingh on 9/23/2017.
 */

public class PatientNameAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> list;
    LayoutInflater mInflater;

    public PatientNameAdapter(Context context, List<String> list) {
        super(context, 0, list);
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PatientNameAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_patientname_item, null);
            holder = new PatientNameAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView.findViewById(R.id.tv_item);
            convertView.setTag(holder);
        } else {
            holder = (PatientNameAdapter.ViewHolder) convertView.getTag();
        }

        holder.txt_item.setText(list.get(position).toString().trim());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_patientname_dropdown, null);
        }
        CustomFontTextView text1 = (CustomFontTextView) convertView.findViewById(R.id.txt_item);
        text1.setText(list.get(position).toString().trim());

        return convertView;
    }

    static class ViewHolder {
        TextView txt_item;
    }
}

