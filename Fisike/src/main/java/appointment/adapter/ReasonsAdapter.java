package appointment.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.List;

/**
 * Created by laxmansingh on 9/24/2017.
 */

public class ReasonsAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> list;
    LayoutInflater mInflater;
    private String isDateOrTime;
    private Typeface font_regular, font_semibold;

    public ReasonsAdapter(Context context, List<String> list) {
        super(context, 0, list);
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        font_semibold = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_semibold));
        font_regular = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PatientNameAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_patientname_item, null);
            holder = new PatientNameAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView.findViewById(R.id.tv_item);
            convertView.setTag(holder);
        } else {
            holder = (PatientNameAdapter.ViewHolder) convertView.getTag();
        }

        if (position==0){
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.navy_blue_opacity_80));
        }else {
            holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        }

        holder.txt_item.setText(list.get(position).toString().trim());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_patientname_dropdown, null);
        }
        CustomFontTextView text1 = (CustomFontTextView) convertView.findViewById(R.id.txt_item);

        if (position == 0) {
            text1.setTextSize(16);
            text1.setTypeface(font_semibold);
            text1.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        } else {
            text1.setTextSize(14);
            text1.setTypeface(font_regular);
            text1.setTextColor(context.getResources().getColor(R.color.navy_blue_opacity_69));
        }

        text1.setText(list.get(position).toString().trim());

        return convertView;
    }

    static class ViewHolder {
        TextView txt_item;
    }
}

