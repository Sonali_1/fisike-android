package appointment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import appointment.fragment.PatientInformationFragment;
import appointment.fragment.RescheduleFragment;
import appointment.fragment.SelectLocationsFragment;
import appointment.fragment.SelectServicesFragment;
import appointment.fragment.SelectSpecialityTypeFragment;
import appointment.fragment.SelectTimeFragment;
import appointment.fragment.SpecialityDoctorFragment;
import appointment.fragment.SpecialityLocationsFragment;
import appointment.fragment.SpecialityServicesFragment;
import appointment.fragment.SuccessFragment;
import appointment.model.OrderSummary;
import careplan.activity.CareTaskActivity;
import careplan.activity.CareTaskAppointment;
import careplan.fragments.CareTaskActionFragment;

/**
 * Created by laxmansingh on 4/1/2016.
 */
public class FragmentBaseActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private static CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    public static List<Fragment> list_fragments = new ArrayList<Fragment>();
    //ImageButton bt_toolbar_cross;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.homesamplecollectionactivity, frameLayout);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("rebook_or_reschedule")) {
            if (bundle.getString("rebook_or_reschedule").equals(TextConstants.RESCHEDULE)) {
                RescheduleFragment rescheduleFragment = new RescheduleFragment();
                rescheduleFragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, rescheduleFragment, "reschedulefragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        } else {
            if (bundle.getString(TextConstants.TEST_TYPE).equals(TextConstants.DOCTORS)) {
                SelectServicesFragment servicesFragment = new SelectServicesFragment();
                servicesFragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, servicesFragment, "selectservicesfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            } else if (bundle.getString(TextConstants.TEST_TYPE).equals(TextConstants.HOME_SAMPLE) || bundle.getString(TextConstants.TEST_TYPE).equals(TextConstants.LAB_TEST)) {
                SelectLocationsFragment locationsFragment = new SelectLocationsFragment();
                locationsFragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, locationsFragment, "selectlocationsfragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            } else if (bundle.getString(TextConstants.TEST_TYPE).equals(TextConstants.SPECIALITY)) {
                SelectSpecialityTypeFragment specialityTypeFragment = new SelectSpecialityTypeFragment();
                specialityTypeFragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentView, specialityTypeFragment, "selectspecialitytypefragment")
                        .addToBackStack(null)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        }
    }


    @Override
    public void onBackPressed() {
        Utils.hideKeyboard(FragmentBaseActivity.this);
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            /*[   Think if the fragment is successfragment we hav to finish otherwise traverse backstack of fragments on backpressed  ]*/
            Fragment currFragment = null;
            if (list_fragments.size() > 0) {
                list_fragments.remove(list_fragments.size() - 1);
                if (list_fragments.size() > 0) {
                    currFragment = list_fragments.get(list_fragments.size() - 1);
                }
            }

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
            if (currentFragment instanceof SuccessFragment || currentFragment instanceof RescheduleFragment) {
                OrderSummary.getInstance().setOrderist(null);
                OrderSummary.getInstance().setAppointmentModelOrderList(null);
                OrderSummary.getInstance().setTotalPrice(0);
                OrderSummary.getInstance().setTestNumber(0);
                if (currentFragment instanceof SuccessFragment)


                    if (CareTaskAppointment.careTaskAppointmentActivity != null && CareTaskAppointment.isSuccessful) {
                        for (int i = 0; i < CareTaskAppointment.listAppointmentActivity.size(); i++) {
                            CareTaskAppointment.listAppointmentActivity.get(i).finish();
                        }
                        CareTaskAppointment.listAppointmentActivity.clear();

                    } else {
                        startActivity(new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }

                finish();
            } else {

                if (currentFragment instanceof PatientInformationFragment && currentFragment.isVisible()) {
                    setToolbarTitleText(getResources().getString(R.string.title_select_time));
                }

                if (currentFragment instanceof SelectTimeFragment && currentFragment.isVisible()) {
                    setToolbarTitleText(getResources().getString(R.string.title_select_location));
                    OrderSummary.getInstance().setOrderist(null);
                    OrderSummary.getInstance().setAppointmentModelOrderList(null);
                    OrderSummary.getInstance().setTotalPrice(0);
                    OrderSummary.getInstance().setTestNumber(0);

                }

                if (currentFragment instanceof SelectLocationsFragment && currentFragment.isVisible()) {
                    setToolbarTitleText(getResources().getString(R.string.services_title));
                    OrderSummary.getInstance().setOrderist(null);
                    OrderSummary.getInstance().setAppointmentModelOrderList(null);
                    OrderSummary.getInstance().setTotalPrice(0);
                    OrderSummary.getInstance().setTestNumber(0);
                }

                if (currentFragment instanceof SelectServicesFragment && currentFragment.isVisible()) {
                    setToolbarTitleText(getResources().getString(R.string.services_title));
                }

                if (currFragment != null && currFragment instanceof SelectSpecialityTypeFragment) {
                    setToolbarTitleText(getResources().getString(R.string.specialities));
                }
                if (currFragment != null && currFragment instanceof SpecialityDoctorFragment) {
                    setToolbarTitleText(getResources().getString(R.string.doctor));
                }

                if (currFragment != null && currFragment instanceof SpecialityServicesFragment) {
                    setToolbarTitleText(getResources().getString(R.string.Services));
                }

                if (currFragment != null && currFragment instanceof SpecialityLocationsFragment) {
                    setToolbarTitleText(getResources().getString(R.string.locations));
                }

                getSupportFragmentManager().popBackStack();

            }
/*[   End of Thinking if the fragment is successfragment we have to finish otherwise traverse backstack of fragments on backpressed  ]*/
        } else {
            list_fragments.clear();
            finish();

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public static void setToolbarTitleText(String title) {
        toolbar_title.setText(title.trim());
    }


    public void executeUpdateProfileResult(updatePatientResponse responseGson, String result) {

        if (fragment != null && fragment instanceof PatientInformationFragment)
            ((PatientInformationFragment) fragment).updateProfileResponse(responseGson, result);
    }
}
