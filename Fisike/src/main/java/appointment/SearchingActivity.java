package appointment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import appointment.adapter.HomeSampleCollectionAdapter;
import appointment.adapter.PractitionerAdapter;
import appointment.adapter.SpecialityAdapter;
import appointment.interfaces.Animable;
import appointment.model.AppointmentSearchConstraintCommon;
import appointment.model.OrderSummary;
import appointment.model.appointmentcommonmodel.AppointmentModel;
import appointment.model.specialitymodel.SpecialityDataModel;
import appointment.request.AppointmentSearchRequestCommon;
import appointment.request.AppointmentSearchResponseCommon;
import appointment.utils.AnimUtils;
import appointment.utils.CommonControls;
import appointment.utils.OnListItemSelected;
import careplan.activity.CareTaskAppointment;

/**
 * Created by laxmansingh on 11/8/2016.
 */

public class SearchingActivity extends BaseActivity implements View.OnClickListener, Filterable, Animable, OnListItemSelected {

    private Context mContext;
    private Animable animable;
    private IconTextView mBtn_back;
    private IconTextView mBtnCross;
    private EditText mEt_search;
    private ProgressBar mSearch_progress_bar;

    private RelativeLayout card_rv;
    private RelativeLayout mNo_data_lyt;
    private ImageView mNo_data_image;
    private CustomFontTextView mTv_no_data_msg;
    private CustomFontButton mBtnAddCondition;


    private RecyclerView mRv_search;
    private int position;
    private String from;
    private String SEARCH_URL_STRING;


    /*[ Doctors Fields ]*/
    private List<AppointmentModel> appointmentModelList = new ArrayList<AppointmentModel>();
    private PractitionerAdapter mAdapterdoctors_search;
    /*[ Doctors Fields ]*/

    /*[ Speciality Fields ]*/
    private List<SpecialityDataModel> specialitylist = new ArrayList<SpecialityDataModel>();
    private SpecialityAdapter mAdapterSpeciality_search;
    /*[ Speciality Fields ]*/

    /*[ Tests or Lab Tests Fields ]*/
    private HomeSampleCollectionAdapter mAdapterHomeSampleCollection_search;
    private FrameLayout frameLayout;
    private Toolbar mToolbar;
    private boolean API_RETURNED_NULL;
    private View footerView;
    private TextView tv_numberOfTests;
    private TextView tvTotalCost;
    private Button btProceed;
    private List<AppointmentModel> appointmentModel;
    private String isShowingSnackbar = "hide";
    /*[ Tests or Lab Tests Fields ]*/

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.isMultipleSelectionEnabled) {
            if (from.equals(TextConstants.HOME_SAMPLE)) {

                if (OrderSummary.getInstance().getOrderist().size() >= 1) {
                    animable.animate("show");
                    mAdapterHomeSampleCollection_search.notifyDataSetChanged();
                } else if (OrderSummary.getInstance().getOrderist().size() == 0) {
                    if (appointmentModel != null && from.equals(TextConstants.LAB_TEST)) {
                        mEt_search.setText("");
                        appointmentModelList.clear();
                        appointmentModel.clear();
                        mAdapterHomeSampleCollection_search.notifyDataSetChanged();
                    }
                    mAdapterHomeSampleCollection_search.notifyDataSetChanged();
                    animable.animate("hide");
                }
                if (from.equals(TextConstants.HOME_SAMPLE)) {
                    handleSelection(from, false);
                }
            }
        } else {
            if (from.equals(TextConstants.HOME_SAMPLE) && appointmentModel != null) {

                mEt_search.setText("");
                appointmentModelList.clear();
                appointmentModel.clear();
                mAdapterHomeSampleCollection_search.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CareTaskAppointment.careTaskAppointmentActivity != null) {
            CareTaskAppointment.listAppointmentActivity.add(this);
        }
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.searchingactivity_appointment, frameLayout);
        showHideToolbar(false);
        mContext = this;
        animable = this;

        Intent intent = getIntent();
        from = intent.getStringExtra(TextConstants.FROM);
        findView();
        bindView();
        initView();
        mEt_search.setSelection(mEt_search.getText().toString().trim().length());

        if (from.equals(TextConstants.DOCTORS)) {
            SEARCH_URL_STRING = MphRxUrl.getFetchBookablePractitionerUrl();
        } else if (from.equals(TextConstants.SPECIALITY)) {
            SEARCH_URL_STRING = MphRxUrl.getfetchBookableSpecialtiesUrl();
        } else if (from.equals(TextConstants.HOME_SAMPLE) || (from.equals(TextConstants.LAB_TEST))) {
            SEARCH_URL_STRING = MphRxUrl.getFetchBookableUrl();
        }
    }


    public String loadJSONFromAsset() {
        String json = null;
        InputStream is;
        try {
            is = mContext.getAssets().open("appointment.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void findView() {
        Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.opensans_ttf_semibold));

        card_rv = (RelativeLayout) findViewById(R.id.card_rv);
        mBtn_back = (IconTextView) findViewById(R.id.btn_back);

        if (Utils.isRTL(this)) {
            mBtn_back.setRotationY(180);
        } else {
            mBtn_back.setRotationY(0);
        }
        mBtnCross = (IconTextView) findViewById(R.id.btnCross);
        mEt_search = (EditText) findViewById(R.id.et_search);
        mEt_search.setTypeface(font);

        mSearch_progress_bar = (ProgressBar) findViewById(R.id.search_progress_bar);
        mRv_search = (RecyclerView) findViewById(R.id.rv_search);


        mNo_data_lyt = (RelativeLayout) findViewById(R.id.no_data_lyt);
        mNo_data_image = (ImageView) findViewById(R.id.no_data_image);
        mTv_no_data_msg = (CustomFontTextView) findViewById(R.id.tv_no_data_msg);
        mBtnAddCondition = (CustomFontButton) findViewById(R.id.btnAddMedicine);
        footerView = findViewById(R.id.snackbar);
        tv_numberOfTests = (TextView) footerView.findViewById(R.id.tv_total_number);
        tvTotalCost = (TextView) footerView.findViewById(R.id.tv_total_bill);
        btProceed = (Button) footerView.findViewById(R.id.btn_proceed);

    }

    private void bindView() {
        if (BuildConfig.isMultipleSelectionEnabled)
            MyApplication.getInstance().registerForListSelection(this);
        mBtn_back.setOnClickListener(this);
        mBtnCross.setOnClickListener(this);
        btProceed.setOnClickListener(this);
        mRv_search.setLayoutManager(new LinearLayoutManager(mContext));


        if (from.equals(TextConstants.DOCTORS)) {
            mAdapterdoctors_search = new PractitionerAdapter(mContext, appointmentModelList, animable, from, true);
            mRv_search.setAdapter(mAdapterdoctors_search);
        } else if (from.equals(TextConstants.SPECIALITY)) {
            mAdapterSpeciality_search = new SpecialityAdapter(mContext, specialitylist, animable, from, true);
            mRv_search.setAdapter(mAdapterSpeciality_search);
        }
        if (from.equals(TextConstants.HOME_SAMPLE) || (from.equals(TextConstants.LAB_TEST))) {
            mAdapterHomeSampleCollection_search = new HomeSampleCollectionAdapter(mContext, appointmentModelList, animable, from, true);
            mRv_search.setAdapter(mAdapterHomeSampleCollection_search);
        }


        mEt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((s.toString().length()) % 3 != 0 || s.toString().trim().length() == 0) {
                    mSearch_progress_bar.setVisibility(View.GONE);
                    mBtnCross.setVisibility(View.VISIBLE);
                    setUIAndData();
                    notifyRVAdapter();
                } else {
                    getFilter().filter(s.toString());
                }

                if (from.equals(TextConstants.DOCTORS)) {
                    mAdapterdoctors_search.setSearchedString(s.toString());
                } else if (from.equals(TextConstants.SPECIALITY)) {
                    mAdapterSpeciality_search.setSearchedString(s.toString());
                } else if (from.equals(TextConstants.HOME_SAMPLE) || (from.equals(TextConstants.LAB_TEST))) {
                    mAdapterHomeSampleCollection_search.setSearchedString(s.toString());
                }
            }
        });

    }

    private void initView() {
        if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.HOME_SAMPLE))
            mEt_search.setHint(mContext.getResources().getString(R.string.search_by_name));
        else if (from.equals(TextConstants.SPECIALITY))
            mEt_search.setHint(mContext.getResources().getString(R.string.search_by_speciality));
        else if (from.equals(TextConstants.DOCTORS))
            mEt_search.setHint(mContext.getResources().getString(R.string.search_by_doctor));
        mNo_data_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_condition));
        mBtnAddCondition.setText(mContext.getResources().getString(R.string.add_contact));
        mSearch_progress_bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                if (from.equals(TextConstants.HOME_SAMPLE)) {
                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    } else {
                        Intent intent1 = new Intent(mContext, SelectTimeActivity.class);
                        mContext.startActivity(intent1);
                    }
                } else if (from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.SERVICES)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(TextConstants.TEST_TYPE, TextConstants.LAB_TEST);
                    Intent intent2 = new Intent(mContext, FragmentBaseActivity.class);
                    intent2.putExtras(bundle);
                    mContext.startActivity(intent2);
                }
                break;
            case R.id.btn_back:
                CommonControls.closeKeyBoard(mContext);
                finish();
                break;

            case R.id.btnCross:
                mEt_search.setText("");
                mBtnCross.setVisibility(View.GONE);
                mSearch_progress_bar.setVisibility(View.GONE);
                notifyRVAdapter();
                setUIAndData();
                break;

            case R.id.btnAddMedicine:
                Intent intent = new Intent();
                DiseaseMO diseaseMoObject = new DiseaseMO(mEt_search.getText().toString().trim(), System.currentTimeMillis() + "", "Unknown");
                diseaseMoObject.setId(-1);
                intent.putExtra("diseaseMo", (Parcelable) diseaseMoObject);
                intent.putExtra("position", position);
                CommonControls.closeKeyBoard(mContext);
                setResult(8008, intent);
                finish();
                break;
        }
    }


    @Override
    public void animate(String what) {
        if (!isShowingSnackbar.equals("show") && what.equals("show")) {
            isShowingSnackbar = "show";
            AnimUtils.setLayoutAnim_slideupfrombottom((ViewGroup) footerView, mContext);
        } else if (!isShowingSnackbar.equals("hide") && what.equals("hide")) {
            isShowingSnackbar = "hide";
            AnimUtils.setLayoutAnim_slidedownfromtop((ViewGroup) footerView, mContext);
        }
    }


    private void notifyRVAdapter() {
        if (from.equals(TextConstants.DOCTORS)) {
            mAdapterdoctors_search.notifyDataSetChanged();
        } else if (from.equals(TextConstants.SPECIALITY)) {
            mAdapterSpeciality_search.notifyDataSetChanged();
        }
        if (from.equals(TextConstants.HOME_SAMPLE)) {
            mAdapterHomeSampleCollection_search.notifyDataSetChanged();
        } else if (from.equals(TextConstants.LAB_TEST)) {
            mAdapterHomeSampleCollection_search.notifyDataSetChanged();
        }
    }



   /* [ Filterable for Searching ]*/

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraints) {
                final FilterResults filterResults = new FilterResults();

                if (constraints.toString().trim().length() == 0) {
                    Handler mainHandler = new Handler(mContext.getMainLooper());
                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            mBtnCross.setVisibility(View.GONE);
                            mSearch_progress_bar.setVisibility(View.GONE);
                        }
                    };
                    mainHandler.post(myRunnable);
                    filterResults.count = 0;
                    return filterResults;
                } else if (constraints.toString().trim().length() < 3 && constraints.toString().trim().length() > 0) {
                    Handler mainHandler = new Handler(mContext.getMainLooper());
                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            mBtnCross.setVisibility(View.VISIBLE);
                            mSearch_progress_bar.setVisibility(View.GONE);
                        }
                    };
                    mainHandler.post(myRunnable);
                    filterResults.count = 0;
                    return filterResults;
                } else if (constraints.toString().trim().length() > 2) {
                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                        Handler mainHandler = new Handler(mContext.getMainLooper());
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                mBtnCross.setVisibility(View.VISIBLE);
                                mSearch_progress_bar.setVisibility(View.GONE);
                                CommonControls.closeKeyBoard(mContext);
                            }
                        };
                        mainHandler.post(myRunnable);
                        filterResults.count = -3;
                        return filterResults;
                    } else {
                        Handler progressHandler = new Handler(mContext.getMainLooper());
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                mBtnCross.setVisibility(View.GONE);
                                mSearch_progress_bar.setVisibility(View.VISIBLE);
                            }
                        };
                        progressHandler.post(myRunnable);

                        AppointmentSearchResponseCommon response = null;
                        APIManager apiManager = APIManager.getInstance();
                        AppointmentSearchRequestCommon appointmentSearchRequestJson = new AppointmentSearchRequestCommon();
                        AppointmentSearchConstraintCommon constraint = new AppointmentSearchConstraintCommon();
                        constraint.setSearchString(constraints.toString().trim());
                        constraint.setSkip(0);
                        constraint.setCount(10);
                        appointmentSearchRequestJson.setConstraints(constraint);

                        String json = new Gson().toJson(appointmentSearchRequestJson);
                        Gson gson = new Gson();
                        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
                        JsonObject jsonObj = element.getAsJsonObject();
                                /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
                        String authToken = SharedPref.getAccessToken();
                        String executeHttpPost = apiManager.executeHttpPost(SEARCH_URL_STRING, jsonObj, authToken, mContext);


                        appointmentModelList.clear();
                        specialitylist.clear();

                        if (executeHttpPost != null && constraints.length() > 2) {
                            Gson gson1 = new Gson();
                            Type listType = null;
                            if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                                listType = new TypeToken<List<AppointmentModel>>() {
                                }.getType();
                                appointmentModelList.addAll((Collection<? extends AppointmentModel>) gson1.fromJson(executeHttpPost.toString(), listType));
                                filterResults.values = appointmentModelList;
                                filterResults.count = appointmentModelList.size();
                            } else if (from.equals(TextConstants.SPECIALITY)) {
                                listType = new TypeToken<List<SpecialityDataModel>>() {
                                }.getType();
                                specialitylist.addAll((Collection<? extends SpecialityDataModel>) gson1.fromJson(executeHttpPost.toString(), listType));
                                filterResults.values = specialitylist;
                                filterResults.count = specialitylist.size();
                            }
                        } else {
                            if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                                filterResults.values = appointmentModelList;
                            } else if (from.equals(TextConstants.SPECIALITY)) {
                                filterResults.values = specialitylist;
                            }
                            filterResults.count = -2;
                        }

                        return filterResults;
                    }
                } else {
                    filterResults.count = 0;
                    return filterResults;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count == -1) {
                    API_RETURNED_NULL = false;

                    if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                        appointmentModelList.clear();
                    } else if (from.equals(TextConstants.SPECIALITY)) {
                        specialitylist.clear();
                    }
                    setUIAndData();
                    notifyRVAdapter();
                } else if (results != null && results.values == null && results.count == -2) {
                    API_RETURNED_NULL = true;
                    if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                        appointmentModelList.clear();
                    } else if (from.equals(TextConstants.SPECIALITY)) {
                        specialitylist.clear();
                    }
                    setUIAndData();
                    notifyRVAdapter();
                } else if (results != null && results.count > 0 && mEt_search.getText().toString().trim().length() > 2) {
                    API_RETURNED_NULL = false;
                    mBtnCross.setVisibility(View.VISIBLE);
                    mSearch_progress_bar.setVisibility(View.GONE);
                    setUIAndData();
                    notifyRVAdapter();
                    MyApplication.getInstance().trackResultCondition(results.count, constraint.toString());
                } else if (results.count == -3 && constraint != null) {
                    API_RETURNED_NULL = true;
                    if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                        appointmentModelList.clear();
                    } else if (from.equals(TextConstants.SPECIALITY)) {
                        specialitylist.clear();
                    }
                    setUIAndData();
                    mBtnCross.setVisibility(View.VISIBLE);
                    mSearch_progress_bar.setVisibility(View.GONE);
                    notifyRVAdapter();
                    MyApplication.getInstance().trackNoResultCondition(0, constraint.toString());
                } else if (results.count == 0 && constraint != null) {
                    API_RETURNED_NULL = false;
                    if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                        appointmentModelList.clear();
                    } else if (from.equals(TextConstants.SPECIALITY)) {
                        specialitylist.clear();
                    }
                    setUIAndData();
                    mBtnCross.setVisibility(View.VISIBLE);
                    mSearch_progress_bar.setVisibility(View.GONE);
                    notifyRVAdapter();
                    MyApplication.getInstance().trackNoResultCondition(0, constraint.toString());
                } else if (mEt_search.getText().toString().trim().length() < 3) {
                    API_RETURNED_NULL = false;
                    if (from.equals(TextConstants.HOME_SAMPLE) || from.equals(TextConstants.LAB_TEST) || from.equals(TextConstants.DOCTORS)) {
                        appointmentModelList.clear();
                    } else if (from.equals(TextConstants.SPECIALITY)) {
                        specialitylist.clear();
                    }
                    setUIAndData();
                    notifyRVAdapter();
                }
            }
        };
        return filter;
    }

    public void setUIAndData() {
        Handler mainHandler = new Handler(mContext.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                mSearch_progress_bar.setVisibility(View.GONE);
                if (mEt_search.getText().toString().trim().length() == 0) {
                    mBtnCross.setVisibility(View.GONE);
                    appointmentModelList.clear();
                    specialitylist.clear();
                    notifyRVAdapter();
                } else {
                    mBtnCross.setVisibility(View.VISIBLE);
                }

                if (specialitylist.size() > 0 || appointmentModelList.size() > 0) {
                    card_rv.setVisibility(View.VISIBLE);
                    mNo_data_lyt.setVisibility(View.GONE);
                    notifyRVAdapter();
                } else {
                    if (mEt_search.getText().toString().trim().length() > 2) {
                        mNo_data_image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_condition));
                        mNo_data_image.setVisibility(View.GONE);
                        if (API_RETURNED_NULL == false)
                            mTv_no_data_msg.setText(mContext.getResources().getString(R.string.no_result_appointment));
                        else
                            mTv_no_data_msg.setText(mContext.getResources().getString(R.string.call_unsuccessful));
                        mTv_no_data_msg.setTextColor(mContext.getResources().getColor(R.color.light_green));
                        mTv_no_data_msg.setVisibility(View.VISIBLE);
                    } else {
                        mNo_data_image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_condition));
                        mNo_data_image.setVisibility(View.GONE);
                        mTv_no_data_msg.setText("");
                        mTv_no_data_msg.setTextColor(mContext.getResources().getColor(R.color.light_green));
                        mTv_no_data_msg.setVisibility(View.VISIBLE);
                    }
                    card_rv.setVisibility(View.GONE);
                    mNo_data_lyt.setVisibility(View.VISIBLE);
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (BuildConfig.isMultipleSelectionEnabled)
            MyApplication.getInstance().unRegisterListSelection(this);
    }

    @Override
    public void handleSelection(String from, boolean selected) {
        if (selected && OrderSummary.getInstance().getOrderist().size() == 1) {
            animable.animate("show");
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (!selected && OrderSummary.getInstance().getOrderist().size() == 1) {
            animable.animate("show");
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (OrderSummary.getInstance().getOrderist().size() > 1) {
            animable.animate("show");
            tv_numberOfTests.setText(OrderSummary.getInstance().getOrderist().size() + "");
            tvTotalCost.setText(OrderSummary.getInstance().getTotalPrice() + "");
        } else if (OrderSummary.getInstance().getOrderist().size() == 0) {
            animable.animate("hide");
        }
    }
    /* [ End of Filterable for Searching ]*/
}
