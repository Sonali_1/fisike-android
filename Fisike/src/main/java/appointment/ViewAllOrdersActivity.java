package appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import appointment.adapter.MyOrdersAdapter;
import appointment.adapter.PatientNameAdapter;
import appointment.adapter.ReasonsAdapter;
import appointment.interfaces.AppointmentResponseCallback;
import appointment.interfaces.OnLoadMoreListener;
import appointment.model.OrderSummary;
import appointment.model.appointmodel.AppointmentDataModel;
import appointment.model.appointmodel.List;
import appointment.request.AppointUpdateRequest;
import appointment.request.AppointmentSearchRequest;
import appointment.request.GetReasonRequest;
import appointment.request.SearchReasonResponse;
import appointment.utils.AppointmentComparator;
import appointment.utils.SimpleDividerItemDecorationHeight;

/**
 * Created by laxmansingh on 3/16/2016.
 */
public class ViewAllOrdersActivity extends BaseActivity implements View.OnClickListener, AppointmentResponseCallback {

    private Context mContext;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private ProgressDialog pdialog;
    private AppointmentResponseCallback responseCallback;
    private RecyclerView rv_viewall;
    private String recent_or_upcoming;
    private MyOrdersAdapter adapterMyOrders;
    private java.util.List<List> myorder_list = new ArrayList<List>();
    private Bundle bundle;
    private TextView tvErrorInfo;
    private boolean loadingmore = false;
    private String position = "", cancel_or_reschedule = "";
    private boolean isRefreshrequired = false;
    private FrameLayout frameLayout;


    private PopupWindow mPopup;
    private CustomFontTextView reasonLabel;
    private Spinner tvReason;
    private CustomFontEditTextView ed_comments;
    private long transactionIdReason;
    private ReasonsAdapter reasonAdapter;
    java.util.List<String> reasonList = new ArrayList<String>();


    public static void newInstance(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ViewAllOrdersActivity.class);
        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.viewallorders_activity, frameLayout);
        mContext = this;
        responseCallback = this;
        bundle = getIntent().getBundleExtra("bundle");
        recent_or_upcoming = bundle.getString("recent_or_upcoming");
        findView();
        initView();
        bindView();

        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            tvErrorInfo.setVisibility(View.VISIBLE);
            return;
        } else {
            ThreadManager.getDefaultExecutorService().submit(new GetReasonRequest(getTransactionIdReason(), TextConstants.REASON_CANCEL));
            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(getResources().getString(R.string.loading_txt));
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.show();
            tvErrorInfo.setVisibility(View.GONE);
            long transactionId = getTransactionId();
            ThreadManager.getDefaultExecutorService().submit(new AppointmentSearchRequest(transactionId, responseCallback, recent_or_upcoming, myorder_list.size()));
        }


/*[     .....Load More listener on adapter.....     ]*/
        adapterMyOrders.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    if (loadingmore) {
                        myorder_list.add(null);
                        adapterMyOrders.notifyItemInserted(myorder_list.size() - 1);
                        long transactionId = getTransactionId();
                        ThreadManager.getDefaultExecutorService().submit(new AppointmentSearchRequest(transactionId, responseCallback, recent_or_upcoming, myorder_list.size() - 1));
                    }
                } catch (Exception ex) {

                }
            }
        });
/*[     .....End of Load More listener on adapter.....     ]*/
    }


    @Override
    public void onBackPressed() {
        /*[   sets previous activity needs to be refreshed with new data available on server  ]*/
        if (isRefreshrequired) {
            Intent intent = new Intent();
            intent.putExtra("data_refresh", TextConstants.REFRESH_SMALLCASE);
            setResult(TextConstants.REFERESH_CONSTANT, intent);
        }
        /*[   End of sets previous activity needs to be refreshed with new data available on server  ]*/
        super.onBackPressed();
    }


    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rv_viewall = (RecyclerView) findViewById(R.id.rv_viewall);
        tvErrorInfo = (TextView) findViewById(R.id.tv_error_info);
    }

    private void initView() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        String title = getResources().getString(R.string.title_view_all);
        if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
            title = getResources().getString(R.string.title_view_all_upcoming);
        } else if (recent_or_upcoming.equals(TextConstants.RECENT)) {
            title = getResources().getString(R.string.title_view_all_recent);
        }

        toolbar_title.setText(title);

        myorder_list.clear();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_viewall.setLayoutManager(layoutManager);
        rv_viewall.addItemDecoration(new SimpleDividerItemDecorationHeight(getResources()));
        adapterMyOrders = new MyOrdersAdapter(mContext, myorder_list, responseCallback, recent_or_upcoming, rv_viewall);
        rv_viewall.setAdapter(adapterMyOrders);
    }

    private void bindView() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void passResponse(final String response, final String recent_or_upcoming) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pdialog.isShowing()) {
                        pdialog.dismiss();
                    }
                    if (response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wrong))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connection))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.Something_went_wron))
                            || response.equalsIgnoreCase(MyApplication.getAppContext().getResources().getString(R.string.check_Internet_Connectio))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.unexpected_error))
                            || response.contains(MyApplication.getAppContext().getResources().getString(R.string.err_network))) {

                        if (myorder_list.size() > 0) {
                        /*[  ... Loading more handeled if comes an error...  ]*/
                            Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                            if (loadingmore) {
                                if (myorder_list.get(myorder_list.size() - 1) == null) {
                                    myorder_list.remove(myorder_list.size() - 1);
                                    adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                }
                                loadingmore = true;
                                adapterMyOrders.notifyDataSetChanged();
                                adapterMyOrders.setLoaded();
                            } else {
                            }
                        /*[  ... End of Loading more handeled if comes an error...  ]*/
                        } else {
                            tvErrorInfo.setVisibility(View.VISIBLE);
                            tvErrorInfo.setText(response);
                            //mProgressbar.setVisibility(View.GONE);
                        }

                    } else {

                        if (recent_or_upcoming.equals(TextConstants.RECENT) || recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<java.util.List<AppointmentDataModel>>() {
                            }.getType();

                            AppointmentDataModel appointmentDataModel = new AppointmentDataModel();
                            try {
                                appointmentDataModel = gson.fromJson(response.toString(), AppointmentDataModel.class);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            if (loadingmore) {
                                if (myorder_list.get(myorder_list.size() - 1) == null) {
                                    myorder_list.remove(myorder_list.size() - 1);
                                    adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                }
                            } else {
                            }

                            for (int i = 0; i < appointmentDataModel.getList().size(); i++) {
                                List list_model = appointmentDataModel.getList().get(i);
                                myorder_list.add(list_model);
                            }

                            if (recent_or_upcoming.equals(TextConstants.UPCOMING)) {
                                Collections.sort(myorder_list, new AppointmentComparator<List>());
                            }

                            if (myorder_list.size() == 0) {
                                tvErrorInfo.setVisibility(View.VISIBLE);
                            } else {
                                tvErrorInfo.setVisibility(View.GONE);
                            }
                            if (appointmentDataModel.getList().size() == 0) {
                                loadingmore = false;
                            } else {
                                loadingmore = true;
                                adapterMyOrders.notifyDataSetChanged();
                                adapterMyOrders.setLoaded();
                            }
                            //..Keep in mind must to do, to avoid crash of illegal state on recyclerview.....
                        }
                        if (recent_or_upcoming.equalsIgnoreCase(TextConstants.CANCEL) || recent_or_upcoming.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
                            if (position.length() != 0 && cancel_or_reschedule.length() != 0) {
                                Gson gson = new Gson();
                                List appointmentmodel = gson.fromJson(response, List.class);
                                myorder_list.remove(Integer.parseInt(position));
                                adapterMyOrders.notifyDataSetChanged();
                                isRefreshrequired = true;// on cancelling the appointment this variable must be set to true

                                if (loadingmore) {
                                    if (myorder_list.get(myorder_list.size() - 1) == null) {
                                        myorder_list.remove(myorder_list.size() - 1);
                                        adapterMyOrders.notifyItemRemoved(myorder_list.size());
                                    }
                                } else {
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception ex) {
        }
    }

    @Override
    public void cancelOrReschedule(List appointmentmodel, String position, String cancel_or_reschedule) {
        if (!Utils.showDialogForNoNetwork(mContext, false)) {
            return;
        } else {
            this.position = position;
            this.cancel_or_reschedule = cancel_or_reschedule;
            showReasonCommentDialogCommon(appointmentmodel, position, cancel_or_reschedule);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String position = bundle.getString("position");
                    String cancel_or_reschedule = bundle.getString("cancel_or_reschedule");
                    List appointmentmodel = (List) bundle.getParcelable("appointment_model");
                    if (cancel_or_reschedule.equalsIgnoreCase(TextConstants.RESCHEDULE)) {
                        myorder_list.remove(Integer.parseInt(position));
                        adapterMyOrders.notifyDataSetChanged();

                        rv_viewall.removeViewAt(Integer.parseInt(position));
                        adapterMyOrders.notifyItemRemoved(Integer.parseInt(position));
                        adapterMyOrders.notifyItemRangeChanged(Integer.parseInt(position), myorder_list.size());

                        myorder_list.add(Integer.parseInt(position), appointmentmodel);

                        if (loadingmore) {
                            if (myorder_list.get(myorder_list.size() - 1) == null) {
                                myorder_list.remove(myorder_list.size() - 1);
                                adapterMyOrders.notifyItemRemoved(myorder_list.size());
                            }
                        } else {
                        }

                        /*[.... Sorting applied if cancel or reschedule......]*/
                        Collections.sort(myorder_list, new AppointmentComparator<List>());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapterMyOrders.notifyDataSetChanged();
                            }
                        },100);

                        /*[.... End of Sorting applied if cancel or reschedule......]*/
                        isRefreshrequired = true;// on rescheduling the appointment this variable must be set to true

                    }
                } else if (null != data && resultCode == TextConstants.REBOOK_CONSTANT) {
                 /*   Bundle bundle = data.getExtras();
                    quest.model.appointmodel.List appointmentmodel = bundle.getParcelable("appointment_model");
                    myorder_list.add(appointmentmodel);
                    Collections.sort(myorder_list, new AppointmentComparator<quest.model.appointmodel.List>());
                    adapterMyOrders.notifyDataSetChanged();*/
                    isRefreshrequired = true;
                }
                break;
        }
    }


    public void showReasonCommentDialogCommon(final List appointmentmodel, final String position, final String cancel_or_reschedule) {
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.reason_comment_dialog, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(mContext.getResources().getString(R.string.txt_cancel_appointment));
        ed_comments = (CustomFontEditTextView) v.findViewById(R.id.ed_comments);
        reasonLabel = (CustomFontTextView) v.findViewById(R.id.tv_static_reason);
        tvReason = (Spinner) v.findViewById(R.id.spinnerReasons);
        reasonAdapter = new ReasonsAdapter(mContext, reasonList);
        tvReason.setAdapter(reasonAdapter);
        showCancelDialog();

        ((CustomFontButton) v.findViewById(R.id.cancel_action)).setText(mContext.getResources().getString(R.string.cancel));
        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        ((CustomFontButton) v.findViewById(R.id.continue_action)).setText(mContext.getResources().getString(R.string.txt_ok));
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.showDialogForNoNetwork(mContext, false)) {
                    alertDialog.dismiss();
                    return;
                } else {
                    String reason;
                    if (tvReason.getSelectedItem().toString().equals(getResources().getString(R.string.select_one)))
                        Toast.makeText(ViewAllOrdersActivity.this, getResources().getString(R.string.reason_for_cancellation), Toast.LENGTH_LONG).show();
                    else {
                        alertDialog.dismiss();
                        reason = tvReason.getSelectedItem().toString();
                        pdialog = new ProgressDialog(mContext);
                        pdialog.setMessage(getResources().getString(R.string.updating));
                        pdialog.setCanceledOnTouchOutside(false);
                        pdialog.show();
                        long transactionId = getTransactionId();
                        ThreadManager.getDefaultExecutorService().submit(new AppointUpdateRequest(transactionId, responseCallback, appointmentmodel, cancel_or_reschedule, reason, ed_comments.getText().toString().trim()));
                    }
                }
            }
        });
        alertDialog.show();
    }


    @Subscribe
    public void onGetReasonResponse(SearchReasonResponse searchReasonResponse) {

        if (transactionIdReason != searchReasonResponse.getTransactionId())
            return;

        if (searchReasonResponse.isSuccessful()) {
            OrderSummary.getInstance().setCancelReason(searchReasonResponse.getReasonList());
        }
    }

    private void showCancelDialog() {
        reasonList.clear();
        ArrayList<String> cancelReason = OrderSummary.getInstance().getCancelReason();
        if (cancelReason.size() > 0) {
            reasonList.addAll(cancelReason);
        } else {
            CharSequence[] arrayPopup = getResources().getStringArray(R.array.appointment_reasons);
            for (int i = 0; i < arrayPopup.length; i++) {
                reasonList.add(arrayPopup[i].toString());
            }
        }
        reasonList.add(0, mContext.getResources().getString(R.string.select_one));
        reasonAdapter.notifyDataSetChanged();
        tvReason.setSelection(0);
    }

    public long getTransactionIdReason() {
        transactionIdReason = System.currentTimeMillis();
        return transactionIdReason;
    }
}
