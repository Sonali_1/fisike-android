package appointment.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by laxmansingh on 3/1/2016.
 */
public class DBHelper extends SQLiteOpenHelper {


    private static DBHelper dbHelper;
    private Context mContext;
    private SQLiteDatabase database;


    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "quest.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";


    /*  [......Query Strings for creating,deleting,update  etc, OrderSummary table in database...]*/
    private static final String SQL_CREATE_TESTSORDERSUMMARY =
            "CREATE TABLE " + TestsOrderSummary.TestsOrderEntry.TABLE_NAME + " (" +
                    TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_INFORMATION + TEXT_TYPE +
                    " )";
    private static final String SQL_DELETE_TESTSORDERSUMMARY =
            "DROP TABLE IF EXISTS " + TestsOrderSummary.TestsOrderEntry.TABLE_NAME;
/*  [......End of Query Strings for creating,deleting,update  etc, OrderSummary table in database...]*/


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }


    /*[    ******** Functions for holiding the DBHelper Instance and opening and closing the database instance *********  ]*/
    public static DBHelper getInstance(Context ctx) {
        if (null == dbHelper) {
            synchronized (DBHelper.class) {
                if (dbHelper == null) {
                    dbHelper = new DBHelper(ctx);
                    dbHelper.open();
                }
            }
        }
        return dbHelper;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        if (database.isOpen()) {
            database.close();
            if (dbHelper != null) {
                dbHelper = null;
            }
        }
    }
/*[    ******** End of Functions for holiding the DBHelper Instance and opening and closing the database instance *********  ]*/


    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TESTSORDERSUMMARY);
    }


    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TESTSORDERSUMMARY);
        onCreate(db);
    }


    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }



    /*[----------- Functions for inserting and deleting and updating data in database -----------]*/


    public void deleteTestsOrderSummary() {

        // Define 'where' part of query.
        String selection = TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID + " LIKE ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(1)};

        // Issue SQL statement.
        database.delete(TestsOrderSummary.TestsOrderEntry.TABLE_NAME, selection, selectionArgs);
        dbHelper.close();
    }


    public void updateTestsOrderSummary(String info) {

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_INFORMATION, info);

// Which row to update, based on the ID
        String selection = TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(1)};

        int count = database.update(
                TestsOrderSummary.TestsOrderEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        dbHelper.close();
    }


    public String getTestsOrderSummary() {

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID,
                TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_INFORMATION
        };


        // Define 'where' part of query.
        String selection = TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID + " LIKE ?";
// Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(1)};


        Cursor cursor = database.query(
                TestsOrderSummary.TestsOrderEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        cursor.moveToFirst();
        int Id = cursor.getInt(cursor.getColumnIndexOrThrow(TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID));
        String testsOrderSummary_json_string = cursor.getString(cursor.getColumnIndexOrThrow(TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_INFORMATION));
        dbHelper.close();

        return testsOrderSummary_json_string;
    }


    public void insertOrUpdateTestsOrderSummary(String info) {
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_ID,1);
        values.put(TestsOrderSummary.TestsOrderEntry.COLUMN_ORDER_INFORMATION, info);

// Insert the new row, returning the primary key value of the new row
        long newRowId;

// this will insert if record is new, update otherwise
        newRowId = database.insertWithOnConflict(TestsOrderSummary.TestsOrderEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        dbHelper.close();
    }



    /*[----------- End of Functions for inserting and deleting and updating data in database -----------]*/


}
