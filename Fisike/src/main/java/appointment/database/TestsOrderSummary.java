package appointment.database;

import android.provider.BaseColumns;

/**
 * Created by laxmansingh on 3/4/2016.
 */
public final class TestsOrderSummary {

    // To prevent someone from accidentally instantiating the TestsOrderSummary class,
    // give it an empty constructor.

    public TestsOrderSummary() {
    }

    /* Inner class that defines the table contents */
    public static abstract class TestsOrderEntry implements BaseColumns {
        public static final String TABLE_NAME = "testsorder";
        public static final String COLUMN_ORDER_ID = "id";
        public static final String COLUMN_ORDER_INFORMATION = "order_info";
    }


}
