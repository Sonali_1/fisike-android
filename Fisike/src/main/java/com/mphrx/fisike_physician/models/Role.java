package com.mphrx.fisike_physician.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Copyright 2015 App Street Software Pvt. Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Role implements Parcelable {

    ArrayList<String> coding;
    ArrayList<String> extension;
    String id;
    String text;

    public ArrayList<String> getCoding() {
        return coding;
    }

    public void setCoding(ArrayList<String> coding) {
        this.coding = coding;
    }

    public ArrayList<String> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<String> extension) {
        this.extension = extension;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.coding);
        dest.writeStringList(this.extension);
        dest.writeString(this.id);
        dest.writeString(this.text);
    }

    public Role() {
    }

    protected Role(Parcel in) {
        this.coding = in.createStringArrayList();
        this.extension = in.createStringArrayList();
        this.id = in.readString();
        this.text = in.readString();
    }

    public static final Parcelable.Creator<Role> CREATOR = new Parcelable.Creator<Role>() {
        public Role createFromParcel(Parcel source) {
            return new Role(source);
        }

        public Role[] newArray(int size) {
            return new Role[size];
        }
    };
}
