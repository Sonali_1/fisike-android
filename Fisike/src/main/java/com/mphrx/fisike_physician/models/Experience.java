package com.mphrx.fisike_physician.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Copyright 2015 App Street Software Pvt. Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Experience implements Parcelable {
    String noOfExp;

    public String getNoOfExp() {
        return noOfExp;
    }

    public void setNoOfExp(String noOfExp) {
        this.noOfExp = noOfExp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.noOfExp);
    }

    public Experience() {
    }

    protected Experience(Parcel in) {
        this.noOfExp = in.readString();
    }

    public static final Parcelable.Creator<Experience> CREATOR = new Parcelable.Creator<Experience>() {
        public Experience createFromParcel(Parcel source) {
            return new Experience(source);
        }

        public Experience[] newArray(int size) {
            return new Experience[size];
        }
    };
}
