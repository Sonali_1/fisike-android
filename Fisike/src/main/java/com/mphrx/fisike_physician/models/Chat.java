package com.mphrx.fisike_physician.models;

/**
 * Created by brijesh on 13/10/15.
 */
public class Chat {

    enum STATUS {
        PENDING, SENT, RECEIVED, SEEN
    }

    public String id;
    public String message;
    public String fromName;
    public String toName;
    public String fromId;
    public String toId;
    public String date;
    public STATUS status;

}
