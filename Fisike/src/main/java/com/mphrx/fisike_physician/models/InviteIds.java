package com.mphrx.fisike_physician.models;

import java.util.List;
import java.util.Map;

/**
 * Created by brijesh on 06/11/15.
 */
public class InviteIds {

    public Map<String, String> mapping;

    public List<String> values;

    public String orgId;

}
