package com.mphrx.fisike_physician.models;

/**
 * Created by Shivansh on 06/10/15.
 */
public class Contact {
    private String name;
    private String salutation;
    private boolean isFisike;
    private String number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public boolean getFisike() {
        return isFisike;
    }

    public void setFisike(boolean isFisike) {
        this.isFisike = isFisike;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
