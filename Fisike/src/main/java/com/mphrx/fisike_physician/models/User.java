package com.mphrx.fisike_physician.models;

import java.io.Serializable;

/**
 * Created by yokohama on 07/10/15.
 */
public class User implements Serializable {

    public String name;
    public String number;
    public String role;
    public String roleId;
    public String organisationId;
    public String inviteeRoleId;

    public boolean isSelected;
    public String userId;
    public boolean isFisikeUser;
    public boolean isInvitationSuccessful;

}
