package com.mphrx.fisike_physician.models;

import com.mphrx.fisike.gson.request.Address;

import java.util.ArrayList;

/**
 * Created by brijesh on 03/12/15.
 */
public class PhysicianMO {

    public String role;
    public String speciality;
    public String experience;
    public ArrayList<Address> address;
    public String userType;
    public String id;
    public String email;
    public String password;
    public String firstName;
    public String lastName;
    public String phoneNumber;
    public String gender;
    public String dateOfBirth;
    public String aboutMe;
    public String changeEmail;
    public String physicianId;
    public String alternateContact;


}
