package com.mphrx.fisike_physician.models;

/**
 * Created by manohar on 28/10/15.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GroupChatBean {

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("dateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("members")
    @Expose
    private List<Member> members = new ArrayList<Member>();

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner The owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return The isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted The isDeleted
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated The dateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return The members
     */
    public List<Member> getMembers() {
        return members;
    }

    /**
     * @param members The members
     */
    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public class Availability {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("status")
        @Expose
        private String status;

        /**
         * @return The type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class Member {

        @SerializedName("jid")
        @Expose
        private String jid;
        @SerializedName("availability")
        @Expose
        private Availability availability;
        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("isActive")
        @Expose
        private Boolean isActive;
        @SerializedName("userType")
        @Expose
        private Object userType;

        /**
         * @return The jid
         */
        public String getJid() {
            return jid;
        }

        /**
         * @param jid The jid
         */
        public void setJid(String jid) {
            this.jid = jid;
        }

        /**
         * @return The availability
         */
        public Availability getAvailability() {
            return availability;
        }

        /**
         * @param availability The availability
         */
        public void setAvailability(Availability availability) {
            this.availability = availability;
        }

        /**
         * @return The role
         */
        public String getRole() {
            return role;
        }

        /**
         * @param role The role
         */
        public void setRole(String role) {
            this.role = role;
        }

        /**
         * @return The isActive
         */
        public Boolean getIsActive() {
            return isActive;
        }

        /**
         * @param isActive The isActive
         */
        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        /**
         * @return The userType
         */
        public Object getUserType() {
            return userType;
        }

        /**
         * @param userType The userType
         */
        public void setUserType(Object userType) {
            this.userType = userType;
        }

    }

}



