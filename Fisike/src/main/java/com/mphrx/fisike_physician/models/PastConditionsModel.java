package com.mphrx.fisike_physician.models;

/**
 * Created by laxmansingh on 5/25/2016.
 */
public class PastConditionsModel {

    private String display = "";
    private String duration = "";


    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
