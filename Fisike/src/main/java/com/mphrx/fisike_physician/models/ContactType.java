package com.mphrx.fisike_physician.models;

import java.util.List;

/**
 * Created by Shivansh on 06/10/15.
 */
public class ContactType {

    private String type;
    private List<Contact> contacts;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
