package com.mphrx.fisike_physician.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by manohar on 12/12/15.
 */

public class Speciality implements Parcelable {

    public static final ArrayList<Speciality> EMPTY = new ArrayList<>(0);

    public static final Parcelable.Creator<Speciality> CREATOR = new Parcelable.Creator<Speciality>() {
        public Speciality createFromParcel(Parcel source) {
            return new Speciality(source);
        }

        public Speciality[] newArray(int size) {
            return new Speciality[size];
        }
    };
    @Expose
    String id;
    @Expose
    String text;

    public Speciality() {
    }

    protected Speciality(Parcel in) {
        this.id = in.readString();
        this.text = in.readString();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.text);
    }
}
