package com.mphrx.fisike_physician.network;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pranav on 27/11/15.
 */
public class LruBitmapCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {

    private Map<String, Integer> profileCacheKeyPrefixes = new ConcurrentHashMap<>(10);

    public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        return cacheSize;
    }

    public LruBitmapCache() {
        this(getDefaultLruCacheSize());
    }

    public LruBitmapCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes() * value.getHeight() / 1024;
    }

    @Override
    public Bitmap getBitmap(String url) {
        recordPrefix(url);
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url, bitmap);
        recordPrefix(url);
    }

    // record the cache-key-prefix for profile pic urls
    private void recordPrefix(String key) {
        if (key.contains(MphRxUrl.getFetchContactImageUrl())) {
            String prefix = key.replaceFirst("http.*", "");
            profileCacheKeyPrefixes.put(prefix, 0);
        }
    }

    public void removeAllProfilePicEntries(String id) {
        for (String prefix : profileCacheKeyPrefixes.keySet()) {
            String key = prefix + MphRxUrl.getFetchContactImageUrl(id);
            remove(key);
        }
    }

    public void removeAllGroupPicEntries(String groupId) {
        for (String prefix : profileCacheKeyPrefixes.keySet()) {
            String key = prefix + MphRxUrl.getFetchContactGroupImageUrl(groupId);
            remove(key);
        }
    }
}
