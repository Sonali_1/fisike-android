package com.mphrx.fisike_physician.network.request;

import android.content.ContentValues;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.beans.PhysicianDetails;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.PhysicianDetailResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.TextPattern;

import org.json.JSONObject;

/**
 * Created by brijesh on 04/12/15.
 */
public class PhysicianDetailRequest extends BaseObjectRequest {

    private long mTransactionId;
    private String userId;
    private boolean isUpdateContactDetailsOnly;
    private long persistenceKeyOfContact;

    public PhysicianDetailRequest(long transactionId, String userId) {
        mTransactionId = transactionId;
        this.userId = userId;
    }

    public PhysicianDetailRequest(long transactionId, String userId, boolean isUpdateContactDetailsOnly, long key) {
        mTransactionId = transactionId;
        this.userId = userId;
        this.isUpdateContactDetailsOnly = isUpdateContactDetailsOnly;
        persistenceKeyOfContact = key;
    }



    @Override
    public void doInBackground() {
        String url = MphRxUrl.getPhysicianDetailUrl() + (userId == null ? SettingManager.getInstance().getUserMO().getPhysicianId() : TextPattern.getUserId(userId));
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, null, this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if (!isUpdateContactDetailsOnly)
            BusProvider.getInstance().post(new PhysicianDetailResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        if (isUpdateContactDetailsOnly) {

            PhysicianDetails physicianDetails= new PhysicianDetailResponse(response, mTransactionId, userId == null).getPhysicianData();

            boolean isNeedUpdate = false;
            if (physicianDetails != null) {
                ContentValues values = new ContentValues();
                if (!TextUtils.isEmpty(physicianDetails.getExperience())) {
                    isNeedUpdate = true;
                    values.put("experience", physicianDetails.getExperience());
                }
                if (!TextUtils.isEmpty(physicianDetails.getRole())) {
                    isNeedUpdate = true;
                    values.put("role", physicianDetails.getRole());
                }
                if (!TextUtils.isEmpty(physicianDetails.getSpeciality())) {
                    isNeedUpdate = true;
                    values.put("speciality", physicianDetails.getSpeciality());
                }
                if (isNeedUpdate)
                    chatContactDBAdapter.getInstance(MyApplication.getAppContext()).updateChatContactMo(persistenceKeyOfContact, values);
            }
        } else {
            BusProvider.getInstance().post(new PhysicianDetailResponse(response, mTransactionId, userId == null));
        }
    }

}
