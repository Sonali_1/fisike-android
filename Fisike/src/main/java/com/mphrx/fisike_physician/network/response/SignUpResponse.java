package com.mphrx.fisike_physician.network.response;

import android.graphics.Bitmap;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by manohar on 19/11/15.
 */
public class SignUpResponse extends BaseResponse {

    public SignUpResponse(JSONObject response, Bitmap bitmap, long transactionId) {
        super(response, transactionId);
        parseResponse(response, bitmap);
    }

    public SignUpResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }


    private void parseResponse(JSONObject response, Bitmap bitmap) {

        try {
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.getString("user"), UserMO.class);
            UserMO userMO = (UserMO) jsonToObjectMapper;
            if (userMO != null) {
                SettingManager.getInstance().updateUserMO(userMO);
            }

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = response.optString("msg", e.getMessage());
        }

    }

}
