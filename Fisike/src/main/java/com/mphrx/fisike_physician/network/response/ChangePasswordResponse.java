package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import org.json.JSONObject;

/**
 * Created by brijesh on 03/12/15.
 */
public class ChangePasswordResponse extends BaseResponse {

    public ChangePasswordResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
    }

    public ChangePasswordResponse(Exception exception, long transactionid) {
        super(exception, transactionid);
    }

    public ChangePasswordResponse(long transactionid) {
        super(new Exception(MyApplication.getAppContext().getString(R.string.incorrect_old_password)), transactionid);
        mMessage = MyApplication.getAppContext().getString(R.string.incorrect_old_password);
    }
}
