package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONObject;

/**
 * Created by brijesh on 16/11/15.
 */
public class CheckUserTokenStatusRequest extends BaseObjectRequest {

    private long mTransactionId;

    public CheckUserTokenStatusRequest(long transactionId) {
        mTransactionId = transactionId;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchContactsUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        //BusProvider.getInstance().post(new ContactResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        //BusProvider.getInstance().post(new ContactResponse(response, mTransactionId));
    }

    private String getPayload() {
        JSONObject object = new JSONObject();
        try {

            AppLog.showInfo(getClass().getSimpleName(), object.toString());
        } catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            e.printStackTrace();
        }

        return object.toString();
    }

}
