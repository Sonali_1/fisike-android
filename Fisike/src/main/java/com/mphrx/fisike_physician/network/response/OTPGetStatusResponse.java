package com.mphrx.fisike_physician.network.response;

import android.util.Log;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.OTPResponse.OtpGetResponseGson;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Aastha on 15/02/2016.
 */
public class OTPGetStatusResponse extends BaseResponse {
    private boolean mIsUserExisted;
    private boolean mIsMessageSent=false;
    private String mStatus="";
    private String msg="";
    private boolean adminCreatedPasswordNeverChanged=false;
    OtpGetResponseGson otpGetResponseGson;


    public OTPGetStatusResponse(JSONObject response, String mNumber, long transactionId) {
        super(response,transactionId);
        try {
            mIsUserExisted=response.getBoolean("userExist");
            mIsMessageSent=response.getBoolean("messageSent");
            msg=response.getString("msg");
            mStatus=response.getString("status");
            //*edited by Aastha for FIS-6306
            try{
            adminCreatedPasswordNeverChanged=response.getBoolean("adminCreatedPasswordNeverChanged");
            }
            catch (Exception e){
                adminCreatedPasswordNeverChanged=false;
            }
            AppLog.d("admin created",adminCreatedPasswordNeverChanged+"");
            //mTransactionId=transactionId;
            this.otpGetResponseGson= (OtpGetResponseGson) GsonUtils.jsonToObjectMapper(response.toString(), OtpGetResponseGson.class);
        }
        catch (JSONException e) {

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }

    }
    public OTPGetStatusResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public boolean isUserExisted() {
        return mIsUserExisted;
    }
    public boolean getMessageSent(){
        return mIsMessageSent;
    }
    public String getMessage(){
        return msg;
    }
    public String getStatus(){
        return mStatus;
    }

    public String getmMessage()
    {
        return  mMessage;
    }

    public boolean isAdminCreatedPasswordNeverChanged() {
        return this.adminCreatedPasswordNeverChanged;
    }

}
