package com.mphrx.fisike_physician.network.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.PdfPreview;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kailash Khurana on 3/28/2016.
 */
public class DownloadRecordFileRequest extends AsyncTask<Void, Void, Boolean> {
    private final String url;
    private final String keyPayload;
    private int position;
    private Context context;
    private int docId;
    private int mimeType;
    private File file;
    private ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(context.getResources().getString(R.string.progress_dialog));
        progressDialog.show();
        try {
            TextView tv1 = (TextView) progressDialog.findViewById(android.R.id.message);
            tv1.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (DownloadRecordFileRequest.this.getStatus() == Status.RUNNING)
                    DownloadRecordFileRequest.this.cancel(true);
            }
        });
        super.onPreExecute();
    }

    public DownloadRecordFileRequest(Context context, int docId, int mimeType, String url, String keyPayload) {
        this.context = context;
        this.docId = docId;
        this.mimeType = mimeType;
        this.url = url;
        this.keyPayload = keyPayload;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String fileName = "tempFileDownloaded";
        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "./temp", docId + ".pdf");
        } else if (mimeType == VariableConstants.MIME_TYPE_DOC) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "./temp", docId + ".doc");
        } else if (mimeType == VariableConstants.MIME_TYPE_IMAGE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "./temp", docId + ".jpg");
        }
        return downloadFile(docId, file);
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (file == null || !aVoid) {
            Toast.makeText(context, context.getResources().getString(R.string.Something_went_wrong), Toast.LENGTH_SHORT).show();
            return;
        }
        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/pdf");

            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(context, PdfPreview.class).putExtra(VariableConstants.PDF_FILE_PATH, file.getAbsolutePath()).putExtra(VariableConstants.MIME_TYPE, mimeType).putExtra(TextConstants.IS_REPORT, true));
            }
        } else if (mimeType == VariableConstants.MIME_TYPE_DOC) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/msword");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
            }
        }


        super.onPostExecute(aVoid);
    }

    private boolean downloadFile(int docId, File file) {
        InputStream input = null;
        FileOutputStream fos = null;
        HttpsURLConnection urlConnection = null;
        ByteArrayOutputStream output = null;
        try {
            URL httpPost = new URL(url);
            urlConnection = (HttpsURLConnection) httpPost.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);
            urlConnection.setRequestProperty("Content-Type", "application/json");

                    /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
            String authToken = SharedPref.getAccessToken();

            if (authToken != null) {
                urlConnection.addRequestProperty("x-auth-token", authToken);
            }

            String headerVersion;
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig"))
                headerVersion = "V2";
            else
                headerVersion = "V1";

            urlConnection.addRequestProperty("api-info", APIManager.getInstance().getHeader(MyApplication.getAppContext(), headerVersion));

            byte[] outputInBytes = keyPayload.getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            urlConnection.connect();

            String headerType = urlConnection.getHeaderField("Content-Type");

            int fileLength = urlConnection.getContentLength();

            if (file == null) {
                return false;
            }

            // download the file
            input = urlConnection.getInputStream();
            fos = new FileOutputStream(file.getAbsoluteFile());

            byte data[] = new byte[2048];
            int count;
            long total = 0;
            output = new ByteArrayOutputStream();

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.writeTo(fos);
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (fos != null)
                    fos.close();
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return true;
    }

}