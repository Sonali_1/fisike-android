package com.mphrx.fisike_physician.network.request;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.response.PendingInviteListResponse;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by brijesh on 20/11/15.
 */
public class PendingInviteListRequest implements Callable<Void> {

    private long mTransactionId;

    public PendingInviteListRequest(long transactionId) {
        mTransactionId = transactionId;
    }

    @Override
    public Void call() throws Exception {

        try {
            List<User> userList = DBAdapter.getInstance(MyApplication.getInstance()).getUserInviteList();
            BusProvider.getInstance().post(new PendingInviteListResponse(userList, mTransactionId));
        }
        catch (Exception e) {
            BusProvider.getInstance().post(new PendingInviteListResponse(new ArrayList<User>(0), mTransactionId));
        }

        return null;
    }


}
