package com.mphrx.fisike_physician.network.response;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.util.Log;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike_physician.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by brijesh on 15/10/15.
 */
public class SearchResponse extends BaseResponse {

    private ArrayList<ChatContactMO> mList;
    private Set<String> mNumberList; // Already Invited
    private ArrayList<ChatContactMO> mContactList;
    boolean shouldInsertInDB;
    private String mSearchString;
    private boolean mIsPermissionGranted;
    private final boolean mIsPartial;

    public SearchResponse(long transactionId, String searchString, boolean permissionGranted, boolean shouldInsertInDB) {
        super(new JSONObject(), transactionId);
        mIsPartial = true;
        mSearchString = searchString;
        mIsPermissionGranted = permissionGranted;
        this.shouldInsertInDB = shouldInsertInDB;
        fetchUserList();
        addDataFromPhoneBook();
    }

    public SearchResponse(JSONObject response, long transactionId, String searchString, boolean permissionGranted, boolean shouldInsertInDB) {
        super(response, transactionId);
        mIsPartial = false;
        mSearchString = searchString;
        mIsPermissionGranted = permissionGranted;
        this.shouldInsertInDB = shouldInsertInDB;
        fetchUserList();
        parseResponse(response);
    }

    public SearchResponse(Exception exception, long transactionId, String searchString, boolean permissionGranted) {
        super(exception, transactionId);
        mIsPartial = false;
        mSearchString = searchString;
        mIsPermissionGranted = permissionGranted;
        fetchUserList();
        addDataFromPhoneBook();
    }

    private void fetchUserList() {
        // TODO implement this functionality
        mNumberList = new HashSet<>();
        try {
            List<User> userList = DBAdapter.getInstance(MyApplication.getInstance()).getUserInviteList();
            for (User user : userList)
                mNumberList.add(user.number);
        } catch (Exception e) {
            // Do Nothing
        }
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray resultList = response.getJSONArray("resultList");
            mList = new ArrayList<>(resultList.length());
            for (int i = 0; i < resultList.length(); i++) {
                try {
                    ChatContactMO user = getUserData(resultList.getJSONObject(i));
                    user.setFisikeContact(true);
                    Log.i("usertype","yes"+user.getUserType().getType()+"   &  "+user.getPatientId()+"   &  "+user.getPhysicianId());
                    if (TextUtils.isEmpty(user.getPhoneNo()))
                        continue;
                    String number = user.getPhoneNo().replaceAll(" ", "").replaceAll("\\[^0-9]", "");
                    if (number.length() > 10)
                        number = number.substring(number.length() - 10, number.length());
                    if (!mNumberList.contains(number))
                        mList.add(user);
                    if (shouldInsertInDB) {
                        chatContactDBAdapter.getInstance(MyApplication.context).updateChatContactMoUserProfile(user);
                    }
                } catch (JSONException ee) {
                    // Skip Data
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Collections.sort(mList, new Comparator<ChatContactMO>() {
                @Override
                public int compare(ChatContactMO lhs, ChatContactMO rhs) {
                    return (lhs.getFirstName() + lhs.getLastName() + lhs.getUsername()).compareTo((rhs.getFirstName() + rhs.getLastName() + rhs.getUsername()));
                }
            });
        } catch (JSONException e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
        addDataFromPhoneBook();
    }

    private void addDataFromPhoneBook() {
        if (!mIsPermissionGranted)
            return;

        if (TextUtils.isEmpty(mSearchString))
            return;

        if (mList == null) {
            mList = new ArrayList<>();
        }

        String select = ContactsContract.Data.HAS_PHONE_NUMBER + " != 0 AND " + ContactsContract.Data.MIMETYPE
                + " = ? AND (" + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? OR " + ContactsContract.Data.DATA1
                + " LIKE ? )";
        String[] selectArgs = {ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, "%" + this.mSearchString + "%",
                "%" + this.mSearchString + "%"};

        Cursor cursor = MyApplication.getAppContext().getContentResolver().query(
                ContactsContract.Data.CONTENT_URI, null, select, selectArgs, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Pair<String, String> nameNumberPair = getNameNumberPair(cursor);
                ChatContactMO tmp = new ChatContactMO();
                tmp.setFirstName(nameNumberPair.first);
                tmp.setPhoneNo(nameNumberPair.second);
                tmp.setFisikeContact(false);
                if (TextUtils.isEmpty(tmp.getPhoneNo()))
                    continue;
                String number = tmp.getPhoneNo().replace(" ", "").replaceAll("\\[^0-9]", "");
                if (number.length() > 10)
                    number = number.substring(number.length() - 10, number.length());
                if (!mNumberList.contains(number))
                    mList.add(tmp);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
            cursor.close();

            if (mList.size() > 0) {
                mIsSuccessful = true;
                mMessage = "";
            }
        }
    }

    public Pair<String, String> getNameNumberPair(Cursor cursor) {
        int nameIdx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int numberIdx = cursor.getColumnIndexOrThrow(ContactsContract.Data.DATA1);

        String number = cursor.getString(numberIdx);
        String name = null;
        if (nameIdx == -1) {
            name = number;
        } else {
            name = cursor.getString(nameIdx);
            if (TextUtils.isEmpty(name)) {
                name = number;
            }
        }

        return new Pair<>(name, number);
    }

    private ChatContactMO getUserData(JSONObject object) throws JSONException {
        return (ChatContactMO) GsonUtils.jsonToObjectMapper(object.toString(), ChatContactMO.class);
    }

    public ArrayList<ChatContactMO> getList() {
        return mList;
    }

    public String getSearchString() {
        return mSearchString;
    }

    public boolean isPartial() {
        return mIsPartial;
    }
}
