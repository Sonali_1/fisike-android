package com.mphrx.fisike_physician.network.request;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.LoginResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by brijesh on 15/10/15.
 */
public class LoginRequest extends BaseObjectRequest {

    private String mUserName;
    private String mPassword;
    private long mTransactionId;

    public LoginRequest(String userName, String password, long transactionId) {
        mUserName = userName;
        mPassword = password;
        mTransactionId = transactionId;
    }

    @Override
    public void doInBackground() {
        // perform login
        String url = MphRxUrl.getLoginUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new LoginResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new LoginResponse(response, mPassword, mTransactionId));
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            if(SharedPref.getIsMobileEnabled()){
              payLoad.put(MphRxUrl.K.PHONE, mUserName);
              payLoad.put(MphRxUrl.K.COUNTRY_CODE,SharedPref.getCountryCode());
            }else if(SharedPref.getIsEmailEnabled()){
                payLoad.put(MphRxUrl.K.EMAIL, mUserName);
            }else if(SharedPref.getIsUserNameEnabled()){
                payLoad.put(MphRxUrl.K.USERNAME, mUserName);
            }
            payLoad.put(MphRxUrl.K.PASSWORD, mPassword);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
            String languageKey = Utils.getLanguageKey(MyApplication.getAppContext(), null);
            payLoad.put(MphRxUrl.K.USER_LANGUAGE, languageKey);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
