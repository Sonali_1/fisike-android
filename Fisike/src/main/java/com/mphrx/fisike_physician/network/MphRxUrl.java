package com.mphrx.fisike_physician.network;

import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;

/**
 * Created by brijesh on 15/10/15.
 */

public class MphRxUrl {

    public static final int TIMEOUT = 10000;
    public static final int RETRY = 0;
    public static final int BACKOFF = 1;
    private static String linkedPatientUrl;
    private static String reasonListUrl;

    public static String getOTPCheckAndSendUrl() {

        return getBaseURL() + "fisikeSignUp/checkUserAndSendOTP";
    }

    public static String getCheckPhoneNoandGetOtpurl() {
        return getBaseURL() + "userApi/checkPhoneNoAndSendOtp";
    }

    public static String getLinkedPatientUrl() {
        return getBaseURL() + "patientLinking/getLinkedPatients?patientId=";
    }

    public static String getReasonListUrl() {
        return getBaseURL() + "ValueSetList/search";
    }

    public static String getTimeSlotUrl() {
        return getBaseURL() + "moSlot/fetchServiceSlotsForZip";
    }

    public static String getBookAppointmentUrl(){
        return getBaseURL()+"moAppointment/bookAppointment";
    }

    public static String fetchSelfAndAssociatedPatientDetails() {
        return getBaseURL() + "moPatient/fetchSelfAndAssociatedPatientDetails";
    }

    public static String getSendOtpUrl() {
        return getBaseURL() + "moSignUp/sendOneTimePassword";
    }

    public static String getVerifyDobUrl() {
        return getBaseURL() + "moSignUp/validateDateOfBirth";
    }

    public static String getVerifyOtpSignupUrl() {
        return getBaseURL() +"moSignUp/verifyOneTimePassword";
    }

    public static String getStateCityUrl() {
        return getBaseURL() + "moCityState/fetchCountryStateMap";
    }

    public static String getCityUrl() {
        return getBaseURL() + "moCityState/fetchCityMap";
    }

    public static String getMoSignupUrl() {
        return getBaseURL() +"moSignUp/signUpUser" ;
    }

    public static String getFetchThirdPartyurl() {
        return getBaseURL() +"moSignUp/fetchUserDetailsFromThirdParty";
    }

    public static String getResetPasswordurl() {
        return getBaseURL() +"moUser/setNewPasswordWithOtp";
    }

    public static class K {
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String DEVICE_ID = "deviceUID";
        public static final String LANGUAGE = "userLanguage";
        public static final String CONTACT = "contact";
        public static final String OBJECT_TYPE = "objectType";
        public static final String OTP = "otp";
        public static final String PHONE_NUMBER = "phoneNo";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String EMAIL = "email";
        public static final String ID = "id";
        public static final String GENDER = "gender";
        public static final String DOB = "dob";
        public static final String ABOUT = "about";
        public static final String DISABLEACCOUNT = "disableAccount";
        public static final String ALTERNATE_CONTACT = "alternateContact";
        public static final String NEW_PASSWORD = "newPassword";
        public static final String USERTYPE = "userType";
        public static final String CONTACT_TYPE = "contactType";
        public static final String USERNAME_N_CAPITAL = "userName";
        public static final String USER_LANGUAGE = "userLanguage";
        public static final String PHONE = "phoneNumber";
        public static final String COUNTRY_CODE = "countryCode";
        public static final String CHECK_USER ="checkUser";
        public static final String SOURCE = "source";
        public static final String TYPE_MOBILE ="mobile";

    }

    public static String getBaseURL() {

        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
          httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/";
        return postURL;
    }

    // TODO Implement Proper structure

    public static String getLoginUrl() {
        return getBaseURL() + "api/login";
    }


    public static String getSignUpUrl() {
        return getBaseURL() + "fisikeSignUp/physicianSignUp";
    }

    public static String getSpecialityAndRoles() {
        return getBaseURL() + "util/physicianSpecialitiesAndDesignations";
    }

    public static String getUserDetailsUrl() {
        return getBaseURL() + "moUser/getUserDetails";
    }

    public static String getFetchContactsUrl() {
        return getBaseURL() + "userApi/getContactList";
    }

    public static String getInviteSearchUrl() {
        return getBaseURL() + "userApi/getAllUsers";
    }

    public static String getGroupChatList() {
        return getBaseURL() + "chat/setUpChat";
    }

    public static String getDeviceRegisterUrl() {
        return getBaseURL() + "mobileApp/registerDevice";
    }

    public static String getMedicationrUrl() {
        return getBaseURL() + "MedicationOrder/searchMedications";
    }

    public static String getUpdateVersionUrl() {
        return getBaseURL() + "util/getMobileAppConfig";
    }

    public static String getInviteIdUrl() {
        return getBaseURL() + "userApi/getUserGroupsAndOrgForInvite";
    }

    public static String getSendInviteUrl() {
        return getBaseURL() + "userApi/sendInvite";
    }

    public static String getAddToContactUrl() {
        return getBaseURL() + "userApi/addToContactList";
    }

    public static String getSignupStatusUrl() {
        return getBaseURL() + "util/getPhysicianSignUpStatus";
    }

    public static String getOTPVerifyUrl() {
        return getBaseURL() + "signUp/sendOtpAndGetUserStatus";
    }

    public static String getUpdateProfileUrl() {
        return getBaseURL() + "moUser/updateUserProfile";
    }

    public static String getChangePhoneNumberUrl() {
        return getBaseURL() + "userApi/changePhoneNo";
    }

    public static String getChangePasswordUrl() {
        return getBaseURL() + "userApi/changePassword";
    }

    public static String getPhysicianDetailUrl() {
        return getBaseURL() + "practitioner/show/";
    }

    public static String getUserImageUrl() {
        return getBaseURL() + "userApi/getProfilePicture";
    }

    public static String getCheckEmailExistenceUrl() {
        return getBaseURL() + "userApi/checkIfEmailExists";
    }

    public static String getPassWordChangeApiAdmin() {
        return getBaseURL() + "user/changePasswordOnFirstLogin";
    }

    public static String getDocumentSearchUrl() {
        return getBaseURL() + "DocumentReference/search";
    }

    public static String getDocumentImageUrl() {
        return getBaseURL() + "DocumentReference/downloadImage";
    }

    public static String getReportFile() {
        return getBaseURL() + "MoDiagnosticOrder/fetchPdfByteStream";
    }

    public static String getMeciationReportFile() {
        return getBaseURL() + "MedicationOrder/fetchAttachmentPdf";
    }

    public static String getDocumentImageUrl(int id) {
        return getDocumentImageUrl() + "?docRefId=" + id;
    }

    public static String getFetchContactImageUrl() {
        return getBaseURL() + "userApi/getProfilePicture";
    }

    public static String getFetchContactGroupImageUrl(String id) {
        if (id != null && id.contains("@"))
            id = id.split("@")[0];
        return getFetchContactImageUrl() + "?groupId=" + id;
    }

    public static String getFetchContactImageUrl(String id) {
        if (id != null && id.contains("@"))
            id = id.split("@")[0];
        return getFetchContactImageUrl() + "?id=" + id;
    }

    //https://fisiketest1.mphrx.com/minerva/Observation/search

    public static String getObservationDetailListingUrl() {
        return getBaseURL() + "MoObservation/search";
    }

    public static String getDiagnosticReportSearchPrimaryUrl() {
        return getBaseURL() + "MoDiagnosticReport/search";
    }

    public static String getObservationAppointment() {
        return getBaseURL() + "MoDiagnosticOrder/searchWithSync";
    }

    public static String getDiagnosticReportSearchSecondaryUrl() {
        return getBaseURL() + "Observation/search";
    }

    public static String getTosUrl() {
        return "https://get.fisike.com/webconnect/#/phy/termsofuse/";
    }

    public static String getFAQUrl() {
        return "https://get.fisike.com/webconnect/#/phy/faqs/";
    }


    public static String getFetchBookableUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchBookableTests";
    }


    public static String getfetchBookableSpecialtiesUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchBookableSpecialties";
    }

    public static String getFetchPopularTestsUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchPopularTests";
    }


    public static String getFetchPopularSpecialitiesUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchAllBookableSpecialties";
    }


    public static String getFetchResourcesForSpecialtyUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchResourcesForSpecialty";
    }

    public static String getPlaceTestOrderUrl() {
        return APIManager.createMinervaBaseUrl() + "/appointment/save/";
    }

    public static String getAppointmentSearchUrl() {
        return APIManager.createMinervaBaseUrl() + "/appointment/search/";

    }

    public static String getfetchAppointmentsUrl() {
        return APIManager.createMinervaBaseUrl() + "/moAppointment/fetchAppointments/";

    }

    public static String getSlotSearchUrl() {
        return APIManager.createMinervaBaseUrl() + "/slot/search/";
    }

    public static String getScheduleSearchUrl() {
        return APIManager.createMinervaBaseUrl() + "/schedule/search/";
    }


    public static String getFetchBookablePractitionerUrl() {
        return APIManager.createMinervaBaseUrl() + "/MoAppointmentEntity/fetchBookablePractitioners";
    }

    public static String getFetchRenderConfigUrl() {
        return APIManager.createMinervaBaseUrl() + "/user/renderConfigList";
    }

    public static String getHCSSearchUrl() {
        return APIManager.createMinervaBaseUrl() + "/healthcareService/search";
    }


    public static String getHCSShow() {
        return APIManager.createMinervaBaseUrl() + "/healthcareService/show/";
    }

    public static String getAppointmentCancelUrl() {
        return APIManager.createMinervaBaseUrl() + "/appointment/cancel/";

    }

    public static String getAppointmentRescheduleUrl() {
        return APIManager.createMinervaBaseUrl() + "/appointment/reschedule/";

    }

    public static String getAppointmentShowUrl(int appointmentID) {
        return APIManager.createMinervaBaseUrl() + "/appointment/show/" + appointmentID;
    }

    public static String getResendOTPTFAUrl() {

        return getBaseURL() + "citadelAuth/resendTFAOTP";
    }

    public static String getVitalsConfigUrl() {
        return getBaseURL() + "selectList/getVitalConfig";
    }

    public static String getFetchAgreementsUrl() {
        return getBaseURL() + "moManageAgreements/fetchAcceptedAgreementsByUser";
    }

    public static String getAgreementStatusUrl() {
        return getBaseURL() + "moManageAgreements/setAgreementAsRead";
    }

    public static String getAboutUsUrl() {
        return getBaseURL() + "MoUser/getAboutBoxDetails";
    }



    public static String getAddressSaveUrl() {
        return getBaseURL() + "moPatient/addAddressForPatient";
    }

    public static String getCancelAppointmentUrl() {
        return getBaseURL() + "moAppointment/cancelAppointment";
    }

    public static String getHealthCareServiceTimezoneUrl(){
        return getBaseURL() + "moHealthcareService/fetchHealthCareServiceTimeZone";
    }

    public static String getRegisterAssoiciatedPatientUrl(){
        return getBaseURL() + "moSignUp/registerAssociatedPatient";
    }

    public static String getUpdateProfileRequest(){
        return getBaseURL() + "moUser/updatePatientOrPractitionerWithUser";
    }
}
