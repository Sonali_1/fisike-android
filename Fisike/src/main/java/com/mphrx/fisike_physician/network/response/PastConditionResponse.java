package com.mphrx.fisike_physician.network.response;

import android.content.Context;
import android.content.SharedPreferences;

import com.mphrx.fisike.Queue.UploadDocumentTaskManager;
import com.mphrx.fisike.background.DownloadUploadedFileTask;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike_physician.models.PastConditionsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by laxmansingh on 5/25/2016.
 */
public class PastConditionResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;
    private Context context;
    ArrayList<PastConditionsModel> mPastConditionsResponseList = new ArrayList<PastConditionsModel>();


    public PastConditionResponse(JSONObject response, long transactionId,
                                 Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public PastConditionResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            jsonString = response.toString();
            processPastCondition();

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private void processPastCondition() {

        try {
            JSONObject mainObject = new JSONObject(jsonString);

            JSONArray jsonArray = mainObject.getJSONArray("list");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                PastConditionsModel pastConditionsModel=new PastConditionsModel();
                pastConditionsModel.setDisplay(jsonObject.getJSONObject("code").getJSONArray("coding").getJSONObject(0).getString("display").toString().trim());
                pastConditionsModel.setDuration(jsonObject.getJSONArray("extension").getJSONObject(0).getJSONArray("value").getJSONObject(0).getString("duration"));
                mPastConditionsResponseList.add(pastConditionsModel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<PastConditionsModel> getmPastConditionsResponseList() {
        return mPastConditionsResponseList;
    }

    public void setmPastConditionsResponseList(ArrayList<PastConditionsModel> mPastConditionsResponseList) {
        this.mPastConditionsResponseList = mPastConditionsResponseList;
    }
}
