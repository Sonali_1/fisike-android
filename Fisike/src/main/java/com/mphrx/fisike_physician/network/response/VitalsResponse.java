package com.mphrx.fisike_physician.network.response;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.enums.VitalLionCodeEnum;
import com.mphrx.fisike.models.VitalsHistoryModels;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike.persistence.VitalsDBAdapter;
import com.mphrx.fisike.vital_submit.request.DiagnosticIdentifier;
import com.mphrx.fisike.vital_submit.request.DiagnosticObservationCode;
import com.mphrx.fisike.vital_submit.request.DiagnosticObservations;
import com.mphrx.fisike.vital_submit.request.DiagnosticType;
import com.mphrx.fisike.vital_submit.request.ObservationValue;
import com.mphrx.fisike.vital_submit.request.ObservationValueUnit;
import com.mphrx.fisike_physician.models.PastConditionsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by laxmansingh on 8/1/2016.
 */
public class VitalsResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;
    private Context context;
    private boolean isHistory;
    ArrayList<VitalsModel> vitalsModelArrayList = new ArrayList<VitalsModel>();
    ArrayList<VitalsHistoryModels> vitalsHistoryModelsArrayList = new ArrayList<VitalsHistoryModels>();

    private final String NORMAL = "Normal";
    private final String OUT_OF_RANGE = "Out Of Range";
    private final String ELEVATED = "Elevated";
    private final String LOW = "Low";


    public VitalsResponse(JSONObject response, long transactionId,
                          Context context, boolean isHistory) {
        super(response, transactionId);
        this.context = context;
        this.isHistory = isHistory;
        parseResponse(response);
    }

    public VitalsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            jsonString = response.toString();
            if (isHistory)
                processVitalsHistory();
            else
                processVitals();

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private void processVitals() {
        vitalsModelArrayList.clear();
        try {
            JSONObject mainObject = new JSONObject(jsonString);
            JSONObject jsonObject = mainObject.getJSONObject("result");
            ArrayList<String> keysList = new ArrayList<>();
            Iterator iter = jsonObject.keys();
            String key = null;
            int j = 0;
            while (iter.hasNext()) {
                key = (String) iter.next();
                keysList.add(j, key);
                j++;
            }

            for (int i = 0; i < jsonObject.length(); i++) {

                final VitalsModel vitalsModel = new VitalsModel();
                vitalsModel.setParamId(keysList.get(i));
                JSONArray jsonArrayKey = jsonObject.optJSONArray(keysList.get(i));


                JSONObject jsonObjectd = jsonArrayKey.optJSONObject(0);
                if (jsonObjectd != null) {
                    vitalsModel.setDate(jsonObjectd.optString("date"));
                    vitalsModel.setId(jsonObjectd.optString("id"));
                    vitalsModel.setLabName(jsonObjectd.optString("labName"));
                    vitalsModel.setUnit(jsonObjectd.optString("unit"));
                    vitalsModel.setPatientName(jsonObjectd.optString("patientName"));
                    vitalsModel.setValue(jsonObjectd.optString("value"));
                    vitalsModel.setConvertedUnit(jsonObjectd.optString("convertedUnit"));
                    vitalsModel.setConvertedValue(jsonObjectd.optString("convertedValue"));
                    vitalsModel.setCode(jsonObjectd.optString("code"));

                    String highValue = "", lowValue = "";
                    if (jsonObjectd.optJSONObject("referenceRange") != null) {
                        highValue = jsonObjectd.optJSONObject("referenceRange").optJSONObject("high").optString("value");
                        lowValue = jsonObjectd.optJSONObject("referenceRange").optJSONObject("low").optString("value");
                        vitalsModel.setHighReferenceRangeUnit(jsonObjectd.optJSONObject("referenceRange").optJSONObject("high").optString("unit"));
                        vitalsModel.setHighReferenceRangeValue(highValue);
                        vitalsModel.setLowReferenceRangeUnit(jsonObjectd.optJSONObject("referenceRange").optJSONObject("low").optString("unit"));
                        vitalsModel.setLowReferenceRangeValue(lowValue);
                    }

                    if (jsonObjectd.optString("convertedValue").equals("") && jsonObjectd.optString("convertedValue") == null && jsonObjectd.optString("convertedValue").equals("null")) {
                        vitalsModel.setRange_status("");
                    } else {
                        try {
                            vitalsModel.setRange_status(setObservationParameter(jsonObjectd.optString("convertedValue"), key, jsonObjectd.optString("convertedUnit"), highValue, lowValue));
                        } catch (Exception e) {
                            vitalsModel.setRange_status("");
                        }
                    }
                }
                vitalsModelArrayList.add(i, vitalsModel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void processVitalsHistory() {
        vitalsHistoryModelsArrayList.clear();
        try {
            JSONObject mainObject = new JSONObject(jsonString);
            JSONObject jsonObject = mainObject.getJSONObject("result");
            ArrayList<String> keysList = new ArrayList<>();
            Iterator iter = jsonObject.keys();
            String key = null;
            int j = 0;
            while (iter.hasNext()) {
                key = (String) iter.next();
                keysList.add(j, key);
                j++;
            }


            for (int i = 0; i < jsonObject.length(); i++) {

                VitalsHistoryModels vitalsHistoryModels = new VitalsHistoryModels();
                vitalsHistoryModels.setSerialNumber(keysList.get(i));
                JSONArray jsonArrayKey = jsonObject.optJSONArray(keysList.get(i));
                vitalsModelArrayList = new ArrayList<>();
                int size = jsonArrayKey.length();
                for (int k = 0; k < size; k++) {
                    final VitalsModel vitalsModel = new VitalsModel();
                    vitalsModel.setParamId(keysList.get(i));

                    JSONObject jsonObjectd = jsonArrayKey.optJSONObject(k);
                    if (jsonObjectd != null) {
                        vitalsModel.setDate(jsonObjectd.optString("date"));
                        vitalsModel.setId(jsonObjectd.optString("id"));
                        vitalsModel.setLabName(jsonObjectd.optString("labName"));
                        vitalsModel.setUnit(jsonObjectd.optString("unit"));
                        vitalsModel.setPatientName(jsonObjectd.optString("patientName"));
                        vitalsModel.setValue(jsonObjectd.optString("value"));
                        vitalsModel.setConvertedUnit(jsonObjectd.optString("convertedUnit"));
                        vitalsModel.setConvertedValue(jsonObjectd.optString("convertedValue"));
                        vitalsModel.setCode(jsonObjectd.optString("code"));

                        String highValue = "", lowValue = "";
                        if (jsonObjectd.optJSONObject("referenceRange") != null) {
                            highValue = jsonObjectd.optJSONObject("referenceRange").optJSONObject("high").optString("value");
                            lowValue = jsonObjectd.optJSONObject("referenceRange").optJSONObject("low").optString("value");
                            vitalsModel.setHighReferenceRangeUnit(jsonObjectd.optJSONObject("referenceRange").optJSONObject("high").optString("unit"));
                            vitalsModel.setHighReferenceRangeValue(highValue);
                            vitalsModel.setLowReferenceRangeUnit(jsonObjectd.optJSONObject("referenceRange").optJSONObject("low").optString("unit"));
                            vitalsModel.setLowReferenceRangeValue(lowValue);
                        }
                        if (jsonObjectd.optString("convertedValue").equals("") && jsonObjectd.optString("convertedValue") == null && jsonObjectd.optString("convertedValue").equals("null")) {
                            vitalsModel.setRange_status("");
                        } else {
                            try {
                                vitalsModel.setRange_status(setObservationParameter(jsonObjectd.optString("convertedValue"), key, jsonObjectd.optString("convertedUnit"), highValue, lowValue));
                            } catch (Exception e) {
                                vitalsModel.setRange_status("");
                            }
                        }
                    }
                    vitalsModelArrayList.add(k, vitalsModel);
                }
                vitalsHistoryModels.setVitalsModels(vitalsModelArrayList);
                vitalsHistoryModelsArrayList.add(i, vitalsHistoryModels);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<VitalsModel> getVitalsModelArrayList() {
        return vitalsModelArrayList;
    }

    public void setVitalsModelArrayList(ArrayList<VitalsModel> vitalsModelArrayList) {
        this.vitalsModelArrayList = vitalsModelArrayList;
    }


    private String setObservationParameter(String val, String code, String unit, String highValue, String lowValue) {
        float observationValue = Float.parseFloat(val);

        String observationInterpretation = "";
        if (observationValue < Float.parseFloat(lowValue)) {
            observationInterpretation = "L";
        } else if (observationValue >= Float.parseFloat(lowValue) && observationValue <= Float.parseFloat(highValue)) {
            observationInterpretation = "Normal";
        } else {
            observationInterpretation = "H";
        }
        return setResponseObservationStatus(observationInterpretation);

       /* if (code.equals(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode())) {
            String observationLevel = "";
            if (observationValue < 70) {
                observationLevel = "L";
            } else if (observationValue >= 70 && observationValue <= 140) {
                observationLevel = "Normal";
            } else {
                observationLevel = "H";
            }
            return setResponseObservationStatus(observationLevel);
        } else if (code.equals(VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode())) {
            String observationLevel = "";
            if (observationValue < 60) {
                observationLevel = "L";
            } else if (observationValue >= 60 && observationValue <= 90) {
                observationLevel = "Normal";
            } else {
                observationLevel = "H";
            }
            return setResponseObservationStatus(observationLevel);
        } else if (code.equals(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode())) {
            float observationValueNew = 0;
            String observationLevel = "";
            if (unit.equalsIgnoreCase("mmol/L")) {
                observationValueNew = (float) (observationValue * 18);
                observationValueNew = (float) (Math.round(observationValueNew * 100) / 100.0);
                //  int mObservation= (int)(observationValue*100);
                //  observationValue=  (mObservation/100);
                if (observationValueNew < 70) {
                    observationLevel = "L";
                } else if (observationValueNew >= 70 && observationValueNew <= 100) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            } else {
                if (observationValue < 70) {
                    observationLevel = "L";
                } else if (observationValue >= 70 && observationValue <= 100) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            }
            return setResponseObservationStatus(observationLevel);
        } else if (code.equals(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode())) {
            String observationLevel = "";
            float observationValueNew = 0;
            if (unit.equalsIgnoreCase("mmol/L")) {
                observationValueNew = (float) (observationValue * 18);
                observationValueNew = (float) (Math.round(observationValueNew * 100) / 100.0);
                //  int mObservation= (int)(observationValue*100);
                //  observationValue=  (mObservation/100);
                if (observationValueNew < 70) {
                    observationLevel = "L";
                } else if (observationValueNew >= 70 && observationValueNew <= 110) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            } else {
                if (observationValue < 70) {
                    observationLevel = "L";
                } else if (observationValue >= 70 && observationValue <= 110) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            }
            return setResponseObservationStatus(observationLevel);
        } else if (code.equals(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode())) {
            float observationValueNew = 0;
            String observationLevel = "";
            if (unit.equalsIgnoreCase("mmol/L")) {
                observationValueNew = (float) (observationValue * 18);
                observationValueNew = (float) (Math.round(observationValueNew * 100) / 100.0);
                // int nObservation=(int)(observationValue*100);
                // observationValue=(nObservation/100);
                if (observationValueNew < 70) {
                    observationLevel = "L";
                } else if (observationValueNew >= 70 && observationValueNew <= 140) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            } else {
                if (observationValue < 70) {
                    observationLevel = "L";
                } else if (observationValue >= 70 && observationValue <= 140) {
                    observationLevel = "Normal";
                } else {
                    observationLevel = "H";
                }
            }
            return setResponseObservationStatus(observationLevel);
        } else if (code.equals(VitalLionCodeEnum.BMI_CODE.getCode())) {
            String observationLevel = "";
            if (observationValue < 18.5) {
                observationLevel = "L";
            } else if (observationValue >= 18.5 && observationValue <= 24.9) {
                observationLevel = "Normal";
            } else {
                observationLevel = "H";
            }
            return setResponseObservationStatus(observationLevel);
        }
        return "";*/
    }


    public String setResponseObservationStatus(String responseObservationStatus) {
        String status = "";
        if (responseObservationStatus.equals("L")) {
            status = LOW;
        } else if (responseObservationStatus.equals(NORMAL)) {
            status = NORMAL;
        } else {
            status = ELEVATED;
        }
        return status;
    }

    public ArrayList<VitalsHistoryModels> getVitalsHistoryModelsArrayList() {
        return vitalsHistoryModelsArrayList;
    }
}
