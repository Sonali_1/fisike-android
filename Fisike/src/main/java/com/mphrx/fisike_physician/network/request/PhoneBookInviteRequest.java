package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by brijesh on 24/11/15.
 */
public class PhoneBookInviteRequest extends BaseObjectRequest {

    private List<User> mUserList;
    private FisikeUserInviteRequest.InviteRequestListener mListener;

    public PhoneBookInviteRequest(List<User> userList, FisikeUserInviteRequest.InviteRequestListener listener) {
        mUserList = userList;
        mListener = listener;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getSendInviteUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mListener.onError(error, mUserList);
    }

    @Override
    public void onResponse(JSONObject response) {
        mListener.onSuccess(response, mUserList);
    }


    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < mUserList.size(); i++) {
                JSONObject object = new JSONObject();
                object.put("phone", mUserList.get(i).number);
                object.put("orgId", mUserList.get(i).organisationId);
                JSONArray userGroup = new JSONArray();
                if (!mUserList.get(i).name.equalsIgnoreCase("Patient"))
                    userGroup.put(Integer.parseInt(mUserList.get(i).inviteeRoleId));
                userGroup.put(Integer.parseInt(mUserList.get(i).roleId));
                object.put("userGroups", userGroup);
                array.put(object);
            }
            payLoad.put("inviteList", array);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
