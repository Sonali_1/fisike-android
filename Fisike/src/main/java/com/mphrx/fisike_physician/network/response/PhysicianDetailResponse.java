package com.mphrx.fisike_physician.network.response;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mphrx.fisike.beans.PhysicianDetails;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.Type;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 04/12/15.
 */
public class PhysicianDetailResponse extends BaseResponse {

    PhysicianDetails physicianData;
    public static int PHYSICIAN_ADDRESS = 0;
    public static int PHYSICIAN_EXPERIENCE = 1;
    public static int PHYSICIAN_ROLE = 2;
    public static int PHYSICIAN_SPECIALITY = 3;
    private ArrayList<Address> addresses;

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public PhysicianDetailResponse(JSONObject response, long transactionId, boolean saveSharedPref) {
        super(response, transactionId);

        AppLog.d("Physician response", response.toString());
        parseData(response, saveSharedPref);
    }

    public PhysicianDetailResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseData(JSONObject response, boolean saveSharedPref) {
        try {
            addresses = new ArrayList<Address>();
            if (!response.isNull("address")) {
                JSONArray addArray = response.getJSONArray("address");
                for (int i = 0; addArray != null && i < addArray.length(); i++) {
                    JSONObject add = addArray.getJSONObject(i);
                    String city = add.getString("city");
                    if (city == null && city.equals("")) {
                        continue;
                    }
                    Address address = new Address();
                    address.setState(add.getString("state"));
                    address.setCity(add.getString("city"));
                    JSONArray line = add.getJSONArray("line");
                    ArrayList<String> lineArray = new ArrayList<>();
                    for (int j = 0; j < line.length(); j++) {
                        lineArray.add(line.getString(j));
                    }
                    address.setLine(lineArray);
                    address.setPostalCode(add.getString("postalCode"));
                    address.setUseCode(add.getString("useCode"));
                    address.setText(add.getString("text"));

                    addresses.add(address);
                }
                /*Gson gson=new Gson();
                java.lang.reflect.Type listType = new TypeToken<List<Address>>() {}.getType();
                addresses=gson.fromJson(response.toString(),listType);*/
            }
            String experience = "";
            JSONArray exp = response.getJSONArray("extension");
            try {
                if (exp.length() > 0) {
                    experience = exp.getJSONObject(0).getJSONArray("value").getJSONObject(0).getString("noOfExp");
                }
            }catch (Exception e){
                experience = "";
            }

            String role = "";
            String speciality = new String();
            JSONArray roleArray = response.getJSONArray("practitionerRole");
            if (roleArray.length() > 0) {
                if (roleArray.optJSONArray(0) != null) {
                    JSONObject rol = roleArray.getJSONObject(0).optJSONObject("role");
                    if (rol != null) {
                        role = rol.optString("text");
                    }
                }
                JSONArray spe = roleArray.getJSONObject(0).optJSONArray("specialty");

                if (spe != null && spe.optJSONObject(0) != null) {
                    speciality = spe.optJSONObject(0).optString("text");
                    AppLog.d("Physician response", speciality);
                }
            }
            if (!saveSharedPref) {
                physicianData = new PhysicianDetails();
//                physicianData.add(address);
                physicianData.setExperience(experience);
                physicianData.setRole(role);
                physicianData.setSpeciality(speciality);
                physicianData.setAddresses(addresses);
                return;
            }
//            SharedPref.setAddress(address);
            SharedPref.setExperience(experience);
            SharedPref.setRole(role);
            SharedPref.setSpeciality(speciality);
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = "Unable to parse physician profile response.";
        }
    }

    public PhysicianDetails getPhysicianData() {
        return physicianData;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }
}
