package com.mphrx.fisike_physician.network.response;

import org.json.JSONObject;

/**
 * Created by brijesh on 03/12/15.
 */
public class ChangeNumberResponse extends BaseResponse {

    public ChangeNumberResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
    }

    public ChangeNumberResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

}
