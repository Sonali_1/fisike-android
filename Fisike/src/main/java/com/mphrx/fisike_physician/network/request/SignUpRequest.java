package com.mphrx.fisike_physician.network.request;

import android.graphics.Bitmap;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.SignUpResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by manohar on 19/11/15.
 */
public class SignUpRequest extends BaseObjectRequest {

    private String mDetails;
    private long mTransactionId;
    private Bitmap mBitmap;

    public SignUpRequest(String details, long transactionId) {
        this.mDetails = details;
        mTransactionId = transactionId;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getSignUpUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SignUpResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SignUpResponse(response, mBitmap, mTransactionId));
    }

    private String getPayLoad() {
        AppLog.showInfo(getClass().getSimpleName(), mDetails.toString());
        return mDetails.toString();
    }
}
