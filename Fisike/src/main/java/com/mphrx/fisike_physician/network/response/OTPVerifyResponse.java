package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by brijesh on 25/11/15.
 */
public class OTPVerifyResponse extends BaseResponse {

    private boolean mIsUserExisted;
    private boolean mIsEmailVerified;
    private String mEmailAddress;
    private String mSignUpStatus;
    private String mPhoneNumber;
    private UserMO mUserInfo;

    public OTPVerifyResponse(JSONObject response, String number, long transactionId) {
        super(response, transactionId);

        try {
            mIsUserExisted = response.getBoolean("userExist");
            mIsEmailVerified = response.getBoolean("verified");
            mSignUpStatus = response.optString("signUpStatus");
            mEmailAddress = response.optString("email");
            mPhoneNumber = number;
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.getString("user"), UserMO.class);
            mUserInfo = (UserMO) jsonToObjectMapper;
            if (BuildConfig.isPatientApp) {
                if (!mUserInfo.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                    mIsSuccessful = false;
                    mMessage = MyApplication.getAppContext().getString(R.string.err_number_already_assigned, mUserInfo.getUserType());
                }
            }
            else {
                if (mUserInfo.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                    mIsSuccessful = false;
                    mMessage = MyApplication.getAppContext().getString(R.string.err_number_already_assigned, "Patient");
                }
            }

        } catch (JSONException e) {
            mUserInfo = null;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }

    }

    public OTPVerifyResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public boolean isUserExisted() {
        return mIsUserExisted;
    }

    public boolean isEmailVerified() {
        return mIsEmailVerified;
    }

    public String getEmailAddress() {
        return mEmailAddress;
    }

    public String getSignUpStatus() {
        return mSignUpStatus;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public UserMO getUserInfo() {
        return mUserInfo;
    }

}
