package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike_physician.models.InviteIds;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by brijesh on 06/11/15.
 */
public class InviteIdResponse extends BaseResponse {

    private InviteIds mInviteIds;

    public InviteIdResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        parseData(response);
    }

    public InviteIdResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseData(JSONObject response) {

        try {
            mInviteIds = new InviteIds();
            JSONArray array = response.getJSONArray("userGroups");
            mInviteIds.mapping = new HashMap<>(array.length());
            mInviteIds.values = new ArrayList<>(array.length());
            mInviteIds.orgId = response.getString("defaultOrgId");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                mInviteIds.mapping.put(object.getString("displayValue"), object.getString("id"));
                if (!object.getString("name").equalsIgnoreCase("Clinical"))
                    mInviteIds.values.add(object.getString("displayValue"));
            }
            Collections.sort(mInviteIds.values);
        }
        catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }

    }

    public InviteIds getInviteIds() {
        return mInviteIds;
    }

}
