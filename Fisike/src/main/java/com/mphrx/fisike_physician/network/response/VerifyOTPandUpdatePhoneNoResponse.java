package com.mphrx.fisike_physician.network.response;

import android.speech.tts.TextToSpeech;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.mphrx.fisike.constant.TextConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Aastha on 17/02/2016.
 */
public class VerifyOTPandUpdatePhoneNoResponse extends BaseResponse{

    String msg;
    String status;
    boolean verified;
    //JSONObject map;


    public VerifyOTPandUpdatePhoneNoResponse(JSONObject response,long mTransactionId){
       super(response,mTransactionId);
        try{
           // map=response.getJSONObject("statusMap");
            msg=response.getString("msg");
            status=response.getString("status");
            verified= response.getBoolean("verified");
        }catch (JSONException je){
            msg= TextConstants.UNEXPECTED_ERROR;
            verified=false;
        }
        catch (Exception e)
        {
            msg= TextConstants.UNEXPECTED_ERROR;
            verified=false;
        }

    }
    public VerifyOTPandUpdatePhoneNoResponse(VolleyError error, long mTransactionId) {
        super(error,mTransactionId);
    }
    public String getMsg() {
        return msg;
    }
    public boolean getVerified() {
        return verified;
    }

    public String getStatus() {
        return status;
    }
   /* public JSONObject getJSONObject(){
        return map;
    }*/
}
