package com.mphrx.fisike_physician.network.response;

import android.content.Context;

import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.models.ContentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by aastha on 5/4/2017.
 */

public class FetchAllAgreementsResponse extends BaseResponse {

    Context mContext;
    ArrayList<AgreementModel> agreementModelsList = new ArrayList<>();
    public FetchAllAgreementsResponse(JSONObject response, long transactionId,
                                      Context context) {
        super(response,transactionId);
        mContext = context;
        parseResponse(response.toString());
    }

    private void parseResponse(String response) {
        try {
            JSONObject responsejson = new JSONObject(response);
            JSONArray accepted = responsejson.optJSONArray("acceptedAgreements");
            for(int i=0; i<accepted.length();i++){
                JSONObject jsonObject = accepted.optJSONObject(i);
                String readDate = jsonObject.optString("readDate");
                String updatedDate = jsonObject.optString("lastUpdated");
                int agreementId = jsonObject.optInt("agreementId");
                AgreementModel agreementModel = new AgreementModel();
                ArrayList<ContentResult> contentResults = new ArrayList<>();
                JSONArray contentResultsJsonArray = jsonObject.optJSONArray("contentResult");
                for(int j=0; j<contentResultsJsonArray.length(); j++){
                     JSONObject contentResultObject = contentResultsJsonArray.optJSONObject(j);
                    ContentResult contentResult = new ContentResult();
                    contentResult.setContent(contentResultObject.optString("content"));
                    contentResult.setLanguage(contentResultObject.optString("language"));
                    contentResult.setName(contentResultObject.optString("name"));
                    contentResults.add(j,contentResult);
                }
                agreementModel.setAgreementId(agreementId);
                agreementModel.setReadDate(readDate);
                agreementModel.setLastUpdated(updatedDate);
                agreementModel.setContentResult(contentResults);
                agreementModelsList.add(i,agreementModel);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public FetchAllAgreementsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }


    public ArrayList<AgreementModel> getAgreementModelsList() {
        return agreementModelsList;
    }
}
