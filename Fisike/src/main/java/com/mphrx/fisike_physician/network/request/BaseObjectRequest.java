package com.mphrx.fisike_physician.network.request;

import com.android.volley.Response;

import org.json.JSONObject;

import java.util.concurrent.Callable;

/**
 * Created by brijesh on 15/10/15.
 */
public abstract class BaseObjectRequest implements Callable<Void>, Response.Listener<JSONObject>, Response.ErrorListener {

    @Override
    public Void call() throws Exception {
        doInBackground();
        return null;
    }

    public abstract void doInBackground();

}
