package com.mphrx.fisike_physician.network.response;

import android.text.TextUtils;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.volleyRequest.UpdatePhysicianResponseGson;
import com.mphrx.fisike_physician.models.PhysicianMO;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

/**
 * Created by brijesh on 03/12/15.
 */
public class UpdateProfileResponse extends BaseResponse {

    UpdatePhysicianResponseGson physicianResponseGson;

    public UpdateProfileResponse(JSONObject response, PractitionerMO physicianMO, long transactionId) {
        super(response, transactionId);

        //        adding speciality
        try {
            SharedPref.setSpeciality(physicianMO.getPractitionerRole().get(0).getSpecialty().get(0).getText());
        } catch (Exception e) {
        }
        //        adding role
        try {
            SharedPref.setRole(physicianMO.getPractitionerRole().get(0).getRole().getText());
        } catch (Exception e) {
        }
        //        adding experience
        try {
            SharedPref.setExperience(physicianMO.getYearsOfExperience());
        } catch (Exception e) {
        }

        try {
//            UserMO userMO = SettingManager.getInstance().getUserMO();
//            userMO.setFirstName(physicianMO.firstName);
//            userMO.setLastName(physicianMO.lastName);
//            SettingManager.getInstance().updateUserMO(userMO);
//            SharedPref.setAddress(physicianMO.address);
            physicianResponseGson = (UpdatePhysicianResponseGson) GsonUtils.jsonToObjectMapper(response.toString(), UpdatePhysicianResponseGson.class);
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = "Something went wrong...!!!";
        }
    }

    public UpdateProfileResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public UpdatePhysicianResponseGson getUpdatePhysicianResponseGSON() {
        return physicianResponseGson;
    }
}
