package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.ChangePasswordResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by brijesh on 03/12/15.
 */
public class ChangePasswordRequest extends BaseObjectRequest {

    private String mOldPassword;
    private String mNewPassword;
    private long mTransactionId;

    public ChangePasswordRequest(String oldPassword, String newPassword, long transactionId) {
        mOldPassword = oldPassword;
        mNewPassword = newPassword;
        mTransactionId = transactionId;
    }


    @Override
    public void doInBackground() {
        String url = MphRxUrl.getChangePasswordUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        String oldPassword = SettingManager.getInstance().getUserMO().getPassword();
        if (oldPassword.equals(mOldPassword)) {
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
            Network.getGeneralRequestQueue().add(request);
        }
        else {
            AppLog.showError(getClass().getSimpleName(), "Incorrect Old Password");
            BusProvider.getInstance().post(new ChangePasswordResponse(mTransactionId));
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new ChangePasswordResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new ChangePasswordResponse(response, mTransactionId));
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put("newPassword", mNewPassword);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

}
