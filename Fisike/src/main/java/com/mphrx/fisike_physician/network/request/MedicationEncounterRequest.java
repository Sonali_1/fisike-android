package com.mphrx.fisike_physician.network.request;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.MedicationEncounterResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;


/**
 * Created by iappstreet on 17/03/16.
 */
public class MedicationEncounterRequest extends BaseObjectRequest {


    private ProgressDialog dialog;
    private JSONObject payLoad;
    private long transactionId;
    private Context context;
    private boolean isMedicationSyncApi;

    // when prescriber,medicine name is updated no need to reset the alarm.
    private boolean isAlarmResetCheckRequired = false;


    public MedicationEncounterRequest(JSONObject payLoad, long transactionId, Context context,
                                      boolean isMedicationSyncApi) {
        this.payLoad = payLoad;
        this.transactionId = transactionId;
        this.context = context;
        this.isMedicationSyncApi = isMedicationSyncApi;
    }

    public MedicationEncounterRequest(JSONObject payLoad, long transactionId, Context context,
                                      boolean isMedicationSyncApi, boolean isAlarmResetCheckRequired) {
        this.payLoad = payLoad;
        this.transactionId = transactionId;
        this.context = context;
        this.isMedicationSyncApi = isMedicationSyncApi;
        this.isAlarmResetCheckRequired = isAlarmResetCheckRequired;
    }

    @Override
    public void doInBackground() {
        String baseUrl = APIManager.createMinervaBaseUrl() + CachedUrlConstants.MEDICATION_PRESCRIPTION;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new MedicationEncounterResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        if(response != null) {
            BusProvider.getInstance().post(new MedicationEncounterResponse(response, transactionId,
                    context, isMedicationSyncApi, isAlarmResetCheckRequired));
        }
    }

    public String getPayLoad() {
        return payLoad.toString();
    }
}
