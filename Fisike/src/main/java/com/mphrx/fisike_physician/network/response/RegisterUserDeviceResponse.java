package com.mphrx.fisike_physician.network.response;

import org.json.JSONObject;

/**
 * Created by brijesh on 01/11/15.
 */
public class RegisterUserDeviceResponse extends BaseResponse {

    public RegisterUserDeviceResponse(JSONObject response, long transactionId) {
        super(response, transactionId);

    }

    public RegisterUserDeviceResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

}
