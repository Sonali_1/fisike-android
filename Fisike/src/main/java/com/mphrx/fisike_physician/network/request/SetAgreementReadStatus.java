package com.mphrx.fisike_physician.network.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.FetchAllAgreementsResponse;
import com.mphrx.fisike_physician.network.response.SetAgreementReadStatusResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.UserDetailResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

/**
 * Created by aastha on 5/4/2017.
 */

public class SetAgreementReadStatus extends BaseObjectRequest {
    
    private String status,payload;
    Context context;
    private long transactionId;

    public SetAgreementReadStatus(long transactionId, Context context,String status, String payload) {
        this.transactionId = transactionId;
        this.context = context;
        this.status = status;
        this.payload = payload;
    }

    @Override
    public void doInBackground() {
        String baseUrl = MphRxUrl.getAgreementStatusUrl();
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, payload, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SetAgreementReadStatusResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SetAgreementReadStatusResponse(response, transactionId,status));
    }
}
