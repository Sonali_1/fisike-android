package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike_physician.models.User;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by brijesh on 20/11/15.
 */
public class PendingInviteListResponse extends BaseResponse {

    private List<User> mUserList;

    public PendingInviteListResponse(List<User> list, long transactionId) {
        super(new JSONObject(), transactionId);
        mUserList = list;
    }

    public PendingInviteListResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public List<User> getUserList() {
        return mUserList;
    }
}
