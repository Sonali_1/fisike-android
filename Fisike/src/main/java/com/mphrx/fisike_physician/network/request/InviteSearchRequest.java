package com.mphrx.fisike_physician.network.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.SearchResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by brijesh on 24/11/15.
 */
public class InviteSearchRequest extends BaseObjectRequest {

    private long mTransactionId;
    private String mSearchString;
    private boolean mIsContactsReadPermissionGranted;
    private boolean shouldInsertToDB;

    public InviteSearchRequest(long transactionId, String searchString, boolean permissionGranted, boolean shouldInsertToDB) {
        mSearchString = searchString;
        mTransactionId = transactionId;
        mIsContactsReadPermissionGranted = permissionGranted;
        this.shouldInsertToDB = shouldInsertToDB;
    }

    @Override
    public void doInBackground() {
        // throw phone book data first
        BusProvider.getInstance().post(new SearchResponse(mTransactionId, mSearchString, mIsContactsReadPermissionGranted, shouldInsertToDB));

        Network.getGeneralRequestQueue().cancelAll(this.getClass().getSimpleName());
        String url = MphRxUrl.getInviteSearchUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        Log.i("usertypepayload",getPayload());
        request.setTag(this.getClass().getSimpleName());
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SearchResponse(error, mTransactionId, mSearchString, mIsContactsReadPermissionGranted));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SearchResponse(response, mTransactionId, mSearchString, mIsContactsReadPermissionGranted, shouldInsertToDB));
    }

    private String getPayload() {
        JSONObject object = new JSONObject();
        try {
            object.put("searchString", mSearchString);
            AppLog.showInfo(getClass().getSimpleName(), object.toString());
        }
        catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            e.printStackTrace();
        }
        return object.toString();
    }
}
