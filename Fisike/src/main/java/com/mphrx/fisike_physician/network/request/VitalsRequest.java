package com.mphrx.fisike_physician.network.request;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.enums.VitalLionCodeEnum;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by laxmansingh on 8/1/2016.
 */
public class VitalsRequest extends BaseObjectRequest {


    private ProgressDialog dialog;
    private long transactionId;
    private Context context;
    private String userid;

    public VitalsRequest(long transactionId, Context context) {
        this.transactionId = transactionId;
        this.context = context;
    }


    public VitalsRequest(long transactionId, Context context, String userid) {
        this.transactionId = transactionId;
        this.context = context;
        this.userid = userid;
    }


    @Override
    public void doInBackground() {
        String baseUrl = APIManager.createMinervaBaseUrl() + CachedUrlConstants.vitals_postfix;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new VitalsResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        if(response==null){
            VolleyError volleyError = new VolleyError();
            AppLog.showError(getClass().getSimpleName(), volleyError.getMessage());
            BusProvider.getInstance().post(new VitalsResponse(volleyError, transactionId));
        }
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new VitalsResponse(response, transactionId, context,false));
    }
    public String getPayLoad() {
        try {
            JSONObject jsonObject = new JSONObject();
            if (BuildConfig.isPatientApp) {
                jsonObject.put("patientId", SettingManager.getInstance().getUserMO().getPatientId());
            } else if (BuildConfig.isPhysicianApp) {
                jsonObject.put("patientId", userid);
            }
            JSONArray paramidArray = new JSONArray();
            paramidArray.put(0, VitalLionCodeEnum.BMI_CODE.getCode());
            paramidArray.put(1, VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode());
            paramidArray.put(2, VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode());
            paramidArray.put(3, VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode());
            paramidArray.put(4, VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode());
            paramidArray.put(5, VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode());

            jsonObject.put("mparamID", paramidArray);
            jsonObject.put("_count",1);
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
