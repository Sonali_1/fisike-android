package com.mphrx.fisike_physician.network.response;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ErrorUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by brijesh on 15/10/15.
 */
public class BaseResponse {

    private String responseBody;
    private long mTransactionId;
    private String mResponse;
    protected String mMessage;
    protected boolean mIsSuccessful;
    private String msg;
    private String httpStatusCode;
    private String statusCode;
    private String code;
    private NetworkResponse networkResponse;
    private boolean infoStatus = false;
    private String inforMessage = "";
    private String infoResponseStatus = "";
    private String infoCode = "";
    private String errorMessage,errorResponseStatus,errorCode;
    private boolean errorStatus;


    public BaseResponse() {
    }

    public BaseResponse(JSONObject response, long transactionId) {
        this.mTransactionId = transactionId;
        mIsSuccessful = true;
        mResponse = response.toString();
        mMessage = "";
        parseInfoParameters(response);
    }

    private void parseInfoParameters(JSONObject response) {
        try {
            JSONArray infoarray = response.optJSONArray("info");
            if (infoarray != null && infoarray.length() > 0) {
                JSONObject element = infoarray.getJSONObject(0);
                infoStatus = element.optBoolean("status");
                inforMessage = element.optString("message");
                infoResponseStatus = element.optString("responseStatus");
                infoCode = element.optString("code");
            }
        } catch (Exception e) {

        }
    }

    private void parseErrorParameters(JSONObject response) {
        try {
            JSONArray errors = response.optJSONArray("errors");
            if (errors != null && errors.length() > 0) {
                JSONObject element = errors.getJSONObject(0);
                errorStatus = element.optBoolean("status");
                errorMessage = element.optString("message");
                errorResponseStatus = element.optString("responseStatus");
                errorCode = element.optString("code");
            }
        } catch (Exception e) {
            errorStatus = false;
            errorResponseStatus = "404";
            errorCode="";
        }
    }
    public BaseResponse(Exception exception, long transactionId) {
        this.mTransactionId = transactionId;
        mIsSuccessful = false;

        if (exception instanceof VolleyError) {
            // attempt to parse the actual body
            if (((VolleyError) exception).networkResponse != null) {
                NetworkResponse response = ((VolleyError) exception).networkResponse;
                this.networkResponse = response;
                int statusCode = response.statusCode;
                this.statusCode = "" + statusCode;
                /*We wont set access token as null if API gives 401 @Aastha,TODO add check for authorization */
                /*if (statusCode == 401) {
                    SharedPref.setAccessToken(null);
                }*/
                if ((statusCode+"").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {

                    Utils.launchLocationChangeActivity(MyApplication.getAppContext());
                }

                try {
                    responseBody = new String(response.data, "utf-8");
                    parseErrorResponse(new JSONObject(responseBody));
                } catch (JSONException exc) {
                    responseBody = "";
                    this.statusCode = "" + response.statusCode;
                    errorResponseStatus = "404";
                    errorStatus = false;
                } catch (Exception ignore) {
                    //  httpStatusCode=""+statusCode;
                }
            }

            if (TextUtils.isEmpty(mMessage)) {
                // set a reasonable fallback value based on volley-error
                if (exception instanceof NetworkError ||
                        exception instanceof TimeoutError) {
                    mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_network);
                } else if (exception instanceof AuthFailureError) {
                    mMessage = MyApplication.getAppContext().getResources().getString(R.string.authorization_error);
                } else if (exception instanceof ParseError ||
                        exception instanceof ServerError) {
                    mMessage = MyApplication.getAppContext().getResources().getString(R.string.unexpected_error);
                } else {
                    mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
                }

            }
        } else {
            mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
        }
    }

    private void parseErrorResponse(JSONObject response) throws JSONException {
        msg = response.optString("msg");
        statusCode = response.optString("status");
        httpStatusCode = response.optString("httpStatusCode");
        mMessage = ErrorUtils.getErrorMessage(msg, statusCode, httpStatusCode);
        parseErrorParameters(response);
    }


    public long getTransactionId() {
        return mTransactionId;
    }

    public String getResponse() {
        return mResponse;
    }

    public String getMessage() {
        if (mMessage == null) {
            return "";
        }
        return mMessage;
    }

    public boolean isSuccessful() {
        return mIsSuccessful;
    }

    public String getMsg() {
        return msg;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public NetworkResponse getNetworkResponse() {
        return networkResponse;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;}

    public boolean isInfoStatus() {
        return infoStatus;
    }

    public void setInfoStatus(boolean infoStatus) {
        this.infoStatus = infoStatus;
    }

    public String getInforMessage() {
        return inforMessage;
    }

    public void setInforMessage(String inforMessage) {
        this.inforMessage = inforMessage;
    }

    public String getInfoResponseStatus() {
        return infoResponseStatus;
    }

    public void setInfoResponseStatus(String infoResponseStatus) {
        this.infoResponseStatus = infoResponseStatus;
    }

    public String getInfoCode() {
        return infoCode;
    }

    public void setInfoCode(String infoCode) {
        this.infoCode = infoCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorResponseStatus() {
        return errorResponseStatus;
    }

    public void setErrorResponseStatus(String errorResponseStatus) {
        this.errorResponseStatus = errorResponseStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(boolean errorStatus) {
        this.errorStatus = errorStatus;
    }
}
