package com.mphrx.fisike_physician.network.request;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.ContactResponse;
import com.mphrx.fisike_physician.network.response.SearchResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by brijesh on 15/10/15.
 */
public class ContactRequest extends BaseObjectRequest {

    public enum ContactType {
        ALL_CONTACT,
        PATIENT_CONTACT,
        PHYSICIAN_CONTACT
    }

    private static final int MAX_LIMIT = 30;

    private ContactType mContactType;
    private int mStartingIndex;

    private long mTransactionId;
    private String mSearchString;
    private boolean mIsContactsReadPermissionGranted;
    boolean shouldInsertInDB;
    private boolean mIsPullToRefreshCall;

    public ContactRequest(long transactionId, ContactType type, int startingIndex, boolean isPullToRefreshCall) {
        mTransactionId = transactionId;
        mContactType = type;
        mStartingIndex = startingIndex;
        mIsContactsReadPermissionGranted = false;
        mIsPullToRefreshCall = isPullToRefreshCall;
    }

    public ContactRequest(long transactionId, String searchString, boolean permissionGranted, ContactType type, boolean shouldInsertInDB) {
        mTransactionId = transactionId;
        mSearchString = searchString;
        mIsContactsReadPermissionGranted = permissionGranted;
        mContactType = type;
        mIsPullToRefreshCall = false;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchContactsUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if (TextUtils.isEmpty(mSearchString)) {
            BusProvider.getInstance().post(new ContactResponse(error, mTransactionId, mIsPullToRefreshCall));
        } else {
            BusProvider.getInstance().post(new SearchResponse(error, mTransactionId, mSearchString, mIsContactsReadPermissionGranted));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        if (TextUtils.isEmpty(mSearchString)) {
            BusProvider.getInstance().post(new ContactResponse(response, mTransactionId, mIsPullToRefreshCall));
        } else {
            BusProvider.getInstance().post(new SearchResponse(response, mTransactionId, mSearchString, mIsContactsReadPermissionGranted, shouldInsertInDB));
        }
    }

    private String getPayload() {
        JSONObject object = new JSONObject();
        try {
            object.put("_count", MAX_LIMIT);
            object.put("_skip", mStartingIndex);
            if (!TextUtils.isEmpty(mSearchString))
                object.put("searchString", mSearchString);
            if (mContactType == ContactType.PATIENT_CONTACT)
                object.put("userType", "Patient");
            else if (mContactType == ContactType.PHYSICIAN_CONTACT)
                object.put("userType", "Clinical");
            AppLog.showInfo(getClass().getSimpleName(), object.toString());
        }
        catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            e.printStackTrace();
        }

        return object.toString();
    }

}
