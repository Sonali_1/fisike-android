package com.mphrx.fisike_physician.network.response;

import org.json.JSONObject;

/**
 * Created by brijesh on 03/11/15.
 */
public class LoginXMPPServerResponse extends BaseResponse {

    public LoginXMPPServerResponse() {
        super(new JSONObject(), 0);
    }

    public LoginXMPPServerResponse(Exception e) {
        super(e, 0);
    }

}
