package com.mphrx.fisike_physician.network.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.UserDetailResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by brijesh on 29/10/15.
 */
public class UserDetailRequest extends BaseObjectRequest {

    private String mPassword;
    private long mTransactionId;
    private Context context;

    public UserDetailRequest(String pwd, long transactionId,Context context) {
        mPassword = pwd;
        mTransactionId = transactionId;
        this.context = context;
        String url = MphRxUrl.getUserDetailsUrl();
        Network.getGeneralRequestQueue().getCache().remove(url + getPayLoad());
        Network.getGeneralRequestQueue().getCache().invalidate(url + getPayLoad(), true);
    }

    @Override
    public void doInBackground() {
        // get user details
        String url = MphRxUrl.getUserDetailsUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new UserDetailResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new UserDetailResponse(response, mPassword, mTransactionId));
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.DEVICE_ID, SharedPref.getDeviceUid());
            payLoad.put(MphRxUrl.K.LANGUAGE, Utils.getUserSelectedLanguage(context));
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

}
