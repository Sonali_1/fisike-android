package com.mphrx.fisike_physician.network.response;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by aastha on 5/4/2017.
 */

public class SetAgreementReadStatusResponse extends BaseResponse {

    String status;
    public SetAgreementReadStatusResponse(JSONObject response, long transactionId, String status) {
        super(response, transactionId);
        this.status =status;
    }

    public SetAgreementReadStatusResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public String getStatus() {
        return status;
    }
}
