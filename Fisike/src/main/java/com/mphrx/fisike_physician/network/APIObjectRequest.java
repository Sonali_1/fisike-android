package com.mphrx.fisike_physician.network;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.volley.wrappers.JsonObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by brijesh on 15/10/15.
 */
public class APIObjectRequest extends JsonObjectRequest {

    private static final CharSequence LOGIN_API = "api/login";
    private static final CharSequence PATIENT_SIGNUP = "patientSignUp";
    private static final CharSequence GET_VITALS = "MoObservation/getVitals";
    private static final CharSequence SIGNUP_USER = "moSignUp/signUpUser";
    String url;

    public APIObjectRequest(int method, String url, String requestBody, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.url = url;
        if (BuildConfig.isPatientApp) {

            if (url.contains(CachedUrlConstants.DIAGNOSTICREPORTSEARCH)
                    || url.contains(CachedUrlConstants.OBSERVATIONSEARCH) || url.endsWith(CachedUrlConstants.GETTASKDETAIL)) {
                setShouldCache(true);
            } else {
                setShouldCache(false);
            }

        } else {
            setShouldCache(false);
        }

        setRetryPolicy(new DefaultRetryPolicy(MphRxUrl.TIMEOUT, MphRxUrl.RETRY, MphRxUrl.BACKOFF));

    }

    public String getCacheKey() {
        String key = "";
        String body = new String(getBody());
        key = getUrl() + body;

        return key;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        if (SharedPref.getAccessToken() != null && !SharedPref.isUserLogout())
            params.put("x-auth-token", SharedPref.getAccessToken());

        String headerVersion = "V1";

        if (url.contains("/MoObservation/getVitals") || url.contains("moObservation/saveList")
                || url.contains("selectList/getVitalConfig")|| url.contains("moSignUp/verifyOneTimePassword")|| url.contains("api/login"))
            headerVersion = "V2";
        else
            headerVersion="V1";

        params.put("api-info", APIManager.getInstance().getHeader(MyApplication.getAppContext(), headerVersion));

        //changed for long live token FIS-6962 by Aastha
        if (url.contains(LOGIN_API) || url.contains(PATIENT_SIGNUP)||url.contains(SIGNUP_USER)) {
            params.put("clientId", "AIzaSyAZr7JuVKx6N0jliRlOnTM0yKfRyFDXjcE");
            params.put("secret", "mobile");
            params.put("grant_type", "JWT");
        }
        AppLog.showInfo(getClass().getSimpleName(), params.toString());
        return params;
    }


}
