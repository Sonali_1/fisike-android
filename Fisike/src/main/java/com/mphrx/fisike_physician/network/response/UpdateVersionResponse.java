package com.mphrx.fisike_physician.network.response;

import android.util.ArraySet;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.persistence.ConfigDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import searchpatient.constants.KeyConstants;

/**
 * Created by iappstreet on 13/01/16.
 */
public class UpdateVersionResponse extends BaseResponse {
    String softVersion = new String();
    String forceVersion = new String();
    int currentVersion;
    private long alternateContactPromptInDays;
    private int pinLockTime = 0;
    private String defaultPasswordType = new String();
    private long passwordLength;
    private String characterSet;
    private long passwordHistoryCount;
    private long autoLockMinutes;
    private String otpAttemptsTotal;
    private String passwordRegex;


    public UpdateVersionResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        currentVersion = DeviceUtils.getAppVersionString();
//        currentVersion = 103;    // FIXME remove after testing
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {

        try {
            if (BuildConfig.isPatientApp) {
                JSONObject jsonPatientApp = response.getJSONObject("androidPatientApp");
                getAllVersions(jsonPatientApp);
            } else {
                JSONObject jsonPhysicianApp = response.getJSONObject("androidPhysicianApp");
                getAllVersions(jsonPhysicianApp);
            }

            ConfigMO config = ConfigManager.getInstance().getConfig();
            alternateContactPromptInDays = response.getLong("alternateContactPromptInDays");
            pinLockTime = response.getInt("pinTimeInMinute");
            // Changes by Aastha for admin flow
            try {
                otpAttemptsTotal = response.optString("otpMaximumAttempt");
                JSONObject passwordConfig = response.getJSONObject("passwordConfig");
                defaultPasswordType = passwordConfig.optString("defaultPasswordType");
                passwordLength = passwordConfig.optLong("minPasswordLength");
                characterSet = passwordConfig.optString("characterSet");
                passwordHistoryCount = passwordConfig.optLong("passwordHistoryCount");
                autoLockMinutes = passwordConfig.optLong("autoLockMinutes");
                passwordRegex = response.optString("passwordRegex");

            } catch (Exception e) {
                otpAttemptsTotal = "0";
                defaultPasswordType = "";
                passwordLength = CustomizedTextConstants.PASSWORD_LENGTH;
                characterSet = null;
                passwordRegex = null;
                passwordHistoryCount = 0;
                autoLockMinutes = 0;
            }
            //set Password policy parameters
            SharedPref.setDefaultPasswordType(defaultPasswordType);
            SharedPref.setPasswordLength(passwordLength);
            SharedPref.setPasswordHistoryCount(passwordHistoryCount);
            String orderNumberKey = response.optString("showDiagnosticOrderNumber");
            SharedPref.setOrderNumberKey(orderNumberKey == null ? TextConstants.RESULT_ORDER_KEY_DEFAULT : orderNumberKey);
//            JSONArray availableModalities = response.optJSONArray("availableModalities");
            /*Set<String> modalitiesSet = new HashSet<>();
            for (int i = 0; availableModalities != null && i < availableModalities.length(); i++) {
                modalitiesSet.add(availableModalities.getJSONObject(i).optString("value"));
                SharedPref.setStringSet(KeyConstants.RESULT_MODALITY_KEYS, modalitiesSet);
            }
            JSONArray availableRecordTypes = response.optJSONArray("availableRecordTypes");
            Set<String> typeSet = new HashSet<>();
            for (int i = 0; availableRecordTypes != null && i < availableRecordTypes.length(); i++) {
                typeSet.add(availableRecordTypes.getJSONObject(i).optString("value"));
                SharedPref.setStringSet(KeyConstants.RESULT_TYPE_KEYS, modalitiesSet);
            }

            JSONArray availableRecordStatuses = response.optJSONArray("availableRecordStatuses");
            Set<String> statusSet = new HashSet<>();
            for (int i = 0; availableRecordStatuses != null && i < availableRecordStatuses.length(); i++) {
                statusSet.add(availableRecordStatuses.getJSONObject(i).optString("value"));
                SharedPref.setStringSet(KeyConstants.RESULT_STATUS_KEYS, modalitiesSet);
            }*/

            SharedPref.setPasswordRegex(characterSet);
            //set isSignUpAvailableToUser
            boolean isSelfSignUpAvailable = response.optBoolean("isSelfSignUpAvailable", BuildConfig.isSelfSignUpEnable);
            SharedPref.setIsSelfSignUpEnabled(isSelfSignUpAvailable);

            // Changes by Aastha for admin flow
            config.setNoOfDaysForAlternateContact(alternateContactPromptInDays);
            config.setPinLockTime(pinLockTime);
            config.setAutoLockMinute(autoLockMinutes);
            config.setOtpAttemptsLeft(otpAttemptsTotal);
            config.setPasswordRegex(passwordRegex);
            //Config for set languages
            LinkedHashMap<String, String> languageList = new LinkedHashMap<>();
            JSONArray languageArray = response.optJSONArray("languagesList");
            for (int i = 0; languageArray != null && i < languageArray.length(); i++) {
                JSONObject language = languageArray.getJSONObject(i);
                languageList.put(language.getString("display"), language.getString("language"));

            }
            if (languageList.size() == 0) {
                languageList.put(TextConstants.DEFALUT_LANGUAGE_TITLE, TextConstants.DEFALUT_LANGUAGE_KEY);
            }
            config.setLanguageList(languageList);
            ConfigManager.getInstance().updateConfig(config);
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }

    }


    private void getAllVersions(JSONObject jsonApp) {
        try {
            softVersion = jsonApp.getString("softVersion");
            forceVersion = jsonApp.getString("forceVersion");
        } catch (Exception e) {
            mIsSuccessful = false;
        }
    }


    public boolean isSoftUpdateRequired() {
        return isLowerVersion(currentVersion, softVersion);
//        return isCurrentVersionLower(currentVersion,softVersion);
    }

    public boolean isForceUpdateRequired() {
        return isLowerVersion(currentVersion, forceVersion);
//        return isCurrentVersionLower(currentVersion, forceVersion);
    }


    private int stringVersionToIntVersion(String version) {
        String tmp = version.replaceAll("\\D", "");
        try {
            return Integer.parseInt(tmp);
        } catch (NumberFormatException ignore) {
            return 0;
        }
    }

    private boolean isLowerVersion(int currVersion, String updateVersion) {

        try {
            int updateVerson = Integer.parseInt(updateVersion);
            if (currVersion <= updateVerson)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }

    }


    private boolean isCurrentVersionLower(String currVersion, String updateVersion) {

        //x.y.z  is version
        int currIntVersion, updateIntVersion;
        String result[];
        for (int i = 0; i < 3; i++) {
            try {
                result = getIntString(currVersion);
                currIntVersion = Integer.parseInt(result[0]);
                currVersion = result[1];
                result = getIntString(updateVersion);
                updateIntVersion = Integer.parseInt(result[0]);
                updateVersion = result[1];

                if (currIntVersion > updateIntVersion)
                    return false;
                if (currIntVersion < updateIntVersion)
                    return true;
                else if (i == 2 && currIntVersion == updateIntVersion)
                    return true;

            } catch (Exception e) {
                return false;
            }
        }//

        return false;
    }

    private String[] getIntString(String version) {
        String result[] = new String[2];
        int index;
        index = version.indexOf(".");
        if (index >= 0) {
            result[0] = version.substring(0, index); //string to use as int for comparing
            result[1] = version.substring(index + 1); // string for next parsing dot
        } else {
            result[0] = version;
            result[1] = "0";
        }
        return result;
    }
}
