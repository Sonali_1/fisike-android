package com.mphrx.fisike_physician.network.request;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.UploadResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by Kailash Khurana on 3/28/2016.
 */
public class UploadRequest extends BaseObjectRequest {


    private ProgressDialog dialog;
    private JSONObject payLoad;
    private long transactionId;
    private Context context;
    private boolean isSyncApi;

    public UploadRequest(JSONObject payLoad, long transactionId, Context context,
                         boolean isSyncApi) {
        this.payLoad = payLoad;
        this.transactionId = transactionId;
        this.context = context;
        this.isSyncApi = isSyncApi;
    }

    @Override
    public void doInBackground()
    {
        String baseUrl = APIManager.createMinervaBaseUrl() + CachedUrlConstants.UPLOAD;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new UploadResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new UploadResponse(response, transactionId,
                context, isSyncApi));
    }

    public String getPayLoad() {
        return payLoad.toString();
    }
}
