package com.mphrx.fisike_physician.network.request;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.PastConditionResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;


/**
 * Created by laxmansingh on 5/25/2016.
 */
public class PastConditionRequest extends BaseObjectRequest {


    private ProgressDialog dialog;
    private long transactionId;
    private Context context;
    private long userid;

    public PastConditionRequest(long transactionId, Context context) {
        this.transactionId = transactionId;
        this.context = context;
    }

    public PastConditionRequest(long transactionId, Context context, long userid) {
        this.transactionId = transactionId;
        this.context = context;
        this.userid = userid;
    }

    @Override
    public void doInBackground() {
        String baseUrl = APIManager.createMinervaBaseUrl() + CachedUrlConstants.postfix;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new PastConditionResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new PastConditionResponse(response, transactionId, context));
    }

    public String getPayLoad() {
        try {

            JSONObject jsonObject = new JSONObject();
            if (BuildConfig.isPatientApp) {
                jsonObject.put("patient", SettingManager.getInstance().getUserMO().getPatientId());
            } else if (BuildConfig.isPhysicianApp) {
                jsonObject.put("patient", userid);
            }

            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            return constraintsObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
