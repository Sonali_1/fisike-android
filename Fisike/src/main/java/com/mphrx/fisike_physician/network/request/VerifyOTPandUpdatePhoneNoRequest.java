package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.VerifyOTPandUpdatePhoneNoResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Aastha on 17/02/2016.
 */
public class VerifyOTPandUpdatePhoneNoRequest extends BaseObjectRequest {

    private String mNumber;
    private long mTransactionId;
    private String otpCode;
    private boolean disable;

    public VerifyOTPandUpdatePhoneNoRequest(String mobileNumber, String otp, long transactionId, boolean disableAccount) {

        mNumber=mobileNumber;
        mTransactionId=transactionId;
        otpCode=otp;
        disable=disableAccount;
    }

    @Override
    public void doInBackground() {
        String url = APIManager.getInstance().getVerifyOTPandUpdatePhoneNoUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new VerifyOTPandUpdatePhoneNoResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new VerifyOTPandUpdatePhoneNoResponse(response, mTransactionId));
    }

    public String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put("phoneNo", mNumber);
            payLoad.put("otp", otpCode);
            payLoad.put("disableAccount",disable);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
