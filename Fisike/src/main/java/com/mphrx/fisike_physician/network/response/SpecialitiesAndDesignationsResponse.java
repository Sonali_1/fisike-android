package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.enums.DesignationEnum;
import com.mphrx.fisike.enums.SpecialityEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by manohar on 23/12/15.
 */
public class SpecialitiesAndDesignationsResponse extends BaseResponse {

    private ArrayList<String> specialities = new ArrayList<>();
    private ArrayList<String> designations = new ArrayList<>();

    public SpecialitiesAndDesignationsResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        parseResponse(response);
    }

    public SpecialitiesAndDesignationsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public ArrayList<String> getSpecialities() {
        return specialities;
    }

    public ArrayList<String> getDesignations() {
        return designations;
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray array1 = response.getJSONArray("physicianSpecialties");
            for (int i = 0; i < array1.length(); i++) {
                specialities.add(SpecialityEnum.getDisplayedValuefromCode(array1.getString(i)));
            }

            JSONArray array2 = response.getJSONArray("physicianDesignations");
            for (int i = 0; i < array2.length(); i++) {
                designations.add(DesignationEnum.getDisplayedValuefromCode(array2.getString(i)));
            }
        } catch (JSONException e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getResources().getString(R.string.err_something_went_wrong);
        }
    }
}
