package com.mphrx.fisike_physician.network.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.FetchAllAgreementsResponse;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by aastha on 5/4/2017.
 */

public class FetchAllAgreementsRequest extends BaseObjectRequest {

    private long transactionId;
    private Context context;

    public FetchAllAgreementsRequest(long transactionId, Context context) {
        this.transactionId = transactionId;
        this.context = context;
    }

    @Override
    public void doInBackground() {
        String baseUrl = MphRxUrl.getFetchAgreementsUrl();
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, baseUrl, null, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new FetchAllAgreementsResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new FetchAllAgreementsResponse(response, transactionId, context));
    }
}

