package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import org.json.JSONObject;

/**
 * Created by brijesh on 24/11/15.
 */
public class SignupStatusResponse extends BaseResponse {

    private String mStatus;
    private String mHttpStatusCode;
    private boolean mEnabled;

    public SignupStatusResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        try {
          /*  mStatus = response.getString("httpStatusCode");
            mEnabled = response.getBoolean("enabled");*/
            mHttpStatusCode=response.getString("httpStatusCode");
        }
        catch (Exception e) {
            mIsSuccessful = false;
            mMessage = MyApplication.getAppContext().getString(R.string.err_something_went_wrong);
        }
    }

    public SignupStatusResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public String getStatus() {
        return mStatus;
    }

    public boolean isEnabled() {
        return mEnabled;
    }


    public String getHttpStatusCode()
    {
        return  mHttpStatusCode;
    }
}
