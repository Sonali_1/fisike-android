package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.UpdateVersionResponse;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONObject;


/**
 * Created by iappstreet on 13/01/16.
 */
public class UpdateVersionRequest extends BaseObjectRequest  {

    private long mTransactionId;

    private UpdateAppRequestListener mListener;

    @Override
    public void doInBackground() {

        String url = MphRxUrl.getUpdateVersionUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url, null, this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    public void registerUpdateAppListener(UpdateAppRequestListener l){
        mListener = l;
    }

    public void unregisterUpdateAppListener(){
        mListener = null;
    }

    public UpdateVersionRequest(long transactionId){
        mTransactionId = transactionId;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if(mListener!=null) {
            mListener.onError(error);
        }
    }

    public long getmTransactionId(){
        return mTransactionId;
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());

        if(mListener!=null) {
            mListener.onSuccess(new UpdateVersionResponse(response, mTransactionId));
        }

    }

    public interface UpdateAppRequestListener {
       void onSuccess(UpdateVersionResponse response);
        void onError(VolleyError error);
    }

}
