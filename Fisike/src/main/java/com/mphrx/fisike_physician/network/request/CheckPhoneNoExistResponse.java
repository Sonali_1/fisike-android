package com.mphrx.fisike_physician.network.request;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Aastha on 17/02/2016.
 */
public class CheckPhoneNoExistResponse extends BaseResponse {
    String message;
    String httpStatusCode;

    String status;
    public CheckPhoneNoExistResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        try{
            message=response.getString("msg");
            httpStatusCode=response.getString("httpStatusCode");
            status=response.getString("status");
          //  mTransactionID=transactionId;
        }
        catch (JSONException je){
            message= TextConstants.UNEXPECTED_ERROR;
        }
        catch (Exception e){
            message= TextConstants.UNEXPECTED_ERROR;
        }

    }

    public CheckPhoneNoExistResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }
    public String getMessage() {
        return message;
    }
    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getStatus() {
        return status;
    }



}
