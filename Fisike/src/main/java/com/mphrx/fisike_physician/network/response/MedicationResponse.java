package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.mo.DrugDetails;
import com.mphrx.fisike.mo.MedicationDetailsMO;
import com.mphrx.fisike.models.MedicationPrescriptionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by iappstreet on 14/01/16.
 */
public class MedicationResponse extends BaseResponse {



    private CopyOnWriteArrayList<Object> medicationList =new CopyOnWriteArrayList<Object>();

    public MedicationResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        parseResponse(response);
    }

    public MedicationResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    void parseResponse(JSONObject response) {

        JSONArray list = null;
        try {
            list = response.getJSONArray("list");
            Map<Integer, List<MedicationPrescriptionModel>> encounterPrescriptionMap =
                    MedicationPrescriptionModel.parseSearchMedicationArray(list);

            for (Map.Entry<Integer, List<MedicationPrescriptionModel>> entry : encounterPrescriptionMap.entrySet()) {
                MedicationDetailsMO details = MedicationDetailsMO
                        .createFromMedicationPrescriptionModel(entry.getKey(), entry.getValue());
                addSectionHeaderItem(details);

//                mAdapter.addSectionHeaderItem(details);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void addSectionHeaderItem(final Object item) {
        if (item instanceof MedicationDetailsMO) {
            medicationList.add(item);
            addItem((MedicationDetailsMO) item);
        }
    }

    public void addItem(final MedicationDetailsMO item) {
//        boolean isReminderAdded = item.isAllReminders();
        for (int i = 0; i < item.getDrugDetails().size(); i++) {
            DrugDetails drugDetails = item.getDrugDetails().get(i);
            medicationList.add(drugDetails);
        }
    }


    public CopyOnWriteArrayList<Object> getMedicationList(){
        return medicationList;
    }

}
