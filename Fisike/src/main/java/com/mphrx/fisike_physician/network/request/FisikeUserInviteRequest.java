package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by brijesh on 24/11/15.
 */
public class FisikeUserInviteRequest extends BaseObjectRequest {

    private User mUser;
    private FisikeUserInviteRequest.InviteRequestListener mListener;

    public FisikeUserInviteRequest(User user, FisikeUserInviteRequest.InviteRequestListener listener) {
        mUser = user;
        mListener = listener;
        AppLog.showInfo("Time", String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getAddToContactUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(mUser.userId), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mListener.onError(error, mUser);
    }

    @Override
    public void onResponse(JSONObject response) {
        mListener.onSuccess(response, mUser);
    }

    private String getPayLoad(String id) {
        JSONObject payLoad = new JSONObject();
        try {
            AppLog.showInfo(getClass().getSimpleName(),""+Integer.parseInt(id));
            payLoad.put("userIdToBeAdd", Integer.parseInt(id));
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

    public interface InviteRequestListener {
        void onSuccess(JSONObject response, User user);
        void onError(VolleyError error, User user);
        void onSuccess(JSONObject response, List<User> user);
        void onError(VolleyError error, List<User> user);
    }

}
