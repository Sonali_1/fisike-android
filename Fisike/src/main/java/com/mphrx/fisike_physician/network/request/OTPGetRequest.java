package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.OTPGetStatusResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by Aastha on 15/02/2016.
 */
public class OTPGetRequest extends BaseObjectRequest {
    private String mNumber;
    private String mDeviceUid;
    private long transactionId;
    private boolean disableAccount;

    public OTPGetRequest(String number, long transactionId, String deviceUUid, boolean disableAccount) {
        mNumber = number;
        this.transactionId = transactionId;
        mDeviceUid = deviceUUid;
        this.disableAccount = disableAccount;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getOTPCheckAndSendUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new OTPGetStatusResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        long currentTimeStamp = Calendar.getInstance().getTime().getTime();
        SharedPref.setOTPSentTime(currentTimeStamp);
        BusProvider.getInstance().post(new OTPGetStatusResponse(response, mNumber, transactionId));

    }

    public String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            if (SharedPref.getIsMobileEnabled())
                payLoad.put(MphRxUrl.K.PHONE_NUMBER, mNumber);
            else if (SharedPref.getIsUserNameEnabled())
                payLoad.put(MphRxUrl.K.USERNAME_N_CAPITAL, mNumber);
            else if (!SharedPref.getIsMobileEnabled())
                payLoad.put(MphRxUrl.K.EMAIL, mNumber);
            payLoad.put(MphRxUrl.K.DEVICE_ID, mDeviceUid);
            payLoad.put(MphRxUrl.K.DISABLEACCOUNT, disableAccount);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
}