package com.mphrx.fisike_physician.network.response;

import org.json.JSONObject;

/**
 * Created by xmb2nc on 21-12-2015.
 */
public class ObservationDetailListingResponse extends BaseResponse {


    public ObservationDetailListingResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
    }

    public ObservationDetailListingResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }
}
