package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Aastha on 17/02/2016.
 */
public class CheckPhoneNoExistRequest extends BaseObjectRequest {
    private String mNumber;
    private long mTransactionId;
    private boolean disableUser;
    private String payLoad;

    public CheckPhoneNoExistRequest(String number, long transactionId,boolean disableUser) {
        mNumber = number;
        mTransactionId = transactionId;
        this.disableUser=disableUser;
    }
    @Override
    public void doInBackground() {
        String url = MphRxUrl.getCheckPhoneNoandGetOtpurl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CheckPhoneNoExistResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());

        BusProvider.getInstance().post(new CheckPhoneNoExistResponse(response, mTransactionId));
    }

    public String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.PHONE_NUMBER, mNumber);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
            payLoad.put(TextConstants.DISABLE_USER,disableUser);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
