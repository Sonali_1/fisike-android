package com.mphrx.fisike_physician.network.response;

import android.content.Context;
import android.content.SharedPreferences;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Extension;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicationModel;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PractitionarModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.response.Coding;
import com.mphrx.fisike.response.DosageInstruction;
import com.mphrx.fisike.response.Encounter;
import com.mphrx.fisike.response.List;
import com.mphrx.fisike.response.Medication;
import com.mphrx.fisike.response.MedicationObjectArray;
import com.mphrx.fisike.response.Reason;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by iappstreet on 17/03/16.
 */
public class MedicationEncounterResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;

    private PractitionarModel practitionarModel;
    private ArrayList<MedicationModel> arrayMedicationModel;
    private boolean isUpdatedPrescription;
    private Context context;
    private boolean isMedicationSyncApi;

    private String medicationSyncTime;
    private int medicationOrdersTotalCount;

    private boolean isAllMedicationOrdersLoaded;

    private boolean isAlarmResetRequired;

    ArrayList<PrescriptionModel> arrayPrescriptionModel = new ArrayList<PrescriptionModel>();


    public MedicationEncounterResponse(JSONObject response, long transactionId,
                                       Context context, boolean isMedicationSyncApi,
                                       boolean isAlarmResetRequired) {
        super(response, transactionId);
        this.context = context;
        this.isMedicationSyncApi = isMedicationSyncApi;
        this.isAlarmResetRequired = isAlarmResetRequired;
        parseResponse(response);
    }


    public MedicationEncounterResponse(Exception exception, long transactionId
    ) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            jsonString = response.toString();
            processPrescriptionModel();

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private void processPrescriptionModel() {
        try {
            Object array = GsonUtils.jsonToObjectMapper(jsonString, MedicationObjectArray.class);
            if (array != null && array instanceof MedicationObjectArray) {
                MedicationObjectArray medicationObjectArray = ((MedicationObjectArray) array);
                medicationOrdersTotalCount = medicationObjectArray.getTotalCount();
                medicationSyncTime = medicationObjectArray.getSyncDate();
                SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (medicationSyncTime != null) {
                    editor.putString(SharedPreferencesConstant.MEDICATION_LAST_SYNC_TIME_SERVER, this.medicationSyncTime);
                }
                arrayMedicationModel = new ArrayList<MedicationModel>();
                if (isMedicationSyncApi) {
                    editor.putLong(SharedPreferencesConstant.MEDICATION_SYNC_TIME_LOCAL, System.currentTimeMillis());
                }
                editor.putInt(SharedPreferencesConstant.MEDICATION_ORDER_TOTAL_COUNT, medicationOrdersTotalCount);
                editor.commit();
                for (int i = 0; i < medicationObjectArray.getTotalCount(); i++) {
                    try {
                        List list = medicationObjectArray.getList().get(i);
                        MedicationModel medicationModel = new MedicationModel();
                        medicationModel.setMedicationID(list.getMedication().getId());
                        medicationModel.setMedician(list.getMedication().getCode().getText());

                        arrayMedicationModel.add(medicationModel);

                        practitionarModel = new PractitionarModel();
                        //setting physician to null if no physician entry found
                        if (list.getPrescriber() == null) {
                            practitionarModel.setPractitionarID(0);
                            practitionarModel.setPhysicianName(null);
                        } else {
                            practitionarModel.setPractitionarID(list.getPrescriber().getId());
                            practitionarModel.setPhysicianName((list.getPrescriber().getName() == null || list.getPrescriber().getName().getText() == null) ? "" : list.getPrescriber().getName().getText());
                        }
                        long patienId = list.getPatient().getId();
                        String patientName = new String();
                        if (list.getPatient().getName().size() != 0 && list.getPatient().getName().get(0).getText() != null) {
                            patientName = list.getPatient().getName().get(0).getText();
                        }

                        String startDate = list.getDateWritten().getValue();
                        if (startDate != null && !startDate.equals("") && !startDate.equals("null")) {
                            startDate = DateTimeUtil.calculateDateEnglis(startDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                        }
                        String endDate = list.getDateEnded().getValue();
                        if (endDate != null && !endDate.equals("") && !endDate.equals("null")) {
                            endDate = DateTimeUtil.calculateDateEnglis(endDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                        }

                        String updatedDate = list.getLastUpdated();

                        Encounter encounter = list.getEncounter();

                        int mimeType = VariableConstants.MIME_TYPE_NOTHING;
                        String sourceName = "";
                        java.util.List<Extension> extension = list.getExtension();
                        for (int j = 0; extension != null && j < extension.size(); j++) {
                            Extension link = extension.get(j);
                            // attachmnet flow
                            if (link != null && link.getUrl() != null && link.getUrl().equals("OriginalAttachmentLink") && link.getValue() != null && link.getValue().size() > 0 && link.getValue().get(0) != null && link.getValue().get(0).contains(".")) {
                                String ext = link.getValue().get(0).substring(link.getValue().get(0).lastIndexOf(".") + 1, link.getValue().get(0).length());

                                if (ext.equalsIgnoreCase("pdf")) {
                                    mimeType = VariableConstants.MIME_TYPE_FILE;
                                } else if (ext.equalsIgnoreCase("doc") || ext.equalsIgnoreCase("docx")) {
                                    mimeType = VariableConstants.MIME_TYPE_DOC;
                                }
                            }

                            // source flow
                            if (link != null && link.getUrl() != null && link.getUrl().equals("tenantLocationName") && link.getValue() != null &&
                                    link.getValue().size() > 0 && link.getValue().get(0) != null) {
                                sourceName = link.getValue().get(0);
                            }
                        }
                        Medication medication = list.getMedication();

                        PrescriptionModel prescriptionModel = new PrescriptionModel();
                        prescriptionModel.setID(list.getId());
                        prescriptionModel.setEncounterID(encounter.getId());
                        prescriptionModel.setPractitionarID(practitionarModel.getPractitionarID());
                        prescriptionModel.setMedicianId(medicationModel.getMedicationID());
                        prescriptionModel.setMedicineName(medicationModel.getMedician());
                        prescriptionModel.setPatientID(patienId);
                        prescriptionModel.setEndDate(endDate);
                        prescriptionModel.setStartDate(startDate);
                        Reason reason = list.getReason();
                        if (reason != null) {
                            java.util.List<Coding> coding;
                            ArrayList<DiseaseMO> diseaseMOs = new ArrayList<>();
                            coding = reason.getCoding();
                            for (Coding code : coding) {
                                DiseaseMO temp = new DiseaseMO(code.getDisplay(), code.getCode(), code.getSystem());
                                diseaseMOs.add(temp);
                            }
                            prescriptionModel.setDiseaseMoList(diseaseMOs);
                        }
                        prescriptionModel.setPractitionerName(practitionarModel.getPhysicianName());
                        prescriptionModel.setUpdatedDate(updatedDate);
                        prescriptionModel.setStatus(VariableConstants.IN_PROGRESS);
                        prescriptionModel.setPatientName(patientName);
                        prescriptionModel.setMimeType(mimeType);
                        prescriptionModel.setExtensionList(extension);
                        if (sourceName == null) {
                            sourceName = "";
                        }
                        prescriptionModel.setSourceName(sourceName);


                        boolean isRepeat = false;
                        if (list.getDosageInstruction().get(0).getTiming().getRepeat() != null && list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod() != null &&
                                list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod().getValue() > 1) {
                            isRepeat = true;
                        }

                        ArrayList<MedicineDoseFrequencyModel> medicineDoseFrequencyModelList = new ArrayList<>();
                        for (DosageInstruction dosageInstruction : list.getDosageInstruction()) {

                            if (dosageInstruction.getAdditionalInstructions() != null &&
                                    dosageInstruction.getAdditionalInstructions().getCoding() != null) {

                                if (list.getDosageInstruction().get(0).getText().contains("SOS")) {
                                    MedicineDoseFrequencyModel medicineDoseFrequencyModel = new MedicineDoseFrequencyModel();
                                    medicineDoseFrequencyModel.setEndDate(endDate);
                                    medicineDoseFrequencyModel.setStartDate(startDate);

                                    ArrayList<com.mphrx.fisike.gson.request.MedicationOrderRequest.Coding> arrayCoding = dosageInstruction.getAdditionalInstructions().getCoding();
                                    if (arrayCoding != null && arrayCoding.size() > 0) {
                                        com.mphrx.fisike.gson.request.MedicationOrderRequest.Coding codingMedication = arrayCoding.get(0);
                                        medicineDoseFrequencyModel.setDoseIntakeTimeInstruction(codingMedication.getDisplay() != null ? codingMedication.getDisplay().trim() : "");
                                    }
                                    medicineDoseFrequencyModel.setDoseHours("0");
                                    medicineDoseFrequencyModel.setDoseMinutes("0");
                                    medicineDoseFrequencyModel.setDoseTimeAMorPM("0");


                                    medicineDoseFrequencyModel.setDoseQuantity(Double.parseDouble(dosageInstruction.getDoseQuantity().getValue()));
                                    medicineDoseFrequencyModel.setDoseUnit(Utils.removeSpace(dosageInstruction.getDoseQuantity().getUnit()));

                                    if (dosageInstruction.getTiming() != null && dosageInstruction.getTiming().getExtension() != null && dosageInstruction.getTiming().getExtension().size() > 0 && dosageInstruction.getTiming().getExtension().get(0).getValue() != null && dosageInstruction.getTiming().getExtension().get(0).getValue().size() > 0) {
                                        ArrayList<String> days = dosageInstruction.getTiming().getExtension().get(0).getValue();
                                        medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                    } else {
                                        ArrayList<String> days = new ArrayList<String>();
                                        days.add(context.getResources().getString(R.string.txt_monday));
                                        days.add(context.getResources().getString(R.string.txt_tuesday));
                                        days.add(context.getResources().getString(R.string.txt_wednesday));
                                        days.add(context.getResources().getString(R.string.txt_thursday));
                                        days.add(context.getResources().getString(R.string.txt_friday));
                                        days.add(context.getResources().getString(R.string.txt_saturday));
                                        days.add(context.getResources().getString(R.string.txt_sunday));
                                        medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                    }

                                    medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(false);
                                    if (isRepeat) {
                                        medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(true);
                                        medicineDoseFrequencyModel.setRepeatDaysInterval(list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod().getValue());
                                    }

                                    medicineDoseFrequencyModelList.add(medicineDoseFrequencyModel);

                                } else {
                                    for (com.mphrx.fisike.gson.request.MedicationOrderRequest.Coding codingMedication : dosageInstruction.getAdditionalInstructions().getCoding()) {

                                        MedicineDoseFrequencyModel medicineDoseFrequencyModel = new MedicineDoseFrequencyModel();
                                        medicineDoseFrequencyModel.setEndDate(endDate);
                                        medicineDoseFrequencyModel.setStartDate(startDate);

                                        String[] time = Utils.getMedicineTimingIn24Hr(codingMedication.getDisplay());
                                        Thread.sleep(1);
                                        medicineDoseFrequencyModel.setAlarmManagerUniqueKey((int) System.currentTimeMillis());
                                        if (time != null && time.length > 0) {
                                            boolean isAmPm = time[0].toUpperCase().contains(TextConstants.AM) || time[0].toUpperCase().contains(TextConstants.PM) ? true : false;

                                            if (isAmPm && (time[0].toUpperCase().contains(TextConstants.PM) || time[0].toUpperCase().contains(TextConstants.AM))) {
                                                String hour = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.destinationTimeFormat, DateTimeUtil.HH);
                                                String min = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.destinationTimeFormat, DateTimeUtil.mm);
                                                medicineDoseFrequencyModel.setDoseHours(hour);
                                                medicineDoseFrequencyModel.setDoseMinutes(min);
                                            } else {
                                                String hour = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.medicationTimeFormat, DateTimeUtil.HH);
                                                String min = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.medicationTimeFormat, DateTimeUtil.mm);
                                                medicineDoseFrequencyModel.setDoseHours(hour);
                                                medicineDoseFrequencyModel.setDoseMinutes(min);
                                            }
                                            if(time.length>1) {
                                                medicineDoseFrequencyModel.setDoseIntakeTimeInstruction(time[1]);
                                            }
                                        }
                                        medicineDoseFrequencyModel.setDoseQuantity(Double.parseDouble(dosageInstruction.getDoseQuantity().getValue()));
                                        medicineDoseFrequencyModel.setDoseUnit(Utils.removeSpace(dosageInstruction.getDoseQuantity().getUnit()));

                                        if (dosageInstruction.getTiming() != null && dosageInstruction.getTiming().getExtension() != null && dosageInstruction.getTiming().getExtension().size() > 0 && dosageInstruction.getTiming().getExtension().get(0).getValue() != null && dosageInstruction.getTiming().getExtension().get(0).getValue().size() > 0) {
                                            ArrayList<String> days = dosageInstruction.getTiming().getExtension().get(0).getValue();
                                            medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                        } else {
                                            ArrayList<String> days = new ArrayList<String>();
                                            days.add(context.getResources().getString(R.string.txt_monday));
                                            days.add(context.getResources().getString(R.string.txt_tuesday));
                                            days.add(context.getResources().getString(R.string.txt_wednesday));
                                            days.add(context.getResources().getString(R.string.txt_thursday));
                                            days.add(context.getResources().getString(R.string.txt_friday));
                                            days.add(context.getResources().getString(R.string.txt_saturday));
                                            days.add(context.getResources().getString(R.string.txt_sunday));
                                            medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                        }

                                        medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(false);
                                        if (isRepeat) {
                                            medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(true);
                                            medicineDoseFrequencyModel.setRepeatDaysInterval(list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod().getValue());
                                        }

                                        medicineDoseFrequencyModelList.add(medicineDoseFrequencyModel);
                                    }
                                }
                            }
                        }

                        if (list.getDosageInstruction().get(0).getTiming().getEvent() == null || list.getDosageInstruction().get(0).getTiming().getEvent().size() < 1 || list.getDosageInstruction().get(0).getTiming().getEvent().get(0) == null || list.getDosageInstruction().get(0).getTiming().getEvent().get(0).getValue() == null || list.getDosageInstruction().get(0).getTiming().getEvent().get(0).getValue().equals("")) {
                            prescriptionModel.setAutoReminderSQLBool(0);
                        } else {
                            prescriptionModel.setAutoReminderSQLBool(1);
                        }

                        if (list.getDosageInstruction().get(0).getText().contains("SOS")) {
                            prescriptionModel.setDoseInstruction(context.getString(R.string.conditional_sos));
                            prescriptionModel.setAutoReminderSQLBool(0);
                        } else {
                            String[] dosage_frequency = context.getResources().getStringArray(R.array.dosage_frequency);
                            prescriptionModel.setDoseInstruction(MedicationFrequancyEnum.getCodeFromValue(dosage_frequency[medicineDoseFrequencyModelList.size() - 1]));
                        }
                        prescriptionModel.setArrayDose(medicineDoseFrequencyModelList);

                        if (list.getExtension() != null && list.getExtension().size() > 0 && list.getExtension().get(0).getValue() != null && list.getExtension().get(0).getValue().size() > 0) {
                            prescriptionModel.setRemark(list.getExtension().get(0).getValue().get(0));
                        }

                        if (BuildConfig.isPatientApp) {
                            if (isMedicationSyncApi) {
                                EncounterMedicationDBAdapter.getInstance(context).fetchAndInsertMedicationPrescription(prescriptionModel, isAlarmResetRequired);
                            } else {
                                EncounterMedicationDBAdapter.getInstance(context).fetchAndInsertMedicationPrescription(prescriptionModel, true);
                            }
                        } else {
                            arrayPrescriptionModel.add(prescriptionModel);
                        }
                        isUpdatedPrescription = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getMessage() {
        return msg;
    }

    public String getStatus() {
        return mStatus;
    }

    public int getMedicationOrdersTotalCount() {
        return medicationOrdersTotalCount;
    }

    public void setMedicationOrdersTotalCount(int medicationOrdersTotalCount) {
        this.medicationOrdersTotalCount = medicationOrdersTotalCount;
    }

    public boolean isMedicationSyncApi() {
        return isMedicationSyncApi;
    }

    public String getMedicationSyncTime() {
        return medicationSyncTime;
    }

    public void setMedicationSyncTime(String medicationSyncTime) {
        this.medicationSyncTime = medicationSyncTime;
    }

    public boolean isAllMedicationOrdersLoaded() {
        return isAllMedicationOrdersLoaded;
    }

    public void setIsAllMedicationOrdersLoaded(boolean isAllMedicationOrdersLoaded) {
        this.isAllMedicationOrdersLoaded = isAllMedicationOrdersLoaded;
    }

    public ArrayList<PrescriptionModel> getPrescriptionModel() {
        return arrayPrescriptionModel;
    }
}
