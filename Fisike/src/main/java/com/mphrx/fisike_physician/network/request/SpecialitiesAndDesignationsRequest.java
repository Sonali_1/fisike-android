package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.SpecialitiesAndDesignationsResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by manohar on 23/12/15.
 */
public class SpecialitiesAndDesignationsRequest extends BaseObjectRequest {

    private static JSONObject mCachedResponse = null;

    private long mTransactionId;

    public SpecialitiesAndDesignationsRequest(long transactionId) {

        mTransactionId = transactionId;
    }

    @Override
    public void doInBackground() {
        if (mCachedResponse != null) {
            handleResponse(mCachedResponse, true);
        }

        String url = MphRxUrl.getSpecialityAndRoles();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url, null, this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        if (mCachedResponse == null)
            BusProvider.getInstance().post(new SpecialitiesAndDesignationsResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        handleResponse(response, mCachedResponse == null);
    }

    private void handleResponse(JSONObject response, boolean post) {
        SpecialitiesAndDesignationsResponse out = new SpecialitiesAndDesignationsResponse(response, mTransactionId);

        if (out.isSuccessful()) {
            mCachedResponse = response;
        }

        if (post) {
            BusProvider.getInstance()
                    .post(new SpecialitiesAndDesignationsResponse(response, mTransactionId));
        }
    }
}
