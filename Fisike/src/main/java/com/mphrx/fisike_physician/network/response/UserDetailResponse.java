package com.mphrx.fisike_physician.network.response;

import com.google.gson.JsonArray;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brijesh on 01/11/15.
 */
public class UserDetailResponse extends BaseResponse {

    private String mPassword;
    private boolean mIsAlreadyLoggedIn;
    private boolean isCorrectAgreementStatus;
    private ArrayList<AgreementModel> agreementModels;

    public UserDetailResponse(JSONObject response, String password, long transactionId) {
        super(response, transactionId);
        mPassword = password;
        parseData(response);
    }

    public UserDetailResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseData(JSONObject response) {

        try {
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.toString(), UserMO.class);
            UserMO newUserMo = (UserMO) jsonToObjectMapper;
            UserMO existingUserMo = SettingManager.getInstance().getUserMO();

            if (existingUserMo != null && existingUserMo.getId() == newUserMo.getId()) {
                //storing data retrieved from auth token
                newUserMo.setPassword(existingUserMo.getPassword());
                newUserMo.setRoles(existingUserMo.getRoles());
                newUserMo.setProfilePicUpdateStatus(existingUserMo.getProfilePicUpdateStatus());
                newUserMo.setPersistenceKey(existingUserMo.getPersistenceKey());
                newUserMo.setHasSignedUp(existingUserMo.getHasSignedUp());
                newUserMo.setmPIN(existingUserMo.getmPIN());

            } else {
                SharedPref.setUserName(newUserMo);
            }
            String countryCode = "+91",
                    phone = newUserMo.getPhoneNumber() != null ? newUserMo.getPhoneNumber() : "";
            if (!phone.equals("") && phone.length() > 10) {
                if (!phone.startsWith("+")) {
                    phone = "+" + phone;
                }
                countryCode = phone.substring(0, 3);
            }
            newUserMo.setCountryCode(countryCode);

            SharedPref.setMobileNumber(newUserMo.getPhoneNumber());
            SharedPref.setEmailAddress(newUserMo.getEmail());
            SharedPref.setLoginUserName(newUserMo.getUsername());
            newUserMo.setPassword(mPassword);
            newUserMo.setUsername(newUserMo.getId() + "");


            JSONObject jsonObject = null;

            try {
                if (response != null) {
                    jsonObject = new JSONObject(response.toString());
                }
                if (jsonObject.has("dependentPatientIds")) {
                    JSONArray jArray = jsonObject.getJSONArray("dependentPatientIds");
                    newUserMo.setDependentPatientsIds(jArray != null ? jArray.toString() : "[]");
                    AppLog.showInfo("tree", jArray.toString());
                } else {
                    newUserMo.setDependentPatientsIds("[]");
                }
            } catch (Exception ex) {
                newUserMo.setDependentPatientsIds("[]");
            }

            newUserMo.setSalutation(TextConstants.NO);
            if (newUserMo.getDateOfBirth() != null) {
                newUserMo.setDateOfBirth(Utils.getFormattedDateEnglish(newUserMo.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));
            }
            /*
            TODO this is bug if the gender is other then also it will save female
             */
            if (newUserMo.getGender() != null && !newUserMo.getGender().equals("") && !newUserMo.getGender().equals("null")) {
                newUserMo.setGender(newUserMo.getGender());
            } else {
                newUserMo.setGender("");
            }
            if(newUserMo.getAgreements()!=null)
                agreementModels =newUserMo.getAgreements();
            isCorrectAgreementStatus = newUserMo.isCorrectAgreementStatus();
            SettingManager.getInstance().updateUserMO(newUserMo);

            if (!newUserMo.isRegistrationStatus()) {
                mIsAlreadyLoggedIn = true;
            }

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
            mIsAlreadyLoggedIn = false;
        }
    }


    public boolean isAlreadyLoggedIn() {
        return mIsAlreadyLoggedIn;
    }

    public boolean isCorrectAgreementStatus() {
        return isCorrectAgreementStatus;
    }

    public ArrayList<AgreementModel> getAgreementModels() {
        return agreementModels;
    }
}
