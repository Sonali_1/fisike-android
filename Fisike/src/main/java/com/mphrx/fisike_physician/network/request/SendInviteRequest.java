package com.mphrx.fisike_physician.network.request;

import android.text.TextUtils;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.response.BaseResponse;
import com.mphrx.fisike_physician.network.response.SendInviteResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 06/11/15.
 */
public class SendInviteRequest extends BaseObjectRequest implements FisikeUserInviteRequest.InviteRequestListener {

    private long mTransactionId;
    private List<User> mFisikeUserList = new ArrayList<User>();
    private List<User> mPhoneBookUserList = new ArrayList<User>();
    private int mTotalAPICall;
    private List<User> mFisikeResponseList = new ArrayList<User>();
    ;
    private boolean mPhoneRequestSuccessful;

    public SendInviteRequest(List<User> list, long transactionId) {
        mTransactionId = transactionId;
        for (User user : list)
            if (user.isFisikeUser)
                mFisikeUserList.add(user);
            else
                mPhoneBookUserList.add(user);
    }

    @Override
    public void doInBackground() {
        //    if(mPhoneBookUserList.size()>0)
        ThreadManager.getDefaultExecutorService().submit(new PhoneBookInviteRequest(mPhoneBookUserList, this));
        for (User user : mFisikeUserList) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ThreadManager.getDefaultExecutorService().submit(new FisikeUserInviteRequest(user, this));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        // Do Nothing
        AppLog.d("error0 ", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        // Do Nothing
        AppLog.d("response0 ", response.toString());
    }


    @Override
    public void onSuccess(JSONObject response, User user) {

        AppLog.d("success ", response.toString());

        AppLog.d("User info", user.userId);
        synchronized (this) {
            user.isInvitationSuccessful = true;
            mFisikeResponseList.add(user);
            checkApiCallCount();
        }
    }

    @Override
    public void onError(VolleyError error, User user) {
        AppLog.d("error ", error.toString());
        error.printStackTrace();
        AppLog.d("User info", user.userId);

        synchronized (this) {
            if (TextUtils.isEmpty(SharedPref.getAccessToken()))
                return;
            new BaseResponse(error, -1);
            user.isInvitationSuccessful = false;
            mFisikeResponseList.add(user);
            checkApiCallCount();
        }
    }

    @Override
    public void onSuccess(JSONObject response, List<User> user) {
        AppLog.d("success1 ", response.toString());
        AppLog.d("User info1", user.size());

        synchronized (this) {
            mPhoneRequestSuccessful = true;
            checkApiCallCount();
        }
    }

    @Override
    public void onError(VolleyError error, List<User> user) {

        AppLog.d("error1 ", error.toString());
        error.printStackTrace();
        AppLog.d("User info1", user.size());
        synchronized (this) {
            if (TextUtils.isEmpty(SharedPref.getAccessToken()))
                return;
            new BaseResponse(error, -1);
            mPhoneRequestSuccessful = false;
            checkApiCallCount();
        }
    }

    private void checkApiCallCount() {
        mTotalAPICall++;
        if (mTotalAPICall == mFisikeUserList.size() + 1) {
            BusProvider.getInstance().post(new SendInviteResponse(this.mFisikeResponseList, this.mPhoneBookUserList, mPhoneRequestSuccessful, this.mTransactionId));
        }
    }
}
