package com.mphrx.fisike_physician.network.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.MedicationEncounterResponse;
import com.mphrx.fisike_physician.network.response.MedicationResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by iappstreet on 14/01/16.
 */
public class MedicationRequest extends BaseObjectRequest
{
    private long mTransactionId;
    int skipCount;
    int nextCount;
    String patientId;
//    DrugRemindersAdapter mAdapter;
    private Context mContext;
    private String payLoad;

    public MedicationRequest(long transactionId,String payLoad,String patientId,Context context){
        mTransactionId = transactionId;
        this.patientId = patientId;
        mContext = context;
        this.payLoad = payLoad;
    }

    @Override
    public void doInBackground() {
        // perform login
        String url = MphRxUrl.getMedicationrUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, payLoad, this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new MedicationResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new MedicationEncounterResponse(response, mTransactionId,mContext,false,true));
    }


    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            JSONObject constraints = new JSONObject();
            constraints.put("patient", patientId);
            constraints.put("_skip", skipCount);
            constraints.put("_count", nextCount);
            payLoad.put("constraints", constraints);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
}
