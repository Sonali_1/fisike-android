package com.mphrx.fisike_physician.network.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.gson.profile.request.Address;
import com.mphrx.fisike.gson.profile.request.Extension;
import com.mphrx.fisike.gson.profile.request.Name;
import com.mphrx.fisike.gson.profile.request.Physician;
import com.mphrx.fisike.gson.profile.request.PractitionerRole;
import com.mphrx.fisike.gson.profile.request.Role;
import com.mphrx.fisike.gson.profile.request.Speciality;
import com.mphrx.fisike.gson.profile.request.Telecom;
import com.mphrx.fisike.gson.profile.request.UserProfile;
import com.mphrx.fisike.gson.profile.request.Value;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.update_user_profile.volleyRequest.UpdateProfileRequestGsonPractitioner;
import com.mphrx.fisike_physician.models.PhysicianMO;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brijesh on 03/12/15.
 */
public class UpdateProfileRequest extends BaseObjectRequest {

    private long mTransactionId;
    private UpdateProfileRequestGsonPractitioner mPhysicianMo;

    public UpdateProfileRequest(UpdateProfileRequestGsonPractitioner physicianMO, long transactionId) {
        mTransactionId = transactionId;
        mPhysicianMo = physicianMO;
    }

    @Override
    public void doInBackground() {
        // perform login
        String url = MphRxUrl.getUpdateProfileUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        String json = new Gson().toJson(mPhysicianMo);
        String payLoad = json;
        AppLog.showInfo(getClass().getSimpleName(), "PayLoad = " + payLoad);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, payLoad, this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new UpdateProfileResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new UpdateProfileResponse(response, mPhysicianMo.getPhysician(), mTransactionId));
    }

/*
    private String getPayLoad() {
        UserProfile userProfile = new UserProfile();
        userProfile.setUserType(mPhysicianMo.userType);
        User user = new User();
        user.setDob(mPhysicianMo.dateOfBirth);
        user.setGender(mPhysicianMo.gender);
//        user.setEmail(mPhysicianMo.email);
//        user.setPassword(mPhysicianMo.password);
        user.setFirstName(mPhysicianMo.firstName);
        user.setLastName(mPhysicianMo.lastName);
        user.setPhoneNo(mPhysicianMo.phoneNumber);
        user.setAlternateContact(mPhysicianMo.alternateContact);
  //      user.setEmail(mPhysicianMo.email);
  */
/*      if (mPhysicianMo.changeEmail != null) {
            user.setUnverifiedEmail(mPhysicianMo.changeEmail);
        }
  *//*

        user.setId(Long.parseLong(mPhysicianMo.id));

        userProfile.setUser(user);
        Physician physician = new Physician();
        physician.setAddress(mPhysicianMo.address);
        ArrayList<Extension> listExtension = new ArrayList<>();
        Extension extension = new Extension();
        ArrayList<Value> listValue = new ArrayList<>();
        Value value = new Value();
        value.setNoOfExp(mPhysicianMo.experience);
        listValue.add(value);
        extension.setValue(listValue);
        listExtension.add(extension);
        physician.setExtension(listExtension);
        Name name = new Name();
        ArrayList<String> arrayFamily = new ArrayList<>();
        arrayFamily.add(mPhysicianMo.lastName);
        name.setFamily(arrayFamily);
        ArrayList<String> arrayGiven = new ArrayList<>();
        arrayGiven.add(mPhysicianMo.firstName);
        name.setGiven(arrayGiven);
        name.setText(mPhysicianMo.firstName + " " + mPhysicianMo.lastName);
        name.setUseCode("Human Name");
        physician.setName(name);
        physician.setId(Integer.parseInt(mPhysicianMo.physicianId));
        physician.set_class("com.mphrx.consus.resources.Practitioner");
        ArrayList<PractitionerRole> arrayPractitionerRole = new ArrayList<>();
        PractitionerRole practitionerRole = new PractitionerRole();
        Role role = new Role();
        role.setText(mPhysicianMo.role);
        practitionerRole.setRole(role);
        ArrayList<Speciality> arraySpeciality = new ArrayList<>();
        Speciality speciality = new Speciality();
        speciality.setText(mPhysicianMo.speciality);
        arraySpeciality.add(speciality);
        practitionerRole.setSpeciality(arraySpeciality);
        arrayPractitionerRole.add(practitionerRole);
        physician.setPractitionerRole(arrayPractitionerRole);
        ArrayList<Telecom> arrayTelecom = new ArrayList<>();
        Telecom telecom = new Telecom();
        telecom.setUseCode("Human Code");
        telecom.setValue(mPhysicianMo.phoneNumber);
        arrayTelecom.add(telecom);
*/
/*        telecom = new Telecom();
        telecom.setUseCode("email");
        telecom.setValue(mPhysicianMo.email);*//*

        physician.setTelecom(arrayTelecom);
        userProfile.setPhysician(physician);

        String json = new Gson().toJson(userProfile);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        return jsonObj.toString();
    }
*/

}
