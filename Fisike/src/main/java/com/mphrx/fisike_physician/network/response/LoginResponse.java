package com.mphrx.fisike_physician.network.response;

import android.content.Context;
import android.net.http.Headers;

import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xbill.DNS.Header;

import java.util.HashMap;

/**
 * Created by brijesh on 15/10/15.
 */
public class LoginResponse extends BaseResponse {

    private String mPassword;
    private JSONObject headers;
    private String usernameHeader;
    private String otpSentToDevices,mobile, email;
    boolean isOtpSent;
    boolean isLoginPossible;


    public LoginResponse(JSONObject response, String password, long transactionId) {
        super(response, transactionId);
        try {
            JSONArray info = response.getJSONArray("info");
            isLoginPossible = false;
        } catch (JSONException e) {
            parseResponse(response);
        }
        mPassword = password;
    }

    public LoginResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        isLoginPossible = true;
        try {
            String token = response.getString("token");
            boolean firstLogin = response.getBoolean("firstLogin");
            boolean passwordExpired = response.getBoolean("passwordExpired");
            String username = response.getString("username");
            String roles = response.getString("roles");
            headers = (JSONObject) response.get("headers");
            JSONObject otpSentToDevicesjson = null;
            try {
                isOtpSent = headers.optBoolean("isOTPSent");
                String otpSentToDevices = headers.optString("oTPSentToDevices");
                if(otpSentToDevicesjson != null)
                otpSentToDevicesjson = new JSONObject(otpSentToDevices);
                usernameHeader = headers.optString("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (otpSentToDevicesjson != null) {
                mobile = otpSentToDevicesjson.optString("mobile");
            }
            if (otpSentToDevicesjson != null) {
                email = otpSentToDevicesjson.optString("email");
            }


            if (SettingManager.getInstance().getUserMO() != null && SharedPref.getUsername() != null) {
                if (!Integer.toString(response.getInt("id")).equals(Long.toString(SettingManager.getInstance().getUserMO().getId()))) {
                    Utils.truncateDB();
                }
            }

            SharedPref.setUserName(response.getInt("id"));
            SharedPref.setDefinedRoles(roles);
            SharedPref.setAccessToken(token);
            SharedPref.setFirstLogin(firstLogin);
            SharedPref.setPasswordExpired(passwordExpired);
            SharedPref.setLoginUserName(username);
            SharedPref.setUserLogout(false);
            Utils.setUserLoggedOut(false);

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }

    }


    public String getPassword() {
        return mPassword;
    }

    public String getUsernameHeader() {
        return usernameHeader;
    }


    public String getOtpSentToDevices() {
        return otpSentToDevices;
    }


    public boolean isOtpSent() {
        return isOtpSent;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public boolean isLoginPossible() {
        return isLoginPossible;
    }
}
