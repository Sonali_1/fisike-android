package com.mphrx.fisike_physician.network.response;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.Gson;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.utils.DateTimeUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Kailash Khurana on 3/28/2016.
 */
public class UploadResponse extends BaseResponse {
    private String mStatus = "";
    private String msg = "";
    private String jsonString;

    private boolean isUpdatedPrescription;
    private Context context;
    private boolean isSyncApi;

    private String uploadSyncTime;
    private int uploadTotalCount;

    private boolean isAllMedicationOrdersLoaded, isMedicationSyncApi;
    ArrayList<DocumentReferenceModel> mdocumentList = new ArrayList<>();

    ArrayList<PrescriptionModel> arrayPrescriptionModel = new ArrayList<PrescriptionModel>();
    private ArrayList<Integer> documentsToDownload;


    public UploadResponse(JSONObject response, long transactionId,
                          Context context, boolean isSyncApi) {
        super(response, transactionId);
        this.context = context;
        this.isSyncApi = isSyncApi;
        parseResponse(response);
    }

    public UploadResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            jsonString = response.toString();
            processUploadModel();

        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private void processUploadModel() {

        try {
            JSONObject mainObject = new JSONObject(jsonString);

            JSONArray jsonArray = mainObject.getJSONArray("list");

            uploadTotalCount = mainObject.getInt("totalCount");
            uploadSyncTime = mainObject.optString("syncDate");

            SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (uploadSyncTime != null && !uploadSyncTime.trim().equals("")) {
                editor.putString(SharedPreferencesConstant.UPLOAD_LAST_SYNC_TIME_SERVER, this.uploadSyncTime);
            }

            if (isSyncApi) {
                editor.putLong(SharedPreferencesConstant.UPLOAD_SYNC_TIME_LOCAL, System.currentTimeMillis());
            }
            editor.putInt(SharedPreferencesConstant.UPLOAD_TOTAL_COUNT, uploadTotalCount);
            editor.commit();
            documentsToDownload = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject mSubJosnObject = jsonArray.getJSONObject(i);
                    DocumentReferenceModel documentModel = new DocumentReferenceModel();
                    documentModel.setDocumentID(mSubJosnObject.getInt("id"));

                    // added content document length
                    JSONArray contentDocument=mSubJosnObject.optJSONArray("contentDocument");
                    if(contentDocument != null)
                        documentModel.setNoOfAttachment(contentDocument.length());

                    JSONArray value = mSubJosnObject.getJSONArray("extension").getJSONObject(0).getJSONArray("value");
                    String mime = value.getJSONObject(0).getString("mimeType");
                    // source name
                    String sourceName = value.getJSONObject(0).optString("tenantLocationName");
                    if(sourceName == null){
                        documentModel.setSourceName("");
                    } else {
                        documentModel.setSourceName(sourceName);
                    }

                    if (documentModel.getMimeType() == VariableConstants.MIME_TYPE_IMAGE &&
                            value.getJSONObject(0).toString().contains("thumbnailData")) {
                        Gson gson = new Gson();
                        byte[] bitmapdata = gson.fromJson(value.getJSONObject(0).getString("thumbnailData"), byte[].class);
                        documentModel.setThumbImage(bitmapdata);
                    }

                    if (mime.startsWith("image")) {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_IMAGE);
                    } else if (mime.contains("pdf")) {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_FILE);
                    } else if (mime.contains("msword")) {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_DOC);
                    } else if (mime.contains("vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_DOCX);
                    } else if (mime.contains("zip")) {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_ZIP);
                    } else {
                        documentModel.setMimeType(VariableConstants.MIME_TYPE_UNKNOWN);
                    }
                    try {
                        JSONArray extensionArray = mSubJosnObject.getJSONArray("extension");
                        if (extensionArray.length() > 1) {
                            String reason = extensionArray.getJSONObject(1).getJSONArray("value").getJSONObject(0).optString("reason");
                            documentModel.setReason(reason);
                        }
                    } catch (Exception e) {
                    }

                    String timestamp = mSubJosnObject.getJSONObject("created").getString("value");
                    String refId = mSubJosnObject.getJSONObject("masterIdentifier").getString("value");
                    String status = mSubJosnObject.getString("status");
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());
                    cal.setTime(sdf.parse(timestamp));
                    documentModel.setTimeStamp(cal.getTime().getTime() + "");
                    documentModel.setDocumentMasterIdentifier(refId);
                    if (status.equalsIgnoreCase("NEW")) {
                        documentModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_DOWNLOADED);
                    } else if (status.equalsIgnoreCase("DIGITIZED")) {
                        documentModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_DIGITIZED);
                    } else if(status.equalsIgnoreCase("CANCELLED")) {
                        documentModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_REJECTED);
                    }
                    else
                    {
                        documentModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_UNKNOWN);
                            //no handling for other status
                    }

                    documentModel.setDocumentType(mSubJosnObject.getJSONObject("type").getString("text"));
                    documentModel.setDocumentID(mSubJosnObject.getInt("id"));


                    mdocumentList.add(documentModel);

                } catch (JSONException ignore) {
                    ignore.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Integer> getDocumentsToDownload() {
        return documentsToDownload;
    }

    public String getMessage() {
        return msg;
    }

    public String getStatus() {
        return mStatus;
    }

    public int getUploadTotalCount() {
        return uploadTotalCount;
    }

    public void setUploadTotalCount(int uploadTotalCount) {
        this.uploadTotalCount = uploadTotalCount;
    }

    public boolean isSyncApi() {
        return isSyncApi;
    }

    public ArrayList<DocumentReferenceModel> getDocumentReferenceModel() {
        return mdocumentList;
    }

    public boolean isMedicationSyncApi() {
        return isMedicationSyncApi;
    }

    public String getUploadSyncTime() {
        return uploadSyncTime;
    }

    public void setUploadSyncTime(String uploadSyncTime) {
        this.uploadSyncTime = uploadSyncTime;
    }

    public boolean isAllMedicationOrdersLoaded() {
        return isAllMedicationOrdersLoaded;
    }

    public void setIsAllMedicationOrdersLoaded(boolean isAllMedicationOrdersLoaded) {
        this.isAllMedicationOrdersLoaded = isAllMedicationOrdersLoaded;
    }

    public ArrayList<PrescriptionModel> getPrescriptionModel() {
        return arrayPrescriptionModel;
    }
}
