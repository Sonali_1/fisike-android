package com.mphrx.fisike_physician.network.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.response.LanguagePreferenceResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by laxmansingh on 5/1/2017.
 */

public class LanguagePreferenceRequest extends BaseObjectRequest {


    private long transactionId;
    private Context context;
    private String selectedLocale;


    public LanguagePreferenceRequest(long transactionId, Context context, String selectedLocale) {
        this.transactionId = transactionId;
        this.context = context;
        this.selectedLocale = selectedLocale;
    }

    @Override
    public void doInBackground() {
        String baseUrl = APIManager.createMinervaBaseUrl() + CachedUrlConstants.LanguagePreference_postfix;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new LanguagePreferenceResponse(error, transactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new LanguagePreferenceResponse(response, transactionId, context));
    }

    public String getPayLoad() {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userLanguage", selectedLocale);
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
