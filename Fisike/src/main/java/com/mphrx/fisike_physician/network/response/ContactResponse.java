package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brijesh on 15/10/15.
 */
public class ContactResponse extends BaseResponse {

    private ArrayList<ChatContactMO> mAllUserList;
    private boolean mIsPullToRefresh;

    public ContactResponse(JSONObject response, long transactionId, boolean isPullRefreshCall) {
        super(response, transactionId);
        parseResponse(response);
        mIsPullToRefresh = isPullRefreshCall;
    }

    public ContactResponse(Exception exception, long transactionId, boolean isPullToRefreshCall) {
        super(exception, transactionId);
        mIsPullToRefresh = isPullToRefreshCall;
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray resultList = response.getJSONArray("resultList");
            mAllUserList = new ArrayList<>(resultList.length());
            for (int i = 0; i < resultList.length(); i++) {
                try {
                    ChatContactMO user = getUserData(resultList.getJSONObject(i));
                    mAllUserList.add(user);
                    chatContactDBAdapter.getInstance(MyApplication.context).updateChatContactMoUserProfile(user);
                }
                catch (JSONException ee) {
                    // Skip Data
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (JSONException e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private ChatContactMO getUserData(JSONObject object) throws JSONException {
        return (ChatContactMO)GsonUtils.jsonToObjectMapper(object.toString(), ChatContactMO.class);
    }

    public ArrayList<ChatContactMO> getAllUserList() {
        return mAllUserList;
    }

    public boolean isPullToRefreshCall() {
        return mIsPullToRefresh;
    }

}
