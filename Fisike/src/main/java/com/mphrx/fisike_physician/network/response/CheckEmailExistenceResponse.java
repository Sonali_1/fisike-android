package com.mphrx.fisike_physician.network.response;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by brijesh on 07/12/15.
 */
public class CheckEmailExistenceResponse extends BaseResponse {


    public CheckEmailExistenceResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
    }

    public CheckEmailExistenceResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        if (exception instanceof VolleyError) {
            if (((VolleyError) exception).networkResponse != null) {
                int statusCode = ((VolleyError) exception).networkResponse.statusCode;
                if (statusCode == 422) {
                    mMessage = "Email Id already used.";
                }
            }
        }
    }

}
