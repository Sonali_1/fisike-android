/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mphrx.fisike_physician.network;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView.ScaleType;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.ProfilePictureGson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Set;

/**
 * A canned request for getting an image at a given URL and calling
 * back with a decoded Bitmap.
 */
public class FisikeContactImageRequest extends FisikeImageRequest {

    public FisikeContactImageRequest(String url, Response.Listener<Bitmap> listener, int maxWidth, int maxHeight, ScaleType scaleType, Config decodeConfig, Response.ErrorListener errorListener) {
        super(url, listener, maxWidth, maxHeight, scaleType, decodeConfig, errorListener);
    }

    public FisikeContactImageRequest(String url, Response.Listener<Bitmap> listener, int maxWidth, int maxHeight, Config decodeConfig, Response.ErrorListener errorListener) {
        super(url, listener, maxWidth, maxHeight, decodeConfig, errorListener);
    }

    @Override
    protected JSONObject getJSONPayload() {
        JSONObject body = new JSONObject();
        try {
            String url = getOriginUrl();
            Uri uri = Uri.parse(url);
            Set<String> keys = uri.getQueryParameterNames();
            for (String key : keys) {
                String value = uri.getQueryParameter(key);
                body.put(key, value);
            }
        } catch (Exception e) {
        }
        return body;
    }

    @Override
    protected Response<Bitmap> doParse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ProfilePictureGson.class);
            if (jsonToObjectMapper instanceof ProfilePictureGson) {
                ProfilePictureGson profilePictureGson = ((ProfilePictureGson) jsonToObjectMapper);
                ArrayList<ProfilePictureGson.ProfilePicture> profilePictureArray = profilePictureGson.getProfilePictureArray();
                for (int i = 0; i < profilePictureArray.size(); i++) {
                    ProfilePictureGson.ProfilePicture profilePicture = profilePictureArray.get(i);
                    String id = profilePicture.getId() + "";
                    byte[] profilePic = profilePicture.getProfilePic();
                    if (!TextUtils.isEmpty(id) && (null != profilePic)) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);

                        if (bitmap == null) {
                            return Response.error(new ParseError(response));
                        } else {
                            return Response.success(bitmap, HttpHeaderParser.parseCacheHeaders(response));
                        }
                    }
                }
            }
        } catch (Exception ignore) {
        }
        return Response.error(new ParseError(response));
    }
}
