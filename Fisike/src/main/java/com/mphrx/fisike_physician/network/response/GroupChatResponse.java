package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.ChatConversationMO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by manohar on 28/10/15.
 */
public class GroupChatResponse extends BaseResponse {

    private ArrayList<ChatConversationMO> mGroupChatBeans;

    public GroupChatResponse(JSONObject response, long transactionId) {

        super(response, transactionId);
        mGroupChatBeans = new ArrayList<>();
        parseResponse(response);
    }

    public GroupChatResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray("list");
            for (int i = 0; i < jsonArray.length(); i++) {
                ChatConversationMO groupChatBean = (ChatConversationMO) GsonUtils.jsonToObjectMapper(jsonArray.getJSONObject(i).toString(), ChatConversationMO.class);
                mGroupChatBeans.add(groupChatBean);
            }
        } catch (JSONException e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    public ArrayList<ChatConversationMO> getGroupChatBeans() {
        return mGroupChatBeans;
    }

}
