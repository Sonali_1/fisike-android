package com.mphrx.fisike_physician.network.response;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 06/11/15.
 */
public class SendInviteResponse extends BaseResponse {

    private List<User> mAllUserList;

    public SendInviteResponse(List<User> fisikeInviteResult, List<User> phoneInviteList, boolean isPhoneRequestSuccessful, long transactionId) {
        super(new JSONObject(), transactionId);
        if (isPhoneRequestSuccessful)
            insertUserListIntoDatabase(phoneInviteList);
        int count = 0;
        for (User user : fisikeInviteResult)
            if (user.isInvitationSuccessful)
                count++;
        mAllUserList = new ArrayList<>();
        mAllUserList.addAll(fisikeInviteResult);
        mAllUserList.addAll(phoneInviteList);
        AppLog.showInfo("SendInviteRespose",fisikeInviteResult.toString());
        if (!(count == fisikeInviteResult.size()&& isPhoneRequestSuccessful)) {
            mIsSuccessful = false;
            mMessage = "Something went wrong...!!!";
        }

    }

    private void insertUserListIntoDatabase(List<User> userList) {
        try {
            DBAdapter.getInstance(MyApplication.getInstance()).insertUserInviteList(userList);
        }
        catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
        }
    }

    public List<User> getAllUserList() {
        return mAllUserList;
    }

}
