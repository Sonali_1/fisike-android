package com.mphrx.fisike_physician.network.response;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by laxmansingh on 5/1/2017.
 */

public class LanguagePreferenceResponse extends BaseResponse {
    private String mStatus = "";
    public String msg = "";
    private String jsonString;
    private Context context;
    private String successfullyChanged = null;
    public int status;
    public int httpStatusCodes;


    public LanguagePreferenceResponse(JSONObject response, long transactionId,
                                      Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public LanguagePreferenceResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            jsonString = response.toString();
            processLanguagePrefrence();
            mIsSuccessful = true;
        } catch (Exception e) {
            mIsSuccessful = false;
            mMessage = e.getMessage();
        }
    }

    private void processLanguagePrefrence() {
        try {
            JSONObject mainObject = new JSONObject(jsonString);
            try {
                successfullyChanged = mainObject.optString("msg");
                status = mainObject.optInt("status");
                httpStatusCodes = mainObject.optInt("httpStatusCode");
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSuccessfullyChanged() {
        return successfullyChanged;
    }

    public void setSuccessfullyChanged(String successfullyChanged) {
        this.successfullyChanged = successfullyChanged;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHttpStatusCodes() {
        return httpStatusCodes;
    }

    public void setHttpStatusCodes(int httpStatusCodes) {
        this.httpStatusCodes = httpStatusCodes;
    }
}
