package com.mphrx.fisike_physician;

import android.support.annotation.Nullable;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by manohar on 11/02/16.
 */
public interface IGroupImageViewLoader {

    /**
     * It display the images of contacts max 4 images. If group image is available or it is a one to one chat then it display a single image of group or other contact.
     *
     * @param contactIds
     * @param groupId
     */
    void displayContactImages(ArrayList<String> contactIds, @Nullable String groupId);

    void setRectangleImage(boolean rectangleImage);

    void setOnClickListener(View.OnClickListener onClickListener);

    void setViewProfileImage(String contactIds);
}
