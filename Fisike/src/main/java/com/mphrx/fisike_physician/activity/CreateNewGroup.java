package com.mphrx.fisike_physician.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.NewGroupAdapter;
import com.mphrx.fisike_physician.adapters.SearchAdapter;
import com.mphrx.fisike_physician.adapters.UserInvitesAdapter;
import com.mphrx.fisike_physician.network.request.ContactRequest;
import com.mphrx.fisike_physician.network.request.PhysicianDetailRequest;
import com.mphrx.fisike_physician.network.response.SearchResponse;
import com.mphrx.fisike_physician.utils.ProfilePicUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.RAutoCompleteTextView;
import com.mphrx.fisike_physician.views.RButton;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class CreateNewGroup extends PhysicianHeaderActivity implements View.OnClickListener, UserInvitesAdapter.InviteUserListener, NewGroupAdapter.OnUserRemove {

    private CustomFontEditTextView etLname;
    private RButton btnCameraIcon;
    private RAutoCompleteTextView mNameOrNumber;
    private ProgressBar mProgressBar;
    private ListView lvContacts;
    private ArrayList<ChatContactMO> mSearchResults, mUserList;
    private NewGroupAdapter mContactAdapter;
    private SearchAdapter mSearchAdapter;
    private Toolbar mToolbar;
    private static String groupID;
    private ImageView mUserImage;
    private Uri mImageUri = null;
    private String mImagePath = null;
    private Bitmap mBitmap;
    private final int SELECT_PHOTO = 1001;
    private final int CLICK_PHOTO = 1002;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_create_new_group);
        showHideToolbar(false);
        findViews();
        initViews();
        bindViews();

    }

    private static final String TEMP = "temp";
    private static final String SUFFIX = ".jpg";

    private File createImageFile() throws IOException {
        File storageDir = Environment.getExternalStorageDirectory();
        File image = new File(storageDir, TEMP + SUFFIX);
        boolean dirOk = image.getParentFile().isDirectory() || image.getParentFile().mkdirs();
        boolean fileOk = image.exists() || image.createNewFile();

        if (!dirOk || !fileOk)
            throw new IOException("Unable to create temp file");

        // Save path for use with ACTION_VIEW intents
        mImagePath = image.getAbsolutePath();
        return image;
    }

    public boolean hasCamera(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    mImageUri = data.getData();
                    loadImage();
                }
                break;
            case CLICK_PHOTO:
                if (resultCode != Activity.RESULT_OK) {
                    mImagePath = null;
                } else {
                    loadImage();
                }
                break;

        }
    }

    private void loadImage() {
        if (mImageUri != null) {
            try {
                mBitmap = ProfilePicUtils.safeDecodeStream(this, mImageUri);
                mUserImage.setImageBitmap(mBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mImageUri = null;
                mImagePath = null;
            }
        } else if (mImagePath != null) {
            try {
                mBitmap = ProfilePicUtils.safeDecodeFile(this, mImagePath);
                mUserImage.setImageBitmap(mBitmap);

                File tmp = new File(mImagePath);
                if (tmp.exists())
                    tmp.delete();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mImageUri = null;
                mImagePath = null;
            }
        }

    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
        try {
            startActivityForResult(intent, SELECT_PHOTO);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, R.string.profile_pic_no_picker, Toast.LENGTH_SHORT).show();
        }
    }

    private void showImagePickerOptions() {
        if (!hasCamera(this)) {
            pickImage();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.profile_pic_dialog_title);
        builder.setItems(R.array.profile_pic_dialog_options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(intent, CLICK_PHOTO);
                        } catch (Exception ex) {
                            Toast.makeText(CreateNewGroup.this, R.string.profile_pic_no_write, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(CreateNewGroup.this, R.string.profile_pic_no_camera, Toast.LENGTH_SHORT).show();
                    }
                } else if (item == 1) {
                    pickImage();
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void bindViews() {
        mNameOrNumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatContactMO mo = (ChatContactMO) mSearchAdapter.getItem(position);
                hideKeyboard();
                if (isUserAlreadyAddedToInviteList(mo.getId()))
                    return;

                mUserList.add(mSearchResults.get(position));
                mContactAdapter.updateUserList(mUserList);
            }
        });

        mNameOrNumber.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {


                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (event == null) {
                            return true;
                        }
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            hideKeyboard();
                            return true;
                        }
                        return false;
                    }
                });

        mNameOrNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!Utils.isNetworkAvailable(CreateNewGroup.this)) {
                    Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_LONG).show();
                    return;
                }
                if (s.length() >= 2) {
                    mSearchResults.clear();
                    mProgressBar.setVisibility(View.VISIBLE);
                    ThreadManager.getDefaultExecutorService().submit(new ContactRequest(getTransactionId(), s.toString(), false, ContactRequest.ContactType.PHYSICIAN_CONTACT, false));
                }
            }
        });

        mNameOrNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    private void initViews() {
        mUserList = new ArrayList<>();
        mSearchResults = new ArrayList<>();
        mContactAdapter = new NewGroupAdapter(mUserList, this, false);
        mContactAdapter.setOnUserRemove(this);
        lvContacts.setAdapter(mContactAdapter);


        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.create_group));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);

        etLname = (CustomFontEditTextView) findViewById(R.id.et_lname);
        btnCameraIcon = (RButton) findViewById(R.id.btn_cameraIcon);
//        FrameLayout addContact = (FrameLayout) findViewById(R.id.add_contact);
        mNameOrNumber = (RAutoCompleteTextView) findViewById(R.id.name_or_number);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
//        ImageView addPersonBtn = (ImageView) findViewById(R.id.add_person_btn);
        lvContacts = (ListView) findViewById(R.id.lv_contacts);
        mUserImage = (ImageView) findViewById(R.id.iv_profilepic);


        btnCameraIcon.setOnClickListener(this);
    }

    private boolean isUserAlreadyAddedToInviteList(String id) {
        if (mSearchResults != null) {
            for (ChatContactMO user : mUserList)
                if (user.getId().equals(id)) {
                    Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.User_already_added), Toast.LENGTH_SHORT).show();
                    return true;
                }
        }
        return false;
    }


    @Override
    public void fetchInitialDataFromServer() {


    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    @Override
    public void onClick(View v) {
        if (v == btnCameraIcon) {
            showImagePickerOptions();
        }
    }


    @Override
    public void onCLickRole(int index) {

    }

    @Override
    public void onClickCancel(int index) {

    }

    @Subscribe
    public void onSearchUserResponse(SearchResponse response) {

        mProgressBar.setVisibility(View.GONE);

        if (getTransactionId() != response.getTransactionId())
            return;

        if (!response.getSearchString().equals(mNameOrNumber.getText().toString()))
            return;

        if (mSearchAdapter == null) {
            mSearchAdapter = new SearchAdapter(mSearchResults);
            mNameOrNumber.setAdapter(mSearchAdapter);
        }
        if (response.isSuccessful()) {
            mSearchResults.clear();
            mSearchResults.addAll(response.getList());
            mSearchAdapter.notifyDataSetChanged();
            if (response.getList().size() == 0) {
                Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.No_user_found), Toast.LENGTH_SHORT).show();
            }
            mNameOrNumber.showDropDown();
        } else {
            mSearchAdapter.updateList(new ArrayList<ChatContactMO>(0));
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                addContactsToDB(mSearchResults);
            }
        }).start();
    }

    private void addContactsToDB(ArrayList<ChatContactMO> searchResults) {
        for (int i = 0; i < searchResults.size(); i++) {
            try {
                chatContactDBAdapter.getInstance(this).updateChatContactMoUserProfile(searchResults.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.add(1, 1, 1, getResources().getString(R.string.Done));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == 1) {

            if (!Utils.isNetworkAvailable(this)) {

                Toast.makeText(this, getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_LONG).show();

                return true;
            }
            if (etLname.getText().toString().trim().length() == 0) {
                Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.Please_enter_the_group_name), Toast.LENGTH_SHORT).show();
                return true;
            }
            if (mUserList.size() == 0) {
                Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.Please_add_any_colleague), Toast.LENGTH_SHORT).show();
                return true;
            }


            showProgressDialog();

            DBAdapter dbAdapter = DBAdapter.getInstance(this);

            for (int i = 0; i < mUserList.size(); i++) {

                String id = mUserList.get(i).getId();
                String userId = id.contains("@") ? id.split("@")[0] : id;

                if (!chatContactDBAdapter.getInstance(this).isChatContactMoAvailable(userId)) {
                    getMessengerService().getGroupChatUsersDetails(id);
                }
                ChatContactMO updateChatContactMO = mUserList.get(i);

                if (updateChatContactMO != null && chatContactDBAdapter.getInstance(this).isChatContactMoAvailable(updateChatContactMO.getId())
                        ) {
                    String physicianId = Long.toString(updateChatContactMO.getPhysicianId());
                    ThreadManager.getDefaultExecutorService().submit(new PhysicianDetailRequest(getTransactionId(), physicianId, true, updateChatContactMO.getPersistenceKey()));
                }
            }
            groupID = System.currentTimeMillis() + "_" + SettingManager.getInstance().getUserMO().getId() + "@conference." + SettingManager.getInstance().getSettings().getFisikeServerIp();
            getMessengerService().createNewGroup(etLname.getText().toString().trim(), mUserList, mBitmap, true, groupID, false);


        }
        return super.onOptionsItemSelected(item);
    }

    private void registerBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(groupCreatedBroadcast, new IntentFilter(VariableConstants.GROUP_INVITE_SENT));
    }


    private void unregisterBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(groupCreatedBroadcast);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterBroadcastReceiver();
    }


    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    private BroadcastReceiver groupCreatedBroadcast = new BroadcastReceiver() {

        @Override

        public void onReceive(Context context, Intent intent) {

            dismissProgressDialog();


            // need to have a check whether group is successfully created
            if (intent.getBooleanExtra(VariableConstants.LAUNCH_GROUP_ROSTER, false)) {
                Intent intent1 = getIntent();
                intent.putExtra(VariableConstants.IS_GROUP_CHAT, true);
                CreateNewGroup.this.setResult(Activity.RESULT_OK, intent1);
                CreateNewGroup.this.finish();
            } else if (intent.getBooleanExtra(VariableConstants.GROUP_CREATION_ERROR, false)) {
                // show error dialog.

                Toast.makeText(CreateNewGroup.this, getResources().getString(R.string.Error), Toast.LENGTH_SHORT).show();
            } else {
                // move to group details screen
                Intent intent1 = new Intent();
                intent1.putExtra(VariableConstants.INVITE_LIST_ADDED_MEMBERS, mUserList);
                CreateNewGroup.this.setResult(RESULT_OK, intent1);
                CreateNewGroup.this.finish();
            }


            UserMO userMO = SettingManager.getInstance().getUserMO();

            String userName = "";//Utils.cleanXMPPUserName(chatConversationMO.getJId());// 1432544588066_phy1%40abis.33mail.com@conference.dev1
            String phoneNumber = null;//chatConversationMO.getPhoneNumber();
            Intent intent1 = new Intent(CreateNewGroup.this, com.mphrx.fisike.MessageActivity.class);
            int myStatus = 0;
            String subString = new String(groupID);
            GroupTextChatMO groupTextChatMO = new GroupTextChatMO();
            subString = Utils.extractGroupId(subString);
            groupTextChatMO.generatePersistenceKey(subString, userMO.getId() + "");
            userName = groupID;
            try {
                groupTextChatMO = GroupChatDBAdapter.getInstance(CreateNewGroup.this).fetchGroupTextChatMO(groupTextChatMO.getPersistenceKey() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            ChatConversationDBAdapter.getInstance(CreateNewGroup.this).updateIsChatInitiated(ChatConversationMO.generatePersistenceKey(groupID, userMO.getId() + ""));

            intent1.putExtra(VariableConstants.GROUP_INFO, groupTextChatMO).putExtra(VariableConstants.MY_STATUS_ACTIVE, myStatus);
            intent1.putExtra(VariableConstants.SENDER_SECOUNDRY_ID, groupID);
            intent1.putExtra(VariableConstants.SENDER_ID, userName)
                    .putExtra(VariableConstants.PHONE_NUMBER, phoneNumber)
                    .putExtra(VariableConstants.SENDER_NICK_NAME, etLname.getText().toString().trim())
                    .putExtra(VariableConstants.SENDER_UNREAD_COUNT, 0).putExtra(TextConstants.IS_RECENT_CHAT, true)
                    .putExtra(VariableConstants.IS_GROUP_CHAT, true)
                    .putExtra("isPhysician", true)
                    .putExtra("groupName", etLname.getText().toString().trim());


            startActivity(intent1);
        }
    };


    @Override
    public void onUserRemoved(int pos) {
        mUserList.remove(pos);
        mContactAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMessengerServiceBind() {
    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showHideToolbar(true);
    }
}
