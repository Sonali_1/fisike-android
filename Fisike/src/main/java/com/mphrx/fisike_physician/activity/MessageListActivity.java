package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.fragments.SingleChatFragment;

/**
 * Created by brijesh on 12/10/15.
 */
public class MessageListActivity extends com.mphrx.fisike.utils.BaseActivity {

    private Toolbar mToolbar;
    private FrameLayout frameLayout;
    public OnNavigationItemClickedListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout= (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_message_list, frameLayout);
        if (!mBound) {
            bindMessengerService();
        }
        Fragment singleChatFragment = SingleChatFragment.newInstance(0);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, singleChatFragment);

        fragmentTransaction.commit();
        findView();
        initView();
    }



    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
    }

    private void initView() {

        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.home_message));
        if(BuildConfig.isPhysicianApp) {
            showHideToolbar(false);
            mToolbar.setNavigationIcon(R.drawable.hamburger);
            mToolbar.setTitle(getString(R.string.home_message));
            //MyApplication.getInstance().registerForNavigationClick(this);
        }
        else {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
    //    mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BuildConfig.isPhysicianApp) {
                    openDrawer();
                }
                else
                  finish();
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentItem(NavigationDrawerActivity.MESSAGES);
    }
}
