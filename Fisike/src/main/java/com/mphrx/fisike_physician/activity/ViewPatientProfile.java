package com.mphrx.fisike_physician.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.PastConditionsAdapter;
import com.mphrx.fisike.adapter.PastConditionsStatusAdapter;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.models.PastConditionsModel;
import com.mphrx.fisike_physician.network.request.PastConditionRequest;
import com.mphrx.fisike_physician.network.response.PastConditionResponse;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by laxmansingh on 6/6/2016.
 */
public class ViewPatientProfile extends PhysicianHeaderActivity {

    private CustomFontTextView mobileNumber, txtNameHeader, txtHeader;
    private CustomFontTextView txtUserDOB;
    private CustomFontTextView txtUserGender;

    ChatContactMO chatContactMO;
    private DBAdapter mHelper;
    private Toolbar mToolbar;
    private AsyncTask<Void, Void, Void> loadContactsRealName;
    private AsyncTask<Void, Void, Void> loadContactProfilePicAsyn;
    private CustomProfileVIewLayout profileLayout;


    /*[................ past conditions instances............]*/
    private PastConditionsAdapter pastConditionsAdapter;
    private View pastconditions_view;
    private CustomFontTextView tv_view_more, tv_add_new_condition;
    private RecyclerView rv_past_conditions;
    private java.util.List<PastConditionsModel> pastConditionsModelList = new ArrayList<PastConditionsModel>();
    private RelativeLayout temp_msg_lyt;
    private ProgressBar progress;
    private CustomFontTextView tv_msg;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout myAppbar;
    private CoordinatorLayout mCoOrdinatorlayout;
    private Context mContext;
    private Spinner spinner_past_conditions_staus;
    private PastConditionsStatusAdapter pastConditionsStatusAdapter;
    private List<String> list_pastConditions_staus;
    /*[................End of past conditions instances............]*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.isPhysicianApp)
            setCurrentItem(-1);

        super.setContentView(R.layout.viewpatientprofile);
        mContext = this;
        findViews();
        showHideToolbar(false);

        String userId = getIntent().getStringExtra(VariableConstants.SENDER_ID);
        Log.i("userid", userId);

        try {
            chatContactMO = chatContactDBAdapter.getInstance(this).fetchChatContactMo(ChatContactMO.getPersistenceKey(TextPattern.getUserId(userId)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
        bindView();


        // BusProvider.getInstance().register(this);
        ThreadManager.getDefaultExecutorService().submit(new PastConditionRequest(getTransactionId(), this, chatContactMO.getPatientId()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initView() {
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        setSupportActionBar(mToolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        //setTitle(chatContactMO.getDisplayName());
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout.setTitle(chatContactMO.getDisplayName());


        spinner_past_conditions_staus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (list_pastConditions_staus.get(position).equals("Active")) {

                } else if (list_pastConditions_staus.get(position).equals("All")) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        list_pastConditions_staus = Arrays.asList(getResources().getStringArray(R.array.pastconditions_status_array));
        pastConditionsStatusAdapter = new PastConditionsStatusAdapter(this, R.layout.spinner_address, list_pastConditions_staus);
        spinner_past_conditions_staus.setAdapter(pastConditionsStatusAdapter);
    }


    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_profile_screen);
        profileLayout = (CustomProfileVIewLayout) findViewById(R.id.iv_profile_picture);

        txtHeader = (CustomFontTextView) findViewById(R.id.txtHeader);
        txtHeader.setVisibility(View.GONE);
        txtNameHeader = (CustomFontTextView) findViewById(R.id.txtNameHeader);
        mobileNumber = (CustomFontTextView) findViewById(R.id.txtUserName);
        txtUserDOB = (CustomFontTextView) findViewById(R.id.txtUserDOB);
        txtUserGender = (CustomFontTextView) findViewById(R.id.txtUserGender);
        mCoOrdinatorlayout = (CoordinatorLayout) findViewById(R.id.ll_profile_container);
        myAppbar = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);

        pastconditions_view = findViewById(R.id.cardpastconditions);
        rv_past_conditions = (RecyclerView) pastconditions_view.findViewById(R.id.rv_past_conditions);
        temp_msg_lyt = (RelativeLayout) pastconditions_view.findViewById(R.id.temp_msg_lyt);
        progress = (ProgressBar) pastconditions_view.findViewById(R.id.progress);
        spinner_past_conditions_staus = (Spinner) pastconditions_view.findViewById(R.id.spinner_past_conditions_staus);
        tv_msg = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_msg);
        tv_view_more = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_view_more);
        tv_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_view_more.getTag().equals(mContext.getResources().getString(R.string.tag_more))) {
                    tv_view_more.setTag(mContext.getResources().getString(R.string.tag_less));
                    tv_view_more.setText(mContext.getResources().getString(R.string.view_less));
                    pastConditionsAdapter.notifyDataSetChanged();
                } else if (tv_view_more.getTag().equals(mContext.getResources().getString(R.string.tag_less))) {
                    tv_view_more.setTag(mContext.getResources().getString(R.string.tag_more));
                    tv_view_more.setText(mContext.getResources().getString(R.string.view_more));
                    pastConditionsAdapter.notifyDataSetChanged();
                }
            }
        });
        tv_add_new_condition = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_add_new_condition);
        tv_add_new_condition.setVisibility(View.GONE);

        rv_past_conditions.setLayoutManager(new LinearLayoutManager(this));
        rv_past_conditions.setNestedScrollingEnabled(false);
        pastConditionsAdapter = new PastConditionsAdapter(this, pastConditionsModelList, tv_view_more);
        rv_past_conditions.setAdapter(pastConditionsAdapter);
    }

    void bindView() {

        String date = chatContactMO.getDob() != null ? (chatContactMO.getDob().contains(":") ? Utils.getFormattedDate(chatContactMO.getDob()) : chatContactMO.getDob()) : "";
        txtNameHeader.setText(getResources().getString(R.string.Mobile_Number));
        mobileNumber.setText(chatContactMO.getPhoneNo());
        txtUserDOB.setText(date);
        txtUserGender.setText(chatContactMO.getGender().equalsIgnoreCase("M") ? getResources().getString(R.string.Male) : chatContactMO.getGender().equalsIgnoreCase("F") ? getResources().getString(R.string.Female) : chatContactMO.getGender());
        profileLayout.setRectangleImage(true);
        profileLayout.setViewProfileImage(chatContactMO.getId());
    }


    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    @Override
    public void onMessengerServiceBind() {
        loadContactsRealName = new LoadContactsRealName(getMessengerService(), chatContactMO.getId(), this).execute();
    }

    @Override
    public void onMessengerServiceUnbind() {

    }


    @Subscribe
    public void onPastConditionResponse(PastConditionResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;


        if (response.isSuccessful() && response.getmPastConditionsResponseList() != null && response.getmPastConditionsResponseList().size() > 0) {
            pastconditions_view.setVisibility(View.VISIBLE);
            pastConditionsModelList.clear();
            pastConditionsModelList.addAll(response.getmPastConditionsResponseList());
            rv_past_conditions.setVisibility(View.VISIBLE);
            temp_msg_lyt.setVisibility(View.GONE);
            spinner_past_conditions_staus.setEnabled(true);
            if (pastConditionsModelList.size() <= 2) {
                tv_view_more.setTag("");
                tv_view_more.setVisibility(View.GONE);
            } else {
                tv_view_more.setTag(mContext.getResources().getString(R.string.tag_more));
                tv_view_more.setVisibility(View.VISIBLE);
            }
            pastConditionsAdapter.notifyDataSetChanged();
        } else {
            rv_past_conditions.setVisibility(View.GONE);
            temp_msg_lyt.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            tv_msg.setText(getResources().getString(R.string.No_Past_Conditions));
            pastconditions_view.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadContactsRealName != null && !loadContactsRealName.isCancelled()) {
            loadContactsRealName.cancel(true);
            loadContactsRealName = null;
        }
        if (loadContactProfilePicAsyn != null && !loadContactProfilePicAsyn.isCancelled()) {
            loadContactProfilePicAsyn.cancel(true);
            loadContactProfilePicAsyn = null;
        }
        showHideToolbar(false);
    }
}
