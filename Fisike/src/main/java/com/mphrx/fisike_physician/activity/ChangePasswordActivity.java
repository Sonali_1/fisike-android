package com.mphrx.fisike_physician.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike_physician.network.request.ChangePasswordRequest;
import com.mphrx.fisike_physician.network.response.ChangePasswordResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

/**
 * Created by brijesh on 03/12/15.
 */
public class ChangePasswordActivity extends NetworkActivity {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;

    private CustomFontEditTextView mOldPassword;
    private CustomFontEditTextView mNewPassword;
    private CustomFontEditTextView mConfirmPassword;
    private Button mSubmitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        findViews();
        initViews();
        bindViews();

    }

    @Override
    public void fetchInitialDataFromServer() {
        // Do Nothing
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }

    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mOldPassword = (CustomFontEditTextView) findViewById(R.id.old_password);
        mNewPassword = (CustomFontEditTextView) findViewById(R.id.new_password);
        mConfirmPassword = (CustomFontEditTextView) findViewById(R.id.confirm_password);
        mSubmitButton = (Button) findViewById(R.id.btn_submit);


        NumberKeyListener PwdkeyListener = new NumberKeyListener() {

            public int getInputType() {
                return InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
            }

            @Override
            protected char[] getAcceptedChars() {
                return TextConstants.passwordAllowedChars;
            }
        };
        mNewPassword.setKeyListener(PwdkeyListener);
        mConfirmPassword.setKeyListener( PwdkeyListener);
    }

    private void initViews() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar_title.setText(getResources().getString(R.string.change_password));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordActivity.this.finish();
            }
        });

        mOldPassword.setTransformationMethod(new PasswordTransformationMethod());
        mNewPassword.setTransformationMethod(new PasswordTransformationMethod());
        mConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void bindViews() {
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubmit();
            }
        });
    }

    private void onClickSubmit() {
        if (TextUtils.isEmpty(mOldPassword.getText().toString().trim())) {
            mOldPassword.setError(getString(R.string.old_pass_empty));
//            Toast.makeText(this, R.string.old_pass_empty, Toast.LENGTH_SHORT).show();
            return;
        }else{
            mOldPassword.setErrorEnabled(false);
        }
        if (TextUtils.isEmpty(mNewPassword.getText().toString().trim())) {
//            Toast.makeText(this, R.string.new_pass_empty, Toast.LENGTH_SHORT).show();
            mNewPassword.setError(getString(R.string.new_pass_empty));
            return;
        }else{
            mNewPassword.setErrorEnabled(false);
        }


        if (mNewPassword.getText().toString().trim().length() < 6) {
//            Toast.makeText(this, "Password minimum length is 6 ", Toast.LENGTH_SHORT).show();
            mNewPassword.setError(getString(R.string.Password_minimum_length));
            mNewPassword.requestFocus();
            return ;
        }else{
            mNewPassword.setErrorEnabled(false);
        }


        if (TextUtils.isEmpty(mConfirmPassword.getText().toString().trim())) {
//            Toast.makeText(this, R.string.confirm_pass_empty, Toast.LENGTH_SHORT).show();
            mConfirmPassword.setError(getString(R.string.confirm_pass_empty));
            return;
        }else{
            mConfirmPassword.setErrorEnabled(false);
        }
        if (!mConfirmPassword.getText().toString().equals(mNewPassword.getText().toString())) {
//            Toast.makeText(this, R.string.confirm_new_pass_mismatch, Toast.LENGTH_SHORT).show();
            mConfirmPassword.setError(getString(R.string.confirm_new_pass_mismatch));
            return;
        }
        if (mConfirmPassword.getText().toString().equals(mOldPassword.getText().toString())) {
//            Toast.makeText(this, R.string.old_password_new_password_are_same, Toast.LENGTH_SHORT).show();
            mConfirmPassword.setError(getString(R.string.old_password_new_password_are_same));
            return;
        }


        showProgressDialog();
        ThreadManager.getDefaultExecutorService().submit(new ChangePasswordRequest(mOldPassword.getText().toString(), mNewPassword.getText().toString(), getTransactionId()));

    }


    @Subscribe
    public void onChangePasswordResponse(ChangePasswordResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();
        if (response.isSuccessful()) {
            SharedPref.setAccessToken(null);
            //openActivity(LoginActivity.class, null, true);
            Toast.makeText(this, R.string.password_changed_successfully, Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
