package com.mphrx.fisike_physician.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike_physician.ChatUserOptionListDialog;
import com.mphrx.fisike_physician.adapters.ContactAdapter;
import com.mphrx.fisike_physician.network.request.ContactRequest;
import com.mphrx.fisike_physician.network.response.SearchResponse;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.REditText;
import com.mphrx.fisike_physician.views.RTextView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by brijesh on 07/10/15.
 */
public class SearchUserActivity extends PhysicianHeaderActivity {


    private ProgressBar mProgressBar;

    //    private View mEmptyView;
    private RTextView mErrorInfo;
    private ListView mContactsList;
    private Toolbar mToolbar;
    private ContactAdapter mContactAdapter;
    private REditText mSearchView;

    private ContactRequest.ContactType mContactType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_search_user);
        showHideToolbar(false);
        findView();
        initView();
        bindView();

        String contactType = TextConstants.ALL_CONTACT;

        if (getIntent().getExtras() != null)
            contactType = getIntent().getExtras().getString(TextConstants.USER_TYPE, TextConstants.ALL_CONTACT);

        if (contactType.equals(TextConstants.ALL_CONTACT))
            mContactType = ContactRequest.ContactType.ALL_CONTACT;
        else if (contactType.equals(TextConstants.PATIENT_CONTACT))
            mContactType = ContactRequest.ContactType.PATIENT_CONTACT;
        else if (contactType.equals(TextConstants.OTHER_CONTACT))
            mContactType = ContactRequest.ContactType.PHYSICIAN_CONTACT;

        setResult(Activity.RESULT_CANCELED);

    }

    @Override
    public void fetchInitialDataFromServer() {
        // Do Nothing
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }

    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mErrorInfo = (RTextView) findViewById(R.id.tv_error_info);
        mContactsList = (ListView) findViewById(R.id.lv_contacts);
        mSearchView = (REditText) findViewById(R.id.search_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        String uniqueID = UUID.randomUUID().toString();


    }

    private void initView() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mErrorInfo.setVisibility(View.GONE);

        boolean loadAllContacts = getIntent().getBooleanExtra("loadAllContacts", true);
        mContactAdapter = new ContactAdapter(chatContactDBAdapter.getInstance(SearchUserActivity.this).getAllChatContactMO(loadAllContacts));
        mContactsList.setAdapter(mContactAdapter);

    }

    private void bindView() {
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mProgressBar.setVisibility(View.VISIBLE);
                if(mSearchView != null && mSearchView != null)
                mSearchView.removeTextChangedListener(this);
                boolean shouldInsertInDB = getIntent().getExtras().getBoolean("isAddColleague", false);
                ThreadManager.getDefaultExecutorService().submit(new ContactRequest(getTransactionId(), s.toString(), false, mContactType, shouldInsertInDB));
                mSearchView.addTextChangedListener(this);
            }
        });

        mContactsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ChatContactMO user = (ChatContactMO) mContactAdapter.getItem(position);
                if (getIntent().getExtras().getBoolean("isAddColleague", false)) {
                    setResult(RESULT_OK, new Intent().putExtra("contact", user));
                    finish();
                    return;
                }
                new ChatUserOptionListDialog(SearchUserActivity.this).onItemCLick((ChatContactMO) mContactAdapter.getItem(position), true);

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void setExitAndEnterAnimation() {
        overridePendingTransition(0, 0);
    }

    @Override
    public void onMessengerServiceBind() {

    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    @Subscribe
    public void onSearchUserResponse(SearchResponse response) {

        mProgressBar.setVisibility(View.GONE);

        if (getTransactionId() != response.getTransactionId())
            return;

        if (!response.getSearchString().equals(mSearchView.getText().toString()))
            return;

        if (response.isSuccessful()) {
            ArrayList<ChatContactMO> list = response.getList();
            if (list.size() == 0) {
                mErrorInfo.setVisibility(View.VISIBLE);
                mContactsList.setVisibility(View.GONE);
            } else {
                mErrorInfo.setVisibility(View.GONE);
                mContactsList.setVisibility(View.VISIBLE);
            }
            mContactAdapter.updateUserList(response.getList());

        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showHideToolbar(true);
    }
}
