package com.mphrx.fisike_physician.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike_physician.adapters.SearchAdapter;
import com.mphrx.fisike_physician.adapters.UserInviteRoleListadapter;
import com.mphrx.fisike_physician.adapters.UserInvitesAdapter;
import com.mphrx.fisike_physician.models.InviteIds;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.request.InviteIdRequest;
import com.mphrx.fisike_physician.network.request.InviteSearchRequest;
import com.mphrx.fisike_physician.network.request.PhysicianDetailRequest;
import com.mphrx.fisike_physician.network.request.SendInviteRequest;
import com.mphrx.fisike_physician.network.response.InviteIdResponse;
import com.mphrx.fisike_physician.network.response.SearchResponse;
import com.mphrx.fisike_physician.network.response.SendInviteResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InviteUsersActivity extends PhysicianHeaderActivity implements View.OnClickListener, UserInvitesAdapter.InviteUserListener {

    private Toolbar mToolbar;
    private ListView mListView;
    private FrameLayout mInviteNow;
    private AutoCompleteTextView mNameOrNumber;
    private ImageView mAddButton;
    private UserInvitesAdapter mAdapter;
    private SearchAdapter mSearchAdapter;

    private ProgressBar mProgressBar;
    private String mUserName;
    private String mNumber;
    private String mCountryCode;
    private final String countryCode = "+91";

    private List<User> mUserList;
    private InviteIds mInviteIds;
    private List<ChatContactMO> chatContactMOList;
    private ChatContactMO saveChatContactMo;

    private boolean mIsContactsReadPermissionGranted;
    DBAdapter mHelper;
    private boolean isPatient, isPhysician;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_invite_users);
        if (getIntent().hasExtra(TextConstants.IS_SECOND_LEVEL)) {
            setCurrentItem(2);
        }
        findViews();
        initViews();
        bindViews();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void findViews() {
        // mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mListView = (ListView) findViewById(R.id.lv_contacts);
        mInviteNow = (FrameLayout) findViewById(R.id.fl_button);
        mAddButton = (ImageView) findViewById(R.id.add_person_btn);
        mNameOrNumber = (AutoCompleteTextView) findViewById(R.id.name_or_number);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void initViews() {
        //setSupportActionBar(mToolbar);
        //setTitle(getString(R.string.invite_user_title));
      /*  if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        mInviteNow.setVisibility(View.GONE);
        mUserList = new ArrayList<User>();
        chatContactMOList = new ArrayList<ChatContactMO>();
        mAdapter = new UserInvitesAdapter(mUserList, this);
        mListView.setAdapter(mAdapter);
        mCountryCode = countryCode;
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        setToolbarTite(getString(R.string.invite_user_title));

    }

    private void bindViews() {
        mInviteNow.setOnClickListener(this);
        mAddButton.setOnClickListener(this);
        //mNameOrNumber.setAdapter(new SearchAdapter(new ArrayList<ChatContactMO>(0)));
        mNameOrNumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatContactMO mo = (ChatContactMO) mSearchAdapter.getItem(position);
                hideKeyboard();
                if (isUserAlreadyAddedToInviteList(mo.getPhoneNo()))
                    return;
                if (mo.isFisikeContact()) {
                    User user = new User();
                    user.name = mo.getDisplayName();
                    user.number = mo.getPhoneNo();
                    user.role = mo.getUserType().getName();
                    user.userId = mo.getId();
                    user.isFisikeUser = true;
                    mUserList.add(0, user);
                    mAdapter.updateList(mUserList);
                    //mAdapter.notifyDataSetChanged();
                    setInviteButtonVisibility();
                    saveChatContactMo = mo;
                    chatContactMOList.add(saveChatContactMo);
                    return;
                }
                mNumber = mo.getPhoneNo();
                mUserName = mo.getDisplayName();
                mNameOrNumber.setText(mUserName + "(" + mNumber + ")");
                addNewInvitation();
            }
        });

        mNameOrNumber.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {


                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (event == null) {
                            return true;
                        }
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            hideKeyboard();
                            return true;
                        }
                        return false;
                    }
                });

        mNameOrNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 2) {
                    mNameOrNumber.dismissDropDown();
                    mProgressBar.setVisibility(View.VISIBLE);
                    ThreadManager.getDefaultExecutorService().submit(new InviteSearchRequest(getTransactionId(), s.toString(), mIsContactsReadPermissionGranted, false));
                } else {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

        mNameOrNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (checkPermission(Manifest.permission.READ_CONTACTS, getResources().getString(R.string.We_need_your_contacts))) {
            mIsContactsReadPermissionGranted = true;
            mCountryCode = DeviceUtils.getCountryCode();
        } else {
            mIsContactsReadPermissionGranted = false;
            mCountryCode = countryCode; // Default
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fl_button:
                if (mUserList.size() > 0) {
                    showProgressDialog();
                    ThreadManager.getDefaultExecutorService().submit(new SendInviteRequest(mUserList, getTransactionId()));
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.Enter_at_least_one_user), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_person_btn:
                if (mProgressBar != null)
                    mProgressBar.setVisibility(View.GONE);
                hideKeyboard();
                addNewInvitation();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void addNewInvitation() {

        if (TextUtils.isEmpty(mNameOrNumber.getText()))
            return;

        if (TextUtils.isEmpty(mNumber) && TextUtils.isEmpty(mUserName)) {
            if (mNameOrNumber.getText().toString().matches("\\d+"))
                mNumber = mNameOrNumber.getText().toString();
            else
                mUserName = mNameOrNumber.getText().toString();
        }


        if (!TextUtils.isEmpty(mNumber)) {
            mNumber = TextPattern.getPhoneNumber(mNumber, mCountryCode);
            if (isUserAlreadyAddedToInviteList(mNumber)) {
                mUserName = "";
                mNumber = "";
                return;
            }
            if (mNumber != null && mNumber.equals(SharedPref.getMobileNumber())) {
                Toast.makeText(this, getResources().getString(R.string.You_cannot_invite_yourself), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        User user = new User();
        user.name = mUserName;
        user.number = mNumber;
        showInviteDialog(user, true);
        mUserName = "";
        mNumber = "";
        mNameOrNumber.setText("");
    }

//    private boolean isInviteDialogOpened;

    private Dialog showInviteDialog(final User user, final boolean addToList) {

//        isInviteDialogOpened = true;
        final Dialog dialog = new Dialog(this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invite);
        dialog.setCancelable(true);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        final CustomFontEditTextView etUsername = (CustomFontEditTextView) dialog.findViewById(R.id.et_username);
        final CustomFontEditTextView etPhone = (CustomFontEditTextView) dialog.findViewById(R.id.et_phone);
        etPhone.setMobileKeyListener();
        final Spinner spRole = (Spinner) dialog.findViewById(R.id.sp_role);
        spRole.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        hideKeyboard();
                        break;
                }
                return false;
            }
        });
        etPhone.setText(countryCode);
        Selection.setSelection(etPhone.getEditText().getText(), etPhone.getText().length());
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etPhone.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
             /*   etPhone.getEditText().removeTextChangedListener(this);
                String prefix = countryCode;
                if (!s.toString().startsWith(prefix)) {
                    String cleanString;
                    String deletedPrefix = prefix.substring(0, prefix.length() - 1);
                    if (s.toString().startsWith(deletedPrefix)) {
                        cleanString = s.toString().replaceAll(Pattern.quote(deletedPrefix), "");
                    } else {
                        cleanString = s.toString().replaceAll(Pattern.quote(prefix), "");
                    }
                    etPhone.setText(prefix + cleanString);
                    etPhone.setSelection(prefix.length());
                }
                etPhone.addTextChangedListener(this);*/

            }
        });
        List<String> inviteRoleValues = new ArrayList<String>(mInviteIds.values);
        inviteRoleValues.add(0, getString(R.string.select_role));
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_list_item_1, inviteRoleValues);
        UserInviteRoleListadapter adapter = new UserInviteRoleListadapter(this,
                R.layout.invite_user_role, inviteRoleValues);


        spRole.setAdapter(adapter);
        spRole.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 16) {
                    if (spRole != null)
                        spRole.setDropDownWidth(spRole.getWidth());
                }
            }
        }, 500);
        int defaultSelection = 0;
        for (int i = 0; i < mInviteIds.values.size(); i++) {
            if (mInviteIds.values.get(i).equals(user.role)) {
                defaultSelection = i;
                break;
            }
        }


        spRole.setSelection(defaultSelection);
        spRole.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        hideKeyboard();
                        break;
                }
                return false;
            }
        });
        etUsername.setText(user.name);
        if (user.number != null && user.number.contains(countryCode)) {
            // etPhone.setText(user.number.substring(countryCode.length()));
            etPhone.setText(user.number.trim().toString());
        } else {
            etPhone.setText(countryCode + "");
        }

        if (TextUtils.isEmpty(user.name))
            etUsername.requestFocus();
        else if (TextUtils.isEmpty(user.number))
            etPhone.requestFocus();

        TextView okButton = (TextView) dialog.findViewById(R.id.btn_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* sending invite here*/
                hideKeyboard();
                if (TextUtils.isEmpty(etUsername.getText().toString().trim())) {
//                    Toast.makeText(InviteUsersActivity.this, "Username cannot be empty", Toast.LENGTH_SHORT).show();
                    etUsername.setError(getString(R.string.error_username_cannot_be_empty));
                    return;
                } else {
                    etUsername.setErrorEnabled(false);
                }
                if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
                    etPhone.setError(getString(R.string.error_mobile_no_empty));
//                    Toast.makeText(InviteUsersActivity.this, "Mobile no. cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    etPhone.setErrorEnabled(false);
                }
                if (!etPhone.getText().toString().matches("^[+][0-9]{10,13}$") && etPhone.getText().toString().length() < 13) {
                    etPhone.setError(getString(R.string.error_invalid_mobile_no));
//                    Toast.makeText(InviteUsersActivity.this, "Invalid Mobile no.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    etPhone.setErrorEnabled(false);
                }
                if (etPhone.getText().toString().equals(SharedPref.getMobileNumber())) {
                    etPhone.setError(getString(R.string.error_you_cannot_invite_yourself));
//                    Toast.makeText(InviteUsersActivity.this, "You cannot invite yourself...!!!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    etPhone.setErrorEnabled(false);
                }

                if (spRole.getSelectedItem().toString().equals(getString(R.string.select_role))) {
                    Toast.makeText(InviteUsersActivity.this, getResources().getString(R.string.Role_cannot_be_empty), Toast.LENGTH_SHORT).show();
                    return;
                }
                user.roleId = (mInviteIds.mapping.get(spRole.getSelectedItem().toString()));
                user.number = (etPhone.getText().toString());
                user.role = (spRole.getSelectedItem().toString());
                user.name = (etUsername.getText().toString());
                user.organisationId = mInviteIds.orgId;
                user.inviteeRoleId = mInviteIds.mapping.get("Clinical");
                if (addToList && isUserAlreadyAddedToInviteList(user.number))
                    return;
                if (mAdapter != null) {
                    if (addToList)
                        mUserList.add(0, user);
                    mAdapter.updateList(mUserList);
                    //mAdapter.notifyDataSetChanged();
                    setInviteButtonVisibility();
                }
                dialog.dismiss();
            }
        });
        dialog.show();

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface d, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    d.dismiss();
                                    dialog.dismiss();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    d.dismiss();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(InviteUsersActivity.this);
                    builder.setMessage(getResources().getString(R.string.Do_you_want_to_cancel_invite)).setPositiveButton(getResources().getString(R.string.txt_yes), dialogClickListener)
                            .setNegativeButton(getResources().getString(R.string.txt_no), dialogClickListener).show();
                    return true;
                }
                return false;
            }
        });

        return dialog;
    }


    private boolean isUserAlreadyAddedToInviteList(String number) {
        if (mUserList != null) {
            for (User user : mUserList)
                if (user.number.equals(number)) {
                    Toast.makeText(InviteUsersActivity.this, getResources().getString(R.string.User_already_added), Toast.LENGTH_SHORT).show();
                    return true;
                }
        }
        return false;
    }


    private void showThanksDialog() {
        setCurrentItem(-1);
        if (mUserList != null)
            for (User user : mUserList)
                if (user.role.isEmpty() || user.role.equalsIgnoreCase(getString(R.string.string_user_role_default)))
                    return;


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_thanks);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = InviteUsersActivity.this.getResources().getDisplayMetrics().widthPixels - 100;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView msg1 = (TextView) dialog.findViewById(R.id.message1);
        TextView msg2 = (TextView) dialog.findViewById(R.id.message2);
        TextView okay = (TextView) dialog.findViewById(R.id.btn_okay);
        title.setText(getResources().getString(R.string.Thanks));
        msg1.setText(getResources().getString(R.string.Your_Invitation) + (mUserList.size() > 1 ? "s" : "") + getResources().getString(R.string.has_been_sent));
        msg2.setVisibility(View.GONE);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* send invitations here and dismiss dialog when finishing activity with animations */
                dialog.dismiss();
                try {
                    for (ChatContactMO saveChatContactMo : chatContactMOList) {
                        chatContactDBAdapter.getInstance(InviteUsersActivity.this).insert(saveChatContactMo);
                        if (saveChatContactMo.getPatientId() != 0) {
                            isPatient = true;
                        } else
                            isPhysician = true;
                        if (saveChatContactMo != null) {
                            if (isPhysician) {
                                String physicianId = Long.toString(saveChatContactMo.getPhysicianId());
                                ThreadManager.getDefaultExecutorService().submit(new PhysicianDetailRequest(getTransactionId(), physicianId, true, saveChatContactMo.getPersistenceKey()));
                            }
                        }
                    }
                } catch (Exception e) {
                }
                Intent intent = getIntent();
                Bundle b = new Bundle();
                b.putBoolean("Update", true);
                b.putBoolean("isPhysician", isPhysician);
                b.putBoolean("isPatient", isPatient);
                intent.putExtra("extras", b);
                setResult(TextConstants.INVITE_RESULT, intent);
                onBackPressed();
            }
        });
        dialog.show();
    }

    @Override
    public void onCLickRole(int index) {
        if (!mUserList.get(index).isFisikeUser)
            showInviteDialog(mUserList.get(index), false);
    }

    @Override
    public void onClickCancel(final int index) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        mUserList.remove(index);
                        setInviteButtonVisibility();
                        mAdapter.updateList(mUserList);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.Remove_this_contact)).setPositiveButton(getResources().getString(R.string.txt_yes), dialogClickListener)
                .setNegativeButton(getResources().getString(R.string.txt_no), dialogClickListener).show();

    }

    public void setInviteButtonVisibility() {
        if (mUserList.size() > 0) {
            mInviteNow.setVisibility(View.VISIBLE);
        } else {
            mInviteNow.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPermissionGranted(String permission) {
        mIsContactsReadPermissionGranted = true;
        mCountryCode = DeviceUtils.getCountryCode();
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        mIsContactsReadPermissionGranted = false;
        mCountryCode = countryCode;
    }

    @Override
    public void fetchInitialDataFromServer() {
        showProgressDialog(getResources().getString(R.string.Redirecting));
        ThreadManager.getDefaultExecutorService().submit(new InviteIdRequest(getTransactionId()));
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        try {
            InviteIdResponse response = new InviteIdResponse(new JSONObject(cacheResponse), getTransactionId());
            BusProvider.getInstance().post(response);
        } catch (JSONException e) {
            fetchInitialDataFromServer();
        }
    }

    @Subscribe
    public void onInviteIdResponse(InviteIdResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful()) {
            mInviteIds = response.getInviteIds();
            setCacheResponse(response.getResponse());
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.invite_menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            openActivity(PendingInvitationActivity.class, new Bundle(), false);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public synchronized void onSearchResponse(SearchResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;

        if (!response.getSearchString().equals(mNameOrNumber.getText().toString()))
            return;

        if (!response.isPartial())
            mProgressBar.setVisibility(View.GONE);

        //((SearchAdapter) mNameOrNumber.getAdapter()).updateUserList(response.getList());
        if (response.isSuccessful()) {
            if (mSearchAdapter == null) {
                mSearchAdapter = new SearchAdapter(response.getList());
                mNameOrNumber.setAdapter(mSearchAdapter);
            } else {
                mSearchAdapter.updateList(response.getList());
            }

            if (response.getList() != null && response.getList().size() > 0)
                mNameOrNumber.showDropDown();
            else if (!response.isPartial())
                Toast.makeText(this, R.string.no_contacts_found, Toast.LENGTH_SHORT).show();
        } else {
            if (mSearchAdapter != null) {
                mSearchAdapter.updateList(new ArrayList<ChatContactMO>(0));
            }
        }
    }

    @Subscribe
    public void onSendInviteResponse(SendInviteResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();
        if (response.isSuccessful()) {
            showThanksDialog();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            for (User user : response.getAllUserList())
                mUserList.remove(user);
            mAdapter.notifyDataSetChanged();
        }

    }
}
