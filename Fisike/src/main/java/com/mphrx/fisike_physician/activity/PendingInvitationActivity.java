package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.adapters.UserInvitesAdapter;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.network.request.PendingInviteListRequest;
import com.mphrx.fisike_physician.network.request.SendInviteRequest;
import com.mphrx.fisike_physician.network.response.PendingInviteListResponse;
import com.mphrx.fisike_physician.network.response.SendInviteResponse;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 19/11/15.
 */
public class PendingInvitationActivity extends PhysicianHeaderActivity {

    private Toolbar mToolbar;
    private ListView mListView;
    private TextView mEmptyView;
    private UserInvitesAdapter mAdapter;
    private List<User> mSelectedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.setContentView(R.layout.activity_listview);
        setCurrentItem(-1);
        showHideToolbar(false);
        findViews();
        initViews();
        bindViews();

    }


    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mListView = (ListView) findViewById(R.id.list_view);
        mEmptyView = (TextView) findViewById(R.id.empty_view);
    }

    private void initViews() {
        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.Pending_Invitations));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListView.setEmptyView(mEmptyView);
        mEmptyView.setVisibility(View.GONE);
        mEmptyView.setText(getResources().getString(R.string.No_Pending_Invite));

        mSelectedUser = new ArrayList<>();

    }

    private void bindViews() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = mAdapter.getItem(position);
                user.isSelected = !user.isSelected;
                if (user.isSelected)
                    mSelectedUser.add(user);
                else
                    mSelectedUser.remove(user);
                invalidateOptionsMenu();
                mAdapter.notifyDataSetChanged();
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        setCurrentItem(NavigationDrawerActivity.INVITE);
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void fetchInitialDataFromServer() {
        ThreadManager.getDefaultExecutorService().submit(new PendingInviteListRequest(getTransactionId()));
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        //Do Nothing
    }

    @Subscribe
    public void onPendingInviteResponse(PendingInviteListResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful()) {
            if (mAdapter == null) {
                mAdapter = new UserInvitesAdapter(response.getUserList(), null);
                mListView.setAdapter(mAdapter);
            }
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.resend_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_resend);
        if (mSelectedUser.isEmpty()) {
            item.setVisible(false);
        } else {
            item.setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_resend) {
            showProgressDialog();
            ThreadManager.getDefaultExecutorService().submit(new SendInviteRequest(mSelectedUser, getTransactionId()));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Subscribe
    public void onSendInviteResponse(SendInviteResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            for (User user : mSelectedUser)
                user.isSelected = false;
            mSelectedUser.clear();
            invalidateOptionsMenu();
            mAdapter.notifyDataSetChanged();
            Toast.makeText(this, getResources().getString(R.string.Resend_invitation_successfully), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        showHideToolbar(true);
        super.onDestroy();
    }
}
