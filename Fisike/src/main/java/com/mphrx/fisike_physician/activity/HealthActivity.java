package com.mphrx.fisike_physician.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.record_screen.adapter.ResultListAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;

/**
 * Created by brijesh on 17/11/15.
 */
public class HealthActivity extends com.mphrx.fisike.utils.BaseActivity implements ResultListAdapter.DownloadListner {

    private static final int DOWNLOAD_RESULT = 11122;
    Toolbar mToolbar;
    Fragment fragment;
    FrameLayout frameLayout;
    VitalsConfigMO vitalsConfigMO;
    private String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_patient_health, frameLayout);
        try {
            vitalsConfigMO = VitalsConfigDBAdapter.getInstance(this).fetchVitalsConfigMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        showHideToolbar(false);

        // setContentView(R.layout.activity_patient_health);
        SharedPreferences sharedPreferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SharedPreferencesConstant.ALL_UPLOADED_LOADED, false);
        editor.putInt(SharedPreferencesConstant.UPLOAD_TOTAL_COUNT, -1);
        editor.putString(SharedPreferencesConstant.UPLOAD_LAST_SYNC_TIME_SERVER, null);
        editor.commit();
        findViews();
        initViews();
        bindViews();

        fragment = new MyHealth();
        fragment.setArguments(getIntent().getExtras());

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content, fragment, "").commit();

        setCurrentItem(-1);

    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
    }


    private void initViews() {
        SharedPreferences sharedPreferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SharedPreferencesConstant.TOTAL_RECORD, 0);
        editor.commit();
        setSupportActionBar(mToolbar);
        setTitle(getIntent().getStringExtra("title"));

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HealthActivity.this.finish();
            }
        });

    }


    private void bindViews() {

    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    @Override
    public boolean onSupportNavigateUp() {

        this.finish();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //       showHideToolbar(true);
    }


    @Override
    protected void onNewIntent(Intent intent) {

        if (fragment instanceof MyHealth) {
            try {
                fragment.onActivityResult(1000, 1000, intent);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        super.onNewIntent(intent);
    }

    @Override
    public void itemClicked(String orderId, IconTextView imgOverFlow) {
        showPopupMenu(imgOverFlow, R.menu.menu_file_download, orderId);
    }

    public void showPopupMenu(View view, final int menuId, final String orderId) {
        this.orderId = orderId;
        final android.support.v7.widget.PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.report_download:
                        if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), DOWNLOAD_RESULT))
                            fileDownload(orderId);
                        break;
                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void fileDownload(String orderId) {
        String payload = "{\"constraints\":{\"diagnosticOrderId\":\"" + orderId + "\"}}";
        if (Utils.isNetworkAvailable(this)) {
            new DownloadRecordFileRequest(this, 0, VariableConstants.MIME_TYPE_FILE, MphRxUrl.getReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPermissionNotGranted() {
        super.onPermissionNotGranted();
    }

    @Override
    public void onPermissionGranted(String permission) {
        fileDownload(orderId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case DOWNLOAD_RESULT:
                    onPermissionGranted(permissions[0]);
                    break;
            }
        } else
            onPermissionNotGranted();
    }
}
