package com.mphrx.fisike_physician.activity;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

/**
 * Created by manohar on 14/12/15.
 */
public class ViewProfile extends PhysicianHeaderActivity {
    private TextView mobileNumber;
    private TextView email;
    private TextView dob;
    private TextView tvWeight;
    private TextView tvHeight;
    private TextView tvSex;
    ChatContactMO chatContactMO;
    private DBAdapter mHelper;
    private Toolbar mToolbar;
    private AsyncTask<Void, Void, Void> loadContactsRealName;
    private AsyncTask<Void, Void, Void> loadContactProfilePicAsyn;
    private CustomProfileVIewLayout profileLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.view_profile);

        findViews();

        String userId = getIntent().getStringExtra(VariableConstants.SENDER_ID);

        try {
            chatContactMO = chatContactDBAdapter.getInstance(this).fetchChatContactMo(ChatContactMO.getPersistenceKey(TextPattern.getUserId(userId)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
        bindView();

        findViewById(R.id.rl_password).setVisibility(View.GONE);
        findViewById(R.id.btn_logout).setVisibility(View.GONE);
        findViewById(R.id.ib_editNumber).setVisibility(View.GONE);
        findViewById(R.id.ib_editEmail).setVisibility(View.GONE);
        findViewById(R.id.ib_editPassword).setVisibility(View.GONE);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initView() {
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        setSupportActionBar(mToolbar);
        setTitle(chatContactMO.getDisplayName());
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_profile_screen);
        profileLayout = (CustomProfileVIewLayout) findViewById(R.id.iv_profile_picture);
        mobileNumber = (TextView) findViewById(R.id.tv_mobile_number);
        email = (TextView) findViewById(R.id.tv_email);
        dob = (TextView) findViewById(R.id.tv_dob);
        tvWeight = (TextView) findViewById(R.id.tv_weight);
        tvHeight = (TextView) findViewById(R.id.tv_height);
        tvSex = (TextView) findViewById(R.id.tv_gender);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);

    }

    void bindView() {

        String date = chatContactMO.getDob() != null ? (chatContactMO.getDob().contains(":") ? Utils.getFormattedDate(chatContactMO.getDob()) : chatContactMO.getDob()) : "";
        mobileNumber.setText(chatContactMO.getPhoneNo());
        email.setText(chatContactMO.getEmail());
        String WeigthUnit = "", HeightUnit = "";
        dob.setText(date);
        if (!TextUtils.isEmpty(chatContactMO.getWeight().getUnit()))
            WeigthUnit = chatContactMO.getWeight().getUnit();
        if (!TextUtils.isEmpty(chatContactMO.getHeight().getUnit()))
            HeightUnit = chatContactMO.getHeight().getUnit();
        tvWeight.setText(chatContactMO.getWeight().getValue() + " " + WeigthUnit);
        tvHeight.setText(chatContactMO.getHeight().getValue() + " " + HeightUnit);
        tvSex.setText(chatContactMO.getGender().equalsIgnoreCase("M") ? getResources().getString(R.string.Male) : chatContactMO.getGender().equalsIgnoreCase("F") ? getResources().getString(R.string.Female) : chatContactMO.getGender());
        profileLayout.setRectangleImage(true);
        profileLayout.setViewProfileImage(chatContactMO.getId());
    }


    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    @Override
    public void onMessengerServiceBind() {
        loadContactsRealName = new LoadContactsRealName(getMessengerService(), chatContactMO.getId(), this).execute();
    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    public void updateProfileDetails() {

        chatContactMO = getMessengerService().getChatContactMo(ChatContactMO.getPersistenceKey(chatContactMO.getId()));
        if (chatContactMO != null) {
            tvWeight.setText(chatContactMO.getWeight().getValue() + " " + chatContactMO.getWeight().getUnit());
            tvHeight.setText(chatContactMO.getHeight().getValue() + " " + chatContactMO.getHeight().getUnit());
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadContactsRealName != null && !loadContactsRealName.isCancelled()) {
            loadContactsRealName.cancel(true);
            loadContactsRealName = null;
        }
        if (loadContactProfilePicAsyn != null && !loadContactProfilePicAsyn.isCancelled()) {
            loadContactProfilePicAsyn.cancel(true);
            loadContactProfilePicAsyn = null;
        }
    }
}
