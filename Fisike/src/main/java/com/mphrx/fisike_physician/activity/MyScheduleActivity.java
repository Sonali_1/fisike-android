package com.mphrx.fisike_physician.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.fragment.MyScheduleFragment;

import careplan.fragments.QuestionaryWebViewFragment;

/**
 * Created by laxmansingh on 8/23/2017.
 */

public class MyScheduleActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    String title = "Task";
    public static WebView wvLoadURL;
    private MyScheduleFragment myScheduleFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.care_task_activity, frameLayout);

        title = "My Schedule";

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        initializeToolbar();

        myScheduleFragment = new MyScheduleFragment();
        myScheduleFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, myScheduleFragment, "myScheduleFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();


    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setOnClickListener(this);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_cross_ico));
        bt_toolbar_right.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(title);
        //toolbar_title.setText(getResources().getString(R.string.questionnary_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_toolbar_right:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}