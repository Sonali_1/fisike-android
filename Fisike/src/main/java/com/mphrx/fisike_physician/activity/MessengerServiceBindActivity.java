package com.mphrx.fisike_physician.activity;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike_physician.utils.AppLog;

/**
 * Created by brijesh on 05/11/15.
 */
public abstract class MessengerServiceBindActivity extends NetworkActivity {

    public MessengerService mService;
    protected boolean mBound;

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            AppLog.showInfo(getClass().getSimpleName(), "On Service Connected");
            MessengerService.MessengerBinder binder = (MessengerService.MessengerBinder) service;
            mService = binder.getService();
            mBound = true;
            onMessengerServiceBind();
        }

        public void onServiceDisconnected(ComponentName className) {
            AppLog.showInfo(getClass().getSimpleName(), "On Service Disconnected");
            onMessengerServiceUnbind();
            mBound = false;
            mService = null;
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        AppLog.showInfo(getClass().getSimpleName(), "Bind Messenger Service");
        bindMessengerService();
    }


    @Override
    protected void onStop() {
        AppLog.showInfo(getClass().getSimpleName(), "Unbind Messenger Service");
        unbindMessengerService();
        super.onStop();
    }

    public boolean checkIfMessengerServiceIsRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals(MyApplication.getMessengerServiceClass().getName())) {
                AppLog.showInfo(getClass().getSimpleName(), "MessengerService is already running in background.");
                return true;
            }
        }
        AppLog.showInfo(getClass().getSimpleName(), "MessengerService is not running.");
        return false;
    }

    public void bindMessengerService() {
        if (!mBound) {
            AppLog.showInfo(getClass().getSimpleName(), "Binding...!!!");
            Intent intent = new Intent(this, MyApplication.getMessengerServiceClass());
            if (!checkIfMessengerServiceIsRunning()) {
                MyApplication.getAppContext().startService(intent);
            }
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    public void unbindMessengerService() {
        if (mBound) {
            AppLog.showInfo(getClass().getSimpleName(), "Unbinding...!!!");
            unbindService(mConnection);
            mBound = false;
        }
    }

    public MessengerService getMessengerService() {
        return mService;
    }

    public abstract void onMessengerServiceBind();

    public abstract void onMessengerServiceUnbind();

    public MessengerService getmService() {
        return mService;
    }

    public void setmService(MessengerService mService) {
        this.mService = mService;
    }

    public boolean ismBound() {
        return mBound;
    }

    public void setmBound(boolean mBound) {
        this.mBound = mBound;
    }


}
