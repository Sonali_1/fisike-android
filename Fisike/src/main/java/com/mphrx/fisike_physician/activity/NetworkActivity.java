package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.text.TextUtils;

import com.mphrx.fisike.HeaderActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

/**
 * Created by brijesh on 15/10/15.
 */
public abstract class NetworkActivity extends HeaderActivity {



    private long mTransactionId;
    private String mCacheResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mTransactionId = System.currentTimeMillis();
            mCacheResponse = null;
        }
        else {
            mTransactionId = savedInstanceState.getLong(TextConstants.TRANSACTION_ID, System.currentTimeMillis());
            mCacheResponse = savedInstanceState.getString(TextConstants.CACHE_RESPONSE, null);
        }
        AppLog.showInfo(getClass().getSimpleName(), "Bus Registered");
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (TextUtils.isEmpty(mCacheResponse))
            fetchInitialDataFromServer();
        else
            fetchInitialDataFromCache(mCacheResponse);
    }

    @Override
    protected void onDestroy() {
        AppLog.showInfo(getClass().getSimpleName(), "Bus Unregistered");
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(TextConstants.TRANSACTION_ID, mTransactionId);
        if (!TextUtils.isEmpty(mCacheResponse))
            outState.putString(TextConstants.CACHE_RESPONSE, mCacheResponse);
    }

    public long getTransactionId() {
        return mTransactionId;
    }

    public void setTransactionId(long id){
        mTransactionId=id;
    }

    public void setCacheResponse(String response) {
        mCacheResponse = response;
    }

    public  void fetchInitialDataFromServer(){};

    public  void fetchInitialDataFromCache(String cacheResponse) {};

}
