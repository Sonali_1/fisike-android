package com.mphrx.fisike_physician.activity;

/**
 * Created by Aastha on 8/5/2016.
 */
public interface OnNavigationItemClickedListener {
    public void onIconClicked();
}
