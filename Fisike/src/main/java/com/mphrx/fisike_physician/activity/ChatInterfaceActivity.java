package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.mphrx.fisike.R;
import com.mphrx.fisike.fragments.GroupChatFragment;

/**
 * Created by brijesh on 13/10/15.
 */
/*OtherContactFragment will not be called*/ //comment by aastha
public class ChatInterfaceActivity extends com.mphrx.fisike.utils.BaseActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_holder);//activity_chat_interface

//        finish();

//        startActivity(new Intent(this,com.mphrx.fisike.HomeActivity.class));
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(R.id.content, GroupChatFragment.newInstance(1));
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commitAllowingStateLoss();
//        ft.add(R.id.content, ChatFragment.instantiate(this, "chat_frag")).commit();

//        findView();
//        initView();
//        bindView();

    }

    private void findView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.Chat));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void bindView() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
