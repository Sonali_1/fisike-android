package com.mphrx.fisike_physician.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike_physician.network.request.CheckEmailExistenceRequest;
import com.mphrx.fisike_physician.network.response.CheckEmailExistenceResponse;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

/**
 * Created by brijesh on 07/12/15.
 */
public class ChangeEmailActivity extends NetworkActivity {


    public static final int CHANGE_EMAIL_ACTIVITY = 101;

    private Toolbar mToolbar;
    private CustomFontEditTextView mNewEmailAddress;
    private Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);
        setResult(RESULT_CANCELED);

        findViews();
        initViews();
        bindViews();

    }


    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNewEmailAddress = (CustomFontEditTextView) findViewById(R.id.new_email_address);
        mSubmit = (Button) findViewById(R.id.btn_submit);
    }

    private void initViews() {
        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.title_Change_Email));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void bindViews() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doAction();
            }
        });
    }


    @Override
    public void fetchInitialDataFromServer() {
        // Do Nothing
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }

    private void doAction() {
        String email = mNewEmailAddress.getText().toString();
        if (TextUtils.isEmpty(email)) {
//            Toast.makeText(this, "Email cannot be empty", Toast.LENGTH_SHORT).show();
            mNewEmailAddress.setError(getString(R.string.error_email_cannot_empty));
            return;
        } else {
            mNewEmailAddress.setErrorEnabled(false);
        }

        if (!email.matches("^[A-Za-z0-9._+%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$")) {
            mNewEmailAddress.setError(getString(R.string.error_enter_valid_mail));
//            Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return;
        } else {
            mNewEmailAddress.setErrorEnabled(false);
        }

        showProgressDialog();
        ThreadManager.getDefaultExecutorService().submit(new CheckEmailExistenceRequest(email, getTransactionId()));

    }


    @Subscribe
    public void onCheckEmailExistenceResponse(CheckEmailExistenceResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful()) {
            setResult(RESULT_OK, new Intent().putExtra(TextConstants.CHANGE_EMAIL, mNewEmailAddress.getText().toString()));
            onBackPressed();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
