package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.Settings;

/**
 * Created by brijesh on 01/11/15.
 */
public class SettingActivity extends com.mphrx.fisike.utils.BaseActivity {

    private Toolbar mToolbar;
    private Fragment fragment;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_settings, frameLayout);
        findViews();
        initViews();
        bindViews();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setCurrentItem(NavigationDrawerActivity.SETTING);
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
    }

    private void initViews() {
        initToolbar();
        fragment = new Settings();
        openFragment(fragment);

    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.settings));
        if (BuildConfig.isPhysicianApp) {
            showHideToolbar(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
        } else {
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BuildConfig.isPatientApp)
                    SettingActivity.this.finish();
                else {
                    openDrawer();
                }

            }
        });
    }

    private void openFragment(Fragment fragment) {
        if (fragment != null) {
            this.fragment = fragment;
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_container_settings, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    }

    private void bindViews() {

    }

    @Override
    public void onBackPressed() {
        SettingActivity.this.finish();
    }


}