package com.mphrx.fisike_physician.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.RegistrationIntentService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.ContactPagerAdapter;
import com.mphrx.fisike_physician.fragment.ColleagueContactFragment;
import com.mphrx.fisike_physician.fragment.PatientContactFragment;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.views.SlidingTabLayout;

/**
 * Created by brijesh on 20/10/15.
 */
public class ContactActivity extends PhysicianHeaderActivity implements View.OnClickListener {

    private ContactPagerAdapter adapter;
    private ViewPager mPager;
    private SlidingTabLayout mTabLayout;
    private boolean isStartService;

    @Override
    protected void onRestart() {
        super.onRestart();
        isStartService = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_physician_base);
        SharedPref.setUserLogout(false);
        findView();
        initView();
        bindView();
       // checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, TextConstants.STORAGE, UploadsFragment.CAMERA_INTENT);
        AppLog.d("INSTANCE", this.toString());
    }

    private void findView() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout = (SlidingTabLayout) findViewById(R.id.tab_layout);

    }

    private void initView() {

        mPager.setOffscreenPageLimit(3);
        adapter = new ContactPagerAdapter(getFragmentManager());
        mPager.setAdapter(adapter);
        mTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tab_txt);
        mTabLayout.setViewPager(mPager);
        mPager.setCurrentItem(0);
/*
        initDrawer();
*/
    }

    private void bindView() {
        // mProfileContainer.setOnClickListener(this);
     /*    mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if (SCREEN_STATE == Screen_State.CONTACT) {
                    // Do nothing
                } else if (SCREEN_STATE == Screen_State.INVITE) {
                    Bundle bundle = new Bundle();
                    openActivity(InviteUsersActivity.class, bundle, false);
                } else if (SCREEN_STATE == Screen_State.PROFILE) {
                    openActivity(com.mphrx.fisike.UserProfileActivity.class, null, false);
                } else if (SCREEN_STATE == Screen_State.MESSAGE) {
                    Bundle bundle = new Bundle();
                    openActivity(MessageListActivity.class, bundle, false);
                } else if (SCREEN_STATE == Screen_State.SETTING) {
                    openActivity(SettingActivity.class, null, false);
                } else if (SCREEN_STATE == Screen_State.NOTIFICATION) {
                    openActivity(AppointmentActivity.class, null, false);
                } else if (SCREEN_STATE == Screen_State.LOGOUT) {
                    DialogUtils.showAlertDialog(ContactActivity.this, "Logout", "Are you sure you want to logout?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == Dialog.BUTTON_POSITIVE) {
                                SharedPref.setAccessToken(null);
                                SharedPref.setUserLogout(true);
                                getMessengerService().logoutUser(ContactActivity.this);
                                //    launchLoginScreen();
                                SharedPref.setUserStatus("");
                                SharedPref.setIsUserStatus(false);
                            } else {
                                dialog.dismiss();
                            }
                        }
                    });


                }
                SCREEN_STATE = Screen_State.CONTACT;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });*/

       /* findViewById(R.id.tos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactActivity.this, SettingWebView.class);
                i.putExtra(VariableConstants.TITLE_BAR, getResources().getString(R.string.terms_of_use));
                i.putExtra(VariableConstants.WEB_URL, MphRxUrl.getTosUrl());
                if (i != null && Utils.showDialogForNoNetwork(ContactActivity.this, false))
                    startActivity(i);
            }
        });
        findViewById(R.id.faq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactActivity.this, SettingWebView.class);
                i.putExtra(VariableConstants.TITLE_BAR, getResources().getString(R.string.faq));
                i.putExtra(VariableConstants.WEB_URL, MphRxUrl.getFAQUrl());
                if (i != null && Utils.showDialogForNoNetwork(ContactActivity.this, false))
                    startActivity(i);
            }
        });*/
    }


    public boolean onOptionsItemSelected(MenuItem item) {
      /*  if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }*/
        if (item.getItemId() == R.id.action_search) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(this, SearchUserActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!BuildConfig.isSearchPatient) {
            if (!Utils.checkToDisplayPINScreen(SettingManager.getInstance().getUserMO()) && SharedPref.getIsToShowAlternateContactDialog())
                showDialogToAddAlternateContact();

            if (isGcmRegistrationRequired()) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }
            setCurrentItem(NavigationDrawerActivity.MYCONTACTS);
        }
    }

    @Override
    public void onClick(View v) {
     /*   switch (v.getId()) {
            case R.id.profile_container:
                SCREEN_STATE = Screen_State.PROFILE;
                mDrawerLayout.closeDrawers();
                break;
            case R.id.drawer_contact:
                SCREEN_STATE = Screen_State.CONTACT;
                mDrawerLayout.closeDrawers();
                break;
            case R.id.drawer_message:
                SCREEN_STATE = Screen_State.MESSAGE;
                mDrawerLayout.closeDrawers();
                break;
            case R.id.drawer_invite:
                SCREEN_STATE = Screen_State.INVITE;
                mDrawerLayout.closeDrawers();
                break;
            case R.id.drawer_appointment:
                SCREEN_STATE = Screen_State.NOTIFICATION;
                mDrawerLayout.closeDrawers();
                break;
//            case R.id.drawer_notification:
//                SCREEN_STATE = Screen_State.NOTIFICATION;
//                mDrawerLayout.closeDrawers();
//                break;
            case R.id.drawer_setting:
                SCREEN_STATE = Screen_State.SETTING;
                mDrawerLayout.closeDrawers();
                break;
            case R.id.drawer_logout:
                SCREEN_STATE = Screen_State.LOGOUT;
                mDrawerLayout.closeDrawers();
                break;
            default:
                *//* Do Nothing *//*
                break;
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (!BuildConfig.isChatEnabled)
            return true;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMessengerServiceBind() {
        UserMO userMO = SettingManager.getInstance().getUserMO();

        if (userMO != null) {
            String userId = userMO.getId() + "";
            if (!userId.equals("")) {
                if (!chatContactDBAdapter.getInstance(this).isChatContactMoAvailable(userId)) {
                    if (getMessengerService() != null) {
                        getMessengerService().getGroupChatUsersDetails(userId);
                    }
                }
            }
            getMessengerService().changeStatus(TextConstants.STATUS_AVAILABLE, SettingManager.getInstance().getUserMO().getStatusMessage());

        }

        if (!isStartService && getMessengerService() != null && !SharedPref.getIsAgreementsUpdated()) {
            mService.initiateTimers();
        } else if (mService != null && SharedPref.getIsAgreementsUpdated()) {
            mService.startGetProfileUpdatesTimer();
        }
    }

    @Override
    public void onMessengerServiceUnbind() {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == TextConstants.INVITE_RESULT) {

            //resetting current item to MyContacts


            Bundle bundle = data.getBundleExtra("extras");
            boolean isPatient = bundle.getBoolean("isPatient");
            boolean isPhysician = bundle.getBoolean("isPhysician");
            if (mPager != null && adapter != null &&
                    adapter.getCount() > 1 && adapter.getmPageReferenceMap().get(0) instanceof PatientContactFragment && isPatient) {
                ((PatientContactFragment) adapter.getmPageReferenceMap().get(0)).getContactList(true);
            }
            if ((mPager != null && adapter != null &&
                    adapter.getCount() > 1 && adapter.getmPageReferenceMap().get(1) instanceof ColleagueContactFragment && isPhysician)) {
                ((ColleagueContactFragment) adapter.getmPageReferenceMap().get(1)).getContactList(true);
            }
        }
    }


}
