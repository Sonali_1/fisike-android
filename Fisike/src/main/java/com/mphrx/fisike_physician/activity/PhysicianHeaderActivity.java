package com.mphrx.fisike_physician.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.NavigationDrawerAdapter;
import com.mphrx.fisike.add_alternate_contact.AddAlternateContact;
import com.mphrx.fisike.background.UploadProfilePic;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;

/**
 * Created by Aastha on 8/5/2016.
 */
public class PhysicianHeaderActivity extends NavigationDrawerActivity implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    enum Screen_State {
        SEARCH_PATIENT, CONTACT, MESSAGE, NOTIFICATION, INVITE, SETTING, LOGOUT, PROFILE
    }

    private static final long EXITTIME = 2000;
    private Screen_State SCREEN_STATE = Screen_State.SEARCH_PATIENT;
    private Toolbar mToolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView leftDrawerList;
    private static Toast toast;
    private boolean doubleBackToExitPressedOnce;

    private ImageView imgUserPic;
    private CustomFontTextView txtUserName, txtPhoneNo;
    private RelativeLayout rlUserDetails;
    private boolean mUploadingImage;

    FrameLayout frameLayout;

    private android.support.v7.app.AlertDialog alertDialogAlternateContact;
    private static final int ADD_ALTERNATE_CONTACT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_physician_header);
        setNavDrawerItems();
        setNavigationDrawerAdapter();
        findView();
        initView();
        toast = Toast.makeText(this, getResources().getString(R.string.exit_confirmation_message), Toast.LENGTH_SHORT);
        Utils.setCurrentActivityContext(this);

        if (getMessengerService() != null && ConfigManager.getInstance().getConfig().getAutoLockMinute() == 0)
            getMessengerService().stopLogoutTimer();
    }

    @Override
    public void onBackPressed() {
        if (!BuildConfig.isSearchPatient && (PhysicianHeaderActivity.this instanceof ContactActivity)) {
            handleBackPress();
        } else if (BuildConfig.isSearchPatient && (PhysicianHeaderActivity.this instanceof SearchPatientFilterActivity)) {
            handleBackPress();
        } else {
            super.onBackPressed();
        }
    }

    private void handleBackPress() {
        if (doubleBackToExitPressedOnce) {
            if (toast != null) {
                toast.cancel();
            }
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, EXITTIME);
    }

    private void findView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
        leftDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        leftDrawerList.setOnItemClickListener(this);

        leftDrawerList.setAdapter(navigationDrawerAdapter);
        rlUserDetails = (RelativeLayout) findViewById(R.id.rl_userDetails);
        imgUserPic = ((ImageView) findViewById(R.id.imgUserPic));
        txtUserName = ((CustomFontTextView) findViewById(R.id.txtUserName));

        Bitmap ProfilePicIcon = BitmapFactory.decodeResource(getResources(), R.drawable.default_profile_pic);
        imgUserPic.setImageBitmap(ProfilePicIcon);
        imgUserPic.setVisibility(View.VISIBLE);
        txtPhoneNo = (CustomFontTextView) findViewById(R.id.tv_email_id);
        rlUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity(com.mphrx.fisike.UserProfileActivity.class, null, false);
            }
        });
    }

    public void setContentView(int resId) {
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(resId, frameLayout);
    }

    public void showDialogToAddAlternateContact() {

        SharedPref.setLastDisplayedAlternateContactDate(System.currentTimeMillis());
        //SharedPref.setDiffrenceCalculatedDate(0 );

        if (alertDialogAlternateContact == null) {
            alertDialogAlternateContact = DialogUtils.returnAlertDialog(this, getString(R.string.alternate_contact_title), getString(R.string.alternate_contact_message), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        dialog.dismiss();
                        startActivityForResult(new Intent(PhysicianHeaderActivity.this, AddAlternateContact.class).putExtra(VariableConstants.START_INSIDE_ACTIVITY, true), ADD_ALTERNATE_CONTACT);

                    } else {
                        dialog.dismiss();


                    }
                    Utils.checkAlternateContactDetails(SettingManager.getInstance().getUserMO().getAlternateContact());
                }
            });


        }
        if (alertDialogAlternateContact != null && !alertDialogAlternateContact.isShowing())
            alertDialogAlternateContact.show();
    }

    @Override
    public void onMessengerServiceBind() {

    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.home_contacts, R.string.home_contacts) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                Utils.hideKeyboard(PhysicianHeaderActivity.this);
                navigationDrawerAdapter.notifyDataSetChanged();
                super.onDrawerOpened(drawerView);

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        if (BuildConfig.isSearchPatient) {
            setToolbarTite(getString(R.string.patient_list));
        } else {
            setToolbarTite(getString(R.string.home_contacts));
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initDrawer();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        txtUserName.setMovementMethod(new ScrollingMovementMethod());
//        setHamburgerIcon();
    }


    public void openDrawer() {
        Utils.hideKeyboard(this);
        int drawerLockMode = drawerLayout.getDrawerLockMode(GravityCompat.START);
        if (drawerLayout.isDrawerVisible(GravityCompat.START)
                && (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_OPEN)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUserProfilePic(imgUserPic);
    }


    private void setUserProfilePic(ImageView imgUserPic) {
        Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());
        if (bitmap != null) {
            imgUserPic.setImageBitmap(bitmap);
        } else {
            Bitmap ProfilePicIcon = BitmapFactory.decodeResource(getResources(), R.drawable.default_profile_pic);
            imgUserPic.setImageBitmap(ProfilePicIcon);
        }
    }

    public void setToolbarTite(String title) {
        setTitle(title);
    }

    @Override
    public void setNavigationDrawerAdapter() {
        navigationDrawerAdapter = new NavigationDrawerAdapter(getApplicationContext(), navDrawerItems);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        showHideToolbar(true);
        if (BuildConfig.isPhysicianApp)

            onItemClickPhysician(parent, view, position, id, drawerLayout);
        else
            onItemClickPatient(parent, view, position, id, drawerLayout, null, -1, this);
    }

    private void setHamburgerIcon() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hamburger);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void setNavigationBackIcon() {
        mToolbar.setNavigationIcon(R.drawable.hamburger);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        showDrawer(drawerLayout);
        updateUserDetail();
        if (SharedPref.getIsToSelectItem())
            selectItemPhysician(SharedPref.getPositionToSelect());
        if (getMessengerService() != null && Utils.checkIsToLogoutUser())
            getMessengerService().logoutUser(this);

    }

    public boolean isGcmRegistrationRequired() {

        if (!SharedPref.isGCMRegistrationOk())
            return true;

        if (!SharedPref.getAppVersion().equals(DeviceUtils.getAppVersion()))
            return true;

        if (!SharedPref.getOSVersion().equals(DeviceUtils.getOSVersion()))
            return true;

        return false;

    }


    private void updateUserDetail() {
        if (SettingManager.getInstance().getUserMO() == null)
            return;
        StringBuilder userName = new StringBuilder();
        userName.append(SettingManager.getInstance().getUserMO().getFirstName());
        userName.append(" ");
        userName.append(SettingManager.getInstance().getUserMO().getLastName());
        txtUserName.setText(userName);
        txtPhoneNo.setText(Utils.getUserName());
        txtPhoneNo.setVisibility(View.VISIBLE);
        Bitmap bm = UserProfilePic.loadImageFromStorage(this);
        if (bm != null) {
            imgUserPic.setImageBitmap(bm);
        }


        if (SharedPref.needToUploadImage() && !mUploadingImage) {
            mUploadingImage = true;
            new UploadProfilePic(this, bm).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void onExceptionOnUploadImage() {
        mUploadingImage = false;
    }

    public void onSuccessfulUploadImage() {
        mUploadingImage = false;
        SharedPref.setNeedToUploadImage(false);
    }

    //if true set toolbar at the top else hide from header
    public void showHideToolbar(boolean value) {
        if (value)
            mToolbar.setVisibility(View.VISIBLE);
        else
            mToolbar.setVisibility(View.GONE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        try {
            unbindMessengerService();
        } catch (Exception e) {

        }
        if (key.equals(SharedPref.LANGUAGE_SELECTED)) {
            recreate();
        }


    }

}
