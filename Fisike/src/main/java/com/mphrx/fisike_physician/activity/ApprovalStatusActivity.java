package com.mphrx.fisike_physician.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.request.SignupStatusRequest;
import com.mphrx.fisike_physician.network.response.SignupStatusResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import login.activity.LoginActivity;

/**
 * Created by brijesh on 06/10/15.
 */
public class ApprovalStatusActivity extends NetworkActivity {

    private CustomFontButton mEmailNow;
    private CustomFontTextView mNoThanks, emailInfo;
    private CustomFontTextView mStatus;
    private boolean mIsRejected;
    private IconTextView imgHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_status);
        findViews();
        initViews();
        bindViews();


    }

    private void findViews() {
        mEmailNow = (CustomFontButton) findViewById(R.id.btn_email_now);
        mNoThanks = (CustomFontTextView) findViewById(R.id.no_thanks_btn);
        mStatus = (CustomFontTextView) findViewById(R.id.lbl_status);
        emailInfo = (CustomFontTextView) findViewById(R.id.email_info);
        imgHeader = (IconTextView) findViewById(R.id.img_header);
    }

    private void initViews() {

        emailInfo.setText(getString(R.string.string_status_info_with_link, CustomizedTextConstants.EMAIL_TO));

        if (BuildConfig.isIcomoonForSplashEnabled) {
            imgHeader.setText(R.string.fa_fisike_spec_icon);
            imgHeader.setTextSize(70f);
        } else {
            imgHeader.setBackgroundResource(R.drawable.fisike_icon);
            imgHeader.setText("");
            imgHeader.setTextSize(0);
        }
    }

    private void bindViews() {
        mEmailNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMailIntent();
            }
        });
        mNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsRejected) {
                    SharedPref.setOTPVerifyStatus(false);
                    openActivity(OtpActivity.class, null, true);
                } else {
                    moveTaskToBack(true);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void openMailIntent() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", CustomizedTextConstants.EMAIL_TO, null));
        emailIntent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(this, SettingManager.getInstance().getUserMO()));
        startActivity(Intent.createChooser(emailIntent, "Send email"));
    }

    @Override
    public void fetchInitialDataFromServer() {
        showProgressDialog();
        ThreadManager.getDefaultExecutorService().submit(new SignupStatusRequest(getTransactionId()));
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }


    private void launchNextScreenBasedOnHttpStatusCode(String statusCode) {
        switch (statusCode) {
            case TextConstants.HTTP_STATUS_423:
                SharedPref.setUserType(SharedPref.UserType.REGISTERED);
                SharedPref.setUserLogout(false);
                openActivity(LoginActivity.class, null, true);
                break;
            case TextConstants.HTTP_STATUS_428:
                mStatus.setText(getResources().getString(R.string.Your_profile_has_been_rejected));
                mIsRejected = true;
                break;
        }
    }


    @Subscribe
    public void onSignupStatusResponse(SignupStatusResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;
        dismissProgressDialog();

        if (response.isSuccessful()) {
            launchNextScreenBasedOnHttpStatusCode(response.getHttpStatusCode());
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
