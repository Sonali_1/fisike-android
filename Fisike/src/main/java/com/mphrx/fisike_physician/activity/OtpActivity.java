package com.mphrx.fisike_physician.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.VolleyRequest.OTPRequest.VerifyOTPRequest;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.verifyOTPResponse;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.ContactAdminActivity;
import com.mphrx.fisike.createUserProfile.CreatePhysicianProfile;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.gson.request.CheckPhoneNumberExistGson;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.OTPInitializingFragment;
import com.mphrx.fisike_physician.fragment.OTPVerificationFragment;
import com.mphrx.fisike_physician.network.request.ChangeNumberRequest;
import com.mphrx.fisike_physician.network.request.CheckPhoneNoExistRequest;
import com.mphrx.fisike_physician.network.request.CheckPhoneNoExistResponse;
import com.mphrx.fisike_physician.network.request.OTPGetRequest;
import com.mphrx.fisike_physician.network.request.VerifyOTPandUpdatePhoneNoRequest;
import com.mphrx.fisike_physician.network.response.ChangeNumberResponse;
import com.mphrx.fisike_physician.network.response.OTPGetStatusResponse;
import com.mphrx.fisike_physician.network.response.OTPVerifyResponse;
import com.mphrx.fisike_physician.network.response.VerifyOTPandUpdatePhoneNoResponse;
import com.mphrx.fisike_physician.services.SMSReceiver;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import login.activity.LoginActivity;

/**
 * Created by brijesh on 31/10/15.
 */
public class OtpActivity extends NetworkActivity implements SMSReceiver.SMSReceiverListener,
        OTPVerificationFragment.OnGetTransactionIdListener,
        OTPInitializingFragment.OnGetTransactionIdListener {

    public static final String CHANGE_NUMBER = "CHANGE_NUMBER";
    public static final String LOGIN_FLOW = "LOGIN_FLOW";
    public static final String LAUNCH_MODE = "LAUNCH_MODE";
    public static final String PREFILL_NUM = "PREFILL_NUM";
    public static final String BACK_PRESSED = "BACK_PRESSED";


    private SMSReceiver mReceiver;
    private boolean mIsUserExisted;
    private boolean mIsEmailVerified;
    private String mSignUpStatus;
    private String mEmailAddress, mEmail;
    private String mLaunchMode;
    private boolean mPrefillNumber;
    private String mPhoneNumber, countrycode;
    private String otpCode;
    private UserMO mUserInfo;
    Bundle changeMobileExtra;
    ImageView ivFisikeIcon;
    private long mTransactionId;
    private boolean isDisableAccunt, isResendOtpCode = false;
    public static final String OTP_INITIALIZATION = "OTP_INITIALIZATION";
    public static final String OTP_VERIFICATION = "OTP_VERIFICATION";
    private boolean isInitializationFragment;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private static boolean mIsAdminCreatedPasswordNeverChanged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_physician_otp);
        findView();
        initializeVariableFromActivityIntent();
        addFragment(OTPInitializingFragment.getInstance(changeMobileExtra), R.id.otp_container, false);
    }

    private void findView() {
        ivFisikeIcon = (ImageView) findViewById(R.id.iv_fisike_icon);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
    }

    private void initializeVariableFromActivityIntent() {
        if (getIntent().getExtras() != null) {
            mLaunchMode = getIntent().getExtras().getString(LAUNCH_MODE, LOGIN_FLOW);
            mPrefillNumber = getIntent().getExtras().getBoolean(PREFILL_NUM, false);
            isDisableAccunt = getIntent().getBooleanExtra(TextConstants.DISABLE_USER, false);
        } else {
            mLaunchMode = LOGIN_FLOW;
            mPrefillNumber = false;
            isDisableAccunt = false;
        }

        changeMobileExtra = getIntent().getExtras();
        if (changeMobileExtra == null) {
            changeMobileExtra = new Bundle();
        }
        changeMobileExtra.putString(LAUNCH_MODE, mLaunchMode);
        changeMobileExtra.putBoolean(PREFILL_NUM, mPrefillNumber);
        changeMobileExtra.putBoolean(TextConstants.DISABLE_USER, isDisableAccunt);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //redirection of signUp flow with disable existing account
        if (isDisableAccunt) {
            showProgressDialog();
            String deviceUid = SharedPref.getDeviceUid();
            if(deviceUid == null){
                SharedPref.setDeviceUid(Utils.getDeviceUUid(this));
            }
            String key = Utils.getUserName();
            ThreadManager.getDefaultExecutorService().submit(new OTPGetRequest(key, getTransactionId(), Utils.getDeviceUUid(MyApplication.getAppContext()), isDisableAccunt));

            setIdInActivity(mTransactionId);
        }
    }

    @Override
    public void onBackPressed() {
        handleBackPress();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Subscribe
    public void onOTPVerifyResponse(OTPVerifyResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful()) {
            registerSMSReceiver();
            mIsUserExisted = response.isUserExisted();
            mIsEmailVerified = response.isEmailVerified();
            mSignUpStatus = response.getSignUpStatus();
            mEmailAddress = response.getEmailAddress();
            mPhoneNumber = response.getPhoneNumber();
            mUserInfo = response.getUserInfo();
            addOrInitOTPFragment();

        } else {

            if (response.getHttpStatusCode() != null && response.getHttpStatusCode().equals(TextConstants.HTTP_STATUS_426)) {
                registerSMSReceiver();
                parseOtpSendResponseFromError(response.getResponseBody());
                addOrInitOTPFragment();
            } else if (response.getHttpStatusCode() != null && response.getHttpStatusCode().equals(TextConstants.HTTP_STATUS_423)) {
                SharedPref.setUserType(SharedPref.UserType.REGISTERED);
                parseOtpSendResponseFromError(response.getResponseBody());
                launchNextScreen();

            } else {
                Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
                changePhoneNumber();
            }
        }
    }


    private void parseOtpSendResponseFromError(String responseData) {

        JSONObject response = null;
        try {
            response = new JSONObject(responseData);
            mIsUserExisted = response.getBoolean("userExist");
            mIsEmailVerified = response.getBoolean("verified");
            mSignUpStatus = response.optString("signUpStatus");
            mEmailAddress = response.optString("email");

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.getString("user"), UserMO.class);
            if (jsonToObjectMapper instanceof UserMO)
                mUserInfo = (UserMO) jsonToObjectMapper;

            mPhoneNumber = SharedPref.getMobileNumber();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void sendOtpSuccessfully() {
        if (SharedPref.getIsMobileEnabled())
            registerSMSReceiver();
        addOrInitOTPFragment();
        if (isResendOtpCode) {
            isResendOtpCode = false;
            Toast.makeText(this, getResources().getString(R.string.sent_4_digit_code_again), Toast.LENGTH_SHORT).show();
        }
    }


    private void handleResponse(String httpStatusCode, String response) {
        switch (httpStatusCode)
        {
            case TextConstants.HTTP_STATUS_424:
            case TextConstants.HTTP_STATUS_423:
            case TextConstants.HTTP_STATUS_425:
                boolean isMobileSignup=true;
                if (SharedPref.getIsMobileEnabled()) {
                    mPhoneNumber = SharedPref.getMobileNumber();
                    countrycode=SharedPref.getCountryCode();
                    isMobileSignup = true;
                }
                if (!SharedPref.getIsMobileEnabled())                       //minerva
                {
                    mEmailAddress = SharedPref.getEmailAddress();
                    isMobileSignup = false;
                }
                SharedPref.setMobileEnabled(isMobileSignup);
                parsePartialRegisterUserResponse(response, true);
                SharedPref.setUserExist(mIsUserExisted);
                //SharedPref.setAdminCreatedPasswordNeverChanged(mIsAdminCreatedPasswordNeverChanged);
                if (BuildConfig.isPhysicianApp) {
                    SharedPref.setUserType(SharedPref.UserType.REGISTERED);
                }
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case TextConstants.HTTP_STATUS_426:
                parsePartialRegisterUserResponse(response, false);
                launchNextScreen();
                break;
            default:
                Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void parsePartialRegisterUserResponse(String response, boolean isAcceptedSigupStatus) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            mIsUserExisted = jsonObject.getBoolean("userExist");
            try {
                mIsAdminCreatedPasswordNeverChanged = jsonObject.getBoolean("adminCreatedPasswordNeverChanged");
            } catch (Exception e) {
                mIsAdminCreatedPasswordNeverChanged = false;
            }
            SharedPref.setAdminCreatedPasswordNeverChanged(mIsAdminCreatedPasswordNeverChanged);
            Log.d("admin created parse", mIsAdminCreatedPasswordNeverChanged + "");
            if (!isAcceptedSigupStatus)
                mSignUpStatus = jsonObject.getString("msg");
            else
                mSignUpStatus = "ACCEPTED";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onOTPGetStatusResponse(OTPGetStatusResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            if (isDisableAccunt || !response.isUserExisted()) {
                if(SharedPref.getIsSelfSignUpEnabled() && !SharedPref.getIsUserNameEnabled())
                    sendOtpSuccessfully();
                else {
                    openActivity(ContactAdminActivity.class, null, true);
                }
            }
        } else if (response != null && response.getHttpStatusCode() != null) {
            handleResponse(response.getHttpStatusCode(), response.getResponseBody());
        } else {
            Toast.makeText(this, response.getmMessage(), Toast.LENGTH_SHORT).show();
            changePhoneNumber();
        }
    }

    @Override
    public long getTransactionId() {
        if (this instanceof NetworkActivity) {
            mTransactionId = System.currentTimeMillis();
            return mTransactionId;
        }
        return 0;
    }


    public void changePhoneNumber() {
        unregisterSMSReceiver();
        if (!isFragmentVisible(OTPInitializingFragment.class)) {

            changeMobileExtra.putBoolean(PREFILL_NUM, true);
            SharedPref.setDisableAccount(false);

            replaceFragment(OTPInitializingFragment.getInstance(changeMobileExtra), R.id.otp_container, false);
            setToolbar(OTP_INITIALIZATION);

        }
    }

    @Override
    public void onSMSReceived(String otp) {
        unregisterSMSReceiver();
        setOTPText(otp);
        navigateUser(otp, true);
    }

    @Override
    protected void onDestroy() {
        unregisterSMSReceiver();
        super.onDestroy();
    }

    private synchronized void unregisterSMSReceiver() {
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
        mReceiver = null;
    }

    public synchronized void registerSMSReceiver() {
        if (mReceiver == null) {
            mReceiver = new SMSReceiver(this);
            registerReceiver(mReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        }
    }


    public synchronized void navigateUser(String otp, boolean auto) {

       /* if(BuildConfig.isPatientApp)
        {*/
        if (Utils.showDialogForNoNetwork(this, false)) {
            showProgressDialog();
            otpCode = otp;
            mTransactionId = System.currentTimeMillis();
            setIdInActivity(mTransactionId);
            if (mLaunchMode.equals(CHANGE_NUMBER))
                ThreadManager.getDefaultExecutorService().submit(new VerifyOTPandUpdatePhoneNoRequest(mPhoneNumber, otpCode, mTransactionId, SharedPref.isDisableAccount()));
            else
                ThreadManager.getDefaultExecutorService().submit(new VerifyOTPRequest(mPhoneNumber, otp, mTransactionId, SharedPref.getDeviceUid(),SharedPref.getCountryCode()));
        }
    }



    public void addOrInitOTPFragment() {
        if (SharedPref.getIsMobileEnabled()) {
            mPhoneNumber = SharedPref.getMobileNumber();
            SharedPref.setMobileNumber(mPhoneNumber);
            countrycode = SharedPref.getCountryCode();
            SharedPref.setCountryCode(countrycode);
        } else if (SharedPref.getIsUserNameEnabled()) {
            mPhoneNumber = SharedPref.getLoginUserName();
            SharedPref.setLoginUserName(mPhoneNumber);
        } else if (!SharedPref.getIsMobileEnabled()) {
            mPhoneNumber = SharedPref.getEmailAddress();
            SharedPref.setEmailAddress(mPhoneNumber);
        }
        Bundle extras = new Bundle();
        extras.putBoolean(TextConstants.DISABLE_USER, isDisableAccunt);
        extras.putString(LAUNCH_MODE, mLaunchMode);
        OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
        if (fragment == null || !fragment.isVisible()) {
            replaceFragment(OTPVerificationFragment.getInstance(extras), R.id.otp_container, false);
        } else {
            fragment.initView();
            if (SharedPref.getIsMobileEnabled())
                fragment.startTimer();
        }
    }

    public void onSMSAutoDetectionTimeComplete() {
        unregisterSMSReceiver();
    }

    public void launchNextScreen() {
        boolean isMobileSignup = false;
        if (SharedPref.getIsMobileEnabled()) {
            mPhoneNumber = SharedPref.getMobileNumber();
            countrycode = SharedPref.getCountryCode();
            isMobileSignup = true;
        }
        if (!SharedPref.getIsMobileEnabled())                       //minerva
        {
            mEmailAddress = SharedPref.getEmailAddress();
            isMobileSignup = false;
        }
        Utils.clearSharedPref(); //to clear sharedPref at the time another user is logging In
        SharedPref.setOTPVerifyStatus(true);
        SharedPref.setUserLogout(false);
        Utils.setUserLoggedOut(false);
        Utils.truncateDB();
        SharedPref.setEmailAddress(mEmailAddress);
        SharedPref.setMobileNumber(mPhoneNumber);
        SharedPref.setCountryCode(countrycode);
        SharedPref.setUserExist(mIsUserExisted);
        SharedPref.setAdminCreatedPasswordNeverChanged(mIsAdminCreatedPasswordNeverChanged);
        SharedPref.setVerifiedUser(mIsEmailVerified);
        SharedPref.setChangeEmailTimestamp(0);
        SharedPref.setNewEmail(null);
        SharedPref.setMobileEnabled(isMobileSignup);
        //saving userMo for unVerified User
        try {
            SettingManager.getInstance().updateUserMO(mUserInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mIsUserExisted) {
            if (!mSignUpStatus.equalsIgnoreCase("ACCEPTED") && BuildConfig.isPhysicianApp) {
                SharedPref.setUserType(SharedPref.UserType.NEW_USER);
                SharedPref.setUserWaitingForApproval(true);
                openActivity(ApprovalStatusActivity.class, null, true);
            } else {
                SharedPref.setUserType(SharedPref.UserType.REGISTERED);
                openActivity(LoginActivity.class, null, true);
            }
            return;
        }
        SharedPref.setUserType(SharedPref.UserType.NEW_USER);
        //SharedPref.clearAllData();
        //TODO handle disable account flow at physician end
        Intent signUpIntent;
        if (BuildConfig.isPatientApp) {
            signUpIntent = new Intent(this, CreateProfileActivity.class);

        } else {
            //    SharedPref.setOTP(otpCode);
            signUpIntent = new Intent(this, CreatePhysicianProfile.class);
        }
        signUpIntent.putExtra(VariableConstants.OTP_CODE, otpCode);
        if (isDisableAccunt) {
            signUpIntent.putExtra(TextConstants.DISABLE_USER, isDisableAccunt);
        }
        signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(signUpIntent);


    }

    @Subscribe
    public void onChangeNumberResponse(ChangeNumberResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();

        if (response.isSuccessful()) {
            SharedPref.setAccessToken(null);
            //openActivity(LoginActivity.class, null, true);
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    @Subscribe
    public void onVerifyOTPandUpdatePhoneNoResponse(VerifyOTPandUpdatePhoneNoResponse
                                                            response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();

        if (response.isSuccessful() && response.getMessage() != null && (response.getMsg().equals(TextConstants.SUCCESSFUL))) {
            SharedPref.setOldMobileNumber(null);
            SharedPref.setOldCountryCode(null);
            SharedPref.setAccessToken(null);
            Intent login = new Intent(this, LoginActivity.class);
            SharedPref.setDisableAccount(false);
            startActivity(login);
        } else if (response.isSuccessful() && response.getMessage() != null && (response.getMsg().equals(TextConstants.OTP_NOT_FOUND))) {
            showCodeNotMatchedError(getResources().getString(R.string.enter_valid_4_digit_code));//showPhoneNumberSameDialog(TextConstants.OTP_NOT_MATCHED_TEXT);

        } else {
            if (!response.isSuccessful()) {
                showCodeNotMatchedError(getResources().getString(R.string.enter_valid_4_digit_code));
            } else
                Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT);
        }
    }



    @Subscribe
    public void onVerifyOTPFromServerResponse(verifyOTPResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            if (response.getVerifyOTPResponseGson().isVerified()) {       //otp successfully verified
                if (mLaunchMode.equals(CHANGE_NUMBER)) {
                    if (mIsUserExisted) {
                        if (SharedPref.getIsMobileEnabled()) {
                            changePhoneNumber();
                            Toast.makeText(this, getResources().getString(R.string.Number_already_registered), Toast.LENGTH_SHORT).show();
                        } else if (!SharedPref.getIsMobileEnabled()) {
                            Toast.makeText(this, getResources().getString(R.string.Email_already_registered), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        showProgressDialog();
                        ThreadManager.getDefaultExecutorService().submit(new ChangeNumberRequest(SharedPref.getMobileNumber(), getTransactionId()));
                    }
                } else
                    launchNextScreen();

            } else {

                if (response.getVerifyOTPResponseGson().getMsg().equals(TextConstants.OTP_NOT_FOUND))
                    showCodeNotMatchedError(getResources().getString(R.string.incorrect_4_digit_code));
                    //otp mismatch
                else if (response.getVerifyOTPResponseGson().getMsg().equals(TextConstants.OTP_TIME_EXPIRED))
                    Toast.makeText(this, getResources().getString(R.string.one_time_password_expired), Toast.LENGTH_SHORT).show();
                    //otp expired

                else {
                    Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                }//something went wrong
            }
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void reverPhoneInSharedPref() {
        if (!SharedPref.getMobileNumber().trim().equals("")) {
            SharedPref.setMobileNumber(SharedPref.getOldMobileNumber());
            SharedPref.setCountryCode(SharedPref.getOldCountryCode());
        }
        SharedPref.setOldCountryCode(null);
        SharedPref.setOldMobileNumber(null);
    }

    @Subscribe
    public void onCheckPhoneNoExistResponse(CheckPhoneNoExistResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if ((response.isSuccessful() && response.getMessage() != null && response.getMessage().equals("SUCCESSFUL"))) {
            if (SharedPref.getIsMobileEnabled())
                registerSMSReceiver();
            addOrInitOTPFragment();
            if (isResendOtpCode) {
                isResendOtpCode = false;
                Toast.makeText(this, getResources().getString(R.string.sent_4_digit_code_again), Toast.LENGTH_SHORT).show();
            }
        } else if (response.isSuccessful() && response.getMessage() != null && response.getMessage().equals("PHONE_NO_NOT_UNIQUE")) {
            if (SharedPref.getIsMobileEnabled())
                showPhoneNumberNotUniqueDialog(getResources().getString(R.string.number_already_registred_existing_user));
        } else if (response.isSuccessful() && response.getMessage() != null && response.getMessage().equals("PHONE_NO_SAME")) {
            showPhoneNumberSameDialog(getResources().getString(R.string.number_already_link_account));
        } else {
            Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            SharedPref.setDisableAccount(false);
            reverPhoneInSharedPref();
        }
    }

    public void executePhoneExist(CheckPhoneNumberExistGson response, String result) {

        if (result.equals(TextConstants.SUCESSFULL_API_CALL)) {
            OTPInitializingFragment fragment = (OTPInitializingFragment) getFragmentByKey(OTPInitializingFragment.class);
            if (fragment != null && fragment.isVisible())
                fragment.sendOTPAPI();
        } else {
            dismissProgressDialog();

            if (result.equals(TextConstants.PHONE_NO_NOT_UNIQUE))
                showPhoneNumberNotUniqueDialog(getString(R.string.phone_no_not_unique));

            else
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        }
    }

    private void showPhoneNumberSameDialog(String phoneNoNotUniqueMsg) {
        DialogUtils.showAlertDialogCommon(this, MyApplication.getAppContext().getResources().getString(R.string.oops), phoneNoNotUniqueMsg, MyApplication.getAppContext().getResources().getString(R.string.oops), null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    reverPhoneInSharedPref();
                }
            }
        });
    }

    private void showPhoneNumberNotUniqueDialog(String phoneNoNotUniqueMsg) {
        DialogUtils.showAlertDialog(this, MyApplication.getAppContext().getResources().getString(R.string.oops), phoneNoNotUniqueMsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    if (SharedPref.getIsMobileEnabled())
                        SharedPref.setDisableAccount(true);
                    else {
                        SharedPref.setDisableAccount(false);
                        return;
                    }
                    showProgressDialog();
                    ThreadManager.getDefaultExecutorService().submit(new CheckPhoneNoExistRequest(SharedPref.getMobileNumber(), getTransactionId(), SharedPref.isDisableAccount()));

                } else if (which == Dialog.BUTTON_NEGATIVE) {
                    reverPhoneInSharedPref();
                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    /*@Override
    protected void registerSharedPreferenceListener() {
        if (mLaunchMode.equals(CHANGE_NUMBER))
            super.registerSharedPreferenceListener();
    }

    @Override
    protected void unregisterSharedPreferenceListener() {
        if (mLaunchMode.equals(CHANGE_NUMBER))
            super.unregisterSharedPreferenceListener();
    }*/


    @Override
    public void onPermissionNotGranted(String permission) {
        Toast.makeText(this, "Without this permission you cannot proceed...!!!", Toast.LENGTH_SHORT).show();

    }


    public void setResendOTPCode(boolean value) {
        this.isResendOtpCode = value;
    }

    @Override
    public void setIdInActivity(long id) {
        mTransactionId = id;
    }


    public void setToolbar(String mode) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        switch (mode) {
            case OTP_INITIALIZATION:
                if (mLaunchMode.equals(CHANGE_NUMBER)) {
                    mToolbar.setVisibility(View.VISIBLE);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                   // mToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
                    toolbar_title.setText(getResources().getString(R.string.change_your_number));
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                } else {
                    mToolbar.setNavigationIcon(null);
                    toolbar_title.setText(getResources().getString(R.string.lets_get_started));
                    mToolbar.setVisibility(View.GONE);
                }
                isInitializationFragment = true;
                break;

            case OTP_VERIFICATION:
                isInitializationFragment = false;
                mToolbar.setVisibility(View.VISIBLE);

          //      mToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
                if (SharedPref.getIsMobileEnabled()) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                }
                break;
        }
    }

    private void handleBackPress() {
        isDisableAccunt = false;
        changeMobileExtra.putBoolean(TextConstants.DISABLE_USER, isDisableAccunt);
        if (isInitializationFragment) {
            if (mLaunchMode.equals(CHANGE_NUMBER) || mLaunchMode.equals(LOGIN_FLOW))
                finish();
        } else {
            changePhoneNumber();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            handleBackPress();
            return true;
        }
        return false;
    }

    public void setOTPText(String code) {
        OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
        if (code.length() != 4)
            return;
        if (fragment == null || !fragment.isVisible()) {
            return;
        } else {
            fragment.displayOtpCode(code);
        }
    }

    public void showCodeNotMatchedError(String error) {
        OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
        if (fragment == null || !fragment.isVisible()) {
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        } else {
            fragment.displayOtpCodeError(error);

        }
    }
}