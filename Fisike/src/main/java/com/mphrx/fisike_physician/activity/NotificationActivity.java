package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.mphrx.fisike.HeaderActivity;
import com.mphrx.fisike.R;

/**
 * Created by brijesh on 12/10/15.
 */
public class NotificationActivity extends HeaderActivity {


    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        findViews();
        initViews();
        bindViews();

    }

    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
    }

    private void initViews() {
        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.settings_notification));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void bindViews() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onPermissionGranted(String permission) {

    }

    @Override
    public void onPermissionNotGranted(String permission) {

    }
}
