package com.mphrx.fisike_physician.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontEditTextView;

public class ForgotPasswordActivity extends NetworkActivity implements TextWatcher {

    private CustomFontEditTextView mNumber;
    private CustomFontEditTextView mEmail;
    private CustomFontEditTextView mNewPassword;
    private CustomFontEditTextView mConfirmPassword;
    private Button mBtnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        findView();
        initView();
        bindView();
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    private void findView() {
        mNumber = (CustomFontEditTextView) findViewById(R.id.mobile_number);
        mEmail = (CustomFontEditTextView) findViewById(R.id.email);
        mNewPassword = (CustomFontEditTextView) findViewById(R.id.new_password);
        mConfirmPassword = (CustomFontEditTextView) findViewById(R.id.confirm_password);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);

        mConfirmPassword.addTextChangedListener(this);
        mNewPassword.addTextChangedListener(this);
        mEmail.addTextChangedListener(this);
        mNumber.addTextChangedListener(this);
        mBtnSubmit.setEnabled(false);
    }


    private void initView() {

        mNewPassword.setTransformationMethod(new PasswordTransformationMethod());
        mConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void bindView() {
        mBtnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.successfully_changed_the_password), Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        mConfirmPassword.setErrorEnabled(false);
        mNewPassword.setErrorEnabled(false);
        mEmail.setErrorEnabled(false);
        mNumber.setErrorEnabled(false);

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (mConfirmPassword.getText().toString().trim().equals("") ||
                mNewPassword.getText().toString().trim().equals("") ||
                mEmail.getText().toString().trim().equals("") ||
                mNumber.getText().toString().trim().equals("")) {
            mBtnSubmit.setEnabled(false);
        } else {
            mBtnSubmit.setEnabled(true);
        }
    }
}
