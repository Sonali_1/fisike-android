package com.mphrx.fisike_physician.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import com.mphrx.fisike.AppTour.AppTourActivity;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.ConfigFetchBaseActivity;
import com.mphrx.fisike.LaunchingLanguageSelectActivity;
import com.mphrx.fisike.Observer.MobileConfigObserver;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import login.activity.LoginActivity;
import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;

/**
 * Created by brijesh on 31/10/15.
 */
public class SplashActivity extends ConfigFetchBaseActivity implements Observer {

    final Handler handler = new Handler();
    private IconTextView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //preventing relaunching of splashActivity after killing and time passed
        if (!Utils.checkToDisplayPINScreen(SettingManager.getInstance().getUserMO()) && !Utils.checkIsToLogoutUser())
            SharedPref.setLastInteractionTime(System.currentTimeMillis());

        Utils.restrictScreenShot(this);
        setContentView(R.layout.activity_physician_splash);
        imageView = (IconTextView) findViewById(R.id.splash_image);
        if (BuildConfig.isIcomoonForSplashEnabled) {
            imageView.setText(R.string.fa_fisike_spec_icon);
            imageView.setTextSize(100f);
        } else {
            imageView.setBackgroundResource(R.drawable.fisike_icon);
            imageView.setText("");
            imageView.setTextSize(0);
        }

        requestConfig();


        Utils.changeAppLanguage(this);

    }

    @Override
    protected void onResume() {
        MobileConfigObserver.getInstance().addObserver(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        MobileConfigObserver.getInstance().deleteObserver(this);
        super.onPause();
    }

    Runnable nextScreenRunnable = new Runnable() {
        @Override
        public void run() {
            navigateToNextScreen();

        }
    };

    private void navigateToNextScreen() {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isAppTourShown = preferences.getBoolean(VariableConstants.APP_TOUR_SHOWN, false);
        ConfigMO configMo = ConfigManager.getInstance().getConfig();
        if (!isAppTourShown && isError) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(LaunchingLanguageSelectActivity.IS_ERROR, true);
            startActivity(LaunchingLanguageSelectActivity.class, bundle);
            this.finish();
        } else if (!isAppTourShown && getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() > 1) {
            startActivity(LaunchingLanguageSelectActivity.class, null);
        } else if (!isAppTourShown && getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() == 1) {
            Set<String> strings = configMo.getLanguageList().keySet();
            Iterator<String> iterator = strings.iterator();
            String next = iterator.next();
            String languageKey = configMo.getLanguageList().get(next);
            Utils.setLocalLanguage(this, languageKey, next);
            startActivity(AppTourActivity.class, null);
            this.finish();
        } else if (!isAppTourShown) {
            startActivity(AppTourActivity.class, null);
            this.finish();
        } else if (SharedPref.getAccessToken() == null || userMO == null) {
            startActivity(LoginActivity.class, null);
        } else if (BuildConfig.isSearchPatient) {
            if (userMO != null)
                Utils.checkAlternateContactDetails(userMO.getAlternateContact());
            startActivity(SearchPatientFilterActivity.class, null);
        } else {
            if (userMO != null)
                Utils.checkAlternateContactDetails(userMO.getAlternateContact());
            startActivity(ContactActivity.class, null);
        }
    }


    private void startActivity(Class<?> activity, Bundle bundle) {
        Intent i = new Intent(this, activity);
        if (bundle != null) {
            i.putExtras(bundle);
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void navigateToNextScreenFromLaunchingScreen() {
        handler.postDelayed(nextScreenRunnable, VariableConstants.SPLASH_TIME_OUT_IMMEDIATE * 1000);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof MobileConfigObserver) {
            navigateToNextScreenFromLaunchingScreen();
        }
    }
}
