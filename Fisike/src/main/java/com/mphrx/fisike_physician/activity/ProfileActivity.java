package com.mphrx.fisike_physician.activity;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.LoadContactsProfilePicAsyn;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.beans.PhysicianDetails;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.*;
import com.mphrx.fisike_physician.network.request.PhysicianDetailRequest;
import com.mphrx.fisike_physician.network.response.PhysicianDetailResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.CircleImageView;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;
import com.mphrx.fisike_physician.views.RCheckBox;
import com.mphrx.fisike_physician.views.RRadioButton;
import com.mphrx.fisike_physician.views.RTextView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ProfileActivity extends com.mphrx.fisike.utils.BaseActivity {

    private Toolbar mToolbar;
    private TextView mStatus;
    private Switch mMute;

    private TextView mName;
    private TextView mSpeciality;
    private TextView mRole;
    private TextView mExperience;
    private TextView mAddress;
    private TextView mNumber;
    //    private ImageView mUserImage;
    private CustomProfileVIewLayout profileLayout;
    private CircleImageView circleImageView;
    private ChatConversationMO chatConversationMO;
    private String senderId;
    private String physicianId;
    private RTextView tvNotificationLabel;
    private String muteTime = "8hr";
    private CircleImageView mStatusImage;
    private ChatContactMO chatContactMO;
    private FrameLayout frameLayout;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_physician_profile);
        //super.setContentView(R.layout.activity_physician_profile);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        //rendering layout inside framelayout to support side drawer
        getLayoutInflater().inflate(R.layout.activity_physician_profile, frameLayout);
        showHideToolbar(false);
        setCurrentItem(-1);
        findView();
        initView();
        bindView();


        new UserProfilePic(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        if (getIntent().getStringExtra(VariableConstants.SENDER_ID) != null) {
            senderId = getIntent().getStringExtra(VariableConstants.SENDER_ID);

            try {
                chatContactMO = chatContactDBAdapter.getInstance(this).fetchChatContactMo(ChatContactMO.getPersistenceKey(TextPattern.getUserId(senderId)));
//                showProgressDialog();
//                ThreadManager.getDefaultExecutorService().submit(new PhysicianDetailRequest(getTransactionId(), senderId));
                showProgressDialog();
                if (chatContactMO != null)
                    physicianId = Long.toString(chatContactMO.getPhysicianId());
                ThreadManager.getDefaultExecutorService().submit(new PhysicianDetailRequest(getTransactionId(), physicianId));

            } catch (Exception e) {
                e.printStackTrace();
            }
            findViewById(R.id.ll_mute).setVisibility(View.GONE);
        } else {
            senderId = SettingManager.getInstance().getUserMO().getId() + "";
        }

        if (chatContactMO == null) {
            mStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showStatusDialog();
                }
            });
            setTitle(getResources().getString(R.string.Profile));

        } else {
            mStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            ((TextView) findViewById(R.id.tv_my_role)).setText(getResources().getString(R.string.Role));
            setTitle(chatContactMO.getDisplayName());

        }

        if (chatContactMO == null || String.valueOf(SettingManager.getInstance().getUserMO().getId()).equals(chatContactMO.getId())) {
            Bitmap bm = UserProfilePic.loadImageFromStorage(this);
            if (bm != null) {
                circleImageView.setImageBitmap(bm);
            }
            circleImageView.setVisibility(View.VISIBLE);
            profileLayout.setVisibility(View.GONE);
            updateDetailView();
        } else if (chatContactMO != null) {
            profileLayout.setViewProfileImage(chatContactMO.getId());
            setContactDetails();
        }

    }


    @Subscribe
    public void onPhysicianDetailResponse(PhysicianDetailResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            PhysicianDetails physicanDetails = response.getPhysicianData();
            setContactDetails(physicanDetails);
        } else {

            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            //SharedPref.clearAllData();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setSubtitleTextColor(Color.WHITE);

        mStatus = (TextView) findViewById(R.id.status_tv);
        mMute = (Switch) findViewById(R.id.mute_switch);
        mStatusImage = (CircleImageView) findViewById(R.id.ivv_status);
        mName = (TextView) findViewById(R.id.name);
        mSpeciality = (TextView) findViewById(R.id.speciality);
        mRole = (TextView) findViewById(R.id.role);
        mExperience = (TextView) findViewById(R.id.experience);
        mAddress = (TextView) findViewById(R.id.address);
        mNumber = (TextView) findViewById(R.id.mobile_number);
//        mUserImage = (ImageView) findViewById(R.id.iv_profile_picture);
        profileLayout = (CustomProfileVIewLayout) findViewById(R.id.iv_profile_picture_custom);
        circleImageView = (CircleImageView) findViewById(R.id.iv_profile_picture);
        tvNotificationLabel = (RTextView) findViewById(R.id.tv_notifications);
    }

    private void initView() {

        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.Profile));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (SharedPref.getIsUserStatus()) {
            if (!SharedPref.getUserStatus().equals("")) {
                mStatus.setText(SharedPref.getUserStatus());
            }
        }
    }

    private void bindView() {


        mMute.setChecked(SharedPref.getIsMute());

        mMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    showMuteDialog();
                } else {
                    SharedPref.setIsMute(false);
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (chatContactMO != null) {

            return true;
        }
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        RelativeLayout layout = (RelativeLayout) menu.findItem(R.id.action_edit).getActionView();

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideKeyboard();
//                Bundle bundle = new Bundle();
//                bundle.putString(SignUpActivity.LAUNCH_MODE, SignUpActivity.EDIT_PROFILE);
//                Intent i = new Intent(ProfileActivity.this, SignUpActivity.class);
//                i.putExtras(bundle);
//                startActivityForResult(i, 0);
            }
        });
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0 && resultCode == RESULT_OK) {
            Bitmap bm = UserProfilePic.loadImageFromStorage(this);
            if (bm != null) {
                circleImageView.setImageBitmap(bm);
            }
            updateDetailView();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void refreshMute() {
        // chatConversationMO = (ChatConversationMO) getIntent().getExtras().get(VariableConstants.CONVERSATION_ITEM);
        ChatConversationMO chatConvMO = new ChatConversationMO();
        if (senderId.contains(SettingManager.getInstance().getSettings().getFisikeServerIp())) {
            chatConvMO.setJId(senderId);
        } else {
            chatConvMO.setJId(senderId + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp());
        }
        // chatConvMO.generatePersistenceKey(userMO.getUserName());
        chatConvMO.generatePersistenceKey(SettingManager.getInstance().getUserMO().getId() + "");
        try {
            chatConversationMO = ChatConversationDBAdapter.getInstance(this).fetchChatConversationMO(chatConvMO.getPersistenceKey() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (chatConversationMO != null) {
            long muteEndDate = chatConversationMO.getMuteChatTillTime();
            long now = System.currentTimeMillis();
            if (muteEndDate >= now) {
                mMute.setChecked(true);
                tvNotificationLabel.setText(getResources().getString(R.string.chat_unmute));
            } else {
                mMute.setChecked(false);
                tvNotificationLabel.setText(getResources().getString(R.string.chat_mute));
            }

            // don't show the notifications if the show notifications are disabled

        }

    }


    private void showMuteDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mute);

        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = getResources().getDisplayMetrics().widthPixels - 100;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        RRadioButton rb_8_hours = (RRadioButton) dialog.findViewById(R.id.rb_8hrs);
        RRadioButton rb_3_hours = (RRadioButton) dialog.findViewById(R.id.rb_3_hours);
        RRadioButton rb_day1 = (RRadioButton) dialog.findViewById(R.id.rb_day1);
        RRadioButton rb_week1 = (RRadioButton) dialog.findViewById(R.id.rb_week1);
        RRadioButton rb_year1 = (RRadioButton) dialog.findViewById(R.id.rb_year1);

        String muteValue = SharedPref.getMuteValue();
        if (muteValue.equals("8hr")) {
            rb_8_hours.setChecked(true);
        } else if (muteValue.equals("3hr")) {
            rb_3_hours.setChecked(true);
        } else if (muteValue.equals("1day")) {
            rb_day1.setChecked(true);
        } else if (muteValue.equals("1week")) {
            rb_week1.setChecked(true);
        } else if (muteValue.equals("1yr")) {
            rb_year1.setChecked(true);
        }

        rb_8_hours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) muteTime = "8hr";
            }
        });
        rb_3_hours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) muteTime = "3hr";

            }
        });
        rb_day1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) muteTime = "1day";

            }
        });
        rb_week1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) muteTime = "1week";

            }
        });
        rb_year1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) muteTime = "1yr";

            }
        });
        final RCheckBox showNotif = (RCheckBox) dialog.findViewById(R.id.chb_show_notification);
        showNotif.setChecked(SharedPref.getIsShowNotification());
        dialog.getWindow().setAttributes(lp);
        TextView okay = (TextView) dialog.findViewById(R.id.btn_okay);
        TextView cancel = (TextView) dialog.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMute.setChecked(SharedPref.getIsMute());
                dialog.dismiss();
            }

        });
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                long muteTime = calculateTimeInMillsForMute();
                SharedPref.setMuteTimeInMills(muteTime);
                SharedPref.setIsMute(true);
                SharedPref.setShowNofification(showNotif.isChecked());
                SharedPref.setMuteValue(ProfileActivity.this.muteTime);
            }

            private long calculateTimeInMillsForMute() {

                long timeToMuteInMills = System.currentTimeMillis();
                if (muteTime.equals("8hr")) {
                    timeToMuteInMills = timeToMuteInMills + TimeUnit.HOURS.toMillis(8);
                } else if (muteTime.equals("3hr")) {
                    timeToMuteInMills = timeToMuteInMills + TimeUnit.HOURS.toMillis(3);
                } else if (muteTime.equals("1day")) {
                    timeToMuteInMills = timeToMuteInMills + TimeUnit.DAYS.toMillis(1);
                } else if (muteTime.equals("1week")) {
                    timeToMuteInMills = timeToMuteInMills + TimeUnit.DAYS.toMillis(7);
                } else if (muteTime.equals("1yr")) {
                    timeToMuteInMills = timeToMuteInMills + TimeUnit.DAYS.toMillis(364);
                }
                return timeToMuteInMills;
            }
        });
        dialog.show();
    }

    private void showStatusDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_status);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
        dialog.setCancelable(true);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(attributes);
        TextView okay = (TextView) dialog.findViewById(R.id.btn_okay);
        final CustomFontEditTextView status = (CustomFontEditTextView) dialog.findViewById(R.id.status);
        final Switch aSwitch = (Switch) dialog.findViewById(R.id.switch1);
        if (!aSwitch.isChecked()) {
            status.setText(getResources().getString(R.string.I_am_available_));
            status.setEnabled(false);
            dialog.findViewById(R.id.min_char).setVisibility(View.GONE);
        } else {
            status.setText("");
            status.setEnabled(true);
            dialog.findViewById(R.id.min_char).setVisibility(View.VISIBLE);
        }

        aSwitch.setChecked(SharedPref.getIsUserStatus());

        if (SharedPref.getIsUserStatus()) {
            if (!SharedPref.getUserStatus().equals("")) {
                status.setText(SharedPref.getUserStatus());
                status.setEnabled(true);
            }
        }
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    status.setText(getResources().getString(R.string.I_am_busy));
                    status.setEnabled(true);
                    dialog.findViewById(R.id.min_char).setVisibility(View.VISIBLE);
                } else {
                    status.setText(getResources().getString(R.string.I_am_available_));
                    status.setEnabled(false);

                    dialog.findViewById(R.id.min_char).setVisibility(View.GONE);
                }
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (aSwitch.isChecked() && status.getText().toString().trim().length() == 0) {
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.Please_enter_your_status), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    dialog.dismiss();
                }

                SharedPref.setIsUserStatus(aSwitch.isChecked());

                if (aSwitch.isChecked()) {
                    mService.changeStatus(TextConstants.STATUS_BUSY, status.getText().toString());
                    SharedPref.setUserStatus(status.getText().toString());
                    mStatusImage.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.red)));

                } else {
                    mService.changeStatus(TextConstants.STATUS_AVAILABLE, getResources().getString(R.string.I_am_available_));
                    SharedPref.setUserStatus(getResources().getString(R.string.I_am_available_));
                    mStatusImage.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.green)));

                }
                mStatus.setText(SharedPref.getUserStatus());

            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (SharedPref.getUserType().equals(SharedPref.UserType.PARTIALLY_REGISTERED.getTypeString())) {
            Toast.makeText(this, getResources().getString(R.string.Clear_App_Data_Uninstall_App_check_flow), Toast.LENGTH_SHORT).show();
        } else /*if (SharedPref.getUserType().equals(SharedPref.UserType.REGISTERED.getTypeString()))*/ {
            super.onBackPressed();
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if (chatContactMO != null ) {
//            setContactDetails();
//        } else {
//            updateDetailView();
//        }
//    }

    private void setContactDetails(PhysicianDetails physicanDetails) {
        ArrayList<Address> address = physicanDetails.getAddresses();
        if (address.size() > 0)
            mAddress.setText(address.get(0).getText().toString());
        mRole.setText(physicanDetails.getRole());
        String experience = physicanDetails.getExperience();
        /*if (experience != null && experience.contains("years")) {
            experience = experience.replace("years", "");
        } else if (experience != null && experience.contains("years")) {
            experience = experience.replace("year", "");
        }*/
        mExperience.setText(experience + getResources().getString(R.string.years_in_the_profession));
        if (!TextUtils.isEmpty(physicanDetails.getSpeciality())) {
            mSpeciality.setText(physicanDetails.getSpeciality());
        }
        boolean isUpdateNeed = false;

        if (chatContactMO != null) {

            ContentValues values = new ContentValues();

            if (!TextUtils.isEmpty(physicanDetails.getExperience())) {
                values.put("experience", physicanDetails.getExperience());
                isUpdateNeed = true;
            }
            if (!TextUtils.isEmpty(physicanDetails.getRole())) {
                values.put("role", physicanDetails.getRole());
                isUpdateNeed = true;
            }
            if (!TextUtils.isEmpty(physicanDetails.getSpeciality())) {
                values.put("speciality", physicanDetails.getSpeciality());
                isUpdateNeed = true;
            }

            if (isUpdateNeed)
                chatContactDBAdapter.getInstance(MyApplication.getAppContext()).updateChatContactMo(chatContactMO.getPersistenceKey(), values);
        }
    }


    private void setContactDetails() {
        mSpeciality.setText(chatContactMO.getSpeciality());
        setCountryCodeAndNumber(chatContactMO.getPhoneNo());
//        mAddress.setText("N/A");
        mRole.setText(chatContactMO.getRoleSingle());
        mExperience.setText(chatContactMO.getExperienceSingle() + getResources().getString(R.string.years_in_the_profession));
        mName.setText(chatContactMO.getDisplayName());
    }

    private void setCountryCodeAndNumber(String contactNumber) {

        if (contactNumber != null) {
            mNumber.setText(contactNumber);
        }
    }

    private void updateDetailView() {
        StringBuilder userName = new StringBuilder();
        userName.append(SettingManager.getInstance().getUserMO().getFirstName());
        userName.append(" ");
        userName.append(SettingManager.getInstance().getUserMO().getLastName());
        mName.setText(userName.toString());
        mSpeciality.setText(SharedPref.getSpeciality());
        mExperience.setText(SharedPref.getExperience() + getResources().getString(R.string.years_in_the_profession));
        mRole.setText(SharedPref.getRole());
//        mAddress.setText(SharedPref.getAddress());
        setCountryCodeAndNumber(SharedPref.getMobileNumber());

    }

 /*   @Override
    public void onMessengerServiceBind() {
        refreshMute();
        if (chatContactMO != null) {

            if (chatContactMO.getPresenceStatus().equals(TextConstants.STATUS_AVAILABLE)) {
                String status = SharedPref.getUserStatus();
                mStatus.setText(status.equals("") ? "I am available." : status);
                mStatusImage.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.green)));

            } else {
                mStatus.setText(mService.getUserStatus(senderId));
                mStatusImage.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.red)));

            }
        }

    }*/


   /* @Override
    public void onMessengerServiceUnbind() {
}
    */


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // showHideToolbar(true);
    }
}
