package com.mphrx.fisike_physician.activity;

/**
 * Created by manohar on 15/12/15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.GroupMembersAdapter;
import com.mphrx.fisike.asynctask.UploadGroupPicAsyncTask;
import com.mphrx.fisike.asynctaskmanger.HandlePresence;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.PiwikConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ErrorMessage;
import com.mphrx.fisike_physician.IGroupImageViewLoader;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.packet.MUCAdmin;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupDetailActivity extends com.mphrx.fisike.utils.BaseActivity implements GroupMembersAdapter.clickListener {
    private RecyclerView reycyclerView;
    private RecyclerView.LayoutManager settingLayoutManager;
    private IGroupImageViewLoader imgPic;

    private CustomFontTextView memberCountTv;
    private CustomFontEditTextView edUserName;

    private View footerView;

    private GroupMembersAdapter adapter;
    private ArrayList<ChatContactMO> chatConatList;

    private static final int SELECT_PHOTO = 1;
    private static final int CROP_RESULT = 2;
    private static final int INVITE_RESULT = 3;
    private static final int UPDATE_CONTACT_DETAIL = 4;

    private static final int REMOVE_GROUP = 11;
    private static final int SEND_MESSAGE = 12;
    private static final int VIEW_PROFILE = 13;

    private File photoUri;
    private Uri mImageCaptureUri = null;

    private Handler handler;
    private Runnable removeGroupTimer;

    // 30ms
    private static final int REMOVE_GROUP_TIMER_INTERVAL = 30 * 1000;

    private static String removeMemberPacketId = null;

    private ChatConversationMO chatConversationMO;
    private GroupTextChatMO groupTextChatMO;
    private boolean isUserAdmin;
    private boolean isRegisteredBroadcast;
    private ConnectionInfo connectionInfo;
    private XMPPConnection connection;
    private String id;

    private ProgressDialog progressDialog;
    private View progressImage;
    private boolean isUploadProfileDone = true;

    private AsyncTask<Void, Void, Void> uploadGroupPicAsyncTask;
    protected AsyncTask<Void, Void, Boolean> removeProfilePicture;

    private int groupManagementAction = -1;
    private Toolbar groupDetailToolBar;
    boolean isGroupInfoChanged;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        showHideToolbar(false);
        Bundle extras = getIntent().getExtras();
        if (BuildConfig.isPhysicianApp)
            setCurrentItem(-1);
        groupTextChatMO = (GroupTextChatMO) extras.get(VariableConstants.GROUP_INFO);
        chatConversationMO = (ChatConversationMO) extras.get(VariableConstants.CONVERSATION_ITEM);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        //rendering layout inside framelayout to support side drawer
        getLayoutInflater().inflate(R.layout.group_details_activity, frameLayout);
        //setContentView(R.layout.group_details_activity);
        findViews();
        if (groupDetailToolBar != null) {
            collapsingToolbarLayout = (CollapsingToolbarLayout) frameLayout.findViewById(R.id.collapsing_toolbar);
            collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
            collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
            setSupportActionBar(groupDetailToolBar);
            groupDetailToolBar.setNavigationIcon(R.drawable.ic_back_w_shadow);
            groupDetailToolBar.setTitleTextColor(Color.WHITE);
            if (chatConversationMO != null) {
                getSupportActionBar().setTitle(getIntent().getStringExtra("groupName"));
            } else {
                getSupportActionBar().setTitle(getResources().getString(R.string.Members));
            }
        }

        UserMO userMO = SettingManager.getInstance().getUserMO();
        String userId = userMO.getId() + "";
        try {
            id = URLDecoder.decode(userId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            id = userId;
        }

        connectionInfo = ConnectionInfo.getInstance();
        connection = connectionInfo.getXmppConnection();

//        registerForContextMenu(reycyclerView);

        if (!mBound) {
            bindMessengerService();
        }

        registerBroadcastListeners();

        isUserAdmin = isAdmin();

        if (isUserAdmin) {
            imgPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edit_pic(v);
                }
            });

            findViewById(R.id.edit_group_name_new).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edit_name(v);
                }
            });
        } else {
            findViewById(R.id.edit_group_name_new).setVisibility(View.GONE);
        }

        findViewById(R.id.delete_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.sendDeleteGroupPacket(chatConversationMO.getJId());
            }
        });
    }


    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            broadcastManager.registerReceiver(updateGroupdetail, new IntentFilter(VariableConstants.BROADCAST_GROUP_CHAT_REMOVE_MEMBER));
            broadcastManager.registerReceiver(statusChangeBroadcast, new IntentFilter(VariableConstants.BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED));
            isRegisteredBroadcast = true;
        }
    }

    private BroadcastReceiver statusChangeBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String chatPK = bundle.getString(VariableConstants.PERSISTENCE_KEY);

            for (int index = 0; chatConatList != null && index < chatConatList.size(); index++) {
                ChatContactMO chatContactMO = chatConatList.get(index);
                if (chatPK.equalsIgnoreCase(chatContactMO.getPersistenceKey() + "")) {
                    chatConatList.get(index).setPresenceType(bundle.getString(VariableConstants.PRESENCE_TYPE));
                    chatConatList.get(index).setPresenceStatus(bundle.getString(VariableConstants.PRESENCE_MESSAGE));
                    adapter.setChatContactList(chatConatList);
                    adapter.notifyDataSetChanged();
                    break;
                }
            }
        }
    };

    private void unRegisterBroadcastListeners() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(updateGroupdetail);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        unRegisterBroadcastListeners();
//        showHideToolbar(true);
        super.onDestroy();
    }

    /**
     * Broadcast for handling conversation list changes
     */
    private BroadcastReceiver updateGroupdetail = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                ChatConversationMO chatConversationMO = (ChatConversationMO) intent.getExtras().get(VariableConstants.CONVERSATION_ITEM);
                if (null != chatConversationMO && GroupDetailActivity.this.chatConversationMO.getJId().equals(chatConversationMO.getJId())) {
                    if (null != progressDialog && progressDialog.isShowing()) {
                        progressDialog.cancel();
                        progressDialog = null;
                    }
                    if (handler != null) {
                        cancelRemoveGroupTimer();
                    }
                    GroupDetailActivity.this.chatConversationMO = chatConversationMO;
                    setGroupMembers();

                    String groupAction = "";
                    String groupText = "";
                    switch (groupManagementAction) {
                        case VariableConstants.ACTION_GROUP_EXIT:
                            groupAction = PiwikConstants.KGROUP_EXIT_GROUP_ACTION;
                            groupText = PiwikConstants.KGROUP_EXIT_GROUP_TXT;
                            GroupDetailActivity.this.setResult(Activity.RESULT_OK);
                            GroupDetailActivity.this.finish();

//                            startActivity(new Intent(GroupDetailActivity.this, MessageListActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                            break;
                        case VariableConstants.ACTION_GROUP_REMOVE:
                            groupAction = PiwikConstants.KGROUP_REMOVE_MEMBER_ACTION;
                            groupText = PiwikConstants.KGROUP_EXIT_MEMBER_TXT;
                            break;
                        case VariableConstants.ACTION_GROUP_UPDATE_NAME:
                            groupAction = PiwikConstants.KGROUP_UPDATE_NAME_ACTION;
                            groupText = PiwikConstants.KGROUP_UPDATE_NAME_TXT;
                            isGroupInfoChanged = true;
                            getSupportActionBar().setTitle(chatConversationMO.getGroupName());
                            collapsingToolbarLayout.setTitle(chatConversationMO.getGroupName());
                            break;
                        case VariableConstants.ACTION_GROUP_UPDATE_PIC:
                            groupAction = PiwikConstants.KGROUP_UPDATE_PIC_ACTION;
                            groupText = PiwikConstants.KGROUP_UPDATE_PIC_TXT;
                            isGroupInfoChanged = true;
//                            ArrayList<String> contactIds = new ArrayList<>();
//
//                            ArrayList<String> moKeys = chatConversationMO.getChatContactMoKeys();
//                            for (int i = 0; i < moKeys.size(); i++) {
//
//                                ChatContactMO mo = DBAdapter.getInstance(GroupDetailActivity.this).fetchChatContactMo(moKeys.get(i));
//                                if (mo != null) {
//                                    contactIds.add(TextPattern.getUserId(mo.getId()));
//                                }
//                            }

//                            imgPic.displayContactImages(contactIds, Utils.extractGroupId(chatConversationMO.getJId()));
                            break;

                        default:
                            groupManagementAction = -1;
                            break;
                    }
                    if (groupManagementAction != -1) {
                        //  PiwikUtils.sendEvent(getApplication(), PiwikConstants.KGROUP_CATEGORY, groupAction, groupText);
                    }
                }
            } catch (Exception e) {

            }
        }
    };

    @Override
    public void finish() {
        unbindMessengerService();
        if (null != uploadGroupPicAsyncTask && !uploadGroupPicAsyncTask.isCancelled()) {
            uploadGroupPicAsyncTask.cancel(true);
            uploadGroupPicAsyncTask = null;
        }
        if (null != removeProfilePicture && !removeProfilePicture.isCancelled()) {
            removeProfilePicture.cancel(true);
            removeProfilePicture = null;
        }
        super.finish();
    }

    /**
     * Set group member details and layout
     */
    protected void setGroupMembers() {
        chatConatList = new ArrayList<ChatContactMO>();
        ArrayList<String> contactListPKs = new ArrayList<String>();
        if (null == chatConversationMO || chatConversationMO.getChatContactMoKeys() == null) {
            finish();
            Toast.makeText(GroupDetailActivity.this, getResources().getString(R.string.Something_went_wrong), Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<String> chatContactMoKeys = chatConversationMO.getChatContactMoKeys();
        Set<String> hs = new HashSet<>();
        hs.addAll(chatContactMoKeys);
        chatContactMoKeys.clear();
        chatContactMoKeys.addAll(hs);
        for (int i = 0; null != chatContactMoKeys && i < chatContactMoKeys.size(); i++) {
            contactListPKs.add(chatConversationMO.getChatContactMoKeys().get(i));
        }

        edUserName.setText(chatConversationMO.getGroupName());

        if (null == groupTextChatMO) {
            groupTextChatMO = new GroupTextChatMO();
        }
        ArrayList<String> admin = groupTextChatMO.getAdmin();

        ArrayList<ChatContactMO> groupContactList = mService.getGroupContactList(contactListPKs);

        for (int index = 0; index < groupContactList.size(); index++) {
            ChatContactMO chatContactMO = groupContactList.get(index);
            if (null != admin && admin.contains(chatContactMO.getPersistenceKey() + "")) {
                chatContactMO.setAdmin(true);
            }
            chatConatList.add(chatContactMO);
        }

        isUserAdmin = getIntent().getBooleanExtra("isUserAdmin", false);

        if (isUserAdmin || isAdmin()) {
            isUserAdmin = true;
        }

        chatConatList = removeDuplicates(chatConatList);

        setAdminRoleTask();

        adapter = new GroupMembersAdapter(this, chatConatList, userMO.getId() + "");
        adapter.setClickListener(this);
        adapter.setGroupTextChatMo(groupTextChatMO);
        adapter.setChatConversationMo(chatConversationMO);
        reycyclerView.setAdapter(adapter);

        memberCountTv.setText(chatConatList.size() + getResources().getString(R.string._of_) + VariableConstants.MAX_MEMBERS);

//        byte[] profilePic = groupTextChatMO.getImageData();
//        if (null != profilePic && profilePic.length != 0) {
//            Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
        ArrayList<String> contactIds = new ArrayList<>();

        for (int i = 0; i < chatConatList.size(); i++) {
            contactIds.add(chatConatList.get(i).getId());
        }
        if (imgPic != null) {
            imgPic.displayContactImages(contactIds, Utils.extractGroupId(chatConversationMO.getJId()));
        }

//        }

        if (null != chatConversationMO && chatConversationMO.getMyStatus() == VariableConstants.chatConversationMyStatusActive && !isUserAdmin) {
            findViewById(R.id.button_exit_group).setVisibility(View.VISIBLE);
        } else if (null != chatConversationMO && chatConversationMO.getMyStatus() == VariableConstants.chatConversationMyStatusInactive) {
            findViewById(R.id.button_exit_group).setVisibility(View.GONE);
        }

    }

    private ArrayList<ChatContactMO> removeDuplicates(ArrayList<ChatContactMO> list) {

        if (list == null) {
            return new ArrayList<>();
        }
        ArrayList<ChatContactMO> finalList = new ArrayList<ChatContactMO>();
        for (ChatContactMO contactMO : list) {
            if (contactMO != null) {
                if (!finalList.contains(contactMO)) {
                    finalList.add(contactMO);
                }
            }
        }
        finalList.size();
        return finalList;
    }

    private void setAdminRoleTask() {
        if (isUserAdmin) {
            findViewById(R.id.edit_group_name).setVisibility(View.VISIBLE);
            findViewById(R.id.edit_profile_pic).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView1).setVisibility(View.VISIBLE);
            if ((footerView != null) && isUserAdmin && footerView.getVisibility() != View.VISIBLE) {
                initaddMembersFootersView();
            } else if (footerView == null && isUserAdmin) {
                initaddMembersFootersView();
            }
        } else {
        }

    }

    /**
     * Tocheck if the user is admin
     *
     * @param
     * @return
     */
    private boolean isAdmin() {
        if (null == groupTextChatMO) {
            return false;
        }
        ArrayList<String> admin = groupTextChatMO.getAdmin();
        if (null == admin) {
            return false;
        }
        for (int i = 0; i < admin.size(); i++) {
            if (admin.get(i).equals(ChatContactMO.getPersistenceKey(id))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the view for the screen
     */
    private void findViews() {
        reycyclerView = (RecyclerView) findViewById(R.id.reycyclerview);
        settingLayoutManager = new LinearLayoutManager(MyApplication.getAppContext());
        reycyclerView.setLayoutManager(settingLayoutManager);
        imgPic = (CustomProfileVIewLayout) findViewById(R.id.group_profile_image);
        imgPic.setRectangleImage(true);

        edUserName = (CustomFontEditTextView) findViewById(R.id.edUserName);
        memberCountTv = (CustomFontTextView) findViewById(R.id.member_count_label);

        progressImage = findViewById(R.id.progressDialog);
        groupDetailToolBar = (Toolbar) frameLayout.findViewById(R.id.toolbar);

    }

    private void initaddMembersFootersView() {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        footerView = inflater.inflate(R.layout.add_member_group_detail, null, false);
//        listView.addFooterView(footerView);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == 1) {
            edUserName.setFocusableInTouchMode(false);
            edUserName.setClickable(false);
            edUserName.setFocusable(false);
            edUserName.setCursorVisible(false);
            edUserName.setBackgroundColor(getResources().getColor(android.R.color.transparent));

            // Toast.makeText(GroupDetailActivity.this, "Yet to be implemented...", 1).show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        if (adapter.isElementYou(info.position)) {
            return;
        }
//        menu.add(Menu.NONE, SEND_MESSAGE, Menu.NONE, "Send Message");
//        menu.add(Menu.NONE, VIEW_PROFILE, Menu.NONE, "View Profile");
        if (isUserAdmin) {
            menu.add(Menu.NONE, REMOVE_GROUP, Menu.NONE, getResources().getString(R.string.Remove_from_Group));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ContextMenu.ContextMenuInfo menuInfo = item.getMenuInfo();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        ChatContactMO chatContactMO = adapter.getItem(info.position);

        switch (item.getItemId()) {
            case SEND_MESSAGE:
                String sender = "";
                String senderId = chatContactMO.getId();
                String realName = chatContactMO.getRealName();
                if (null != chatContactMO && null != chatContactMO.getId()) {
                    sender = chatContactMO.getId();
                } else {
                    try {
                        sender = URLEncoder.encode(senderId, "UTF-8") + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
                    } catch (UnsupportedEncodingException e) {
                    }
                }
                Intent intent = new Intent(this, com.mphrx.fisike.MessageActivity.class);
                if (null != realName && !("".equals(realName))) {
                    intent.putExtra(VariableConstants.SENDER_NICK_NAME, realName);
                }
                intent.putExtra(VariableConstants.ADD_SCREEN, true);
                intent.putExtra(VariableConstants.SENDER_ID, sender);
                intent.putExtra(TextConstants.CHAT_DETAIL, chatContactMO);
                startActivity(intent);
                return true;
            case VIEW_PROFILE:
                String subString = new String(chatContactMO.getId());
                String fisikeServerIp = SettingManager.getInstance().getSettings().getFisikeServerIp();
                if (subString.contains("@" + fisikeServerIp)) {
                    int indexOf = subString.lastIndexOf("@" + fisikeServerIp);
                    subString = subString.substring(0, indexOf);
                }
                try {
                    subString = URLDecoder.decode(subString, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                }


//                if (BuildConfig.isPatientApp) {
//                    startActivityForResult(
//                            new Intent(this, ContactDetailActivity.class).putExtra(TextConstants.CHAT_DETAIL, chatContactMO)
//                                    .putExtra(TextConstants.IS_RECENT_CHAT, false).putExtra(VariableConstants.IS_GROUP_DETAIL, true)
//                                    .putExtra(VariableConstants.SENDER_ID, subString), UPDATE_CONTACT_DETAIL);
//                } else {

                if (chatContactMO.getUserType().getName().equalsIgnoreCase("patient")) {
                    startActivity(new Intent(this, ViewProfile.class).putExtra(VariableConstants.SENDER_ID, subString)
                            .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));

                } else {
                    startActivityForResult(
                            new Intent(this, com.mphrx.fisike_physician.activity.ProfileActivity.class).putExtra(TextConstants.CHAT_DETAIL, chatContactMO)
                                    .putExtra(TextConstants.IS_RECENT_CHAT, false).putExtra(VariableConstants.IS_GROUP_DETAIL, true)
                                    .putExtra(VariableConstants.SENDER_ID, subString), UPDATE_CONTACT_DETAIL);
                }
//                }
                return true;
            case REMOVE_GROUP:
                if (Utils.showDialogForNoXMPPconnection(GroupDetailActivity.this)) {
                    progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage(VariableConstants.REMOVE_GROUP_MEMBER_MESSAGE);
                    progressDialog.setCancelable(true);
                    progressDialog.show();
                    removeMemberFromGroup(chatContactMO);
                    startTimer(chatContactMO);
                }
                groupManagementAction = VariableConstants.ACTION_GROUP_REMOVE;

                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void removeMemberFromGroup(ChatContactMO chatContactMO) {
        String jid;
        try {
            jid = URLEncoder.encode(chatContactMO.getId(), "UTF-8") + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
        } catch (UnsupportedEncodingException e) {
            jid = chatContactMO.getId() + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
        }
        connectionInfo = ConnectionInfo.getInstance();
        connection = connectionInfo.getXmppConnection();
        if (Utils.showDialogForNoNetwork(this, true)) {
            removeMemberFromGroup(chatConversationMO.getJId(), jid, null);
        } else if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
            progressDialog = null;
            cancelRemoveGroupTimer();
        }
    }

    public void removeMemberFromGroup(String groupId, String memberjid, String reason) {
        MUCAdmin removeMemberFromGroupPacket = new MUCAdmin();
        MUCAdmin.Item item = new MUCAdmin.Item(null, "none");
        item.setNick(memberjid);
        item.setReason(reason);
        removeMemberFromGroupPacket.addItem(item);
        removeMemberFromGroupPacket.setFrom(userMO.getId() + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp());
        removeMemberFromGroupPacket.setTo(groupId);
        removeMemberFromGroupPacket.setType(IQ.Type.SET);
        removeMemberPacketId = removeMemberFromGroupPacket.getPacketID();
        mService.removeMemberFromGroup(removeMemberFromGroupPacket);

    }

    private void startTimer(ChatContactMO chatContactMO) {
        HandlerThread hThread = new HandlerThread("HandlerThread");
        hThread.start();
        removeGroupTimer(hThread, chatContactMO);
    }

    private void removeGroupTimer(HandlerThread hThread, final ChatContactMO chatContactMO) {
        handler = new Handler(hThread.getLooper());
        final long timerInterval = REMOVE_GROUP_TIMER_INTERVAL;

        removeGroupTimer = new Runnable() {
            @Override
            public void run() {
                if (removeMemberPacketId != null) {
                    List<String> packetList = new ArrayList<String>();
                    packetList.add(removeMemberPacketId);
                    mService.createSpoolPacket(packetList);
                }
                if (null != progressDialog && progressDialog.isShowing()) {
                    progressDialog.cancel();
                    progressDialog = null;
                }
                Toast.makeText(GroupDetailActivity.this, getResources().getString(R.string.Something_went_wrong_Please_try_again), Toast.LENGTH_LONG).show();
            }
        };

        // Schedule the first execution
        handler.postDelayed(removeGroupTimer, timerInterval);
    }

    public void cancelRemoveGroupTimer() {
        handler.removeCallbacks(removeGroupTimer);

    }

    public void exit_group(View v) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getResources().getString(R.string.Are_you_sure_you_want_to_exit_this_group));

        dialog.setPositiveButton(getResources().getString(R.string.Exit), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Utils.showDialogForNoXMPPconnection(GroupDetailActivity.this)) {
                    exitGroup();
                }
            }
        });
        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void exitGroup() {
        connectionInfo = ConnectionInfo.getInstance();
        connection = connectionInfo.getXmppConnection();
        if (Utils.showDialogForNoNetwork(this, true)) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(VariableConstants.EXIT_GROUP_MESSAGE);
            progressDialog.setCancelable(false);
            progressDialog.show();
            groupManagementAction = VariableConstants.ACTION_GROUP_EXIT;
            mService.exitGroup(chatConversationMO.getJId());
            groupManagementAction = VariableConstants.ACTION_GROUP_EXIT;
        }

    }

    public void edit_pic(View v) {
        connectionInfo = ConnectionInfo.getInstance();
        connection = connectionInfo.getXmppConnection();
        if (!isUploadProfileDone) {
            return;
        }
        if (Utils.showDialogForNoNetwork(this, true)) {
            openSelectPopup();
        } else {
            Toast.makeText(GroupDetailActivity.this, getResources().getString(R.string.Connection_Errror), Toast.LENGTH_SHORT).show();
        }
        // Toast.makeText(GroupDetailActivity.this, "Yet to be implemented...", 1).show();
    }

    public void edit_name(View v) {

        connectionInfo = ConnectionInfo.getInstance();
        connection = connectionInfo.getXmppConnection();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.Edit_name));
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        View countableEditText = inflater.inflate(R.layout.countable_edit_text, null);
        final CustomFontEditTextView etGroupName = (CustomFontEditTextView) countableEditText.findViewById(R.id.et_group_name);
        final CustomFontTextView tvGroupNameLength = (CustomFontTextView) countableEditText.findViewById(R.id.tv_group_name_length);
        etGroupName.setText(edUserName.getText());
        if (edUserName.getText().length() > 0) {
            tvGroupNameLength.setVisibility(View.VISIBLE);
            tvGroupNameLength.setText(VariableConstants.GROUP_NAME_CHARACTER_LIMIT - edUserName.getText().length() + "");
        }
        builder.setView(countableEditText);
        etGroupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String space = " ";
                if (s.toString().trim().length() > 0) {
                    tvGroupNameLength.setVisibility(View.VISIBLE);
                } else {
                    tvGroupNameLength.setVisibility(View.GONE);
                }
                if (etGroupName.getText().toString().trim().length() >= 25) {
                    Toast.makeText(GroupDetailActivity.this, getResources().getString(R.string.group_name_character_limit_exceeded),
                            Toast.LENGTH_LONG).show();
                } else {
                    etGroupName.setEnabled(true);
                }
                tvGroupNameLength.setText(VariableConstants.GROUP_NAME_CHARACTER_LIMIT - s.toString().trim().length() + "");
                if (s.toString().startsWith(space)) {
                    etGroupName.setText(s.toString().trim());
                } else
                    return;
            }
        });

        // Set up the buttons
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Utils.showDialogForNoNetwork(GroupDetailActivity.this, true)) {
                    String groupName = etGroupName.getText().toString();
                    if (isGroupNameChanged(groupName)) {
                        mService.changeGroupName(groupName, chatConversationMO.getJId());
                        progressDialog = new ProgressDialog(GroupDetailActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        groupManagementAction = VariableConstants.ACTION_GROUP_UPDATE_NAME;
                    }
                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
        // Remove padding from parent
        ViewGroup parent = (ViewGroup) countableEditText.getParent();
        parent.setPadding(0, 0, 0, 0);
    }

    protected boolean isGroupNameChanged(String groupName) {
        if (groupName.equals("")) {
            showError(VariableConstants.GROUP_NAME_BLANK_ERROR);
            return false;
        } else if (groupName.trim().equals(edUserName.getText().toString().trim())) {
            showError(VariableConstants.GROUP_NAME_SAME_ERROR);
            return false;
        }
        return true;
    }

    /**
     * Open image File from camera or gallery
     */
    private void openSelectPopup() {
        Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*");

        // look for available intents
        List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);

        // info.add(new ResolveInfo());

        if (listCam.size() > 0) {
            ResolveInfo res = listCam.get(0);
            // if (null != userMO.getProfilePic()) {
            // Intent intent = new Intent();
            // intent.putExtra(REMOVE_PIC, true);
            // intent.setComponent(new ComponentName(res.activityInfo.packageName, "Remove pic"));
            // yourIntentsList.add(intent);
            // info.add(res);
            // }

            final Intent finalIntent = new Intent(camIntent);
            photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    System.currentTimeMillis() + ".jpg");

            photoUri.setWritable(true);
            mImageCaptureUri = Uri.fromFile(photoUri);
            finalIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            finalIntent.putExtra("return-data", true);

            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra("return-data", true);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }

        openDialog(yourIntentsList, info, getResources().getString(R.string.Select_Image), SELECT_PHOTO);
    }

    private void openDialog(final List<Intent> yourIntentsList, List<ResolveInfo> info, String title, final int returnCode) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        // boolean isToShowRemovePic = (null == userMO.getProfilePic()) ? false : true;
        dialog.setAdapter(buildAdapter(this, info, false), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = yourIntentsList.get(id);
                GroupDetailActivity.this.startActivityForResult(intent, returnCode);
                dialog.dismiss();
            }
        });

        dialog.setNeutralButton(GroupDetailActivity.this.getResources().getString(R.string.cancel),
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }

    /**
     * Build the list of items to show using the intent_listview_row layout.
     *
     * @param context
     * @param activitiesInfo
     * @param isToShowRemovePic
     * @return
     */
    private static ArrayAdapter<ResolveInfo> buildAdapter(final Context context, final List<ResolveInfo> activitiesInfo,
                                                          final boolean isToShowRemovePic) {
        return new ArrayAdapter<ResolveInfo>(context, R.layout.intent_listview_row, R.id.title, activitiesInfo) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (isToShowRemovePic && 0 == position) {
                    ImageView image = (ImageView) view.findViewById(R.id.icon);
                    image.setImageResource(R.drawable.ic_pu_rem_pp_pic);
                    CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                    textview.setText(context.getResources().getString(R.string.Remove_profile_picture));
                    return view;
                }
                ResolveInfo res = activitiesInfo.get(position);
                ImageView image = (ImageView) view.findViewById(R.id.icon);
                image.setImageDrawable(res.loadIcon(context.getPackageManager()));
                CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                textview.setText(res.loadLabel(context.getPackageManager()).toString());
                return view;
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bmp;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_PHOTO:
                    if (null != photoUri && !"".equals(photoUri)) {
                        mImageCaptureUri = Uri.fromFile(photoUri);
                    }
                    mImageCaptureUri = (null == data || null == data.getData()) ? mImageCaptureUri : data.getData();
                    if (null != mImageCaptureUri) {
                        String realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
                        if (null == realPathFromURI) {
                            realPathFromURI = mImageCaptureUri.getPath();
                            if (null == realPathFromURI) {
                                break;
                            }
                        }
                        doCrop(mImageCaptureUri);
                    }
                    onUserInteraction();
                    break;
                case CROP_RESULT:
                    if (Utils.showDialogForNoNetwork(this, true)) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            bmp = extras.getParcelable("data");
                            Drawable drawable = new BitmapDrawable(getResources(), bmp);
//                            imgPic.setImageDrawable(drawable);
                            uploadGroupPicAsyncTask = new UploadGroupPicAsyncTask(this, bmp, chatConversationMO.getJId())
                                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    } else {
                        errorInUploading();
                    }
                    onUserInteraction();
                    break;
                case INVITE_RESULT:
                    Bundle extras = data.getExtras();
                    ArrayList<ChatContactMO> invite = (ArrayList<ChatContactMO>) extras.get(VariableConstants.INVITE_LIST_ADDED_MEMBERS);
                    ArrayList<String> chatContactMoKeys = chatConversationMO.getChatContactMoKeys();
                    if (null == chatContactMoKeys) {
                        chatContactMoKeys = new ArrayList<String>();
                    }
                    for (int i = 0; i < invite.size(); i++) {
                        String pk = invite.get(i).getPersistenceKey() + "";
                        if (!chatContactMoKeys.contains(pk)) {
                            chatContactMoKeys.add(pk);
                        }
                    }
                    chatConversationMO.setChatContactMoKeys(chatContactMoKeys);

                    setGroupMembers();

                    for (int i = 0; i < invite.size(); i++) {
                        String jid = invite.get(i).getId();
                        new HandlePresence(new ApiResponseCallback<HandlePresence.HandlePresenceStanza>() {

                            @Override
                            public void onSuccessRespone(HandlePresence.HandlePresenceStanza result) {
                                if (result != null && result.getChatConversationMO() != null) {
                                    mService.updateLocalConversationMo(result);
                                }
                            }

                            @Override
                            public void onFailedResponse(Exception exception) {

                            }
                        }, GroupDetailActivity.this, false, jid, chatConversationMO.getJId()).execute();
                    }

                    break;

                case UPDATE_CONTACT_DETAIL:
                    ChatContactMO tempChatContactMo = (ChatContactMO) data.getExtras().get(VariableConstants.UPDATED_CONTACT);
                    int index = chatConatList.indexOf(tempChatContactMo);
                    if (index >= 0) {
                        chatConatList.set(index, tempChatContactMo);
                    }

                    setGroupMembers();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (isGroupInfoChanged) {
            setResult(RESULT_OK, new Intent().putExtra("groupNameText", chatConversationMO.getGroupName()));
        }
        try {
            if (null != uploadGroupPicAsyncTask && !uploadGroupPicAsyncTask.isCancelled()) {
                uploadGroupPicAsyncTask.cancel(true);
            }
        } catch (Exception e) {
        }
        finish();
        super.onBackPressed();
    }

    /**
     * Error while uploading the profile pic
     */
    private void errorInUploading() {
        if (Utils.isNetworkAvailable(this)) {
            showError(MyApplication.getAppContext().getResources().getString(R.string.fail_upload_profile_pic_try_again));
        } else {
            showError(MyApplication.getAppContext().getResources().getString(R.string.connection_not_available));
        }
        byte[] profilePic = groupTextChatMO.getImageData();
        Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);

        if (null != profilePic && profilePic.length != 0) {
            BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
//            imgPic.setImageDrawable(drawable);
        } else {
//            imgPic.setImageResource(R.drawable.cg_profile_picturedefault);
        }
    }

    private void showError(String txtMessage) {
        ErrorMessage localErrorMessage = new ErrorMessage(this, R.id.layoutParent);
        localErrorMessage.showMessage(txtMessage);
    }

    /**
     * Crop image for maintaining the aspect ratio
     *
     * @param mImageCaptureUri
     */
    private void doCrop(Uri mImageCaptureUri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(mImageCaptureUri, "image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        // List<Intent> yourIntentsList = new ArrayList<Intent>();
        int size = list.size();
        if (size == 0) {
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 120);
            intent.putExtra("outputY", 120);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            boolean isAppLaunched = false;
            for (ResolveInfo res : list) {
                if (res.activityInfo.packageName.equalsIgnoreCase("com.google.android.apps.plus")) {
                    isAppLaunched = true;
                    Intent i = new Intent(intent);
                    i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    startActivityForResult(i, CROP_RESULT);
                    break;
                }
            }

            if (!isAppLaunched) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(i, CROP_RESULT);
            }
        }
    }

    public void uploadProfilePic(String jsonString, byte[] bitmapdata, String groupId) {
        isUploadProfileDone = true;
        progressImage.setVisibility(View.GONE);
        JSONObject respObj;
        try {
            respObj = new JSONObject(jsonString);
            String status = respObj.getString("status");
            if (status.equals("SC200")) {
                isGroupInfoChanged = true;
                if (null != connection && connection.isConnected() && connection.isAuthenticated()) {
                    groupManagementAction = VariableConstants.ACTION_GROUP_UPDATE_PIC;
                    mService.changeGroupImage(groupId);
                }
            } else {

                errorInUploading();
            }
        } catch (JSONException e) {
            errorInUploading();
        } catch (Exception e) {
            errorInUploading();
        }
    }

    public void progressStarted() {
        isUploadProfileDone = false;
        progressImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void itemClicked(View view, int position) {

//            if (BuildConfig.isPatientApp) {
//                if (view.getId() == (R.id.add_members_layout)) {
//                    // add members flow
//                    // add connection check
//                    ArrayList<ChatContactMO> mock = chatConatList;
//                    int size = mock.size();
//                    if (chatConatList.size() >= GroupChatInviteActivity.GROUP_MEMBER_SIZE_LIMIT) {
//                        Toast.makeText(GroupDetailActivity.this, getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG)
//                                .show();
//                    } else {
//                        if (Utils.showDialogForNoNetwork(GroupDetailActivity.this, true)) {
//                            startAddMemberScreen();
//                        }
//                    }
//                } else {
//                    view.showContextMenu();
//                }
//            } else {

        if (position >= adapter.getItemCount()) {
            return;
        }

        ChatContactMO chatContactMO = adapter.getItem(position);
        String subString = new String(chatContactMO.getId());
        String fisikeServerIp = SettingManager.getInstance().getSettings().getFisikeServerIp();
        if (subString.contains("@" + fisikeServerIp)) {
            int indexOf = subString.lastIndexOf("@" + fisikeServerIp);
            subString = subString.substring(0, indexOf);
        }
        try {
            subString = URLDecoder.decode(subString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }


//                if (BuildConfig.isPatientApp) {
//                    startActivityForResult(
//                            new Intent(GroupDetailActivity.this, ContactDetailActivity.class).putExtra(TextConstants.CHAT_DETAIL, chatContactMO)
//                                    .putExtra(TextConstants.IS_RECENT_CHAT, false).putExtra(VariableConstants.IS_GROUP_DETAIL, true)
//                                    .putExtra(VariableConstants.SENDER_ID, subString), UPDATE_CONTACT_DETAIL);
//                } else {

        if (chatContactMO.getUserType().getName().equalsIgnoreCase("patient")) {
            startActivity(new Intent(GroupDetailActivity.this, ViewProfile.class).putExtra(VariableConstants.SENDER_ID, subString)
                    .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));

        } else {
            startActivityForResult(
                    new Intent(GroupDetailActivity.this, com.mphrx.fisike_physician.activity.ProfileActivity.class).putExtra(TextConstants.CHAT_DETAIL, chatContactMO)
                            .putExtra(TextConstants.IS_RECENT_CHAT, false).putExtra(VariableConstants.IS_GROUP_DETAIL, true)
                            .putExtra(VariableConstants.SENDER_ID, subString), UPDATE_CONTACT_DETAIL);
        }
//                }
//            }
    }

    @Override
    public void onMessengerServiceBind() {
        super.onMessengerServiceBind();
        mService.setSenderId("");

        setGroupMembers();

        if (mBound
                && null != chatConversationMO
                && (null == chatConversationMO.getNickName() || chatConversationMO.getNickName().contains(
                "@" + VariableConstants.groupConferenceName))) {
            String groupJid = chatConversationMO.getJId();
            mService.getGroupMembers(groupJid);
            mService.getGroupName(groupJid);
        }
    }
}
