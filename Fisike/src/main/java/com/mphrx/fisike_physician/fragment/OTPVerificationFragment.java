package com.mphrx.fisike_physician.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.background.VolleyRequest.OTPRequest.ResendOtpTFARequest;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.EditTextSplitView;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.network.request.CheckPhoneNoExistRequest;
import com.mphrx.fisike_physician.network.request.OTPGetRequest;
import com.mphrx.fisike_physician.services.SMSReceiver;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.TimerView;

/**
 * Created by brijesh on 25/11/15.
 */
public class OTPVerificationFragment extends BaseFragment {


    private static OTPVerificationFragment fragment;
    private LinearLayout mAutoDetectionErrorContainer;
    private String mPhoneNumber;
    private ImageView mEditNumber;
    private LinearLayout mTimerContainer;
    private EditTextSplitView mOTPText;
    private Button mSubmit;
    private TextView mResend;
    private TextView tvOtpVerifyText;
    private LinearLayout mButtonContainer;
    private TimerView mTimerProgressBar;
    private CountDownTimer mCountTimer;
    private CustomFontTextView tvAutoVerificationFail, tv_otp_verify_text_patient;
    //    private LinearLayout bottomLayout;
    private Button bt_resend;
    private SMSReceiver mReceiver;
    private long mTransactionId;
    public OnGetTransactionIdListener mListener;
    private boolean disableAccount;
    private boolean isChangeNumber;
    private IconTextView ivSad;
    private String mobileHeader;
    private String emailHeader;
    private String usernameHeader;
    private int otpSentDevices;
    private String number;
    private boolean hasSmsPermission;


    public static OTPVerificationFragment getInstance(Bundle bundle) {
        fragment = new OTPVerificationFragment();
        //SharedPref.setOTP("1234"); //for automation purpose
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    protected void findViewPatient() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");
        tvOtpVerifyText = (TextView) getView().findViewById(R.id.tv_otp_verify_text_patient);
        mTimerProgressBar = (TimerView) getView().findViewById(R.id.pb_custom_timer_patient);
        tvAutoVerificationFail = (CustomFontTextView) getView().findViewById(R.id.tv_auto_verification_failed);
        mOTPText = (EditTextSplitView) getView().findViewById(R.id.et_manual_otp_patient);
        mSubmit = (Button) getView().findViewById(R.id.btn_manual_otp_ok_patient);
        bt_resend = (Button) getView().findViewById(R.id.btn_bottom);
        ivSad = (IconTextView) getView().findViewById(R.id.iv_sad);
        bt_resend.setText(R.string.resend_code);
        readArguments(fragment.getArguments());
        if (getArguments() != null)
            disableAccount = getArguments().getBoolean(TextConstants.DISABLE_USER);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_otp_verification_patient;
    }

    @Override
    public void findView() {
        if (getArguments().getString(TextConstants.LAUNCH_MODE) != null)
            isChangeNumber = getArguments().getString(TextConstants.LAUNCH_MODE).equals(TextConstants.CHANGE_NUMBER) ? true : false;
        findViewPatient();
    }

    public void readArguments(Bundle bundle) {
        mobileHeader = bundle.getString("mobile", "");
        emailHeader = bundle.getString("email", "");
        otpSentDevices = bundle.getInt("otpSentOnDevices", 2);
        usernameHeader = bundle.getString("username", "");
        if (SharedPref.getIsMobileEnabled() && mobileHeader.equals("") && emailHeader.equals("")) {
            number = SharedPref.getMobileNumber().trim();
            tvOtpVerifyText.setText(getVerificationText(getActivity().getResources().getString(R.string.sent_sms)) + ":" + number);
        } else if (!SharedPref.getIsMobileEnabled() && mobileHeader.equals("") && emailHeader.equals("")) {
            number = SharedPref.getEmailAddress().trim();
            tvOtpVerifyText.setText(getVerificationText(getActivity().getResources().getString(R.string.sent_email)) + ":" + number);
            mTimerProgressBar.setVisibility(View.GONE);
            tvAutoVerificationFail.setVisibility(View.GONE);
        }
        //text to be taken from vibha
        else if (otpSentDevices == TextConstants.OTP_SENT_ON_EMAIL_AND_MOBILE) {
            tvOtpVerifyText.setText(getVerificationText(getActivity().getResources().getString(R.string.sent_sms)) + ":" + mobileHeader + " and " + emailHeader);
        } else if (otpSentDevices == TextConstants.OTP_SENT_ON_MOBILE_ONLY) {
            tvOtpVerifyText.setText(getVerificationText(getActivity().getResources().getString(R.string.sent_sms)) + ":" + mobileHeader);
        } else if (otpSentDevices == TextConstants.OTP_SENT_ON_EMAIL_ONLY) {
            tvOtpVerifyText.setText(getVerificationText(getActivity().getResources().getString(R.string.sent_email)) + ":" + emailHeader);
            mTimerProgressBar.setVisibility(View.GONE);
            tvAutoVerificationFail.setVisibility(View.GONE);
        }
    }

    private String getVerificationText(String smsOrEmail) {
        if (hasSmsPermission) {
            return smsOrEmail;
        } else
            return getActivity().getResources().getString(R.string.sent_email);

    }

    @Override
    public void initView() {

    }

    @Override
    public void bindView() {
        bindViewPatient();
    }


    private void unregisterSMSReceiver() {
        if (mReceiver != null)
            getActivity().unregisterReceiver(mReceiver);
        mReceiver = null;
    }

  /*  private void addOrInitOTPFragment() {

        mNumber = SharedPref.getMobileNumber();
        SharedPref.setMobileNumber(mNumber);
        OTPVerificationFragment fragment = (OTPVerificationFragment) getFragmentByKey(OTPVerificationFragment.class);
        if (fragment == null || !fragment.isVisible()) {
            replaceFragment(OTPVerificationFragment.getInstance(null), R.id.otp_container, false);
        } else {
            fragment.initView();
            fragment.startTimer();
        }

    }*/

    private Fragment getFragmentByKey(Class<? extends BaseFragment> clazz) {
        FragmentManager fm = getFragmentManager();
        return fm.findFragmentByTag(clazz.getSimpleName());
    }

    private void bindViewPatient() {

        if (SharedPref.getIsMobileEnabled() || !mobileHeader.equals("")) {
            mCountTimer = new CountDownTimer(30 * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimerProgressBar.setTimerProgress(30, millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    if (getActivity() instanceof OtpActivity)
                        ((OtpActivity) getActivity()).onSMSAutoDetectionTimeComplete();
                    else if (getActivity() instanceof LoginActivity)
                        ((LoginActivity) getActivity()).onSMSAutoDetectionTimeComplete();
                    mTimerProgressBar.setVisibility(View.GONE);
                    tvOtpVerifyText.setVisibility(View.GONE);
                    tvAutoVerificationFail.setVisibility(View.VISIBLE);
                    ivSad.setVisibility(View.VISIBLE);
                }
            };
        }
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOTPText.getValue().length() == 4) {
                    if (getActivity() instanceof OtpActivity)
                        ((OtpActivity) getActivity()).navigateUser(mOTPText.getValue(), false);
                } else if (mOTPText.getValue().trim().equals("")) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_valid_4_digit_code), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_valid_4_digit_code), Toast.LENGTH_SHORT).show();
                }
            }
        });

        bt_resend.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                                                 mOTPText.clearEditText();
                                                 if (!checkPermission(Manifest.permission.READ_PHONE_STATE, getActivity().getResources().getString(R.string.We_need_your_device_ID)))
                                                     return;
                                                 showProgressDialog(getResources().getString(R.string.progress_loading));
                                                 mTransactionId = getTransactionId();
                                                 mListener.setIdInActivity(mTransactionId);
                                                 if (getActivity() instanceof OtpActivity)
                                                     ((OtpActivity) getActivity()).setResendOTPCode(true);
                                                 if (isChangeNumber) {
                                                     disableAccount = SharedPref.isDisableAccount();
                                                     ThreadManager.getDefaultExecutorService().submit(new CheckPhoneNoExistRequest(SharedPref.getMobileNumber(), mTransactionId, disableAccount));
                                                 } else {
                                                     if (SharedPref.getIsMobileEnabled()) {
                                                         mPhoneNumber = SharedPref.getMobileNumber();
                                                     } else if (!SharedPref.getIsMobileEnabled()) {
                                                         mPhoneNumber = SharedPref.getEmailAddress();
                                                         disableAccount = false;
                                                     }
                                                     if (getActivity() instanceof LoginActivity) {
                                                         ((LoginActivity) getActivity()).registerSMSReceiver();
                                                         mTransactionId = getTransactionId();
                                                         ThreadManager.getDefaultExecutorService().submit(new ResendOtpTFARequest(mTransactionId));
                                                         mListener.setIdInActivity(mTransactionId);
                                                     } else {
                                                         String deviceUid = SharedPref.getDeviceUid();
                                                         if (deviceUid == null) {
                                                             SharedPref.setDeviceUid(Utils.getDeviceUUid(getActivity()));
                                                         }
                                                         ThreadManager.getDefaultExecutorService().submit(new OTPGetRequest(mPhoneNumber, mTransactionId, SharedPref.getDeviceUid(), disableAccount));
                                                     }
                                                 }
                                             }
                                         }
                                     }
        );

        if (getActivity() instanceof OtpActivity)
            ((OtpActivity) getActivity()).setToolbar(TextConstants.OTP_VERIFICATION);
       /* else if(getActivity() instanceof LoginActivity)
            ((LoginActivity) getActivity()).setToolbar(OtpActivity.OTP_VERIFICATION);*/
        if (SharedPref.getIsMobileEnabled() || !mobileHeader.equals(""))
            startTimerPatient();


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mListener = (OnGetTransactionIdListener) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    public void displayOtpCodeError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public void displayOtpCode(String code) {
        mOTPText.setText(code);
    }


    public interface OnGetTransactionIdListener {
        public void setIdInActivity(long id);
    }

    private void setTransactionId(long mTransactionId) {
        if (getActivity() instanceof NetworkActivity) {
            ((NetworkActivity) getActivity()).setTransactionId(mTransactionId);
        }
    }

    @Override
    public long getTransactionId() {
        if (getActivity() instanceof NetworkActivity) {
            mTransactionId = System.currentTimeMillis();
            return mTransactionId;
        }
        return 0;
    }

    @Override
    public void onPermissionGranted(String permission) {
        /*super.onPermissionGranted(permission);
        showProgressDialog();
        if (getActivity() instanceof OtpActivity)
            ((OtpActivity) getActivity()).registerSMSReceiver();
        else if (getActivity() instanceof LoginActivity)
            ((LoginActivity) getActivity()).registerSMSReceiver();
        ThreadManager.getDefaultExecutorService().submit(new OTPVerifyRequest(SharedPref.getMobileNumber(), getTransactionId()));
        startTimer();*/
    }

    public void startTimer() {

        startTimerPatient();
    }

    public void startTimerPatient() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            hasSmsPermission = true;
            mTimerProgressBar.setVisibility(View.GONE);
            tvOtpVerifyText.setVisibility(View.VISIBLE);
            tvAutoVerificationFail.setVisibility(View.GONE);
            ivSad.setVisibility(View.GONE);
        } else if (mCountTimer != null) {
            mTimerProgressBar.setVisibility(View.VISIBLE);
            tvOtpVerifyText.setVisibility(View.VISIBLE);
            tvAutoVerificationFail.setVisibility(View.GONE);
            ivSad.setVisibility(View.GONE);
            mCountTimer.start();
        }
    }

    @Override
    public void onDestroyView() {
        if (mCountTimer != null)
            mCountTimer.cancel();
        super.onDestroyView();
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Without_this_permission), Toast.LENGTH_SHORT).show();
    }

}