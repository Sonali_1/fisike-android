package com.mphrx.fisike_physician.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import careplan.activity.CareTaskQuestionnaryActivity;

/**
 * Created by laxmansingh on 8/23/2017.
 */

public class MyScheduleFragment extends Fragment implements View.OnClickListener {

    private WebView wvLoadURL;
    private String questionnaireId;
    private View myFragmentView;
    private long mTransactionId;
    private Dialog pdialog;
    private String appId;

    private static final int INPUT_FILE_REQUEST_CODE = 1;
    private static final int FILECHOOSER_RESULTCODE = 1;
    private static String TAG;
    private WebSettings webSettings;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private String[] permissions = new String[]{
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.CAPTURE_VIDEO_OUTPUT,
            Manifest.permission.CAPTURE_AUDIO_OUTPUT,
            Manifest.permission.CAPTURE_SECURE_VIDEO_OUTPUT,
    };
    public static final int MULTIPLE_PERMISSIONS = 6;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.questionnery_webview, container, false);
        String languageKey = Utils.getLanguageKey(getActivity(), null);
        questionnaireId = APIManager.getMyScheduleBaseUrl() + SharedPref.getAccessToken(); /*getArguments().getString(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL);*/
        BusProvider.getInstance().register(this);
        findView();
        initView();
        setWebView();
        //    ThreadManager.getDefaultExecutorService().submit(new CareTaskQuetionnaireRequest(getTransactionId(), getActivity(), questionnaireId));
        return myFragmentView;

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                } else {
                   /* String permissions = "";
                    for (String per : permissionsList) {
                        permissions += "\n" + per;
                    }*/
                    // permissions list of don't granted permission
                }
                return;
            }
        }
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void findView() {
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        if(Build.VERSION.SDK_INT >=21 ){
            CookieManager.getInstance().removeAllCookies(null);
        }else{
            CookieManager.getInstance().removeAllCookie();
        }
        cookieManager.setAcceptCookie(false);

        wvLoadURL = (WebView) myFragmentView.findViewById(R.id.wv_loadUrl);
    }

    private void initView() {
        showProgressDialog();
    }


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void setWebView() {

        if (checkPermissions()) {
            //  permissions  granted.
        }


        CareTaskQuestionnaryActivity.wvLoadURL = wvLoadURL;
        wvLoadURL.getSettings().setJavaScriptEnabled(true);
        wvLoadURL.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");
        wvLoadURL.getSettings().setLoadWithOverviewMode(true);
        wvLoadURL.getSettings().setBuiltInZoomControls(true);
        wvLoadURL.getSettings().setDisplayZoomControls(false);
        wvLoadURL.getSettings().setAllowFileAccess(true);
        wvLoadURL.getSettings().setUseWideViewPort(true);
        wvLoadURL.clearCache(true);
        wvLoadURL.getSettings().setSaveFormData(false);
        wvLoadURL.getSettings().setSavePassword(false);
        wvLoadURL.clearHistory();
        wvLoadURL.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvLoadURL.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        wvLoadURL.setWebViewClient(new MyScheduleFragment.MyBrowser());
        wvLoadURL.setWebChromeClient(new ChromeClient());

        if (Build.VERSION.SDK_INT >= 19) {
            wvLoadURL.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 19) {
            wvLoadURL.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        wvLoadURL.loadUrl(questionnaireId);

        wvLoadURL.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && wvLoadURL.canGoBack()) {
                    wvLoadURL.goBack();
                    return true;
                }
                getActivity().finish();
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_right:
                // showProgressDialog();
                // ThreadManager.getDefaultExecutorService().submit(new CareTaskQuetionnaireSubmitRequest(getTransactionId(), getActivity(), appId));
                getActivity().finish();
                break;
        }
    }

    private void showProgressDialog() {
        pdialog = DialogUtils.showProgressDialog(getActivity(), getResources().getString(R.string.progress_dialog));
        pdialog.show();
    }


    private class MyBrowser extends WebViewClient {

        public MyBrowser() {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(getActivity().getResources().getString(R.string.http)) || url.startsWith(getActivity().getResources().getString(R.string.https))) {
                view.loadUrl(url);
                return true;

            }
            return false;
        }


        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (request.getUrl().toString().startsWith(getActivity().getResources().getString(R.string.http)) || request.getUrl().toString().startsWith(getActivity().getResources().getString(R.string.https))) {
                view.loadUrl(request.getUrl().toString());
                return true;
            }
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(final WebView view, String url) {
            String cookies = CookieManager.getInstance().getCookie(url);
            Log.d(TAG, "Cookies andriod:" + cookies);
            super.onPageFinished(view, url);
            cancelProgressDialog();
            view.post(new Runnable() {
                public void run() {
                    view.scrollTo(0, 0);
                }
            });
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            cancelProgressDialog();
            Toast.makeText(getContext(), getContext().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
        }
    }


    //  Interface to create bridge betweeen Javascript and Android...
    public class WebAppInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * callbackFromWeb
         *
         * @param dialogMsg
         */
        @JavascriptInterface
        public void callbackFromWeb(String dialogMsg) {

            if (dialogMsg.equalsIgnoreCase("success")) {
                Intent data = new Intent();
                data.putExtra("isRefresh", true);
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            } else {
                Toast.makeText(mContext, dialogMsg.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void cancelProgressDialog() {
        if (pdialog != null && pdialog.isShowing()) {
            pdialog.dismiss();
            pdialog = null;
        }
    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            Uri[] results = null;
            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }
            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage) {
                    return;
                }
                Uri result = null;
                try {
                    if (resultCode != Activity.RESULT_OK) {
                        result = null;
                    } else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
        return;
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }


    public class ChromeClient extends WebChromeClient {

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            android.util.Log.d("WebView", consoleMessage.message());
            return true;
        }


        @Override
        public void onPermissionRequest(final PermissionRequest request) {
            Log.d(TAG, "onPermissionRequest");
            getActivity().runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    request.grant(request.getResources());
                }
            });
        }


        // For Android 5.0
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {
            // Double check that we don't have any existing callbacks
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.e(TAG, "Unable to create Image File", ex);
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                } else {
                    takePictureIntent = null;
                }
            }
            Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
            contentSelectionIntent.setType("*/*");
            Intent[] intentArray;
            if (takePictureIntent != null) {
                intentArray = new Intent[]{takePictureIntent};
            } else {
                intentArray = new Intent[0];
            }
            Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
            startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);
            return true;
        }


        //The undocumented magic method override
        //Eclipse will swear at you if you try to put @Override here
        // For Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {

            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);

        }

        // For Android 3.0+
        public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            startActivityForResult(
                    Intent.createChooser(i, "File Browser"),
                    FILECHOOSER_RESULTCODE);
        }

        //For Android 4.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);

        }

    }
}
