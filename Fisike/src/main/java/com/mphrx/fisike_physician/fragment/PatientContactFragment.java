package com.mphrx.fisike_physician.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.ChatUserOptionListDialog;
import com.mphrx.fisike_physician.activity.InviteUsersActivity;
import com.mphrx.fisike_physician.adapters.ContactAdapter;
import com.mphrx.fisike_physician.network.request.ContactRequest;
import com.mphrx.fisike_physician.network.response.ContactResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by brijesh on 20/10/15.
 */
public class PatientContactFragment extends NetworkFragment {

    private ListView mContactList;
    private View mEmptyView;
    private TextView mEmptyMessage;
    private Button mInviteNow;
    private TextView mCreateView;
    private View mDivider;
    private ContactAdapter mAdapter;
    private AtomicBoolean mIsNetworkCallRunning;
    private SwipeRefreshLayout mSwipeContainer;
    private boolean mNextURL;
    private boolean isAlreadyLoaded;

    public static PatientContactFragment getInstance(Bundle bundle) {
        PatientContactFragment fragment = new PatientContactFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
            //fragment.getContactList(true);
        }
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_contact_list;
    }



    /*@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     if(savedInstanceState == null && ! isAlreadyLoaded){
            isAlreadyLoaded=true;
        }
    }*/


    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get View Cannot Be NULL");
        mContactList = (ListView) getView().findViewById(R.id.list);
        mEmptyView = getView().findViewById(R.id.empty_list);
        mCreateView = (TextView) getView().findViewById(R.id.create_message);
        mDivider = getView().findViewById(R.id.divider);
        mEmptyMessage = (TextView) getView().findViewById(R.id.empty_msg);
        mInviteNow = (Button) getView().findViewById(R.id.btn_invite_now);
        mSwipeContainer = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
    }


    @Override
    public void initView() {
        //TODO move text to string.xml
        mEmptyMessage.setText(getResources().getString(R.string.info_no_patients_colleagues, getActivity().getResources().getString(R.string.patients), getResources().getString(R.string.app_title)));

        if (!BuildConfig.isChatEnabled)
            mInviteNow.setVisibility(View.INVISIBLE);

        mCreateView.setVisibility(View.GONE);
        mDivider.setVisibility(View.GONE);
        mContactList.setEmptyView(mEmptyView);
        mContactList.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.GONE);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.action_bar_bg));
        mEmptyView.setVisibility(View.GONE);

        mIsNetworkCallRunning = new AtomicBoolean();
        mIsNetworkCallRunning.set(false);

        mAdapter = new ContactAdapter(new ArrayList<ChatContactMO>(0), getActivity(), true);
        mContactList.setAdapter(mAdapter);

    }

    @Override
    public void bindView() {
        mContactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!BuildConfig.isChatEnabled)
                    return;

                if (!SharedPref.getSetupChatApiCall()) {
                    new ChatUserOptionListDialog(getActivity()).onItemCLick((ChatContactMO) mContactList.getAdapter().getItem(position), false);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Please_wait_for_connecting_to_chat_server), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mInviteNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                openActivity(InviteUsersActivity.class, bundle, false);
            }
        });

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContactList(true);
            }
        });

        mContactList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount <= 0 || mAdapter == null)
                    return;
                if (firstVisibleItem + visibleItemCount == view.getAdapter().getCount()) {
                    if (mNextURL) {
                        getContactList(false);
                    }
                }
            }
        });
    }


    public void getContactList(boolean isPullToRefresh) {
        if (mIsNetworkCallRunning.get())
            return;
        mIsNetworkCallRunning.set(true);
        int count = 0;
        if (!isPullToRefresh && mAdapter != null)
            count = mAdapter.getCount();
        ThreadManager.getDefaultExecutorService().submit(new ContactRequest(getTransactionId(), ContactRequest.ContactType.PATIENT_CONTACT, count, isPullToRefresh));
    }

    @Subscribe
    public void onContactResponse(ContactResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        mIsNetworkCallRunning.set(false);
        mSwipeContainer.setRefreshing(false);
        if (response.isSuccessful()) {
            if (response.getAllUserList().isEmpty())
                mNextURL = false;
            else
                mNextURL = true;
            if (response.isPullToRefreshCall()) {
                mAdapter.updateUserList(response.getAllUserList());
            } else {
                mAdapter.addUserList(response.getAllUserList());
            }

        } else {
            // Show Error View
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void fetchInitialDataFromServer() {
        mSwipeContainer.post(new Runnable() {
            @Override
            public void run() {
                mSwipeContainer.setRefreshing(true);
            }
        });
        getContactList(true);
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }

}
