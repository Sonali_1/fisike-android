package com.mphrx.fisike_physician.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.mphrx.fisike.HeaderActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;

public abstract class BaseFragment extends LogFragment  {

    public static final int PERMISSION_REQUEST_CODE = 1;
    protected boolean isFirstLaunch = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(getLayoutId(), null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        isFirstLaunch = (savedInstanceState == null);

        findView();
        initView();
        bindView();

    }


    protected abstract int getLayoutId();

    public abstract void findView();

    public abstract void initView();

    public abstract void bindView();


    public void openActivity(Class<? extends HeaderActivity> clazz, Bundle bundle, boolean finishThisActivity) {
        ((HeaderActivity) getActivity()).openActivity(clazz, bundle, finishThisActivity);
    }

    public void replaceFragment(BaseFragment fragment, int containerId, boolean addToBackStack) {
        ((HeaderActivity) getActivity()).addFragment(fragment, containerId, addToBackStack);
    }

    public void showProgressDialog() {
        if (getActivity() instanceof NetworkActivity)
            ((NetworkActivity) getActivity()).showProgressDialog();
    }

    public void showProgressDialog(String message) {
        if (getActivity() instanceof NetworkActivity)
            ((NetworkActivity) getActivity()).showProgressDialog(message);
    }

    public void dismissProgressDialog() {
        if (getActivity() instanceof NetworkActivity)
            ((NetworkActivity) getActivity()).dismissProgressDialog();
    }

    public void hideKeyboard() {
        ((HeaderActivity) getActivity()).hideKeyboard();
    }


    public long getTransactionId() {
        if (getActivity() instanceof NetworkActivity)
            return ((NetworkActivity) getActivity()).getTransactionId();
        return 0;
    }

    public boolean checkPermission(final String permissionType, String message) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                            } else
                                onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionNotGranted(permissions[0]);
            }
        }
    }

    public void onPermissionGranted(String permission) {
        throw new RuntimeException(getActivity().getResources().getString(R.string.You_must_override_ask_Runtime_Permission));
    }

    public void onPermissionNotGranted(String permission) {
        throw new RuntimeException(getActivity().getResources().getString(R.string.You_must_override_ask_Runtime_Permission));
    }

    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
            int contentViewTop = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
            if (heightDiff <= contentViewTop) {
                onHideKeyboard();
                rootHeightWithOutKeyBoard = contentViewTop;
            } else {
                int keyboardHeight = heightDiff - contentViewTop;
                onShowKeyboard(keyboardHeight);
                rootHeightWithKeyBoard = contentViewTop;
                getActivity().onBackPressed();
            }
        }
    };


    protected int getKeyBoardHeight() {
        return rootHeightWithOutKeyBoard - rootHeightWithKeyBoard;
    }

    protected int getFooterHeightWithKeyBoard() {
        return (int) (56 + com.mphrx.fisike.utils.Utils.convertPixelsToDp(rootHeightWithOutKeyBoard - rootHeightWithKeyBoard, getActivity()));
    }

    private int rootHeightWithKeyBoard, rootHeightWithOutKeyBoard;
    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;

    protected void onShowKeyboard(int keyboardHeight) {
    }

    protected void onHideKeyboard() {
    }

    protected void attachKeyboardListeners() {
        if (keyboardListenersAttached) {
            return;
        }

        rootLayout = (ViewGroup) getView().findViewById(R.id.rootLayout);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        keyboardListenersAttached = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
        }
    }


}
