
package com.mphrx.fisike_physician.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.MedicationActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.adapter.MedicationAdapter;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike_physician.network.request.MedicationRequest;
import com.mphrx.fisike_physician.network.response.MedicationEncounterResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by manohar on 24/11/15.
 */
public class MedicationFragment extends Fragment implements MedicationAdapter.clickListener, SwipeRefreshLayout.OnRefreshListener {

    private CustomFontTextView tvTrackMedication;
    private RelativeLayout default_view;
    private RecyclerView listMedication;
    private static final int PAGINATION_RECORD_COUNT = 10;
    private View loadingView;
    private LinearLayout loadingViewContainer;
    private View fragmentView;
    MedicationAdapter medicationAdapter;
    private ArrayList<PrescriptionModel> itemList = new ArrayList<PrescriptionModel>();
    private long mTransactionId;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLoading;
    private String patientId;
    private int selectedPosition;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isAllOrdersLoaded;
    private int medicationOrderTotalCount;

    public MedicationFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        isAllOrdersLoaded = sharedPreferences.getBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, false);
        fragmentView = inflater.inflate(R.layout.fragment_medication_physican, container, false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        loadingView = (View) fragmentView.findViewById(R.id.loadingview);
        tvTrackMedication = (CustomFontTextView) fragmentView.findViewById(R.id.tv_track_medication);
        default_view = (RelativeLayout) fragmentView.findViewById(R.id.default_view);
        listMedication = (RecyclerView) fragmentView.findViewById(R.id.listMedication);
        medicationAdapter = new MedicationAdapter(itemList, getActivity());
        listMedication.setLayoutManager(linearLayoutManager);
        medicationAdapter.setClickListener(MedicationFragment.this);
        listMedication.setAdapter(medicationAdapter);
        loadingViewContainer = (LinearLayout) fragmentView.findViewById(R.id.ll_loading_view_container);

        ((CustomFontTextView) loadingView.findViewById(R.id.tv_footer_text)).setText(getActivity().getString(R.string.Hang_on_tight_Fetching_records));
        if (!isLoading)
            getMedicationDetails();
        listMedication.setHasFixedSize(true);
        listMedication.setOnScrollListener(new EndLessScrollingListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // if all the the records are loaded then there is no need to hit the pagination call
                if (isAllOrdersLoaded ) {
                    if(swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
                if (!isLoading) {
                    swipeRefreshLayout.setRefreshing(true);
                    paginationApiCall();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(recyclerView==null || recyclerView.getChildCount()>0) {
                    if (recyclerView.getChildAt(0).getTop() == 0) {
                        swipeRefreshLayout.setEnabled(true);
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                    }
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);
        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }


    private void paginationApiCall() {
        if (medicationOrderTotalCount > 0 && medicationAdapter != null && medicationAdapter.getItemCount() == medicationOrderTotalCount) {
            isAllOrdersLoaded = true;
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, true);
            editor.commit();
            medicationAdapter.setFooterVisible(false);
            if(swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        if (!isLoading) {
            ThreadManager.getDefaultExecutorService().submit(new MedicationRequest(mTransactionId, getPayLoad(), patientId, getActivity()));
            isLoading = true;
            if (medicationAdapter != null) {
                medicationAdapter.notifyDataSetChanged();

            } else {
                medicationAdapter = new MedicationAdapter(itemList, getActivity());
                listMedication.setAdapter(medicationAdapter);
            }
            medicationAdapter.setFooterVisible(true);
        }
    }

    void showProgressBar(boolean show) {
        if (show) {
            loadingView.setVisibility(View.VISIBLE);
            loadingViewContainer.setVisibility(View.VISIBLE);

        } else {
            loadingView.setVisibility(View.GONE);
            loadingViewContainer.setVisibility(View.GONE);
        }
    }

    void showListView(boolean show) {
        listMedication.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    @Subscribe
    public void onMedicationEncounterResponse(MedicationEncounterResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;
        isLoading = false;
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        showProgressBar(false);
        if (response.isSuccessful()) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            ArrayList<PrescriptionModel> arrayPrescriptionModel;
            arrayPrescriptionModel = response.getPrescriptionModel();
            medicationOrderTotalCount = arrayPrescriptionModel.size();
            itemList.addAll(arrayPrescriptionModel);

            if (medicationAdapter.getPrescriptionModelListCount() == 0) {
                default_view.setVisibility(View.VISIBLE);
                listMedication.setVisibility(View.GONE);
            } else {
                default_view.setVisibility(View.GONE);
                listMedication.setVisibility(View.VISIBLE);
            }
            if (medicationOrderTotalCount <= itemList.size()) {
                isAllOrdersLoaded = true;
                editor.putBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, true);
                editor.commit();
            }
            showListView(medicationAdapter.getItemCount() != 0);
            medicationAdapter.notifyDataSetChanged();
            medicationAdapter.setFooterVisible(false);
        } else {
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    void getMedicationDetails() {
        showProgressBar(true);
        patientId = String.valueOf(getArguments().getString("patientId"));
        mTransactionId = System.currentTimeMillis();
        ThreadManager.getDefaultExecutorService().submit(new MedicationRequest(mTransactionId, getPayLoad(), patientId, getActivity()));
    }

    private long getTransactionId() {
        return mTransactionId;
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            JSONObject constraints = new JSONObject();
            constraints.put("patient", patientId);
            constraints.put("_sort:desc", "lastUpdated");
            constraints.put("_skip", itemList == null ? 0 : itemList.size());
            constraints.put("_count", PAGINATION_RECORD_COUNT);
            JSONArray includeArray = new JSONArray();
            includeArray.put("MedicationOrder:prescriber");
            includeArray.put("MedicationOrder:medication");
            includeArray.put("MedicationOrder:patient");
            constraints.put("_include", includeArray);
            payLoad.put("constraints", constraints);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    @Override
    public void itemClicked(View view, int position) {
        selectedPosition = position;
        PrescriptionModel clickedPrescriptionModel = itemList.get(selectedPosition);
        if (clickedPrescriptionModel != null) {
            Intent medicationPrescriptionActivityIntent = new Intent(getActivity(), MedicationActivity.class);
            medicationPrescriptionActivityIntent.putExtra(TextConstants.MEDICATION_READ_MODE, clickedPrescriptionModel);
            getActivity().startActivityForResult(medicationPrescriptionActivityIntent, TextConstants.READ_MEDICATION);
        }
    }

    @Override
    public void itemLongClicked(View view, int position) {

    }

    @Override
    public void onRefresh() {
        if (!isLoading && (itemList == null || itemList.size() == 0)) {
            swipeRefreshLayout.setRefreshing(true);
            paginationApiCall();
        }
        else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
