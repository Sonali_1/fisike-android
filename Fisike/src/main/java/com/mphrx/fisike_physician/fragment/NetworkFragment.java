package com.mphrx.fisike_physician.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

/**
 * Created by brijesh on 21/10/15.
 */
public abstract class NetworkFragment extends BaseFragment {


    private long mTransactionId;
    private String mCacheResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mTransactionId = System.currentTimeMillis();
            mCacheResponse = null;
        } else {
            mTransactionId = savedInstanceState.getLong(TextConstants.TRANSACTION_ID, System.currentTimeMillis());
            mCacheResponse = savedInstanceState.getString(TextConstants.CACHE_RESPONSE, null);
        }
        BusProvider.getInstance().register(this);
        AppLog.showInfo(getClass().getSimpleName(), "Bus Registered");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (TextUtils.isEmpty(mCacheResponse))
            fetchInitialDataFromServer();
        else
            fetchInitialDataFromCache(mCacheResponse);
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(TextConstants.TRANSACTION_ID, mTransactionId);
        if (!TextUtils.isEmpty(mCacheResponse))
            outState.putString(TextConstants.CACHE_RESPONSE, mCacheResponse);
    }

    public long getTransactionId() {
        return mTransactionId;
    }

    public void setCacheResponse(String response) {
        mCacheResponse = response;
    }

    public abstract void fetchInitialDataFromServer();

    public abstract void fetchInitialDataFromCache(String cacheResponse);

}
