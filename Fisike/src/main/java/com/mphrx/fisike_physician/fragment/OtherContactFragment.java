package com.mphrx.fisike_physician.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.activity.ChatInterfaceActivity;
import com.mphrx.fisike_physician.activity.InviteUsersActivity;
import com.mphrx.fisike_physician.adapters.ContactAdapter;
import com.mphrx.fisike_physician.network.response.ContactResponse;
import com.squareup.otto.Subscribe;

/**
 * Created by brijesh on 20/10/15.
 */
public class OtherContactFragment extends NetworkFragment {

    private ListView mContactList;
    private View mEmptyView;
    private TextView mEmptyMessage;
    private Button mInviteNow;
    private TextView mCreateView;
    private View mDivider;
    private ContactAdapter mAdapter;

    public static OtherContactFragment getInstance(Bundle bundle) {
        OtherContactFragment fragment = new OtherContactFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_contact_list;
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get View Cannot Be NULL");
        mContactList = (ListView) getView().findViewById(R.id.list);
        mEmptyView = getView().findViewById(R.id.empty_list);
        mCreateView = (TextView) getView().findViewById(R.id.create_message);
        mDivider = getView().findViewById(R.id.divider);
        mEmptyMessage = (TextView) getView().findViewById(R.id.empty_msg);
        mInviteNow = (Button) getView().findViewById(R.id.btn_invite_now);
    }

    @Override
    public void initView() {

        mCreateView.setText(getActivity().getResources().getString(R.string.Create_group_similar_patients_disease));
        mEmptyMessage.setText(getActivity().getResources().getString(R.string.dummy_Dont_know_what_to_show));
        mInviteNow.setVisibility(View.INVISIBLE);

        mCreateView.setVisibility(View.GONE);
        mDivider.setVisibility(View.GONE);
        mContactList.setEmptyView(mEmptyView);
    }

    @Override
    public void bindView() {
        mContactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), ChatInterfaceActivity.class));

//                openActivity(ChatInterfaceActivity.class, null);
            }
        });

        mCreateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), InviteUsersActivity.class));
            }
        });
    }

    @Subscribe
    public void onContactResponse(ContactResponse response) {

    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }
}
