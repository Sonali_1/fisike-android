
package com.mphrx.fisike_physician.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.LoginSettings;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.CountryCodeUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.adapters.CountryAdapter;
import com.mphrx.fisike_physician.network.request.CheckPhoneNoExistRequest;
import com.mphrx.fisike_physician.network.request.OTPGetRequest;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by brijesh on 25/11/15.
 */
public class OTPInitializingFragment extends BaseFragment {

    private static final int PERMISSION_REQUEST_GET_DEVICEUID_CODE = 2;
    //    private Spinner mCountryCodes;
    private CustomEditTextWhite mCountryCodes;
    private CustomEditTextWhite mMobileNumber, mEmailId;
    private Button mSubmit;
    private boolean isChangeNumberMode;
    private String countryCode;
    private String number;
    private boolean disableAccount;
    private long mTransactionId;
    public OnGetTransactionIdListener mListener;
    private Spinner spinner_change_country_code;
    private int position = 0;
    private RelativeLayout layout_mobile_number;
    private CustomFontTextView tvHeaderLabel;
    private RelativeLayout layout_email_id;
    private LinearLayout label_or;
    CountryAdapter adapter_country;
    private CustomEditTextWhite etUserName;
    private boolean mBackPressed;
    private String countryId;
    /*[********** End of Edited by laxman *******]*/

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (getArguments() != null) {
            disableAccount = getArguments().getBoolean(TextConstants.DISABLE_USER);
            isChangeNumberMode = getArguments().getString(TextConstants.LAUNCH_MODE).equals(TextConstants.CHANGE_NUMBER) ? true : false;
        }

        //resettings fields to show on OTP screen
        if (!disableAccount && !isChangeNumberMode)
            Utils.resetLoginAttributes();

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionIdListener) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    public static OTPInitializingFragment getInstance(Bundle bundle) {
        OTPInitializingFragment fragment = new OTPInitializingFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_otp_initialization_patient;
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        layout_mobile_number = (RelativeLayout) getView().findViewById(R.id.mobile_number_flow);
        layout_email_id = (RelativeLayout) getView().findViewById(R.id.email_flow);
        label_or = (LinearLayout) getView().findViewById(R.id.or_layout);
        spinner_change_country_code = (Spinner) getView().findViewById(R.id.spinner_change_country_code);
        mMobileNumber = (CustomEditTextWhite) getView().findViewById(R.id.et_mobile_number);
        mEmailId = (CustomEditTextWhite) getView().findViewById(R.id.et_email_address_signup);
        etUserName = (CustomEditTextWhite) getView().findViewById(R.id.et_user_name_signup);
        tvHeaderLabel = (CustomFontTextView) getView().findViewById(R.id.enter_phone);
        mSubmit = (Button) getView().findViewById(R.id.btn_ok);

        if (SharedPref.getIsMobileEnabled() || SharedPref.getEmailAndMobile()) {
            TelephonyManager telMgr = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
            int simState = telMgr.getSimState();
            switch (simState) {
                case TelephonyManager.SIM_STATE_ABSENT:
                    setCountryCode(false);
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                    setCountryCode(false);
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                    setCountryCode(false);
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                    setCountryCode(false);
                    break;
                case TelephonyManager.SIM_STATE_READY:
                    setCountryCode(true);
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN:
                    setCountryCode(false);
                    break;
            }
        }


        if (BuildConfig.isFisike) {
            tvHeaderLabel.setVisibility(View.VISIBLE);
            mMobileNumber.setHint("");

        } else if (!BuildConfig.isFisike) {
            tvHeaderLabel.setVisibility(View.GONE);
            mMobileNumber.setHint(getResources().getString(R.string.enter_your_phone_number_hint));
            mEmailId.setHint(getResources().getString(R.string.enter_email));
        }

        if (SharedPref.getIsUserNameEnabled()) {
            etUserName.setHint(getResources().getString(R.string.username));
            layout_email_id.setVisibility(View.GONE);
            layout_mobile_number.setVisibility(View.GONE);
            label_or.setVisibility(View.GONE);
        } else if (SharedPref.getEmailAndMobile() && !isChangeNumberMode) {
            layout_mobile_number.setVisibility(View.VISIBLE);
            label_or.setVisibility(View.VISIBLE);
            layout_email_id.setVisibility(View.VISIBLE);
            tvHeaderLabel.setVisibility(View.GONE);
            mMobileNumber.setHint(getResources().getString(R.string.enter_your_phone_number_hint));
            mEmailId.setHint(getResources().getString(R.string.enter_email));
            mEmailId.addTextChangedListener(new InternalTextWatcher(mEmailId));
            mMobileNumber.addTextChangedListener(new InternalTextWatcher(mMobileNumber));
            etUserName.setVisibility(View.GONE);
        } else if (SharedPref.getIsMobileEnabled() || isChangeNumberMode) {
            label_or.setVisibility(View.GONE);
            layout_email_id.setVisibility(View.GONE);
            tvHeaderLabel.setVisibility(View.VISIBLE);
            mMobileNumber.setHint("");
            etUserName.setVisibility(View.GONE);
        } else {
            layout_mobile_number.setVisibility(View.GONE);
            label_or.setVisibility(View.GONE);
            layout_email_id.setVisibility(View.VISIBLE);
            mEmailId.setHint(getResources().getString(R.string.enter_email));
            etUserName.setVisibility(View.GONE);
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE);

        if (sharedPreferences.contains(VariableConstants.ERROR_MESSAGE_LOGIN) && sharedPreferences.getString(VariableConstants.ERROR_MESSAGE_LOGIN, "").equals(VariableConstants.REVOKE_USER)) {
            DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getResources().getString(R.string.device_deactive_title), getActivity().getResources().getString(R.string.device_deactive_msg), getActivity().getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            sharedPreferences.edit().putString(VariableConstants.ERROR_MESSAGE_LOGIN, "").commit();
        }


        if (getArguments() != null) {
            isChangeNumberMode = getArguments().getString(OtpActivity.LAUNCH_MODE).equals(OtpActivity.CHANGE_NUMBER) ? true : false;
            mBackPressed = getArguments().getBoolean(TextConstants.BACK_PRESSED);
            //prefill username if username config is enabled
            if (getArguments().getBoolean(OtpActivity.PREFILL_NUM) && SharedPref.getIsUserNameEnabled() && SharedPref.getLoginUserName() != null && !SharedPref.getLoginUserName().equals("")) {
                try {
                    etUserName.setText(SharedPref.getLoginUserName());
                    etUserName.post(new Runnable() {
                        @Override
                        public void run() {
                            etUserName.requestFocus();
                            etUserName.setSelection(etUserName.getText().length());
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (getArguments().getBoolean(OtpActivity.PREFILL_NUM) && SharedPref.getOldMobileEnabled() && SharedPref.getMobileNumber() != null && !SharedPref.getMobileNumber().equals("")) {
                try {
                    //    StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                    String countrycode = SharedPref.getCountryCode();
                    if (!countrycode.equals("") && countrycode != null) {
                        position = CountryCodeUtils.getCountryPosition(getActivity(), Integer.parseInt(countrycode.replace("+", "")), countryId);
                        mMobileNumber.setText(SharedPref.getMobileNumber().replace(countrycode.toString().trim(), ""));
                        adapter_country.notifyDataSetChanged();
                        spinner_change_country_code.setSelection(position);

                        mMobileNumber.post(new Runnable() {
                            @Override
                            public void run() {
                                mMobileNumber.requestFocus();
                                mMobileNumber.setSelection(mMobileNumber.getText().length());
                            }
                        });
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (getArguments().getBoolean(TextConstants.PREFILL_NUM) && !SharedPref.getOldMobileEnabled() && SharedPref.getEmailAddress() != null) {
                try {
                    mEmailId.setText(SharedPref.getEmailAddress());
                    mEmailId.post(new Runnable() {
                        @Override
                        public void run() {
                            mEmailId.requestFocus();
                            mEmailId.setSelection(mEmailId.getText().length());
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (getArguments().containsKey(VariableConstants.REVOKE_USER) && getArguments().getBoolean(VariableConstants.REVOKE_USER)) {
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getResources().getString(R.string.device_deactive_title), getActivity().getResources().getString(R.string.device_deactive_msg), getActivity().getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }

            if (!SharedPref.getOldMobileNumber().equals("")) {
                SharedPref.setMobileNumber(SharedPref.getOldMobileNumber());
                SharedPref.setCountryCode(SharedPref.getOldCountryCode());
            }

            SharedPref.setOldCountryCode("");
            SharedPref.setOldMobileNumber("");

        }

        if (getArguments() != null)
            disableAccount = getArguments().getBoolean(TextConstants.DISABLE_USER);


    }


    @Override
    public void initView() {
        if (!mBackPressed)  //if back is pressed while resetting password , we don't need to update sharedpref FIS-9391
            SharedPref.setAdminCreatedPasswordNeverChanged(false); // for handling logout scenario in case of admin login for physician
    }

    @Override
    public void bindView() {

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doAction();
            }
        });
        ((OtpActivity) getActivity()).setToolbar(TextConstants.OTP_INITIALIZATION);


        spinner_change_country_code.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Utils.hideKeyboard(getActivity());
                return false;
            }
        });
    }


    public interface OnGetTransactionIdListener {
        public void setIdInActivity(long id);
    }


    private void setError(CharSequence error) {
        if (SharedPref.getIsMobileEnabled())
            mMobileNumber.setError(error);
        else if (!SharedPref.getIsMobileEnabled())
            mEmailId.setError(error);
    }


    private void doAction() {
        hideKeyboard();
        String countryCode = new String();
        String numbertext = new String();
        // countryCode = spinner_change_country_code.getSelectedItem().toString().trim();

        if (mMobileNumber.getText().toString().trim().equals("") && mEmailId.getText().toString().trim().equals("") && etUserName.getText().toString().trim().equals("")) {
            if (mMobileNumber.hasFocus()) {
                mMobileNumber.setError(getActivity().getResources().getString(R.string.enter_mobile_number));
                mEmailId.setError(null);
                etUserName.setError(null);
            } else if (mEmailId.hasFocus()) {
                mEmailId.setError(getActivity().getString(R.string.enter_email_addresss));
                mMobileNumber.setError(null);
                etUserName.setError(null);
            } else if (etUserName.hasFocus()) {
                etUserName.setError(getActivity().getString(R.string.enter_user_name));
                mMobileNumber.setError(null);
                mEmailId.setError(null);
            }
            return;
        }


        if (SharedPref.getIsMobileEnabled() || SharedPref.getEmailAndMobile()) {
            StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
            countryCode = "+" + stringTokenizer.nextToken().toString().trim();
            numbertext = mMobileNumber.getText().toString().trim();
        }
        String number = new String();

        if (SharedPref.getIsMobileEnabled()) {
            number = mMobileNumber.getText().toString().trim();
        } else if (SharedPref.getIsUserNameEnabled()) {
            number = etUserName.getText().toString().trim();
            SharedPref.setLoginUserName(number);
            numbertext = number;
        } else {
            number = mEmailId.getText().toString().trim();
            SharedPref.setEmailAddress(number);
            numbertext = number;
        }
        String alternateContact = null;
        if (isChangeNumberMode) {
            alternateContact = SettingManager.getInstance().getUserMO().getAlternateContact();
        }

        //Edited by laxman.....
        // countryCode = mCountryCodes.getText().toString().trim();

        if (numbertext.trim().equals("")) {
            if (SharedPref.getIsMobileEnabled()) {
                mMobileNumber.setError(getActivity().getResources().getString(R.string.enter_mobile_number));
            } else if (SharedPref.getIsUserNameEnabled()) {
                etUserName.setError(getActivity().getResources().getString(R.string.enter_user_name));
            } else {
                mEmailId.setError(getActivity().getResources().getString(R.string.enter_email_addresss));
            }
            return;
        } else if (SharedPref.getIsMobileEnabled() && countryCode.equals(TextConstants.INDIA_COUNTRYCODE) && (numbertext.length() != TextConstants.INDIA_PHONENO_LENGTH)) {
            setError(getActivity().getResources().getString(R.string.error_invalid_mobile_no));
            return;
        } else if (SharedPref.getIsMobileEnabled() && (countryCode.length() + number.length()) <= TextConstants.MIN_PHONENO_LENGTH) {
            setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            return;
        } else if (SharedPref.getIsMobileEnabled() && (countryCode.length() + number.length()) > TextConstants.MAX_PHONENO_LENGTH) {
            setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            return;
        } else if (!SharedPref.getIsUserNameEnabled() && !SharedPref.getIsMobileEnabled() && !Utils.isEmailValid(number)) {
            setError(getActivity().getResources().getString(R.string.enter_valid_email_address));
            return;
        } else if (SharedPref.getIsMobileEnabled() && Long.parseLong(numbertext) == 0) //to change server settings
        {
            mMobileNumber.setErrorEnabled(false);
            mEmailId.setErrorEnabled(false);
            startActivity(new Intent(getActivity(), LoginSettings.class).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            return;
        } else if (!SharedPref.getIsMobileEnabled() && numbertext.equals("MphRxP0C")) //to change server settings
        {
            mMobileNumber.setErrorEnabled(false);
            mEmailId.setErrorEnabled(false);
            startActivity(new Intent(getActivity(), LoginSettings.class).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            return;
        } else if (SharedPref.getIsMobileEnabled() && isChangeNumberMode && ((countryCode + "" + numbertext).equals(SharedPref.getMobileNumber()))) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.number_already_link_account), Toast.LENGTH_SHORT).show();
            mMobileNumber.setErrorEnabled(false);
        } else if (isChangeNumberMode && alternateContact != null && !alternateContact.equals("") && ((countryCode + "" + numbertext).equals(alternateContact))) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alternate_primary_number_different), Toast.LENGTH_SHORT).show();
            mMobileNumber.setErrorEnabled(false);
        } else if (!SharedPref.getIsMobileEnabled() && isChangeNumberMode && numbertext.equals(SharedPref.getEmailAddress())) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.email_already_link_account), Toast.LENGTH_SHORT).show();
            mEmailId.setErrorEnabled(false);
        } else if (!SharedPref.getIsMobileEnabled() && isChangeNumberMode && alternateContact != null && !alternateContact.equals("") && ((numbertext).equals(alternateContact))) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alternate_primary_number_different), Toast.LENGTH_SHORT).show();
            mEmailId.setErrorEnabled(false);
        } else {
            mMobileNumber.setErrorEnabled(false);
            mEmailId.setErrorEnabled(false);
            etUserName.setErrorEnabled(false);
            CheckMultiplePermission();
        }
    }


    /*called for change mobile number and login flow*/
    private void getOtp() {

        if (Utils.showDialogForNoNetwork(getActivity(), false)) {

            if (SharedPref.getIsMobileEnabled()) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                countryCode = "+" + stringTokenizer.nextToken().toString().trim();
                number = countryCode + mMobileNumber.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            } else if (SharedPref.getIsUserNameEnabled()) {
                number = etUserName.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            } else {
                number = mEmailId.getText().toString().trim();
                showProgressDialog(getResources().getString(R.string.progress_loading));
            }
            //TODO need to add userType in change phone number flow

            if (isChangeNumberMode) { //to be checked in case of email
                if (SharedPref.getIsMobileEnabled()) {
                    SharedPref.setOldMobileNumber(SharedPref.getMobileNumber());
                    SharedPref.setOldCountryCode(SharedPref.getCountryCode());
                    SharedPref.setMobileNumber(number);
                    SharedPref.setCountryCode(countryCode);
                    disableAccount = SharedPref.isDisableAccount();
                } else {
                    SharedPref.setoldEmailAddress(SharedPref.getEmailAddress());
                    SharedPref.setEmailAddress(number);
                    disableAccount = false;
                }

                mTransactionId = getTransactionId();
                mListener.setIdInActivity(mTransactionId);
                ThreadManager.getDefaultExecutorService().submit(new CheckPhoneNoExistRequest(number, mTransactionId, disableAccount));
            } else {
                sendOTPAPI();
            }
        }
    }


    @Override
    public long getTransactionId() {
        if (getActivity() instanceof NetworkActivity) {
            mTransactionId = System.currentTimeMillis();
            return mTransactionId;
        }
        return 0;
    }

    public void sendOTPAPI() {

        String deviceUid = SharedPref.getDeviceUid();
        if (deviceUid == null) {
            deviceUid = Utils.getDeviceUUid(getActivity());
            SharedPref.setDeviceUid(deviceUid);
        }
        if (SharedPref.getIsMobileEnabled()) {
            SharedPref.setMobileNumber(number);
            SharedPref.setCountryCode(countryCode);

        } else if (SharedPref.getIsUserNameEnabled()) {
            SharedPref.setLoginUserName(number);
        } else if (!SharedPref.getIsMobileEnabled()) {
            SharedPref.setEmailAddress(number);
        }
        mTransactionId = getTransactionId();
        mListener.setIdInActivity(mTransactionId);
        ThreadManager.getDefaultExecutorService().submit(new OTPGetRequest(number, mTransactionId, deviceUid, disableAccount));
    }


    @Override
    public void onPermissionGranted(String permission) {
        getOtp();
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                //denied
                DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.deny_permission), getActivity().getResources().getString(R.string.app_name), getActivity().getResources().getString(R.string.btn_ok),  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                return;
            }
            DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.permission_require_message), getActivity().getResources().getString(R.string.grant_permission), getActivity().getResources().getString(R.string.exit_app), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (which == AlertDialog.BUTTON_POSITIVE) {
                        FragmentCompat.requestPermissions(OTPInitializingFragment.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                                TextConstants.PERMISSION_REQUEST_CODE);
                    } else if (which == AlertDialog.BUTTON_NEGATIVE) {
                        OTPInitializingFragment.this.getActivity().onBackPressed();
                    }
                }
            });
        }
    }


    private void CheckMultiplePermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add(getActivity().getString(R.string.permission_phone_state));

        if (BuildConfig.isSelfSignUpEnable && BuildConfig.isPhoneEnabled && !addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add(getActivity().getString(R.string.permission_sms_recieve));

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = getActivity().getString(R.string.permission_msg)+ " " +permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    FragmentCompat.requestPermissions(OTPInitializingFragment.this, permissionsList.toArray(new String[permissionsList.size()]),
                                            TextConstants.PERMISSION_REQUEST_CODE);
                                }
                            }
                        });
                return;
            }
            //System dialogs generated for each permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                FragmentCompat.requestPermissions(OTPInitializingFragment.this, permissionsList.toArray(new String[permissionsList.size()]),
                        TextConstants.PERMISSION_REQUEST_CODE);
            }
            return;
        }
        // otp flow
        getOtp();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.allow), okListener)
                .setNegativeButton(getResources().getString(R.string.deny), null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                permissionsList.add(permission);
                return false;
            } else if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && !FragmentCompat.shouldShowRequestPermissionRationale(OTPInitializingFragment.this, permission)) {
                permissionsList.add(permission);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case TextConstants.PERMISSION_REQUEST_CODE: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    onPermissionGranted(permissions[0]);
                } else if (perms.get(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // Permission Denied
                    onPermissionNotGranted(permissions[0]);
                }

            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void setCountryCode(boolean on) {

        int country_code = 0;
        if (on) {
            try {
                //country_code = Integer.parseInt(CountryCodeUtils.getCountryZipCode(getActivity()).toString().trim());
            } catch (Exception e) {
                country_code = 91;
            }
        } else if (!on) {
            country_code = 91;
        }

        countryId = "IN";
        position = CountryCodeUtils.getCountryPosition(getActivity(), country_code, countryId);

        spinner_change_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                String countrycode = "+" + stringTokenizer.nextToken().toString().trim();

                if (countrycode.equals(TextConstants.INDIA_COUNTRYCODE)) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    mMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countrycode.length()));
                    mMobileNumber.setFilters(FilterArray);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        adapter_country = new CountryAdapter(getActivity(), countrylist, "white");
        spinner_change_country_code.setAdapter(adapter_country);
        spinner_change_country_code.setSelection(position);
    }

    private class InternalTextWatcher implements TextWatcher {
        View view;

        public InternalTextWatcher(CustomEditTextWhite view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    mEmailId.setErrorEnabled(false);
                    mEmailId.setError(getActivity().getResources().getString(R.string.no_error));
                    mEmailId.setError(null);
                    break;
                case R.id.et_mobile_number:
                    mMobileNumber.setErrorEnabled(false);
                    mMobileNumber.setError(getActivity().getResources().getString(R.string.no_error));
                    mMobileNumber.setError(null);
                    break;
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    mEmailId.setErrorEnabled(false);
                    mEmailId.setError(getActivity().getResources().getString(R.string.no_error));
                    mEmailId.setError(null);
                    break;
                case R.id.et_mobile_number:
                    mMobileNumber.setErrorEnabled(false);
                    mMobileNumber.setError(getActivity().getResources().getString(R.string.no_error));
                    mMobileNumber.setError(null);
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_email_address_signup:
                    if (SharedPref.getEmailAndMobile()) {
                        if (!mMobileNumber.getText().equals(""))
                            mMobileNumber.setText("");
                        SharedPref.setMobileEnabled(false);
                    }
                    mEmailId.setError("");
                    mMobileNumber.setError("");
                    break;
                case R.id.et_mobile_number:
                    if (SharedPref.getEmailAndMobile()) {
                        if (!mEmailId.getText().equals(""))
                            mEmailId.setText("");
                        SharedPref.setMobileEnabled(true);
                        mMobileNumber.setErrorEnabled(false);
                    }
                    mMobileNumber.setError("");
                    mEmailId.setError("");
                    break;
            }
        }
    }
}
