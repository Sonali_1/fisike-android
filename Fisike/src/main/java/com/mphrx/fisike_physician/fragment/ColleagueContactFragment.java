package com.mphrx.fisike_physician.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike_physician.ChatUserOptionListDialog;
import com.mphrx.fisike_physician.activity.CreateNewGroup;
import com.mphrx.fisike_physician.activity.InviteUsersActivity;
import com.mphrx.fisike_physician.adapters.ContactAdapter;
import com.mphrx.fisike_physician.network.request.ContactRequest;
import com.mphrx.fisike_physician.network.response.ContactResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by brijesh on 20/10/15.
 */
public class ColleagueContactFragment extends NetworkFragment {

    private ListView mContactList;
    private View mEmptyView;
    private TextView mEmptyMessage;
    private Button mInviteNow;
    private TextView mCreateView;
    private View mDivider;
    private ContactAdapter mAdapter;
    private AtomicBoolean mIsNetworkCallRunning;
    private SwipeRefreshLayout mSwipeContainer;
    private boolean mNextURL;
    private boolean isAlreadyLoaded;

    public static ColleagueContactFragment getInstance(Bundle bundle) {
        ColleagueContactFragment fragment = new ColleagueContactFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
            //fragment.getContactList(true);
        }
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_contact_list;
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get View Cannot Be NULL");
        mContactList = (ListView) getView().findViewById(R.id.list);
        mEmptyView = getView().findViewById(R.id.empty_list);
        mCreateView = (TextView) getView().findViewById(R.id.create_message);
        mDivider = getView().findViewById(R.id.divider);
        mEmptyMessage = (TextView) getView().findViewById(R.id.empty_msg);
        mInviteNow = (Button) getView().findViewById(R.id.btn_invite_now);
        mSwipeContainer = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
    }

    @Override
    public void initView() {
        // TODO move text to string.xml
        mCreateView.setText(getActivity().getResources().getString(R.string.Create_a_group));
        mEmptyMessage.setText(getResources().getString(R.string.info_no_patients_colleagues, getActivity().getResources().getString(R.string.colleagues), getResources().getString(R.string.app_title)));

        if (!BuildConfig.isChatEnabled)
            mInviteNow.setVisibility(View.INVISIBLE);


        mCreateView.setVisibility(View.GONE);
        mDivider.setVisibility(View.GONE);
        mContactList.setEmptyView(mEmptyView);
        mEmptyView.setVisibility(View.GONE);

        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.action_bar_bg));
        mEmptyView.setVisibility(View.GONE);

        mIsNetworkCallRunning = new AtomicBoolean();
        mIsNetworkCallRunning.set(false);

        mAdapter = new ContactAdapter(new ArrayList<ChatContactMO>(0));
        mContactList.setAdapter(mAdapter);
    }

    @Override
    public void bindView() {
        mContactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!BuildConfig.isChatEnabled)
                    return;

                if (!SharedPref.getSetupChatApiCall()) {
                    new ChatUserOptionListDialog(getActivity()).onItemCLick((ChatContactMO) mContactList.getAdapter().getItem(position), false);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Please_wait_for_connecting_to_chat_server), Toast.LENGTH_SHORT).show();
                }
            }

        });

        mCreateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent groupChatActivityIntent = new Intent(getActivity(), NewGroupActivity.class);
//                startActivity(groupChatActivityIntent);
                openActivity(CreateNewGroup.class, null, false);

            }

        });

        mInviteNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(TextConstants.IS_SECOND_LEVEL, true);
                openActivity(InviteUsersActivity.class, bundle, false);

            }
        });

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContactList(true);
            }
        });

        mContactList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount <= 0 || mAdapter == null)
                    return;
                if (firstVisibleItem + visibleItemCount == view.getAdapter().getCount()) {
                    if (mNextURL) {
                        getContactList(false);
                    }
                }
            }
        });
    }

    @Subscribe
    public void onContactResponse(ContactResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        mIsNetworkCallRunning.set(false);
        mSwipeContainer.setRefreshing(false);
        if (response.isSuccessful()) {

            if (response.getAllUserList().isEmpty())
                mNextURL = false;
            else
                mNextURL = true;

            if (response.isPullToRefreshCall()) {
                if (!response.getAllUserList().isEmpty() && BuildConfig.isChatEnabled) {
                    mCreateView.setVisibility(View.VISIBLE);
                    mDivider.setVisibility(View.VISIBLE);
                } else {
                    mCreateView.setVisibility(View.GONE);
                    mDivider.setVisibility(View.GONE);
                }
                mAdapter.updateUserList(response.getAllUserList());
            } else {
                mAdapter.addUserList(response.getAllUserList());
            }

        } else {
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void getContactList(boolean isPullToRefresh) {
        if (mIsNetworkCallRunning.get())
            return;
        mIsNetworkCallRunning.set(true);
        int count = 0;
        if (!isPullToRefresh && mAdapter != null)
            count = mAdapter.getCount();
        ThreadManager.getDefaultExecutorService().submit(new ContactRequest(getTransactionId(), ContactRequest.ContactType.PHYSICIAN_CONTACT, count, isPullToRefresh));
    }

    @Override
    public void fetchInitialDataFromServer() {
        mSwipeContainer.post(new Runnable() {
            @Override
            public void run() {
                mSwipeContainer.setRefreshing(true);
            }
        });
        getContactList(true);
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }
}
