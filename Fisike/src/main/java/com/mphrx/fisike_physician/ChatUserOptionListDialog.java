package com.mphrx.fisike_physician;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by manohar on 21/01/16.
 */
public class ChatUserOptionListDialog {
    Activity activity;
    private ChatConversationMO hiddenSingleChat;

    public ChatUserOptionListDialog(Activity activity) {
        this.activity = activity;
    }

    public void onItemCLick(final ChatContactMO user, final boolean isFinishAcivity) {

        DBAdapter dbAdapter = DBAdapter.getInstance(activity);

        final List<ChatConversationMO> chatConversationMOs = ChatConversationDBAdapter.getInstance(activity).getConverstionsListOfAUser(user.getId());

        for (int i = chatConversationMOs.size() - 1; i >= 0; i--) {
            boolean isInActive = chatConversationMOs.get(i).getMyStatus() == VariableConstants.chatConversationMyStatusInactive;

            if (TextUtils.isEmpty(chatConversationMOs.get(i).getGroupName()) &&
                    TextUtils.isEmpty(chatConversationMOs.get(i).getNickName())
                    || isInActive) {

                if (chatConversationMOs.get(i).getIsChatInitiated() == 0) {
                    hiddenSingleChat = chatConversationMOs.remove(i);
                } else {
                    chatConversationMOs.remove(i);
                }
            }
        }

        if (chatConversationMOs.size() == 0) {

            openNewChat(user);

            if (isFinishAcivity) activity.finish();
            return;
        }


        if (chatConversationMOs.size() == 1 && chatConversationMOs.get(0).getChatContactMoKeys().size() == 2 && chatConversationMOs.get(0).getJId().startsWith("single")) {
            openPreviousChats(chatConversationMOs.get(0));
            if (isFinishAcivity) activity.finish();

            return;
        }


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                activity,
                android.R.layout.select_dialog_singlechoice);

        boolean isSingleChatAvailable = false;

        String contact = "";

        Collections.sort(chatConversationMOs, new Comparator<ChatConversationMO>() {
            @Override
            public int compare(ChatConversationMO lhs, ChatConversationMO rhs) {
                if (lhs.getGroupName() != null && rhs.getGroupName() != null) {
                    return lhs.getGroupName().equals(rhs.getGroupName()) ? 1 : 0;
                }

                return 0;
            }
        });

        ChatConversationMO contactChatConversationMO = null;

        ArrayList<String> singles = new ArrayList<>();
        for (int i = 0; i < chatConversationMOs.size(); i++) {

            String groupName = chatConversationMOs.get(i).getGroupName();

            if (chatConversationMOs.get(i).getChatContactMoKeys().size() == 2 && chatConversationMOs.get(i).getJId().startsWith("single")) {
                isSingleChatAvailable = true;
                contact = chatConversationMOs.get(i).getNickName();
                singles.add(chatConversationMOs.get(i).getJId());
                contactChatConversationMO = chatConversationMOs.get(i);

            } else {
                if (groupName == null || groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                    if (!TextUtils.isEmpty(chatConversationMOs.get(i).getNickName())) {
                        if (chatConversationMOs.get(i).getChatContactMoKeys().size() == 2) {
                            contact = chatConversationMOs.get(i).getNickName();
                            contactChatConversationMO = chatConversationMOs.get(i);

                        } else {
                            arrayAdapter.add(chatConversationMOs.get(i).getNickName());
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(groupName)) {
                        arrayAdapter.add(chatConversationMOs.get(i).getGroupName());
                    }
                }
            }

        }
        for (int i = chatConversationMOs.size() - 1; i >= 0; i--) {
            if (singles.contains(chatConversationMOs.get(i).getJId())) {
                chatConversationMOs.remove(chatConversationMOs.get(i));
            }
        }

        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setIcon(R.drawable.ic_launcher);
        View view = LayoutInflater.from(activity).inflate(R.layout.chat_options_dialog, null, false);
        builderSingle.setView(view);

        builderSingle.setNegativeButton(
                MyApplication.getAppContext().getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if (!isSingleChatAvailable) {
            builderSingle.setPositiveButton(
                    MyApplication.getAppContext().getResources().getString(R.string.Start_new_conversation),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            openNewChat(user);

                            if (isFinishAcivity) activity.finish();

                        }
                    });
        }

        final AlertDialog dialog = builderSingle.create();
        ListView lv = (ListView) view.findViewById(R.id.list);
        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openPreviousChats(chatConversationMOs.get(position));
                dialog.dismiss();
                if (isFinishAcivity) activity.finish();
            }
        });

        if (chatConversationMOs.size() == 0) {
            view.findViewById(R.id.t2).setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(contact)) {
            view.findViewById(R.id.rl).setVisibility(View.GONE);
        } else {
            TextView contactName = (TextView) view.findViewById(R.id.contactName);

            final ChatConversationMO finalContactChatConversationMO = contactChatConversationMO;
            view.findViewById(R.id.rl).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openPreviousChats(finalContactChatConversationMO);
                    dialog.dismiss();
                }
            });
            RadioButton radioButton = (RadioButton) view.findViewById(R.id.radioButton);
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    openPreviousChats(finalContactChatConversationMO);
                    dialog.dismiss();
                }
            });
            contactName.setText(contact);
        }
        dialog.show();

    }

    private boolean openNewChat(ChatContactMO user) {
        if (hiddenSingleChat != null) {
            openPreviousChats(hiddenSingleChat);
            return true;
        }
        activity.startActivity(new Intent(activity, MessageActivity.class).putExtra(VariableConstants.SENDER_ID, "").putExtra(VariableConstants.ADD_SCREEN, false)
                .putExtra(VariableConstants.SENDER_NICK_NAME, user.getFirstName() + " " + user.getLastName())
                .putExtra(TextConstants.CHAT_DETAIL, user).putExtra(TextConstants.IS_RECENT_CHAT, false)
                .putExtra("isPhysician", true)
                .putExtra(VariableConstants.SENDER_SECOUNDRY_ID, user.getId())
                .putExtra(VariableConstants.SENDER_USER_TYPE, user.getUserType()));
        return false;
    }

    private void openPreviousChats(ChatConversationMO chatConversationMO) {

        int unReadCount = chatConversationMO.getUnReadCount();
        chatConversationMO.setReadStatus(true);

        boolean isGroupChat = chatConversationMO.getConversationType() == VariableConstants.conversationTypeGroup ? true : false;

        String userName = Utils.cleanXMPPUserName(chatConversationMO.getJId());
        String phoneNumber = chatConversationMO.getPhoneNumber();

        Intent intent = new Intent(activity, MessageActivity.class);
        String name = chatConversationMO.getJId().startsWith("single") ? chatConversationMO.getNickName() : chatConversationMO.getGroupName();
        intent.putExtra(VariableConstants.SENDER_ID, userName).putExtra(VariableConstants.PHONE_NUMBER, phoneNumber)
                .putExtra(VariableConstants.SENDER_NICK_NAME, name)
                .putExtra(VariableConstants.SENDER_UNREAD_COUNT, unReadCount).putExtra(TextConstants.IS_RECENT_CHAT, true)
                .putExtra(VariableConstants.IS_GROUP_CHAT, isGroupChat)
                .putExtra(VariableConstants.SENDER_SECOUNDRY_ID, chatConversationMO.getSecoundryJid())
                .putExtra(VariableConstants.MY_STATUS_ACTIVE, chatConversationMO.getMyStatus());

        if (TextUtils.isEmpty(chatConversationMO.getGroupName())) {
            intent.putExtra("groupName", chatConversationMO.getGroupName());
        }
        activity.startActivity(intent);


    }
}
