package com.mphrx.fisike_physician.results.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomCheckBox;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.customview.EditTextFontView;
import com.mphrx.fisike.enums.ResultStatusEnum;
import com.mphrx.fisike.enums.SpecificDaysEnum;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.results.activity.ResultViewActivity;
import com.mphrx.fisike_physician.results.model.SearchModel;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import searchpatient.constants.KeyConstants;

/**
 * Created by kailashkhurana on 07/07/17.
 */

public class BySearch extends SearchBaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private RadioButton radioSpinner;
    private RadioButton radioSpecificDays;
    private CustomFontEditTextView fromDate;
    private CustomFontEditTextView toDate;
    private String dateType;
    private RadioGroup radioGroupDate;
    private CustomFontEditTextView edDOB;
    private ScrollView scrollView;
    private EditTextFontView edKeyword;
    private CustomFontEditTextView edName;
    private CustomFontEditTextView edId;
    private CustomFontEditTextView edPhoneNumber;
    private CustomFontEditTextView edEmail;
    private CustomFontEditTextView spinnerGender;
    private CustomFontEditTextView spinnerDays;
    private CustomFontTextView txtSpecificDays;
    private CustomFontEditTextView spinnerType;
    private CustomFontEditTextView spinnerStatus;
    private CustomFontEditTextView spinnerModality;
    private CustomFontEditTextView edOrderPhysician;
    private CustomFontEditTextView edLocation;
    private CustomFontEditTextView uploadedBy;
    private CustomCheckBox cbBulk;
    private View myFragmentView;
    private CustomFontEditTextView txtEncounter;
    private CustomFontEditTextView edOrderName;
    private CustomFontEditTextView edOrderId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.layout_search_results, container, false);

        findView();
        initView();

        return myFragmentView;
    }

    private void findView() {
        if (myFragmentView == null)
            throw new RuntimeException("Get view cannot be null.");
        else {
            edKeyword = (EditTextFontView) myFragmentView.findViewById(R.id.ed_keyword);
            edName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_name);
            edId = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_id);
            edPhoneNumber = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_phone_number);
            edEmail = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_email_id);
            spinnerGender = (CustomFontEditTextView) myFragmentView.findViewById(R.id.txt_gender);
            txtEncounter = (CustomFontEditTextView) myFragmentView.findViewById(R.id.txt_encounter);
            spinnerDays = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_date);
            txtSpecificDays = (CustomFontTextView) myFragmentView.findViewById(R.id.txt_specific_days);
            spinnerType = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_type);
            spinnerStatus = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_status);
            spinnerModality = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_modality);
            edOrderName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_order_name);
            edOrderId = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_order_id);
            edOrderPhysician = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_order_physician);
            edLocation = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_location);
            uploadedBy = (CustomFontEditTextView) myFragmentView.findViewById(R.id.uploaded_by);
            cbBulk = (CustomCheckBox) myFragmentView.findViewById(R.id.cb_bulk);
            edDOB = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_dob);
            radioGroupDate = (RadioGroup) myFragmentView.findViewById(R.id.radio_group_date);
            radioSpinner = (RadioButton) myFragmentView.findViewById(R.id.radio_spinner);
            radioSpecificDays = (RadioButton) myFragmentView.findViewById(R.id.radio_specificDays);
            fromDate = (CustomFontEditTextView) myFragmentView.findViewById(R.id.fromDate);
            toDate = (CustomFontEditTextView) myFragmentView.findViewById(R.id.toDate);
            scrollView = (ScrollView) myFragmentView.findViewById(R.id.scroll_view);
        }
    }

    private void initView() {
        myFragmentView.findViewById(R.id.txt_reset_date).setOnClickListener(this);
        if (edKeyword.getText().toString().equals("")) {
            edKeyword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search, 0, 0, 0);
        } else {
            edKeyword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search, 0, R.drawable.clear_text, 0);
        }
        edKeyword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (edKeyword.getCompoundDrawables()[DRAWABLE_RIGHT] != null && event.getRawX() >= (edKeyword.getRight() - edKeyword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        edKeyword.setText("");
                        return true;
                    }
                }
                return false;
            }
        });
        edKeyword.addTextChangedListener(new ClearableEditText.TextChangedListener() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().equals("")) {
                    edKeyword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search, 0, 0, 0);
                } else {
                    edKeyword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search, 0, R.drawable.clear_text, 0);
                }

            }
        });

        setSpinnerProperty(spinnerGender, R.string.select, R.drawable.dropdown);
        spinnerGender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerGender);
                }
                return true;
            }
        });
        setSpinnerProperty(toDate, R.string.to_hint, R.drawable.calendar);
        toDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(toDate);
                }
                return true;
            }
        });
        setSpinnerProperty(fromDate, R.string.from_hint, R.drawable.calendar);
        fromDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(fromDate);
                }
                return true;
            }
        });
        setSpinnerProperty(edDOB, R.string.dd_mmm_yyyy, R.drawable.calendar);
        edDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(edDOB);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerDays, R.string.select, R.drawable.dropdown);
        spinnerDays.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerDays);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerType, R.string.select, R.drawable.dropdown);
        spinnerType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerType);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerStatus, R.string.select, R.drawable.dropdown);
        spinnerStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerStatus);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerModality, R.string.select, R.drawable.dropdown);
        spinnerModality.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerModality);
                }
                return true;
            }
        });

        radioSpinner.setOnClickListener(this);
        radioSpecificDays.setOnClickListener(this);

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (Utils.isKeyBoardOpen(getActivity())) {
                    Utils.hideKeyboard(getActivity());
                }
                return false;
            }
        });


        edEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    Utils.hideKeyboard(BySearch.this.getActivity(), v);
                }
                return false;
            }
        });
        txtEncounter.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    Utils.hideKeyboard(BySearch.this.getActivity(), v);
                }
                return false;
            }
        });
        edOrderId.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    Utils.hideKeyboard(BySearch.this.getActivity(), v);
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_gender:
                showPopupMenu(R.menu.menu_gender, spinnerGender, spinnerGender);
                break;
            case R.id.spinner_status:
                showPopupMenu(R.menu.menu_result_status, spinnerStatus, spinnerStatus);
                break;
            case R.id.spinner_type:
                showPopupMenu(R.menu.menu_result_type, spinnerType, spinnerType);
                break;
            case R.id.spinner_modality:
                showPopupMenu(R.menu.menu_result_modality, spinnerModality, spinnerModality);
                break;
            case R.id.radio_spinner:
                radioSpecificDays.setChecked(false);
                radioSpinner.setSelected(!radioSpinner.isChecked());
                fromDate.setText(getString(R.string.from_hint));
                toDate.setText(getString(R.string.to_hint));
                break;
            case R.id.radio_specificDays:
                radioSpinner.setChecked(false);
                radioSpecificDays.setSelected(!radioSpecificDays.isChecked());
                spinnerDays.setText(getString(R.string.select));
                break;
            case R.id.fromDate:
                radioSpecificDays.setChecked(true);
                radioSpinner.setChecked(false);
                if (radioSpecificDays.isChecked()) {
                    radioSpecificDays.setChecked(true);
                }
                dateType = getResources().getString(R.string.txt_startDate);
                showDateDialog(fromDate, BySearch.this, getResources().getString(R.string.from_date_title)).show();
                break;
            case R.id.toDate:
                radioSpecificDays.setChecked(true);
                radioSpinner.setChecked(false);
                if (radioSpecificDays.isChecked()) {
                    radioSpecificDays.setChecked(true);
                }
                dateType = getResources().getString(R.string.txt_endDate);
                showDateDialog(toDate, BySearch.this, getResources().getString(R.string.to_date_title)).show();
                break;
            case R.id.ed_dob:
                dateType = "dob";
                DatePickerDialog datePickerDialog = showDateDialog(edDOB, BySearch.this, null);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
            case R.id.spinner_date:
                radioSpinner.setChecked(true);
                radioSpecificDays.setChecked(false);
                showPopupMenu(R.menu.menu_dates, spinnerDays, spinnerDays);
                break;
            case R.id.btn_search:
                getActivity().startActivity(new Intent(getActivity(), ResultViewActivity.class).putExtra(ResultViewActivity.SEARCH_KEY, getSearchModel()));
                break;
            case R.id.resetButton:
                reset();
                break;
            case R.id.txt_reset_date:
                resetDate();
                break;
        }
    }

    private void resetDate() {
        radioSpecificDays.setChecked(false);
        radioSpinner.setChecked(false);
        spinnerDays.setText(getString(R.string.select));
        fromDate.setText(getString(R.string.from_hint));
        toDate.setText(getString(R.string.to_hint));
    }

    private SearchModel getSearchModel() {
        SearchModel searchModel = new SearchModel();
        String keyword = edKeyword.getText().toString().trim();
        searchModel.setKeyword(keyword.equals(getString(R.string.enter_keyword)) ? "" : keyword);
        searchModel.setName(Utils.getText(edName));
        searchModel.setId(Utils.getText(edId));
        searchModel.setPhoneNumber(Utils.getText(edPhoneNumber));
        searchModel.setEmailId(Utils.getText(edEmail));
        String gender = spinnerGender.getText().toString();
        searchModel.setGender(gender.equals(getString(R.string.select)) ? "" : gender);
        String dob = edDOB.getText().toString();
        searchModel.setDob(dob.equals(getString(R.string.dd_mmm_yyyy)) ? "" : dob);
        String encounter = txtEncounter.getText().toString();
        searchModel.setEncounter(encounter.equals(getString(R.string.select)) ? "" : encounter);
        if (radioSpecificDays.isChecked()) {
            searchModel.setOrderStartDate(Utils.getText(fromDate));
            searchModel.setOrderEndDate(Utils.getText(toDate));
        } else if (radioSpinner.isChecked()) {
            String text = SpecificDaysEnum.getCodeFromValue(spinnerDays.getText());
            if (text.equalsIgnoreCase(getString(R.string.select))) {

            } else if (text.equalsIgnoreCase(getString(R.string.today))) {
                searchModel.setOrderStartDate(DateTimeUtil.getCurrentDate(new Date(System.currentTimeMillis())));
            } else if (text.equalsIgnoreCase(getString(R.string.yesterday))) {
                DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                cal.set(Calendar.HOUR, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                searchModel.setOrderStartDate(dateFormat.format(cal.getTime()));

            } else if (text.equalsIgnoreCase(getString(R.string.this_week))) {
                DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 1);
                cal.set(Calendar.HOUR, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                searchModel.setOrderEndDate(dateFormat.format(cal.getTime()));
                cal.add(Calendar.DATE, -7);
                searchModel.setOrderStartDate(dateFormat.format(cal.getTime()));
            } else if (text.equalsIgnoreCase(getString(R.string.last_week))) {
                DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_WEEK) - 2) - Calendar.DAY_OF_WEEK);
                searchModel.setOrderStartDate(dateFormat.format(cal.getTime()));
                cal.add(Calendar.DATE, Calendar.DAY_OF_WEEK - 1);
                searchModel.setOrderEndDate(dateFormat.format(cal.getTime()));
            } else if (text.equalsIgnoreCase(getString(R.string.this_month))) {
                DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_MONTH) - 1));
                cal.set(Calendar.HOUR, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                searchModel.setOrderStartDate(dateFormat.format(cal.getTime()));
                cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 1);
//                cal.add(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
                searchModel.setOrderEndDate(dateFormat.format(cal.getTime()));
            } else if (text.equalsIgnoreCase(getString(R.string.last_month))) {
                DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_MONTH) - 1) - cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                cal.set(Calendar.HOUR, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                searchModel.setOrderStartDate(dateFormat.format(cal.getTime()));
                cal.add(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
                searchModel.setOrderEndDate(dateFormat.format(cal.getTime()));
            }
        }
        searchModel.setOrderName(Utils.getText(edOrderName));
        searchModel.setOrderId(Utils.getText(edOrderId));
        String oderType = spinnerType.getText().toString();
        searchModel.setOrderType(oderType.equals(getString(R.string.select)) ? "" : oderType);
        String status = spinnerStatus.getText().toString();
        searchModel.setOrderStatus(status.equals(getString(R.string.select)) ? "" : status);
        String oderModality = spinnerModality.getText().toString();
        searchModel.setModality(oderModality.equals(getString(R.string.select)) ? "" : oderModality);
        searchModel.setOrderPhysicianName(Utils.getText(edOrderPhysician));
        searchModel.setOrderLocationName(Utils.getText(edLocation));
        searchModel.setOrderBy(Utils.getText(uploadedBy));
        searchModel.setByBulkUpload(cbBulk.isChecked());

        return searchModel;
    }

    private void reset() {
        edKeyword.setText("");
        edName.setText("");
        edId.setText("");
        edPhoneNumber.setText("");
        edEmail.setText("");
        spinnerGender.setText(getString(R.string.select));
        edDOB.setText(getString(R.string.dd_mmm_yyyy));
        txtEncounter.setText("");
        radioSpinner.setChecked(false);
        radioSpecificDays.setChecked(false);
        spinnerDays.setText(getString(R.string.select));
        fromDate.setText(getString(R.string.from_hint));
        toDate.setText(getString(R.string.to_hint));
        edOrderName.setText("");
        edOrderId.setText("");
        spinnerType.setText(getString(R.string.select));
        spinnerStatus.setText(getString(R.string.select));
        spinnerModality.setText(getString(R.string.select));
        edOrderPhysician.setText("");
        edLocation.setText("");
        uploadedBy.setText("");
        cbBulk.setChecked(false);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String date = null;
        if (view.isShown()) {
            date = DateTimeUtil.getFormattedDateWithoutUTC(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(fromDate.getWindowToken(), 0);
            if (dateType.equals("dob")) {
                edDOB.setText(date);
                return;
            }
            if (!compareStartAndEndDate(dateType, date)) {
                if (dateType.equals(getResources().getString(R.string.txt_endDate))) {
                    toDate.setError(this.getResources().getString(R.string.enddate_after));
                    Toast.makeText(getActivity(), this.getResources().getString(R.string.enddate_after), Toast.LENGTH_SHORT).show();
                } else if (dateType.equals(getResources().getString(R.string.txt_startDate))) {
                    fromDate.setError(this.getResources().getString(R.string.startdate_bfore));
                    Toast.makeText(getActivity(), this.getResources().getString(R.string.startdate_bfore), Toast.LENGTH_SHORT).show();
                }
                fromDate.setText("");
            } else {
                if (dateType.equals(getResources().getString(R.string.txt_endDate))) {
                    toDate.setText(date);
                    toDate.setError(null);
                } else if (dateType.equals(getResources().getString(R.string.txt_startDate))) {
                    fromDate.setText(date);
                    fromDate.setError(null);
                }
            }
        }
    }

    public boolean compareStartAndEndDate(String type, String date) {
        if (type.equals(getResources().getString(R.string.txt_endDate))) {
            return Utils.compareStartAndEndDate(fromDate.getText().toString().trim(), date);
        } else {
            return Utils.compareStartAndEndDate(date, toDate.getText().toString().trim());
        }
    }
}
