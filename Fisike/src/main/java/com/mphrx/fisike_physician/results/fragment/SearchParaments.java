package com.mphrx.fisike_physician.results.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.icomoon.IcoMoonUtil;
import com.mphrx.fisike.record_screen.fragment.GenralFragment;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.results.Observer.MakeApiCallObserver;
import com.mphrx.fisike_physician.utils.BusProvider;

import java.net.URLEncoder;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kailashkhurana on 07/07/17.
 */

public class SearchParaments extends PayloadJson implements DatePickerDialog.OnDateSetListener ,CompoundButton.OnCheckedChangeListener,View.OnClickListener {

    private DatePickerDialog datePickerDialog;
    private String fromDateTemp = "";
    private String toDateTemp = "", category = "", modality = "", location = "";

    private ToggleButton cbLab, cbXRay, cbPrescription, cbNotes, last, _24hours, _7days, _1month;

    private int from_to_view_id;

    private ClearableEditText fromEditText, toEditText;
    private Spinner modalitySpinner;
    private EditText locationEditText;

    private RelativeLayout container;
    private ScrollView scrollView;
    private CustomFontTextView lab, rad, others;
    private android.support.v7.widget.CardView modalityFieldLayout;

    private Animation animHide, animShow;

//    public FloatingActionMenu fabMenu;

    public SwipeRefreshLayout swipeRefreshLayout;
    public SearchView searchView;
    public MenuItem searchItem;
    public boolean isLoadingContacts = false;
    public ArrayList<String> searchParam;
    public boolean isToRefresh;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initPopupView();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initPopupView() {
        container = (RelativeLayout) getActivity().findViewById(R.id.rl_dialog_container);
        fromEditText = (ClearableEditText) getActivity().findViewById(R.id.fromDate);
        toEditText = (ClearableEditText) getActivity().findViewById(R.id.toDate);

        fromEditText.setEditTextSize(14);
        toEditText.setEditTextSize(14);

        fromEditText.getEditText().setFocusableInTouchMode(false);
        toEditText.getEditText().setFocusableInTouchMode(false);

        modalitySpinner = (Spinner) getActivity().findViewById(R.id.modalitySpinner);
        locationEditText = (EditText) getActivity().findViewById(R.id.locationEditText);

        modalityFieldLayout = (android.support.v7.widget.CardView) getActivity().findViewById(R.id.modalityFieldLayout);

        modalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stub
                modality = (String) modalitySpinner.getAdapter().getItem(arg2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        fromEditText.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                from_to_view_id = R.id.fromDate;
                showDatePickerDialog();

                if (!last.isChecked()) {
                    last.setChecked(true);
                }


            }
        });

        fromEditText.getclearTextButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                fromDateTemp = "";
                fromEditText.setText(fromDateTemp);
            }
        });

        toEditText.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                from_to_view_id = R.id.toDate;
                showDatePickerDialog();
                if (!last.isChecked()) {
                    last.setChecked(true);
                }
            }


        });

        toEditText.getclearTextButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                toDateTemp = "";
                toEditText.setText(toDateTemp);
            }
        });


        scrollView = (ScrollView) getActivity().findViewById(R.id.adv_search_container);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                // TODO Auto-generated method stub
                if (container.getVisibility() == View.VISIBLE)
                    swipeRefreshLayout.setEnabled(false);

            }
        });

        getActivity().findViewById(R.id.iv_done_popup_button).setOnClickListener(this);
        getActivity().findViewById(R.id.searchButton).setOnClickListener(this);
        getActivity().findViewById(R.id.resetButton).setOnClickListener(this);
        getActivity().findViewById(R.id.iv_hide_popup_button).setOnClickListener(this);

        cbLab = (ToggleButton) getActivity().findViewById(R.id.cbLab);
        cbXRay = (ToggleButton) getActivity().findViewById(R.id.cbRay);
        cbNotes = (ToggleButton) getActivity().findViewById(R.id.cbNotes);
        cbPrescription = (ToggleButton) getActivity().findViewById(R.id.cbPrescription);

        cbLab.setTypeface(IcoMoonUtil.getTypeFace(getContext()));
        cbXRay.setTypeface(IcoMoonUtil.getTypeFace(getContext()));
        cbNotes.setTypeface(IcoMoonUtil.getTypeFace(getContext()));
        cbPrescription.setTypeface(IcoMoonUtil.getTypeFace(getContext()));

        lab = (CustomFontTextView) getActivity().findViewById(R.id.lab_reportTxt);
        rad = (CustomFontTextView) getActivity().findViewById(R.id.rad_reportTxt);
        others = (CustomFontTextView) getActivity().findViewById(R.id.otherTxt);

        last = (ToggleButton) getActivity().findViewById(R.id.last);
        _24hours = (ToggleButton) getActivity().findViewById(R.id.last24);
        _7days = (ToggleButton) getActivity().findViewById(R.id.last7);
        _1month = (ToggleButton) getActivity().findViewById(R.id.last1);

        cbLab.setOnCheckedChangeListener(this);
        cbXRay.setOnCheckedChangeListener(this);
        cbNotes.setOnCheckedChangeListener(this);
        cbPrescription.setOnCheckedChangeListener(this);

        last.setOnCheckedChangeListener(this);
        _1month.setOnCheckedChangeListener(this);
        _24hours.setOnCheckedChangeListener(this);
        _7days.setOnCheckedChangeListener(this);



        BusProvider.getInstance().register(this);

        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_hide);

        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_show);

        animHide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (BuildConfig.isPatientApp && BuildConfig.isMedicationEnabled) {
                    MyHealth myHealthFragment = (MyHealth) getParentFragment();
                    String generalFragment = Utils.getMyHealthTabNameBasedOnPosition(myHealthFragment.getCurrentFragmentVisible());
                    if (myHealthFragment.getMyHelthAdapter().getFragment(generalFragment) instanceof GenralFragment) {
//                        if (fabMenu.getVisibility() != View.VISIBLE) {
//                            fabMenu.setVisibility(View.VISIBLE);
//                        }
                    }
                }
            }
        });

        animShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                if (BuildConfig.isPatientApp && BuildConfig.isMedicationEnabled)
//                    fabMenu.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if (datePicker.isShown()) {
            if (from_to_view_id == R.id.fromDate) {
                fromEditText.getEditText().setText(day + " " + new DateFormatSymbols().getMonths()[month] + " " + year);
                fromDateTemp = fromEditText.getText().trim();


                try {
                    if ((new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(fromDateTemp).getTime()) > (System.currentTimeMillis() - 1000)) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.future_date), Toast.LENGTH_LONG).show();
                        fromEditText.setText("");
                        fromDateTemp = fromEditText.getText().trim();
                        return;
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }


                if (!toDateTemp.equals("")) {
                    try {
                        if ((new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(fromDateTemp)).after(new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(toDateTemp))) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.from_date), Toast.LENGTH_LONG).show();
                            fromEditText.setText("");
                            fromDateTemp = fromEditText.getText();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            } else if (from_to_view_id == R.id.toDate) {
                toEditText.getEditText().setText(day + " " + new DateFormatSymbols().getMonths()[month] + " " + year);
                toDateTemp = toEditText.getText().trim();

                try {
                    if ((new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(toDateTemp).getTime()) > (System.currentTimeMillis() - 1000)) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.future_date), Toast.LENGTH_LONG).show();
                        toEditText.setText("");
                        toDateTemp = toEditText.getText().trim();
                        return;
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                if (!fromDateTemp.equals("")) {
                    try {
                        if ((new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(fromDateTemp)).after(new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(toDateTemp))) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.to_date), Toast.LENGTH_LONG).show();
                            toEditText.setText("");
                            toDateTemp = toEditText.getText().trim();
                        }
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Calendar cal = Calendar.getInstance();
        if (isChecked && buttonView.getTag().equals(getResources().getString(R.string.txt_Category))) {
            if (buttonView != cbLab) {
                cbLab.setChecked(false);
            }
            if (buttonView != cbXRay) {
                cbXRay.setChecked(false);
            }
            if (buttonView != cbNotes) {
                cbNotes.setChecked(false);
            }
            if (buttonView != cbPrescription) {
                cbPrescription.setChecked(false);
            }

        } else if (isChecked && buttonView.getTag().equals(getResources().getString(R.string.txt_Date))) {
            if (buttonView != last) {
                last.setChecked(false);
            }
            if (buttonView != _1month) {
                _1month.setChecked(false);
            }
            if (buttonView != _24hours) {
                _24hours.setChecked(false);
            }
            if (buttonView != _7days) {
                _7days.setChecked(false);
            }

        }


        switch (buttonView.getId()) {
            case R.id.cbLab:
                modalitySpinner.setSelection(0);
                modalityFieldLayout.setVisibility(View.GONE);
                if (isChecked) {
                    category = getResources().getString(R.string.txt_lab);
                    lab.setTextColor(getResources().getColor(R.color.action_bar_bg));
                    cbLab.setTextColor(getResources().getColor(R.color.action_bar_bg));
                } else {
                    category = "";
                    lab.setTextColor(getResources().getColor(R.color.blue_grey));
                    cbLab.setTextColor(getResources().getColor(R.color.blue_grey));
                }

                break;

            case R.id.cbRay:

                if (isChecked) {
                    category = getResources().getString(R.string.txt_radiology);
                    modalityFieldLayout.setVisibility(View.VISIBLE);
                    rad.setTextColor(getResources().getColor(R.color.action_bar_bg));
                    cbXRay.setTextColor(getResources().getColor(R.color.action_bar_bg));
                } else {
                    category = "";
                    modalityFieldLayout.setVisibility(View.GONE);
                    modalitySpinner.setSelection(0);
                    rad.setTextColor(getResources().getColor(R.color.blue_grey));
                    cbXRay.setTextColor(getResources().getColor(R.color.blue_grey));
                }

                break;

            case R.id.cbNotes:
                modalitySpinner.setSelection(0);
                modalityFieldLayout.setVisibility(View.GONE);
                if (isChecked)
                    category = getResources().getString(R.string.txt_note);
                else
                    category = "";
                break;

            case R.id.cbPrescription:
                modalitySpinner.setSelection(0);
                modalityFieldLayout.setVisibility(View.GONE);
                if (isChecked) {
                    category = getResources().getString(R.string.txt_other);
                    others.setTextColor(getResources().getColor(R.color.action_bar_bg));
                    cbPrescription.setTextColor(getResources().getColor(R.color.action_bar_bg));
                } else {
                    category = "";
                    others.setTextColor(getResources().getColor(R.color.blue_grey));
                    cbPrescription.setTextColor(getResources().getColor(R.color.blue_grey));
                }
                break;

            case R.id.last:
                fromDateTemp = "";
                toDateTemp = "";
                fromEditText.setText(fromDateTemp);
                toEditText.setText(toDateTemp);
                break;
            case R.id.last24:
                if (isChecked) {
                    fromDateTemp = "";
                    toDateTemp = "";


                    fromEditText.setText(fromDateTemp);
                    toEditText.setText(toDateTemp);


                    toDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());
                    cal.add(Calendar.DAY_OF_YEAR, -1);
                    fromDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());

                } else {
                    fromDateTemp = "";
                    toDateTemp = "";
                }


                break;
            case R.id.last7:

                if (isChecked) {
                    fromDateTemp = "";
                    toDateTemp = "";
                    fromEditText.setText(fromDateTemp);
                    toEditText.setText(toDateTemp);

                    toDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());
                    cal.add(Calendar.DAY_OF_YEAR, -7);
                    fromDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());

                } else {
                    fromDateTemp = "";
                    toDateTemp = "";
                }


                break;
            case R.id.last1:
                if (isChecked) {
                    fromEditText.setText(fromDateTemp);
                    toEditText.setText(toDateTemp);

                    toDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());
                    cal.add(Calendar.DAY_OF_YEAR, -30);
                    fromDateTemp = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(cal.getTime());
                } else {
                    fromDateTemp = "";
                    toDateTemp = "";
                }
                break;

            default:
                break;
        }
    }

    public void showDatePickerDialog() {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        // Create a new instance of DatePickerDialog and return it
        // datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void reset() {
        // TODO Auto-generated method stub
        category = "";
        fromDateTemp = "";
        toDateTemp = "";
        location = "";

        locationEditText.setText(location);
        fromEditText.setText(fromDateTemp);
        toEditText.setText(toDateTemp);
        modalitySpinner.setSelection(0);

        cbLab.setChecked(false);
        cbXRay.setChecked(false);
        cbNotes.setChecked(false);
        cbPrescription.setChecked(false);

        last.setChecked(false);
        _1month.setChecked(false);
        _24hours.setChecked(false);
        _7days.setChecked(false);
    }

    public void onClick(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        switch (view.getId()){
            case R.id.iv_hide_popup_button:
                swipeRefreshLayout.setEnabled(true);
                container.setVisibility(View.GONE);
                container.startAnimation(animHide);
                mgr.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                break;
            case R.id.iv_done_popup_button:
                if (advanceSearchFromForm()) {
                    container.setVisibility(View.GONE);
                    container.startAnimation(animHide);
                }
                mgr.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                break;
            case R.id.searchButton:

                if (advanceSearchFromForm()) {
                    container.setVisibility(View.GONE);
                    container.startAnimation(animHide);
                }

                mgr.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                break;
            case R.id.resetButton:
                reset();
                break;
            default:
            if (BuildConfig.isPatientApp) {
//                if (container == null || container.getVisibility() == View.VISIBLE)
//                    fabMenu.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case TextConstants.MENU_ITEM_FILTERS:
                container.setVisibility(View.VISIBLE);
                container.startAnimation(animShow);
                if (!searchView.isIconified()) {
                    searchItem.collapseActionView();
                }
                break;
            case R.id.action_search_records:
                if (container.getVisibility() == View.VISIBLE) {
                    container.setVisibility(View.GONE);
                    container.startAnimation(animHide);
                }
                break;
        }
        return true;
    }

    public boolean isAdvanceSearchQuery() {
        if (searchParam.isEmpty() && toDateTemp.equalsIgnoreCase("") && fromDateTemp.equalsIgnoreCase("") && queryJsonObject == null)
            return false;
        else
            return true;
    }

    public void hideAdvanceSearchForm() {
        if (container.getVisibility() == View.VISIBLE) {
            container.setVisibility(View.GONE);
            container.startAnimation(animHide);
        }
    }
    private boolean advanceSearchFromForm() {
        // TODO Auto-generated method stub

        if (fromDateTemp.equals("") || toDateTemp.equals("")) {
            if (!(fromDateTemp.equals("") && toDateTemp.equals(""))) {
                Toast.makeText(getActivity(), getResources().getString(R.string.to_from_combi), Toast.LENGTH_LONG).show();
                return false;
            } else
                searchParam.removeAll(searchParam);

        } else {
            searchParam.removeAll(searchParam);
            try {

                SimpleDateFormat f = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());

                fromDate = URLEncoder.encode(f.format(new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(fromDateTemp)));
                toDate = URLEncoder.encode(f.format(new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).parse(toDateTemp)));

//                searchParam.add("SearchTag.scheduledDateTo|" + URLEncoder.encode(f.format(new SimpleDateFormat("dd MMM yyyy").parse(toDate))));
//                searchParam.add("SearchTag.scheduledDateFrom|" + URLEncoder.encode(f.format(new SimpleDateFormat("dd MMM yyyy").parse(fromDate)) ));


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        //getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        //getActivity().findViewById(R.id.toolbar).animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));


        if (!category.equals("")) {
            searchParam.add("SearchTag.category|" + category);
        }


        //modality = (String) modalitySpinner.getSelectedItem();
        if (!modality.equals("") && !modality.equalsIgnoreCase("Select Modality") && (modalityFieldLayout.getVisibility() == View.VISIBLE)) {
            searchParam.add("SearchTag.modality|" + modality);
        }

        location = locationEditText.getText().toString().trim();
        if (!location.equals("")) {
            searchParam.add("SearchTag.performingLocationName|" + location);
        }

        MakeApiCallObserver.getInstance().callNotify();


        return true;

    }

}
