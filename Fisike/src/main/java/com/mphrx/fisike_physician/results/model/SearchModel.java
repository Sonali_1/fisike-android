package com.mphrx.fisike_physician.results.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.utils.Utils;

import careplan.model.CareTaskModel;

/**
 * Created by kailashkhurana on 14/07/17.
 */

public class SearchModel implements Parcelable {

    private String keyword;
    private String name;
    private String id;
    private String phoneNumber;
    private String emailId;
    private String gender;
    private String dob;
    private String encounter;
    private String orderStartDate;
    private String orderEndDate;
    private String orderName;
    private String orderId;
    private String orderType;
    private String orderStatus;
    private String modality;
    private String orderPhysicianName;
    private String orderLocationName;
    private String orderBy;
    private Boolean isByBulkUpload;

    public SearchModel() {

    }

    protected SearchModel(Parcel in) {
        keyword = in.readString();
        name = in.readString();
        id = in.readString();
        phoneNumber = in.readString();
        emailId = in.readString();
        gender = in.readString();
        dob = in.readString();
        encounter = in.readString();
        orderStartDate = in.readString();
        orderEndDate = in.readString();
        orderName = in.readString();
        orderId = in.readString();
        orderType = in.readString();
        orderStatus = in.readString();
        modality = in.readString();
        orderPhysicianName = in.readString();
        orderLocationName = in.readString();
        orderBy = in.readString();
        byte isEnableVal = in.readByte();
        isByBulkUpload = isEnableVal == 0x02 ? false : isEnableVal != 0x00;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(keyword);
        parcel.writeString(name);
        parcel.writeString(id);
        parcel.writeString(phoneNumber);
        parcel.writeString(emailId);
        parcel.writeString(gender);
        parcel.writeString(dob);
        parcel.writeString(encounter);
        parcel.writeString(orderStartDate);
        parcel.writeString(orderEndDate);
        parcel.writeString(orderName);
        parcel.writeString(orderId);
        parcel.writeString(orderType);
        parcel.writeString(orderStatus);
        parcel.writeString(modality);
        parcel.writeString(orderPhysicianName);
        parcel.writeString(orderLocationName);
        parcel.writeString(orderBy);
        if (isByBulkUpload == null) {
            parcel.writeByte((byte) (0x02));
        } else {
            parcel.writeByte((byte) (isByBulkUpload ? 0x01 : 0x00));
        }
    }

    public static final Creator<SearchModel> CREATOR = new Creator<SearchModel>() {
        @Override
        public SearchModel createFromParcel(Parcel in) {
            return new SearchModel(in);
        }

        @Override
        public SearchModel[] newArray(int size) {
            return new SearchModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEncounter() {
        return encounter;
    }

    public void setEncounter(String encounter) {
        this.encounter = encounter;
    }

    public String getOrderStartDate() {
        if(!Utils.isValueAvailable(orderStartDate)){
            return "";
        }
        return orderStartDate;
    }

    public void setOrderStartDate(String orderStartDate) {
        this.orderStartDate = orderStartDate;
    }

    public String getOrderEndDate() {
        if(!Utils.isValueAvailable(orderEndDate)){
            return "";
        }
        return orderEndDate;
    }

    public void setOrderEndDate(String orderEndDate) {
        this.orderEndDate = orderEndDate;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getModality() {
        return modality;
    }

    public void setModality(String modality) {
        this.modality = modality;
    }

    public String getOrderPhysicianName() {
        return orderPhysicianName;
    }

    public void setOrderPhysicianName(String orderPhysicianName) {
        this.orderPhysicianName = orderPhysicianName;
    }

    public String getOrderLocationName() {
        return orderLocationName;
    }

    public void setOrderLocationName(String orderLocationName) {
        this.orderLocationName = orderLocationName;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public Boolean getByBulkUpload() {
        return isByBulkUpload;
    }

    public void setByBulkUpload(Boolean byBulkUpload) {
        isByBulkUpload = byBulkUpload;
    }

    @Override
    public String toString() {
        return keyword + name + id + phoneNumber + emailId + gender + dob + encounter + getOrderStartDate() + getOrderEndDate() + orderName + orderId + orderType + modality + orderStatus + orderPhysicianName + orderLocationName + orderBy + (isByBulkUpload?"true":"");
    }
}
