package com.mphrx.fisike_physician.results.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.adapter.ResultListAdapter;
import com.mphrx.fisike.record_screen.fragment.GenralFragment;
import com.mphrx.fisike.record_screen.view_holder.DiagnosticOrderViewHolder;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;
import com.mphrx.fisike_physician.results.fragment.ResultSearchFragment;
import com.mphrx.fisike_physician.results.fragment.SearchParaments;

/**
 * Created by kailashkhurana on 06/07/17.
 */

public class ResultViewActivity extends BaseActivity implements ResultListAdapter.DownloadListner {

    public static final String SEARCH_KEY = "SEARCH_KEY ";
    private static final int DOWNLOAD_RESULT = 10001;
    private FrameLayout frameLayout;
    private Toolbar mToolbar;
    private Bundle bundle;
    private GenralFragment genralFragment;
    private CustomFontTextView toolbar_title;
    private ImageButton btnRight;
    private String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.care_task_activity, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        bundle = getIntent().getExtras();

        initializeToolbar();

        genralFragment = new GenralFragment();
        genralFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, genralFragment, "genralFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResultViewActivity.this.finish();
            }
        });
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        btnRight = (ImageButton) mToolbar.findViewById(R.id.toolbar_right_img);
        btnRight.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) toolbar_title.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, btnRight.getId());

        toolbar_title.setLayoutParams(params);
        toolbar_title.setText(getResources().getText(R.string.result_searched));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View view) {
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
        if (fragmentById instanceof GenralFragment) {
            ((GenralFragment) fragmentById).onClick(view);
        }
    }

    @Override
    public void itemClicked(String orderId, IconTextView imgOverFlow) {
        showPopupMenu(imgOverFlow, R.menu.menu_file_download, orderId);
    }

    public void showPopupMenu(View view, final int menuId, final String orderId) {
        this.orderId = orderId;
        final android.support.v7.widget.PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.report_download:
                        if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), DOWNLOAD_RESULT))
                            fileDownload(orderId);
                        break;
                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void fileDownload(String orderId) {
        String payload = "{\"constraints\":{\"diagnosticOrderId\":\"" + orderId + "\"}}";
        if (Utils.isNetworkAvailable(this)) {
            new DownloadRecordFileRequest(this, 0, VariableConstants.MIME_TYPE_FILE, MphRxUrl.getReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPermissionNotGranted() {
        super.onPermissionNotGranted();
    }

    @Override
    public void onPermissionGranted(String permission) {
        fileDownload(orderId);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case DOWNLOAD_RESULT:
                    onPermissionGranted(permissions[0]);
                    break;
            }
        } else
            onPermissionNotGranted();
    }


}