package com.mphrx.fisike_physician.results.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.util.ArrayList;

/**
 * Created by kailashkhurana on 06/07/17.
 */

public class ResultSearchFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener {


    public static final int SEARCH_RECORD = 0;
    private static final int LOOKUP_RECORD = 1;
    private View myFragmentView;
    private ArrayList<Fragment> fragmentMap = new ArrayList<>();
    private ViewPager viewPager;
//    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_result_filter, container, false);

        BaseActivity.bt_toolbar_icon.setVisibility(View.VISIBLE);
        BaseActivity.bt_toolbar_icon.setOnClickListener(this);
        BaseActivity.bt_toolbar_icon.setText(R.string.fa_info);
        BaseActivity.bt_toolbar_icon.setTextSize(32);
        ((BaseActivity) getActivity()).setHamburgerIcon();
        setHasOptionsMenu(true);
        findViews();
        initView();

        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_SEARCH_FRAGMENT);

        return myFragmentView;
    }

    private void findViews() {
        viewPager = (ViewPager) myFragmentView.findViewById(R.id.viewpager);
//        tabLayout = (TabLayout) myFragmentView.findViewById(R.id.tabs);
    }


    private void initView() {
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Fragment getItem(int position) {
                Fragment fragment;
                if (position == SEARCH_RECORD) {
                    fragment = new BySearch();
                } else {
                    fragment = new ByLookup();
                }
                fragmentMap.add(position, fragment);
                return fragment;

            }


            @Override
            public CharSequence getPageTitle(int position) {
                CharSequence pageTitle = null;
                switch (position) {
                    case SEARCH_RECORD:
                        pageTitle = getResources().getString(R.string.search);
                        break;
                    case LOOKUP_RECORD:
                        pageTitle = getResources().getString(R.string.lookup);
                        break;
                }
                return pageTitle;
            }


        });

//        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case SEARCH_RECORD:
                if (fragmentMap.get(LOOKUP_RECORD) != null) {
                    ((ByLookup) fragmentMap.get(LOOKUP_RECORD)).setSearchClick();
                }
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_SEARCH_FRAGMENT);
                break;
            case LOOKUP_RECORD:
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_LOOKUP_FRAGMENT);
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        Fragment fragment;
        if (viewPager.getCurrentItem() == SEARCH_RECORD) {
            fragment = ((BySearch) fragmentMap.get(SEARCH_RECORD));
        } else {
            fragment = ((ByLookup) fragmentMap.get(LOOKUP_RECORD));
        }
        switch (v.getId()) {
            case R.id.bt_toolbar_icon:
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getString(R.string.disclaimer), getActivity().getString(R.string.disclaimer_body), getActivity().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
        }
        if (viewPager.getCurrentItem() == SEARCH_RECORD && fragment instanceof BySearch) {
            ((BySearch) fragment).onClick(v);
        } else if (viewPager.getCurrentItem() == LOOKUP_RECORD && fragment instanceof ByLookup) {
            ((ByLookup) fragment).onClick(v);
        }
    }


    public ViewPager getViewPager() {
        return viewPager;
    }
}
