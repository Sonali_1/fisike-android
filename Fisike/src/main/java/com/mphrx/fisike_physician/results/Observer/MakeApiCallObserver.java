package com.mphrx.fisike_physician.results.Observer;

import java.util.Observable;

/**
 * Created by kailashkhurana on 10/07/17.
 */

public class MakeApiCallObserver extends Observable {

    private static MakeApiCallObserver makeApiCallObserver;

    public static MakeApiCallObserver getInstance() {
        if (makeApiCallObserver == null) {
            makeApiCallObserver = new MakeApiCallObserver();
        }
        return makeApiCallObserver;
    }

    private MakeApiCallObserver() {

    }

    public void callNotify() {
        setChanged();
        notifyObservers();
    }
}
