package com.mphrx.fisike_physician.results.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.results.activity.ResultViewActivity;
import com.mphrx.fisike_physician.results.model.LookupSearchModel;

import java.util.ArrayList;

/**
 * Created by kailashkhurana on 07/07/17.
 */

public class ByLookup extends SearchBaseFragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private String reason;
    private String comment;
    private View myFragmentView;
    private CustomFontEditTextView edGivenName;
    private CustomFontEditTextView edFamilyName;
    private CustomFontEditTextView edDOB;
    private CustomFontEditTextView spinnerGender;
    private ArrayList<String> listBTGReason;
    private CustomFontEditTextView edReason;
    private RelativeLayout layoutBtg;
    private RelativeLayout layoutLookup;
    private CustomFontEditTextView edDescription;
    private ScrollView scrollView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.layout_lookup_view, container, false);

        listBTGReason = new ArrayList<>();
        listBTGReason.add(getString(R.string.btg_emrgency));
        listBTGReason.add(getString(R.string.btg_necessary));
        listBTGReason.add(getString(R.string.btg_verbal));

        findView();
        initView();

        return myFragmentView;
    }


    public void findView() {
        edReason = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_reason);
        edDescription = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_description);
        layoutLookup = (RelativeLayout) myFragmentView.findViewById(R.id.layout_lookup);
        edGivenName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_given_name);
        edFamilyName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_family_name);
        spinnerGender = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_gender);
        edDOB = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_dob);
        layoutBtg = (RelativeLayout) myFragmentView.findViewById(R.id.layout_btg);
        scrollView = (ScrollView) myFragmentView.findViewById(R.id.scroll_view);
    }

    public void initView() {
        edDescription.getEditText().setSingleLine(false);
        edDescription.getEditText().setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        edDescription.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        edDescription.getEditText().setMinLines(1);
        edDescription.getEditText().setVerticalScrollBarEnabled(true);
        edDescription.getEditText().setMovementMethod(ScrollingMovementMethod.getInstance());
        edDescription.getEditText().setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);

        setSpinnerProperty(edReason, 0, R.drawable.dropdown);
        edReason.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(edReason);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerGender, 0, R.drawable.dropdown);
        spinnerGender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerGender);
                }
                return true;
            }
        });

        setSpinnerProperty(edDOB, 0, R.drawable.calendar);
        edDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(edDOB);
                }
                return true;
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (Utils.isKeyBoardOpen(getActivity())) {
                    Utils.hideKeyboard(getActivity());
                }
                return false;
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.spinner_gender:
                showPopupMenu(R.menu.menu_gender, spinnerGender, spinnerGender);
                break;
            case R.id.btn_search:
                LookupSearchModel lookUpModel = getLookUpModel();
                if (isValidateData(lookUpModel))
                    getActivity().startActivity(new Intent(getActivity(), ResultViewActivity.class).putExtra(ResultViewActivity.SEARCH_KEY, lookUpModel));
                break;
            case R.id.btn_reset:
                reset();
                break;
            case R.id.ed_dob:
                showDateDialog(edDOB, ByLookup.this,null).show();
                break;
            case R.id.btn_cancel:
                ((ResultSearchFragment) getParentFragment()).getViewPager().setCurrentItem(ResultSearchFragment.SEARCH_RECORD);
                break;
            case R.id.btn_allow_access:
                Utils.hideKeyboard(getActivity());
                reason = Utils.getText(edReason);
                comment = Utils.getText(edDescription);
                if (reason.equals("")) {
                    edReason.setError(getString(R.string.error_enter_reason));
                    return;
                }
                layoutBtg.setVisibility(View.GONE);
                layoutLookup.setVisibility(View.VISIBLE);
                break;
            case R.id.ed_reason:
                showPopupMenu(listBTGReason, edReason);
                break;
        }

    }

    private boolean isValidateData(LookupSearchModel lookUpModel) {
        boolean isValidateData = true;
        if (lookUpModel.getGivenName().equals("")) {
            isValidateData = false;
            edGivenName.setError(getString(R.string.enter_given_name));
        }
        if (lookUpModel.getFamilyName().equals("")) {
            isValidateData = false;
            edFamilyName.setError(getString(R.string.enter_family_name));
        }
        if (lookUpModel.getGender().equals("")) {
            isValidateData = false;
            spinnerGender.setError(getString(R.string.enter_gender));
        }
        if (lookUpModel.getDob().equals("")) {
            isValidateData = false;
            edDOB.setError(getString(R.string.enter_dob));
        }

        return isValidateData;
    }

    private void reset() {
        edGivenName.setText("");
        edFamilyName.setText("");
        edDOB.setText("");
        spinnerGender.setText("");
    }

    private LookupSearchModel getLookUpModel() {
        LookupSearchModel lookupSearchModel = new LookupSearchModel();
        lookupSearchModel.setGivenName(Utils.getText(edGivenName));
        lookupSearchModel.setFamilyName(Utils.getText(edFamilyName));
        lookupSearchModel.setGender(Utils.getText(spinnerGender));
        lookupSearchModel.setDob(Utils.getText(edDOB));
        lookupSearchModel.setReason(reason);
        lookupSearchModel.setComment(comment);
        return lookupSearchModel;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (view.isShown()) {
            String date = DateTimeUtil.getFormattedDateWithoutUTC(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edDOB.getWindowToken(), 0);
            edDOB.setText(date);
        }
    }

    public void setSearchClick() {
        if (layoutBtg.getVisibility() == View.GONE) {
            layoutBtg.setVisibility(View.VISIBLE);
            layoutLookup.setVisibility(View.GONE);

            edReason.setText("");
            edDescription.setText("");

            reset();
        }
    }
}
