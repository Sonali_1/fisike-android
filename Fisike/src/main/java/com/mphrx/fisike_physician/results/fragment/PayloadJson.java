package com.mphrx.fisike_physician.results.fragment;

import android.support.v4.app.Fragment;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.GenderApiEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.enums.ResultStatusEnum;
import com.mphrx.fisike.enums.ResultuTypeEnum;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.results.activity.ResultViewActivity;
import com.mphrx.fisike_physician.results.model.LookupSearchModel;
import com.mphrx.fisike_physician.results.model.SearchModel;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by kailashkhurana on 20/07/17.
 */

public class PayloadJson extends Fragment {

    private static final int PAGINATION_RECORD_COUNT = 10;
    private Object obj;
    public String queryJsonObject;
    public String fromDate = "";
    public String toDate = "";
    public String syncTime;
    private JSONObject jsonObject;


    public boolean getObject() {
        if (getArguments() != null && getArguments().containsKey(ResultViewActivity.SEARCH_KEY)) {
            obj = getArguments().get(ResultViewActivity.SEARCH_KEY);
            /*if (obj instanceof LookupSearchModel) {
                Toast.makeText(getActivity(), "Lookup", Toast.LENGTH_SHORT).show();
            } else if (obj instanceof SearchModel) {
                Toast.makeText(getActivity(), "Search", Toast.LENGTH_SHORT).show();
            }*/
            return true;
        }
        return false;
    }

    public boolean isAnyDataEntered() {
        if (obj instanceof SearchModel) {
            SearchModel searchModel = ((SearchModel) obj);
            if (!searchModel.toString().equalsIgnoreCase("")) {
                return true;
            }
        }
        return false;
    }


    /**
     * Request Json for pagination
     *
     * @return
     */
    public JSONObject paginationJson(int skip) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put("_count", PAGINATION_RECORD_COUNT);
            jsonObject.put("_skip", skip);
            jsonObject.put("sendPhotoData", false);
            sortingOrder();
            if (BuildConfig.isPhysicianApp && obj != null) {
                addObjKey();
            } else {
                if (BuildConfig.isPatientApp) {
                    jsonObject.put("authParams", "showSelf");
                } else {
                    if (getArguments() != null && getArguments().containsKey("patientId")) {
                        jsonObject.put("subject", getArguments().getString("patientId"));// 116242
                    } else {
                        jsonObject.put("authParams", "showSelf");
                    }
                }
                JSONArray includeArray = new JSONArray();
                includeArray.put("DiagnosticOrder:orderer");
                includeArray.put("DiagnosticOrder:subject");
                jsonObject.put("_include", includeArray);
                jsonObject.put("_query", queryJsonObject);

                if (!fromDate.equalsIgnoreCase("")) {
                    jsonObject.put("scheduledDateFrom", fromDate);
                }

                if (!toDate.equalsIgnoreCase("")) {
                    jsonObject.put("scheduledDateTo", toDate);
                }
                //change the drStatus , status value here correspondingly
                if (BuildConfig.isToShowDoStatus)
                    jsonObject.put(TextConstants.DO_STATUS, TextConstants.STATUS_FINAL);

                if (BuildConfig.isToShowDrStatus)
                    jsonObject.put(TextConstants.DR_STATUS, TextConstants.STATUS_FINAL);
            }
            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            constraintsObject.put("syncDateRequired", BuildConfig.isPatientApp ? true : false);
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void addObjKey() {
        try {
            jsonObject.put("_include", "DiagnosticOrder:subject");
            if (obj instanceof SearchModel) {
                addSearchKey();
            } else if (obj instanceof LookupSearchModel) {
                addLookupSearch();
            }
        } catch (JSONException e) {


        }
    }

    private void addLookupSearch() {

    }

    private void addSearchKey() {
        SearchModel searchModel = ((SearchModel) obj);
        try {
            String orderId = null;
            String ordertType = null;
            String modality = null;
            String performingLocationName = null;
            String physicianName = null;
            String bulkUploader = null;
            String orderBy = null;
            String orderName = null;
            String keyword = null;

            if (Utils.isValueAvailable(searchModel.getOrderId())) {//acces
                orderId = getSplitRegex(searchModel.getOrderId().toLowerCase(), "ac");
            }
            String orderType = ResultuTypeEnum.getCodeFromValue(searchModel.getOrderType());
            if (Utils.isValueAvailable(orderType)) {
                ordertType = getSplitRegex(orderType.toLowerCase(), "c");
            }

            if (Utils.isValueAvailable(searchModel.getModality())) {
                modality = getSplitRegex(searchModel.getModality().toLowerCase(), "m");
            }

            if (Utils.isValueAvailable(searchModel.getOrderLocationName())) {
                performingLocationName = getSplitRegex(searchModel.getOrderLocationName().toLowerCase(), "pln");
            }
            if (Utils.isValueAvailable(searchModel.getOrderPhysicianName())) {
                physicianName = getSplitRegex(searchModel.getOrderPhysicianName().toLowerCase(), "opn");
            }

            if (searchModel.getByBulkUpload()) {
                JSONArray bulkUploaderArray = new JSONArray();
                bulkUploaderArray.put("SearchTag.uploadedFrom|bulkUploader");
                jsonObject.put("extension:url", bulkUploaderArray);
//                bulkUploader = "{\"$regex\":\"^uf_bulkUploader.*\"}";
            }
            if (Utils.isValueAvailable(searchModel.getOrderBy())) {
                orderBy = getSplitRegex(searchModel.getOrderBy().toLowerCase(), "ub");
            }

            if (Utils.isValueAvailable(searchModel.getOrderName())) {
                orderName = getSplitRegex(searchModel.getOrderName().toLowerCase(), "it");
            }

            if (Utils.isValueAvailable(searchModel.getKeyword())) {
                keyword = getSplitRegex(searchModel.getKeyword().toLowerCase(), "k");
            }

            String searchElementList = "{\"$and\":[{\"searchElementsList\":{\"$all\":[";
            boolean isAndEntered = false;
            if (orderId != null || ordertType != null || modality != null || performingLocationName != null || physicianName != null || orderBy != null || orderName != null || keyword != null) {
                isAndEntered = true;
                if (orderId != null) {
                    searchElementList += orderId + ", ";
                }
                if (ordertType != null) {
                    searchElementList += ordertType + ", ";
                }
                if (orderName != null) {
                    searchElementList += orderName + ", ";
                }
                if (orderBy != null) {
                    searchElementList += orderBy + ", ";
                }
                if (modality != null) {
                    searchElementList += modality + ", ";
                }
                if (performingLocationName != null) {
                    searchElementList += performingLocationName + ", ";
                }
                if (physicianName != null) {
                    searchElementList += physicianName + ", ";
                }
//                if (bulkUploader != null) {
//                    searchElementList += bulkUploader + ", ";
//                }
                if (keyword != null) {
                    searchElementList += keyword + ", ";
                }
                if (searchElementList.endsWith(", ")) {
                    searchElementList = searchElementList.substring(0, searchElementList.length() - 2);
                }
                searchElementList += "]}}]}";

            }

            if (isAndEntered) {
                jsonObject.put("_query", searchElementList);
            }

            if (Utils.isValueAvailable(searchModel.getEncounter())) {
                jsonObject.put("encounter:Encounter.identifier:text", "patientVisitNumber|" + getEncoderString(searchModel.getEncounter()));
            }

            if (Utils.isValueAvailable(searchModel.getOrderStartDate())) {
                jsonObject.put("scheduledDateFrom", DateTimeUtil.convertSourceDestinationDate(searchModel.getOrderStartDate(), DateTimeUtil
                        .destinationDateFormatWithoutTime, DateTimeUtil
                        .yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            }
            if (Utils.isValueAvailable(searchModel.getOrderEndDate())) {
                jsonObject.put("scheduledDateTo", DateTimeUtil.convertSourceDestinationDate(searchModel.getOrderEndDate(), DateTimeUtil
                        .destinationDateFormatWithoutTime, DateTimeUtil
                        .yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            }
            String orderStatus = ResultStatusEnum.getCodeFromValue(searchModel.getOrderStatus());
            if (Utils.isValueAvailable(orderStatus)) {
                jsonObject.put("status", orderStatus);
            }
            if (Utils.isValueAvailable(searchModel.getDob())) {
                jsonObject.put("subject:Patient.birthDate", DateTimeUtil.convertSourceDestinationDate(searchModel.getDob(), DateTimeUtil
                        .destinationDateFormatWithoutTime, DateTimeUtil
                        .yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            }

            JSONObject patientObject = new JSONObject();
            if (Utils.isValueAvailable(searchModel.getName())) {
                patientObject.put("name", getEncoderString(searchModel.getName()));
            }
            if (Utils.isValueAvailable(searchModel.getId())) {
                patientObject.put("patientId", getEncoderString(searchModel.getId()));
            }
            if (Utils.isValueAvailable(searchModel.getPhoneNumber())) {
                patientObject.put("phone", getEncoderString(searchModel.getPhoneNumber()));
            }
            if (Utils.isValueAvailable(searchModel.getEmailId())) {
                patientObject.put("email", getEncoderString(searchModel.getEmailId()));
            }
            String gender = GenderEnum.getCodeFromValue(searchModel.getGender());
            gender = GenderApiEnum.getCodeFromValue(gender);
            if (Utils.isValueAvailable(gender)) {
                patientObject.put("gender", getEncoderString(gender.toLowerCase()));
            }
            jsonObject.put("patientConstraints", patientObject);

        } catch (JSONException e) {
        }
    }

    private String getEncoderString(String string) {
        try {
            return URLEncoder.encode(string.toLowerCase(), TextConstants.ENCODED_FORMATE)
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
        }
        return "";
    }

    private String getSplitRegex(String orderId, String keyword) {
        String returnString = "";
        String str[] = orderId.split(" ");
        for (int i = 0; i < str.length; i++) {
            returnString += "{\"$regex\":\"^" + keyword + "_" + str[i] + ".*\"}" + (i == str.length - 1 ? "" : ", ");
        }
        return returnString;
    }

    /**
     * Request Json for Sync
     *
     * @return
     */
    public JSONObject syncJson() {
        try {
            jsonObject = new JSONObject();
            jsonObject.put("syncDate", syncTime);

            sortingOrder();
            if (BuildConfig.isPhysicianApp && obj != null) {
                addObjKey();
            } else {
                if (BuildConfig.isPatientApp) {
                    JSONArray includeArray = new JSONArray();
                    jsonObject.put("authParams", "showSelf");
                } else {
                    jsonObject.put("subject", getArguments().getString("patientId"));
                }
                JSONArray includeArray = new JSONArray();
                includeArray.put("DiagnosticOrder:orderer");
                includeArray.put("DiagnosticOrder:subject");
                jsonObject.put("_include", includeArray);

                String query = "{\"extension\":{$elemMatch:{\"url\":\"ReportAvailable\", \"value.0\":\"true\"}}}";
                jsonObject.put("_query", query.toString());

                //change the status , reportStatus , status value here correspondingly -FIS-9170(aastha)
                if (BuildConfig.isToShowDoStatus)
                    jsonObject.put(TextConstants.DO_STATUS, TextConstants.STATUS_FINAL);
                if (BuildConfig.isToShowDrStatus)
                    jsonObject.put(TextConstants.DR_STATUS, TextConstants.STATUS_FINAL);
            }
            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            constraintsObject.put("syncDateRequired", BuildConfig.isPatientApp ? true : false);
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sortingOrder() throws Exception {
        if (SharedPref.getResultFilter() == VariableConstants.RESULT_ORDERDATE_ASC) {
            jsonObject.put("_sort:asc", "scheduledDate");
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_ORDERDATE_DESC) {
            jsonObject.put("_sort:desc", "scheduledDate");
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_LAST_UPDATED_ASC) {
            jsonObject.put("_sort:asc", "lastUpdated");
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_LAST_UPDATED_DESC) {
            jsonObject.put("_sort:desc", "lastUpdated");
        }

    }
}