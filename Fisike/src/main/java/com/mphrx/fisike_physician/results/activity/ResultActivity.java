package com.mphrx.fisike_physician.results.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.results.fragment.ResultSearchFragment;

/**
 * Created by kailashkhurana on 06/07/17.
 */

public class ResultActivity extends BaseActivity {

    private FrameLayout frameLayout;
    private Toolbar mToolbar;
    private Bundle bundle;
    private ResultSearchFragment resultSearchFragment;
    private CustomFontTextView toolbar_title;
    private ImageButton btnRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.result_search_activity, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        bundle = getIntent().getBundleExtra("bundle");

        initializeToolbar();

        resultSearchFragment = new ResultSearchFragment();
        resultSearchFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, resultSearchFragment, "resultSearchFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        btnRight = (ImageButton) mToolbar.findViewById(R.id.toolbar_right_img);
        btnRight.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) toolbar_title.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, btnRight.getId());

        toolbar_title.setLayoutParams(params);

        toolbar_title.setText(getResources().getString(R.string.search_result));

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void onClick(View view) {
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
        if (fragmentById instanceof ResultSearchFragment) {
            ((ResultSearchFragment) fragmentById).onClick(view);
        }
    }

}
