package com.mphrx.fisike_physician.results.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kailashkhurana on 14/07/17.
 */

public class LookupSearchModel implements Parcelable {
    private String givenName;
    private String familyName;
    private String dob;
    private String gender;
    private String reason;
    private String comment;

    public LookupSearchModel(){

    }

    protected LookupSearchModel(Parcel in) {
        givenName = in.readString();
        familyName = in.readString();
        dob = in.readString();
        gender = in.readString();
        reason = in.readString();
        comment = in.readString();
    }

    public static final Creator<LookupSearchModel> CREATOR = new Creator<LookupSearchModel>() {
        @Override
        public LookupSearchModel createFromParcel(Parcel in) {
            return new LookupSearchModel(in);
        }

        @Override
        public LookupSearchModel[] newArray(int size) {
            return new LookupSearchModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(givenName);
        parcel.writeString(familyName);
        parcel.writeString(dob);
        parcel.writeString(gender);
        parcel.writeString(reason);
        parcel.writeString(comment);
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
