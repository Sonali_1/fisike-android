package com.mphrx.fisike_physician.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.mphrx.fisike.constant.CustomizedTextConstants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by brijesh on 25/11/15.
 */
public class SMSReceiver extends BroadcastReceiver {

    private SMSReceiverListener mListener;

    public SMSReceiver(SMSReceiverListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        if (messages == null)
            return;
        String smsData = messages.getMessageBody();
        String smsSender = messages.getOriginatingAddress();
        if (smsSender != null && smsSender.toUpperCase().contains(CustomizedTextConstants.SMS_SENDER)) {
            if (mListener != null)
                mListener.onSMSReceived(extractOTP(smsData));
        }
    }

    private String extractOTP(String data) {
         Pattern p = Pattern.compile("(|^)\\d{4}");

        String str = data;
        if(str!=null)
        {
            Matcher m = p.matcher(str);
            if(m.find()) {
                str=m.group(0);
            }
            else
            {
                str="";
            }
        }
        return str;
    }


    public interface SMSReceiverListener {
        void onSMSReceived(String otp);
    }


}
