package com.mphrx.fisike_physician.services;

import android.content.Context;

/**
 * Wrapper around existing MessengerService to ensure package difference
 */
public class MessengerService extends com.mphrx.fisike.services.MessengerService {


    @Override
    public void onCreate() {
        super.onCreate();
       // SharedPref.register(VariableConstants.AUTH_TOKEN, this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
       // SharedPref.unregister(VariableConstants.AUTH_TOKEN, this);
    }

    /*@Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(VariableConstants.AUTH_TOKEN) && TextUtils.isEmpty(SharedPref.getAccessToken())) {
          //  logoutUser(this);
        }
    }
*/
    public void logoutUser(Context context) {

       /*
        changeStatus(TextConstants.STATUS_UNAVAILABLE, SettingManager.getInstance().getUserMO().getStatusMessage());
        disconnectXMPPConnection();
        shutdownTimers();
        stopSelf();
        */

            super.logoutUser(context);
       }

}