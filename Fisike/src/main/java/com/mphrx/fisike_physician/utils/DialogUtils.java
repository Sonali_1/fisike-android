package com.mphrx.fisike_physician.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 03/11/15.
 */
public class DialogUtils {

    private static List<Dialog> sActiveDialogs = new ArrayList<>();

    private static DialogInterface.OnShowListener sDialogRegister = new DialogInterface.OnShowListener() {
        @Override
        public void onShow(DialogInterface dialog) {
            if (sActiveDialogs == null)
                return;

            synchronized (sActiveDialogs) {
                sActiveDialogs.add((Dialog) dialog);
                AppLog.showInfo(getClass().getSimpleName(), "Add Dialog, Active Dialog Size " + sActiveDialogs.size());
            }
        }
    };

    private static DialogInterface.OnDismissListener sDialogUnregister = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            if (sActiveDialogs == null)
                return;

            synchronized (sActiveDialogs) {
                sActiveDialogs.remove(dialog);
                AppLog.showInfo(getClass().getSimpleName(), "Remove Dialog, Active Dialog Size " + sActiveDialogs.size());
            }
        }
    };

    private DialogUtils() {

    }

    public static void dismissAll() {
        if (sActiveDialogs == null)
            return;

        synchronized (sActiveDialogs) {
            for (Dialog aDialog : sActiveDialogs) {
                if (aDialog.isShowing()) {
                    aDialog.setOnDismissListener(null);
                    aDialog.dismiss();
                }
            }
            sActiveDialogs.clear();
        }
    }


    public static Dialog showProgressDialog(Context context, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(android.R.color.transparent));
        dialog.setCancelable(false);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = DeviceUtils.getScreenWidth() / 2;
        dialog.getWindow().setAttributes(attributes);
        TextView msg = (TextView) dialog.findViewById(R.id.progress_message);
        msg.setVisibility(View.VISIBLE);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.action_bar_bg), android.graphics.PorterDuff.Mode.MULTIPLY);
        msg.setText(message);
        dialog.setOnShowListener(sDialogRegister);
        dialog.setOnDismissListener(sDialogUnregister);
        dialog.show();
        return dialog;
    }

    public static Dialog showProgressDialog(Context context) {
        return showProgressDialog(context, context.getResources().getString(R.string.Processing_loading));
    }

    public static void showAlertDialog(Context context, String title, String message, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_redesign, null, false);
        alertDialog.setView(v);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);

        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        alertDialog.setCanceledOnTouchOutside(false);

        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_NEGATIVE);
            }
        });
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
            }
        });


        alertDialog.show();
    }

    public static AlertDialog returnAlertDialog(Context context, String title, String message, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_redesign, null, false);
        alertDialog.setView(v);

        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);
        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        alertDialog.setCanceledOnTouchOutside(false);

        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_NEGATIVE);
            }
        });
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
            }
        });

        return alertDialog;
    }


    public static void showErrorDialog(Context context, String title, String message, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_redesign, null, false);
        alertDialog.setView(v);

        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);
        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        ((CustomFontTextView) v.findViewById(R.id.continue_action)).setText(context.getResources().getString(R.string.ok));
        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
            }
        });
        v.findViewById(R.id.cancel_action).setVisibility(View.GONE);
        alertDialog.show();
    }

    public static void showAlertDialogCommon(Context context, String title, String message, String positiveBtnName, String negtiveBtnName, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_redesign, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        if (title == null) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setText(title);
        }
        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setMovementMethod(new ScrollingMovementMethod());
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        if (negtiveBtnName == null) {
            ((CustomFontTextView) v.findViewById(R.id.cancel_action)).setVisibility(View.GONE);
        } else {
            ((CustomFontTextView) v.findViewById(R.id.cancel_action)).setText(negtiveBtnName);
            v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_NEGATIVE);
                }
            });
        }
        if (positiveBtnName == null) {
            ((CustomFontTextView) v.findViewById(R.id.continue_action)).setVisibility(View.INVISIBLE);
        } else {
            ((CustomFontTextView) v.findViewById(R.id.continue_action)).setText(positiveBtnName);
            v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
                }
            });
        }
        alertDialog.show();
    }


    public static AlertDialog showAlertDialogThreeButtons(Context context, String title, String message, String positiveBtnName, String negtiveBtnName, String neutralBtnName, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_redesign, null, false);
        alertDialog.setView(v);

        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);
        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        if (negtiveBtnName == null) {
            ((CustomFontTextView) v.findViewById(R.id.cancel_action)).setVisibility(View.INVISIBLE);
        } else {
            ((CustomFontTextView) v.findViewById(R.id.cancel_action)).setText(negtiveBtnName);
            v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_NEGATIVE);
                }
            });
        }
        if (positiveBtnName == null) {
            ((CustomFontTextView) v.findViewById(R.id.continue_action)).setVisibility(View.INVISIBLE);
        } else {
            ((CustomFontTextView) v.findViewById(R.id.continue_action)).setText(positiveBtnName);
            v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
                }
            });
        }

        if (neutralBtnName == null) {
            ((CustomFontTextView) v.findViewById(R.id.neutal_action)).setVisibility(View.GONE);
        } else {
            ((CustomFontTextView) v.findViewById(R.id.neutal_action)).setVisibility(View.VISIBLE);

            ((CustomFontTextView) v.findViewById(R.id.neutal_action)).setText(neutralBtnName);
            v.findViewById(R.id.neutal_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_NEUTRAL);
                }
            });
        }

        return alertDialog;
    }

    public static AlertDialog showOrangeThemeDialog(Context context, String title, String message, String positiveBtnName, String negtiveBtnName, String neutralBtnName, final DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.layout_dialog_orange_button, null, false);
        alertDialog.setView(v);

        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);
        CustomFontTextView tvMessage = (CustomFontTextView) v.findViewById(R.id.message_text);
        tvMessage.setText(message);
        alertDialog.setOnShowListener(sDialogRegister);
        alertDialog.setOnDismissListener(sDialogUnregister);
        if (negtiveBtnName == null) {
            ((CustomFontButton) v.findViewById(R.id.cancel_action)).setVisibility(View.INVISIBLE);
        } else {
            ((CustomFontButton) v.findViewById(R.id.cancel_action)).setText(negtiveBtnName);
            v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_NEGATIVE);
                }
            });
        }
        if (positiveBtnName == null) {
            ((CustomFontButton) v.findViewById(R.id.continue_action)).setVisibility(View.INVISIBLE);
        } else {
            ((CustomFontButton) v.findViewById(R.id.continue_action)).setText(positiveBtnName);
            v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
                }
            });
        }

        if (neutralBtnName == null) {
            ((CustomFontButton) v.findViewById(R.id.neutal_action)).setVisibility(View.GONE);
        } else {
            ((CustomFontButton) v.findViewById(R.id.neutal_action)).setVisibility(View.VISIBLE);

            ((CustomFontButton) v.findViewById(R.id.neutal_action)).setText(neutralBtnName);
            v.findViewById(R.id.neutal_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    listener.onClick(alertDialog, Dialog.BUTTON_NEUTRAL);
                }
            });
        }

        return alertDialog;
    }

}

