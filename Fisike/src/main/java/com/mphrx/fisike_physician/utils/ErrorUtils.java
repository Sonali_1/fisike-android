package com.mphrx.fisike_physician.utils;

import android.content.res.Resources;

import com.mphrx.fisike.MyApplication;

import java.util.Locale;

/**
 * Created by brijesh on 16/12/15.
 */
public class ErrorUtils {

    public static String getErrorMessage(String msg, String status, String httpStatusCode) {
        String message = null;

        Resources res = MyApplication.getAppContext().getResources();
        String pkg = MyApplication.getAppContext().getPackageName();

        // first attempt to fetch error from msg ELSE from status ELSE from httpStatusCode
        int resId = res.getIdentifier(msg.toLowerCase(Locale.US), "string", pkg);
        if (resId == 0) {
            resId = res.getIdentifier(status.toLowerCase(), "string", pkg);
            if (resId == 0) {
                resId = res.getIdentifier(httpStatusCode.toLowerCase(), "string", pkg);
            }
        }

        try {
            message = MyApplication.getAppContext().getString(resId);
            AppLog.showInfo(ErrorUtils.class.getClass().getSimpleName(), String.format(Locale.US,
                    "Error Lookup %s, %s, %s => %s", msg, status, httpStatusCode, message));
        } catch (Resources.NotFoundException e) {
            AppLog.showInfo(ErrorUtils.class.getClass().getSimpleName(), String.format(Locale.US,
                    "Error Lookup %s, %s, %s => <NO MESSAGE>", msg, status, httpStatusCode));
        }

        return message;
    }

}
