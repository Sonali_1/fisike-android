package com.mphrx.fisike_physician.utils;

import android.util.Log;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.utils.Utils;

public class AppLog {

    private static final boolean DEBUG_MODE = true;

    public static void showInfo(String tag, String message) {
        if (BuildConfig.DEBUG && Utils.isValueAvailable(message))
            Log.i(tag, message);
    }

    public static void showWarn(String tag, String message) {
        if (BuildConfig.DEBUG && Utils.isValueAvailable(message))
            Log.w(tag, message);
    }

    public static void showWarn(String tag, Exception e) {
        if(BuildConfig.DEBUG)
            Log.w(tag, e.toString());
    }
    public static void showError(String tag, String message) {
        if (BuildConfig.DEBUG && Utils.isValueAvailable(message))
            Log.e(tag, message);
    }

    public static void showError(String tag, String message, Throwable e) {
        if (BuildConfig.DEBUG && Utils.isValueAvailable(message))
            Log.e(tag, message, e);
    }

    public static void d(String tag,String msg)
    {
        if(BuildConfig.DEBUG && Utils.isValueAvailable(msg))
            Log.d(tag,msg);
    }
    public static void d(String tag,int msg)
    {
        if(BuildConfig.DEBUG)
            Log.d(tag,msg+"");
    }

    public static void e(String error, String s, Exception ex) {
        if(BuildConfig.DEBUG )
            Log.e(error,s,ex);
    }

}
