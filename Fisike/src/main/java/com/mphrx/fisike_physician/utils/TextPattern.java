package com.mphrx.fisike_physician.utils;

import java.util.regex.Pattern;

/**
 * Created by brijesh on 25/11/15.
 */
public class TextPattern {

    private static Pattern SINGLE_CHAT_JID = Pattern.compile("singlechat_\\d+_\\d+_\\d+.*");

    public static String maskString(String text, char mask) {
        if (text.length() < 4)
            return text;
        StringBuilder str = new StringBuilder(text);
        for (int i = 2; i < str.length() - 2; i++)
            str.replace(i, i + 1, mask + "");
        return str.toString();
    }

    public static String getPhoneNumber(String number, String countryCode) {
        StringBuilder num = new StringBuilder();
        for (char ch : number.toCharArray()) {
            if (ch >= '0' && ch <= '9')
                num.append(ch);
            if (ch == '+' && num.length() == 0)
                num.append(ch);
        }
        if (num.length() > 0) {
            if (num.charAt(0) == '0') {
                num.deleteCharAt(0);
                num.insert(0, countryCode);
            } else if (num.charAt(0) != '+') {
                num.insert(0, countryCode);
            }
        }
        return num.toString();
    }

    public static String getUserId(String senderId) {
        return senderId.contains("@") ? senderId.split("@")[0] : senderId;
    }

    public static boolean isSingleGroupChat(String jid) {
        return SINGLE_CHAT_JID.matcher(jid).matches();
    }
}
