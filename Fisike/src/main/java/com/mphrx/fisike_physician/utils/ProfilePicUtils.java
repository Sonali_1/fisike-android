package com.mphrx.fisike_physician.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;


/**
 * Created by brijesh on 01/11/15.
 */
public class ProfilePicUtils {


    public static Bitmap safeDecodeFile(Context context, String filePath) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        BitmapFactory.Options finalOptions = new BitmapFactory.Options();
        finalOptions.inSampleSize = ExifUtils.calculateInSampleSize(options, 400, 400);
        Bitmap tmp = BitmapFactory.decodeFile(filePath, finalOptions);
        return /*ExifUtils.rotateBitmap(filePath,*/ tmp/*)*/;
    }


    public static Bitmap safeDecodeFile(Context context, String filePath,boolean changeSize) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        BitmapFactory.Options finalOptions = new BitmapFactory.Options();
        Bitmap tmp = BitmapFactory.decodeFile(filePath, finalOptions);
        return /*ExifUtils.rotateBitmap(filePath, */tmp/*)*/;
    }

    public static Bitmap safeDecodeStream(Context context, Uri uri,boolean needResize) throws IOException {
        try {
            // try decoding using file-path, because that gives us the power of EXIF
            // decode stream only as a fallback
            String path = getPathFromUri(context, uri);
            if (path!=null && !path.isEmpty()) {
                Bitmap bmp = safeDecodeFile(context, path,needResize);
                if (bmp != null) {
                    return bmp;
                }
            }
        } catch (Exception ignore) {}


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, options);

        // Calculate inSampleSize
        BitmapFactory.Options finalOptions = new BitmapFactory.Options();
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, finalOptions);
    }

    public static Bitmap safeDecodeStream(Context context, Uri uri) throws IOException {
        try {
            // try decoding using file-path, because that gives us the power of EXIF
            // decode stream only as a fallback
            String path = getPathFromUri(context, uri);
            if (path!=null && !path.isEmpty())
            {
                Bitmap bmp = /*ExifUtils.rotateBitmap(uri.getPath().toString(),*/safeDecodeFile(context, path)/*)*/;
                if (bmp != null) {
                    return bmp;
                }
            }
        } catch (Exception ignore) {}


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, options);

        // Calculate inSampleSize
        BitmapFactory.Options finalOptions = new BitmapFactory.Options();
        finalOptions.inSampleSize = ExifUtils.calculateInSampleSize(options, 640, 640);

        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, finalOptions);
    }

    public static String getPathFromUri(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    // Method returns here with null value
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public  static void rotateImageByAngle(int degree)
    {

    }
}
