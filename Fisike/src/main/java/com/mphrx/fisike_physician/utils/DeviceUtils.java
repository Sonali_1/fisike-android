package com.mphrx.fisike_physician.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

/**
 * Created by brijesh on 01/11/15.
 */
public class DeviceUtils {


    public static int getScreenWidth() {
        return MyApplication.getAppContext().getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return MyApplication.getAppContext().getResources().getDisplayMetrics().heightPixels;
    }

    public static int getPixelSize(int dp) {
        return Math.round(getDensity() * dp);
    }

    public static float getDensity() {
        return MyApplication.getAppContext().getResources().getDisplayMetrics().density;
    }

    public static String getCountryCode() {
        String countryCode = "+";
        TelephonyManager manager = (TelephonyManager) MyApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
        String countryID= manager.getSimCountryIso().toUpperCase();
        String[] rl = MyApplication.getAppContext().getResources().getStringArray(R.array.countrycodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(countryID.trim())){
                countryCode = countryCode + g[0];
                break;
            }
        }
        countryCode ="+91";
        return countryCode;
    }

    public static String getAppVersion() {
        String version = String.valueOf(Integer.MIN_VALUE);
        try {
            PackageManager manager = MyApplication.getAppContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(MyApplication.getAppContext().getPackageName(),
                    PackageManager.GET_META_DATA);
            version = String.valueOf(info.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.showError("Package Manager", e.getLocalizedMessage());
            // Do Nothing;
        }
        return version;
    }

    public static String getOSVersion() {
        // String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return String.valueOf(sdkVersion);
    }

    public static int getAppVersionString() {
        try {
            PackageManager manager = MyApplication.getAppContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(MyApplication.getAppContext().getPackageName(),
                    PackageManager.GET_META_DATA);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.showError("Package Manager", e.getLocalizedMessage());
            return 0;
        }
    }

    public static String getPlayStoreUri() {
        // FIXME test and replace with play store URI instead of browser url if required
        String pckg = MyApplication.getAppContext().getPackageName();
        if (pckg.endsWith(".debug"))
            pckg = pckg.replace(".debug", "");
        return "https://play.google.com/store/apps/details?id=" + pckg;
    }



}
