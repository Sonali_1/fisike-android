package com.mphrx.fisike_physician.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserFileAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import searchpatient.constants.KeyConstants;

public class SharedPref {


    public static final String SEARCH_PATIENT_BTG_TIME = "SEARCH_PATIENT_BTG_TIME";
    public static final String PATEINT_BTG_REASON = "PATEINT_BTG_REASON";
    public static final String PATEINT_BTG_COMMENT = "PATEINT_BTG_COMMENT";

    private static String oldEmailAddress;
    private static String RESULT_ORDER_KEY = "RESULT_ORDER_KEY";


    public static void setoldEmailAddress(String oldEmailAddress) {
        SharedPref.oldEmailAddress = oldEmailAddress;
    }

    public enum UserType {
        REGISTERED("Registered"), PARTIALLY_REGISTERED("Partially_Registered"), NEW_USER("New_User");

        String type;

        UserType(String type) {
            this.type = type;
        }

        public String getTypeString() {
            return this.type;
        }

    }

    private static final String SETUP_CHAT_API_CALL = "SETUP_CHAT_API_CALL";
    private static final String DEFINED_ROLES = "DEFINED_ROLES";
    private static final String TOTAL_ACTIVE_CARE_PLANS_COUNT = "TOTAL_ACTIVE_CARE_PLANS_COUNT";
    private static final String TOTAL_ACTIVE_CARE_TASK_COUNT = "TOTAL_ACTIVE_CARE_TASK_COUNT";

    public static final String USER_NAME = "USER_NAME";

    private static final String WAITING_FOR_APPROVAL = "WAITING_FOR_APPROVAL";
    private static final String USER_TYPE = "USER_TYPE";
    private static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String LOGIN_USER_NAME = "LOGIN_USER_NAME";
    public static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    public static final String LANGUAGE_SELECTED = "LANGUAGE_SELECTED";
    public static final String LANGUAGE_SELECTED_KEY = "LANGUAGE_SELECTED_KEY";
    public static final String DEVICEUID = "DEVICEUID";
    private static final String OTP = "OTP";
    private static final String OLD_MOBILE_NUMBER = "OLD_MOBILE_NUMBER";
    private static final String OLD_COUNTRY_CODE = "OLD_COUNTRY_CODE";
    private static final String ROLE = "ROLE";
    private static final String SPECIALITY = "SPECIALITY";
    private static final String EXPERIENCE = "EXPERIENCE";
    private static final String ADDRESS = "ADDRESS";
    private static final String IS_RESET_PASS = "IS_RESET_PASS";

    private static final String MUTE_INTERVAL = "MUTE_INTERVAL";
    private static final String IS_MUTE = "IS_MUTE";
    private static final String SHOW_NOTIFICATION = "SHOW_NOTIFICATION";
    private static final String IS_USER_STATUS = "IS_USER_STATUS";
    private static final String USER_STATUS = "USER_STATUS";

    private static final String TIME_STAMP = "TIME_STAMP";
    private static final String NEW_EMAIL = "NEW_EMAIL";
    private static final String UPLOAD_IMAGE = "UPLOAD_IMAGE";
    private static final String MUTE_VALUE = "MUTE_VALUE";
    public static final String UPDATE_VERSION_TIME_STAMP = "UPDATE_VERSION_TIME_STAMP";
    private static final String UPDATE_ALERT_TIME_STANP = "UPDATE_ALERT_TIME_STAMP";

    public static final String APP_VERSION = "APP_VERSION";
    public static final String OS_VERSION = "OS_VERSION";
    public static final String GCM_REGISTRATION = "GCM_REGISTRATION";
    public static final String BLOCKING_API_CALL = "BLOCKING_API_CALL";
    public static final String GCM_TOKEN = "GCM_TOKEN";
    private static final String LOGIN_ATTEMPTS = "LOGIN_ATTEMPTS";
    private static final String LAST_INTERACTION_TIME = "LAST_INTERACTION_TIME";
    private static final String ALTERNATE_CONTACT = "ALTERNATE_CONTACT";
    private static final String NO_OF_DAYS_PASSED_FOR_PROMPT = "NO_OF_DAYS_PASSED_FOR_PROMPT";
    private static final String NO_OF_SESSION_OF_DAY = "NO_OF_SESSION_OF_DAY";
    private static final String IS_TO_SHOW_ALT_DIALOG = "IS_TO_SHOW_ALT_DIALOG";
    private static final String TODAY_DATE = "TODAY_DATE";
    private static final String DIFFRENCE_CALCULATED_DATE = "DIFFRENCE_CALCULATED_DATE";

    private static final String EMAIL_OR_MOBILE = "Email Mobile";
    private static final String EMAIL_AND_MOBILE = "Email and Mobile";
    private static final String OLD_EMAIL_AND_MOBILE = "OLD_EMAIL_AND_MOBILE";
    private static final String OLD_MOBILE_ENABLED = "OLD_MOBILE_ENABLED";
    private static final String OLD_IS_MOBILE_ENABLED = "OLD_ISMOBILE_ENABLED";
    private static final String IS_MOBILE_ENABLED = "Mobile enabled";
    private static final String IS_PHONE_ENABLED = "Phone enabled";
    private static final String IS_USER_NAME_ENABLED = "UserName enabled";
    private static final String IS_EMAIL_ENABLED = "Email enabled";
    private static final String IS_KEY_MIGRATED = "IS_KEY_MIGRATED";

    public static final String ADMIN_CREATED_PASSWORD_NEVER_CHANGED = "adminCreatedPasswordNeverChanged";
    private static final String DEFAULT_PASSWORD_TYPE = "DEFAULT_PASSWORD_TYPE";
    private static final String LOGO_DOWNLOAD_TIME = "LOGO_DOWNLOAD_TIME";
    private static final String DEMO_SIDE_DRAWER = "DEMO_SIDE_DRAWER";
    private static final String IS_TO_SELECT_ITEM = "IS_TO_SELECT_ITEM";
    private static final String POSITION_TO_SELECT = "POSITION_TO_SELECT";
    private static final String IS_SELF_SIGNUP_ENABLED = "IS_SELF_SIGNUP_ENABLED";

    private static final String PASSOWORD_REGEX = "PASSOWORD_REGEX";
    private static final String PASSWORD_LENGTH = "PASSWORD_LENGTH";
    private static final String PASSWORD_HISTORY_COUNT = "PASSWORD_HISTORY_COUNT";
    private static final String IS_VITAL_CONFIG_PASSED = "IS_VITAL_CONFIG_PASSED";
    private static final String REFRESH_RECORDS = "REFRESH_RECORDS";
    private static final String REFRESH_UPLOADS = "REFRESH_UPLOADS";
    private static final String CLEAR_RECORDS = "CLEAR_RECORDS";
    private static final String RENDER_CONFIG = "RENDER_CONFIG";
    private static final String IS_OUT_OF_REGION = "IS_OUT_OF_REGION";
    private static final String AGREEMENT_IDS = "AGREEMENT_IDS";
    private static final String CONTENTS = "CONTENTS";
    private static final String READ_DATES = "READ_DATES";
    private static final String UPDATED_DATES = "UPDATED_DATES";
    private static final String IS_AGREEMENTS_UPDATED = "IS_AGREEMENTS_UPDATED";

    private static final String IS_MRN_AVAILABLE = "ISMRNNUMBERAVAILABLE";
    private static final String INTERPRETATION_HIGHLIGHT_LIST = "INTERPRETATION_HIGHLIGHT_LIST";
    public static final String IS_BTG_ACCESS = "IS_BTG_ACCESS";
    private static final String IS_COVERAGE_UPDATED = "ISCOVERAGEUPDATED";

    private static SharedPreferences mSharedPreferences, mAppointmentSharedPreferences;
    private static SharedPreferences.Editor mPreferenceEditor, mAppointmentEditor;

    private static Map<String, Set<SharedPreferences.OnSharedPreferenceChangeListener>> mListeners;
    private static Context mContext;

    public static void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE);
        mAppointmentSharedPreferences = context.getSharedPreferences(VariableConstants.APPOINTMENT_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        mPreferenceEditor = mSharedPreferences.edit();
        mPreferenceEditor.apply();
        mAppointmentEditor = mAppointmentSharedPreferences.edit();
        mAppointmentEditor.apply();
        mListeners = new ConcurrentHashMap<>();
        mContext = context;
    }


    public static void register(String preferenceKey, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (mListeners.containsKey(preferenceKey)) {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(preferenceKey);
            listeners.add(listener);
        } else {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = new HashSet<>();
            listeners.add(listener);
            mListeners.put(preferenceKey, listeners);
        }
    }

    public static void unregister(String preferenceKey, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (mListeners.containsKey(preferenceKey)) {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(preferenceKey);
            listeners.remove(listener);
            if (listeners.size() == 0) {
                mListeners.remove(preferenceKey);
            }
        } else {
            AppLog.showError(SharedPref.class.getSimpleName(), "Can't find the listener to unregister");
        }
    }


    public static void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (mListeners.containsKey(key)) {
                Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(key);
                for (SharedPreferences.OnSharedPreferenceChangeListener listener : listeners) {
                    listener.onSharedPreferenceChanged(sharedPreferences, key);
                }
            }
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (mListeners.containsKey(key)) {
                        Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(key);
                        for (SharedPreferences.OnSharedPreferenceChangeListener listener : listeners) {
                            listener.onSharedPreferenceChanged(sharedPreferences, key);
                        }
                    }
                }
            });
        }
    }

    public static void setSearchPatientSort(String key, boolean isAsc) {
        mPreferenceEditor.putBoolean(key, isAsc);
        apply();
    }

    public static void setResultSortOrder(String key, int resultSort) {
        mPreferenceEditor.putInt(key, resultSort);
        apply();
    }

    public static void setStringSet(String key, Set<String> modalities) {
        mPreferenceEditor.putStringSet(key, modalities);
        apply();
    }

    public static Set<String> getSet(String key) {
        return mSharedPreferences.getStringSet(key, null);
    }

    public static void setFilter(ArrayList<String> filterList) {
        String name = filterList.get(0);
        String id = filterList.get(1);
        String mobileNumber = filterList.get(2);
        String emailId = filterList.get(3);
        String countryCode = filterList.get(4);
        if (name != null && !name.equals("")) {
            mPreferenceEditor.putString(KeyConstants.PATIENT_SEARCH_FILTER_NAME, name);
        }
        if (id != null && !id.equals("")) {
            mPreferenceEditor.putString(KeyConstants.PATIENT_SEARCH_FILTER_ID, id);
        }
        if (mobileNumber != null && !mobileNumber.equals("")) {
            mPreferenceEditor.putString(KeyConstants.PATIENT_SEARCH_FILTER_MOBILE_NUMBER, mobileNumber);
        }
        if (emailId != null && !emailId.equals("")) {
            mPreferenceEditor.putString(KeyConstants.PATIENT_SEARCH_FILTER_EMAIL_ID, emailId);
        }
        if (countryCode != null && !countryCode.equals("")) {
            mPreferenceEditor.putString(KeyConstants.PATIENT_SEARCH_FILTER_COUNTRY_CODE, countryCode);
        }
        apply();
    }

    public static void clearFilter() {
        mPreferenceEditor.remove(KeyConstants.PATIENT_SEARCH_FILTER_NAME);
        mPreferenceEditor.remove(KeyConstants.PATIENT_SEARCH_FILTER_ID);
        mPreferenceEditor.remove(KeyConstants.PATIENT_SEARCH_FILTER_MOBILE_NUMBER);
        mPreferenceEditor.remove(KeyConstants.PATIENT_SEARCH_FILTER_EMAIL_ID);
        mPreferenceEditor.remove(KeyConstants.PATIENT_SEARCH_FILTER_COUNTRY_CODE);
        apply();
    }

    public static String getFilterName() {
        return mSharedPreferences.getString(KeyConstants.PATIENT_SEARCH_FILTER_NAME, null);
    }

    public static String getFilterId() {
        return mSharedPreferences.getString(KeyConstants.PATIENT_SEARCH_FILTER_ID, null);
    }

    public static String getFilterMobileNumber() {
        return mSharedPreferences.getString(KeyConstants.PATIENT_SEARCH_FILTER_MOBILE_NUMBER, null);
    }

    public static String getFilterEmailId() {
        return mSharedPreferences.getString(KeyConstants.PATIENT_SEARCH_FILTER_EMAIL_ID, null);
    }

    public static String getPatientSearchCountryCode() {
        return mSharedPreferences.getString(KeyConstants.PATIENT_SEARCH_FILTER_COUNTRY_CODE, null);
    }

    public static boolean getFilterIsAsc() {
        return mSharedPreferences.getBoolean(KeyConstants.PATIENT_SEARCH_FILTER_IS_ASC, true);
    }

    public static int getResultFilter() {
        return mSharedPreferences.getInt(KeyConstants.RESULT_FILTER, VariableConstants.RESULT_ORDERDATE_DESC);
    }

    private static void apply() {
        mPreferenceEditor.apply();
    }

    private static void applyAppointmentEditor() {
        mAppointmentEditor.apply();
    }

    public static void updatePreferences(String key, String value) {
        if (value == null || value.isEmpty()) {
            mPreferenceEditor.remove(key);
        } else {
            mPreferenceEditor.putString(key, value);
        }
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    public static void updatePreferences(String key, boolean value) {
        mPreferenceEditor.putBoolean(key, value);
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + mSharedPreferences.getBoolean(key, false));
    }

    public static void updatePreferences(String key, long value) {
        mPreferenceEditor.putLong(key, value);
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    /*[..................For Appointment.............]*/
    public static void updateAppointmentPreferences(String key, String value) {
        if (value == null || value.isEmpty()) {
            mAppointmentEditor.remove(key);
        } else {
            mAppointmentEditor.putString(key, value);
        }
        applyAppointmentEditor();
        onSharedPreferenceChanged(mAppointmentSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    public static void updateAppointmentPreferences(String key, boolean value) {
        mAppointmentEditor.putBoolean(key, value);
        applyAppointmentEditor();
        onSharedPreferenceChanged(mAppointmentSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + mSharedPreferences.getBoolean(key, false));
    }

    public static void updateAppointmentPreferences(String key, long value) {
        mAppointmentEditor.putLong(key, value);
        applyAppointmentEditor();
        onSharedPreferenceChanged(mAppointmentSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }
    /*[..................For Appointment.............]*/


    public static String getAccessToken() {
        if (mSharedPreferences.getString(VariableConstants.AUTH_TOKEN, null) != null)
            return mSharedPreferences.getString(VariableConstants.AUTH_TOKEN, null);
        else {
            try {
                return UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getAuthToken());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static void setAccessToken(String token) {
        if (getAccessToken() == null && (token == null || token.equalsIgnoreCase("null")))
            return;
        else {
            try {
                boolean inserted = UserFileAdapter.getInstance(mContext).insert(token, UserFileAdapter.getAuthToken());
                if (!inserted) {
                    updatePreferences(VariableConstants.AUTH_TOKEN, null);
                    Intent intent = new Intent(VariableConstants.LOGOUT_IF_AUTH_TOKEN_NULL);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setLong(String key, long longValue) {
        mPreferenceEditor.putLong(key, longValue);
        apply();
    }

    public static boolean isTrue(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    public static Long getLong(String key) {
        return mSharedPreferences.getLong(key, 0);
    }

    public static String getString(String key) {
        return mSharedPreferences.getString(key, "");
    }


    public static void setOTPVerifyStatus(boolean isVerified) {
        updatePreferences(VariableConstants.HAS_VERIFIED_OTP, isVerified);
    }

    public static boolean isOTPVerified() {
        return mSharedPreferences.getBoolean(VariableConstants.HAS_VERIFIED_OTP, false);
    }

    public static boolean isDisableAccount() {
        return mSharedPreferences.getBoolean(TextConstants.DISABLE_USER, false);
    }

    public static void setDisableAccount(boolean isDisabled) {
        updatePreferences(TextConstants.DISABLE_USER, isDisabled);
    }

    public static void setUserLogout(boolean isLogout) {
        updatePreferences(VariableConstants.IS_LOGOUT, isLogout);
    }

    public static boolean isUserLogout() {
        return mSharedPreferences.getBoolean(VariableConstants.IS_LOGOUT, false);
    }

    public static void setUserWaitingForApproval(boolean flag) {
        updatePreferences(WAITING_FOR_APPROVAL, flag);
    }

    public static boolean isUserWaitingForApproval() {
        return mSharedPreferences.getBoolean(WAITING_FOR_APPROVAL, false);
    }

    public static void setUserType(UserType type) {
        updatePreferences(USER_TYPE, type.getTypeString());
    }

    public static String getUserType() {
        return mSharedPreferences.getString(USER_TYPE, UserType.NEW_USER.getTypeString());
    }


    public static boolean getFirstLogin() {
        return mSharedPreferences.getBoolean(VariableConstants.FIRST_LOGIN_HIPPA, false);
    }

    public static void setFirstLogin(boolean firstLogin) {
        updatePreferences(VariableConstants.FIRST_LOGIN_HIPPA, firstLogin);
    }


    public static boolean getPasswordExpired() {
        return mSharedPreferences.getBoolean(VariableConstants.PASSWORD_EXPIRED_HIPPA, false);
    }

    public static void setPasswordExpired(boolean passwordExpired) {
        updatePreferences(VariableConstants.PASSWORD_EXPIRED_HIPPA, passwordExpired);
    }


   /* public static String getUserNameTemp() {
        return mSharedPreferences.getString(USER_NAME, "");
    }*/

    /*username returned from loginresponse (Aastha)*/
    public static void setUserNameTemp(String username) {
        updatePreferences(USER_NAME, username);
    }


    public static String getDefinedRoles() {
        return mSharedPreferences.getString(DEFINED_ROLES, "");
    }

    public static void setDefinedRoles(String roles) {
        updatePreferences(DEFINED_ROLES, roles);
    }


    public static String getMobileNumber() {

        if ((mSharedPreferences.getString(MOBILE_NUMBER, null) != null)) {
            return mSharedPreferences.getString(MOBILE_NUMBER, null);
        } else {
            //fetch fromdb
            try {
                return UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getPhoneNumber());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static void setMobileNumber(String number) {

        try {
            UserFileAdapter.getInstance(mContext).insert(number, UserFileAdapter.getPhoneNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreferences(MOBILE_NUMBER, null);
    }

    public static String getEmailAddress() {
        if ((mSharedPreferences.getString(EMAIL_ADDRESS, null) != null)) {
            return mSharedPreferences.getString(EMAIL_ADDRESS, null);
        } else {
            //fetch from db
            try {
                return UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getEmailId());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String getLanguageSelected() {
        return mSharedPreferences.getString(LANGUAGE_SELECTED, TextConstants.DEFALUT_LANGUAGE_TITLE);
    }

    public static String getLanguageKey() {
        return mSharedPreferences.getString(LANGUAGE_SELECTED_KEY, TextConstants.DEFALUT_LANGUAGE_KEY);
    }


    public static void setLanguageSelected(String language, String languageKey) {
        if (languageKey == null || languageKey.isEmpty()) {
            mPreferenceEditor.remove(LANGUAGE_SELECTED_KEY);
        } else {
            mPreferenceEditor.putString(LANGUAGE_SELECTED_KEY, languageKey);
        }
        updatePreferences(LANGUAGE_SELECTED, language);
    }


    public static void setEmailAddress(String emailAddress) {

        try {
            UserFileAdapter.getInstance(mContext).insert(emailAddress, UserFileAdapter.getEmailId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreferences(EMAIL_ADDRESS, null);
    }


    public static String getLoginUserName() {

        if ((mSharedPreferences.getString(LOGIN_USER_NAME, null) != null)) {
            return mSharedPreferences.getString(LOGIN_USER_NAME, null);
        } else {
            //fetch from db
            try {
                return UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getUSERNAME());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /*username entered by user and returned from userdetailresponse*/
    public static void setLoginUserName(String userName) {
        try {
            UserFileAdapter.getInstance(mContext).insert(userName, UserFileAdapter.getUSERNAME());
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreferences(LOGIN_USER_NAME, null);
    }

    public static void setDeviceUid(String deviceUid) {
        try {
            UserFileAdapter.getInstance(mContext).insert(deviceUid, UserFileAdapter.getDeviceUid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreferences(DEVICEUID, null);
    }

    public static String getDeviceUid() {
        if ((mSharedPreferences.getString(DEVICEUID, null) != null)) {
            return mSharedPreferences.getString(DEVICEUID, null);
        } else {
            //fetch from db
            try {
                String deviceUID = UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getDeviceUid());
                if (deviceUID == null) {
                    deviceUID = Utils.getDeviceUUid(MyApplication.getAppContext());
                }
                return deviceUID;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    /* id returned from loginresponse (Aastha)*/
    public static long setUserName(long userId) {
        long userKey = (UserMO.KEY + "-" + userId).hashCode();
        try {
            UserFileAdapter.getInstance(mContext).insert(String.valueOf(userKey), UserFileAdapter.getPatientId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreferences(UserMO.KEY, null);
        return userKey;
    }

    public static String getUsername() {

        if ((mSharedPreferences.getString(UserMO.KEY, null) != null)) {
            return mSharedPreferences.getString(UserMO.KEY, null);
        } else {
            //fetch from db
            try {
                return UserFileAdapter.getInstance(mContext).fetchValue(UserFileAdapter.getPatientId());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String getCountryCode() {
        return mSharedPreferences.getString(COUNTRY_CODE, null);
    }

    public static void setCountryCode(String code) {
        updatePreferences(COUNTRY_CODE, code);
    }


    public static void setUserName(UserMO userMO) {
        long userKey = 0;
        SharedPref.setUserName(userMO.getId());
        userMO.setPersistenceKey(Long.parseLong(SharedPref.getUsername()));
        try {
            SettingManager.getInstance().updateUserMO(userMO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setVerificationStatus(boolean isVerified) {
        updatePreferences(VariableConstants.IS_USER_LOGGED_IN, isVerified);
    }

    public static boolean isUserVerified() {
        return mSharedPreferences.getBoolean(VariableConstants.IS_USER_LOGGED_IN, false);
    }


    public static void setOTP(String otp) {
        updatePreferences(OTP, otp);
    }


    public static void clearAllData() {
        setUserLogout(true);
        try {
            setAccessToken(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setOldMobileNumber(String number) {
        updatePreferences(OLD_MOBILE_NUMBER, number);
    }

    public static String getOldMobileNumber() {
        return mSharedPreferences.getString(OLD_MOBILE_NUMBER, "");
    }


    public static void setOldCountryCode(String code) {
        updatePreferences(OLD_COUNTRY_CODE, code);
    }

    public static String getOldCountryCode() {
        return mSharedPreferences.getString(OLD_COUNTRY_CODE, "");
    }

    public static void setUserExist(boolean isExist) {
        updatePreferences(VariableConstants.USER_EXIST, isExist);
    }

    public static boolean isUserExist() {
        return mSharedPreferences.getBoolean(VariableConstants.USER_EXIST, false);
    }

    public static void setVerifiedUser(boolean isExist) {
        updatePreferences(VariableConstants.VERIFIED_USER, isExist);
    }

    public static boolean isVerifiedUser() {
        return mSharedPreferences.getBoolean(VariableConstants.VERIFIED_USER, false);
    }

    public static void setRole(String role) {
        updatePreferences(ROLE, role);
    }

    public static String getRole() {
        return mSharedPreferences.getString(ROLE, "");
    }

    public static void setSpeciality(String speciality) {
        updatePreferences(SPECIALITY, speciality);
    }

    public static String getSpeciality() {
        return mSharedPreferences.getString(SPECIALITY, "");
    }

    public static void setExperience(String experience) {
        updatePreferences(EXPERIENCE, experience);
    }

    public static String getExperience() {
        return mSharedPreferences.getString(EXPERIENCE, "");
    }

    public static Boolean isSignedUpUser() {
        return mSharedPreferences.getBoolean(VariableConstants.SIGNED_UP, false);
    }

    public static void setMuteTimeInMills(long mills) {
        updatePreferences(MUTE_INTERVAL, mills);
    }

    public static long getMuteTimeInMills() {
        return mSharedPreferences.getLong(MUTE_INTERVAL, 0L);
    }

    public static void setIsMute(boolean mute) {
        updatePreferences(IS_MUTE, mute);
    }

    public static boolean getIsMute() {
        return mSharedPreferences.getBoolean(IS_MUTE, false);
    }

    public static void setShowNofification(boolean showNofification) {
        updatePreferences(SHOW_NOTIFICATION, showNofification);
    }

    public static boolean getIsShowNotification() {
        return mSharedPreferences.getBoolean(SHOW_NOTIFICATION, true);
    }

    public static void setChangeEmailTimestamp(long timestamp) {
        updatePreferences(TIME_STAMP, timestamp);
    }

    public static long getChangeEmailTimestamp() {
        return mSharedPreferences.getLong(TIME_STAMP, 0);
    }

    public static void setUpdateVersionTimeStamp(long timestamp) {
        updatePreferences(UPDATE_VERSION_TIME_STAMP, timestamp);
    }

    public static long getUpdateVersionTimeStamp() {
        return mSharedPreferences.getLong(UPDATE_VERSION_TIME_STAMP, 0);
    }

    public static void setUpdateAlertShownTimeStamp(long timestamp) {
        updatePreferences(UPDATE_ALERT_TIME_STANP, timestamp);
    }

    public static long getUpdateAlertShownTimeStamp() {
        return mSharedPreferences.getLong(UPDATE_ALERT_TIME_STANP, 0);
    }

    public static void setNewEmail(String email) {
        updatePreferences(NEW_EMAIL, email);
    }

    public static String getNewEmail() {
        return mSharedPreferences.getString(NEW_EMAIL, "");
    }

    public static void setNeedToUploadImage(boolean flag) {
        updatePreferences(UPLOAD_IMAGE, flag);
    }

    public static boolean needToUploadImage() {
        return mSharedPreferences.getBoolean(UPLOAD_IMAGE, false);
    }

    public static void setIsUserStatus(boolean status) {
        updatePreferences(IS_USER_STATUS, status);
    }

    public static boolean getIsUserStatus() {
        return mSharedPreferences.getBoolean(IS_USER_STATUS, false);
    }

    public static void setUserStatus(String status) {
        updatePreferences(USER_STATUS, status);
    }

    public static String getUserStatus() {
        return mSharedPreferences.getString(USER_STATUS, "I am available.");
    }

    public static void setMuteValue(String status) {
        updatePreferences(MUTE_VALUE, status);
    }

    public static String getMuteValue() {
        return mSharedPreferences.getString(MUTE_VALUE, "8hr");
    }

    public static void resetAppVersion() {
        updatePreferences(APP_VERSION, DeviceUtils.getAppVersion());
    }

    public static String getAppVersion() {
        return mSharedPreferences.getString(APP_VERSION, DeviceUtils.getAppVersion());
    }

    public static void resetOSVersion() {
        updatePreferences(OS_VERSION, DeviceUtils.getOSVersion());
    }

    public static String getOSVersion() {
        return mSharedPreferences.getString(OS_VERSION, DeviceUtils.getOSVersion());
    }

    public static void setGCMRegistration(boolean flag) {
        updatePreferences(GCM_REGISTRATION, flag);
    }

    public static boolean isGCMRegistrationOk() {
        return mSharedPreferences.getBoolean(GCM_REGISTRATION, false);
    }

    public static void setOTPSentTime(long time) {
        updatePreferences(VariableConstants.OTP_SEND_TIME, time);
    }

    public static long getOTPSentTime() {
        return mSharedPreferences.getLong(VariableConstants.OTP_SEND_TIME, 0);
    }

    public static void setForgetPassOTP(String otp) {
        updatePreferences(VariableConstants.FORGET_PASS_OTP, otp);
    }

    public static String getForgetPassOTP() {
        return mSharedPreferences.getString(VariableConstants.FORGET_PASS_OTP, "");
    }

    public static long getForgetPassOTPSendTime() {
        return mSharedPreferences.getLong(VariableConstants.FORGET_PASS_OTP_SEND_TIME, 0);
    }

    public static void setForgetPassOTPSendTime(long time) {
        updatePreferences(VariableConstants.FORGET_PASS_OTP_SEND_TIME, time);
    }

    public static boolean getIsUserInfoUpdated() {
        return mSharedPreferences.getBoolean(VariableConstants.IS_USERMO_UPDATED, true);
    }

    public static void setIsUserInfoUpdated(boolean isUpdated) {
        updatePreferences(VariableConstants.IS_USERMO_UPDATED, isUpdated);
    }

    public static void setLoginAttempts(long value) {
        updatePreferences(LOGIN_ATTEMPTS, value);
    }

    public static long getLoginAttempts() {
        return mSharedPreferences.getLong(LOGIN_ATTEMPTS, 0);
    }

    public static long getLastInteractionTime() {
        return mSharedPreferences.getLong(LAST_INTERACTION_TIME, 0);
    }

    public static void setLastInteractionTime(long value) {
        updatePreferences(LAST_INTERACTION_TIME, value);
    }

    public static void setForgetPasswordContactAltenate(String value) {
        updatePreferences(ALTERNATE_CONTACT, value);
    }

    public static String getForgetPasswordContactAltenate() {
        return mSharedPreferences.getString(ALTERNATE_CONTACT, "");
    }

    public static void setResetPassword(boolean value) {
        updatePreferences(IS_RESET_PASS, value);
    }

    public static boolean getResetPassword() {
        return mSharedPreferences.getBoolean(IS_RESET_PASS, false);
    }

    public static long getNoOfDaysForAlternateContact() {
        return mSharedPreferences.getLong(NO_OF_DAYS_PASSED_FOR_PROMPT, 0);
    }

    public static void setNoOfDaysForAlternateContact(long value) {
        updatePreferences(NO_OF_DAYS_PASSED_FOR_PROMPT, value);
    }

    public static long getNoOfSessionOfDay() {
        return mSharedPreferences.getLong(NO_OF_SESSION_OF_DAY, 0);
    }

    public static void setNoOfSessionOfDay(long value) {
        updatePreferences(NO_OF_SESSION_OF_DAY, value);
    }

    public static boolean getIsToShowAlternateContactDialog() {
        return mSharedPreferences.getBoolean(IS_TO_SHOW_ALT_DIALOG, false);
    }

    public static void setIsToShowAlternateContactDialog(boolean value) {
        updatePreferences(IS_TO_SHOW_ALT_DIALOG, value);
    }

    public static void setLastDisplayedAlternateContactDate(long value) {
        updatePreferences(TODAY_DATE, value);
    }

    public static long getLastDisplayedAlternateContactDate() {
        return mSharedPreferences.getLong(TODAY_DATE, 0);
    }

    public static long getDiffrenceCalculatedDate() {
        return mSharedPreferences.getLong(DIFFRENCE_CALCULATED_DATE, 0);
    }

    public static void setDiffrenceCalculatedDate(long value) {
        updatePreferences(DIFFRENCE_CALCULATED_DATE, value);
    }

    public static void setProgressForChat(boolean isToShowProgress) {
        mPreferenceEditor.putBoolean(SETUP_CHAT_API_CALL, isToShowProgress);
        apply();
        onSharedPreferenceChanged(mSharedPreferences, SETUP_CHAT_API_CALL);
    }

    public static boolean getSetupChatApiCall() {
        return mSharedPreferences.getBoolean(SETUP_CHAT_API_CALL, true);
    }


    public static void setMobileEnabled(boolean value) {
        updatePreferences(IS_MOBILE_ENABLED, value);
    }

    public static boolean getIsMobileEnabled() {
        return mSharedPreferences.getBoolean(IS_MOBILE_ENABLED, false);
    }

    public static void setUserNameEnabled(boolean value) {
        updatePreferences(IS_USER_NAME_ENABLED, value);
    }

    public static boolean getIsUserNameEnabled() {
        return mSharedPreferences.getBoolean(IS_USER_NAME_ENABLED, false);
    }

    public static boolean getEmailAndMobile() {
        return mSharedPreferences.getBoolean(EMAIL_AND_MOBILE, false);
    }

    public static void setEmailAndMobile(boolean value) {
        updatePreferences(EMAIL_AND_MOBILE, value);
    }

    public static void setIsEmailEnabled(boolean value) {
        updatePreferences(IS_EMAIL_ENABLED, value);
    }

    public static boolean getIsEmailEnabled() {
        return mSharedPreferences.getBoolean(IS_EMAIL_ENABLED, false);
    }


    public static void setOldEmailAndMoble(boolean value) {
        updatePreferences(OLD_EMAIL_AND_MOBILE, value);
    }


    public static void setOldMobileEnabled(boolean value) {
        updatePreferences(OLD_IS_MOBILE_ENABLED, value);
    }

    public static boolean getOldMobileEnabled() {
        return mSharedPreferences.getBoolean(OLD_IS_MOBILE_ENABLED, false);
    }

    /* Edited by Aastha*/
    public static void setAdminCreatedPasswordNeverChanged(boolean adminCreatedPasswordNeverChanged) {
        AppLog.d("admin created shared", adminCreatedPasswordNeverChanged + "");
        updatePreferences(ADMIN_CREATED_PASSWORD_NEVER_CHANGED, adminCreatedPasswordNeverChanged);
    }

    public static boolean getAdminCreatedPasswordNeverChanged() {
        return mSharedPreferences.getBoolean(ADMIN_CREATED_PASSWORD_NEVER_CHANGED, false);
    }

    public static String getDefaultPasswordType() {
        return mSharedPreferences.getString(DEFAULT_PASSWORD_TYPE, "Password");
    }

    public static void setDefaultPasswordType(String defaultPasswordType) {
        updatePreferences(DEFAULT_PASSWORD_TYPE, defaultPasswordType);
    }

    /* Edited by Aastha*/

    public static boolean isKeyMigrated() {
        return mSharedPreferences.getBoolean(IS_KEY_MIGRATED, false);
    }

    public static void setKeyMigrated(boolean value) {
        updatePreferences(IS_KEY_MIGRATED, value);
    }

    public static long getLogoDownloadTime() {
        return mSharedPreferences.getLong(LOGO_DOWNLOAD_TIME, 0);
    }

    public static void setLogoDownloadTime(long time) {
        updatePreferences(LOGO_DOWNLOAD_TIME, time);
    }

    public static boolean isSideDrawerShown() {
        return mSharedPreferences.getBoolean(DEMO_SIDE_DRAWER, false);
    }

    public static void setSideDrawerShown(boolean value) {
        updatePreferences(DEMO_SIDE_DRAWER, value);
    }

    public static void setTotalActiveCarePlansCount(long totalActiveCarePlansCount) {
        updatePreferences(TOTAL_ACTIVE_CARE_PLANS_COUNT, totalActiveCarePlansCount);
    }

    public static long getTotalActiveCarePlansCount() {
        return mSharedPreferences.getLong(TOTAL_ACTIVE_CARE_PLANS_COUNT, -1);
    }

    public static void setTotalActiveCareTaskCount(long totalActiveCareTaskCount) {
        updatePreferences(TOTAL_ACTIVE_CARE_TASK_COUNT, totalActiveCareTaskCount);
    }

    public static long getTotalActiveCareTaskCount() {
        return mSharedPreferences.getLong(TOTAL_ACTIVE_CARE_TASK_COUNT, -1);
    }

    public static boolean getIsToSelectItem() {
        return mSharedPreferences.getBoolean(IS_TO_SELECT_ITEM, false);
    }

    public static void setIsToSelectItem(boolean value) {
        updatePreferences(IS_TO_SELECT_ITEM, value);
    }

    public static int getPositionToSelect() {
        return (int) mSharedPreferences.getLong(POSITION_TO_SELECT, -1);
    }

    public static void setPositionToSelect(int value) {
        updatePreferences(POSITION_TO_SELECT, value);
    }

    public static boolean getIsSelfSignUpEnabled() {
        return mSharedPreferences.getBoolean(IS_SELF_SIGNUP_ENABLED, BuildConfig.isSelfSignUpEnable);
    }

    public static void setIsSelfSignUpEnabled(boolean value) {
        updatePreferences(IS_SELF_SIGNUP_ENABLED, value);
    }

    public static long getPasswordLength() {
        return mSharedPreferences.getLong(PASSWORD_LENGTH, CustomizedTextConstants.PASSWORD_LENGTH);
    }

    public static void setPasswordLength(long value) {
        updatePreferences(PASSWORD_LENGTH, value);
    }

    public static String getPasswordRegex() {
        return mSharedPreferences.getString(PASSOWORD_REGEX, "");
    }

    public static void setPasswordRegex(String value) {
        updatePreferences(PASSOWORD_REGEX, value);
    }

    public static boolean getRefreshRecords() {
        return mSharedPreferences.getBoolean(REFRESH_RECORDS, false);
    }

    public static void setRefreshRecords(boolean value) {
        updatePreferences(REFRESH_RECORDS, value);
    }

    public static long getPasswordHistoryCount() {
        return mSharedPreferences.getLong(PASSWORD_HISTORY_COUNT, 0);
    }

    public static void setPasswordHistoryCount(long value) {
        updatePreferences(PASSWORD_HISTORY_COUNT, value);
    }

    public static void setOrderNumberKey(String orderKey) {
        updatePreferences(RESULT_ORDER_KEY, orderKey);
    }

    public static String getOrderNumberKey() {
        return mSharedPreferences.getString(RESULT_ORDER_KEY, TextConstants.RESULT_ORDER_KEY_DEFAULT);
    }

    public static boolean isVitalConfigApiExecuted() {
        return mSharedPreferences.getBoolean(IS_VITAL_CONFIG_PASSED, false);
    }

    public static void setVitalConfigExecuted(boolean value) {
        updatePreferences(IS_VITAL_CONFIG_PASSED, value);
    }

    public static boolean isToRefreshUploads() {
        return mSharedPreferences.getBoolean(REFRESH_UPLOADS, false);
    }

    public static void setIsToRefreshUploads(boolean value) {
        updatePreferences(REFRESH_UPLOADS, value);
    }

    public static boolean isToClearRecords() {
        return mSharedPreferences.getBoolean(CLEAR_RECORDS, BuildConfig.isToClearRecords);
    }

    public static void setClearRecords(boolean value) {
        updatePreferences(CLEAR_RECORDS, value);
    }

     /*[......getter and setter for Appointment Shared Preferences.....]*/

    public static String getRenderConfig() {
        return mAppointmentSharedPreferences.getString(RENDER_CONFIG, "");
    }

    public static void setRenderConfig(String value) {
        updateAppointmentPreferences(RENDER_CONFIG, value);
    }

    public static boolean isOutOfRegion() {
        return mSharedPreferences.getBoolean(IS_OUT_OF_REGION, false);
    }

    public static void setOutOfRegion(boolean value) {
        updatePreferences(IS_OUT_OF_REGION, value);
    }

    public static void setIsMRNAvailable(boolean value) {
        updatePreferences(IS_MRN_AVAILABLE, value);
    }

    public static boolean isMRNAvailable() {
        return mSharedPreferences.getBoolean(IS_MRN_AVAILABLE, false);
    }


    public static String getUpdatedDates() {
        return mSharedPreferences.getString(UPDATED_DATES, "");
    }

    public static void setUpdatedDates(String date) {
        if (date == null) {
            updatePreferences(UPDATED_DATES, "");
            return;
        }
        String dates;
        if (!getUpdatedDates().equals(""))
            dates = date + "^" + getUpdatedDates();
        else
            dates = date;
        updatePreferences(UPDATED_DATES, dates);
    }

    public static String getAgreementIds() {
        return mSharedPreferences.getString(AGREEMENT_IDS, "");
    }

    public static void setAgreementIds(String id) {
        if (id == null) {
            updatePreferences(AGREEMENT_IDS, "");
            return;
        }
        String ids;
        if (!getAgreementIds().equals(""))
            ids = id + "^" + getAgreementIds();
        else
            ids = id;
        updatePreferences(AGREEMENT_IDS, ids);
    }

    public static String getCONTENTS() {
        return mSharedPreferences.getString(CONTENTS, "");
    }

    public static void setContents(String content) {
        if (content == null) {
            updatePreferences(CONTENTS, "");
            return;
        }
        String contents;
        if (!getCONTENTS().equals(""))
            contents = content + "^" + getCONTENTS();
        else
            contents = content;
        updatePreferences(CONTENTS, contents);
    }

    public static String getReadDates() {
        return mSharedPreferences.getString(READ_DATES, "");
    }

    public static void setReadDates(String readDate) {
        if (readDate == null) {
            updatePreferences(READ_DATES, "");
            return;
        }
        String readDates;
        if (!getReadDates().equals(""))
            readDates = readDate + "^" + getReadDates();
        else
            readDates = readDate;
        updatePreferences(READ_DATES, readDates);
    }

    public static boolean getIsAgreementsUpdated() {
        return mSharedPreferences.getBoolean(IS_AGREEMENTS_UPDATED, false);
    }

    public static void setIsAgreementsUpdated(boolean isUpdated) {
        updatePreferences(IS_AGREEMENTS_UPDATED, isUpdated);
    }

    public static boolean getIsCoverageUpdated() {
        return mSharedPreferences.getBoolean(IS_COVERAGE_UPDATED, false);
    }

    public static void setIsCoverageUpdated(boolean isCoverageUpdated) {
        updatePreferences(IS_COVERAGE_UPDATED, isCoverageUpdated);
    }
    /*[......getter and setter for Appointment Shared Preferences.....]*/

    public static String getInterPretatRionListToHighLight() {
        return mSharedPreferences.getString(INTERPRETATION_HIGHLIGHT_LIST, MyApplication.getAppContext().getString(R.string.observation_highlight_list));
    }

    public static void setInterPretationListToHighLight(String listString) {
        updatePreferences(INTERPRETATION_HIGHLIGHT_LIST, listString);
    }


}
    /*[......getter and setter for Appointment Shared Preferences.....]*/