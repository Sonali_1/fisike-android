package com.mphrx.fisike_physician.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike_physician.IGroupImageViewLoader;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import java.util.ArrayList;

/**
 * Created by brijesh on 15/10/15.
 */
public class SearchAdapter extends BaseAdapter implements Filterable {

    private ArrayList<ChatContactMO> mList;

    public SearchAdapter(ArrayList<ChatContactMO> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        if(mList != null)
            return mList.size();
        else
            return  0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ChatContactMO mo = mList.get(position);
        if (convertView == null) {
            if(mo != null && mo.isFisikeContact()) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_search_list_physician, null);
                holder = new ViewHolder(convertView , true);
            } else {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_search_list_child, null);
                holder = new ViewHolder(convertView ,false);
            }

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }




        if (mo != null && mo.isFisikeContact()) {
            holder.tvContactName.setText(mo.getDisplayName());
            String contactIds =  TextPattern.getUserId(mo.getId());
            if(holder.imgPerson != null)
            holder.imgPerson.setViewProfileImage(contactIds);
            holder.tvNumber.setText(TextPattern.maskString(mo.getPhoneNo(), '#'));
        }else {
            holder.tvNumber.setText(mo.getPhoneNo());
            holder.tvContactName.setText(mo.getDisplayName());
        }
        if (mo.getUserType() == null) {
            holder.tvSalutation.setVisibility(View.GONE);
        } else {
            holder.tvSalutation.setVisibility(View.VISIBLE);
            holder.tvSalutation.setText(mList.get(position).getUserType().getName());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                results.values = mList;
                results.count = (mList != null ? mList.size(): 0);
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                // ignore the results .... the entire dataset is already the result
                notifyDataSetChanged();
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return "";
            }
        };
    }

    class ViewHolder {
        TextView tvContactName;
        private IGroupImageViewLoader imgPerson;
        TextView tvSalutation;
        TextView tvNumber;

        public ViewHolder(View view ,boolean isFisikeContact) {
            tvContactName = (TextView) view.findViewById(R.id.tv_contact_name);
            tvSalutation = (TextView) view.findViewById(R.id.tv_salutation);
            tvNumber = (TextView) view.findViewById(R.id.tv_number);
            if(isFisikeContact)
            imgPerson = (CustomProfileVIewLayout)view.findViewById(R.id.img_profile);
        }
    }

    public void updateList(ArrayList<ChatContactMO> list) {
        mList = list;
        notifyDataSetChanged();
    }
}
