package com.mphrx.fisike_physician.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike_physician.activity.HealthActivity;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import java.util.ArrayList;

/**
 * Created by brijesh on 15/10/15.
 */
public class  ContactAdapter extends BaseAdapter implements Filterable {

    private ArrayList<ChatContactMO> mFilterList;
    private ArrayList<ChatContactMO> mOriginalList;
    private Activity activity = null;
    private boolean mShowRecordLauncher = false;

    public ContactAdapter(ArrayList<ChatContactMO> list) {
        mOriginalList = list;
        mFilterList = list;
    }

    public ContactAdapter(ArrayList<ChatContactMO> list, Activity activity, boolean showRecordLauncher) {
        mOriginalList = list;
        mFilterList = list;
        this.activity = activity;
        mShowRecordLauncher = true;
    }

    @Override
    public int getCount() {
        return mFilterList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_fisike_list_child, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ChatContactMO contact = mFilterList.get(position);

        holder.imgContactPic.setViewProfileImage(contact.getId());
        holder.tvContactName.setText(contact.getDisplayName());
        if (contact.getUserType().getName().equalsIgnoreCase("patient")) {
            holder.tvSalutation.setVisibility(View.GONE);
        } else {
            holder.tvSalutation.setText(contact.getUserType().getName());
            holder.tvSalutation.setVisibility(View.VISIBLE);
        }
        if (mShowRecordLauncher) {
            final String patientName = contact.getDisplayName();
            final String patientId = String.valueOf(contact.getPatientId());
            holder.imgRecLauncher.setVisibility(View.VISIBLE);
            holder.imgRecLauncher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, HealthActivity.class)
                            .putExtra("title", patientName)
                            .putExtra("patientId", String.valueOf(patientId)));
                }
            });
        } else {
            holder.imgRecLauncher.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        CustomProfileVIewLayout imgContactPic;
        TextView tvContactName;
        TextView tvSalutation;
        ImageView imgRecLauncher;

        public ViewHolder(View view) {
            imgContactPic = (CustomProfileVIewLayout) view.findViewById(R.id.img_profile);
            tvContactName = (TextView) view.findViewById(R.id.tv_contact_name);
            tvSalutation = (TextView) view.findViewById(R.id.tv_salutation);
            imgRecLauncher = (ImageView) view.findViewById(R.id.img_rec_launcher);
        }
    }

    public void updateUserList(ArrayList<ChatContactMO> list) {
        mFilterList = list;
        mOriginalList = list;
        notifyDataSetChanged();
    }

    public void addUserList(ArrayList<ChatContactMO> list) {
        mFilterList.addAll(list);
        mOriginalList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase().trim();
                FilterResults results = new FilterResults();
                final ArrayList<ChatContactMO> contactList = mOriginalList;
                int count = contactList.size();
                ArrayList<ChatContactMO> contactResult = new ArrayList<>(count);
                for (ChatContactMO contact : contactList) {
                    if (!TextUtils.isEmpty(contact.getDisplayName()) && contact.getDisplayName().toLowerCase().contains(filterString))
                        contactResult.add(contact);
                }
                results.values = contactResult;
                results.count = contactList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                resetList((ArrayList<ChatContactMO>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    private void resetList(ArrayList<ChatContactMO> newList) {
        mFilterList = newList;
    }

}
