package com.mphrx.fisike_physician.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.mo.ChatConversationMO;

import java.util.ArrayList;

/**
 * Created by brijesh on 13/10/15.
 */
public class MessageAdapter extends BaseAdapter {

    private ArrayList<ChatConversationMO> mMessageList;

    public MessageAdapter(ArrayList<ChatConversationMO> list) {
        mMessageList = list;
    }

    @Override
    public int getCount() {
        return mMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item_view, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(mMessageList.get(position).getJId());
        holder.subtitle.setText(mMessageList.get(position).getGroupName());

        return convertView;
    }


    class ViewHolder {

        TextView title;
        TextView subtitle;
        ImageView image;

        public ViewHolder(View view) {
            title = (TextView) view.findViewById(R.id.message_title);
            subtitle = (TextView) view.findViewById(R.id.message_subtitle);
            image = (ImageView) view.findViewById(R.id.user_image);
        }

    }

    public ArrayList<ChatConversationMO> getList() {
        return mMessageList;
    }

}
