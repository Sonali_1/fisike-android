package com.mphrx.fisike_physician.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.fragment.ColleagueContactFragment;
import com.mphrx.fisike_physician.fragment.OtherContactFragment;
import com.mphrx.fisike_physician.fragment.PatientContactFragment;

import java.util.HashMap;

/**
 * Created by brijesh on 21/10/15.
 */
public class ContactPagerAdapter extends FragmentPagerAdapter {


    private HashMap<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();

    public ContactPagerAdapter(FragmentManager manger) {
        super(manger);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment patientFragment = PatientContactFragment.getInstance(null);
                mPageReferenceMap.put(0, patientFragment);
                return patientFragment;
            case 1:
                Fragment collegueFragment = ColleagueContactFragment.getInstance(null);
                mPageReferenceMap.put(1, collegueFragment);
                return collegueFragment;
            case 2:
                return OtherContactFragment.getInstance(null);
        }
        return null;
    }

    public HashMap<Integer, Fragment> getmPageReferenceMap() {
        return mPageReferenceMap;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return MyApplication.getAppContext().getString(R.string.Patients);
            case 1:
                return MyApplication.getAppContext().getString(R.string.Colleagues);
            case 2:
                return MyApplication.getAppContext().getString(R.string.Lists);
        }
        return null;
    }

}
