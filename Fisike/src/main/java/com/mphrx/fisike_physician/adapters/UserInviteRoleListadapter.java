package com.mphrx.fisike_physician.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;

import java.util.List;

/**
 * Created by iappstreet on 23/02/16.
 */
public class UserInviteRoleListadapter extends ArrayAdapter {
    private List<String> inviteRoleValues;
    private int _resource;
    LayoutInflater inflater;

    public UserInviteRoleListadapter(Context context, int resource,  List<String> objects) {
        super(context, resource,  objects);
        inviteRoleValues = objects;
        _resource = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row;
        row = inflater.inflate(_resource, null);
        TextView _textView = (TextView) row.findViewById(R.id.invite_role);
        _textView.setText(inviteRoleValues.get(position));
        return row;
    }


    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }


    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }
}

