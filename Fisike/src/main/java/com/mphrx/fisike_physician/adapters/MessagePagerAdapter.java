package com.mphrx.fisike_physician.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

/**
 * Created by brijesh on 13/10/15.
 */
public class MessagePagerAdapter extends FragmentPagerAdapter {

    public MessagePagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return MyApplication.getAppContext().getResources().getString(R.string.Patient);
            case 1:
                return MyApplication.getAppContext().getResources().getString(R.string.Colleague);
            case 2:
                return MyApplication.getAppContext().getResources().getString(R.string.List);
        }
        return null;
    }
}
