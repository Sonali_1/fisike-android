package com.mphrx.fisike_physician.adapters;

/**
 * Created by manohar on 03/12/15.
 */

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike_physician.IGroupImageViewLoader;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import java.util.ArrayList;

/**
 * Created by brijesh on 15/10/15.
 */
public class NewGroupAdapter extends BaseAdapter implements Filterable {

    private ArrayList<ChatContactMO> mFilterList;
    private ArrayList<ChatContactMO> mOriginalList;
    private Activity activity = null;
    private boolean mShowRecordLauncher = false;
    OnUserRemove onUserRemove;

    public interface OnUserRemove {
        void onUserRemoved(int pos);
    }

    public void setOnUserRemove(OnUserRemove onUserRemove) {
        this.onUserRemove = onUserRemove;
    }

    public NewGroupAdapter(ArrayList<ChatContactMO> list) {
        mOriginalList = list;
        mFilterList = list;
    }

    public NewGroupAdapter(ArrayList<ChatContactMO> list, Activity activity, boolean showRecordLauncher) {
        mOriginalList = list;
        mFilterList = list;
        this.activity = activity;
        mShowRecordLauncher = true;
    }

    @Override
    public int getCount() {
        return mFilterList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_group_user_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String name = mFilterList.get(position).getFirstName() + " " + mFilterList.get(position).getLastName();
        if (name.trim().equals("")) {
            name = mFilterList.get(position).getUsername();
        }
        name = Character.toUpperCase(name.charAt(0)) + name.substring(1);

        holder.tvContactName.setText(name);
        holder.tvSalutation.setText(mFilterList.get(position).getUserType().getName());

        String contactIds = TextPattern.getUserId(mFilterList.get(position).getId());
        holder.imgPerson.setViewProfileImage(contactIds);
        if (mFilterList.get(position).getUserType().getName().equalsIgnoreCase("patient")) {
            holder.tvSalutation.setVisibility(convertView.GONE);
        } else {
            holder.tvSalutation.setVisibility(convertView.VISIBLE);

        }
        if (mShowRecordLauncher) {
            final String patientName = name;
            final String patientId = String.valueOf(mFilterList.get(position).getPatientId());
            holder.imgRecLauncher.setVisibility(View.VISIBLE);
            holder.imgRecLauncher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onUserRemove.onUserRemoved(position);

                }
            });
        } else {
            holder.imgRecLauncher.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        TextView tvContactName;
        TextView tvSalutation;
        ImageView imgRecLauncher;
        private IGroupImageViewLoader imgPerson;

        public ViewHolder(View view) {
            tvContactName = (TextView) view.findViewById(R.id.tv_contact_name);
            tvSalutation = (TextView) view.findViewById(R.id.tv_salutation);
            imgRecLauncher = (ImageView) view.findViewById(R.id.img_rec_launcher);
            imgPerson = (CustomProfileVIewLayout) view.findViewById(R.id.img_profile);
        }
    }

    public void updateUserList(ArrayList<ChatContactMO> list) {
        mFilterList = list;
        mOriginalList = list;
        notifyDataSetChanged();
    }

    public void addUserList(ArrayList<ChatContactMO> list) {
        mFilterList.addAll(list);
        mOriginalList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase().trim();
                FilterResults results = new FilterResults();
                final ArrayList<ChatContactMO> contactList = mOriginalList;
                int count = contactList.size();
                ArrayList<ChatContactMO> contactResult = new ArrayList<>(count);
                for (ChatContactMO contact : contactList) {
                    if (!TextUtils.isEmpty(contact.getDisplayName()) && contact.getDisplayName().toLowerCase().contains(filterString))
                        contactResult.add(contact);
                }
                results.values = contactResult;
                results.count = contactList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                resetList((ArrayList<ChatContactMO>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    private void resetList(ArrayList<ChatContactMO> newList) {
        mFilterList = newList;
    }

}
