package com.mphrx.fisike_physician.adapters;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.util.Pair;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.TextView;

import com.mphrx.fisike.R;


public class ContactsAutoCompleteCursorAdapter extends CursorAdapter implements Filterable {

    private static final String TAG = "ContactsAutoCompleteCursorAdapter";
    private ContentResolver mContent;

    public ContactsAutoCompleteCursorAdapter(Context context, Cursor c) {
        super(context, c, true);
        mContent = context.getContentResolver();
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        View inflate = inflater.inflate(R.layout.item_auto_complete_contact, parent, false);
        return inflate;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Pair<String, String> nameNumberPair = getNameNumberPair(cursor);
        TextView userName = (TextView) view.findViewById(R.id.tv_user_name);
        userName.setText(nameNumberPair.first);
        TextView userNumber = (TextView) view.findViewById(R.id.tv_user_number);
        userNumber.setText(nameNumberPair.second);
    }

    @Override
    public String convertToString(Cursor cursor) {
        // THIS METHOD DICTATES WHAT IS SHOWN WHEN THE USER CLICKS EACH ENTRY IN
        // YOUR AUTOCOMPLETE LIST
        // IN MY CASE I WANT THE NUMBER DATA TO BE SHOWN
        Pair<String, String> nameNumberPair = getNameNumberPair(cursor);
        return String.format("%s (%s)", nameNumberPair.first, nameNumberPair.second);
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        if (constraint == null || TextUtils.isEmpty(constraint.toString())) {
            return null;
        }
        // THIS IS HOW YOU QUERY FOR SUGGESTIONS
        // NOTICE IT IS JUST A STRINGBUILDER BUILDING THE WHERE CLAUSE OF A
        // CURSOR WHICH IS THE USED TO QUERY FOR RESULTS
        String select = ContactsContract.Data.HAS_PHONE_NUMBER + " != 0 AND " + ContactsContract.Data.MIMETYPE
                + " = ? AND (" + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? OR " + ContactsContract.Data.DATA1
                + " LIKE ? )";
        String[] selectArgs = {ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, "%" + constraint + "%",
                "%" + constraint + "%"};
        return mContent.query(ContactsContract.Data.CONTENT_URI, null, select, selectArgs, null);
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        return super.swapCursor(newCursor);
    }

    public Pair<String, String> getNameNumberPair(Cursor cursor) {
        int nameIdx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int numberIdx = cursor.getColumnIndexOrThrow(ContactsContract.Data.DATA1);

        String number = cursor.getString(numberIdx);
        String name = null;
        if (nameIdx == -1) {
            name = number;
        } else {
            name = cursor.getString(nameIdx);
            if (TextUtils.isEmpty(name)) {
                name = number;
            }
        }

        return new Pair<>(name, number);
    }
}