package com.mphrx.fisike_physician.adapters;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.utils.TextPattern;

import java.util.List;

/**
 * Created by shivansh on 07/10/15.
 */
public class UserInvitesAdapter extends BaseAdapter {

    private List<User> mUsersList;
    private InviteUserListener mListener;

    public UserInvitesAdapter(List<User> usersList, InviteUserListener listener) {
        mUsersList = usersList;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mUsersList.size();
    }

    @Override
    public User getItem(int position) {
        return mUsersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_invite, null);
            viewHolder = new ViewHolder();
            viewHolder.imgProfile = (ImageView) convertView.findViewById(R.id.img_profile);
            viewHolder.name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.number = (TextView) convertView.findViewById(R.id.tv_number);
            viewHolder.role = (TextView) convertView.findViewById(R.id.tv_role);
            viewHolder.imgCancel = (LinearLayout) convertView.findViewById(R.id.imageView);
            viewHolder.logo = (ImageView) convertView.findViewById(R.id.fisike_logo);
            convertView.setTag(viewHolder);
            viewHolder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = (int) v.getTag();
                    if (mListener != null)
                        mListener.onClickCancel(index);
                }
            });
            if (mListener == null) {
                viewHolder.imgCancel.setVisibility(View.GONE);
                viewHolder.role.setCompoundDrawables(null, null, null, null);
                viewHolder.role.requestLayout();
            }
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        User user = mUsersList.get(position);
        if (user.isSelected)
            convertView.setBackgroundColor(Color.parseColor("#3000ff00"));
        else
            convertView.setBackgroundColor(Color.TRANSPARENT);

        viewHolder.name.setText(user.name);
        viewHolder.role.setText(user.role.isEmpty() ? parent.getContext().getString(R.string.string_user_role_default) : user.role);
        viewHolder.role.setTag(position);
        viewHolder.role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (int) v.getTag();
                if (mListener != null)
                    mListener.onCLickRole(index);
            }
        });
        viewHolder.imgCancel.setTag(position);
        if (user.isFisikeUser) {
            viewHolder.number.setText(TextPattern.maskString(user.number, '#'));
            viewHolder.logo.setVisibility(View.VISIBLE);
            viewHolder.role.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            viewHolder.role.requestLayout();
        }
        else {
            viewHolder.number.setText(user.number);
            viewHolder.logo.setVisibility(View.GONE);
            Drawable img = parent.getContext().getResources().getDrawable( R.drawable.ic_arrow_drop_down_black_24dp);
            viewHolder.role.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
            viewHolder.role.requestLayout();
        }
        return convertView;
    }

    public void updateList(List<User> mUserList) {
        mUsersList=mUserList;
        notifyDataSetChanged();
    }

    private class ViewHolder {
        ImageView imgProfile;
        LinearLayout imgCancel;
        TextView name;
        TextView number;
        TextView role;
        ImageView logo;
    }


    public interface InviteUserListener {
        void onCLickRole(int index);
        void onClickCancel(int index);
    }

}
