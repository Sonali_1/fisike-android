package com.mphrx.fisike_physician.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by brijesh on 05/08/15.
 */
public class RButton extends Button implements RTypeface {

    public RButton(Context context) {
        super(context);
        Sans.init(this, null);
    }

    public RButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Sans.init(this, attrs);
    }

    public RButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Sans.init(this, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Sans.init(this, attrs);
    }

    @Override
    public void setRobotoTypeface(Typeface typeface) {
        setTypeface(typeface);
    }

}
