package com.mphrx.fisike_physician.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.IGroupImageViewLoader;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.TextPattern;

import java.util.ArrayList;

/**
 * Created by iappstreet on 02/02/16.
 */
public class CustomProfileVIewLayout extends RelativeLayout implements IGroupImageViewLoader {

    LayoutInflater mInflater;
    private ImageView ivActionStatusIndicator;
    //    private CircularNetworkImageView gpProfilePic1, gpProfilePic2, gpProfilePic3, gpProfilePic4;
    private CircularNetworkImageView[] mPic = new CircularNetworkImageView[4];
    private LinearLayout mBottomRow;
    private ArrayList<String> gpContactIds = new ArrayList<>();
    private boolean mIsRectangular = false;
    private String groupid = null;
    private String groupImgUrl = "";

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            String contactId = bundle.getString(VariableConstants.UpdatedPicContactId);
            if(contactId.contains("_") && contactId.equals(groupid)){
                setGroupProfileImage(MphRxUrl.getFetchContactGroupImageUrl(contactId));
            }else {
                updateProfilePic(contactId);
            }
        }
    };

    @Deprecated
    private void updateProfilePic(String contactId) {
        contactId = TextPattern.getUserId(contactId);
        NetworkImageView updateImgView = (NetworkImageView) findViewWithTag(contactId);
        if (updateImgView != null && updateImgView.getVisibility() == VISIBLE) {
            updateImgView.setImageUrl(null, null);
            updateImgView.setImageUrl(MphRxUrl.getFetchContactImageUrl(contactId), Network.getImageLoader());
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

    public void registerBroadCast() {
        if (!isInEditMode())
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).registerReceiver(broadcastReceiver, new IntentFilter(VariableConstants.PROFILE_PIC_CHANGED_BROADCAST));
    }

    private void unregisterBroadCast() {
        if (!isInEditMode())
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).unregisterReceiver(broadcastReceiver);
    }

    public CustomProfileVIewLayout(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        findView();
    }

    public CustomProfileVIewLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
        findView();
    }

    public CustomProfileVIewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        findView();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        registerBroadCast();
        reloadImages();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unregisterBroadCast();
    }

    private void findView() {
        View view = mInflater.inflate(R.layout.action_bar_status_image_view, this, true);
        mPic[0] = (CircularNetworkImageView) view.findViewById(R.id.iv_gp_member1);
        mPic[1] = (CircularNetworkImageView) view.findViewById(R.id.iv_gp_member2);
        mPic[2] = (CircularNetworkImageView) view.findViewById(R.id.iv_gp_member3);
        mPic[3] = (CircularNetworkImageView) view.findViewById(R.id.iv_gp_member4);
        mBottomRow = (LinearLayout) view.findViewById(R.id.group_contact_bottomlayout);
        ivActionStatusIndicator = (ImageView) view.findViewById(R.id.iv_status);
    }

    private void resetVisibility(boolean isGroupImageAvailable) {
        int start = isGroupImageAvailable ? 1 : gpContactIds.size();
        ImageView.ScaleType fallBackScaleType = mIsRectangular ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.CENTER_INSIDE;
        mBottomRow.setVisibility(!isGroupImageAvailable && gpContactIds.size() > 2 ? VISIBLE : GONE);
        for (int i = 0; i < 4; i++) {
            if (isGroupImageAvailable && i == 0) {
                mPic[i].setDefaultImageResId(R.drawable.cg_profile_picturedefault, fallBackScaleType);
                mPic[i].setErrorImageResId(R.drawable.cg_profile_picturedefault, fallBackScaleType);
                mPic[i].setImageUrl(null, Network.getImageLoader());
            } else {
                mPic[i].setDefaultImageResId(R.drawable.img_profile_man, fallBackScaleType);
                mPic[i].setErrorImageResId(R.drawable.img_profile_man, fallBackScaleType);
                mPic[i].setImageUrl(null, Network.getImageLoader());
            }
            if (i < start) {
                mPic[i].setVisibility(VISIBLE);
            } else {
                mPic[i].setVisibility(GONE);
            }
        }
    }

    private void loadProfileImages() {
        resetVisibility(false);
        if (gpContactIds != null && gpContactIds.size() > 0) {
            ivActionStatusIndicator.setVisibility(View.INVISIBLE);
            int  size ;
            if(gpContactIds.size() < 4 )
                size = gpContactIds.size();
            else
                size = 4;
            for (int i = 0; i < size ; i++) {
                if (gpContactIds.size() == 2 &&
                        SettingManager.getInstance().getUserMO() != null &&
                        gpContactIds.get(i).equals(Long.toString(SettingManager.getInstance().getUserMO().getId()))) {
                    mPic[i].setVisibility(GONE);
                    continue;
                }
                mPic[i].setImageUrl(MphRxUrl.getFetchContactImageUrl(gpContactIds.get(i)), Network.getImageLoader());
                mPic[i].setTag(gpContactIds.get(i));
            }
        }
    }

    public void setViewProfileImage(String contactId) {
        gpContactIds.clear();
        gpContactIds.add(contactId);
        this.groupid = null;
        reloadImages();
    }

    public void setStatus(ChatContactMO chatContactMO) {
        if (ivActionStatusIndicator == null) {
            return;
        }
        String status = chatContactMO.getPresenceType();
        if (chatContactMO.getPresenceType() == null) {
            ivActionStatusIndicator.setImageResource(R.drawable.ic_status_available);
            return;
        }
        if (status.equals("") || status.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
            ivActionStatusIndicator.setImageResource(R.drawable.ic_status_available);
        } else if (status.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
            ivActionStatusIndicator.setImageResource(R.drawable.ic_status_busy);
        } else if (status.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
            ivActionStatusIndicator.setImageResource(R.drawable.ic_status_away);
        }
    }

    @Override
    public void displayContactImages(ArrayList<String> contactIds, String groupId) {
        gpContactIds.clear();
        gpContactIds.addAll(contactIds);
        this.groupid = groupId;
        reloadImages();
    }

    protected void reloadImages() {
        if (this.groupid == null) {
            loadProfileImages();
        } else if (!this.groupid.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            this.groupImgUrl = MphRxUrl.getFetchContactGroupImageUrl(this.groupid);
            setGroupProfileImage(this.groupImgUrl);
        } else {
            this.groupImgUrl = MphRxUrl.getFetchContactGroupImageUrl(this.groupid);
            com.android.volley.toolbox.ImageLoader imageLoader = Network.getImageLoader();
            imageLoader.get(this.groupImgUrl, new ImageLoader.ImageListener() {
                private void loadImages(@Nullable final String url) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            //FIXME
                            // sanity check (only possible for 19 and above)
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && !isAttachedToWindow())
//                                return;

                            if (url == null) {
                                loadProfileImages();
                            } else {
                                setGroupProfileImage(url);
                            }
                        }
                    });
                }

                @Override
                public void onResponse(final ImageContainer response, boolean isImmediate) {
                    if (!response.getRequestUrl().equals(CustomProfileVIewLayout.this.groupImgUrl))
                        return;

                    if (response.getBitmap() == null) {
                        loadImages(null);
                    } else {
                        loadImages(response.getRequestUrl());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    // FIXME need url validation here
                    loadImages(null);
                }
            });
        }
    }

    private void setGroupProfileImage(String url) {
        resetVisibility(true);
        mPic[0].setTag(groupid);
        mPic[0].setImageUrl(url, Network.getImageLoader());
    }


    @Override
    public void setRectangleImage(boolean rectangleImage) {
        this.mIsRectangular = rectangleImage;
        // FIXME note this is assuming that a rectangular image will never change to circular
        // ideally the rectangle vs circular stuff should be part of inflaction via custom attributes
        // and there should be NO setter for it
        if (mIsRectangular) {
            for (int i = 0; i < 4; i++) {
                mPic[i].setBackgroundColor(0xFFFFFFFF); // FIXME this is just to handle the default / error images
                mPic[i].setShapeType(mIsRectangular, ImageView.ScaleType.FIT_XY);
            }
        }
    }

    public void fallback() {
        resetVisibility(false);
        mPic[0].setVisibility(VISIBLE);
    }
}
