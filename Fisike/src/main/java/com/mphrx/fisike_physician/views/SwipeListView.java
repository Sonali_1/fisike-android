package com.mphrx.fisike_physician.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

/**
 * Created by brijesh on 19/11/15.
 */
public class SwipeListView extends FrameLayout {

    public interface State {
        interface Data {
            int EMPTY = 0x00;
            int NORMAL = 0x10;
        }

        interface Network {
            int NONE = 0x00;
            int LOADING = 0x01;
            int ERROR = 0x02;
        }
    }

    private final LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

    private int mState = -1;

    private boolean mOnRefreshListenerAvailable = false;

    private SwipeRefreshLayout mTarget;
    private View mErrorView;
    private Snackbar mErrorSnack;
    private View mFullLoadingView;
    private ImageView mErrorIcon;
    private TextView mErrorMessage;
    private TextView mErrorButton;
    private ProgressBar mFullLoadingSpinner;
    private boolean mBlockedPage;

    public SwipeListView(Context context) {
        super(context);
        init();
    }

    public SwipeListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {



        inflate(getContext(), R.layout.layout_swipe_refresh, this);

        mTarget = (SwipeRefreshLayout) findViewById(R.id.__nsrl_swipe_refresh_layout__);
        mFullLoadingView = findViewById(R.id.__nsrl_fullscreen_loading_layout__);
        mErrorView = findViewById(R.id.__nsrl_fullscreen_error_layout__);

        mTarget.setEnabled(false);  // by default the swipe-refresh behavior is off
        mErrorIcon = (ImageView) mErrorView.findViewById(R.id.__nsrl_fullscreen_error_icon__);
        mErrorMessage = (TextView) mErrorView.findViewById(R.id.__nsrl_fullscreen_error_message__);
        mErrorButton = (TextView) mErrorView.findViewById(R.id.__nsrl_fullscreen_error_retry__);

        // set default state
        mFullLoadingView.setVisibility(GONE);
        mErrorView.setVisibility(GONE);
        mState = 0;

        // set color scheme for swipe refresh progress circle
        mTarget.setColorSchemeResources(R.color.action_bar_bg);

        // set color scheme for full screen progress circle
        mFullLoadingSpinner = (ProgressBar) mFullLoadingView.findViewById(R.id.__nsrl_fullscreen_loading_spinner__);
        mFullLoadingSpinner.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.theme_green),
                PorterDuff.Mode.SRC_ATOP);
        mErrorIcon.setColorFilter(0);
    }

    /**
     * @see {@link #setState(int, int, String, OnClickListener)}
     */
    public void setDataState(int dataState) {
        if (mState == -1) {
            setState(dataState, 0, null, null);
        } else {
            int newState = dataState | (0x0F & mState);
            setState(newState, 0, null, null);
        }
    }

    /**
     * @see {@link #setState(int, int, String, OnClickListener)}
     */
    public void setNetworkState(int netState, @DrawableRes int errorIcon,
                                @Nullable String errorMessage, @Nullable OnClickListener retryListener) {
        if (mState == -1) {
            setState(netState, 0, null, null);
        } else {
            int newState = netState | (0xF0 & mState);
            setState(newState, errorIcon, errorMessage, retryListener);
        }
    }

    /**
     * @param state         A valid state composed from {@link State.Data} & {@link State.Network}
     * @param errorIcon     An optional icon to show when full error screen is to be shown i.e.
     *                      state == {@link State.Data.EMPTY} | {@link State.Network.ERROR}
     * @param errorMessage  An optional icon to show when error screen (full/inline) is to be shown i.e.
     *                      state & {@link State.Network.ERROR}
     * @param retryListener An optional click listener to use for retry when error screen (full/inline) is to be shown i.e.
     *                      state & {@link State.Network.ERROR}
     */
    public void setState(int state, @DrawableRes int errorIcon,
                         @Nullable String errorMessage, @Nullable OnClickListener retryListener) {
        int dataState = state & 0xF0;
        int netState = state & 0x0F;

        switch (netState) {
            case State.Network.LOADING:
                int temp=dataState;
                if(mBlockedPage)
                {
                    temp=State.Data.EMPTY;
                }
                mTarget.setEnabled(false);  // disable swiping when already loading

                switch (temp) {
                    case State.Data.EMPTY:
                        mFullLoadingSpinner.setIndeterminate(false);
                        mFullLoadingView.setVisibility(VISIBLE);
                        mTarget.setRefreshing(false);
                        break;
                    case State.Data.NORMAL:
                        mFullLoadingSpinner.setIndeterminate(true);
                        mFullLoadingView.setVisibility(GONE);
                        mTarget.setRefreshing(true);
                        break;
                }
                mErrorView.setVisibility(GONE);
                if (mErrorSnack != null)
                    mErrorSnack.dismiss();
                break;
            case State.Network.ERROR:
                mFullLoadingSpinner.setIndeterminate(false);
                mFullLoadingView.setVisibility(GONE);
                mTarget.setRefreshing(false);
                errorMessage = TextUtils.isEmpty(errorMessage) ? MyApplication.getAppContext().getString(R.string.default_error_msg) : errorMessage;
                switch (dataState) {
                    case State.Data.EMPTY:
                        mErrorIcon.setImageResource(errorIcon);
                        mErrorMessage.setText(errorMessage);
                        if (retryListener == null) {
                            mErrorButton.setVisibility(GONE);
                        } else {
                            mErrorButton.setOnClickListener(retryListener);
                        }
                        mErrorView.setVisibility(VISIBLE);
                        if (mErrorSnack != null)
                            mErrorSnack.dismiss();
                        mTarget.setEnabled(retryListener == null && mOnRefreshListenerAvailable);  // enable swiping IFF retry policy is missing
                        break;
                    case State.Data.NORMAL:
                        mErrorSnack = getInlineError();
                        mErrorSnack.dismiss();
                        mErrorSnack.setText(errorMessage);
                        if (retryListener == null) {
                            mErrorSnack.setAction(null, null);
                        } else {
                            mErrorSnack.setAction("Retry", retryListener);
                        }
                        mErrorView.setVisibility(GONE);
                        mErrorSnack.show();
                        mTarget.setEnabled(mOnRefreshListenerAvailable);  // always attempt to enable swiping in case of snack error
                        break;
                }
                break;
            default:
                mTarget.setEnabled(mOnRefreshListenerAvailable);
                mErrorView.setVisibility(GONE);
                if (mErrorSnack != null)
                    mErrorSnack.dismiss();
                mFullLoadingSpinner.setIndeterminate(false);
                mFullLoadingView.setVisibility(GONE);
                mTarget.setRefreshing(false);
                break;
        }

        mState = state;
    }

    private Snackbar getInlineError() {
        mErrorSnack = Snackbar.make(this, "", Snackbar.LENGTH_SHORT);
        mErrorSnack.setActionTextColor(getResources().getColor(R.color.action_bar_bg));
        return mErrorSnack;
    }

    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        mTarget.setOnRefreshListener(listener);
        mOnRefreshListenerAvailable = (listener != null);
        mTarget.setEnabled(mOnRefreshListenerAvailable);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        int id = child.getId();
        if (id == R.id.__nsrl_swipe_refresh_layout__ ||
                id == R.id.__nsrl_fullscreen_error_layout__ ||
                id == R.id.__nsrl_fullscreen_loading_layout__) {
            super.addView(child, index, params);
        } else {
            mTarget.addView(child);
        }
    }

    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params, boolean preventRequestLayout) {
        int id = child.getId();
        if (id == R.id.__nsrl_swipe_refresh_layout__ ||
                id == R.id.__nsrl_fullscreen_error_layout__ ||
                id == R.id.__nsrl_fullscreen_loading_layout__) {
            return super.addViewInLayout(child, index, params, preventRequestLayout);
        } else {
            mTarget.addView(child);
            return false;
        }
    }

    public void setBlockedPage(boolean block) {
        mBlockedPage = block;
    }

    public boolean getBlockedPage() {
        return mBlockedPage;
    }


}
