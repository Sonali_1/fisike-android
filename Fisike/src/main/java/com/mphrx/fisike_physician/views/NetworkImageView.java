package com.mphrx.fisike_physician.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by pranav on 08/12/15.
 */
public class NetworkImageView extends com.android.volley.toolbox.NetworkImageView {

    private ScaleType mScaleForDefaultImage = ScaleType.CENTER,
            mScaleForErrorImage = ScaleType.CENTER,
            mScaleForNormalImage = ScaleType.CENTER;

    private @DrawableRes int mResDefault;
    private @DrawableRes int mResError;

    public NetworkImageView(Context context) {
        super(context);
    }

    public NetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setDefaultImageResId(int defaultImage, ScaleType scaleType) {
        super.setDefaultImageResId(defaultImage);
        mResDefault = defaultImage;
        mScaleForDefaultImage = scaleType;
    }

    public void setErrorImageResId(int errorImage, ScaleType scaleType) {
        super.setErrorImageResId(errorImage);
        mResError = errorImage;
        mScaleForErrorImage = scaleType;
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (resId == mResDefault) {
            super.setScaleType(mScaleForDefaultImage);
        } else if (resId == mResError) {
            super.setScaleType(mScaleForErrorImage);
        }
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        super.setScaleType(scaleType);
        mScaleForNormalImage = scaleType;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        super.setScaleType(mScaleForNormalImage);
    }

    @Override
    public void setImageUrl(String url, ImageLoader imageLoader) {
        super.setImageUrl(url, imageLoader);
    }
}
