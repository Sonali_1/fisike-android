package com.mphrx.fisike_physician.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.mphrx.fisike.R;

/**
 * Created by brijesh on 25/11/15.
 */
public class TimerView extends View {

    private Paint mDrawPaint;
    private Paint mTextPaint;

    private float mPadding;
    private float mStrokeWidth;

    private float mMaxTime;
    private float mProgressTime;

    private Rect mRect;
    private String mText;
    private RectF mDrawRect;
    Context context;
    private int progress_color;
    private int circle_color;

    public TimerView(Context context) {
        super(context);
        this.context=context;
        init(null);
    }

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init(attrs);
    }

    public TimerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.timerView);
        circle_color = attributeArray.getColor(R.styleable.timerView_circle_color, Color.WHITE);
        progress_color = attributeArray.getColor(R.styleable.timerView_progress_color, Color.LTGRAY);
        mDrawPaint = new Paint();
        mDrawPaint.setAntiAlias(true);
        mDrawPaint.setStyle(Paint.Style.STROKE);
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setStyle(Paint.Style.STROKE);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setColor(circle_color);
        mPadding = 5;/*getContext().getResources().getDimensionPixelSize(R.dimen.padding);*/
        mStrokeWidth = 5F;
        mDrawPaint.setStrokeWidth(mStrokeWidth);

        mRect = new Rect();
        mDrawRect = new RectF();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (getWidth() > 0 && getHeight() > 0) {
            mDrawPaint.setColor(circle_color);
            mDrawRect.set(mStrokeWidth, mPadding, getHeight() + mStrokeWidth - mPadding * 2, getHeight() - mPadding);
            canvas.drawOval(mDrawRect, mDrawPaint);
            //mDrawPaint.setColor(getResources().getColor(R.color.white_50));
            mDrawPaint.setColor(progress_color);
            if (mMaxTime > 0) {
                mDrawRect.set(mStrokeWidth, mPadding, getHeight() + mStrokeWidth - mPadding * 2, getHeight() - mPadding);
                canvas.drawArc(mDrawRect, 270, -(mProgressTime / mMaxTime) * 360, false, mDrawPaint);
            }

            mText = Math.round(mProgressTime) + "";
            mTextPaint.setTextSize((getHeight() - mPadding * 2) / 2.5f);
            mTextPaint.getTextBounds(mText, 0, mText.length(), mRect);

            canvas.drawText(mText, mStrokeWidth + (getHeight() - mPadding * 2) / 2, (getHeight() + mRect.height()) / 2, mTextPaint);
        }
        super.onDraw(canvas);
    }

    public void setTimerProgress(int maxTime, long progressTime) {
        mMaxTime = maxTime;
        mProgressTime = progressTime;
        invalidate();
    }
}
