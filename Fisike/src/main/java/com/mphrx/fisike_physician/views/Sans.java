package com.mphrx.fisike_physician.views;

import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.mphrx.fisike.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xmb2nc on 24-02-2016.
 */
public class Sans {
    private static final String                HYPHEN_SEPARATOR = "-";
    private static final String                TTF_SUFFIX       = ".ttf";
    private static final Map<String, Typeface> CACHE            = new HashMap<>();

    private Sans() {
    }

    public enum Style {
        THIN("Thin", 0), LIGHT("Light", 1), REGULAR("Regular", 2), MEDIUM("Medium", 3), BOLD("Bold", 4), BLACK("Black",
                5);

        private String id;
        private int    value;

        Style(String id, int value) {
            this.id = id;
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public int getValue() {
            return value;
        }

        public static String getId(int value) {
            for (Style style : Style.values()) {
                if (style.value == value) {
                    return style.getId();
                }
            }
            return null;
        }
    }

    public static Typeface getTypeface(View view, int value) {
        String style = Style.getId(value);
        String path = "sans/OpenSans" + HYPHEN_SEPARATOR + style + TTF_SUFFIX;
        Typeface result = CACHE.get(path);
        if (result != null) {
            return result;
        }
        try {
            result = Typeface.createFromAsset(view.getContext().getAssets(), path);
        } catch (Exception ignoreMe) {
            Log.i("SANS", "Font path not found");
        }
        if (result == null) {
            System.out.println("Result Null");
            result = Typeface.create("sans", Typeface.NORMAL);
        }
        CACHE.put(path, result);
        return result;
    }

    protected static void init(View view, AttributeSet attrs) {
        if (view.isInEditMode()) {
            Log.i("Sans", "View is in edit mode");
            return;
        }
        if (!(view instanceof RTypeface)) {
            Log.i("Sans", "View should be instance of RTypeface");
            return;
        }
        int fontStyle = Style.REGULAR.getValue(); // Default
        if (attrs != null) {
            TypedArray styledAttrs = view.getContext().obtainStyledAttributes(attrs, R.styleable.RobotoView);
            fontStyle = styledAttrs.getInt(R.styleable.RobotoView_fontStyle, Style.REGULAR.getValue());
            styledAttrs.recycle();
        }
        ((RTypeface) view).setRobotoTypeface(getTypeface(view, fontStyle));
    }
}


