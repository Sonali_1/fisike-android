package com.mphrx.fisike_physician.views;

import android.graphics.Typeface;

/**
 * Created by brijesh on 05/08/15.
 */
public interface RTypeface {

    void setRobotoTypeface(Typeface typeface);

}
