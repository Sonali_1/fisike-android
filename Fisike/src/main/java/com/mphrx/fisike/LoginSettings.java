package com.mphrx.fisike;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.platform.SettingManager;

public class LoginSettings extends PreferenceActivity {

    private SettingMO settingMO;
    private SettingManager settingManager;

    private EditTextPreference autoLogoutTime;
    private CheckBoxPreference autoLogoutCheckBox;

    private EditTextPreference messageStoreSize;
    private EditTextPreference fisikeServer;
    private EditTextPreference fisikeServerPort;
    private EditTextPreference fisikehost;
    private EditTextPreference mphRxServer;
    private EditTextPreference mphRxServerPort;
    private CheckBoxPreference fisikeuserhttps;
    private CheckBoxPreference userhttps;

    // Added new checkbox for xmppServer
    private CheckBoxPreference xmppServerCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

//        setContentView(R.layout.setting_activity);

        try {
            settingManager = SettingManager.getInstance();
            settingMO = settingManager.getSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }

        fisikehost = (EditTextPreference) findPreference("fisikehost");
        fisikehost.getEditText().setSingleLine(true);
        fisikehost.setText(settingMO.getFisikeServerHost());
        fisikehost.setSummary(settingMO.getFisikeServerHost());
        fisikeServerPort = (EditTextPreference) findPreference("fisikeServerPort");
        fisikeServerPort.getEditText().setSingleLine(true);
        fisikeServerPort.setText(settingMO.getFisikeServerPort());
        fisikeServerPort.setSummary(settingMO.getFisikeServerPort());
        fisikeServer = (EditTextPreference) findPreference("fisikeServer");
        fisikeServer.getEditText().setSingleLine(true);
        fisikeServer.setText(settingMO.getFisikeServerIp());
        fisikeServer.setSummary(settingMO.getFisikeServerIp());
        mphRxServer = (EditTextPreference) findPreference("mphRxServer");
        mphRxServer.getEditText().setSingleLine(true);
        mphRxServer.setText(settingMO.getServerIP());
        mphRxServer.setSummary(settingMO.getServerIP());
        mphRxServerPort = (EditTextPreference) findPreference("mphRxServerPort");
        mphRxServerPort.getEditText().setSingleLine(true);
        mphRxServerPort.setText(settingMO.getServerPort());
        mphRxServerPort.setSummary(settingMO.getServerPort());

        setPreferenceChange();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    private void setPreferenceChange() {
        autoLogoutTime = (EditTextPreference) findPreference("autoLogoutTime");
        autoLogoutTime.getEditText().setSingleLine(true);
        autoLogoutTime.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            private ProgressDialog dialog;

            public boolean onPreferenceChange(Preference preference, Object object) {
                dialog = ProgressDialog.show(LoginSettings.this, "", getResources().getString(R.string.Updating_Settings_Please_wait), true);

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String autoLogoutTimeValue = autoLogoutTime.getText();
                            settingMO.setAutoLogoutTimeInterval(Float.parseFloat(autoLogoutTimeValue));
                            settingManager.saveSettings(settingMO);
                            dialog.dismiss();
                        } catch (NumberFormatException e1) {
                            e1.printStackTrace();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    dialog.dismiss();
                                }
                            });
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    dialog.dismiss();
                                }
                            });
                        }
                    }
                };

                SaveThread.start();
                return true;
            }
        });

		/*
         * Added code for the xmpp server config
		 */

        xmppServerCheckBox = (CheckBoxPreference) findPreference("useEjabberd");
        // Add a listener to check for the change in preference
        xmppServerCheckBox.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            private ProgressDialog dialog;

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                dialog = ProgressDialog.show(LoginSettings.this, "", getResources().getString(R.string.Updating_Settings_Hang_Tight), true);

                Thread SaveThread = new Thread() {
                    public void run() {

                        try {
                            if (xmppServerCheckBox.isChecked()) {
                                settingMO.setXmppServerType("ejabberd");
                            } else {
                                settingMO.setXmppServerType("openfire");
                            }
                            settingManager.saveSettings(settingMO);
                            dialog.dismiss();
                        } catch (Exception eX) {
                            eX.printStackTrace();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    dialog.dismiss();
                                }
                            });
                        }
                    }
                };

                SaveThread.start();
                return true;
            }
        });

        autoLogoutCheckBox = (CheckBoxPreference) findPreference("autoLogout");
        autoLogoutCheckBox.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            private ProgressDialog dialog;

            public boolean onPreferenceChange(Preference preference, Object object) {
                dialog = ProgressDialog.show(LoginSettings.this, "", getResources().getString(R.string.Updating_Settings_Please_wait), true);

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            if (autoLogoutCheckBox.isChecked()) {
                                settingMO.setAutoLogout(true);
                            } else {
                                settingMO.setAutoLogout(false);
                            }
                            settingManager.saveSettings(settingMO);
                            dialog.dismiss();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    dialog.dismiss();
                                }
                            });
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        messageStoreSize = (EditTextPreference) findPreference("messageStoreSize");
        messageStoreSize.getEditText().setSingleLine(true);
        messageStoreSize.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String messageStore = messageStoreSize.getText();
                            settingMO.setNoOfMsgsToStoreLocally(Integer.parseInt(messageStore));
                            settingManager.saveSettings(settingMO);
                        } catch (NumberFormatException e1) {
                            e1.printStackTrace();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        fisikehost.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String host = fisikehost.getText();
                            settingMO.setFisikeServerHost(host);
                            settingManager.saveSettings(settingMO);
                            fisikehost.setSummary(host);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        fisikeServer.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String server = fisikeServer.getText();
                            settingMO.setFisikeServerIp(server);
                            settingManager.saveSettings(settingMO);
                            fisikeServer.setSummary(server);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        fisikeServerPort.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String port = fisikeServerPort.getText();
                            settingMO.setFisikeServerPort(port);
                            settingManager.saveSettings(settingMO);
                            fisikeServerPort.setSummary(port);
                        } catch (NumberFormatException e1) {
                            e1.printStackTrace();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        fisikeuserhttps = (CheckBoxPreference) findPreference("fisikeuserhttps");
        autoLogoutCheckBox.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            private ProgressDialog dialog;

            public boolean onPreferenceChange(Preference preference, Object object) {
                dialog = ProgressDialog.show(LoginSettings.this, "", getResources().getString(R.string.Updating_Settings_Please_wait), true);

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            if (fisikeuserhttps.isChecked()) {
                                settingMO.setFisikeUseHTTPS(true);
                            } else {
                                settingMO.setFisikeUseHTTPS(false);
                            }
                            settingManager.saveSettings(settingMO);
                            dialog.dismiss();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        mphRxServer.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String server = mphRxServer.getText();
                            settingMO.setServerIP(server);
                            settingManager.saveSettings(settingMO);
                            mphRxServer.setSummary(server);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        mphRxServerPort.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            public boolean onPreferenceChange(Preference preference, Object object) {

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            String port = mphRxServerPort.getText();
                            settingMO.setServerPort(port);
                            settingManager.saveSettings(settingMO);
                            mphRxServerPort.setSummary(port);
                        } catch (NumberFormatException e1) {
                            e1.printStackTrace();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });

        userhttps = (CheckBoxPreference) findPreference("userhttps");
        userhttps.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            private ProgressDialog dialog;

            public boolean onPreferenceChange(Preference preference, Object object) {
                dialog = ProgressDialog.show(LoginSettings.this, "", getResources().getString(R.string.Updating_Settings_Please_wait), true);

                Thread SaveThread = new Thread() {
                    public void run() {
                        try {
                            if (userhttps.isChecked()) {
                                settingMO.setUseHTTPS(true);
                            } else {
                                settingMO.setUseHTTPS(false);
                            }
                            settingManager.saveSettings(settingMO);
                            dialog.dismiss();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                SaveThread.start();
                return true;
            }
        });
    }
}