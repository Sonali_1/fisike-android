package com.mphrx.fisike;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.AppTour.AppTourActivity;
import com.mphrx.fisike.adapter.SupportAdapter;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SupportDataItem;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.models.ContentResult;
import com.mphrx.fisike.models.SupportAgreementModel;
import com.mphrx.fisike.persistence.AgreementFileDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SupportActivity extends BaseActivity implements SupportAdapter.clickListener {
    private RecyclerView supportRecyclerView;
    private SupportAdapter supportAdapter;
    private RecyclerView.LayoutManager supportLayoutManager;
    List<String> titles;
    List<String> iconsList = new ArrayList<>();
    List<String> updatedList = new ArrayList<>();
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title, tvAppVersion;
    private IconTextView bt_toolbar_right;
    ArrayList<SupportDataItem> itemList;
    private FrameLayout frameLayout;
    private ArrayList<AgreementModel> agreementModels;
    private ArrayList<SupportAgreementModel> supportAgreementModels;
    private List<String> titlesAgreement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_support, frameLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else {
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(R.string.Support);
        tvAppVersion = (CustomFontTextView) findViewById(R.id.tv_app_version);

        titles = new ArrayList<>();
        titles = Arrays.asList(getResources().getStringArray(R.array.support_title));

        iconsList = Arrays.asList(getResources().getStringArray(R.array.support_icons));

        supportAgreementModels = addItemsToTitle(Utils.getUserSelectedLanguage(SupportActivity.this));

        supportRecyclerView = (android.support.v7.widget.RecyclerView) this.findViewById(R.id.my_recycler_view);
        supportLayoutManager = new LinearLayoutManager(this);
        supportRecyclerView.setLayoutManager(supportLayoutManager);
        itemList = new ArrayList<SupportDataItem>();
        for (int i = 0; i < updatedList.size(); i++) {
            SupportDataItem data = new SupportDataItem();
            data.setText(updatedList.get(i));
            if (titlesAgreement.size() != 0 && i >= titles.size())
                data.setResIcon(getResources().getString(R.string.fa_agreements));
            else
                data.setResIcon(iconsList.get(i));
            itemList.add(data);
        }
        supportAdapter = new SupportAdapter(itemList);
        supportAdapter.setClickListener(this);
        supportRecyclerView.setAdapter(supportAdapter);

        ((CustomFontTextView) findViewById(R.id.manage_life)).setText(getResources().getString(R.string.manage_your_life, getString(R.string.app_name)));
        try {
            tvAppVersion.setText(getResources().getString(R.string.app_version,
                    getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private ArrayList<SupportAgreementModel> addItemsToTitle(String language) {
        agreementModels = AgreementFileDBAdapter.getInstance(SupportActivity.this).fetchAllAgreements();
        supportAgreementModels = new ArrayList<>();
        titlesAgreement = new ArrayList<>();
        int k = 0;
        for (int i = 0; i < agreementModels.size(); i++) {
            SupportAgreementModel supportAgreementModel = new SupportAgreementModel();
            supportAgreementModel.setReadDate(agreementModels.get(i).getReadDate());
            ArrayList<ContentResult> contentResults = agreementModels.get(i).getContentResult();
            for (int j = 0; j < contentResults.size(); j++) {
                if (contentResults.get(j).getLanguage().equals(language)) {
                    supportAgreementModel.setTitle(contentResults.get(j).getName());
                    titlesAgreement.add(k, contentResults.get(j).getName());
                    k++;
                    supportAgreementModel.setContent(contentResults.get(j).getContent());
                    break;
                }
            }

            supportAgreementModels.add(i, supportAgreementModel);
        }
        updatedList.clear();
        updatedList.addAll(titles);
        updatedList.addAll(titlesAgreement);
        return supportAgreementModels;
    }

    @Override
    public void itemClicked(View view, int position) {
        // TODO Auto-generated method stub
        showView(updatedList.get(position));
    }

    public void showView(String text) {
        Intent i = null;
        if (text.equalsIgnoreCase(this.getResources().getString(R.string.user_guide))) {
            i = new Intent(this, SettingWebView.class);
            i.putExtra(VariableConstants.TITLE_BAR, text);
            i.putExtra(VariableConstants.WEB_URL, APIManager.getInstance().getFAQURL());
            if (i != null && Utils.showDialogForNoNetwork(this, false))
                startActivity(i);
        } else if (text.equalsIgnoreCase(this.getResources().getString(R.string.contact_us))) {

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(this, userMO));
            intent.setData(Uri.parse(getResources().getString(R.string.mailto))); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        } else if (text.equalsIgnoreCase(this.getResources().getString(R.string.rate_us))) {

            String pckg = this.getPackageName();
            if (pckg.endsWith(".debug"))
                pckg = pckg.replace(".debug", "");

            Uri uri = Uri.parse(TextConstants.MARKET_URL + pckg);
            i = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(i);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(DeviceUtils.getPlayStoreUri())));
            }
        } else if (text.equalsIgnoreCase(this.getResources().getString(R.string.share_app))) {
            String pckg = this.getPackageName();
            if (pckg.endsWith(".debug"))
                pckg = pckg.replace(".debug", "");
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app_subject, getString(R.string.app_name)));
            intent.putExtra(Intent.EXTRA_TEXT, getString(BuildConfig.isPatientApp ? R.string.share_app_pat_message : R.string.share_app_phy_message, getResources().getString(R.string.app_name)) + " " + DeviceUtils.getPlayStoreUri());
            i = Intent.createChooser(intent, TextConstants.SHARE_APP);
            if (i != null && Utils.showDialogForNoNetwork(this, false))
                startActivity(i);
        } else if (text.equalsIgnoreCase(this.getResources().getString(R.string.app_tour))) {
            i = new Intent(this, AppTourActivity.class);
            startActivity(i);
        } else if (text.equalsIgnoreCase(this.getResources().getString(R.string.about))) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                new LoadAboutUs().execute(MphRxUrl.getAboutUsUrl());
            }
        } else {
            Intent intent = new Intent(SupportActivity.this, AgreementsActivity.class);
            int index = titlesAgreement.indexOf(text);
            intent.putExtra(TextConstants.FROM_SUPPORT, true);
            intent.putExtra(TextConstants.TEXT, supportAgreementModels.get(index).getContent());
            intent.putExtra(TextConstants.NAME_AGREEMENT, supportAgreementModels.get(index).getTitle());
            intent.putExtra(TextConstants.READ_DATE, supportAgreementModels.get(index).getReadDate());
            startActivity(intent);
        }

    }

    private class LoadAboutUs extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... url) {
            String jsonString = APIManager.getInstance().executeHttpGet(url[0], SharedPref.getAccessToken());
            return jsonString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dismissProgressDialog();
            if (result != null) {
                String content = "";
                try {
                    JSONObject jsonObj = new JSONObject(result);
                    content = jsonObj.getString("content");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(SupportActivity.this, AgreementsActivity.class);
                intent.putExtra(TextConstants.FROM_SUPPORT, true);
                intent.putExtra("text", content);
                intent.putExtra("name", getResources().getString(R.string.about));
                intent.putExtra("readDate", "");
                startActivity(intent);
            } else {
                //TODO-Text text from Vibha MNV-8921
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.about_not_available), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
