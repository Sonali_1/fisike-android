package com.mphrx.fisike.exception;

public class UnexpectedException extends Exception {
	private static final long serialVersionUID = 1589881067813568597L;

	/**
	 * 
	 */
	public UnexpectedException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Exception with
	 * 
	 * @param detailMessage
	 */
	public UnexpectedException(String detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructs a new {@code RuntimeException} with the current stack trace, the specified detail message and the specified cause.
	 * 
	 * @param detailMessage
	 *            the detail message for this exception.
	 * @param throwable
	 *            the cause of this exception.
	 */
	public UnexpectedException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	/**
	 * Constructs a new {@code RuntimeException} with the current stack trace and the specified cause.
	 * 
	 * @param throwable
	 *            the cause of this exception.
	 */
	public UnexpectedException(Throwable throwable) {
		super(throwable);
	}

}
