
package com.mphrx.fisike;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.Queue.AttachmentQueue;
import com.mphrx.fisike.Queue.DownloadAttachmentFile;
import com.mphrx.fisike.Queue.SendAttachmentFile;
import com.mphrx.fisike.adapter.MessageAdapter;
import com.mphrx.fisike.adapter.MessageItem;
import com.mphrx.fisike.adapter.SectionItem;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.asynctask.UpdateDatabaseAsyncTask;
import com.mphrx.fisike.background.DownloadFileRunnable;
import com.mphrx.fisike.background.EncriptFile;
import com.mphrx.fisike.background.LoadContactsProfilePicAsyn;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.QuestionActivityMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.AttchmentDBAdapter;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.services.MessengerService.MessengerBinder;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ErrorMessage;
import com.mphrx.fisike_physician.activity.GroupDetailActivity;
import com.mphrx.fisike_physician.activity.HealthActivity;
import com.mphrx.fisike_physician.activity.ProfileActivity;
import com.mphrx.fisike_physician.activity.SearchUserActivity;
import com.mphrx.fisike_physician.activity.ViewPatientProfile;
import com.mphrx.fisike_physician.activity.ViewProfile;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import org.jivesoftware.smack.XMPPConnection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * MessageActivity is a main Activity to show a ListView containing Message items
 */
public class MessageActivity extends BaseActivity implements OnClickListener, OnItemClickListener, OnMenuItemClickListener {
    private static final int CAMERA_IMAGE_INTENT = 0;
    private static final int GALLERY_IMAGE_INTENT = 1;
    private static final int CAMERA_VIDEO_INTENT = 2;
    private static final int GALLERY_VIDEO_INTENT = 3;
    private static final int ATTACHMET_QUESTIONARY = 4;
    private static final int ATTACHMENT_PREVIEW = 5;
    private static final int QUESTION_BUILDER_ACTIVITY = 6;
    private static final int ADD_COLLEAGUE = 10;

    private static final int EXIT_GROUP = 11;
    private static boolean isHandled = true;

    protected ArrayList<ChatMessageMO> onlineChat = new ArrayList<ChatMessageMO>();
    protected LastSegmentName captureLastId;
    // This is the comparator that is defined for comparing messages in the list
    Comparator<MessageItem> messageComparator = new Comparator<MessageItem>() {

        public int compare(MessageItem object1, MessageItem object2) {
            if (null == object1) {
                return -1;
            } else if (null == object2) {
                return 1;
            }
            if (object1.isSection() || object2.isSection()) {
                return 0;
            }

            ChatMessageMO chatMessageMO1 = ((ChatMessageMO) object1);
            ChatMessageMO chatMessageMO2 = ((ChatMessageMO) object2);

            if (chatMessageMO1.getLasUpdatedTimeUTC() < chatMessageMO2.getLasUpdatedTimeUTC()) {
                return -1;
            } else if (chatMessageMO1.getLasUpdatedTimeUTC() > chatMessageMO2.getLasUpdatedTimeUTC()) {
                return 1;
            }
            return 0;
        }
    };
    private ArrayList<MessageItem> messages;
    private MessageAdapter adapter;
    private String username;
    private String senderSecondaryId;
    private ListView listMessage;
    private String nickName;
    private UserMO userMO;
    private SettingMO settings;
    private ChatConversationMO chatConversationMO;
    private int unreadCount;
    private boolean isSubscribeScreen = false;
    private boolean isSubscriptionSend = false;
    private SectionItem unreadHeader;
    private boolean isRecentChat = false;
    private boolean isGroupChat = false;
    private boolean isRegisteredBroadcast = false;
    private MyObserver observer;
    private File cameraImageFile;
    private String imageFileName;
    private int myStatus = VariableConstants.chatConversationMyStatusActive;
    private boolean isCapturedImage;
    private String filePath;
    private Toolbar toolBarChatView;
    private ChatContactMO chatContactMO;
    private UserType chatContactUserType;
    private AsyncTask<Void, Void, Void> loadContactsRealName;
    private AsyncTask<Void, Void, Void> loadContactProfilePicAsyn;
    private ImageView ivActionBack, ivActionPhone;

    private CustomFontTextView tvActionContactName, tvNumberofParticipants;
    private LinearLayout llActionContactDetail;
    private GroupTextChatMO groupTextChatMO;
    private CustomProfileVIewLayout groupProfileLayout;
    private EditText etMessageText;
    private CustomFontButton btnSendMessage;
    private ProgressBar progressBar;
    private boolean isGroupCreated;
    private Handler groupCreatedHandler;
    private String contactPhoneNumber;
    private HashMap<String, String> senderNickNameHash;
    //    private ArrayList<ChatContactMO> chatContactMOArrayList;
    private String groupName;
    public static boolean isActivityRunning;
    /**
     * This is the delivery status listener
     */
    private BroadcastReceiver deliveryStatusListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            ChatConversationMO chatConvMO = (ChatConversationMO) extras.get(VariableConstants.BROADCAST_CHAT_CONV_MO);
            ChatMessageMO chatMessageMO = (ChatMessageMO) extras.get(VariableConstants.BROADCAST_CHAT_MSG_MO);
            boolean isAckFromServer = extras.getBoolean(VariableConstants.ACK_MESSAGE_SERVER_RECEIVED);

            // Check if the message is for the same conversation
            if (!isAckFromServer && chatConvMO != null && chatConversationMO != null
                    && chatConvMO.getPersistenceKey() == chatConversationMO.getPersistenceKey()) {
                changeMessageDeliveryStatus(chatMessageMO.getPersistenceKey());
            } else if (isAckFromServer && chatConvMO != null && chatConversationMO != null
                    && chatConvMO.getPersistenceKey() == chatConversationMO.getPersistenceKey()) {
                changeMessageAckStatus(chatMessageMO.getPersistenceKey());
            }
        }
    };
    /**
     * This is the delivery status listener
     */
    private BroadcastReceiver msgStatusChangeListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ChatConversationMO chatConvMO = (ChatConversationMO) intent.getExtras().get(VariableConstants.BROADCAST_CHAT_CONV_MO);
            ChatMessageMO chatMessageMO = (ChatMessageMO) intent.getExtras().get(VariableConstants.BROADCAST_CHAT_MSG_MO);

            // Check if the message is for the same conversation
            if (chatConvMO != null && chatConvMO.getPersistenceKey() == chatConversationMO.getPersistenceKey()) {
                changeMessageStatus(chatMessageMO.getPersistenceKey(), chatMessageMO.getMsgStatus());
            }
        }
    };
    /**
     * Broadcast for handling connection status
     */
    // This is the msg Received Listener
    private BroadcastReceiver msgReceivedListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ChatConversationMO chatConvMO = (ChatConversationMO) intent.getExtras().get(VariableConstants.BROADCAST_CHAT_CONV_MO);
            ChatMessageMO chatMessageMO = (ChatMessageMO) intent.getExtras().get(VariableConstants.BROADCAST_CHAT_MSG_MO);
            chatMessageMO = mService.setUserNickName(chatMessageMO, isGroupChat, senderNickNameHash);
            boolean isOfflineMessage = intent.getExtras().getBoolean(VariableConstants.IS_OFFLINE_MESSAGE);
            if (chatConvMO == null || chatMessageMO == null) {
                return;
            }

            // Check if the message is for the same conversation
            if (chatConvMO.getPersistenceKey() == chatConversationMO.getPersistenceKey()) {
                addMessageToView(chatMessageMO);
            } else if (!isOfflineMessage) {
                Utils.generateNotification(getApplicationContext(), VariableConstants.PUSH_NOTIFICATION_MESSAGE, false, chatConvMO);
            }
        }
    };
    private boolean isConversationCreated;
    private BroadcastReceiver statusChangeBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String chatPK = bundle.getString(VariableConstants.PERSISTENCE_KEY);
            if (chatPK != null && chatContactMO != null && chatPK.equalsIgnoreCase(chatContactMO.getPersistenceKey() + "")) {
                chatContactMO.setPresenceType(bundle.getString(VariableConstants.PRESENCE_TYPE));
                chatContactMO.setPresenceStatus(bundle.getString(VariableConstants.PRESENCE_MESSAGE));
                groupProfileLayout.setStatus(chatContactMO);
                setStatusMessage();
            }
        }
    };


    /**
     * This is the attachmentStatus change
     */
    private BroadcastReceiver refereshAttachmentStatusListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, final Intent intent) {
            Bundle bundle = intent.getExtras();

            ChatConversationMO chatConvMO = (ChatConversationMO) bundle.get(VariableConstants.BROADCAST_CHAT_CONV_MO);
            ChatMessageMO chatMessageMO = (ChatMessageMO) bundle.get(VariableConstants.BROADCAST_CHAT_MSG_MO);
            boolean isToRefreshView = bundle.getBoolean(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW);
            int percentage = bundle.getInt(TextConstants.DOWNLOAD_PERCENTAGE);
            refreshAttachment(chatConvMO, chatMessageMO, isToRefreshView, percentage);
        }

    };
    private BroadcastReceiver groupCreatedBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (progressBar.isShown()) {
                // update chat conversation mo that group has been created.
                chatConversationMO.setGroupStatus(TextConstants.GROUP_CREATED);
                ContentValues updatedValues = new ContentValues();
                updatedValues.put(ChatConversationDBAdapter.getGROUPSTATUS(), TextConstants.GROUP_CREATED);
                new UpdateDatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE, updatedValues).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                new DatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                chatConversationMO.setIsChatInitiated(1);

                mService.setChatConversation(chatConversationMO);

                dismissProgress();
                isGroupCreated = true;
                if (groupCreatedHandler != null) {
                    groupCreatedHandler.removeCallbacksAndMessages(null);
                    groupCreatedHandler = null;
                }
            }
        }
    };
    private BroadcastReceiver conversationChanged = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                ChatConversationMO chatConversationMO = (ChatConversationMO) intent.getExtras().get(VariableConstants.CONVERSATION_ITEM);
//                if(chatConversationMO.getJId().equals(MessageActivity.this.chatConversationMO.getJId())){
                MessageActivity.this.chatConversationMO = chatConversationMO;
                refreshMessages(false, true);
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private BroadcastReceiver newGroupCreatedBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (progressBar.isShown()) {
                // update chat conversation mo that group has been created.
                chatConversationMO.setGroupStatus(TextConstants.GROUP_CREATED);
                ContentValues updatedValues = new ContentValues();
                updatedValues.put(ChatConversationDBAdapter.getGROUPSTATUS(), TextConstants.GROUP_CREATED);
                new UpdateDatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE, updatedValues).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                new DatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                mService.setChatConversation(chatConversationMO);

                dismissProgress();
                isGroupCreated = true;
                if (groupCreatedHandler != null) {
                    groupCreatedHandler.removeCallbacksAndMessages(null);
                    groupCreatedHandler = null;
                }
            }
        }
    };
    private ProgressDialog progressDialog;
    private BottomSheetDialog mBottomSheetDialog;

    private FrameLayout frameLayout;

    public void refreshAttachment(final ChatConversationMO chatConvMO, final ChatMessageMO chatMessageMO, final boolean isToRefreshView,
                                  final int percentage) {
        new Thread(new Runnable() {
            public void run() {
                if (chatConversationMO == null) {
                    return;
                }
                chatConversationMO.getJId();
                if (isGroupChat && userMO.getUsername().equals(Utils.cleanXMPPServer(chatMessageMO.getSenderUserId()))) {// a%40khura.33mail.com@dev1
                    chatMessageMO.setSenderNickName(VariableConstants.YOU);
                }

                try {
                    chatMessageMO.setPercentage(percentage);
                } catch (Exception e) {
                }

                Bitmap thumbImage = MyApplication.getImageThumb();
                if (thumbImage != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thumbImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    chatMessageMO.setImageThumb(stream.toByteArray());
                }

                // MyApplication.setImageThumb(null);

                // Check if the message is for the same conversation
                if (chatConvMO != null && chatConvMO.getPersistenceKey() == chatConversationMO.getPersistenceKey()) {
                    int position = adapter.getPosition(chatMessageMO.getPersistenceKey());
                    if (position != -1) {
                        updateItemAtPosition(position, chatMessageMO);
                        AppLog.d("TAG", "run: position : " + position);
                    } else {
                        refreshFileMessages(chatMessageMO, isToRefreshView, chatConvMO.getNickName());
                        AppLog.d("TAG", "run: position : -1--------------");
                    }
                    String attachmentStatus = chatMessageMO.getAttachmentStatus();
                    if (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_RECEIVED)
                            || attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED)) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (Utils.showDialogForNoNetwork(MessageActivity.this, false)) {
                                    Utils.showApiError(MessageActivity.this);
                                }
                            }
                        });
                    }
                }
            }
        }).start();
    }

    /**
     * Change the status that are not in queue
     */
    protected void changeStaustForAttachments() {
        ArrayList<AttachmentMo> messagesInConversation = mService.getMessagesInConversationForAttachment(chatConversationMO);

        for (int i = 0; messagesInConversation != null && i < messagesInConversation.size(); i++) {
            AttachmentMo attachmentMo = messagesInConversation.get(i);
            String attachmentId = attachmentMo.getAttachmentId();
            String chatMessageMOPKey = attachmentMo.getChatMessageMOPKey();
            if (TextConstants.ATTACHMENT_STATUS_IN_PROGRESS.equals(attachmentMo.getStatus())
                    || TextConstants.ATTACHMENT_STATUS_WAITING.equals(attachmentMo.getStatus())) {
                // FileProcessStatus ismTaskQueueExist = TaskManager.getInstance().ismTaskQueueExist(chatMessageMOPKey);
//                if (!TaskManager.getInstance().ismTaskQueueExist(chatMessageMOPKey)) {
//                    if (null == attachmentId || "".equals(attachmentId)) {
//                        attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
//                    } else {
//                        attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
//                    }
//                }
                if (!AttachmentQueue.getInstance().ismTaskQueueExist(chatMessageMOPKey)) {
                    if (null == attachmentId || "".equals(attachmentId)) {
                        attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
                    } else {
                        attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
                    }
                }
            } else if (TextConstants.ATTACHMENT_STATUS_DOWNLOADING.equals(attachmentMo.getStatus())) {
                attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
            }
        }


        mService.storeAttachmentStatus(messagesInConversation);
    }

    private void updateItemAtPosition(final int position, final ChatMessageMO chatMessageMO) {


        runOnUiThread(new Runnable() {
            public void run() {
                adapter.setmMessages(chatMessageMO, position);
//                int visiblePosition = listMessage.getFirstVisiblePosition();
//                View view = listMessage.getChildAt(position - visiblePosition);
//                listMessage.getAdapter().getView(position, view, listMessage);
//                adapter.notifyDataSetInvalidated();
                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * This method registers the broadcast listeners for new messages and delivery status notifications
     */
    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            broadcastManager.registerReceiver(msgReceivedListener, new IntentFilter(VariableConstants.NEW_MSG_BROADCAST));
            broadcastManager.registerReceiver(deliveryStatusListener, new IntentFilter(VariableConstants.BROADCAST_DELIVERY_STATUS));
            broadcastManager.registerReceiver(msgStatusChangeListener, new IntentFilter(VariableConstants.BROADCAST_MSG_CHANGE_LISTNER));
            broadcastManager.registerReceiver(refereshAttachmentStatusListener, new IntentFilter(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED));
            broadcastManager.registerReceiver(statusChangeBroadcast, new IntentFilter(VariableConstants.BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED));
            broadcastManager.registerReceiver(groupCreatedBroadcast, new IntentFilter(VariableConstants.GROUP_INVITE_SENT_FOR_SINGLE_CHAT));
            broadcastManager.registerReceiver(newGroupCreatedBroadcast, new IntentFilter(VariableConstants.GROUP_INVITE_SENT));
            broadcastManager.registerReceiver(mBroadcastListener, new IntentFilter(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED));

            broadcastManager.registerReceiver(conversationChanged, new IntentFilter(VariableConstants.BROADCAST_GROUP_CHAT_ADD_MEMBER));
            isRegisteredBroadcast = true;
        }

    }

    /**
     * This method unregisters the two XMPP receivers - for new messages and delivery reciepts
     */
    private void unregisterBroadcastListeners() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        // Unregister Msg Received listener
        try {
            localBroadcastManager.unregisterReceiver(msgReceivedListener);
            localBroadcastManager.unregisterReceiver(refereshAttachmentStatusListener);
            // Unregister Delivery Status Listener
            localBroadcastManager.unregisterReceiver(deliveryStatusListener);
            localBroadcastManager.unregisterReceiver(msgStatusChangeListener);
            localBroadcastManager.unregisterReceiver(statusChangeBroadcast);
            localBroadcastManager.unregisterReceiver(groupCreatedBroadcast);
            localBroadcastManager.unregisterReceiver(conversationChanged);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastListener);

        } catch (Exception e) {
        }
        isRegisteredBroadcast = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActivityRunning = true;

    }

    @Override
    protected void onStop() {
        super.onStop();
        isActivityRunning = false;

    }

    /**
     * This method is called when this activity is created
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (BuildConfig.isPhysicianApp)
            setCurrentItem(-1);
        super.onCreate(savedInstanceState);
        if (isActivityRunning) {
            finish();
        }
        AppLog.d("Fisike", "MessageActivity - Activity onCreate");
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        //rendering layout inside framelayout to support side drawer
        getLayoutInflater().inflate(R.layout.chat_view_activity, frameLayout);
        //setContentView(R.layout.chat_view_activity);
        //PiwikUtils.sendEvent(getApplication(), PiwikConstants.KCHAT_CATEGORY, PiwikConstants.KCHAT_DETAILS_ACTION, PiwikConstants.KCHAT_DETAILS_TXT);

        settings = SettingManager.getInstance().getSettings();
        userMO = SettingManager.getInstance().getUserMO();
        toolBarChatView = (Toolbar) findViewById(R.id.toolbar_chat_view);
        etMessageText = (EditText) findViewById(R.id.text);

        btnSendMessage = (CustomFontButton) findViewById(R.id.btnSendMessage);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        initializeCustomActionBar();

        etMessageText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() >= 1) {
                    btnSendMessage.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

        getIntentValues();

        // Bind MessengerService
        if (!mBound) {
            bindMessengerService();
        }

        registerBroadcastListeners();

        if (observer == null) {
            observer = new MyObserver(new Handler());
        }

        if (!Build.MANUFACTURER.equals(getResources().getString(R.string.samsung))) {
            getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, observer);
        }

        senderNickNameHash = new HashMap<String, String>();
        senderNickNameHash.put(userMO.getUsername(), VariableConstants.YOU);
        setContentViewProperty();

        listMessage = (ListView) findViewById(R.id.listMessage);
        listMessage.setOnItemClickListener(this);
        observer = new MyObserver(new Handler());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Utils.hideKeyboard(MessageActivity.this, etMessageText);
                onBackPressed();
                break;

            case R.id.action_view_contact:
                // load contact detail screen
                mService.setSenderId("");

                if (isGroupChat || (chatConversationMO != null && chatConversationMO.getChatContactMoKeys() != null && chatConversationMO.getChatContactMoKeys().size() > 2)) {
                    startActivityForResult(new Intent(this, com.mphrx.fisike_physician.activity.GroupDetailActivity.class).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO).putExtra(
                            VariableConstants.GROUP_INFO, groupTextChatMO).putExtra("groupName", nickName), EXIT_GROUP);
                } else {

//                    if (BuildConfig.isPatientApp) {
//                        startActivity(new Intent(this, ContactDetailActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
//                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
//                    } else {

                    if (chatContactMO.getUserType().getName().equalsIgnoreCase("patient")) {

                        startActivity(new Intent(this, ViewPatientProfile.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
                    } else {
                        startActivity(new Intent(this, com.mphrx.fisike_physician.activity.ProfileActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
                    }

//                    }
                }

                break;

            case R.id.action_clear_chat:
                // db call to clear the chat
                messages.clear();
                ChatConversationDBAdapter.getInstance(MessageActivity.this).clearChat(chatConversationMO.getPersistenceKey());
                adapter.notifyDataSetChanged();
                invalidateOptionsMenu();
                //item.setVisible(false);
                break;
            case R.id.action_add_colleague:

                if (!Utils.isNetworkAvailable(this)) {

                    Toast.makeText(this, getResources().getString(R.string.check_Internet_Connection), Toast.LENGTH_LONG).show();

                    return true;
                }
                Intent intent = new Intent(MessageActivity.this, SearchUserActivity.class);

                intent.putExtra("isAddColleague", true);
                intent.putExtra("loadAllContacts", false);
                intent.putExtra(TextConstants.USER_TYPE, TextConstants.OTHER_CONTACT);
                startActivityForResult(intent, ADD_COLLEAGUE);


                break;

            case R.id.action_view_records:
                if (chatContactMO == null)
                    return true;

                String pk = isPatientAvailable();

                if (!pk.equals("")) {

                    pk = Utils.generateChatContactPK(pk);

                    ChatContactMO chatContactMO = mService.getChatContactMo(pk);

                    startActivity(new Intent(this, HealthActivity.class)
                            .putExtra(TextConstants.TITLE, chatContactMO.getDisplayName())
                            .putExtra(TextConstants.PATIENT_ID, String.valueOf(chatContactMO.getPatientId())));
                }

                break;


        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeCustomActionBar() {

        setSupportActionBar(toolBarChatView);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.message_activity_custom_action_bar, null);
        initActionView(mCustomView);
        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }

    public void initActionView(View actionView) {
        ivActionBack = (ImageView) actionView.findViewById(R.id.iv_ac_back);
        ivActionPhone = (ImageView) actionView.findViewById(R.id.iv_phone);
        tvNumberofParticipants = (CustomFontTextView) actionView.findViewById(R.id.tv_ac_participants);
        ivActionPhone.setOnClickListener(this);

        groupProfileLayout = (CustomProfileVIewLayout) actionView.findViewById(R.id.iv_ac_profile_pic);

        tvActionContactName = (CustomFontTextView) actionView.findViewById(R.id.tv_ac_contact_name);
//        ivActionStatusIndicator = (ImageView) actionView.findViewById(R.id.iv_status);
        llActionContactDetail = (LinearLayout) actionView.findViewById(R.id.ll_ac_contact_detail);
        llActionContactDetail.setOnClickListener(this);
        ivActionBack.setOnClickListener(this);
        // ivActionAttachment.setOnClickListener(this);

    }

    private void initAttachment() {
        etMessageText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) etMessageText.getLayoutParams();
                    if ((event.getRawX() - lp.leftMargin) <= etMessageText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width()) {
                        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_storage)))
                            showBottomSheet();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void showOrHideRemoveImageMenu(View view) {
        Utils.hideKeyboard(this);
    }

    /**
     * Show bottom sheet for sending the attachment
     */
    private void showBottomSheet() {
        int numberOfColumn = 3;

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.message_bottom_sheet, null);
        showOrHideRemoveImageMenu(view);

        view.findViewById(R.id.btnGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();
                galleryOption();

            }
        });

        view.findViewById(R.id.btnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();
                clickOption();

            }


        });

        view.findViewById(R.id.btnShot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();
                shotOption();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void dismissBottomSheet() {
        if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing()) {
            mBottomSheetDialog.dismiss();
        }
    }

    private void shotOption() {
        Intent videoIntent = new Intent("android.media.action.VIDEO_CAPTURE");
        PackageManager videoPackageManager = getPackageManager();
        List<ResolveInfo> listVideo = videoPackageManager.queryIntentActivities(videoIntent, 0);
        if (listVideo.size() > 0) {
            isHandled = false;
            final Intent finalIntent = new Intent(videoIntent);
            MessageActivity.this.startActivityForResult(finalIntent, CAMERA_VIDEO_INTENT);
        }
    }

    private void clickOption() {
        Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);
        if (listCam.size() > 0) {
            isHandled = false;

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new java.util.Date());

            imageFileName = MessageActivity.this.getString(R.string.app_name) + timeStamp + "_";
            try {
                cameraImageFile = File.createTempFile(imageFileName, /* prefix */
                        ".jpg", /* suffix */
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES) /* directory */);
                cameraImageFile.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            final Intent finalIntent = new Intent(camIntent);
            finalIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraImageFile));
            MessageActivity.this.startActivityForResult(finalIntent, CAMERA_IMAGE_INTENT);
        }
    }

    private void galleryOption() {
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*,video/*");
        // startActivityForResult(gallIntent, GALLERY_IMAGE_INTENT);

        PackageManager packageManager1 = getPackageManager();
        List<ResolveInfo> listGall = packageManager1.queryIntentActivities(gallIntent, 0);
        int size = listGall.size();
        for (int i = 0; i < size; i++) {
            ResolveInfo res = listGall.get(i);
            if (listGall.size() == 1) {
                if (res.activityInfo.packageName.equals("com.google.android.apps.photos")) {
                    Intent target = new Intent(Intent.ACTION_PICK);
                    target.setType("*/*");
                    target.setPackage(res.activityInfo.packageName);
                    startActivityForResult(target, GALLERY_IMAGE_INTENT);
                    return;
                } else if (res.activityInfo.packageName.toLowerCase().contains("gallery")) {
                    Intent target = new Intent(Intent.ACTION_PICK);
                    target.setType("*/*");
                    target.setPackage(res.activityInfo.packageName);
                    startActivityForResult(target, GALLERY_IMAGE_INTENT);
                    return;
                }
            } else if (res.activityInfo.packageName.toLowerCase().contains(getResources().getString(R.string.gallery))) {
                Intent target = new Intent(Intent.ACTION_PICK);
                target.setType("*/*");
                target.setPackage(res.activityInfo.packageName);
                startActivityForResult(target, GALLERY_IMAGE_INTENT);
                return;
            } else if (i == size - 1) {// if last element
                Intent target = new Intent(Intent.ACTION_PICK);
                target.setType("*/*");
                target.setPackage(listGall.get(0).activityInfo.packageName);
                startActivityForResult(target, GALLERY_IMAGE_INTENT);
            }
        }
    }

    public void setProfile() {
        if (chatConversationMO == null) {
            chatConversationMO = mService.getConversationMO(sender, senderSecondaryId, nickName, false, isSubscribeScreen, sender);
        }

        if (chatConversationMO.getChatContactMoKeys() != null && chatConversationMO.getChatContactMoKeys().size() > 2) {
            setGroupImage();
        } else if (chatContactMO != null) {
            groupProfileLayout.setViewProfileImage(chatContactMO.getId());
        } else {
            groupProfileLayout.fallback();
        }
    }

    private void setGroupImage() {
        if (chatConversationMO == null) {
            return;
        }
        ArrayList<String> contactIds = new ArrayList<>();

        ArrayList<String> moKeys = chatConversationMO.getChatContactMoKeys();
        for (int i = 0; i < moKeys.size(); i++) {

            ChatContactMO mo = null;
            try {
                mo = chatContactDBAdapter.getInstance(this).fetchChatContactMo(moKeys.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mo != null) {
                contactIds.add(TextPattern.getUserId(mo.getId()));
            }
        }

        groupProfileLayout.displayContactImages(contactIds, Utils.extractGroupId(chatConversationMO.getJId()));
    }


    private void setStatusMessage() {
        // tvActionContactStatus.setText(chatContactMO.getPresenceStatus());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.message_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (isUserAdmin() || isGroupChat) {
            menu.findItem(R.id.action_add_colleague).setVisible(true);
        } else {
            menu.findItem(R.id.action_add_colleague).setVisible(false);

        }

        String pk = isPatientAvailable();

        if (!pk.equals("") && BuildConfig.isPhysicianApp) {
            menu.findItem(R.id.action_view_records).setVisible(true);

        } else {
            menu.findItem(R.id.action_view_records).setVisible(false);
        }

        if (messages != null && messages.size() > 0) {
            menu.findItem(R.id.action_clear_chat).setVisible(true);
        } else {
            menu.findItem(R.id.action_clear_chat).setVisible(false);

        }


        if (!etMessageText.isShown()) {
            menu.findItem(R.id.action_add_colleague).setVisible(false);

        }
        return super.onPrepareOptionsMenu(menu);
    }

    /*In case of doctors groupid will be of format  VariableConstants.SINGLE_CHAT_IDENTIFIER _{admin_id}_{otheruser_id}
      in case of patients the invited doctor is admin so group_id creation logic is opposite */
    public String generateGroupID(long sender, String receiver) {

        if (BuildConfig.isPhysicianApp && getIntent().getStringExtra("groupName") != null) {
            return TextConstants.GROUP_CHAT_IDENTIFIER + sender + "_" + System.currentTimeMillis() + "@" + VariableConstants.groupConferenceName + settings.getFisikeServerIp();
        }

        UserMO userMO = SettingManager.getInstance().getUserMO();
        String groupID = null;
        if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
            groupID = VariableConstants.SINGLE_CHAT_IDENTIFIER + TextConstants.UNDERSCORE + receiver + TextConstants.UNDERSCORE + sender
                    + TextConstants.UNDERSCORE + System.currentTimeMillis() + "@" + VariableConstants.groupConferenceName + settings.getFisikeServerIp();
        } else {
            try {
                groupID = VariableConstants.SINGLE_CHAT_IDENTIFIER + TextConstants.UNDERSCORE + sender + TextConstants.UNDERSCORE + receiver
                        + TextConstants.UNDERSCORE + System.currentTimeMillis() + "@" + VariableConstants.groupConferenceName + settings.getFisikeServerIp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  String [] parts = groupID.split(TextConstants.UNDERSCORE);
        return groupID;
    }

    private String sender;

    /**
     * This method will refresh the messages list from the DB
     */
    private void refreshMessages(final boolean isToScrollDown, final boolean isMemberAdded) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        fetchChatConversationMo();

        if (chatConversationMO != null && chatConversationMO.getGroupName() != null && !chatConversationMO.getGroupName().equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            setGroupImage();
        } else {
            setProfile();
        }
        messages = mService.getMessagesInConversation(chatConversationMO, isGroupChat, senderNickNameHash);
        if ((messages == null || messages.size() <= 0)
                && (chatConversationMO == null || chatConversationMO.getGroupStatus() != TextConstants.GROUP_CREATED)) {
            isConversationCreated = true;
            List<ChatContactMO> invitelist = new ArrayList<ChatContactMO>();
            if (chatContactMO != null) {
                invitelist.add(chatContactMO);
            }
            chatConversationMO = mService.getConversationMO(sender, senderSecondaryId, nickName, false, false, sender);

            groupName = getIntent().getStringExtra("groupName");

            if (groupName != null && !groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                nickName = groupName;
            }
            if (groupName != null) {
                tvActionContactName.setVisibility(View.VISIBLE);
                tvActionContactName.setText(nickName);
                tvNumberofParticipants.setVisibility(View.VISIBLE);
            } else {
                mService.createNewGroup(VariableConstants.SINGLE_CHAT_IDENTIFIER, invitelist, null, true, sender, true);
            }

        } else {
            if (isMemberAdded) {
                groupName = chatConversationMO.getGroupName();
            }
            if (groupName != null && !groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                nickName = groupName;
            }
        }
        runOnUiThread(new Runnable() {
            public void run() {
                if (isConversationCreated) {
                    isConversationCreated = false;
                    // add an extra check to make sure whether group is created or not
                    // set the visibilty of view to gone
                    btnSendMessage.setVisibility(View.GONE);
                    etMessageText.setVisibility(View.GONE);
                    supportInvalidateOptionsMenu();
                    progressBar.setVisibility(View.VISIBLE);

                    groupCreatedHandler = new Handler();
                    groupCreatedHandler.postDelayed(new Runnable() {
                        public void run() {
                            errorGroupCreating();
                        }
                    }, TimeUnit.SECONDS.toMillis(30));
                }

                if (null != chatConversationMO) {
                    unreadCount = chatConversationMO.getUnReadCount();
                }

                if (null == nickName || "".equals(nickName)) {
                    ChatConversationMO conversationMO = mService.getConversationMO(sender, senderSecondaryId, nickName, false, true, sender);
                    if (null != conversationMO && conversationMO.getNickName() != null && !conversationMO.getNickName().equals("")) {
                        nickName = conversationMO.getNickName();
                        getSupportActionBar().setTitle(nickName);
                    }
                }
                // Sorting the list of conversations
                try {
                    Collections.sort(messages, messageComparator);
                } catch (Exception e) {

                }

                if (unreadCount > 0) {
                    String msg = unreadCount == 1 ? MyApplication.getAppContext().getString(R.string.unread_message) : MyApplication.getAppContext().getResources().getString(R.string.unread_messages);
                    unreadHeader = new SectionItem(unreadCount + msg);
                    if (unreadCount > messages.size()) {
                        unreadCount = messages.size();
                    }
                    int position = messages.size() - unreadCount;
                    for (int index = position; -1 > index && index < messages.size(); index++) {
                        if (null == messages || null == messages.get(index)) {
                            position--;
                            continue;
                        }

                        if (messages.get(index).isSection()) {
                            position--;
                        }
                    }
                    if (-1 < position)
                        messages.add(position, unreadHeader);
                }

                if (null == adapter || isMemberAdded) {
                    boolean isPatient = userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT) ? true : false;
                    boolean isToShowChatContactNames;
                    if (chatConversationMO != null && chatConversationMO.getChatContactMoKeys() != null) {
                        isToShowChatContactNames = chatConversationMO != null && chatConversationMO.getChatContactMoKeys().size() > 2 ? true : false;
                    } else {
                        isToShowChatContactNames = false;
                    }
                    if (isToShowChatContactNames) {
                        tvNumberofParticipants.setVisibility(View.VISIBLE);
                        int membersCount = chatConversationMO.getChatContactMoKeys().size() - 2;
                        tvNumberofParticipants.setText("+ " + membersCount + (membersCount == 1 ? getResources().getString(R.string.participant) : getResources().getString(R.string.participants)));
                    }
                    adapter = new MessageAdapter(MessageActivity.this, messages, isGroupChat, isPatient, isToShowChatContactNames);
                    listMessage.setAdapter(adapter);
                } else {
                    adapter.clear();
                    adapter.setmMessages(messages);
                    adapter.notifyDataSetChanged();
                }

                /**
                 * Not to scroll to the end of list in case of attachment downloading and uploading
                 */
                if (!isToScrollDown) {
                    return;
                }
                if (unreadCount > 0) {
                    if (adapter.getCount() > unreadCount) {
                        listMessage.setSelectionFromTop(adapter.getCount() - unreadCount, 130);
                    } else {
                        listMessage.setSelection(adapter.getCount() - unreadCount);
                    }
                } else {
                    listMessage.setSelection(adapter.getCount());
                }

//                addGroupMembers();
            }
        });

        setHeaderUsersInfo();


    }


    private void setHeaderUsersInfo() {

        if (TextPattern.isSingleGroupChat(chatConversationMO.getJId())) {

            if (chatConversationMO.getChatContactMoKeys() != null) {
                tvActionContactName.setText(nickName);

                if (chatConversationMO.getChatContactMoKeys().size() > 2) {
                    tvNumberofParticipants.setVisibility(View.VISIBLE);
                    tvNumberofParticipants.setText("+" + (chatConversationMO.getChatContactMoKeys().size() - 2) + getResources().getString(R.string._participants));
                } else {
                    tvNumberofParticipants.setVisibility(View.GONE);

                }
            }

            return;
        }
        ArrayList<String> chatContactMoKeys = chatConversationMO.getChatContactMoKeys();
        if (chatContactMoKeys == null) {
            return;
        }
        // FIXME is this still needed to handle duplicate keys ??
        Set<String> hs = new HashSet<>();
        hs.addAll(chatContactMoKeys);
        chatContactMoKeys.clear();
        chatContactMoKeys.addAll(hs);
        String participants = "";

        for (int i = 0; i < chatContactMoKeys.size(); i++) {

            ChatContactMO chatContactMO = mService.getChatContactMo(chatContactMoKeys.get(i));

            if (chatContactMO != null) {
//                if (!TextUtils.isEmpty(chatContactMO.getDisplayName()) && !TextPattern.getUserId(chatContactMO.getId()).equals(String.valueOf(userMO.getId()))) {
//                    participants = participants + chatContactMO.getDisplayName() + ", ";
//                }
                if (!TextPattern.getUserId(chatContactMO.getId()).equals(String.valueOf(userMO.getId()))) {
                    participants = participants + chatContactMO.getDisplayName() + ", ";
                }
            }
        }

        int charCount = 0;
        char temp;

        for (int i = 0; i < participants.length(); i++) {
            temp = participants.charAt(i);
            if (temp == ',')
                charCount++;
        }
        if (isGroupChat || charCount > 0) {
            participants = participants.trim();
            if (!participants.equals("") && participants.length() > 1) {
                participants = participants.substring(participants.startsWith(",") ? 1 : 0, participants.length() - 1);

                if (chatConversationMO.getChatContactMoKeys().contains(ChatContactMO.getPersistenceKey(userMO.getId() + ""))) {
                    participants = participants + getResources().getString(R.string.comma_You);
                }
                tvActionContactName.setText(nickName);
                tvNumberofParticipants.setVisibility(View.VISIBLE);
                tvNumberofParticipants.setText(participants);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_IMAGE_INTENT:
                    // seperate handling
                    if (cameraImageFile == null || cameraImageFile.getAbsolutePath() == null) {
                        break;
                    }
                    Bitmap imageBitmap = Utils.decodeSampledBitmapFromFile(cameraImageFile.getAbsolutePath(), 1000, 700);
                    MyApplication.setTempImagePreview(imageBitmap);
                    Utils.deleteExternalStoragePrivatePicture(cameraImageFile);
                    EncriptFile encriptBitmapFile = new EncriptFile(MessageActivity.this, imageBitmap, senderSecondaryId);
                    if (Build.VERSION.SDK_INT >= 11) {
                        encriptBitmapFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        encriptBitmapFile.execute();
                    }
                    break;

                case CAMERA_VIDEO_INTENT:
                case GALLERY_IMAGE_INTENT:
                case GALLERY_VIDEO_INTENT:
                    isCapturedImage = false;

                    int mediaType = (requestCode == CAMERA_VIDEO_INTENT || requestCode == GALLERY_VIDEO_INTENT) ? Utils.MEDIA_TYPE_VIDEO
                            : Utils.MEDIA_TYPE_IMAGE;
                    filePath = null;
                    if (requestCode == CAMERA_VIDEO_INTENT) {
                        isCapturedImage = true;
                        captureLastId = getLastImageId(requestCode);
                        filePath = captureLastId.getPath();
                        if (null != filePath && !"".equals(filePath)) {
                            EncriptFile encriptFile = new EncriptFile(MessageActivity.this, filePath, requestCode, isCapturedImage, mediaType, senderSecondaryId);
                            if (Build.VERSION.SDK_INT >= 11) {
                                encriptFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                encriptFile.execute();
                            }
                        }
                    } else if (null != data && null != data.getData()) {
                        Uri uri = data.getData();
                        ContentResolver cr = this.getContentResolver();
                        String mime = cr.getType(uri);
                        mediaType = mime.contains("video") ? Utils.MEDIA_TYPE_VIDEO : Utils.MEDIA_TYPE_IMAGE;
                        requestCode = mime.contains("video") ? GALLERY_VIDEO_INTENT : GALLERY_IMAGE_INTENT;
                        filePath = Utils.getRealPathFromURI(uri, this);
                        if (null != filePath && !"".equals(filePath)) {
                            EncriptFile encriptFile = new EncriptFile(MessageActivity.this, filePath, requestCode, isCapturedImage, mediaType, senderSecondaryId);
                            if (Build.VERSION.SDK_INT >= 11) {
                                encriptFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                encriptFile.execute();
                            }
                        }
                    }

                    break;
                case ATTACHMET_QUESTIONARY:
                    boolean isSucessfullyAttached = false;
                    Bundle extraQBRespose = data.getExtras();
                    try {
                        isSucessfullyAttached = extraQBRespose.getBoolean(TextConstants.ATTACHMENT_QUESTIONARY_RESULT);
                    } catch (Exception e) {
                    }

                    if (isSucessfullyAttached) {
                        // refreshMessages(true);
                        addMessageToView((ChatMessageMO) extraQBRespose.get(VariableConstants.QB_ATTACHMENT_SUCESS));
                    }
                    break;
                case ATTACHMENT_PREVIEW:
                    Bundle extras = data.getExtras();
                    String attachmentType = extras.getString(VariableConstants.ATTACHMENT_TYPE);
                    String attachmentPath = extras.getString(VariableConstants.VIDEO_URI);
                    File attachmentFile = new File(attachmentPath);
                    if (attachmentFile.exists()) {
                        // Adding roster
                        if (!isSubscriptionSend && isSubscribeScreen) {
                            chatConversationMO = mService.getConversationMO(sender, senderSecondaryId, nickName, false, false, sender);
                            List<ChatContactMO> invitelist = new ArrayList<ChatContactMO>();
                            invitelist.add(chatContactMO);
                            mService.updateGroupRoster(senderSecondaryId, invitelist, sender, chatConversationMO, null);
                        }
                        isSubscriptionSend = true;

                        AttachmentQueue attachmentQueue = AttachmentQueue.getInstance();
                        SendAttachmentFile sendImageFile2 = null;
                        if (Utils.isNetworkAvailable(MessageActivity.this)) {
                            sendImageFile2 = new SendAttachmentFile(this, null, chatConversationMO, attachmentFile, mService, attachmentType,
                                    attachmentFile.length(), true);
                        } else {
                            sendImageFile2 = new SendAttachmentFile(this, null, chatConversationMO, attachmentFile, mService, attachmentType,
                                    attachmentFile.length(), false);
                        }
                        attachmentQueue.addTask(sendImageFile2.getPersistenceKey() + "", sendImageFile2);

                        onUserInteraction();
                    }
                    break;
                case QUESTION_BUILDER_ACTIVITY:
                    if (mBound && null != mService) {
                        refreshMessages(true, false);
                    }
                    break;
                case ADD_COLLEAGUE:
                    if (chatConversationMO == null || chatConversationMO.getChatContactMoKeys() == null) {
                        return;
                    }
                    if (chatConversationMO.getChatContactMoKeys().size() == 2 && chatConversationMO.getJId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);

                        final EditText edittext = new EditText(this);
                        edittext.setHint(getResources().getString(R.string.Group_Name));
                        alert.setMessage(getResources().getString(R.string.Group_Name));
                        alert.setTitle("");
                        alert.setView(edittext);

                        alert.setPositiveButton(getResources().getString(R.string.Done), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                ConnectionInfo connectionInfo = ConnectionInfo.getInstance();
                                XMPPConnection connection = connectionInfo.getXmppConnection();
                                if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
                                    final String changedGroupName = edittext.getText().toString();
                                    progressDialog = new ProgressDialog(MessageActivity.this);
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    addColleague(data);
                                    if (!changedGroupName.trim().equals("")) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                mService.changeGroupName(changedGroupName, chatConversationMO.getJId());
                                            }
                                        }, 2000);
                                    }
                                }
                            }
                        });

                        alert.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what ever you want to do with No option.
                            }
                        });

                        alert.show();
                    } else {
                        ConnectionInfo connectionInfo = ConnectionInfo.getInstance();
                        XMPPConnection connection = connectionInfo.getXmppConnection();
                        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
                            addColleague(data);
                        }
                    }

                    break;

                case EXIT_GROUP:
                    if (data == null || TextUtils.isEmpty(data.getStringExtra("groupNameText"))) {
                        finish();
                    } else {
                        refreshMessages(false, false);

                        tvActionContactName.setText(data.getStringExtra("groupNameText"));
                    }
                    break;

            }
        }
    }

    private void collegeAdded() {
//        addColleague(data);
//
//        mService.changeUpdateGroupName(changedGroupName, sender);
//
//        ChatConversationDBAdapter.getInstance(MessageActivity.this).updateIsChatInitiated(chatConversationMO.getPersistenceKey());
//        chatConversationMO.setIsChatInitiated(1);
//        mService.setChatConversation(chatConversationMO);
//        nickName = changedGroupName;
////                                            refreshMessages(false, false);
//        tvActionContactName.setText(changedGroupName);
//
//        refreshMessages(false, false);
    }

    private void addColleague(Intent data) {
        ChatContactMO chatContactMO = data.getParcelableExtra("contact");
        List<ChatContactMO> chatContactMOs = new ArrayList<>();
        chatContactMO.generatePersistenceKey(chatContactMO.getId().contains("@") ? chatContactMO.getId().split("@")[0] : chatContactMO.getId());
        chatContactMOs.add(chatContactMO);

        if (mService == null) {
            Toast.makeText(MessageActivity.this, getResources().getString(R.string.Cant_establish_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        String newUserId = TextPattern.getUserId(chatContactMO.getId());

        if (!chatContactDBAdapter.getInstance(MessageActivity.this).isChatContactMoAvailable(newUserId)) {
            mService.getGroupChatUsersDetails(newUserId);
        }

        List<String> pks = chatConversationMO.getChatContactMoKeys();
        if (pks != null) {
            for (int i = 0; i < pks.size(); i++) {

                ChatContactMO mo = mService.getChatContactMo(pks.get(i));
                if (mo != null) {
                    String id = TextPattern.getUserId(mo.getId());
                    if (newUserId.equals(id)) {
                        Toast.makeText(MessageActivity.this, getResources().getString(R.string.Colleague_already_added), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }

        }

        ArrayList<String> chatContactMoKeys = ChatConversationDBAdapter.getInstance(MessageActivity.this).addContactMoToConversation(chatConversationMO, chatContactMO.getPersistenceKey() + "", TextPattern.getUserId(String.valueOf(userMO.getId())));

        mService.updateChatContactMoKeysInChatConversationCacheObject(chatContactMoKeys, "" + chatConversationMO.getPersistenceKey());

        refreshMessages(false, false);
        if (chatConversationMO == null) {
            mService.sendInvite(chatContactMOs, sender);
        } else {
            mService.sendInvite(chatContactMOs, chatConversationMO.getJId());
        }

        MessageItem messageItem = new SectionItem(chatContactMO.getDisplayName() + getResources().getString(R.string.has_been_added_to_the_group));
        messages.add(messageItem);

        adapter.notifyDataSetChanged();
    }

    /**
     * Show error
     *
     * @param txtMessage
     */
    private void showError(String txtMessage) {
        ErrorMessage localErrorMessage = new ErrorMessage(this, R.id.layoutParent);
        localErrorMessage.showMessage(txtMessage);
    }

    public void postEncription(File moveToInternalStorage, int requestCode, String filePath) {
        Uri mImageCaptureUri = null;
        if (moveToInternalStorage.exists()) {
            filePath = new String(moveToInternalStorage.getAbsolutePath());
        }
        if (null != filePath && !"".equals(filePath)) {
            mImageCaptureUri = Uri.fromFile(new File(filePath));
        }
        if (null != mImageCaptureUri) {
            String realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
            if (null == realPathFromURI) {
                realPathFromURI = mImageCaptureUri.getPath();
                if (null == realPathFromURI) {
                    return;
                }
            }
            File file = new File(realPathFromURI);
            if (null != file && file.exists()) {
                String attachmentType = null;
                if (requestCode == CAMERA_IMAGE_INTENT || requestCode == GALLERY_IMAGE_INTENT) {
                    attachmentType = VariableConstants.ATTACHMENT_IMAGE;
                } else if (requestCode == CAMERA_VIDEO_INTENT || requestCode == GALLERY_VIDEO_INTENT) {
                    attachmentType = VariableConstants.ATTACHMENT_VIDEO;
                }
                onUserInteraction();
                boolean isCapturedImage = false;
                if (requestCode == CAMERA_IMAGE_INTENT || requestCode == CAMERA_VIDEO_INTENT) {
                    isCapturedImage = true;
                }
                openFile(file, attachmentType, realPathFromURI, true, isCapturedImage);
            }
        }
    }

    /**
     * This method sets the read status for this conversation
     */
    private void setReadStatus() {
        if (null == chatConversationMO) {
            fetchChatConversationMo();
        }
        if (null != chatConversationMO) {
            if (null != chatConversationMO) {
                // chatConversationMO.setReadStatus(true);
                // chatConversationMO.setUnReadCount(0);
                // mService.updateChatConversationMO(chatConversationMO);
                ContentValues updatedValues = new ContentValues();
                updatedValues.put("readStatus", 1);
                updatedValues.put("unReadCount", 0);
                new UpdateDatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE, updatedValues)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    /**
     * Set Record uploaded for the attachment
     */
    public void setRecordUploaded(int position, int recoredSyncStaus) {
        Object item = adapter.getItem(position);
        if (item instanceof ChatMessageMO) {
            adapter.setMessageItem(position, recoredSyncStaus);
            ContentValues updatedValues = new ContentValues();
            updatedValues.put("imageRecordSyncStatus", recoredSyncStaus);
            new UpdateDatabaseAsyncTask(((ChatMessageMO) item).getAttachmentPath(), DatabaseTables.AttachmentMO_TABLE, updatedValues)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        findViewById(R.id.layoutChat).setVisibility(View.VISIBLE);
    }

    private void fetchChatConversationMo() {
        if (mBound) { // sender
            if (isGroupChat) {
                chatConversationMO = mService.getGroupConversationMO(sender, senderSecondaryId, nickName, false, isSubscribeScreen);// jid
            } else {
                chatConversationMO = mService.getConversationMO(sender, senderSecondaryId, nickName, false, isSubscribeScreen, sender);
            }
        }


        if (BuildConfig.isPhysicianApp) {
            if (isUserAdmin()) invalidateOptionsMenu();
        }
    }


    private BroadcastReceiver mBroadcastListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshMessages(false, true);
        }
    };

    private boolean isUserAdmin() {

        if (!isPatientAvailable().equals("")) {

            String userId = String.valueOf(userMO.getId());

            String extractedId = Utils.getAdminId(chatConversationMO.getJId());

            if (userId.equals(extractedId)) {
                return true;
            }
        }
        return false;
    }

    private String isPatientAvailable() {

        if (chatConversationMO == null) {
            return "";
        }
        String pateintIdFromJid = Utils.getPateintIdFromJid(chatConversationMO.getJId());

        if (pateintIdFromJid.equals("")) {
            return "";
        }

        String pk = chatContactDBAdapter.getInstance(this).isPatientAvaiable(pateintIdFromJid);

        if (pk.equals("")) {
            mService.getGroupChatUsersDetails(pateintIdFromJid);
        }

        return pk;
    }

    /**
     * Process the Intent values for this activity and load up data into memory
     */
    private void getIntentValues() {
        Bundle extras = getIntent().getExtras();
        sender = extras.getString(VariableConstants.SENDER_ID);


        senderSecondaryId = extras.getString(VariableConstants.SENDER_SECOUNDRY_ID);
        nickName = extras.getString(VariableConstants.SENDER_NICK_NAME);
        isRecentChat = extras.getBoolean(TextConstants.IS_RECENT_CHAT, false);
        isGroupChat = extras.getBoolean(VariableConstants.IS_GROUP_CHAT, false);

        if (!isGroupChat && senderSecondaryId != null && !senderSecondaryId.equals("") && !senderSecondaryId.contains("@" + settings.getFisikeServerIp())) {
            senderSecondaryId += "@" + settings.getFisikeServerIp();
        }
        String groupBackId = "@" + VariableConstants.groupConferenceName + settings.getFisikeServerIp();

        if (!isGroupChat && !sender.equals("") && !sender.contains(groupBackId)) {
            sender += groupBackId;
        }
        if (isGroupChat) {
            groupTextChatMO = (GroupTextChatMO) extras.get(VariableConstants.GROUP_INFO);
        }
        isSubscribeScreen = extras.getBoolean(VariableConstants.ADD_SCREEN, false);
        if (isSubscribeScreen) {
            chatContactMO = (ChatContactMO) extras.get(TextConstants.CHAT_DETAIL);
            if (chatContactMO != null) {
                nickName = chatContactMO.getRealName();
            }
        }

        if (senderSecondaryId == null) {
            senderSecondaryId = Utils.genrateSecoundryId(sender);
        }
        username = new String(senderSecondaryId);
        if (username.contains("@")) {
            String[] arr = senderSecondaryId.split("@");
            username = arr[0];
        }

        if (TextUtils.isEmpty(sender)) {
            sender = generateGroupID(userMO.getId(), TextPattern.getUserId(senderSecondaryId));
        }
        this.setTitle(senderSecondaryId);
        if (nickName != null && !nickName.equals("")) {
            tvActionContactName.setText(nickName);
        } else {
            try {
                tvActionContactName.setText(URLDecoder.decode(username, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

//        if (isGroupChat) {
        try {
            myStatus = extras.getInt(VariableConstants.MY_STATUS_ACTIVE);
            if (myStatus == VariableConstants.chatConversationMyStatusInactive) {
                etMessageText.setVisibility(View.GONE);
                supportInvalidateOptionsMenu();
                btnSendMessage.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }
//        }
    }

    private void setContentViewProperty() {
        if (isGroupChat) {
            findViewById(R.id.iv_phone).setVisibility(View.GONE);
            findViewById(R.id.iv_status).setVisibility(View.GONE);
        }
        Utils.cancelNotification(getApplicationContext(), VariableConstants.CHAT_NOTIFICATION_NUMBER);
        initAttachment();
    }

    @Override
    protected void onResume() {
        AppLog.d("Fisike", "MessageActivity - Activity onResume");

        registerBroadcastListeners();

        if (mService != null && mBound) {
            mService.setSenderId(senderSecondaryId);
        }

        Utils.cancelNotification(getApplicationContext(), VariableConstants.CHAT_NOTIFICATION_NUMBER);
        if (!mBound) {
            bindMessengerService();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        adapter.clearSDCard();
        setReadStatus();
        super.onBackPressed();
    }

    public void updateProfileRealName() {
        if (isSubscribeScreen) {
            chatContactMO = mService.getChatContactMo(ChatContactMO.getPersistenceKey(chatContactMO.getId()));
            if (chatContactMO != null) {
                nickName = chatContactMO.getRealName();
            }
            // tvActionContactName.setText(nickName);
        }

        refreshMessages(false, false);
    }

    public void updateProfilePic() {
        chatContactMO = mService.getChatContactMo(ChatContactMO.getPersistenceKey(chatContactMO.getId()));
        if (isSubscribeScreen) {
            setProfile();
        }
    }

    @Override
    protected void onDestroy() {
        if (loadContactsRealName != null && !loadContactsRealName.isCancelled()) {
            loadContactsRealName.cancel(true);
            loadContactsRealName = null;
        }
        if (loadContactProfilePicAsyn != null && !loadContactProfilePicAsyn.isCancelled()) {
            loadContactProfilePicAsyn.cancel(true);
            loadContactProfilePicAsyn = null;
        }

        try {
            adapter.clearSDCard();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unregisterBroadcastListeners();
        unbindMessengerService();
        getContentResolver().unregisterContentObserver(observer);
        super.onDestroy();
    }

    /**
     * This is the onclick method
     */
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnSendMessage:
                // Called when the send  com.mphrx.fisike.customview.CustomFontButton is clicked
                sendMessage();
                btnSendMessage.setEnabled(false);
                break;

            case R.id.iv_ac_back:
                Utils.hideKeyboard(MessageActivity.this, etMessageText);
                onBackPressed();
                break;

            case R.id.ll_ac_contact_detail:
                mService.setSenderId("");
                if (isGroupChat || (chatConversationMO != null && chatConversationMO.getChatContactMoKeys() != null && chatConversationMO.getChatContactMoKeys().size() > 2)) {
                    startActivityForResult(new Intent(this, GroupDetailActivity.class).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO).putExtra(
                            VariableConstants.GROUP_INFO, groupTextChatMO).putExtra("groupName", nickName), EXIT_GROUP);
                } else {

//                    if (BuildConfig.isPatientApp) {
//                        startActivity(new Intent(this, ContactDetailActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
//                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
//                    } else {

                    if (chatContactMO.getUserType().getName().equalsIgnoreCase(getResources().getString(R.string.conditional_check_patient))) {
                        startActivity(new Intent(this, ViewPatientProfile.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
                    } else {
                        startActivity(new Intent(this, ProfileActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
                    }

//                    }
                }

                break;

            case R.id.iv_phone:
                if (contactPhoneNumber != null && contactPhoneNumber != "") {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    String phone = getResources().getString(R.string.tel) + contactPhoneNumber;
                    intent.setData(Uri.parse(phone));
                    startActivity(intent);
                } else {
                    Toast.makeText(MessageActivity.this, getResources().getString(R.string.Contact_number_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }
    }


    public void showAttachmentPopUp(View view) {
        PopupMenu popupMenu = new PopupMenu(MessageActivity.this, view);
        popupMenu.setOnMenuItemClickListener((OnMenuItemClickListener) MessageActivity.this);
        if (null != userMO && !userMO.getRoles().contains(TextConstants.ROLE_PATIENT) && chatContactMO != null && null != chatContactMO.getUserType()
                && TextConstants.PATIENT.equalsIgnoreCase(chatContactMO.getUserType().getName())
                && myStatus == VariableConstants.chatConversationMyStatusActive) {
            popupMenu.inflate(R.menu.attachment_menu);
        } else if (myStatus == VariableConstants.chatConversationMyStatusActive) {
            popupMenu.inflate(R.menu.attachment_menu_pat);
        }

        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Utils.hideKeyboard(MessageActivity.this, etMessageText);
                onBackPressed();
                break;
            case R.id.attachImage:
                if (isGroupChat && myStatus == VariableConstants.chatConversationMyStatusInactive) {
                    break;
                }
                isHandled = false;

                String timeStamp = new SimpleDateFormat(DateTimeUtil.yyyyMMdd_HHmmss, Locale.getDefault()).format(new java.util.Date());
                imageFileName = "Fisike" + timeStamp + "_";
                try {
                    cameraImageFile = File.createTempFile(imageFileName, /* prefix */
                            ".jpg", /* suffix */
                            getExternalFilesDir(Environment.DIRECTORY_PICTURES) /* directory */);
                    cameraImageFile.getAbsolutePath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                IntentUtils.attachImageFile(MessageActivity.this, CAMERA_IMAGE_INTENT, GALLERY_IMAGE_INTENT, cameraImageFile, null, false, true);
                break;
            case R.id.attachVideo:
                if (isGroupChat && myStatus == VariableConstants.chatConversationMyStatusInactive) {
                    break;
                }
                attachVideoFile();
                break;
        }
        return true;
    }

	/*
     * TODO change the conversation created on first message instead of conversation open called if one to one chat is initiated for the first time
	 * creates a new group and invites the contact and then sends the message
	 * 
	 * private ChatMessageMO createOnetoOneGroupAndSendMessage(String messageText, String groupID) { ChatMessageMO chatMsgMO = null;
	 * List<ChatContactMO> invitelist = new ArrayList<ChatContactMO>(); invitelist.add(chatContactMO); int msgSendStatus =
	 * DefaultConnection.MESSAGE_NOT_SENT; long timeStamp = System.currentTimeMillis(); // create a chat message object here Message message =
	 * createMessageObject(messageText, null, null, 0, chatConversationMO, null, false, false);
	 * 
	 * // updates the chatConversationMo and chatMessageMo db chatMsgMO = mService.storeSentMessage(message, chatConversationMO, messageText.trim(),
	 * msgSendStatus, timeStamp); mService.updateGroupRoster(senderSecondaryId, invitelist, groupID, chatConversationMO, chatMsgMO); return chatMsgMO; }
	 */

    public void sendMessage() {
        String messageText = etMessageText.getText().toString().trim();
        ChatMessageMO chatMsgMO = null;
        // ArrayList<String> chatConversationKeyList = userMO.getChatConversationKeyList();

        // check if the conversation already exists
        // if it exists the check the status of group
        /*
         * if(chatConversationMO != null && chatConversationMO.getConversationType() == VariableConstants.conversationTypeChat){ String groupName =
       * chatConversationMO.getGroupName(); String groupId = chatConversationMO.getJId(); String invitedMemberId =
       * chatConversationMO.getSecoundryJid(); ChatContactMO invitedMemberContactMo = chatContactMO; // check the status of group
       * if(chatConversationMO.getGroupStatus() == TextConstants.GROUP_NOT_CREATED){ chatMsgMO = createOnetoOneGroupAndSendMessage(messageText,
       * groupId);
       *
       * } else if (chatConversationMO.getGroupStatus() == TextConstants.GROUP_CREATED){ // again send the invite // id of the contact } }
       */

        if (messageText == null || messageText.equals("")) {
            return;
        }

        // checks for whether group created successfully

        // Adding roster
        if (!isSubscriptionSend && isSubscribeScreen) {
            // String groupID = VariableConstants.SINGLE_CHAT_IDENTIFIER + "_" + userMO.getId() + "_" + Utils.cleanXMPPServer(chatContactMO.getId())
            // + "_" + System.currentTimeMillis() + "@" + VariableConstants.groupConferenceName + settings.getFisikeServerIp();
            // chatConversationMO = mService.getConversationMO(senderSecondaryId, nickName, false, false, groupID);
            chatMsgMO = mService.sendMessage(messageText, null, null, 0, chatConversationMO, null, false, false);

            // add checks to identify whether its a new conversation
            // if yes create a new chatConversation
            /*
             * if (chatConversationKeyList != null && !chatConversationKeyList.contains(chatConversationMO.getPersistenceKey() + "")) { chatMsgMO =
          * createOnetoOneGroupAndSendMessage(messageText, groupID); }
          */
        } else {
            chatMsgMO = mService.sendMessage(messageText, null, null, 0, chatConversationMO, null, false, false);
        }

        // this will be part of else block as in a new conversation we are handling the message in a
        // sepatarate case

        if (null != chatMsgMO) {
            //  if (isGroupChat) {
            chatMsgMO.setSenderNickName(VariableConstants.YOU);
            //}
            addMessageToView(chatMsgMO);
        }
        isSubscriptionSend = true;
        etMessageText.setText("");
    }

    /**
     * Gets the last image id from the media store
     *
     * @param attachmentType
     * @return
     */
    private LastSegmentName getLastImageId(int attachmentType) {
        String mediaId = MediaStore.Images.Media._ID;
        String mediaData = MediaStore.Images.Media.DATA;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        if (CAMERA_VIDEO_INTENT == attachmentType || GALLERY_VIDEO_INTENT == attachmentType) {
            mediaId = MediaStore.Video.Media._ID;
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        }
        final String[] imageColumns = {mediaId};
        final String imageOrderBy = mediaId + " DESC";
        final String imageWhere = null;
        final String[] imageArguments = null;
        Cursor imageCursor = getContentResolver().query(uri, imageColumns, imageWhere, imageArguments, imageOrderBy);
        // Cursor imageCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, imageColumns, imageWhere, imageArguments, imageOrderBy);
        if (imageCursor.moveToFirst()) {

            int id = imageCursor.getInt(imageCursor.getColumnIndex(mediaId));

            String[] projection = {mediaData};
            String whereClause = mediaId + "=?";
            Cursor cursor = getContentResolver().query(uri, projection, whereClause, new String[]{id + ""}, null);
            int column_index = cursor.getColumnIndexOrThrow(mediaData);
            String string = "";
            if (cursor.moveToFirst()) {
                string = cursor.getString(column_index);
            }
            imageCursor.close();
            return new LastSegmentName(id, string);
        } else {
            return new LastSegmentName(0, "");
        }
    }

    /**
     * Open video File from camera or gallery
     */
    private void attachVideoFile() {
        Intent camIntent = new Intent("android.media.action.VIDEO_CAPTURE");
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("video/*");

        // look for available intents
        List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);
        if (listCam.size() > 0) {
            ResolveInfo res = listCam.get(0);
            final Intent finalIntent = new Intent(camIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra(VariableConstants.RESULT_CODE, CAMERA_VIDEO_INTENT);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra(VariableConstants.RESULT_CODE, GALLERY_VIDEO_INTENT);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        // openDialog(yourIntentsList, info, "Select Video");
    }

    /**
     * Return the chat conversationMo
     *
     * @return
     */
    public ChatConversationMO getChatConversationMO() {
        return chatConversationMO;
    }

    /**
     * This method will add the message to the list
     *
     * @param chatMsgMO
     */
    private synchronized void addMessageToView(ChatMessageMO chatMsgMO) {
        // Check if the message is already in the list or not

        Iterator<MessageItem> msgIte = messages.iterator();
        boolean msgExists = false;
        while (msgIte.hasNext()) {
            MessageItem next = msgIte.next();
            if (next instanceof SectionItem) {
                continue;
            }
            ChatMessageMO currMsgMO = (ChatMessageMO) next;
            if (null != currMsgMO && currMsgMO.getPersistenceKey() == chatMsgMO.getPersistenceKey()) {
                msgExists = true;
                break;
            }
        }
        if (!msgExists) {
            if (unreadCount > 0 && unreadHeader != null) {
                messages.remove(unreadHeader);
                unreadCount = 0;
            }
            int size = messages.size();
            if (size == 0) {
                Date date = new Date(chatMsgMO.getLasUpdatedTimeUTC());
                SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
                messages.add(new SectionItem(date_format.format(date).toString()));
            } else {
                ChatMessageMO chatMessageMO = getLastMessage();
                // ChatMessageMO chatMessageMO = ((ChatMessageMO) messages.get(size - 1));
                if (!Utils.isSameDate(chatMessageMO.getLasUpdatedTimeUTC(), chatMsgMO.getLasUpdatedTimeUTC())) {
                    Date date = new Date(chatMsgMO.getLasUpdatedTimeUTC());
                    SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
                    messages.add(new SectionItem(date_format.format(date).toString()));
                }
            }
            messages.add(chatMsgMO);

            try {
                Collections.sort(messages, messageComparator);
            } catch (Exception e) {

            }
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter.notifyDataSetChanged();
                    listMessage.setSelection(adapter.getCount());
                }
            });
        }
    }

    private ChatMessageMO getLastMessage() {
        int i = 1;
        int size = messages.size();
        while (size > i && messages.get(size - i).isSection()) {
            i++;
        }
        return ((ChatMessageMO) messages.get(size - i));
    }

    public void refreshFileMessages(final ChatMessageMO chatMessageMO, final boolean isToRefreshView, final String senderNickName) {
        if (isToRefreshView) {
            refreshMessages(false, false);
        } else if (null != chatMessageMO) {
           /* if (isGroupChat) {*/
            chatMessageMO.setSenderNickName(senderNickName);
           /* }*/
            runOnUiThread(new Runnable() {
                public void run() {
                    addMessageToView(chatMessageMO);
                }
            });
            // refreshMessages(false);
        }
    }

    /**
     * This method changes the delivery status of the messageMO in the view
     *
     * @param messageKey
     */
    protected void changeMessageDeliveryStatus(long messageKey) {
        try {
            ChatMessageMO chatMessageMO = convContainsMessage(messageKey);
            chatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_DELIVERED);

            try {
                Collections.sort(messages, messageComparator);
            } catch (Exception e) {

            }
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {

        }
    }

    /**
     * This method changes the ack status of the messageMO in the view
     *
     * @param messageKey
     */
    protected void changeMessageAckStatus(long messageKey) {
        try {
            ChatMessageMO chatMessageMO = convContainsMessage(messageKey);
            if (DefaultConnection.MESSAGE_DELIVERED != chatMessageMO.getMsgStatus()) {
                chatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_SENT_TO_SERVER);
            }

            try {
                Collections.sort(messages, messageComparator);
            } catch (Exception e) {

            }
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {

        }
    }

    /**
     * This method changes the message status of the messageMO in the view
     *
     * @param msgStatus
     * @param messageKey
     */
    protected void changeMessageStatus(long messageKey, int msgStatus) {
        try {
            ChatMessageMO chatMessageMO = convContainsMessage(messageKey);
            chatMessageMO.setMsgStatus(msgStatus);
            try {
                Collections.sort(messages, messageComparator);
            } catch (Exception e) {
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {

        }
    }

    /**
     * This method gives you the Sender Id for this Message Screen
     *
     * @return
     */
    public String getSenderSecondaryId() {
        if (senderSecondaryId == null || senderSecondaryId.equals("")) {
            senderSecondaryId = getIntent().getStringExtra(VariableConstants.SENDER_ID);
        }
        return senderSecondaryId;
    }

    /**
     * This private method checks if the message is present in the messages list and then returns the MessageMO
     *
     * @param persistenceKey
     * @return chatContactMO
     */
    private ChatMessageMO convContainsMessage(long persistenceKey) {
        Iterator<MessageItem> msgIte = messages.iterator();

        while (msgIte.hasNext()) {
            MessageItem next = msgIte.next();
            if (next instanceof SectionItem) {
                continue;
            }
            ChatMessageMO currMO = (ChatMessageMO) next;
            if (currMO.getPersistenceKey() == persistenceKey) {
                return currMO;
            }
        }
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MessageItem item = (MessageItem) adapter.getItem(position);
        if (!item.isSection()) {
            ChatMessageMO message = (ChatMessageMO) item;
            String attachmentID = message.getAttachmentID();
            String attachmentType = message.getAttachmentType();
            if (null != attachmentID) {
                if (null != attachmentType && TextConstants.ATTACHMENT_ACTVITY.equalsIgnoreCase(attachmentType)) {

                } else if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)
                        || attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)
                        || attachmentType.equals(TextConstants.ATTACHMENT_TEXT)) {
                    AttachmentMo attachmentMo = mService.getAttachmentMo(message.getPersistenceKey() + "");
                    if (null != attachmentMo) {
                        String attachmentPath = attachmentMo.getAttachmentPath();
                        String status = attachmentMo.getStatus();
                        File file = null;
                        file = null != attachmentPath && !"".equals(attachmentPath) ? new File(attachmentPath) : null;

                        if (null != file && file.exists()) {
                            openFile(file, attachmentType, attachmentPath, false, false);
                        }
                        /**
                         * File does not exist in the directory
                         */
                        else if (null != file && !file.exists()) {
                            showError(MyApplication.getAppContext().getResources().getString(R.string.File_does_not_exist));
                        }

                        /**
                         * attachment id exist but not path in case of file not downloaded or file received but file download is not in process
                         */
                        else if (status.equals(TextConstants.ATTACHMENT_STATUS_RECEIVED)
                                || status.equals(TextConstants.ATTACHMENT_STATUS_IN_PROGRESS)) {
                            String fileType = null;
                            if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)) {
                                fileType = VariableConstants.ATTACHMENT_IMAGE;
                            } else if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)) {
                                fileType = VariableConstants.ATTACHMENT_VIDEO;
                            }

//                   TaskManager instance = TaskManager.getInstance();
//                   String attachmentStatus = message.getAttachmentStatus();
//                   if (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_RECEIVED)
//                         && !instance.ismTaskQueueExist(message.getPersistenceKey() + "")) {
//                      if (Utils.showDialogForNoNetwork(MessageActivity.this, false)) {
//                         DownloadFileRunnable downloadFileRunnable = new DownloadFileRunnable(MessageActivity.this, attachmentID, message,
//                               chatConversationMO, mService, fileType);
//                         instance.addTask(downloadFileRunnable);
//                         instance.startTask();
//                      }
//                   }

                            AttachmentQueue attachmentQueue = AttachmentQueue.getInstance();
                            String attachmentStatus = message.getAttachmentStatus();
                            if (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_RECEIVED)
                                    && !attachmentQueue.ismTaskQueueExist(message.getPersistenceKey() + "")) {
                                if (Utils.showDialogForNoNetwork(MessageActivity.this, false)) {
                                    DownloadFileRunnable downloadFileRunnable = new DownloadFileRunnable(MessageActivity.this, attachmentID, message,
                                            chatConversationMO, mService, fileType);
                                    DownloadAttachmentFile downloadAttachmentFile = new DownloadAttachmentFile(MessageActivity.this, attachmentID, message,
                                            chatConversationMO, mService, fileType);
                                    attachmentQueue.addTask(message.getPersistenceKey() + "", downloadAttachmentFile);
                                }
                            }

                        }
                        // for cancel the file downloading
                        else if (status.equals(TextConstants.ATTACHMENT_STATUS_DOWNLOADING) || status.equals(TextConstants.ATTACHMENT_STATUS_WAITING)) {
                            Toast.makeText(this, getResources().getString(R.string.Need_to_implement_cancel_funtionality), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            // File not uploaded add file to upload again from the path
            else if (null == attachmentID && null != attachmentType && !"".equals(attachmentType)) {
                AttachmentMo attachmentMo = mService.getAttachmentMo(message.getPersistenceKey() + "");
                String status = attachmentMo.getStatus();
                // for cancel the file downloading
                if (status.equals(TextConstants.ATTACHMENT_STATUS_IN_PROGRESS) || status.equals(TextConstants.ATTACHMENT_STATUS_WAITING)) {
                    String attachmentPath = attachmentMo.getAttachmentPath();
                    File file = new File(attachmentPath);
                    if (null != file && file.exists()) {
                        openFile(file, attachmentType, attachmentPath, false, false);
                    }
                    return;

                }
                if (!TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED.equals(status)) {
                    return;
                }
                if (!TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED.equals(attachmentMo.getStatus())) {
                    return;
                }
                String attachmentPath = attachmentMo.getAttachmentPath();
                if (null != attachmentPath) {
                    File file = new File(attachmentPath);
                    if (file.exists()) {
//                        TaskManager instance = TaskManager.getInstance();
//                        if (!instance.ismTaskQueueExist(message.getPersistenceKey() + "")) {
//                            if (Utils.showDialogForNoNetwork(MessageActivity.this, false)) {
//                                message.setImageThumbnailPath(attachmentMo.getImageThumbnailPath());
//                                message.setImageThumb(message.getImageThumb());
//                                SendImageFileRunnable sendImageFile = new SendImageFileRunnable(this, message, chatConversationMO, file, mService,
//                                        attachmentMo.getAttachmentType(), file.length(), true);
//                                instance.addTask(sendImageFile);
//                                instance.startTask();
//                            }
//                        }
                        AttachmentQueue attachmentQueue = AttachmentQueue.getInstance();
                        if (!attachmentQueue.ismTaskQueueExist(message.getPersistenceKey() + "")) {
                            if (Utils.showDialogForNoNetwork(MessageActivity.this, false)) {
                                message.setImageThumbnailPath(attachmentMo.getImageThumbnailPath());
                                message.setImageThumb(message.getImageThumb());
                                SendAttachmentFile sendImageFile = new SendAttachmentFile(this, message, chatConversationMO, file, mService,
                                        attachmentMo.getAttachmentType(), file.length(), true);
                                attachmentQueue.addTask(message.getPersistenceKey() + "", sendImageFile);
                            }
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.File_does_not_exist), Toast.LENGTH_LONG).show();
                    }
                } else {
//                    Toast.makeText(this, "Path not found", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    /**
     * File exist in the the path (file already downloaded)
     *
     * @param file
     * @param attachmentType
     * @param attachmentPath
     * @param isAttachementPrevew
     * @param isCapturedImage
     */
    private void openFile(File file, String attachmentType, String attachmentPath, boolean isAttachementPrevew, boolean isCapturedImage) {
        if (isAttachementPrevew) {
            Intent shareImageIntent = new Intent(this, VideoViewActivity.class);
            shareImageIntent.putExtra(VariableConstants.VIDEO_URI, attachmentPath);
            shareImageIntent.putExtra(VariableConstants.ATTACHMENT_TYPE, attachmentType);
            shareImageIntent.putExtra(VariableConstants.IS_ATTACHMENT_PREVIEW_BEFORE_SEND, isAttachementPrevew);
            shareImageIntent.putExtra(VariableConstants.IS_CAPTURED_IMAGE, isCapturedImage);
            startActivityForResult(shareImageIntent, ATTACHMENT_PREVIEW);
        } else {
            startActivityForResult(
                    new Intent(this, VideoViewActivity.class).putExtra(VariableConstants.VIDEO_URI, attachmentPath)
                            .putExtra(VariableConstants.ATTACHMENT_TYPE, attachmentType)
                            .putExtra(VariableConstants.IS_ATTACHMENT_PREVIEW_BEFORE_SEND, isAttachementPrevew), ATTACHMENT_PREVIEW);
        }
    }

    /**
     * Encript the file for storing in sdcard
     *
     * @param file
     */
    public void encriptThumb(final File file) {
        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap previewBitmap = MyApplication.getPreviewBitmap();
                new EncriptFile(MessageActivity.this, previewBitmap, file).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                System.gc();
            }
        });
    }

    private void errorGroupCreating() {
        if (isGroupCreated || groupCreatedHandler == null) {
            return;
        }
        if (progressBar.isShown()) {
            dismissProgress();
        }
        Toast.makeText(MessageActivity.this, getResources().getString(R.string.error_group_creating), Toast.LENGTH_SHORT).show();
        finish();
    }

   /*
     * private Message createMessageObject(String text, String attachmentId, String attachmentType, long timeStamp, ChatConversationMO chatConvMO,
    * ChatMessageMO chatMessageMO, boolean isApiSendMessage, boolean isPendingMessages) { String messageText = text.trim(); Message msg = null; msg =
    * new Message(chatConvMO.getJId(), Message.Type.groupchat);
    *
    * if (null != chatMessageMO) { // This means this is resending a failed message - hence manually updating the packetId
    * msg.setPacketID(chatMessageMO.getMsgId()); }
    *
    * messageText = msg.getPacketID() + VariableConstants.DELIMITER + messageText; msg.setBody(messageText); msg.setAttachmentId(attachmentId);
    * msg.setAttachmentType(attachmentType); msg.setTo(chatConvMO.getJId()); return msg; }
    */

    public void dismissProgress() {
        isConversationCreated = false;
        btnSendMessage.setVisibility(View.VISIBLE);
        etMessageText.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    class LastSegmentName {
        private int id;
        private String path;

        public LastSegmentName(int id, String filePath) {
            if (null != filePath && !"".equals(filePath)) {
                this.id = id;
                this.path = filePath;
            } else {
                this.id = 0;
                this.path = "";
            }
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    class MyObserver extends ContentObserver {
        public MyObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            if (!isHandled) {
                isHandled = true;
                String mediaId = MediaStore.Images.Media._ID;
                String mediaData = MediaStore.Images.Media.DATA;
                Uri uri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

                final String[] imageColumns = {mediaId};
                final String imageOrderBy = mediaId + " DESC";
                final String imageWhere = null;
                final String[] imageArguments = null;
                Cursor imageCursor = getContentResolver().query(uri1, imageColumns, imageWhere, imageArguments, imageOrderBy);
                if (imageCursor != null && imageCursor.moveToNext()) {

                    int id = imageCursor.getInt(imageCursor.getColumnIndex(mediaId));

                    String[] projection = {mediaData};
                    String whereClause = mediaId + "=?";
                    Cursor cursor = getContentResolver().query(uri1, projection, whereClause, new String[]{id + ""}, null);
                    int column_index = cursor.getColumnIndexOrThrow(mediaData);
                    String string = "";
                    if (cursor.moveToFirst()) {
                        string = cursor.getString(column_index);
                        Utils.deleteFile(MessageActivity.this, string, uri1, "1");
                    }
                    imageCursor.close();
                }
            }
        }

    }


    @Override
    public void onPermissionGranted(String permission) {
        super.onPermissionGranted(permission);
        showBottomSheet();
    }

    @Override
    public void onMessengerServiceBind() {
        super.onMessengerServiceBind();
        boolean isUpdateChatContactMo = false;
        mService.setSenderId(senderSecondaryId);
        if (isGroupChat) {
            setGroupImage();
            //GroupChatTextMo
        } else if (isRecentChat) {
            ChatContactMO tempChatContactMO = new ChatContactMO();
            try {
                tempChatContactMO.generatePersistenceKey(URLDecoder.decode(username, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
            chatContactMO = mService.getChatContactMo(tempChatContactMO.getPersistenceKey() + "");
            if (chatContactMO != null) {
                contactPhoneNumber = chatContactMO.getPhoneNo();
                setProfile();
            }
        } else {
            chatContactMO = (ChatContactMO) getIntent().getExtras().get(TextConstants.CHAT_DETAIL);
            // Fix Me
            chatContactUserType = (UserType) getIntent().getExtras().get(VariableConstants.SENDER_USER_TYPE);
            chatContactMO.setUserType(chatContactUserType);
            // Fix Me
            byte[] profilePic = chatContactMO.getProfilePic();

            ChatContactMO tempChatContactMO = mService.getChatContactMo(chatContactMO.getPersistenceKey() + "");
            if (null != tempChatContactMO) {
                isUpdateChatContactMo = true;
                if ((null == profilePic || profilePic.length <= 0) && null != tempChatContactMO.getProfilePic()) {
                    chatContactMO.setProfilePic(tempChatContactMO.getProfilePic());
                    chatContactDBAdapter.getInstance(MessageActivity.this).updateChatContactMoUserProfile(chatContactMO.getPersistenceKey(), chatContactMO, false);
                } else
                    chatContactDBAdapter.getInstance(MessageActivity.this).updateChatContactMoUserProfile(chatContactMO.getPersistenceKey(), chatContactMO, false);
                if (tempChatContactMO.getPresenceType() != null) {
                    chatContactMO.setPresenceType(tempChatContactMO.getPresenceType());
                }
                if (tempChatContactMO.getPresenceStatus() != null) {
                    chatContactMO.setPresenceStatus(tempChatContactMO.getPresenceStatus());
                }
            }

            if (chatContactMO.getPersistenceKey() == 0) {
                chatContactMO.generatePersistenceKey(chatContactMO.getId());
            }

            setProfile();

            if (mBound && null != mService) {
                if (!isUpdateChatContactMo)
                    chatContactDBAdapter.getInstance(MessageActivity.this).insert(chatContactMO);
            }
        }
        refreshMessages(true, false);

            /*

            TODO  ---if problem persist need to uncomment it

            */

        // changeStaustForAttachments();

        setReadStatus();

        if (mBound
                && null != chatConversationMO
                && (null == chatConversationMO.getNickName() || chatConversationMO.getNickName().contains(
                "@" + VariableConstants.groupConferenceName))) {
            String groupJid = chatConversationMO.getJId();

            if (!groupJid.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                mService.getGroupName(groupJid);
                mService.getGroupMembers(groupJid);
            }
        }
        if (isSubscribeScreen && (nickName == null || nickName.equals("") || nickName.contains("@" + settings.getFisikeServerIp()))) {
            loadContactsRealName = new LoadContactsRealName(mService, Utils.cleanXMPPServer(senderSecondaryId), MessageActivity.this)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            loadContactProfilePicAsyn = new LoadContactsProfilePicAsyn(mService, Utils.cleanXMPPServer(senderSecondaryId), MessageActivity.this)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

//            if (TextUtils.isEmpty(contactPhoneNumber)) {

        if (ivActionPhone == null) {
            ivActionPhone = (ImageView) findViewById(R.id.iv_phone);
        }
        ivActionPhone.setVisibility(View.GONE);
//            }

    }


}
