package com.mphrx.fisike.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.NumberKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.Utils;

import java.lang.reflect.Field;

public class EditTextFontView extends android.support.v7.widget.AppCompatEditText {

    private final Context context;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public EditTextFontView(Context context) {
        super(context);
        this.context = context;
        applyCustomFont(context, null);
    }

    public EditTextFontView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }


    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);
        Typeface customFont = selectTypeface(context, fontName, fontName);
        setTypeface(customFont);
        setTypeface(customFont);
        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName, String textStyle) {
        if (textStyle == null) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        }
        if (textStyle.equals(VariableConstants.BOLD)) {
            return FontCache.getTypeface("sans/OpenSans-Bold.ttf", context);
        } else if (textStyle.equals(VariableConstants.MEDIUM)) {
            return FontCache.getTypeface("sans/OpenSans-Semibold.ttf", context);
        } else if (textStyle.equals(VariableConstants.LIGHT)) {
            return FontCache.getTypeface("sans/OpenSans-Light.ttf", context);
        } else if (textStyle.equals(VariableConstants.REGULAR)) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        } else if (textStyle.equals(VariableConstants.ITALIC)) {
            return FontCache.getTypeface("sans/OpenSans-Italic.ttf", context);
        }
        return FontCache.getTypeface("sans/OpenSans-Semibold.ttf", context);

    }
}
