package com.mphrx.fisike.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;

/**
 * Created by kailashkhurana on 18/07/17.
 */

public class CustomCheckBox extends android.support.v7.widget.AppCompatCheckBox {

    private final Context context;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public CustomCheckBox(Context context) {
        super(context);
        this.context = context;
        applyCustomFont(context, null);

    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);
        Typeface customFont = selectTypeface(context, fontName, fontName);
        setTypeface(customFont);
        setTypeface(customFont);
        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName, String textStyle) {
        if (textStyle == null) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        }
        if (textStyle.equals(VariableConstants.BOLD)) {
            return FontCache.getTypeface("sans/OpenSans-Bold.ttf", context);
        } else if (textStyle.equals(VariableConstants.MEDIUM)) {
            return FontCache.getTypeface("sans/OpenSans-Semibold.ttf", context);
        } else if (textStyle.equals(VariableConstants.LIGHT)) {
            return FontCache.getTypeface("sans/OpenSans-Light.ttf", context);
        } else if (textStyle.equals(VariableConstants.REGULAR)) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        } else if (textStyle.equals(VariableConstants.ITALIC)) {
            return FontCache.getTypeface("sans/OpenSans-Italic.ttf", context);
        }
        return FontCache.getTypeface("sans/OpenSans-Semibold.ttf", context);

    }
}
