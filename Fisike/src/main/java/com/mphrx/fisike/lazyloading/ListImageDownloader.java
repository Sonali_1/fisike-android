package com.mphrx.fisike.lazyloading;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.mphrx.fisike.R;
import com.mphrx.fisike.beans.Series;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.widget.ImageView;


public class ListImageDownloader
{
    public enum Mode { NO_ASYNC_TASK, NO_DOWNLOADED_DRAWABLE, CORRECT }
    private Mode mode = Mode.CORRECT;

    private String token;
    private JSONObject jsonObject;
    Context context;

    public ListImageDownloader(Context context) {
        this.context = context;
    }

    /**
     * Download the specified image from the Internet and binds it to the provided ImageView. The
     * binding is immediate if the image is found in the cache and will be done asynchronously
     * otherwise. A null bitmap will be associated to the ImageView if an error occurs.
     *
     * @param url The URL of the image to download.
     * @param imageView The ImageView to bind the downloaded image to.
     */
    public void download(String url, ImageView imageView, String token, JSONObject jsonObject)
    {
        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(url);

        this.token = token;
        this.jsonObject = jsonObject;

        // System.out.println(jsonObject.toString());

        if (bitmap == null)
        {
            forceDownload(url, imageView);
        }
        else
        {
            cancelPotentialDownload(url, imageView);
            imageView.setImageBitmap(bitmap);
        }
    }

    /*
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
       private void forceDownload(String url, ImageView view)
       {
       		forceDownload(url, view, null);
       }
     */

    /**
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
     */
    private void forceDownload(String url, ImageView imageView)
    {
        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
        if (url == null)
        {
            imageView.setImageDrawable(null);
            return;
        }

        if (cancelPotentialDownload(url, imageView))
        {
            switch (mode)
            {
                case NO_ASYNC_TASK:
                    Bitmap bitmap = downloadBitmap(url,null);
                    addBitmapToCache(url, bitmap);
                    imageView.setImageBitmap(bitmap);
                    break;

                case NO_DOWNLOADED_DRAWABLE:
                    //imageView.setMinimumHeight(156);
                    BitmapDownloaderTask task1 = new BitmapDownloaderTask(imageView);
                    task1.execute(url);
                    break;

                case CORRECT:
                    BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
                    DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
                    imageView.setImageDrawable(downloadedDrawable);
                    //imageView.setMinimumHeight(156);
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                    break;
            }
        }
    }

    /**
     * Returns true if the current download has been canceled or if there was no download in
     * progress on this image view.
     * Returns false if the download in progress deals with the same url. The download is not
     * stopped in that case.
     */
    private static boolean cancelPotentialDownload(String url, ImageView imageView)
    {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null)
        {
            String bitmapUrl = bitmapDownloaderTask.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url)))
            {
                bitmapDownloaderTask.cancel(true);
            }
            else
            {
                // The same URL is already being downloaded.
                return false;
            }
        }
        return true;
    }

    /**
     * @param imageView Any imageView
     * @return Retrieve the currently active download task (if any) associated with this imageView.
     * null if there is no such task.
     */
    private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView)
    {
        if (imageView != null)
        {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable)
            {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    Bitmap downloadBitmap(String url,BitmapFactory.Options opt)
    {
        @SuppressWarnings("unused")
        final int IO_BUFFER_SIZE = 4 * 1024;

        // AndroidHttpClient is not allowed to be used from the main thread
        final HttpClient client = (mode == Mode.NO_ASYNC_TASK) ? new DefaultHttpClient() :
                AndroidHttpClient.newInstance("Android");
        final HttpPost postRequest = new HttpPost(url);


        StringEntity se = null;
        try {
            se = new StringEntity(jsonObject.toString());
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        // se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        if (token != null) {
            postRequest.setHeader("x-auth-token", token);
        }
        postRequest.setEntity(se);
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");


        try
        {
            HttpResponse response = client.execute(postRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK)
            {
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null)
            {
                InputStream inputStream = null;
                try
                {
                    inputStream = entity.getContent();
                    return BitmapFactory.decodeStream(new FlushedInputStream(inputStream),null,opt);
                }
                finally
                {
                    if (inputStream != null)
                    {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        }
        catch (IOException e)
        {
            postRequest.abort();
        }
        catch (IllegalStateException e)
        {
            postRequest.abort();
        }
        catch (Exception e)
        {
            postRequest.abort();
        }
        finally
        {
            if ((client instanceof AndroidHttpClient))
            {
                ((AndroidHttpClient) client).close();
            }
        }
        return null;
    }


    public Bitmap decodeSampledBitmapFromResource(FlushedInputStream res, int reqWidth,String url)
    {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(res, null, options);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, (options.outHeight*reqWidth)/options.outWidth);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return downloadBitmap(url, options);
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth)
        {
            if (width > height)
            {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            }
            else
            {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }
        return inSampleSize;
    }

    /*
     * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
     */
    static class FlushedInputStream extends FilterInputStream
    {
        public FlushedInputStream(InputStream inputStream)
        {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException
        {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n)
            {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L)
                {
                    int b = read();
                    if (b < 0)
                    {
                        break;  // we reached EOF
                    }
                    else
                    {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    /**
     * The actual AsyncTask that will asynchronously download the image.
     */
    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap>
    {
        private String url;
        private ImageView imageViewReference;

        public BitmapDownloaderTask(ImageView imageView)
        {
            imageViewReference =  imageView;
        }

        /**
         * Actual download method.
         */
        @Override
        protected Bitmap doInBackground(String... params)
        {
            url = params[0];

            BitmapFactory.Options options = new BitmapFactory.Options();

            // Calculate inSampleSize
            options.inSampleSize = 2;

//            // Decode bitmap with inSampleSize set
//            options.inJustDecodeBounds = false;

            return downloadBitmap(url,options);
        }

        /**
         * Once the image is downloaded, associates it to the imageView
         */
        @Override
        protected void onPostExecute(Bitmap bitmap)
        {
            if (isCancelled())
            {
                bitmap = null;
            }


            if(bitmap==null)
            {
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_na);
                imageViewReference.setImageBitmap(bitmap);
            }
            else
                imageViewReference.setImageBitmap(bitmap);

            addBitmapToCache(url, bitmap);
        }
    }

    /**
     * A fake Drawable that will be attached to the imageView while the download is in progress.
     *
     * <p>Contains a reference to the actual download task, so that a download task can be stopped
     * if a new binding is required, and makes sure that only the last started download process can
     * bind its result, independently of the download finish order.</p>
     */
    static class DownloadedDrawable extends ColorDrawable
    {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask)
        {
            super(Color.GRAY);
            bitmapDownloaderTaskReference =  new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask()
        {
            return bitmapDownloaderTaskReference.get();
        }
    }

    public void setMode(Mode mode)
    {
        this.mode = mode;
        clearCache();
    }
    
    /*
     * Cache-related fields and methods.
     * 
     * We use a hard and a soft cache. A soft reference cache is too aggressively cleared by the
     * Garbage Collector.
     */

    private static final int HARD_CACHE_CAPACITY = 50;
    private static final int DELAY_BEFORE_PURGE = 50 * 1000; // in milliseconds

    // Hard cache, with a fixed maximum capacity and a life duration
    @SuppressWarnings("serial")
    private final HashMap<String, Bitmap> sHardBitmapCache =
            new LinkedHashMap<String, Bitmap>(HARD_CACHE_CAPACITY / 2, 0.75f, true)
            {
                @Override
                protected boolean removeEldestEntry(LinkedHashMap.Entry<String, Bitmap> eldest)
                {
                    if (size() > HARD_CACHE_CAPACITY)
                    {
                        // Entries push-out of hard reference cache are transferred to soft reference cache
                        sSoftBitmapCache.put(eldest.getKey(), new SoftReference<Bitmap>(eldest.getValue()));
                        return true;
                    }
                    else
                        return false;
                }
            };

    // Soft cache for bitmaps kicked out of hard cache
    private final static ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache =
            new ConcurrentHashMap<String, SoftReference<Bitmap>>(HARD_CACHE_CAPACITY / 2);

    private final Handler purgeHandler = new Handler();

    private final Runnable purger = new Runnable()
    {
        @Override
        public void run()
        {
            clearCache();
        }
    };

    /**
     * Adds this bitmap to the cache.
     * @param bitmap The newly downloaded bitmap.
     */
    private void addBitmapToCache(String url, Bitmap bitmap)
    {
        if (bitmap != null)
        {
            synchronized (sHardBitmapCache)
            {
                sHardBitmapCache.put(url, bitmap);
            }
        }
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    private Bitmap getBitmapFromCache(String url)
    {
        // First try the hard reference cache
        synchronized (sHardBitmapCache)
        {
            final Bitmap bitmap = sHardBitmapCache.get(url);
            if (bitmap != null)
            {
                // Bitmap found in hard cache
                // Move element to first position, so that it is removed last
                sHardBitmapCache.remove(url);
                sHardBitmapCache.put(url, bitmap);
                return bitmap;
            }
        }

        // Then try the soft reference cache
        SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
        if (bitmapReference != null)
        {
            final Bitmap bitmap = bitmapReference.get();
            if (bitmap != null)
            {
                // Bitmap found in soft cache
                return bitmap;
            }
            else
            {
                // Soft reference has been Garbage Collected
                sSoftBitmapCache.remove(url);
            }
        }

        return null;
    }

    /**
     * Clears the image cache used internally to improve performance. Note that for memory
     * efficiency reasons, the cache will automatically be cleared after a certain inactivity delay.
     */
    public void clearCache()
    {
        sHardBitmapCache.clear();
        sSoftBitmapCache.clear();
    }

    /**
     * Allow a new delay before the automatic cache clear is done.
     */
    private void resetPurgeTimer()
    {
        purgeHandler.removeCallbacks(purger);
        purgeHandler.postDelayed(purger, DELAY_BEFORE_PURGE);
    }
}
