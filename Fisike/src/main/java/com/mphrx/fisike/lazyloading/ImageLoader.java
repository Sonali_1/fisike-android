package com.mphrx.fisike.lazyloading;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.ProfilePictureGson;
import com.mphrx.fisike.gson.request.ProfilePictureGson.ProfilePicture;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.*;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.SharedPref;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import javax.net.ssl.HttpsURLConnection;

public class ImageLoader {

    private static final String CONTACT_PIC_ARRAY = "profilePicture";
    private final String GET_PROFILE_PIC = "/getProfilePicture";
    private static final String USER_NAME = "username";
    private static final String PROFILE_PIC = "profilePic";
    private MemoryCache memoryCache = MemoryCache.getInstance();
    private FileCache fileCache;
    private ExecutorService executorService;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private Context context;
    private byte[] profilePic;
    private boolean isupload_physician = false;
    private int curved_radius = 0;

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        this.context = context;
        executorService = Executors.newFixedThreadPool(10);
    }

    // final int stub_id=R.drawable.physician_generic;

    public void DisplayImage(String userJid, ImageView imageView, int drawable) {
        imageViews.put(imageView, userJid);
        Bitmap bitmap = memoryCache.get(userJid);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            queuePhoto(userJid, imageView);
            imageView.setImageResource(drawable);
        }
    }


    /*[   Method used for downloading images for upload fragment in physican app ]*/
    public void DisplayImage(String userJid, ImageView imageView, int drawable, boolean isupload_physician, int curved_radius) {
        this.isupload_physician = isupload_physician;
        this.curved_radius = curved_radius;
        imageViews.put(imageView, userJid);
        Bitmap bitmap = memoryCache.get(userJid);
        if (bitmap != null) {
            if (isupload_physician) {
                bitmap = Utils.getCurvedCornerBitmap(bitmap, curved_radius);
            }
            imageView.setImageBitmap(bitmap);
        } else {
            queuePhoto(userJid, imageView);
            imageView.setImageResource(drawable);

        }
    }

    private void queuePhoto(String userJid, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(userJid, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String userJid) {
        File f = fileCache.getFile(userJid);

        Bitmap bitmap = decodeFile(f);
        if (bitmap != null)
            return bitmap;

        try {

            String url;

            JSONObject jsonObject = new JSONObject();

            if (isupload_physician) {
                byte[] docImage = downloadFile(userJid);
                if (docImage != null && docImage.length > 0) {
                    bitmap = BitmapFactory.decodeByteArray(docImage, 0, docImage.length);
                }

                return bitmap;

            } else {
                jsonObject.put("id", userJid);
                url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + GET_PROFILE_PIC;
            }
            APIManager jsonManager = APIManager.getInstance();
            String jsonString = jsonManager.executeHttpPost(url, "", jsonObject, context);
            if (jsonString != null && !("".equals(jsonString))) {
                if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
                    return null;
                }

                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ProfilePictureGson.class);

                if (jsonToObjectMapper instanceof ProfilePictureGson) {
                    ProfilePictureGson profilePictureGson = ((ProfilePictureGson) jsonToObjectMapper);
                    ArrayList<ProfilePicture> profilePictureArray = profilePictureGson.getProfilePictureArray();
                    for (int i = 0; i < profilePictureArray.size(); i++) {
                        ProfilePicture profilePicture = profilePictureArray.get(i);
                        profilePic = profilePicture.getProfilePic();
                    }
                    if (profilePic != null) {
                        bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private byte[] downloadFile(String docId) {
        InputStream input = null;
        HttpsURLConnection urlConnection = null;
        ByteArrayOutputStream output = null;
        try {
            URL httpPost = new URL(MphRxUrl.getDocumentImageUrl());
            urlConnection = (HttpsURLConnection) httpPost.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);
            urlConnection.setRequestProperty("Content-Type", "application/json");
         /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
            String authToken = SharedPref.getAccessToken();
            if (authToken != null) {
                urlConnection.addRequestProperty("x-auth-token", authToken);
            }

            String str = "{\"docRefId\":" + Integer.parseInt(docId) + "}";
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            urlConnection.connect();

            String headerType = urlConnection.getHeaderField("Content-Type");

            int fileLength = urlConnection.getContentLength();

            // download the file
            input = urlConnection.getInputStream();

            byte data[] = new byte[2048];
            int count;
            long total = 0;
            output = new ByteArrayOutputStream();

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
            return output.toByteArray();
        } catch (Exception e) {
            return null;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }


    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {

        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2
            final int REQUIRED_SIZE = 70;

            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.userJid))
            return true;
        return false;
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    private Bitmap parseprofilePic(JSONArray jsonArray) throws Exception {
        byte[] profilePic = new byte[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            profilePic[i] = (byte) jsonArray.getInt(i);
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
        return bitmap;
    }

    private Bitmap parseJson(String responseString) throws Exception {
        JSONObject contactArray = new JSONObject(responseString);
        JSONArray jsonArray = contactArray.getJSONArray(CONTACT_PIC_ARRAY);
        jsonArray.length();
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        jsonObject.getString(USER_NAME);
        // parses the json as well as updates the db
        return parseprofilePic(jsonObject.getJSONArray(PROFILE_PIC));

    }

    public void clearSdCard() {
        fileCache.clear();
    }

    // Task for the queue
    private class PhotoToLoad

    {
        public String userJid;
        ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            userJid = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.userJid);
            memoryCache.put(photoToLoad.userJid, bmp);
            if (isupload_physician) {
                bmp = Utils.getCurvedCornerBitmap(bmp, curved_radius);
            }
            //  BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            // Activity a = (Activity) photoToLoad.imageView.getContext();
            // a.runOnUiThread(bd);
            final Bitmap finalBmp = bmp;
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (imageViewReused(photoToLoad))
                        return;
                    if (finalBmp != null) {
                        photoToLoad.imageView.setImageBitmap(finalBmp);
                    }
                }
            });
        }
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
            }
        }
    }

}