package com.mphrx.fisike.lazyloading;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.fragments.UploadTaggingFragment;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;

public class DecriptThumbnail {

    private MemoryCache memoryCache = MemoryCache.getInstance();
    private FileCache fileCache;
    private ExecutorService executorService;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private Context context;
    private byte[] imageThumb;
    private int drawable;
    private ProgressBar progressDownloading;
    private UploadTaggingFragment uploadTaggingFragment;

    public DecriptThumbnail(Context context) {
        this.context = context;
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(10);
    }

    // final int stub_id=R.drawable.physician_generic;

    public void DisplayImage(String url, ImageView imageView, int drawable, byte[] imageThumb, ProgressBar progressDownloading,
                             UploadTaggingFragment uploadTaggingFragment) {
        this.drawable = drawable;
        this.imageThumb = imageThumb;
        this.progressDownloading = progressDownloading;
        this.uploadTaggingFragment = uploadTaggingFragment;
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            lp1.height = LayoutParams.MATCH_PARENT;
            lp1.width = LayoutParams.MATCH_PARENT;
            imageView.setLayoutParams(lp1);
        } else {
            queuePhoto(url, imageView);
            if (imageThumb != null) {
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageThumb, 0, imageThumb.length));
            } else {
                imageView.setImageResource(drawable);
            }
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String url) {
        byte[] decodFile = Citadel.decodFile(url, context);
        if (decodFile == null) {
            return null;
        }
        if (uploadTaggingFragment != null) {
            uploadTaggingFragment.setDecodedFile(decodFile);
        }
        return BitmapFactory.decodeByteArray(decodFile, 0, decodFile.length);
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    public void clearSdCard() {
        fileCache.clear();
    }

    // Task for the queue
    private class PhotoToLoad

    {
        public String url;
        ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.url);
            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
//			Activity a = (Activity) photoToLoad.imageView.getContext();
//			a.runOnUiThread(bd);
            photoToLoad.imageView.post(bd);
            memoryCache.put(photoToLoad.url, bmp);
        }
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad)) {
                return;
            }
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) photoToLoad.imageView.getLayoutParams();
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
                lp1.height = LayoutParams.MATCH_PARENT;
                lp1.width = (int) Utils.convertDpToPixel(200, context);
                photoToLoad.imageView.setLayoutParams(lp1);
            } else if (imageThumb != null) {
                photoToLoad.imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageThumb, 0,
                        imageThumb.length));
                lp1.height = LayoutParams.MATCH_PARENT;
                lp1.width = (int) Utils.convertDpToPixel(200, context);
                photoToLoad.imageView.setLayoutParams(lp1);
            } else {
                photoToLoad.imageView.setImageResource(drawable);
            }
            // progressDownloading.setVisibility(View.GONE);
            // photoToLoad.imageView.setImageBitmap(bitmap);
            // else
            // photoToLoad.imageView.setImageResource(stub_id);
        }
    }
}