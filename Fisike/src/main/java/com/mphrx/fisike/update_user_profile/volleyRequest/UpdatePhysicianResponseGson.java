package com.mphrx.fisike.update_user_profile.volleyRequest;

import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike_physician.models.PhysicianMO;

/**
 * Created by Neha on 26-12-2016.
 */

public class UpdatePhysicianResponseGson {
    String msg;
    String status;
    String httpStatusCode;
    UserMO user;
    String verificationCode;
    PractitionerMO physician;

    public PractitionerMO getPhysician() {
        return physician;
    }

    public void setPhysician(PractitionerMO physician) {
        this.physician = physician;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public UserMO getUser() {
        return user;
    }

    public void setUser(UserMO user) {
        this.user = user;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
