package com.mphrx.fisike.update_user_profile.Enum;

import android.content.Context;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha Rathore on 3/31/2017.
 */

public enum MaritalStatusEnum {

    UNMARRIED(MyApplication.getAppContext().getString(R.string.unmarried_key), R.string.unmarried),
    ANNULLED(MyApplication.getAppContext().getString(R.string.annulled_key), R.string.annulled),
    DIVORCED(MyApplication.getAppContext().getString(R.string.divorced_key), R.string.divorced),
    INTERLOCUTORY(MyApplication.getAppContext().getString(R.string.interlocutory_key), R.string.interlocutory),
    LEGALLYSEPERATED(MyApplication.getAppContext().getString(R.string.legally_seperated_key), R.string.legally_seperated),
    MARRIED(MyApplication.getAppContext().getString(R.string.married_key), R.string.married),
    POLYGAMOUS(MyApplication.getAppContext().getString(R.string.polygamous_key), R.string.polygamous),
    NEVERMARRIED(MyApplication.getAppContext().getString(R.string.never_married_key), R.string.never_married),
    DOMESTICPARTNER(MyApplication.getAppContext().getString(R.string.domestic_partner_key), R.string.domestic_partner),
    WIDOWED(MyApplication.getAppContext().getString(R.string.widowed_key), R.string.widowed);

    private String code;
    private int value;
    private static LinkedHashMap<String, MaritalStatusEnum> maritalStatusMap = null;
    private static Context context = MyApplication.getAppContext();

    private MaritalStatusEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static LinkedHashMap<String, MaritalStatusEnum> getMaritalStatusMap() {
        if (maritalStatusMap == null) {
            maritalStatusMap = new LinkedHashMap<String, MaritalStatusEnum>();
            for (MaritalStatusEnum statusEnum : MaritalStatusEnum.values()) {
                maritalStatusMap.put(statusEnum.code, statusEnum);
            }
        }
        return maritalStatusMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getMaritalStatusMap();
        for (Map.Entry<String, MaritalStatusEnum> statusEnumMap : maritalStatusMap.entrySet())
            if (statusEnumMap.getValue().getValue().equals(value))
                return statusEnumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        try {
            getMaritalStatusMap();
            if (maritalStatusMap != null) {
                for (Map.Entry<String, MaritalStatusEnum> enumMap : maritalStatusMap.entrySet())
                    if (enumMap.getValue().getCode().equals(value))
                        return enumMap.getValue().getValue();
            }
            return value;
        } catch (Exception e) {

        }
        return value;
    }
}
