package com.mphrx.fisike.update_user_profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.background.getPatientShowDetails;
import com.mphrx.fisike.background.updateUserProfileBG;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.AddressTypeEnum;
import com.mphrx.fisike.enums.DesignationEnum;
import com.mphrx.fisike.enums.ExperienceEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.enums.SpecialityEnum;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.BirthDate;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.Contact_name;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.UpdatePatientRequest;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.request.patIdentifier;
import com.mphrx.fisike.gson.request.telecom;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.Practitioner_Data.Extension;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.Practitioner_Data.Role;
import com.mphrx.fisike.mo.Practitioner_Data.Speciality;
import com.mphrx.fisike.mo.Practitioner_Data.Value;
import com.mphrx.fisike.mo.Practitioner_Data.practitionerRole;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.PhysicianDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowRequest;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowResponse;
import com.mphrx.fisike.update_user_profile.volleyRequest.UpdateProfileRequestGsonPractitioner;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.request.UpdateProfileRequest;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by Kailash Khurana on 4/11/2016.
 */
public class UpdateProfileBaseActivity extends BaseActivity {

    private static final int BASIC_INFO = 0;
    private static final int ADDRESS_INFO = 1;
    private static final int ADDRESS_INFO_DELETE = 2;
    private static final int ALTERNATE_CONTACT = 3;
    private static final int DELETE_CONTACT = 4;
    private PatientMO patientMo;
    private ProgressDialog pd_ring;
    private User userdata = new User();

    private String firstName;
    private String lastName;
    private String maritalStatus;
    private String dob;
    private String gender;
    private String myRole;
    private String speciality;
    private String yearOfExperience;

    private CustomFontEditTextView edFirstName;
    private CustomFontEditTextView edLastName;
    private CustomFontTextView txtGender;
    private CustomFontEditTextView edDOB;
    private CustomFontTextView txtMaritalStatus;

    private ArrayList<Address> addressUserMos;
    private int index;

    private CustomFontButton tvAddressType;
    private CustomFontEditTextView etPostalCode;
    private CustomFontEditTextView etCity;
    private CustomFontEditTextView etAdd1;
    private CustomFontEditTextView etState;
    private CustomFontEditTextView etAdd2;
    private CustomFontEditTextView edMyRole;
    private CustomFontEditTextView edSpeciality;
    private CustomFontEditTextView edYearOfExperience;
    private CustomFontEditTextView etCountry;

    private int updateFlow;
    private String city;
    private String state;
    private String zip;
    private String userCode;
    private ArrayList<String> line;
    private String alternateContact;
    private String country;
    private PractitionerMO physicianMo;
    private CustomFontEditTextView edAadhar;
    private String aadharNumber;


    public void updateProfileFields(CustomFontEditTextView edFirstName, CustomFontEditTextView
            edLastName, CustomFontTextView txtGender, CustomFontEditTextView edDOB,
                                    CustomFontTextView txtMaritalStatus, CustomFontEditTextView edAadhar) {
        updateFlow = BASIC_INFO;
        this.edFirstName = edFirstName;
        this.edLastName = edLastName;
        this.txtGender = txtGender;
        this.edDOB = edDOB;
        this.txtMaritalStatus = txtMaritalStatus;
        this.edAadhar = edAadhar;
        if (updateFlow != ALTERNATE_CONTACT && updateFlow != DELETE_CONTACT) {
            alternateContact = userMO.getAlternateContact();
        }
        if (isFieldsEntered() && isValidateAge()) {
            if (isFieldUpdated()) {
                if (Utils.showDialogForNoNetwork(this, false)) {
                    getPatientInfo(userMO.getPatientId() + "");
                }
            } else {
                this.finish();
            }
        }
    }

    public void updatePhysicianProfileFields(CustomFontEditTextView edFirstName,
                                             CustomFontEditTextView edLastName,
                                             CustomFontEditTextView edMyRole,
                                             CustomFontEditTextView edSpeciality,
                                             CustomFontEditTextView edYearOfExperience) {
        updateFlow = BASIC_INFO;
        this.edFirstName = edFirstName;
        this.edLastName = edLastName;
        this.edMyRole = edMyRole;
        this.edSpeciality = edSpeciality;
        this.edYearOfExperience = edYearOfExperience;
        if (isFieldsEntered() && isSpecialityEntered() && isRoleEntered() &&
                isYearsOfExperiencEntered()) {
            if (isPhysicianFieldUpdated()) {
                if (Utils.showDialogForNoNetwork(this, false)) {
                    getPhysicianInfo();
                }
            } else {
                this.finish();
            }
        }
    }

    public void addAddressFields(ArrayList<Address> addressUserMos, int index, CustomFontButton
            tvAddressType, CustomFontEditTextView etPostalCode, CustomFontEditTextView etCity,
                                 CustomFontEditTextView etState, CustomFontEditTextView etAdd1,
                                 CustomFontEditTextView etAdd2, CustomFontEditTextView etCountry) {
        updateFlow = ADDRESS_INFO;
        this.etState = etState;
        this.etAdd2 = etAdd2;
        this.addressUserMos = addressUserMos;
        this.index = index;
        this.tvAddressType = tvAddressType;
        this.etPostalCode = etPostalCode;
        this.etCity = etCity;
        this.etAdd1 = etAdd1;
        this.etCountry = etCountry;
        getUserFields();
        if (isFieldsEnteredAddress()) {
            if (!(index >= 0 && index < addressUserMos.size()) || isFieldUpdatedAddress
                    (addressUserMos.get(index))) {
                if (Utils.showDialogForNoNetwork(this, false)) {
                    setAddressUserMo();
                    if (BuildConfig.isPatientApp) {
                        getPatientInfo(userMO.getPatientId() + "");
                    } else {
                        getPhysicianInfo();
                    }
                }
            } else {
                this.finish();
            }
        }
    }

    private void getPhysicianInfo() {
        showprogressdialog();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        try {
            physicianMo = PhysicianDBAdapter.getInstance(this).fetchPhysicianInfo(userMO.getPhysicianId() + "");
        } catch (Exception e) {
        }

        if (physicianMo == null) {
            ThreadManager.getDefaultExecutorService().submit(new PractionerShowRequest(getTransactionId(), userMO.getPhysicianId() + ""));
        } else
            sendApiForUpdatePhysician(physicianMo);

    }

    public void updateProfileRequest(UpdateProfileResponse response) {
        dismissProgressDialog_pat();
        if (response.isSuccessful()) {
            updateUserInfo(null);
            if (BuildConfig.isPhysicianApp) {
                updatePhysicianMo(response.getUpdatePhysicianResponseGSON().getPhysician());
            }
            setResult(RESULT_OK);
            if (updateFlow == DELETE_CONTACT && (this instanceof UserProfileActivity)) {
                ((UserProfileActivity) this).setAlternateContact();

                return;
            }
            if (updateFlow != DELETE_CONTACT) {
                finish();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.Something_went_wron), Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePhysicianMo(PractitionerMO physicianMO) {
        if (physicianMO != null) {
            try {
                PhysicianDBAdapter.getInstance(this).createOrUpdatePhysicianMo(physicianMO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updatePhysicianAddress(UpdateProfileResponse response) {
        if (updateFlow == DELETE_CONTACT) {
            updateProfileRequest(response);
            return;
        }
        if (response.isSuccessful()) {
            updateAddressInfo(null);
            setResult(RESULT_OK);
            if (updateFlow != ADDRESS_INFO_DELETE) {
                finish();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.Something_went_wron), Toast.LENGTH_SHORT).show();
        }
        dismissProgressDialog_pat();
    }

    public void addAlternateContactFields(String alternateContact) {
        updateFlow = ALTERNATE_CONTACT;
        this.alternateContact = alternateContact;
        getUserFields();
        if (BuildConfig.isPatientApp) {
            getPatientInfo(userMO.getPatientId() + "");
        } else {
            getPhysicianInfo();
        }
    }


    public void deleteAlternateContactFields() {
        updateFlow = DELETE_CONTACT;
        alternateContact = "";
        getUserFields();
        if (BuildConfig.isPatientApp) {
            getPatientInfo(userMO.getPatientId() + "");
        } else {
            getPhysicianInfo();
        }
    }

    public void deleteAddress(ArrayList<Address> addressUserMos) {
        updateFlow = ADDRESS_INFO_DELETE;
        this.addressUserMos = addressUserMos;
        getUserFields();
        if (BuildConfig.isPatientApp) {
            getPatientInfo(userMO.getPatientId() + "");
        } else {
            getPhysicianInfo();
        }
    }

    private void getAddressFields() {
        city = etCity.getText().toString().trim();
        state = etState.getText().toString().trim();

        userCode = AddressTypeEnum.getCodeFromValue(tvAddressType.getText().toString());
        if (userCode.equals("")) {
            userCode = getResources().getString(R.string.type_addres);
        }

        zip = etPostalCode.getText().toString().trim();
        line = new ArrayList<>();
        line.add(etAdd1.getText().toString().trim());
        line.add(etAdd2.getText().toString().trim());
        country = etCountry.getText().toString().trim();
    }

    private void setAddressUserMo() {
        Address address = new Address();
        if(userCode.equals(getResources().getString(R.string.type_addres))){
            address.setUseCode("");
        }
        else
        address.setUseCode(userCode);
        address.setCity(city);
        address.setState(state);
        address.setPostalCode(zip);
        address.setLine(line);
        address.setCountry(country);
        String line2Address = line.get(1).trim().equals("") ? "" : line.get(1) + " ";
        String text = line.get(0) + " " + line2Address + city + " " + state + " " + zip + " " +
                country;
        address.setText(text.trim());
        if (addressUserMos.size() > index) {
            addressUserMos.remove(index);
            addressUserMos.add(index, address);
        } else {
            addressUserMos.add(address);
        }
    }

    private void getUserFields() {
        firstName = userMO.getFirstName();
        lastName = userMO.getLastName();
        if (BuildConfig.isPatientApp) {
            if ((dob == null) && userMO.getDateOfBirth() == null) {
                dob = null;
            } else {
                dob = Utils.getFormattedDate(userMO.getDateOfBirth(), DateTimeUtil
                        .yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil
                        .destinationDateFormatWithoutTime);
            }
            gender = userMO.getGender();

            String maritalStatusValue = userMO.getMaritalStatus();
             maritalStatus = (Utils.isValueAvailable(maritalStatusValue)) ?
                    (MaritalStatusEnum.getMaritalStatusMap().containsKey(maritalStatusValue)
                            ? MaritalStatusEnum.getMaritalStatusMap().get(maritalStatusValue).getValue() : "")
                    : "";
        } else {
            myRole = SharedPref.getRole();
            speciality = SharedPref.getSpeciality();
            yearOfExperience = SharedPref.getExperience();
            initSpecRoleExp();

        }
        aadharNumber = userMO.getAadharNumber();
    }

    //checking weather user has updated aadhar or not
    private boolean checkAadhar() {
        //added aadhar number
        if (Utils.isValueAvailable(aadharNumber)) {
            if (Utils.isValueAvailable(userMO.getAadharNumber())) //aadhar available in UserMo
                if (aadharNumber.equalsIgnoreCase(userMO.getAadharNumber()))
                    return false; //aadhar is same as previous aadhar
                else return true;
        }
        return false;
    }

    private boolean isFieldUpdated() {
        String date = DateTimeUtil.convertSourceDestinationDate(userMO.getDateOfBirth(),DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated,DateTimeUtil.destinationDateFormatWithoutTime);
        if (firstName.equals(userMO.getFirstName()) && lastName.equals(userMO.getLastName()) &&
                dob.equals(date) && gender.equals(userMO.getGender()) &&
                MaritalStatusEnum.getCodeFromValue(maritalStatus).equals(userMO.getMaritalStatus())) {
            if (BuildConfig.isAadharEnabled)
                return (checkAadhar());

            return false;
        }
        return true;
    }

    private boolean isPhysicianFieldUpdated() {
        if (firstName.equals(userMO.getFirstName()) && lastName.equals(userMO.getLastName()) &&
                myRole.equals(SharedPref.getRole()) && speciality.equals(SharedPref.getSpeciality
                ()) && yearOfExperience.equals(SharedPref.getExperience())) {
            return false;
        }
        return true;
    }

    private boolean isFieldUpdatedAddress(Address address) {
        if (userCode.equals(address.getUseCode()) && zip.equals(address.getPostalCode()) && city
                .equals(address.getCity()) && state.equals(address.getState()) && line.get(0)
                .equals(address.getLine().get(0)) &&
                line.get(1).equals(address.getLine().get(1))
                ) {
            if (address.getCountry() != null && country.equalsIgnoreCase(address.getCountry()))
                return false;
        }
        return true;
    }

    private boolean isFieldsEntered() {
        getString();
        if (firstName.equals("")) {
            edFirstName.setError(this.getResources().getString(R.string.please_enter_first_name));
            return false;
        }else if (BuildConfig.isAadharEnabled) {
            if (Utils.isValueAvailable(userMO.getAadharNumber()) && aadharNumber.equals("")) {
                edAadhar.setError(getString(R.string.aadhar_length_error));
                return false;

            } else if (!aadharNumber.equals("") && aadharNumber.length() != Integer.parseInt(getString(R.string.aadhar_length))) {
                edAadhar.setError(getString(R.string.aadhar_length_error));
                return false;
            }
        }
        return true;
    }

    private boolean isSpecialityEntered() {
        if (edSpeciality.getText().trim().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.blank_speciality), Toast
                    .LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isRoleEntered() {
        if (edMyRole.getText().trim().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.blank_role), Toast
                    .LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isYearsOfExperiencEntered() {
        if (edYearOfExperience.getText().trim().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.blank_years_of_exp), Toast
                    .LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isFieldsEnteredAddress() {
        getAddressFields();
        if (line.get(0).equals("")) {
            etAdd1.requestFocus();
            etAdd1.setError(getResources().getString(R.string.enter_a_valid_value));
            return false;
        } /*else if (zip.equals("")) {
            etPostalCode.setError(TextConstants.BLANK_ERROR);
			etPostalCode.requestFocus();
			return false;
		} */ else if (city.equals("")) {
            etCity.setError(getResources().getString(R.string.enter_a_valid_value));
            etCity.requestFocus();
            return false;
        } else if (state.equals("")) {
            etState.setError(getResources().getString(R.string.enter_a_valid_value));
            etState.requestFocus();
            return false;
        } else if (country.equals("")) {
            etCountry.setError(getResources().getString(R.string.enter_a_valid_value));
            etCountry.requestFocus();
            return false;
        }
        return true;
    }

    private void getString() {
        firstName = edFirstName.getText().toString().trim();
        lastName = edLastName.getText().toString().trim();
        if (BuildConfig.isPatientApp) {
            dob = edDOB.getText().toString();
            gender = GenderEnum.getCodeFromValue(txtGender.getText().toString().trim());
            maritalStatus = txtMaritalStatus.getText().toString();
            aadharNumber = edAadhar.getText().toString();
        } else {

            myRole = edMyRole.getText().toString().trim();
            speciality = edSpeciality.getText().toString().trim();
            yearOfExperience = edYearOfExperience.getText().toString().trim();

            /*getting code to be sent in API from
            displayed value for speciality designation and role
             */
            if (Utils.isValueAvailable(myRole))
                myRole = DesignationEnum.getCodeFromValue(myRole);

            if (Utils.isValueAvailable(speciality))
                speciality = SpecialityEnum.getCodeFromValue(speciality);

            if (Utils.isValueAvailable(yearOfExperience)){
                String tempYears = edYearOfExperience.getText().toString().trim();
                yearOfExperience = tempYears.substring(0, tempYears.indexOf(" "));
//                yearOfExperience = ExperienceEnum.getCodeFromValue(yearOfExperience);
            }
        }
    }

    private void getPatientInfo(String id) {
        showprogressdialog();
        patientMo = SettingManager.getInstance().getPatientMo(id);

        if (patientMo == null) {
            new getPatientShowDetails(this, id).execute();
        } else {
            sendApiForUpdate();
        }

    }

    private boolean isValidateAge() {

        try {
            String month[] = getResources().getStringArray(R.array.month_arr);
            ArrayList<String> mon = new ArrayList<String>(Arrays.asList(month));
            String[] date = dob.split(" ");

            int d = Integer.parseInt(date[0]);
            int index = mon.indexOf(date[1].toLowerCase().toString());
            int m = index + 1;
            int y = Integer.parseInt(date[2]);
            int age = getAge(y, m, d);
            edDOB.setErrorEnabled(false);
            if (age >= 18)
                return true;
            else {
                edDOB.requestFocus();
                edDOB.setError(MyApplication.getAppContext().getResources().getString(R.string.must_18_old_above));
                return false;
            }
        } catch (Exception e) {
            edDOB.setText("");
            edDOB.requestFocus();
            edDOB.setError(MyApplication.getAppContext().getResources().getString(R.string.enter_valid_age));
            return false;
        }
    }

    public int getAge(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;
        try {

            y = cal.get(Calendar.YEAR);
            m = cal.get(Calendar.MONTH) + 1;
            d = cal.get(Calendar.DAY_OF_MONTH);
            cal.set(year, month, day);
            a = y - year;
            if ((m < month)
                    || ((m == month) && (d < day))) {
                --a;
            }
        } catch (Exception e) {
            e.printStackTrace();
            a = 0;
        }
        return a;
    }

    private void sendApiForUpdate() {
        getData();
        String maritalStatusCode = MaritalStatusEnum.getCodeFromValue(maritalStatus);
        if (Utils.isValueAvailable(maritalStatus)) {
            if (patientMo.getMaritalStatus() == null) {
                CodingExtensionTextFormat codingExtensionTextFormat = new
                        CodingExtensionTextFormat();
                codingExtensionTextFormat.setText(maritalStatusCode);
                patientMo.setMaritalStatus(codingExtensionTextFormat);
            } else {
                patientMo.getMaritalStatus().setText(maritalStatusCode);
            }
        }

        UpdatePatientRequest req = new UpdatePatientRequest();
        if (updateFlow == ALTERNATE_CONTACT) {
            userdata.setAlternateContact(alternateContact);
        } else if (updateFlow == DELETE_CONTACT)
            userdata.setAlternateContact(alternateContact);

        req.setUser(userdata);
        req.setUserType(TextConstants.PATIENT);
        BirthDate newdob = patientMo.getBirthDate();
        if (Utils.isValueAvailable(userdata.getDob())) {
            if (newdob == null) {
                newdob = new BirthDate();
                newdob.setValue(userdata.getDob());
            } else
                newdob.setValue(userdata.getDob());
            patientMo.setBirthDate(newdob);
        }

        if (patientMo.getGender() != null && !patientMo.getGender().equals(getResources().getString(R.string.txt_null))) {
            if (userdata.getGender() != null && !userdata.getGender().equals("")) {
                patientMo.setGender(userdata.getGender().toUpperCase().substring(0, 1));
            } else {
                patientMo.setGender(null);
            }
        } else {
            if (userdata.getGender() != null && !userdata.getGender().equals("")) {
                patientMo.setGender(userdata.getGender().toUpperCase().substring(0, 1));
            } else
                patientMo.setGender(null);
        }
        ArrayList<Contact_name> newName = patientMo.getName();
        ArrayList<String> familyListlname = new ArrayList<String>();
        ArrayList<String> familyListfname = new ArrayList<String>();

        if (patientMo.getName() != null && patientMo.getName().get(0) != null) {
            if (patientMo.getName().get(0).getFamily() != null) {
                familyListlname.add(userdata.getLastName() != null && !userdata.getLastName().equals(getResources().getString(R.string.txt_null)) ? userdata.getLastName() : "");
                newName.get(0).setFamily(familyListlname);
            }

            familyListfname.add(userdata.getFirstName());
            newName.get(0).setGiven(familyListfname);
            newName.get(0).setText(userdata.getFirstName() + " " + (userdata.getLastName() != null
                    ? userdata.getLastName() : ""));

        }

        ArrayList<telecom> Newtelecom = patientMo.getTelecom();
        for (int i = 0; i < Newtelecom.size(); i++) {
            if (Utils.isValueAvailable(Newtelecom.get(i).getUseCode()) && Newtelecom.get(i).getUseCode().equals("email")) {
                Newtelecom.get(i).setValue(userdata.getEmail());
            } else if (Utils.isValueAvailable(Newtelecom.get(i).getUseCode()) && Newtelecom.get(i).getUseCode().equals("Home Phone")) {
                Newtelecom.get(i).setValue(userdata.getPhoneNo());
            }
        }


        if (updateFlow == ADDRESS_INFO || updateFlow == ADDRESS_INFO_DELETE) {
            patientMo.setAddress(addressUserMos);
        }

        if (BuildConfig.isAadharEnabled)
            updateAadharInPatientMo();

        req.setPatient(patientMo);

        new updateUserProfileBG(this, req, false).executeOnExecutor(AsyncTask
                .THREAD_POOL_EXECUTOR);

    }

    private void updateAadharInPatientMo() {
        patIdentifier aadharIdentifier = patientMo.getIdentifierObjectBasedOnKey(getString(R.string.aadhar_key));
        if (Utils.isValueAvailable(aadharNumber)) {
            if (aadharIdentifier != null)
                aadharIdentifier.setValue(aadharNumber);
            else
                createIdentifierforAadhar(aadharNumber);
        } else if (aadharIdentifier != null && aadharNumber != null && aadharNumber.equals("")) //removed aadhar Number
        {
            patientMo.getIdentifier().remove(patientMo.getIdentifierObjectIndexOnKey(getString(R.string.aadhar_key)));
        }
    }

    private void createIdentifierforAadhar(String aadharNumber) {
        patIdentifier identifier = new patIdentifier();
        identifier.setValue(aadharNumber);
        identifier.setSystem(getString(R.string.system_type));
        identifier.setUseCode(getString(R.string.usual_key));
        identifier.setUse(getString(R.string.usual_key));
        HashMap<String, Object> mapType = new HashMap<>();
        try {
            mapType.put(getString(R.string.coding_type), new ArrayList<Object>());
            mapType.put(getString(R.string.extension_type), new ArrayList<Object>());
            mapType.put(getString(R.string.id_type), null);
            mapType.put(getString(R.string.text_type), getString(R.string.aadhar_key));
        } catch (Exception e) {

        }
        identifier.setType(mapType);
        if (patientMo.getIdentifier() == null)
            patientMo.setIdentifier(new ArrayList<patIdentifier>());
        patientMo.getIdentifier().add(identifier);
    }


    private void getData() {
        userdata.setId(userMO.getId());
        userdata.setFirstName(firstName);
        userdata.setLastName(lastName);
        if (BuildConfig.isPatientApp) {
            if (gender != null && !gender.equals(GenderEnum.getGenderEnumLinkedHashMap().get(getString(R.string.gender_other_key)).getValue())) {
                userdata.setGender(gender.length() == 1 ? gender.equalsIgnoreCase(getResources().getString(R.string.gender_male_initial)) ? getResources().getString(R.string.gender_male_key) :
                        getResources().getString(R.string.gender_female_key) : gender);
            }
            //      Log.i("genderrrr", userdata.getGender().toString());
            Weight wt = new Weight();
            wt.setValue(userMO.getWeight().getValue());
            wt.setUnit(userMO.getWeight().getUnit());
            userdata.setWeight(wt);
            Height ht = new Height();
            ht.setValue(userMO.getHeight().getValue());
            ht.setUnit(userMO.getHeight().getUnit());
            userdata.setHeight(ht);
            try {
                if (dob == null) {
                    userdata.setDob(dob);
                } else if (Utils.isValueAvailable(dob)) {
                    userdata.setDob(DateTimeUtil.convertSourceDestinationDateEnglish(dob, DateTimeUtil
                            .destinationDateFormatWithoutTime, DateTimeUtil
                            .yyyy_MM_dd_T_HH_mm_ss_z_appended));
                } else {
                    dob = Utils.getFormattedDateEnglish(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil
                            .yyyy_MM_dd_T_HH_mm_ss_z_appended);
                    userdata.setDob(dob);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                userdata.setDob(userMO.getDateOfBirth());
            }
        }
        userdata.setEmail(SharedPref.getEmailAddress());
        userdata.setPhoneNo(SharedPref.getMobileNumber());
        userdata.setAlternateContact(userMO.getAlternateContact());
    }

    public void executePatientCallResult(PatientMO result) {
        if (result != null) {
            patientMo = result;
            sendApiForUpdate();
        } else {
            pd_ring.dismiss();
            showErro(MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again));
        }
    }

    private void showprogressdialog() {
        pd_ring = new ProgressDialog(this);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hangon));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
        pd_ring.show();
    }

    void showErro(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void updateUserInfo(updatePatientResponse result) {
        ContentValues contentValues = new ContentValues();
        UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(this);
        contentValues.put(userDBAdapter.getFIRSTNAME(), firstName);
        contentValues.put(userDBAdapter.getLASTNAME(), lastName);
        if (BuildConfig.isPatientApp) {
            contentValues.put(userDBAdapter.getWEIGHT_UNIT(), userdata.getWeight().getUnit());
            contentValues.put(userDBAdapter.getWEIGHT_VALUE(), userdata.getWeight().getValue());
            contentValues.put(userDBAdapter.getHEIGHT_UNIT(), userdata.getHeight().getUnit());
            contentValues.put(userDBAdapter.getHEIGHT_VALUE(), userdata.getHeight().getValue());
            contentValues.put(userDBAdapter.getDATEOFBIRTH(), Utils.getFormattedDateEnglish(result.getUser().getDob()
                    , DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil
                            .yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));
            contentValues.put(userDBAdapter.getGENDER(), userdata.getGender());
            if (patientMo.getMaritalStatus() != null && Utils.isValueAvailable(patientMo.getMaritalStatus().getText())) {
                String maritalStatusText = patientMo.getMaritalStatus().getText();
                contentValues.put(UserDBAdapter.getMARITALSTATUS(), maritalStatusText);
            }
            userMO.setGender(userdata.getGender());
        }


        contentValues.put(userDBAdapter.getALTERNATECONTACT(), alternateContact);

        try {
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (BuildConfig.isPatientApp) {
            try {
                SettingManager.getInstance().savePatientMo(result.getPatient());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateAddressInfo(updatePatientResponse result) {
        try {
            ContentValues contentValues = new ContentValues();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(this);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(addressUserMos);
            contentValues.put(userDBAdapter.getADDRESS(), bytes.toByteArray());
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (BuildConfig.isPatientApp) {
            try {
                SettingManager.getInstance().savePatientMo(result.getPatient());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dismissProgressDialog_pat() {
        if (pd_ring != null && pd_ring.isShowing()) {
            pd_ring.dismiss();
        }
    }

    public void executeUpdateProfileResult(updatePatientResponse result, String resultStatus) {
        dismissProgressDialog_pat();
        userMO = SettingManager.getInstance().getUserMO();
        if (result != null && resultStatus.equals(TextConstants.SUCESSFULL_API_CALL)) {
            if (updateFlow == ADDRESS_INFO) {
                updateAddressInfo(result);
                setResult(RESULT_OK, getIntent());
                finish();
            } else if (updateFlow == ADDRESS_INFO_DELETE) {
                updateAddressInfo(result);
                if (this instanceof UserProfileActivity) {
                    ((UserProfileActivity) this).setAddressUserData();
                }
            } else {
                updateUserInfo(result);
                if (updateFlow == ALTERNATE_CONTACT) {
                    SharedPref.setIsToShowAlternateContactDialog(false);
                    Toast.makeText(this, getString(R.string.add_update_alternate_contact), Toast.LENGTH_SHORT).show();
                }
                if (updateFlow == DELETE_CONTACT && (this instanceof UserProfileActivity)) {
                    ((UserProfileActivity) this).setAlternateContact();
                    return;
                }

                setResult(RESULT_OK);
                finish();
            }
        } else {
            showErro(resultStatus);
        }
    }

    public boolean checkPermission(final String permissionType, String message, final int
            requestCode) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager
                .PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(UpdateProfileBaseActivity.this, new
                                    String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted();

                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
            }

            return false;
        }

        return true;
    }

    public void onPermissionNotGranted() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
    }

    public void onPhysicianDetailResponse(PractionerShowResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        userMO = SettingManager.getInstance().getUserMO();

        if (response.isSuccessful() && response.getPractitioneMOFromResponse() != null) {
            try {
                physicianMo = response.getPractitioneMOFromResponse();
                PhysicianDBAdapter.getInstance(this).createOrUpdatePhysicianMo(physicianMo);
                updateAddressInUserMo(physicianMo);
                sendApiForUpdatePhysician(physicianMo);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            dismissProgressDialog();
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void sendApiForUpdatePhysician(PractitionerMO physicianMo) {
        UpdateProfileRequestGsonPractitioner req = new UpdateProfileRequestGsonPractitioner();
        //setting User Information
        User user = new User();
        user.setFirstName(Utils.isValueAvailable(firstName) ? firstName : userMO.getFirstName());
        user.setLastName(Utils.isValueAvailable(lastName) ? lastName : userMO.getLastName());
        user.setId(userMO.getId());
        user.setDob(userMO.getDob());
        /*if (gender != null && !gender.equals(getString(R.string.i_other)))
            user.setGender(gender.length() == 1 ? gender.equalsIgnoreCase("M") ? "Male" :
                    "Female" : gender);*/
        user.setGender(userMO.getGender());
        user.setDob(userMO.getDateOfBirth());
        user.setEmail(SharedPref.getEmailAddress());
        user.setPhoneNo(SharedPref.getMobileNumber());
        user.setAlternateContact(userMO.getAlternateContact());

        //Updating alternate contact
        if (updateFlow == ALTERNATE_CONTACT || updateFlow == DELETE_CONTACT) {
            user.setAlternateContact(alternateContact);
        } else {
            user.setAlternateContact(userMO.getAlternateContact());
            alternateContact = userMO.getAlternateContact();
        }

        //updating address
        if (updateFlow == ADDRESS_INFO || updateFlow == ADDRESS_INFO_DELETE) {
            physicianMo.setAddress(addressUserMos);
        } else {
            physicianMo.setAddress(userMO.getAddressUserMos());
        }

        Role userRole;
        ArrayList<practitionerRole> practitionerRoleList;
        practitionerRole practitionerSpecRole;
        //getting physician Speciality Role List if null initialize new
        if (physicianMo.getPractitionerRole() == null || physicianMo.getPractitionerRole().size() == 0)
            practitionerRoleList = new ArrayList<>();
        else
            practitionerRoleList = physicianMo.getPractitionerRole();


        //getting element at 0th index if null created onle
        if (practitionerRoleList.size() > 0 && practitionerRoleList.get(0) != null)
            practitionerSpecRole = practitionerRoleList.get(0);
        else
            practitionerSpecRole = new practitionerRole();

        //getting role if null initializing
        if (physicianMo.getPractitionerRole() != null && physicianMo.getPractitionerRole().size() >= 1 && physicianMo.getPractitionerRole().get(0).getRole() != null)
            userRole = physicianMo.getPractitionerRole().get(0).getRole();
        else
            userRole = new Role();

        //setting User Role
        userRole.setText(myRole);
        //setting role in practitioner
        if (Utils.isValueAvailable(myRole))
            practitionerSpecRole.setRole(userRole);

        ArrayList<Speciality> specialityArrayList = null;
        if (physicianMo.getPractitionerRole().size() > 0) {
            specialityArrayList = physicianMo.getPractitionerRole().get(0).getSpecialty();
        }
        if (specialityArrayList == null || specialityArrayList.size() == 0)
            specialityArrayList = new ArrayList<>();

        Speciality userSpeciality;
        //getting speciality if null initializing
        if (physicianMo.getPractitionerRole() != null && physicianMo.getPractitionerRole().size() >= 1 && physicianMo.getPractitionerRole().get(0).getSpecialty() != null && physicianMo.getPractitionerRole().get(0).getSpecialty().size() > 0)
            userSpeciality = physicianMo.getPractitionerRole().get(0).getSpecialty().get(0);
        else
            userSpeciality = new Speciality();

        userSpeciality.setText(speciality);

        try {
            specialityArrayList.set(0, userSpeciality);
        } catch (Exception e) {
            specialityArrayList.add(0, userSpeciality);
        }

        if (Utils.isValueAvailable(speciality))
            practitionerSpecRole.setSpecialty(specialityArrayList);

        if (Utils.isValueAvailable(myRole) || Utils.isValueAvailable(speciality)) {
            if (practitionerRoleList.size() == 0) {
                practitionerRoleList.add(0, practitionerSpecRole);
            } else {
                practitionerRoleList.set(0, practitionerSpecRole);
            }
            //setting speciality and designation in physician
            physicianMo.setPractitionerRole(practitionerRoleList);
        }
        boolean hasfound = false;
        int index = 0;
        Extension years = null;
//        if (yearOfExperience != null || !yearOfExperience.equals("")){
            ArrayList<Extension> yearsOfExp = physicianMo.getExtension();
        if (yearsOfExp != null || yearsOfExp.size() > 0) {
            for (Extension ext : yearsOfExp) {
                if (Utils.isValueAvailable(ext.getUrl()) && ext.getUrl().equalsIgnoreCase(TextConstants.EXPERIENCE)) {
                    years = ext;
                    ArrayList<Value> valueList = ext.getValue();
                    Value val = valueList.get(0);
                    val.setNoOfExp(yearOfExperience);
                    hasfound = true;
                    valueList.set(0, val);
                    years.setValue(valueList);
                    break;
                }
                index = index + 1;
            }

            if (hasfound) {
                yearsOfExp.set(index, years);
            } else {
                Extension ext = new Extension();
                ext.setUrl(TextConstants.EXPERIENCE);
                ArrayList<Value> valueArrayList = new ArrayList<>();
                Value v = new Value();
                v.setNoOfExp(yearOfExperience);
                valueArrayList.add(0, v);
                ext.setValue(valueArrayList);
                yearsOfExp.add(0, ext);
            }

        } else {
            yearsOfExp = new ArrayList<>();
            Extension ext = new Extension();
            ext.setUrl(getString(R.string.experience));
            ArrayList<Value> valueArrayList = new ArrayList<>();
            Value v = new Value();
            v.setNoOfExp(yearOfExperience);
            valueArrayList.add(0, v);
            ext.setValue(valueArrayList);
            yearsOfExp.add(0, ext);
        }


        physicianMo.setExtension(yearsOfExp);
        Contact_name physiName = physicianMo.getName();
        ArrayList<String> familyListlnam, familyListfname;
        if (physiName == null)
            physiName = new Contact_name();

        if (physiName != null) {
            familyListlnam = physiName.getFamily();
            //setting last name
            if (familyListlnam == null || familyListlnam.size() == 0) {
                familyListlnam = new ArrayList<>();
                familyListlnam.add(0, user.getLastName() != null && !user.getLastName().equals(getResources().getString(R.string.txt_null)) ? user.getLastName() : "");
            } else {
                familyListlnam.set(0, user.getLastName() != null && !user.getLastName().equals(getResources().getString(R.string.txt_null)) ? user.getLastName() : "");
            }
            if (physiName.getUseCode() == null || physiName.getUseCode().equals("")) {
                physiName.setUseCode("Usual");
            }

            physiName.setFamily(familyListlnam);

            familyListfname = physiName.getGiven();
            if (familyListfname == null || familyListfname.size() == 0) {
                familyListfname = new ArrayList<>();
                familyListfname.add(0, user.getFirstName());
            } else {
                familyListfname.set(0, user.getFirstName());
            }
            physiName.setGiven(familyListfname);
            physiName.setText(user.getFirstName() + " " + (user.getLastName() != null
                    ? user.getLastName() : ""));
        }

        physicianMo.setName(physiName);

        if (updateFlow == ADDRESS_INFO || updateFlow == ADDRESS_INFO_DELETE) {
            physicianMo.setAddress(addressUserMos);
        }

        ArrayList<telecom> Newtelecom = physicianMo.getTelecom();
        for (int i = 0; i < Newtelecom.size(); i++) {
            if (Utils.isValueAvailable(Newtelecom.get(i).getUseCode()) && Newtelecom.get(i).getUseCode().equals("email")) {
                Newtelecom.get(i).setValue(user.getEmail());
            } else if (Utils.isValueAvailable(Newtelecom.get(i).getUseCode()) && Newtelecom.get(i).getUseCode().equals("Home Phone")) {
                Newtelecom.get(i).setValue(user.getPhoneNo());
            }
        }

        physicianMo.setTelecom(Newtelecom);
        //setting Userdate
        req.setUser(user);
        //setting UserType
        req.setUserType(userMO.getUserType());
        //setting physician info
        req.setPhysician(physicianMo);

        ThreadManager.getDefaultExecutorService().submit(new UpdateProfileRequest(req,
                getTransactionId()));
    }


    private void updateAddressInUserMo(PractitionerMO practitionerMO) {
        try {
            ContentValues contentValues = new ContentValues();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(this);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getAddress());
            contentValues.put(userDBAdapter.getADDRESS(), bytes.toByteArray());
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initSpecRoleExp() {
          /*getting code to be sent in API from
            displayed value for speciality designation and role
             */
        if (Utils.isValueAvailable(myRole))
            myRole = DesignationEnum.getCodeFromValue(myRole);

        if (Utils.isValueAvailable(speciality))
            speciality = SpecialityEnum.getCodeFromValue(speciality);

//        if(Utils.isValueAvailable(yearOfExperience))
//            yearOfExperience= ExperienceEnum.getCodeFromValue(yearOfExperience);
    }
}