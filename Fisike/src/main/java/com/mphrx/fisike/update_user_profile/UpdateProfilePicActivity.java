package com.mphrx.fisike.update_user_profile;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.background.RemoveProfilePicture;
import com.mphrx.fisike.background.UploadProfilePic;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UploadPicture;
import com.mphrx.fisike.imageCropping.Crop;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.views.CircleImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Kailash Khurana on 4/5/2016.
 */
public class UpdateProfilePicActivity extends UpdateProfileBaseActivity implements View.OnClickListener {

    protected static final int SUCESS_RESULT = 1, CAMERA_RESULT = 2, GALLERY_RESULT = 3, CROP_RESULT = 4,
                               READ_WRITE_INTENT = 5;;

    private RelativeLayout rlContainerUserImage;
    private ImageView imgProfile;
    private CircleImageView civUserProfilePic;
    private RelativeLayout btnChangeProfilePic;
    private CustomFontTextView tvUserNumber;

    private File photoUri;
    private Bitmap profilePic;
    private String realPathFromURI;
    private Uri mImageCaptureUri = null;
    private byte[] profilePictureByte;
    private boolean isCameraImage;
    private Bitmap tempBitmap;
    private ProgressDialog pd_ring;
    private BottomSheetDialog mBottomSheetDialog;
    public boolean isReadOnly = false;
    private String lastname;

    public void userProfileData() {
        findView();
    }

    private void setProfilePic() {
        profilePic = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());
        tvUserNumber.setText(Utils.getUserName());
        setUserProfilePic(profilePic);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setProfilePic();
    }

    public void showOrHideRemoveImageMenu(View view) {
        Utils.hideKeyboard(this);
        if (profilePic != null)
            view.findViewById(R.id.remove_image).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.remove_image).setVisibility(View.GONE);
    }

    /**
     * Show bottom sheet for sending the attachment
     */
    private void showBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.change_image_layout, null);
        showOrHideRemoveImageMenu(view);
        float peekHeight = view.getMeasuredHeight();
        view.findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        getString(R.string.permission_msg) + " " +getString(R.string.permission_storage), READ_WRITE_INTENT)) {
                    // camera permission
                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_camera), CAMERA_RESULT)){
                        takePhoto();
                    }
                }

            }
        });

        view.findViewById(R.id.choose_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), GALLERY_RESULT))
                    openGallery();

            }
        });

        view.findViewById(R.id.remove_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeImage();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void findView() {
        rlContainerUserImage = (RelativeLayout) findViewById(R.id.rl_container_userImage);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        civUserProfilePic = (CircleImageView) findViewById(R.id.civ_user_profile_pic);
        btnChangeProfilePic = (RelativeLayout) findViewById(R.id.btn_change_profile_pic);
        tvUserNumber = (CustomFontTextView) findViewById(R.id.tv_user_number);
    }

    private void setUserProfilePic(final Bitmap bitmap) {
        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap blurBitmap;

                if (bitmap != null) {
                    civUserProfilePic.setImageBitmap(bitmap);
                    profilePictureByte = Utils.convertBitmapToByteArray(bitmap);
                    try {
                        blurBitmap = Utils.blur(UpdateProfilePicActivity.this, BitmapFactory.decodeByteArray(profilePictureByte, 0, profilePictureByte.length), 20f);
                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        blurBitmap = Utils.scaleBitmapAndKeepRation(blurBitmap, (int) Utils.convertDpToPixel(187, UpdateProfilePicActivity.this), metrics.widthPixels);
                        if (UpdateProfilePicActivity.this instanceof UserProfileActivity && isReadOnly) {
                            findViewById(R.id.btn_change_profile_pic).setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        civUserProfilePic.setImageBitmap(BitmapFactory.decodeResource(UpdateProfilePicActivity.this.getResources(), R.drawable.default_profile_pic));
                        blurBitmap = BitmapFactory.decodeResource(UpdateProfilePicActivity.this.getResources(), R.drawable.bg_default_profile_bg);
                    }
                } else {
                    if (UpdateProfilePicActivity.this instanceof UserProfileActivity && isReadOnly) {
                        findViewById(R.id.btn_change_profile_pic).setVisibility(View.VISIBLE);
                    }

                    civUserProfilePic.setImageBitmap(BitmapFactory.decodeResource(UpdateProfilePicActivity.this.getResources(), R.drawable.default_profile_pic));
                    blurBitmap = BitmapFactory.decodeResource(UpdateProfilePicActivity.this.getResources(), R.drawable.bg_default_profile_bg);
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    imgProfile.setImageBitmap(blurBitmap);
                } else {
                    imgProfile.setImageBitmap(blurBitmap);
                }

            }
        });
    }


    private void takePhoto() {
        dismissBottomSheet();
        try {
            photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    System.currentTimeMillis() + ".jpg");
            photoUri.setWritable(true);
            mImageCaptureUri = Uri.fromFile(photoUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentUtils.onClickTakePhoto(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }

    private void removeImage() {
        dismissBottomSheet();
        if (Utils.showDialogForNoNetwork(this, false)) {
            new RemoveProfilePicture(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    private void openGallery() {
        dismissBottomSheet();
        photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                System.currentTimeMillis() + ".jpg");
        photoUri.setWritable(true);
        mImageCaptureUri = Uri.fromFile(photoUri);
        IntentUtils.onClickChooseImage(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }

    public void onClick(View view) {
        if (isReadOnly) {
            Toast.makeText(this, "under development...", Toast.LENGTH_SHORT).show();
        } else {
            switch (view.getId()) {
                case R.id.btn_change_profile_pic:
                    if (Utils.showDialogForNoNetwork(this, false))
                        showBottomSheet();
                    break;
                case R.id.civ_user_profile_pic:
                    if (btnChangeProfilePic.getVisibility() == View.VISIBLE) {
                        onClick(btnChangeProfilePic);
                    }
                    break;

            }
        }
    }

    /**
     * Dismiss the bottom sheet
     */
    private void dismissBottomSheet() {
        if (mBottomSheetDialog.isShowing()) {
            mBottomSheetDialog.dismiss();
        }
    }

    private void captureAndCropImage(Intent data) {
        if (null != photoUri && !"".equals(photoUri)) {
            mImageCaptureUri = Uri.fromFile(photoUri);
        }
        mImageCaptureUri = (null == data || null == data.getData()) ? mImageCaptureUri : data.getData();
        if (null != mImageCaptureUri) {
            realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
            if (null == realPathFromURI) {
                realPathFromURI = mImageCaptureUri.getPath();
                if (null == realPathFromURI) {
                    return;
                }
            }

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(realPathFromURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            startCropImage();
        }
    }

    private void startCropImage() {

        DisplayMetrics display = getResources().getDisplayMetrics();
        int screenWidth = display.widthPixels;
        int screenHeight = display.heightPixels;
        int gcd = GCD(screenWidth, screenHeight);
        int aspect_x = screenHeight / gcd;
        int aspect_y = screenWidth / gcd;
        if (screenWidth > 400)
            screenWidth = 400;
        int output_x = 400;
        int output_y = 400;
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped-profile-pic"));
        startActivityForResult(Crop.of(Uri.fromFile(photoUri), destination)
                .asSquare()
                .withMaxSize(output_x, output_y)
                .getIntent(this), CROP_RESULT);
    }

    private int GCD(int a, int b) {
        return (b == 0 ? a : GCD(b, a % b));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALLERY_RESULT:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(photoUri);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    isCameraImage = false;
                    startCropImage();
                    break;

                case CAMERA_RESULT:
                    isCameraImage = true;
                    captureAndCropImage(data);
                    break;

                case CROP_RESULT:
                    if (Utils.showDialogForNoNetwork(this, false)) {
                        Uri croppedUri = Crop.getOutput(data);
                        try {
                            //    tempBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), croppedUri);

                            ExifInterface exif = null;
                            try {
                                exif = new ExifInterface(croppedUri.getPath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                            AppLog.d("orientation", "" + orientation);

                            Matrix matrix = new Matrix();
                            if (orientation == 6) {
                                matrix.postRotate(90);
                            } else if (orientation == 3) {
                                matrix.postRotate(180);
                            } else if (orientation == 8) {
                                matrix.postRotate(270);
                            }

                            Bitmap imageBitmap =  /*ExifUtils.rotateBitmap(croppedUri.getPath().toString(),*/BitmapFactory.decodeFile(croppedUri.getPath())/*)*/;

                            tempBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);

                            profilePictureByte = Utils.convertBitmapToByteArray(tempBitmap);
                            new UploadProfilePic(this, tempBitmap, true, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } catch (Exception e) {
                            // FIXME there should atleast be a toast here ... but there wasn't one before so not adding
                        }
                        if (isCameraImage) {
                            // FIXME @Rajan probably not needed anymore
                            String croppedImagePath = realPathFromURI.toString().replace(".jpg", "~2.jpg");
                            File croppedImageFile = new File(croppedImagePath);
                            if (croppedImageFile.exists()) {
                                croppedImageFile.delete();
                            }

                            File file = new File(realPathFromURI);
                            Uri uri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                            try {
                                ContentResolver resolver = this.getContentResolver();

                                // change type to image, otherwise nothing will
                                // be deleted
                                ContentValues contentValues = new ContentValues();
                                int media_type = 1;
                                contentValues.put("media_type", media_type);
                                resolver.update(Uri.parse(realPathFromURI), contentValues, null, null);
                                resolver.delete(Uri.parse(realPathFromURI), null, null);
                            } catch (Exception e) {

                            }
                            Utils.deleteFile(this, realPathFromURI, uri1, VariableConstants.ATTACHMENT_IMAGE);
                            if (file.exists()) {
                                boolean deleted = file.delete();
                            }
                        }
                    }

                    break;

                default:
                    break;
            }
        }
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * uploadProgress
     */
    public void progressStarted() {
        showprogressdialog();
    }

    private void showprogressdialog() {
        pd_ring = new ProgressDialog(this);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hangon));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
        pd_ring.show();

    }

    private void dismissProgressDialog_pat() {
        if (pd_ring != null && pd_ring.isShowing()) {
            pd_ring.dismiss();
        }
    }

    public void removeProfilePic(String jsonString) {
        dismissProgressDialog_pat();
        if (null != jsonString) {
            if (jsonString.contains("503 Service Unavailable")) {
                errorInRemoving();
                return;
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, UploadPicture.class);

            if (jsonToObjectMapper instanceof UploadPicture) {
                UploadPicture removeProfilPicGson = ((UploadPicture) jsonToObjectMapper);
                if (removeProfilPicGson != null && removeProfilPicGson.getStatus().equals("SC200")) {
                    setUserProfilePic(null);
                    UserProfilePic.deleteProfilePic(MyApplication.getAppContext());
                    profilePictureByte = null;
                    profilePic = null;
                } else {
                    errorInRemoving();
                }
            }

        } else
            errorInRemoving();

    }

    public void uploadProfilePic(String jsonString, byte[] bitmapdata, UploadPicture uploadGson, boolean isTostopProgress) {
        if (isTostopProgress)
            dismissProgressDialog_pat();
        if (jsonString != null && uploadGson != null && uploadGson.getStatus().equals("SC200")) {
            UserProfilePic.saveToInternalStorage(tempBitmap, this);
            setUserProfilePic(tempBitmap);

        } else if (jsonString.equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
            Utils.launchLocationChangeActivity(this);
        } else if (jsonString.equalsIgnoreCase(TextConstants.UNAUTHORIZED_ACCESS)) {
            SharedPref.setAccessToken(null);
        } else {
            showErro(getResources().getString(R.string.failed_update_profile_pic));
        }

        setProfilePic();
    }

    public void errorInRemoving() {
        dismissProgressDialog_pat();
        if (Utils.isNetworkAvailable(this)) {
            showErro(getResources().getString(R.string.unable_to_contact_server));
        } else {
            showErro(MyApplication.getAppContext().getResources().getString(R.string.connection_not_available));
        }
    }

    void showErro(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_RESULT:
                    takePhoto();
                    break;
                case GALLERY_RESULT:
                    openGallery();
                    break;
                case READ_WRITE_INTENT:
                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_camera), CAMERA_RESULT))
                        takePhoto();
                    break;

            }
        } else
            onPermissionNotGranted();
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        super.onPermissionNotGranted(permission);
        Toast.makeText(this, getResources().getString(R.string.We_need_permission_to_read_external_storage), Toast.LENGTH_SHORT).show();
    }
}
