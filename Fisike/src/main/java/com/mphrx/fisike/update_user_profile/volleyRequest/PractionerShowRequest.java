package com.mphrx.fisike.update_user_profile.volleyRequest;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.network.response.LoginResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.TextPattern;
import org.json.JSONObject;

/**
 * Created by Neha on 23-12-2016.
 */
public class PractionerShowRequest extends BaseObjectRequest {

    long mTransactionId;
    String userId;

    public PractionerShowRequest(long transactionId, String userId) {
        mTransactionId = transactionId;
        this.userId = userId;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getPhysicianDetailUrl() + (userId == null ? SettingManager.getInstance().getUserMO().getPhysicianId() : TextPattern.getUserId(userId));
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, null, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new PractionerShowResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new PractionerShowResponse(response, mTransactionId));
    }
}
