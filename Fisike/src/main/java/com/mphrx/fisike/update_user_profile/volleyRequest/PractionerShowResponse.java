package com.mphrx.fisike.update_user_profile.volleyRequest;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.PhysicianDBAdapter;
import com.mphrx.fisike.persistence.PractitionerDBAdapter;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import appointment.model.practitionermodel.Practitioner;

/**
 * Created by Neha on 23-12-2016.
 */
public class PractionerShowResponse extends BaseResponse {
    PractitionerMO practitionerMO;

    public PractionerShowResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        try {
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.toString(), PractitionerMO.class);
            if (jsonToObjectMapper instanceof PractitionerMO)
                practitionerMO = (PractitionerMO) jsonToObjectMapper;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PractionerShowResponse(Exception exception, long transactionId) {
        super(exception, transactionId);

    }

    public PractitionerMO getPractitioneMOFromResponse() {
        return practitionerMO;
    }
}
