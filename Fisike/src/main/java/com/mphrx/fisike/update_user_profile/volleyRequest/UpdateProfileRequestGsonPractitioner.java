package com.mphrx.fisike.update_user_profile.volleyRequest;

import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;

/**
 * Created by Neha on 26-12-2016.
 */

public class UpdateProfileRequestGsonPractitioner {

    User user;
    String userType;
    PractitionerMO physician;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public PractitionerMO getPhysician() {
        return physician;
    }

    public void setPhysician(PractitionerMO physician) {
        this.physician = physician;
    }
}
