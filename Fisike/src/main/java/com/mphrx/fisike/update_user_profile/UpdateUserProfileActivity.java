package com.mphrx.fisike.update_user_profile;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DesignationEnum;
import com.mphrx.fisike.enums.ExperienceEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.enums.SpecialityEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowResponse;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.request.SpecialitiesAndDesignationsRequest;
import com.mphrx.fisike_physician.network.response.SpecialitiesAndDesignationsResponse;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Kailash Khurana on 4/5/2016.
 */
public class UpdateUserProfileActivity extends UpdateProfilePicActivity implements DatePickerDialog.OnDateSetListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private CustomFontTextView spinnerGenderHeader;
    private CustomFontTextView spinnerMaritalStatus;
    private CustomFontEditTextView edDOB;
    private CustomFontEditTextView edFirstName;
    private CustomFontEditTextView edLastName;
    private CustomFontTextView txtGender;
    private CustomFontTextView txtMaritalStatus;
    private CustomFontEditTextView edMyRole;
    private CustomFontEditTextView edSpeciality;
    private CustomFontEditTextView edYearOfExperience;
    private ViewStub layoutGenralInfo;

    private DatePickerDialog dobDialog;
    private FrameLayout frameLayout;
    private CustomFontEditTextView edAadharNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_update_user_profile, frameLayout);
        //setContentView(R.layout.activity_update_user_profile);
        findViews();
        setData();

        super.userProfileData();
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        layoutGenralInfo = (ViewStub) findViewById(R.id.layoutGenralInfo);
        if (BuildConfig.isPatientApp) {
            layoutGenralInfo.setLayoutResource(R.layout.update_user_genral_profile);
        } else {
            layoutGenralInfo.setLayoutResource(R.layout.activity_update_user_physician_profile);
        }
        layoutGenralInfo.inflate();

        edFirstName = (CustomFontEditTextView) findViewById(R.id.edFirstName);
        edLastName = (CustomFontEditTextView) findViewById(R.id.edLastName);

        if (BuildConfig.isPatientApp) {
            spinnerGenderHeader = (CustomFontTextView) findViewById(R.id.spinnerGenderHeader);
            spinnerMaritalStatus = (CustomFontTextView) findViewById(R.id.spinnerMaritalStatus);
            edDOB = (CustomFontEditTextView) findViewById(R.id.eddob);
            edLastName.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        v.clearFocus();
                        Utils.hideKeyboard(UpdateUserProfileActivity.this, v);
                        //                 edDOB.requestFocus();
                    }
                    return false;
                }
            });

            edFirstName.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        v.clearFocus();
                        Utils.hideKeyboard(UpdateUserProfileActivity.this, v);
                        //                 edDOB.requestFocus();
                    }
                    return false;
                }
            });

            txtGender = (CustomFontTextView) findViewById(R.id.txtGender);
            txtMaritalStatus = (CustomFontTextView) findViewById(R.id.txtMaritalStatus);
            edAadharNumber = (CustomFontEditTextView) findViewById(R.id.edAadhar);
        } else {
            edMyRole = (CustomFontEditTextView) findViewById(R.id.edMyRole);
            edSpeciality = (CustomFontEditTextView) findViewById(R.id.edSpeciality);
            edYearOfExperience = (CustomFontEditTextView) findViewById(R.id.edYearOfExperience);
            edMyRole.getEditText().setCompoundDrawablePadding(10);
            edMyRole.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdown, 0);
            edSpeciality.getEditText().setCompoundDrawablePadding(10);
            edSpeciality.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdown, 0);
            edYearOfExperience.getEditText().setCompoundDrawablePadding(10);
            edYearOfExperience.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdown, 0);


            edLastName.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        v.clearFocus();
                        Utils.hideKeyboard(UpdateUserProfileActivity.this, v);
                        //                 edDOB.requestFocus();
                    }
                    return false;
                }
            });

            edFirstName.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        v.clearFocus();
                        Utils.hideKeyboard(UpdateUserProfileActivity.this, v);
                        //                 edDOB.requestFocus();
                    }
                    return false;
                }
            });

            showProgressDialog();
            ThreadManager.getDefaultExecutorService().submit(new SpecialitiesAndDesignationsRequest(getTransactionId()));
        }
    }


    private void setData() {
        initToolbar();

        edFirstName.setText(userMO.getFirstName());
        edFirstName.setSelection(edFirstName.getText().toString().length());
        edLastName.setText(userMO.getLastName());

        if (BuildConfig.isPatientApp) {
            setDOBDataLisner();
            edDOB.setText(Utils.getFormattedDate(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime));

            if (userMO.getGender() != null && !userMO.getGender().trim().equals("") && GenderEnum.getGenderEnumLinkedHashMap().containsKey(userMO.getGender())) {
                txtGender.setText(GenderEnum.getDisplayedValuefromCode(userMO.getGender()));
            } else {
                txtGender.setText(GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_other_key)));
            }
            String maritalStatus = userMO.getMaritalStatus();
            if (maritalStatus == null || maritalStatus.equals("") || maritalStatus.equals(getResources().getString(R.string.txt_null))) {
                txtMaritalStatus.setVisibility(View.GONE);
            } else {
                txtMaritalStatus.setText(MaritalStatusEnum.getMaritalStatusMap().get(maritalStatus).getValue());
                txtMaritalStatus.setVisibility(View.VISIBLE);
            }

            spinnerGenderHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupMenu(R.menu.menu_gender, spinnerGenderHeader, txtGender);
                }
            });
            spinnerMaritalStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupMenu(R.menu.menu_marital_status, spinnerMaritalStatus, txtMaritalStatus);
                }
            });

            if (BuildConfig.isAadharEnabled)
                edAadharNumber.setText(userMO.getAadharNumber());
            else
                edAadharNumber.setVisibility(View.GONE);

        } else {
            //setting role
            if (Utils.isValueAvailable(SharedPref.getRole()))
                edMyRole.setText(DesignationEnum.getDisplayedValuefromCode(SharedPref.getRole()));

            //setting speciality
            if (Utils.isValueAvailable(SharedPref.getSpeciality()))
                edSpeciality.setText(SpecialityEnum.getDisplayedValuefromCode(SharedPref.getSpeciality()));

            String experince = SharedPref.getExperience();

            if (experince != null && !experince.equals("")) {
                if (experince.contains("<") || experince.trim().equals("1")) {
                    experince = experince + " " + getResources().getString(R.string.year);
                } else {
                    experince = experince + " " + getResources().getString(R.string.years);
                }
            }
            if (Utils.isValueAvailable(experince))
                edYearOfExperience.setText(ExperienceEnum.getDisplayedValuefromCode(experince));

            String[] dataExp = getResources().getStringArray(R.array.year_of_experience);
            final ArrayList<String> dataArrayExp = new ArrayList<>();
            for (int i = 0; i < dataExp.length; i++) {
                dataArrayExp.add(dataExp[i]);
            }

            edYearOfExperience.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        showPopupMenu(dataArrayExp, R.id.spinnerGenderHeader, edYearOfExperience);
                    }
                    return true;
                }

            });

            edMyRole.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        Utils.hideKeyboard(UpdateUserProfileActivity.this);
                        dobDialog.show();
                    }
                }
            });
            edSpeciality.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        Utils.hideKeyboard(UpdateUserProfileActivity.this);
                        dobDialog.show();
                    }
                }
            });
            edYearOfExperience.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        Utils.hideKeyboard(UpdateUserProfileActivity.this);
                        dobDialog.show();
                    }
                }
            });

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onSpecialitiesAndDesignationsResponse(SpecialitiesAndDesignationsResponse response) {
        ArrayList<String> roles = response.getDesignations();
        ArrayList<String> speciality = response.getSpecialities();
        showSpinnerSpecialityAndRole(roles, speciality);
        dismissProgressDialog();
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateProfileResponse response) {
        if (response.getTransactionId() != super.getTransactionId())
            return;
        super.updateProfileRequest(response);
    }


    @Subscribe
    public void onPhysicianDetailResponse(PractionerShowResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        super.onPhysicianDetailResponse(response);
    }

    private void showSpinnerSpecialityAndRole(final ArrayList<String> roles, final ArrayList<String> speciality) {
        edMyRole.getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showPopupMenu(roles, R.id.spinnerGenderHeader, edMyRole);
                }
                return true;
            }

        });
        edSpeciality.getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showPopupMenu(speciality, R.id.spinnerMaritalStatus, edSpeciality);
                }
                return true;
            }

        });
    }

    public void showPopupMenu(final int menuId, View anchorView, final CustomFontTextView txtView) {
        final PopupMenu popup = new PopupMenu(UpdateUserProfileActivity.this, anchorView);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.setText(item.getTitle());
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public void showPopupMenu(ArrayList<String> menuId, int anchorViewId, final CustomFontEditTextView txtView) {

        final PopupMenu popup = new PopupMenu(UpdateUserProfileActivity.this, txtView);
        for (int i = 0; i < menuId.size(); i++) {
            popup.getMenu().add(Menu.NONE, i, Menu.NONE, menuId.get(i));
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.getEditText().setText(item.getTitle());
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    /**
     * Initialize the date picker dialog and set listner on the dob field
     */
    private void setDOBDataLisner() {
        final Calendar c = Calendar.getInstance();
        String dob = userMO.getDateOfBirth();
        if (dob == null || dob.equals("")) {
            dob = Utils.calculateMillis(System.currentTimeMillis(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New);
        }
        String[] date = dob.split(" ");
        String[] ymd = date[0].split("-");


        dobDialog = new DatePickerDialog(this, this, Integer.parseInt(ymd[0]), Integer.parseInt(ymd[1]) - 1, Integer.parseInt(ymd[2]));
        dobDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

        edDOB.setInputType(InputType.TYPE_NULL);
        edDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.hideKeyboard(UpdateUserProfileActivity.this);
                dobDialog.show();
                return false;
            }
        });
        edDOB.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(UpdateUserProfileActivity.this);
                    dobDialog.show();
                }
            }
        });
    }

    /**
     * Initilizing and handling toolbar
     */
    private void initToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.update_profile));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        edDOB.setText(DateTimeUtil.getFormattedDateWithoutUTC(((monthOfYear + 1) + "-" + dayOfMonth + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (BuildConfig.isPatientApp) {
                    updateProfileFields(edFirstName, edLastName, txtGender, edDOB, txtMaritalStatus, edAadharNumber);
                } else {
                    updatePhysicianProfileFields(edFirstName, edLastName, edMyRole, edSpeciality, edYearOfExperience);
                }
                break;
            default:
                super.onClick(view);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        UpdateUserProfileActivity.this.finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, getIntent());
        super.onBackPressed();
    }



}
