package com.mphrx.fisike.volley.wrappers;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.mphrx.fisike.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.android.volley.toolbox.HttpHeaderParser.parseCharset;
import static com.mphrx.fisike.volley.wrappers.HttpHeaderParser.parseIgnoreCacheHeaders;

/**
 * Created by Aastha on 15/01/2016.
 */

public class JsonObjectRequest extends com.android.volley.toolbox.JsonObjectRequest {

    // private Map<String, String> mParams;
    private  String url;

    public JsonObjectRequest(int method, String url, String jsonRequest,
                             Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        super(method, url, (jsonRequest == null) ? null : jsonRequest.toString(), listener,
                errorListener);

    }


    public JsonObjectRequest(String url, String jsonRequest, Response.Listener<JSONObject> listener,
                             Response.ErrorListener errorListener) {
        this(jsonRequest == null ? Method.GET : Method.POST, url, jsonRequest,
                listener, errorListener);
    }
    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        if (!shouldCache() || BuildConfig.isPhysicianApp) {
            String jsonString = null;
            JSONObject jsonResponse= null;
            try {
                jsonString = new String(response.data, parseCharset(response.headers, PROTOCOL_CHARSET));
                if (!jsonString.equals(""))
                  jsonResponse = new JSONObject(jsonString);
                else
                  jsonResponse= new JSONObject();
                jsonResponse.put("headers",new JSONObject(response.headers));
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return Response.success(jsonResponse, null);

        }
        else {
            try {
                String jsonString = new String(response.data,
                        parseCharset(response.headers, PROTOCOL_CHARSET));
                return Response.success(new JSONObject(jsonString), parseIgnoreCacheHeaders(response));

            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }
    }

}
