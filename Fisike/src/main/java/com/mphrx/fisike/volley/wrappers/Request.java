package com.mphrx.fisike.volley.wrappers;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

/**
 * Created by Aastha on 15/01/2016.
 */
public abstract class Request<T> extends com.android.volley.Request {

    public Request(int method, String url, Response.ErrorListener listener) {
        super(method, url, listener);
    }



    public String getCacheKey() {
        String key="";
        try {
            key= getUrl()+String.valueOf(getBody());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        return key;
    }


}
