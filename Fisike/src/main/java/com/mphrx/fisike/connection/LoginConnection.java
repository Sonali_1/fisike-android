package com.mphrx.fisike.connection;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.receipts.AckReceipt;
import org.jivesoftware.smackx.receipts.AckReceiptRequest;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;

import android.app.Activity;

import com.mphrx.fisike.R;
import login.activity.LoginActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.apache.commons.codec.binary.Base64;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.asmack.wrappers.NewGroupSuccessPacketProvider;
import com.mphrx.fisike.background.UploadProfilePic;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.provider.ChatIQProvider;
import com.mphrx.fisike.provider.ListIQProvider;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.AppLog;

/**
 * This background thread is responsible for authenticating with the XMPP Server
 *
 * @author kkhurana
 */
public class LoginConnection extends AsyncTask<Void, Void, Boolean> {
    public static boolean isConnecting = false;
    private ConnectionConfiguration connConfig = null;
    private XMPPConnection connection = null;
    private Context context;
    private ConnectionInfo connectionInfo;
    private ProgressDialog progressDialog;
    private String userName;
    private String password;
    private SettingMO settings;

    public LoginConnection(Context context, String userName, String password) {
        this.context = context;
        try {
            password = URLDecoder.decode(password, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        password = password.replaceAll("%20", " ");
        try {
            userName = URLDecoder.decode(userName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        userName = userName.replaceAll("%20", " ");
        this.userName = userName;
        this.password = password;

    }

    public LoginConnection(Context context) {
        this.context = context;
    }

    /**
     * This is the preExecute method
     */
    protected void onPreExecute() {

        settings = SettingManager.getInstance().getSettings();

        connectionInfo = ConnectionInfo.getInstance();

        getConnectionDetails();
        isConnecting = true;

        if (context instanceof LoginActivity) {
            // If this is coming from the loginactivity
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hang_tight_connecting_now));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    ;

    /**
     * This method updates the connection details from the SettingsMO to the connectionInfo object
     */
    private void getConnectionDetails() {
        String tempPassword;
        connectionInfo.setHost(settings.getFisikeServerHost());
        connectionInfo.setPort(settings.getFisikeServerPort());
        // Change for supporting ejabberd
        if (SettingManager.isEjabberdEnabled()) {
            connectionInfo.setService(settings.getFisikeServerIp());
        } else {
            connectionInfo.setService(settings.getFisikeResource());
        }

        if (!(context instanceof LoginActivity)) {
            try {
                UserMO userMO = SettingManager.getInstance().getUserMO();
                userName = userMO.getUsername();
                //userName=URLDecoder.decode(userName, "UTF-8");
                // password = encrypt(userMO.getPassword());
                password = userMO.getPassword();
                try {
                    password = URLDecoder.decode(password, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                password = password.replaceAll("%20", " ");


            } catch (Exception e) {
                userName = "";
                password = "";
                e.printStackTrace();
            }
        } else {
            // password = encrypt(password);
            try {
                userName = URLEncoder.encode(userName, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        connectionInfo.setUsername(userName);
        tempPassword = new String(password);
        try {
            tempPassword = URLEncoder.encode(password, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        connectionInfo.setPassword(tempPassword);
    }

    /**
     * This method does the MD5 encryption for the password -- this needs to be done as the encrypted password is stored on the XMPP Server
     *
     * @param toEnc
     * @return
     */
    private String encrypt(String toEnc) {
        try {
            MessageDigest mdEnc = MessageDigest.getInstance("MD5"); // Encryption algorithm
            mdEnc.update(toEnc.getBytes(), 0, toEnc.length());
            byte digest[] = mdEnc.digest();

            // Base64 encoder = new Base64();
            String base64 = Base64.encodeBase64String(digest);
            base64 = base64.trim(); // string

            return base64;
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return null;
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String host = connectionInfo.getHost();
        String port = connectionInfo.getPort();
        String service = connectionInfo.getService();
        String username = connectionInfo.getUsername();
        String password = connectionInfo.getPassword();

        try {
            password = URLDecoder.decode(password, "UTF-8");
            password = password.replaceAll(" ", "%20");
        } catch (UnsupportedEncodingException e1) {
        }

        if (username.equals("") || password.equals("")) {
            return false;
        }

        // Create a connection
        try {
            connConfig = new ConnectionConfiguration(host, Integer.parseInt(port), service);
            connConfig.setSASLAuthenticationEnabled(true);
            // connConfig.setDebuggerEnabled(true);
            connConfig.setReconnectionAllowed(true);

            // This is the SSL Implementation
            connConfig.setCompressionEnabled(true);
            connConfig.setSecurityMode(SecurityMode.enabled);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                connConfig.setTruststoreType("AndroidCAStore");
                connConfig.setTruststorePassword(null);
                connConfig.setTruststorePath(null);
            } else {
                connConfig.setTruststoreType("BKS");
                String path = System.getProperty("javax.net.ssl.trustStore");
                if (path == null)
                    path = System.getProperty("java.home") + File.separator + "etc" + File.separator + "security" + File.separator + "cacerts.bks";
                connConfig.setTruststorePath(path);
            }

            connection = new XMPPConnection(connConfig);

            // SSLContext sc = SSLContext.getInstance("TLS");
            // sc.init(null, null, new SecureRandom());
            // connConfig.setCustomSSLContext(sc);

            AppLog.d("Fisike", "com.mphrx.fisike.connection.doInBackground | connecting to host " + connection.getHost());
            connection.connect();

            AppLog.d("Fisike", "com.mphrx.fisike.connection.doInBackground | connected to host " + connection.getHost());
        } catch (XMPPException ex) {
            AppLog.showError("Fisike", "com.mphrx.fisike.connection.doInBackground | error " + ex.getMessage());
            return false;
        }

        if (connection == null) {
            return false;
        }

        // After the connection is successful, we are setting the flag to enable delivery reciepts
        DeliveryReceiptManager.getInstanceFor(connection).setAutoReceiptsEnabled(true);

        // register providers with the XMPP server
        registerProviders(ProviderManager.getInstance());

        try {
            if (!connection.isConnected()) {
                return false;
            }
            connection.login(username, encrypt(password));

            AppLog.d("Fisike", "com.mphrx.fisike.connection.doInBackground | Logged in as " + connection.getUser());

            // Set the status to available
            Presence presence = new Presence(Presence.Type.available);
            connection.sendPacket(presence);

            connectionInfo.setXmppConnection(connection);

        } catch (XMPPException ex) {
            AppLog.d("Fisike", "com.mphrx.fisike.connection.doInBackground | Failed to login " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } catch (Exception e) {
            AppLog.d("Fisike", "com.mphrx.fisike.connection.doInBackground | Failed to login " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        isConnecting = false;

        AppLog.d("Fisike", "com.mphrx.fisike.connection.onPostExecute : " + isConnecting);

        // Only do the following if the user is successfully logged in
        if (result) {
            if (context instanceof LoginActivity) {
                try {
                    saveUserMO();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // If this is from the messenger service then call the post successful login
            if (context instanceof MessengerService) {
                ((MessengerService) context).postSuccessfulLogin();
            }
        }

        if (context instanceof LoginActivity) {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                    int connectionState = DefaultConnection.STATE_NOT_CONNECTED;
                    if (connection == null || !connection.isConnected()) {
                        connectionState = DefaultConnection.STATE_NOT_CONNECTED;
                    } else if (!connection.isAuthenticated()) {
                        connectionState = DefaultConnection.STATE_NOT_AUTHENTICATED;
                    } else {
                        connectionState = DefaultConnection.STATE_AUTHENTICATED;
                    }
                    ((LoginActivity) context).connectionPostExecute(connectionState);
                }
            });
        }

        if (!result) {
            return;
        }

        if (context instanceof LoginActivity) {
            try {
                // savingUserMO();
                // context.startActivity(new Intent(context, ListPersonsActivity.class));
                UserMO userMo = SettingManager.getInstance().getUserMO();
                Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());

                if (bitmap != null && userMo.getProfilePicUpdateStatus() != null && !userMo.getProfilePicUpdateStatus()) {
                    new UploadProfilePic(context, bitmap, true, false).execute();
                }
                new UserProfilePic(context).execute();
                context.startActivity(new Intent(context, HomeActivity.class).putExtra(VariableConstants.IS_TO_SHOW_PIN, true).putExtra(VariableConstants.IS_TO_SET_PASSWORD, true));

                // ((Activity) context).overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (context instanceof HomeActivity) {
//            try {
//                ((ListPersonsActivity) context).addContacts();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }

        Intent intent = new Intent(VariableConstants.CONNECTION_ESTABLISH_TASK);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        super.onPostExecute(result);
    }

    /**
     * This method saves the userMO
     */
    private void saveUserMO() {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (userMO == null) {
            userMO = new UserMO();
        }
        String tempPassword = new String(password);

        try {
            tempPassword = URLEncoder.encode(password, "UTF-8");
            tempPassword.replaceAll(" ", "%20");

        } catch (UnsupportedEncodingException e1) {
        }
        userMO.setPassword(tempPassword);
        userMO.setUsername(userName);
        userMO.setSalutation(TextConstants.YES);
//        long userKey = (UserMO.KEY + "-" + userMO.getUserName()).hashCode();
        String userKey = (SharedPref.getUsername());
        if (userKey != null) {
            userMO.setPersistenceKey(Long.parseLong(userKey));
        } else {
            SharedPref.setUserName(userMO.getId());
            userMO.setPersistenceKey(Long.parseLong(SharedPref.getUsername()));
        }
        userMO.setChatConversationKeyList(new ArrayList<String>());

        try {
            SettingManager.getInstance().updateUserMO(userMO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method registers XMPP providers with the XMPP Server
     *
     * @param pm Removed unnecessary extensions
     */
    @SuppressWarnings("deprecation")
    private void registerProviders(ProviderManager pm) {

        try {
            pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            AppLog.showWarn("TestClient", "Can't load class for org.jivesoftware.smackx.packet.Time");
        }

        // Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

        // pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

        // pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());

        groupChatProviders(pm);

        // Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

        pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());

        pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());

        // Offline Message Indicator
        pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());

        // group creation Indicator
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new NewGroupSuccessPacketProvider());

        try {
            pm.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
            pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            pm.addExtensionProvider(AckReceipt.ELEMENT, AckReceipt.NAMESPACE, new AckReceipt.Provider());
            pm.addExtensionProvider(AckReceiptRequest.ELEMENT, new AckReceiptRequest().getNamespace(), new AckReceiptRequest.Provider());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // pm.addExtensionProvider(AttachmentTag.ELEMENT, AttachmentTag.NAMESPACE, new AttachmentTag.Provider());

        // Getting chat
        pm.addIQProvider("chat", "urn:xmpp:archive", new ChatIQProvider());
        // Getting time
        pm.addIQProvider("list", "urn:xmpp:archive", new ListIQProvider());

        ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection);

        if (sdm == null) {
            sdm = new ServiceDiscoveryManager(connection);
        }

        sdm.addFeature("http://jabber.org/protocol/disco#info");
        sdm.addFeature("http://jabber.org/protocol/disco#item");
        sdm.addFeature("jabber:iq:privacy");
        sdm.addFeature("urn:xmpp:archive");
    }

    private void groupChatProviders(ProviderManager pm) {
        // spool packets
        pm.addIQProvider("query", "jabber:iq:private", new com.mphrx.fisike.asmack.wrappers.SpoolSuccessProvider());
        // Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());
        // Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
        // Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
        // Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
        // MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());
        // MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());
        // MUC Owner
        // pm.addIQProvider("query","http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());
        // Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());
        pm.addExtensionProvider("delay", "urn:xmpp:delay", new DelayInformationProvider());
        // Version
        try {
            pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) {
            System.err.println("Can't load class for org.jivesoftware.smackx.packet.Version");
        }
    }
}