package com.mphrx.fisike.connection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.AgreementsActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GetUserDetailsGsonRequest;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.persistence.AgreementFileDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class is the thread responsible to validate and get user details Completes the following -- authentication -- validation and updation of user
 * details
 *
 * @author kkhurana
 */
public class UserDetailsBackground extends AsyncTask<Void, Void, String> {

    private static final int SHOW_AGREEMENTS = 100;
    public static boolean isUserDetailCall;
    private final MessengerService messengerService;

    private Context context;
    private ProgressDialog progressDialog;

    private UserMO userMO;
    private GetUserDetailsGsonRequest request;
    private String deviceUID;
    private boolean isHippaUpdate;
    private String indexes = "";
    private boolean isPending;

    /**
     * Constructor
     * @param context
     * @param messengerService
     * @param isPending
     */
    public UserDetailsBackground(Context context, MessengerService messengerService, boolean isPending) {
        this.context = context;
        this.deviceUID = SharedPref.getDeviceUid();
        this.isPending = isPending;
        this.messengerService = messengerService;
    }

    protected void onPreExecute() {
        isUserDetailCall = true;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            userMO = getUserDetails();

            // we need to update config parameters in Db as well
        } catch (Exception e) {
            e.printStackTrace();
            // ErrorMessage localErrorMessage = new ErrorMessage((Activity) context, R.id.layoutParent);
            if (e.getMessage().equalsIgnoreCase(TextConstants.SERVICE_UNAVAILABLE)) {
                return TextConstants.SERVICE_UNAVAILABLE;
            } else if (e.getMessage().equalsIgnoreCase(TextConstants.INVALID_LOGIN)) {
                return TextConstants.INVALID_LOGIN;
            } else if (e.getMessage().equalsIgnoreCase(TextConstants.UNEXPECTED_ERROR)) {
                return TextConstants.UNEXPECTED_ERROR;
            }
            return TextConstants.UNEXPECTED_ERROR;
        }
        if (userMO == null) {
            return TextConstants.UNEXPECTED_ERROR;
        }
        return TextConstants.SUCESSFULL_USERDETAILS_CALL;
    }

    /**
     * This gets the result from the doInBackground call
     */
    @Override
    protected void onPostExecute(String result) {
        isUserDetailCall = false;
        Utils.changeAppLanguage(context, userMO);
        //clean up---removed invalid login as it is handled automatically from api manager
            if (userMO == null) {
                if (context instanceof MyApplication) {
                    messengerService.clearProfileUpdatesTimer();
                    messengerService.startGetProfileUpdatesTimer();
                }
                return;
            } else if (context instanceof MyApplication) {
                try {
                    SettingManager.getInstance().updateUserMO(userMO);
                    if(!Utils.isAppbackground(context) && userMO.getAgreements()!=null && userMO.getAgreements().size() >0) {
                        startAgreementActivity(userMO.getAgreements(),isPending);
                            SharedPref.setIsAgreementsUpdated(false);
                            //clearing and resetting timer for getuserdetails
                            messengerService.clearProfileUpdatesTimer();
                            messengerService.startGetProfileUpdatesTimer();
                    }
                    else {
                        //When messenger service binds, this flag is checked.The flag is true if a call to getusedetails is pending when user will launch the app or app .
                           SharedPref.setIsAgreementsUpdated(true);
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isToLaunchAgreementActivity(ArrayList<AgreementModel> agreements) {
        for(int i=0;i<agreements.size();i++)
        if(!agreements.get(i).getLastUpdated().equals
                (AgreementFileDBAdapter.getInstance(context).fetchUpatedDate(agreements.get(i).getAgreementId())))
        {
            isHippaUpdate = true;
            indexes = indexes+i+","; //indexes of updated agreements MNV-8030(Aastha)
        }
        return isHippaUpdate;
    }
    private void showError(String textMessage) {
        Toast.makeText(context, textMessage, Toast.LENGTH_LONG).show();
    }


    public void showUserAlert(final UserMO userMO) {
        SharedPref.setIsAgreementsUpdated(false);
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getResources().getString(R.string.language_no_info));
                alertDialog.setMessage(context.getResources().getString(R.string.sorry_logout));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.contact), new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        messengerService.logoutUser(context);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, MyApplication.getAppContext().getResources().getString(R.string.cancel), new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private boolean startAgreementActivity(ArrayList<AgreementModel> agreements, boolean isPending) {
        SharedPref.setIsAgreementsUpdated(false);
        boolean hasToLaunch = isToLaunchAgreementActivity(agreements);
        if(hasToLaunch && userMO.getSalutation().equals(TextConstants.YES)){
            if(!userMO.isCorrectAgreementStatus())
                showUserAlert(userMO);
            else {
                Intent agreementsIntent = new Intent(context, AgreementsActivity.class);
                agreementsIntent.putExtra(TextConstants.FROM_LOGIN, false);
                agreementsIntent.putExtra(TextConstants.INDEXES, indexes);
                agreementsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(agreementsIntent);
            }
        }
        return hasToLaunch;
    }

    /**
     * This method launches the home screen
     *
     * @param result
     */
    private void launchHomeScreen(String result) {
        UserMO userMO;
        try {
            SettingManager settingManager = SettingManager.getInstance();
            userMO = settingManager.getUserMO();
            userMO.setSalutation(TextConstants.NO);
            settingManager.updateUserMO(userMO);

            SettingManager settingManager1 = SettingManager.getInstance();
            userMO = settingManager1.getUserMO();
            ConnectionInfo connectionInfo = ConnectionInfo.getInstance();
            XMPPConnection connection = connectionInfo.getXmppConnection();

            if (connection != null) {
                if (!connection.isConnected() && !connection.isAuthenticated()) {
                    connection.disconnect();
                    connectionInfo.setXmppConnection(null);
                }
            }

            settingManager.updateUserMO(userMO);
            SharedPref.setAccessToken(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This makes the API call to get User Details -- authentication on Fisike Web server
     *
     * @return
     * @throws Exception
     */
    private UserMO getUserDetails() throws Exception {
        String jsonString;
        try {
            String apiUrl = MphRxUrl.getUserDetailsUrl();
            request = new GetUserDetailsGsonRequest();
            request.setDeviceUID(deviceUID);
            request.setUserLanguage(Utils.getUserSelectedLanguage(context));
            String authToken = SharedPref.getAccessToken();
            String json = new Gson().toJson(request);
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();
            jsonString = APIManager.getInstance().executeHttpPost(apiUrl, jsonObj, authToken, context);
        } catch (Exception e) {
            jsonString = null;
            e.printStackTrace();
        }
        if (null != jsonString) {

            if (jsonString.contains("503 Service Unavailable")) {
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString.toString(), UserMO.class);
            UserMO newUserMo = (UserMO) jsonToObjectMapper;
            UserMO existingUserMo = SettingManager.getInstance().getUserMO();

            JSONObject jsonObject = null;
            try {
                if (jsonString != null) {
                    jsonObject = new JSONObject(jsonString.toString());
                }
                if (jsonObject.has("dependentPatientIds")) {
                    JSONArray jArray = jsonObject.getJSONArray("dependentPatientIds");
                    newUserMo.setDependentPatientsIds(jArray != null ? jArray.toString() : "[]");
                    AppLog.showInfo("tree", jArray.toString());
                } else {
                    newUserMo.setDependentPatientsIds("[]");
                }
            } catch (Exception ex) {
                newUserMo.setDependentPatientsIds("[]");
            }
            existingUserMo.setFirstName(newUserMo.getFirstName());
            existingUserMo.setAlternateContact(newUserMo.getAlternateContact());
            existingUserMo.setDateOfBirth(Utils.getFormattedDate(newUserMo.getDateOfBirth(),
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z,
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));
            existingUserMo.setGender(newUserMo.getGender());
            existingUserMo.setLastName(newUserMo.getLastName());
            existingUserMo.setWeight(newUserMo.getWeight());
            existingUserMo.setHeight(newUserMo.getHeight());
            existingUserMo.setRealName(newUserMo.getRealName());
            existingUserMo.setUserGroups(newUserMo.getUserGroups());
            existingUserMo.setUserSettings(newUserMo.getUserSettings());
            existingUserMo.setCorrectAgreementMessage(newUserMo.getCorrectAgreementMessage());
            existingUserMo.setAgreements(null);
            existingUserMo.setAgreements(newUserMo.getAgreements());
            existingUserMo.setCorrectAgreementStatus(newUserMo.isCorrectAgreementStatus());
            existingUserMo.setDependentPatientsIds(null);
            existingUserMo.setDependentPatientsIds(newUserMo.getDependentPatientsIds());
            existingUserMo.setUserType(newUserMo.getUserType());
            existingUserMo.setDeviceActivationDate(newUserMo.getDeviceActivationDate());
            existingUserMo.setRegistrationStatus(newUserMo.isRegistrationStatus());
            return existingUserMo;
        }

        // If it does not return before this, then it is an unexpected error
        return null;
    }
}
