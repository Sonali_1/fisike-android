package com.mphrx.fisike.connection;

import java.util.UUID;

import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.provider.SearchRecentSuggestions;
import android.telephony.TelephonyManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.PresenceMapEnum;
import com.mphrx.fisike.gson.request.DeregisterApiGson;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.provider.MySuggestionProvider;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

public class DeRegisterUserDeviceBG extends AsyncTask<Void, Void, Boolean> {
    private Context context;
    private MessengerService messengerService;
    private boolean isPinScreen;
    private boolean isWipeData = false;

    private static final String DEREGISTER_BASE_URL = "/deRegisterDevice";

    public DeRegisterUserDeviceBG(Context context, MessengerService messengerService, boolean isPinScreen, int retryAttempts) {
        this.context = context;
        this.messengerService = messengerService;
        this.isPinScreen = isPinScreen;
    }

    public DeRegisterUserDeviceBG(Context context, MessengerService messengerService, boolean isPinScreen, int retryAttempts, boolean isWipeData) {
        this.context = context;
        this.messengerService = messengerService;
        this.isPinScreen = isPinScreen;
        this.isWipeData = isWipeData;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            String url = APIManager.createMinervaBaseUrl() + APIManager.API_MOBILE + DEREGISTER_BASE_URL;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("deviceUID", SharedPref.getDeviceUid());

            APIManager jsonManager = APIManager.getInstance();
            String jsonResponse = jsonManager.executeHttpPost(url, "", jsonObject, context);

            // {"success":[{"code":0,"text":"logout successfully"}]}

            if (null != jsonResponse) {
                if (jsonResponse.contains("503 Service Unavailable")) {
                    return false;
                }

                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonResponse, DeregisterApiGson.class);

                if (jsonToObjectMapper instanceof DeregisterApiGson) {
                    String success = ((DeregisterApiGson) jsonToObjectMapper).getMsg();
                    if (success != null && success.equals("SUCCESSFUL")) {
                        return true;
                    }
                } else {
                    return false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        messengerService.showDialog(context);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        messengerService.dismissDialog();
        UserMO userMo = SettingManager.getInstance().getUserMO();
        if (result) {
            messengerService.logoutUser(context);
            DBAdapter.getInstance(context).truncateDB();
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(context, MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
            suggestions.clearHistory();
        } else {
            XMPPConnection connection = ConnectionInfo.getInstance().getXmppConnection();
            if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
                messengerService.changeStatus(PresenceMapEnum.getPresenceMap().get(userMo.getStatus()), userMo.getStatusMessage());
            }

            DialogUtils.showErrorDialog(context, MyApplication.getAppContext().getResources().getString(R.string.sign_out_failed), MyApplication.getAppContext().getResources().getString(R.string.nosignout_error_server), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        dialog.dismiss();
                    }
                }
            });

        }

        super.onPostExecute(result);
    }
}
