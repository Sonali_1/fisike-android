package com.mphrx.fisike.connection;

import java.io.Serializable;

import org.jivesoftware.smack.XMPPConnection;

/**
 * This is a singleton class that maintains all the information about the XMPP Connection
 *
 * @author kkhurana
 */
public class ConnectionInfo implements Serializable {
    private static final long serialVersionUID = -7923365556423643119L;
    private static ConnectionInfo connectionInfo;
    private String host;
    private String port;
    private String resource;
    private String service;
    private String username;
    private String password;
    private XMPPConnection xmppConnection;

    public static ConnectionInfo getInstance() {
        if (connectionInfo == null) {
            synchronized (ConnectionInfo.class) {
                if (connectionInfo == null) {
                    connectionInfo = new ConnectionInfo();
                }
            }
        }
        return connectionInfo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public XMPPConnection getXmppConnection() {
        return xmppConnection;
    }

    public void setXmppConnection(XMPPConnection xmppConnection) {
        this.xmppConnection = xmppConnection;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}