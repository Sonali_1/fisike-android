package com.mphrx.fisike.connection;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;

public class CustomMultiPartEntity extends MultipartEntity {
	private final ProgressListener listener;
	private static boolean isIntrupted;

	public CustomMultiPartEntity(final ProgressListener listener, boolean isIntrupted) {
		super();
		this.listener = listener;
		this.isIntrupted = isIntrupted;
	}

	public CustomMultiPartEntity(final HttpMultipartMode mode, final ProgressListener listener) {
		super(mode);
		this.listener = listener;
	}

	public CustomMultiPartEntity(HttpMultipartMode mode, final String boundary, final Charset charset, final ProgressListener listener) {
		super(mode, boundary, charset);
		this.listener = listener;
	}

	@Override
	public void writeTo(OutputStream outstream) throws IOException {
		try {
			CountingOutputStream countingOutputStream = new CountingOutputStream(outstream, this.listener);
			super.writeTo(countingOutputStream);
		} catch (IOException e) {
			outstream.flush();
			outstream.close();
			outstream = null;
			isIntrupted = true;
			e.printStackTrace();
			super.writeTo(null);
			return;
		}
	}

	public static interface ProgressListener {
		boolean transferred(long num) throws InterruptedIOException;
	}

	public static class CountingOutputStream extends FilterOutputStream {

		private final ProgressListener listener;
		private long transferred;

		public CountingOutputStream(final OutputStream out, final ProgressListener listener) {
			super(out);
			this.listener = listener;
			this.transferred = 0;
		}

		public void write(byte[] b, int off, int len) throws IOException {
			if (isIntrupted) {
				b = null;
				off = 0;
				len = 0;
				out.flush();
				out.close();
				out = null;
				return;
			}
			out.write(b, off, len);
			this.transferred += len;
			this.listener.transferred(this.transferred);
		}

		public void write(int b) throws IOException {
			if (isIntrupted) {
				out.flush();
				out.close();
				out = null;
				return;
			}
			out.write(b);
			this.transferred++;
			this.listener.transferred(this.transferred);

		}
	}

}