package com.mphrx.fisike;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.mphrx.fisike.Observer.MobileConfigObserver;
import com.mphrx.fisike.connection.ConnectionStatus;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.LaunchingScreen;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.Observable;
import java.util.Observer;

public class SplashActivity extends ConfigFetchBaseActivity implements Observer {

    private Handler handler = new Handler();
    private IconTextView imageView;
    ImageView imagePoweredBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();

        //preventing relaunching of splashActivity after killing and time passed
        if (!Utils.checkToDisplayPINScreen(SettingManager.getInstance().getUserMO()) && !Utils.checkIsToLogoutUser())
            SharedPref.setLastInteractionTime(System.currentTimeMillis());

        if (extras != null && extras.containsKey(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE) &&
                extras.getBoolean(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE, false)) {
            LaunchingScreen.getInstance().notificationFlow(this, extras);
            return;
        }

        ConnectionStatus.isConnectionAvailable = Utils.isNetworkAvailable(this);
        Utils.restrictScreenShot(this);
        setContentView(R.layout.activity_splash);
        imageView = (IconTextView) findViewById(R.id.splash_image);
        imagePoweredBy = (ImageView) findViewById(R.id.iv_mphrx_powered_by_logo);

        if (BuildConfig.isIcomoonForSplashEnabled) {

            imageView.setText(R.string.fa_fisike_icon);
            imageView.setTextSize(100f);
        } else {
            imageView.setBackgroundResource(R.drawable.splash_icon);
            imageView.setText("");
            imageView.setTextSize(0);
        }

        requestConfig();

        if (BuildConfig.isToShowPoweredByMphRxOnSplash)
            imagePoweredBy.setVisibility(View.VISIBLE);

        Utils.changeAppLanguage(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        MobileConfigObserver.getInstance().addObserver(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        MobileConfigObserver.getInstance().deleteObserver(this);
        super.onPause();
    }

    Runnable nextScreenRunnable = new Runnable() {
        @Override
        public void run() {
            if (isError && getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).
                    getString(SharedPref.LANGUAGE_SELECTED, null) == null) {
                startActivity(new Intent(SplashActivity.this, LaunchingLanguageSelectActivity.class).putExtra(LaunchingLanguageSelectActivity.IS_ERROR, isError));
                SplashActivity.this.finish();
            } else {
                LaunchingScreen.getInstance().launchDesion(SplashActivity.this);
            }
            SplashActivity.this.finish();
        }
    };

    // handler redirection and launch decision based on UserMO
    private void redirectToLaunchScreenAndTakeLaunchDecision() {
        handler.postDelayed(nextScreenRunnable, VariableConstants.SPLASH_TIME_OUT * 1000);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof MobileConfigObserver) {
            redirectToLaunchScreenAndTakeLaunchDecision();
        }
    }
}