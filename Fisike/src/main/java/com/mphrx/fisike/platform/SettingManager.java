package com.mphrx.fisike.platform;

import android.content.SharedPreferences;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.AuthenticationGson;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.PatientDBAdapter;
import com.mphrx.fisike.persistence.SettingsDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

public class SettingManager {

    private static SettingManager manager;
    private static SettingMO settingMO;
    // Internally keep a UserMO in memory
    private static UserMO userMO;
    private static PatientMO patientMO;
    private static AuthenticationGson authenticationGson;
    private long key = SettingMO.SETTING_KEY;
    private DBAdapter dbAdapter;
    private SettingsDBAdapter settingsDBAdapter;

    protected SettingManager() {

    }

    /**
     * Static method to get boolean flag to determine if ejabberd is enabled or not
     *
     * @return boolean
     */
    public static boolean isEjabberdEnabled() {
        if (settingMO != null) {
            if (settingMO.getXmppServerType().equalsIgnoreCase("openfire")) {
                return false;
            } else {
                return true;
            }
        } else {
            return isEjabberdEnabledByDefault();
        }
    }

    /**
     * Checks if ejabberd is enabled by default
     *
     * @return boolean (false if openfire)
     */
    private static boolean isEjabberdEnabledByDefault() {
        if (DefaultConnection.XMPP_SERVER.equalsIgnoreCase("openfire")) {
            return false;
        }
        return true;
    }

    /**
     * Singleton method to get SettingsManager
     *
     * @return
     */
    public static SettingManager getInstance() {
        if (null == manager) {
            manager = new SettingManager();
        }
        return manager;
    }

    /**
     * This method returns the settings object
     *
     * @return
     */
    public SettingMO getSettings() {
        if (null == settingMO) {
            settingsDBAdapter = SettingsDBAdapter.getInstance(MyApplication.getAppContext());
            try {
                settingMO = settingsDBAdapter.fetchSettingMO(key + "");
            } catch (Exception e) {
                settingMO = null;
            }
            if (null == settingMO) {
                settingMO = getDefaultSettingMO();
            }
        }
        return settingMO;

    }

    public PatientMO getPatientMo(String patid) {

        PatientDBAdapter dbAdapter = PatientDBAdapter.getInstance(MyApplication.getAppContext());

        try {
            patientMO = dbAdapter.fetchPatientInfo(patid);
        } catch (Exception e) {
            patientMO = null;
        }

        return patientMO;

    }

    public void savePatientMo(PatientMO patientMO) {
        PatientDBAdapter dbAdapter = PatientDBAdapter.getInstance(MyApplication.getAppContext());
        try {
            dbAdapter.createOrUpdatePatientMo(patientMO);
            this.patientMO = patientMO;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * This method returns the current UserMO on the device
     *
     * @return
     */
    public UserMO getUserMO() {
        try {

            // if the userMO is null in memory, then fetch it from the DB

            String loggedInUsername = SharedPref.getUsername();

            if (!Utils.isValueAvailable(loggedInUsername)) {
                return null;
            }
            String persistenceKey = SharedPref.getUsername();

            if (null == userMO) {
                dbAdapter = DBAdapter.getInstance(MyApplication.getAppContext());

                try {
                    userMO = UserDBAdapter.getInstance(MyApplication.getAppContext()).fetchUserMO(persistenceKey);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception("UserMO is null");
                }
            } else if (SharedPref.getIsUserInfoUpdated()) {
                userMO = UserDBAdapter.getInstance(MyApplication.getAppContext()).fetchUserMO(persistenceKey);
                SharedPref.setIsUserInfoUpdated(false);
            }

            return userMO;
        } catch (Exception e) {
            return userMO;
        }
    }

    /**
     * This method updates the UserMO in persistence
     *
     * @param newUserMO
     * @throws Exception
     */
    public void updateUserMO(UserMO newUserMO) throws Exception {
        if (newUserMO == null)
            return;
        dbAdapter = DBAdapter.getInstance(MyApplication.getAppContext());

        String loggedInUsername = SharedPref.getUsername();
        if (newUserMO.getPersistenceKey() == 0 && loggedInUsername != null)
            newUserMO.setPersistenceKey(Long.parseLong(loggedInUsername));
        else if (loggedInUsername == null && newUserMO.getPersistenceKey() == 0) {
            long userKey = SharedPref.setUserName(newUserMO.getId());
            newUserMO.setPersistenceKey(userKey);
        }
        UserDBAdapter.getInstance(MyApplication.getAppContext()).createOrUpdateUserMO(newUserMO);
        // Update the userMO in memory
        userMO = newUserMO;
    }


    public void updateLocalUserMo(UserMO newUserMo) {
        if (newUserMo != null)
            userMO = newUserMo;
    }


    public void remove(UserMO newUserMO) throws Exception {
        dbAdapter = DBAdapter.getInstance(MyApplication.getAppContext());

        String loggedInUsername = SharedPref.getUsername();
        if (newUserMO.getPersistenceKey() == 0 && loggedInUsername != null)
            newUserMO.setPersistenceKey(SharedPref.setUserName(Long.parseLong(loggedInUsername)));

        UserDBAdapter.getInstance(MyApplication.getAppContext()).removeUser(newUserMO);
        userMO = null;
        // Update the userMO in memory
    }

    /**
     * This method updates the UserMO in persistence
     *
     * @throws Exception
     */
    public void updateUserMO() throws Exception {
        dbAdapter = DBAdapter.getInstance(MyApplication.getAppContext());

        String loggedInUsername = SharedPref.getUsername();

        if (loggedInUsername == null)
            throw new Exception("UserMO is null");

        dbAdapter.truncateUserMo();

        // Update the userMO in memory
        userMO = null;
    }

    public void saveSettings(SettingMO setMO) throws Exception {
        setMO.setPersistenceKey(key);
        settingsDBAdapter.createOrUpdateSettingMO(setMO);
    }

    private SettingMO getDefaultSettingMO() {
        SettingMO setMo = null;
        try {
            setMo = new SettingMO();

            setMo.setServerIP(DefaultConnection.SERVER);
            setMo.setServerPort(DefaultConnection.PORT);
            setMo.setUseHTTPS(true);
            setMo.setAutoLogout(true);
            setMo.setFisikeServerHost(DefaultConnection.FISIKE_HOST);
            setMo.setFisikeServerIp(DefaultConnection.FISIKE_SERVER);
            setMo.setFisikeServerPort(DefaultConnection.FISIKE_PORT);
            setMo.setFisikeResource(DefaultConnection.FISIKE_SERVICE);
            setMo.setFisikeUseHTTPS(true);
            setMo.setAutoLogoutTimeInterval(DefaultConnection.AUTO_SCREEN_TIME_OUT);
            setMo.setNoOfMsgsToStoreLocally(DefaultConnection.MESSAGE_SIZE);

            // Added xmpp server option
            setMo.setXmppServerType(DefaultConnection.XMPP_SERVER);

            settingsDBAdapter.createOrUpdateSettingMO(setMo);

            settingMO = setMo;
            return setMo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
