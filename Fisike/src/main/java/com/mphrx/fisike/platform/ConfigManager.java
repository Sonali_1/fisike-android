package com.mphrx.fisike.platform;

import android.support.annotation.Nullable;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.persistence.ConfigDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;

import java.util.LinkedHashMap;

public class ConfigManager {

    private static ConfigManager configManager;
    private static ConfigMO configMO;
    private long configKey = ConfigMO.CONFIG_KEY;
    private ConfigDBAdapter dbAdapter;

    protected ConfigManager() {
    }

    public static ConfigManager getInstance() {
        if (configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }

    public ConfigMO getConfig() {
        dbAdapter = ConfigDBAdapter.getInstance(MyApplication.getAppContext());
            try {
                configMO = dbAdapter.fetchConfigMO(configKey + "");
            } catch (Exception e) {
                configMO = null;
            }
            if (null == configMO) {
                configMO = getDefaultConfigMO();
            }
        return configMO;
    }

    @Nullable
    private ConfigMO getDefaultConfigMO() {
        ConfigMO conMO = null;
        try {
            conMO = new ConfigMO();
            conMO.setPersistenceKey(ConfigMO.CONFIG_KEY);
            conMO.setChatEnabled(DefaultConnection.CONFIG_IS_CHAT_ENABLE);
            conMO.setShowPin(DefaultConnection.CONFIG_IS_SHOW_PIN);
            conMO.setCanChangePinStatus(DefaultConnection.CAN_EDIT_PIN_STATUS);
            conMO.setNoOfDaysForAlternateContact(TextConstants.ALTER_CONTACT_DAYS_DIFF);
            conMO.setPinLockTime(TextConstants.PIN_LOCK_TIME);
            conMO.setAutoLockMinute(0);
            LinkedHashMap<String, String> stringStringLinkedHashMap = new LinkedHashMap<>();
            stringStringLinkedHashMap.put(TextConstants.DEFALUT_LANGUAGE_TITLE,TextConstants.DEFALUT_LANGUAGE_KEY);
            conMO.setLanguageList(stringStringLinkedHashMap);
            dbAdapter.createOrUpdateConfigMO(conMO);
            configMO = conMO;
            return conMO;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ConfigMO updateConfig(ConfigMO conMO) {
        try {
            dbAdapter.createOrUpdateConfigMO(conMO);
            ConfigMO fetchConfigMO = dbAdapter.fetchConfigMO(ConfigMO.CONFIG_KEY + "");
            configMO = conMO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return configMO;
    }

}
