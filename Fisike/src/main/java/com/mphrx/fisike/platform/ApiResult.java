package com.mphrx.fisike.platform;

/**
 * Created by Kailash Khurana on 6/13/2016.
 */
public class ApiResult {

    private String jsonString;
    private int statusCode;

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
