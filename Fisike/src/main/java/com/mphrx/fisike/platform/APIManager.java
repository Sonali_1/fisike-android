package com.mphrx.fisike.platform;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.google.gson.JsonObject;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.CommonApiResponse;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;

public class APIManager {

    private static APIManager manager;
    private static final String FISIKE = "/fisike";
    private static final String COLON = ":";
    private static final String MINERVA = "/minerva";

    public static final String API_USER = "/userApi";
    public static final String API_MOBILE = "/mobileApp";
    public static String headerVersion = "V1";
    private String deviceToken;
    private String patientFaqUrl;
    private String physicianfaqUrl;


    protected APIManager() {
    }

    public static APIManager getInstance() {
        if (manager == null) {
            synchronized (APIManager.class) {
                if (manager == null)
                    manager = new APIManager();
            }

        }
        return manager;
    }

    /**
     * This method executes a HTTP GET Request
     *
     * @param shareUrlStr
     * @return
     */
    public String executeUrlResponse(String shareUrlStr) {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (userMO != null && userMO.getSalutation().equals(TextConstants.YES)) {
            return getUrlResponse(shareUrlStr);
        }
        return null;
    }

    /**
     * This method executes a HTTP GET Request
     *
     * @param shareUrlStr
     * @return
     */
    public String getUrlResponse(String shareUrlStr) {
        try {
            String jsonString = executeHttpGet(shareUrlStr);
            isUserAuthentic(jsonString);
            return jsonString;
        } catch (Exception ex) {
            ex.printStackTrace();
            AppLog.d("LOG_MANAGER", "Getting Error during share report call : " + ex.getMessage());
        }
        return null;
    }

    /**
     * This method executes a HTTP POST Request
     *
     * @param shareUrlStr
     * @return
     */
    public String executeUrlPostResponse(String shareUrlStr) {
        try {
            UserMO userMO = SettingManager.getInstance().getUserMO();
            if (userMO != null && userMO.getSalutation().equals(TextConstants.YES)) {
                String jsonString = executeHttpPost(shareUrlStr);
                isUserAuthentic(jsonString);
                return jsonString;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            AppLog.d("LOG_MANAGER", "Getting Error during share report call : " + ex.getMessage());
        }
        return null;
    }

    /**
     * This method check if the user is authentic
     *
     * @param jsonString
     * @return boolean
     * @throws Exception
     */
    private boolean isUserAuthentic(String jsonString) {
        if (null != jsonString && !jsonString.equals("")) {
            try {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, CommonApiResponse.class);
                if (jsonToObjectMapper instanceof CommonApiResponse) {
                    String error = ((CommonApiResponse) jsonToObjectMapper).getError();
                    if (error != null && error.equals("User name or password is incorrect.")) {
                        Context context = MyApplication.getAppContext();

                        AppLog.d("LoginError", "ApiManager----Invalid Login---Set Salutation no");
                        SettingManager settingManager = SettingManager.getInstance();
                        UserMO userMO = settingManager.getUserMO();
                        userMO.setSalutation(TextConstants.NO);
                        //settingManager.updateUserMO(userMO);
                        UserDBAdapter.getInstance(context).updateSalutation(userMO.getPersistenceKey(), userMO.getSalutation());
                        // SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                        // SharedPreferences.Editor editor = settings.edit();
                        // editor.remove(UserMO.KEY);
                        // editor.commit();

                        if (!Utils.checkTopActivity(context, TextConstants.OTP_SCREEN)) {
                            AppLog.d("LoginError", "ApiManager----Invalid Login");
                            context.startActivity(new Intent(Intent.ACTION_MAIN).setClass(context, com.mphrx.fisike_physician.activity.OtpActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra(VariableConstants.REVOKE_USER, true));

                            ((Activity) context).finish();
                        }
                    }
                }

            } catch (Exception e) {
                return true;
            }
        }
        return true;
    }

    /**
     * This method executes a HTTP GET request
     *
     * @param url
     * @return
     */
    public String executeHttpGet(String url) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {

            url = url.replaceAll("#", "%23");

            HttpGet httpGet = new HttpGet(url);
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpGet);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method executes a HTTP GET request
     *
     * @param url
     * @return
     */
    public String executeHttpGet(String url, String authToken) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {

            url = url.replaceAll("#", "%23");

            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("x-auth-token", authToken);
            httpGet.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig")||url.contains("moSignUp/verifyOneTimePassword") || url.contains("moUser/getUserDetails"))
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpGet.setHeader("api-info", getHeader(MyApplication.getAppContext(), headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpGet);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method executes a HTTP Post request
     *
     * @param url
     * @return
     */
    public String executeHttpPost(String url) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {

            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");

            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpPost);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // make sure the id is part of url
    public boolean executeHttpDelete(String url, String authToken) {
        if (url == null || "".equals(url))
            return false;
        BufferedReader in = null;
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            url = url.replaceAll("#", "%23");
            // StringEntity se = new StringEntity(gsonObject.toString());
            httpDelete.setHeader("x-auth-token", authToken);
            // ((HttpResponse) httpDelete).setEntity(se);
            httpDelete.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if (url.contains("/MoObservation/getVitals") || url.contains("moObservation/saveList")
                    || url.contains("selectList/getVitalConfig") )
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpDelete.setHeader("api-info", getHeader(MyApplication.getAppContext(), headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpDelete);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == VariableConstants.DELETE_API_SUCCESSFUL_RESPONSE) {
                return true;
            }

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * This method executes a HTTP Post request this method is used for token generation, this needs to be moved to gson json based implementation
     *
     * @param url
     * @return
     */
    public String executeHttpPost(String url, String authToken, JSONObject jsonObject, Context context) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {

            if (authToken.equals("")) {
                authToken = SharedPref.getAccessToken();
            }
            headerVersion = "V1";

            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            // se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            if (authToken != null) {
                httpPost.setHeader("x-auth-token", authToken);
            }
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig")||url.contains("moSignUp/verifyOneTimePassword"))
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpPost.setHeader("api-info", getHeader(context, headerVersion));
            if (jsonObject != null) {
                StringEntity se = new StringEntity(jsonObject.toString());
                httpPost.setEntity(se);
            }
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpPost);
            int status = response.getStatusLine().getStatusCode();
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();

            if (status == 401) {
                AppLog.d(url, 401);
                launchHomeScreen(TextConstants.INVALID_LOGIN, context);
            } else if ((status + "").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                AppLog.d(url, TextConstants.BARRED_LOCATION_ERROR_CODE);
                Utils.launchLocationChangeActivity(context);
            } else if (status != 200) {
                return null;
            }

            return page;

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method launches the home screen
     *
     * @param result
     */
    public void launchHomeScreen(String result, Context context) {

        UserMO userMO;
        try {
            SettingManager settingManager = SettingManager.getInstance();
            userMO = settingManager.getUserMO();
            userMO.setSalutation(TextConstants.NO);
            //settingManager.updateUserMO(userMO);
            UserDBAdapter.getInstance(context).updateSalutation(userMO.getPersistenceKey(), userMO.getSalutation());

            SettingManager settingManager1 = SettingManager.getInstance();
            userMO = settingManager1.getUserMO();
            ConnectionInfo connectionInfo = ConnectionInfo.getInstance();
            XMPPConnection connection = connectionInfo.getXmppConnection();
            if (connection != null) {
                if (!connection.isConnected() && !connection.isAuthenticated()) {
                    connection.disconnect();
                    connectionInfo.setXmppConnection(null);
                }
            }
            UserDBAdapter.getInstance(context).updateSalutation(userMO.getPersistenceKey(), userMO.getSalutation());
            SharedPreferences settings = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = settings.edit();
            if (result.equals(TextConstants.INVALID_LOGIN)) {

                if (!Utils.isAppbackground(context)) {
                    SharedPref.setUserLogout(true);
                    SharedPref.setAccessToken(null);
                } else {
                    editor.putString(VariableConstants.ERROR_MESSAGE_LOGIN, VariableConstants.REVOKE_USER);
                    editor.commit();
                }
            } else {
                if (!Utils.isAppbackground(context)) {
                    context.startActivity(new Intent(context, com.mphrx.fisike_physician.activity.OtpActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra(
                            VariableConstants.REGISTER_ERROR, true));
                    ((Activity) context).finish();
                } else {
                    editor.putString(VariableConstants.ERROR_MESSAGE_LOGIN, VariableConstants.REGISTER_ERROR);
                    editor.commit();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method executes a HTTP Post request
     *
     * @param url
     * @return
     */
    public String executeHttpPost(String url, JsonObject gsonObject, String authToken, Context context) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            httpPost.setHeader("x-auth-token", authToken);
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig")||url.contains("moSignUp/verifyOneTimePassword")
                    || url.contains("moUser/getUserDetails")) {
                headerVersion = "V2";
            } else {
                headerVersion = "V1";
            }

            httpPost.setHeader("api-info", getHeader(context, headerVersion));
            if (gsonObject != null) {
                StringEntity se = new StringEntity(gsonObject.toString());
                httpPost.setEntity(se);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            }
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpPost);


            int status = response.getStatusLine().getStatusCode();
            if (status == 401) {
                AppLog.d(url, 401);
                launchHomeScreen(TextConstants.INVALID_LOGIN, context);
            } else if ((status + "").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                AppLog.d(url, TextConstants.BARRED_LOCATION_ERROR_CODE);
                Utils.launchLocationChangeActivity(context);
            } else if (status != 200) {
                return null;
            }

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getHeader(Context context, String headerVersion) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        return headerVersion + "|" + BuildConfig.VERSION_NAME + "|" + Build.BRAND + "|" + Build.MODEL + "|" + width + "X" + height + "|" + "Android " + "|" + Build.VERSION.RELEASE + "|" + carrierName + "|" + getNetworkClass(context);
    }


    private String getNetworkClass(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return "-"; //not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                    return "4G";
                default:
                    return "Unknown";
            }
        }
        return "Unknown";
    }

    /**
     * This method executes a HTTP Post request
     *
     * @param url
     * @return
     */
    public boolean isExecuteHttpPostStatus(String url, JsonObject gsonObject, String authToken) {
        if (url == null || "".equals(url))
            return false;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(gsonObject.toString());
            httpPost.setHeader("x-auth-token", authToken);
            httpPost.setEntity(se);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpPost);

            if (response.getStatusLine().toString().contains("200")) {
                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuffer sb = new StringBuffer("");
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + NL);
                }
                in.close();
                String page = sb.toString();
                return true;
            }
            return false;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getDiagnosticOrderApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/moDiagnosticOrder/search";
        return postURL;

    }

    public String getObservationApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/moObservation/saveList";
        return postURL;
    }

    public String generateDiagnosticViewerURL(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + MINERVA + "/viewer/generateDiagnosticViewerURL";
        return postURL;
    }

    public String getDiagnosticReportSearchUrl(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + MINERVA + "/DiagnosticReport/search";
        return postURL;
    }

    /**
     * This method executes a HTTP Post request
     *
     * @param url
     * @return
     */
    public String executeHttpPut(String url, JsonObject gsonObject, String authToken, Context context) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPut httpPut = new HttpPut(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(gsonObject.toString());
            httpPut.setHeader("x-auth-token", authToken);
            httpPut.setEntity(se);
            httpPut.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if (url.contains("/MoObservation/getVitals") || url.contains("moObservation/saveList")
                    || url.contains("selectList/getVitalConfig"))
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpPut.setHeader("api-info", getHeader(MyApplication.getAppContext(), headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpPut);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            int status = response.getStatusLine().getStatusCode();
            if (status == 401) {
                AppLog.d(url, 401);
                launchHomeScreen(TextConstants.INVALID_LOGIN, context);
            } else if (status == 503)
                return TextConstants.SERVICE_UNAVAILABLE;

            else if (status != 200)
                return TextConstants.UNEXPECTED_ERROR;

            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method will uplaod the Android device token to the Fisike server
     *
     * @param token
     * @param username
     * @param password
     * @param context
     */
    public void uploadAndroidDeviceToken(String token, String username, String password, Context context) {
        try {
            PackageInfo pkg = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String osType = URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8");
            String deviceType = "android:" + URLEncoder.encode(android.os.Build.MODEL, "UTF-8");
            String appVersion = pkg.versionName;
            executeHttpGet(uploadToken(token, username, password, osType, deviceType, appVersion, context));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getOTPUrl() {
        String getAPIServlet = "signUp/sendOtpAndGetUserStatus";

        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/" + getAPIServlet;

        return postURL;
    }

    public String getSignUpUrl(Context context) {
        String getAPIServlet = "fisikeSignUp/patientSignUp";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        final String postURL = httpStr + setMO.getServerIP() + "/minerva/" + getAPIServlet;
        return postURL;
    }

    public String getAppTokenApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/api/login";
        return postURL;
    }

    public String getPractitionerApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/Practitioner/save";
        return postURL;
    }

    public String getUploadDeviceTokenUrl(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/mobileApp/uploadDeviceToken";
        return postURL;
    }

    public String getMedicationApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/Medication/findOrSave";
        return postURL;
    }

    public String getCompositeMedicationApi() {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/composite/compositeTransactions";
        return postURL;
    }

    public String getMedicationPrescriptionApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + "/minerva/MedicationOrder/saveObject";
        return postURL;
    }

    public String getMedicationPrescriptionUpdateApi(Context context, int id) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        String postURL = httpStr + setMO.getServerIP() + "/minerva/MedicationOrder/update/";
        postURL = postURL + id;
        return postURL;
    }

    private String uploadToken(String token, String username, String password, String osType, String deviceType, String appVersion, Context context)
            throws Exception {
        String getAPIServlet = "uploadDeviceToken";

        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        String deviceUUid = SharedPref.getDeviceUid();

        final String postURL = httpStr + setMO.getServerIP() + "/fisike/userApi/" + getAPIServlet + "?username="
                + username + "&password=" + password + "&token=" + token + "&deviceType=" + deviceType + "&osType=" + osType + "&appVersion="
                + appVersion + "&deviceUID=" + deviceUUid;
        return postURL;
    }


    /**
     * This method uploads the Device token to the server
     *
     * @param context
     */
    public void uploadDeviceToken(Context context) {

        // registerWithGCM(context.getApplicationContext());
    }

    /**
     * This method is called to register with the Google Cloud Messenging Application
     *
     * @param context
     */
    private void registerWithGCM(Context context) {
        // GCMRegistrar.checkDevice(context);
        // GCMRegistrar.checkManifest(context);
        // String regId = GCMRegistrar.getRegistrationId(context);
        // if (regId.equals("")) {
        // GCMRegistrar.register(context, VariableConstants.GCM_SENDER_ID);
        // } else {
        //
        // try {
        // UserMO userMO = SettingManager.getInstance().getUserMO();
        // uploadAndroidDeviceToken(regId, userMO.getUserName(), userMO.getPassword(), context);
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // }
    }

    /**
     * This method parses the string value in a JSON object
     *
     * @param obj
     * @param key
     * @return
     * @throws JSONException
     */
    public String getStringOrNull(JSONObject obj, String key) throws JSONException {
        try {
            String str = obj.getString(key);
            if (str != null) {
                if (str.startsWith("[")) {
                    str = str.substring(1, str.length() - 1);
                }
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // https://dev1.mphrx.com/fisike
    public static String createBaseUrl() {
        String baseUrl = null;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        String httpStr = "https://";
        if (!setMO.isUseHTTPS())
            httpStr = "http://";
        baseUrl = httpStr + setMO.getServerIP() + FISIKE;
        return baseUrl;
    }

    public static String createUrl(String url) {
        return createBaseUrl() + url;
    }

    public static String createMinervaUrl(String url) {
        return createBaseUrl() + url;
    }

    /**
     * @return https://server:443/minerva
     */
    public static String createMinervaBaseUrl() {
        String baseUrl = null;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        String httpStr = "https://";
        if (!setMO.isUseHTTPS())
            httpStr = "http://";
        baseUrl = httpStr + setMO.getServerIP() + MINERVA;
        return baseUrl;
    }


    public String getLogoutApi(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + MINERVA + "api/logout";
        return postURL;
    }

    public String getAgreementAcceptUrl(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + MINERVA + "/user/acceptAgreement";
        return postURL;
    }

    public String getAgreementDeclineUrl(Context context) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();

        String httpStr = "https://";

        if (!useHTTPS)
            httpStr = "http://";

        final String postURL = httpStr + setMO.getServerIP() + MINERVA + "user/declineAgreement";
        return postURL;
    }


    public String getEncounterApi(Context context, String apiName) {
        final String postURL = createMinervaBaseUrl() + "/Encounter/" + apiName;
        return postURL;
    }

    public static String getWebConnectBaseUrl() {
        String baseUrl = null;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        String httpStr = "https://";
        if (!setMO.isUseHTTPS())
            httpStr = "http://";

        baseUrl = httpStr + setMO.getServerIP() + "/webconnect/#/taskDetails";
        return baseUrl;
    }

    public static String getMyScheduleBaseUrl() {
        String baseUrl = null;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        String httpStr = "https://";
        if (!setMO.isUseHTTPS())
            httpStr = "http://";

      //  https://server/webconnect/#/scheduleMeetings/:token
        baseUrl = httpStr + setMO.getServerIP() + "/webconnect/#/scheduleMeetings/list/";
        return baseUrl;
    }





    public String getEncounterUpdateApiUrl(Context context, String apiName, int encounterId) {
        String postURL = createMinervaBaseUrl() + "/Encounter/" + apiName;
        postURL = postURL + encounterId;
        return postURL;
    }

    public String getMinervaPlatformURL(Context context, String parameter) {
        final String postURL = createMinervaBaseUrl() + parameter;
        return postURL;
    }

    public String getUserDetailsApi() {
        String getAPIServlet = "/minerva/user/getUserDetails";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }


    /*****
     * this method execute http post request without auth token
     ******/

    public String executeHttpPost(String url, JsonObject gsonObject) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(gsonObject.toString());
            httpPost.setEntity(se);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig")|| url.contains("moSignUp/verifyOneTimePassword") )
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpPost.setHeader("api-info", getHeader(MyApplication.getAppContext(), headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpPost);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            int status = response.getStatusLine().getStatusCode();
            if (status == 401)
                return TextConstants.UNAUTHORIZED_ACCESS;
            else if ((status + "").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                AppLog.d(url, TextConstants.BARRED_LOCATION_ERROR_CODE);
                Utils.launchLocationChangeActivity(MyApplication.getAppContext());
            } else if (status == 503)
                return TextConstants.SERVICE_UNAVAILABLE;

            else if (status != 200)
                return TextConstants.UNEXPECTED_ERROR;

            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String executeHttpPost(String url, JSONObject jsonObject) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(jsonObject.toString());
            httpPost.setEntity(se);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if(url.contains("/MoObservation/getVitals")||url.contains("moObservation/saveList")
                    ||url.contains("selectList/getVitalConfig")|| url.contains("moSignUp/verifyOneTimePassword") )
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpPost.setHeader("api-info", getHeader(MyApplication.getAppContext(), headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpPost);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            int status = response.getStatusLine().getStatusCode();
           /* if (status == 401)
                return TextConstants.UNAUTHORIZED_ACCESS;*/
            if ((status+"").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                AppLog.d(url,TextConstants.BARRED_LOCATION_ERROR_CODE);
                Utils.launchLocationChangeActivity(MyApplication.getAppContext());
            } else if (status == 503)
                return TextConstants.SERVICE_UNAVAILABLE;

            else if (status != 200)
                return TextConstants.UNEXPECTED_ERROR;

            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String executeHttpPost_handle(String url, JsonObject gsonObject) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(gsonObject.toString());
            httpPost.setEntity(se);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpPost);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            int status = response.getStatusLine().getStatusCode();
            if (status == 401)
                return TextConstants.UNAUTHORIZED_ACCESS;

            else if (status == 503)
                return TextConstants.SERVICE_UNAVAILABLE;

            else if (status != 200 && status != 422)
                return TextConstants.UNEXPECTED_ERROR;

            return page;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public String getRegisterDeviceApi() {
        String getAPIServlet = "/minerva/mobileApp/registerDevice";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getPatientShowCall(String patient_id) {
        String getAPIServlet = "/minerva/patient/show/" + patient_id;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getProfilePicURL() {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/userApi/getProfilePicture";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getUploadProfilePicAPI() {
        String getAPIServlet = "/minerva/userApi/uploadProfilePicture";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getUpdateProfileAPi() {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/moUser/updateUserProfile";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    // Execute api with 401 status
    public ApiResult executeHttpPost(String url, JsonObject gsonObject, Boolean isAuthTokenRequired, Context context) {
        if (url == null || "".equals(url))
            return null;
        BufferedReader in = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            url = url.replaceAll("#", "%23");
            StringEntity se = new StringEntity(gsonObject.toString());
            if (isAuthTokenRequired) {
                String authToken = SharedPref.getAccessToken();
                httpPost.setHeader("x-auth-token", authToken);
                httpPost.setEntity(se);
            }
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            if (url.contains("/MoObservation/getVitals") || url.contains("moObservation/saveList")
                    || url.contains("selectList/getVitalConfig"))
                headerVersion = "V2";
            else
                headerVersion = "V1";
            httpPost.setHeader("api-info", getHeader(context, headerVersion));
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 15000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 15000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpResponse response = httpClient.execute(httpPost);
            int status = response.getStatusLine().getStatusCode();

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String page = sb.toString();
            if (status == 401) {
                AppLog.d(url, 401);
                launchHomeScreen(TextConstants.INVALID_LOGIN, context);
            } else if ((status + "").equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE)) {
                AppLog.d(url, TextConstants.BARRED_LOCATION_ERROR_CODE);
                Utils.launchLocationChangeActivity(context);
            }

            ApiResult apiResult = new ApiResult();
            apiResult.setJsonString(page);
            apiResult.setStatusCode(status);

            return apiResult;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getSignUpMailVerificationUrl(Context context) {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/util/getSignUpVerificationStatus";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getMailChangeVerificationUrl(Context context) {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/util/getEmailVerificationStatus";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getForgetPasswordUrl() {
        // TODO Auto-generated method stub
        String getAPIServlet = "/webconnect/#/forgotPassword";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getEmailExitUrl() {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/userApi/checkIfEmailExists";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getPhoneExistUrl() {
        String getAPIServlet = "/minerva/userApi/checkIfPhoneNumberExists";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }


    public String getResendLinkUrl() {
        String getAPIServlet = "/minerva/signUp/resendSignUpVerificationMail";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }


    public String getPrivacyPolicyURL() {
        String postURL = null;
        if (BuildConfig.isStaticURLEnabled) {
            postURL = CustomizedTextConstants.STATIC_PRIVACY_POLICY;
        } else {
            String getAPIServlet = BuildConfig.isPatientApp ? CustomizedTextConstants.PP_URL_PATIENT : CustomizedTextConstants.PP_URL_PHY;
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO;
            setMO = settingManager.getSettings();
            boolean useHTTPS = setMO.isUseHTTPS();
            String httpStr = "https://";
            if (!useHTTPS)
                httpStr = "http://";
            postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        }
        return postURL;
    }

    public String getTermsOfUseURL() {
        String postURL = null;
        if (BuildConfig.isStaticURLEnabled) {
            postURL = BuildConfig.isPatientApp ? CustomizedTextConstants.STATIC_TOU_PATIENT :
                    CustomizedTextConstants.STATIC_TOU_PHYSICIAN;
        } else {
            String getAPIServlet = BuildConfig.isPatientApp ? CustomizedTextConstants.TOU_URL_PATIENT : CustomizedTextConstants.TOU__URL_PHY;
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO;
            setMO = settingManager.getSettings();
            boolean useHTTPS = setMO.isUseHTTPS();
            String httpStr = "https://";
            if (!useHTTPS)
                httpStr = "http://";
            postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        }
        return postURL;
    }

    public String getFAQURL() {
        String postURL = null;
        if (BuildConfig.isStaticURLEnabled) {
            postURL = BuildConfig.isPatientApp ? getPatientFaqUrl() :
                    getPhysicianfaqUrl();
        } else {
            String getAPIServlet = BuildConfig.isPatientApp ? CustomizedTextConstants.FAQ_URL_PATIENT : CustomizedTextConstants.FAQ_URL_PHY;
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO;
            setMO = settingManager.getSettings();
            boolean useHTTPS = setMO.isUseHTTPS();
            String httpStr = "https://";
            if (!useHTTPS)
                httpStr = "http://";
            postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        }
        return postURL;
    }


    public String getChangePasswordURL() {
        String getAPIServlet = "/minerva/user/changePassword";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getForgetPassOTPSendURL() {

        String getAPIServlet = "/minerva/util/sendForgotPasswordEmailOtp";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getForgetPassOTPVerifyURL() {

        String getAPIServlet = "/minerva/util/verifyForgotPasswordOtp";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getForgetPassPasswordResetURL() {

        String getAPIServlet = "/minerva/util/processForgotPassword";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getEncounterDeleteAPI(String encounterId) {
        String getAPIServlet = "/minerva/Encounter/delete/" + encounterId;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getVerifyOTPUrl() {


        String getAPIServlet = "/minerva/fisikeSignUp/verifyOtp";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getVerifyOTPandUpdatePhoneNoUrl() {
        String getAPIServlet = "/minerva/userApi/verifyOtpAndUpdatePhoneNo";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getForgetPassOTPSendOTPURL() {

        String getAPIServlet = "/minerva/fisikeSignUp/sendForgotPasswordOtp";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getverifyOtpAndSetPasswordURL() {

        String getAPIServlet = "/minerva/fisikeSignUp/verifyOtpAndSetPassword";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public static String getOrganizationWithLogoRequestUrl() {

        String getAPIServlet = "/minerva/Organization/getOrganizationWithLogo";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String sendOtpToAlternateContactUrl() {
        String getAPIServlet = "/minerva/userApi/sendOtpToAlternateContact";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String VerifyOtpForAlternateContactUrl() {
        String getAPIServlet = "/minerva/userApi/verifyOtpForAlternateContact";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getVerifyOTPTFAUrl() {

        String getAPIServlet = "/minerva/citadelAuth/validateTFAOTP";
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;

    }

    public String getLinkedPatientUpdateProfileURL(long id) {
        // TODO Auto-generated method stub
        String getAPIServlet = "/minerva/patient/update/" + id;
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO;
        setMO = settingManager.getSettings();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String postURL = httpStr + setMO.getServerIP() + getAPIServlet;
        return postURL;
    }

    public String getPatientFaqUrl() {
       // if (Utils.getUserSelectedLanguage(MyApplication.getAppContext()).equals(TextConstants.DEFALUT_LANGUAGE_KEY))
            return CustomizedTextConstants.STATIC_FAQ_PATIENT;
       // else
       //     return CustomizedTextConstants.STATIC_FAQ_PATIENT_FR;
    }

    public String getPhysicianfaqUrl() {
     //   if (Utils.getUserSelectedLanguage(MyApplication.getAppContext()).equals(TextConstants.DEFALUT_LANGUAGE_KEY))
            return CustomizedTextConstants.STATIC_FAQ_PHYSICIAN;
      //  else
      //      return CustomizedTextConstants.STATIC_FAQ_PHYSICIAN_FR;
    }

}
