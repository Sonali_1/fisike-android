package com.mphrx.fisike;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mphrx.fisike.constant.CachedUrlConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.google_analytics.AnalyticsTrackers;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.OnNavigationItemClickedListener;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.SharedPref;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import appointment.fragment.HomeSampleCollectionFragment;
import appointment.model.OrderSummary;
import appointment.utils.OnListItemSelected;
import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {

    public static Context context;
    private static boolean activityVisible;
    private static MyApplication mInstance = null;
    private static Bitmap previewBitmap;
    private Handler mHandler = new Handler();
    private static int uploadStaus;
    private static Bitmap imageThumb;
    private static Bitmap tempImagePreview;
    private static MessengerService messengerSeviceInstance;

    private static ArrayList<OnListItemSelected> listeners = new ArrayList<OnListItemSelected>();
    private static OnListItemSelected listener;
    private static ArrayList<OnNavigationItemClickedListener> navigationItemClickedListeners = new ArrayList<OnNavigationItemClickedListener>();
    private static OnNavigationItemClickedListener navigationItemClickedListener;

    public MessengerService getMessengerSeviceInstance() {
        return messengerSeviceInstance;
    }

    public void setMessengerSeviceInstance(MessengerService messengerSeviceInstance) {
        messengerSeviceInstance = messengerSeviceInstance;
    }

    public static Bitmap getImageThumb() {
        return imageThumb;
    }

    public static void setImageThumb(Bitmap imageThumb) {
        if (imageThumb == null && MyApplication.imageThumb != null) {
            MyApplication.imageThumb.recycle();
        }
        MyApplication.imageThumb = imageThumb;
    }

    public static Bitmap getPreviewBitmap() {
        return previewBitmap;
    }

    public static Bitmap getTempImagePreview() {
        return tempImagePreview;
    }

    public static void setTempImagePreview(Bitmap tempImagePreview) {
        MyApplication.tempImagePreview = tempImagePreview;
    }

    public static void setPreviewBitmap(Bitmap previewBitmap) {
        if (previewBitmap == null && MyApplication.previewBitmap != null) {
            MyApplication.previewBitmap.recycle();
        }
        MyApplication.previewBitmap = previewBitmap;
    }

    public static void setUploadStaus(int uploadStaus) {
        MyApplication.uploadStaus = uploadStaus;
    }

    // spool MessageIdListonc
    private List<String> packetIdList;

    public MyApplication() {

    }

    public static MyApplication getInstance() {
        if (null == mInstance) {
            synchronized (MyApplication.class) {
                if (null == mInstance) {
                    mInstance = new MyApplication();
                }
            }
        }
        return mInstance;
    }

    public static Context getAppContext() {
        return context;
    }


    public void onCreate() {
        super.onCreate();
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit);
        Network.init(this.getApplicationContext());
        SharedPref.init(this.getApplicationContext());

        SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        SharedPreferences.Editor edit = preferences.edit();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SQLiteDatabase.loadLibs(getApplicationContext());


        int numberOfTimeApplicationOpened = preferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0);
        if (!preferences.getBoolean(VariableConstants.HAS_RATED_APP, false))
            edit.putInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, (numberOfTimeApplicationOpened + 1));


        edit.commit();
        MyApplication.context = getApplicationContext();
        MyApplication.uploadStaus = VariableConstants.DOCUMENT_STATUS_REDDY;

        packetIdList = new ArrayList<String>();

        AnalyticsTrackers.initialize(this.getApplicationContext());
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        Utils.deleteDir(new File(Environment.getExternalStorageDirectory(), "/" + getString(R.string.app_name) + "/.temp"));  }


    public static void migrateDB(Context ctxt, String dbName, String passphrase) throws IOException {
        File originalFile = ctxt.getDatabasePath(dbName);

        if (originalFile.exists()) {
            File newFile = File.createTempFile("sqlcipherutils", "tmp", ctxt.getCacheDir());
            SQLiteDatabase db = SQLiteDatabase.openDatabase(originalFile.getAbsolutePath(), "", null, SQLiteDatabase.OPEN_READWRITE);
            db.rawExecSQL(String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", newFile.getAbsolutePath(), passphrase));
            db.rawExecSQL("SELECT sqlcipher_export('encrypted')");
            db.rawExecSQL("DETACH DATABASE encrypted;");
            int version = db.getVersion();
            db.close();
            db = SQLiteDatabase.openDatabase(newFile.getAbsolutePath(), passphrase, null, SQLiteDatabase.OPEN_READWRITE);
            db.setVersion(version);
            db.close();
            originalFile.delete();
            newFile.renameTo(originalFile);
        }
    }

    public List<String> getMessageIdList() {
        return this.packetIdList;
    }

    public void setMessageIdList(List<String> packetList) {
        if (packetList != null) {
            if (this.packetIdList != null && this.packetIdList.size() > 0) {
                try {
                    this.packetIdList.addAll(packetList);
                } catch (Exception e) {
                    System.out.println("inside exception e :::::::" + e);
                }
                return;
            } else {
                this.packetIdList = packetList;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void lightenSpoolPacket(int numberOfElements) {
        this.packetIdList = (List<String>) packetIdList.subList(numberOfElements, packetIdList.size());
    }

    public void removeSuccessFullyDeletedMessageId(List<String> packetIdList) {
        if (packetIdList != null && packetIdList.size() > 0) {
            for (String packedIdString : packetIdList) {
                try {
                    this.packetIdList.remove(packedIdString);
                } catch (Exception e) {
                }
            }
        }
    }


    public void clearSpool() {
        if (null != packetIdList && packetIdList.size() > 0) {
            this.packetIdList.clear();
        }
    }

    public static Class getMessengerServiceClass() {
        if (BuildConfig.isPhysicianApp) {
            return com.mphrx.fisike_physician.services.MessengerService.class;
        } else {
            return MessengerService.class;
        }
    }

    public void trackScreenView(String fragmentName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(fragmentName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }


    public void trackNoResultCondition(int count, String constraint) {
        Tracker t = getGoogleAnalyticsTracker();
        //t.set("&uid",userId);
        t.send(new HitBuilders.EventBuilder().setCategory("Condition").setAction("Search Unsuccessful").setLabel(constraint).setValue(count).build());

    }

    public void trackResultCondition(int count, String constraint) {
        Tracker t = getGoogleAnalyticsTracker();
        //t.set("&uid",userId);
        t.send(new HitBuilders.EventBuilder().setCategory("Condition").setAction("Search Successful").setLabel(constraint).setValue(count).build());

    }

    public void trackEvent(String category, String action, String key, String value, String lable) {
        Tracker t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).set(key, value).setLabel(lable).build());
    }


    public void trackEvent(String categoryId, String actionId, long value, String labelId) {
        // Get tracker.
        Tracker tracker = getGoogleAnalyticsTracker();
        // Build and send an Event.
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .setLabel(labelId)
                .setValue(value)
                .build());
    }

    public static ArrayList<String> getCachedURLList() {
        ArrayList<String> cachedUrlList = new ArrayList<String>();
        cachedUrlList.add(CachedUrlConstants.DIAGNOSTICREPORTSEARCH);
        cachedUrlList.add(CachedUrlConstants.OBSERVATIONSEARCH);
        return cachedUrlList;
    }


    public void registerForListSelection(HomeSampleCollectionFragment fragment) {
        listener = (OnListItemSelected) fragment;
        listeners.add(listener);
    }


    public void updateSnackBarValues(int testCost, boolean selected, String from) {

        int totalCost = OrderSummary.getInstance().getTotalPrice();
        int totalNumber = OrderSummary.getInstance().getTestNumber();
        if (selected) {
            totalNumber += 1;
            totalCost += testCost;
            OrderSummary.getInstance().setTestNumber(totalNumber);
            OrderSummary.getInstance().setTotalPrice(totalCost);
            for (int i = 0; i < listeners.size(); i++) {
                ((OnListItemSelected) listeners.get(i)).handleSelection(from, selected);
            }
        } else {
            totalNumber -= 1;
            totalCost -= testCost;
            OrderSummary.getInstance().setTestNumber(totalNumber);
            OrderSummary.getInstance().setTotalPrice(totalCost);
            for (int i = 0; i < listeners.size(); i++) {
                ((OnListItemSelected) listeners.get(i)).handleSelection(from, selected);
            }
        }

    }

    public void registerForListSelection(Activity activity) {
        listener = (OnListItemSelected) activity;
        listeners.add(listener);
    }

    public void unRegisterListSelection(Activity activity) {
        for (int i = 0; i < listeners.size(); i++) {
            if (listeners.get(i) == (OnListItemSelected) activity) {
                listeners.remove(i);
            }
        }
    }

    public void unRegisterListSelection(HomeSampleCollectionFragment fragment) {
        for (int i = 0; i < listeners.size(); i++) {
            if (listeners.get(i) == (HomeSampleCollectionFragment) fragment) {
                listeners.remove(i);
            }
        }
    }

}
