package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Kailash Khurana on 7/22/2016.
 */
public class DiagnosticReportDBAdapter {
    private static final String DIAGNOSTIC_RECORD_TABLE = "DiagnosticReport";
    private static DiagnosticReportDBAdapter diagnosticReportDbAdapter;
    private Context context;
    private SQLiteDatabase database;

    public static String ORDER_ID = "orderId";
    private static String REPORTNAME = "reportName";
    private static String REPORTID = "reportId";
    private static String REPORTDATE = "reportDate";


    public DiagnosticReportDBAdapter(Context context) {
        this.context = context;
    }


    public static DiagnosticReportDBAdapter getInstance(Context ctx) {
        if (null == diagnosticReportDbAdapter) {
            synchronized (DiagnosticOrderDBAdapter.class) {
                if (diagnosticReportDbAdapter == null) {
                    diagnosticReportDbAdapter = new DiagnosticReportDBAdapter(ctx);
                    diagnosticReportDbAdapter.open();
                }
            }
        }
        return diagnosticReportDbAdapter;
    }

    public void open() throws SQLException {

        database = DBHelper.getInstance(context);
    }

    public static String createDiagnosticReportTableQuery() {
        String query = "CREATE TABLE " + DIAGNOSTIC_RECORD_TABLE + "(" +
                REPORTID + " varchar(50) PRIMARY KEY, " +
                ORDER_ID + " varchar(255), " +
                REPORTNAME + " varchar(255), " +
                REPORTDATE + " varchar(255))";

        AppLog.d("CREATE " + DIAGNOSTIC_RECORD_TABLE, query);
        return query;
    }

    /**
     * Create or update  a diagnostic order in db
     *
     * @param diagnosticOrder
     * @return
     */
    public boolean insertDiagnosticReport(DiagnosticReport diagnosticOrder) {
        try {
            ContentValues values = new ContentValues();
            values.put(REPORTID, diagnosticOrder.getReportId());
            values.put(ORDER_ID, diagnosticOrder.getDiagnosticOrderId());
            values.put(REPORTDATE, diagnosticOrder.getReportDate());
            values.put(REPORTNAME, diagnosticOrder.getReportName());
            long count = database.insert(DIAGNOSTIC_RECORD_TABLE, null, values);
            if (count != -1) {
                return true;
            } else {
                try {
                    DiagnosticReport fetchDiagnosticReport = fetchDiagnosticReport(diagnosticOrder.getReportId());
                    if (fetchDiagnosticReport != null) {
                        int numDeleted = database.delete(DIAGNOSTIC_RECORD_TABLE, REPORTID + " = " + fetchDiagnosticReport.getReportId(), null);
                        if (numDeleted > 0) {
                            insertDiagnosticReport(diagnosticOrder);
                        }
                    }
                } catch (Exception e1) {
                    return false;
                }
            }
        } catch (Exception e) {

        }
        return false;
    }

    /**
     * This method will fetch a diagonstic order from the db
     *
     * @param id
     * @return
     * @throws Exception
     */
    public DiagnosticReport fetchDiagnosticReport(String id) throws Exception {
        Cursor mCursor = database.query(true, DIAGNOSTIC_RECORD_TABLE, null, REPORTID + " =" + id, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            DiagnosticReport diagnosticReport = new DiagnosticReport();
            diagnosticReport.setDiagnosticOrderId(mCursor.getString(mCursor.getColumnIndex(ORDER_ID)));
            diagnosticReport.setReportId(mCursor.getString(mCursor.getColumnIndex(REPORTID)));
            diagnosticReport.setReportDate(mCursor.getString(mCursor.getColumnIndex(REPORTDATE)));
            diagnosticReport.setReportName(mCursor.getString(mCursor.getColumnIndex(REPORTNAME)));
            mCursor.close();
            return diagnosticReport;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * This method will fetch a diagonstic order from the db
     *
     * @param id
     * @return
     * @throws Exception
     */
    public ArrayList<DiagnosticReport> fetchDiagnosticReportList(String id) throws Exception {
        Cursor mCursor = database.query(true, DIAGNOSTIC_RECORD_TABLE, null, ORDER_ID + " =" + id, null, null, null, null, null);

        if (mCursor == null) {
            return null;
        }
        try {
            ArrayList<DiagnosticReport> arrayDiagnosticReport = new ArrayList<>();
            while (mCursor.moveToNext()) {
                DiagnosticReport diagnosticReport = new DiagnosticReport();
                diagnosticReport.setDiagnosticOrderId(mCursor.getString(mCursor.getColumnIndex(ORDER_ID)));
                diagnosticReport.setReportId(mCursor.getString(mCursor.getColumnIndex(REPORTID)));
                diagnosticReport.setReportDate(mCursor.getString(mCursor.getColumnIndex(REPORTDATE)));
                diagnosticReport.setReportName(mCursor.getString(mCursor.getColumnIndex(REPORTNAME)));
                arrayDiagnosticReport.add(diagnosticReport);
            }
            return arrayDiagnosticReport;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }
        return null;
    }
}
