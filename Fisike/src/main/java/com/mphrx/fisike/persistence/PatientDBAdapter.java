package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.mphrx.fisike.gson.request.Active;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.BirthDate;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.Contact;
import com.mphrx.fisike.gson.request.Contact_name;
import com.mphrx.fisike.gson.request.Deceased;
import com.mphrx.fisike.gson.request.Extension;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.LinkingList;
import com.mphrx.fisike.gson.request.ManagingOrganization;
import com.mphrx.fisike.gson.request.patIdentifier;
import com.mphrx.fisike.gson.request.telecom;
import com.mphrx.fisike.mo.PatientMO;

import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by administrate on 1/21/2016.
 */
public class PatientDBAdapter {

    private static PatientDBAdapter patientDBAdapter;
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;
    public static final String PATIENT_MO_TABLE = "PatientMo";
    private static String ACTIVE = "active";
    private static String ADDRESS = "address";
    private static String ANIMAL = "animal";
    private static String BIRTHDATE = "birthDate";
    private static String CAREPROVIDER = "careProvider";
    private static String COMMUNICATION = "communication";
    private static String CONTACT = "contact";
    private static String DECEASED = "deceased";
    private static String EXTENSION = "extension";
    private static String GENDER = "gender";
    private static String IDENTIFIER = "identifier";
    private static String LINKLIST = "linkList";
    private static String MANAGINGORGANIZATION = "managingOrganization";
    private static String MARITALSTATUS = "maritalStatus";
    private static String MULTIPLEBIRTH = "multipleBirth";
    private static String NAME = "name";
    private static String PATCLASS = "patclass";
    private static String TELECOM = "telecom";
    public static String PHOTO="photo";
    private static String ID = "id";

    public PatientDBAdapter(Context context) {
        this.context = context;
    }

    public static PatientDBAdapter getInstance(Context ctx) {
        if (null == patientDBAdapter) {
            synchronized (DiagnosticOrderDBAdapter.class) {
                if (patientDBAdapter == null) {
                    patientDBAdapter = new PatientDBAdapter(ctx);
                    patientDBAdapter.open();
                }
            }
        }
        return patientDBAdapter;
    }

    public void open() throws SQLException {

        database = DBHelper.getInstance(context);
    }


    public PatientMO fetchPatientInfo(String patid) throws Exception {
        Cursor mCursor = database.query(true, PATIENT_MO_TABLE, null, "id =" + patid, null, null, null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            String tempDataHolder;
            byte[] tempByteArrayHolder;
            PatientMO patMo = new PatientMO();

            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(ACTIVE));
            if (tempDataHolder != null)
                patMo.setActive((Active) GsonUtils.jsonToObjectMapper(tempDataHolder, Active.class));
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(ADDRESS));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Address> StringArray = (ArrayList<Address>) ois.readObject();
                patMo.setAddress(StringArray);
            }

            patMo.setAnimal(mCursor.getString(mCursor.getColumnIndex(ANIMAL)));
            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(BIRTHDATE));
            if (tempDataHolder != null)
                patMo.setBirthDate((BirthDate) GsonUtils.jsonToObjectMapper(tempDataHolder, BirthDate.class));

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(CAREPROVIDER));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> StringArray = (ArrayList<Object>) ois.readObject();
                patMo.setCareProvider(StringArray);
            }
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(COMMUNICATION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> StringArray = (ArrayList<String>) ois.readObject();
                patMo.setCommunication(StringArray);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(CONTACT));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);

                ArrayList<Contact> addressList = (ArrayList<Contact>) ois.readObject();

                patMo.setContact(addressList);
            }

            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(DECEASED));
            if (tempDataHolder != null)
                patMo.setDeceased((Deceased) GsonUtils.jsonToObjectMapper(tempDataHolder, Deceased.class));
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(EXTENSION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Extension> StringArray = (ArrayList<Extension>) ois.readObject();
                patMo.setExtension(StringArray);
            }
            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(GENDER));
            if (tempDataHolder != null)
                patMo.setGender(tempDataHolder);
            patMo.setId(mCursor.getLong(mCursor.getColumnIndex(ID)));

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(IDENTIFIER));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<patIdentifier> addressList = (ArrayList<patIdentifier>) ois.readObject();
                ;
                patMo.setIdentifier(addressList);
            }
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(LINKLIST));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<LinkingList> addressList = (ArrayList<LinkingList>) ois.readObject();
                ;
                patMo.setLinkList(addressList);
            }
            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(MANAGINGORGANIZATION));
            if (tempDataHolder != null)
                patMo.setManagingOrganization((ManagingOrganization) GsonUtils.jsonToObjectMapper(tempDataHolder, ManagingOrganization.class));
            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(MARITALSTATUS));
            if (tempDataHolder != null)
                patMo.setMaritalStatus((CodingExtensionTextFormat) GsonUtils.jsonToObjectMapper(tempDataHolder, CodingExtensionTextFormat.class));
            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(MULTIPLEBIRTH));
//            if (tempDataHolder != null)
//                patMo.setMultipleBirth((MultipleBirth) GsonUtils.jsonToObjectMapper(tempDataHolder, MultipleBirth.class));
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(NAME));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Contact_name> addressList = (ArrayList<Contact_name>) ois.readObject();
                patMo.setName(addressList);
            }
            patMo.setPatclass(mCursor.getString(mCursor.getColumnIndex(PATCLASS)));
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(TELECOM));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<telecom> addressList = (ArrayList<telecom>) ois.readObject();
                patMo.setTelecom(addressList);
            }
            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(PHOTO));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> addressList = (ArrayList<Object>) ois.readObject();
                patMo.setPhoto(addressList);
            }
            if (mCursor != null) {
                mCursor.close();
            }
            return patMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public boolean createOrUpdatePatientMo(PatientMO patMo) throws Exception {

        PatientMO patmo = fetchPatientInfo(patMo.getId() + "");

        if (patmo != null) {
            database.delete(PATIENT_MO_TABLE, "id" + "=" + patMo.getId(), null);
        }

        ContentValues values = new ContentValues();
        values.put(ACTIVE, new Gson().toJson(patMo.getActive()));
        if (patMo.getAddress() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getAddress());
            values.put(ADDRESS, bytes.toByteArray());
        }

        values.put(ANIMAL, patMo.getAnimal());
        values.put(BIRTHDATE, new Gson().toJson(patMo.getBirthDate()));
        if (patMo.getCareProvider() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getCareProvider());
            values.put(CAREPROVIDER, bytes.toByteArray());
        }

        if (patMo.getCommunication() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getCommunication());
            values.put(COMMUNICATION, bytes.toByteArray());
        }

        if (patMo.getContact() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getContact());
            values.put(CONTACT, bytes.toByteArray());
        }

        values.put(DECEASED, new Gson().toJson(patMo.getDeceased()));

        if (patMo.getExtension() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getExtension());
            values.put(EXTENSION, bytes.toByteArray());
        }

        values.put(GENDER, new Gson().toJson(patMo.getGender()));

        values.put(ID, patMo.getId());

            if (patMo.getIdentifier() != null)
            {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(patMo.getIdentifier());
                values.put(IDENTIFIER, bytes.toByteArray());
            }

        if (patMo.getLinkList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getLinkList());
            values.put(LINKLIST, bytes.toByteArray());
        }

        values.put(MANAGINGORGANIZATION, new Gson().toJson(patMo.getManagingOrganization()));
        values.put(MARITALSTATUS, new Gson().toJson(patMo.getMaritalStatus()));
//        values.put(MULTIPLEBIRTH, new Gson().toJson(patMo.getMultipleBirth()));
        if (patMo.getName() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getName());
            values.put(NAME, bytes.toByteArray());
        }

        values.put(PATCLASS, patMo.getPatclass());
        if (patMo.getTelecom() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getTelecom());
            values.put(TELECOM, bytes.toByteArray());
        }

        if (patMo.getPhoto() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(patMo.getPhoto());
            values.put(PHOTO, bytes.toByteArray());
        }

        long count = database.insert(PATIENT_MO_TABLE, null, values);

        if (count == -1) {
            return false;
        }
        return true;
    }


    public static String createPatientTableQuery() {
        String query = "CREATE TABLE " + PATIENT_MO_TABLE + " (" + ACTIVE + " varchar(255)," +
                ADDRESS + " BLOB," +
                ANIMAL + " varchar(255)," +
                BIRTHDATE + " varchar(255)," +
                CAREPROVIDER + " BLOB," +
                COMMUNICATION + " BLOB," +
                CONTACT + " BLOB," +
                DECEASED + " varchar(255)," +
                EXTENSION + " BLOB," +
                GENDER + " varchar(255)," +
                ID + " LONG," +
                IDENTIFIER + " BLOB," +
                LINKLIST + " varchar(255)," +
                MANAGINGORGANIZATION + " varchar(255)," +
                MARITALSTATUS + " varchar(255)," +
                MULTIPLEBIRTH + " varchar(255)," +
                NAME + " BLOB," +
                PATCLASS + " varchar(255)," +
                TELECOM + " BLOB,"+
                PHOTO+" BLOB)";

        AppLog.d("CREATE PATIENT QUERY", query);
        return query;
    }


    public void updatePatientPhoneNumber(String phoneNumber, ArrayList<telecom> telecom) {

        ContentValues values = new ContentValues();
        if (telecom != null) {
            for (int i = 0; i < telecom.size(); i++) {
                if (telecom.get(i).getUseCode().equals("Home Phone")) {
                    telecom.get(i).setValue(phoneNumber);
                }
            }

            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(telecom);
                values.put(TELECOM, bytes.toByteArray());
                int i = database.update(PATIENT_MO_TABLE, values, ID + " =?", new String[]{phoneNumber});

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
