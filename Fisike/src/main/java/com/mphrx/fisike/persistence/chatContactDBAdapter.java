package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.TextPattern;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by administrate on 1/25/2016.
 */
public class chatContactDBAdapter {

    private static chatContactDBAdapter chatContactDBAdapter;
    private static final String ChatContactMo_TABLE = "ChatContactMO";
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;


    private static String ID = "id";
    private static String USERNAME = "username";
    private static String EMAIL = "email";
    private static String FIRSTNAME = "firstName";
    private static String LASTNAME = "lastName";
    private static String PHONENO = "phoneNo";
    private static String GENDER = "gender";
    private static String DOB = "dob";
    private static String ENABLED = "enabled";
    private static String WEIGHT = "weight";
    private static String WEIGHTUNIT = "weightUnit";
    private static String HEIGHT = "height";
    private static String HEIGHTUNIT = "heightUnit";
    private static String ACCOUNTLOCKED = "accountLocked";
    private static String PHYSICIANID = "physicianId";
    private static String PATIENTID = "patientId";
    private static String SPECIALITY = "speciality";
    private static String DESIGNATION = "designation";
    private static String PERSISTENCEKEY = "persistenceKey";
    private static String ISSELECTED = "isSelected";
    private static String MEMBERSHIPSTATUS = "membershipStatus";
    private static String USERTYPE = "userType";
    private static String ABOUTME = "aboutMe";
    private static String PROFILEPIC = "profilePic";
    private static String JOINEDGROUPMOPK = "joinedGroupMOPK";
    private static String ISADMIN = "isAdmin";
    private static String ROLE = "role";
    private static String EXPERIENCE = "experience";
    private static String PRESENCETYPE = "presenceType";

    private static String PRESENCESTATUS = "presenceStatus";

    public static String getPRESENCESTATUS() {
        return PRESENCESTATUS;
    }

    public static String getPRESENCETYPE() {
        return PRESENCETYPE;
    }

    public chatContactDBAdapter(Context context) {
        this.context = context;
    }

    public static chatContactDBAdapter getInstance(Context ctx) {
        if (null == chatContactDBAdapter) {
            synchronized (chatContactDBAdapter.class) {
                if (chatContactDBAdapter == null) {
                    chatContactDBAdapter = new chatContactDBAdapter(ctx);
                    chatContactDBAdapter.open();
                }
            }
        }
        return chatContactDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public void updateChatContactMo(long pk, ContentValues contentValues) {
        try {
            database.update(ChatContactMo_TABLE, contentValues, PERSISTENCEKEY + "=" + pk, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updateChatContactMoUserProfile(ChatContactMO chatContactMO) {
        chatContactMO.setPersistenceKey(Long.parseLong(ChatContactMO.getPersistenceKey(chatContactMO.getId())));
        if (isChatContactMoAvailable(chatContactMO.getId())) {
            updateChatContactMoUserProfile(0, chatContactMO, false);
        } else {
            insert(chatContactMO);
        }
    }

    public void updateChatContactMoUserProfile(long pk, ChatContactMO chatContactMO, boolean isUpdateProfilePic) {
        ContentValues values = new ContentValues();
        values.put(ID, chatContactMO.getId());
        values.put(USERNAME, chatContactMO.getUsername());
        values.put(EMAIL, chatContactMO.getEmail());
        values.put(FIRSTNAME, chatContactMO.getFirstName());
        values.put(LASTNAME, chatContactMO.getLastName());
        values.put(PHONENO, chatContactMO.getPhoneNo());
        values.put(GENDER, chatContactMO.getGender());
        values.put(DOB, chatContactMO.getDob());
        values.put(ENABLED, chatContactMO.isEnabled() == null ? false : true);
        if (chatContactMO.getWeight() != null) {
            values.put(WEIGHT, chatContactMO.getWeight().getValue());
            values.put(WEIGHTUNIT, chatContactMO.getWeight().getUnit());
        }
        if (chatContactMO.getHeight() != null) {
            values.put(HEIGHT, chatContactMO.getHeight().getValue());
            values.put(HEIGHTUNIT, chatContactMO.getHeight().getUnit());
        }
        values.put(ACCOUNTLOCKED, chatContactMO.isAccountLocked() == null ? false : true);
        values.put(PHYSICIANID, chatContactMO.getPhysicianId());
        values.put(PATIENTID, chatContactMO.getPatientId());
        values.put(SPECIALITY, chatContactMO.getSpeciality());
        values.put(ROLE, chatContactMO.getRoleSingle());
        values.put(EXPERIENCE, chatContactMO.getExperienceSingle());
        values.put(DESIGNATION, chatContactMO.getDesignation());
        if (chatContactMO.getUserType() != null) {
            values.put(USERTYPE, chatContactMO.getUserType().getName());
        }
        values.put(ABOUTME, chatContactMO.getAboutMe());
        //if(isUpdateProfilePic)
        //    values.put(PROFILEPIC,chatContactMO.getProfilePic());

        updateChatContactMo(chatContactMO.getPersistenceKey(), values);

    }

    private ChatContactMO createChatContactMoFromMCursor(Cursor mCursor) {

        try {

            ChatContactMO chatContactMo = new ChatContactMO();
            chatContactMo.setId(mCursor.getString(mCursor.getColumnIndex(ID)));
            chatContactMo.setUsername(mCursor.getString(mCursor.getColumnIndex(USERNAME)));
            chatContactMo.setEmail(mCursor.getString(mCursor.getColumnIndex(EMAIL)));
            chatContactMo.setFirstName(mCursor.getString(mCursor.getColumnIndex(FIRSTNAME)));
            chatContactMo.setLastName(mCursor.getString(mCursor.getColumnIndex(LASTNAME)));
            chatContactMo.setPhoneNo(mCursor.getString(mCursor.getColumnIndex(PHONENO)));
            chatContactMo.setGender(mCursor.getString(mCursor.getColumnIndex(GENDER)));
            chatContactMo.setDob(mCursor.getString(mCursor.getColumnIndex(DOB)));
            chatContactMo.setEnabled(mCursor.getInt(mCursor.getColumnIndex(ENABLED)) == 1 ? true : false);
            Weight weight = new Weight();
            weight.setValue(mCursor.getString(mCursor.getColumnIndex(WEIGHT)));
            weight.setUnit(mCursor.getString(mCursor.getColumnIndex(WEIGHTUNIT)));
            chatContactMo.setWeight(weight);
            Height height = new Height();
            height.setValue(mCursor.getString(mCursor.getColumnIndex(HEIGHT)));
            height.setUnit(mCursor.getString(mCursor.getColumnIndex(HEIGHTUNIT)));
            chatContactMo.setHeight(height);
            chatContactMo.setAccountLocked(mCursor.getInt(mCursor.getColumnIndex(ACCOUNTLOCKED)) == 1 ? true : false);
            chatContactMo.setPhysicianId(mCursor.getLong(mCursor.getColumnIndex(PHYSICIANID)));
            chatContactMo.setPatientId(mCursor.getLong(mCursor.getColumnIndex(PATIENTID)));
            chatContactMo.setSpecialityFromSingle(mCursor.getString(mCursor.getColumnIndex(SPECIALITY)));
            chatContactMo.setDesignation(mCursor.getString(mCursor.getColumnIndex(DESIGNATION)));
            chatContactMo.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
            chatContactMo.setIsSelected(mCursor.getInt(mCursor.getColumnIndex(ISSELECTED)) == 1 ? true : false);
            chatContactMo.setMembershipStatus(mCursor.getString(mCursor.getColumnIndex(MEMBERSHIPSTATUS)));
            UserType userType = new UserType();
            userType.setName(mCursor.getString(mCursor.getColumnIndex(USERTYPE)));
            chatContactMo.setUserType(userType);
            chatContactMo.setAboutMe(mCursor.getString(mCursor.getColumnIndex(ABOUTME)));
            //byte[] profilePic = mCursor.getBlob(mCursor.getColumnIndex(PROFILEPIC));
            //chatContactMo.setProfilePic(profilePic);

            byte[] listBytes3 = mCursor.getBlob(mCursor.getColumnIndex(JOINEDGROUPMOPK));
            if (listBytes3 != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes3);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
                chatContactMo.setJoinedGroupMOPK(listVector);
            }
            chatContactMo.setAdmin(mCursor.getInt(mCursor.getColumnIndex(ISADMIN)) == 1 ? true : false);

            chatContactMo.setRoleFromSingle(mCursor.getString(mCursor.getColumnIndex(ROLE)));
            chatContactMo.setExperienceFromSingle(mCursor.getString(mCursor.getColumnIndex(EXPERIENCE)));
            chatContactMo.setPresenceType(mCursor.getString(mCursor.getColumnIndex(PRESENCETYPE)));
            chatContactMo.setPresenceStatus(mCursor.getString(mCursor.getColumnIndex(PRESENCESTATUS)));
            return chatContactMo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * This method will fetch a chat Contact from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public ChatContactMO fetchChatContactMo(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, ChatContactMo_TABLE, null, PERSISTENCEKEY + " =" + persistenceKey, null, null, null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            ChatContactMO chatContactMo = new ChatContactMO();
            chatContactMo = createChatContactMoFromMCursor(mCursor);
            mCursor.close();
            return chatContactMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public synchronized boolean isChatContactMoAvailable(String id) {

        Cursor mCursor = database.query(true, ChatContactMo_TABLE, new String[]{ID}, ID + "=? OR " + ID + "=?", new String[]{id, id + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp()}, null,
                null, null, null);

        if (mCursor.moveToFirst()) {
            String _id = mCursor.getString(0);
            if (_id != null && _id.equalsIgnoreCase(id)) {
                mCursor.close();
                return true;
            }

        }
        mCursor.close();

        return false;

    }


    /**
     * This method creates or updates the chatContactMo
     *
     * @param chatContactMO
     * @return
     * @throws Exception
     */
    public synchronized boolean insert(ChatContactMO chatContactMO) {

        try {
            ContentValues values = new ContentValues();
            values.put(ID, chatContactMO.getId());
            values.put(USERNAME, chatContactMO.getUsername());
            values.put(EMAIL, chatContactMO.getEmail());
            values.put(FIRSTNAME, chatContactMO.getFirstName());
            values.put(LASTNAME, chatContactMO.getLastName());
            values.put(PHONENO, chatContactMO.getPhoneNo());
            values.put(GENDER, chatContactMO.getGender());
            values.put(DOB, chatContactMO.getDob());
            values.put(ENABLED, chatContactMO.isEnabled() == null ? false : true);
            if (chatContactMO.getWeight() != null) {
                values.put(WEIGHT, chatContactMO.getWeight().getValue());
                values.put(WEIGHTUNIT, chatContactMO.getWeight().getUnit());
            }
            if (chatContactMO.getHeight() != null) {
                values.put(HEIGHT, chatContactMO.getHeight().getValue());
                values.put(HEIGHTUNIT, chatContactMO.getHeight().getUnit());
            }
            values.put(ACCOUNTLOCKED, chatContactMO.isAccountLocked() == null ? false : true);
            values.put(PHYSICIANID, chatContactMO.getPhysicianId());
            values.put(PATIENTID, chatContactMO.getPatientId());
            values.put(SPECIALITY, chatContactMO.getSpeciality());
            values.put(ROLE, chatContactMO.getRoleSingle());
            values.put(EXPERIENCE, chatContactMO.getExperienceSingle());
            values.put(DESIGNATION, chatContactMO.getDesignation());
            values.put(PERSISTENCEKEY, chatContactMO.getPersistenceKey());
            values.put(ISSELECTED, chatContactMO.getIsSelected() == null ? false : true);
            values.put(MEMBERSHIPSTATUS, chatContactMO.getMembershipStatus());
            if (chatContactMO.getUserType() != null) {
                values.put(USERTYPE, chatContactMO.getUserType().getName());
            }
            values.put(ABOUTME, chatContactMO.getAboutMe());
            //values.put(PROFILEPIC, chatContactMO.getProfilePic());

            // new group related changes
            if (chatContactMO.getJoinedGroupMOPK() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatContactMO.getJoinedGroupMOPK());
                values.put(JOINEDGROUPMOPK, bytes.toByteArray());
            }
            values.put(ISADMIN, chatContactMO.isAdmin() == null ? false : true);
            values.put(PRESENCETYPE, chatContactMO.getPresenceType());
            values.put(PRESENCESTATUS, chatContactMO.getPresenceStatus());


            long count = database.insert(ChatContactMo_TABLE, null, values);

            if (count == -1) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public String getChatSenderName(String id) {

        Cursor c = database.query(ChatContactMo_TABLE, new String[]{FIRSTNAME, LASTNAME}, ID + "=? OR " + ID + " =?", new String[]{id, id + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp()}, null, null, null);

        if (c != null && c.moveToFirst()) {
            String name = c.getString(c.getColumnIndex(FIRSTNAME)) + " " + c.getString(c.getColumnIndex(LASTNAME));
            c.close();
            return name;
        }
        if (c != null)
            c.close();
        return "";
    }


    public int fetchGroupMemberListSum() {
        try {
            Cursor mCursor = database.query(false, ChatContactMo_TABLE, new String[]{JOINEDGROUPMOPK}, null, null, null, null, null, null);
            int size = 0;
            if (null != mCursor) {
                while (mCursor.moveToNext()) {
                    byte[] joinedGroupBLOB = mCursor.getBlob(mCursor.getColumnIndex(JOINEDGROUPMOPK));
                    if (joinedGroupBLOB != null) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(joinedGroupBLOB);
                        ObjectInputStream ois = new ObjectInputStream(bais);
                        ArrayList<String> joinedGroupArray = (ArrayList<String>) ois.readObject();
                        size += joinedGroupArray.size();
                    }
                }
            }
            if (mCursor != null) {
                mCursor.close();
            }
            return size;
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getID() {
        return ID;
    }

    public static String getUSERNAME() {
        return USERNAME;
    }

    public static String getEMAIL() {
        return EMAIL;
    }

    public static String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public static String getLASTNAME() {
        return LASTNAME;
    }

    public static String getPHONENO() {
        return PHONENO;
    }

    public static String getGENDER() {
        return GENDER;
    }

    public static String getDOB() {
        return DOB;
    }

    public static String getENABLED() {
        return ENABLED;
    }

    public static String getWEIGHT() {
        return WEIGHT;
    }

    public static String getWEIGHTUNIT() {
        return WEIGHTUNIT;
    }

    public static String getHEIGHT() {
        return HEIGHT;
    }

    public static String getHEIGHTUNIT() {
        return HEIGHTUNIT;
    }

    public static String getACCOUNTLOCKED() {
        return ACCOUNTLOCKED;
    }

    public static String getPHYSICIANID() {
        return PHYSICIANID;
    }

    public static String getPATIENTID() {
        return PATIENTID;
    }

    public static String getSPECIALITY() {
        return SPECIALITY;
    }

    public static String getDESIGNATION() {
        return DESIGNATION;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getISSELECTED() {
        return ISSELECTED;
    }

    public static String getMEMBERSHIPSTATUS() {
        return MEMBERSHIPSTATUS;
    }

    public static String getUSERTYPE() {
        return USERTYPE;
    }

    public static String getABOUTME() {
        return ABOUTME;
    }

    public static String getPROFILEPIC() {
        return PROFILEPIC;
    }

    public static String getJOINEDGROUPMOPK() {
        return JOINEDGROUPMOPK;
    }

    public static String getISADMIN() {
        return ISADMIN;
    }

    public static String getROLE() {
        return ROLE;
    }

    public static String getEXPERIENCE() {
        return EXPERIENCE;
    }

    public static String getChatContactMo_TABLE() {
        return ChatContactMo_TABLE;
    }

    public String isPatientAvaiable(String id) {


        Cursor mCursor = database.query(true, ChatContactMo_TABLE, new String[]{USERTYPE}, ID + " =?", new String[]{id}, null,
                null, null, null);

        if (mCursor.moveToFirst()) {
            String _id = mCursor.getString(mCursor.getColumnIndex(USERTYPE));
            if (_id != null && _id.equalsIgnoreCase(VariableConstants.PATIENT)) {
                mCursor.close();
                return id;
            }

        } else {
            mCursor.close();

            Cursor c = database.query(true, ChatContactMo_TABLE, new String[]{USERTYPE}, ID + " =?", new String[]{id + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp()}, null,
                    null, null, null);
            if (c.moveToFirst()) {
                String _id = c.getString(0);
                if (_id != null && _id.equalsIgnoreCase(VariableConstants.PATIENT)) {
                    c.close();
                    return id.split("@")[0];
                }
            }

            c.close();
        }
        mCursor.close();

        return "";
    }

    public void updateJOINEDGROUPMOPK(long persistanceKey, ArrayList<String> joinedGroup) {

        try {
            ContentValues values = new ContentValues();
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(joinedGroup);
            values.put(JOINEDGROUPMOPK, bytes.toByteArray());
            updateChatContactMo(persistanceKey, values);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Deprecated
    public void updateProfilePic(long persistanceKey, byte[] profilePic) {
        //
        //try {
        //    ContentValues values=new ContentValues();
        //    values.put(PROFILEPIC, profilePic);
        //    updateChatContactMo(persistanceKey,values);
        //}
        //catch (Exception e)
        //{
        //    e.printStackTrace();
        //}
        //
        //
    }

    public static String createChatContactTableQuery() {

        String query = "CREATE TABLE " + ChatContactMo_TABLE + " ( " +
                ID + " varchar(255), " +
                USERNAME + " varchar(255), " +
                EMAIL + " varchar(255), " +
                FIRSTNAME + " varchar(255), " +
                LASTNAME + " varchar(255), " +
                PHONENO + " varchar(255), " +
                GENDER + " varchar(255), " +
                DOB + " varchar(255), " +
                ENABLED + " INT, " +
                WEIGHT + " DOUBLE, " +
                WEIGHTUNIT + " varchar(255), " +
                HEIGHT + " DOUBLE, " +
                HEIGHTUNIT + " varchar(255), " +
                ACCOUNTLOCKED + " INT, " +
                PHYSICIANID + " varchar(255), " +
                PATIENTID + " varchar(255), " +
                SPECIALITY + " varchar(255), " +
                DESIGNATION + " varchar(255), " +
                PERSISTENCEKEY + " varchar(255), " +
                ISSELECTED + " INT, " +
                MEMBERSHIPSTATUS + " varchar(255), " +
                USERTYPE + " varchar(255), " +
                ABOUTME + " varchar(255), " +
                PROFILEPIC + " BLOB, " +
                JOINEDGROUPMOPK + " BLOB, " +
                ISADMIN + " INT, " +
                ROLE + " varchar(255), " +
                EXPERIENCE + " varchar(255), " +
                PRESENCESTATUS + " varchar(255), " +
                PRESENCETYPE + " varchar(255) " +
                ")";


        AppLog.d("CREATE " + getChatContactMo_TABLE(), query);
        return query;
    }

    public ArrayList<ChatContactMO> getAllChatContactMO(boolean loadAllContacts) {
        String userId = SettingManager.getInstance().getUserMO().getId() + "";

        Cursor mCursor = database.query(true, ChatContactMo_TABLE, null, null, null, null,
                null, null, null);

        ArrayList<ChatContactMO> chatContactMOs = new ArrayList<>();

        if (mCursor.getCount() > 0) {
            while (mCursor.moveToNext()) {
                try {
                    ChatContactMO mo = createChatContactMoFromMCursor(mCursor);
                    if (!TextPattern.getUserId(mo.getId()).equals(TextPattern.getUserId(userId))
                            && (!mo.getUserType().getName().equalsIgnoreCase(VariableConstants.PATIENT)
                            || loadAllContacts) && mo.getEmail() != null) {
                        chatContactMOs.add(mo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        mCursor.close();

        Set<ChatContactMO> hs = new HashSet<>();
        hs.addAll(chatContactMOs);
        chatContactMOs.clear();
        chatContactMOs.addAll(hs);
        return chatContactMOs;
    }

}
