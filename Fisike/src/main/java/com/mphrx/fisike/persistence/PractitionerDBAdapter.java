package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.models.PractitionarModel;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by xmb2nc on 22-01-2016.
 */
public class PractitionerDBAdapter {
    private static PractitionerDBAdapter practitionerDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static final String PRACTITIONER_TABLE = "Practitonar";
    private static final String PRACTITIONER_ID = "practitionarID";
    private static final String PHYSICIAN_NAME = "physicianName";

    //db.execSQL("CREATE TABLE Practitonar(practitionarID INTEGER PRIMARY KEY, physicianName varchar(255))");

    public PractitionerDBAdapter(Context context) {
        this.context = context;
    }

    public static PractitionerDBAdapter getInstance(Context ctx) {
        if (null == practitionerDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (practitionerDBAdapter == null) {
                    practitionerDBAdapter = new PractitionerDBAdapter(ctx);
                    practitionerDBAdapter.open();
                }
            }
        }
        return practitionerDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String createPractitionerQuery() {
        String query;
        query = "CREATE TABLE " + PRACTITIONER_TABLE + "(" +
                PRACTITIONER_ID + " INTEGER PRIMARY KEY, " +
                PHYSICIAN_NAME + " varchar(255))";
        AppLog.d("CREATE PRACTITIONER TABLE:", query);
        return query;
    }

    public static String dropTable() {
        return "DROP TABLE IF EXISTS " + PRACTITIONER_TABLE;
    }

    /**
     * This method will fetch a Practitionar from the db
     *
     * @param practitionarID
     * @return
     * @throws Exception
     */
    public PractitionarModel fetchPractitionar(int practitionarID) throws Exception {
        Cursor mCursor = database.query(true, PRACTITIONER_TABLE, new String[]{PRACTITIONER_ID, PHYSICIAN_NAME}, PRACTITIONER_ID + "="
                + practitionarID, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            PractitionarModel practitionarModel = new PractitionarModel();
            practitionarModel.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONER_ID)));
            practitionarModel.setPhysicianName(mCursor.getString(mCursor.getColumnIndex(PHYSICIAN_NAME)));
            mCursor.close();
            return practitionarModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * This method creates or updates the PractitionarModel details
     *
     * @param practitionarModel
     * @return
     * @throws Exception
     */
    public boolean createOrUpdatePractitionarModel(PractitionarModel practitionarModel) throws Exception {
        PractitionarModel existingObject = fetchPractitionar(practitionarModel.getPractitionarID());

        if (existingObject != null) {
            database.delete(PRACTITIONER_TABLE, PRACTITIONER_ID + "=" + practitionarModel.getPractitionarID(), null);
        }

        ContentValues values = new ContentValues();
        values.put(PRACTITIONER_ID, practitionarModel.getPractitionarID());
        values.put(PHYSICIAN_NAME, practitionarModel.getPhysicianName());

        long count = database.insert(PRACTITIONER_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }


    public static String getPractitionerTable() {
        return PRACTITIONER_TABLE;
    }

}
