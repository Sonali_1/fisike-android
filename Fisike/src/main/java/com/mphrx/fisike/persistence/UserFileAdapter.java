package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import org.apache.http.auth.AUTH;

/**
 * Created by aastha on 12/23/2016.
 */
public class UserFileAdapter {

    private static UserFileAdapter userFileAdapter;

    private Context context;
    private SQLiteDatabase database;
    private Cursor mCursor;

    public static String getUserFileTable() {
        return USER_FILE_TABLE;
    }

    private static final String USER_FILE_TABLE = "UserDetailsFileMo";
    private static final String AUTH_TOKEN = "authToken";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL_ID = "emailId";
    private static final String USERNAME = "userName";
    private static final String DEVICE_UID = "deviceUid";
    private static final String patientId = "patientId";
    private static final String PK = "pk";
    private static final String pkKey = "1";

    public UserFileAdapter(Context context) {
        this.context = context;
    }

    public static String getAuthToken() {
        return AUTH_TOKEN;
    }

    public static String getEmailId() {
        return EMAIL_ID;
    }

    public static String getPhoneNumber() {
        return PHONE_NUMBER;
    }

    public static String getDeviceUid() {
        return DEVICE_UID;
    }

    public static String getPatientId() {
        return patientId;
    }

    public static String getUSERNAME() {
        return USERNAME;
    }

    public static UserFileAdapter getInstance(Context ctx) {
        if (null == userFileAdapter) {
            synchronized (UserFileAdapter.class) {
                if (userFileAdapter == null) {
                    userFileAdapter = new UserFileAdapter(ctx);
                    userFileAdapter.open();
                }
            }
        }
        return userFileAdapter;
    }

    public static String getUserFileTableQuery() {
        String query = "CREATE TABLE " + getUserFileTable()
                + " ( " + getAuthToken() + " varchar(255), "
                + getPhoneNumber() + " varchar(255), "
                + getEmailId() + " varchar(255), "
                + getDeviceUid() + " varchar(255), "
                + getPatientId() + " integer, "
                + getPrimaryKey() + " varchar(255) PRIMARY KEY, "
                + getUSERNAME() + " varchar(255))";
        return query;
    }


    private void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    private boolean insertData(ContentValues contentValues) {
        contentValues.put(getPrimaryKey(), pkKey);

        try {

            long row=database.insert(getUserFileTable(), null, contentValues);
            if(row>0)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }

    }

    private boolean updateData(ContentValues contentValues) {

        try {
            database.update(getUserFileTable(), contentValues, getPrimaryKey() + " = " + pkKey, null);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean insert(String parameter, String columnName)  {

        ContentValues values = new ContentValues();
        values.put(columnName, parameter);
        if(!insertData(values))
        {
            updateData(values);
        }

        if (parameter == null) {
            return false;
        } else
            return true;
    }

    public String fetchValue(String columnName) {

        Cursor mCursor = database.query(getUserFileTable(), new String[]{columnName}, getPrimaryKey()+ " = " + pkKey, null, null, null, null);

        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            String tableValue = mCursor.getString(mCursor.getColumnIndex(columnName));
            mCursor.close();
            return tableValue;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }

    }

    private Cursor getFromDb(String columnName) {

        mCursor = database.query(getUserFileTable(), new String[]{columnName}, null, null, null, null, null);

        return mCursor;
    }

    public static String getPrimaryKey() {
        return PK;
    }

    public static String getPkKey() {
        return pkKey;
    }
}
