package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.Cursor;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import signup.fragment.OTPFragment;

/**
 * Created by administrate on 1/21/2016.
 */
public class ConfigDBAdapter
{

    private static ConfigDBAdapter configDBAdapter;
    private Context context;
    private static SQLiteDatabase database;
    private DBManager dbManager;
    private static final String ConfigMO_TABLE = "ConfigMO";
    private static String ISCHATENABLED="isChatEnabled";
    private static String ISSHOWPIN="isShowPin";
    private static String CANCHANGEPINSTATUS="canChangePinStatus";
    private static String PERSISTENCEKEY="persistenceKey";
    private static String CAREPROVIDER="careProvider";
    private static String COMMUNICATION  ="communication";
    private static String NOOFDAYSALTERNATECONTACT="NoOfDaysAlternateContact";
    private static String PINLOCKTIME="pinLockTime";
    public static String AUTOLOGOUTTIME="autoLockTime";
    public static String LANGUAGELIST="languageList";
    public static String OTPATTEMPTS = "otpAttempts";
    public static String PASSWORDREGEX = "passwordRegex";

    public ConfigDBAdapter(Context context)
    {
        this.context = context;
    }

    public static ConfigDBAdapter getInstance(Context ctx) {
        if (null == configDBAdapter) {
            synchronized (DiagnosticOrderDBAdapter.class) {
                if (configDBAdapter == null) {
                    configDBAdapter = new ConfigDBAdapter(ctx);
                    configDBAdapter.open();
                }
            }
        }
        return configDBAdapter;
    }

    public static String getNOOFDAYSALTERNATECONTACT() {
        return NOOFDAYSALTERNATECONTACT;
    }

    public static String getConfigMO_TABLE() {
        return ConfigMO_TABLE;
    }

    public void open() throws SQLException
    {
        database = DBHelper.getInstance(context);
    }



    public ConfigMO fetchConfigMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, ConfigMO_TABLE,null, "persistenceKey ="
                + persistenceKey, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            ConfigMO configMo = new ConfigMO();
            configMo.setChatEnabled(mCursor.getInt(mCursor.getColumnIndex(ISCHATENABLED)) == 0 ? false : true);
            configMo.setShowPin(mCursor.getInt(mCursor.getColumnIndex(ISSHOWPIN)) == 0 ? false : true);
            configMo.setCanChangePinStatus(mCursor.getInt(mCursor.getColumnIndex(CANCHANGEPINSTATUS)) == 0 ? false : true);
            configMo.setNoOfDaysForAlternateContact((mCursor.getLong(mCursor.getColumnIndex(NOOFDAYSALTERNATECONTACT)))!=0?mCursor.getLong(mCursor.getColumnIndex(NOOFDAYSALTERNATECONTACT)):TextConstants.ALTER_CONTACT_DAYS_DIFF);
            configMo.setPinLockTime((mCursor.getInt(mCursor.getColumnIndex(PINLOCKTIME))!=0?mCursor.getInt(mCursor.getColumnIndex(PINLOCKTIME)):TextConstants.PIN_LOCK_TIME));
            configMo.setAutoLockMinute((mCursor.getInt(mCursor.getColumnIndex(AUTOLOGOUTTIME))!=0?mCursor.getInt(mCursor.getColumnIndex(AUTOLOGOUTTIME)):0));
            configMo.setOtpAttemptsLeft((mCursor.getString(mCursor.getColumnIndex(OTPATTEMPTS))!=null?mCursor.getString(mCursor.getColumnIndex(OTPATTEMPTS)):"0"));
            configMo.setPasswordRegex((mCursor.getString(mCursor.getColumnIndex(PASSWORDREGEX))!=null?mCursor.getString(mCursor.getColumnIndex(PASSWORDREGEX)):""));
            byte[] listBytes = mCursor.getBlob(mCursor.getColumnIndex(LANGUAGELIST));
            if (listBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                LinkedHashMap<String,String> listVector = (LinkedHashMap<String,String>) ois.readObject();
                configMo.setLanguageList(listVector);
            }
            mCursor.close();
            return configMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public boolean createOrUpdateConfigMO(ConfigMO configMo) throws Exception
    {
        if(configMo.getPersistenceKey()==0)
            configMo.setPersistenceKey(ConfigMO.CONFIG_KEY);

        ConfigMO existingConfigMO = fetchConfigMO(configMo.getPersistenceKey() + "");
        if (existingConfigMO != null)
        {
            database.delete(ConfigMO_TABLE, PERSISTENCEKEY+"=" + configMo.getPersistenceKey(), null);
        }
        ContentValues values = new ContentValues();
        values.put(PERSISTENCEKEY, configMo.getPersistenceKey());
        values.put(ISCHATENABLED, configMo.isChatEnabled() ? 1 : 0);
        values.put(ISSHOWPIN, configMo.isShowPin() ? 1 : 0);
        values.put(CANCHANGEPINSTATUS, configMo.isCanChangePinStatus() ? 1 : 0);
        values.put(NOOFDAYSALTERNATECONTACT, configMo.getNoOfDaysForAlternateContact()!=0 ?configMo.getNoOfDaysForAlternateContact(): TextConstants.ALTER_CONTACT_DAYS_DIFF );
        values.put(PINLOCKTIME,configMo.getPinLockTime());
        values.put(AUTOLOGOUTTIME,configMo.getAutoLockMinute()!=0?configMo.getAutoLockMinute():0);
        values.put(OTPATTEMPTS, Utils.isValueAvailable(configMo.getOtpAttemptsLeft())?configMo.getOtpAttemptsLeft():"0");
        values.put(PASSWORDREGEX, Utils.isValueAvailable(configMo.getPasswordRegex())?configMo.getPasswordRegex():"");

        if (configMo.getLanguageList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(configMo.getLanguageList());
            values.put(LANGUAGELIST, bytes.toByteArray());
        }


        // long count = database.update(ConfigMO_TABLE, values, null, null);
        long count = database.insert(ConfigMO_TABLE, null, values);
        if (count == -1)
        {
            return false;
        }
        return true;
    }

    public static String createConfigMoQuery()
    {
        String query="CREATE TABLE "+ConfigMO_TABLE+"("
                        +ISCHATENABLED+" INT,"
                        +ISSHOWPIN+" INT,"
                        +CANCHANGEPINSTATUS+" INT,"
                        +NOOFDAYSALTERNATECONTACT+" LONG,"
                        +PERSISTENCEKEY+" varchar(255),"
                        +PINLOCKTIME+" INT,"
                        +AUTOLOGOUTTIME+" INT,"
                        +OTPATTEMPTS+" varchar(255),"
                        +PASSWORDREGEX+" varchar(255),"
                        +LANGUAGELIST +" BLOB)";

        AppLog.d("CREATE CONFIG MO TABLE", query);
        return query;
    }


    public  void updateNOOFDAYSALTERNATECONTACT(long no_of_dayz, long persistanceKey )
    {
         ContentValues values=new ContentValues();
         values.put(NOOFDAYSALTERNATECONTACT,no_of_dayz);
        try {
            database.update(ConfigMO_TABLE, values, PERSISTENCEKEY + "=" + persistanceKey, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public  void updateIsToShowPINScreen(boolean value, long persistanceKey )
    {
        ContentValues values=new ContentValues();
        values.put(ISSHOWPIN,value);
        try {
            database.update(ConfigMO_TABLE, values, PERSISTENCEKEY + "=" + persistanceKey, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPINLOCKTIME() {
        return PINLOCKTIME;
    }

    public static void setPINLOCKTIME(String PINLOCKTIME) {
        ConfigDBAdapter.PINLOCKTIME = PINLOCKTIME;
    }

    public  void updatePinLockTime(int pinTime, long persistanceKey )
    {
        ContentValues values=new ContentValues();

        values.put(PINLOCKTIME,pinTime);
        try {
            database.update(ConfigMO_TABLE, values, PERSISTENCEKEY + "=" + persistanceKey, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
