
package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.models.PractitionarModel;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import appointment.database.TestsOrderSummary;

/**
 * Created by laxmansingh on 8/2/2016.
 */
public class VitalsDBAdapter {
    private static VitalsDBAdapter vitalsDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static final String VITALS_TABLE = "Vitals";
    private static final String VITALS_ID = "VitalsID";
    private static final String VITALS_DATE = "VitalsDate";
    private static final String VITALS_LABNAME = "VitalsLabName";
    private static final String VITALS_PATIENTNAME = "VitalsPatientName";
    private static final String VITALS_UNIT = "VitalsUnit";
    private static final String VITALS_VALUE = "VitalsValue";
    private static final String VITALS_PARAMID = "VitalsParamID";
    private static final String VITALS_RANGE = "VitalsRange";


    //db.execSQL("CREATE TABLE Practitonar(practitionarID INTEGER PRIMARY KEY, physicianName varchar(255))");

    public VitalsDBAdapter(Context context) {
        this.context = context;
    }

    public static VitalsDBAdapter getInstance(Context ctx) {
        if (null == vitalsDBAdapter) {
            synchronized (VitalsDBAdapter.class) {
                if (vitalsDBAdapter == null) {
                    vitalsDBAdapter = new VitalsDBAdapter(ctx);
                    vitalsDBAdapter.open();
                }
            }
        }
        return vitalsDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String createVitalsQuery() {
        String query;
        query = "CREATE TABLE " + VITALS_TABLE + "(" +
                VITALS_PARAMID + " varchar(255) PRIMARY KEY NOT NULL," +
                VITALS_DATE + " varchar(255)," +
                VITALS_LABNAME + " varchar(255)," +
                VITALS_PATIENTNAME + " varchar(255)," +
                VITALS_UNIT + " varchar(255)," +
                VITALS_VALUE + " varchar(255)," +
                VITALS_RANGE + " varchar(255)," +
                VITALS_ID + " varchar(255))";
        AppLog.d("CREATEVITALSTABLE:", query);
        return query;
    }

    public static String dropTable() {
        return "DROP TABLE IF EXISTS " + VITALS_TABLE;
    }

    /**
     * This method will fetch a Vitals from the db
     *
     * @param paramsId
     * @return
     * @throws Exception
     */
    public VitalsModel fetchVitals(String paramsId) throws Exception {

        //Cursor mCursor = database.query(false, VITALS_TABLE, null, null, null, null, null, null, null);

        //  Cursor mCursor = database.rawQuery("select * from Vitals WHERE " + VITALS_PARAMID + "=" + paramsId, null);
        //   Log.i("thisisnowhere", "count  " + mCursor.getCount());
        String[] projection = {
                VITALS_ID,
                VITALS_DATE,
                VITALS_LABNAME,
                VITALS_PATIENTNAME,
                VITALS_UNIT,
                VITALS_VALUE,
                VITALS_RANGE,
                VITALS_PARAMID
        };


        // Define 'where' part of query.
        String selection = VITALS_PARAMID + " LIKE ?";
// Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(paramsId)};


        Cursor mCursor = database.query(
                VITALS_TABLE,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (mCursor != null && mCursor.moveToFirst()) {
            VitalsModel vitalsModel = new VitalsModel();
            try {
                vitalsModel.setParamId(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_PARAMID)).trim());
                vitalsModel.setId(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_ID)));
                vitalsModel.setDate(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_DATE)));
                vitalsModel.setLabName(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_LABNAME)));
                vitalsModel.setPatientName(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_PATIENTNAME)));
                vitalsModel.setUnit(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_UNIT)));
                vitalsModel.setRange_status(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_RANGE)));
                vitalsModel.setValue(mCursor.getString(mCursor.getColumnIndexOrThrow(VITALS_VALUE)));
            } finally {
                mCursor.close();
            }
            return vitalsModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * This method creates or updates the PractitionarModel details
     *
     * @param vitalsModel
     * @return
     * @throws Exception
     */
    public boolean insertOrUpdateVitalsModel(VitalsModel vitalsModel)  {

        ContentValues values = new ContentValues();
        values.put(VITALS_PARAMID, vitalsModel.getParamId().toString().trim());
        values.put(VITALS_ID, vitalsModel.getId().toString().trim());
        values.put(VITALS_DATE, vitalsModel.getDate().toString().trim());
        values.put(VITALS_LABNAME, vitalsModel.getLabName().toString().trim());
        values.put(VITALS_PATIENTNAME, vitalsModel.getPatientName().toString().trim());
        values.put(VITALS_UNIT, vitalsModel.getUnit().toString().trim());
        values.put(VITALS_RANGE, vitalsModel.getRange_status().toString().trim());
        values.put(VITALS_VALUE, vitalsModel.getValue().toString().trim());

        long count = database.insert(VITALS_TABLE, null, values);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = 0;

// this will insert if record is new, update otherwise
        try {
            newRowId = database.insertWithOnConflict(VITALS_TABLE, null, values, android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE);
        } catch (Exception ex) {
        }
        if (newRowId == -1) {
            return false;
        }
        return true;
    }


    public static String getVitalsTable() {
        return VITALS_TABLE;
    }

}
