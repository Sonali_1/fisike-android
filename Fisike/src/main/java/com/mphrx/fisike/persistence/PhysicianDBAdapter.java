package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.mphrx.fisike.gson.profile.request.PractitionerRole;
import com.mphrx.fisike.gson.request.Active;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.BirthDate;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.Contact;
import com.mphrx.fisike.gson.request.Contact_name;
import com.mphrx.fisike.gson.request.Deceased;
import com.mphrx.fisike.mo.Practitioner_Data.Extension;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.LinkingList;
import com.mphrx.fisike.gson.request.ManagingOrganization;
import com.mphrx.fisike.gson.request.patIdentifier;
import com.mphrx.fisike.gson.request.telecom;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.Practitioner_Data.practitionerRole;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Neha on 23-12-2016.
 */

public class PhysicianDBAdapter {

    private static PhysicianDBAdapter physicianDBAdapter;
    private Context context;
    private SQLiteDatabase database;
    public static final String PHYSICIAN_MO_TABLE = "PhysicianMo";
    public static final String IDENTIFIER = "identifier";
    public static final String EXTENSION = "extension";
    public static final String ADDRESS = "address";
    public static final String GENDER = "gender";
    public static final String PRACTITIONER_ROLE = "practitionerRole";
    public static final String ACTIVE = "active";
    public static final String PHOTO = "photo";
    public static final String BIRTHDATE = "birthDate";
    public static final String LOWER_CASE_NAME = "lowerCaseName";
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String TELECOM = "telecom";
    public static final String LOCATION = "location";
    public static final String COMMUNICATION = "communication";
    public static final String QUALIFICATION = "qualification";
    public static final String _CLASS = "_CLASS";


    public PhysicianDBAdapter(Context context) {
        this.context = context;
    }

    public static PhysicianDBAdapter getInstance(Context ctx) {
        if (null == physicianDBAdapter) {
            synchronized (DiagnosticOrderDBAdapter.class) {
                if (physicianDBAdapter == null) {
                    physicianDBAdapter = new PhysicianDBAdapter(ctx);
                    physicianDBAdapter.open();
                }
            }
        }
        return physicianDBAdapter;
    }

    public void open() throws SQLException {

        database = DBHelper.getInstance(context);
    }


    public PractitionerMO fetchPhysicianInfo(String physicianId) throws Exception {
        Cursor mCursor = database.query(true, PHYSICIAN_MO_TABLE, null, "id =" + physicianId, null, null, null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            String tempDataHolder;
            byte[] tempByteArrayHolder;
            PractitionerMO practitionerMO = new PractitionerMO();

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(IDENTIFIER));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<patIdentifier> addressList = (ArrayList<patIdentifier>) ois.readObject();
                practitionerMO.setIdentifier(addressList);
            }


            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(EXTENSION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Extension> StringArray = (ArrayList<Extension>) ois.readObject();
                practitionerMO.setExtension(StringArray);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(ADDRESS));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Address> StringArray = (ArrayList<Address>) ois.readObject();
                practitionerMO.setAddress(StringArray);
            }

            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(GENDER));
            if (tempDataHolder != null)
                practitionerMO.setGender(tempDataHolder);


            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(PRACTITIONER_ROLE));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<practitionerRole> StringArray = (ArrayList<practitionerRole>) ois.readObject();
                practitionerMO.setPractitionerRole(StringArray);
            }

            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(ACTIVE));
            if (tempDataHolder != null)
                practitionerMO.setActive((Active) GsonUtils.jsonToObjectMapper(tempDataHolder, Active.class));

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(PHOTO));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> addressList = (ArrayList<Object>) ois.readObject();
                practitionerMO.setPhoto(addressList);
            }

            tempDataHolder = mCursor.getString(mCursor.getColumnIndex(BIRTHDATE));
            if (tempDataHolder != null)
                practitionerMO.setBirthDate((BirthDate) GsonUtils.jsonToObjectMapper(tempDataHolder, BirthDate.class));

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(LOWER_CASE_NAME));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                Contact_name addressList = (Contact_name) ois.readObject();
                practitionerMO.setLowerCaseName(addressList);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(NAME));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                Contact_name addressList = (Contact_name) ois.readObject();
                practitionerMO.setName(addressList);
            }

            practitionerMO.setId(mCursor.getLong(mCursor.getColumnIndex(ID)));


            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(TELECOM));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<telecom> addressList = (ArrayList<telecom>) ois.readObject();
                practitionerMO.setTelecom(addressList);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(LOCATION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> addressList = (ArrayList<Object>) ois.readObject();
                practitionerMO.setLocation(addressList);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(COMMUNICATION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> StringArray = (ArrayList<Object>) ois.readObject();
                practitionerMO.setCommunication(StringArray);
            }

            tempByteArrayHolder = mCursor.getBlob(mCursor.getColumnIndex(QUALIFICATION));
            if (tempByteArrayHolder != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(tempByteArrayHolder);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Object> StringArray = (ArrayList<Object>) ois.readObject();
                practitionerMO.setQualification(StringArray);
            }

            practitionerMO.set_class(mCursor.getString(mCursor.getColumnIndex(_CLASS)));
            if (mCursor != null)
            {
                mCursor.close();
            }
            return practitionerMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public boolean createOrUpdatePhysicianMo(PractitionerMO practitionerMO) throws Exception {

        PractitionerMO patmo = fetchPhysicianInfo(practitionerMO.getId() + "");

        if (patmo != null) {
            database.delete(PHYSICIAN_MO_TABLE, "id" + "=" + practitionerMO.getId(), null);
        }

        ContentValues values = new ContentValues();

        if (practitionerMO.getIdentifier() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getIdentifier());
            values.put(IDENTIFIER, bytes.toByteArray());
        }


        if (practitionerMO.getExtension() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getExtension());
            values.put(EXTENSION, bytes.toByteArray());
        }

        if (practitionerMO.getAddress() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getAddress());
            values.put(ADDRESS, bytes.toByteArray());
        }

        values.put(GENDER, practitionerMO.getGender());

        if (practitionerMO.getPractitionerRole() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getPractitionerRole());
            values.put(PRACTITIONER_ROLE, bytes.toByteArray());
        }

        values.put(ACTIVE, new Gson().toJson(practitionerMO.getActive()));

        if (practitionerMO.getPhoto() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getPhoto());
            values.put(PHOTO, bytes.toByteArray());
        }

        values.put(BIRTHDATE, new Gson().toJson(practitionerMO.getBirthDate()));


        if (practitionerMO.getLowerCaseName() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getLowerCaseName());
            values.put(LOWER_CASE_NAME, bytes.toByteArray());
        }

        if (practitionerMO.getName() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getName());
            values.put(NAME, bytes.toByteArray());
        }

        values.put(ID, practitionerMO.getId());

        if (practitionerMO.getCommunication() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getCommunication());
            values.put(COMMUNICATION, bytes.toByteArray());
        }

        if (practitionerMO.getTelecom() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getTelecom());
            values.put(TELECOM, bytes.toByteArray());
        }

        if (practitionerMO.getLocation() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getLocation());
            values.put(LOCATION, bytes.toByteArray());
        }

        if (practitionerMO.getCommunication() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getCommunication());
            values.put(COMMUNICATION, bytes.toByteArray());
        }

        if (practitionerMO.getQualification() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(practitionerMO.getQualification());
            values.put(QUALIFICATION, bytes.toByteArray());
        }

        values.put(_CLASS,practitionerMO.get_class());
        long count = database.insert(PHYSICIAN_MO_TABLE, null, values);
        Utils.setPractitionerSpecDesigRole(practitionerMO);

        if (count == -1) {
            return false;
        }
        return true;
    }


    public static String createPhysicianTableQuery() {

        String query = "CREATE TABLE " + PHYSICIAN_MO_TABLE + " (" +
                IDENTIFIER + " BLOB," +
                EXTENSION + " BLOB," +
                ADDRESS + " BLOB," +
                GENDER + " varchar(255)," +
                PRACTITIONER_ROLE + " BLOB," +
                ACTIVE + " varchar(255)," +
                PHOTO + " BLOB," +
                BIRTHDATE + " varchar(255)," +
                LOWER_CASE_NAME + " varchar(255)," +
                NAME + " BLOB," +
                ID + " LONG," +
                LOCATION + " BLOB," +
                COMMUNICATION + " BLOB," +
                QUALIFICATION + " BLOB," +
                TELECOM + " BLOB," +
                _CLASS+ " varchar(255))";


        AppLog.d("CREATE PATIENT QUERY", query);
        return query;
    }


}
