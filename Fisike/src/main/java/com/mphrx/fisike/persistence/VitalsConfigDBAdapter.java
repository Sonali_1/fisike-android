package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.Vital;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;

import net.sqlcipher.Cursor;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.util.ArrayList;

/**
 * Created by Neha on 02-12-2016.
 */

public class VitalsConfigDBAdapter {
    private static VitalsConfigDBAdapter vitalsConfigDBAdapter;

    private static Context context;
    private SQLiteDatabase database;

    private static String DEFAULT_SYSTEM = "DEFAULT_SYSTEM";
    private static String BMI = "BMI";
    private static String BP = "BP";
    private static String GLUCOSE = "GLUCOSE";
    private static String VITALS_CONFIG_TABLE = "VitalsConfig";

    public static String getVitalsConfigTable() {
        return VITALS_CONFIG_TABLE;
    }

    private VitalsConfigDBAdapter(Context context) {
        context = context;
    }

    public static VitalsConfigDBAdapter getInstance(Context ctx) {
        if (null == vitalsConfigDBAdapter) {
            synchronized (VitalsConfigDBAdapter.class) {
                if (vitalsConfigDBAdapter == null) {
                    context = ctx;
                    vitalsConfigDBAdapter = new VitalsConfigDBAdapter(ctx);
                    vitalsConfigDBAdapter.open();
                }
            }
        }
        return vitalsConfigDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String getCreateVitalsConfigTableQuery() {
        String query = "CREATE TABLE " + VITALS_CONFIG_TABLE + "("
                + DEFAULT_SYSTEM + " varchar(255),"
                + BMI + " BLOB,"
                + BP + " BLOB,"
                + GLUCOSE + " BLOB)";

        AppLog.d("CREATE " + VITALS_CONFIG_TABLE + " TABLE", query);
        return query;
    }



    public VitalsConfigMO fetchVitalsConfigMO() throws Exception {
        Cursor mCursor = database.query(true, VITALS_CONFIG_TABLE, null, null, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {

            VitalsConfigMO vitalsConfigMO = VitalsConfigMO.getInstance();
            String defaulSystem = mCursor.getString(mCursor.getColumnIndex(DEFAULT_SYSTEM));
            vitalsConfigMO.setDefaultSystem(defaulSystem);
            //converting blob to string

            byte[] blob = mCursor.getBlob(mCursor.getColumnIndex(BMI));
            if (blob != null) {
                String bmi = new String(blob);
                vitalsConfigMO.setBMI(new Vital(bmi));
            }

            byte[] blob1 = mCursor.getBlob(mCursor.getColumnIndex(BP));
            if(blob1!=null) {
                String bp = new String(blob1);
                vitalsConfigMO.setBloodPressure(new Vital(bp));
            }

            byte[] blob2 = mCursor.getBlob(mCursor.getColumnIndex(GLUCOSE));
            if(blob2!=null) {
                String glucose = new String(blob2);
                vitalsConfigMO.setGlucose(new Vital(glucose));
            }
            mCursor.close();
            return vitalsConfigMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public Vital fetchVital(String coloumn) throws Exception {
        Cursor mCursor = database.query(true, VITALS_CONFIG_TABLE, new String[]{coloumn}, null, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            byte[] blob = mCursor.getBlob(mCursor.getColumnIndex(coloumn));
            String vitalcoloumn = new String(blob);
            Vital coloumnVital = new Vital(vitalcoloumn);

            mCursor.close();
            return coloumnVital;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public Vital fetchBMI() {
        try {
            return fetchVital(BMI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vital fetchBP() {
        try {
            return fetchVital(BP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vital fetchGlucose() {
        try {
            return fetchVital(GLUCOSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private void addVitalToInsert(String PARAMETER, Vital vitalValue, ContentValues values) {
        try {
            values.put(PARAMETER, String.valueOf(vitalValue).getBytes());
        } catch (Exception e) {
            AppLog.e(e.toString(),"Cannot insert : "+PARAMETER,null);
        }

    }

    public void insertVitalConfig(VitalsConfigMO configMO) {

        try {
            clearTable();
            ContentValues values = new ContentValues();
            values.put(DEFAULT_SYSTEM, configMO.getDefaultSystem());
            addVitalToInsert(BMI,configMO.getBMI(),values);
            addVitalToInsert(BP,configMO.getBloodPressure(),values);
            addVitalToInsert(GLUCOSE,configMO.getGlucose(),values);
            long count = database.insert(VITALS_CONFIG_TABLE, null, values);
        } catch (Exception e) {

        }
    }

    private void clearTable() {
        try {
            database.delete(VITALS_CONFIG_TABLE, null, null);
        } catch (Exception e) {
            AppLog.d("VitalsConfigDBAdapter", "no data in table");
        }
    }
}
