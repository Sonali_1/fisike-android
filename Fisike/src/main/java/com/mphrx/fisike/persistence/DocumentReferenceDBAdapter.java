package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.Queue.UploadDocumentTaskManager;
import com.mphrx.fisike.Queue.UploadRecordQueue;
import com.mphrx.fisike.background.DownloadUploadedFileTask;
import com.mphrx.fisike.background.LoadContactsProfilePic;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.models.DocumentReferenceModel;

import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.ProfilePicTaskManager;
import com.mphrx.fisike_physician.network.request.DownloadFileRequest;
import com.mphrx.fisike_physician.utils.TextPattern;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by administrate on 1/25/2016.
 */
public class DocumentReferenceDBAdapter {

    private static DocumentReferenceDBAdapter documentReferenceDBAdapter;
    private Context context;
    private SQLiteDatabase database;

    private static final String DOCUMENT_REFERENCE_TABLE = "DocumentReference";

    private static String DOCUMENTID = "documentID";
    private static String DOCUMENTTYPE = "documentType";
    private static String DOCUMENTLOCALPATH = "documentLocalPath";
    private static String DOCUMENTMASTERIDENTIFIER = "documentMasterIdentifier";
    private static String PATIENTID = "patientID";
    private static String THUMBIMAGE = "thumbImage";
    private static String TIMESTAMP = "timestamp";
    private static String DOCUMENTUPLOADEDSTATUS = "documentUploadedStatus";
    private static String PERCENTAGE = "percentage";
    private static String REASON = "reason";
    private static String MIMETYPE = "mimeType";
    private static String ISDIRTY="isDirty";
    private static String NOOFATTACHMENTATTRIBUTES="noOfAttachmentAttributes";
    private static String SOURCENAME = "sourceName";


    public static String getDocumentReferenceTable() {
        return DOCUMENT_REFERENCE_TABLE;
    }

    public DocumentReferenceDBAdapter(Context context) {

        this.context = context;

    }

    public static DocumentReferenceDBAdapter getInstance(Context ctx) {

        if (null == documentReferenceDBAdapter) {

            synchronized (DocumentReferenceDBAdapter.class) {

                if (documentReferenceDBAdapter == null) {

                    documentReferenceDBAdapter = new DocumentReferenceDBAdapter(ctx);

                    documentReferenceDBAdapter.open();

                }

            }

        }

        return documentReferenceDBAdapter;

    }


    public void open() throws SQLException {

        database = DBHelper.getInstance(context);

    }

    public void updateDocumentReference(DocumentReferenceModel documentReferenceModel, ContentValues updatedValue) {
        try {
            DocumentReferenceModel existingObject = fetchDocumentReferenceOnFile(documentReferenceModel.getDocumentLocalPath() + "");
            if (existingObject != null) {
                database.update(DOCUMENT_REFERENCE_TABLE, updatedValue, DOCUMENTLOCALPATH + "='" + documentReferenceModel.getDocumentLocalPath()
                        + "'", null);
            } else {
                insertDocumentReference(documentReferenceModel);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void updateDocumentReferenceDigitized(int documentID, ContentValues updatedValue) {
        try {
            database.update(DOCUMENT_REFERENCE_TABLE, updatedValue, DOCUMENTID + "='" + documentID
                    + "'", null);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void updateDocumentReference(int documentId, ContentValues updatedValue) {
        try {
            database.update(DOCUMENT_REFERENCE_TABLE, updatedValue, DOCUMENTID + "='" + documentId
                    + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDocumentReference() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DOCUMENTUPLOADEDSTATUS, VariableConstants.DOCUMENT_STATUS_REDDY);
        contentValues.put(PERCENTAGE, 0);
        try {
            database.update(DOCUMENT_REFERENCE_TABLE, contentValues, DOCUMENTUPLOADEDSTATUS + "=" + VariableConstants.DOCUMENT_STATUS_IN_PROCESS,
                    null);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static String getISDIRTY() {
        return ISDIRTY;
    }

    public static String getNOOFATTACHMENTATTRIBUTES() {
        return NOOFATTACHMENTATTRIBUTES;
    }

    /**
     * Fetch document reference for particular documentPath
     *
     * @param documentLocalPath
     * @return
     * @throws Exception
     */
    public DocumentReferenceModel fetchDocumentReferenceOnFile(String documentLocalPath) throws Exception {
        Cursor mCursor = database.query(true, DOCUMENT_REFERENCE_TABLE, null, DOCUMENTLOCALPATH + " ='"
                + documentLocalPath + "'", null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
            documentReferenceModel.setDocumentID(mCursor.getInt(mCursor.getColumnIndex(DOCUMENTID)));
            documentReferenceModel.setDocumentType(mCursor.getString(mCursor.getColumnIndex(DOCUMENTTYPE)));
            documentReferenceModel.setDocumentLocalPath(mCursor.getString(mCursor.getColumnIndex(DOCUMENTLOCALPATH)));
            documentReferenceModel.setDocumentMasterIdentifier(mCursor.getString(mCursor.getColumnIndex(DOCUMENTMASTERIDENTIFIER)));
            documentReferenceModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));
            documentReferenceModel.setTimeStamp(mCursor.getString(mCursor.getColumnIndex(TIMESTAMP)));
            documentReferenceModel.setThumbImage(mCursor.getBlob(mCursor.getColumnIndex(THUMBIMAGE)));
            documentReferenceModel.setDocumentUploadedStatus(mCursor.getInt(mCursor.getColumnIndex(DOCUMENTUPLOADEDSTATUS)));
            documentReferenceModel.setPercentage(mCursor.getInt(mCursor.getColumnIndex(PERCENTAGE)));
            documentReferenceModel.setReason(mCursor.getString(mCursor.getColumnIndex(REASON)));
            documentReferenceModel.setMimeType(mCursor.getInt(mCursor.getColumnIndex(MIMETYPE)));
            documentReferenceModel.setDirty(mCursor.getInt(mCursor.getColumnIndex(ISDIRTY))==0?false:true);
            documentReferenceModel.setNoOfAttachment(mCursor.getInt(mCursor.getColumnIndex(NOOFATTACHMENTATTRIBUTES)));
            documentReferenceModel.setSourceName(mCursor.getString(mCursor.getColumnIndex(SOURCENAME)));

            mCursor.close();

            return documentReferenceModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * Fetch all the documents refrence
     *
     * @return
     * @throws Exception
     */
    public ArrayList<DocumentReferenceModel> fetchDocumentReference(Context context) throws Exception {
        Cursor mCursor = database.query(true, DOCUMENT_REFERENCE_TABLE, null, null, null, null, null,
                null, null);

        ArrayList<Integer> documentsToDownload = new ArrayList<>();

        ArrayList<DocumentReferenceModel> arryDocumentReferenceModels = new ArrayList<DocumentReferenceModel>();
        while (mCursor != null && mCursor.moveToNext()) {
            DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
            documentReferenceModel.setDocumentID(mCursor.getInt(mCursor.getColumnIndex(DOCUMENTID)));
            documentReferenceModel.setDocumentType(mCursor.getString(mCursor.getColumnIndex(DOCUMENTTYPE)));
            documentReferenceModel.setDocumentLocalPath(mCursor.getString(mCursor.getColumnIndex(DOCUMENTLOCALPATH)));
            documentReferenceModel.setDocumentMasterIdentifier(mCursor.getString(mCursor.getColumnIndex(DOCUMENTMASTERIDENTIFIER)));
            documentReferenceModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));
            documentReferenceModel.setTimeStamp(mCursor.getString(mCursor.getColumnIndex(TIMESTAMP)));
            documentReferenceModel.setThumbImage(mCursor.getBlob(mCursor.getColumnIndex(THUMBIMAGE)));
            documentReferenceModel.setDocumentUploadedStatus(mCursor.getInt(mCursor.getColumnIndex(DOCUMENTUPLOADEDSTATUS)));
            documentReferenceModel.setPercentage(mCursor.getInt(mCursor.getColumnIndex(PERCENTAGE)));
            documentReferenceModel.setReason(mCursor.getString(mCursor.getColumnIndex(REASON)));
            documentReferenceModel.setMimeType(mCursor.getInt(mCursor.getColumnIndex(MIMETYPE)));
            documentReferenceModel.setDirty(((mCursor.getInt(mCursor.getColumnIndex(ISDIRTY)) == 0) ? false : true));
            documentReferenceModel.setNoOfAttachment(mCursor.getInt(mCursor.getColumnIndex(NOOFATTACHMENTATTRIBUTES)));
            documentReferenceModel.setSourceName(mCursor.getString(mCursor.getColumnIndex(SOURCENAME)));


            if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_IMAGE && documentReferenceModel.getThumbImage() == null) {
                documentsToDownload.add(documentReferenceModel.getDocumentID());
                UploadDocumentTaskManager profilePicTaskManager = UploadDocumentTaskManager.getInstance();
                DownloadUploadedFileTask loadContactsProfilePic = new DownloadUploadedFileTask(context, documentReferenceModel.getDocumentID(), documentReferenceModel.getMimeType(), documentReferenceModel);
                profilePicTaskManager.addTask(loadContactsProfilePic);
                profilePicTaskManager.startTask();
            }
            arryDocumentReferenceModels.add(documentReferenceModel);
        }
        mCursor.close();
//        ArrayList<DocumentReferenceModel> tempDocumentReferenceModels = new ArrayList<DocumentReferenceModel>();
//        for (int i = arryDocumentReferenceModels.size() - 1; i >= 0; i--) {
//            tempDocumentReferenceModels.add(arryDocumentReferenceModels.get(i));
//        }
        return arryDocumentReferenceModels;
    }

    public boolean insertDocumentReference(DocumentReferenceModel documentReferenceModel) throws Exception {
        // we need to write a query for update as well
        ContentValues values = new ContentValues();
        values.put(DOCUMENTID, documentReferenceModel.getDocumentID());
        values.put(DOCUMENTTYPE, documentReferenceModel.getDocumentType());
        values.put(DOCUMENTLOCALPATH, documentReferenceModel.getDocumentLocalPath());
        values.put(DOCUMENTMASTERIDENTIFIER, documentReferenceModel.getDocumentMasterIdentifier());
        values.put(PATIENTID, documentReferenceModel.getPatientID());
        values.put(TIMESTAMP, documentReferenceModel.getTimeStamp());
        values.put(THUMBIMAGE, documentReferenceModel.getThumbImage());
        values.put(DOCUMENTUPLOADEDSTATUS, documentReferenceModel.getDocumentUploadedStatus());
        values.put(PERCENTAGE, documentReferenceModel.getPercentage());
        values.put(REASON, documentReferenceModel.getReason());
        values.put(MIMETYPE, documentReferenceModel.getMimeType());
        values.put(ISDIRTY,documentReferenceModel.isDirty()?1:0);
        values.put(NOOFATTACHMENTATTRIBUTES,documentReferenceModel.getNoOfAttachment());
        values.put(SOURCENAME, documentReferenceModel.getSourceName());
        long count = database.insert(DOCUMENT_REFERENCE_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    public void insertDocumentReferenceList(ArrayList<DocumentReferenceModel> documentReferenceModelList) {
        for (int i = 0; documentReferenceModelList != null && i < documentReferenceModelList.size(); i++) {
            try {
                DocumentReferenceModel documentReferenceModel = documentReferenceModelList.get(i);
                if (documentReferenceModel.isToUpdate())
                {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(getDOCUMENTUPLOADEDSTATUS(), documentReferenceModel.getDocumentUploadedStatus());
                    contentValues.put(getREASON(), documentReferenceModel.getReason());
                    contentValues.put(getISDIRTY(),documentReferenceModel.isDirty());
                    contentValues.put(getTHUMBIMAGE(),documentReferenceModel.getThumbImage());
                    contentValues.put(getTIMESTAMP(),documentReferenceModel.getTimeStamp());
                    contentValues.put(getNOOFATTACHMENTATTRIBUTES(),documentReferenceModel.getNoOfAttachment());
                    contentValues.put(getSOURCENAME(), documentReferenceModel.getSourceName());
                    if(documentReferenceModel.getDocumentLocalPath()!=null){
                        contentValues.put(getDOCUMENTLOCALPATH(),documentReferenceModel.getDocumentLocalPath());
                    }
                    updateDocumentReference(documentReferenceModel.getDocumentID(), contentValues);
                } else {
                    insertDocumentReference(documentReferenceModelList.get(i));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getDOCUMENTID() {
        return DOCUMENTID;
    }

    public static String getDOCUMENTTYPE() {
        return DOCUMENTTYPE;
    }

    public static String getDOCUMENTLOCALPATH() {
        return DOCUMENTLOCALPATH;
    }

    public static String getDOCUMENTMASTERIDENTIFIER() {
        return DOCUMENTMASTERIDENTIFIER;
    }

    public static String getPATIENTID() {
        return PATIENTID;
    }

    public static String getTHUMBIMAGE() {
        return THUMBIMAGE;
    }

    public static String getTIMESTAMP() {
        return TIMESTAMP;
    }

    public static String getDOCUMENTUPLOADEDSTATUS() {
        return DOCUMENTUPLOADEDSTATUS;
    }

    public static String getPERCENTAGE() {
        return PERCENTAGE;
    }

    public static String getREASON() {
        return REASON;
    }

    public static String getMIMETYPE() {
        return MIMETYPE;
    }

    public static String getSOURCENAME() {
        return SOURCENAME;
    }

    public static void setSOURCENAME(String SOURCENAME) {
        DocumentReferenceDBAdapter.SOURCENAME = SOURCENAME;
    }

    public static String createDocumentReferenceTableQuery() {
        String query = "CREATE TABLE " + DOCUMENT_REFERENCE_TABLE + " (" +
                DOCUMENTID + " INTEGER , " +
                DOCUMENTTYPE + " varchar(255), " +
                DOCUMENTLOCALPATH + " varchar(255), " +
                DOCUMENTMASTERIDENTIFIER + " varchar(255), " +
                PATIENTID + " INTEGER, " +
                TIMESTAMP + " varchar(255)," +
                THUMBIMAGE + " BLOB, " +
                DOCUMENTUPLOADEDSTATUS + " INTEGER, " +
                PERCENTAGE + " INTEGER, " +
                REASON + " varchar(255), " +
                SOURCENAME + " varchar(255), " +
                MIMETYPE + " INTEGER, "+
                ISDIRTY + " INTEGER, " +
                NOOFATTACHMENTATTRIBUTES+ " INTEGER " +")";
        AppLog.d("CREATE " + DOCUMENT_REFERENCE_TABLE, query);
        return query;
    }
}
