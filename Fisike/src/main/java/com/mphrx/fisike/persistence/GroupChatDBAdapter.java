package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.mo.GroupTextChatMO;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by administrate on 1/25/2016.
 */
public class GroupChatDBAdapter {

    private static GroupChatDBAdapter GroupChatDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;
    private static final String GROUP_TEXT_CHAT_MO_TABLE = "GroupTextChatMO";
    private static final String CHATCONVERSATIONMOPK = "chatConversationMOPK";
    private static final String ADMIN = "admin";
    private static final String IMAGEDATA = "imageData";
    private static final String PERSISTENCEKEY = "persistenceKey";


    public static String getCHATCONVERSATIONMOPK() {
        return CHATCONVERSATIONMOPK;
    }

    public static String getADMIN() {
        return ADMIN;
    }

    @Deprecated
    public static String getIMAGEDATA() {
        return null;//IMAGEDATA;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public GroupChatDBAdapter(Context context) {
        this.context = context;
    }

    public static String getGroupTextChatMoTable() {
        return GROUP_TEXT_CHAT_MO_TABLE;
    }

    public static GroupChatDBAdapter getInstance(Context ctx) {
        if (null == GroupChatDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (GroupChatDBAdapter == null) {

                    GroupChatDBAdapter = new GroupChatDBAdapter(ctx);
                    GroupChatDBAdapter.open();
                }
            }
        }
        return GroupChatDBAdapter;
    }

    public void open() throws SQLException
    {
        database = DBHelper.getInstance(context);
    }




    public boolean insert(GroupTextChatMO groupChatTextMo) throws Exception {
        ContentValues values = new ContentValues();
        values.put(CHATCONVERSATIONMOPK, groupChatTextMo.getChatConversationMOPK());
        values.put(PERSISTENCEKEY, groupChatTextMo.getPersistenceKey());
        //values.put(IMAGEDATA, groupChatTextMo.getImageData());

        if (groupChatTextMo.getAdmin() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out;
            try {
                out = new ObjectOutputStream(bytes);
                out.writeObject(groupChatTextMo.getAdmin());
                values.put(ADMIN, bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        long count = database.insert(GROUP_TEXT_CHAT_MO_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    public GroupTextChatMO fetchGroupTextChatMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, GROUP_TEXT_CHAT_MO_TABLE,null, PERSISTENCEKEY+" =" + persistenceKey, null, null,
                null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            GroupTextChatMO groupTextChatMO = new GroupTextChatMO();
            groupTextChatMO.setChatConversationMOPK(mCursor.getString(mCursor.getColumnIndex(CHATCONVERSATIONMOPK)));
            // groupTextChatMO.setAdmin(admin);

            byte[] adminArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(ADMIN));
            if (adminArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(adminArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> adminArray = (ArrayList<String>) ois.readObject();
                groupTextChatMO.setAdmin(adminArray);
            }
            //byte[] profilePic = mCursor.getBlob(mCursor.getColumnIndex(IMAGEDATA));
            //groupTextChatMO.setImageData(profilePic);
            groupTextChatMO.setPersistenceKey(Long.parseLong(persistenceKey));

            mCursor.close();
            return groupTextChatMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }



    public void updateGroupTextChatMO(long persistanceKey, ContentValues contentValues) {
        try {
            database.update(GROUP_TEXT_CHAT_MO_TABLE, contentValues, PERSISTENCEKEY + "=" + persistanceKey, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAdminKeyList(long persistanceKey,ArrayList<String> adminKeys)
    {
       ContentValues values=new ContentValues();
        if (adminKeys != null)
        {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out;
            try {
                out = new ObjectOutputStream(bytes);
                out.writeObject(adminKeys);
                values.put(ADMIN, bytes.toByteArray());
                }
            catch (IOException e)
                {
                e.printStackTrace();
                }
        }

        if(values!=null)
        updateGroupTextChatMO(persistanceKey,values);
    }


    public static String createGroupChatTableQuery() {
        String query="CREATE TABLE "+GROUP_TEXT_CHAT_MO_TABLE+" ( "+
                CHATCONVERSATIONMOPK+" varchar(255), "+
                ADMIN+" BLOB, "+
                IMAGEDATA+" BLOB, "+
                PERSISTENCEKEY+" varchar(255))";

        return query;


    }
}
