package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.models.MedicationModel;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteConstraintException;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by administrate on 1/22/2016.
 */
public class MedicationDBAdapter {

    private static MedicationDBAdapter medicationDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;
    private static final String MEDICATION_TABLE = "Medication";
    private static String MEDICATIONID="medicationID";
    private static String MEDICIANNAME="medicianName";

    public MedicationDBAdapter(Context context) {
        this.context = context;
    }

    public static MedicationDBAdapter getInstance(Context ctx) {
        if (null == medicationDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (medicationDBAdapter == null) {
                    medicationDBAdapter = new MedicationDBAdapter(ctx);
                    medicationDBAdapter.open();
                }
            }
        }
        return medicationDBAdapter;
    }

    public void open() throws SQLException
    {
        database = DBHelper.getInstance(context);
    }


    public MedicationModel fetchMedication(int medicationID) throws Exception {

        Cursor mCursor = database.query(true, MEDICATION_TABLE,null, MEDICATIONID+" =" + medicationID,
                null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            MedicationModel medicationModel = new MedicationModel();
            medicationModel.setMedicationID(mCursor.getInt(mCursor.getColumnIndex(MEDICATIONID)));
            medicationModel.setMedician(mCursor.getString(mCursor.getColumnIndex(MEDICIANNAME)));
            mCursor.close();
            return medicationModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public boolean createOrUpdateMedicationModel(MedicationModel medicationModel) throws Exception
    {
           ContentValues values = new ContentValues();
           values.put(MEDICATIONID, medicationModel.getMedicationID());
        values.put(MEDICIANNAME, medicationModel.getMedician());
           long count=0;
        try {
                database.insert(MEDICATION_TABLE, null, values);
            }
         catch (SQLiteConstraintException e)
        {
            e.printStackTrace();
            count=-1;
        }
        if (count == -1)
           {
               return false;
           }

           return true;
        }



    public static String createMedicationTableQuery()
    {
        String query="CREATE TABLE "+MEDICATION_TABLE+"("+
                MEDICATIONID+" INTEGER PRIMARY KEY,"+
                MEDICIANNAME+" VARCHAR(255) )";
        AppLog.d("CREATE TABLE" + MEDICATION_TABLE, query);
        return query;
    }

    public static String dropTable(){
        return "DROP TABLE IF EXISTS " + MEDICATION_TABLE;
    }

    public static String getMedicationTable() {
        return MEDICATION_TABLE;
    }

    public static String getMEDICATIONID() {
        return MEDICATIONID;
    }

    public static void setMEDICATIONID(String MEDICATIONID) {
        MedicationDBAdapter.MEDICATIONID = MEDICATIONID;
    }

    public static String getMEDICIANNAME() {
        return MEDICIANNAME;
    }

    public static void setMEDICIANNAME(String MEDICIANNAME) {
        MedicationDBAdapter.MEDICIANNAME = MEDICIANNAME;
    }
}
