package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Kailash on 12/29/2015.
 */
public class DiagnosticOrderDBAdapter {

    public static final String PATIENT_MOGO_ID = "PATIENT_MOGO_ID";
    private static DiagnosticOrderDBAdapter diagnosticOrderDbAdapter;
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;
    public static String ID = "ID";
    private static String PATIENTNAME = "patientName";

    private static String PHYSICIANNAME = "physicianName";
    private static String PATIENTID = "patientId";
    private static String PATIENTDOB = "patientDob";


    private static String PATIENTSEX = "patientSex";
    private static String ITEM = "item";
    private static String CATEGORY = "category";
    private static String ORDERID = "orderId";
    private static String ORDERINGPHYSICIANNAME = "orderingPhysicianName";
    private static String ORDERNO = "orderNo";
    private static String PERFORMINGLOCATIONID = "performingLocationId";
    public static String SCHEDULEDDATE = "scheduledDate";
    private static String STATUS = "status";
    private static String READSTATUS = "readStatus";
    private static String RECORDSCEDULETIMESTAMP = "recordSceduleTimeStamp";
    private static String ORGANISATION_ID = "organisationId";
    public static final String PATIENT_RECORD_TABLE = "PatientRecords";
    private static final String LABNAME = "labName";
    private static final String ACCESSION_NUMBER = "accessionNumber";
    private static final String DIAGNOSTICREPORT = "diagnosticReport";
    public static final String PERFORMINGlOCATIONNAME = "performingLocationName";
    public static final String PATIENT_PHONE_NUMBER = "patientPhoneNumber";
    public static final String PATIENT_EMAIL_ID = "patientEmailId";
    public static final String PATIENT_DECEASED = "patientDeceased";
    public static final String PATIENT_DECEASED_DATE = "patientDeceasedDate";
    public static final String ORDER_KEY = "orderKeyElement";

    public DiagnosticOrderDBAdapter(Context context) {
        this.context = context;
    }

    public static String getPatientRecordTABLE() {
        return PATIENT_RECORD_TABLE;
    }


    public static DiagnosticOrderDBAdapter getInstance(Context ctx) {
        if (null == diagnosticOrderDbAdapter) {
            synchronized (DiagnosticOrderDBAdapter.class) {
                if (diagnosticOrderDbAdapter == null) {
                    diagnosticOrderDbAdapter = new DiagnosticOrderDBAdapter(ctx);
                    diagnosticOrderDbAdapter.open();
                }
            }
        }
        return diagnosticOrderDbAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String createPatientRecordsTableQuery() {
        String query = "CREATE TABLE " + PATIENT_RECORD_TABLE + "(" +
                ID + " varchar(10) PRIMARY KEY, " +
                PATIENTNAME + " varchar(255), " +
                PATIENTID + " varchar(255), " +
                PATIENTDOB + " varchar(255), " +
                PATIENTSEX + " varchar(255), " +
                ITEM + " varchar(255), " +
                CATEGORY + " varchar(255), " +
                ORDERID + " varchar(255), " +
                ORDERINGPHYSICIANNAME + " varchar(255), " +
                ORDERNO + " varchar(255), " +
                PERFORMINGLOCATIONID + " varchar(255), " +
                SCHEDULEDDATE + " varchar(255), " +
                STATUS + " varchar(255), " +
                READSTATUS + " INTEGER, " +
                ORGANISATION_ID + " varchar(255), " +
                RECORDSCEDULETIMESTAMP + " LONG DEFAULT 0, " +
                PHYSICIANNAME + " varchar(255), " +
                LABNAME + " varchar(255)," +
                ACCESSION_NUMBER + " varchar(255)," +
                DIAGNOSTICREPORT + " BLOB," +
                PERFORMINGlOCATIONNAME + " varchar(255), " +
                PATIENT_PHONE_NUMBER + " varchar(255), " +
                PATIENT_EMAIL_ID + " vachar(255), " +
                PATIENT_DECEASED + "INT, " +
                PATIENT_DECEASED_DATE + " varchar(255)," +
                PATIENT_MOGO_ID + " varchar(255) ," +
                ORDER_KEY + " varchar(255) )";

        AppLog.d("CREATE " + PATIENT_RECORD_TABLE, query);
        return query;
    }


    /**
     * This method creates or updates the array of DiagnosticOrder
     *
     * @param arrayDiagnosticOrders
     * @return
     * @throws Exception
     */
    public void insertDiagnosticOrderArray(ArrayList<DiagnosticOrder> arrayDiagnosticOrders) throws Exception {
        for (int index = 0; index < arrayDiagnosticOrders.size(); index++) {
            DiagnosticOrder diagnosticOrders = arrayDiagnosticOrders.get(index);
            try {
                insertDiagnosticOrder(diagnosticOrders);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create or update  a diagnostic order in db
     *
     * @param diagnosticOrder
     * @return
     */
    public boolean insertDiagnosticOrder(DiagnosticOrder diagnosticOrder) {
        try {
            ContentValues values = new ContentValues();
            values.put(ID, diagnosticOrder.getId());
            values.put(PATIENTNAME, diagnosticOrder.getPatientName());
            values.put(PATIENTID, diagnosticOrder.getPatientId());
            values.put(PATIENTDOB, diagnosticOrder.getPatientDob());
            values.put(PATIENTSEX, diagnosticOrder.getPatientSex());
            values.put(ITEM, diagnosticOrder.getItem());
            values.put(CATEGORY, diagnosticOrder.getCategory());
            values.put(ORDERID, diagnosticOrder.getOrderId());
            values.put(ORDERINGPHYSICIANNAME, diagnosticOrder.getOrderingPhysicianName());
            values.put(ORDERNO, diagnosticOrder.getOrderNo());
            values.put(PERFORMINGLOCATIONID, diagnosticOrder.getPerformingLocationId());
            values.put(SCHEDULEDDATE, diagnosticOrder.getScheduledDate());
            values.put(STATUS, diagnosticOrder.getStatus());
            values.put(READSTATUS, diagnosticOrder.getReadStatus());
            values.put(RECORDSCEDULETIMESTAMP, diagnosticOrder.getRecordSceduleTimeStamp());
            values.put(ORGANISATION_ID, diagnosticOrder.getOrganisationId());
            values.put(PHYSICIANNAME, diagnosticOrder.getPhysicianName());
            values.put(LABNAME, diagnosticOrder.getLabName());
            values.put(ACCESSION_NUMBER, diagnosticOrder.getAccessionNumber());
            values.put(PERFORMINGlOCATIONNAME, diagnosticOrder.getPerformingLocationName());
            if (null != diagnosticOrder.getDiagnosticReportList()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(diagnosticOrder.getDiagnosticReportList());
                values.put(DIAGNOSTICREPORT, bytes.toByteArray());
            }
            values.put(PATIENT_PHONE_NUMBER, diagnosticOrder.getPatientPhoneNumber());
            values.put(PATIENT_EMAIL_ID, diagnosticOrder.getPatientEmailId());
            values.put(PATIENT_DECEASED, diagnosticOrder.isPatientDeceased() ? 1 : 0);
            values.put(PATIENT_DECEASED_DATE, diagnosticOrder.getPatientDeceasedDate());
            values.put(PATIENT_MOGO_ID, diagnosticOrder.getPatientMogoId());
            values.put(ORDER_KEY, diagnosticOrder.getOrderKey());
            long count = database.insert(PATIENT_RECORD_TABLE, null, values);
            if (count != -1) {
                return true;
            } else {
                return UpdateDiagnosticOrder(diagnosticOrder);

            }
        } catch (Exception e) {

        }
        return false;
    }


    /**
     * This method will fetch a all diagonstic order from the db
     *
     * @return
     * @throws Exception
     */
    public LinkedHashMap<String, DiagnosticOrder> fetchDiagnosticOrderArray() throws Exception {
        Cursor mCursor = database.query(true, PATIENT_RECORD_TABLE, null, null, null, null, null, null, null);

        LinkedHashMap<String, DiagnosticOrder> arrayDiagnosticOrder = new LinkedHashMap<String, DiagnosticOrder>();

        if (mCursor == null) {
            return null;
        }
        try {
            while (mCursor.moveToNext()) {
                DiagnosticOrder diagnosticOrder = new DiagnosticOrder();
                diagnosticOrder.setId(mCursor.getString(mCursor.getColumnIndex(ID)));
                diagnosticOrder.setPatientName(mCursor.getString(mCursor.getColumnIndex(PATIENTNAME)));
                diagnosticOrder.setPatientId(mCursor.getString(mCursor.getColumnIndex(PATIENTID)));
                diagnosticOrder.setPatientDob(mCursor.getString(mCursor.getColumnIndex(PATIENTDOB)));
                diagnosticOrder.setPatientSex(mCursor.getString(mCursor.getColumnIndex(PATIENTSEX)));
                diagnosticOrder.setItem(mCursor.getString(mCursor.getColumnIndex(ITEM)));
                diagnosticOrder.setCategory(mCursor.getString(mCursor.getColumnIndex(CATEGORY)));
                diagnosticOrder.setOrderId(mCursor.getString(mCursor.getColumnIndex(ORDERID)));
                diagnosticOrder.setOrderingPhysicianName(mCursor.getString(mCursor.getColumnIndex(ORDERINGPHYSICIANNAME)));
                diagnosticOrder.setOrderNo(mCursor.getString(mCursor.getColumnIndex(ORDERNO)));
                diagnosticOrder.setPerformingLocationId(mCursor.getString(mCursor.getColumnIndex(PERFORMINGLOCATIONID)));
                diagnosticOrder.setScheduledDate(mCursor.getString(mCursor.getColumnIndex(SCHEDULEDDATE)));
                diagnosticOrder.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
                diagnosticOrder.setReadStatus(mCursor.getInt(mCursor.getColumnIndex(READSTATUS)));
                diagnosticOrder.setRecordSceduleTimeStamp(mCursor.getLong(mCursor.getColumnIndex(RECORDSCEDULETIMESTAMP)));
                diagnosticOrder.setOrganisationId(mCursor.getString(mCursor.getColumnIndex(ORGANISATION_ID)));
                diagnosticOrder.setPhysicianName(mCursor.getString(mCursor.getColumnIndex(PHYSICIANNAME)));
                diagnosticOrder.setLabName(mCursor.getString(mCursor.getColumnIndex(LABNAME)));
                diagnosticOrder.setAccessionNumber(mCursor.getString(mCursor.getColumnIndex(ACCESSION_NUMBER)));
                byte[] reportListBytes = mCursor.getBlob(mCursor.getColumnIndex(DIAGNOSTICREPORT));
                diagnosticOrder.setPerformingLocationName(mCursor.getString(mCursor.getColumnIndex(PERFORMINGlOCATIONNAME)));
                if (reportListBytes != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(reportListBytes);
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    ArrayList<DiagnosticReport> reportList = (ArrayList<DiagnosticReport>) ois.readObject();
                    diagnosticOrder.setDiagnosticReportList(reportList);
                }
                diagnosticOrder.setPatientPhoneNumber(mCursor.getString(mCursor.getColumnIndex(PATIENT_PHONE_NUMBER)));
                diagnosticOrder.setPatientEmailId(mCursor.getString(mCursor.getColumnIndex(PATIENT_EMAIL_ID)));
                diagnosticOrder.setPatientDeceased(mCursor.getInt(mCursor.getColumnIndex(PATIENT_DECEASED)) == 1 ? true : false);
                diagnosticOrder.setPatientDeceasedDate(mCursor.getString(mCursor.getColumnIndex(PATIENT_DECEASED_DATE)));
                diagnosticOrder.setOrderKey(mCursor.getString(mCursor.getColumnIndex(ORDER_KEY)));
                arrayDiagnosticOrder.put(diagnosticOrder.getId(), diagnosticOrder);
            }
            return arrayDiagnosticOrder;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }
        return null;
    }

    public static String getOrganisationId() {
        return ORGANISATION_ID;
    }

    public static void setOrganisationId(String organisationId) {
        ORGANISATION_ID = organisationId;
    }

    public static String getLABNAME() {
        return LABNAME;
    }

    public static String getAccessionNumber() {
        return ACCESSION_NUMBER;
    }

    public static String getDIAGNOSTICREPORT() {
        return DIAGNOSTICREPORT;
    }

    private boolean UpdateDiagnosticOrder(DiagnosticOrder diagnosticOrder) {
        try {
            ContentValues values = new ContentValues();
            values.put(ID, diagnosticOrder.getId());
            values.put(PATIENTNAME, diagnosticOrder.getPatientName());
            values.put(PATIENTID, diagnosticOrder.getPatientId());
            values.put(PATIENTDOB, diagnosticOrder.getPatientDob());
            values.put(PATIENTSEX, diagnosticOrder.getPatientSex());
            values.put(ITEM, diagnosticOrder.getItem());
            values.put(CATEGORY, diagnosticOrder.getCategory());
            values.put(ORDERID, diagnosticOrder.getOrderId());
            values.put(ORDERINGPHYSICIANNAME, diagnosticOrder.getOrderingPhysicianName());
            values.put(ORDERNO, diagnosticOrder.getOrderNo());
            values.put(PERFORMINGLOCATIONID, diagnosticOrder.getPerformingLocationId());
            values.put(SCHEDULEDDATE, diagnosticOrder.getScheduledDate());
            values.put(STATUS, diagnosticOrder.getStatus());
            values.put(READSTATUS, diagnosticOrder.getReadStatus());
            values.put(RECORDSCEDULETIMESTAMP, diagnosticOrder.getRecordSceduleTimeStamp());
            values.put(ORGANISATION_ID, diagnosticOrder.getOrganisationId());
            values.put(PHYSICIANNAME, diagnosticOrder.getPhysicianName());
            values.put(LABNAME, diagnosticOrder.getLabName());
            values.put(ACCESSION_NUMBER, diagnosticOrder.getAccessionNumber());
            if (null != diagnosticOrder.getDiagnosticReportList()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(diagnosticOrder.getDiagnosticReportList());
                values.put(DIAGNOSTICREPORT, bytes.toByteArray());
            }
            values.put(PATIENT_PHONE_NUMBER, diagnosticOrder.getPatientPhoneNumber());
            values.put(PATIENT_EMAIL_ID, diagnosticOrder.getPatientEmailId());
            values.put(PATIENT_DECEASED, diagnosticOrder.isPatientDeceased() ? 1 : 0);
            values.put(PATIENT_DECEASED_DATE, diagnosticOrder.getPatientDeceasedDate());
            values.put(PATIENT_MOGO_ID, diagnosticOrder.getPatientMogoId());
            values.put(ORDER_KEY, diagnosticOrder.getOrderKey());

            int count = database.update(PATIENT_RECORD_TABLE, values, ID + "=" + diagnosticOrder.getId(), null);
            if (count != 0)
                return true;

        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public void deleteRecords() {
        if (database == null)
            open();
        database.delete(PATIENT_RECORD_TABLE, null, null);
    }
}
