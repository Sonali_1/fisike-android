package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by administrate on 1/25/2016.
 */
public class AttchmentDBAdapter {

    private static AttchmentDBAdapter AttchmentDBAdapter;
    public static final String AttachmentMO_TABLE = "AttachmentMo";
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static String ATTACHMENTID = "attachmentId";
    private static String ATTACHMENTTYPE = "attachmentType";
    private static String ATTACHMENTPATH = "attachmentPath";
    private static String CHATCONVERSATIONMOPKEY = "chatConversationMOPKey";
    private static String CHATMESSAGEMOPKEY = "chatMessageMOPKey";
    private static String RECEIVERUSERNAME = "receiverUserName";
    private static String SENDERUSERNAME = "senderUserName";
    private static String STATUS = "status";
    private static String PERSISTENCEKEY = "persistenceKey";
    private static String FILESIZE = "fileSize";
    private static String IMAGETHUMBNAIL = "imageThumbnail";
    private static String IMAGETHUMBNAILPATH = "imageThumbnailPath";
    private static String IMAGERECORDSYNCSTATUS = "imageRecordSyncStatus";

    public AttchmentDBAdapter(Context context) {
        this.context = context;
    }

    public static AttchmentDBAdapter getInstance(Context ctx) {
        if (null == AttchmentDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (AttchmentDBAdapter == null) {
                    AttchmentDBAdapter = new AttchmentDBAdapter(ctx);
                    AttchmentDBAdapter.open();
                }
            }
        }
        return AttchmentDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String createAttachmentTableQuery() {
        String query = "CREATE TABLE " + AttachmentMO_TABLE + " ( " +
                ATTACHMENTID + " varchar(255), " +
                ATTACHMENTTYPE + " varchar(255), " +
                ATTACHMENTPATH + " varchar(255), " +
                CHATCONVERSATIONMOPKEY + " varchar(255), " +
                CHATMESSAGEMOPKEY + " varchar(255), " +
                RECEIVERUSERNAME + " varchar(255), " +
                SENDERUSERNAME + " varchar(255), " +
                STATUS + " varchar(255), " +
                PERSISTENCEKEY + " varchar(255), " +
                FILESIZE + " LONG, " +
                IMAGETHUMBNAIL + " BLOB, " +
                IMAGETHUMBNAILPATH + " varchar(255), " +
                IMAGERECORDSYNCSTATUS + " INTEGER )";

        AppLog.d("CREATE " + AttachmentMO_TABLE, query);
        return query;
    }

    /**
     * This method gives you a list of messages that are in a particular conversation for attachments
     *
     * @param convPersistenceKey
     * @return
     */
    // TODO - need to check the extra attachment mo in db to delete
    public ArrayList<AttachmentMo> fetchMessagesInConversationForAttachments(long convPersistenceKey) {
        ArrayList<AttachmentMo> messages;

        try {
            messages = new ArrayList<AttachmentMo>();

            // Cursor mCursor = database.rawQuery("Select chatMessage.persistenceKey from " + AttachmentMO_TABLE + " attachment, " +
            // ChatMessageMO_TABLE + " chatMessage, " + ChatConversationMO_TABLE
            // + " conversation where conversation.convPersistenceKey=" + convPersistenceKey + " and attachment.status=" +
            // TextConstants.ATTACHMENT_STATUS_IN_PROGRESS, null);

            // Cursor mCursor = database.query(true, AttachmentMO_TABLE, new String[] { "attachmentId", "attachmentType", "attachmentPath",
            // "chatConversationMOPKey", "chatMessageMOPKey",
            // "receiverUserName", "senderUserName", "status", "persistenceKey" }, "chatConversationMOPKey =" + convPersistenceKey + " and (status=" +
            // TextConstants.ATTACHMENT_STATUS_IN_PROGRESS
            // + " or status=" + TextConstants.ATTACHMENT_STATUS_DOWNLOADING + ")", null, null, null, null, null);

            Cursor mCursor = database
                    .rawQuery(
                            "Select attachmentId, attachmentType, attachmentPath, chatConversationMOPKey, chatMessageMOPKey, receiverUserName, senderUserName, status, persistenceKey, imageThumbnail, fileSize, imageThumbnailPath, imageRecordSyncStatus from "
                                    + AttachmentMO_TABLE
                                    + " where chatConversationMOPKey = \""
                                    + convPersistenceKey
                                    + "\"  and ( status = \""
                                    + TextConstants.ATTACHMENT_STATUS_IN_PROGRESS
                                    + "\" or status = \""
                                    + TextConstants.ATTACHMENT_STATUS_WAITING
                                    + "\" or status = \"" + TextConstants.ATTACHMENT_STATUS_DOWNLOADING + "\" )", null);

            if (null != mCursor) {
                while (mCursor.moveToNext()) {
                    AttachmentMo attachmentMO = new AttachmentMo();

                    attachmentMO.setAttachmentId(mCursor.getString(0));
                    attachmentMO.setAttachmentType(mCursor.getString(1));
                    attachmentMO.setAttachmentPath(mCursor.getString(2));
                    attachmentMO.setChatConversationMOPKey(mCursor.getString(3));
                    attachmentMO.setChatMessageMOPKey(mCursor.getString(4));
                    attachmentMO.setReceiverUserName(mCursor.getString(5));
                    attachmentMO.setSenderUserName(mCursor.getString(6));
                    // Added retrieval of the conversation persistence key
                    attachmentMO.setStatus(mCursor.getString(7));
                    attachmentMO.setPersistenceKey(mCursor.getLong(8));
                    attachmentMO.setImageThumbnail(mCursor.getBlob(9));
                    attachmentMO.setFileSize(mCursor.getLong(10));
                    attachmentMO.setImageThumbnailPath(mCursor.getString(11));
                    attachmentMO.setImageRecordSyncStatus(mCursor.getInt(12));

                    // Adding this to the list
                    messages.add(attachmentMO);
                }

                mCursor.close();
            }
        } catch (Exception e) {
            messages = null;
        }

        return messages;
    }

    public static String getAttachmentMO_TABLE() {
        return AttachmentMO_TABLE;
    }

    public static String getATTACHMENTID() {

        return ATTACHMENTID;
    }

    public static String getATTACHMENTTYPE() {
        return ATTACHMENTTYPE;
    }

    public static String getATTACHMENTPATH() {
        return ATTACHMENTPATH;
    }

    public static String getCHATCONVERSATIONMOPKEY() {
        return CHATCONVERSATIONMOPKEY;
    }

    public static String getCHATMESSAGEMOPKEY() {
        return CHATMESSAGEMOPKEY;
    }

    public static String getRECEIVERUSERNAME() {
        return RECEIVERUSERNAME;
    }

    public static String getSENDERUSERNAME() {
        return SENDERUSERNAME;
    }

    public static String getSTATUS() {
        return STATUS;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getFILESIZE() {
        return FILESIZE;
    }

    public static String getIMAGETHUMBNAIL() {
        return IMAGETHUMBNAIL;
    }

    public static String getIMAGETHUMBNAILPATH() {
        return IMAGETHUMBNAILPATH;
    }

    public static String getIMAGERECORDSYNCSTATUS() {
        return IMAGERECORDSYNCSTATUS;
    }


    /**
     * This method will fetch a QuestionActivityMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public AttachmentMo fetchAttachmentMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, AttachmentMO_TABLE, new String[]{"attachmentId", "attachmentType", "attachmentPath",
                "chatConversationMOPKey", "chatMessageMOPKey", "receiverUserName", "senderUserName", "status", "persistenceKey", "fileSize",
                "imageThumbnail", "imageThumbnailPath", "imageRecordSyncStatus"}, "persistenceKey =" + persistenceKey, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            AttachmentMo attachmentMo = new AttachmentMo();
            attachmentMo.setAttachmentId(mCursor.getString(0));
            attachmentMo.setAttachmentType(mCursor.getString(1));
            attachmentMo.setAttachmentPath(mCursor.getString(2));
            attachmentMo.setChatConversationMOPKey(mCursor.getString(3));
            attachmentMo.setChatMessageMOPKey(mCursor.getString(4));
            attachmentMo.setReceiverUserName(mCursor.getString(5));
            attachmentMo.setSenderUserName(mCursor.getString(6));
            attachmentMo.setStatus(mCursor.getString(7));
            attachmentMo.setPersistenceKey(Long.parseLong(mCursor.getString(8)));
            attachmentMo.setFileSize(mCursor.getLong(9));
            attachmentMo.setImageThumbnail(mCursor.getBlob(10));
            attachmentMo.setImageThumbnailPath(mCursor.getString(11));
            attachmentMo.setImageRecordSyncStatus(mCursor.getInt(12));

            mCursor.close();
            return attachmentMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public void updateAttachmentObject(String attachmentPath, ContentValues updatedValue) {
        try {
            int update = database.update(AttachmentMO_TABLE, updatedValue, "attachmentPath" + "='" + attachmentPath + "'", null);
            System.out.println("DBAdapter.updateAttachmentObject()---" + update);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    /**
     * This method creates or updates the AttachmentMO
     *
     * @param attachmentMO
     * @return
     * @throws Exception
     */
    public synchronized boolean insert(AttachmentMo attachmentMO) throws Exception {


        ContentValues values = new ContentValues();
        values.put("attachmentId", attachmentMO.getAttachmentId());
        values.put("attachmentType", attachmentMO.getAttachmentType());
        values.put("attachmentPath", attachmentMO.getAttachmentPath());
        values.put("chatConversationMOPKey", attachmentMO.getChatConversationMOPKey());
        values.put("chatMessageMOPKey", attachmentMO.getChatMessageMOPKey());
        values.put("receiverUsername", attachmentMO.getReceiverUserName());
        values.put("senderUserName", attachmentMO.getSenderUserName());
        values.put("status", attachmentMO.getStatus());
        values.put("persistenceKey", attachmentMO.getPersistenceKey());
        values.put("fileSize", attachmentMO.getFileSize());
        values.put("imageThumbnail", attachmentMO.getImageThumbnail());
        values.put("imageThumbnailPath", attachmentMO.getImageThumbnailPath());
        values.put("imageRecordSyncStatus", attachmentMO.getImageRecordSyncStatus());

        long count = database.insert(AttachmentMO_TABLE, null, values);
        if (count == -1) {
            // LogManager.getInstance().writeToLog("Error inserting row for AttachmentyMO for persistenceKey:" + attachmentMO.getPersistenceKey());
            return false;
        }
        return true;
    }


    /**
     * This method will fetch a QuestionActivityMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public AttachmentMo fetchAttachmentMO_ChatMessageMo(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, AttachmentMO_TABLE, new String[]{"attachmentId", "attachmentType", "attachmentPath",
                        "chatConversationMOPKey", "chatMessageMOPKey", "receiverUserName", "senderUserName", "status", "persistenceKey", "fileSize",
                        "imageThumbnail", "imageThumbnailPath", "imageRecordSyncStatus"}, "chatMessageMOPKey =" + persistenceKey, null, null, null, null,
                null);

        if (mCursor != null && mCursor.moveToFirst()) {
            AttachmentMo attachmentMo = new AttachmentMo();
            attachmentMo.setAttachmentId(mCursor.getString(0));
            attachmentMo.setAttachmentType(mCursor.getString(1));
            attachmentMo.setAttachmentPath(mCursor.getString(2));
            attachmentMo.setChatConversationMOPKey(mCursor.getString(3));
            attachmentMo.setChatMessageMOPKey(mCursor.getString(4));
            attachmentMo.setReceiverUserName(mCursor.getString(5));
            attachmentMo.setSenderUserName(mCursor.getString(6));
            attachmentMo.setStatus(mCursor.getString(7));
            attachmentMo.setPersistenceKey(Long.parseLong(mCursor.getString(8)));
            attachmentMo.setFileSize(mCursor.getLong(9));
            attachmentMo.setImageThumbnail(mCursor.getBlob(10));
            attachmentMo.setImageThumbnailPath(mCursor.getString(11));
            attachmentMo.setImageRecordSyncStatus(mCursor.getInt(12));

            mCursor.close();
            return attachmentMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public AttachmentMo fetchAttachmentMO_attachmentPath(String attachmentPath) throws Exception {
        Cursor mCursor = database.query(true, AttachmentMO_TABLE, new String[]{"attachmentId", "attachmentType", "attachmentPath",
                        "chatConversationMOPKey", "chatMessageMOPKey", "receiverUserName", "senderUserName", "status", "persistenceKey", "fileSize",
                        "imageThumbnail", "imageThumbnailPath", "imageRecordSyncStatus"}, "attachmentPath = '" + attachmentPath + "'", null, null, null, null,
                null);

        if (mCursor != null && mCursor.moveToFirst()) {
            AttachmentMo attachmentMo = new AttachmentMo();
            attachmentMo.setAttachmentId(mCursor.getString(0));
            attachmentMo.setAttachmentType(mCursor.getString(1));
            attachmentMo.setAttachmentPath(mCursor.getString(2));
            attachmentMo.setChatConversationMOPKey(mCursor.getString(3));
            attachmentMo.setChatMessageMOPKey(mCursor.getString(4));
            attachmentMo.setReceiverUserName(mCursor.getString(5));
            attachmentMo.setSenderUserName(mCursor.getString(6));
            attachmentMo.setStatus(mCursor.getString(7));
            attachmentMo.setPersistenceKey(Long.parseLong(mCursor.getString(8)));
            attachmentMo.setFileSize(mCursor.getLong(9));
            attachmentMo.setImageThumbnail(mCursor.getBlob(10));
            attachmentMo.setImageThumbnailPath(mCursor.getString(11));
            attachmentMo.setImageRecordSyncStatus(mCursor.getInt(12));

            mCursor.close();
            return attachmentMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public void updateAttachmentMo(long persistanceKey, ContentValues contentValues) {
        try {
            database.update(AttachmentMO_TABLE, contentValues, PERSISTENCEKEY + "=" + persistanceKey, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAttachmentMo_AttachmentPatg(String attachmentPath, ContentValues contentValues) {
        try {
            database.update(AttachmentMO_TABLE, contentValues, ATTACHMENTPATH + "='" + attachmentPath + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
