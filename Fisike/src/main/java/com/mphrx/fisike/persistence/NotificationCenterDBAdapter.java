package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.Encounter;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by administrate on 1/25/2016.
 */
public class NotificationCenterDBAdapter {
    private static final String NOTIFICATION_CENTER_TABLE = "NotificationCenter";

    private static NotificationCenterDBAdapter notificationCenterDBAdapter;
    private Context context;
    private SQLiteDatabase database;

    private static final String _ID = "_Id";
    private static final String NOTIFICATIONTYPE = "notificationType";
    private static final String MEDICIANNAME = "medicianName";
    private static final String MEDICATIONTIME = "medicationTime";
    private static final String MEDICATIONDATE = "medicationDate";
    private static final String MEDICIANQUANTITY = "medicianQuantity";
    private static final String ENCOUNTERID = "encounterId";
    private static final String DOCUMENTID = "documentId";
    private static final String DOCUMENTSTATUS = "documentStatus";
    private static final String DOCUMENTTYPE = "documentType";
    private static final String ISREAD = "isRead";
    private static final String UNREADCOUNT = "unreadCount";
    private static final String REFID = "refId";
    private static final String ORGANIZATION_ID = "organizationId";
    private static final String ORGANIZATION_NAME = "organizationName";


    private NotificationCenterDBAdapter(Context context) {
        this.context = context;
    }

    public static NotificationCenterDBAdapter getInstance(Context ctx) {
        if (null == notificationCenterDBAdapter) {
            synchronized (NotificationCenterDBAdapter.class) {
                if (notificationCenterDBAdapter == null) {
                    notificationCenterDBAdapter = new NotificationCenterDBAdapter(ctx);
                    notificationCenterDBAdapter.open();
                }
            }
        }
        return notificationCenterDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    public static String createNotificationCenterTableQuery() {
        String query = "CREATE TABLE " + NOTIFICATION_CENTER_TABLE + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NOTIFICATIONTYPE + " varchar(255), " +
                MEDICIANNAME + " varchar(255), " +
                MEDICATIONTIME + " varchar(255), " +
                MEDICATIONDATE + " varchar(255), " +
                MEDICIANQUANTITY + " varchar(255), " +
                ENCOUNTERID + " varchar(255), " +
                DOCUMENTID + " varchar(255), " +
                DOCUMENTSTATUS + " varchar(255), " +
                DOCUMENTTYPE + " varchar(255), " +
                ISREAD + " INTEGER, " +
                UNREADCOUNT + " INTEGER, " +
                REFID + " varchar(255)," +
                ORGANIZATION_ID + " varchar(255), " +
                ORGANIZATION_NAME + " varchar(255) )";
        AppLog.d("CREATE" + NOTIFICATION_CENTER_TABLE, query);
        return query;

    }


    /**
     * Fetch Notification
     *
     * @throws Exception
     */
    public ArrayList<NotificationCenter> fetchNotificationCenter() throws Exception {
        ArrayList<NotificationCenter> arrayNotificationCenterSaveResponse = new ArrayList<NotificationCenter>();

        Cursor mCursor = database.query(true, NOTIFICATION_CENTER_TABLE, null, null, null, null, null, null, null);
        try {
            // _Id notificationType medicianName medicationTime medicationDate medicianQuantity encounterId documentId documentStatus, documentType
            if (mCursor == null && mCursor.getCount() < 1) {
                mCursor.close();
                return arrayNotificationCenterSaveResponse;
            }
            mCursor.moveToLast();
            do {
                NotificationCenter notificationCenterResponse = new NotificationCenter();
                notificationCenterResponse.set_Id(mCursor.getInt(mCursor.getColumnIndex(_ID)));
                notificationCenterResponse.setNotificationType(mCursor.getString(mCursor.getColumnIndex(NOTIFICATIONTYPE)));
                notificationCenterResponse.setMedicianName(mCursor.getString(mCursor.getColumnIndex(MEDICIANNAME)));
                notificationCenterResponse.setMedicationTime(mCursor.getString(mCursor.getColumnIndex(MEDICATIONTIME)));
                notificationCenterResponse.setMedicationDate(mCursor.getString(mCursor.getColumnIndex(MEDICATIONDATE)));
                notificationCenterResponse.setMedicianQuantity(mCursor.getString(mCursor.getColumnIndex(MEDICIANQUANTITY)));
                notificationCenterResponse.setEncounterId(mCursor.getString(mCursor.getColumnIndex(ENCOUNTERID)));
                notificationCenterResponse.setDocumentId(mCursor.getString(mCursor.getColumnIndex(DOCUMENTID)));
                notificationCenterResponse.setDocumentStatus(mCursor.getString(mCursor.getColumnIndex(DOCUMENTSTATUS)));
                notificationCenterResponse.setDocumentType(mCursor.getString(mCursor.getColumnIndex(DOCUMENTTYPE)));
                notificationCenterResponse.setRead(mCursor.getInt(mCursor.getColumnIndex(ISREAD)) == 0 ? false : true);
                notificationCenterResponse.setUnreadCount(mCursor.getColumnIndex(UNREADCOUNT));
                notificationCenterResponse.setRefId(mCursor.getString(mCursor.getColumnIndex(REFID)));
                notificationCenterResponse.setOrganizationId(mCursor.getString(mCursor.getColumnIndex(ORGANIZATION_ID)));
                notificationCenterResponse.setOrganizationName(mCursor.getString(mCursor.getColumnIndex(ORGANIZATION_NAME)));
                arrayNotificationCenterSaveResponse.add(notificationCenterResponse);
            } while (mCursor.moveToPrevious());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }

        return arrayNotificationCenterSaveResponse;
    }

    /**
     * Insert notification entries in db
     *
     * @param notificationCenter
     * @return
     * @throws Exception
     */
    public boolean insertNotificationCenter(NotificationCenter notificationCenter) throws Exception {
        int delete = 0;
        try {
            if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_MEDICATION)) {
                delete = database.delete(NOTIFICATION_CENTER_TABLE, NOTIFICATIONTYPE + "=\"" + notificationCenter.getNotificationType() + "\"", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //deleting MRN Notification only when new MRN Notification comes otherwise not
        if(notificationCenter.getNotificationType().equals(VariableConstants.MRN_NOTIFICATION))
            deleteMRNNotification();

        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        int preferencesCount = preferences.getInt(VariableConstants.ALARM_COUNT_UNREAD, 0);
//      long notifivationCount = net.sqlcipher.DatabaseUtils.queryNumEntries(database, NOTIFICATION_CENTER_TABLE) - delete;
        boolean medicationStatus = preferences.getBoolean(VariableConstants.ALARM_COUNT_MEDICATION, false);
        int notifivationCount = medicationStatus ? preferencesCount - delete : preferencesCount;
        ContentValues values = new ContentValues();
        values.put(DOCUMENTID, notificationCenter.getDocumentId());
        values.put(DOCUMENTSTATUS, notificationCenter.getDocumentStatus());
        values.put(DOCUMENTTYPE, notificationCenter.getDocumentType());
        values.put(NOTIFICATIONTYPE, notificationCenter.getNotificationType());
        values.put(MEDICIANNAME, notificationCenter.getMedicianName());
        values.put(MEDICATIONTIME, notificationCenter.getMedicationTime());
        values.put(MEDICATIONDATE, notificationCenter.getMedicationDate());
        values.put(MEDICIANQUANTITY, notificationCenter.getMedicianQuantity());
        values.put(ENCOUNTERID, notificationCenter.getEncounterId());
        values.put(ISREAD, notificationCenter.isRead() ? 1 : 0);
        values.put(UNREADCOUNT, ++notifivationCount);
        values.put(REFID, notificationCenter.getRefId());
        values.put(ORGANIZATION_ID, notificationCenter.getOrganizationId());
        values.put(ORGANIZATION_NAME, notificationCenter.getOrganizationName());
        long count = database.insert(NOTIFICATION_CENTER_TABLE, null, values);
        if (count == -1) {
            return false;
        }

        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(VariableConstants.ALARM_READ, false);
        edit.putInt(VariableConstants.ALARM_COUNT_UNREAD, (int) notifivationCount);
        edit.commit();

        return true;
    }

    public static String getNotificationCenterTable() {
        return NOTIFICATION_CENTER_TABLE;
    }


    public static String getOrganizationId() {
        return ORGANIZATION_ID;
    }

    public static String getOrganizationName() {
        return ORGANIZATION_NAME;
    }

    public void deleteMRNNotification()
    {
        try{
         database.delete(NOTIFICATION_CENTER_TABLE, NOTIFICATIONTYPE + "=\"" + VariableConstants.MRN_NOTIFICATION + "\"", null);
        }
        catch (Exception e)
        {
            AppLog.e(e.getMessage(),VariableConstants.MRN_NOTIFICATION,e);
        }
    }
}
