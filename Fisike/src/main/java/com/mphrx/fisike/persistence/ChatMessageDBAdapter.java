package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrate on 1/25/2016.
 */
public class ChatMessageDBAdapter {



    private static ChatMessageDBAdapter chatMessageDBAdapter;
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static String SENDERUSERNAME="senderUsername";
    private static String MESSAGETEXT="messageText";
    private static String LASTUPDATEDTIMEUTC="lastUpdatedTimeUTC";
    private static String SUBJECTTEXT="subjectText";
    private static String CONVPERSISTENCEKEY="convPersistenceKey";
    private static String PERSISTENCEKEY  ="persistenceKey";
    private static String SERVERRECEIVEDMSG="serverReceivedMsg";
    private static String RECIPIENTRECEIVEDMSG="recipientReceivedMsg";
    private static String MSGSTATUS="msgStatus";
    private static String MSGID="msgId";

    private static String ATTACHMENTID="attachmentID";
    private static String ATTACHMENTPKEY="attachmentPKey";
    private static String ATTACHMENTTYPE="attachmentType";
    private static String CHATMESSAGEMOTYPE="chatMessageMOType";
    private static String GROUPID="groupID";
    private static String SUBTYPE="subType";


    public static final String ChatMessageMO_TABLE = "ChatMessageMO";

    public ChatMessageDBAdapter(Context context) {
        this.context = context;
    }

    public static ChatMessageDBAdapter getInstance(Context ctx) {
        if (null == chatMessageDBAdapter) {
            synchronized (ChatMessageDBAdapter.class) {
                if (chatMessageDBAdapter == null) {
                    chatMessageDBAdapter = new ChatMessageDBAdapter(ctx);
                    chatMessageDBAdapter.open();
                }
            }
        }
        return chatMessageDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    public  static String createChatMessageTableQuery()
    {
        String query=" CREATE TABLE "+ChatMessageMO_TABLE+" ( "+
                SENDERUSERNAME+" varchar(255), "+
                MESSAGETEXT+" varchar(255),  "+
                LASTUPDATEDTIMEUTC+" LONG DEFAULT 0, "+
                SUBJECTTEXT+" varchar(255), "+
                PERSISTENCEKEY+" varchar(255), "+
                CONVPERSISTENCEKEY+" varchar(255), "+
                SERVERRECEIVEDMSG+" INT, "+
                RECIPIENTRECEIVEDMSG+" INT, "+
                MSGSTATUS+" int DEFAULT "+ DefaultConnection.MESSAGE_SENT+ ", "+
                MSGID+" varchar(255), "+
                ATTACHMENTID+" varchar(255), "+
                ATTACHMENTPKEY+" varchar(255), "+
                ATTACHMENTTYPE+" varchar(255), "+
                CHATMESSAGEMOTYPE+" INT, "+
                GROUPID+" varchar(255), "+
                SUBTYPE+" INT )";

        AppLog.d("CREATE"+ChatMessageMO_TABLE,query);
        return  query;
    }


    /**
     * This method creates or updates the chatMessageMO in the db
     *
     * @param chatMsgMO
     *
     * @return
     * @throws Exception
     */

        private int calCount()
        {
            Cursor mcursor=database.query(true, ChatMessageMO_TABLE, null, null, null, null, null, null, null);
            int size=0;
            while (mcursor.moveToNext())
                size=size+1;

            mcursor.close();
            return size;
        }
    public synchronized boolean insert(ChatMessageMO chatMsgMO) {
        try {
        AppLog.d("COUNT before ",calCount());

            ChatConversationDBAdapter.getInstance(context).updateIsChatInitiated(chatMsgMO.getConvPersistenceKey());

            ContentValues values = new ContentValues();

            // Fix to prevent any message being stored in the DB without being cleaned of delimeters
            String messageText = Utils.cleanMessageText(chatMsgMO.getMessageText());

            // Added the conversation persistence key
            values.put(CONVPERSISTENCEKEY, chatMsgMO.getConvPersistenceKey());

            // Added flags for the server and recipient getting the message
            values.put(SERVERRECEIVEDMSG, chatMsgMO.isServerReceivedMsg() ? 1 : 0);
            values.put(RECIPIENTRECEIVEDMSG, chatMsgMO.isRecipientReceivedMsg() ? 1 : 0);

            values.put(SENDERUSERNAME, chatMsgMO.getSenderUserId());
            values.put(MESSAGETEXT, messageText);
            values.put(LASTUPDATEDTIMEUTC, chatMsgMO.getLasUpdatedTimeUTC());
            values.put(SUBJECTTEXT, chatMsgMO.getSubjectText());
            values.put(PERSISTENCEKEY, chatMsgMO.getPersistenceKey());
            values.put(MSGSTATUS, chatMsgMO.getMsgStatus());
            values.put(MSGID, chatMsgMO.getMsgId());
            values.put(ATTACHMENTID, chatMsgMO.getAttachmentID());
            values.put(ATTACHMENTPKEY, chatMsgMO.getAttachmentPKey());
            values.put(ATTACHMENTTYPE, chatMsgMO.getAttachmentType());

            // new group chat related
            values.put(CHATMESSAGEMOTYPE, chatMsgMO.getChatMessageMOType());
            values.put(GROUPID, chatMsgMO.getGroupID());
            values.put(SUBTYPE, chatMsgMO.getSubType());

            long count = database.insert(ChatMessageMO_TABLE, null, values);
            AppLog.d("COUNT after ",calCount());
            if (count == -1) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public ChatMessageMO fetchMessageObjectFromMcursor(Cursor mCursor)
    {
        ChatMessageMO chatMsgMO = new ChatMessageMO();

        chatMsgMO.setSenderUserId(mCursor.getString(mCursor.getColumnIndex(SENDERUSERNAME)));
        chatMsgMO.setMessageText(mCursor.getString(mCursor.getColumnIndex(MESSAGETEXT)));
        chatMsgMO.setLastUpdatedTimeUTC(mCursor.getLong(mCursor.getColumnIndex(LASTUPDATEDTIMEUTC)));
        chatMsgMO.setSubjectText(mCursor.getString(mCursor.getColumnIndex(SUBJECTTEXT)));
        chatMsgMO.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
        chatMsgMO.setMsgStatus(mCursor.getInt(mCursor.getColumnIndex(MSGSTATUS)));
        chatMsgMO.setMsgId(mCursor.getString(mCursor.getColumnIndex(MSGID)));
        // Added retrieval of the conversation persistence key
        chatMsgMO.setConvPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(CONVPERSISTENCEKEY)));

        // Added retrieval of message flags
        chatMsgMO.setServerReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(SERVERRECEIVEDMSG)) == 1 ? true : false);
        chatMsgMO.setRecipientReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(RECIPIENTRECEIVEDMSG)) == 1 ? true : false);
        chatMsgMO.setAttachmentID(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTID)));
        chatMsgMO.setAttachmentPKey(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTPKEY)));
        chatMsgMO.setAttachmentType(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTTYPE)));
        chatMsgMO.setChatMessageMOType(mCursor.getInt(mCursor.getColumnIndex(CHATMESSAGEMOTYPE)));
        chatMsgMO.setGroupID(mCursor.getString(mCursor.getColumnIndex(GROUPID)));
        chatMsgMO.setSubType(mCursor.getInt(mCursor.getColumnIndex(SUBTYPE)));

        mCursor.close();
        return chatMsgMO;

    }

    /**
     * This method fetches the chatMessageMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */



    public ChatMessageMO fetchChatMessageMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, ChatMessageMO_TABLE, null, PERSISTENCEKEY+" =" + persistenceKey, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {

            ChatMessageMO chatMessageMO=fetchMessageObjectFromMcursor(mCursor);

            mCursor.close();
            return  chatMessageMO;
          } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * This db query fetches all the pending messages Checks by the msgStatus to see if it is MESSAGE_NOT_SENT
     *
     * @return
     * @throws Exception
     */
    public List<ChatMessageMO> fetchPendingChatMessages() {
        List<ChatMessageMO> pendingMessages;
        try {
            pendingMessages = new ArrayList<ChatMessageMO>();

            Cursor mCursor = database.query(true, ChatMessageMO_TABLE,null, MSGSTATUS+" =" + DefaultConnection.MESSAGE_NOT_SENT, null, null, null, null,
                    null);

            if (null != mCursor) {
                while (mCursor.moveToNext()) {
                    ChatMessageMO chatMsgMO = new ChatMessageMO();
                   // (mCursor.getColumnIndex(ID)));
                    chatMsgMO.setSenderUserId(mCursor.getString(mCursor.getColumnIndex(SENDERUSERNAME)));
                    chatMsgMO.setMessageText(mCursor.getString(mCursor.getColumnIndex(MESSAGETEXT)));
                    chatMsgMO.setLastUpdatedTimeUTC(mCursor.getLong(mCursor.getColumnIndex(LASTUPDATEDTIMEUTC)));
                    chatMsgMO.setSubjectText(mCursor.getString(mCursor.getColumnIndex(SUBJECTTEXT)));
                    chatMsgMO.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
                    chatMsgMO.setMsgStatus(mCursor.getInt(mCursor.getColumnIndex(MSGSTATUS)));
                    chatMsgMO.setMsgId(mCursor.getString(mCursor.getColumnIndex(MSGID)));
                    // Added retrieval of the conversation persistence key
                    chatMsgMO.setConvPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(CONVPERSISTENCEKEY)));

                    // Added retrieval of message flags
                    chatMsgMO.setServerReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(SERVERRECEIVEDMSG)) == 1 ? true : false);
                    chatMsgMO.setRecipientReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(RECIPIENTRECEIVEDMSG)) == 1 ? true : false);
                    chatMsgMO.setAttachmentID(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTID)));
                    chatMsgMO.setAttachmentPKey(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTPKEY)));
                    chatMsgMO.setAttachmentType(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTTYPE)));

                    // Adding this to the list
                    pendingMessages.add(chatMsgMO);
                }

                mCursor.close();
            }

        } catch (Exception e) {
            pendingMessages = null;
        }
        return pendingMessages;
    }

    /**
     * This method gives you a list of messages that are in a particular conversation
     *
     * @param convPersistenceKey
     * @return
     */
    public ArrayList<ChatMessageMO> fetchMessagesInConversation(long convPersistenceKey) {
        ArrayList<ChatMessageMO> messages;

        try {
            messages = new ArrayList<ChatMessageMO>();

            String sqlQuery = "SELECT t1."+SENDERUSERNAME+" , t1."+MESSAGETEXT+" , t1."+LASTUPDATEDTIMEUTC+" , t1."+SUBJECTTEXT+", t1."+
            PERSISTENCEKEY+", t1."+MSGSTATUS+", t1."+MSGID+", t1."+CONVPERSISTENCEKEY+", t1."+SERVERRECEIVEDMSG+", t1."+RECIPIENTRECEIVEDMSG+
            ", t1."+ATTACHMENTID+", t1."+ATTACHMENTPKEY+", t1."+ATTACHMENTTYPE+", t2."+AttchmentDBAdapter.getIMAGETHUMBNAIL()+", t2."+
            AttchmentDBAdapter.getIMAGETHUMBNAILPATH()+", t2."+AttchmentDBAdapter.getIMAGERECORDSYNCSTATUS()+", t2."+AttchmentDBAdapter.getATTACHMENTPATH()+" FROM "
                    + ChatMessageMO_TABLE + " t1 LEFT JOIN "+ AttchmentDBAdapter.getAttachmentMO_TABLE()
                    + " t2 ON t1."+PERSISTENCEKEY+" = t2."+AttchmentDBAdapter.getCHATMESSAGEMOPKEY()+" where "+CONVPERSISTENCEKEY+" =" + convPersistenceKey;

            Cursor mCursor = database.rawQuery(sqlQuery, null);

            if (null != mCursor) {
                while (mCursor.moveToNext()) {
                    ChatMessageMO chatMsgMO = new ChatMessageMO();

                    chatMsgMO.setSenderUserId(mCursor.getString(mCursor.getColumnIndex(SENDERUSERNAME)));
                    chatMsgMO.setMessageText(mCursor.getString(mCursor.getColumnIndex(MESSAGETEXT)));
                    chatMsgMO.setLastUpdatedTimeUTC(mCursor.getLong(mCursor.getColumnIndex(LASTUPDATEDTIMEUTC)));
                    chatMsgMO.setSubjectText(mCursor.getString(mCursor.getColumnIndex(SUBJECTTEXT)));
                    chatMsgMO.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
                    chatMsgMO.setMsgStatus(mCursor.getInt(mCursor.getColumnIndex(MSGSTATUS)));
                    chatMsgMO.setMsgId(mCursor.getString(mCursor.getColumnIndex(MSGID)));
                    // Added retrieval of the conversation persistence key
                    chatMsgMO.setConvPersistenceKey((mCursor.getColumnIndex(CONVPERSISTENCEKEY)));

                    // Added retrieval of message flags
                    chatMsgMO.setServerReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(SERVERRECEIVEDMSG)) == 1 ? true : false);
                    chatMsgMO.setRecipientReceivedMsg(mCursor.getInt(mCursor.getColumnIndex(RECIPIENTRECEIVEDMSG)) == 1 ? true : false);
                    chatMsgMO.setAttachmentID(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTID)));
                    chatMsgMO.setAttachmentPKey(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTPKEY)));
                    chatMsgMO.setAttachmentType(mCursor.getString(mCursor.getColumnIndex(ATTACHMENTTYPE)));
                    chatMsgMO.setImageThumb(mCursor.getBlob(13));
                    chatMsgMO.setImageThumbnailPath(mCursor.getString(14));
                    chatMsgMO.setImageRecordSyncStatus(mCursor.getInt(15));
                    chatMsgMO.setAttachmentPath(mCursor.getString(16));

                    // Adding this to the list
                    messages.add(chatMsgMO);
                }

                mCursor.close();
            }
        } catch (Exception e) {
            messages = null;
        }

        return messages;
    }


    /**
     * This will return all the messages that have the status send in the DB
     *
     * @return ArrayList<ChatMessageMO>
     */
    public ArrayList<ChatMessageMO> fetchMessagesStatusSend() {
        ArrayList<ChatMessageMO> messages;

        try {
            messages = new ArrayList<ChatMessageMO>();

            Cursor mCursor = database.query(true, ChatMessageMO_TABLE, null, MSGSTATUS+" =" + DefaultConnection.MESSAGE_SENT, null, null, null, null,
                    null);

            if (null != mCursor) {
                while (mCursor.moveToNext()) {
                    ChatMessageMO chatMsgMO = new ChatMessageMO();

                    chatMsgMO.setSenderUserId(mCursor.getString(0));
                    chatMsgMO.setMessageText(mCursor.getString(1));
                    chatMsgMO.setLastUpdatedTimeUTC(mCursor.getLong(2));
                    chatMsgMO.setSubjectText(mCursor.getString(3));
                    chatMsgMO.setPersistenceKey(mCursor.getLong(4));
                    chatMsgMO.setMsgStatus(mCursor.getInt(5));
                    chatMsgMO.setMsgId(mCursor.getString(6));
                    // Added retrieval of the conversation persistence key
                    chatMsgMO.setConvPersistenceKey(mCursor.getLong(7));

                    // Added retrieval of message flags
                    chatMsgMO.setServerReceivedMsg(mCursor.getInt(8) == 1 ? true : false);
                    chatMsgMO.setRecipientReceivedMsg(mCursor.getInt(9) == 1 ? true : false);
                    chatMsgMO.setAttachmentID(mCursor.getString(10));
                    chatMsgMO.setAttachmentPKey(mCursor.getString(11));
                    chatMsgMO.setAttachmentType(mCursor.getString(12));

                    // Adding this to the list
                    messages.add(chatMsgMO);
                }

                mCursor.close();
            }
        } catch (Exception e) {
            messages = null;
        }

        return messages;
    }

    public static String getChatMessageMO_TABLE() {
        return ChatMessageMO_TABLE;
    }

    /**
     * This method deletes a chatMessageMO from db
     *
     * @param chatMsgMO
     * @throws Exception
     */
    public void deleteChatMessageMO(ChatMessageMO chatMsgMO) throws Exception
    {
        database.delete(ChatMessageMO_TABLE, "persistenceKey" + "=" + chatMsgMO.getPersistenceKey(), null);
    }


    public static String getSENDERUSERNAME() {
        return SENDERUSERNAME;
    }

    public static String getMESSAGETEXT() {
        return MESSAGETEXT;
    }

    public static String getLASTUPDATEDTIMEUTC() {
        return LASTUPDATEDTIMEUTC;
    }

    public static String getSUBJECTTEXT() {
        return SUBJECTTEXT;
    }

    public static String getCONVPERSISTENCEKEY() {
        return CONVPERSISTENCEKEY;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getSERVERRECEIVEDMSG() {
        return SERVERRECEIVEDMSG;
    }

    public static String getRECIPIENTRECEIVEDMSG() {
        return RECIPIENTRECEIVEDMSG;
    }

    public static String getMSGSTATUS() {
        return MSGSTATUS;
    }

    public static String getMSGID() {
        return MSGID;
    }

    public static String getATTACHMENTID() {
        return ATTACHMENTID;
    }

    public static String getATTACHMENTPKEY() {
        return ATTACHMENTPKEY;
    }

    public static String getATTACHMENTTYPE() {
        return ATTACHMENTTYPE;
    }

    public static String getCHATMESSAGEMOTYPE() {
        return CHATMESSAGEMOTYPE;
    }

    public static String getGROUPID() {
        return GROUPID;
    }

    public static String getSUBTYPE() {
        return SUBTYPE;
    }

    public boolean updateChatMessageMo(long pk, ContentValues contentValues) {
        try {

            database.update(ChatMessageMO_TABLE, contentValues, PERSISTENCEKEY + "=" + pk, null);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
