package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.models.ContentResult;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by aastha on 5/3/2017.
 */

public class AgreementFileDBAdapter {

    private static AgreementFileDBAdapter agreementFileDBAdapter;
    private Context context;
    private SQLiteDatabase database;
    private Cursor mCursor;

    public static String getAgreementFileTable() {
        return AGREEMENT_FILE_TABLE;
    }
    private static final String AGREEMENT_FILE_TABLE = "AgreementDetailsFileMo";
    private static final String CONTENT = "contentResult";
    private static final String AGREEMENT_ID = "agreementId";
    private static final String READ_DATE = "readDate";
    private static final String LAST_UPDATED = "lastUpdated";

    public AgreementFileDBAdapter(Context context) {
        this.context = context;
    }

    public static AgreementFileDBAdapter getInstance(Context ctx) {
        if (null == agreementFileDBAdapter) {
            synchronized (UserFileAdapter.class) {
                if (agreementFileDBAdapter == null) {
                    agreementFileDBAdapter = new AgreementFileDBAdapter(ctx);
                    agreementFileDBAdapter.open();
                }
            }
        }
        return agreementFileDBAdapter;
    }
    public static String getAgreementFileQuery() {
        String query = "CREATE TABLE " + getAgreementFileTable()
                + " ( " + getAgreementId() + " integer PRIMARY KEY, "
                + getReadDate() + " varchar(255), "
                + getLastUpdated() + " varchar(255), "
                + getCONTENT() + " BLOB )";
        return query;
    }


    private void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public boolean insertAgreementData(AgreementModel agreementModel) {

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(getAgreementId(),agreementModel.getAgreementId());
            contentValues.put(getReadDate(),agreementModel.getReadDate());
            contentValues.put(getLastUpdated(),agreementModel.getLastUpdated());
            if(agreementModel.getContentResult()!=null)
            {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(agreementModel.getContentResult());
                contentValues.put(getCONTENT(), bytes.toByteArray());
            }
            long row = database.insert(getAgreementFileTable(), null, contentValues);
            if(row<0)
                database.update(getAgreementFileTable(), contentValues, getAgreementId() + " = " + agreementModel.getAgreementId(), null);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<AgreementModel> fetchAllAgreements() {
        Cursor mCursor = database.query(true, getAgreementFileTable(), null, null, null, null, null, null, null);
        ArrayList<AgreementModel> agreementModelArrayList = new ArrayList<AgreementModel>();
        if (mCursor == null) {
            return null;
        }
        try {
            while (mCursor.moveToNext()) {
                AgreementModel agreementModel = new AgreementModel();
                agreementModel.setAgreementId(Integer.parseInt(mCursor.getString(mCursor.getColumnIndex(getAgreementId()))));
                agreementModel.setReadDate(mCursor.getString(mCursor.getColumnIndex(getReadDate())));
                agreementModel.setLastUpdated(mCursor.getString(mCursor.getColumnIndex(getLastUpdated())));
                byte[] contentResults = mCursor.getBlob(mCursor.getColumnIndex(getCONTENT()));
                if (contentResults != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(contentResults);
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    ArrayList<ContentResult> contentResultsList = (ArrayList<ContentResult>) ois.readObject();
                    agreementModel.setContentResult(contentResultsList);
                }
                agreementModelArrayList.add(agreementModel);
            }
            return agreementModelArrayList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }
        return null;
    }

    public void updateAgreementData(int id,String readDate,String content,String updated,String language){
        Cursor mCursor = database.query(getAgreementFileTable(), new String[]{getCONTENT()}, getAgreementId()+ " = " + id, null, null, null, null);
        try {
            mCursor.moveToFirst();
            byte[] contentResult = mCursor.getBlob(mCursor.getColumnIndex(getCONTENT()));
            ArrayList<ContentResult> contentResults= null;
            if (contentResult != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(contentResult);
                ObjectInputStream ois = new ObjectInputStream(bais);
                contentResults = (ArrayList<ContentResult>) ois.readObject();
            }
            for(int i=0;i<contentResults.size();i++){
                if(contentResults.get(i).getLanguage().equals(language)){
                    contentResults.get(i).setContent(content);
                }
            }
            ContentValues values = new ContentValues();
            values.put(READ_DATE, readDate);
            values.put(LAST_UPDATED,updated);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(contentResults);
            values.put(CONTENT, bytes.toByteArray());
            long count = database.update(getAgreementFileTable(), values, getAgreementId() + "=" + id, null);
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }
    }


    public static String getCONTENT() {
        return CONTENT;
    }

    public static String getAgreementId() {
        return AGREEMENT_ID;
    }

    public static String getReadDate() {
        return READ_DATE;
    }

    public String fetchUpatedDate(int agreementId) {
        Cursor mCursor = database.query(true, getAgreementFileTable(), null, getAgreementId() + " = " + agreementId, null, null,
                null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            String updatedDate = mCursor.getString(mCursor.getColumnIndex(getLastUpdated()));
            mCursor.close();
            return updatedDate;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public static String getLastUpdated() {
        return LAST_UPDATED;
    }
}
