package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Members;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.gson.request.UserSettings;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.response.Authority;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;

/**
 * Created by administrate on 1/25/2016.
 */
public class UserDBAdapter {

    private static final String UserMO_TABLE1 = "UserMO";
    private static String USERNAME = "userName";
    private static String FIRSTNAME = "firstName";
    private static String LASTNAME = "lastName";
    private static String SALUTATION = "salutation";
    private static String PASSWORD = "password";
    private static String PERSISTENCEKEY = "persistenceKey";
    private static String EMAILADDRESS = "emailAddress";
    private static String PHONENUMBER = "phoneNumber";
    private static String COUNTRYCODE = "countryCode";
    private static String GENDER = "gender";
    private static String FISIKEUSERID = "fisikeUserId";
    private static String DATEOFBIRTH = "dateOfBirth";
    private static String USERTYPE = "userType";
    private static String HEIGHT_VALUE = "height_value";
    private static String HEIGHT_UNIT = "height_unit";
    private static String WEIGHT_VALUE = "weight_value";
    private static String WEIGHT_UNIT = "weight_unit";
    private static String DEVICEUID = "deviceUID";
    private static String DEVICEACTIVATIONDATE = "deviceActivationDate";
    private static String REGISTRATIONSTATUS = "registrationStatus";
    private static String ENABLED = "enabled";
    private static String PROFILEPIC = "profilePic";
    private static String OTPCODE = "otpCode";
    private static String ACCOUNTLOCKED = "accountLocked";
    private static String PHYSICIANID = "physicianId";
    private static String PATIENTID = "patientId";
    private static String AUTHORITIES = "authorities";
    private static String FISIKEUSERROLES = "fisikeUserRoles";
    private static String USERGROUPS = "userGroups";
    private static String FISIKEUSERTYPE = "fisikeUserType";
    private static String PROFILEPICUPDATESTATUS = "profilePicUpdateStatus";
    private static String SIGNUPSTATUS = "SignUpStatus";
    private static String SIGNUPTIME = "SignUpTime";
    private static String CHATCONVERSATIONKEYLIST = "chatConversationKeyList";
    private static String LASTAUTHENTICATEDTIME = "lastAuthenticatedTime";
    private static String CREATEDTIMEUTC = "createdTimeUTC";
    private static String HASSIGNEDUP = "hasSignedUp";
    private static String GROUPNAME = "groupName";
    private static String PRIVILEGEGROUP = "privilegeGroup";
    private static String ABOUTME = "aboutMe";
    private static String SPECIALTY = "specialty";
    private static String DESIGNATION = "designation";
    private static String SPECIALITIESARRAY = "specialitiesArray";
    private static String DESIGNATIONARRAY = "designationArray";
    private static String INVITEUSERPRIGROUPLIST = "inviteUserPriGroupList";
    private static String USERPRIVILEGES = "userPrivileges";
    private static String STATUS = "status";
    private static String STATUSMESSAGE = "statusMessage";
    private static String ORGANIZATIONID = "organizationId";
    private static String ALTERNATECONTACT = "alternateContact";
    private static String MARITALSTATUS = "maritalStatus";
    private static String ADDRESS = "address";
    private static String MPIN = "mPIN";
    private static String DEPENDENTPATIENTS = "dependentPatients";
    private static String USERLANGUAGE = "userLanguage";
    private static String AGREEMENTS = "agreements";
    private static String PENDING_DIAGNOSTICS = "pendingDiagnosticOrders";
    private static String TOKEN = "token";
    private static String COVERAGE = "coverage";
    /**
     * Not used now
     */
    private static String SECONDRYEMAIL = "secondryEmail";
    private static String SECONDRYEMAILCHANGETIME = "secondryEmailChangeTime";
    private static String SECONDRYEMAILVERIFICATIONSTATUS = "secondryEmailVerificationStatus";
    private static String VERIFICATIONCODE = "verificationCode";

    private static String IDENTIFIER = "IDENTIFIER";
    private static String EMAILLIST = "EMAILLIST";
    private static String PHONELIST = "PHONELIST";
    private static String MIDDLENAME = "MIDDLENAME";



    private static UserDBAdapter UserDBAdapter;

    private static Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private UserDBAdapter(Context context) {
        context = context;
    }

    public static UserDBAdapter getInstance(Context ctx) {
        if (null == UserDBAdapter) {
            synchronized (UserDBAdapter.class) {
                if (UserDBAdapter == null) {
                    context = ctx;
                    UserDBAdapter = new UserDBAdapter(ctx);
                    UserDBAdapter.open();
                }
            }
        }
        return UserDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    public boolean removeUser(UserMO userMO) throws Exception {

        UserMO existingUserMO = fetchUserMO(userMO.getPersistenceKey() + "");
        int count = -1;
        if (existingUserMO != null) {
            count = database.delete(UserMO_TABLE1, PERSISTENCEKEY + "=" + userMO.getPersistenceKey(), null);
        }

        if (count == -1)
            return false;
        else
            return true;
    }

    public static String getALTERNATECONTACT() {
        return ALTERNATECONTACT;
    }

    public static void setALTERNATECONTACT(String ALTERNATECONTACT) {
        com.mphrx.fisike.persistence.UserDBAdapter.ALTERNATECONTACT = ALTERNATECONTACT;
    }

    public UserMO fetchUserMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, UserMO_TABLE1, null, PERSISTENCEKEY + " = " + persistenceKey, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            UserMO userMO = new UserMO();
            userMO.setUsername(mCursor.getString(mCursor.getColumnIndex(USERNAME)));
            userMO.setFirstName(mCursor.getString(mCursor.getColumnIndex(FIRSTNAME)));
            userMO.setMiddleName(mCursor.getString(mCursor.getColumnIndex(MIDDLENAME)));
            userMO.setLastName(mCursor.getString(mCursor.getColumnIndex(LASTNAME)));
            userMO.setSalutation(mCursor.getString(mCursor.getColumnIndex(SALUTATION)));
            userMO.setRoles(mCursor.getString(mCursor.getColumnIndex(FISIKEUSERROLES)));
            userMO.setPersistenceKey(Long.parseLong(persistenceKey));
            userMO.setEmail(mCursor.getString(mCursor.getColumnIndex(EMAILADDRESS)));
            userMO.setId(mCursor.getLong(mCursor.getColumnIndex(FISIKEUSERID)));
            userMO.setDateOfBirth(mCursor.getString(mCursor.getColumnIndex(DATEOFBIRTH)));
            userMO.setEnabled(Boolean.parseBoolean(mCursor.getString(mCursor.getColumnIndex(ENABLED))));
            userMO.setDependentPatientsIds(mCursor.getString(mCursor.getColumnIndex(DEPENDENTPATIENTS)));
            Height height = new Height();
            height.setValue(mCursor.getString(mCursor.getColumnIndex(HEIGHT_VALUE)));
            height.setUnit(mCursor.getString(mCursor.getColumnIndex(HEIGHT_UNIT)));
            userMO.setHeight(height);
            Weight weight = new Weight();
            weight.setValue(mCursor.getString(mCursor.getColumnIndex(WEIGHT_VALUE)));
            weight.setUnit(mCursor.getString(mCursor.getColumnIndex(WEIGHT_UNIT)));
            userMO.setWeight(weight);
            String countrycode = mCursor.getString(mCursor.getColumnIndex(COUNTRYCODE));
            if (countrycode != null) {
                countrycode = countrycode.startsWith("+") ? countrycode : "+" + countrycode;
            } else {
                countrycode = "+91";
            }

            userMO.setCountryCode(countrycode);

            userMO.setPhoneNumber(mCursor.getString(mCursor.getColumnIndex(PHONENUMBER)));

            userMO.setGender(mCursor.getString(mCursor.getColumnIndex(GENDER)));
            userMO.setAccountLocked(Boolean.parseBoolean(mCursor.getString(mCursor.getColumnIndex(ACCOUNTLOCKED))));
            userMO.setPhysicianId(Long.parseLong(mCursor.getString(mCursor.getColumnIndex(PHYSICIANID))));
            userMO.setPatientId(Long.parseLong(mCursor.getString(mCursor.getColumnIndex(PATIENTID))));
            /*UserType usertype = new UserType();
            usertype.setName(mCursor.getString(mCursor.getColumnIndex(USERTYPE)));*/
            userMO.setUserType(mCursor.getString(mCursor.getColumnIndex(USERTYPE)));


            UserSettings userSettings = new UserSettings();
            userSettings.setUserLanguage(mCursor.getString(mCursor.getColumnIndex(USERLANGUAGE)));
            userMO.setUserSettings(userSettings);

            //byte[] profilePic = mCursor.getBlob(mCursor.getColumnIndex(PROFILEPIC));
            //userMO.setProfilePic(profilePic);
            userMO.setDeviceUID(mCursor.getString(mCursor.getColumnIndex(DEVICEUID)));
            userMO.setDeviceActivationDate(mCursor.getString(mCursor.getColumnIndex(DEVICEACTIVATIONDATE)));
            userMO.setRegistrationStatus(Boolean.parseBoolean(mCursor.getString(mCursor.getColumnIndex(REGISTRATIONSTATUS))));
            userMO.setPassword(mCursor.getString(mCursor.getColumnIndex(PASSWORD)));
            if (mCursor.getString(mCursor.getColumnIndex(PROFILEPICUPDATESTATUS)) != null && mCursor.getString(mCursor.getColumnIndex(PROFILEPICUPDATESTATUS)).equals("1"))
                userMO.setProfilePicUpdateStatus(true);
            else
                userMO.setProfilePicUpdateStatus(false);

            if (mCursor.getString(mCursor.getColumnIndex(SIGNUPSTATUS)) != null && mCursor.getString(mCursor.getColumnIndex(SIGNUPSTATUS)).equals("1"))
                userMO.setSignUpStatus(true);
            else
                userMO.setSignUpStatus(false);
            userMO.setSignUpTime(mCursor.getLong(mCursor.getColumnIndex(SIGNUPTIME)));

            byte[] chatConversationKeyListByte = mCursor.getBlob(mCursor.getColumnIndex(CHATCONVERSATIONKEYLIST));
            if (chatConversationKeyListByte != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(chatConversationKeyListByte);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> chatConversationKeyList = (ArrayList<String>) ois.readObject();
                userMO.setChatConversationKeyList(chatConversationKeyList);
            }
            userMO.setLastAuthenticatedTime(mCursor.getLong(mCursor.getColumnIndex(LASTAUTHENTICATEDTIME)));
            userMO.setCreatedTimeUTC(mCursor.getLong(mCursor.getColumnIndex(CREATEDTIMEUTC)));
            userMO.setGroupName(mCursor.getString(mCursor.getColumnIndex(GROUPNAME)));
            userMO.setPrivilegeGroup(mCursor.getString(mCursor.getColumnIndex(PRIVILEGEGROUP)));
            userMO.setAboutMe(mCursor.getString(mCursor.getColumnIndex(ABOUTME)));
            userMO.setSpecialty(mCursor.getString(mCursor.getColumnIndex(SPECIALTY)));
            userMO.setDesignation(mCursor.getString(mCursor.getColumnIndex(DESIGNATION)));
            byte[] specialitiesByteArray = mCursor.getBlob(mCursor.getColumnIndex(SPECIALITIESARRAY));
            if (specialitiesByteArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(specialitiesByteArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> specialitiesArray = (ArrayList<String>) ois.readObject();
                userMO.setSpecialitiesArray(specialitiesArray);
            }
            byte[] designationByteArray = mCursor.getBlob(mCursor.getColumnIndex(DESIGNATIONARRAY));
            if (designationByteArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(designationByteArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> designationArray = (ArrayList<String>) ois.readObject();
                userMO.setDesignationArray(designationArray);
            }
            byte[] inviteUserByteArray = mCursor.getBlob(mCursor.getColumnIndex(INVITEUSERPRIGROUPLIST));
            if (inviteUserByteArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(inviteUserByteArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                LinkedHashMap<String, String> inviteUserArray = (LinkedHashMap<String, String>) ois.readObject();
                userMO.setInviteUserPriGroupList(inviteUserArray);
            }
            byte[] userPrivilegesByteArray = mCursor.getBlob(mCursor.getColumnIndex(USERPRIVILEGES));
            if (userPrivilegesByteArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(userPrivilegesByteArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> userPrivilegesArray = (ArrayList<String>) ois.readObject();
                userMO.setUserPrivileges(userPrivilegesArray);
            }
            userMO.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
            userMO.setStatusMessage(mCursor.getString(mCursor.getColumnIndex(STATUSMESSAGE)));
            userMO.setHasSignedUp(Boolean.parseBoolean(mCursor.getString(mCursor.getColumnIndex(HASSIGNEDUP))));
            userMO.setAlternateContact(mCursor.getString(mCursor.getColumnIndex(ALTERNATECONTACT)));
            ArrayList<Members> members = new ArrayList<Members>();
            Members member = new Members();
            member.setOrganizationId(mCursor.getInt(mCursor.getColumnIndex(ORGANIZATIONID)));
            members.add(member);
            userMO.setMembers(members);
            byte[] addressByteArray = mCursor.getBlob(mCursor.getColumnIndex(ADDRESS));
            if (addressByteArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(addressByteArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Address> addressArray = (ArrayList<Address>) ois.readObject();
                userMO.setAddressUserMos(addressArray);
            }
            userMO.setMaritalStatus(mCursor.getString(mCursor.getColumnIndex(MARITALSTATUS)));
            userMO.setmPIN(mCursor.getString(mCursor.getColumnIndex(MPIN)));
            byte[] agreementsArray = mCursor.getBlob(mCursor.getColumnIndex(AGREEMENTS));
            if (agreementsArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(agreementsArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<AgreementModel> agreementModels = (ArrayList<AgreementModel>) ois.readObject();
                userMO.setAgreements(agreementModels);
            }
            byte[] authoritiesArray = mCursor.getBlob(mCursor.getColumnIndex(AUTHORITIES));
            if (authoritiesArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(authoritiesArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Authority> agreementModels = (ArrayList<Authority>) ois.readObject();
                userMO.setAuthorities(agreementModels);
            }
            userMO.setToken(mCursor.getString(mCursor.getColumnIndex(TOKEN)));
            byte[] coverageArray = mCursor.getBlob(mCursor.getColumnIndex(COVERAGE));
            if (coverageArray != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(coverageArray);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Coverage> coverages = (ArrayList<Coverage>) ois.readObject();
                userMO.setCoverage(coverages);
            }
            byte[] pendingDiagnostic = mCursor.getBlob(mCursor.getColumnIndex(PENDING_DIAGNOSTICS));
            if (pendingDiagnostic != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(pendingDiagnostic);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<Integer> pendingDiagnostics = (ArrayList<Integer>) ois.readObject();
                userMO.setPendingDiagnosticOrders(pendingDiagnostics);
            }


            byte[] identifier = mCursor.getBlob(mCursor.getColumnIndex(IDENTIFIER));
            if (identifier != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(identifier);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<FetchUpdateIdentifier> identifiers = (ArrayList<FetchUpdateIdentifier>) ois.readObject();
                userMO.setIdentifierArrayList(identifiers);
            }

            byte[] phoneList = mCursor.getBlob(mCursor.getColumnIndex(PHONELIST));
            if (phoneList != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(phoneList);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<FetchUpdateTelecom> phoneListArray = (ArrayList<FetchUpdateTelecom>) ois.readObject();
                userMO.setPhoneList(phoneListArray);
            }

            byte[] emailList = mCursor.getBlob(mCursor.getColumnIndex(EMAILLIST));
            if (emailList != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(emailList);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<FetchUpdateTelecom> emailArrayList = (ArrayList<FetchUpdateTelecom>) ois.readObject();
                userMO.setEmailList(emailArrayList);
            }

            mCursor.close();
            return userMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public void updateUserInfo(long persistanceKey, ContentValues contentValues) {
        try {
            database.update(UserMO_TABLE1, contentValues, PERSISTENCEKEY + "=" + persistanceKey, null);
            SharedPref.setIsCoverageUpdated(true);
            SharedPref.setIsUserInfoUpdated(true);
        } catch (Exception e) {
            SharedPref.setIsCoverageUpdated(false);
            e.printStackTrace();
        }
    }

    public String getLASTAUTHENTICATEDTIME() {
        return LASTAUTHENTICATEDTIME;
    }

    public String getUSERNAME() {

        return USERNAME;

    }


    public static String getDEPENDENTPATIENTS() {
        return DEPENDENTPATIENTS;
    }

    public static void setDEPENDENTPATIENTS(String DEPENDENTPATIENTS) {
        com.mphrx.fisike.persistence.UserDBAdapter.DEPENDENTPATIENTS = DEPENDENTPATIENTS;
    }

    public String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public String getLASTNAME() {
        return LASTNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public String getEMAILADDRESS() {
        return EMAILADDRESS;
    }

    public String getPHONENUMBER() {
        return PHONENUMBER;
    }

    public String getCOUNTRYCODE() {
        return COUNTRYCODE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public static String getMARITALSTATUS() {
        return MARITALSTATUS;
    }

    public static String getADDRESS() {
        return ADDRESS;
    }

    public String getDATEOFBIRTH() {
        return DATEOFBIRTH;
    }

    public String getHEIGHT_UNIT() {
        return HEIGHT_UNIT;
    }

    public String getHEIGHT_VALUE() {
        return HEIGHT_VALUE;
    }

    public String getWEIGHT_VALUE() {
        return WEIGHT_VALUE;
    }

    public static String getMPIN() {
        return MPIN;
    }

    public static String getUSERLANGUAGE() {
        return USERLANGUAGE;
    }

    public String getWEIGHT_UNIT() {
        return WEIGHT_UNIT;
    }

    @Deprecated
    public String getPROFILEPIC() {
        return null;//PROFILEPIC;
    }

    public boolean createOrUpdateUserMO(UserMO userMO) {
        UserMO existingUserMO = null;
        try {
            existingUserMO = fetchUserMO(userMO.getPersistenceKey() + "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (existingUserMO != null) {
            database.delete(UserMO_TABLE1, PERSISTENCEKEY + "=" + userMO.getPersistenceKey(), null);
        }

        try {
            ContentValues values = new ContentValues();
            values.put(USERNAME, userMO.getUsername());
            values.put(FIRSTNAME, userMO.getFirstName());
            values.put(MIDDLENAME,userMO.getMiddleName());
            values.put(LASTNAME, userMO.getLastName());
            values.put(SALUTATION, userMO.getSalutation());
            values.put(PERSISTENCEKEY, userMO.getPersistenceKey());
            values.put(EMAILADDRESS, userMO.getEmail());
            values.put(FISIKEUSERID, userMO.getId());
            values.put(DATEOFBIRTH, userMO.getDateOfBirth());

            values.put(ENABLED, userMO.isEnabled());
            if (userMO.getHeight() != null) {
                values.put(HEIGHT_VALUE, userMO.getHeight().getValue());
                values.put(HEIGHT_UNIT, userMO.getHeight().getUnit());
            }

            if (userMO.getWeight() != null) {
                values.put(WEIGHT_VALUE, userMO.getWeight().getValue());
                values.put(WEIGHT_UNIT, userMO.getWeight().getUnit());
            }
            values.put(COUNTRYCODE, userMO.getCountryCode());
            values.put(PHONENUMBER, userMO.getPhoneNumber());
            values.put(GENDER, userMO.getGender());
            values.put(ACCOUNTLOCKED, userMO.isAccountLocked());
            values.put(PHYSICIANID, userMO.getPhysicianId());
            values.put(PATIENTID, userMO.getPatientId());
            if (userMO.getUserType() != null)
                values.put(USERTYPE, userMO.getUserType());
            values.put(DEVICEUID, userMO.getDeviceUID());
            values.put(DEVICEACTIVATIONDATE, userMO.getDeviceActivationDate());
            values.put(REGISTRATIONSTATUS, userMO.isRegistrationStatus());
            values.put(FISIKEUSERROLES, userMO.getRoles());
            //values.put(PROFILEPIC, userMO.getProfilePic());
            values.put(PROFILEPICUPDATESTATUS, userMO.getProfilePicUpdateStatus());
            values.put(SIGNUPSTATUS, userMO.getSignUpStatus());
            values.put(SIGNUPTIME, userMO.getSignUpTime());
            values.put(PASSWORD, userMO.getPassword());
            values.put(HASSIGNEDUP, String.valueOf(userMO.getHasSignedUp()));

            if (userMO.getChatConversationKeyList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getChatConversationKeyList());
                values.put(CHATCONVERSATIONKEYLIST, bytes.toByteArray());
            }
            values.put(LASTAUTHENTICATEDTIME, userMO.getLastAuthenticatedTime());
            values.put(CREATEDTIMEUTC, userMO.getCreatedTimeUTC());
            values.put(GROUPNAME, userMO.getGroupName());
            values.put(PRIVILEGEGROUP, userMO.getPrivilegeGroup());
            values.put(ABOUTME, userMO.getAboutMe());
            values.put(SPECIALTY, userMO.getSpecialty());
            values.put(DESIGNATION, userMO.getDesignation());
            values.put(DEPENDENTPATIENTS, userMO.getDependentPatientsIds() != null ? userMO.getDependentPatientsIds() : "[]");
            if (userMO.getSpecialitiesArray() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getSpecialitiesArray());
                values.put(SPECIALITIESARRAY, bytes.toByteArray());
            }
            if (userMO.getDesignation() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getDesignation());
                values.put(DESIGNATIONARRAY, bytes.toByteArray());
            }
            if (userMO.getInviteUserPriGroupList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getInviteUserPriGroupList());
                values.put(INVITEUSERPRIGROUPLIST, bytes.toByteArray());
            }
            if (userMO.getUserPrivileges() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getUserPrivileges());
                values.put(USERPRIVILEGES, bytes.toByteArray());
            }
            values.put(STATUS, userMO.getStatus());
            values.put(STATUSMESSAGE, userMO.getStatusMessage());
            values.put(ALTERNATECONTACT, userMO.getAlternateContact());
            if (userMO.getMembers() != null && userMO.getMembers().size() > 0) {
                values.put(ORGANIZATIONID, userMO.getMembers().get(0).getOrganizationId());
            } else {
                values.put(ORGANIZATIONID, 0);
            }
            if (userMO.getAddressUserMos() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getAddressUserMos());
                values.put(ADDRESS, bytes.toByteArray());
            }
            try {
                UserSettings userSettings = userMO.getUserSettings();
                values.put(USERLANGUAGE, userMO.getUserSettings() != null ? (userMO.getUserSettings().getUserLanguage() != null ? userMO.getUserSettings().getUserLanguage() : TextConstants.DEFALUT_LANGUAGE_KEY) : TextConstants.DEFALUT_LANGUAGE_KEY);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            values.put(MARITALSTATUS, userMO.getMaritalStatus());
            values.put(MPIN, userMO.getmPIN());
            if (userMO.getAgreements() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getAgreements());
                try {
                    values.put(AGREEMENTS, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }
            if (userMO.getAuthorities() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getAuthorities());
                values.put(AUTHORITIES, bytes.toByteArray());
            }
            if (userMO.getPendingDiagnosticOrders() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getPendingDiagnosticOrders());
                try {
                    values.put(PENDING_DIAGNOSTICS, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

            if (userMO.getCoverage() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getCoverage());
                try {
                    values.put(COVERAGE, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }
            if (userMO.getPhoneList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getPhoneList());
                try {
                    values.put(PHONELIST, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

            if (userMO.getEmailList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getEmailList());
                try {
                    values.put(EMAILLIST, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

            if (userMO.getIdentifierArrayList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(userMO.getIdentifierArrayList());
                try {
                    values.put(IDENTIFIER, bytes.toByteArray());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

            if (userMO.getToken() != null) {
                try {
                    values.put(TOKEN, userMO.getToken());
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }
            long count = database.insert(UserMO_TABLE1, null, values);
            if (count > 0) {
                return true;
            }
            if (count == -1) {
                return false;
            }
        } catch (Exception ec) {
            ec.printStackTrace();
            return false;
        }

        SharedPref.setIsUserInfoUpdated(true);
        return true;
    }


    public boolean insert(UserMO userMO) throws Exception {

        ContentValues values = new ContentValues();
        values.put(USERNAME, userMO.getUsername());
        values.put(FIRSTNAME, userMO.getFirstName());
        values.put(MIDDLENAME,userMO.getMiddleName());
        values.put(LASTNAME, userMO.getLastName());
        values.put(SALUTATION, userMO.getSalutation());
        values.put(PERSISTENCEKEY, userMO.getPersistenceKey());
        values.put(EMAILADDRESS, userMO.getEmail());
        values.put(FISIKEUSERID, userMO.getId());
        values.put(DATEOFBIRTH, userMO.getDateOfBirth());

        values.put(ENABLED, userMO.isEnabled());
        if (userMO.getHeight() != null) {
            values.put(HEIGHT_VALUE, userMO.getHeight().getValue());
            values.put(HEIGHT_UNIT, userMO.getHeight().getUnit());
        }

        if (userMO.getWeight() != null) {
            values.put(WEIGHT_VALUE, userMO.getWeight().getValue());
            values.put(WEIGHT_UNIT, userMO.getWeight().getUnit());
        }
        values.put(COUNTRYCODE, userMO.getCountryCode());
        values.put(PHONENUMBER, userMO.getPhoneNumber());
        values.put(GENDER, userMO.getGender());
        values.put(ACCOUNTLOCKED, userMO.isAccountLocked());
        values.put(PHYSICIANID, userMO.getPhysicianId());
        values.put(PATIENTID, userMO.getPatientId());
        if (userMO.getUserType() != null)
            values.put(USERTYPE, userMO.getUserType());
        values.put(DEVICEUID, userMO.getDeviceUID());
        values.put(DEVICEACTIVATIONDATE, userMO.getDeviceActivationDate());
        values.put(REGISTRATIONSTATUS, userMO.isRegistrationStatus());
        values.put(FISIKEUSERROLES, userMO.getRoles());
        values.put(PROFILEPIC, userMO.getProfilePic());
        values.put(PROFILEPICUPDATESTATUS, userMO.getProfilePicUpdateStatus());
        values.put(SIGNUPSTATUS, userMO.getSignUpStatus());
        values.put(SIGNUPTIME, userMO.getSignUpTime());
        values.put(PASSWORD, userMO.getPassword());
        values.put(HASSIGNEDUP, String.valueOf(userMO.getHasSignedUp()));

        if (userMO.getChatConversationKeyList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getChatConversationKeyList());
            values.put(CHATCONVERSATIONKEYLIST, bytes.toByteArray());
        }
        values.put(LASTAUTHENTICATEDTIME, userMO.getLastAuthenticatedTime());
        values.put(CREATEDTIMEUTC, userMO.getCreatedTimeUTC());
        values.put(GROUPNAME, userMO.getGroupName());
        values.put(PRIVILEGEGROUP, userMO.getPrivilegeGroup());
        values.put(ABOUTME, userMO.getAboutMe());
        values.put(SPECIALTY, userMO.getSpecialty());
        values.put(DESIGNATION, userMO.getDesignation());
        if (userMO.getSpecialitiesArray() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getSpecialitiesArray());
            values.put(SPECIALITIESARRAY, bytes.toByteArray());
        }
        if (userMO.getDesignation() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getDesignation());
            values.put(DESIGNATIONARRAY, bytes.toByteArray());
        }
        if (userMO.getInviteUserPriGroupList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getInviteUserPriGroupList());
            values.put(INVITEUSERPRIGROUPLIST, bytes.toByteArray());
        }
        if (userMO.getUserPrivileges() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getUserPrivileges());
            values.put(USERPRIVILEGES, bytes.toByteArray());
        }
        values.put(STATUS, userMO.getStatus());
        values.put(STATUSMESSAGE, userMO.getStatusMessage());
        values.put(ALTERNATECONTACT, userMO.getAlternateContact());
        if (userMO.getMembers() != null && userMO.getMembers().size() > 0) {
            values.put(ORGANIZATIONID, userMO.getMembers().get(0).getOrganizationId());
        } else {
            values.put(ORGANIZATIONID, 0);
        }
        if (userMO.getAddressUserMos() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getAddressUserMos());
            values.put(ADDRESS, bytes.toByteArray());
        }
        try {
            UserSettings userSettings = userMO.getUserSettings();
            values.put(USERLANGUAGE, userMO.getUserSettings() != null ? (userMO.getUserSettings().getUserLanguage() != null ? userMO.getUserSettings().getUserLanguage() : TextConstants.DEFALUT_LANGUAGE_KEY) : TextConstants.DEFALUT_LANGUAGE_KEY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        values.put(MARITALSTATUS, userMO.getMaritalStatus());
        values.put(DEPENDENTPATIENTS, userMO.getDependentPatientsIds() != null ? userMO.getDependentPatientsIds() : "[]");
        values.put(MPIN, userMO.getmPIN());
        if (userMO.getAuthorities() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getAuthorities());
            values.put(AUTHORITIES, bytes.toByteArray());
        }
        if (userMO.getAgreements() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getAgreements());
            try {
                values.put(AGREEMENTS, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }
        if (userMO.getPendingDiagnosticOrders() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getPendingDiagnosticOrders());
            try {
                values.put(PENDING_DIAGNOSTICS, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }

        if (userMO.getCoverage() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getCoverage());
            try {
                values.put(COVERAGE, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }
        if (userMO.getPhoneList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getPhoneList());
            try {
                values.put(PHONELIST, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }

        if (userMO.getEmailList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getEmailList());
            try {
                values.put(EMAILLIST, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }

        if (userMO.getIdentifierArrayList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(userMO.getIdentifierArrayList());
            try {
                values.put(IDENTIFIER, bytes.toByteArray());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }

        if (userMO.getToken() != null) {
            try {
                values.put(TOKEN, userMO.getToken());
            } catch (Exception ec) {
                ec.printStackTrace();
            }
        }
        long count = database.insert(UserMO_TABLE1, null, values);

        if (count == -1) {
            return false;
        }
        SharedPref.setIsUserInfoUpdated(true);
        return true;
    }

    public static String getUserMO_TABLE() {
        return UserMO_TABLE1;
    }

    public String getSALUTATION() {
        return SALUTATION;
    }

    public void updateSalutation(long persistanceKey, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SALUTATION, value);
        updateUserInfo(persistanceKey, contentValues);

    }

    public void updateHasSignedUp(long persistanceKey, boolean value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HASSIGNEDUP, value);
        updateUserInfo(persistanceKey, contentValues);

    }

    public String getHASSIGNEDUP() {
        return HASSIGNEDUP;
    }

    public String getSTATUSMESSAGE() {
        return STATUSMESSAGE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void updateConversationKeyList(long persistanceKey, ArrayList<String> chatConversationKeyList) {
        try {
            ContentValues contentValues = new ContentValues();
            if (chatConversationKeyList != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatConversationKeyList);
                contentValues.put(CHATCONVERSATIONKEYLIST, bytes.toByteArray());
                updateUserInfo(persistanceKey, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public void updateProfilePic(long persistenceKey, byte[] bitmapdata) {
        //ContentValues values=new ContentValues();
        //values.put(PROFILEPIC,bitmapdata);
        //updateUserInfo(persistenceKey,values);
    }

    public String getPROFILEPICUPDATESTATUS() {
        return PROFILEPICUPDATESTATUS;
    }


    public static String getPendingDiagnostics() {
        return PENDING_DIAGNOSTICS;
    }

    public static String getTOKEN() {
        return TOKEN;
    }

    public static String getCOVERAGE() {
        return COVERAGE;
    }

    public void updateLastAuthenticatedTime(long persistenceKey, long lastAuthenticatedTime) {
        ContentValues values = new ContentValues();
        values.put(LASTAUTHENTICATEDTIME, lastAuthenticatedTime);
        updateUserInfo(persistenceKey, values);
    }


    public static String getCreateUserQuery() {
        String query = "CREATE TABLE " + UserMO_TABLE1 + " (" +
                USERNAME + " varchar(255), " +
                FIRSTNAME + " varchar(255), " +
                LASTNAME + " varchar(255), " +
                PASSWORD + " varchar(255), " +
                SALUTATION + " varchar(255), " +
                PERSISTENCEKEY + " varchar(255) PRIMARY KEY, " +
                EMAILADDRESS + " varchar(255), " +
                PHONENUMBER + " varchar(255), " +
                COUNTRYCODE + " varchar(255), " +
                GENDER + " varchar(255), " +
                FISIKEUSERID + " LONG, " +
                DATEOFBIRTH + " varchar, " +
                HEIGHT_VALUE + " Real, " +
                USERTYPE + " varchar(255), " +
                HEIGHT_UNIT + " varchar(255), " +
                WEIGHT_VALUE + " real, " +
                WEIGHT_UNIT + " varchar(255), " +
                DEVICEUID + " varchar(255), " +
                DEVICEACTIVATIONDATE + " varchar(255), " +
                REGISTRATIONSTATUS + " varchar(255), " +
                ENABLED + " varchar(255)," +
                PROFILEPIC + " BLOB, " +
                OTPCODE + " varchar(255), " +
                ACCOUNTLOCKED + " varchar(255), " +
                PHYSICIANID + " LONG, " +
                PATIENTID + " LONG, " +
                AUTHORITIES + " BLOB, " +
                FISIKEUSERROLES + " varchar(255), " +
                USERGROUPS + " BLOB, " +
                FISIKEUSERTYPE + " varchar(255), " +
                PROFILEPICUPDATESTATUS + " varchar(255), " +
                SIGNUPSTATUS + " varchar(255), " +
                SIGNUPTIME + " LONG, " +
                CHATCONVERSATIONKEYLIST + " BLOB, " +
                CREATEDTIMEUTC + " LONG, " +
                LASTAUTHENTICATEDTIME + " varchar(255), " +
                SECONDRYEMAIL + " varchar(255), " +
                SECONDRYEMAILCHANGETIME + " LONG, " +
                SECONDRYEMAILVERIFICATIONSTATUS + " varchar(255), " +
                HASSIGNEDUP + " varchar(255), " +
                VERIFICATIONCODE + " varchar(255), " +
                GROUPNAME + " varchar(255), " +
                PRIVILEGEGROUP + " varchar(255), " +
                ABOUTME + " varchar(400), " +
                SPECIALTY + " varchar(255), " +
                DESIGNATION + " varchar(255), " +
                SPECIALITIESARRAY + "  BLOB, " +
                DESIGNATIONARRAY + " BLOB, " +
                INVITEUSERPRIGROUPLIST + " BLOB, " +
                USERPRIVILEGES + " BLOB, " +
                STATUS + " varchar(255) DEFAULT \"" + TextConstants.STATUS_AVAILABLE_TEXT + "\", " +
                STATUSMESSAGE + " varchar(255) DEFAULT '', " +
                ALTERNATECONTACT + " varchar(255), " +
                ORGANIZATIONID + " INT," +
                MARITALSTATUS + " varchar(255), " +
                ADDRESS + " BLOB ," +
                DEPENDENTPATIENTS + " varchar(255), " +
                MPIN + " varchar(255), " +
                USERLANGUAGE + " varchar(255), " +
                AGREEMENTS + " BLOB, " +
                TOKEN + " varchar(255), " +
                COVERAGE + " BLOB, " +
                PENDING_DIAGNOSTICS + " BLOB, " +
                IDENTIFIER + " BLOB, " +
                EMAILLIST + " BLOB, " +
                PHONELIST + " BLOB, "+
                MIDDLENAME+" varchar(255) )";

        AppLog.d("Create " + UserMO_TABLE1, query);
        return query;
    }

    public void updateMPIN(String pin, UserMO user) {
        ContentValues values = new ContentValues();
        values.put(MPIN, pin);
        updateUserInfo(user.getPersistenceKey(), values);

    }

    public void updateAddress(ArrayList<Address> addresses, UserMO user) {
        try {

            if (addresses != null) {
                ContentValues values = new ContentValues();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(addresses);
                values.put(ADDRESS, bytes.toByteArray());
                updateUserInfo(user.getPersistenceKey(), values);
            }

        } catch (Exception e) {

        }
    }

    public void updateIdentifier(ArrayList<FetchUpdateIdentifier> identifier, long persistanceKey) {

        try {

            if (identifier != null) {
                ContentValues values = new ContentValues();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(identifier);
                values.put(IDENTIFIER, bytes.toByteArray());
                updateUserInfo(persistanceKey, values);
            }

        } catch (Exception e) {

        }
    }

    public void updateEmailList(ArrayList<FetchUpdateTelecom> emaiList, long persistanceKey) {

        try {

            if (emaiList != null) {
                ContentValues values = new ContentValues();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(emaiList);
                values.put(EMAILLIST, bytes.toByteArray());
                updateUserInfo(persistanceKey, values);
            }

        } catch (Exception e) {

        }
    }

    public void updatePhoneList(ArrayList<FetchUpdateTelecom> phoneList, long persistanceKey) {

        try {

            if (phoneList != null) {
                ContentValues values = new ContentValues();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(phoneList);
                values.put(PHONELIST, bytes.toByteArray());
                updateUserInfo(persistanceKey, values);
            }

        } catch (Exception e) {

        }
    }

    public static String getIDENTIFIER() {
        return IDENTIFIER;
    }

    public static String getEMAILLIST() {
        return EMAILLIST;
    }

    public static String getPHONELIST() {
        return PHONELIST;
    }

    public static String getUserMO_TABLE1() {
        return UserMO_TABLE1;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getFISIKEUSERID() {
        return FISIKEUSERID;
    }

    public static String getUSERTYPE() {
        return USERTYPE;
    }

    public static String getHeightValue() {
        return HEIGHT_VALUE;
    }

    public static String getHeightUnit() {
        return HEIGHT_UNIT;
    }

    public static String getWeightValue() {
        return WEIGHT_VALUE;
    }

    public static String getWeightUnit() {
        return WEIGHT_UNIT;
    }

    public static String getDEVICEUID() {
        return DEVICEUID;
    }

    public static String getDEVICEACTIVATIONDATE() {
        return DEVICEACTIVATIONDATE;
    }

    public static String getREGISTRATIONSTATUS() {
        return REGISTRATIONSTATUS;
    }

    public static String getENABLED() {
        return ENABLED;
    }

    public static String getOTPCODE() {
        return OTPCODE;
    }

    public static String getACCOUNTLOCKED() {
        return ACCOUNTLOCKED;
    }

    public static String getPHYSICIANID() {
        return PHYSICIANID;
    }

    public static String getPATIENTID() {
        return PATIENTID;
    }

    public static String getAUTHORITIES() {
        return AUTHORITIES;
    }

    public static String getFISIKEUSERROLES() {
        return FISIKEUSERROLES;
    }

    public static String getUSERGROUPS() {
        return USERGROUPS;
    }

    public static String getFISIKEUSERTYPE() {
        return FISIKEUSERTYPE;
    }

    public static String getSIGNUPSTATUS() {
        return SIGNUPSTATUS;
    }

    public static String getSIGNUPTIME() {
        return SIGNUPTIME;
    }

    public static String getCHATCONVERSATIONKEYLIST() {
        return CHATCONVERSATIONKEYLIST;
    }

    public static String getCREATEDTIMEUTC() {
        return CREATEDTIMEUTC;
    }

    public static String getGROUPNAME() {
        return GROUPNAME;
    }

    public static String getPRIVILEGEGROUP() {
        return PRIVILEGEGROUP;
    }

    public static String getABOUTME() {
        return ABOUTME;
    }

    public static String getSPECIALTY() {
        return SPECIALTY;
    }

    public static String getDESIGNATION() {
        return DESIGNATION;
    }

    public static String getSPECIALITIESARRAY() {
        return SPECIALITIESARRAY;
    }

    public static String getDESIGNATIONARRAY() {
        return DESIGNATIONARRAY;
    }

    public static String getINVITEUSERPRIGROUPLIST() {
        return INVITEUSERPRIGROUPLIST;
    }

    public static String getUSERPRIVILEGES() {
        return USERPRIVILEGES;
    }

    public static String getORGANIZATIONID() {
        return ORGANIZATIONID;
    }

    public static String getAGREEMENTS() {
        return AGREEMENTS;
    }

    public static String getSECONDRYEMAIL() {
        return SECONDRYEMAIL;
    }

    public static String getSECONDRYEMAILCHANGETIME() {
        return SECONDRYEMAILCHANGETIME;
    }

    public static String getSECONDRYEMAILVERIFICATIONSTATUS() {
        return SECONDRYEMAILVERIFICATIONSTATUS;
    }

    public static String getVERIFICATIONCODE() {
        return VERIFICATIONCODE;
    }

    public static String getMIDDLENAME() {
        return MIDDLENAME;
    }

    public void updateUserData(long persistanceKey, ContentValues contentValues) {
        try {
            database.update(UserMO_TABLE1, contentValues, PERSISTENCEKEY + "=" + persistanceKey, null);
            SharedPref.setIsUserInfoUpdated(true);
        } catch (Exception e) {
            SharedPref.setIsUserInfoUpdated(false);
            e.printStackTrace();
        }
    }

}
