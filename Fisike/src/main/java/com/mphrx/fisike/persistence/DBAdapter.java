package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.Queue.AttachmentQueue;
import com.mphrx.fisike.Queue.UploadRecordQueue;
import com.mphrx.fisike.adapter.VitalsAdapter;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.QuestionActivityMO;
import com.mphrx.fisike.mo.QuestionMO;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.ProfilePicTaskManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationProfileDBAdapter;
import com.mphrx.fisike_physician.models.User;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import careplan.database.databaseAdapter.CarePlanDatabaseAdapter;
import careplan.database.databaseAdapter.CareTaskDatabaseAdapter;

/**
 * This class manages the DB interactions for the application -- all persistence functions for UserMO, SettingMO, ChatConversationMO, ChatMessageMO
 *
 * @author kkhurana
 */
public class DBAdapter {

    //    public static final String ChatMessageMO_TABLE = "ChatMessageMO";
    private static final String QuestionActivityMO_TABLE = "QuestionActivityMO";
    private static final String QuestionMO_TABLE = "QuestionMO";
    /*   public static final String AttachmentMO_TABLE = "AttachmentMO";*/
    //
    public static final String USER_INVITE_TABLE = "user_invite_table";
    public static final String USER_NUMBER = "user_number";
    public static final String USER_NAME = "user_name";
    public static final String USER_ROLE = "user_role";
    public static final String USER_ROLE_ID = "user_role_id";
    public static final String USER_INVITEE_ROLE_ID = "user_invitee_role_id";
    public static final String USER_ORG_ID = "user_org_id";

    private static DBAdapter dbAdapter;

    private static Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;


    public DBAdapter() {

    }

    public static DBAdapter getInstance(Context ctx) {
        if (null == dbAdapter) {
            synchronized (DBAdapter.class) {
                if (dbAdapter == null) {
                    dbAdapter = new DBAdapter();
                    context = ctx;
                    dbAdapter.open();
                }
            }
        }
        return dbAdapter;
    }

    public DBAdapter open() throws SQLException {
        if (database == null || !database.isOpen()) {
            dbManager = new DBManager(context);
        }

        try {
            database = dbManager.getWritableDatabase(Utils.generateUniqueKey(context));
        }
        catch (Exception e)
        {
            database=dbManager.getWritableDatabase(VariableConstants.DB_KEY);
            dbManager.updateToPasskey(database);
            database = dbManager.getWritableDatabase(Utils.generateUniqueKey(context));
        }

        return this;
    }

    public void close() {
        if (database.isOpen()) {
            dbManager.close();
            if (dbAdapter != null) {
                dbAdapter = null;
            }
        }
    }


    public void insertUserInviteList(List<User> inviteList) throws Exception {
        for (User user : inviteList) {
            ContentValues values = new ContentValues();
            values.put(USER_NAME, user.name);
            values.put(USER_NUMBER, user.number);
            values.put(USER_ROLE, user.role);
            values.put(USER_ROLE_ID, user.roleId);
            values.put(USER_INVITEE_ROLE_ID, user.inviteeRoleId);
            values.put(USER_ORG_ID, user.organisationId);
            long count = database.insert(USER_INVITE_TABLE, null, values);
            if (count == -1)
                AppLog.showError(getClass().getSimpleName(), "Error >>> Name = " + user.name + " Number = " + user.number + " Role = " + user.role);
            else
                AppLog.showInfo(getClass().getSimpleName(), "Insert >>> Name = " + user.name + " Number = " + user.number + " Role = " + user.role);
        }

    }

    public void deleteUserInviteTable(String number) throws Exception {
        database.delete(DBAdapter.USER_INVITE_TABLE, DBAdapter.USER_NUMBER + "=" + number, null);
        AppLog.showInfo(getClass().getSimpleName(), "Delete >>> Number = " + number);
    }

    public synchronized List<User> getUserInviteList() throws Exception {
        List<User> list = new ArrayList<>();
        Cursor cursor = database.query(USER_INVITE_TABLE, new String[]{USER_NUMBER, USER_NAME, USER_ROLE, USER_ROLE_ID, USER_INVITEE_ROLE_ID, USER_ORG_ID}, null, null, null, null, null);
        if (cursor == null)
            throw new Exception("Something went wrong...");
        try {
            cursor.moveToFirst();
            do {
                User user = new User();
                user.number = (cursor.getString(cursor.getColumnIndex(USER_NUMBER)));
                user.name = (cursor.getString(cursor.getColumnIndex(USER_NAME)));
                user.role = (cursor.getString(cursor.getColumnIndex(USER_ROLE)));
                user.roleId = (cursor.getString(cursor.getColumnIndex(USER_ROLE_ID)));
                user.inviteeRoleId = (cursor.getString(cursor.getColumnIndex(USER_INVITEE_ROLE_ID)));
                user.organisationId = (cursor.getString(cursor.getColumnIndex(USER_ORG_ID)));
                list.add(user);
            } while (cursor.moveToNext());
        } finally {
            cursor.close();
            AppLog.showInfo(getClass().getSimpleName(), "Fetch User Invite List size = " + list.size());
        }

        return list;
    }


    /**
     * This method creates or updates the questionActivityMO
     *
     * @param questionActivityMO
     * @return
     * @throws Exception
     */
    public boolean createOrUpdateQuestionActivityMO(QuestionActivityMO questionActivityMO) throws Exception {
        QuestionActivityMO existingObject = fetchQuestionActivityMO(questionActivityMO.getPersistenceKey() + "");

        if (existingObject != null) {
            database.delete(QuestionActivityMO_TABLE, "persistenceKey" + "=" + questionActivityMO.getPersistenceKey(), null);
        }

        ContentValues values = new ContentValues();
        values.put("activityID", questionActivityMO.getActivityID());
        values.put("activityStatus", questionActivityMO.getActivityStatus());
        values.put("submissionTimeUTC", questionActivityMO.getSubmissionTimeUTC());
        values.put("activitySubmitted", questionActivityMO.getActivitySubmitted());
        values.put("activityTitle", questionActivityMO.getActivityTitle());
        values.put("chatConversationMOPKey", questionActivityMO.getChatConversationMOPKey());
        values.put("chatMessageMOPKey", questionActivityMO.getChatMessageMOPKey());
        values.put("estimatedTime", questionActivityMO.getEstimatedTime());
        values.put("numberOfQuestions", questionActivityMO.getNumberOfQuestions());
        values.put("receiverUsername", questionActivityMO.getReceiverUsername());
        values.put("senderUsername", questionActivityMO.getSenderUsername());

        if (questionActivityMO.getQuestionMOPKeys() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(questionActivityMO.getQuestionMOPKeys());
            values.put("questionMOPKeys", bytes.toByteArray());
        }

        values.put("persistenceKey", questionActivityMO.getPersistenceKey());

        long count = database.insert(QuestionActivityMO_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    /**
     * This method creates or updates the questionActivityMO
     *
     * @param questionMO
     * @return
     * @throws Exception
     */
    public boolean createOrUpdateQuestionMO(QuestionMO questionMO) throws Exception {
        QuestionMO existingObject = fetchQuestionMO(questionMO.getPersistenceKey() + "");

        if (existingObject != null) {
            database.delete(QuestionMO_TABLE, "persistenceKey" + "=" + questionMO.getPersistenceKey(), null);
        }

        ContentValues values = new ContentValues();

        if (questionMO.getAnsTextArray() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(questionMO.getAnsTextArray());
            values.put("ansTextArray", bytes.toByteArray());
        }

        if (questionMO.getImageUrlArray() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(questionMO.getImageUrlArray());
            values.put("imageUrlArray", bytes.toByteArray());
        }

        if (questionMO.getOptionsArray() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(questionMO.getOptionsArray());
            values.put("optionsArray", bytes.toByteArray());
        }

        values.put("questionDescription", questionMO.getQuestionDescription());
        values.put("questionID", questionMO.getQuestionID());
        values.put("questionText", questionMO.getQuestionText());
        values.put("questionType", questionMO.getQuestionType());

        values.put("persistenceKey", questionMO.getPersistenceKey());

        long count = database.insert(QuestionMO_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }


    /**
     * This method will fetch a QuestionActivityMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public QuestionActivityMO fetchQuestionActivityMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, QuestionActivityMO_TABLE, new String[]{"activityID", "activityStatus", "submissionTimeUTC",
                        "activitySubmitted", "activityTitle", "chatConversationMOPKey", "chatMessageMOPKey", "estimatedTime", "numberOfQuestions",
                        "questionMOPKeys", "receiverUsername", "senderUsername", "persistenceKey"}, "persistenceKey =" + persistenceKey, null, null, null,
                null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            QuestionActivityMO questionActivityMO = new QuestionActivityMO();

            questionActivityMO.setActivityID(mCursor.getString(0));
            questionActivityMO.setActivityStatus(mCursor.getString(1));
            questionActivityMO.setSubmissionTimeUTC(mCursor.getString(2));
            questionActivityMO.setActivitySubmitted(mCursor.getInt(3) == 1 ? true : false);
            questionActivityMO.setActivityTitle(mCursor.getString(4));
            questionActivityMO.setChatConversationMOPKey(mCursor.getString(5));
            questionActivityMO.setChatMessageMOPKey(mCursor.getString(6));
            questionActivityMO.setEstimatedTime(mCursor.getString(7));
            questionActivityMO.setNumberOfQuestions(mCursor.getInt(8));

            byte[] listBytes = mCursor.getBlob(9);
            if (listBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
                questionActivityMO.setQuestionMOPKeys(listVector);

            }

            questionActivityMO.setReceiverUsername(mCursor.getString(10));
            questionActivityMO.setSenderUsername(mCursor.getString(11));
            questionActivityMO.setPersistenceKey(Long.parseLong(mCursor.getString(12)));

            mCursor.close();
            return questionActivityMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    /**
     * This method will fetch a QuestionMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public QuestionMO fetchQuestionMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, QuestionMO_TABLE, new String[]{"ansTextArray", "imageUrlArray", "optionsArray",
                        "questionDescription", "questionID", "questionText", "questionType", "persistenceKey"}, "persistenceKey =" + persistenceKey, null,
                null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            QuestionMO questionMO = new QuestionMO();

            byte[] listBytes = mCursor.getBlob(0);
            if (listBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
                questionMO.setAnsTextArray(listVector);
            }

            byte[] listBytes2 = mCursor.getBlob(1);
            if (listBytes2 != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes2);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
                questionMO.setImageUrlArray(listVector);
            }

            byte[] listBytes3 = mCursor.getBlob(2);
            if (listBytes3 != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(listBytes3);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
                questionMO.setOptionsArray(listVector);
            }
            questionMO.setQuestionDescription(mCursor.getString(3));
            questionMO.setQuestionID(mCursor.getString(4));
            questionMO.setQuestionText(mCursor.getString(5));
            questionMO.setQuestionType(mCursor.getString(6));
            questionMO.setPersistenceKey(Long.parseLong(mCursor.getString(7)));

            mCursor.close();
            return questionMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    private void deleteTable(String tableName, String whereClause, String[] whereArgs) {
        try {
            database.delete(tableName, whereClause, whereArgs);
        } catch (Exception e) {
            AppLog.d("Truncate Table", tableName + " No SUCH TABLE FOUND");
        }
    }


    public void truncateUserMo() {
        try {
            deleteTable(UserDBAdapter.getUserMO_TABLE(), null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Query for truncate the database
     */
    public void truncateDB() {
        try {
            deleteTable(ChatMessageDBAdapter.getChatMessageMO_TABLE(), null, null);
            deleteTable(ChatMessageDBAdapter.getChatMessageMO_TABLE(), null, null);
            deleteTable(ChatConversationDBAdapter.getChatConversationMO_TABLE(), null, null);
            deleteTable(chatContactDBAdapter.getChatContactMo_TABLE(), null, null);
            deleteTable(QuestionActivityMO_TABLE, null, null);
            deleteTable(QuestionMO_TABLE, null, null);
            deleteTable(AttchmentDBAdapter.getAttachmentMO_TABLE(), null, null);
            deleteTable(UserDBAdapter.getUserMO_TABLE(), null, null);
            deleteTable(GroupChatDBAdapter.getGroupTextChatMoTable(), null, null);
            deleteTable(MedicationPrescriptionDBAdapter.getMedicationPrescriptionTable(), null, null);
            deleteTable(PatientDBAdapter.PATIENT_MO_TABLE, null, null);
            deleteTable(DocumentReferenceDBAdapter.getDocumentReferenceTable(), null, null);
            deleteTable(NotificationCenterDBAdapter.getNotificationCenterTable(), null, null);
            deleteTable(DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE, null, null);
            deleteTable(EncounterDBAdapter.getEncounterTable(), null, null);
            deleteTable(MedicationDBAdapter.getMedicationTable(), null, null);
            deleteTable(PractitionerDBAdapter.getPractitionerTable(), null, null);
            deleteTable(VaccinationDBAdapter.getVaccinationTable(),null,null);
            deleteTable(VaccinationProfileDBAdapter.getVaccinationProfileTable(),null,null);
            deleteTable(VitalsDBAdapter.getVitalsTable(),null,null);
            deleteTable(USER_INVITE_TABLE, null, null);
            deleteTable(PhysicianDBAdapter.PHYSICIAN_MO_TABLE,null,null);
            CarePlanDatabaseAdapter.getInstance(MyApplication.getAppContext()).truncateDB();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ProfilePicTaskManager.getInstance().removeTaskAllTask();
//    TaskManager.getInstance().removeTaskAllTask();
//    TaskManagerUploadReport.getInstance().removeTaskAllTask();
        UploadRecordQueue.getInstance().removeTaskAllTask();
        AttachmentQueue.getInstance().removeTaskAllTask();
    }


    // diseases table
    // check if a record exists so it won't insert the next time you run this code
    public boolean checkIfExists(String objectName) {
        Cursor cursor = null;
        boolean recordExists = false;
        try {
            cursor = database.rawQuery("SELECT " + "diseaseID" + " FROM " + "DiseaseTableMO" + " WHERE " + "diseaseName" + " = '" + objectName + "'",
                    null);

            if (cursor != null) {

                if (cursor.getCount() > 0) {
                    recordExists = true;
                }
            }
        } catch (Exception e) {
            System.out.println("exception e ::::" + e.toString());
        }

        if (cursor != null)
            cursor.close();

        return recordExists;
    }


}
