package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.gson.request.MedicationOrderRequest.Extension;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kailash Khurana on 2/16/2016.
 */
public class EncounterMedicationDBAdapter {
    private static final String REMARK = "REMARK";
    private static final String EXTENTION = "extention";
    private static EncounterMedicationDBAdapter encounterMedicationDbAdapter;
    private Context context;
    private SQLiteDatabase database;
    private static final String MEDICATION_PRESCRIPTION_TABLE = "MedicationPrescription";

    private static String ID = "ID";

    public static String getID() {
        return ID;
    }

    private static String ENCOUNTERID = "encounterID";
    private static String PRACTITIONARID = "practitionarID";
    //    private static String UNIT = "unit";
    private static String FROMDATE = "fromDate";
    private static String TODATE = "toDate";
    private static String DOSAGE = "dosage";
    //    private static String REMINDER = "reminder";
    private static String PATIENTID = "patientID";
    private static String MEDICIANID = "medicianId";
    private static String PRACTIONERNAME = "practionerName";
    public static String MEDICINENAME = "medicineName";
    private static String ISALARMSET = "isAlarmSet";
    private static String DISEASELIST = "diseaseList";
    private static String STATUS = "status";
    private static String UPDATEDDATE = "updatedDate";
    private static String DOSEINSTRUCTION = "doseInstruction";
    private static String PATIENTNAME = "patientName";

    private static String SOURCENAME = "sourceName";

    public static String getMimeType() {
        return MIME_TYPE;
    }

    public static void setMimeType(String mimeType) {
        MIME_TYPE = mimeType;
    }

    private static String MIME_TYPE = "mimeType";

    public static final String status = "";
    public static final Date updateDate = null;
    public static final int id = 10;
    public static final int medicationId = -1;

    public static String getMedicationPrescriptionTable() {
        return MEDICATION_PRESCRIPTION_TABLE;
    }

    public EncounterMedicationDBAdapter(Context context) {
        this.context = context;
    }

    public static EncounterMedicationDBAdapter getInstance(Context ctx) {
        if (null == encounterMedicationDbAdapter) {
            synchronized (MedicationPrescriptionDBAdapter.class) {
                if (encounterMedicationDbAdapter == null) {
                    encounterMedicationDbAdapter = new EncounterMedicationDBAdapter(ctx);
                    encounterMedicationDbAdapter.open();
                }
            }
        }
        return encounterMedicationDbAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    /**
     * This method will fetch a MedicationPrescriptionModel from the db
     *
     * @param id
     * @return
     * @throws Exception
     */
    public PrescriptionModel fetchMedicationPrescription(int id) throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, ID + " =" + id, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            PrescriptionModel prescriptionMo = new PrescriptionModel();
            prescriptionMo.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
            prescriptionMo.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            prescriptionMo.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
            prescriptionMo.setStartDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            prescriptionMo.setEndDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<MedicineDoseFrequencyModel> dosesList = (ArrayList<MedicineDoseFrequencyModel>) ois.readObject();
                prescriptionMo.setArrayDose(dosesList);
            }
            prescriptionMo.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));

            prescriptionMo.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
            prescriptionMo.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
            prescriptionMo.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
            prescriptionMo.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));

            byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
            if (diseaseListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                prescriptionMo.setDiseaseMoList(diseaseList);
            }
            prescriptionMo.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
            prescriptionMo.setDoseInstruction(mCursor.getString(mCursor.getColumnIndex(DOSEINSTRUCTION)));
            prescriptionMo.setUpdatedDate(mCursor.getString(mCursor.getColumnIndex(UPDATEDDATE)));
            prescriptionMo.setRemark(mCursor.getString(mCursor.getColumnIndex(REMARK)));
            prescriptionMo.setPatientName(mCursor.getString(mCursor.getColumnIndex(PATIENTNAME)));
            prescriptionMo.setMimeType(mCursor.getInt(mCursor.getColumnIndex(MIME_TYPE)));
            prescriptionMo.setSourceName(mCursor.getString(mCursor.getColumnIndex(SOURCENAME)));
            byte[] extentionListBytes = mCursor.getBlob(mCursor.getColumnIndex(EXTENTION));
            if (extentionListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(extentionListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                List<Extension> extentionList = (List<Extension>) ois.readObject();
                prescriptionMo.setExtensionList(extentionList);
            }
            mCursor.close();
            return prescriptionMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public void dropMedicationTable() {
        database.execSQL("DROP TABLE IF EXISTS " + MEDICATION_PRESCRIPTION_TABLE);
    }


    /**
     * This method will fetch a MedicationPrescriptionModel from the db
     *
     * @return
     * @throws Exception
     */
    public ArrayList<PrescriptionModel> fetchAllMedication() throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, null, null, null, null, null, null);

        ArrayList<PrescriptionModel> arrayPrescriptionModel = new ArrayList<PrescriptionModel>();
        if (mCursor != null) {
            boolean status = mCursor.moveToLast();
            do {
                if (!status)
                    break;
                PrescriptionModel prescriptionMo = new PrescriptionModel();
                prescriptionMo.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
                prescriptionMo.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
                prescriptionMo.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
                prescriptionMo.setStartDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
                prescriptionMo.setEndDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
                byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
                if (dosesArrayBytes != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    ArrayList<MedicineDoseFrequencyModel> dosesList = (ArrayList<MedicineDoseFrequencyModel>) ois.readObject();
                    prescriptionMo.setArrayDose(dosesList);
                }
                prescriptionMo.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));

                prescriptionMo.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
                prescriptionMo.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
                prescriptionMo.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
                prescriptionMo.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));
                byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
                if (diseaseListBytes != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                    prescriptionMo.setDiseaseMoList(diseaseList);
                }
                prescriptionMo.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
                prescriptionMo.setDoseInstruction(mCursor.getString(mCursor.getColumnIndex(DOSEINSTRUCTION)));
                prescriptionMo.setUpdatedDate(mCursor.getString(mCursor.getColumnIndex(UPDATEDDATE)));
                prescriptionMo.setRemark(mCursor.getString(mCursor.getColumnIndex(REMARK)));
                prescriptionMo.setPatientName(mCursor.getString(mCursor.getColumnIndex(PATIENTNAME)));
                prescriptionMo.setMimeType(mCursor.getInt(mCursor.getColumnIndex(MIME_TYPE)));
                prescriptionMo.setSourceName(mCursor.getString(mCursor.getColumnIndex(SOURCENAME)));

                byte[] extentionListBytes = mCursor.getBlob(mCursor.getColumnIndex(EXTENTION));
                if (extentionListBytes != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(extentionListBytes);
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    List<Extension> extentionList = (List<Extension>) ois.readObject();
                    prescriptionMo.setExtensionList(extentionList);
                }
                arrayPrescriptionModel.add(prescriptionMo);
            } while (mCursor.moveToPrevious());
        }
        mCursor.close();
        return arrayPrescriptionModel;
    }

    // deletes the medication prescription corresponding to this prescription id
    public void deleteMedicationPrescription(int medicationPrescriptionId) {
        database.delete(MEDICATION_PRESCRIPTION_TABLE, "ID" + "=" + medicationPrescriptionId, null);
    }


    public void updateMedicationPrescription(int medicationPrescriptionID, ContentValues contentValues) throws Exception {
        database.update(MEDICATION_PRESCRIPTION_TABLE, contentValues, ID + "=" + medicationPrescriptionID, null);
    }

    public static String getPATIENTNAME() {
        return PATIENTNAME;
    }

    public static String createMedicationPrescriptionTableQuery() {
        String query = "CREATE TABLE " + MEDICATION_PRESCRIPTION_TABLE + " ( " +
                ID + " INTEGER PRIMARY KEY, " +
                ENCOUNTERID + " Integer, " +
                PRACTITIONARID + " Integer, " +
                FROMDATE + " varchar(255), " +
                TODATE + " varchar(255), " +
                DOSAGE + " BLOB, " +
                PATIENTID + " INTEGER, " +
                MEDICIANID + " INTEGER, " +
                PRACTIONERNAME + " varchar(255), " +
                MEDICINENAME + " varchar(255), " +
                ISALARMSET + " Integer, " +
                DISEASELIST + " BLOB ," +
                DOSEINSTRUCTION + " varchar(255), " +
                STATUS + " varchar(255)," +
                UPDATEDDATE + " date, " +
                REMARK + " varchar(255), " +
                PATIENTNAME + " varchar(255), " +
                SOURCENAME + " varchar(255), " +
                MIME_TYPE + " Integer," +
                EXTENTION + " BLOB )";


        AppLog.d("CREATE " + MEDICATION_PRESCRIPTION_TABLE, query);
        return query;
    }

    public boolean insertMedicationPrescription(PrescriptionModel prescriptionModel) {
        try {
            ContentValues values = new ContentValues();
            values.put(ID, prescriptionModel.getID());
            values.put(ENCOUNTERID, prescriptionModel.getEncounterID());
            values.put(PRACTITIONARID, prescriptionModel.getPractitionarID());
            values.put(FROMDATE, prescriptionModel.getStartDate());
            values.put(TODATE, prescriptionModel.getEndDate());
            if (null != prescriptionModel.getArrayDose()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getArrayDose());
                values.put(DOSAGE, bytes.toByteArray());
            }
            if (prescriptionModel.getDiseaseMoList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getDiseaseMoList());
                values.put(DISEASELIST, bytes.toByteArray());

            }
            values.put(PATIENTID, prescriptionModel.getPatientID());

            if (prescriptionModel.getPractitionerName() != null) {
                values.put(PRACTIONERNAME, prescriptionModel.getPractitionerName());
            }
            values.put(MEDICIANID, prescriptionModel.getMedicianId());
            values.put(MEDICINENAME, prescriptionModel.getMedicineName());
            values.put(ISALARMSET, prescriptionModel.getAutoReminderSQLBool());
            values.put(STATUS, prescriptionModel.getStatus());
            values.put(UPDATEDDATE, prescriptionModel.getUpdatedDate());
            values.put(DOSEINSTRUCTION, prescriptionModel.getDoseInstruction());
            values.put(REMARK, prescriptionModel.getRemark());
            if (prescriptionModel.getPatientName() != null) {
                values.put(PATIENTNAME, prescriptionModel.getPatientName());
            }
            if (prescriptionModel.getSourceName() == null) {
                values.put(SOURCENAME, "");
            } else {
                values.put(SOURCENAME, prescriptionModel.getSourceName());
            }

            values.put(MIME_TYPE, prescriptionModel.getMimeType());
            if (null != prescriptionModel.getExtensionList()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getExtensionList());
                values.put(EXTENTION, bytes.toByteArray());
            }

//            String query = "INSERT OR REPLACE INTO  " + MEDICATION_PRESCRIPTION_TABLE + "("+ID, name) WHERE " + ID + "=" + medicationPrescriptionID;
//            long count = database.rawQuery(MEDICATION_PRESCRIPTION_TABLE, null, values);
            long count = database.insert(MEDICATION_PRESCRIPTION_TABLE, null, values);
            if (count > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateEncounterLastUpdatedTime(int medicationPrescriptionID, ContentValues updatedEncounter) throws Exception {
        long count = -1;
        String query = "SELECT * from " + MEDICATION_PRESCRIPTION_TABLE + " WHERE " + ID + "=" + medicationPrescriptionID;
        Cursor mCursor = database.rawQuery(query, null);

        count = database.update(MEDICATION_PRESCRIPTION_TABLE, updatedEncounter, ID + "=" + medicationPrescriptionID, null);
        if (count == -1) {
            return false;
        }
        return true;
    }


    public void updateMedicationPrescription(PrescriptionModel prescriptionModel) {
        try {

            ContentValues values = new ContentValues();
            values.put(ENCOUNTERID, prescriptionModel.getEncounterID());
            values.put(PRACTITIONARID, prescriptionModel.getPractitionarID());
            values.put(FROMDATE, prescriptionModel.getStartDate());
            values.put(TODATE, prescriptionModel.getEndDate());
            if (null != prescriptionModel.getArrayDose()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getArrayDose());
                values.put(DOSAGE, bytes.toByteArray());
            }
            if (prescriptionModel.getDiseaseMoList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getDiseaseMoList());
                values.put(DISEASELIST, bytes.toByteArray());

            }
            values.put(PATIENTID, prescriptionModel.getPatientID());

            if (prescriptionModel.getPractitionerName() != null) {
                values.put(PRACTIONERNAME, prescriptionModel.getPractitionerName());
            }
            values.put(MEDICIANID, prescriptionModel.getMedicianId());
            values.put(MEDICINENAME, prescriptionModel.getMedicineName());
            values.put(ISALARMSET, prescriptionModel.getAutoReminderSQLBool());
            values.put(STATUS, prescriptionModel.getStatus());
            values.put(DOSEINSTRUCTION, prescriptionModel.getDoseInstruction());
            values.put(UPDATEDDATE, prescriptionModel.getUpdatedDate());
            values.put(REMARK, prescriptionModel.getRemark());
            if (prescriptionModel.getPatientName() != null) {
                values.put(PATIENTNAME, prescriptionModel.getPatientName());
            }
            values.put(MIME_TYPE, prescriptionModel.getMimeType());
            if (prescriptionModel.getSourceName() == null) {
                values.put(SOURCENAME, "");
            } else {
                values.put(SOURCENAME, prescriptionModel.getSourceName());
            }
            if (null != prescriptionModel.getExtensionList()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(prescriptionModel.getExtensionList());
                values.put(EXTENTION, bytes.toByteArray());
            }
            updateMedicationPrescription(prescriptionModel.getID(), values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<PrescriptionModel> getMedicationPrescriptionsWhoseAlarmIsActive() throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, ISALARMSET + " >" + 0, null, null, null, null, null);
        ArrayList<PrescriptionModel> activeAlarmMedicationPrescriptionList = new ArrayList<PrescriptionModel>();
        while (mCursor != null && mCursor.moveToNext()) {
            PrescriptionModel prescriptionMo = new PrescriptionModel();
            prescriptionMo.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
            prescriptionMo.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            prescriptionMo.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
            prescriptionMo.setStartDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            prescriptionMo.setEndDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<MedicineDoseFrequencyModel> dosesList = (ArrayList<MedicineDoseFrequencyModel>) ois.readObject();
                prescriptionMo.setArrayDose(dosesList);
            }
            prescriptionMo.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));

            prescriptionMo.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
            prescriptionMo.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
            prescriptionMo.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
            prescriptionMo.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));
            prescriptionMo.setSourceName(mCursor.getString(mCursor.getColumnIndex(SOURCENAME)));

            byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
            if (diseaseListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                prescriptionMo.setDiseaseMoList(diseaseList);
            }
            prescriptionMo.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
            prescriptionMo.setDoseInstruction(mCursor.getString(mCursor.getColumnIndex(DOSEINSTRUCTION)));
            prescriptionMo.setUpdatedDate(mCursor.getString(mCursor.getColumnIndex(UPDATEDDATE)));
            prescriptionMo.setRemark(mCursor.getString(mCursor.getColumnIndex(REMARK)));
            prescriptionMo.setMimeType(mCursor.getInt(mCursor.getColumnIndex(MIME_TYPE)));
            byte[] extentionListBytes = mCursor.getBlob(mCursor.getColumnIndex(EXTENTION));
            if (extentionListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(extentionListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                List<Extension> extentionList = (List<Extension>) ois.readObject();
                prescriptionMo.setExtensionList(extentionList);
            }
            activeAlarmMedicationPrescriptionList.add(prescriptionMo);
        }

        if (mCursor != null) {
            mCursor.close();
        }
        return activeAlarmMedicationPrescriptionList;
    }

    public static String getISALARMSET() {
        return ISALARMSET;
    }

    public static String getDOSAGE() {
        return DOSAGE;
    }

    public void fetchAndInsertMedicationPrescription(PrescriptionModel prescriptionModel, boolean isResetAlarm) {
        try {
            PrescriptionModel fetchPrescriptionModel = fetchMedicationPrescription(prescriptionModel.getID());
            if (fetchPrescriptionModel == null) {
                if (insertMedicationPrescription(prescriptionModel) && prescriptionModel.getAutoReminderSQLBool() > 0) {
                    ArrayList<MedicineDoseFrequencyModel> arrayMedicineDoseFrequencyModels = prescriptionModel.getArrayDose();
                    for (int j = 0; arrayMedicineDoseFrequencyModels != null && j < arrayMedicineDoseFrequencyModels.size(); j++) {
                        MedicineDoseFrequencyModel medicineDoseFrequencyModel = arrayMedicineDoseFrequencyModels.get(j);
                        int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;
                        Utils.startAlarmMedication(context, medicineDoseFrequencyModel.getAlarmManagerUniqueKey(), prescriptionModel, interval, medicineDoseFrequencyModel);
                    }
                }
            } else if (isResetAlarm) {
                if (fetchPrescriptionModel.getAutoReminderSQLBool() > 0) {
                    ArrayList<MedicineDoseFrequencyModel> arrayMedicineDoseFrequencyModels = fetchPrescriptionModel.getArrayDose();
                    for (int j = 0; arrayMedicineDoseFrequencyModels != null && j < arrayMedicineDoseFrequencyModels.size(); j++) {
                        MedicineDoseFrequencyModel medicineDoseFrequencyModel = arrayMedicineDoseFrequencyModels.get(j);
                        int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;
                        Utils.cancelAlarm(context, medicineDoseFrequencyModel.getAlarmManagerUniqueKey());
                    }
                }
                if (prescriptionModel.getAutoReminderSQLBool() > 0) {
                    ArrayList<MedicineDoseFrequencyModel> arrayMedicineDoseFrequencyModels = prescriptionModel.getArrayDose();
                    for (int j = 0; arrayMedicineDoseFrequencyModels != null && j < arrayMedicineDoseFrequencyModels.size(); j++) {
                        MedicineDoseFrequencyModel medicineDoseFrequencyModel = arrayMedicineDoseFrequencyModels.get(j);
                        int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;
                        Utils.startAlarmMedication(context, medicineDoseFrequencyModel.getAlarmManagerUniqueKey(), prescriptionModel, interval, medicineDoseFrequencyModel);
                    }
                }

                updateMedicationPrescription(prescriptionModel);
            } else if (!isResetAlarm) {
                updateMedicationPrescription(prescriptionModel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getEXTENTION() {
        return EXTENTION;
    }

    public static String getSOURCENAME() {
        return SOURCENAME;
    }

    public static void setSOURCENAME(String SOURCENAME) {
        EncounterMedicationDBAdapter.SOURCENAME = SOURCENAME;
    }


}
