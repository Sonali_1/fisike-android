package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.models.EncounterModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by administrate on 1/22/2016.
 */
public class EncounterDBAdapter {

    private static EncounterDBAdapter encounterDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static final String ENCOUNTER_TABLE = "Encounter";
    private static String ENCOUNTERID="encounterID";
    private static String FROMDATE="fromDate";
    private static String TODATE="toDate";
    private static String STATUS="status";
    private static String PHYSICIANID="physicianID";
    private static String PATIENTID  ="patientID";
    private static String UPDATEDDATE="updatedDate";


    public EncounterDBAdapter(Context context) {
        this.context = context;
    }

    public static EncounterDBAdapter getInstance(Context ctx) {
        if (null == encounterDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (encounterDBAdapter == null) {
                    encounterDBAdapter = new EncounterDBAdapter(ctx);
                    encounterDBAdapter.open();
                }
            }
        }
        return encounterDBAdapter;
    }

    public void open() throws SQLException
    {
        database = DBHelper.getInstance(context);
    }

    public static String getEncounterTable() {
        return ENCOUNTER_TABLE;
    }

    public static String createEncounterDBTable()
    {


        String query="CREATE TABLE "+ENCOUNTER_TABLE+" ("+
                ENCOUNTERID+" INTEGER PRIMARY KEY,"+
                FROMDATE+" date, "+
                TODATE+" date, "+
                STATUS+" varchar(255), "+
                PHYSICIANID+" INTEGER, "+
                PATIENTID+" INTEGER, "+
                UPDATEDDATE+" date)";

        AppLog.d(ENCOUNTER_TABLE,query);
        return query;
    }

    public static String dropTable(){
        return "DROP TABLE IF EXISTS " + ENCOUNTER_TABLE;
    }


    /**
     * This method will fetch a EncounterModel from the db
     *
     * @param encounterID
     * @return
     * @throws Exception
     */
    public EncounterModel fetchEncounter(int encounterID) throws Exception {
        Cursor mCursor = database.query(true, ENCOUNTER_TABLE, null, ENCOUNTERID+" =" + encounterID, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            EncounterModel encounterModel = new EncounterModel();
            encounterModel.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            encounterModel.setFromDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            encounterModel.setToDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            encounterModel.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
            encounterModel.setPhysicianID(mCursor.getInt(mCursor.getColumnIndex(PHYSICIANID)));
            encounterModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PHYSICIANID)));
            encounterModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(UPDATEDDATE)));
            mCursor.close();
            return encounterModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    /**
     * This method will fetch a EncounterModel from the db
     *
     * @return
     * @throws Exception
     */
    public ArrayList<EncounterModel> fetchEncounter() throws Exception {
        ArrayList<EncounterModel> tempEncounterModels = null;
        String query = "SELECT * from " + ENCOUNTER_TABLE + " WHERE " + UPDATEDDATE + " >= date('now','localtime', '-7 day')" + " OR " + TODATE
                + " >=date('now') ";


        // String query = "SELECT * from " + ENCOUNTER_TABLE + " WHERE " + "updatedDate" + " >= date('now','localtime', '-7 day')"  ;
        try {
            String query1 = "SELECT * from " + ENCOUNTER_TABLE;
            Cursor mCursor = database.rawQuery(query, null);

            ArrayList<EncounterModel> arrayEncounterModels = new ArrayList<EncounterModel>();

            while (mCursor != null && mCursor.moveToNext()) {
                EncounterModel encounterModel = new EncounterModel();
                encounterModel.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
                encounterModel.setFromDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
                encounterModel.setToDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
                encounterModel.setStatus(mCursor.getString(mCursor.getColumnIndex(STATUS)));
                encounterModel.setPhysicianID(mCursor.getInt(mCursor.getColumnIndex(PHYSICIANID)));
                encounterModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PHYSICIANID)));
                encounterModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(UPDATEDDATE)));
                arrayEncounterModels.add(encounterModel);
            }
            mCursor.close();
            tempEncounterModels = new ArrayList<EncounterModel>();
            for (int i = arrayEncounterModels.size() - 1; i >= 0; i--) {
                tempEncounterModels.add(arrayEncounterModels.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempEncounterModels;

    }


    /**
     * This method creates or updates the Medication details
     *
     * @param encounterModel
     * @return
     * @throws Exception
     */
    public boolean createOrUpdateEncounterModel(EncounterModel encounterModel) throws Exception {
        EncounterModel existingObject = fetchEncounter(encounterModel.getEncounterID());

        if (existingObject != null) {
            database.delete(ENCOUNTER_TABLE, ENCOUNTERID + "=" + encounterModel.getEncounterID(), null);
        }

        ContentValues values = new ContentValues();
        values.put(ENCOUNTERID, encounterModel.getEncounterID());
        values.put(FROMDATE, encounterModel.getFromDate());
        values.put(TODATE, encounterModel.getToDate());
        values.put(STATUS, encounterModel.getStatus());
        values.put(PHYSICIANID, encounterModel.getPhysicianID());
        values.put(PATIENTID, encounterModel.getPatientID());


        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        /*
         * Calendar calender = Calendar.getInstance(); calender.set(Calendar.DAY_OF_MONTH, -1);
		 */
        String date = sdf.format(new Date());

        values.put(UPDATEDDATE, date);

        long count = database.insert(ENCOUNTER_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    public boolean deleteEncounter(int encounterID) throws Exception {
        EncounterModel existingObject = fetchEncounter(encounterID);
        long count = -1;
        if (existingObject != null) {
            count = database.delete(ENCOUNTER_TABLE, ENCOUNTERID + "=" + encounterID, null);
        }
        if (count == -1) {
            return false;
        }
        return true;
    }

    public boolean checkIfEncounterExists(int encounterID) throws Exception {
        EncounterModel existingObject = fetchEncounter(encounterID);
        if (existingObject != null) {
            return true;
        } else
            return false;

    }

    /*
    * Called when medication prescription is updated or created. for listing we check if the any medication prescription is active this method checks
    * if the prescription endDate is greater than corresponding value in encounterTable, the greater value is added
    */
    public boolean updateEncounterEndDateBasedOnPrescriptionEndDate(int encounterID, String endDate) {
        long count = -1;
        String query = "SELECT toDate from " + ENCOUNTER_TABLE + " WHERE " + ENCOUNTERID + "=" + encounterID;
        Cursor mCursor = database.rawQuery(query, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
            String toDate = mCursor.getString(0);
            try {
                if ((toDate != null && !toDate.isEmpty()) && (!sdf.parse(toDate).before(sdf.parse(endDate)))) {
                    return true;
                } else {
                    return updateEncounterEndDate(encounterID, endDate);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                mCursor.close();
            }
        } else {
            // update the toDate
            mCursor.close();
            return updateEncounterEndDate(encounterID, endDate);
        }


        return true;
    }

    private boolean updateEncounterEndDate(int encounterID, String endDate) {
        long count = -1;
        ContentValues contentValues = new ContentValues();
        contentValues.put("toDate", endDate);
        count = database.update(ENCOUNTER_TABLE, contentValues, ENCOUNTERID + "=" + encounterID, null);

        if (count == -1) {
            return false;
        }
        return true;
    }

    public boolean updateEncounterLastUpdatedTime(int encounterID, ContentValues updatedEncounter, String endDate) throws Exception {
        long count = -1;
        String query = "SELECT * from " + ENCOUNTER_TABLE + " WHERE " + ENCOUNTERID + "=" + encounterID + " AND " + TODATE + " >= date('now')";
        Cursor mCursor = database.rawQuery(query, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            updatedEncounter.put(TODATE, endDate);
        }
        count = database.update(ENCOUNTER_TABLE, updatedEncounter, ENCOUNTERID + "=" + encounterID, null);
        if (count == -1) {
            return false;
        }
        return true;
    }


}
