package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by administrate on 1/21/2016.
 */

public class SettingsDBAdapter {

    private static SettingsDBAdapter settingsDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static final String SettingMO_TABLE = "SettingMO";

    private static String SERVERIP="serverIP";
    private static String SERVERPORT="serverPort";
    private static String USEHTTPS="useHTTPS";
    private static String PERSISTENCEKEY="persistenceKey";
    private static String AUTOLOGOUT="autoLogout";
    private static String AUTOLOGOUTTIMEINTERVAL  ="autoLogoutTimeInterval";
    private static String FISIKESERVERHOST="fisikeServerHost";
    private static String FISIKESERVERIP="fisikeServerIp";
    private static String FISIKESERVERPORT="fisikeServerPort";
    private static String FISIKEUSEHTTPS="fisikeUseHTTPS";
    private static String FISIKERESOURCE="fisikeResource";
    private static String NOOFMSGSTOSTORELOCALLY="noOfMsgsToStoreLocally";
    private static String NOOFDAYSOFMSGSTOSTORELOCALLY="noOfDaysOfMsgsToStoreLocally";
    private static String XMPPSERVER="xmppServer";

    public SettingsDBAdapter(Context context) {
        this.context = context;
    }

    public static SettingsDBAdapter getInstance(Context ctx) {
        if (null == settingsDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (settingsDBAdapter == null) {
                    settingsDBAdapter = new SettingsDBAdapter(ctx);
                    settingsDBAdapter.open();
                }
            }
        }
        return settingsDBAdapter;
    }

    public void open() throws SQLException
    {
        database = DBHelper.getInstance(context);
    }



    //Store user Settings in DB
    public boolean createOrUpdateSettingMO(SettingMO settingMO) throws Exception {
        SettingMO existingSettingMO = fetchSettingMO(settingMO.getPersistenceKey() + "");

        if (existingSettingMO != null)
        {
            database.delete(SettingMO_TABLE,  PERSISTENCEKEY+ "=" + settingMO.getPersistenceKey(), null);
        }

        ContentValues values = new ContentValues();
        values.put(SERVERIP, settingMO.getServerIP());
        values.put(SERVERPORT, settingMO.getServerPort());
        values.put(USEHTTPS, settingMO.isUseHTTPS() ? 1 : 0);
        values.put(PERSISTENCEKEY, settingMO.getPersistenceKey());
        values.put(AUTOLOGOUT, settingMO.isAutoLogout() ? 1 : 0);
        values.put(AUTOLOGOUTTIMEINTERVAL, settingMO.getAutoLogoutTimeInterval());
        values.put(FISIKESERVERIP, settingMO.getFisikeServerIp());
        values.put(FISIKESERVERPORT, settingMO.getFisikeServerPort());
        values.put(FISIKEUSEHTTPS, settingMO.isFisikeUseHTTPS() ? 1 : 0);
        values.put(FISIKESERVERHOST, settingMO.getFisikeServerHost());
        values.put(FISIKERESOURCE, settingMO.getFisikeResource());
        values.put(NOOFMSGSTOSTORELOCALLY, settingMO.getNoOfMsgsToStoreLocally());
        values.put(NOOFDAYSOFMSGSTOSTORELOCALLY, settingMO.getNoOfDaysOfMsgsToStoreLocally());
        values.put(XMPPSERVER, settingMO.getXmppServerType());

        long count = database.insert(SettingMO_TABLE, null, values);
        if (count == -1) {
              return false;
        }
        return true;
    }


    //fetching settings from server,ports etc

    public SettingMO fetchSettingMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.query(true, SettingMO_TABLE, null, PERSISTENCEKEY+" =" + persistenceKey, null,
                null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            SettingMO settingMO = new SettingMO();

            settingMO.setServerIP(mCursor.getString(mCursor.getColumnIndex(SERVERIP)));
            settingMO.setServerPort(mCursor.getString(mCursor.getColumnIndex(SERVERPORT)));
            settingMO.setUseHTTPS(mCursor.getInt(mCursor.getColumnIndex(USEHTTPS)) == 1 ? true : false);
            settingMO.setPersistenceKey(Long.parseLong(persistenceKey));
            settingMO.setAutoLogout(mCursor.getInt(mCursor.getColumnIndex(AUTOLOGOUT)) == 1 ? true : false);
            settingMO.setAutoLogoutTimeInterval(mCursor.getFloat(mCursor.getColumnIndex(AUTOLOGOUTTIMEINTERVAL)));
            settingMO.setFisikeServerIp(mCursor.getString(mCursor.getColumnIndex(FISIKESERVERIP)));
            settingMO.setFisikeServerPort(mCursor.getString(mCursor.getColumnIndex(FISIKESERVERPORT)));
            settingMO.setFisikeUseHTTPS(mCursor.getInt(mCursor.getColumnIndex(FISIKEUSEHTTPS)) == 1 ? true : false);
            settingMO.setNoOfMsgsToStoreLocally(mCursor.getInt(mCursor.getColumnIndex(NOOFMSGSTOSTORELOCALLY)));
            settingMO.setNoOfDaysOfMsgsToStoreLocally(mCursor.getInt(mCursor.getColumnIndex(NOOFDAYSOFMSGSTOSTORELOCALLY)));
            settingMO.setFisikeServerHost(mCursor.getString(mCursor.getColumnIndex(FISIKESERVERHOST)));
            settingMO.setFisikeResource(mCursor.getString(mCursor.getColumnIndex(FISIKERESOURCE)));
            // Set the xmpp server type
            settingMO.setXmppServerType(mCursor.getString(mCursor.getColumnIndex(XMPPSERVER)));

            mCursor.close();
            return settingMO;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public static String createSettingMoQuery()
    {
        String query;
        query="CREATE TABLE "+SettingMO_TABLE+"("
                + SERVERIP +" varchar(255),"
                + SERVERPORT +" varchar(10),"
                +USEHTTPS+" INT,"
                +PERSISTENCEKEY+" varchar(255),"
                +AUTOLOGOUT+" INT,"
                +AUTOLOGOUTTIMEINTERVAL+" FLOAT,"
                +FISIKESERVERHOST+" varchar(255),"
                +FISIKESERVERIP+" varchar(255),"
                +FISIKESERVERPORT+" varchar(255),"
                +FISIKEUSEHTTPS+" INT,"
                +FISIKERESOURCE+" varchar(255),"
                +NOOFMSGSTOSTORELOCALLY+" INT,"
                +NOOFDAYSOFMSGSTOSTORELOCALLY+" INT, "
                +XMPPSERVER+" varchar(255))";
        AppLog.d("CREATE SETTINGS TABLE :", query);
        return  query;
    }
}
