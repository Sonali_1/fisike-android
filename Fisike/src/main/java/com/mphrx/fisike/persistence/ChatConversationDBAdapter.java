package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;

import com.mphrx.fisike_physician.utils.AppLog;

import com.mphrx.fisike_physician.utils.TextPattern;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Created by administrate on 1/25/2016.
 */
public class ChatConversationDBAdapter {

    private static ChatConversationDBAdapter ChatConversationDBAdapter;
    private static final String ChatConversationMO_TABLE = "ChatConversationMO";
    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;
    private static String USERNAME = "userName";
    private static String NICKNAME = "nickName";
    private static String GROUPNAME = "groupName";
    private static String CREATEDTIMEUTC = "createdTimeUTC";
    private static String READSTATUS = "readStatus";
    private static String PERSISTENCEKEY = "persistenceKey";
    private static String CHATMESSAGEKEYLIST = "chatMessageKeyList";
    private static String OFFLINECHATMESSAGEKEYLIST = "offlineChatMessageKeyList";
    private static String PHONENUMBER = "phoneNumber";
    private static String AUTOLOGOUT = "autoLogout";
    private static String UNREADCOUNT = "unReadCount";
    private static String LASTMSG = "lastMsg";
    private static String LASTMSGSTATUS = "lastMsgStatus";
    private static String LASTMSGID = "lastMsgId";
    private static String CHATCONTACTMOKEYS = "chatContactMoKeys";
    private static String CONVERSATIONTYPE = "conversationType";
    private static String MYSTATUS = "myStatus";
    private static String TYPEDATAMOPK = "typeDataMOPK";
    private static String SECOUNDRYJID = "secoundryJid";
    private static String MUTECHATTILLTIME = "muteChatTillTime";
    private static String ISNOTIFICATIONENABLED = "isNotificationEnabled";
    private static String GROUPSTATUS = "groupStatus";
    private static String IS_CHAT_INITIATED = "isChatInitiated";

    public static String getOFFLINECHATMESSAGEKEYLIST() {
        return OFFLINECHATMESSAGEKEYLIST;
    }

    public static String getPHONENUMBER() {
        return PHONENUMBER;
    }

    public static String getAUTOLOGOUT() {
        return AUTOLOGOUT;
    }

    public static String getTYPEDATAMOPK() {
        return TYPEDATAMOPK;
    }

    public static String getMUTECHATTILLTIME() {
        return MUTECHATTILLTIME;
    }

    public static String getISNOTIFICATIONENABLED() {
        return ISNOTIFICATIONENABLED;
    }

    public static String getIsChatInitiated() {
        return IS_CHAT_INITIATED;
    }

    public static String getMYSTATUS() {
        return MYSTATUS;
    }

    public static String getCONVERSATIONTYPE() {
        return CONVERSATIONTYPE;
    }

    public static String getSECOUNDRYJID() {

        return SECOUNDRYJID;
    }

    public static String getGROUPSTATUS() {
        return GROUPSTATUS;
    }

    public static String getREADSTATUS() {

        return READSTATUS;
    }

    public static String getNICKNAME() {
        return NICKNAME;
    }

    public static String getGROUPNAME() {
        return GROUPNAME;
    }


    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getLASTMSG() {
        return LASTMSG;
    }

    public static String getLASTMSGSTATUS() {
        return LASTMSGSTATUS;
    }

    public static String getLASTMSGID() {
        return LASTMSGID;
    }


    public ChatConversationDBAdapter(Context context) {
        this.context = context;
    }

    public static ChatConversationDBAdapter getInstance(Context ctx) {
        if (null == ChatConversationDBAdapter) {
            synchronized (ChatConversationDBAdapter.class) {
                if (ChatConversationDBAdapter == null) {
                    ChatConversationDBAdapter = new ChatConversationDBAdapter(ctx);
                    ChatConversationDBAdapter.open();
                }
            }
        }
        return ChatConversationDBAdapter;
    }

    public static String getUSERNAME() {
        return USERNAME;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    public ArrayList<String> addContactMoToConversation(ChatConversationMO chatConvMO, String chatContactMoPersistanceKey, String userId) {
        ArrayList<String> chatContactMoKeys = chatConvMO.getChatContactMoKeys();

        try {

            if (chatContactMoKeys == null) {
                chatContactMoKeys = new ArrayList<>();
                chatContactMoKeys.add(ChatContactMO.getPersistenceKey(userId));
            }
            if (chatContactMoKeys.contains(chatContactMoPersistanceKey)) {
                return chatContactMoKeys;
            }
            chatContactMoKeys.add(chatContactMoPersistanceKey);
            ContentValues values = new ContentValues();
            if (chatContactMoKeys.size() > 0) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatContactMoKeys);
                values.put(CHATCONTACTMOKEYS, bytes.toByteArray());
            }
            long count = database.update(ChatConversationMO_TABLE, values, PERSISTENCEKEY + "=?", new String[]{chatConvMO.getPersistenceKey() + ""});

            AppLog.d("ChatConversationMO", "==>ChatConversationMO  update at " + count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chatContactMoKeys;

    }

    /**
     * This method will fetch a chat conversationMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */
    public synchronized ChatConversationMO fetchChatConversationMO(String persistenceKey) throws Exception {
        Cursor mCursor = null;
        ChatConversationMO chatConvMO = null;
        try {

            mCursor = database.query(true, ChatConversationMO_TABLE, null, PERSISTENCEKEY + " =" + persistenceKey, null, null,
                    null, null, null);
            if (mCursor != null && mCursor.moveToFirst()) {
                chatConvMO = getChatConversationMO(mCursor, persistenceKey);
            } else {
                AppLog.d("DB ", "No row returned for ChatConversationMO for persistenceKey:" + persistenceKey);
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            return chatConvMO;
        }

    }

    private ChatConversationMO getChatConversationMO(Cursor mCursor, String persistenceKey) throws IOException, ClassNotFoundException {
        ChatConversationMO chatConvMO = new ChatConversationMO();

        chatConvMO.setJId(mCursor.getString(mCursor.getColumnIndex(USERNAME)));
        chatConvMO.setSecoundryJid(mCursor.getString(mCursor.getColumnIndex(SECOUNDRYJID)));

        chatConvMO.setNickName(mCursor.getString(mCursor.getColumnIndex(NICKNAME)));
        chatConvMO.setGroupName(mCursor.getString(mCursor.getColumnIndex(GROUPNAME)));
        chatConvMO.setCreatedTimeUTC(mCursor.getLong(mCursor.getColumnIndex(CREATEDTIMEUTC)));
        chatConvMO.setReadStatus(mCursor.getInt(mCursor.getColumnIndex(READSTATUS)) == 1 ? true : false);
        chatConvMO.setUnReadCount(mCursor.getInt(mCursor.getColumnIndex(UNREADCOUNT)));
        if (persistenceKey != null) {
            chatConvMO.setPersistenceKey(Long.parseLong(persistenceKey));
        } else {
            chatConvMO.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));

        }
        chatConvMO.setLastMsg(mCursor.getString(mCursor.getColumnIndex(LASTMSG)));
        chatConvMO.setLastMsgStatus(mCursor.getInt(mCursor.getColumnIndex(LASTMSGSTATUS)));
        chatConvMO.setLastMsgId(mCursor.getString(mCursor.getColumnIndex(LASTMSGID)));
        chatConvMO.setIsChatInitiated(mCursor.getInt(mCursor.getColumnIndex(IS_CHAT_INITIATED)));

        chatConvMO.setPhoneNumber(mCursor.getString(mCursor.getColumnIndex(PHONENUMBER)));

        byte[] listBytes = mCursor.getBlob(mCursor.getColumnIndex(CHATMESSAGEKEYLIST));
        if (listBytes != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(listBytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            Object o = ois.readObject();
            Vector<String> listVector;
            if (o instanceof ArrayList) {
                listVector = new Vector<>();
                listVector.addAll((ArrayList<String>) o);
            } else {
                listVector = (Vector<String>) o;
            }
            chatConvMO.setChatMessageKeyList(listVector);

        }

        byte[] offlineListBytes = mCursor.getBlob(mCursor.getColumnIndex(OFFLINECHATMESSAGEKEYLIST));
        if (offlineListBytes != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(offlineListBytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            Vector<String> listVector = (Vector<String>) ois.readObject();
            chatConvMO.setOfflineChatMessageKeyList(listVector);
        }

        byte[] chatContactMoKeysBytes = mCursor.getBlob(mCursor.getColumnIndex(CHATCONTACTMOKEYS));
        if (chatContactMoKeysBytes != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(chatContactMoKeysBytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            ArrayList<String> listVector = (ArrayList<String>) ois.readObject();
            chatConvMO.setChatContactMoKeys(listVector);
        }

        chatConvMO.setConversationType(mCursor.getInt(mCursor.getColumnIndex(CONVERSATIONTYPE)));
        chatConvMO.setMyStatus(mCursor.getInt(mCursor.getColumnIndex(MYSTATUS)));
        chatConvMO.setTypeDataMOPK(mCursor.getString(mCursor.getColumnIndex(TYPEDATAMOPK)));
        chatConvMO.setIsNotificationEnabled(mCursor.getInt(mCursor.getColumnIndex(ISNOTIFICATIONENABLED)));
        chatConvMO.setMuteChatTillTime(mCursor.getLong(mCursor.getColumnIndex(MUTECHATTILLTIME)));

        chatConvMO.setGroupStatus(mCursor.getInt(mCursor.getColumnIndex(GROUPSTATUS)));

        return chatConvMO;
    }


    /**
     * This method will fetch a chat conversationMO from the db
     *
     * @param persistenceKey
     * @return
     * @throws Exception
     */

    public ChatConversationMO fetchTargettedConversationMO(String persistenceKey) throws Exception {
        Cursor mCursor = database.rawQuery(
                "Select * from " + ChatConversationMO_TABLE + " where " + PERSISTENCEKEY + " =  \"" + persistenceKey + "\"", null);

        if (mCursor != null && mCursor.moveToFirst()) {
            ChatConversationMO chatConvMO = getChatConversationMO(mCursor, persistenceKey);
            mCursor.close();
            return chatConvMO;
        } else {
            // LogManager.getInstance().writeToLog("No row returned for ChatConversationMO for persistenceKey:" + persistenceKey);
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public static String getChatConversationMO_TABLE() {
        return ChatConversationMO_TABLE;
    }

//    public void updateChatConversationMo(ChatConversationMO chatConvMO, ContentValues updatedValue) {
//        try {
//            ChatConversationMO existingObject = fetchChatConversationMO(chatConvMO.getPersistenceKey() + "");
//            if (existingObject != null) {
//                database.update(ChatConversationMO_TABLE, updatedValue, "persistenceKey" + "=" + chatConvMO.getPersistenceKey(), null);
//            } else {
//                createOrUpdateChatConversationMO(chatConvMO, false);
//            }
//        } catch (Exception e) {
//
//        }
//    }

    public static String getUNREADCOUNT() {
        return UNREADCOUNT;
    }

    public static String getCREATEDTIMEUTC() {
        return CREATEDTIMEUTC;

    }

    public static String getCHATMESSAGEKEYLIST() {
        return CHATMESSAGEKEYLIST;
    }

    public static String getCHATCONTACTMOKEYS() {
        return CHATCONTACTMOKEYS;
    }

    public List<ChatConversationMO> getConverstionsListOfAUser(String userId) {

        Cursor mCursor = database.query(true, ChatConversationMO_TABLE, null, null, null, null, null, null, null);

        userId = TextPattern.getUserId(userId);

        String userPersistanceKey = ChatContactMO.getPersistenceKey(userId);

        List<ChatConversationMO> chatConversationMOs = new ArrayList<>();

        while (mCursor.moveToNext()) {

            ChatConversationMO conversationMO = null;

            try {
                conversationMO = getChatConversationMO(mCursor, null);
            } catch (IOException e) {
                e.printStackTrace();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();

            }

            if (conversationMO == null) {
                continue;
            }
            List<String> chatContactMoKeys = conversationMO.getChatContactMoKeys();

            if (chatContactMoKeys != null && chatContactMoKeys.contains(userPersistanceKey)) { //conversationMO.getJId().contains("_" + userId) ||

                chatConversationMOs.add(conversationMO);

            }

        }

        mCursor.close();

        return chatConversationMOs;

    }

    public void clearChat(long convPersistenceKey) {
        try {
            database.delete(ChatMessageDBAdapter.getChatMessageMO_TABLE(), ChatMessageDBAdapter.getCONVPERSISTENCEKEY() + "=" + convPersistenceKey, null);
            database.delete(AttchmentDBAdapter.getAttachmentMO_TABLE(), AttchmentDBAdapter.getCHATCONVERSATIONMOPKEY() + "=" + convPersistenceKey, null);

            ContentValues contentValues = new ContentValues();
            contentValues.put(LASTMSG, "");
            contentValues.put(READSTATUS, 1);
            contentValues.put(UNREADCOUNT, 0);
            contentValues.putNull(CHATMESSAGEKEYLIST);
            contentValues.putNull(OFFLINECHATMESSAGEKEYLIST);
            contentValues.put(CREATEDTIMEUTC, 0);
            updateChatConversation(convPersistenceKey, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long updateChatConversation(long pk, ContentValues contentValues) {
        try {
            return database.update(ChatConversationMO_TABLE, contentValues, PERSISTENCEKEY + "=" + pk, null);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public boolean insertChatConversationMo(ChatConversationMO chatConvMO) {
        try {
            ContentValues values = new ContentValues();

            if (chatConvMO.getChatMessageKeyList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatConvMO.getChatMessageKeyList());
                values.put(CHATMESSAGEKEYLIST, bytes.toByteArray());
            }

            if (chatConvMO.getOfflineChatMessageKeyList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatConvMO.getOfflineChatMessageKeyList());
                values.put(OFFLINECHATMESSAGEKEYLIST, bytes.toByteArray());
            }

            values.put(USERNAME, chatConvMO.getJId());
            values.put(SECOUNDRYJID, chatConvMO.getSecoundryJid());
            String nickName = chatConvMO.getNickName();
            values.put(NICKNAME, nickName);
            values.put(GROUPNAME, nickName);
            values.put(UNREADCOUNT, chatConvMO.getUnReadCount());

            // Manohar
            values.put(IS_CHAT_INITIATED, chatConvMO.getIsChatInitiated());

            values.put(READSTATUS, chatConvMO.getReadStatus() ? 1 : 0);

            values.put(PERSISTENCEKEY, chatConvMO.getPersistenceKey());
            values.put(PHONENUMBER, chatConvMO.getPhoneNumber());

            // Added new values for last messages in this conversation
            String lastMsg = chatConvMO.getLastMsg();
            values.put(LASTMSG, chatConvMO.getLastMsg());
            values.put(LASTMSGSTATUS, chatConvMO.getLastMsgStatus());
            values.put(LASTMSGID, chatConvMO.getLastMsgId());
            values.put(CREATEDTIMEUTC, chatConvMO.getCreatedTimeUTC());

            // new group related changes
            if (chatConvMO.getChatContactMoKeys() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatConvMO.getChatContactMoKeys());
                values.put(CHATCONTACTMOKEYS, bytes.toByteArray());
            }
            values.put(CONVERSATIONTYPE, chatConvMO.getConversationType());
            values.put(MYSTATUS, chatConvMO.getMyStatus());
            values.put(TYPEDATAMOPK, chatConvMO.getTypeDataMOPK());
            values.put(MUTECHATTILLTIME, chatConvMO.getMuteChatTillTime());
            values.put(ISNOTIFICATIONENABLED, chatConvMO.getIsNotificationEnabled());

            values.put(GROUPSTATUS, chatConvMO.getGroupStatus());

            long count = database.insert(ChatConversationMO_TABLE, null, values);
            if (count == -1) {
                AppLog.d("Error inserting row for ChatConversationMO for persistenceKey:", chatConvMO.getPersistenceKey() + "");
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String createChatConversationTableQuery() {
        String query = "CREATE TABLE " + ChatConversationMO_TABLE + " (" +
                USERNAME + " varchar(255), " +
                NICKNAME + " varchar(255), " +
                GROUPNAME + " varchar(255), " +
                CREATEDTIMEUTC + " LONG DEFAULT 0, " +
                READSTATUS + " INT, " +
                PERSISTENCEKEY + " varchar(255), " +
                CHATMESSAGEKEYLIST + " BLOB, " +
                OFFLINECHATMESSAGEKEYLIST + " BLOB, " +
                PHONENUMBER + " varchar(255) DEFAULT '', " +
                AUTOLOGOUT + " INT, " +
                IS_CHAT_INITIATED + " INT, " +
                UNREADCOUNT + " varchar(255), " +
                /*
                TODO
                handle update as DataType for unread count was varchat(255)
                * */
                LASTMSG + " varchar(255), " +
                LASTMSGSTATUS + " INT, " +
                LASTMSGID + " varchar(255), " +
                CHATCONTACTMOKEYS + " BLOB, " +
                CONVERSATIONTYPE + " INT DEFAULT " + VariableConstants.conversationTypeChat + ", myStatus INT, " +
                TYPEDATAMOPK + " varchar(255), " +
                SECOUNDRYJID + " varchar(255), " +
                MUTECHATTILLTIME + " LONG, " +
                ISNOTIFICATIONENABLED + " Integer, " +
                GROUPSTATUS + " Integer ) ";

        AppLog.d(" CREATE " + ChatConversationMO_TABLE, query);
        return query;
    }

    public ArrayList<ChatConversationMO> getAllChatConversationMo() {
        Cursor mCursor = database.query(true, ChatConversationMO_TABLE, null, null, null, null,
                null, null, null);
        ArrayList<ChatConversationMO> chatConversationMOs = new ArrayList<>();
        if (mCursor.getCount() > 0) {
            while (mCursor.moveToNext()) {
                try {
                    ChatConversationMO mo = getChatConversationMO(mCursor, null);
                    chatConversationMOs.add(mo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        mCursor.close();
        return chatConversationMOs;
    }


    public void updateIsChatInitiated(long persistenceKey) {
        ContentValues isChatInitiated = new ContentValues();
        isChatInitiated.put(IS_CHAT_INITIATED, 1);
        updateChatConversation(persistenceKey, isChatInitiated);
    }
}
