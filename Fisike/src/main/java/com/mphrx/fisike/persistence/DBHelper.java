package com.mphrx.fisike.persistence;

import android.content.Context;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by xmb2nc on 21-01-2016.
 */
public class DBHelper
{
    private static SQLiteDatabase database;
    private static DBManager dbManager;
    private static DBHelper dbHelper;
    private static Context context;


    public static SQLiteDatabase getInstance(Context ctx)
    {
        if (database == null)
        {
            synchronized (DBHelper.class)
            {
                if (database == null)
                {
                     context=ctx;
                     database = DBHelper.open();
                }
            }
        }
        return database;
    }


    public static SQLiteDatabase open() throws SQLException {
        if (database == null || !database.isOpen()) {
            dbManager = new DBManager(context);
        }

        try{
            database = dbManager.getWritableDatabase(Utils.generateUniqueKey(context));
        }
        catch (Exception e)
        {
            database=dbManager.getWritableDatabase(VariableConstants.DB_KEY);
            dbManager.updateToPasskey(database);
            database = dbManager.getWritableDatabase(Utils.generateUniqueKey(context));
        }

        return database;
    }

    public void close() {
        if (database.isOpen()) {
            dbManager.close();
            if (dbHelper != null) {
                dbHelper = null;
            }
        }
    }

}
