package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.DoseModel;
import com.mphrx.fisike.models.MedicationPrescriptionModel;
import com.mphrx.fisike.models.ReminderModel;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by administrate on 1/11/2016.
 */
public class MedicationPrescriptionDBAdapter {
    private static MedicationPrescriptionDBAdapter medicationDbAdapter;
    private Context context;
    private SQLiteDatabase database;

    private static final String MEDICATION_PRESCRIPTION_TABLE = "MedicationPrescription";
    private static final String TEMP_MEDICATION_PRESCRIPTION_TABLE = "TempMedicationPrescription";


    private static String ID = "ID";
    private static String ENCOUNTERID = "encounterID";
    private static String PRACTITIONARID = "practitionarID";
    private static String UNIT = "unit";
    private static String FROMDATE = "fromDate";
    private static String TODATE = "toDate";
    private static String DOSAGE = "dosage";
    private static String REMINDER = "reminder";
    private static String PATIENTID = "patientID";
    private static String MEDICIANID = "medicianId";
    private static String PRACTIONERNAME = "practionerName";
    private static String MEDICINENAME = "medicineName";
    private static String ISALARMSET = "isAlarmSet";
    private static String DISEASELIST = "diseaseList";
    private static String STATUS = "status";
    private static String UPDATEDDATE = "updatedDate";

    public static String getMedicationPrescriptionTable() {
        return MEDICATION_PRESCRIPTION_TABLE;
    }

    public MedicationPrescriptionDBAdapter(Context context) {
        this.context = context;
    }

    public static MedicationPrescriptionDBAdapter getInstance(Context ctx) {
        if (null == medicationDbAdapter) {
            synchronized (MedicationPrescriptionDBAdapter.class) {
                if (medicationDbAdapter == null) {
                    medicationDbAdapter = new MedicationPrescriptionDBAdapter(ctx);
                    medicationDbAdapter.open();
                }
            }
        }
        return medicationDbAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public void updatePrescriptionDisease(String encounterId, ArrayList<DiseaseMO> disease) {

        String columnName = "";
        ContentValues values = new ContentValues();

        if (disease != null) {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(disease);
                values.put(DISEASELIST, bytes.toByteArray());
                int i = database.update(MEDICATION_PRESCRIPTION_TABLE, values, ENCOUNTERID + "=?", new String[]{encounterId});

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public ArrayList<MedicationPrescriptionModel> getMedicationPrescriptionsWhoseAlarmIsActive() throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, ISALARMSET + " >" + 0, null, null, null, null, null);
        ArrayList<MedicationPrescriptionModel> activeAlarmMedicationPrescriptionList = new ArrayList<MedicationPrescriptionModel>();
        while (mCursor != null && mCursor.moveToNext()) {
            MedicationPrescriptionModel medicationPrescriptionMo = new MedicationPrescriptionModel();
            medicationPrescriptionMo.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
            medicationPrescriptionMo.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            medicationPrescriptionMo.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
            medicationPrescriptionMo.setUnit(mCursor.getString(mCursor.getColumnIndex(UNIT)));
            medicationPrescriptionMo.setFromDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            medicationPrescriptionMo.setToDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DoseModel> dosesList = (ArrayList<DoseModel>) ois.readObject();
                medicationPrescriptionMo.setArrayDose(dosesList);
            }
            byte[] reminderListBytes = mCursor.getBlob(mCursor.getColumnIndex(REMINDER));
            if (reminderListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(reminderListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<ReminderModel> reminderList = (ArrayList<ReminderModel>) ois.readObject();
                medicationPrescriptionMo.setArrayReminder(reminderList);
            }
            medicationPrescriptionMo.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));

            medicationPrescriptionMo.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
            medicationPrescriptionMo.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
            medicationPrescriptionMo.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
            medicationPrescriptionMo.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));
            byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
            if (diseaseListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                medicationPrescriptionMo.setDiseaseMoList(diseaseList);
            }
            activeAlarmMedicationPrescriptionList.add(medicationPrescriptionMo);
        }

        if (mCursor != null) {
            mCursor.close();
        }
        return activeAlarmMedicationPrescriptionList;
    }


    /**
     * This method will fetch a MedicationPrescriptionModel from the db
     *
     * @param id
     * @return
     * @throws Exception
     */
    public MedicationPrescriptionModel fetchMedicationPrescription(int id) throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, ID + " =" + id, null, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            MedicationPrescriptionModel medicationPrescriptionMo = new MedicationPrescriptionModel();
            medicationPrescriptionMo.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
            medicationPrescriptionMo.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            medicationPrescriptionMo.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
            medicationPrescriptionMo.setUnit(mCursor.getString(mCursor.getColumnIndex(UNIT)));
            medicationPrescriptionMo.setFromDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            medicationPrescriptionMo.setToDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DoseModel> dosesList = (ArrayList<DoseModel>) ois.readObject();
                medicationPrescriptionMo.setArrayDose(dosesList);
            }
            byte[] reminderListBytes = mCursor.getBlob(mCursor.getColumnIndex(REMINDER));
            if (reminderListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(reminderListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<ReminderModel> reminderList = (ArrayList<ReminderModel>) ois.readObject();
                medicationPrescriptionMo.setArrayReminder(reminderList);
            }
            medicationPrescriptionMo.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));

            medicationPrescriptionMo.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
            medicationPrescriptionMo.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
            medicationPrescriptionMo.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
            medicationPrescriptionMo.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));

            byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
            if (diseaseListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                medicationPrescriptionMo.setDiseaseMoList(diseaseList);
            }
            mCursor.close();
            return medicationPrescriptionMo;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    /**
     * This method will fetch a MedicationPrescriptionModel from the db
     *
     * @param encounterID
     * @return
     * @throws Exception
     */
    public ArrayList<MedicationPrescriptionModel> fetchMedicationPrescriptionOnEncounter(int encounterID) throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, null, ENCOUNTERID + " =" + encounterID, null, null, null, null, null);

        ArrayList<MedicationPrescriptionModel> arrayMedicationPrescriptionModels = new ArrayList<MedicationPrescriptionModel>();
        while (mCursor != null && mCursor.moveToNext()) {
            MedicationPrescriptionModel medicationPrescriptionModel = new MedicationPrescriptionModel();
            medicationPrescriptionModel.setID(mCursor.getInt(mCursor.getColumnIndex(ID)));
            medicationPrescriptionModel.setEncounterID(mCursor.getInt(mCursor.getColumnIndex(ENCOUNTERID)));
            medicationPrescriptionModel.setPractitionarID(mCursor.getInt(mCursor.getColumnIndex(PRACTITIONARID)));
            medicationPrescriptionModel.setUnit(mCursor.getString(mCursor.getColumnIndex(UNIT)));
            medicationPrescriptionModel.setFromDate(mCursor.getString(mCursor.getColumnIndex(FROMDATE)));
            medicationPrescriptionModel.setToDate(mCursor.getString(mCursor.getColumnIndex(TODATE)));
            byte[] dosesArrayBytes = mCursor.getBlob(mCursor.getColumnIndex(DOSAGE));
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DoseModel> dosesList = (ArrayList<DoseModel>) ois.readObject();
                medicationPrescriptionModel.setArrayDose(dosesList);
            }
            byte[] reminderListBytes = mCursor.getBlob(mCursor.getColumnIndex(REMINDER));
            if (reminderListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(reminderListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<ReminderModel> reminderList = (ArrayList<ReminderModel>) ois.readObject();
                medicationPrescriptionModel.setArrayReminder(reminderList);
            }
            medicationPrescriptionModel.setPatientID(mCursor.getInt(mCursor.getColumnIndex(PATIENTID)));


            medicationPrescriptionModel.setMedicianId(mCursor.getInt(mCursor.getColumnIndex(MEDICIANID)));
            medicationPrescriptionModel.setPractitionerName(mCursor.getString(mCursor.getColumnIndex(PRACTIONERNAME)));
            medicationPrescriptionModel.setMedicineName(mCursor.getString(mCursor.getColumnIndex(MEDICINENAME)));
            medicationPrescriptionModel.setAutoReminderSQLBool(mCursor.getInt(mCursor.getColumnIndex(ISALARMSET)));

            byte[] diseaseListBytes = mCursor.getBlob(mCursor.getColumnIndex(DISEASELIST));
            if (diseaseListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(diseaseListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DiseaseMO> diseaseList = (ArrayList<DiseaseMO>) ois.readObject();
                medicationPrescriptionModel.setDiseaseMoList(diseaseList);
            }
            arrayMedicationPrescriptionModels.add(medicationPrescriptionModel);
        }
        mCursor.close();
        return arrayMedicationPrescriptionModels;
    }

    // deletes the medication prescription corresponding to this prescription id
    public void deleteMedicationPrescription(int medicationPrescriptionId) {
        database.delete(MEDICATION_PRESCRIPTION_TABLE, "ID" + "=" + medicationPrescriptionId, null);
    }

    /**
     * This method creates or updates the MedicationPrescriptionModel details
     *
     * @param medicationPrescriptionModel
     * @return
     * @throws Exception
     */
    public boolean createOrUpdateMedicationPrescriptionModel(MedicationPrescriptionModel medicationPrescriptionModel) throws Exception {
        MedicationPrescriptionModel existingObject = fetchMedicationPrescription(medicationPrescriptionModel.getID());

        if (existingObject != null) {
            database.delete(MEDICATION_PRESCRIPTION_TABLE, ID + "=" + medicationPrescriptionModel.getID(), null);
        }

        ContentValues values = new ContentValues();
        values.put(ID, medicationPrescriptionModel.getID());
        values.put(ENCOUNTERID, medicationPrescriptionModel.getEncounterID());
        values.put(PRACTITIONARID, medicationPrescriptionModel.getPractitionarID());
        values.put(UNIT, medicationPrescriptionModel.getUnit());
        values.put(FROMDATE, medicationPrescriptionModel.getFromDate());
        values.put(TODATE, medicationPrescriptionModel.getToDate());
        if (null != medicationPrescriptionModel.getArrayDose()) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(medicationPrescriptionModel.getArrayDose());
            values.put(DOSAGE, bytes.toByteArray());
        }
        if (null != medicationPrescriptionModel.getArrayReminder()) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(medicationPrescriptionModel.getArrayReminder());
            values.put(REMINDER, bytes.toByteArray());
        }

        if (medicationPrescriptionModel.getDiseaseMoList() != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(medicationPrescriptionModel.getDiseaseMoList());
            values.put(DISEASELIST, bytes.toByteArray());

        }
        values.put(PATIENTID, medicationPrescriptionModel.getPatientID());

        if (medicationPrescriptionModel.getPractitionerName() != null) {
            values.put(PRACTIONERNAME, medicationPrescriptionModel.getPractitionerName());
        }
        values.put(MEDICIANID, medicationPrescriptionModel.getMedicianId());
        values.put(MEDICINENAME, medicationPrescriptionModel.getMedicineName());
        values.put(ISALARMSET, medicationPrescriptionModel.getAutoReminderSQLBool());

        long count = database.insert(MEDICATION_PRESCRIPTION_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }


    /* checks if alarm is enabled corresponding to this medication ptrescription */
    public boolean doesMedicationPrescriptionAlarmExists(int medicationPrescriptionId) {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, new String[]{ISALARMSET}, ID + " =" + medicationPrescriptionId, null,
                null, null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            if ((mCursor.getInt(mCursor.getColumnIndex(ISALARMSET))) > 0) {
                mCursor.close();
                return true;
            }
        }

        if (mCursor != null)
            mCursor.close();
        return false;
    }

    public void updatePhysicianName(int encounterID, String physicianName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRACTIONERNAME, physicianName);
        try {
            database.update(MEDICATION_PRESCRIPTION_TABLE, contentValues, ENCOUNTERID + "=" + encounterID, null);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public HashMap<String, Integer> fetchSavedReminders(int id) throws Exception {
        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, new String[]{REMINDER, ISALARMSET}, ID + " =" + id, null, null,
                null, null, null);
        HashMap<String, Integer> reminderHashMap = new HashMap<String, Integer>();

        if (mCursor != null && mCursor.moveToFirst()) {
            MedicationPrescriptionModel medicationPrescriptionMo = new MedicationPrescriptionModel();
            byte[] reminderListBytes = mCursor.getBlob(mCursor.getColumnIndex(REMINDER));
            if (reminderListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(reminderListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<ReminderModel> reminderList = (ArrayList<ReminderModel>) ois.readObject();
                for (ReminderModel reminderModel : reminderList) {
                    reminderHashMap.put(reminderModel.getDescription(), reminderModel.getAlarmManagerUniqueKey());
                }
            }
            mCursor.close();
            return reminderHashMap;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    public void updateMedicationPrescriptionDiseaseName(int encounterID, ArrayList<DiseaseMO> diseaseList) {
        ContentValues contentValues = new ContentValues();
        try {
            if (diseaseList != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(diseaseList);
                contentValues.put(DISEASELIST, bytes.toByteArray());

            }
            database.update(MEDICATION_PRESCRIPTION_TABLE, contentValues, ENCOUNTERID + "=" + encounterID, null);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static String createMedicationPrescriptionTableQuery() {
        String query = "CREATE TABLE " + MEDICATION_PRESCRIPTION_TABLE + " ( " +
                ID + " INTEGER PRIMARY KEY, " +
                ENCOUNTERID + " Integer, " +
                PRACTITIONARID + " Integer, " +
                UNIT + " varchar(255), " +
                FROMDATE + " varchar(255), " +
                TODATE + " varchar(255), " +
                DOSAGE + " BLOB, " +
                REMINDER + " BLOB, " +
                PATIENTID + " INTEGER, " +
                MEDICIANID + " INTEGER, " +
                PRACTIONERNAME + " varchar(255), " +
                MEDICINENAME + " varchar(255), " +
                ISALARMSET + " Integer, " +
                DISEASELIST + " BLOB ," +
                STATUS + " varchar(255)," +
                UPDATEDDATE + " date)";


        AppLog.d("CREATE " + MEDICATION_PRESCRIPTION_TABLE, query);
        return query;
    }

    public static String dropTable(){
        return "DROP TABLE IF EXISTS " + MEDICATION_PRESCRIPTION_TABLE;
    }

    public ArrayList<MedicationPrescriptionModel> fetchMedicationPrescription() throws Exception {

        if (database == null) {
            open();
        }

        Cursor mCursor = database.query(true, MEDICATION_PRESCRIPTION_TABLE, new String[]{"ID", "encounterID", "practitionarID", "unit",
                "fromDate", "toDate", "dosage", "reminder", "patientID", "medicianId",
                "practionerName", "medicineName", "isAlarmSet", "diseaseName", "diseaseCodingSystem", "diseaseCode"}, null, null, null, null, null, null);

        ArrayList<MedicationPrescriptionModel> arrayMedicationPrescriptionModels = new ArrayList<MedicationPrescriptionModel>();
        while (mCursor != null && mCursor.moveToNext()) {
            MedicationPrescriptionModel medicationPrescriptionModel = new MedicationPrescriptionModel();
            medicationPrescriptionModel.setID(mCursor.getInt(0));
            medicationPrescriptionModel.setEncounterID(mCursor.getInt(1));
            medicationPrescriptionModel.setPractitionarID(mCursor.getInt(2));
            medicationPrescriptionModel.setUnit(mCursor.getString(3));
            medicationPrescriptionModel.setFromDate(mCursor.getString(4));
            medicationPrescriptionModel.setToDate(mCursor.getString(5));
            byte[] dosesArrayBytes = mCursor.getBlob(6);
            if (dosesArrayBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<DoseModel> dosesList = (ArrayList<DoseModel>) ois.readObject();
                medicationPrescriptionModel.setArrayDose(dosesList);
            }
            byte[] reminderListBytes = mCursor.getBlob(7);
            if (reminderListBytes != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(reminderListBytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<ReminderModel> reminderList = (ArrayList<ReminderModel>) ois.readObject();
                medicationPrescriptionModel.setArrayReminder(reminderList);
            }
            medicationPrescriptionModel.setPatientID(mCursor.getInt(8));


            medicationPrescriptionModel.setMedicianId(mCursor.getInt(9));
            medicationPrescriptionModel.setPractitionerName(mCursor.getString(10));
            medicationPrescriptionModel.setMedicineName(mCursor.getString(11));
            medicationPrescriptionModel.setAutoReminderSQLBool(mCursor.getInt(12));
            medicationPrescriptionModel.setDiseaseName(mCursor.getString(13));
            medicationPrescriptionModel.setDiseaseCodingSystem(mCursor.getString(14));
            medicationPrescriptionModel.setDiseaseCode(mCursor.getString(15));

            arrayMedicationPrescriptionModels.add(medicationPrescriptionModel);

        }
        mCursor.close();
        return arrayMedicationPrescriptionModels;
    }

    public void insertMedicationPrescription(MedicationPrescriptionModel medicationPrescriptionModel) {
        try {

            ContentValues values = new ContentValues();
            values.put("ID", medicationPrescriptionModel.getID());
            values.put("encounterID", medicationPrescriptionModel.getEncounterID());
            values.put("practitionarID", medicationPrescriptionModel.getPractitionarID());
            values.put("unit", medicationPrescriptionModel.getUnit());
            values.put("fromDate", medicationPrescriptionModel.getFromDate());
            values.put("toDate", medicationPrescriptionModel.getToDate());
            if (null != medicationPrescriptionModel.getArrayDose()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(medicationPrescriptionModel.getArrayDose());
                values.put("dosage", bytes.toByteArray());
            }
            if (null != medicationPrescriptionModel.getArrayReminder()) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(medicationPrescriptionModel.getArrayReminder());
                values.put("reminder", bytes.toByteArray());
            }

            if (medicationPrescriptionModel.getDiseaseMoList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(medicationPrescriptionModel.getDiseaseMoList());
                values.put("diseaseList", bytes.toByteArray());

            }
            values.put("patientID", medicationPrescriptionModel.getPatientID());

            if (medicationPrescriptionModel.getPractitionerName() != null) {
                values.put("practionerName", medicationPrescriptionModel.getPractitionerName());
            }
            values.put("medicianId", medicationPrescriptionModel.getMedicianId());
            values.put("medicineName", medicationPrescriptionModel.getMedicineName());
            values.put("isAlarmSet", medicationPrescriptionModel.getAutoReminderSQLBool());

            long count = database.insert(TEMP_MEDICATION_PRESCRIPTION_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void renameDeleteMedicationPrescriptionTable() {
        database.execSQL("DROP TABLE IF EXISTS " + MEDICATION_PRESCRIPTION_TABLE);
        database.execSQL("ALTER TABLE " + TEMP_MEDICATION_PRESCRIPTION_TABLE + " RENAME TO " + MEDICATION_PRESCRIPTION_TABLE);
    }
}
