package com.mphrx.fisike.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.LocalTimeZone;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.MedicineModelDb;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicationPrescriptionModel;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationProfileDBAdapter;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.SharedPref;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import careplan.database.databaseAdapter.CareTaskDatabaseAdapter;

public class DBManager extends SQLiteOpenHelper {

    public static final String DB_FILE_NAME = "fisike.db";
    private Context context;

    public DBManager(Context context) {
        // upto fisike release 3.9  version 7---- upgrading now
        //for apk version 1.2.4 version 8
        //southend release db version 9 released app 1.3.1
        //sprint-40 branch, upgrading to 14
        super(context, DB_FILE_NAME, null, 17);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        AppLog.d("Fisike", "com.mphrx.fisike.persistence.onCreate");

        db.execSQL(PatientDBAdapter.createPatientTableQuery());
        db.execSQL(SettingsDBAdapter.createSettingMoQuery());
        db.execSQL(ConfigDBAdapter.createConfigMoQuery());
        db.execSQL(EncounterMedicationDBAdapter.createMedicationPrescriptionTableQuery());
        db.execSQL(NotificationCenterDBAdapter.createNotificationCenterTableQuery());
        db.execSQL(DocumentReferenceDBAdapter.createDocumentReferenceTableQuery());
        db.execSQL(DiagnosticOrderDBAdapter.createPatientRecordsTableQuery());
        db.execSQL(UserDBAdapter.getCreateUserQuery());

        db.execSQL(ChatConversationDBAdapter.createChatConversationTableQuery());

        db.execSQL(ChatMessageDBAdapter.createChatMessageTableQuery());
        db.execSQL(chatContactDBAdapter.createChatContactTableQuery());

        db.execSQL(GroupChatDBAdapter.createGroupChatTableQuery());

        db.execSQL(AttchmentDBAdapter.createAttachmentTableQuery());

        db.execSQL("CREATE TABLE " + DBAdapter.USER_INVITE_TABLE + "( " + DBAdapter.USER_NUMBER + " varchar(10) PRIMARY KEY, " + DBAdapter.USER_NAME + " varchar(50), " + DBAdapter.USER_ROLE + " varchar(50), " + DBAdapter.USER_ROLE_ID + " varchar(20), " + DBAdapter.USER_INVITEE_ROLE_ID + " varchar(20), " + DBAdapter.USER_ORG_ID + " varchar(20))");

        addColumns(db);

        db.execSQL(VaccinationProfileDBAdapter.createVaccinationProfileTableQuery());
        db.execSQL(VaccinationDBAdapter.createVaccinationTableQuery());
        db.execSQL(DiagnosticReportDBAdapter.createDiagnosticReportTableQuery());
        db.execSQL(VitalsDBAdapter.createVitalsQuery());
        db.execSQL(VitalsConfigDBAdapter.getCreateVitalsConfigTableQuery());
        db.execSQL(PhysicianDBAdapter.createPhysicianTableQuery());
        //updating the query for new users
        db.execSQL(UserFileAdapter.getUserFileTableQuery());

        deleteRedundantSharedPref(db);
        deleteObservationTable(db);
        db.execSQL(AgreementFileDBAdapter.getAgreementFileQuery());
    }

    private void addColumns(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE UserMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE UserMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE UserMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE PatientMo ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE PatientMo ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE PatientMo ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE SettingMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE SettingMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE SettingMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE ChatConversationMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE ChatConversationMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE ChatConversationMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE ChatMessageMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE ChatMessageMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE ChatMessageMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE GroupTextChatMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE GroupTextChatMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE GroupTextChatMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE ChatContactMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE ChatContactMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE ChatContactMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE QuestionMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE QuestionMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE QuestionMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE QuestionActivityMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE QuestionActivityMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE QuestionActivityMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE AttachmentMo ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE AttachmentMo ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE AttachmentMo ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE Medication ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE Medication ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE Medication ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE Practitonar ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE Practitonar ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE Practitonar ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE DocumentReference ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE DocumentReference ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE DocumentReference ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE Encounter ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE Encounter ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE Encounter ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE MedicationPrescription ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE MedicationPrescription ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE MedicationPrescription ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE Observation ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE Observation ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE Observation ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE NotificationCenter ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE NotificationCenter ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE NotificationCenter ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE ConfigMO ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE ConfigMO ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE ConfigMO ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE + " ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE + " ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE + " ADD RFU3 BLOB");

            db.execSQL("ALTER TABLE " + DBAdapter.USER_INVITE_TABLE + " ADD RFU1 BLOB");
            db.execSQL("ALTER TABLE " + DBAdapter.USER_INVITE_TABLE + " ADD RFU2 BLOB");
            db.execSQL("ALTER TABLE " + DBAdapter.USER_INVITE_TABLE + " ADD RFU3 BLOB");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void migrateUserMo(SQLiteDatabase db) {
        try {
            db.execSQL("UPDATE ChatContactMO SET profilePic=NULL");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void migrateGroupTextMo(SQLiteDatabase db) {
        try {
            db.execSQL("UPDATE GroupTextChatMO SET imageData=NULL");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void migrateChatContactMO(SQLiteDatabase db) {
        try {
            db.execSQL("UPDATE ChatContactMO SET profilePic=NULL");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;

        switch (oldVersion) {
            case 1:
                updateToVersionTwo(db);
                onUpgrade(db, 2, newVersion);
                break;
            case 2:
                Network.getGeneralRequestQueue().getCache().clear();
                updateToVersionThree(db);
                onUpgrade(db, 3, newVersion);
                break;
            case 3:
                updateToVersionFour(db);
                onUpgrade(db, 4, newVersion);
                break;
            case 4:
                updateToVersionFive(db);
                onUpgrade(db, 5, newVersion);
                break;
            case 5:
                updateToVersionSix(db);
                onUpgrade(db, 6, newVersion);
                break;
            case 6:
                updateToVersionSeven(db);
                onUpgrade(db, 7, newVersion);
                break;
            case 7:
                updateToVersionEight(db);
                onUpgrade(db, 8, newVersion);
                break;
            case 8:
                updateDocumentReferenceModel(db);
                onUpgrade(db, 9, newVersion);
                break;
            case 9:
                // labmate changes
                updateToVersionTen(db);
                onUpgrade(db, 10, newVersion);
                break;
            case 10:
                updateToPasskey(db);
                updateToVersionEleven(db);
                updateVaccinationId(db);
                onUpgrade(db, 11, newVersion);
                break;
            case 11:
                try {
                    updateToVersionTwelve(db);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                onUpgrade(db, 12, newVersion);
                break;
            case 12:
                createVitalsConfigTable(db);
                updateConfigMoTable(db);
                createPhysicianMoTable(db);
                onUpgrade(db, 13, newVersion);
                break;
            case 13:
                updatePatientMoTable(db);
                createUserFilesTable(db);
                try {
                    insertSharedPrefsToDB(db);
                    deleteRedundantSharedPref(db);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //clear vitalsConfig, latest request will be fetched
                clearVitalsConfigRequest(db);

                try {
                    updateToVersionFourteen(db);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                onUpgrade(db, 14, newVersion);
                break;
            case 14:
                // adding support for performing location name
                // microbiology
                updatePatientRecords(db);
                createAgreementDb(db);
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + "ADD agreements BLOB");
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                        " ADD " + UserDBAdapter.getUSERLANGUAGE() + " varchar(255)");
                deleteObservationTable(db);
                onUpgrade(db, 14, 15);
                break;
            case 15:
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getCOVERAGE() + " BLOB");
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getTOKEN() + " varchar(255)");
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getPendingDiagnostics() + " BLOB");
                onUpgrade(db,15,newVersion);
                break;
            case 16:
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getIDENTIFIER() + " BLOB");
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getEMAILLIST() + " BLOB");
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getPHONELIST() + " BLOB");
                db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() + " ADD " + ConfigDBAdapter.OTPATTEMPTS + " varchar(255)");
                db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() + " ADD " + ConfigDBAdapter.PASSWORDREGEX + " varchar(255)");
                updateCareTaskDB(db);
                db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() + " ADD " + UserDBAdapter.getMIDDLENAME() + " varchar(255)");
                break;
        }
    }

    private void updateResult(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() + "ADD " + DiagnosticOrderDBAdapter.PATIENT_PHONE_NUMBER + " varchar(255)");
        db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() + "ADD " + DiagnosticOrderDBAdapter.PATIENT_EMAIL_ID + " varchar(255)");
        db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() + "ADD " + DiagnosticOrderDBAdapter.PATIENT_DECEASED + " INT");
        db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() + "ADD " + DiagnosticOrderDBAdapter.PATIENT_DECEASED_DATE + " varchar(255)");
    }

    private void updateConfig(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() + "ADD " + ConfigDBAdapter.LANGUAGELIST + " BLOB");
    }

    private void deleteRedundantSharedPref(SQLiteDatabase db) {

        SharedPref.setOTP(null);
        SharedPref.setUserNameTemp(null);
        SharedPreferences prefs = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(SharedPref.UPDATE_VERSION_TIME_STAMP);
        editor.remove(VariableConstants.OTP_SEND_TIME);
        editor.apply();

    }

    private void insertSharedPrefsToDB(SQLiteDatabase db) {

        ContentValues values = new ContentValues();

        values.put(UserFileAdapter.getPrimaryKey(), UserFileAdapter.getPkKey());
        if (SharedPref.getAccessToken() != null) {
            values.put(UserFileAdapter.getAuthToken(), SharedPref.getAccessToken());
            SharedPref.updatePreferences(VariableConstants.AUTH_TOKEN, null);
        }
        if (SharedPref.getMobileNumber() != null) {
            values.put(UserFileAdapter.getPhoneNumber(), SharedPref.getMobileNumber());
            SharedPref.updatePreferences(SharedPref.MOBILE_NUMBER, null);
        }

        if (SharedPref.getEmailAddress() != null) {
            values.put(UserFileAdapter.getEmailId(), SharedPref.getEmailAddress());
            SharedPref.updatePreferences(SharedPref.EMAIL_ADDRESS, null);
        }
        if (SharedPref.getLoginUserName() != null) {
            values.put(UserFileAdapter.getUSERNAME(), SharedPref.getLoginUserName());
            SharedPref.updatePreferences(SharedPref.LOGIN_USER_NAME, null);
        }

        SharedPreferences preferences = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        if (preferences.getString(VariableConstants.DEVICEUID, null) != null) {
            values.put(UserFileAdapter.getDeviceUid(), preferences.getString(VariableConstants.DEVICEUID, null));
            SharedPref.updatePreferences(VariableConstants.DEVICEUID, null);
        }

        if (preferences.getString(UserMO.KEY, null) != null) {
            long userKey = (UserMO.KEY + "-" + preferences.getString(UserMO.KEY, null)).hashCode();
            values.put(UserFileAdapter.getPatientId(), String.valueOf(userKey));
            SharedPref.updatePreferences(UserMO.KEY, null);
        }

        db.insert(UserFileAdapter.getUserFileTable(), null, values);

    }

    private void updateUserFilesTable(SQLiteDatabase db) {

        db.execSQL("ALTER TABLE " + UserFileAdapter.getUserFileTable() +
                " ADD " + UserFileAdapter.getEmailId() + " varchar(255)");
        db.execSQL("ALTER TABLE " + UserFileAdapter.getUserFileTable() +
                " ADD " + UserFileAdapter.getPhoneNumber() + " varchar(255)");
        db.execSQL("ALTER TABLE " + UserFileAdapter.getUserFileTable() +
                " ADD " + UserFileAdapter.getDeviceUid() + " varchar(255)");
        db.execSQL("ALTER TABLE " + UserFileAdapter.getUserFileTable() +
                " ADD " + UserFileAdapter.getPatientId() + " integer");
        db.execSQL("ALTER TABLE " + UserFileAdapter.getUserFileTable() +
                " ADD " + UserFileAdapter.getUSERNAME() + " varchar(255)");

    }

    private void insertAuthTokenInDB(SQLiteDatabase db) throws Exception {

        if (SharedPref.getAccessToken() == null)
            return;
        else {
            db.delete(UserFileAdapter.getUserFileTable(), null, null);
            ContentValues values = new ContentValues();
            values.put(UserFileAdapter.getAuthToken(), SharedPref.getAccessToken());
            db.insert(UserFileAdapter.getUserFileTable(), null, values);
            SharedPref.updatePreferences(VariableConstants.AUTH_TOKEN, null);
        }

    }

    private void createUserFilesTable(SQLiteDatabase db) {
        try {
            db.execSQL(UserFileAdapter.getUserFileTableQuery());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createAgreementDb(SQLiteDatabase db) {
        try {
            db.execSQL(AgreementFileDBAdapter.getAgreementFileQuery());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateToVersionTwelve(SQLiteDatabase db) throws IOException, ClassNotFoundException {
        updateMedicationUnitDataSet(db);
    }

    private void updateMedicationUnitDataSet(SQLiteDatabase db) {

        List<MedicineModelDb> doseUnits = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + EncounterMedicationDBAdapter.getMedicationPrescriptionTable();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int i = 0;
        if (cursor.moveToFirst()) {

            do {
                ArrayList<MedicineDoseFrequencyModel> dosesList = null;
                byte[] dosesArrayBytes = cursor.getBlob(cursor.getColumnIndex(EncounterMedicationDBAdapter.getDOSAGE()));
                if (dosesArrayBytes != null) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(dosesArrayBytes);
                    ObjectInputStream ois = null;
                    try {
                        ois = new ObjectInputStream(bais);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        dosesList = (ArrayList<MedicineDoseFrequencyModel>) ois.readObject();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                int id = cursor.getInt(cursor.getColumnIndex(EncounterMedicationDBAdapter.getID()));
                MedicineModelDb medicineModelDb = new MedicineModelDb(dosesList, id);
                doseUnits.add(i, medicineModelDb);
                i++;
            } while (cursor.moveToNext());

            for (int j = 0; j < doseUnits.size(); j++) {
                ContentValues contentValues = new ContentValues();
                int sizeOfArrayDose = doseUnits.get(j).getArrayMedicine().size();
                for (int k = 0; k < sizeOfArrayDose; k++) {
                    String doseUnit = doseUnits.get(j).getArrayMedicine().get(k).getDoseUnit();
                    String unitToInsert = Utils.removeSpace(doseUnit);
                    doseUnits.get(j).getArrayMedicine().get(k).setDoseUnit(unitToInsert);
                }

                if (doseUnits.get(j).getArrayMedicine() != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = null;
                    try {
                        out = new ObjectOutputStream(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        out.writeObject(doseUnits.get(j).getArrayMedicine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    contentValues.put(EncounterMedicationDBAdapter.getDOSAGE(), bytes.toByteArray());
                }
                db.update(EncounterMedicationDBAdapter.getMedicationPrescriptionTable(), contentValues, EncounterMedicationDBAdapter.getID() + " = ?",
                        new String[]{String.valueOf(doseUnits.get(j).getId())});
            }
        }
    }


    private void createVitalsConfigTable(SQLiteDatabase db) {
        try {
            db.execSQL(VitalsConfigDBAdapter.getCreateVitalsConfigTableQuery());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createPhysicianMoTable(SQLiteDatabase db) {
        try {
            db.execSQL(PhysicianDBAdapter.createPhysicianTableQuery());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateVaccinationId(SQLiteDatabase db) {
        //while inserting id was not set so updating it
        try {
            db.execSQL("UPDATE TABLE " + VaccinationProfileDBAdapter.getVaccinationProfileTable() + " SET " + VaccinationProfileDBAdapter.getID() + " 1");
        } catch (Exception e) {
            AppLog.d("UPDATE VACCINATION FAILED", e.toString());
        }

    }

    public void updateToPasskey(SQLiteDatabase db) {
        //do not add any statement to this funtion as it is called from other sides
        db.rawExecSQL("PRAGMA rekey = " + Utils.generateUniqueKey(context) + ";");
    }

    public void updateToVersionTen(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + EncounterMedicationDBAdapter.getMedicationPrescriptionTable() +
                " ADD " + EncounterMedicationDBAdapter.getSOURCENAME() + " varchar(255)");
        db.execSQL("ALTER TABLE " + DocumentReferenceDBAdapter.getDocumentReferenceTable() +
                " ADD " + DocumentReferenceDBAdapter.getSOURCENAME() + " varchar(255)");
    }

    private void updateDocumentReferenceModel(SQLiteDatabase db) {
        AppLog.d("Upating", "updating db");
        db.execSQL("ALTER TABLE " + DocumentReferenceDBAdapter.getDocumentReferenceTable() +
                " ADD " + DocumentReferenceDBAdapter.getISDIRTY() + " Integer");
        db.execSQL("ALTER TABLE " + DocumentReferenceDBAdapter.getDocumentReferenceTable() +
                " ADD " + DocumentReferenceDBAdapter.getNOOFATTACHMENTATTRIBUTES() + " Integer");
        db.execSQL("ALTER TABLE " + EncounterMedicationDBAdapter.getMedicationPrescriptionTable() +
                " ADD " + EncounterMedicationDBAdapter.getEXTENTION() + " BLOB");


    }

    private void updateToVersionEight(SQLiteDatabase db) {

        db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                " ADD " + UserDBAdapter.getMPIN() + " varchar(255)");

        db.execSQL("ALTER TABLE " + EncounterMedicationDBAdapter.getMedicationPrescriptionTable() +
                " ADD " + EncounterMedicationDBAdapter.getMimeType() + " Integer");
        db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() +
                " ADD " + ConfigDBAdapter.getPINLOCKTIME() + " INT ");
        updateRecordsTimetoLocalTimeZone(db);
        db.execSQL(VaccinationProfileDBAdapter.createVaccinationProfileTableQuery());
        db.execSQL(VaccinationDBAdapter.createVaccinationTableQuery());
        db.execSQL("ALTER TABLE " + NotificationCenterDBAdapter.getNotificationCenterTable() +
                " ADD " + NotificationCenterDBAdapter.getOrganizationId() + " varchar(255)");
        db.execSQL("ALTER TABLE " + NotificationCenterDBAdapter.getNotificationCenterTable() +
                " ADD " + NotificationCenterDBAdapter.getOrganizationName() + " varchar(255)");

    }


    private void updateToVersionFourteen(SQLiteDatabase db) {

        db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                " ADD " + UserDBAdapter.getDEPENDENTPATIENTS() + " varchar(255)");
    }

    public void updateToVersionEleven(SQLiteDatabase db) {

        try {
            db.delete(DiagnosticOrderDBAdapter.getPatientRecordTABLE(), "1", null);
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() +
                    " ADD " + DiagnosticOrderDBAdapter.getLABNAME() + " varchar(255)");
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() +
                    " ADD " + DiagnosticOrderDBAdapter.getAccessionNumber() + " varchar(255)");
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() +
                    " ADD " + DiagnosticOrderDBAdapter.getDIAGNOSTICREPORT() + " BLOB");
            db.execSQL(VitalsDBAdapter.createVitalsQuery());
        } catch (Exception e) {
            AppLog.d("FISIKE", "ALready upgraded to version 8");
        }

        if (BuildConfig.isVaccinationEnabled) {
            try {
                db.execSQL("UPDATE TABLE " + VaccinationProfileDBAdapter.getVaccinationProfileTable() + " SET " + VaccinationProfileDBAdapter.getID() + " 1");
            } catch (Exception e) {
                AppLog.d("UPDATE VACCINATION FAILED", e.toString());
            }
        }
    }


    private void updateRecordsTimetoLocalTimeZone(SQLiteDatabase db) {
        List<LocalTimeZone> timeZoneList = new ArrayList<LocalTimeZone>();
        String selectQuery = "SELECT  * FROM " + DiagnosticOrderDBAdapter.getPatientRecordTABLE();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LocalTimeZone localTimeZone = new LocalTimeZone();
                localTimeZone.setId(cursor.getString(cursor.getColumnIndex(DiagnosticOrderDBAdapter.ID)));
                String newTime = DateTimeUtil.calculateDate(cursor.getString(cursor.getColumnIndex(DiagnosticOrderDBAdapter.SCHEDULEDDATE)), DateTimeUtil.destinationDateFormatWithTime, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                localTimeZone.setTime(newTime);
                timeZoneList.add(localTimeZone);
            } while (cursor.moveToNext());
        }

        //update to local timezone
        for (int i = 0; i < timeZoneList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DiagnosticOrderDBAdapter.SCHEDULEDDATE, timeZoneList.get(i).getTime());
            db.update(DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE, contentValues, DiagnosticOrderDBAdapter.ID + " = ?",
                    new String[]{timeZoneList.get(i).getId()});
        }

    }


    private void updateToVersionSix(SQLiteDatabase db) {
        try {
            updateUserMoAddAlternateContactField(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateToVersionSeven(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                    " ADD " + UserDBAdapter.getMARITALSTATUS() + " varchar(255)");

            db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                    " ADD " + UserDBAdapter.getADDRESS() + " BLOB");

            db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() +
                    " ADD " + ConfigDBAdapter.getNOOFDAYSALTERNATECONTACT() + " Long ");
            db.execSQL("ALTER TABLE " + EncounterMedicationDBAdapter.getMedicationPrescriptionTable() +
                    " ADD " + EncounterMedicationDBAdapter.getPATIENTNAME() + " varchar(255)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateToVersionFive(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.getPatientRecordTABLE() +
                    " ADD " + DiagnosticOrderDBAdapter.getOrganisationId() + " varchar(255)");
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.execSQL(MedicationDBAdapter.dropTable());
        db.execSQL(PractitionerDBAdapter.dropTable());
        db.execSQL(MedicationPrescriptionDBAdapter.dropTable());
        db.execSQL(EncounterDBAdapter.dropTable());
        db.execSQL(EncounterMedicationDBAdapter.createMedicationPrescriptionTableQuery());
    }

    private void updateToVersionFour(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + chatContactDBAdapter.getChatContactMo_TABLE() +
                    " ADD " + chatContactDBAdapter.getPRESENCESTATUS() + " varchar(255)");
            db.execSQL("ALTER TABLE " + chatContactDBAdapter.getChatContactMo_TABLE() +
                    " ADD " + chatContactDBAdapter.getPRESENCETYPE() + " varchar(255)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // NOTE: this is being done for patient-app ONLY because the same migration for physician
            // app was already done in version 3
            if (BuildConfig.isPatientApp) {
                // NOTE THIS MUST BE THE LAST COMMAND COZ IT CAN FAIL IF WE ARE MIGRATING DIRECTLY FROM VERSION 3
                db.execSQL("ALTER TABLE " + ChatConversationDBAdapter.getChatConversationMO_TABLE() +
                        " ADD " + ChatConversationDBAdapter.getIsChatInitiated() + " INT DEFAULT 0");
                db.execSQL("UPDATE " + ChatConversationDBAdapter.getChatConversationMO_TABLE() +
                        " SET " + ChatConversationDBAdapter.getIsChatInitiated() + " = 1");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            db.execSQL("DROP TABLE AttachmentMO");
            db.execSQL(AttchmentDBAdapter.createAttachmentTableQuery());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateToVersionThree(SQLiteDatabase db) {
        try {
            migrateChatContactMO(db);
            migrateGroupTextMo(db);
            migrateUserMo(db);

            if (BuildConfig.isPhysicianApp) {
                db.execSQL("ALTER TABLE ChatConversationMO ADD isChatInitiated INT DEFAULT 0");
                db.execSQL("UPDATE ChatConversationMO SET isChatInitiated = 1");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateToVersionTwo(SQLiteDatabase db) {
        try {
            addColumns(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE DocumentReference ADD reason varchar(255)");
            db.execSQL("ALTER TABLE DocumentReference ADD mimeType INTEGER");

            db.execSQL("CREATE TABLE TempMedicationPrescription(ID INTEGER PRIMARY KEY,encounterID Integer, practitionarID Integer, unit varchar(255), fromDate varchar(255), toDate varchar(255),dosage BLOB, reminder BLOB, patientID INTEGER, medicianId INTEGER, practionerName varchar(255), "
                    + "medicineName varchar(255), isAlarmSet Integer, diseaseList BLOB)");

            new MigradeDbQuery().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            db.execSQL("ALTER TABLE NotificationCenter ADD refId varchar(255)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUserMoAddAlternateContactField(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + UserDBAdapter.getUserMO_TABLE() +
                " ADD " + UserDBAdapter.getALTERNATECONTACT() + " varchar(255)");
    }

    public class MigradeDbQuery extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            migrateDBMedication();
            return null;
        }
    }


    private void migrateDBMedication() {
        try {
            MedicationPrescriptionDBAdapter dbAdapter = MedicationPrescriptionDBAdapter.getInstance(context);
            ArrayList<MedicationPrescriptionModel> arrayMedicationPrescriptionModel = MedicationPrescriptionDBAdapter.getInstance(context).fetchMedicationPrescription();
            for (int i = 0; i < arrayMedicationPrescriptionModel.size(); i++) {
                MedicationPrescriptionModel medicationPrescriptionModel = arrayMedicationPrescriptionModel.get(i);
                ArrayList<DiseaseMO> diseaseMOArrayList = new ArrayList<DiseaseMO>();
                diseaseMOArrayList.add(new DiseaseMO(medicationPrescriptionModel.getMedicineName(), medicationPrescriptionModel.getDiseaseCode(), medicationPrescriptionModel.getDiseaseCodingSystem()));
                medicationPrescriptionModel.setDiseaseMoList(diseaseMOArrayList);
                dbAdapter.insertMedicationPrescription(medicationPrescriptionModel);
            }
            dbAdapter.renameDeleteMedicationPrescriptionTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateConfigMoTable(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + ConfigDBAdapter.getConfigMO_TABLE() +
                    " ADD " + ConfigDBAdapter.AUTOLOGOUTTIME + " INT");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePatientMoTable(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + PatientDBAdapter.PATIENT_MO_TABLE +
                    " ADD " + PatientDBAdapter.PHOTO + " BLOB");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePatientRecords(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE +
                    " ADD " + DiagnosticOrderDBAdapter.PERFORMINGlOCATIONNAME + " varchar(255)");

            db.execSQL("ALTER TABLE " + DiagnosticOrderDBAdapter.PATIENT_RECORD_TABLE +
                    " ADD " + DiagnosticOrderDBAdapter.PATIENT_MOGO_ID + " varchar(255)");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateCareTaskDB(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.CATEGORY + " varchar(255)");
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.PRIORITY + " LONG ");
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.TASKSUBTYPE + " varchar(255)");
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.CREATETIME + " varchar(255)");
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.ASSIGNEE + " varchar(255)");
            db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.CARE_TASK_TABLE_NAME +
                    " ADD " + CareTaskDatabaseAdapter.VITALNAME + " varchar(255)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void clearVitalsConfigRequest(SQLiteDatabase db) {
        SharedPref.setVitalConfigExecuted(false);
        try {
            db.delete(VitalsConfigDBAdapter.getVitalsConfigTable(), null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteObservationTable(SQLiteDatabase db) {
        try {
            db.delete("Observation", null, null);
        } catch (Exception e) {
        }
    }

}