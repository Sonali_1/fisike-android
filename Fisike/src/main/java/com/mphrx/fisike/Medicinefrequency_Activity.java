package com.mphrx.fisike;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.beans.DoseBean;
import com.mphrx.fisike.constant.MedicationConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Text;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * Created by laxmansingh on 5/6/2016.
 */
public class Medicinefrequency_Activity extends BaseActivity implements View.OnClickListener, NumberPicker.OnValueChangeListener {
    private IconTextView btn_close;
    private DoseBean doseBean;
    private Context mContext;
    private boolean is_sos;
    private int clicked_position;
    private LinearLayout rlSetTime;
    private NumberPicker npDosageCount;
    private NumberPicker npDosageUnit;
    private NumberPicker npTimehh;
    private NumberPicker npTimemm;
    private NumberPicker npTimeba;
    private RadioGroup rgDosageDuration;
    private Button btSubmit;
    private Button btnCancel;
    private EditText inputText;
    private RadioButton rbAfter, rbBefore;
    private boolean boolAfterFoodSelected = false;
    //changed units data in FIS-7789 -aastha
    final String[] UNITS = MedicationUnitsEnum.units();
    private boolean boolBeforeFoodSelected = false;
    private String no_of_tablets;
    private String unit;
    private String hour;
    private int min;
    private String AP, isAfterOrBefore;
    private String orgTablets;
    private boolean isValueChanged = false;
    private GestureDetector getstureDetector;
    private EditText numberPickerDoseQuantityEditText;
    private boolean isSelectedFromKeyBoard = false;
    private String amLocale;
    private String pmLocale;
    private String[] am_pm_array_locale;

   /*
      try {
        String now = "12:00 PM";
        System.out.println("time in 12 hour format : " + now);
        SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat outFormat = new SimpleDateFormat("HH");
        String time24 = outFormat.format(inFormat.parse(now));
        System.out.println("time in 24 hour format : " + time24);
    } catch (Exception e) {
        System.out.println("Exception : " + e.getMessage());
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.medicinefrequency_dialog, frameLayout);

        Bundle bundle = getIntent().getExtras();
        doseBean = bundle.getParcelable("dosebean");
        clicked_position = bundle.getInt("clicked_position");
        is_sos = bundle.getBoolean("is_sos");
        mContext = this;
        findView();
        bindView();
        initView();

    }

    private void findView() {
        btn_close = (IconTextView) findViewById(R.id.btn_close);
        rlSetTime = (LinearLayout) findViewById(R.id.rl_set_time);
        npDosageCount = (NumberPicker) findViewById(R.id.npDosageCount);
        npDosageUnit = (NumberPicker) findViewById(R.id.npDosageUnit);
        rgDosageDuration = (RadioGroup) findViewById(R.id.rgDosageDuration);
        btSubmit = (Button) findViewById(R.id.bt_number_picker);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        rbAfter = (RadioButton) findViewById(R.id.rbAfter);
        rbBefore = (RadioButton) findViewById(R.id.rbBefore);
        Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.opensans_ttf_semibold));
        rbAfter.setTypeface(font);
        rbBefore.setTypeface(font);
    }

    private void bindView() {
        btn_close.setOnClickListener(this);
        npDosageCount.setOnValueChangedListener(this);
        npDosageUnit.setOnValueChangedListener(this);
        btSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    private void initView() {
        try {
            no_of_tablets = doseBean.getAmount();
            orgTablets = doseBean.getAmount();
            float doseAmout = Float.parseFloat(no_of_tablets);
            if (((int) doseAmout) == doseAmout) {
                no_of_tablets = "" + (int) doseAmout;
                orgTablets = "" + (int) doseAmout;
            }

            unit = doseBean.getUnits();
            hour = doseBean.getTime();
            min = Integer.parseInt(doseBean.getMin().trim());
            AP = DateTimeUtil.convertSourceDestinationDateLocale(hour + ":" + min, "HH:mm", "a");
            isAfterOrBefore = doseBean.getIsAfterOrBefore();

            setInputTextForNumberFormatter();

            amLocale = Utils.getAM(Utils.getUserSelectedLanguage(mContext));
            pmLocale = Utils.getPM(Utils.getUserSelectedLanguage(mContext));
            am_pm_array_locale = Utils.getAMPM(Utils.getUserSelectedLanguage(mContext));


            npDosageCount.setMinValue(0);
            npDosageCount.setMaxValue(39999);
            no_of_tablets = String.valueOf(Float.parseFloat(no_of_tablets) * 4);
            npDosageCount.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    if (isSelectedFromKeyBoard || !isValidDoseQuantity(orgTablets)) {
                        return orgTablets;
                    }

                    if (value == 0 && ((Float.valueOf(orgTablets)) % 0.25 != 0)) {  //Handling the case of showing manually entered medicine quantity <.25 (Aastha for FIS-9323))
                        return orgTablets;
                    } else if (!isValueChanged && orgTablets != null && ((value + 1) == Math.floor(Float.valueOf(no_of_tablets)))) {
                        return orgTablets;
                    }
                    float valueres = (value + 1) * 0.25f;
                    if (((int) valueres) == valueres)
                        return "" + (int) valueres;
                    else {
                        return "" + valueres;
                    }
                }
            });
            double noOfTablet = Double.parseDouble(no_of_tablets);

            int value = (int) (noOfTablet); //removed the previous calculations as they resulted in floor[no_of_tablets]

            if (value > 0)
                npDosageCount.setValue(value - 1);
            else
                npDosageCount.setValue(0);

            npDosageUnit.setMinValue(0);
            npDosageUnit.setMaxValue(47);
            // npDosageUnit.setWrapSelectorWheel(true);
            npDosageUnit.setDisplayedValues(UNITS);
            int index = indexOf(Arrays.asList(UNITS), unit);
            if (index == -1) {
                npDosageUnit.setValue(0);
            } else {
                npDosageUnit.setValue(index);
            }

            npDosageUnit.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int i) {
                    return UNITS[i];
                }
            });


            if (isAfterOrBefore != null && isAfterOrBefore.equalsIgnoreCase(MedicationConstants.AFTER_FOOD)) {
                boolAfterFoodSelected = true;
                boolBeforeFoodSelected = false;
                rbAfter.setChecked(true);
                rbAfter.setTextColor(getResources().getColor(R.color.action_bar_bg));
                rbBefore.setTextColor(getResources().getColor(R.color.overflow_color));
            } else if (isAfterOrBefore != null && isAfterOrBefore.equalsIgnoreCase(MedicationConstants.BEFORE_FOOD)) {
                boolBeforeFoodSelected = true;
                boolAfterFoodSelected = false;
                rbBefore.setChecked(true);
                rbBefore.setTextColor(getResources().getColor(R.color.action_bar_bg));
                rbAfter.setTextColor(getResources().getColor(R.color.overflow_color));
            } else {
                boolBeforeFoodSelected = false;
                boolAfterFoodSelected = false;
                rbBefore.setTextColor(getResources().getColor(R.color.overflow_color));
                rbAfter.setTextColor(getResources().getColor(R.color.overflow_color));
                rgDosageDuration.clearCheck();
            }

            if (!is_sos) {
                rlSetTime.setVisibility(View.VISIBLE);
                npTimehh = (NumberPicker) findViewById(R.id.npTimehh);
                npTimehh.setMinValue(DateFormat.is24HourFormat(this) ? 0 : 1);
                npTimehh.setMaxValue(DateFormat.is24HourFormat(this) ? 23 : 12);
                int hourInt = Integer.parseInt(doseBean.getTime().toString());
                hourInt = DateFormat.is24HourFormat(mContext) ? hourInt : hourInt > 12 ? hourInt - 12 : hourInt;
                npTimehh.setValue(hourInt);
                npTimehh.setWrapSelectorWheel(true);
                npTimehh.setOnValueChangedListener(this);
                npTimemm = (NumberPicker) findViewById(R.id.npTimemm);
                npTimemm.setMinValue(Integer.parseInt("0"));
                npTimemm.setMaxValue(Integer.parseInt("59"));
                npTimemm.setWrapSelectorWheel(true);
                npTimemm.setValue(min);
               /* npTimemm.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        if (value < 10) {
                            return "0" + value;
                        }
                        return value + "";
                    }
                });*/
                npTimemm.setOnValueChangedListener(this);
                npTimeba = (NumberPicker) findViewById(R.id.npTimeba);
                if (!DateFormat.is24HourFormat(this)) {
                    npTimeba.setVisibility(View.VISIBLE);
                    npTimeba.setMinValue(0);
                    npTimeba.setMaxValue(1);
                    npTimeba.setWrapSelectorWheel(true);
                    npTimeba.setDisplayedValues(am_pm_array_locale);
                    npTimeba.setValue(Arrays.asList(am_pm_array_locale).indexOf(AP));
                } else {
                    npTimeba.setVisibility(View.GONE);
                }

                npTimeba.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int i) {
                        return am_pm_array_locale[Arrays.asList(am_pm_array_locale).indexOf(AP)];
                    }
                });
                npTimeba.setOnValueChangedListener(this);
            } else {
                npTimeba = (NumberPicker) findViewById(R.id.npTimeba);
                rlSetTime.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            System.out.println("Exception e :::::" + e.toString());
        }

        if (!is_sos) {
            npTimehh.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            npTimemm.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            npTimeba.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        }
        npDosageUnit.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        setNumberPickerTextColor(npDosageCount, ContextCompat.getColor(this, R.color.medium_green), mContext);
        setNumberPickerTextColor(npDosageUnit, ContextCompat.getColor(this, R.color.medium_green), mContext);

        if (!is_sos) {
            setNumberPickerTextColor(npTimehh, ContextCompat.getColor(this, R.color.medium_green), mContext);
            setNumberPickerTextColor(npTimemm, ContextCompat.getColor(this, R.color.medium_green), mContext);
            setNumberPickerTextColor(npTimeba, ContextCompat.getColor(this, R.color.medium_green), mContext);
        }

    }

    private int indexOf(List<String> strings, String unit) {
        if (unit.equalsIgnoreCase(getResources().getString(R.string.conditional_spray_puffs)))
            unit = getResources().getString(R.string.unit_spray_puff);
        else if (unit.equalsIgnoreCase(getResources().getString(R.string.conditional_inhaler_puffs)))
            unit = getResources().getString(R.string.unit_inhaler_puff);
        else if (unit.equalsIgnoreCase(getResources().getString(R.string.conditional_drops_count)))
            unit = getResources().getString(R.string.unit_drop_count);
        else if (unit.equalsIgnoreCase(getResources().getString(R.string.conditional_drops_ml)))
            unit = getResources().getString(R.string.unit_drop_ml);
        for (int i = 0; i < strings.size(); i++) {
            if (unit.contains(strings.get(i))) {
                return i;
            }
        }
        return 0;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                hideKeyboard();
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.bottom_down);
                break;

            case R.id.btnCancel:
                hideKeyboard();
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.bottom_down);
                break;


            case R.id.bt_number_picker:
                if (!isValidDoseQuantity(inputText.getText().toString())) {
                    Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                    break;
                }
                try {
                    if ((Float.valueOf(inputText.getText().toString())) == 0) {
                        orgTablets = inputText.getText().toString();
                        Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                        break;
                    }
                } catch (Exception e) {
                    orgTablets = inputText.getText().toString();
                    Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                    break;
                }
                hideKeyboard();

                String newHour = "";
                String newMin = "";
                if (!is_sos && npTimeba.getVisibility() == View.VISIBLE && AP != null && (AP.equalsIgnoreCase(pmLocale) || AP.equalsIgnoreCase(amLocale))) {

                    newHour = String.valueOf(npTimehh.getValue());
                    newMin = String.valueOf(npTimemm.getValue());

                    String amOrpm = (am_pm_array_locale[npTimeba.getValue()]).equalsIgnoreCase(pmLocale) ? TextConstants.PM : TextConstants.AM;
                    newHour = DateTimeUtil.getHoursIn24HrFormat(npTimehh.getValue() + ":" + npTimemm.getValue() + " " + amOrpm);
                }

                if (is_sos) {
                    newHour = hour;
                    newMin = "" + min;
                }

                DoseBean doseBean = new DoseBean(inputText.getText().toString(), unit, newHour.trim(), newMin.trim() + "", isAfterOrBefore, is_sos);
                Intent intent = new Intent();
                intent.putExtra("dosebean", doseBean);
                intent.putExtra("clicked_position", clicked_position);
                setResult(TextConstants.MEDICINE_FREQUNCY_INTET, intent);
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.bottom_down);
                break;

            default:
                break;
        }
    }

    private boolean isValidDoseQuantity(String doseQuantity) {
        if (doseQuantity.equals("") || doseQuantity.equals(".") || doseQuantity.equals(".0") || doseQuantity.equals("0.0")
                || doseQuantity.equals("0") || doseQuantity.equals("0.0") || doseQuantity.equals("0.00") || doseQuantity.equals(".00")
                || doseQuantity.equals("0.") || doseQuantity.equals("00"))
            return false;
        else
            return true;
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        switch (picker.getId()) {
            case R.id.npDosageCount:
//                    no_of_tablets = String.valueOf((newVal) * 0.25);
                isValueChanged = true;
                isSelectedFromKeyBoard = false;
                if (oldVal == newVal)
                    isValueChanged = false;

                no_of_tablets = String.valueOf(newVal);
                break;
            case R.id.npDosageUnit:
                unit = UNITS[newVal];
                break;
            case R.id.npTimehh:
                hour = newVal + "";
                break;
            case R.id.npTimemm:
                min = newVal;
                break;
            case R.id.npTimeba:
                AP = am_pm_array_locale[newVal];
                break;
        }

    }

    /*@Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getCheckedRadioButtonId()) {
            case R.id.rbAfter:
                isAfterOrBefore = TextConstants.AFTER_FOOD;
                break;
            case R.id.rbBefore:
                isAfterOrBefore = TextConstants.BEFORE_FOOD;
                break;
        }

    }*/


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (inputText.getText().equals(".") || inputText.getText().equals(".0"))
            Toast.makeText(Medicinefrequency_Activity.this, "Please enter valid value ", Toast.LENGTH_SHORT).show();
        overridePendingTransition(R.anim.no_change, R.anim.bottom_down);
    }


    private String getValue(int val) {
        if (val == 0)
            return getResources().getString(R.string.number_00);
        if (val == 1)
            return getResources().getString(R.string.number_01);
        if (val == 2)
            return getResources().getString(R.string.number_02);
        if (val == 3)
            return getResources().getString(R.string.number_03);
        if (val == 4)
            return getResources().getString(R.string.number_04);
        if (val == 5)
            return getResources().getString(R.string.number_05);
        if (val == 6)
            return getResources().getString(R.string.number_06);
        if (val == 7)
            return getResources().getString(R.string.number_07);
        if (val == 8)
            return getResources().getString(R.string.number_08);
        if (val == 9)
            return getResources().getString(R.string.number_09);
        return val + "";
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.rbAfter:
                if (checked && boolAfterFoodSelected) {
                    // clear all
                    rgDosageDuration.clearCheck();
                    isAfterOrBefore = "";
                    boolAfterFoodSelected = false;
                    boolBeforeFoodSelected = false;
                    rbAfter.setTextColor(getResources().getColor(R.color.overflow_color));
                    rbBefore.setTextColor(getResources().getColor(R.color.overflow_color));
                } else if (checked) {
                    isAfterOrBefore = MedicationConstants.AFTER_FOOD;
                    boolAfterFoodSelected = true;
                    boolBeforeFoodSelected = false;
                    rbAfter.setTextColor(getResources().getColor(R.color.action_bar_bg));
                    rbBefore.setTextColor(getResources().getColor(R.color.overflow_color));
                }
                break;
            case R.id.rbBefore:
                if (checked && boolBeforeFoodSelected) {
                    // clear all
                    rgDosageDuration.clearCheck();
                    isAfterOrBefore = "";
                    boolAfterFoodSelected = false;
                    boolBeforeFoodSelected = false;
                    rbAfter.setTextColor(getResources().getColor(R.color.overflow_color));
                    rbBefore.setTextColor(getResources().getColor(R.color.overflow_color));
                } else if (checked) {
                    isAfterOrBefore = MedicationConstants.BEFORE_FOOD;
                    boolAfterFoodSelected = false;
                    boolBeforeFoodSelected = true;
                    rbBefore.setTextColor(getResources().getColor(R.color.action_bar_bg));
                    rbAfter.setTextColor(getResources().getColor(R.color.overflow_color));
                }

                break;
        }
    }


    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color, Context mContext) {
        if (numberPicker == null) {
            return false;
        }
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField(mContext.getResources().getString(R.string.txt_mSelectorWheelPaint));
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                } catch (IllegalAccessException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        return false;
    }

    private void setInputTextForNumberFormatter() {
        Field f = null;
        try {
            f = NumberPicker.class.getDeclaredField("mInputText");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        f.setAccessible(true);

        try {
            inputText = (EditText) f.get(npDosageCount);
            inputText.setRawInputType(Configuration.KEYBOARD_12KEY);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            inputText = null;
        }

        if (inputText != null) {
            inputText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            inputText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        if (!isValidDoseQuantity(inputText.getText().toString())) {
                            orgTablets = textView.getText().toString();
                            Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                            return true;
                        } else {
                            try {
                                if ((Float.valueOf(textView.getText().toString())) == 0) {
                                    orgTablets = textView.getText().toString();
                                    Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                                    return true;
                                }
                            } catch (Exception e) {
                                orgTablets = textView.getText().toString();
                                Toast.makeText(Medicinefrequency_Activity.this, getResources().getString(R.string.enter_valid_dose), Toast.LENGTH_SHORT).show();
                                return true;
                            }
                        }
                        isSelectedFromKeyBoard = true;
                        String orig = textView.getText().toString();
                        isValueChanged = false;
                        orgTablets = orig;
                        no_of_tablets = String.valueOf(Float.parseFloat(orig) * 4);
                        double noOfTablet = Double.parseDouble(no_of_tablets);

                        int value = (int) (noOfTablet); //removed the previous calculations as they resulted in floor[no_of_tablets]

                        if (value > 0)
                            npDosageCount.setValue(value - 1);
                        else
                            npDosageCount.setValue(0);

                        hideKeyboard();

                        // textView.setText(orig);
                        return true;
                    }

                    return false;
                }

            });
            inputText.setFilters(new InputFilter[]{new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                    if (source.length() == 0) {
                        return source;
                    }

                    String text = dest.toString();

                    /**
                     * 0 not allowed in starting
                     */
                    if (text.length() == 0 && source.charAt(0) == '0' && source.length() == 1) {
                        return "";
                    }

                    boolean isDot = source.charAt(0) == '.';
                    /**
                     *  return if there is already a . (dot)
                     */
                    if ((text.contains(".") && isDot) || (!Character.isDigit(source.charAt(0)) && !isDot)) {
                        return "";
                    }

                    if (text.length() > 1 && Double.parseDouble(text + source) > 10000.0) {

                        return "";

                    }

                    if (text.contains(".") && source.length() != 0) {

                        String[] x = text.split("\\.");

                        if (x.length == 1) {
                            char ch = source.charAt(0);
                            if (ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7' || ch == '8' || ch == '9') {
                                return source;
                            } else {
                                return "";
                            }
                        } else if (x.length == 2) {
                            if (x[1].length() >= 2) {
                                return "";
                            }

                        }
                    }

                    return source;

                }
            }
            });
        }
    }
}
