package com.mphrx.fisike;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Text;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.io.File;

public class PdfPreview extends BaseActivity {

    private WebView webView;
    private String downloadFile;
    private int mimeType;
    private boolean isReport;
    private static final int PDF_DOWNLOAD_RESULT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_preview);

        Toolbar profileToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(profileToolBar);
        profileToolBar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        mimeType = getIntent().getExtras().getInt(VariableConstants.MIME_TYPE);

        webView = (WebView) findViewById(R.id.wvDocumentViewer);

        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            getSupportActionBar().setTitle(getResources().getString(R.string.Pdf_Viewer));
            setPdfView();
        }

        isReport = getIntent().getBooleanExtra(TextConstants.IS_REPORT, false);

    }

    /**
     * Show the pdf file
     */
    private void setPdfView() {
        downloadFile = getIntent().getExtras().getString(VariableConstants.PDF_FILE_PATH);


        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        //The default value is true for API level android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1 and below,
        //and false for API level android.os.Build.VERSION_CODES.JELLY_BEAN and above.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
            settings.setAllowUniversalAccessFromFileURLs(true);

        settings.setBuiltInZoomControls(true);
        setWebClientForLoading();
        webView.loadUrl("file:///android_asset/pdfviewer/index.html?file=file://" + downloadFile);
    }

    /**
     * Web view client handle the web events
     */
    private void setWebClientForLoading() {
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            public void onPageFinished(WebView view, String url) {
                webView.clearCache(true);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(PdfPreview.this, getResources().getString(R.string.Oh_no) + description, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Handle the view click
     *
     * @param view
     */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                Intent putExtra = getIntent().putExtra(VariableConstants.PDF_FILE_STATUS, true).putExtra(VariableConstants.PDF_FILE_PATH, downloadFile);
                setResult(RESULT_OK, putExtra);
                this.finish();
                break;
            case R.id.btnCancel:
                putExtra = getIntent().putExtra(VariableConstants.PDF_FILE_STATUS, false);
                setResult(RESULT_OK, putExtra);
                this.finish();
                break;
        }
    }

    /**
     * Handle the navigation events
     *
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.download) {
            if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), PDF_DOWNLOAD_RESULT))
                showDisclaimer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDisclaimer() {
        DialogUtils.showAlertDialog(this, getString(R.string.disclaimer_title), getString(R.string.disclaimer_msg_pdf_download), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    try {
                        showProgressDialog();
                        File from = new File(downloadFile);
                        File to = new File(downloadFile.replace("./temp", "/pdf"));
                        from.renameTo(to);
                        dismissProgressDialog();
                        Toast.makeText(PdfPreview.this, getString(R.string.success_pdf_download), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        dismissProgressDialog();
                        Toast.makeText(PdfPreview.this, getString(R.string.fail_pdf_download), Toast.LENGTH_SHORT).show();
                    }
                } else
                    dialog.dismiss();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (isReport)
            getMenuInflater().inflate(R.menu.menu_download_pdf, menu);
        return true;
    }

    @Override
    public void onPermissionNotGranted() {
        super.onPermissionNotGranted();
    }

    @Override
    public void onPermissionGranted(String permission) {
        showDisclaimer();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case PDF_DOWNLOAD_RESULT:
                    onPermissionGranted(permissions[0]);
                    break;
            }
        } else
            onPermissionNotGranted();
    }
}
