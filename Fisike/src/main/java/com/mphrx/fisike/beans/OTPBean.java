package com.mphrx.fisike.beans;

public class OTPBean {

	private String otp_number;
	private String otp_generated_time;
	private boolean isOTPVerified;
	private String mobile_number;
	private String countryCode;	


	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getOtp_Code() {
		return otp_number;
	}

	public void setOtp_Code(String otp_number) {
		this.otp_number = otp_number;
	}

	public String getOtp_generated_time() {
		return otp_generated_time;
	}

	public void setOtp_generated_time(String otp_generated_time) {
		this.otp_generated_time = otp_generated_time;
	}

	public boolean isOTPVerified() {
		return isOTPVerified;
	}

	public void setOTPVerified(boolean isOTPVerified) {
		this.isOTPVerified = isOTPVerified;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

}
