package com.mphrx.fisike.beans;

public class HashPresence {

    private String presenceStatus;
    private String presenceType;

    public HashPresence(String presenceStatus, String presenceType) {
        this.presenceStatus = presenceStatus;
        this.presenceType = presenceType;
    }

    public String getPresenceStatus() {
        return presenceStatus;
    }

    public void setPresenceStatus(String presenceStatus) {
        this.presenceStatus = presenceStatus;
    }

    public String getPresenceType() {
        return presenceType;
    }

    public void setPresenceType(String presenceType) {
        this.presenceType = presenceType;
    }

}
