package com.mphrx.fisike.beans;

import com.mphrx.fisike.gson.request.Address;

import java.util.ArrayList;

/**
 * Created by Aastha on 7/21/2016.
 */
public class PhysicianDetails {
    private String experience ;
    private String role;
    private String speciality;
    private ArrayList<Address> addresses;

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }
}
