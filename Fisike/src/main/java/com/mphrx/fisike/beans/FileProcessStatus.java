package com.mphrx.fisike.beans;

public class FileProcessStatus {
    private boolean isFileExecutingInProcess;
    private int percentDownload;

    public boolean isFileExecutingInProcess() {
        return isFileExecutingInProcess;
    }

    public void setFileExecutingInProcess(boolean isFileExecutingInProcess) {
        this.isFileExecutingInProcess = isFileExecutingInProcess;
    }

    public int getPercentDownload() {
        return percentDownload;
    }

    public void setPercentDownload(int percentDownload) {
        this.percentDownload = percentDownload;
    }
}
