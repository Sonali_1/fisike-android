package com.mphrx.fisike.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;

/**
 * Created by Aastha on 21/01/2016.
 */
public class DoseBean implements Parcelable {
    private boolean isSOS;
    private String units;
    private String amount;
    private String time;
    private String AM_PM;
    private String isAfterOrBefore;

    private String min;


    public String getAM_PM() {
        return AM_PM;
    }

    public void setAM_PM(String AM_PM) {
        this.AM_PM = AM_PM;
    }


    public String getIsAfterOrBefore() {
        return isAfterOrBefore;
    }

    public void setIsAfterOrBefore(String isAfterOrBefore) {
        this.isAfterOrBefore = isAfterOrBefore;
    }

    public DoseBean(String amount, String units, String hour, String min, String isAfterOrBefore, boolean isSOS) {
        this.amount = amount;
        this.units = units;
        this.time = hour;
        this.min = min;
        this.isAfterOrBefore = isAfterOrBefore;
        this.isSOS = isSOS;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getTime() {
        return time;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setTime(String time) {
        this.time = time;
    }








/*
*
*   .................Parceable code here..........
*
* */

    public static final Creator<DoseBean> CREATOR = new Creator<DoseBean>() {
        @Override
        public DoseBean createFromParcel(Parcel in) {
            return new DoseBean(in);
        }

        @Override
        public DoseBean[] newArray(int size) {
            return new DoseBean[size];
        }
    };

    protected DoseBean(Parcel in) {
        units = in.readString();
        amount = in.readString();
        time = in.readString();
        AM_PM = in.readString();
        isAfterOrBefore = in.readString();
        min = in.readString();
        isSOS = in.readInt() == 0 ? false : true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(units);
        dest.writeString(amount);
        dest.writeString(time);
        dest.writeString(AM_PM);
        dest.writeString(isAfterOrBefore);
        dest.writeString(min);
        dest.writeInt(isSOS ? 1 : 0);
    }

    public boolean isSOS() {
        return isSOS;
    }
}
