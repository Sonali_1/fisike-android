package com.mphrx.fisike.beans;

import java.io.Serializable;

import org.jivesoftware.smack.packet.Message;

public class MsgObj implements Serializable {
    private static final long serialVersionUID = -1381076236353103582L;

    private Message message;
    private long timeStamp;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

}
