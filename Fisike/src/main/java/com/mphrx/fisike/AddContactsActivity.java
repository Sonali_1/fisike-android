package com.mphrx.fisike;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.adapter.AddContactListAdapter;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.connection.ConnectionStatus;
import com.mphrx.fisike.constant.PiwikConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.ContactListArray;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ErrorMessage;
import com.mphrx.fisike_physician.ChatUserOptionListDialog;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.TextPattern;

public class AddContactsActivity extends BaseActivity implements OnScrollListener, OnItemClickListener {

    public boolean isLoadingContacts = false;
    protected boolean isScrollDownError = false;
    private String jsonString = "", searchString = "";
    private int pageNum = 0;
    private View loadingView;
    private ListView listContacts;
    private CustomFontTextView tvDoneLoading;
    private LinearLayout loadingViewContainer;
    private boolean loadMore, loadingMore;
    private ArrayList<ChatContactMO> contacts;
    private AddContactListAdapter adapter;
    private CustomFontTextView txtNoMoreHistory;
    private AsyncTask<Void, Void, Void> loadingContacts;
    private UserMO userMO;
    private boolean isLoadMoreConnectionError;

    private boolean hastheSearchQueryBeenCalled = false;

    private SearchView searchView;

    private Toolbar addContactToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout frameLayout= (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_add_contacts, frameLayout);
        //setContentView(R.layout.activity_add_contacts);

        addContactToolbar = (Toolbar) findViewById(R.id.toolbar_add_contact);
        setSupportActionBar(addContactToolbar);
        addContactToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        getSupportActionBar().setTitle(getResources().getString(R.string.all_contacts));

        contacts = new ArrayList<ChatContactMO>();

        listContacts = (ListView) findViewById(R.id.listContacts);
        txtNoMoreHistory = (CustomFontTextView) findViewById(R.id.txtNoMoreHistory);

        LayoutInflater inflater = LayoutInflater.from(this);
        loadingView = inflater.inflate(R.layout.loading_view, null);
        tvDoneLoading = (CustomFontTextView) loadingView.findViewById(R.id.tv_done_loading);
        loadingViewContainer = (LinearLayout) loadingView.findViewById(R.id.ll_loading_view_container);
        listContacts.addFooterView(loadingView);

        adapter = new AddContactListAdapter(AddContactsActivity.this, contacts);
        listContacts.setAdapter(adapter);

        listContacts.setOnScrollListener(this);
        listContacts.setOnItemClickListener(this);

        if (!isLoadingContacts) {
            loadingContacts = new LoadContactsAsynTask(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    protected void onResume() {
        userMO = SettingManager.getInstance().getUserMO();
        super.onResume();
    }

    @Override
    protected void onPause() {
        // Utils.hideKeyboard(this, searchBox);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (null == userMO) {
            userMO = SettingManager.getInstance().getUserMO();
        }
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            // searchView = (SearchView) searchItem.getActionView();
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (hastheSearchQueryBeenCalled && newText.length() == 0) {
                    hastheSearchQueryBeenCalled = false;
                    searchString = "";
                    search();
                    return true;
                }

                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                hastheSearchQueryBeenCalled = true;
                searchString = query;
                search();
                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
        LinkedHashMap<String, String> inviteUserPriGroupList = userMO.getInviteUserPriGroupList();
        return true;
    }

    private void loadContacts() {
        try {
            if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
                showErrorInvalidUserName();
                if (contacts.size() == 0) {
                    txtNoMoreHistory.setVisibility(View.VISIBLE);
                    loadingView.setVisibility(View.GONE);
                } else {
                    txtNoMoreHistory.setVisibility(View.GONE);
                }
                return;
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ContactListArray.class);
            if (jsonToObjectMapper instanceof ContactListArray) {
                contacts.addAll(((ContactListArray) jsonToObjectMapper).getResultList());
                if (contacts.size() == 0)
                    loadMore = false;
//          ArrayList<ChatContactMO> physicianList = new ArrayList<ChatContactMO>();
//          physicianList = ((ContactListArray) jsonToObjectMapper).getResultList();
//          if (!userMO.getUserType().getName().equalsIgnoreCase(VariableConstants.PATIENT)) {
//             contacts.addAll(((ContactListArray) jsonToObjectMapper).getResultList());
//          } else if (physicianList != null && physicianList.size() > 0) {
//             for (ChatContactMO chatContactMo : physicianList) {
//                if (!chatContactMo.getUserType().getName().equals(TextConstants.PATIENT)) {
//                   contacts.add(chatContactMo);
//                }
//             }
//          }
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    if (contacts.size() == 0) {
                        if (loadMore) {
                            txtNoMoreHistory.setVisibility(View.GONE);
                        } else {
                            txtNoMoreHistory.setVisibility(View.VISIBLE);
                            loadingView.setVisibility(View.GONE);
                        }
                    } else {
                        txtNoMoreHistory.setVisibility(View.GONE);
                    }

                    adapter.notifyDataSetChanged();
                    loadingMore = false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

            showError(false);
        }
    }

    private void showErrorInvalidUserName() {
        ErrorMessage localErrorMessage = new ErrorMessage(AddContactsActivity.this, R.id.layoutParent);
        localErrorMessage.showMessage(MyApplication.getAppContext().getResources().getString(R.string.oops_invalid_user_credentials), false);
    }

    public void showError(final boolean isOnclick) {
        AddContactsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                loadingMore = false;
                String txtMessage = TextConstants.UNEXPECTED_ERROR;
                if (!Utils.isNetworkAvailable(AddContactsActivity.this)) {
                    txtMessage =MyApplication.getAppContext().getResources().getString(R.string.connection_not_available);
                    if (isOnclick) {
                        ErrorMessage localErrorMessage = new ErrorMessage(AddContactsActivity.this, R.id.layoutParent);
                        localErrorMessage.showMessage(txtMessage);
                        isScrollDownError = true;
                    }
                }
            }
        });
    }

    public void handleError(boolean isOnclick) {
        loadingMore = false;
        ErrorMessage localErrorMessage = new ErrorMessage(AddContactsActivity.this, R.id.layoutParent);
        localErrorMessage.showMessage(getResources().getString(R.string.unable_to_contact_server));
        isScrollDownError = true;
    }

    public void search() {
        // searchString = searchBox.getText().toString().trim();
        pageNum = 0;
        contacts.clear();
        txtNoMoreHistory.setVisibility(View.GONE);
        loadingContacts.cancel(true);
        isLoadingContacts = false;
        if (!loadMore) {
            loadingViewContainer.setVisibility(View.VISIBLE);
            tvDoneLoading.setVisibility(View.GONE);
        }
        /*PiwikUtils.sendEvent(getApplication(), PiwikConstants.KCONTACT_CATEGORY, PiwikConstants.KCONTACT_SEARCHED_ACTION,
                PiwikConstants.KCONTACT_SEARCHED_TXT);*/
        nextPage(true);
        adapter.notifyDataSetChanged();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        // imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
    }

    public void clear(View view) {
        // searchBox.setText("");
        search();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!loadMore) {
            return;
        }

        if (firstVisibleItem == visibleItemCount) {
            return;
        }

        if (((firstVisibleItem + visibleItemCount) == totalItemCount) && !(loadingMore)) {
            if (firstVisibleItem != 0 && !isLoadingContacts) {
                // nextPage(true);
                nextPage(true, isLoadMoreConnectionError);
                if (!Utils.isNetworkAvailable(AddContactsActivity.this)) {
                    isLoadMoreConnectionError = true;
                } else {
                    isLoadMoreConnectionError = false;
                }
            }
        }

    }

    private void nextPage(boolean isOnclick) {
        if (!isLoadingContacts) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                loadingContacts = new LoadContactsAsynTask(isOnclick).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            isLoadMoreConnectionError = false;
        }
    }

    private void nextPage(boolean isOnclick, boolean isLoadMoreConnectionError) {
        if (!isLoadingContacts && !isLoadMoreConnectionError) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                loadingContacts = new LoadContactsAsynTask(isOnclick).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (loadingContacts != null && !loadingContacts.isCancelled()) {
                loadingContacts.cancel(true);
                AppLog.d("Fisike", "com.mphrx.fisike.doInBackground : onBackPressed canceled");
            } else {
                AppLog.d("Fisike", "com.mphrx.fisike.doInBackground : onBackPressed not canceled");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            adapter.clearSDCard();
        }
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.clearSDCard();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (view.getId() == R.id.loadingView) {
            return;
        }
        String userName = "";

        new ChatUserOptionListDialog(this).onItemCLick(contacts.get(position), true);

//        userName = TextPattern.getUserId(contacts.get(position).getId());
//
//        DBAdapter dbAdapter = DBAdapter.getInstance(this);
//
//        final List<ChatConversationMO> chatConversationMOs = dbAdapter.getConverstionsListOfAUser(userName);
//
//        for (int i = chatConversationMOs.size() - 1; i >= 0; i--) {
//            if (TextUtils.isEmpty(chatConversationMOs.get(i).getGroupName()) &&
//                    TextUtils.isEmpty(chatConversationMOs.get(i).getNickName())
//                    || !chatConversationMOs.get(i).getJId().contains("_" + userName)
//                    || chatConversationMOs.get(i).getChatContactMoKeys().size() > 2) {
//                chatConversationMOs.remove(i);
//            }
//        }
//
//
//        if (chatConversationMOs.size() > 0) {
//            openPreviousChats(chatConversationMOs.get(0));
//            this.finish();
//            return;
//        }
//
//        UserType userType = contacts.get(position).getUserType();
//        startActivity(new Intent(this, MessageActivity.class).putExtra(VariableConstants.SENDER_ID, "").putExtra(VariableConstants.ADD_SCREEN, true)
//                .putExtra(VariableConstants.SENDER_NICK_NAME, contacts.get(position).getRealName())
//                .putExtra(TextConstants.CHAT_DETAIL, contacts.get(position)).putExtra(TextConstants.IS_RECENT_CHAT, false)
//                .putExtra(VariableConstants.SENDER_SECOUNDRY_ID, userName)
//                .putExtra(VariableConstants.SENDER_USER_TYPE, userType));
//
//        this.finish();

    }

//    private void openPreviousChats(ChatConversationMO chatConversationMO) {
//
//        int unReadCount = chatConversationMO.getUnReadCount();
//        chatConversationMO.setReadStatus(true);
//
//        boolean isGroupChat = chatConversationMO.getConversationType() == VariableConstants.conversationTypeGroup ? true : false;
//
//        String userName = Utils.cleanXMPPUserName(chatConversationMO.getJId());
//        String phoneNumber = chatConversationMO.getPhoneNumber();
//
//        Intent intent = new Intent(this, MessageActivity.class);
//
//        intent.putExtra(VariableConstants.SENDER_ID, userName).putExtra(VariableConstants.PHONE_NUMBER, phoneNumber)
//                .putExtra(VariableConstants.SENDER_NICK_NAME, chatConversationMO.getNickName())
//                .putExtra(VariableConstants.SENDER_UNREAD_COUNT, unReadCount).putExtra(TextConstants.IS_RECENT_CHAT, true)
//                .putExtra(VariableConstants.IS_GROUP_CHAT, isGroupChat)
//                .putExtra(VariableConstants.SENDER_SECOUNDRY_ID, chatConversationMO.getSecoundryJid());
//
//        startActivity(intent);
//
//    }

    @Override
    public void changeOnConnectionAvalable() {
        if (isScrollDownError || adapter.getCount() == 0) {
            isScrollDownError = false;
            nextPage(false);
            loadingView.setVisibility(View.VISIBLE);
        }
        super.changeOnConnectionAvalable();
    }

    @Override
    public void changeOnConnectionDisconnected() {
        ConnectionStatus.isConnectionAvailable = false;
        showError(true);
        ConnectionInfo.getInstance().setXmppConnection(null);

        loadingView.setVisibility(View.GONE);
        super.changeOnConnectionDisconnected();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
    }

    private class LoadContactsAsynTask extends AsyncTask<Void, Void, Void> {
        private boolean isOnclick;

        public LoadContactsAsynTask(boolean isOnclick) {
            this.isOnclick = isOnclick;
        }

        @Override
        protected void onPreExecute() {
            isLoadingContacts = true;
            loadMore = true;
            loadingView.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                loadingMore = true;
                String searchText = URLEncoder.encode(searchString.trim(), "UTF-8");
                searchText = searchText.replaceAll("\\+", "%20");
                if (null == userMO) {
                    userMO = SettingManager.getInstance().getUserMO();
                }
                final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + "/getContactList";

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("_count", 30);
                jsonObject.put("_skip", pageNum);
                jsonObject.put("searchText", searchText);

                APIManager jsonManager = APIManager.getInstance();
                jsonString = jsonManager.executeHttpPost(url, "", jsonObject, AddContactsActivity.this);
            } catch (Exception e) {
                jsonString = null;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            isLoadingContacts = false;
            try {
                if (jsonString != null && !(jsonString.equals(""))) {
                    loadContacts();
                    if (loadMore) {
                        pageNum += 10;
                        loadingView.setVisibility(View.GONE);
                        if (contacts.size() == 0 || contacts.size() <= listContacts.getLastVisiblePosition()) {
                            nextPage(false);
                        }
                    } else {
                        isLoadingContacts = false;
                        loadingViewContainer.setVisibility(View.GONE);
                        tvDoneLoading.setVisibility(View.VISIBLE);
                    }

                } else {
                    loadingView.setVisibility(View.GONE);
                    handleError(isOnclick);
                }
            } catch (Exception e) {
                showError(isOnclick);
            }
            if (!Utils.isNetworkAvailable(AddContactsActivity.this)) {
                loadingView.setVisibility(View.GONE);
            }
            super.onPostExecute(result);
        }
    }
}
