package com.mphrx.fisike.background.VolleyRequest.ForgetPasswordRequest;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse.ForgetPasswordSendOTPResponse;
import com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse.SetPasswordResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Neha on 19-02-2016.
 */
public class SetPasswordApiRequest extends BaseObjectRequest
{

    private String mNumber;
    private String mCountryCode;
    private long mTransactionId;
    private String otpCode;
    private String deviceUID;
    private String mAlternateContact;
    private String newPassword;

    public SetPasswordApiRequest(String phoneNumber,String newPassword, String deviceUID,String otpCode,String mAlternateContact,String mCountryCode, long mTransactionId) {
        super();
        this.mNumber = phoneNumber;
        this.otpCode = otpCode;
        this.mTransactionId = mTransactionId;
        this.deviceUID = deviceUID;
        this.mAlternateContact = mAlternateContact;
        this.newPassword=newPassword;
        this.mCountryCode=mCountryCode;
    }

    @Override
    public void doInBackground()
    {
        String url = APIManager.getInstance().getverifyOtpAndSetPasswordURL();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        request.setShouldCache(false);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SetPasswordResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new  SetPasswordResponse(response, mTransactionId));
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            if(SharedPref.getIsMobileEnabled()){
              payLoad.put(MphRxUrl.K.PHONE_NUMBER, mNumber);
              payLoad.put(MphRxUrl.K.COUNTRY_CODE,mCountryCode);
            }
            else if(!SharedPref.getIsMobileEnabled())
                payLoad.put(MphRxUrl.K.EMAIL,mNumber);
            payLoad.put(MphRxUrl.K.DEVICE_ID, deviceUID);
            if(mAlternateContact!=null&&!mAlternateContact.equals(""))
                payLoad.put(MphRxUrl.K.ALTERNATE_CONTACT,mAlternateContact);

            payLoad.put(MphRxUrl.K.OTP, otpCode);
            payLoad.put(MphRxUrl.K.NEW_PASSWORD,newPassword);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
