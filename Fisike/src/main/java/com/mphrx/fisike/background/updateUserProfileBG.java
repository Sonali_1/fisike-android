package com.mphrx.fisike.background;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UpdatePatientRequest;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike.update_user_profile.UpdateProfileBaseActivity;
import com.mphrx.fisike_physician.utils.AppLog;

import appointment.FragmentBaseActivity;
import appointment.fragment.PatientInformationFragment;

public class updateUserProfileBG extends AsyncTask<Void, Void, String> {

    UpdatePatientRequest request;
    Context context;
    String response;
    boolean isPasswordUpdated;
    updatePatientResponse responseGson;
    Fragment fragment;
    public updateUserProfileBG(Context context, UpdatePatientRequest request, boolean isPasswordUpdated) {
        // TODO Auto-generated constructor stub
        this.request = request;
        this.context = context;
        this.isPasswordUpdated = isPasswordUpdated;
    }

    public updateUserProfileBG(Context context,Fragment fragment, UpdatePatientRequest request, boolean isPasswordUpdated) {
        // TODO Auto-generated constructor stub
        this.request = request;
        this.context = context;
        this.isPasswordUpdated = isPasswordUpdated;
        this.fragment=fragment;
    }

    @Override
    protected String doInBackground(Void... arg0) {
        try {
            responseGson = updatePatient();
            if (responseGson != null)
                return TextConstants.SUCESSFULL_API_CALL;

            else
                return TextConstants.UNEXPECTED_ERROR;
        } catch (Exception e) {
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else if (e.getMessage().equals(TextConstants.PHONE_NO_NOT_UNIQUE))
                return TextConstants.PHONE_NO_NOT_UNIQUE;
            else if (e.getMessage().equals(TextConstants.EMAIL_NOT_UNIQUE))
                return TextConstants.EMAIL_NOT_UNIQUE;
            else
                return TextConstants.UNEXPECTED_ERROR;
        }

    }

    protected void onPostExecute(String result) {
        if (context instanceof ChangeDetails)
            ((com.mphrx.fisike.ChangeDetails) context).executeUpdateProfileResult(responseGson, isPasswordUpdated);

        else if (context instanceof UpdateProfileBaseActivity)
            ((UpdateProfileBaseActivity) context).executeUpdateProfileResult(responseGson, result);

        else if(context instanceof FragmentBaseActivity && fragment!=null && fragment instanceof PatientInformationFragment)
            ((PatientInformationFragment) fragment).updateProfileResponse(responseGson, result);
    }


    private updatePatientResponse updatePatient() throws Exception {
        APIManager apimanager = APIManager.getInstance();
        String url = apimanager.getUpdateProfileAPi();

        String json = new Gson().toJson(request);
        Gson gson = new Gson();
        AppLog.showInfo("UpdateUserProfile",json.toString());
        JsonElement element = gson.fromJson(json, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        ApiResult apiResult = apimanager.executeHttpPost(url, jsonObj, true, context);
        response = apiResult != null ? apiResult.getJsonString() : null;
        if (response != null) {
            if (apiResult.getStatusCode() == 503)
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, updatePatientResponse.class);
                updatePatientResponse responseGson = ((updatePatientResponse) jsonToObjectMapper);
                if (responseGson.getMsg().equals(TextConstants.PHONE_NO_NOT_UNIQUE))
                    throw new Exception(TextConstants.PHONE_NO_NOT_UNIQUE);
                else if (responseGson.getMsg().equals(TextConstants.EMAIL_NOT_UNIQUE))
                    throw new Exception(TextConstants.EMAIL_NOT_UNIQUE);
                else
                    return responseGson;

            }
        } else {
            throw new Exception(TextConstants.UNEXPECTED_ERROR);
        }
    }

}
