package com.mphrx.fisike.background;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.beans.HashPresence;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike_physician.activity.ViewProfile;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;

/**
 * This asynchronous task is responsible for set real name for contacts
 * 
 * @author Kailash
 */
public class LoadContactsRealName extends AsyncTask<Void, Void, Void> {
	public static boolean isLoadContactRealNameRunning;
	private String jsonString;
	private String emailString;
	private boolean isSetUpChatApi;
	private MessengerService messengerService;
	private Context context;

	private static final String CONTACT_REAL_NAME_URL = "/getUserProfiles";
	private HashMap<String, HashPresence> emailPresence;

	public LoadContactsRealName(MessengerService messengerService, String emailString, HashMap<String, HashPresence> emailPresence) {
		this.messengerService = messengerService;
		this.emailString = emailString;
		this.emailPresence = emailPresence;
	}

	public LoadContactsRealName(MessengerService messengerService, String emailString) {
		this.messengerService = messengerService;
		this.emailString = emailString;
	}

	public LoadContactsRealName(MessengerService messengerService, String emailString,boolean isSetUpChatApi) {
		this.messengerService = messengerService;
		this.emailString = emailString;
		this.isSetUpChatApi = isSetUpChatApi;
	}

	public LoadContactsRealName(MessengerService messengerService, String emailString, Context context) {
		this.messengerService = messengerService;
		this.emailString = emailString;
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		isLoadContactRealNameRunning = true;
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			emailString = URLDecoder.decode(emailString, "UTF-8");
			String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + CONTACT_REAL_NAME_URL;

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("commaSeparatedIds", emailString);

			APIManager jsonManager = APIManager.getInstance();
			jsonString = jsonManager.executeHttpPost(url, "", jsonObject, messengerService.getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		isLoadContactRealNameRunning = false;
		if(isSetUpChatApi) {
			SharedPref.setProgressForChat(false);
			messengerService.notifyNoConversationBroadCast();
		}
		messengerService.updateContactRealName(jsonString, emailPresence);

		if (null != context && context instanceof MessageActivity) {
			((MessageActivity) context).updateProfileRealName();
		}

		if(null != context && context instanceof ViewProfile) {
			((ViewProfile) context).updateProfileDetails();

		}


		super.onPostExecute(result);
	}
}