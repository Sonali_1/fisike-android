package com.mphrx.fisike.background;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

/**
 * This is the background process for downloading the images for the options answer
 *
 * @author kkhurana
 */
public class DownloadImages extends AsyncTask<Void, Void, Void> {

    private ArrayList<String> imageUrlArray;
    private ArrayList<Bitmap> imgArray;
    private Context context;

    public DownloadImages(Context context, ArrayList<String> imageUrlArray) {
        this.context = context;
        this.imageUrlArray = imageUrlArray;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Bitmap img;
        try {
            for (int i = 0; i < imageUrlArray.size(); i++) {
                String urlImage = imageUrlArray.get(i);
                if (null != urlImage && !"null".equalsIgnoreCase(urlImage)) {
                    InputStream in = new java.net.URL(urlImage).openStream();
                    img = BitmapFactory.decodeStream(in);
                } else {
                    img = null;
                }
                imgArray.add(img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }
}