package com.mphrx.fisike.background;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.ProfilePictureGson;
import com.mphrx.fisike.gson.request.ProfilePictureGson.ProfilePicture;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.services.MessengerService;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This asynchronous task is responsible for set real name for contacts
 * 
 * @author Kailash
 */
public class LoadContactsProfilePicAsyn extends AsyncTask<Void, Void, Void> {
	private String jsonString;
	private String emailString;
	private MessengerService messengerService;
	private Context context;
	private ImageView profilepicImageView;
	private Bitmap bitmap;
	private byte[] profilePic;
	private ProgressBar progressDialog;
	private int id;
	private final String GET_PROFILE_PIC = "/getProfilePicture";

	public LoadContactsProfilePicAsyn(MessengerService messengerService, String emailString, Context context) {
		this.messengerService = messengerService;
		this.emailString = emailString;
		this.context = context;
	}

	public LoadContactsProfilePicAsyn(MessengerService messengerService, String emailString, Context context, ImageView imageView,
			ProgressBar progressDialog) {
		this.messengerService = messengerService;
		this.emailString = emailString;
		this.context = context;
		this.profilepicImageView = imageView;
		this.progressDialog = progressDialog;
	}

	@Override
	protected Void doInBackground(Void... params) {
		runApi();
		return null;
	}

	protected void onPostExecute(Void result) {
		if (profilepicImageView == null) {
			messengerService.updateContactProfilePic(jsonString);

			if (null != context && context instanceof MessageActivity) {
				((MessageActivity) context).updateProfilePic();
			}
//			else if(null != context && context instanceof ProfileActivity){
//				((ProfileActivity) context).updateProfilePic();
//			}
		} else {
			if (bitmap != null) {
				profilepicImageView.setImageBitmap(bitmap);
				messengerService.updateContactProfilePic(profilePic, id + "");
			}
			if (progressDialog != null) {
				progressDialog.setVisibility(View.GONE);
			}
		}
	}

	private void parseJson(String json) {
		try {
			if (json != null && !("".equals(json))) {
				if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
					return;
				}

				Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ProfilePictureGson.class);

				if (jsonToObjectMapper instanceof ProfilePictureGson) {
					ProfilePictureGson profilePictureGson = ((ProfilePictureGson) jsonToObjectMapper);
					ArrayList<ProfilePicture> profilePictureArray = profilePictureGson.getProfilePictureArray();
					for (int i = 0; i < profilePictureArray.size(); i++) {
						ProfilePicture profilePicture = profilePictureArray.get(i);
						id = profilePicture.getId();
						profilePic = profilePicture.getProfilePic();
					}
					if (profilePic != null) {
						bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void runApi() {
		try {
			final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + GET_PROFILE_PIC;

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", emailString);

			APIManager jsonManager = APIManager.getInstance();
			jsonString = jsonManager.executeHttpPost(url, "", jsonObject, context);
			if (profilepicImageView != null) {
				parseJson(jsonString);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}