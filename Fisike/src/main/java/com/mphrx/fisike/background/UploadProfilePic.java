package com.mphrx.fisike.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UploadPicture;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;

import signup.activity.SignUpActivity;

public class UploadProfilePic extends AsyncTask<Void, Void, Void> {

    private Bitmap bitmap;
    private Context context;
    private String jsonString;
    private byte[] bitmapdata;
    private boolean isCallingClassHandleResponse, isToShowProgress, isunauthorized = false;

    public UploadProfilePic(Context context, Bitmap bitmap) {
        this.context = context;
        this.bitmap = bitmap;
        this.isCallingClassHandleResponse = false;
        this.isToShowProgress = false;
    }

    public UploadProfilePic(Context context, Bitmap bitmap, boolean isCallingClassHandleResponse, boolean isToShowProgress) {
        this.context = context;
        this.bitmap = bitmap;
        this.isCallingClassHandleResponse = isCallingClassHandleResponse;
        this.isToShowProgress = isToShowProgress;
    }

    @Override
    protected void onPreExecute() {
        if (isToShowProgress) {
            if (context instanceof UserProfileActivity)
                ((UserProfileActivity) context).progressStarted();
            else if (context instanceof UpdateUserProfileActivity)
                ((UpdateUserProfileActivity) context).progressStarted();
            else if(context instanceof SignUpActivity)
                ((SignUpActivity) context).showProgressDialog();
        }
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        postImageData();
        return null;
    }

    protected void onPostExecute(Void result) {

        if (context == null)
            return;


        if (context instanceof com.mphrx.fisike_physician.activity.ContactActivity) {
            if (isunauthorized || jsonString == null) {
                ((com.mphrx.fisike_physician.activity.ContactActivity) context).onExceptionOnUploadImage();
            }
            else {
                ((com.mphrx.fisike_physician.activity.ContactActivity) context).onSuccessfulUploadImage();
            }
            return;
        }

        if (isCallingClassHandleResponse) {
            if (jsonString != null)
                {
                Object jsonToObjectMapper = null;

                if((!jsonString.equals(TextConstants.SERVICE_UNAVAILABLE)) && (!jsonString.equals(TextConstants.UNAUTHORIZED_ACCESS)) && (!jsonString.equals(TextConstants.UNEXPECTED_ERROR)) && (!jsonString.equals(TextConstants.BARRED_LOCATION_ERROR_CODE)))
                    jsonToObjectMapper=GsonUtils.jsonToObjectMapper(jsonString,UploadPicture.class);

                if (context instanceof UserProfileActivity) {
                    ((UserProfileActivity) context).uploadProfilePic(jsonString, bitmapdata, ((UploadPicture) jsonToObjectMapper), isToShowProgress);
                }

                else if (context instanceof UpdateUserProfileActivity) {
                    ((UpdateUserProfileActivity) context).uploadProfilePic(jsonString, bitmapdata, ((UploadPicture) jsonToObjectMapper), isToShowProgress);
                }

            }
            else {
                if (context instanceof UserProfileActivity) {
                    ((UserProfileActivity) context).uploadProfilePic(jsonString, bitmapdata, null, isToShowProgress);
                }

                else if (context instanceof UpdateUserProfileActivity) {
                    ((UpdateUserProfileActivity) context).uploadProfilePic(jsonString, bitmapdata, null, isToShowProgress);
                }
            }
        }
        else {
            ((BaseActivity) context).uploadProfilePic(jsonString, bitmapdata);
        }
        super.onPostExecute(result);
    }

    /**
     *
     */
    public void postImageData() {

        try {
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO = settingManager.getSettings();
            UserMO userMO = settingManager.getUserMO();
            boolean useHTTPS = setMO.isUseHTTPS();

            String httpStr = "https://";

            if (!useHTTPS)
                httpStr = "http://";

            DefaultHttpClient httpclient = new DefaultHttpClient();
            APIManager manager = APIManager.getInstance();
            String url = manager.getUploadProfilePicAPI();

            AppLog.showInfo(getClass().getSimpleName(), url);
            HttpPost httpPost = new HttpPost(url);
            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();
            httpPost.setHeader("x-auth-token", authToken);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            if (null != bitmap) {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                bitmapdata = stream.toByteArray();
                entity.addPart("profilePic", new InputStreamBody(new ByteArrayInputStream(bitmapdata), System.currentTimeMillis() + ".jpg"));
            }
            httpPost.setEntity(entity);
            // httpPost.setHeader("content-length", bitmapdata.length+"");
            HttpResponse response = httpclient.execute(httpPost, new BasicHttpContext());
            int status = response.getStatusLine().getStatusCode();
            if (status == 401)
                jsonString = TextConstants.UNAUTHORIZED_ACCESS;
            else if((""+status).equalsIgnoreCase(TextConstants.BARRED_LOCATION_ERROR_CODE))
                jsonString=TextConstants.BARRED_LOCATION_ERROR_CODE;

            else if (status == 503)
                jsonString = TextConstants.SERVICE_UNAVAILABLE;

            else if (status != 200)
                jsonString = TextConstants.UNEXPECTED_ERROR;

            else {
                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer sb = new StringBuffer("");
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + NL);
                }
                in.close();
                jsonString = sb.toString();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}