package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.VideoViewActivity;
import com.mphrx.fisike.adapter.UploadedRecyclerAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.utils.Citadel;

public class DecriptFile extends AsyncTask<Void, Void, Void> {

    private String attachment_type;
    private String attachmentPath;
    private Context context;
    private byte[] decodFile;
    private ProgressDialog progressDialog;

    public DecriptFile(Context context, String attachmentPath, String attachment_type) {
        this.context = context;
        this.attachmentPath = attachmentPath;
        this.attachment_type=attachment_type;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
       // super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        decodFile = Citadel.decodFile(attachmentPath, context);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (context instanceof VideoViewActivity) {
            ((VideoViewActivity) context).postDecriptFile(decodFile);
        }
        else if (context instanceof OpenImageFragment) {
            ((OpenImageFragment) context).showDecriptedFile(decodFile);
        } else if (context instanceof HomeActivity) {
            ((HomeActivity) context).showDecriptedFile(decodFile, attachment_type);
        } else if (context instanceof MessageActivity) {

        }

      //  super.onPostExecute(result);
    }
}