package com.mphrx.fisike.background.VolleyResponse.OTPResponse;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 10/26/2016.
 */
public class ResendOtpTFAResponse extends BaseResponse {
    private JSONObject headers;
    private String usernameHeader;
    private String mobile,email;
    boolean isOtpSent;

    public ResendOtpTFAResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        parseResponse(response);
    }
    public ResendOtpTFAResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }
    private void parseResponse(JSONObject response) {
        JSONObject otpSentToDevicesjson = null;
        try {
            headers = (JSONObject) response.get("headers");
            isOtpSent = headers.getBoolean("isOTPSent");
            usernameHeader = headers.getString("username");
            headers.getString("oTPSentToDevices");
            String otpSentToDevices = headers.getString("oTPSentToDevices");

            otpSentToDevicesjson = new JSONObject(otpSentToDevices);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            mobile = otpSentToDevicesjson.getString("mobile");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            email=otpSentToDevicesjson.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    public String getUsernameHeader() {
        return usernameHeader;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public boolean isOtpSent() {
        return isOtpSent;
    }
}
