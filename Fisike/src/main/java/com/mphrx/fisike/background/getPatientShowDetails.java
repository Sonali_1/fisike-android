package com.mphrx.fisike.background;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.AgreementsActivity;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.UpdateProfileBaseActivity;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

public class getPatientShowDetails extends AsyncTask<Void, Void, PatientMO> {

    private Context context;

    private String patient_id;

    public getPatientShowDetails(Context context, String patient_id) {
        this.context = context;
        this.patient_id = patient_id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected PatientMO doInBackground(Void... params) {
        PatientMO patientMO = null;
        APIManager jsonManger = APIManager.getInstance();
        String url = jsonManger.getPatientShowCall(patient_id);
        String authToken = SharedPref.getAccessToken();
        String response = jsonManger.executeHttpPost(url, authToken, null, context);
        if (null != response) {

            if (response.contains("503 Service Unavailable")) {
                try {
                    throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            try {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, PatientMO.class);
                patientMO = (PatientMO) jsonToObjectMapper;
                if (patientMO != null) {
                    SettingManager.getInstance().savePatientMo(patientMO);
                    ContentValues contentValues = new ContentValues();
                 /*   if (patientMO.getAddress() != null) {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        ObjectOutputStream out = new ObjectOutputStream(bytes);
                        out.writeObject(patientMO.getAddress());
                        contentValues.put(UserDBAdapter.getADDRESS(), bytes.toByteArray());
                    }*/
                    if (patientMO.getMaritalStatus() != null) {
                        String maritalStatusText = patientMO.getMaritalStatus().getText();
                        contentValues.put(UserDBAdapter.getMARITALSTATUS(), maritalStatusText);
                    }
                    UserDBAdapter.getInstance(context).updateUserInfo(SettingManager.getInstance().getUserMO().getPersistenceKey(), contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return patientMO;
        }
        return null;
    }


    @Override
    protected void onPostExecute(PatientMO result) {
        if (context instanceof UpdateProfileBaseActivity) {
            ((UpdateProfileBaseActivity) context).executePatientCallResult(result);
        } else if (context instanceof LoginActivity) {
            ((LoginActivity) context).executePatientProfileResponse(result);
        } else if (context instanceof AgreementsActivity) {
            ((AgreementsActivity) context).executePatientProfileResponse(result);
        }
        if (context instanceof ChangeDetails)
            ((com.mphrx.fisike.ChangeDetails) context).executePatientCallResult(result);
    }
}
