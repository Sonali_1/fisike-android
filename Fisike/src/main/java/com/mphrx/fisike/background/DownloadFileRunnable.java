package com.mphrx.fisike.background;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.webkit.MimeTypeMap;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.VideoViewActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ExifUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class DownloadFileRunnable implements Runnable {

    private Context context;
    private String attachmentId;
    private ChatMessageMO chatMessageMo;
    private File file;
    private MessengerService mService;
    private ChatConversationMO chatConversationMO;
    private String fileType;
    private Intent intent;
    private int percentage;
    private String encodeFile;
    private String appName;
    private Bitmap createScaledBitmap;

    public DownloadFileRunnable(Context context, String attachmentId, ChatMessageMO message, ChatConversationMO chatConversationMO,
                                MessengerService mService, String fileType) {
        this.context = context;
        this.attachmentId = attachmentId;
        this.chatMessageMo = message;
        this.chatConversationMO = chatConversationMO;
        this.mService = mService;
        this.fileType = fileType;
        appName = context.getString(R.string.app_name);

        mService.saveAttachmentPath(chatMessageMo, null, TextConstants.ATTACHMENT_STATUS_WAITING, null, 0, null);

        chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_WAITING);

        intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED).putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMo).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void run() {
        try {
            chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_IN_PROGRESS);
            mService.saveAttachmentPath(chatMessageMo, null, TextConstants.ATTACHMENT_STATUS_IN_PROGRESS, null, 0, null);

            intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                    .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                    .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMo).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, true);
            // LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            if (!Utils.isNetworkAvailable(context)) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }

            }

            boolean isDownloadFile = downloadFile();

            if (isDownloadFile) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                createScaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] byteArray = baos.toByteArray();
                chatMessageMo.setImageThumb(byteArray);

                chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_DOWNLOADED);
                mService.saveAttachmentPath(chatMessageMo, encodeFile, TextConstants.ATTACHMENT_STATUS_DOWNLOADED, byteArray,
                        VariableConstants.RECORED_SYNC_STATUS_DEFAULT, chatMessageMo.getImageThumbnailPath());
            } else {
                chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
                mService.saveAttachmentPath(chatMessageMo, null, TextConstants.ATTACHMENT_STATUS_RECEIVED, null,
                        VariableConstants.RECORED_SYNC_STATUS_DEFAULT, null);
            }
        } catch (Exception e) {
            chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
            mService.saveAttachmentPath(chatMessageMo, null, TextConstants.ATTACHMENT_STATUS_RECEIVED, null,
                    VariableConstants.RECORED_SYNC_STATUS_DEFAULT, null);
            e.printStackTrace();
        } finally {
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    private boolean downloadFile() {
        try {
            URL httpPost = new URL(APIManager.createMinervaBaseUrl() + "/attachment/receiveAttachment");
            HttpsURLConnection urlConnection = (HttpsURLConnection) httpPost.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();

            if (authToken != null) {
                urlConnection.addRequestProperty("x-auth-token", authToken);
                // Uri.Builder builder = new Uri.Builder().appendQueryParameter("x-auth-token", authToken);
                // String query = builder.build().getEncodedQuery();
                //
                // OutputStream os = urlConnection.getOutputStream();
                // BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                // writer.write(query);

            }

            String str = "{\"attachmentId\":" + attachmentId + "}";
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            urlConnection.connect();

            String contentLengthStr = urlConnection.getHeaderField("Content-Length");

            String headerType = urlConnection.getHeaderField("Content-Type");

            long fileSize = Long.parseLong(contentLengthStr);

            boolean isSaveAttachmentSize = mService.isSaveAttachmentSize(chatMessageMo, fileSize);

            if (isSaveAttachmentSize) {
                intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                        .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                        .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMo).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            // int lenghtOfFile = urlConnection.getContentLength();
            String contentType = urlConnection.getContentType();

            InputStream input = new BufferedInputStream(urlConnection.getInputStream());

            String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(headerType);
            // File file2 = null;
            if (fileType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)) {
                // file = Utils.getOutputMediaFile(Utils.MEDIA_TYPE_IMAGE, extensionFromMimeType, context);
                file = Utils.saveToInternalSorage(context.getApplicationContext(), Utils.MEDIA_TYPE_IMAGE, "downloadTempFile.jpg");
            } else {
                // file = Utils.getOutputMediaFile(Utils.MEDIA_TYPE_VIDEO, extensionFromMimeType, context);
                file = Utils.saveToInternalSorage(context.getApplicationContext(), Utils.MEDIA_TYPE_VIDEO, "downloadTempFile.mp4");
            }
            // if (!(file2.exists())) {
            // file2.mkdirs();
            // }

            // file = new File(file2.getPath() + "/" + userMO.getUserName() + "-" + System.currentTimeMillis() + "." + extensionFromMimeType);

            // Output stream to write file
            // FileOutputStream output = new FileOutputStream(file);
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte data[] = new byte[2048];

            long total = 0;

            try {
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    percentage = (int) ((total * 100) / fileSize);
                    intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                            .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                            .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMo)
                            .putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false).putExtra(TextConstants.DOWNLOAD_PERCENTAGE, percentage);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    output.write(data, 0, count);
                }

                FileOutputStream fos = new FileOutputStream(file);
                output.writeTo(fos);

                if (fileType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)) {
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(output.toByteArray(), 0, output.toByteArray().length);
                    Bitmap thumbnail = ExifUtils.extractThumbnail(decodeByteArray);
                    Bitmap icon = ExifUtils.extractThumbnailMini(thumbnail);
                    MyApplication.setPreviewBitmap(thumbnail);
                    createScaledBitmap = icon;
                    chatMessageMo.setImageRecordSyncStatus(VariableConstants.RECORED_SYNC_STATUS_DEFAULT);
                    MyApplication.setImageThumb(icon);
                } else {
                    Bitmap imgthumb = ThumbnailUtils.createVideoThumbnail(file.getAbsolutePath(), MediaStore.Images.Thumbnails.MINI_KIND);
                    Bitmap thumbnail = ExifUtils.extractThumbnail(imgthumb);
                    Bitmap icon = ExifUtils.extractThumbnailMini(thumbnail);
                    MyApplication.setPreviewBitmap(thumbnail);
                    createScaledBitmap = icon;
                    MyApplication.setImageThumb(icon);
                }

                File thumbFile = Utils.createFile(Environment.getExternalStorageDirectory(), File.separator + context.getString(R.string.app_name)
                        + File.separator + VariableConstants.THUMB_PATH_STRING, System.currentTimeMillis() + "");
                ((MessageActivity) context).encriptThumb(thumbFile);
                String thumbImagePath = thumbFile.getAbsolutePath();
                chatMessageMo.setImageThumbnailPath(thumbImagePath);

                encodeFile = Citadel.encodeFile(file.getAbsolutePath(), context, appName, chatConversationMO.getJId());
                chatMessageMo.setAttachmentPath(encodeFile);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getPercentage() {
        return percentage;
    }

    public ChatMessageMO getChatMsgMO() {
        return chatMessageMo;
    }
}
