package com.mphrx.fisike.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

public class RemoveProfilePicture extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private String jsonString;
    private static final String REMOVE_PIC_BASE_URL = "/removeProfilePicture";

    public RemoveProfilePicture(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
            ((BaseActivity) context).progressStarted();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + REMOVE_PIC_BASE_URL;
            APIManager jsonManager = APIManager.getInstance();
            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();
            jsonString = jsonManager.executeHttpPost(url, null, authToken, context);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    protected void onPostExecute(Boolean result) {

        if (context instanceof UserProfileActivity) {
            ((UserProfileActivity) context).removeProfilePic(jsonString);
        } else if (context instanceof UpdateUserProfileActivity) {
            ((UpdateUserProfileActivity) context).removeProfilePic(jsonString);
        }
        super.onPostExecute(result);
    }
}