package com.mphrx.fisike.background;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UserLoggedInGson;
import com.mphrx.fisike.gson.request.UserLoggedInGson.LoggedInDetails;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.UUID;
import com.mphrx.fisike.utils.Utils;

public class CheckUserLoggedInBG extends AsyncTask<Void, Void, String> {

	public static boolean isUserDetailCall;
	private Context context;
	private String userName;
	private String password;
	private String jsonString;
	private UserMO userMO;
	private static final String IS_USER_LOGGED_IN = "/isUserLoggedIn";

	public CheckUserLoggedInBG(Context context, String userName, String password) {
		this.context = context;
		this.userName = userName;
		this.password = password;
		userMO = SettingManager.getInstance().getUserMO();
	}

	@Override
	protected void onPreExecute() {
		isUserDetailCall = true;
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(Void... params) {
		try {
			if(SharedPref.getAccessToken() != null)
				userMO = isUserLoggedIn();
		} catch (Exception e) {
			userMO.setRegistrationStatus(true);
			e.printStackTrace();
			//handling crash after signup
			if(!Utils.isValueAvailable(e.getMessage()))
				return TextConstants.UNEXPECTED_ERROR;

			if (e.getMessage().equalsIgnoreCase(TextConstants.SERVICE_UNAVAILABLE)) {
				return TextConstants.SERVICE_UNAVAILABLE;
			} else if (e.getMessage().equalsIgnoreCase(TextConstants.INVALID_LOGIN)) {
				return TextConstants.INVALID_LOGIN;
			} else if (e.getMessage().equalsIgnoreCase(TextConstants.UNEXPECTED_ERROR)) {
				return TextConstants.UNEXPECTED_ERROR;
			} else if (e.getMessage().equalsIgnoreCase(TextConstants.API_ERROR)) {
				return TextConstants.API_ERROR;
			}
			return TextConstants.UNEXPECTED_ERROR;
		}
		if (userMO == null) {
			return TextConstants.UNEXPECTED_ERROR;
		}
		return TextConstants.SUCESSFULL_USERDETAILS_CALL;
	}

	private UserMO isUserLoggedIn() throws Exception {
		String decodeUserName = URLDecoder.decode(userName, "UTF-8");
		decodeUserName.replaceAll(" ", "%20");
		userName = userName.replaceAll(" ", "%20");

		String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + IS_USER_LOGGED_IN;

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("deviceUID", SharedPref.getDeviceUid());

		APIManager jsonManager = APIManager.getInstance();
		jsonString = jsonManager.executeHttpPost(url,"", jsonObject, context);
		// jsonString = jsonManager.executeUrlResponse(isUserLoggedIn);
		if (null != jsonString && !jsonString.equals("")) {
			if (jsonString.contains("503 Service Unavailable")) {
				throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
			}
			Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, UserLoggedInGson.class);
			if (jsonToObjectMapper instanceof UserLoggedInGson) {
				UserLoggedInGson userLoggedIn = (UserLoggedInGson) jsonToObjectMapper;
				LoggedInDetails loggedInDetails = userLoggedIn.getLoggedInDetails();

				// If the user is still null - then it is an unexpected error and we return it as null
				if (loggedInDetails == null) {
					throw new Exception(TextConstants.API_ERROR);
				} else if (!loggedInDetails.isRegistrationStatus()) {
					throw new Exception(TextConstants.INVALID_LOGIN);
				}

				// Create UserMO
				userMO.setDeviceActivationDate(loggedInDetails.getDeviceActivationDate());
				// This is stored strictly on the server
				userMO.setRegistrationStatus(loggedInDetails.isRegistrationStatus());
				userMO.setDeviceUID(loggedInDetails.getDeviceUID());
				userMO.setPassword(password);
				userMO.setUsername(userName);
				return userMO;
			}

		}
		throw new Exception(TextConstants.API_ERROR);

	}

	/**
	 * This gets the result from the doInBackground call
	 */
	@Override
	protected void onPostExecute(String result) {
		isUserDetailCall = false;
		if (result.equals(TextConstants.INVALID_LOGIN)) {
			Log.d("LoginError", TextConstants.INVALID_LOGIN);
			AppLog.d("checkUserLoggedIn",401);
			if (BuildConfig.isPatientApp)
				APIManager.getInstance().launchHomeScreen(result, context);
			else
				SharedPref.setAccessToken(null);
			return;
		}

		if (result.equals(TextConstants.API_ERROR)) {
			Log.d("LoginError", TextConstants.API_ERROR + "-----Return");
			return;
		}

		if (userMO == null) {
			Log.d("LoginError", "User Mo null -----Return");
			return;
		}

		// if (!userMO.isRegistrationStatus()) {
		// launchHomeScreen(result);
		// return;
		// }

		UserMO orignalUserMo = SettingManager.getInstance().getUserMO();
		if (null == orignalUserMo) {
			return;
		}
		orignalUserMo.setDeviceActivationDate(userMO.getDeviceActivationDate());
		orignalUserMo.setRegistrationStatus(userMO.isRegistrationStatus());
		orignalUserMo.setDeviceUID(userMO.getDeviceUID());

		try {
			SettingManager.getInstance().updateUserMO(orignalUserMo);
		} catch (Exception e) {
		}
	}

	@SuppressLint("NewApi")
	private void removeBacktrace() {
		try {
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
				((Activity) context.getApplicationContext()).finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
