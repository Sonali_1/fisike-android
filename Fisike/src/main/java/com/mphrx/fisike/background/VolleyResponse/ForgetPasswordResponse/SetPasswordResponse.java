package com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.ForgetPasswordResponseGson.ForgetPasswordSendOTPResponseGson;
import com.mphrx.fisike.gson.response.ForgetPasswordResponseGson.SetPasswordResponseGson;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by Neha on 19-02-2016.
 */
public class SetPasswordResponse extends BaseResponse {


    SetPasswordResponseGson setPasswordResponseGson;

    public SetPasswordResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        setPasswordResponseGson= (SetPasswordResponseGson) GsonUtils.jsonToObjectMapper(response.toString(), SetPasswordResponseGson.class);
    }

    public SetPasswordResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public SetPasswordResponseGson getSetPasswordResponseGson() {
        return setPasswordResponseGson;
    }
}
