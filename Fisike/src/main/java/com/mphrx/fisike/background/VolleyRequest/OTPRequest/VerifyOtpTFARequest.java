package com.mphrx.fisike.background.VolleyRequest.OTPRequest;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.background.VolleyResponse.VerifyOtpTFAResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 10/25/2016.
 */
public class VerifyOtpTFARequest extends BaseObjectRequest {
    private String username;
    private String otpCode;
    private long transactionID;

    public VerifyOtpTFARequest(String usernameHeader, String otp, long mTransactionId) {
        username=usernameHeader;
        otpCode=otp;
        transactionID=mTransactionId;
    }

    @Override
    public void doInBackground() {
        String url = APIManager.getInstance().getVerifyOTPTFAUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.USERNAME, username);
            payLoad.put(MphRxUrl.K.OTP, otpCode);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new VerifyOtpTFAResponse(error, transactionID));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new VerifyOtpTFAResponse(response, transactionID));
    }
}
