package com.mphrx.fisike.background;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.AuthenticationGson;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.utils.SharedPref;

public class TokenGenrationApiBG extends AsyncTask<Void, Void, AuthenticationGson> {

	private Context context;
	private boolean isToShowProgressBar;
	ProgressDialog pd_ring;
	private boolean isCallingClassHandleResponse;

	public TokenGenrationApiBG(Context context) {
		this.context = context;
		this.isToShowProgressBar = false;
		this.isCallingClassHandleResponse = false;
	}

	public TokenGenrationApiBG(Context context, boolean isToShowProgressBar, boolean ishandleApiResult) {
		this.context = context;
		this.isToShowProgressBar = isToShowProgressBar;
		this.isCallingClassHandleResponse = ishandleApiResult;
		pd_ring = new ProgressDialog(context);
		pd_ring.setMessage("loading..");
		pd_ring.setCancelable(false);
		pd_ring.setCanceledOnTouchOutside(false);

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		if (isToShowProgressBar) {
			pd_ring.show();
		}

	}

	@Override
	protected AuthenticationGson doInBackground(Void... params) {
		try {
			APIManager apiManager = APIManager.getInstance();
			String otpUrl = apiManager.getAppTokenApi(context);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("username", "sys-admin");
			jsonObject.put("password", "sys-admin");
			String executeHttpPost = apiManager.executeHttpPost(otpUrl,null, jsonObject, context);
			Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(executeHttpPost, AuthenticationGson.class);
			if (jsonToObjectMapper instanceof AuthenticationGson) {
				return ((AuthenticationGson) jsonToObjectMapper);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(AuthenticationGson result) {
		if (isToShowProgressBar) {
			pd_ring.cancel();

		}
		if (result != null) {

			/*SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
			Editor edit = sharedPreferences.edit();
			edit.putString(VariableConstants.AUTH_TOKEN, result.getToken());
			edit.commit();*/
			SharedPref.setAccessToken(result.getToken());
		}

		if (isCallingClassHandleResponse) {
//			((com.mphrx.fisike.SignUpActivity) context).executeAuthTokenResult(result);
		}
		super.onPostExecute(result);
	}
}
