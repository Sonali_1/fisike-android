package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.emailStatusGson;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;

public class checkEmailChangeStatus extends AsyncTask<Void, Void, emailStatusGson> {


    Context context;
    emailStatusGson request;
    String verificationCode;
    private ProgressDialog pd_ring;


    public checkEmailChangeStatus(Context context, String verificationCode) {
        this.context = context;
        this.verificationCode = verificationCode;
        pd_ring = new ProgressDialog(context);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.verifying_your_email_account));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        pd_ring.show();
        super.onPreExecute();
    }


    @Override
    protected emailStatusGson doInBackground(Void... arg0) {
        // TODO Auto-generated method stub
        request = new emailStatusGson();
        request.setVerificationCode(verificationCode);
        String json = new Gson().toJson(request);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();

        APIManager apiManager = APIManager.getInstance();
        String mailVerificationUrl = apiManager.getMailChangeVerificationUrl(context);
        String response = apiManager.executeHttpPost(mailVerificationUrl, jsonObj);
        if (response != null) {
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, emailStatusGson.class);
            if (jsonToObjectMapper instanceof emailStatusGson) {
                return ((emailStatusGson) jsonToObjectMapper);
            }
        }
        return null;

    }

    @Override
    protected void onPostExecute(emailStatusGson result) {


        if (result != null) {
            UserMO user = new UserMO();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(context);
            user = SettingManager.getInstance().getUserMO();
            ContentValues values = new ContentValues();
//            values.put(userDBAdapter.getSign);
            user.setSignUpStatus(result.getEmailVerified());
            try {
                SettingManager.getInstance().updateUserMO(user);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (pd_ring != null && pd_ring.isShowing())
            pd_ring.dismiss();

    }


}
