package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.Fragment;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UploadsTaggingActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.UploadTaggingFragment;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;

import java.io.File;

public class EncriptFile extends AsyncTask<Void, Void, Void> {

    private String toEncriptFilePath;
    private ProgressDialog progressDialog;

    private File moveToInternalStorage;
    private int requestCode;
    private String filePath;
    private Context context;
    private boolean isCapturedImage;
    private int mediaType;
    private String senderName;
    private boolean isCameraBitmap = false;
    private Bitmap bitmap;

    private Fragment uploadsFragment;
    private DocumentReferenceModel documentReferenceModel;

    private String fileName;

    private String appName;

    private File fileImageThumb;
    private String encodeFilePath;

    public EncriptFile(Context context, String filePath, int requestCode, boolean isCapturedImage, int mediaType, String senderName) {
        this.context = context;
        this.filePath = filePath;
        this.requestCode = requestCode;
        this.isCapturedImage = isCapturedImage;
        this.mediaType = mediaType;
        this.senderName = senderName;
        appName = context.getResources().getString(R.string.app_name);

    }

    public EncriptFile(Context context, Bitmap bitmap, String senderName) {
        this.context = context;
        this.bitmap = bitmap;
        this.senderName = senderName;
        this.isCameraBitmap = true;
        appName = context.getResources().getString(R.string.app_name);
    }

    public EncriptFile(Context context, Bitmap bitmap, File fileImageThumb) {
        this.context = context;
        this.bitmap = bitmap;
        this.fileImageThumb = fileImageThumb;
        this.isCameraBitmap = true;
        appName = context.getResources().getString(R.string.app_name);
    }

    public EncriptFile(Context context, Bitmap bitmap, Fragment uploadsFragment, String toEncriptFilePath) {
        this.context = context;
        this.bitmap = bitmap;
        this.uploadsFragment = uploadsFragment;
        this.isCameraBitmap = true;
        this.fileName = System.currentTimeMillis() + "";
        this.toEncriptFilePath = toEncriptFilePath;
        appName = context.getResources().getString(R.string.app_name);
        String absoluteFile = (Utils.createFile(Environment.getExternalStorageDirectory(), "/" + appName, "" + fileName)).getAbsoluteFile()
                .toString();
        if (uploadsFragment instanceof UploadsFragment) {
            ((UploadsFragment) uploadsFragment).openFile(absoluteFile, toEncriptFilePath);
        } else if (uploadsFragment instanceof UploadTaggingFragment) {
            ((UploadTaggingFragment) uploadsFragment).setAttachmentPath(absoluteFile, toEncriptFilePath);
            ((UploadsTaggingActivity) uploadsFragment.getActivity()).setAttachmentPath(absoluteFile, toEncriptFilePath);
        }
    }

    public EncriptFile(Context context, Bitmap bitmap, BaseFragment uploadsFragment, String toEncriptFilePath) {
        this.context = context;
        this.bitmap = bitmap;
        this.isCameraBitmap = true;
        this.fileName = System.currentTimeMillis() + "";
        this.toEncriptFilePath = toEncriptFilePath;
        appName = context.getResources().getString(R.string.app_name);
        String absoluteFile = (Utils.createFile(Environment.getExternalStorageDirectory(), "/" + appName, "" + fileName)).getAbsoluteFile()
                .toString();

    }

    public EncriptFile(Context context, Bitmap bitmap, Fragment uploadsFragment, String toEncriptFilePath, DocumentReferenceModel documentReferenceModel) {
        this.context = context;
        this.bitmap = bitmap;
        this.uploadsFragment = uploadsFragment;
        this.documentReferenceModel = documentReferenceModel;
        this.isCameraBitmap = true;
        this.fileName = System.currentTimeMillis() + "";
        this.toEncriptFilePath = toEncriptFilePath;
        appName = context.getResources().getString(R.string.app_name);
    }

    @Override
    protected void onPreExecute() {
        if (uploadsFragment == null) {
            try {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (fileImageThumb != null) {
            String encodeFilePath = Citadel.encodeFile(context, appName, fileName, fileImageThumb, Citadel.getByteArray(bitmap));
            return null;
        } else if (toEncriptFilePath != null) {
            encodeFilePath = Citadel.encodeFile(context, appName, fileName, fileImageThumb, Citadel.getByteArrayFile(toEncriptFilePath));
            return null;
        }
        if (isCameraBitmap) {
            moveToInternalStorage = Utils.encriptBitmap(context, bitmap, fileName, appName);
        } else {
            moveToInternalStorage = Utils.encriptAndMove(context, filePath, mediaType, isCapturedImage, senderName, appName);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (uploadsFragment instanceof UploadsFragment && documentReferenceModel != null) {
            documentReferenceModel.setDocumentLocalPath(encodeFilePath);
            ContentValues contentValues = new ContentValues();
            contentValues.put(DocumentReferenceDBAdapter.getDOCUMENTLOCALPATH(), encodeFilePath);
            contentValues.put(DocumentReferenceDBAdapter.getISDIRTY(),0);
            if (documentReferenceModel.getThumbImage() != null) {
                contentValues.put(DocumentReferenceDBAdapter.getTHUMBIMAGE(), documentReferenceModel.getThumbImage());
            }
            DocumentReferenceDBAdapter.getInstance(context).updateDocumentReference(documentReferenceModel.getDocumentID(), contentValues);
            return;
        }

        if (moveToInternalStorage != null) {
            if (context instanceof MessageActivity) {
                ((MessageActivity) context).postEncription(moveToInternalStorage, requestCode, filePath);
            } else if (context instanceof HomeActivity) {
                if (uploadsFragment instanceof UploadsFragment) {
                    ((UploadsFragment) uploadsFragment).postEncription(moveToInternalStorage, requestCode, filePath);
                }
            }
        }
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        super.onPostExecute(result);
    }

}
