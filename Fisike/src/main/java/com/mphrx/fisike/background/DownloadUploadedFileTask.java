package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.PdfPreview;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kailash Khurana on 3/30/2016.
 */
public class DownloadUploadedFileTask implements Runnable {
    private DocumentReferenceModel documentReferenceModel;
    private int position;
    private Context context;
    private int docId;
    private int mimeType;
    private File file;

    public DownloadUploadedFileTask(Context context, int docId, int mimeType, DocumentReferenceModel documentReferenceModel) {
        this.context = context;
        this.docId = docId;
        this.mimeType = mimeType;
        this.documentReferenceModel = documentReferenceModel;
    }

    @Override
    public void run() {
        String fileName = "tempFileDownloadedLazy_" + docId;
        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", fileName + ".pdf");
        } else if (mimeType == VariableConstants.MIME_TYPE_DOC) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", fileName + ".doc");
        } else if (mimeType == VariableConstants.MIME_TYPE_IMAGE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", fileName + ".jpg");
        }
        downloadFile(docId, file);

        if (!file.exists() && file.length() <= 0) {
            return;
        }
        if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_IMAGE) {
            Bitmap imageBitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getAbsolutePath()), 150, 150);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (imageBitmap != null)
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageData = baos.toByteArray();
            documentReferenceModel.setThumbImage(imageData);
        }

        String appName = context.getResources().getString(R.string.app_name);
        String encodeFilePath = Citadel.encodeFile(context, appName, System.currentTimeMillis() + "", null, Citadel.getByteArrayFile(file.getAbsolutePath()));

        documentReferenceModel.setDocumentLocalPath(encodeFilePath);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DocumentReferenceDBAdapter.getDOCUMENTLOCALPATH(), encodeFilePath);
        if (documentReferenceModel.getThumbImage() != null) {
            contentValues.put(DocumentReferenceDBAdapter.getTHUMBIMAGE(), documentReferenceModel.getThumbImage());
        }
        DocumentReferenceDBAdapter.getInstance(context).updateDocumentReference(documentReferenceModel.getDocumentID(), contentValues);

        Intent intent = (new Intent(VariableConstants.BROADCAST_DOCUMENT)).putExtra(VariableConstants.DOCUMENT_DOWNLOADED_MODEL, documentReferenceModel);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        file.deleteOnExit();
    }

    public int getDocId() {
        return docId;
    }

    private boolean downloadFile(int docId, File file) {
        InputStream input = null;
        FileOutputStream fos = null;
        HttpsURLConnection urlConnection = null;
        ByteArrayOutputStream output = null;
        try {
            URL httpPost = new URL(MphRxUrl.getDocumentImageUrl());
            urlConnection = (HttpsURLConnection) httpPost.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();

            if (authToken != null) {
                urlConnection.addRequestProperty("x-auth-token", authToken);
            }

            String str = "{\"docRefId\":" + docId + "}";
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            urlConnection.connect();

            String headerType = urlConnection.getHeaderField("Content-Type");

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
//            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                return false;
//            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = urlConnection.getContentLength();

            if (file == null) {
                return false;
            }

            // download the file
            input = urlConnection.getInputStream();
            fos = new FileOutputStream(file.getAbsoluteFile());

            byte data[] = new byte[2048];
            int count;
            long total = 0;
            output = new ByteArrayOutputStream();

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.writeTo(fos);
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (fos != null)
                    fos.close();
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return true;
    }
}
