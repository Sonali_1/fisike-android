package com.mphrx.fisike.background;

import org.w3c.dom.Text;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class CheckIfEmailExistsBG extends AsyncTask<Void, Void, String> {

    String email;
    private ProgressDialog pd_ring;
    Context context;
    CheckEmailExistGson response;

    public CheckIfEmailExistsBG(String email, Context context) {
        this.email = email;
        this.context = context;
        pd_ring = new ProgressDialog(context);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hang_email_exist_or_not));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        pd_ring.show();
        super.onPreExecute();


    }

    @Override
    protected String doInBackground(Void... arg0) {
        // TODO Auto-generated method stub
        try {
            response = executeApi();
            if (response != null) {
                if (response.getMsg().equals(TextConstants.EMAIL_NOT_UNIQUE))
                    return TextConstants.EMAIL_NOT_UNIQUE;
                else
                    return TextConstants.SUCESSFULL_API_CALL;
            }

            return TextConstants.UNEXPECTED_ERROR;
        } catch (Exception e) {
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else
                return TextConstants.UNEXPECTED_ERROR;

        }

    }


    private CheckEmailExistGson executeApi() throws Exception {
        CheckEmailExistGson request = new CheckEmailExistGson();
        request.setEmail(email);
        String json = new Gson().toJson(request);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        APIManager apiManager = APIManager.getInstance();
        String url = apiManager.getEmailExitUrl();
        ApiResult apiResult = apiManager.executeHttpPost(url, jsonObj, true, context);
        String response = apiResult != null ? apiResult.getJsonString() : null;
        if (response != null) {
            // error 401
            if (response.equals(TextConstants.UNAUTHORIZED_ACCESS))
                throw new Exception(TextConstants.UNAUTHORIZED_ACCESS);
                // error 503
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
                // error not 200
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, CheckEmailExistGson.class);
                if (jsonToObjectMapper instanceof CheckEmailExistGson)
                    return ((CheckEmailExistGson) jsonToObjectMapper);

            }

        } else
            throw new Exception(TextConstants.UNEXPECTED_ERROR);
        return null;

    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        pd_ring.dismiss();
        if (context instanceof ChangeDetails)
            ((com.mphrx.fisike.ChangeDetails) context).executeCheckEmailExistResult(response, result);
    }

}
