package com.mphrx.fisike.background.VolleyResponse;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 10/25/2016.
 */
public class VerifyOtpTFAResponse extends BaseResponse {

    private String msg="",status="";

    public VerifyOtpTFAResponse(JSONObject response, long transactionId) {

        super(response, transactionId);
        /*try {
            msg=response.getString("msg");
            status=response.getString("status");
        } catch (JSONException e) {
            msg="Something went wrong";
            e.printStackTrace();
        }*/

    }

    public VerifyOtpTFAResponse(VolleyError error, long transactionID) {

        super(error, transactionID);
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }
}
