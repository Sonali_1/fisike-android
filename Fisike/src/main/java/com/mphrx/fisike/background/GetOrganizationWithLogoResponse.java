package com.mphrx.fisike.background;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.GetOrganizationInformationLogoResponse;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by Neha on 10-03-2016.
 */
public class GetOrganizationWithLogoResponse extends BaseResponse
{


    GetOrganizationInformationLogoResponse getOrganizationWithLogoResponse;

    public GetOrganizationWithLogoResponse(JSONObject response, long transactionId)
    {
         super(response, transactionId);
         getOrganizationWithLogoResponse= (GetOrganizationInformationLogoResponse)GsonUtils.jsonToObjectMapper(response.toString(),GetOrganizationInformationLogoResponse.class);
    }

    public GetOrganizationWithLogoResponse(Exception exception, long transactionId)
    {
        super(exception, transactionId);
    }

    public GetOrganizationInformationLogoResponse getGetOrganizationWithLogoResponse() {
        return getOrganizationWithLogoResponse;
    }
}
