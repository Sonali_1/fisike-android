package com.mphrx.fisike.background.VolleyRequest.OTPRequest;



import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.ResendOtpTFAResponse;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.network.response.LoginResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by aastha on 10/25/2016.
 */
public class ResendOtpTFARequest extends BaseObjectRequest {
    private long transactionID;

    public ResendOtpTFARequest(long transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public void doInBackground() {

        String url= MphRxUrl.getResendOTPTFAUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url,null, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new ResendOtpTFAResponse(error, transactionID));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new ResendOtpTFAResponse(response, transactionID));
    }
}
