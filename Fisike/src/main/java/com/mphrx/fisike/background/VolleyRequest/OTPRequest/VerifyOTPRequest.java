package com.mphrx.fisike.background.VolleyRequest.OTPRequest;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.verifyOTPResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Neha on 15-02-2016.
 */
public class VerifyOTPRequest extends BaseObjectRequest {


    private String mNumber;
    private long mTransactionId;
    private String otpCode;
    private String deviceUID;
    private String mAlternateContact;
    private String mCountryCode;

    public VerifyOTPRequest(String number, String otpCode,long transactionId,String deviceUID,String mCountryCode) {
        this.mNumber = number;
        this.otpCode=otpCode;
        mTransactionId = transactionId;
        this.deviceUID=deviceUID;
        mAlternateContact=null;
        this.mCountryCode=mCountryCode;

    }
    public VerifyOTPRequest(String number, String otpCode,long transactionId,String deviceUID,String mAlternateContact,String mCountryCode) {
        this.mNumber = number;
        this.otpCode=otpCode;
        mTransactionId = transactionId;
        this.deviceUID=deviceUID;
        this.mAlternateContact=mAlternateContact;
        this.mCountryCode=mCountryCode;
    }


    @Override
    public void doInBackground() {
        String url = APIManager.getInstance().getVerifyOTPUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new verifyOTPResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new verifyOTPResponse(response, mTransactionId));
    }

    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.DEVICE_ID, deviceUID);
            if(SharedPref.getIsMobileEnabled()){
               payLoad.put(MphRxUrl.K.PHONE_NUMBER, mNumber);
                payLoad.put(MphRxUrl.K.COUNTRY_CODE,mCountryCode);
            }
            else if(!SharedPref.getIsMobileEnabled())
                payLoad.put(MphRxUrl.K.EMAIL,mNumber);
            if(mAlternateContact!=null&&!mAlternateContact.equals(""))
                payLoad.put(MphRxUrl.K.ALTERNATE_CONTACT,mAlternateContact);

            payLoad.put(MphRxUrl.K.OTP, otpCode);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad.toString();
    }
}
