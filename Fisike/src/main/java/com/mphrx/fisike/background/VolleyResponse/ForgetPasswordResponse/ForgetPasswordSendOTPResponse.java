package com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.ForgetPasswordResponseGson.ForgetPasswordSendOTPResponseGson;
import com.mphrx.fisike.gson.response.OTPResponse.VerifyOTPResponseGson;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by Neha on 18-02-2016.
 */
public class ForgetPasswordSendOTPResponse extends BaseResponse{


    ForgetPasswordSendOTPResponseGson forgetPasswordSendOTPResponse;

    public ForgetPasswordSendOTPResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        forgetPasswordSendOTPResponse= (ForgetPasswordSendOTPResponseGson) GsonUtils.jsonToObjectMapper(response.toString(),ForgetPasswordSendOTPResponseGson.class);
    }

    public ForgetPasswordSendOTPResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public ForgetPasswordSendOTPResponseGson getForgetPasswordSendOTPResponse() {
        return forgetPasswordSendOTPResponse;
    }
}
