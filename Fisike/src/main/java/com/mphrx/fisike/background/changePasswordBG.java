package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.mphrx.fisike.ChangeDetails;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.emailStatusGson;
import com.mphrx.fisike.gson.response.ForgetPassOtpVerifyResponse;
import com.mphrx.fisike.gson.response.GenericStatusMsgResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.network.MphRxUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by administrate on 12/8/2015.
 */
public class changePasswordBG extends AsyncTask<Void, Void, String> {
    String newPass;
    String oldPass;
    Context context;
    String deviceType;
    private ProgressDialog pd_ring;
    private String response;
    private ForgetPassOtpVerifyResponse responseJson;
    private boolean isAdminFlow;

    public changePasswordBG(boolean isAdminFlow, String oldPassword, String newPass, Context context) {
        this.newPass = newPass;
        this.context = context;
        this.oldPass = oldPassword;
        this.isAdminFlow = isAdminFlow;
        pd_ring = new ProgressDialog(context);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hangon));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        pd_ring.show();
        try {
            TextView tv1 = (TextView) pd_ring.findViewById(android.R.id.message);
            tv1.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            APIManager jsonManager = APIManager.getInstance();
            JsonObject jsonobject = new JsonObject();
            jsonobject.addProperty("newPassword", newPass);
            if (!isAdminFlow)
                jsonobject.addProperty("deviceType", "android");
            jsonobject.addProperty("oldPassword", oldPass);
            APIManager apiManager = APIManager.getInstance();

            String changePasswordURL = new String();
            if (!isAdminFlow)
                changePasswordURL = apiManager.getChangePasswordURL();
            else
                changePasswordURL = MphRxUrl.getPassWordChangeApiAdmin();
            ApiResult apiResult = apiManager.executeHttpPost(changePasswordURL, jsonobject, true, context);
            response = apiResult != null ? apiResult.getJsonString() : null;
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, ForgetPassOtpVerifyResponse.class);
            responseJson = (ForgetPassOtpVerifyResponse) jsonToObjectMapper;
            if (responseJson.getHttpStatusCode() == 200)
                return TextConstants.SUCESSFULL_API_CALL;

            else if (responseJson.getHttpStatusCode() == 422)
                return TextConstants.MISMATCH_OLD_PASS;
            else if (response != null && (responseJson.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_423)
                    && responseJson.getStatus().equals(TextConstants.HTTP_STATUS_UV09)) {
                return context.getResources().getString(R.string.same_old_new_pass);
            }


        } catch (Exception e) {
            responseJson = null;
            return TextConstants.UNEXPECTED_ERROR;
        }
        return TextConstants.UNEXPECTED_ERROR;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        pd_ring.dismiss();
        if (context instanceof ChangeDetails)
            ((com.mphrx.fisike.ChangeDetails) context).executeChangePassword(responseJson, s);
        else if (context instanceof LoginActivity)
            ((LoginActivity) context).executeChangePassword(responseJson, s);
    }
}
