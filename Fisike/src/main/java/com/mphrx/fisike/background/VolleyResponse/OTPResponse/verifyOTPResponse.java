package com.mphrx.fisike.background.VolleyResponse.OTPResponse;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.OTPResponse.VerifyOTPResponseGson;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by Neha on 15-02-2016.
 */
public class verifyOTPResponse extends BaseResponse {

    VerifyOTPResponseGson verifyOTPResponseGson;

    public verifyOTPResponse(JSONObject response, long transactionId) {
        super(response, transactionId);
        this.verifyOTPResponseGson= (VerifyOTPResponseGson) GsonUtils.jsonToObjectMapper(response.toString(),VerifyOTPResponseGson.class);
    }

    public verifyOTPResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }


    public VerifyOTPResponseGson getVerifyOTPResponseGson() {
        return verifyOTPResponseGson;
    }
}
