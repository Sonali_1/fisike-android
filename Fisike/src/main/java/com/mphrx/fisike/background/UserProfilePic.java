package com.mphrx.fisike.background;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.ProfilePictureGson;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.ExifUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class UserProfilePic extends AsyncTask<Void, Void, ProfilePictureGson> {

    private Context context;
    private final String GET_PROFILE_PIC = "/getProfilePicture";

    public UserProfilePic(Context context) {
        this.context = context;
    }

    @Override
    protected ProfilePictureGson doInBackground(Void... params) {

        final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + GET_PROFILE_PIC;
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIManager jsonManager = APIManager.getInstance();
        String response = jsonManager.executeHttpPost(url, "", null, context);

        if (null != response) {

            if (response.contains("503 Service Unavailable")) {
                try {
                    throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (!response.contains("not found")) {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, ProfilePictureGson.class);
                return (ProfilePictureGson) jsonToObjectMapper;
            } else {
                return null;
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(ProfilePictureGson result) {
        super.onPostExecute(result);
        if (result != null) {

            byte[] pic = result.getProfilePictureArray().get(0).getProfilePic();
            saveToInternalStorage(BitmapFactory.decodeByteArray(pic, 0, pic.length), context);
        }
    }

    public static String saveToInternalStorage(Bitmap bitmapImage, Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, SettingManager.getInstance().getUserMO().getId() + ".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public static Bitmap loadImageFromStorage(Context context) {
        ContextWrapper cw = new ContextWrapper(context);

        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {

            File f = new File(directory.getAbsolutePath(), SettingManager.getInstance().getUserMO().getId() + ".jpg");
            if (f.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                /*ExifUtils.rotateBitmap(f.getAbsolutePath(), */
                BitmapFactory.decodeStream(new FileInputStream(f), null, options)/*)*/;
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                options.inJustDecodeBounds = false;
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void deleteProfilePic(Context context) {
        ContextWrapper cw = new ContextWrapper(context);

        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f = new File(directory.getAbsolutePath(), SettingManager.getInstance().getUserMO().getId() + ".jpg");

            if (f.exists()) {
                f.delete();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
