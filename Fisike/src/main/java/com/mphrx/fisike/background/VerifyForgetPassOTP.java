package com.mphrx.fisike.background;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.mphrx.fisike.ForgetPasswordActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.ForgetPassOtpVerifyResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

/**
 * Created by administrate on 12/23/2015.
 */
public class VerifyForgetPassOTP extends AsyncTask<Void,Void,String> {
    Context context;
    private ProgressDialog pd_ring;
    String OtpCode,deviceUID;
    ForgetPassOtpVerifyResponse response;

    public VerifyForgetPassOTP(Context context, String OtpCode) {
        this.context = context;
        pd_ring = new ProgressDialog(context);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.verifying_your_email_account));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
        this.OtpCode=OtpCode;
        this.deviceUID= SharedPref.getDeviceUid();
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        pd_ring.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {

        try
        {
            response=executeMailVerificationAPI();
            if(response!=null)
                return TextConstants.SUCESSFULL_API_CALL;
            else
                return TextConstants.UNEXPECTED_ERROR;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            if(e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if(e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else
                return TextConstants.UNEXPECTED_ERROR;
        }


    }

    @Override
    protected void onPostExecute(String result) {

        pd_ring.dismiss();
        if(context instanceof ForgetPasswordActivity)
            ((com.mphrx.fisike.ForgetPasswordActivity)context).executeForgetPassOTPVerifyAPI(response,result,OtpCode);

    }

    private ForgetPassOtpVerifyResponse executeMailVerificationAPI() throws Exception
    {
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("otp",OtpCode);
        jsonObj.addProperty("deviceUID",deviceUID);
        APIManager apiManager = APIManager.getInstance();
        String mailVerificationUrl = apiManager.getForgetPassOTPVerifyURL();
        String response = apiManager.executeHttpPost(mailVerificationUrl, jsonObj);

        if (response != null) {

            if(response.equals(TextConstants.UNAUTHORIZED_ACCESS))
                throw new Exception(TextConstants.UNAUTHORIZED_ACCESS);
            else if(response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if(response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else
            {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, ForgetPassOtpVerifyResponse.class);
                if (jsonToObjectMapper instanceof ForgetPassOtpVerifyResponse)
                    return ((ForgetPassOtpVerifyResponse) jsonToObjectMapper);
            }
        }
        else
            throw new Exception(TextConstants.UNEXPECTED_ERROR);

        return null;
    }

}

