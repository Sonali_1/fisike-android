package com.mphrx.fisike.background;

import java.net.URLDecoder;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;

/**
 * This asynchronous task is responsible for set specialty and designation
 * 
 * @author Kkhurana
 */
public class CustomSelectedListTask extends AsyncTask<Void, Void, Void> {
    private String jsonString;
    private static final String CUSTOM_SELECT_LIST_URL = "/userApi/getCustomSelectList?&username=";
    private static final String PASSWORD = "&password=";
    private String userName;
    private String password;

    public CustomSelectedListTask(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            String url = CUSTOM_SELECT_LIST_URL + URLDecoder.decode(userName, "UTF-8") + PASSWORD + password;
            final String customSelectURL = APIManager.createUrl(url);
            APIManager jsonManager = APIManager.getInstance();
            jsonString = jsonManager.getUrlResponse(customSelectURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (null == jsonString || jsonString.equals("")) {
            return;
        }
        try {
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, UserMO.class);
            if (jsonToObjectMapper instanceof UserMO) {
                UserMO tempUserMO = ((UserMO) jsonToObjectMapper);
                UserMO userMO = SettingManager.getInstance().getUserMO();
                if (userMO != null) {
                    userMO.setSpecialitiesArray(tempUserMO.getSpecialitiesArray());
                    userMO.setDesignationArray(tempUserMO.getDesignationArray());
                    SettingManager.getInstance().updateUserMO(userMO);
                }
            }
        } catch (Exception e) {

        }
        Intent intent = new Intent(VariableConstants.CUSTOM_SELECTED_LIST);
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);

        super.onPostExecute(result);
    }
}