package com.mphrx.fisike.background;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.CheckPhoneNumberExistGson;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.OtpActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class CheckIfPhoneExist extends AsyncTask<Void, Void, String> {

    String phoneNumber;
    private ProgressDialog pd_ring;
    Context context;
    CheckPhoneNumberExistGson response;

    public CheckIfPhoneExist(String phoneNumber, Context context) {
        this.phoneNumber = phoneNumber;
        this.context = context;

    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();


    }

    @Override
    protected String doInBackground(Void... arg0) {
        // TODO Auto-generated method stub
        try {
            response = executeApi();
            if (response != null) {
                if (response.getMsg().equals(TextConstants.MULTIPLE_USERS_FOUND))
                    return TextConstants.PHONE_NO_NOT_UNIQUE;
                else
                    return TextConstants.SUCESSFULL_API_CALL;
            }

            return TextConstants.UNEXPECTED_ERROR;
        } catch (Exception e) {
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else
                return TextConstants.UNEXPECTED_ERROR;

        }

    }


    private CheckPhoneNumberExistGson executeApi() throws Exception {
        CheckPhoneNumberExistGson request = new CheckPhoneNumberExistGson();
        request.setPhoneNumber(phoneNumber);
        request.setUserType(Utils.getUserType());
        String json = new Gson().toJson(request);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        APIManager apiManager = APIManager.getInstance();
        String url = apiManager.getPhoneExistUrl();
        ApiResult apiResult = apiManager.executeHttpPost(url, jsonObj, true, context);
        String response = apiResult != null ? apiResult.getJsonString() : null;
        if (response != null) {
            // error 503
            if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
                // error not 200
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, CheckPhoneNumberExistGson.class);
                if (jsonToObjectMapper instanceof CheckPhoneNumberExistGson)
                    return ((CheckPhoneNumberExistGson) jsonToObjectMapper);

            }

        } else
            throw new Exception(TextConstants.UNEXPECTED_ERROR);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
//		pd_ring.dismiss();
//        if (context instanceof OtpActivity)
//            ((OtpActivity) context).executePhoneExist(response, result);
    }

}
