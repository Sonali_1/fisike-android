package com.mphrx.fisike.background;

import java.net.URLDecoder;

import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;

public class LoadGroupContactProfilePic implements Runnable {
    private String jsonString;
    private String emailString;
    private MessengerService messengerService;
    private static final String LOAD_GROUP_PROFILE_PIC_URL = "/userApi/getGroupChatDisplayPic?&username=";
    private static final String PASSWORD = "&password=";
    private static final String GROUP_ID = "&groupId=";

    public LoadGroupContactProfilePic(MessengerService messengerService, String emailString) {
        this.messengerService = messengerService;
        this.emailString = emailString;
    }

    @Override
    public void run() {
        runApi();
        messengerService.updateGroupContactProfilePic(jsonString, emailString);
    }

    private void runApi() {
        try {
            SettingManager settingManager = SettingManager.getInstance();
            UserMO userMO = settingManager.getUserMO();
            if (userMO == null) {
                return;
            }

            final String url = LOAD_GROUP_PROFILE_PIC_URL + URLDecoder.decode(userMO.getUsername(), "UTF-8") + PASSWORD + userMO.getPassword()
                    + GROUP_ID + emailString;
            final String loadGroupProfilePicURL = APIManager.createUrl(url);

            APIManager jsonManager = APIManager.getInstance();
            jsonString = jsonManager.executeUrlResponse(loadGroupProfilePicURL);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getEmailString() {
        return emailString;
    }
}