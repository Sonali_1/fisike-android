package com.mphrx.fisike.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.UploadDeviceTokenRequest;
import com.mphrx.fisike.gson.response.UploadDeviceTokenResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

/**
 * Created by xmb2nc on 18-11-2015.
 */
public class UploadDeviceTokenBG extends AsyncTask<Void, Void, Void> {
    private Context context;
    private String token;

    public UploadDeviceTokenBG(Context context, String token) {
        this.context = context;
        this.token = token;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        UploadDeviceTokenRequest uploadDeviceTokenRequest = new UploadDeviceTokenRequest();
        uploadDeviceTokenRequest.setDeviceUID(SharedPref.getDeviceUid());
        uploadDeviceTokenRequest.setToken(token);
        uploadDeviceTokenRequest.setOsType(TextConstants.OS_TYPE);
        uploadDeviceTokenRequest.setDeviceType(TextConstants.DEVICE_TYPE);
        uploadDeviceTokenRequest.setAppVersion(TextConstants.APP_VERSION);

        String json = new Gson().toJson(uploadDeviceTokenRequest);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(json, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();

        try {
            APIManager apiManager = APIManager.getInstance();
            String uploadDeviceTokenUrl = apiManager.getUploadDeviceTokenUrl(context);
            ApiResult apiResult = apiManager.executeHttpPost(uploadDeviceTokenUrl, jsonObj, true, context);
            String response = (apiResult.getStatusCode() != 200) ? null : apiResult.getJsonString();
            Object jsonToObjectMapper;
            if (Utils.isValueAvailable(response))
                jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, UploadDeviceTokenResponse.class);

            if (response != null) {
                AppLog.showInfo(getClass().getSimpleName(), "GCM Response = " + response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String msg = jsonResponse.getString("msg");
                    String status = jsonResponse.getString("status");
                    if (msg.equals("SUCCESSFUL") && status.equals("SC200")) {
                        SharedPref.setGCMRegistration(true);
                    }
                    SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                    sharedPreferences.edit().putBoolean(VariableConstants.SENT_TOKEN_TO_SERVER, true).apply();
                } catch (Exception e) {
                    AppLog.showError(getClass().getSimpleName(), e.getMessage());
                }
            } else {
                AppLog.showError(getClass().getSimpleName(), "Error while registering GCM token to our server");
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
