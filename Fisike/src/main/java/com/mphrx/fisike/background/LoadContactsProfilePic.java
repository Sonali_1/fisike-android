package com.mphrx.fisike.background;

import org.json.JSONObject;

import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.services.MessengerService;

/**
 * This asynchronous task is responsible for set real name for contacts
 * 
 * @author Kailash
 */
public class LoadContactsProfilePic implements Runnable {
	private String jsonString;
	private String emailString;
	private MessengerService messengerService;
	private static final String GET_PROFILE_PIC = "/getProfilePicture";

	public LoadContactsProfilePic(MessengerService messengerService, String emailString) {
		this.messengerService = messengerService;
		this.emailString = emailString;
	}

	@Override
	public void run() {
		runApi();
		messengerService.updateContactProfilePic(jsonString);
	}

	private void runApi() {
		try {
			final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + GET_PROFILE_PIC;

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", emailString);

			APIManager jsonManager = APIManager.getInstance();
			jsonString = jsonManager.executeHttpPost(url, "", jsonObject, messengerService.getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getEmailString() {
		return emailString;
	}
}