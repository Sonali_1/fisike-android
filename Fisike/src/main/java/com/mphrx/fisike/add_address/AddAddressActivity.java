package com.mphrx.fisike.add_address;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.CountryListAdapter;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.AddressTypeEnum;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.update_user_profile.UpdateProfileBaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class AddAddressActivity extends UpdateProfileBaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;

    private CustomFontButton tvAddressType;
    private CustomFontEditTextView etPostalCode, etCity, etState, etAdd1, etAdd2, etCountry;
    private int index = -1;
    private FrameLayout frameLayout;
    private Spinner spinner_country;
    private CountryListAdapter adapter_country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_add_address, frameLayout);
        findView();
        initView();

    }

    private void findView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        tvAddressType = (CustomFontButton) findViewById(R.id.tv_address_type);
        etPostalCode = (CustomFontEditTextView) findViewById(R.id.et_postal_code);
        etCity = (CustomFontEditTextView) findViewById(R.id.et_city);
        etState = (CustomFontEditTextView) findViewById(R.id.et_state);
        etAdd1 = (CustomFontEditTextView) findViewById(R.id.et_add_1);
        etAdd2 = (CustomFontEditTextView) findViewById(R.id.et_add_2);
        etPostalCode.setAlphaNumericKeyListener();
        etCountry = (CustomFontEditTextView) findViewById(R.id.et_country);
        spinner_country = (Spinner) findViewById(R.id.spinner_country);
    }

    private void initView() {
        etCountry.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.title_activity_add_address));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(VariableConstants.ADDRESS)) {
            Address address = (Address) getIntent().getExtras().get(VariableConstants.ADDRESS);
            index = getIntent().getExtras().getInt(VariableConstants.ADDRESS_INDEX);
            String addressTpe = AddressTypeEnum.getDisplayedValuefromCode(address.getUseCode());
            if (address.getUseCode() != null && !address.getUseCode().equals(getResources().getString(R.string.txt_null))
                    && Utils.isValueAvailable(addressTpe)) {
                tvAddressType.setText(addressTpe);
            } else {
                tvAddressType.setText(getResources().getString(R.string.type_addres));
            }

            etPostalCode.setText(((address.getPostalCode() != null && !address.getPostalCode().equals(getResources().getString(R.string.txt_null)) && !address.getPostalCode().equals("")) ? address.getPostalCode() : ""));
            etCity.setText(((address.getCity() != null && !address.getCity().equals(getResources().getString(R.string.txt_null)) && !address.getCity().equals("")) ? address.getCity() : ""));
            etState.setText(((address.getState() != null && !address.getState().equals(getResources().getString(R.string.txt_null)) && !address.getState().equals("")) ? address.getState() : ""));
            etCountry.setText((address.getCountry() != null && !address.getCountry().equals(getResources().getString(R.string.txt_null)) ? address.getCountry() : ""));
            if (address.getLine() != null && address.getLine().size() > 0) {
                etAdd1.setText(address.getLine().get(0));
                if (address.getLine().size() > 1)
                    etAdd2.setText(address.getLine().get(1));
            }
        }

        final List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        int selectedcountry = 0, i = 0;
        for (String country : getResources().getStringArray(R.array.CountryCodes)) {
            String[] country_arr = country.split(",");
            countrylist.add(country_arr[2]);

            if (country_arr[2].equalsIgnoreCase(etCountry.getText())) {
                selectedcountry = i;
            }
            i++;
        }
        countrylist.add(0, getResources().getString(R.string.Select_Country));
        adapter_country = new CountryListAdapter(AddAddressActivity.this, countrylist, "dusky_blue");
        spinner_country.setAdapter(adapter_country);
        if (!etCountry.getText().equals("")) {
            spinner_country.setSelection(++selectedcountry);
        }

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (countrylist.get(position).equals(getResources().getString(R.string.Select_Country))) {
                    etCountry.setText("");
                } else {
                    etCountry.setText(countrylist.get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (spinner_country.getSelectedItem().equals(getResources().getString(R.string.Select_Country))) {
                    Toast.makeText(AddAddressActivity.this, getResources().getString(R.string.please_select_your_country), Toast.LENGTH_SHORT).show();
                } else {
                    saveAddress();
                }
                break;
            case R.id.tv_address_type:
                showPopupMenu(R.menu.menu_address_type, R.id.tv_address_type, tvAddressType);
                break;
            case R.id.et_country:
                Utils.hideKeyboard(AddAddressActivity.this);
        }
    }


    private void saveAddress() {
        ArrayList<Address> addressUserMos = userMO.getAddressUserMos();
        if (addressUserMos == null) {
            addressUserMos = new ArrayList<>();
        }
        addAddressFields(addressUserMos, index < 0 ? addressUserMos.size() : index, tvAddressType, etPostalCode, etCity, etState, etAdd1, etAdd2, etCountry);
    }

    public void showPopupMenu(final int menuId, int anchorViewId, final CustomFontButton txtView) {

        Utils.hideKeyboard(this, txtView);

        final PopupMenu popup = new PopupMenu(AddAddressActivity.this, txtView);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setText(item.getTitle().equals(getResources().getString(R.string.conditional_none)) ? getResources().getString(R.string.type_addres) : item.getTitle());
                popup.dismiss();
                return true;
            }
        });

        popup.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateProfileResponse response) {
        if (response.getTransactionId() != super.getTransactionId())
            return;
        super.updatePhysicianAddress(response);
    }
}
