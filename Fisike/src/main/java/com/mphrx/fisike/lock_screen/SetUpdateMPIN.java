package com.mphrx.fisike.lock_screen;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.EditTextSplitView;

import java.util.concurrent.TimeUnit;

import appointment.utils.CommonControls;

public class SetUpdateMPIN extends BaseActivity {


    private Toolbar mToolbar;
    private ImageButton btnBack;
    private static CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    CustomFontTextView tvTextSetMpin, tvDeviceSpecificmPIN;
    CustomFontEditTextView etPasssword;
    EditTextSplitView etOTP;
    CustomFontButton btnDone;
    String title;
    private final int SET_MPIN = 0;
    private final int ENTER_OLD_MPIN = 1;
    private final int ENTER_NEW_MPIN = 2;
    private final int REMOVE_MPIN = 3;
    private int ACTIVE_MODE;
    UserMO user;
    String storedOldPin, userEnteredOldPin, password;
    UserDBAdapter adapter;
    public static String ISREMOVEMPIN;
    private boolean isRemoveMpin;
    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_create_mpin, frameLayout);
        //setContentView(R.layout.activity_create_mpin);
        findView();
        initView();
        bindView();
    }

    private void findView() {

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        tvTextSetMpin = (CustomFontTextView) findViewById(R.id.tv_text_set_mpin);
        etOTP = (EditTextSplitView) findViewById(R.id.etOTP);
        btnDone = (CustomFontButton) findViewById(R.id.btn_done);
        etPasssword = (CustomFontEditTextView) findViewById(R.id.etPasssword);
        tvDeviceSpecificmPIN = (CustomFontTextView) findViewById(R.id.tv_device_specific_mpin);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        user = SettingManager.getInstance().getUserMO();
        storedOldPin = user.getmPIN();
        adapter = UserDBAdapter.getInstance(this);
        etPasssword.setTransformationMethod(new PasswordTransformationMethod());
        etPasssword.setPasswordKeyListener();
        etOTP.setActivity(SetUpdateMPIN.this);
        if (getIntent().getBooleanExtra(ISREMOVEMPIN, false))
            isRemoveMpin = true;

        if (user.getmPIN() == null || user.getmPIN().equals(""))
            showViewMode(SET_MPIN);
        else if (isRemoveMpin)
            showViewMode(REMOVE_MPIN);
        else
            showViewMode(ENTER_OLD_MPIN);
    }

    private void updateConfigMo(boolean value) {
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(value);
        ConfigManager.getInstance().updateConfig(config);
    }

    private void bindView() {
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNext();
            }
        });
    }


    private void launchNext() {
        if (ACTIVE_MODE == SET_MPIN)
            setMPIN(etOTP.getValue(), getResources().getString(R.string.mpin_added_success));
        else if (ACTIVE_MODE == ENTER_OLD_MPIN)
            setOLDMPIN(etOTP.getValue());

        else if (ACTIVE_MODE == ENTER_NEW_MPIN)
            updateMPIN(etOTP.getValue());

        else if (ACTIVE_MODE == REMOVE_MPIN)
            removeMPIN(etPasssword.getText().toString().trim());

    }

    private void removeMPIN(String password) {
        if (password.equals(""))
            showToast(getResources().getString(R.string.remove_mpin_blank_password));
        else if (!password.equals(user.getPassword()))
            showToast(getResources().getString(R.string.remove_mpin_incorrect_password));
        else {
            adapter.updateMPIN(null, user);
            updateConfigMo(false);
            if (mBound) {
                if (!BuildConfig.isPasswordPinLockEnabled)
                    mService.stopPinTimer();
            }
            showToast(getResources().getString(R.string.remove_mpin_success));
            finish();
        }
    }

    private void updateMPIN(String mPIN) {
        if (mPIN.trim().equals(""))
            showToast(getResources().getString(R.string.blank_newmPIN));
        else if (mPIN.trim().length() != 4)
            showToast(getResources().getString(R.string.new_mpin_length_less_than_4));
        else if (mPIN.trim().equals(storedOldPin))
            showToast(getResources().getString(R.string.same_old_new_mpin));
        else {
            adapter.updateMPIN(mPIN, user);
            updateConfigMo(true);
            if (mBound) {
                mService.startPinScreenTimer();
            }
            showToast(getResources().getString(R.string.mpin_updated_successfully));
            this.finish();
        }
    }

    private void setOLDMPIN(String mPIN) {
        if (mPIN.trim().equals(""))
            showToast(getResources().getString(R.string.blank_old_mpin));
        else if (mPIN.trim().length() != 4)
            showToast(getResources().getString(R.string.old_mpin_length_less_than_4));
        else if (!mPIN.trim().equals(storedOldPin))
            showToast(getResources().getString(R.string.old_mpin_incorrect));
        else {
            userEnteredOldPin = mPIN;
            showViewMode(ENTER_NEW_MPIN);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void setMPIN(String mPIN, String successMsg) {
        if (mPIN.trim().equals(""))
            showToast(getResources().getString(R.string.blank_mpin));
        else if (mPIN.trim().length() != 4)
            showToast(getResources().getString(R.string.mpin_length_less_than_4));
        else {
            adapter.updateMPIN(mPIN, user);
            updateConfigMo(true);
            if (mBound) {
                mService.startPinScreenTimer();
            }
            showToast(successMsg);
            this.finish();

        }

    }

    public void setToolbarTitleText(String title) {
        toolbar_title.setText(title);
    }


    private void showViewMode(int mode) {
        etPasssword.setVisibility(View.GONE);
        switch (mode) {
            case SET_MPIN:
                tvTextSetMpin.setText(R.string.create_4_digit_pin);
                title = getResources().getString(R.string.add_mpin);
                etOTP.clearEditText();
                tvDeviceSpecificmPIN.setVisibility(View.VISIBLE);
                ACTIVE_MODE = SET_MPIN;
                break;
            case ENTER_OLD_MPIN:
                title = getResources().getString(R.string.change_mpin);
                tvTextSetMpin.setText(R.string.enter_old_4_digit_pin);
                etOTP.clearEditText();
                btnDone.setText(getString(R.string.next));
                ACTIVE_MODE = ENTER_OLD_MPIN;
                tvDeviceSpecificmPIN.setVisibility(View.VISIBLE);
                break;
            case ENTER_NEW_MPIN:
                title = getResources().getString(R.string.change_mpin);
                tvTextSetMpin.setText(R.string.create_4_digit_pin);
                etOTP.clearEditText();
                ACTIVE_MODE = ENTER_NEW_MPIN;
                tvDeviceSpecificmPIN.setVisibility(View.VISIBLE);
                break;
            case REMOVE_MPIN:
                title = getResources().getString(R.string.remove_mpin);
                tvTextSetMpin.setText(R.string.enter_your_password);
                etPasssword.setVisibility(View.VISIBLE);
                etOTP.setVisibility(View.GONE);
                tvDeviceSpecificmPIN.setVisibility(View.GONE);
                ACTIVE_MODE = REMOVE_MPIN;
                break;
        }
        setToolbarTitleText(title);
    }

    private void handleBack() {

        if (ACTIVE_MODE == SET_MPIN || ACTIVE_MODE == ENTER_OLD_MPIN || ACTIVE_MODE == REMOVE_MPIN)
            finish();
        else if (ACTIVE_MODE == ENTER_NEW_MPIN)
            showViewMode(ENTER_OLD_MPIN);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        CommonControls.closeKeyBoard(this);
        handleBack();
    }
}
