package com.mphrx.fisike.lock_screen;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.EditTextSplitView;
import com.mphrx.fisike_physician.activity.ContactActivity;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.views.CircleImageView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;

import static android.util.Log.d;

public class LockScreenActivity extends AppCompatActivity {


    CustomFontTextView et_phone_number, tv_enterpin;
    CircleImageView civUserProfilePicture;
    CustomFontButton btnDone, btn_signout;
    EditTextSplitView pin_lock;
    UserMO userMo;
    int incorrectAttempts;
    protected MessengerService mService;
    protected boolean mBound;
    CustomEditTextWhite etPass;
    private final int MPIN_FLOW = 0;
    private final int PASSWORD_FLOW = 1;
    private int ACTIVE_MODE;
    private ConfigMO configMO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_lock_screen);
        findView();
        initView();
    }

    /**
     * This is the service connection that is created to the MessengerService
     */
    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            d("Fisike", "com.mphrx.fisike.onServiceConnected : Bind Service");
            MessengerService.MessengerBinder binder = (MessengerService.MessengerBinder) service;
            mService = binder.getService();
            mBound = true;
            if (mService != null)
                mService.stopPinTimer();
        }

        public void onServiceDisconnected(ComponentName className) {
            d("Fisike", "ListPersonActivity - Service Unbound");
            mBound = false;
        }
    };

    /**
     * This method disconnects the messenger service from this activity
     */
    private void unbindMessengerService() {
        try {
            if (mBound) {
                unbindService(mConnection);
                mBound = false;
            }
        } catch (Exception e) {

        }
    }


    /**
     * This method binds the messenger service to this activity
     */
    public void bindMessengerService() {
        if (!mBound) {
            d("Fisike", "com.mphrx.fisike.bindMessengerService");
            Intent intent = new Intent(this, MyApplication.getMessengerServiceClass());
            if (!checkIfMessengerServiceIsRunning()) {
                MyApplication.getAppContext().startService(intent);
            }
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    /**
     * This method checks if the Messenger Service is running or not
     *
     * @return
     */
    public boolean checkIfMessengerServiceIsRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals(MyApplication.getMessengerServiceClass().getName())) {
                return true;
            }
        }
        return false;
    }

    private void findView() {
        et_phone_number = (CustomFontTextView) findViewById(R.id.et_phone_number);
        civUserProfilePicture = (CircleImageView) findViewById(R.id.civ_user_profile_pic);
        btnDone = (CustomFontButton) findViewById(R.id.btn_done);
        pin_lock = (EditTextSplitView) findViewById(R.id.pin_lock);
        etPass = (CustomEditTextWhite) findViewById(R.id.et_password);
        etPass.setTransformationMethod(new PasswordTransformationMethod());
        etPass.setPasswordKeyListener();
        tv_enterpin = (CustomFontTextView) findViewById(R.id.tv_enterpin);
        btn_signout = (CustomFontButton) findViewById(R.id.btn_signout);
    }

    private void initView() {
        incorrectAttempts = 3;
        userMo = SettingManager.getInstance().getUserMO();
        if (userMo.getmPIN() == null && BuildConfig.isPasswordPinLockEnabled)
            showView(PASSWORD_FLOW);
        else if (userMo.getmPIN() != null)
            showView(MPIN_FLOW);
        else
            AppLog.showInfo("MPIN", "mpin id null and password enable pin is false");

        setProfilePicture();
        if (!mBound) {
            bindMessengerService();
        }
    }

    private void showView(int mode) {
        btn_signout.setVisibility(View.GONE);
        et_phone_number.setText(Utils.getUserName());
        switch (mode) {
            case MPIN_FLOW:
                tv_enterpin.setText(R.string.enter_4_digit_pin);
                pin_lock.setVisibility(View.VISIBLE);
                etPass.setVisibility(View.GONE);
                ACTIVE_MODE = MPIN_FLOW;
                break;
            case PASSWORD_FLOW:
                tv_enterpin.setText(R.string.enter_your_password);
                pin_lock.setVisibility(View.GONE);
                etPass.setVisibility(View.VISIBLE);
                ACTIVE_MODE = PASSWORD_FLOW;
                break;
        }
    }

    private void setProfilePicture() {
        Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());
        if (bitmap != null) {
            civUserProfilePicture.setImageBitmap(bitmap);
        } else {
            Bitmap ProfilePicIcon = BitmapFactory.decodeResource(getResources(), R.drawable.default_profile_pic);
            civUserProfilePicture.setImageBitmap(ProfilePicIcon);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_done:
                hideKeyPad();
                openLock();
                if (incorrectAttempts <= 0) {
                    if (ACTIVE_MODE == MPIN_FLOW)
                        showToast(getString(R.string.incorrect_mpin_3_attempt));

                    else
                        showToast(getString(R.string.incorrect_password_3_attempt));

                    SharedPref.setLastInteractionTime(System.currentTimeMillis());
                    mService.logoutUser(LockScreenActivity.this);
                } else {
                    if (incorrectAttempts <= 2)
                        btn_signout.setVisibility(View.VISIBLE);
                    else
                        btn_signout.setVisibility(View.GONE);
                }

                break;
            case R.id.btn_signout:
                if (ACTIVE_MODE == MPIN_FLOW)
                    showDialogToResetMpin();
                else
                    mService.logoutUser(this);
        }
    }

    private void hideKeyPad() {
        InputMethodManager inputManager =
                (InputMethodManager) LockScreenActivity.this.
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void updateConfigMo(boolean value) {
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(value);
        ConfigManager.getInstance().updateConfig(config);
    }


    private void showDialogToResetMpin() {

        DialogUtils.showAlertDialog(this, getString(R.string.reset_mpin_sign_out_title), getString(R.string.reset_mpin_signout_msg), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    UserDBAdapter.getInstance(LockScreenActivity.this).updateMPIN(null, userMo);
                    updateConfigMo(false);
                    mService.logoutUser(LockScreenActivity.this);
                } else if (which == Dialog.BUTTON_NEGATIVE) {

                }
            }
        });
    }


    @Override
    public void onBackPressed() {

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void openLockThroughPIN() {

        String lock = pin_lock.getValue();
        if (lock.trim().equals(""))
            showToast(getResources().getString(R.string.enter_pin));
        else if (lock.trim().length() < 4)
            showToast(getResources().getString(R.string.mpin_length_less_than_4));

        else if (lock.equals(userMo.getmPIN())) {
            backFromPINScreen();
        } else {
            incorrectAttempts = incorrectAttempts - 1;
            pin_lock.clearEditText();
            if (incorrectAttempts != 0) {
                String message = getString(R.string.incorrect_mpin_attempts, numberToWordConversion(incorrectAttempts), attempt(incorrectAttempts));
                showToast(message);
            }


        }
    }

    private String numberToWordConversion(int incorrectAttempts) {
        String text = "";
        switch (incorrectAttempts) {
            case 1:
                text = getResources().getString(R.string.one);
                break;
            case 2:
                text = getResources().getString(R.string.two);
                break;
        }
        return text;
    }

    private void backFromPINScreen() {
        SharedPref.setLastInteractionTime(System.currentTimeMillis());
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            //// This is last activity
            launchHomeScreen();
        } else {
            this.finish();
        }
        mService.startPinScreenTimer();
    }

    private void openLockThroughPassword() {

        String pass = etPass.getText().toString().trim();
        if (pass.trim().equals(""))
            showToast(getString(R.string.remove_mpin_blank_password));
        else if (pass.equals(userMo.getPassword())) {
            backFromPINScreen();
        } else {
            incorrectAttempts = incorrectAttempts - 1;
            if (incorrectAttempts != 0) {
                String message = getString(R.string.incorrect_password_attempts, numberToWordConversion(incorrectAttempts), attempt(incorrectAttempts));
                etPass.setText("");
                showToast(message);
            }
        }
    }

    private String attempt(int incorrectAttempts) {
        if (incorrectAttempts <= 1)
            return getResources().getString(R.string.attempt);
        else
            return getResources().getString(R.string.attempts);
    }

    private void openLock() {
        if (ACTIVE_MODE == MPIN_FLOW)
            openLockThroughPIN();
        else
            openLockThroughPassword();
    }

    private void launchHomeScreen() {
        Intent i;
        if (BuildConfig.isPatientApp) {
            i = new Intent(this, HomeActivity.class);
            i.putExtra(VariableConstants.START_INSIDE_ACTIVITY, true);
        } else if (BuildConfig.isPhysicianApp && !BuildConfig.isSearchPatient) {
            i = new Intent(this, ContactActivity.class);
        } else if (BuildConfig.isPhysicianApp && BuildConfig.isSearchPatient) {
            i = new Intent(this, SearchPatientFilterActivity.class);
        } else { // TODO: need to launch minerva home screen
            i = new Intent(this, HomeActivity.class);
            //i.putExtra(VariableConstants.START_INSIDE_ACTIVITY, true);//changed by Aastha for side drawe FIS-6382
        }
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        unbindMessengerService();
        super.onDestroy();
    }
}
