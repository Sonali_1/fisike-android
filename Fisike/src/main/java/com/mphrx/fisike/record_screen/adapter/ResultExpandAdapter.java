package com.mphrx.fisike.record_screen.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.record_screen.view_holder.DiagnosticOrderViewHolder;
import com.mphrx.fisike.record_screen.view_holder.DiagnosticReportViewHolder;
import com.mphrx.fisike.view.ExpandableRecyclerView.ExpandableRecyclerViewAdapter;
import com.mphrx.fisike.view.ExpandableRecyclerView.models.ExpandableGroup;

import java.util.List;

/**
 * Created by kailashkhurana on 24/07/17.
 */

public class ResultExpandAdapter extends ExpandableRecyclerViewAdapter<DiagnosticOrderViewHolder, DiagnosticReportViewHolder> {
    public int totalCount;
    private DiagnosticOrderViewHolder diagnosticOrderViewHolder;
    private DiagnosticOrderViewHolder.DownloadListner listener;

    public ResultExpandAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public DiagnosticOrderViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_group, parent, false);
        diagnosticOrderViewHolder = new DiagnosticOrderViewHolder(view, this);
        if (listener != null) {
            diagnosticOrderViewHolder.setListener(listener);
        }
        return diagnosticOrderViewHolder;
    }

    @Override
    public DiagnosticReportViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new DiagnosticReportViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(final DiagnosticReportViewHolder holder, int flatPosition, final ExpandableGroup group, int childIndex) {
        holder.setDiagnosticReport((OrderReportModel) group, childIndex);
        if (((OrderReportModel) group).getItems().size() - 1 == childIndex) {
            holder.layout_hide.setVisibility(View.VISIBLE);
        } else {
            holder.layout_hide.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isListRefreshing() {
        boolean listRefreshing = super.isListRefreshing();
        if (diagnosticOrderViewHolder != null) {
            diagnosticOrderViewHolder.setListRefreshing(listRefreshing);
        }
        return listRefreshing;
    }

    @Override
    public void setIsListRefreshing(boolean isListRefreshing) {
        super.setIsListRefreshing(isListRefreshing);
        isListRefreshing();
    }

    @Override
    public void onBindGroupViewHolder(DiagnosticOrderViewHolder holder, int flatPosition, ExpandableGroup group, boolean isExpanded) {
        holder.setGenreTitle(group);
        if (!isExpanded) {
            holder.layoutShow.setVisibility(View.VISIBLE);
        } else {
            holder.layoutShow.setVisibility(View.INVISIBLE);
        }
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public void setListener(DiagnosticOrderViewHolder.DownloadListner listener) {
        this.listener = listener;
        if (diagnosticOrderViewHolder != null) {
            diagnosticOrderViewHolder.setListener(listener);
        }
    }

}
