
package com.mphrx.fisike.record_screen.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.GetOrganizationWithLogoResponse;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.ObservationInterpretationEnum;
import com.mphrx.fisike.enums.ObservationStatusEnum;
import com.mphrx.fisike.enums.ReportTypeEnum;
import com.mphrx.fisike.icomoon.IconButton;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.adapter.ObservationAdapter;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.record_screen.request.DiagnosticObservationSearchRequest;
import com.mphrx.fisike.record_screen.request.GetOrganizationWithLogoRequest;
import com.mphrx.fisike.record_screen.response.DiagnosticObservation;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.NotifyingScrollView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;

public class ObservationActivity extends BaseActivity implements AdapterView.OnItemClickListener, ObservationAdapter.clickListener,
        NotifyingScrollView.OnScrollChangedListener {


    private final static String RECORD_ID = "RECORD_ID";
    private final static String RECORD_HEADER = "RECORD_HEADER";
    private final static String RECORD_STATUS = "RECORD_STATUS";
    private static final String TITLE = "TITLE";
    private static final String PHYSICIAN_NAME = "PHYSICIAN_NAME";
    private static final String PERFORMING_LOCATION_NAME = "PERFORMING_LOCATION_NAME";
    private static final String REPORT_FORMAT = "REPORT_FORMAT";
    private static final String PATIENT_ID = "PATIENT_ID";
    private static final String ORDER_ID = "ORDER_ID";
    private static final int PDF_VIEW_RESULT = 100;
    private String seperator_string = " : ";

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title, tvToolbarSubtitle;
    private IconTextView bt_toolbar_right;
    private Drawable mActionBarBackgroundDrawable;
    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener;
    private Drawable drawable;
    private CardView temp_margin_view;
    private CardView cardview_patient_info;


    private RecyclerView mListView;
    private CustomFontTextView tvNoResult;
    private CustomFontTextView tv_more_less;
    private String mRecordId, reportStatus;
    private String mOrganisationId;
    private CustomFontTextView tvReportDetail, tv_labinfo_name, tv_pat_name, tv_age, /*tv_dob,*/
            tv_gender, tv_height, tv_weight, tv_report_date, tv_reffered_by_name, tv_refferd_by_label;
    private CustomFontTextView edRemarks;
    private LinearLayout ll_layoutFoter, ll_container_pat_name, ll_report, ll_refferd_by, ll_patient_information_header, llPatientInfoContainer;
    RelativeLayout llLabInfoContainer;
    IconButton btn_know_more;
    ProgressBar mLogoDownloadProgressBar;
    private ImageView img_logo;
    private String physicianName;
    private String fileURL;
    private int fileType;
    private int documentRefId;
    private String vitalText;
    private RecyclerView.LayoutManager observationLayoutManager;
    private ObservationAdapter adapter;
    private CustomFontTextView tv_report_date_label;
    private CollapsingToolbarLayout collapsingToolBarLayout;
    private AppBarLayout appbar;
    private LinearLayout ll_image_holder;
    private boolean isLogoDownLoading = false;
    private boolean isToLoadLogoFromServer = false;
    private CustomFontButton mbtnViewReport;
    private FrameLayout frameLayout;
    private String performingLocationName;
    private String report_status;
    private CustomFontTextView tvReportStatus, tvTestMethod;
    private LinearLayout llReportStatus, llTestMethod;
    private CardView cardLayoutReportStatusAndTestMethod;
    private LinearLayout llPatientAge, llPatientGender, llPatientHeight, llPatientWeight, llPatientId, llPerformingLocation;
    private CustomFontTextView tvPatientId, tvPerformingLocation;
    private View lview;
    String reportFormat;
    private String patientId;
    private boolean isToShowPatientData = false;
    private LinearLayout ll_patient_demographic;
    private String patientPk;
    private String orderId;
    private CardView cardReportList;


    public static Intent newIntent(Context context, String recordId, String title, String organisationId,
                                   String physicianName, String status, String performingLocationName,
                                   String reportFormat, String patientId, String orderId) {
        Intent intent = new Intent(context, ObservationActivity.class);
        intent.putExtra(RECORD_ID, recordId);
        intent.putExtra(TITLE, title);
        intent.putExtra(VariableConstants.ORGANISATION_ID, organisationId);
        intent.putExtra(RECORD_STATUS, status);
        intent.putExtra(PHYSICIAN_NAME, physicianName);
        intent.putExtra(PERFORMING_LOCATION_NAME, performingLocationName);
        intent.putExtra(REPORT_FORMAT, reportFormat);
        intent.putExtra(PATIENT_ID, patientId);
        intent.putExtra(ORDER_ID, orderId);
        return intent;
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mListView = (RecyclerView) frameLayout.findViewById(R.id.list);
        tvNoResult = (CustomFontTextView) frameLayout.findViewById(R.id.tv_no_result);
        LayoutInflater mInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        temp_margin_view = (CardView) findViewById(R.id.temp_margin_view);
        mRecordId = getIntent().getStringExtra(RECORD_ID);
        reportStatus = getIntent().getStringExtra(RECORD_STATUS);
        vitalText = getIntent().getStringExtra(TITLE);
        physicianName = getIntent().getStringExtra(PHYSICIAN_NAME);
        performingLocationName = getIntent().getStringExtra(PERFORMING_LOCATION_NAME);
        patientId = getIntent().getStringExtra(PATIENT_ID);
        orderId = getIntent().getStringExtra(ORDER_ID);
        reportFormat = getIntent().getStringExtra(REPORT_FORMAT);
        mRecordId = getIntent().getStringExtra(RECORD_ID);
        reportStatus = getIntent().getStringExtra(RECORD_STATUS);
        mOrganisationId = getIntent().getStringExtra(VariableConstants.ORGANISATION_ID);
        tvReportDetail = (CustomFontTextView) findViewById(R.id.tv_reportType);
        tvReportDetail.setText(getResources().getString(R.string.observation_results));
        edRemarks = (CustomFontTextView) findViewById(R.id.remarks);
        ll_layoutFoter = (LinearLayout) findViewById(R.id.layoutFoter);
        img_logo = (ImageView) findViewById(R.id.img_logo);
        tv_labinfo_name = (CustomFontTextView) findViewById(R.id.tv_labinfo_name);
        tv_more_less = (CustomFontTextView) findViewById(R.id.tv_more_less);
        tv_pat_name = (CustomFontTextView) findViewById(R.id.tv_pat_name);
        llLabInfoContainer = (RelativeLayout) findViewById(R.id.ll_lab_info);
        llPatientInfoContainer = (LinearLayout) findViewById(R.id.ll_patient_info);
        cardview_patient_info = (CardView) findViewById(R.id.cardview_patient_info);
        ll_container_pat_name = (LinearLayout) findViewById(R.id.ll_container_pat_name);
        ll_patient_information_header = (LinearLayout) findViewById(R.id.ll_patient_information_header);
        ll_patient_demographic = (LinearLayout) findViewById(R.id.ll_patient_demographic);
        tv_age = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_age);
        // tv_dob = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_dob);
        tv_gender = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_gender);
        tv_height = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_height);
        tv_weight = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_weight);
        tv_report_date = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_report_date);
        tv_refferd_by_label = (CustomFontTextView) findViewById(R.id.tv_refferd_by_label);
        //tv_pat_age_gender = (CustomFontTextView) findViewById(R.id.tv_pat_age_gender);
        tv_reffered_by_name = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_reffered_by_name);
        mbtnViewReport = (CustomFontButton) findViewById(R.id.btnViewReport);
        tv_report_date_label = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_report_date_label);
        ll_report = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_report);
        ll_refferd_by = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_refferd_by);
        btn_know_more = (IconButton) findViewById(R.id.btn_know_more);
        mLogoDownloadProgressBar = (ProgressBar) findViewById(R.id.logo_progress_bar);
        ll_image_holder = (LinearLayout) findViewById(R.id.ll_image_holder);

        lview = findViewById(R.id.view);
        tvReportStatus = (CustomFontTextView) findViewById(R.id.tv_report_status_value);
        llReportStatus = (LinearLayout) findViewById(R.id.ll_report_status);
        tvTestMethod = (CustomFontTextView) findViewById(R.id.tv_test_method_value);
        llTestMethod = (LinearLayout) findViewById(R.id.ll_text_method);
        cardLayoutReportStatusAndTestMethod = (CardView) findViewById(R.id.card_status);
        llPatientAge = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_age);
        llPatientGender = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_patient_gender);
        llPatientHeight = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_height);
        llPatientWeight = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_weight);
        llPatientId = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_patient_id);
        llPerformingLocation = (LinearLayout) ll_patient_information_header.findViewById(R.id.ll_performing_location);
        tvPatientId = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_patient_id_value);
        tvPerformingLocation = (CustomFontTextView) ll_patient_information_header.findViewById(R.id.tv_performing_location);
        cardReportList = (CardView) findViewById(R.id.temp_margin_view);
        showCollapsedView();
        if (SharedPref.getLogoDownloadTime() == 0) {
            isToLoadLogoFromServer = true;
        } else {
            long current = System.currentTimeMillis();
            if ((current - SharedPref.getLogoDownloadTime()) / 1000 >= 24 * 60 * 60)
                isToLoadLogoFromServer = true;
            else
                isToLoadLogoFromServer = false;

        }

        if (vitalText != null && (vitalText.equalsIgnoreCase("Glucose") || vitalText.equalsIgnoreCase("Bmi") ||
                vitalText.equalsIgnoreCase("Blood Pressure")) || mOrganisationId == null ||
                mOrganisationId.equals("") || mOrganisationId.equalsIgnoreCase(getResources().getString(R.string.txt_null))) {

            llLabInfoContainer.setVisibility(View.GONE);
            cardview_patient_info.setVisibility(View.GONE);
        } else {
            loadLogoInfo();
        }

    }

    private void loadLogoInfo() {
        llLabInfoContainer.setVisibility(View.VISIBLE);

        if (logoSavedInMemory() && !isToLoadLogoFromServer) {
            ll_image_holder.setVisibility(View.VISIBLE);
            img_logo.setImageBitmap(Utils.scaleBitmapAndKeepRation(loadLogo(this), (int) Utils.convertDpToPixel(43f, this), (int) Utils.convertDpToPixel(180f, this)));
            mLogoDownloadProgressBar.setVisibility(View.GONE);
        } else {
            isLogoDownLoading = true;
            ThreadManager.getDefaultExecutorService()
                    .submit(new GetOrganizationWithLogoRequest(getTransactionId(), mOrganisationId));
            mLogoDownloadProgressBar.setVisibility(View.GONE);

        }
    }

    private void setPatientData(DiagnosticObservation diagnosticObservation) {
        tv_pat_name.setText(diagnosticObservation.getPatientName());
        String age = "";
        String gender = null;
        if (diagnosticObservation.getDateofBirth() != null && !diagnosticObservation.getDateofBirth().equals(getResources().getString(R.string.txt_null))) {
            setDOB(DateTimeUtil.getFormattedDateWithoutTime(diagnosticObservation.getDateofBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            try {
                age = Utils.calculateAgeAsPerConstraints(diagnosticObservation.getDateofBirth(), null, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, false);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            setAge("" + age);
        } else {
            setDOB("N/A");
            setAge("N/A");
            age = "";
        }
        if (diagnosticObservation.getGender() != null) {
            gender = ((diagnosticObservation.getGender().toUpperCase().startsWith(getString(R.string.gender_male_initial)))
                    ? getString(R.string.gender_male) : ((diagnosticObservation.getGender().toUpperCase().
                    startsWith(getString(R.string.gender_female_initial))) ? getString(R.string.gender_male) : getString(R.string.gender_other)));
            setGender(gender);
        } else {
            setGender("N/A");
        }

        setHeight("N/A");
        setWeight("N/A");
        if (diagnosticObservation.getReportDate() != null) {
            String destinationFormat = "";
            if (DateFormat.is24HourFormat(ObservationActivity.this)) {
                destinationFormat = DateTimeUtil.destinationDateFormatWithTime24hr;
            } else {
                destinationFormat = DateTimeUtil.destinationDateFormatWithTimeOld;
            }
            setReportDate(DateTimeUtil.convertSourceDestinationDate(diagnosticObservation.getReportDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationFormat));
        } else {
            ll_report.setVisibility(View.GONE);
        }

        String patientAge = (age) + (gender != null ? (!age.equals("") ? ", " : "") + gender.toLowerCase() : "");

        if (physicianName != null && !physicianName.trim().equals("") && !physicianName.trim().equals(getResources().getString(R.string.txt_null))) {
            tv_reffered_by_name.setText(physicianName);
            ll_refferd_by.setVisibility(View.VISIBLE);
        } else {

            ll_refferd_by.setVisibility(View.GONE);
        }

        if (!isToShowPatientData)
            ll_patient_demographic.setVisibility(View.GONE);

        setPatientId(diagnosticObservation.getPatientId());
        setPerformingLocation(performingLocationName);

    }

    private void showCollapsedView() {
        tv_more_less.setText(getResources().getString(R.string.txt_more));
        ll_patient_information_header.setVisibility(View.GONE);
        btn_know_more.setRotation(0f);
    }

    private void showExpandedView() {
        tv_more_less.setText(getResources().getString(R.string.txt_less));
        ll_patient_information_header.setVisibility(View.VISIBLE);
        btn_know_more.setRotation(180f);
    }


    private int fetchAccentColor() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = this.obtainStyledAttributes(typedValue.data, new int[]{R.attr.colorPrimary});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_observation, frameLayout);
        vitalText = getIntent().getStringExtra(TITLE);
        mOrganisationId = getIntent().getStringExtra(VariableConstants.ORGANISATION_ID);
        if (vitalText != null && (vitalText.equalsIgnoreCase("Glucose") || vitalText.equalsIgnoreCase("Bmi") ||
                vitalText.equalsIgnoreCase("Blood Pressure")) || mOrganisationId == null ||
                mOrganisationId.equals("") || mOrganisationId.equalsIgnoreCase(getResources().getString(R.string.txt_null))) {
        } else {
            mOnScrollChangedListener = this;
            mActionBarBackgroundDrawable = new ColorDrawable(getResources().getColor(R.color.action_bar_bg));
            mActionBarBackgroundDrawable.setAlpha(0);
        }
        findViews();

        ((NotifyingScrollView) findViewById(R.id.scroll_view)).setOnScrollChangedListener(mOnScrollChangedListener);

        Drawable.Callback mDrawableCallback = new Drawable.Callback() {
            @Override
            public void invalidateDrawable(Drawable who) {
                getActionBar().setBackgroundDrawable(who);
            }

            @Override
            public void scheduleDrawable(Drawable who, Runnable what, long when) {
            }

            @Override
            public void unscheduleDrawable(Drawable who, Runnable what) {
            }
        };
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }

        if (mToolbar != null) {
            btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
            btnBack.setVisibility(View.GONE);
            toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
            tvToolbarSubtitle = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_sub_title);
            tvToolbarSubtitle.setVisibility(View.VISIBLE);
            tvToolbarSubtitle.setText((vitalText != null && !vitalText.equals(getResources().getString(R.string.txt_null))) ? vitalText : "");
            bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
            bt_toolbar_right.setVisibility(View.GONE);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar_title.setText(getResources().getString(R.string.txt_report_detail));

            if (vitalText != null && (vitalText.equalsIgnoreCase("Glucose") || vitalText.equalsIgnoreCase("Bmi") ||
                    vitalText.equalsIgnoreCase("Blood Pressure")) || mOrganisationId == null ||
                    mOrganisationId.equals("") || mOrganisationId.equalsIgnoreCase(getResources().getString(R.string.txt_null))) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
                } else
                    mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
                TypedValue tv = new TypedValue();
                int toolbar_height = 0;
                if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                    toolbar_height = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
                }

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) temp_margin_view.getLayoutParams();
                params.setMargins(0, toolbar_height, 0, 5); //substitute parameters for left, top, right, bottom
                temp_margin_view.setLayoutParams(params);
            } else {
                getSupportActionBar().setBackgroundDrawable(mActionBarBackgroundDrawable);

            }

        }


        showProgressDialog();
        ThreadManager.getDefaultExecutorService().submit(
                new DiagnosticObservationSearchRequest(mRecordId, getTransactionId(), ReportTypeEnum.LAB, patientId));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.observation_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //view report option only for Final and Corrected Status
        if ((fileURL == null && BuildConfig.isFisike) || !(Utils.isValueAvailable(reportStatus)
                && (reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("F").getCompleteCode())
                || reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("C").getCompleteCode())
                || (BuildConfig.isPhysicianApp &&
                reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("P").getCompleteCode()))
        ))) {
            MenuItem item = menu.findItem(R.id.view_report);
            menu.removeItem(item.getItemId());
        }
        return true;
    }

    private void fileDownload() {
        String payload = "{\"constraints\":{\"diagnosticReportId\":\"" + documentRefId + "\",\"diagnosticOrderId\":\"" + orderId + "\"}}";
        if (fileType == VariableConstants.MIME_TYPE_DOC) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setType("application/msword");
            if (intent.resolveActivity(getPackageManager()) == null)

                if (Utils.isNetworkAvailable(this)) {
                    new DownloadRecordFileRequest(this, documentRefId, fileType, MphRxUrl.getReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {
                    Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                }
        } else {
            if (Utils.isNetworkAvailable(this)) {
                new DownloadRecordFileRequest(this, documentRefId, fileType, MphRxUrl.getReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public boolean
    onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.view_report:
                if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), PDF_VIEW_RESULT))
                    fileDownload();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        // ObservationActivity.this.finish();
        onBackPressed();
        return true;
    }

    @Override
    public void fetchInitialDataFromServer() {
        // nothing to do
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // nothing to do
    }

    @Subscribe
    public void onObservationReceived(DiagnosticObservation diagnosticObservation) {

        if (getTransactionId() != diagnosticObservation.getTransactionId())
            return;

        dismissProgressDialog();
        if (isLogoDownLoading)
            mLogoDownloadProgressBar.setVisibility(View.VISIBLE);
        else
            mLogoDownloadProgressBar.setVisibility(View.GONE);

        patientPk = diagnosticObservation.getPatientPk();
        setPatientData(diagnosticObservation);
        setReportStatusAndTestMethod(reportStatus, null);
        fileType = diagnosticObservation.getFileType();
        documentRefId = diagnosticObservation.getDocumentRefId();

        if (fileType == VariableConstants.MIME_TYPE_FILE || fileType == VariableConstants.MIME_TYPE_DOC) {
            fileURL = diagnosticObservation.getFileURL();
        }

        //view report option only for Final and Corrected Status
        if (Utils.isValueAvailable(reportStatus)
                && (reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("F").getObservationStatusValue())
                || reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("C").getObservationStatusValue())
                || (BuildConfig.isPhysicianApp && reportStatus.equals(ObservationStatusEnum.getObservationStatusMap().get("P").getObservationStatusValue()
        )))) {

            mbtnViewReport.setVisibility(View.VISIBLE);
            if (fileURL == null && BuildConfig.isFisike)
                mbtnViewReport.setVisibility(View.GONE);
        } else {
            mbtnViewReport.setVisibility(View.GONE);
        }

        invalidateOptionsMenu();

        try {
            if (diagnosticObservation != null && diagnosticObservation.getTitle() != null && !diagnosticObservation.getTitle().equals("")) {
                tvReportDetail.setText(getResources().getString(R.string.observation_results));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!diagnosticObservation.isSuccessful()) {
            Toast.makeText(this, diagnosticObservation.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            if (diagnosticObservation.getObservations() != null && diagnosticObservation.getObservations().size() > 0) {
                adapter = new ObservationAdapter(this, diagnosticObservation.getObservations(), vitalText, reportFormat);

                observationLayoutManager = new LinearLayoutManager(this);
                observationLayoutManager
                        .setAutoMeasureEnabled(true);
                mListView.setLayoutManager(observationLayoutManager);
                mListView.setNestedScrollingEnabled(false);
                adapter.setClickListener(this);
                tvNoResult.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
                mListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } else {
                mListView.setVisibility(View.GONE);
                tvNoResult.setVisibility(View.VISIBLE);

            } /*else if ((diagnosticObservation.getRemark() != null && (!(diagnosticObservation.getRemark().equals("null")))) && diagnosticObservation.getObservations() != null || diagnosticObservation.getObservations().size() == 0) {
                adapter = new ObservationAdapter(this, diagnosticObservation.getObservations(), vitalText);
                observationLayoutManager = new LinearLayoutManager(this);
                observationLayoutManager.setAutoMeasureEnabled(true);
                mListView.setLayoutManager(observationLayoutManager);
                mListView.setNestedScrollingEnabled(false);
                adapter.setClickListener(this);
                mListView.setAdapter(adapter);
            }*/
             /*else if (diagnosticObservation.getObservations().isEmpty() && fileURL == null && diagnosticObservation.getRemark() == null && ((diagnosticObservation.getRemark().equals("null")))) {
                Toast.makeText(this, "No observations to review right now.", Toast.LENGTH_SHORT).show();
                finish();
            }*/
            if (diagnosticObservation.getRemark() == null || (diagnosticObservation.getRemark().equals(getResources().getString(R.string.txt_null)))) {
                ll_layoutFoter.setVisibility(View.GONE);
            } else {
                if (reportStatus.equalsIgnoreCase(TextConstants.OBSERVATION_STATUS_CANCELLED)) {
                    ll_layoutFoter.setVisibility(View.GONE);
                } else {
                    edRemarks.setText(Html.fromHtml(diagnosticObservation.getRemark().toString().trim()));
                    ll_layoutFoter.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Observation observation = (Observation) parent.getAdapter().getItem(position);
        // paramId
        if (observation == null) {
            return;
        }
        String paramId = observation.getParamId();

        // patientId
        String patientId = patientPk;

        String secoundryParamId = null;
        if (observation.getName().equalsIgnoreCase("SYSTOLIC") || observation.getName().equalsIgnoreCase("DIASTOLIC")) {
            secoundryParamId = position == 0 ? ((Observation) parent.getAdapter().getItem(position + 1)).getParamId() :
                    ((Observation) parent.getAdapter().getItem(position - 1)).getParamId();
        }
        startActivity(ObservationHistoryActivity.newIntent(this,
                paramId, patientId,
                vitalText, observation.getName(), secoundryParamId, false));

    }


    private void showHidePatientHeader() {
        if (ll_patient_information_header.getVisibility() == View.GONE) {
            showExpandedView();
        } else {
            showCollapsedView();
        }
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_more_less:
                showHidePatientHeader();
                break;

            case R.id.btnViewReport:
                if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), PDF_VIEW_RESULT))
                    fileDownload();
                break;


        }
    }

    private void setAge(String age) {
        if (Utils.isValueAvailable(age) && !age.trim().equalsIgnoreCase("n/a")) {
            tv_age.setText(age);
            isToShowPatientData = true;
        } else {
            llPatientAge.setVisibility(View.GONE);
        }
    }

    private void setDOB(String dob) {
      /*  if (!dob.trim().equals("") && dob != null && !dob.trim().equalsIgnoreCase("n/a")) {
            tv_dob.setText("DOB" + seperator_string + dob);
        } else {
            tv_dob.setVisibility(View.GONE);
        }*/
    }

    private void setGender(String gender) {
        if (Utils.isValueAvailable(gender) && !gender.trim().equalsIgnoreCase("n/a")) {
            tv_gender.setText(gender);
            isToShowPatientData = true;
        } else {
            llPatientGender.setVisibility(View.GONE);
        }
    }

    private void setHeight(String Height) {
        if (Utils.isValueAvailable(Height) && !Height.trim().equalsIgnoreCase("n/a")) {
            tv_height.setText(Height);
            isToShowPatientData = true;
        } else {
            llPatientHeight.setVisibility(View.GONE);
        }
    }

    private void setWeight(String Weight) {
        if (!Utils.isValueAvailable(Weight) && !Weight.trim().equalsIgnoreCase("n/a")) {
            tv_weight.setText(Weight);
            isToShowPatientData = true;
        } else {
            llPatientWeight.setVisibility(View.GONE);
        }
    }

    private void setReportDate(String Report) {
        if (!Report.trim().equals("") && Report != null && !Report.trim().equalsIgnoreCase("n/a")) {
            ll_report.setVisibility(View.VISIBLE);
            tv_report_date.setText(Report);
        } else {
            ll_report.setVisibility(View.GONE);
        }
    }


    private void setPatientId(String id) {

        if (Utils.isValueAvailable(id) && !id.trim().equalsIgnoreCase("n/a") &&
                (id.matches("[0-9]+") && id.length() == 14 && !id.toLowerCase().contains(getString(R.string.mphrx)))) {
            tvPatientId.setText(id);
        } else {
            llPatientId.setVisibility(View.GONE);
        }
    }

    private void setPerformingLocation(String location) {
        if (Utils.isValueAvailable(location) && !location.trim().equalsIgnoreCase("n/a")) {
            tvPerformingLocation.setText(location);
        } else {
            llPerformingLocation.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onGetOrganizationWithLogoResponse(GetOrganizationWithLogoResponse logoResponse) {

        if (getTransactionId() != logoResponse.getTransactionId())
            return;
        dismissProgressDialog();
        isLogoDownLoading = false;
        mLogoDownloadProgressBar.setVisibility(View.GONE);
        if (logoResponse.isSuccessful()) {
            SharedPref.setLogoDownloadTime(System.currentTimeMillis());
            if (logoResponse.getGetOrganizationWithLogoResponse().getLogo() != null)
                setLogo(logoResponse.getGetOrganizationWithLogoResponse().getLogo());
            else
                setLabName(logoResponse.getGetOrganizationWithLogoResponse().getOrganization().getName());
        }

    }

    private void setLogo(byte[] logo) {
        ll_image_holder.setVisibility(View.VISIBLE);
        mLogoDownloadProgressBar.setVisibility(View.GONE);
        img_logo.setImageBitmap(Utils.scaleBitmapAndKeepRation(BitmapFactory.decodeByteArray(logo, 0, logo.length), (int) Utils.convertDpToPixel(43f, this), (int) Utils.convertDpToPixel(180f, this)));
        tv_labinfo_name.setVisibility(View.GONE);
        //removing check as logo needs to replace from local database
        saveLogo(this, logo);
    }


    private void setLabName(String labName) {
        ll_image_holder.setVisibility(View.GONE);
        tv_labinfo_name.setVisibility(View.VISIBLE);
        tv_labinfo_name.setText(labName);

    }

    private boolean logoSavedInMemory() {
        ContextWrapper cw = new ContextWrapper(this);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f = new File(directory.getAbsolutePath(), "logo" + mOrganisationId + ".png");
            if (f.exists())
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }


    public String saveLogo(Context context, byte[] logo) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        Bitmap bitmap = BitmapFactory.decodeByteArray(logo, 0, logo.length);
        File mypath = new File(directory, "logo" + mOrganisationId + ".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }


    public Bitmap loadLogo(Context context) {
        ContextWrapper cw = new ContextWrapper(context);

        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f = new File(directory.getAbsolutePath(), "logo" + mOrganisationId + ".png");
            if (f.exists()) {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                return b;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    //it will open trends Activity seperated it from called from inside menu Click
    private void openTrends(int position) {
        Observation observation = (Observation) adapter.getObservationAtPostition(position);
        // paramId
        if (observation == null) {
            return;
        }
        if (!observation.isToShowViewTrend())
            return;

        String paramId = observation.getParamId();

        // patientId
        String patientId = patientPk;

        String secoundryParamId = null;
        if (observation.getName().equalsIgnoreCase("SYSTOLIC") || observation.getName().equalsIgnoreCase("DIASTOLIC")) {
            secoundryParamId = position == 0 ? ((Observation) adapter.getObservationAtPostition(position + 1)).getParamId() :
                    ((Observation) adapter.getObservationAtPostition(position - 1)).getParamId();
        }

        startActivity(ObservationHistoryActivity.newIntent(this,
                paramId, patientId,
                vitalText, observation.getName(), secoundryParamId, false));

    }


    @Override
    public void itemClicked(View view, int position, boolean isHistoryScreenNeedsToBeLaunched, boolean isToShowViewDetails, boolean isToShowViewTrends) {
        if (adapter == null)
            return;
        if (isHistoryScreenNeedsToBeLaunched) {
            openTrends(position);
        } else {
            showPopupMenuCRUD(R.menu.menu_observation_activity, view, position, isToShowViewDetails, isToShowViewTrends);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    //display popup menu for each parameter
    public void showPopupMenuCRUD(final int menuId, View view, final int position, boolean isToShowviewDetails, boolean isToShowViewTrends) {
        final android.support.v7.widget.PopupMenu popup = new PopupMenu(ObservationActivity.this, view);

        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        if (!isToShowviewDetails) {
            popup.getMenu().findItem(R.id.view_details).setVisible(false);
        }
        if (!isToShowViewTrends) {
            popup.getMenu().findItem(R.id.view_trends).setVisible(false);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.view_trends:
                        openTrends(position);
                        break;
                    case R.id.view_details:
                        showDialogToViewDetails(position);
                        break;
                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }


    //showing dialog with specifying % of screen to be displayed
    private void showDialogToViewDetails(int position) {

        AlertDialog dialog = createAlertDialogFromUI(this, getString(R.string.view_details), "", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }, R.layout.layout_view_details, position);

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
            DisplayMetrics display = getResources().getDisplayMetrics();
            int screenWidth = display.widthPixels;
            int screenHeight = display.heightPixels;
            dialog.getWindow().setLayout((int) (screenWidth * 0.75f), (int) (screenHeight * 0.85f));
        }

    }

    //view Details dialog created here with custom UI as well as displayed value on it
    private AlertDialog createAlertDialogFromUI(Context context, String title, String message, final DialogInterface.OnClickListener listener, int layoutid, int position) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(layoutid, null, false);
        alertDialog.setView(v);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.title);

        CustomFontTextView tvBodySiteHeader, tvBodySiteValue, tvObservationMethodHeader, tvObservationMethodValue;
        CustomFontTextView tvInterpretationHeader, tvInterpretationValue, tvReferenceRangeHeader, tvReferenceRangeValue;
        CustomFontTextView tvCommentHeader, tvStatusHeader, tvStatusValue;
        WebView tvCommentValue;

        Observation observation = (Observation) adapter.getObservationAtPostition(position);
        // TODO as currently api is not sending body site and method need to handle and set it

        tvTitle.setText(observation.getName());
        tvBodySiteHeader = (CustomFontTextView) v.findViewById(R.id.body_site_header);
        tvBodySiteValue = (CustomFontTextView) v.findViewById(R.id.body_site_value);

        //manage visibility of value and header based on its value is null or not
        if (Utils.isValueAvailable(observation.getBodySite())) {
            tvBodySiteHeader.setVisibility(View.VISIBLE);
            tvBodySiteValue.setVisibility(View.VISIBLE);

            tvBodySiteValue.setText(observation.getBodySite());
        } else {
            tvBodySiteHeader.setVisibility(View.GONE);
            tvBodySiteValue.setVisibility(View.GONE);
        }
        tvObservationMethodHeader = (CustomFontTextView) v.findViewById(R.id.obs_method_header);
        tvObservationMethodValue = (CustomFontTextView) v.findViewById(R.id.obs_method_value);
        //manage visibility of observation method value and header based on its value is null or not
        if (Utils.isValueAvailable(observation.getObservationMethod())) {
            tvObservationMethodHeader.setVisibility(View.VISIBLE);
            tvObservationMethodValue.setVisibility(View.VISIBLE);

            tvObservationMethodValue.setText(observation.getObservationMethod());
        } else {
            tvObservationMethodHeader.setVisibility(View.GONE);
            tvObservationMethodValue.setVisibility(View.GONE);
        }
        tvInterpretationHeader = (CustomFontTextView) v.findViewById(R.id.tv_interpretation_header);
        tvInterpretationValue = (CustomFontTextView) v.findViewById(R.id.tv_interpretation_value);
        //manage visibility of Interprevation value and header based on its value is null or not
        if (Utils.isValueAvailable(observation.getInterpretation())) {
            tvInterpretationHeader.setVisibility(View.VISIBLE);
            tvInterpretationValue.setVisibility(View.VISIBLE);
            String value;
            ObservationInterpretationEnum observation_enum = ObservationInterpretationEnum.getMap().get(observation.getInterpretation());

            if (observation_enum != null)
                value = observation_enum.getDescription();
            else
                value = observation.getInterpretation();

            tvInterpretationValue.setText(value);
        } else {
            tvInterpretationHeader.setVisibility(View.GONE);
            tvInterpretationValue.setVisibility(View.GONE);
        }

        //manage visibility of Refrence Range value and header based on its value is null or not
        tvReferenceRangeHeader = (CustomFontTextView) v.findViewById(R.id.tv_refrence_header);
        tvReferenceRangeValue = (CustomFontTextView) v.findViewById(R.id.tv_refrence_value);
        if (Utils.isValueAvailable(observation.getDisplayableRange(vitalText))) {
            tvReferenceRangeHeader.setVisibility(View.VISIBLE);
            tvReferenceRangeValue.setVisibility(View.VISIBLE);
            tvReferenceRangeValue.setText(observation.getDisplayableRange(vitalText));
        } else {
            tvReferenceRangeHeader.setVisibility(View.GONE);
            tvReferenceRangeValue.setVisibility(View.GONE);
        }


        tvCommentHeader = (CustomFontTextView) v.findViewById(R.id.comments_header);
        tvCommentValue = (WebView) v.findViewById(R.id.comments_value);

        final WebSettings webSettings = tvCommentValue.getSettings();
        tvCommentValue.getSettings().setJavaScriptEnabled(true);
//        webSettings.setDefaultFontSize((int) Utils.convertSpToPixel(5, this));
        webSettings.setDefaultFontSize(getResources().getInteger(R.integer.font_size));
        tvCommentValue.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                view.loadUrl(
                        getString(R.string.webview_color_change_text)
                );
            }
        });

        //manage visibility of Comments value and header based on its value is null or not
        if (Utils.isValueAvailable(observation.getComment())) {
            tvCommentHeader.setVisibility(View.VISIBLE);
            tvCommentValue.setVisibility(View.VISIBLE);
            tvCommentValue.loadData(observation.getComment(), getString(R.string.mime_html_text), null);
        } else {
            tvCommentHeader.setVisibility(View.GONE);
            tvCommentValue.setVisibility(View.GONE);
        }
        tvStatusHeader = (CustomFontTextView) v.findViewById(R.id.tv_status_header);
        tvStatusValue = (CustomFontTextView) v.findViewById(R.id.tv_status_value);
        //manage visibility of Observation status value and header based on its value is null or not
        if (Utils.isValueAvailable(observation.getStatus())) {
            tvStatusHeader.setVisibility(View.VISIBLE);
            tvStatusValue.setVisibility(View.VISIBLE);
            if (ObservationStatusEnum.getObservationStatusMap().containsKey(observation.getStatus())) {
                tvStatusValue.setText(ObservationStatusEnum.getObservationStatusMap().
                        get(observation.getStatus()).getObservationStatusValue());
            } else {
                tvStatusValue.setText(getString(R.string.status_pending));
            }
        } else {
            tvStatusValue.setText(getString(R.string.status_pending));
        }

        alertDialog.setCanceledOnTouchOutside(false);
        v.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.onClick(alertDialog, Dialog.BUTTON_POSITIVE);
            }
        });

        return alertDialog;
    }

    @Override
    public void onPermissionNotGranted() {
        super.onPermissionNotGranted();
    }

    @Override
    public void onPermissionGranted(String permission) {
        fileDownload();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case PDF_VIEW_RESULT:
                    onPermissionGranted(permissions[0]);
                    break;
            }
        } else
            onPermissionNotGranted();
    }


    //return temporary file instance created for downloading and view report
    private File getFileInstance() {
        File file = null;
        if (fileType == VariableConstants.MIME_TYPE_FILE) {
            file = new File(Environment.getExternalStorageDirectory(), "/" + this.getString(R.string.app_name) + "./temp" + "/" + documentRefId + ".pdf");
        } else if (fileType == VariableConstants.MIME_TYPE_DOC) {
            file = new File(Environment.getExternalStorageDirectory(), "/" + this.getString(R.string.app_name) + "./temp" + "/" + documentRefId + ".doc");
        } else if (fileType == VariableConstants.MIME_TYPE_IMAGE) {
            file = new File(Environment.getExternalStorageDirectory(), "/" + this.getString(R.string.app_name) + "./temp" + "/" + documentRefId + ".jpg");
        }
        return file;
    }


    //deleting the temporary file created , inside try catch if (marshmallow permission not there)
    @Override
    protected void onDestroy() {
        try {
            File file = getFileInstance();
            if (file != null && file.exists())
                file.delete();
        } catch (Exception e) {
            AppLog.d("ERROR", "unable to delete");
        }
        super.onDestroy();
    }


    @Override
    public void onScrollChanged(NestedScrollView who, int l, int t, int oldl, int oldt) {
        final int headerHeight = llLabInfoContainer.getHeight() - mToolbar.getHeight();
        final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
        final int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }

    private void setReportStatusAndTestMethod(String status, String testMethod) {
        boolean isStatusAvailable = Utils.isValueAvailable(status);
        boolean isTestMethodAvailable = Utils.isValueAvailable(testMethod);
        if (!isStatusAvailable && !isTestMethodAvailable) {
            cardLayoutReportStatusAndTestMethod.setVisibility(View.GONE);
            return;
        }
        cardLayoutReportStatusAndTestMethod.setVisibility(View.VISIBLE);

        //set report status
        if (isStatusAvailable) {
            cardLayoutReportStatusAndTestMethod.setVisibility(View.VISIBLE);
            llReportStatus.setVisibility(View.VISIBLE);
            tvReportStatus.setText(ObservationStatusEnum.getFullDisplayedValuefromCode(status));
            //for MNV-9888, moved entire block into this as status can be null
            if (status.equalsIgnoreCase(TextConstants.OBSERVATION_STATUS_CANCELLED)) {
                tvReportStatus.setPaintFlags(tvReportStatus.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                cardReportList.setVisibility(View.GONE);
                ll_layoutFoter.setVisibility(View.GONE);

            } else {
                cardReportList.setVisibility(View.VISIBLE);
            }

        } else {
            llReportStatus.setVisibility(View.GONE);
            lview.setVisibility(View.GONE);
        }


        //set test method
        if (isTestMethodAvailable) {
            cardLayoutReportStatusAndTestMethod.setVisibility(View.VISIBLE);
            llTestMethod.setVisibility(View.VISIBLE);
            tvTestMethod.setText(testMethod);
            lview.setVisibility(View.VISIBLE);
        } else {
            llTestMethod.setVisibility(View.GONE);
            lview.setVisibility(View.GONE);
        }
    }

}
