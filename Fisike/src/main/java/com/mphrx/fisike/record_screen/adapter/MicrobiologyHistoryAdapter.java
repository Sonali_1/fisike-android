package com.mphrx.fisike.record_screen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.SupportAdapter;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SupportDataItem;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.response.MicrobiologyComponent;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Neha Rathore on 4/11/2017.
 */

public class MicrobiologyHistoryAdapter extends
        RecyclerView.Adapter<MicrobiologyHistoryAdapter.MicrobiologyHistoryViewHolder> {
    Context context;
    private List<MicrobiologyComponent> microbiologyComponents;

    public MicrobiologyHistoryAdapter(Context context, List<MicrobiologyComponent> microbiologyComponents) {
        this.context = context;
        this.microbiologyComponents = microbiologyComponents;
    }

    @Override
    public MicrobiologyHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.micribiology_history_row, null, false);
        MicrobiologyHistoryViewHolder viewHolder = new MicrobiologyHistoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MicrobiologyHistoryViewHolder holder, int position) {

        MicrobiologyComponent component = microbiologyComponents.get(position);
        String componentName = Utils.isValueAvailable(component.getComponentName()) ?
               component.getComponentName() : "";

        String MIC = Utils.isValueAvailable(component.getMicValue()) ?
                component.getMicValue(): "";

        String sensitivity = Utils.isValueAvailable(component.getSensitivity()) ?
                component.getSensitivity(): "";

        holder.tvComponentName.setText(componentName);
        holder.tvMIC.setText(MIC);
        holder.tvSensitivity.setText(sensitivity);
    }

    @Override
    public int getItemCount() {
        return microbiologyComponents.size();
    }

    public class MicrobiologyHistoryViewHolder extends RecyclerView.ViewHolder {
        private TextView tvComponentName, tvMIC, tvSensitivity;

        public MicrobiologyHistoryViewHolder(View itemView) {
            super(itemView);
            tvComponentName = (TextView) itemView.findViewById(R.id.tv_title);
            tvMIC = (TextView) itemView.findViewById(R.id.tv_value);
            tvSensitivity = (TextView) itemView.findViewById(R.id.tv_interpretation);

        }
    }

}
