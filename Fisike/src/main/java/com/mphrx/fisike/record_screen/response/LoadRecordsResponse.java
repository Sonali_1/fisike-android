package com.mphrx.fisike.record_screen.response;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by kailash on 10-03-2016.
 */
public class LoadRecordsResponse extends BaseResponse {

    private boolean isSyncApi;
    private boolean isObservationSubmitApi;
    private boolean isOnclick;
    private String jsonString;

    public LoadRecordsResponse(JSONObject response, long transactionId, boolean isSyncApi, boolean isObservationSubmitApi, boolean isOnclick) {
        super(response, transactionId);
        jsonString = response.toString();
        this.isSyncApi = isSyncApi;
        this.isObservationSubmitApi = isObservationSubmitApi;
        this.isOnclick = isOnclick;
    }

    public LoadRecordsResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public String getJsonString() {
        return jsonString;
    }

    public boolean isSyncApi() {
        return isSyncApi;
    }

    public boolean isObservationSubmitApi() {
        return isObservationSubmitApi;
    }

    public boolean isOnclick() {
        return isOnclick;
    }
}
