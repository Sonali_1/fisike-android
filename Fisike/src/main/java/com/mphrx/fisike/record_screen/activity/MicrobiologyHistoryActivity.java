package com.mphrx.fisike.record_screen.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.ObservationStatusEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.adapter.MicrobiologyHistoryAdapter;
import com.mphrx.fisike.record_screen.response.MicrobiologyObservation;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;

public class MicrobiologyHistoryActivity extends BaseActivity {

    MicrobiologyObservation observation;
    CustomFontTextView tvColonalCountValue, tvCultureLineHeaderValue, tvGrowthValue, tvIncubationPeriodValue,
            tvStatusValue, tvTestMethodValue;
    Toolbar mToolbar;
    private FrameLayout frameLayout;
    private CustomFontTextView toolbar_title;
    private CustomFontTextView tvAbbrevations;
    public static String MICROBIOLOGY_DATA = "MICROBIOLOGY_DATA";
    private MicrobiologyHistoryAdapter adapter;
    private LinearLayoutManager microbiologyLayoutManager;
    private RecyclerView rvMicrobiologyList;
    private WebView tvRemarkValue;
    private CustomFontTextView tvStatusHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_microbiology_history, frameLayout);
        try {
            observation = getIntent().getBundleExtra("bundle").getParcelable(MICROBIOLOGY_DATA);
        } catch (Exception e) {
            AppLog.e(e.getMessage(), "Microbiology history obesrvation not found", e);
        }
        findViews();
        initToolbar();
        initView();
    }

    private void initToolbar() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String titleText = "";
        titleText = observation.getTestType();
        toolbar_title.setText(titleText);

    }

    @Override
    public boolean onSupportNavigateUp() {
        MicrobiologyHistoryActivity.this.finish();
        return true;
    }

    private void findViews() {
        tvColonalCountValue = (CustomFontTextView) findViewById(R.id.tv_colony_count_value);
        tvIncubationPeriodValue = (CustomFontTextView) findViewById(R.id.tv_incubation_period_value);
        tvCultureLineHeaderValue = (CustomFontTextView) findViewById(R.id.tv_culture_line_value);
        tvGrowthValue = (CustomFontTextView) findViewById(R.id.tv_growth_value);
        tvRemarkValue = (WebView) findViewById(R.id.tv_remarks_value);
        tvStatusHeader = (CustomFontTextView) findViewById(R.id.tv_status);
        tvStatusValue = (CustomFontTextView) findViewById(R.id.tv_status_value);
        tvAbbrevations = (CustomFontTextView) findViewById(R.id.tv_abbrevations);
        rvMicrobiologyList = (RecyclerView) findViewById(R.id.rv_microbiology_list);
        tvTestMethodValue = (CustomFontTextView) findViewById(R.id.tv_test_method_value);
    }

    private void initView() {
        tvRemarkValue.getSettings().setJavaScriptEnabled(true);
        final WebSettings webSettings = tvRemarkValue.getSettings();
//        webSettings.setDefaultFontSize((int)Utils.convertSpToPixel(5,this));
        webSettings.setDefaultFontSize(getResources().getInteger(R.integer.font_size));
        tvRemarkValue.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                view.loadUrl(
                        getString(R.string.webview_color_change_text)
                );
            }
        });

        setHeaderMicrobiologyParameter();
        tvAbbrevations.setText(getString(R.string.sensitivity_interpretation));
        adapter = new MicrobiologyHistoryAdapter(this, observation.getComponentList());
        microbiologyLayoutManager = new LinearLayoutManager(this);
        rvMicrobiologyList.setLayoutManager(microbiologyLayoutManager);
        rvMicrobiologyList.setAdapter(adapter);
    }

    private void setHeaderMicrobiologyParameter() {
        setTheFieldValueIfAvailable(observation.getTestMethod(), tvTestMethodValue);
        setTheFieldValueIfAvailable(observation.getColonyCount(), tvColonalCountValue);
        setTheFieldValueIfAvailable(observation.getIncubationPeriod(), tvIncubationPeriodValue);
        setTheFieldValueIfAvailable(observation.getCultureLine(), tvCultureLineHeaderValue);
        setTheFieldValueIfAvailable(observation.getGrowth(), tvGrowthValue);

        if (Utils.isValueAvailable(observation.getRemarks())) {
            tvRemarkValue.loadData(observation.getRemarks(), getString(R.string.mime_html_text), null);
        } else {
            tvRemarkValue.loadData(getString(R.string.html_unavailable), getString(R.string.mime_html_text), null);
        }

        tvStatusValue.setVisibility(View.GONE);
        tvStatusHeader.setVisibility(View.GONE);

    }

    private void setTheFieldValueIfAvailable(String value, CustomFontTextView fieldView) {
        if (Utils.isValueAvailable(value)) {
            fieldView.setText(value);
        }
    }

}

