package com.mphrx.fisike.record_screen.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Kailash Khurana on 7/22/2016.
 */
public class DiagnosticReport implements Parcelable {

    private String reportId;
    private String reportDate;
    private String reportName;

    private String diagnosticOrderId;
    private String patientName;
    private String reportStatus;
    private String reportType;
    private String reportFormat;
    private String resultDate;
    private String expectedDate;

    public DiagnosticReport(){

    }

    protected DiagnosticReport(Parcel in) {
        reportId = in.readString();
        reportDate = in.readString();
        reportName = in.readString();
        diagnosticOrderId = in.readString();
        patientName = in.readString();
        reportStatus = in.readString();
        reportType = in.readString();
        reportFormat = in.readString();
        resultDate = in.readString();
        expectedDate = in.readString();
    }

    public static final Creator<DiagnosticReport> CREATOR = new Creator<DiagnosticReport>() {
        @Override
        public DiagnosticReport createFromParcel(Parcel in) {
            return new DiagnosticReport(in);
        }

        @Override
        public DiagnosticReport[] newArray(int size) {
            return new DiagnosticReport[size];
        }
    };

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getDiagnosticOrderId() {
        return diagnosticOrderId;
    }

    public void setDiagnosticOrderId(String diagnosticOrderId) {
        this.diagnosticOrderId = diagnosticOrderId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(String reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }


    public String getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    public String getResultDate() {
        return resultDate;
    }

    public void setResultDate(String resultDate) {
        this.resultDate = resultDate;
    }

    public String getExpectedDate() {
        return expectedDate;
    }

    public void setExpectedDate(String expectedDate) {
        this.expectedDate = expectedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(reportId);
        out.writeString(reportDate);
        out.writeString(reportName);
        out.writeString(diagnosticOrderId);
        out.writeString(patientName);
        out.writeString(reportStatus);
        out.writeString(reportType);
        out.writeString(reportFormat);
        out.writeString(resultDate);
        out.writeString(expectedDate);
    }
}
