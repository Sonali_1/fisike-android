package com.mphrx.fisike.record_screen.request;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.mphrx.fisike.background.GetOrganizationWithLogoResponse;
import com.mphrx.fisike.record_screen.response.LoadRecordsResponse;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONObject;

import static com.mphrx.fisike_physician.utils.BusProvider.getInstance;

/**
 * Created by Neha on 10-03-2016.
 */
public class LoadRecordsRequest extends BaseObjectRequest {

    private final long mTransactionId;
    private final String diagnosticOrderSearchURL;
    private final String constraintsObject;
    private boolean isSyncApi;
    private boolean isObservationSubmitApi;
    private boolean isOnclick;

    public LoadRecordsRequest(long mTransactionId, String diagnosticOrderSearchURL, String constraintsObject,boolean isSyncApi,boolean isObservationSubmitApi,boolean isOnclick) {
        this.mTransactionId = mTransactionId;
        this.diagnosticOrderSearchURL = diagnosticOrderSearchURL;
        this.constraintsObject = constraintsObject;
        this.isSyncApi = isSyncApi;
        this.isObservationSubmitApi = isObservationSubmitApi;
        this.isOnclick = isOnclick;
    }

    @Override
    public void doInBackground() {
        RequestFuture<JSONObject> primaryListener = RequestFuture.newFuture();
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                diagnosticOrderSearchURL, constraintsObject, this, this);

        Network.getGeneralRequestQueue().add(reportRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        getInstance().post(new LoadRecordsResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        getInstance().post(new LoadRecordsResponse(response, mTransactionId,isSyncApi,isObservationSubmitApi,isOnclick));
    }

}
