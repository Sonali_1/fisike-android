package com.mphrx.fisike.record_screen.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xmb2nc on 10-04-2017.
 */

public class MicrobiologyComponent implements Parcelable {
    private String micValue;
    private String sensitivity;
    private String componentName;

    protected MicrobiologyComponent(Parcel in) {
        micValue = in.readString();
        sensitivity = in.readString();
        componentName=in.readString();
    }

    public MicrobiologyComponent() {

    }

    public String getMicValue() {
        return micValue;
    }

    public void setMicValue(String micValue) {
        this.micValue = micValue;
    }

    public String getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(String sensitivity) {
        this.sensitivity = sensitivity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(micValue);
        parcel.writeString(sensitivity);
        parcel.writeString(componentName);
    }

    public static final Creator<MicrobiologyComponent> CREATOR = new Creator<MicrobiologyComponent>() {
        @Override
        public MicrobiologyComponent createFromParcel(Parcel in) {
            return new MicrobiologyComponent(in);
        }

        @Override
        public MicrobiologyComponent[] newArray(int size) {
            return new MicrobiologyComponent[size];
        }
    };
    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

}
