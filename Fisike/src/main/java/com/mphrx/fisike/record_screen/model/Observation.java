package com.mphrx.fisike.record_screen.model;;

import android.text.TextUtils;

import com.mphrx.fisike.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright 2015 App Street Software Pvt. Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Observation {

    private String mParamId;            // required
    private String mInterpretation;
    private String mIssueDate;
    private String mName;               // required
    private String mReferenceLow;
    private String mReferenceHigh;
    private String mReferenceRange;
    private String mPatientId;
    private String mValue;              // required
    private String mUnit;               // required
    private String mStyleCode;
    private String bodySite;
    private String observationMethod;
    private String comment;
    private Object colonyCount;
    private String status;
    private boolean isToShowViewDetails;
    private boolean isToShowViewTrend;
    public Observation() {
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isToShowViewDetails() {
        return isToShowViewDetails;
    }

    public void setToShowViewDetails(boolean toShowViewDetails) {
        isToShowViewDetails = toShowViewDetails;
    }

    public boolean isToShowViewTrend() {
        return isToShowViewTrend;
    }

    public void setToShowViewTrend(boolean toShowViewTrend) {
        isToShowViewTrend = toShowViewTrend;
    }

    public String getParamId() {
        return mParamId;
    }

    public String getInterpretation() {
        return mInterpretation;
    }

    public String getIssueDate() {
        return mIssueDate;
    }

    public String getName() {
        return mName;
    }

    public String getReferenceLow() {
        return mReferenceLow;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getObservationMethod() {
        return observationMethod;
    }

    public void setObservationMethod(String observationMethod) {
        this.observationMethod = observationMethod;
    }

    public String getBodySite() {
        return bodySite;
    }

    public void setBodySite(String bodySite) {
        this.bodySite = bodySite;
    }

    public String getReferenceHigh() {
        return mReferenceHigh;
    }

    public String getReferenceRange() {
        return mReferenceRange;
    }

    public String getPatientId() {
        return mPatientId;
    }

    public String getValue() {
        return mValue;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setmParamId(String mParamId) {
        this.mParamId = mParamId;
    }

    public void setmInterpretation(String mInterpretation) {
        if (mInterpretation != null && mInterpretation.equalsIgnoreCase("No range defined")) {
            mInterpretation = "-";
        }

        if (mInterpretation.trim().equalsIgnoreCase("=") || mInterpretation.trim().equalsIgnoreCase("N")) {
            mInterpretation = "Normal";

        }
        this.mInterpretation = mInterpretation;
    }

    public void setmIssueDate(String mIssueDate) {
        this.mIssueDate = mIssueDate;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmReferenceLow(String mReferenceLow) {
        this.mReferenceLow = mReferenceLow;
    }

    public void setmReferenceHigh(String mReferenceHigh) {
        this.mReferenceHigh = mReferenceHigh;
    }

    public void setmReferenceRange(String mReferenceRange) {
        this.mReferenceRange = mReferenceRange;
    }

    public void setmPatientId(String mPatientId) {
        this.mPatientId = mPatientId;
    }

    public String getmPatientId()
    {
        return mPatientId;
    }
    public void setmValue(String mValue) {
        this.mValue = mValue;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }

    public void setmStyleCode(String mStyleCode) {
        this.mStyleCode = mStyleCode;
    }

    public String getDisplayableRange(String vitalText) {
        if (vitalText != null && vitalText.equalsIgnoreCase("Glucose")) {
            if (mUnit.equalsIgnoreCase("mmol/L") && Utils.isValueAvailable(mReferenceLow) && Utils.isValueAvailable(mReferenceHigh)) {
                float mReferenceLowNewFloat = (float) (Float.parseFloat(mReferenceLow) / 18.0);
                String mReferenceLowNew = Math.round(mReferenceLowNewFloat * 100.0) / 100.0 + "";
                float mReferenceLowHighFloat = (float) (Float.parseFloat(mReferenceHigh) / 18.0);
                String mReferenceHighNew = Math.round(mReferenceLowHighFloat * 100.0) / 100.0 + "";
                return "" + mReferenceLowNew + "-" + mReferenceHighNew + " " + mUnit;
            } else if (mUnit.equalsIgnoreCase("mmol/L") && Utils.isValueAvailable(mReferenceRange) && mReferenceRange.contains("-")) {
                float mReferenceLowNewFloat = (float) (Float.parseFloat(mReferenceRange.split("-")[0].trim()) / 18.0);
                String mReferenceLowNew = Math.round(mReferenceLowNewFloat * 100.0) / 100.0 + "";
                float mReferenceLowHighFloat = (float) (Float.parseFloat(mReferenceRange.split("-")[1].trim()) / 18.0);
                String mReferenceHighNew = Math.round(mReferenceLowHighFloat * 100.0) / 100.0 + "";
                return "" + mReferenceLowNew + "-" + mReferenceHighNew + " " + mUnit;
            }
        }
        //check null or blank before creating reference range
        if (Utils.isValueAvailable(mReferenceLow) && Utils.isValueAvailable(mReferenceHigh)) {
            return "" + mReferenceLow + "-" + mReferenceHigh + " " + (Utils.isValueAvailable(mUnit) ? mUnit :"");
        } else if (Utils.isValueAvailable(mReferenceRange)) {
            if(mReferenceRange.equals("-")){
                return "";
            } else
                return "" + mReferenceRange + " " + (Utils.isValueAvailable(mUnit) ? mUnit :"");
        } else {
            return "";
        }
    }

    public String getmStyleCode() {
        return mStyleCode;
    }


}