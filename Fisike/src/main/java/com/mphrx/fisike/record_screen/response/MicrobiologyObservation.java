package com.mphrx.fisike.record_screen.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xmb2nc on 10-04-2017.
 */

public class MicrobiologyObservation implements Parcelable {

    private List<MicrobiologyComponent> componentList = new ArrayList<>();
    private String colonyCount, cultureLine, growth, incubationPeriod, status, testType, remarks, testMethod;

    MicrobiologyObservation() {
    }

    protected MicrobiologyObservation(Parcel in) {
        componentList = in.createTypedArrayList(MicrobiologyComponent.CREATOR);
        colonyCount = in.readString();
        cultureLine = in.readString();
        growth = in.readString();
        incubationPeriod = in.readString();
        status = in.readString();
        testType = in.readString();
        remarks = in.readString();
        testMethod = in.readString();
    }



    public static final Creator<MicrobiologyObservation> CREATOR = new Creator<MicrobiologyObservation>() {
        @Override
        public MicrobiologyObservation createFromParcel(Parcel in) {
            return new MicrobiologyObservation(in);
        }

        @Override
        public MicrobiologyObservation[] newArray(int size) {
            return new MicrobiologyObservation[size];
        }
    };

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<MicrobiologyComponent> getComponentList() {
        return componentList;
    }

    public String getColonyCount() {
        return colonyCount;
    }

    public void setColonyCount(String colonyCount) {
        this.colonyCount = colonyCount;
    }

    public String getCultureLine() {
        return cultureLine;
    }

    public void setCultureLine(String cultureLine) {
        this.cultureLine = cultureLine;
    }

    public String getGrowth() {
        return growth;
    }

    public void setGrowth(String growth) {
        this.growth = growth;
    }

    public String getIncubationPeriod() {
        return incubationPeriod;
    }

    public void setIncubationPeriod(String incubationPeriod) {
        this.incubationPeriod = incubationPeriod;
    }

    public String getTestMethod() {
        return testMethod;
    }

    public void setTestMethod(String testMethod) {
        this.testMethod = testMethod;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeTypedList(componentList);
        parcel.writeString(colonyCount);
        parcel.writeString(cultureLine);
        parcel.writeString(growth);
        parcel.writeString(incubationPeriod);
        parcel.writeString(status);
        parcel.writeString(testType);
        parcel.writeString(remarks);
        parcel.writeString(testMethod);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setComponentList(List<MicrobiologyComponent> componentList) {
        this.componentList = componentList;
    }



}
