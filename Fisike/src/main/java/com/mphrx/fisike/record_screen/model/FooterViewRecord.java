package com.mphrx.fisike.record_screen.model;

/**
 * Created by Kailash Khurana on 7/18/2016.
 */
public class FooterViewRecord {

    private boolean isLoading;

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
