package com.mphrx.fisike.record_screen.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by aastha on 12/5/2016.
 */
public class GetObservationHistoryRequest extends BaseObjectRequest {

    private static final String postfix = "/MoObservation/getVitals";
    private ProgressDialog dialog;
    private long transactionId;
    private Context context;
    private int count;
    private String paramId;
    private String secondaryParamId;
    private String patientId;


    public GetObservationHistoryRequest(long transactionId, Context context, int count, String paramId , String secondaryParamId,String patientId) {
        this.transactionId = transactionId;
        this.context = context;
        this.count = count;
        this.paramId = paramId;
        this.secondaryParamId = secondaryParamId;
        this.patientId=patientId;
    }

    @Override
    public void doInBackground() {
        String baseUrl = APIManager.createMinervaBaseUrl() + postfix;
        AppLog.showInfo(getClass().getSimpleName(), baseUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new VitalsResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        if(response==null){
            VolleyError error = new VolleyError();
            AppLog.showError(getClass().getSimpleName(), error.getMessage());
            BusProvider.getInstance().post(new VitalsResponse(error, transactionId));
        }
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new VitalsResponse(response, transactionId, context,true));
    }

    public String getPayLoad() {
        try {

         /*   {
                "mparamID": ["8462-4", "8480-6", 39156-5,2345-7],
                "subject": 19
            }*/

            JSONObject jsonObject = new JSONObject();
            if (BuildConfig.isPatientApp) {
                jsonObject.put("patientId", SettingManager.getInstance().getUserMO().getPatientId());
            }
            else
                jsonObject.put("patientId", patientId);
            JSONArray paramidArray = new JSONArray();
            paramidArray.put(0, paramId);
            if(secondaryParamId != null)
            paramidArray.put(1, secondaryParamId);

            jsonObject.put("mparamID", paramidArray);
            jsonObject.put("_count",count);
            Log.i("json", jsonObject.toString());
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

}
