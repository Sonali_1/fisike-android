package com.mphrx.fisike.record_screen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.RadiologyDetailScreen;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.ResultStatusEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.activity.MicrobiologyActivity;
import com.mphrx.fisike.record_screen.activity.ObservationActivity;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.List;

/**
 * Created by kailashkhurana on 21/08/17.
 */

public class ResultReportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<DiagnosticReport> recordList;
    private DiagnosticOrder diagnosticOrder;

    public ResultReportAdapter(Context context, List<DiagnosticReport> recordList, DiagnosticOrder diagnosticOrder) {
        this.context = context;
        this.recordList = recordList;
        this.diagnosticOrder = diagnosticOrder;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View HeaderView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.result_item, viewGroup, false);
        ResultReportAdapter.DiagnosticReportViewHolder reportHolder = new ResultReportAdapter.DiagnosticReportViewHolder(HeaderView);
        return reportHolder;
    }


    @Override
    public int getItemCount() {
        return recordList == null ? 0 : recordList.size();
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder recyclerViewHolder, final int position) {
        if (recyclerViewHolder instanceof ResultReportAdapter.DiagnosticReportViewHolder) {
            ResultReportAdapter.DiagnosticReportViewHolder diagnosticReportViewHolder = (ResultReportAdapter.DiagnosticReportViewHolder) recyclerViewHolder;

            final DiagnosticReport diagnosticReport = recordList.get(position);

            if (Utils.isRTL(context)) {
                diagnosticReportViewHolder.icon_arrow.setRotationY(180);
            } else {
                diagnosticReportViewHolder.icon_arrow.setRotationY(0);
            }

            String reportName = diagnosticReport.getReportName();
            diagnosticReportViewHolder.txt_test.setText(Utils.isValueAvailable(reportName) ? reportName : diagnosticReportViewHolder.txt_test.getResources().getString(R.string.unavailable));
            String reportStatus = diagnosticReport.getReportStatus();
            reportStatus = ResultStatusEnum.getDisplayedValuefromCode(reportStatus);
            diagnosticReportViewHolder.txt_test_status.setText(Utils.isValueAvailable(reportStatus) ? reportStatus : diagnosticReportViewHolder.txt_test_status.getResources().getString(R.string.unavailable));
            diagnosticReportViewHolder.txt_test_status.setTextColor(reportStatus.equalsIgnoreCase(context.getString(R.string.unavailable_key)) ? ContextCompat.getColor(context, R.color.dusky_blue_70) : ContextCompat.getColor(context, R.color.dusky_blue));
            String reportDate = diagnosticReport.getResultDate();
            String expectedDate = diagnosticReport.getExpectedDate();
            if (Utils.isValueAvailable(reportDate)) {
                diagnosticReportViewHolder.txt_expected_date.setVisibility(View.VISIBLE);
                diagnosticReportViewHolder.txtDateKey.setText(context.getString(R.string.result_date));
                diagnosticReportViewHolder.txtDateKey.setVisibility(View.VISIBLE);
                String destinationTimeFormat = DateFormat.is24HourFormat(diagnosticReportViewHolder.txt_expected_date.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
                diagnosticReportViewHolder.txt_expected_date.setText(DateTimeUtil.convertSourceDestinationDate(reportDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat));
            } else if (Utils.isValueAvailable(expectedDate)) {
                diagnosticReportViewHolder.txt_expected_date.setVisibility(View.VISIBLE);
                diagnosticReportViewHolder.txtDateKey.setVisibility(View.VISIBLE);
                diagnosticReportViewHolder.txtDateKey.setText(context.getString(R.string.expected_date));
                if (Utils.compareDateWithPresent(reportDate)) {
                    String destinationTimeFormat = DateFormat.is24HourFormat(diagnosticReportViewHolder.txt_expected_date.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
                    diagnosticReportViewHolder.txt_expected_date.setText(DateTimeUtil.convertSourceDestinationDate(reportDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat));
                } else if (Utils.compareDateWithPresent(reportDate)) {
                    diagnosticReportViewHolder.txt_expected_date.setText(diagnosticReportViewHolder.txt_test_status.getResources().getString(R.string.delayed));
                    diagnosticReportViewHolder.txt_expected_date.setTextColor(diagnosticReportViewHolder.txt_expected_date.getResources().getColor(R.color.red));
                }
            } else {
                diagnosticReportViewHolder.txt_expected_date.setVisibility(View.GONE);
                diagnosticReportViewHolder.txtDateKey.setVisibility(View.GONE);

            }

            int statusKey = diagnosticOrder.getCategory().equalsIgnoreCase(diagnosticReportViewHolder.txtStatusKey.getContext().getString(R.string.conditional_radiology)) ? R.string.rad_status_key : R.string.lab_status_key;
            diagnosticReportViewHolder.txtStatusKey.setText(diagnosticReportViewHolder.txtStatusKey.getResources().getString(statusKey));

            diagnosticReportViewHolder.layoutReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (diagnosticOrder.getCategory().equals(context.getString(R.string.conditional_radiology)))
                            context.startActivity(new Intent(context, RadiologyDetailScreen.class).putExtra("id", diagnosticReport.getDiagnosticOrderId()).putExtra("token",
                                    SharedPref.getAccessToken()));
                        else {
                            String status = diagnosticReport.getReportStatus();
                            // check for microbiology
                            if (diagnosticReport.getReportType().equals(TextConstants.MICROBIOLOGY_REPORT_TYPE)) {
                                context.startActivity(MicrobiologyActivity.newIntent(context, diagnosticReport.getReportId(),
                                        diagnosticReport.getReportName(), diagnosticOrder.getOrganisationId(),
                                        diagnosticOrder.getPhysicianName(), status == null ? "" : status,
                                        diagnosticOrder.getPerformingLocationName(), diagnosticOrder.getPatientId(), diagnosticOrder.getId()));
                            } else {
                                context.startActivity(ObservationActivity.newIntent(context, diagnosticReport.getReportId(),
                                        diagnosticReport.getReportName(), diagnosticOrder.getOrganisationId(),
                                        diagnosticOrder.getPhysicianName(), status == null ? "" : status,
                                        diagnosticOrder.getPerformingLocationName(), diagnosticReport.getReportFormat(), diagnosticOrder.getPatientId(), diagnosticOrder.getId()));

                            }
                        }
                    } catch (Exception ignore) {
                        ignore.getMessage();
                    }
                }
            });
        }
    }

    private class DiagnosticReportViewHolder extends RecyclerView.ViewHolder {
        private CustomFontTextView txtStatusKey;
        private CustomFontTextView txtDateKey;
        private LinearLayout layoutDate;
        public CustomFontTextView txt_test;
        public CustomFontTextView txt_test_status;
        public CustomFontTextView txt_expected_date;
        private OrderReportModel orderReportModel;
        private IconTextView icon_arrow;
        private LinearLayout layoutReport;

        public DiagnosticReportViewHolder(View itemView) {
            super(itemView);
            icon_arrow = (IconTextView) itemView.findViewById(R.id.icon_arrow);
            txt_test = (CustomFontTextView) itemView.findViewById(R.id.txt_test);
            txt_test_status = (CustomFontTextView) itemView.findViewById(R.id.txt_test_status);
            txt_expected_date = (CustomFontTextView) itemView.findViewById(R.id.txt_expected_date);
            txtStatusKey = (CustomFontTextView) itemView.findViewById(R.id.txt_status_key);
            txtDateKey = (CustomFontTextView) itemView.findViewById(R.id.txt_date_key);
            layoutDate = (LinearLayout) itemView.findViewById(R.id.layout_date);
            layoutReport = (LinearLayout) itemView.findViewById(R.id.layout_report);
        }
    }
}
