package com.mphrx.fisike.record_screen.request;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.mphrx.fisike.background.GetOrganizationWithLogoResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mphrx.fisike_physician.utils.BusProvider.getInstance;

/**
 * Created by Neha on 10-03-2016.
 */
public class GetOrganizationWithLogoRequest extends BaseObjectRequest {

    private final long mTransactionId;
    private String mOrganizationId;

    public GetOrganizationWithLogoRequest(long mTransactionId,String mOrganizationId)
    {
        this.mTransactionId=mTransactionId;
        this.mOrganizationId=mOrganizationId;
    }
    @Override
    public void doInBackground() {
        RequestFuture<JSONObject> primaryListener = RequestFuture.newFuture();
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                APIManager.getOrganizationWithLogoRequestUrl(),getPayload() , this, this);

        Network.getGeneralRequestQueue().add(reportRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        getInstance().post(new GetOrganizationWithLogoResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        getInstance().post(new GetOrganizationWithLogoResponse(response, mTransactionId));
    }
    private String getPayload() {
        JSONObject payload = new JSONObject();
        try {
            JSONObject constraints = new JSONObject();
            JSONArray details = new JSONArray();
            payload.put("organizationId", mOrganizationId);
        } catch (JSONException ignore) {
        }
        return payload.toString();
    }

}
