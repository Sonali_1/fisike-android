package com.mphrx.fisike.record_screen.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by xmb2nc on 10-04-2017.
 */

public class MicrobiologyObservationCollection extends BaseResponse implements Parcelable{
    private String title;
    private ArrayList<MicrobiologyObservation> observations = new ArrayList<>();

    private String remark;
    private String patientName;
    private String gender;
    private String dateofBirth;
    private String reportDate;
    private String fileURL;
    private int fileType;
    private int documentRefId;
    private String patientId;
    
    protected MicrobiologyObservationCollection(Parcel in) {
        title = in.readString();
        observations = in.readArrayList(ClassLoader.getSystemClassLoader());
        remark = in.readString();
        patientName = in.readString();
        gender = in.readString();
        dateofBirth = in.readString();
        reportDate = in.readString();
        fileURL = in.readString();
        fileType = in.readInt();
        documentRefId = in.readInt();
        patientId= in.readString();
    }

    public MicrobiologyObservationCollection(JSONObject response, long transactionId ,String mPatientId) {
        super(response, transactionId);
        patientId = mPatientId;
        doParse(response,mPatientId);
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    private void doParse(JSONObject response,String mPatientId)
    {
        remark = response.optString("remarks");
        patientName = response.optString("patientName");
        dateofBirth = response.optString("patientDob");
        gender = response.optString("patientSex");
        reportDate = response.optString("reportDate");
        fileURL = response.optString("pdfUrl");
        patientId=mPatientId;
        fileType = VariableConstants.MIME_TYPE_FILE;
        //supporting file type pdf
        if(BuildConfig.isFisike)
            fileType = VariableConstants.MIME_TYPE_UNKNOWN;
        if (Utils.isValueAvailable(fileURL) && fileURL.contains(".")) {
            String contentType = fileURL.substring(
                    fileURL.lastIndexOf(".") + 1, fileURL.length());
            if (contentType != null || !contentType.equalsIgnoreCase("null")) {
                if (contentType.equalsIgnoreCase("pdf")) {
                    fileType = VariableConstants.MIME_TYPE_FILE;
                } else if (contentType.equalsIgnoreCase("doc") || contentType.equalsIgnoreCase("docx")) {
                    fileType = VariableConstants.MIME_TYPE_DOC;
                } else {
                    fileType = VariableConstants.MIME_TYPE_ZIP;
                }
            }
        }
        observations=new ArrayList<>();
        documentRefId= response.optInt("id");
        try {
            JSONArray observationList = response.optJSONArray("observationList");
            for (int i = 0; observationList != null && i < observationList.length(); i++) {
                JSONObject observationListJSONObject = observationList.getJSONObject(i);
                MicrobiologyObservation observation = new MicrobiologyObservation();

                observation.setColonyCount(observationListJSONObject.optString("colonyCount"));
                observation.setCultureLine(observationListJSONObject.optString("cultureLine"));
                observation.setGrowth(observationListJSONObject.optString("growth"));
                observation.setIncubationPeriod(observationListJSONObject.optString("incubationPeriod"));
                observation.setStatus(observationListJSONObject.optString("status"));
                observation.setRemarks(observationListJSONObject.optString("remarks"));
                observation.setTestType(observationListJSONObject.optString("testType"));
                observation.setTestMethod(observationListJSONObject.optString("method"));
                JSONArray componentArray=observationListJSONObject.optJSONArray("component");
                ArrayList<MicrobiologyComponent> componentArrayList=new ArrayList<>();
                for(int j=0;componentArray!=null && j<componentArray.length();j++)
                {
                    JSONObject componentJson=componentArray.getJSONObject(j);
                    MicrobiologyComponent microBiologyComponentObject=new MicrobiologyComponent();
                    microBiologyComponentObject.setMicValue(componentJson.optString("referenceRange"));
                    microBiologyComponentObject.setSensitivity(componentJson.optString("value"));
                    microBiologyComponentObject.setComponentName(componentJson.optString("componentName"));
                    componentArrayList.add(microBiologyComponentObject);
                }
                observation.setComponentList(componentArrayList);
                observations.add(observation);
            }

        }catch (Exception e)
        {

        }
    }

    public MicrobiologyObservationCollection(VolleyError error, long mTransactionId, String mPatientId) {
        super(error, mTransactionId);
        patientId = mPatientId;
    }

    public static final Creator<MicrobiologyObservationCollection> CREATOR = new Creator<MicrobiologyObservationCollection>() {
        @Override
        public MicrobiologyObservationCollection createFromParcel(Parcel in) {
            return new MicrobiologyObservationCollection(in);
        }

        @Override
        public MicrobiologyObservationCollection[] newArray(int size) {
            return new MicrobiologyObservationCollection[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<MicrobiologyObservation> getObservations() {
        return observations;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getDocumentRefId() {
        return documentRefId;
    }

    public void setDocumentRefId(int documentRefId) {
        this.documentRefId = documentRefId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeList(observations);
        parcel.writeString(remark);
        parcel.writeString(patientName);
        parcel.writeString(gender);
        parcel.writeString(dateofBirth);
        parcel.writeString(reportDate);
        parcel.writeString(fileURL);
        parcel.writeInt(fileType);
        parcel.writeInt(documentRefId);
        parcel.writeString(patientId);
    }
}
