package com.mphrx.fisike.record_screen.request;

import com.android.volley.VolleyError;
import com.mphrx.fisike.enums.ReportTypeEnum;
import com.mphrx.fisike.record_screen.response.DiagnosticObservation;
import com.mphrx.fisike.record_screen.response.MicrobiologyObservation;
import com.mphrx.fisike.record_screen.response.MicrobiologyObservationCollection;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mphrx.fisike_physician.utils.BusProvider.getInstance;

/**
 * Copyright 2015 App Street Software Pvt. Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class DiagnosticObservationSearchRequest extends BaseObjectRequest {

    private final long mTransactionId;
    private final String mReportId ,mPatientId;
    private ReportTypeEnum reportType;

    public DiagnosticObservationSearchRequest(String reportId, long transactionId,
                                              ReportTypeEnum reportType, String patientId) {
        mTransactionId = transactionId;
        mReportId = reportId;
        this.reportType = reportType;
        this.mPatientId = patientId;
    }

    public long getmTransactionId() {
        return mTransactionId;
    }

    @Override
    public void doInBackground() {
        APIObjectRequest reportRequest = new APIObjectRequest(Request.Method.POST,
                MphRxUrl.getDiagnosticReportSearchPrimaryUrl(), getPrimaryPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(reportRequest);
    }

    private String getPrimaryPayLoad() {
        JSONObject payload = new JSONObject();
        try {
            JSONObject constraints = new JSONObject();
            constraints.put("_sort", "scheduledDate");
            constraints.put("id", Integer.parseInt(mReportId));
            constraints.put("extension:url", new JSONArray());
            JSONArray includeArray = new JSONArray();
            includeArray.put("DiagnosticReport:performer");
            includeArray.put("DiagnosticReport:subject");
            constraints.put("_include", includeArray);
            payload.put("constraints", constraints);
        } catch (JSONException ignore) {
        }
        return payload.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        switch(reportType){
            case LAB:
                BusProvider.getInstance().post(new DiagnosticObservation(error, mTransactionId, mPatientId));
                break;
            case MICROBIOLOGY:
                BusProvider.getInstance().post(new MicrobiologyObservationCollection(error, mTransactionId,mPatientId));
                break;

        }

    }

    @Override
    public void onResponse(JSONObject response) {
        if(response==null){
            VolleyError volleyError = new VolleyError();
            onErrorResponse(volleyError);
            BusProvider.getInstance().post(new DiagnosticObservation(volleyError, mTransactionId, mPatientId));
            return;
        }
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        switch(reportType){
            case LAB:
                BusProvider.getInstance().post(new DiagnosticObservation(response, mTransactionId ,mPatientId));
                break;
            case MICROBIOLOGY:
                BusProvider.getInstance().post(new MicrobiologyObservationCollection(response, mTransactionId,mPatientId));
                break;
        }
    }

}
