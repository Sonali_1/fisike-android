package com.mphrx.fisike.record_screen.request;

import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.record_screen.response.ObservationHistory;
import com.mphrx.fisike.volley.wrappers.Request;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike.record_screen.response.DiagnosticObservation;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by xmb2nc on 21-12-2015.
 */
public class ObservationDetailListingRequest extends BaseObjectRequest {
    // {"constraints":{"_count":5,"_skip":0,"_sort":"issued","subject":40,"identifier:text":"ParamId|mphrx-Glucose-004"}}

    private int count;
    private int skip;
    private String sortOrder;
    private String patientId;
    private String paramId;
    private long mTransactionId;
    private String vitalText;


    public ObservationDetailListingRequest(int count, int skip, String sortOrder, String patientId,
                                           String paramId, long mTransactionId, String vitalText) {
        this.count = count;
        this.skip = skip;
        this.sortOrder = sortOrder;
        this.patientId = patientId;
        this.paramId = paramId;
        this.mTransactionId = mTransactionId;
        this.vitalText = vitalText;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getObservationDetailListingUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new ObservationHistory(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        if(response==null){
            VolleyError error = new VolleyError();
            AppLog.showError(getClass().getSimpleName(), error.getMessage());
            BusProvider.getInstance().post(new ObservationHistory(error, mTransactionId));
        }
        if(response==null){
            VolleyError error = new VolleyError();
            AppLog.showError(getClass().getSimpleName(), error.getMessage());
            BusProvider.getInstance().post(new ObservationHistory(error, mTransactionId));
        }
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new ObservationHistory(response, mTransactionId, vitalText));
    }


    private String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        JSONObject constraintsObject = new JSONObject();
        try {
            payLoad.put("_count", count);
            payLoad.put("_skip", skip);
            payLoad.put("_sort:desc", sortOrder);
            payLoad.put("subject", patientId);

            if(BuildConfig.isPatientApp)
                payLoad.put("status", "F,C");
            else
                payLoad.put("status", "F,C,P");

            payLoad.put("identifier:text", "paramId" + "|" + paramId);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
            constraintsObject.put("constraints", payLoad);
        } catch (JSONException e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return constraintsObject.toString();
    }
}