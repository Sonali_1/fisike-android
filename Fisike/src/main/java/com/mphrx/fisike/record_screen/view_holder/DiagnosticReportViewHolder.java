package com.mphrx.fisike.record_screen.view_holder;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.RadiologyDetailScreen;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.activity.MicrobiologyActivity;
import com.mphrx.fisike.record_screen.activity.ObservationActivity;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ExpandableRecyclerView.viewholders.ChildViewHolder;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.List;

/**
 * Created by kailashkhurana on 24/07/17.
 */

public class DiagnosticReportViewHolder extends ChildViewHolder {
    private final Context context;
    private RelativeLayout layoutChild;
    private CustomFontTextView txtStatusKey;
    private CustomFontTextView txtDateKey;
    private LinearLayout layoutDate;
    public RelativeLayout layout_hide;
    public CustomFontTextView txt_test;
    public CustomFontTextView txt_test_status;
    public CustomFontTextView txt_expected_date;
    private OrderReportModel orderReportModel;
    private int childIndex;
    private IconTextView icon_arrow;

    public DiagnosticReportViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        icon_arrow = (IconTextView) itemView.findViewById(R.id.icon_arrow);
        txt_test = (CustomFontTextView) itemView.findViewById(R.id.txt_test);
        txt_test_status = (CustomFontTextView) itemView.findViewById(R.id.txt_test_status);
        txt_expected_date = (CustomFontTextView) itemView.findViewById(R.id.txt_expected_date);
        layout_hide = (RelativeLayout) itemView.findViewById(R.id.layout_hide);
        txtStatusKey = (CustomFontTextView) itemView.findViewById(R.id.txt_status_key);
        txtDateKey = (CustomFontTextView) itemView.findViewById(R.id.txt_date_key);
        layoutDate = (LinearLayout) itemView.findViewById(R.id.layout_date);
        layoutChild = (RelativeLayout) itemView.findViewById(R.id.layout_child);
    }

    @Override
    public void expand() {
        super.expand();
    }

    @Override
    public void collapse() {
        super.collapse();
    }

    public void setDiagnosticReport(OrderReportModel orderReportModel, int childIndex) {
        this.orderReportModel = orderReportModel;
        this.childIndex = childIndex;
        DiagnosticReport diagnosticReport = orderReportModel.getItems().get(childIndex);

        if (Utils.isRTL(context)) {
            icon_arrow.setRotationY(180);
        } else {
            icon_arrow.setRotationY(0);
        }

        String reportName = diagnosticReport.getReportName();
        txt_test.setText(Utils.isValueAvailable(reportName) ? reportName : txt_test.getResources().getString(R.string.unavailable));
        String reportStatus = diagnosticReport.getReportStatus();
        txt_test_status.setText(Utils.isValueAvailable(reportStatus) ? reportStatus : txt_test_status.getResources().getString(R.string.unavailable));
        String reportDate = diagnosticReport.getResultDate();
        String expectedDate = diagnosticReport.getExpectedDate();
        if (Utils.isValueAvailable(reportDate)) {
            txt_expected_date.setVisibility(View.VISIBLE);
            txtDateKey.setVisibility(View.VISIBLE);
            String destinationTimeFormat = DateFormat.is24HourFormat(txt_expected_date.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
            txt_expected_date.setText(DateTimeUtil.convertSourceDestinationDate(reportDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat));
        } else if (Utils.isValueAvailable(expectedDate)) {
            txt_expected_date.setVisibility(View.VISIBLE);
            txtDateKey.setVisibility(View.VISIBLE);
            if (Utils.compareDateWithPresent(reportDate)) {
                String destinationTimeFormat = DateFormat.is24HourFormat(txt_expected_date.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
                txt_expected_date.setText(DateTimeUtil.convertSourceDestinationDate(reportDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat));
            } else if (Utils.compareDateWithPresent(reportDate)) {
                txt_expected_date.setText(txt_test_status.getResources().getString(R.string.delayed));
                txt_expected_date.setTextColor(txt_expected_date.getResources().getColor(R.color.red));
            }
        } else {
            txt_expected_date.setVisibility(View.GONE);
            txtDateKey.setVisibility(View.GONE);

        }


        DiagnosticOrder order = orderReportModel.getTitle();

        int statusKey = order.getCategory().equalsIgnoreCase(txtStatusKey.getContext().getString(R.string.conditional_radiology)) ? R.string.rad_status_key : R.string.lab_status_key;
        txtStatusKey.setText(txtStatusKey.getResources().getString(statusKey));

        if (orderReportModel.getTitle().isPatientDeceased()) {
            layoutChild.setBackgroundColor(layoutChild.getResources().getColor(R.color.died_bg));
        } else {
            layoutChild.setBackgroundColor(layoutChild.getResources().getColor(R.color.white));
        }
    }

    @Override
    public void setRecordClick() {
        try {
            List<DiagnosticReport> items = orderReportModel.getItems();
            DiagnosticReport itemAtPosition = items.get(childIndex);
            DiagnosticOrder diagnosticOrder = orderReportModel.getTitle();
            if (diagnosticOrder.getCategory().equals(context.getString(R.string.conditional_radiology)))
                context.startActivity(new Intent(context, RadiologyDetailScreen.class).putExtra("id", itemAtPosition.getDiagnosticOrderId()).putExtra("token",
                        SharedPref.getAccessToken()));
            else {
                String status = itemAtPosition.getReportStatus();
                // check for microbiology
                if (itemAtPosition.getReportType().equals(TextConstants.MICROBIOLOGY_REPORT_TYPE)) {
                    context.startActivity(MicrobiologyActivity.newIntent(context, itemAtPosition.getReportId(),
                            itemAtPosition.getReportName(), diagnosticOrder.getOrganisationId(),
                            diagnosticOrder.getPhysicianName(), status == null ? "" : status,
                            diagnosticOrder.getPerformingLocationName(), diagnosticOrder.getPatientId(), diagnosticOrder.getId()));
                } else {
                    context.startActivity(ObservationActivity.newIntent(context, itemAtPosition.getReportId(),
                            itemAtPosition.getReportName(), diagnosticOrder.getOrganisationId(),
                            diagnosticOrder.getPhysicianName(), status == null ? "" : status,
                            diagnosticOrder.getPerformingLocationName(), itemAtPosition.getReportFormat(), diagnosticOrder.getPatientId(), diagnosticOrder.getId()));

                }
            }
        } catch (Exception ignore) {
            ignore.getMessage();
        }
        super.setRecordClick();
    }
}

