package com.mphrx.fisike.record_screen.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 7/15/2016.
 */
public class ObservationHistory extends BaseResponse implements Cloneable {

    private String title;
    private ArrayList<Observation> observations = new ArrayList<>();
    private String reportDate;
    private int documentRefId;
    private String vitalText;
    private String referenceRangeLow;
    private String referenceRangeHigh;
    private String referenceRangeUnit;


    public ObservationHistory(JSONObject response, long transactionId, String vitalText) {
        super(response, transactionId);
        this.vitalText = vitalText;
        doParse(response);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ObservationHistory(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }

    private void doParse(JSONObject response) {
        try {
            JSONArray observationList = response.optJSONArray("list");
            JSONObject referenceRange = response.optJSONObject("referencerange");
            setTitle(response.optString("title"));
            setReferenceRangeLow(referenceRange.optString("low"));
            setReferenceRangeHigh(referenceRange.optString("high"));
            setReferenceRangeUnit(referenceRange.optString("units"));
            for (int i = 0; observationList != null && i < observationList.length(); i++) {
                JSONObject observationListJSONObject = observationList.getJSONObject(i);
                Observation observation = new Observation();
                observation.setmInterpretation(observationListJSONObject.optString("interpretation"));
                JSONObject valueJson = observationListJSONObject.optJSONObject("value");
                String mValue = valueJson.optString("value");
                String mUnit = valueJson.optString("unit");
                if (vitalText != null && vitalText.equalsIgnoreCase("Glucose") && mUnit.equalsIgnoreCase("mmol/L")) {
                    float newValue = (float) (Float.parseFloat(mValue) * 18.0);
                    mValue = Math.round(newValue * 100.0) / 100.0 + "";
                }
                observation.setmValue(mValue);
                observation.setmUnit(mUnit);
                observation.setmIssueDate(observationListJSONObject.optString("date"));
                observations.add(observation);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getReferenceRangeLow() {
        return referenceRangeLow;
    }

    public void setReferenceRangeLow(String referenceRangeLow) {
        this.referenceRangeLow = referenceRangeLow;
    }

    public String getReferenceRangeHigh() {
        return referenceRangeHigh;
    }

    public void setReferenceRangeHigh(String referenceRangeHigh) {
        this.referenceRangeHigh = referenceRangeHigh;
    }

    public String getReferenceRangeUnit() {
        return referenceRangeUnit;
    }

    public void setReferenceRangeUnit(String referenceRangeUnit) {
        this.referenceRangeUnit = referenceRangeUnit;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Observation> getObservations() {
        return observations;
    }

    public String getReportDate() {
        return reportDate;
    }

    public int getDocumentRefId() {
        return documentRefId;
    }

    public void setDocumentRefId(int documentRefId) {
        this.documentRefId = documentRefId;
    }

    public String getVitalText() {
        return vitalText;
    }

    public void setVitalText(String vitalText) {
        this.vitalText = vitalText;
    }

    public void setObservations(ArrayList<Observation> observations) {
        this.observations = observations;
    }
}
