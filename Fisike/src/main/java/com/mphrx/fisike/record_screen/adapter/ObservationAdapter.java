package com.mphrx.fisike.record_screen.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.ObservationStatusEnum;
import com.mphrx.fisike.gson.request.Line;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Copyright 2015 App Street Software Pvt. Ltd.
 * <p/>
 * Licensed under the Apache License, Version -2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ObservationAdapter extends RecyclerView.Adapter<ObservationAdapter.ObservationViewHolder> {

    private Context context;
    private List<Observation> mObservations;
    private String vitalText, reportFormat;
    public clickListener listener;
    private List<String> observationHighLightList;
    boolean isHighlightListAvailable = false;


    public ObservationAdapter(Context context, List<Observation> observations, String vitalText,
                               String reportFormat) {
        this.context = context;
        mObservations = observations;
        this.vitalText = vitalText;
        this.reportFormat = reportFormat;
        String observationHighLightListString = SharedPref.getInterPretatRionListToHighLight();
        if (Utils.isValueAvailable(observationHighLightListString)) {
            observationHighLightList = new ArrayList<>(Arrays.asList(observationHighLightListString.replaceAll
                    (context.getString(R.string.open_Square_bracket), "").
                    replaceAll(context.getString(R.string.close_square_bracket), "").
                    split(context.getString(R.string.comma))));
            if (observationHighLightList != null && observationHighLightList.size() > 0)
                isHighlightListAvailable = true;
        }
    }


    @Override
    public ObservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_observation, parent, false);
        ObservationViewHolder observationViewHolder = new ObservationViewHolder(row);
        return observationViewHolder;
    }

    @Override
    public void onBindViewHolder(ObservationViewHolder holder, int position) {

        boolean isToShowViewDetails = istoShowViewDetails(mObservations.get(position));
        boolean isToShowViewTrends=istoShowViewTrend(mObservations.get(position));
        mObservations.get(position).setToShowViewDetails(isToShowViewDetails);
        mObservations.get(position).setToShowViewTrend(isToShowViewTrends);
        Observation observation = mObservations.get(position);
        if (observation.getmStyleCode() != null)
            displayResult(holder, observation, observation.getmStyleCode());
        else
            displayResult(holder, observation, "N");
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mObservations.size();
    }


    public static String boldText(String name) {
        return "<b>" + name + "</b>";
    }


    private void displayResult(ObservationViewHolder holder, Observation observation, String style) {
        boolean isStarBefore = false;
        boolean isStarAfter = false;


        if (style.equals("BOLD_ROW")) {
            holder.mName.setTypeface(null, Typeface.BOLD);
            holder.mValue.setTypeface(null, Typeface.BOLD);
            holder.mUnit.setTypeface(null, Typeface.BOLD);

        } else if (style.equals("BOLD_RESULT")) {
            holder.mValue.setTypeface(null, Typeface.BOLD);
            holder.mUnit.setTypeface(null, Typeface.BOLD);
        } else if (style.equals("BOLD_AFTER_RESULT")) {
            isStarAfter = true;
        } else if (style.equals("BOLD_BEFORE_RESULT")) {
            isStarBefore = true;
        } else {
            isStarAfter = false;
            isStarBefore = false;

        }

        holder.mName.setText(observation.getName());
        if (!Utils.isValueAvailable(observation.getValue())) {
            holder.mValue.setText("");
        } else
            holder.mValue.setText((isStarBefore ? "*" + observation.getValue().replaceFirst("^0+(?!$)", "") : (isStarAfter ? observation.getValue().replaceFirst("^0+(?!$)", "") + "*" : observation.getValue().replaceFirst("^0+(?!$)", ""))));
        if (!Utils.isValueAvailable(observation.getUnit())) {
            holder.mUnit.setText("");
        } else
            holder.mUnit.setText(observation.getUnit());


        if (isToHighLightObservation(observation.getInterpretation())) {

            holder.mValue.setTextColor(context.getResources().getColor(R.color.red_radical));
            holder.mUnit.setTextColor(context.getResources().getColor(R.color.red_radical));
        } else {
            holder.mValue.setTextColor(context.getResources().getColor(R.color.dusky_blue));
            holder.mUnit.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        }

        if(!observation.isToShowViewDetails() && !observation.isToShowViewTrend() || reportFormat.equals("Template")){
            holder.cfb_view_history.setVisibility(View.GONE);
        }else{
            holder.cfb_view_history.setVisibility(View.VISIBLE);
        }

    }


    /*  returns true if view Details is to be  shown in overflow,
    else returns false*/
    private boolean istoShowViewDetails(Observation observation) {
        if (observation == null) {
            return false;
        }
        // body site , observation method, interpretation, reference range, comments.
        if (Utils.isValueAvailable(observation.getBodySite()) ||
                Utils.isValueAvailable(observation.getObservationMethod()) ||
                (Utils.isValueAvailable(observation.getInterpretation()) &&
                        (!observation.getInterpretation().equals("-"))) ||
                Utils.isValueAvailable(observation.getDisplayableRange(vitalText)) ||
                Utils.isValueAvailable(observation.getComment()) || Utils.isValueAvailable(observation.getStatus())) {
            return true;
        }
        return false;

    }

    private boolean istoShowViewTrend(Observation observation) {
        if (observation == null) {
            return false;
        }
        //return true for corrected and final reports
        if (Utils.isValueAvailable(observation.getStatus()) &&
        (observation.getStatus().equalsIgnoreCase(context.getString(R.string.observation_status_key_c))
        || observation.getStatus().equalsIgnoreCase(context.getString(R.string.observation_status_key_f))))
        {
            return true;
        }
        //MNV-9872, open partial reports for physician
        else if(Utils.isValueAvailable(observation.getStatus())
        && BuildConfig.isPhysicianApp
        && observation.getStatus().equalsIgnoreCase(context.getString(R.string.observation_status_key_p)))
            return  true;

        return false;

    }


    private boolean isToHighLightObservation(String interpretation) {
        if (BuildConfig.isObservationOutofRangeHighlightEnabled && Utils.isValueAvailable(interpretation) && isHighlightListAvailable)
            if (observationHighLightList.contains(interpretation))
                return true;

        return false;
    }

    class ObservationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CustomFontTextView mName, mValue, mUnit;
        public CustomFontButton cfb_view_history;

        public ObservationViewHolder(View parent) {
            super(parent);
            mName = (CustomFontTextView) parent.findViewById(R.id.tv_name);
            mName.setSelected(true);
            mValue = (CustomFontTextView) parent.findViewById(R.id.tv_value);
            mUnit = (CustomFontTextView) parent.findViewById(R.id.tv_unit);
            cfb_view_history = (CustomFontButton) parent.findViewById(R.id.cfb_view_history);
            if(Utils.isValueAvailable(reportFormat) && reportFormat.equals("Template")){
                cfb_view_history.setVisibility(View.GONE);
            }  else {
                cfb_view_history.setOnClickListener(this);
                parent.setOnClickListener(this);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cfb_view_history:
                    if (listener != null) {
                        listener.itemClicked(v, getAdapterPosition(), false,mObservations.get(getAdapterPosition()).isToShowViewDetails(),mObservations.get(getAdapterPosition()).isToShowViewTrend());
                    }
                    break;

                default:
                    // launch the  history screen
                    if (listener != null) {
                        listener.itemClicked(v, getAdapterPosition(), true,mObservations.get(getAdapterPosition()).isToShowViewDetails(),mObservations.get(getAdapterPosition()).isToShowViewTrend());
                    }
                    break;
            }
        }
    }


    public interface clickListener {
        public void itemClicked(View view, int position, boolean isHistoryScreenNeedsToBeLaunched,boolean isToShowDetail, boolean isToShowViewTrend);
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;

    }

    public Observation getObservationAtPostition(int position) {
        return mObservations.get(position);
    }
}