package com.mphrx.fisike.record_screen.view_holder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.RadiologyDetailScreen;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.activity.MicrobiologyActivity;
import com.mphrx.fisike.record_screen.activity.ObservationActivity;
import com.mphrx.fisike.record_screen.adapter.ResultExpandAdapter;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ExpandableRecyclerView.models.ExpandableGroup;
import com.mphrx.fisike.view.ExpandableRecyclerView.viewholders.GroupViewHolder;
import com.mphrx.fisike_physician.activity.HealthActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.List;

import searchpatient.activity.SearchPatientActivity;
import searchpatient.model.SearchPatientResponseModel;

public class DiagnosticOrderViewHolder extends GroupViewHolder {

    private final Context context;
    private CardView cardView;
    private ImageView imgReportType;
    public RelativeLayout layoutShow;
    private CustomFontTextView tvTitle;
    private CustomFontTextView tvBornTitle;
    private CustomFontTextView tvBornValue;
    private CustomFontTextView tvIdTitle;
    private CustomFontTextView tvIdValue;
    private CustomFontTextView tvCell;
    private CustomFontTextView tvOrderStatus;
    private CustomFontTextView tvOrderNumber;
    private CustomFontTextView tvOrderDate;
    private CustomFontTextView tvPhysician;
    private CustomFontTextView tvLocation;
    private IconTextView imgOverflow;
    public DownloadListner listener;
    private LinearLayout layoutPhysician;
    private LinearLayout layoutOrderNumber;
    private LinearLayout layoutLocation;
    private ResultExpandAdapter resultExpandAdapter;
    private OrderReportModel orderReportModel;


    public DiagnosticOrderViewHolder(View itemView, ResultExpandAdapter resultExpandAdapter) {
        super(itemView);
        context = itemView.getContext();
        tvTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_title);
        cardView = (CardView) itemView.findViewById(R.id.card_view1);
        tvBornTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_born_title);
        tvIdTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_id_title);
        tvBornValue = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_born_value);
        tvIdValue = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_id_value);
        tvCell = (CustomFontTextView) itemView.findViewById(R.id.tv_cell);
        tvOrderStatus = (CustomFontTextView) itemView.findViewById(R.id.tv_order_status);
        tvOrderNumber = (CustomFontTextView) itemView.findViewById(R.id.tv_order_number);
        tvOrderDate = (CustomFontTextView) itemView.findViewById(R.id.tv_order_date);
        tvPhysician = (CustomFontTextView) itemView.findViewById(R.id.tv_order_physician);
        tvLocation = (CustomFontTextView) itemView.findViewById(R.id.tv_location);
        layoutShow = (RelativeLayout) itemView.findViewById(R.id.layout_show);
        imgReportType = (ImageView) itemView.findViewById(R.id.img_report_type);
        imgOverflow = (IconTextView) itemView.findViewById(R.id.imgOverflow);
        layoutPhysician = (LinearLayout) itemView.findViewById(R.id.layout_physician);
        layoutOrderNumber = (LinearLayout) itemView.findViewById(R.id.layout_order_number);
        layoutLocation = (LinearLayout) itemView.findViewById(R.id.layout_location);
        this.resultExpandAdapter = resultExpandAdapter;
    }

    public void setGenreTitle(final ExpandableGroup genre) {
        if (genre instanceof OrderReportModel) {
            this.orderReportModel = (OrderReportModel) genre;
            tvTitle.setText(genre.getTitle().getPatientName());

            String patientId = genre.getTitle().getPatientId();

            tvIdTitle.setText(tvIdTitle.getContext().getString(R.string.id) + " ");
            String patientInfo1 = "";
            if (Utils.isValueAvailable(patientId)) {
                patientInfo1 = " " + genre.getTitle().getPatientId();
            } else {
                patientInfo1 = " " + tvIdTitle.getContext().getString(R.string.unknown);
            }
            tvIdValue.setText(patientInfo1);
            tvCell.setText(genre.getTitle().getItem());
            String status = genre.getTitle().getStatus();
            tvOrderStatus.setText(Utils.isValueAvailable(status) ? status : tvOrderStatus.getResources().getString(R.string.unavailable));
            String orderNo = genre.getTitle().getOrderNo();
            if (Utils.isValueAvailable(orderNo)) {
                layoutOrderNumber.setVisibility(View.VISIBLE);
                tvOrderNumber.setText(orderNo);
            } else {
                layoutOrderNumber.setVisibility(View.GONE);
            }
            String physicianName = genre.getTitle().getPhysicianName();
            if (Utils.isValueAvailable(physicianName)) {
                layoutPhysician.setVisibility(View.VISIBLE);
                tvPhysician.setText(physicianName);
            } else {
                layoutPhysician.setVisibility(View.GONE);
            }
            String performingLocationName = genre.getTitle().getPerformingLocationName();
            if (Utils.isValueAvailable(performingLocationName)) {
                tvLocation.setText(performingLocationName);
                layoutLocation.setVisibility(View.VISIBLE);
            } else {
                layoutLocation.setVisibility(View.GONE);
            }
            if (genre.getTitle().getCategory().equals(tvOrderDate.getContext().getString(R.string.conditional_radiology))) {
                imgReportType.setBackgroundResource(R.drawable.rad_report);
            } else {
                imgReportType.setBackgroundResource(R.drawable.lab_report);
            }

            if (genre.getTitle().getDiagnosticReportList() == null || genre.getTitle().getDiagnosticReportList().size() <= 0) {
                layoutShow.setVisibility(View.INVISIBLE);
            } else {
                layoutShow.setVisibility(View.VISIBLE);
            }

            String scheduledDate = genre.getTitle().getScheduledDate();
            String destinationTimeFormat = DateFormat.is24HourFormat(imgOverflow.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
            tvOrderDate.setText(Utils.isValueAvailable(scheduledDate) ? DateTimeUtil.convertSourceDestinationDate(scheduledDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat) : tvOrderDate.getResources().getString(R.string.unavailable));


            imgOverflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.itemClicked(genre.getTitle().getId(), imgOverflow);
                    }
                }
            });

            if (genre.getTitle().isPatientDeceased()) {
                cardView.setBackgroundColor(cardView.getResources().getColor(R.color.died_bg));
                tvBornTitle.setText(tvBornTitle.getContext().getString(R.string.txt_patient_died) + " ");
                String patientInfo = "";
                String patientDied = genre.getTitle().getPatientDeceasedDate();

                if (Utils.isValueAvailable(patientDied)) {
                    try {
                        patientInfo = " " + DateTimeUtil.getFormattedDateWithoutTime(patientDied, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                    } catch (Exception e) {

                    }
                } else {
                    patientInfo = " " + tvTitle.getContext().getString(R.string.unknown);
                }
                tvBornValue.setText(patientInfo);
            } else {
                tvBornTitle.setText(tvBornTitle.getContext().getString(R.string.txt_born) + " ");
                String patientInfo = "";
                String patientDob = genre.getTitle().getPatientDob();

                if (Utils.isValueAvailable(patientDob)) {
                    try {
                        String age = Utils.calculateAgeAsPerConstraints(patientDob, null, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, false);
                        patientInfo = " " + DateTimeUtil.getFormattedDateWithoutTime(patientDob, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z) + "(" + age + ")";
                    } catch (Exception e) {

                    }
                } else {
                    patientInfo = " " + tvTitle.getContext().getString(R.string.unknown);
                }
                tvBornValue.setText(patientInfo);
                cardView.setBackgroundColor(cardView.getResources().getColor(R.color.white));
            }

        }
    }

    @Override
    public void setPatientInfoClick() {
        try {
            if (getAdapterPosition() > resultExpandAdapter.totalCount) {
                System.out.println("Adapter Clicked not postion : " + getAdapterPosition());
            }
            System.out.println("Adapter Clicked Actual : " + getAdapterPosition());
            context.startActivity(new Intent(context, HealthActivity.class)
                    .putExtra("title", orderReportModel.getTitle().getPatientName())
                    .putExtra("patientId", String.valueOf(orderReportModel.getTitle().getPatientMogoId())));
        } catch (Exception ignore) {
            ignore.getMessage();
        }
        super.setPatientInfoClick();
    }

    public void setListener(DownloadListner listener) {
        this.listener = listener;
    }

    @Override
    public void expand() {
        System.out.println("Adapter Clicked Actual : " + getAdapterPosition());
        layoutShow.setVisibility(View.INVISIBLE);
    }

    @Override
    public void collapse() {
        layoutShow.setVisibility(View.VISIBLE);
    }


    public interface DownloadListner {
        public void itemClicked(String orderId, IconTextView imgOverFlow);
    }
}
