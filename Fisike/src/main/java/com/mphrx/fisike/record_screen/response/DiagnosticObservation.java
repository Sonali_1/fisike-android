package com.mphrx.fisike.record_screen.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Kailash Khurana
 *         <p/>
 *         This is the response for the Lab report/ vital with all the observation in that
 */
public class DiagnosticObservation extends BaseResponse {

    private String title;
    private final ArrayList<Observation> observations = new ArrayList<>();
    private String remark;
    private String patientName;
    private String gender;
    private String dateofBirth;

    private String reportDate;
    private String fileURL;
    private int fileType;
    private int documentRefId;
    private String patientId;
    private String patientPk;


    public DiagnosticObservation(JSONObject response, long transactionId, String mPatientId) {
        super(response, transactionId);
        patientId =mPatientId;
        doParse(response);
    }

    public DiagnosticObservation(VolleyError error, long mTransactionId,String mPatientId) {
        super(error, mTransactionId);
        patientId= mPatientId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    private void doParse(JSONObject response) {
        try {
            JSONArray observationList = response.optJSONArray("observationList");
            for (int i = 0; observationList != null && i < observationList.length(); i++) {
                JSONObject observationListJSONObject = observationList.getJSONObject(i);
                Observation observation = new Observation();
                observation.setmInterpretation(observationListJSONObject.optString("interpretation"));
                observation.setmName(observationListJSONObject.optString("testType"));
                JSONArray identifier = observationListJSONObject.optJSONArray("identifier");
                for (int index = 0; identifier != null && identifier.length() > 0; index++) {
                    if (identifier.optJSONObject(index).optString("text").equalsIgnoreCase("paramId")) {
                        observation.setmParamId(identifier.optJSONObject(index).optString("paramId"));
                        break;
                    }
                }
                observation.setmValue(observationListJSONObject.optString("value"));
                observation.setmUnit(observationListJSONObject.optString("unit"));
                observation.setmStyleCode(observationListJSONObject.optString("styleCode"));
                observation.setStatus(observationListJSONObject.optString("status"));
                JSONObject referenceRange = observationListJSONObject.optJSONObject("referenceRange");
                observation.setmReferenceLow(referenceRange.optString("low"));
                observation.setmReferenceHigh(referenceRange.optString("high"));
                observation.setmPatientId(patientId);
                observations.add(observation);
            }
            remark = response.optString("remarks");
            patientName = response.optString("patientName");
            dateofBirth = response.optString("patientDob");
            gender = response.optString("patientSex");
            reportDate = response.optString("reportDate");
            this.patientId= patientId;
            patientPk = response.getString("patientPk");
            fileURL = response.optString("pdfUrl");
            //supporting file type pdf for
            fileType = VariableConstants.MIME_TYPE_FILE;
            if(BuildConfig.isFisike)
                fileType = VariableConstants.MIME_TYPE_UNKNOWN;
            if (Utils.isValueAvailable(fileURL) && fileURL.contains(".")) {
                String contentType = fileURL.substring(fileURL.lastIndexOf(".") + 1, fileURL.length());
                if (contentType != null || !contentType.equalsIgnoreCase("null")) {
                    if (contentType.equalsIgnoreCase("pdf")) {
                        fileType = VariableConstants.MIME_TYPE_FILE;
                    } else if (contentType.equalsIgnoreCase("doc") || contentType.equalsIgnoreCase("docx")) {
                        fileType = VariableConstants.MIME_TYPE_DOC;
                    } else {
                        fileType = VariableConstants.MIME_TYPE_ZIP;
                    }
                }
            }
            documentRefId= response.optInt("id");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Observation> getObservations() {
        return observations;
    }

    public String getRemark() {
        return remark;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getGender() {
        return gender;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public String getReportDate() {
        return reportDate;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getDocumentRefId() {
        return documentRefId;
    }

    public String getPatientPk() {
        return patientPk;
    }

    public void setPatientPk(String patientPk) {
        this.patientPk = patientPk;
    }
}