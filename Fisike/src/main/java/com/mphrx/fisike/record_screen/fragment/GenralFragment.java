package com.mphrx.fisike.record_screen.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.interfaces.EndlessScrollListener;
import com.mphrx.fisike.persistence.DiagnosticOrderDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.provider.MySuggestionProvider;
import com.mphrx.fisike.record_screen.adapter.ResultExpandAdapter;
import com.mphrx.fisike.record_screen.adapter.ResultListAdapter;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.record_screen.request.LoadRecordsRequest;
import com.mphrx.fisike.record_screen.response.LoadRecordsResponse;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.EndlessScrollView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.WrapContentLinearLayoutManager;
import com.mphrx.fisike_physician.activity.HealthActivity;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.results.Observer.MakeApiCallObserver;
import com.mphrx.fisike_physician.results.activity.ResultViewActivity;
import com.mphrx.fisike_physician.results.fragment.SearchParaments;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import searchpatient.constants.KeyConstants;

@SuppressLint("ResourceAsColor")
public class GenralFragment extends SearchParaments implements OnScrollListener, OnClickListener, OnRefreshListener, EndlessScrollListener, Observer {
    private static final int OBSERVATION_SELECT = 1111;
    public static final int RESULT_SEARCH_QUERY = 1000;

    private View myFragmentView;
    protected boolean isScrollDownError = false;
    private int totalCount = -1;

    private View loadingView;
    private boolean isLoadMoreConnectionError;

    private boolean loadingMore;
    private LinkedHashMap<String, DiagnosticOrder> records;
    private ArrayList<OrderReportModel> arrayListRecords;
    private CustomFontTextView txtNoMoreHistory;
    private CustomFontTextView txtNoMoreHistory1;
    private RelativeLayout no_record_view;

    private RecyclerView recordListView;
    private ResultListAdapter adapter;
    private EndlessScrollView myScrollView;

    private boolean isAllRecordLoaded;
    private LinearLayout countView;
    private boolean isRegisteredBroadcast;
    private long transactionId = System.currentTimeMillis();

    private RelativeLayout layoutSortOption;
    private RadioButton rbDescending;
    private RadioButton rbAscending;
    private RadioButton rbLastUpdatedAsc;
    private RadioButton rbLastUpdatedDesc;


    public GenralFragment() {

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.search_record_list, container, false);


        findView();

        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        totalCount = sharedPreferences.getInt(SharedPreferencesConstant.TOTAL_RECORD, -1);
        syncTime = sharedPreferences.getString(SharedPreferencesConstant.RECORD_SYNC_TIME, null);

        initView();

        /*if (getArguments() != null && getArguments().containsKey(VariableConstants.DIGITIZATION_REMINDER)) {
            notificationWithId(sharedPreferences);
        }*/
        registerBoadCast();

        return myFragmentView;
    }

    private void findView() {
        if (BuildConfig.isPatientApp) {
            //  ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle();
            ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.txt_my_health));
            myFragmentView.findViewById(R.id.card_header).setVisibility(View.GONE);
        } else {
            if (getObject()) {
                myFragmentView.findViewById(R.id.card_header).setVisibility(View.VISIBLE);
                if (!isAnyDataEntered()) {
                    myFragmentView.findViewById(R.id.card_header).findViewById(R.id.iv_filter).setBackgroundResource(R.drawable.filter_search_patient_normal);
                    ((CustomFontTextView) myFragmentView.findViewById(R.id.card_header).findViewById(R.id.tv_filter)).setTextColor(getResources().getColor(R.color.dusky_blue));
                }
            } else {
                myFragmentView.findViewById(R.id.card_header).setVisibility(View.GONE);
            }
        }


        layoutSortOption = (RelativeLayout) myFragmentView.findViewById(R.id.layout_sort_option);
        rbDescending = (RadioButton) myFragmentView.findViewById(R.id.rb_descending);
        rbAscending = (RadioButton) myFragmentView.findViewById(R.id.rb_ascending);
        rbLastUpdatedAsc = (RadioButton) myFragmentView.findViewById(R.id.rb_last_updated_asc);
        rbLastUpdatedDesc = (RadioButton) myFragmentView.findViewById(R.id.rb_last_updated_desc);
        if (SharedPref.getResultFilter() == VariableConstants.RESULT_ORDERDATE_ASC) {
            rbAscending.setChecked(true);
            rbDescending.setChecked(false);
            rbLastUpdatedAsc.setChecked(false);
            rbLastUpdatedDesc.setChecked(false);
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_ORDERDATE_DESC) {
            rbAscending.setChecked(false);
            rbDescending.setChecked(true);
            rbLastUpdatedAsc.setChecked(false);
            rbLastUpdatedDesc.setChecked(false);
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_LAST_UPDATED_ASC) {
            rbAscending.setChecked(false);
            rbDescending.setChecked(false);
            rbLastUpdatedAsc.setChecked(true);
            rbLastUpdatedDesc.setChecked(false);
        } else if (SharedPref.getResultFilter() == VariableConstants.RESULT_LAST_UPDATED_DESC) {
            rbAscending.setChecked(false);
            rbDescending.setChecked(false);
            rbLastUpdatedAsc.setChecked(false);
            rbLastUpdatedDesc.setChecked(true);
        }
    }

    private void registerBoadCast() {
        if (isRegisteredBroadcast) {
            return;
        }
        getActivity().registerReceiver(timeZoneChangedBroadcast, new IntentFilter(VariableConstants.TIME_ZONE_CHANGED));
        isRegisteredBroadcast = true;
    }


    private void initView() {
//        fabMenu = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);
//
//        if (BuildConfig.isPatientApp) {
//
//            if (!BuildConfig.isMedicationEnabled)
//                fabMenu.setVisibility(View.GONE);
//        }

        if (getActivity().findViewById(BaseActivity.RECORDS_ENTER_DETAILS) != null) {
            getActivity().findViewById(BaseActivity.RECORDS_ENTER_DETAILS).setOnClickListener(this);
        }

        myScrollView = (EndlessScrollView) myFragmentView.findViewById(R.id.scroll_view);
        myScrollView.setScrollViewListener(this);


        records = new LinkedHashMap<String, DiagnosticOrder>();
        arrayListRecords = new ArrayList<OrderReportModel>();
        searchParam = new ArrayList<String>();

        txtNoMoreHistory = (CustomFontTextView) myFragmentView.findViewById(R.id.txtNoMoreHistory);
        txtNoMoreHistory1 = (CustomFontTextView) myFragmentView.findViewById(R.id.txtNoMoreHistory1);
        no_record_view = (RelativeLayout) myFragmentView.findViewById(R.id.no_record_view);

        recordListView = (RecyclerView) myFragmentView.findViewById(R.id.searchlistView);
//        linearLayoutManager = new LinearLayoutManager(getActivity());
        recordListView.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
        recordListView.setNestedScrollingEnabled(false);
        loadingView = myFragmentView.findViewById(R.id.record_footer_view);
        countView = (LinearLayout) myFragmentView.findViewById(R.id.countView);

        ((CustomFontTextView) loadingView.findViewById(R.id.tv_footer_text)).setText(getResources().getString(R.string.txt_hangon_fetching_records));

        adapter = new ResultListAdapter(getActivity(), arrayListRecords);

        recordListView.setAdapter(adapter);
        if (getActivity() instanceof ResultViewActivity) {
            adapter.setListener((ResultViewActivity) getActivity());
        } else if (getActivity() instanceof HomeActivity) {
            adapter.setListener((HomeActivity) getActivity());
        }else if(getActivity() instanceof HealthActivity){
            adapter.setListener((HealthActivity) getActivity());
        }

        swipeRefreshLayout = (SwipeRefreshLayout) myFragmentView.findViewById(R.id.swipe_container);

        // sets the colors used in the refresh animation
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);

        swipeRefreshLayout.setOnRefreshListener(this);

        setPullToRefresh(true);

        if (!isLoadingContacts && BuildConfig.isPatientApp) {
            if (!SharedPref.isToClearRecords())
                new FetchAllRecordsDB().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else {
                //in the case of clearing records if pending records are synced and we request for final records. FIS-9170-Aastha
                new DatabaseAsyncTask(SharedPref.isToClearRecords(), DatabaseTables.PATIENT_RECORD_TABLE).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                SharedPref.setClearRecords(false);
                Network.getGeneralRequestQueue().getCache().clear();
                fetchPaginationResult();
            }

        } else if (BuildConfig.isPhysicianApp && !isLoadingContacts) {
            Network.getGeneralRequestQueue().getCache().clear();
            fetchPaginationResult();
        }
        // if not loading contacts &&
        // sync time has expire or all records are not loaded
        if (BuildConfig.isPatientApp && !isLoadingContacts && (isSyncTimeExpired())) {
            Network.getGeneralRequestQueue().getCache().clear();
            fetchSyncResult();
        }

        rbDescending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbAscending.setChecked(false);
                    rbLastUpdatedAsc.setChecked(false);
                    rbLastUpdatedDesc.setChecked(false);
                    layoutSortOption.setVisibility(View.GONE);
                    SharedPref.setResultSortOrder(KeyConstants.RESULT_FILTER, VariableConstants.RESULT_ORDERDATE_DESC);
                    arrayListRecords.removeAll(arrayListRecords);
                    records.clear();
                    adapter.notifyDataSetChanged();
                    setEmptyView(false);
                    fetchPaginationResult();
                }
            }
        });

        rbAscending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbDescending.setChecked(false);
                    rbLastUpdatedAsc.setChecked(false);
                    rbLastUpdatedDesc.setChecked(false);
                    layoutSortOption.setVisibility(View.GONE);
                    SharedPref.setResultSortOrder(KeyConstants.RESULT_FILTER, VariableConstants.RESULT_ORDERDATE_ASC);
                    arrayListRecords.removeAll(arrayListRecords);
                    records.clear();
                    adapter.notifyDataSetChanged();
                    setEmptyView(false);
                    fetchPaginationResult();
                }

            }
        });
        rbLastUpdatedAsc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbDescending.setChecked(false);
                    rbAscending.setChecked(false);
                    rbLastUpdatedDesc.setChecked(false);
                    layoutSortOption.setVisibility(View.GONE);
                    SharedPref.setResultSortOrder(KeyConstants.RESULT_FILTER, VariableConstants.RESULT_LAST_UPDATED_ASC);
                    noRecordsFoundView();
                    arrayListRecords.removeAll(arrayListRecords);
                    records.clear();
                    adapter.notifyDataSetChanged();
                    setEmptyView(false);
                    fetchPaginationResult();
                }
            }
        });
        rbLastUpdatedDesc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbDescending.setChecked(false);
                    rbAscending.setChecked(false);
                    rbLastUpdatedAsc.setChecked(false);
                    layoutSortOption.setVisibility(View.GONE);
                    SharedPref.setResultSortOrder(KeyConstants.RESULT_FILTER, VariableConstants.RESULT_LAST_UPDATED_DESC);
                    noRecordsFoundView();
                    arrayListRecords.removeAll(arrayListRecords);
                    records.clear();
                    adapter.notifyDataSetChanged();
                    setEmptyView(false);
                    fetchPaginationResult();
                }
            }
        });

        layoutSortOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSortOption.setVisibility(View.GONE);
            }
        });
    }

    private void fetchSyncResult() {
        if (!isLoadingContacts ) {
            isLoadingContacts = true;
            ThreadManager.getDefaultExecutorService().submit(
                    new LoadRecordsRequest(getTransactionId(), paginationSyncURL(), syncJson().toString(), true, false, true));
        }
    }

    public void fetchPaginationResult() {
        if (!isLoadingContacts) {
            isLoadingContacts = true;
            ThreadManager.getDefaultExecutorService().submit(
                    new LoadRecordsRequest(getTransactionId(), paginationSyncURL(), paginationJson(records == null ? 0 : records.size()).toString(), false, false, true));
        }
    }


    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
        int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached
        if (distanceToEnd < 20) {
            // do stuff your load more stuff
            if (isAllRecordLoaded) {
                return;
            }

            if (!(loadingMore)) {
                if (!isLoadingContacts) {
                    nextPage(true, isLoadMoreConnectionError);
                    if (!Utils.isNetworkAvailable(getActivity())) {
                        isLoadMoreConnectionError = true;
                    } else {
                        isLoadMoreConnectionError = false;
                    }
                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isSyncAfteDigitizationDone = sharedPreferences.getBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION, true);
        if (!isSyncAfteDigitizationDone) {
            if (syncTime == null && !isLoadingContacts) {
                fetchPaginationResult();
            } else {
                // sync call
                advanceSearch();
            }
        }

        MakeApiCallObserver.getInstance().addObserver(this);
    }

    @Override
    public void onPause() {
        MakeApiCallObserver.getInstance().deleteObserver(this);
        super.onPause();
    }

    public long getTransactionId() {
        return transactionId;
    }

    @Override
    public void update(Observable observable, Object o) {
        queryJsonObject = null;
        setPullToRefresh(true);
        isToRefresh = true;
        records.clear();
        arrayListRecords.removeAll(arrayListRecords);
        if (!isLoadingContacts) {
            fetchPaginationResult();
        }

    }

    private class FetchAllRecordsDB extends AsyncTask<Void, Void, LinkedHashMap<String, DiagnosticOrder>> {
        private ProgressDialog progressDialog;
        private DiagnosticOrderDBAdapter diagnosticOrderDBAdapter;
        private boolean isToRefresh;

        public FetchAllRecordsDB(boolean isToRefresh) {
            this.isToRefresh = isToRefresh;
        }

        public FetchAllRecordsDB() {

        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
            diagnosticOrderDBAdapter = (DiagnosticOrderDBAdapter) DiagnosticOrderDBAdapter.getInstance(getActivity());
            super.onPreExecute();
        }

        @Override
        protected LinkedHashMap<String, DiagnosticOrder> doInBackground(Void... voids) {
            try {
                return diagnosticOrderDBAdapter.fetchDiagnosticOrderArray();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(LinkedHashMap<String, DiagnosticOrder> arrayFetchDiagonsticOrder) {
            if (isToRefresh) {
                if (records != null) {
                    records.clear();
                }
                if (arrayListRecords != null) {
                    arrayListRecords.clear();
                }
            }
            progressDialog.dismiss();
            if (arrayFetchDiagonsticOrder == null || arrayFetchDiagonsticOrder.size() <= 0) {
                if (!isLoadingContacts) {
                    fetchPaginationResult();
                }
                return;
            }

            records.putAll(arrayFetchDiagonsticOrder);
            advanceSearch();

            ArrayList<DiagnosticOrder> temp_list = new ArrayList<DiagnosticOrder>(arrayFetchDiagonsticOrder.values());
            for (int i = 0; i < temp_list.size(); i++) {
                DiagnosticOrder diagnosticOrder = temp_list.get(i);
                OrderReportModel orderReportModel = new OrderReportModel(diagnosticOrder, diagnosticOrder.getDiagnosticReportList(), diagnosticOrder.getOrderId());
                arrayListRecords.add(orderReportModel);
            }

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(SharedPreferencesConstant.RECORD_PAGE_NUMBER, records.size());
            editor.commit();

            totalCount = sharedPreferences.getInt(SharedPreferencesConstant.TOTAL_RECORD, -1);


            if (records.size() >= totalCount) {
                //   loadingView.setVisibility(View.GONE);
                //Remove loading item
                removeFotterView();
            }

            if (getActivity() == null) {
                return;
            }


            updateUIRecordAdapter();

            checkDigitizationRecord();

            super.onPostExecute(arrayFetchDiagonsticOrder);
        }
    }

    private void checkDigitizationRecord() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);

        if (getArguments() != null && getArguments().containsKey(VariableConstants.DIGITIZATION_REMINDER)) {
            int precription = (getArguments().getInt(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION));

            for (int i = 0; arrayListRecords != null && i < arrayListRecords.size(); i++) {
                OrderReportModel orderReportModel = arrayListRecords.get(i);
                if (precription == Integer.parseInt(orderReportModel.getTitle().getId())) {
                    return;
                }
            }

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION, false);
            editor.commit();
        }
    }

    // check whether to sync or not

    private boolean isSyncTimeExpired() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        long val = sharedPreferences.getLong(SharedPreferencesConstant.TIME_FOR_LOAD_RECORDS, 0);
        if (val == 0) {
            return false;
        }
        if ((System.currentTimeMillis() - val >= TimeUnit.DAYS.toMillis(1))) {
            return true;
        }
        return false;
    }


    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        boolean enable = false;
        if (view != null && view.getChildCount() > 0) {
            // check if the first item of the list is visible
            boolean firstItemVisible = view.getFirstVisiblePosition() > 0;
            // check if the top of the first item is visible
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() < view.getPaddingTop();
            // enabling or disabling the refresh layout
            enable = firstItemVisible || topOfFirstItemVisible;
        }
        setPullToRefresh(!enable);

        if (isAllRecordLoaded) {
            return;
        }

        if (firstVisibleItem == visibleItemCount) {
            return;
        }

        if (((firstVisibleItem + visibleItemCount) == totalItemCount) && !(loadingMore)) {
            if (firstVisibleItem != 0 && !isLoadingContacts) {
                nextPage(true, isLoadMoreConnectionError);
                if (!Utils.isNetworkAvailable(getActivity())) {
                    isLoadMoreConnectionError = true;
                } else {
                    isLoadMoreConnectionError = false;
                }
            }
        }

    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    private void setPullToRefresh(boolean isToRefresh) {
        swipeRefreshLayout.setEnabled(BuildConfig.isPatientApp ? isToRefresh : false);
    }


    private void nextPage(boolean isOnclick, boolean isLoadMoreConnectionError) {
        if (!isLoadingContacts && !isLoadMoreConnectionError) {
            isLoadingContacts = true;
            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                ThreadManager.getDefaultExecutorService().submit(
                        new LoadRecordsRequest(getTransactionId(), paginationSyncURL(), paginationJson(records == null ? 0 : records.size()).toString(), false, false, isOnclick));
            }
        }
    }

    private void loadJsonData(boolean isSyncApi, boolean isObservationSubmitApi, boolean isOnclick, String jsonString) {
        isLoadingContacts = false;
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
            isToRefresh = false;
        }
        try {
            removeFotterView();

            if (jsonString != null && !(jsonString.equals(""))) {
                loadRecords(isSyncApi, isObservationSubmitApi, jsonString);

                if (totalCount == 0) {
                    //Remove loading item
                    removeFotterView();
                    if (records.size() > 0) {
                        if (!isSyncApi)
                            Toast.makeText(getActivity(), getResources().getString(R.string.done_loading), Toast.LENGTH_SHORT).show();
                    }
                } else if (totalCount <= records.size()) {
                    isAllRecordLoaded = true;
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(SharedPreferencesConstant.ALL_RECORDS_LOADED, true);
                    editor.commit();

                    //Remove loading item
                    if (arrayListRecords.size() != 0) {
                        if (arrayListRecords.get(arrayListRecords.size() - 1) == null) {
                            arrayListRecords.remove(arrayListRecords.size() - 1);
                            adapter.notifyItemRemoved(arrayListRecords.size());
                        }
                    }
                    if (!isSyncApi) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.done_loading), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loadingView.setVisibility(View.GONE);
                }

            } else if (records.size() <= 0) {
                //Remove loading item
                if (arrayListRecords.size() != 0) {
                    if (arrayListRecords.get(arrayListRecords.size() - 1) == null) {
                        arrayListRecords.remove(arrayListRecords.size() - 1);
                        adapter.notifyItemRemoved(arrayListRecords.size());
                    }
                }
                handleError(isOnclick);
                Toast.makeText(GenralFragment.this.getActivity(), getResources().getString(R.string.done_loading), Toast.LENGTH_SHORT).show();
                countView.setVisibility(View.GONE);
                noRecordsFoundView();
            }
        } catch (Exception e) {
            countView.setVisibility(View.GONE);

        }

        if (!Utils.isNetworkAvailable(GenralFragment.this.getActivity())) {
            //Remove loading item
            if (arrayListRecords.size() != 0) {
                if (arrayListRecords.get(arrayListRecords.size() - 1) == null) {
                    arrayListRecords.remove(arrayListRecords.size() - 1);
                    adapter.notifyItemRemoved(arrayListRecords.size());
                }
            }
        }
        addFooterView();
    }

    private void noRecordsFoundView() {
        if (arrayListRecords == null || arrayListRecords.size() == 0) {
            setEmptyView(true);
            if (BuildConfig.isPhysicianApp) {
                if (!isAdvanceSearchQuery()) {
                    txtNoMoreHistory.setText(R.string.patient_still_to_add);
                } else {
                    txtNoMoreHistory.setText(R.string.no_search_found);
                }
            } else if (isAdvanceSearchQuery() || !BuildConfig.isMedicationEnabled) {
                txtNoMoreHistory.setText(R.string.no_search_found);
            } else {
                txtNoMoreHistory.setText(R.string.no_records_found);
                txtNoMoreHistory1.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setEmptyView(boolean isEmptyView) {
        if (isEmptyView) {
            no_record_view.setVisibility(View.VISIBLE);
            loadingView.setVisibility(View.GONE);
            txtNoMoreHistory.setVisibility(View.VISIBLE);
            txtNoMoreHistory1.setVisibility(View.INVISIBLE);
        } else {
            no_record_view.setVisibility(View.GONE);
            loadingView.setVisibility(View.VISIBLE);
            txtNoMoreHistory.setVisibility(View.GONE);
            txtNoMoreHistory1.setVisibility(View.GONE);
        }
    }

    private void removeFotterView() {
        if (arrayListRecords.size() != 0) {
            myFragmentView.findViewById(R.id.record_footer_view1).setVisibility(View.GONE);
        }
    }

    private String paginationSyncURL() {
        APIManager apiManager = APIManager.getInstance();
        return apiManager.getDiagnosticOrderApi(getActivity());
    }


    @Subscribe
    public void onLoadRecordsLoaded(LoadRecordsResponse loadRecordsResponse) {

        if (getTransactionId() != loadRecordsResponse.getTransactionId())
            return;

        loadJsonData(loadRecordsResponse.isSyncApi(), loadRecordsResponse.isObservationSubmitApi(), loadRecordsResponse.isOnclick(), loadRecordsResponse.getJsonString());
    }


    private void loadRecords(boolean isSyncApi, boolean isObservationSubmitApi, String jsonString) {
        try {
            JSONObject recordJsonObject = new JSONObject(jsonString);
            JSONArray recordArray = recordJsonObject.getJSONArray("list");
            if (!isSyncApi) {
                totalCount = recordJsonObject.optInt("totalCount");
            } else {
                totalCount += recordJsonObject.optInt("totalCount");
            }

            if ((recordArray == null || recordArray.length() == 0) && records.size() > 0) {
                noRecordsFoundView();
                return;
            }

            arrayListRecords.clear();

            if (!isAdvanceSearchQuery()) {
                String syncDate = recordJsonObject.optString("syncDate");
                if (syncDate != null && !syncDate.equals("") && !syncDate.equals(getActivity().getResources().getString(R.string.txt_null))) {
                    this.syncTime = syncDate;
                }
                if ((isSyncApi && syncDate != null && !syncDate.equals("") && !syncDate.equals(getActivity().getResources().getString(R.string.txt_null)))) {
                    syncDateCount(syncDate);
                }
            }

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);

            if (isSyncApi) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(SharedPreferencesConstant.TIME_FOR_LOAD_RECORDS, System.currentTimeMillis());
                editor.putBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION, true);
                editor.commit();
            }

            ArrayList<DiagnosticOrder> tempDiagnosticOrdersUpdate = new ArrayList<>();
            ArrayList<DiagnosticOrder> tempDiagnosticOrders = new ArrayList<>();

            for (Map.Entry entry : records.entrySet()) {
                tempDiagnosticOrders.add(records.get(entry.getKey()));
            }

            // focus here
            for (int i = 0; i < recordArray.length(); i++) {
                DiagnosticOrder recordItem = addRecord(recordArray.getJSONObject(i));
                tempDiagnosticOrders.add(recordItem);
            }

            ArrayList<DiagnosticOrder> temp_list = new ArrayList<DiagnosticOrder>(tempDiagnosticOrders);

            boolean isHeaderChanged = false;
            for (int i = 0; i < temp_list.size(); i++) {
                DiagnosticOrder recordItem = temp_list.get(i);
                records.put(recordItem.getId(), recordItem);
                arrayListRecords.add(new OrderReportModel(recordItem, recordItem.getDiagnosticReportList(), recordItem.getOrderId()));
            }
            if (totalCount <= records.size()) {
                removeFotterView();
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(SharedPreferencesConstant.RECORD_SYNC_TIME, this.syncTime);

            if (!isAdvanceSearchQuery()) {
                editor.putInt(SharedPreferencesConstant.TOTAL_RECORD, totalCount);
            } else {
                editor.putInt(SharedPreferencesConstant.TOTAL_RECORD, sharedPreferences.getInt(SharedPreferencesConstant.TOTAL_RECORD, -1));
            }
            editor.putBoolean(SharedPreferencesConstant.ALL_RECORDS_LOADED, totalCount <= records.size() ? true : false);
            editor.commit();

            if (BuildConfig.isPatientApp && !isAdvanceSearchQuery()) {
                new DatabaseAsyncTask(new ArrayList<DiagnosticOrder>(tempDiagnosticOrdersUpdate)
                        , DatabaseTables.PATIENT_RECORD_TABLE).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
            updateUIRecordAdapter();
            noRecordsFoundView();
        } catch (Exception e) {
            Toast.makeText(GenralFragment.this.getActivity(), getResources().getString(R.string.txt_server_error), Toast.LENGTH_SHORT).show();
            countView.setVisibility(View.GONE);
            noRecordsFoundView();
        }
    }

    private int findPositionOfDiagnosticOrder(String id) {
        int i = 0;
        int position;
        Iterator<DiagnosticOrder> iterator = records.values().iterator();
        while (iterator.hasNext()) {
            DiagnosticOrder dia = iterator.next();
            if (dia.getId().equals(id)) {
                position = i;
                return position;
            } else
                i++;
        }
        return -1;
    }

    private DiagnosticOrder addRecord(JSONObject listObj) {
        try {
            DiagnosticOrder recordItem = new DiagnosticOrder();
            recordItem.setStatus(listObj.optString("status"));
            recordItem.setCategory(listObj.optString("category"));
            recordItem.setId(listObj.optString("id"));
            recordItem.setOrderId(listObj.optString("orderId"));
            recordItem.setOrderNo(listObj.optString("orderNumber"));
            recordItem.setOrderKey(listObj.optString(SharedPref.getOrderNumberKey()));
            recordItem.setPhysicianName(listObj.optString("orderingPhysicianName"));
            recordItem.setOrganisationId(listObj.optString("organizationId"));
            recordItem.setPatientDob(listObj.optString("dob"));
            recordItem.setPatientId(listObj.optString("patientId"));
            recordItem.setPatientMogoId(listObj.optString("pId"));
            recordItem.setPatientSex(listObj.optString("gender"));
            recordItem.setPatientName(listObj.optString("fullName"));
            recordItem.setOrganisationId(listObj.optString("organizationId"));
            recordItem.setPatientPhoneNumber(listObj.optString("phoneNo"));
            recordItem.setPatientEmailId(listObj.optString("email"));
            recordItem.setPatientDeceased(listObj.optBoolean("deceased"));
            recordItem.setPatientDeceasedDate(listObj.optString("deceasedDate"));
            recordItem.setOrderingPhysicianName(listObj.optString("orderingPhysicianName"));
            String scheduleDate = listObj.optString("scheduleDate");
            recordItem.setLabName(listObj.optString("labName"));
            recordItem.setScheduledDate(scheduleDate);
            recordItem.setPerformingLocationName(listObj.optString("performingLocationName"));
            recordItem.setPerformingLocationId(listObj.optString("performingLocationId"));
            recordItem.setRecordSceduleTimeStamp(Utils.convertDateToLong(scheduleDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
            recordItem.setAccessionNumber(listObj.optString("accessionNumber"));
            ArrayList<DiagnosticReport> diagnosticReportList = new ArrayList<>();
            JSONArray itemJsonArray = listObj.getJSONArray("reports");
            if (itemJsonArray != null && itemJsonArray.length() == 0) {
                return recordItem;
            }
            for (int j = 0; j < itemJsonArray.length(); j++) {
                DiagnosticReport diagnosticReport = new DiagnosticReport();
                JSONObject itemObj = itemJsonArray.getJSONObject(j);
                String reportName = itemObj.optString("reportName");
                diagnosticReport.setReportId(itemObj.optString("reportId"));
                diagnosticReport.setReportDate((itemObj.optString("reportDate")));
                diagnosticReport.setResultDate(itemObj.optString("resultDate"));
                diagnosticReport.setExpectedDate(itemObj.optString("expectedDate"));
                diagnosticReport.setReportStatus(itemObj.optString("reportStatus"));
                diagnosticReport.setReportType(itemObj.optString("reportType"));
                diagnosticReport.setReportFormat(itemObj.optString("reportFormat"));
                diagnosticReport.setReportName(reportName);
                diagnosticReport.setDiagnosticOrderId(recordItem.getId());
                diagnosticReportList.add(diagnosticReport);
            }
            recordItem.setDiagnosticReportList(diagnosticReportList);
            recordItem.setItem(listObj.optString("orderName"));
            return recordItem;
        } catch (Exception ignore) {
            ignore.printStackTrace();
            return null;
        }
    }

    private void addFooterView() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (records.size() == totalCount && totalCount > 0) {
                myFragmentView.findViewById(R.id.record_footer_view1).setVisibility(View.GONE);
            } else {
                if (arrayListRecords.size() != 0) {
                    no_record_view.setVisibility(View.GONE);
                    loadingView.setVisibility(View.GONE);
                    myFragmentView.findViewById(R.id.record_footer_view1).setVisibility(View.VISIBLE);
                } else {
                    no_record_view.setVisibility(View.VISIBLE);
                    loadingView.setVisibility(View.GONE);
                    myFragmentView.findViewById(R.id.record_footer_view1).setVisibility(View.GONE);
                }

//            myScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        } else {
            if (arrayListRecords.size() == 0) {
                no_record_view.setVisibility(View.VISIBLE);
                loadingView.setVisibility(View.GONE);
                myFragmentView.findViewById(R.id.record_footer_view1).setVisibility(View.GONE);
            }
            removeFotterView();
        }
    }

    /**
     * To make sync api call
     *
     * @param syncDate
     */

    private void syncDateCount(String syncDate) {
        if (this.syncTime == null || !this.syncTime.equals(syncDate)) {
            if (!isLoadingContacts) {
                Network.getGeneralRequestQueue().getCache().clear();
                fetchSyncResult();
            }
        }
    }

    private void updateUIRecordAdapter() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                adapter.notifyDataSetChanged();
                setEmptyView(records == null || records.size() == 0 ? true : false);
                loadingMore = false;
            }
        });
    }

    public void handleError(boolean isOnclick) {
        loadingMore = false;
        // ErrorMessage localErrorMessage = new ErrorMessage(getActivity(), R.id.layoutParent);
        // localErrorMessage.showMessage(getResources().getString(R.string.unable_to_contact_server));
        isScrollDownError = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_sort:
                if (layoutSortOption.getVisibility() == View.GONE) {
                    layoutSortOption.setVisibility(View.VISIBLE);
                } else {
                    layoutSortOption.setVisibility(View.GONE);
                }
                break;
            case R.id.resetButton:
                super.onClick(v);
                break;
            case R.id.layout_filter:
                getActivity().onBackPressed();
                break;
            default:
                super.onClick(v);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OBSERVATION_SELECT) {
            if (resultCode == getActivity().RESULT_OK) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                        isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                        if (!isLoadingContacts && (records == null || records.size() == 0)) {
                            fetchPaginationResult();
                        } else if (!isLoadingContacts) {
                            isLoadingContacts = true;
                            Network.getGeneralRequestQueue().getCache().clear();
                            ThreadManager.getDefaultExecutorService().submit(
                                    new LoadRecordsRequest(getTransactionId(), paginationSyncURL(), syncJson().toString(), true, true, true));

                        }
                    }
                }
            }
        } else if (requestCode == RESULT_SEARCH_QUERY && resultCode == getActivity().RESULT_OK) {
            if (Intent.ACTION_SEARCH.equals(data.getAction())) {
                String query = data.getStringExtra(SearchManager.QUERY);
                query = query.trim().toString();
                // use the query to search your data somehow
                // Toast.makeText(getActivity(), query, 1).show();
                SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getActivity(), MySuggestionProvider.AUTHORITY,
                        MySuggestionProvider.MODE);
                suggestions.saveRecentQuery(query, null);

                searchView.setQuery(query, false);
                searchView.clearFocus();

                //
                // searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                // @Override
                // public boolean onSuggestionSelect(int i) {
                // return false;
                // }
                //
                // @Override
                // public boolean onSuggestionClick(int position) {
                // searchView.setQuery(query, false); //to set the text
                // return true;
                // }
                // });

                JSONObject patNameIDJsonObject = new JSONObject();
                try {
                    patNameIDJsonObject.put("$regex", query);

                    patNameIDJsonObject.put("$options", "i");

                    JSONObject jsonNameObject = new JSONObject();
//                    jsonNameObject.put("extension.value.patientName", patNameIDJsonObject);
                    jsonNameObject.put("name.text.value", patNameIDJsonObject);
                    JSONObject subQueryObjectName = new JSONObject();
                    subQueryObjectName.put("$subQuery:subject:Patient", jsonNameObject);

                    JSONObject jsonIDObject = new JSONObject();
//                    jsonIDObject.put("extension.value.patientId", patNameIDJsonObject);
                    jsonIDObject.put("identifier.value.value", patNameIDJsonObject);
                    JSONObject subQueryObjectId = new JSONObject();
                    subQueryObjectId.put("$subQuery:subject:Patient", jsonIDObject);

                    JSONObject jsonDONAMEObject = new JSONObject();
                    jsonDONAMEObject.put("item.code.text.value", patNameIDJsonObject);
                    JSONObject jsonDOIDObject = new JSONObject();
                    jsonDOIDObject.put("extension.value.orderId", patNameIDJsonObject);

                    JSONObject jsonAccessionNoObject = new JSONObject();
                    jsonAccessionNoObject.put("extension.value.accessionNo", patNameIDJsonObject);
                    JSONObject jsonOrderNoObject = new JSONObject();
                    jsonOrderNoObject.put("extension.value.orderNo", patNameIDJsonObject);

                    JSONArray jsonArray = new JSONArray();

                    jsonArray.put(jsonDONAMEObject);
                    jsonArray.put(jsonDOIDObject);

                    jsonArray.put(jsonAccessionNoObject);
                    jsonArray.put(jsonOrderNoObject);


                    if (BuildConfig.isPhysicianApp) {
                        jsonArray.put(subQueryObjectName);
                        jsonArray.put(subQueryObjectId);

                    }

                    JSONObject qJsonObject = new JSONObject();
                    qJsonObject.put("$or", jsonArray);


                    queryJsonObject = qJsonObject.toString();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                isToRefresh = true;
                // searchParam.removeAll(searchParam);
                // searchParam.add("SearchTag.patientName|" + query);
                records.clear();
                arrayListRecords.removeAll(arrayListRecords);

                reset();

                searchParam.removeAll(searchParam);

    /*
    * container.setVisibility(View.GONE); container.startAnimation(animHide);
    *
    * // TODO Auto-generated method stub if (!patName.getText().toString().trim().isEmpty()) { searchParam.add("SearchTag.patientName|" +
    * patName.getText().toString().trim()); }
    *
    * if (!patID.getText().toString().trim().isEmpty()) { searchParam.add("SearchTag.patientId|" + patID.getText().toString().trim()); }
    */

                if (!isLoadingContacts) {
                    fetchPaginationResult();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void advanceSearch() {

        if (isAdvanceSearchQuery()) {
            reset();
            queryJsonObject = null;
            arrayListRecords.removeAll(arrayListRecords);
            records.clear();
            searchParam.removeAll(searchParam);

            new FetchAllRecordsDB().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            return;
        }

        if (!isLoadingContacts && (records == null || records.size() == 0)) {
            fetchPaginationResult();
        } else if (!isLoadingContacts) {
            isAllRecordLoaded = true;
            Network.getGeneralRequestQueue().getCache().clear();
            ThreadManager.getDefaultExecutorService().submit(
                    new LoadRecordsRequest(getTransactionId(), paginationSyncURL(), syncJson().toString(), true, true, true));
        }
    }

    @Override
    public void onRefresh() {
        if (searchView != null && !searchView.isIconified()) {
            searchItem.collapseActionView();
        }
        advanceSearch();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        if (!BuildConfig.isAdvanceSearchEnabled || getActivity() instanceof ResultViewActivity)
            return;

        inflater.inflate(R.menu.record_menu, menu);
        menu.add(Menu.NONE, TextConstants.MENU_ITEM_FILTERS, Menu.NONE, getResources().getString(R.string.txt_filters)).setIcon(R.drawable.ic_filter_list_white_36dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


        searchItem = menu.findItem(R.id.action_search_records);
        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        // Return true to allow the action view to expand
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                        // When the action view is collapsed, reset the query
                        if (isAdvanceSearchQuery())
                            advanceSearch();
                        // Return true to allow the action view to collapse
                        return true;
                    }
                });


        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

//        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search_records));
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        }

        if (BuildConfig.isPatientApp) {
            searchView.setQueryHint(getResources().getString(R.string.patientSearch));
        } else {
            searchView.setQueryHint(getResources().getString(R.string.physicianSearch));
        }


    }

    private BroadcastReceiver timeZoneChangedBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                AppLog.d("broadcast", "timezone broadcast received");
                if (BuildConfig.isPatientApp) {
                    new FetchAllRecordsDB(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            } else if (BuildConfig.isPhysicianApp) {
                /*int size = arrayListRecords.size();
                for (int i = 0; i < size; i++) {
                    if (arrayListRecords.get(i) instanceof DiagnosticReport) {
                        DiagnosticReport diagnosticReport = (DiagnosticReport) arrayListRecords.get(i);
                        String recordDate = diagnosticReport.getReportDate();
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());
                        try {
                            cal.setTime(sdf.parse(recordDate));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long timeInMillis = cal.getTimeInMillis();
                        sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());
                        String formattedDate = sdf.format(timeInMillis);
                        diagnosticReport.setReportDate(formattedDate);
                        arrayListRecords.set(i, diagnosticReport);
                    }
                }*/
                adapter.notifyDataSetChanged();
                setEmptyView(records == null || records.size() <= 0 ? true : false);
            }
        }
    };

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(timeZoneChangedBroadcast);
        isRegisteredBroadcast = false;
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

}
