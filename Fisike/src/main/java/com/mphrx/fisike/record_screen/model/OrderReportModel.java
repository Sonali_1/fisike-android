package com.mphrx.fisike.record_screen.model;

import com.mphrx.fisike.view.ExpandableRecyclerView.models.ExpandableGroup;

import java.util.List;

/**
 * Created by kailashkhurana on 24/07/17.
 */

public class OrderReportModel extends ExpandableGroup<DiagnosticReport> {

    private String orderId;

    public OrderReportModel(DiagnosticOrder title, List<DiagnosticReport> items, String orderId) {
        super(title, items);
        this.orderId = orderId;
    }

    public String getIconResId() {
        return orderId;
    }

    /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderReportModel)) return false;

        OrderReportModel genre = (OrderReportModel) o;

        return getIconResId().equals(genre.getIconResId());

    }

    @Override
    public int hashCode() {
        return Integer.parseInt(getIconResId());
    }*/
}