package com.mphrx.fisike.record_screen.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DiagnosticOrder implements Parcelable {

    private String patientName;
    private String patientId;
    private String patientDob;
    private String patientSex;
    private String physicianName;

    private String Id;
    private String item;
    private String category;
    private String orderId;
    private String orderingPhysicianName;
    private String orderNo;
    private String performingLocationId;

    private String scheduledDate;
    private String status;
    private int readStatus;
    private long recordSceduleTimeStamp;
    private String organisationId;
    private String labName;
    private String accessionNumber;

    private ArrayList<DiagnosticReport> diagnosticReportList;
    private String reportId;
    private String reportDate;
    private String reportName;
    private String reportFormat;
    private String patientPhoneNumber;
    private String patientEmailId;
    private boolean patientDeceased;
    private String patientDeceasedDate;
    private String patientMogoId;
    private String orderKey;

    public DiagnosticOrder() {

    }

    protected DiagnosticOrder(Parcel in) {
        patientName = in.readString();
        patientId = in.readString();
        patientDob = in.readString();
        patientSex = in.readString();
        patientPhoneNumber = in.readString();
        patientEmailId = in.readString();
        patientDeceased = in.readInt() == 0 ? false : true;
        patientDeceasedDate = in.readString();
        physicianName = in.readString();
        Id = in.readString();
        item = in.readString();
        category = in.readString();
        orderId = in.readString();
        orderingPhysicianName = in.readString();
        orderNo = in.readString();
        performingLocationId = in.readString();
        scheduledDate = in.readString();
        status = in.readString();
        readStatus = in.readInt();
        recordSceduleTimeStamp = in.readLong();
        organisationId = in.readString();
        labName = in.readString();
        accessionNumber = in.readString();
        diagnosticReportList = in.readArrayList(getClass().getClassLoader());
        reportId = in.readString();
        reportDate = in.readString();
        reportName = in.readString();
        reportFormat = in.readString();
        orderKey = in.readString();

    }


    private String performingLocationName;

    public long getRecordSceduleTimeStamp() {
        return recordSceduleTimeStamp;
    }

    public void setRecordSceduleTimeStamp(long recordSceduleTimeStamp) {
        this.recordSceduleTimeStamp = recordSceduleTimeStamp;
    }

    public String getPhysicianName() {
        return physicianName;
    }

    public void setPhysicianName(String physicianName) {
        this.physicianName = physicianName;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderingPhysicianName() {
        return orderingPhysicianName;
    }

    public void setOrderingPhysicianName(String orderingPhysicianName) {
        this.orderingPhysicianName = orderingPhysicianName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPerformingLocationId() {
        return performingLocationId;
    }

    public void setPerformingLocationId(String performingLocationId) {
        this.performingLocationId = performingLocationId;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber;
    }

    public ArrayList<DiagnosticReport> getDiagnosticReportList() {
        return diagnosticReportList;
    }

    public void setDiagnosticReportList(ArrayList<DiagnosticReport> diagnosticReportList) {
        this.diagnosticReportList = diagnosticReportList;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }


    public String getPerformingLocationName() {
        return performingLocationName;
    }

    public void setPerformingLocationName(String performingLocationName) {
        this.performingLocationName = performingLocationName;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(patientName);
        parcel.writeString(patientId);
        parcel.writeString(patientDob);
        parcel.writeString(patientSex);
        parcel.writeString(patientDob);
        parcel.writeString(patientPhoneNumber);
        parcel.writeString(patientEmailId);
        parcel.writeInt(patientDeceased ? 1 : 0);
        parcel.writeString(patientDeceasedDate);
        parcel.writeString(physicianName);
        parcel.writeString(Id);
        parcel.writeString(item);
        parcel.writeString(category);
        parcel.writeString(orderId);
        parcel.writeString(orderingPhysicianName);
        parcel.writeString(orderNo);
        parcel.writeString(performingLocationId);
        parcel.writeString(scheduledDate);
        parcel.writeString(status);
        parcel.writeInt(readStatus);
        parcel.writeLong(recordSceduleTimeStamp);
        parcel.writeString(organisationId);
        parcel.writeString(labName);
        parcel.writeString(accessionNumber);
        parcel.writeList(diagnosticReportList);
        parcel.writeString(reportId);
        parcel.writeString(reportDate);
        parcel.writeString(reportName);
        parcel.writeString(reportFormat);
        parcel.writeString(orderKey);

    }

    public static Creator<DiagnosticReport> CREATOR = new Creator<DiagnosticReport>() {

        @Override
        public DiagnosticReport createFromParcel(Parcel source) {
            return new DiagnosticReport(source);
        }

        @Override
        public DiagnosticReport[] newArray(int size) {
            return new DiagnosticReport[size];
        }

    };

    public void setPatientPhoneNumber(String patientPhoneNumber) {
        this.patientPhoneNumber = patientPhoneNumber;
    }

    public void setPatientEmailId(String patientEmailId) {
        this.patientEmailId = patientEmailId;
    }

    public void setPatientDeceased(boolean patientDeceased) {
        this.patientDeceased = patientDeceased;
    }

    public boolean isPatientDeceased() {
        return patientDeceased;
    }

    public String getPatientDeceasedDate() {
        return patientDeceasedDate;
    }

    public void setPatientDeceasedDate(String patientDeceasedDate) {
        this.patientDeceasedDate = patientDeceasedDate;
    }

    public String getPatientPhoneNumber() {
        return patientPhoneNumber;
    }

    public String getPatientEmailId() {
        return patientEmailId;
    }

    public void setPatientMogoId(String patientMogoId) {
        this.patientMogoId = patientMogoId;
    }

    public String getPatientMogoId() {
        return patientMogoId;
    }

    public String getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }
}
