
package com.mphrx.fisike.record_screen.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.record_screen.adapter.ObservationHistoryAdapter;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.record_screen.request.GetObservationHistoryRequest;
import com.mphrx.fisike.record_screen.request.ObservationDetailListingRequest;
import com.mphrx.fisike.record_screen.response.ObservationHistory;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.DividerItemDecoration;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.UnitsEnum;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class ObservationHistoryActivity extends BaseActivity {
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title, tvAnalysis;
    private IconTextView bt_toolbar_right;
    private RecyclerView observationHistoryListView;
    private String patientId;
    private String paramId;
    private String secoundryParamId;
    private TextView range;
    private LineChartView chartView;

    private static final String TITLE = "title";
    private static final String SUBTITLE = "subtitle";
    private static final String PARAM_ID = "paramId";
    private static final String SECOUNDRY_PARAM_ID = "secoundryParamId";
    private static final String PATIENT_ID = "patientId";
    private static final String ISVITAL = "isVital";
    private boolean isNotMadeApiCall, isVital;
    private Axis axisX;
    private Axis axisY;
    private ArrayList<Line> lines;
    private LinearLayout layoutSymbol;
    private String vitalText;
    private String is_Systolic_or_Dystolic = "";
    private FrameLayout frameLayout;
    private LinearLayout observationHeader;
    private ObservationHistoryAdapter adapter;
    private String referenceRangeLow, referenceRangeHigh;
    private CustomFontTextView tvUnit;

    public static Intent newIntent(Context context, String paramId, String patientId, String title, String subTitle, String secoundryParamId, boolean isVital) {
        Intent observationHistoryIntent = new Intent(context, ObservationHistoryActivity.class);
        observationHistoryIntent.putExtra(PARAM_ID, paramId);
        observationHistoryIntent.putExtra(PATIENT_ID, patientId);
        observationHistoryIntent.putExtra(TITLE, title);
        observationHistoryIntent.putExtra(SECOUNDRY_PARAM_ID, secoundryParamId);
        observationHistoryIntent.putExtra(SUBTITLE, subTitle);
        observationHistoryIntent.putExtra(ISVITAL, isVital);
        return observationHistoryIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_observation_history);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_observation_history, frameLayout);
        initToolbar();

        patientId = getIntent().getStringExtra(PATIENT_ID);
        paramId = getIntent().getStringExtra(PARAM_ID);
        secoundryParamId = getIntent().getStringExtra(SECOUNDRY_PARAM_ID);
        isVital = getIntent().getBooleanExtra(ISVITAL, false);

        observationHistoryListView = (RecyclerView) findViewById(R.id.lv_observation_history);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        observationHistoryListView.setLayoutManager(linearLayoutManager);
        observationHistoryListView.addItemDecoration(new DividerItemDecoration(this));
        //observationHistoryListView.setNestedScrollingEnabled(false);

        vitalText = getIntent().getStringExtra(TITLE);

        observationHeader = (LinearLayout) findViewById(R.id.observation_history_header);
        range = (TextView) findViewById(R.id.tv_range);
        chartView = (LineChartView) findViewById(R.id.chart_view);
        layoutSymbol = (LinearLayout) findViewById(R.id.layoutSymbol);
        tvAnalysis = (CustomFontTextView) findViewById(R.id.tv_analysis);
        tvUnit = (CustomFontTextView) findViewById(R.id.tv_unit);
        if (secoundryParamId != null) {
            is_Systolic_or_Dystolic = getIntent().getStringExtra(SUBTITLE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 6);
            int color1;
            int color2;
            if ("DIASTOLIC".equals(getIntent().getStringExtra(SUBTITLE))) {
                color1 = 0xffCACA4A;
                color2 = 0xffaaaaaa;
            } else {
                color1 = 0xffCACA4A;
                color2 = 0xffaaaaaa;
            }
            View systolicView = new View(ObservationHistoryActivity.this);
            if (Utils.isRTL(this)) {
                params.setMargins(0, 0, 40, 0);
            } else {
                params.setMargins(40, 0, 0, 0);
            }
            systolicView.setBackgroundColor(color2);
            systolicView.setLayoutParams(params);
            layoutSymbol.addView(systolicView);
            CustomFontTextView txtSystolic = new CustomFontTextView(ObservationHistoryActivity.this);
            txtSystolic.setText(getResources().getString(R.string.systotic_caps));
            txtSystolic.setSingleLine(true);
            if (Utils.isRTL(this)) {
                txtSystolic.setPadding(2, 0, 16, 0);
            } else {
                txtSystolic.setPadding(16, 0, 2, 0);
            }
            layoutSymbol.addView(txtSystolic);

            View diastolicView = new View(ObservationHistoryActivity.this);
            diastolicView.setBackgroundColor(color1);
            diastolicView.setLayoutParams(params);
            layoutSymbol.addView(diastolicView);
            CustomFontTextView txtDistolic = new CustomFontTextView(ObservationHistoryActivity.this);
            txtDistolic.setText(getResources().getString(R.string.diasystotic_caps));
            txtDistolic.setSingleLine(true);
            if (Utils.isRTL(this)) {
                txtDistolic.setPadding(2, 0, 16, 0);
            } else {
                txtDistolic.setPadding(16, 0, 2, 0);
            }
            layoutSymbol.setGravity(Gravity.CENTER_VERTICAL);
            layoutSymbol.addView(txtDistolic);
        }

        showProgressDialog();
        if (!isVital)
            ThreadManager.getDefaultExecutorService().
                    submit(new ObservationDetailListingRequest(5, 0, "issued", patientId, paramId,
                            getTransactionId(), vitalText));
        else
            ThreadManager.getDefaultExecutorService().submit(new GetObservationHistoryRequest
                    (getTransactionId(), ObservationHistoryActivity.this, 5, paramId, secoundryParamId, patientId));

    }

    private void initToolbar() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        mToolbar.setTitle(getIntent().getStringExtra(SUBTITLE) + getResources().getString(R.string._Report_History));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String titleText = "";

//in case of BP display BP
        if (getIntent().getStringExtra(TITLE).equalsIgnoreCase("Blood Pressure"))
            titleText = this.getResources().getString(R.string.blood_pressure);

            //in case of Glucose display Glucose - category
        else if (getIntent().getStringExtra(TITLE).equalsIgnoreCase("Glucose"))
            titleText = getString(R.string.glucose_screen_title) + " - " + getIntent().getStringExtra(SUBTITLE);
        else
            titleText = getIntent().getStringExtra(SUBTITLE);

        toolbar_title.setText(titleText);
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    @Subscribe
    public void onObservationReceived(VitalsResponse vitalsResponse) {
        if (getTransactionId() != vitalsResponse.getTransactionId())
            return;

        dismissProgressDialog();

        if (!vitalsResponse.isSuccessful()) {

            Toast.makeText(this, vitalsResponse.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        } else if (vitalsResponse.getVitalsHistoryModelsArrayList().size() == 0) {
            Toast.makeText(this, getResources().getString(R.string.No_observations_to_review_right_now), Toast.LENGTH_SHORT).show();
            finish();
        } else {

            if (BuildConfig.isToHideInterPretationOnVitals)
                tvAnalysis.setVisibility(View.GONE);

            if (vitalsResponse.getVitalsHistoryModelsArrayList().size() == 1) {
                ArrayList<VitalsModel> vitalsModels = vitalsResponse.getVitalsHistoryModelsArrayList().get(0).getVitalsModels();
                VitalsConfigMO vitalsConfig = null;
                try {
                    vitalsConfig = VitalsConfigDBAdapter.getInstance(this).fetchVitalsConfigMO();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (vitalsModels == null || vitalsModels.size() <= 0) {
                    Toast.makeText(this, getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                VitalsConfigMO.setInstance(vitalsConfig);
                referenceRangeLow = vitalsModels.get(0).getLowReferenceRangeValue();
                referenceRangeHigh = vitalsModels.get(0).getHighReferenceRangeValue();
                if ((Utils.isValueAvailable(referenceRangeLow) && Utils.isValueAvailable(referenceRangeHigh))) {
                    String unit = vitalsModels.get(0).getConvertedUnit();
                    //setting Range
                    range.setText(getString(R.string.Range) + referenceRangeLow + "-" + referenceRangeHigh);
                    //set unit
                    if (Utils.isValueAvailable(unit)) {
                        tvUnit.setVisibility(View.VISIBLE);
                        if (unit.equalsIgnoreCase(TextConstants.BMI_KGM))
                            unit = TextConstants.BMI_KGM2;

                        if (unit.contains(TextConstants.BMI_SEPARATOR)) {
                            String[] parts = unit.split("\\^");
                            String part0value = UnitsEnum.getValueFrom(parts[0]);
                            String part1value = parts.length >= 2 ? parts[1] : "";
                            tvUnit.setText(Html.fromHtml(getString(R.string.unit) + " " + part0value + "<sup><small>" + part1value + "</small></sup>"));
                        } else
                            tvUnit.setText(getString(R.string.unit) + " " + UnitsEnum.getValueFrom(unit));
                    } else
                        tvUnit.setVisibility(View.GONE);
                }
                adapter = new ObservationHistoryAdapter(vitalsModels, this, true);
                observationHistoryListView.setAdapter(adapter);
                if (vitalsModels.size() <= 1) {
                    chartView.setVisibility(View.GONE);
                    observationHeader.setVisibility(View.VISIBLE);
                    observationHistoryListView.setVisibility(View.VISIBLE);

                } else {
                    chartView.setVisibility(View.VISIBLE);
                    observationHeader.setVisibility(View.VISIBLE);
                    observationHistoryListView.setVisibility(View.VISIBLE);
                }
            } else {
                if (vitalsResponse.getVitalsHistoryModelsArrayList().size() > 1) {
                    chartView.setVisibility(View.VISIBLE);
                }
                observationHistoryListView.setVisibility(View.VISIBLE);
                observationHeader.setVisibility(View.VISIBLE);

                ArrayList<VitalsModel> systolic = new ArrayList<>();
                for (VitalsModel model : vitalsResponse.getVitalsHistoryModelsArrayList().get(0).getVitalsModels()) {
                    try {
                        systolic.add((VitalsModel) model.clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
                ArrayList<VitalsModel> diastolic = new ArrayList<>();
                for (VitalsModel model : vitalsResponse.getVitalsHistoryModelsArrayList().get(1).getVitalsModels()) {
                    try {
                        diastolic.add((VitalsModel) model.clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
                String lowValue, highValue;
                try {
                    double low = Double.parseDouble(systolic.get(0).getLowReferenceRangeValue());
                    double lowDia = Double.parseDouble(diastolic.get(0).getLowReferenceRangeValue());
                    if (low <= lowDia)
                        lowValue = systolic.get(0).getLowReferenceRangeValue();
                    else
                        lowValue = diastolic.get(0).getLowReferenceRangeValue();
                } catch (Exception e) {
                    lowValue = diastolic.get(0).getLowReferenceRangeValue();
                }

                try {
                    double high = Double.parseDouble(systolic.get(0).getHighReferenceRangeValue());
                    double highDia = Double.parseDouble(diastolic.get(0).getHighReferenceRangeValue());
                    if (high >= highDia)
                        highValue = systolic.get(0).getHighReferenceRangeValue();
                    else
                        highValue = diastolic.get(0).getHighReferenceRangeValue();
                } catch (Exception e) {
                    highValue = systolic.get(0).getHighReferenceRangeValue();
                }
                referenceRangeLow = lowValue + "";
                referenceRangeHigh = highValue + "";
                if (Utils.isValueAvailable(referenceRangeLow) && Utils.isValueAvailable(referenceRangeHigh)) {
                    String unit = systolic.get(0).getConvertedUnit();
                    //setting Range
                    range.setText(getString(R.string.Range) + referenceRangeLow + "-" + referenceRangeHigh);
                    //set unit
                    if (Utils.isValueAvailable(unit)) {
                        tvUnit.setVisibility(View.VISIBLE);
                        if (unit.equalsIgnoreCase(TextConstants.BMI_KGM))
                            unit = TextConstants.BMI_KGM2;
                        //unit contains ^
                        if (unit.contains(TextConstants.BMI_SEPARATOR)) {
                            String[] parts = unit.split("\\^");
                            String part0value = UnitsEnum.getValueFrom(parts[0]);
                            String part1value = parts.length >= 2 ? parts[1] : "";
                            tvUnit.setText(Html.fromHtml(getString(R.string.unit) + " " + part0value + "<sup><small>" + part1value + "</small></sup>"));
                        } else
                            tvUnit.setText(getString(R.string.unit) + " " + UnitsEnum.getValueFrom(unit));
                    } else
                        tvUnit.setVisibility(View.GONE);
                }
                for (int i = 0; i < systolic.size(); i++) {
                    systolic.get(i).setConvertedValue(systolic.get(i).getConvertedValue() + "/" + diastolic.get(i).getConvertedValue());
                }
                adapter = new ObservationHistoryAdapter(systolic, this, true);
                observationHistoryListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            try {
                // prepare the graph
                if ((is_Systolic_or_Dystolic.equalsIgnoreCase("DIASTOLIC") || is_Systolic_or_Dystolic.equalsIgnoreCase("SYSTOLIC")) && !is_Systolic_or_Dystolic.equals("")) {

                    isNotMadeApiCall = false;
                    plotGraph(vitalsResponse.getVitalsHistoryModelsArrayList().get(1).getVitalsModels(), isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A, isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A);
                    isNotMadeApiCall = true;
                    plotGraph(vitalsResponse.getVitalsHistoryModelsArrayList().get(0).getVitalsModels(), isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A, isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A);
                } else {
                    plotGraph(vitalsResponse.getVitalsHistoryModelsArrayList().get(0).getVitalsModels(), 0xffCACA4A, 0xffaaaaaa);
                }
            } catch (Exception numberFormatException) {
                numberFormatException.printStackTrace();

            }
        }
    }


    @Subscribe
    public void onObservationReceived(ObservationHistory diagnosticObservation) {
        if (getTransactionId() != diagnosticObservation.getTransactionId())
            return;

        if (isNotMadeApiCall || secoundryParamId == null) {
            //observationHeader.setVisibility(View.VISIBLE);
            dismissProgressDialog();
        }
        if (!diagnosticObservation.isSuccessful()) {
            DialogUtils.showAlertDialogCommon(this, getString(R.string.title_view_trend_fail), getString(R.string.text_view_trend_fail), getString(R.string.btn_ok), null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        finish();
                    }
                }
            });
//            Toast.makeText(this, diagnosticObservation.getMessage(), Toast.LENGTH_SHORT).show();

        } else if (diagnosticObservation.getObservations().isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.No_observations_to_review_right_now), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            if (secoundryParamId == null || !isNotMadeApiCall) {
                // set the list adapter
                diagnosticObservation.getObservations().size();
                adapter = new ObservationHistoryAdapter(diagnosticObservation.getObservations(), this);
                observationHistoryListView.setAdapter(adapter);
                referenceRangeLow = diagnosticObservation.getReferenceRangeLow();
                referenceRangeHigh = diagnosticObservation.getReferenceRangeHigh();
                if (Utils.isValueAvailable(referenceRangeLow) && Utils.isValueAvailable(referenceRangeHigh)) {
                    String unit = diagnosticObservation.getReferenceRangeUnit();
                    //setting Range
                    range.setText(getString(R.string.Range) + referenceRangeLow + "-" + referenceRangeHigh);
                    //set unit
                    if (Utils.isValueAvailable(unit)) {
                        tvUnit.setVisibility(View.VISIBLE);
                        //unit contains ^
                        if (unit.contains(TextConstants.BMI_SEPARATOR)) {
                            String[] parts = unit.split("\\^");
                            String part0value = UnitsEnum.getValueFrom(parts[0]);
                            String part1value = parts.length >= 2 ? parts[1] : "";
                            tvUnit.setText(Html.fromHtml(getString(R.string.unit) + " " + part0value + "<sup><small>" + part1value + "</small></sup>"));
                        } else
                            tvUnit.setText(getString(R.string.unit) + " " + UnitsEnum.getValueFrom(unit));

                    } else
                        tvUnit.setVisibility(View.GONE);
                }
/*
                range.setText(diagnosticObservation.getDisplayableRangeHistory(vitalText));
*/
                if (diagnosticObservation.getObservations().size() <= 1) {
                    chartView.setVisibility(View.GONE);
                    observationHeader.setVisibility(View.VISIBLE);
                    observationHistoryListView.setVisibility(View.VISIBLE);

                } else {
                    chartView.setVisibility(View.VISIBLE);
                    observationHeader.setVisibility(View.VISIBLE);
                    observationHistoryListView.setVisibility(View.VISIBLE);
                }
            } else {
                if (diagnosticObservation.getObservations().size() > 1) {
                    chartView.setVisibility(View.VISIBLE);
                }
                observationHistoryListView.setVisibility(View.VISIBLE);
                observationHeader.setVisibility(View.VISIBLE);
                if (adapter != null) {
                    adapter.setDiastolicValue(diagnosticObservation.getObservations());
                }
            }

            if (diagnosticObservation.getObservations().size() > 1) {
                try {
                    // prepare the graph
                    if (is_Systolic_or_Dystolic.equals("DIASTOLIC") && !is_Systolic_or_Dystolic.equals("")) {
                        plotGraph(diagnosticObservation, isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A, isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A);
                    } else {
                        plotGraph(diagnosticObservation, !isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A, !isNotMadeApiCall ? 0xffaaaaaa : 0xffCACA4A);
                    }
                } catch (Exception numberFormatException) {
                    numberFormatException.printStackTrace();

                }
            }

            if (!isNotMadeApiCall && secoundryParamId != null) {
                isNotMadeApiCall = true;
                chartView.setVisibility(View.GONE);
                observationHeader.setVisibility(View.GONE);
                observationHistoryListView.setVisibility(View.GONE);
                ThreadManager.getDefaultExecutorService().
                        submit(new ObservationDetailListingRequest(5, 0, "issued", patientId, secoundryParamId,
                                getTransactionId(), vitalText));
            }
        }
    }

    private void plotGraph(ObservationHistory tempDiagnosticObservation, int lineColor, int pointColor) throws NumberFormatException {
        String referenceRangeHigh = tempDiagnosticObservation.getReferenceRangeHigh();
        String referenceRangeLow = tempDiagnosticObservation.getReferenceRangeLow();
        ObservationHistory diagnosticObservation = null;
        try {
            diagnosticObservation = (ObservationHistory) tempDiagnosticObservation.clone();
        } catch (CloneNotSupportedException e) {
            return;
        }
        ArrayList<Observation> observations = new ArrayList<Observation>();
        for (int i = 0; i < tempDiagnosticObservation.getObservations().size(); i++) {
            try {
                Float.valueOf(tempDiagnosticObservation.getObservations().get(i).getValue());
                observations.add(tempDiagnosticObservation.getObservations().get(i));
            } catch (Exception e) {
                continue;
            }

        }
        diagnosticObservation.setObservations(observations);
        if (diagnosticObservation.getObservations().size() < 2) {
            chartView.setVisibility(View.GONE);
            return;
        }
        Observation first = diagnosticObservation.getObservations().get(0);
        if (secoundryParamId == null || !isNotMadeApiCall) {
            lines = new ArrayList<>(diagnosticObservation.getObservations().size());
            axisX = new Axis();
            axisY = new Axis().setHasLines(true);
        }
        List<PointValue> values = new ArrayList<>();
        ArrayList<String> datesCollection = new ArrayList<>();
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        // Add chart data
        int j = 0;
        for (int i = diagnosticObservation.getObservations().size() - 1; i >= 0; i--) {
            Observation obv = diagnosticObservation.getObservations().get(i);
            float value = Float.valueOf(obv.getValue());
            String date = DateTimeUtil.calculateDate(obv.getIssueDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z,
                    DateTimeUtil.dd_MMM_yy_HiphenSepertedDate);
            int matchingdateIndex = retrieveMatchingDateIndex(date, datesCollection);
            if (matchingdateIndex == -1) {
                datesCollection.add(date);
                values.add(new PointValue(j, value));
                axisValues.add(new AxisValue(j).setLabel(date));
            } else {
                values.add(new PointValue(matchingdateIndex, value));
                axisValues.add(new AxisValue(matchingdateIndex).setLabel(date));
            }
            j = j + 1;
        }

        lines.add(new Line(values).setColor(lineColor).setPointColor(pointColor)
                .setHasLabelsOnlyForSelected(true));


        if (secoundryParamId == null || isNotMadeApiCall) {
            lines.addAll(generateLinesForLowandHighValues(referenceRangeLow, referenceRangeHigh,
                    diagnosticObservation.getObservations().size()));
            LineChartData data = new LineChartData(lines);
            axisX.setHasTiltedLabels(true);
            axisX.setAutoGenerated(true);
            axisY.setName(first.getUnit());
            axisY.setHasLines(true);
            data.setAxisXBottom(new Axis(axisValues).setHasLines(false));
            data.setAxisYLeft(axisY);
            chartView.setInteractive(true);
            chartView.setOnValueTouchListener(new ValueTouchListener());
            chartView.setScrollEnabled(true);
            chartView.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
            chartView.setValueSelectionEnabled(true);
            if (Utils.isRTL(this)) {
                chartView.setPadding(40, 50, 0, 20);
            } else {
                chartView.setPadding(0, 50, 40, 20);
            }
            chartView.setLineChartData(data);
        }
    }

    private void plotGraph(ArrayList<VitalsModel> vitalsModelsArrayList, int lineColor, int pointColor) throws NumberFormatException {
        if (vitalsModelsArrayList.size() < 2) {
            layoutSymbol.setVisibility(View.GONE);
            chartView.setVisibility(View.GONE);
            return;
        }
        VitalsModel first = vitalsModelsArrayList.get(0);
        if (secoundryParamId == null || !isNotMadeApiCall) {
            lines = new ArrayList<>();
            axisX = new Axis();
            axisY = new Axis().setHasLines(true);
        }
        List<PointValue> values = new ArrayList<>();
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        ArrayList<String> datesCollection = new ArrayList<>();
        int j = 0;
        for (int i = vitalsModelsArrayList.size() - 1; i >= 0; i--) {
            VitalsModel obv = vitalsModelsArrayList.get(i);
            float value = Float.parseFloat(obv.getConvertedValue());
            String date = DateTimeUtil.calculateDate(obv.getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z,
                    DateTimeUtil.dd_MMM_yy_HiphenSepertedDate);
            int matchingdateIndex = retrieveMatchingDateIndex(date, datesCollection);
            if (matchingdateIndex == -1) {
                datesCollection.add(date);
                values.add(new PointValue(j, value));
                axisValues.add(new AxisValue(j).setLabel(date));
                j = j + 1;
            } else {
                values.add(new PointValue(matchingdateIndex, value));
                axisValues.add(new AxisValue(matchingdateIndex).setLabel(date));
            }
        }
        Line line = new Line(values).setColor(lineColor).setCubic(false).
                setFilled(false).setHasLabels(true).
                setHasLabelsOnlyForSelected(true).setHasLines(true).
                setHasPoints(true).setPointColor(pointColor);
        lines.add(line);

        // setting the chart data
        if (secoundryParamId == null || isNotMadeApiCall) {

            lines.addAll(generateLinesForLowandHighValues(referenceRangeLow, referenceRangeHigh,
                    vitalsModelsArrayList.size()));

            LineChartData data = new LineChartData(lines);
            axisX.setHasTiltedLabels(true);
            axisX.setAutoGenerated(true);
            String unit = first.getConvertedUnit();
            //String unit = "kg/m^2";
            if (unit.contains(TextConstants.BMI_SEPARATOR)) {
                String[] parts = unit.split("\\^");
                String part0value = UnitsEnum.getValueFrom(parts[0]);
                String part1value = parts.length >= 2 ? parts[1] : "";
                axisY.setName(String.valueOf(Html.fromHtml(getString(R.string.unit) + " " + part0value + "<sup><small>" + part1value + "</small></sup>")));
            } else
                axisY.setName(UnitsEnum.getValueFrom(first.getConvertedUnit()));
            axisY.setHasLines(true);
            data.setAxisXBottom(new Axis(axisValues).setHasLines(false));
            data.setAxisYLeft(axisY);
            chartView.setInteractive(true);
            chartView.setOnValueTouchListener(new ValueTouchListener());
            chartView.setScrollEnabled(true);
            chartView.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
            chartView.setValueSelectionEnabled(true);
            if (Utils.isRTL(this)) {
                chartView.setPadding(40, 50, 0, 20);
            } else {
                chartView.setPadding(0, 50, 40, 20);
            }
            chartView.setLineChartData(data);
        }
    }

    private int retrieveMatchingDateIndex(String date, ArrayList<String> xAxisDateCollection) {
        int i = -1;
        if (xAxisDateCollection == null || xAxisDateCollection.size() == 0) {
            return -1;
        }
        for (int j = 0; j < xAxisDateCollection.size(); j++) {
            if (date.equals(xAxisDateCollection.get(j))) {
                return j;
            }
        }
        return i;
    }

    private ArrayList<Line> generateLinesForLowandHighValues(String referenceRangeLow, String referenceRangeHigh,
                                                             int observationSize) {
        ArrayList<Line> lines = new ArrayList<Line>();
        PointValue lo1;
        PointValue lo2;
        PointValue hi1;
        PointValue hi2;
        if (!Utils.isValueAvailable(referenceRangeLow)) referenceRangeLow = String.valueOf(0);
        if (!Utils.isValueAvailable(referenceRangeHigh)) referenceRangeHigh = String.valueOf(0);

        if (!referenceRangeLow.equals("0")) {
            lo1 = new PointValue(0, Float.valueOf(referenceRangeLow));
            lo2 = new PointValue(observationSize, Float.valueOf(referenceRangeLow));
        } else {
            lo1 = new PointValue(0, Float.valueOf(0));
            lo2 = new PointValue(observationSize, Float.valueOf(0));
        }
        if (!referenceRangeHigh.equals("0")) {
            hi1 = new PointValue(0, Float.valueOf(referenceRangeHigh));
            hi2 = new PointValue(observationSize, Float.valueOf(referenceRangeHigh));
        } else {
            hi1 = new PointValue(0, Float.valueOf(0));
            hi2 = new PointValue(observationSize, Float.valueOf(0));
        }

        List<PointValue> loValues = new ArrayList<>();
        loValues.add(lo1);
        loValues.add(lo2);
        List<PointValue> hiValues = new ArrayList<>();
        hiValues.add(hi1);
        hiValues.add(hi2);
        lines.add(new Line(loValues).setColor(0xff0000ff).setFilled(false)
                .setPointRadius(0).setHasLabelsOnlyForSelected(false).setHasLabels(false));
        lines.add(new Line(hiValues).setColor(0xff0000ff)
                .setPointRadius(0).setHasLabelsOnlyForSelected(false).setHasLabels(false));
        return lines;
    }

    private class ValueTouchListener implements LineChartOnValueSelectListener {

        @Override
        public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
//            Toast.makeText(ObservationHistoryActivity.this, "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }

}
