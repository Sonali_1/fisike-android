package com.mphrx.fisike.record_screen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.enums.ObservationInterpretationEnum;
import com.mphrx.fisike.models.VitalsHistoryModels;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.utils.DateTimeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xmb2nc on 23-12-2015.
 */
public class ObservationHistoryAdapter extends RecyclerView.Adapter<ObservationHistoryAdapter.ViewHolder> {

    private List observationHistoryList;
    private Context context;
    private ArrayList<VitalsHistoryModels> BPValues;
    private boolean isVital;

    public ObservationHistoryAdapter(List<VitalsModel> observations, Context context, boolean isVital) {
        observationHistoryList = observations;
        this.context = context;
        this.isVital = isVital;
    }

    public ObservationHistoryAdapter(List<Observation> observations, Context context) {
        observationHistoryList = observations;
        this.context = context;
        this.isVital = false;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.observation_history_row, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return observationHistoryList == null ? 0 : observationHistoryList.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder recyclerViewHolder, final int position) {
        if (!isVital) {
            Observation observation = (Observation) observationHistoryList.get(position);
            recyclerViewHolder.tvDateValue.setText(convertDate(observation.getIssueDate()));
            recyclerViewHolder.tvResultValue.setText(observation.getValue().replaceFirst("^0+(?!$)", ""));
            ObservationInterpretationEnum observation_enum = ObservationInterpretationEnum.getMap().get(observation.getInterpretation());
            String value = "";
            if (observation_enum != null)
                value = observation_enum.getDescription();
            else
                value = observation.getInterpretation();
            if (value != null && !value.equals(context.getResources().getString(R.string.txt_null))) {
                recyclerViewHolder.tvAnalysisValue.setText(value);
            } else {
                recyclerViewHolder.tvAnalysisValue.setText("");
            }
        } else {
            VitalsModel vitalsModel = (VitalsModel) observationHistoryList.get(position);
            recyclerViewHolder.tvDateValue.setText(convertDate(vitalsModel.getDate()));
            recyclerViewHolder.tvResultValue.setText(vitalsModel.getConvertedValue().replaceFirst("^0+(?!$)", ""));
            recyclerViewHolder.tvAnalysisValue.setText("");
            if(BuildConfig.isToHideInterPretationOnVitals) {
                recyclerViewHolder.tvAnalysisValue.setVisibility(View.GONE);
            }
        }
    }


    private String convertDate(String issueDate) {
        String[] newDate = null;
        try {
            newDate = issueDate.split(" ");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return DateTimeUtil.calculateDate(newDate[0], DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.destinationDateFormatWithoutTime);
    }

    public void setDiastolicValue(ArrayList<Observation> diastolicValueArray) {
        if (diastolicValueArray != null && diastolicValueArray.size() == observationHistoryList.size()) {
            for (int i = 0; i < observationHistoryList.size(); i++) {
                Observation observation = (Observation) observationHistoryList.get(i);
                //  observationHistoryList.get(i).setValue(observation.getValue() + "/" + observation.getValue());
            }
            notifyDataSetChanged();
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDateValue, tvResultValue, tvAnalysisValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDateValue = (TextView) itemView.findViewById(R.id.tv_date_value);
            tvResultValue = (TextView) itemView.findViewById(R.id.tv_result_value);
            tvAnalysisValue = (TextView) itemView.findViewById(R.id.tv_analysis_value);

        }
    }


}
