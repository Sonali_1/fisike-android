package com.mphrx.fisike.record_screen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.ResultStatusEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.record_screen.model.OrderReportModel;
import com.mphrx.fisike.record_screen.view_holder.DiagnosticOrderViewHolder;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.HealthActivity;

import java.util.ArrayList;

/**
 * Created by kailashkhurana on 21/08/17.
 */

public class ResultListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<OrderReportModel> recordList;
    private Context context;
    private DownloadListner listener;
    private ArrayList<Boolean> isListExpanded;


    public ResultListAdapter(Context context, ArrayList<OrderReportModel> recordList) {
        this.context = context;
        this.recordList = recordList;
        isListExpanded = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View HeaderView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.result_group_header, viewGroup, false);
        RecordHeader recordHeader = new RecordHeader(HeaderView);
        return recordHeader;
    }


    @Override
    public int getItemCount() {
        return recordList == null ? 0 : recordList.size();
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder recyclerViewHolder, final int position) {
        if (recyclerViewHolder instanceof RecordHeader) {
            final RecordHeader recordHeader = (RecordHeader) recyclerViewHolder;

            final OrderReportModel item = recordList.get(position);
            recordHeader.tvTitle.setText(item.getTitle().getPatientName());

            String patientId = item.getTitle().getPatientId();

            recordHeader.tvIdTitle.setText(recordHeader.tvIdTitle.getContext().getString(R.string.id) + " ");
            String patientInfo1 = "";
            if (Utils.isValueAvailable(patientId)) {
                patientInfo1 = " " + item.getTitle().getPatientId();
            } else {
                patientInfo1 = " " + recordHeader.tvIdTitle.getContext().getString(R.string.unknown);
            }
            recordHeader.tvIdValue.setText(patientInfo1);
            recordHeader.tvCell.setText(Utils.isValueAvailable(item.getTitle().getItem()) ? item.getTitle().getItem() : recordHeader.tvCell.getResources().getString(R.string.unknown));
            String status = item.getTitle().getStatus();
            status = ResultStatusEnum.getDisplayedValuefromCode(status);
            recordHeader.tvOrderStatus.setText(Utils.isValueAvailable(status) ? status : recordHeader.tvOrderStatus.getResources().getString(R.string.unavailable));
            String orderNo = item.getTitle().getOrderKey();
            if (Utils.isValueAvailable(orderNo)) {
                recordHeader.layoutOrderNumber.setVisibility(View.VISIBLE);
                recordHeader.tvOrderNumber.setText(orderNo);
            } else {
                recordHeader.layoutOrderNumber.setVisibility(View.GONE);
            }
            String physicianName = item.getTitle().getPhysicianName();
            if (Utils.isValueAvailable(physicianName)) {
                recordHeader.layoutPhysician.setVisibility(View.VISIBLE);
                recordHeader.tvPhysician.setText(physicianName);
            } else {
                recordHeader.layoutPhysician.setVisibility(View.GONE);
            }
            String performingLocationName = item.getTitle().getPerformingLocationName();
            if (Utils.isValueAvailable(performingLocationName)) {
                recordHeader.tvLocation.setText(performingLocationName);
                recordHeader.layoutLocation.setVisibility(View.VISIBLE);
            } else {
                recordHeader.layoutLocation.setVisibility(View.GONE);
            }
            if (item.getTitle().getCategory().equals(recordHeader.tvOrderDate.getContext().getString(R.string.conditional_radiology))) {
                recordHeader.imgReportType.setBackgroundResource(R.drawable.rad_report);
            } else {
                recordHeader.imgReportType.setBackgroundResource(R.drawable.lab_report);
            }

            final ResultReportAdapter resultReportAdapter = new ResultReportAdapter(context, item.getItems(), item.getTitle());

            if (isListExpanded.size() <= position) {
                isListExpanded.add(position, false);
            }

            if (item.getTitle().getDiagnosticReportList() == null || item.getTitle().getDiagnosticReportList().size() <= 0) {
                recordHeader.layoutShow.setVisibility(View.GONE);
                recordHeader.resultCell.setVisibility(View.GONE);
                recordHeader.layoutHide.setVisibility(View.GONE);
            } else if (isListExpanded.get(position) != null && isListExpanded.get(position)) {
                recordHeader.layoutShow.setVisibility(View.GONE);
                recordHeader.resultCell.setVisibility(View.VISIBLE);
                recordHeader.layoutHide.setVisibility(View.VISIBLE);
            } else {
                recordHeader.layoutShow.setVisibility(View.VISIBLE);
                recordHeader.resultCell.setVisibility(View.GONE);
                recordHeader.layoutHide.setVisibility(View.GONE);
            }

            recordHeader.resultCell.setHasFixedSize(true);
            recordHeader.resultCell.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            recordHeader.resultCell.setAdapter(resultReportAdapter);

            recordHeader.layoutShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recordHeader.resultCell.setVisibility(View.VISIBLE);
                    recordHeader.layoutShow.setVisibility(View.GONE);
                    recordHeader.layoutHide.setVisibility(View.VISIBLE);
                    isListExpanded.set(position, true);
                }
            });
            recordHeader.layoutHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recordHeader.resultCell.setVisibility(View.GONE);
                    recordHeader.layoutShow.setVisibility(View.VISIBLE);
                    recordHeader.layoutHide.setVisibility(View.GONE);
                    isListExpanded.set(position, false);
                }
            });

            String scheduledDate = item.getTitle().getScheduledDate();
            String destinationTimeFormat = DateFormat.is24HourFormat(recordHeader.imgOverflow.getContext()) ? DateTimeUtil.destinationDateFormatWithTime24hr : DateTimeUtil.destinationDateFormatWithTimeOld;
            recordHeader.tvOrderDate.setText(Utils.isValueAvailable(scheduledDate) ? DateTimeUtil.convertSourceDestinationDate(scheduledDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, destinationTimeFormat) : recordHeader.tvOrderDate.getResources().getString(R.string.unavailable));


            recordHeader.imgOverflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.itemClicked(item.getTitle().getId(), recordHeader.imgOverflow);
                    }
                }
            });

            recordHeader.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BuildConfig.isPhysicianApp) {
                        Intent intent = new Intent(context, HealthActivity.class);
                        intent.putExtra("title", item.getTitle().getPatientName());
                        intent.putExtra("patientId", item.getTitle().getPatientMogoId());
                        context.startActivity(intent);
                    }
                }
            });

            if (item.getTitle().isPatientDeceased()) {
                recordHeader.layoutCell.setBackgroundColor(recordHeader.layoutCell.getResources().getColor(R.color.died_bg));
                recordHeader.tvBornTitle.setText(recordHeader.tvBornTitle.getContext().getString(R.string.txt_patient_died) + " ");
                String patientInfo = "";
                String patientDied = item.getTitle().getPatientDeceasedDate();

                if (Utils.isValueAvailable(patientDied)) {
                    try {
                        patientInfo = " " + DateTimeUtil.getFormattedDateWithoutTime(patientDied, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                    } catch (Exception e) {

                    }
                } else {
                    patientInfo = " " + recordHeader.tvTitle.getContext().getString(R.string.unknown);
                }
                recordHeader.tvBornValue.setText(patientInfo);
                recordHeader.tvTitle.setTextColor(context.getResources().getColor(R.color.gray80));

            } else {
                recordHeader.tvBornTitle.setText(recordHeader.tvBornTitle.getContext().getString(R.string.txt_born) + " ");
                String patientInfo = "";
                String patientDob = item.getTitle().getPatientDob();

                if (Utils.isValueAvailable(patientDob)) {
                    try {
                        String age = Utils.calculateAgeAsPerConstraints(patientDob, null, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, false);
                        patientInfo = " " + DateTimeUtil.getFormattedDateWithoutTime(patientDob, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z) + "(" + age + ")";
                    } catch (Exception e) {

                    }
                } else {
                    patientInfo = " " + recordHeader.tvTitle.getContext().getString(R.string.unknown);
                }
                recordHeader.tvBornValue.setText(patientInfo);
                recordHeader.tvTitle.setTextColor(context.getResources().getColor(R.color.dusky_blue));
                recordHeader.layoutCell.setBackgroundColor(recordHeader.layoutCell.getResources().getColor(R.color.white));
            }

        }
    }

    public void setListener(ResultListAdapter.DownloadListner listener) {
        this.listener = listener;
    }

    public class RecordHeader extends RecyclerView.ViewHolder {
        private LinearLayout layoutCell;
        private CardView cardView;
        private ImageView imgReportType;
        private RelativeLayout layoutShow;
        private RelativeLayout layoutHide;
        private CustomFontTextView tvTitle;
        private CustomFontTextView tvBornTitle;
        private CustomFontTextView tvBornValue;
        private CustomFontTextView tvIdTitle;
        private CustomFontTextView tvIdValue;
        private CustomFontTextView tvCell;
        private CustomFontTextView tvOrderStatus;
        private CustomFontTextView tvOrderNumber;
        private CustomFontTextView tvOrderDate;
        private CustomFontTextView tvPhysician;
        private CustomFontTextView tvLocation;
        private IconTextView imgOverflow;
        public DiagnosticOrderViewHolder.DownloadListner listener;
        private LinearLayout layoutPhysician;
        private LinearLayout layoutOrderNumber;
        private LinearLayout layoutLocation;
        private RecyclerView resultCell;

        public RecordHeader(View v) {
            super(v);
            tvTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_title);
            cardView = (CardView) itemView.findViewById(R.id.card_view1);
            tvBornTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_born_title);
            tvIdTitle = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_id_title);
            tvBornValue = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_born_value);
            tvIdValue = (CustomFontTextView) itemView.findViewById(R.id.tv_sub_id_value);
            tvCell = (CustomFontTextView) itemView.findViewById(R.id.tv_cell);
            tvOrderStatus = (CustomFontTextView) itemView.findViewById(R.id.tv_order_status);
            tvOrderNumber = (CustomFontTextView) itemView.findViewById(R.id.tv_order_number);
            tvOrderDate = (CustomFontTextView) itemView.findViewById(R.id.tv_order_date);
            tvPhysician = (CustomFontTextView) itemView.findViewById(R.id.tv_order_physician);
            tvLocation = (CustomFontTextView) itemView.findViewById(R.id.tv_location);
            layoutShow = (RelativeLayout) itemView.findViewById(R.id.layout_show);
            imgReportType = (ImageView) itemView.findViewById(R.id.img_report_type);
            imgOverflow = (IconTextView) itemView.findViewById(R.id.imgOverflow);
            layoutPhysician = (LinearLayout) itemView.findViewById(R.id.layout_physician);
            layoutOrderNumber = (LinearLayout) itemView.findViewById(R.id.layout_order_number);
            layoutLocation = (LinearLayout) itemView.findViewById(R.id.layout_location);
            resultCell = (RecyclerView) itemView.findViewById(R.id.result_cell);
            layoutHide = (RelativeLayout) itemView.findViewById(R.id.layout_hide);
            layoutCell = (LinearLayout) itemView.findViewById(R.id.layout_cell);
        }
    }

    public interface DownloadListner {
        public void itemClicked(String orderId, IconTextView imgOverFlow);
    }
}
