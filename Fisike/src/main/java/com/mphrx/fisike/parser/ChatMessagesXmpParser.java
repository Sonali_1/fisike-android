package com.mphrx.fisike.parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.mphrx.fisike.beans.ChatMessageXmlObj;

public class ChatMessagesXmpParser {

    public ChatMessageXmlObj parseXml(String xml) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();

        parser.setInput(new StringReader(xml));

        int eventType = parser.getEventType();
        ChatMessageXmlObj chatObj = null;
        ArrayList<ChatMessageXmlObj.ChatMessage> chat = new ArrayList<ChatMessageXmlObj.ChatMessage>();
        String id;
        String to;
        String type;
        int index;
        int count;
        String start;
        String with;
        String subject;
        ChatMessageXmlObj.ChatMessage tempChat = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name;
            int last;
            int first;
            String secs;
            String body;
            boolean isSendByMe;

            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("iq")) {
                        chatObj = new ChatMessageXmlObj();
                        id = parser.getAttributeValue("", "id");
                        to = parser.getAttributeValue("", "to");
                        type = parser.getAttributeValue("", "type");
                        chatObj.setTo(to);
                        chatObj.setId(id);
                        chatObj.setType(type);
                    } else if (name.equalsIgnoreCase("chat")) {
                        start = parser.getAttributeValue("", "start");
                        with = parser.getAttributeValue("", "with");
                        subject = parser.getAttributeValue("", "subject");
                        chatObj.setStart(start);
                        chatObj.setWith(with);
                        chatObj.setSubject(subject);
                    } else if (name.equalsIgnoreCase("from")) {
                        tempChat = new ChatMessageXmlObj.ChatMessage();
                        secs = parser.getAttributeValue("", "secs");
                        isSendByMe = false;
                        tempChat.setSecs(secs);
                        tempChat.setSendByMe(isSendByMe);
                    } else if (name.equalsIgnoreCase("to")) {
                        tempChat = new ChatMessageXmlObj.ChatMessage();
                        secs = parser.getAttributeValue("", "secs");
                        isSendByMe = true;
                        tempChat.setSecs(secs);
                        tempChat.setSendByMe(isSendByMe);
                    } else if (name.equalsIgnoreCase("body")) {
                        body = parser.nextText();
                        tempChat.setBody(body);
                    } else if (name.equalsIgnoreCase("first")) {
                        index = parseInt(parser.getAttributeValue("", "index"));
                        first = parseInt(parser.nextText());
                        chatObj.setIndex(index);
                        chatObj.setFirst(first);
                    } else if (name.equalsIgnoreCase("last")) {
                        last = parseInt(parser.nextText());
                        chatObj.setIndex(last);
                    } else if (name.equalsIgnoreCase("count")) {
                        count = parseInt(parser.nextText());
                        chatObj.setCount(count);
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if ((name.equalsIgnoreCase("from") || name.equalsIgnoreCase("to")) && chatObj != null && tempChat != null) {
                        chat.add(tempChat);
                    } else if (name.equalsIgnoreCase("iq") && chatObj != null && chat != null) {
                        chatObj.setChatMessages(chat);
                    }
                    break;
            }
            eventType = parser.next();
        }
        return chatObj;
    }

    private int parseInt(String integer) {
        return Integer.parseInt((integer != null ? integer : "0"));
    }

    // <iq id="test1@dev1" to="chicken@dev1/Smack" type="result"><list xmlns="urn:xmpp:archive"><chat with="test1@dev1" start="2014-05-21T04:44:00.166Z"/><chat with="test1@dev1"
    // start="2014-05-21T05:09:30.207Z"/><set xmlns="http://jabber.org/protocol/rsm"><first index="0">101</first><last>105</last><count>2</count></set></list></iq>

}
