package com.mphrx.fisike.parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.mphrx.fisike.beans.ChatXmlObj;

public class ChatTimeXmlParser {

    public ChatXmlObj parseXml(String xml) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();

        parser.setInput(new StringReader(xml));

        int eventType = parser.getEventType();
        ChatXmlObj chatObj = null;
        ArrayList<ChatXmlObj.Chat> chat = new ArrayList<ChatXmlObj.Chat>();
        String id;
        String to;
        int index;
        int count;
        ChatXmlObj.Chat tempChat = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            String name;
            int last;
            int first;
            String start;
            String with;

            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("iq")) {
                        chatObj = new ChatXmlObj();
                        id = parser.getAttributeValue("", "id");
                        to = parser.getAttributeValue("", "to");
                        chatObj.setTo(to);
                        chatObj.setId(id);
                    } else if (name.equalsIgnoreCase("chat")) {
                        tempChat = new ChatXmlObj.Chat();
                        start = parser.getAttributeValue("", "start");
                        with = parser.getAttributeValue("", "with");
                        tempChat.setStart(start);
                        tempChat.setWith(with);
                    } else if (name.equalsIgnoreCase("first")) {
                        index = parseInt(parser.getAttributeValue("", "index"));
                        first = parseInt(parser.nextText());
                        chatObj.setIndex(index);
                        chatObj.setFirst(first);
                    } else if (name.equalsIgnoreCase("last")) {
                        last = parseInt(parser.nextText());
                        chatObj.setIndex(last);
                    } else if (name.equalsIgnoreCase("count")) {
                        count = parseInt(parser.nextText());
                        chatObj.setCount(count);
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("chat") && chatObj != null && tempChat != null) {
                        chat.add(tempChat);
                    }
                    if (name.equalsIgnoreCase("iq") && chatObj != null && chat != null) {
                        chatObj.setChat(chat);
                    }
                    break;
            }
            eventType = parser.next();
        }
        return chatObj;
    }

    private int parseInt(String integer) {
        return Integer.parseInt((integer != null ? integer : "0"));
    }

    // <iq id="test1@dev1" to="chicken@dev1/Smack" type="result"><list xmlns="urn:xmpp:archive"><chat with="test1@dev1" start="2014-05-21T04:44:00.166Z"/><chat with="test1@dev1"
    // start="2014-05-21T05:09:30.207Z"/><set xmlns="http://jabber.org/protocol/rsm"><first index="0">101</first><last>105</last><count>2</count></set></list></iq>

}