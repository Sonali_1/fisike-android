package com.mphrx.fisike;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.RSDriverException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;

public class SettingWebView extends HeaderActivity {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private WebView wvLoadURL;
    private CustomFontTextView tvError;
    private static final int LOAD_VIEW = 0;
    private static final int ERROR_VIEW = 1;
    String title, url;
    LinearLayout llLoading;
    ProgressBar pbLoading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_webview);
        title = getIntent().getExtras().getString(VariableConstants.TITLE_BAR);
        url = getIntent().getExtras().getString(VariableConstants.WEB_URL);
        findView();

    }


    public void findView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);

        View toolbarBack = null;
        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof ImageButton) {
                toolbarBack = mToolbar.getChildAt(i);
                if (toolbarBack != null) {
                    // change arrow direction
                    if (Utils.isRTL(this)) {
                        toolbarBack.setRotationY(180);
                    } else {
                        toolbarBack.setRotationY(0);
                    }
                }
                break;
            }
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(title);



        wvLoadURL = (WebView) findViewById(R.id.wv_loadUrl);

        tvError = (CustomFontTextView) findViewById(R.id.tv_error);
        pbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        llLoading = (LinearLayout) findViewById(R.id.ll_loading);
        llLoading.setVisibility(View.GONE);

//        if (url.endsWith(".pdf")) {
////            wvLoadURL.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
//            wvLoadURL.getSettings().setJavaScriptEnabled(true);
//            wvLoadURL.getSettings().setBuiltInZoomControls(true);
//            wvLoadURL.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url);
//            wvLoadURL.setWebViewClient(new WebViewClient() {
//                @Override
//                public boolean shouldOverrideUrlLoading(WebView view, String url) {
////                    view.loadUrl(url);
//                    return true;
//                }
//
//                @Override
//                public void onPageFinished(WebView view, String url) {
//                    // TODO Auto-generated method stub
//                    super.onPageFinished(view, url);
//                    llLoading.setVisibility(View.GONE);
//                }
//            });
//            wvLoadURL.loadUrl("https://docs.google.com/viewer?url=" + url);
//            return;
//        }

        wvLoadURL.getSettings().setJavaScriptEnabled(true);
        wvLoadURL.getSettings().setLoadWithOverviewMode(true);
        wvLoadURL.getSettings().setUseWideViewPort(true);
        wvLoadURL.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvLoadURL.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);

        if (url.endsWith(".pdf")) {
//            wvLoadURL.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
            wvLoadURL.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
            return;
        }

//        wvLoadURL.setWebViewClient(new MyBrowser(llLoading, tvError, pbLoading));
        wvLoadURL.setWebChromeClient(new WebChromeClient() {

        });

        wvLoadURL.loadUrl(url);

    }

    private class MyBrowser extends WebViewClient {
        LinearLayout llLoading;
        CustomFontTextView tvError;
        ProgressBar pbLoading;

        public MyBrowser(LinearLayout llLoading, CustomFontTextView tvError, ProgressBar pbLoading) {
            // TODO Auto-generated constructor stub
            this.llLoading = llLoading;
            this.tvError = tvError;
            this.pbLoading = pbLoading;
            tvError.setText(MyApplication.getAppContext().getResources().getString(R.string.please_wait));
            tvError.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(getResources().getString(R.string.http)) || url.startsWith(getResources().getString(R.string.https))) {
                view.loadUrl(url);
                return true;

            } else if (url.startsWith(getResources().getString(R.string.mailto))) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(MyApplication.getAppContext(), userMO));
                intent.setData(Uri.parse(getResources().getString(R.string.mailto))); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                return true;
            }
            return false;

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            llLoading.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            // TODO Auto-generated method stub
            super.onReceivedError(view, errorCode, description, failingUrl);
            pbLoading.setVisibility(View.GONE);
            tvError.setText(MyApplication.getAppContext().getResources().getString(R.string.unable_load_try_after_sometime));
            //wvLoadURL.setVisibility(View.GONE);
        }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
