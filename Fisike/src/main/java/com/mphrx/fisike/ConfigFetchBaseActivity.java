package com.mphrx.fisike;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.android.volley.VolleyError;
import com.mphrx.fisike.Observer.MobileConfigObserver;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike_physician.network.request.UpdateVersionRequest;
import com.mphrx.fisike_physician.network.response.UpdateVersionResponse;
import com.mphrx.fisike_physician.results.Observer.MakeApiCallObserver;
import com.mphrx.fisike_physician.utils.DeviceUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class ConfigFetchBaseActivity extends HeaderActivity implements UpdateVersionRequest.UpdateAppRequestListener {

    private UpdateVersionResponse versionResponse;
    private boolean isDialogShowing = false;
    public boolean isError;
    private UpdateVersionRequest updateRequest;

    @Override
    public void onSuccess(final UpdateVersionResponse response) {
        isError = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                versionResponse = response;
                onSuccessDialog(response);
            }
        });
    }

    @Override
    public void onError(VolleyError error) {
        isError = true;
        redirectToLaunchScreenAndTakeLaunchDecision();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (versionResponse != null && !isDialogShowing) {
            onSuccessDialog(versionResponse);
        }
    }

    public void requestConfig() {
        updateRequest = new UpdateVersionRequest(System.currentTimeMillis());
        updateRequest.registerUpdateAppListener(this);
        ThreadManager.getDefaultExecutorService().submit(updateRequest);
    }

    private void onSuccessDialog(UpdateVersionResponse response) {
        if (updateRequest != null && updateRequest.getmTransactionId() != response
                .getTransactionId())
            return;

        if (response.isSuccessful()) {
            if (response.isForceUpdateRequired()) {
                showUpdateDialog(R.string.fisike_update, R.string.force_a_lot_changed_new_version,
                        R.string.force_please_update_to_continue, true);
            } else {
                SharedPref.setUpdateVersionTimeStamp(System.currentTimeMillis());
                if (response.isSoftUpdateRequired()) {
                    long lastUpdateTimeStamp = SharedPref.getUpdateAlertShownTimeStamp();
                    if (System.currentTimeMillis() - lastUpdateTimeStamp > 7 * 24 * 60 * 60 * 1000) {
                        showUpdateDialog(R.string.fisike_update, R.string.newer_version_available,
                                R.string.would_you_like_to_update, false);
                    } else redirectToLaunchScreenAndTakeLaunchDecision();
                } else redirectToLaunchScreenAndTakeLaunchDecision();
            }
        } else redirectToLaunchScreenAndTakeLaunchDecision();
    }

    private void redirectToLaunchScreenAndTakeLaunchDecision() {
        MobileConfigObserver.getInstance().callNotify(isError);
    }

    private void showUpdateDialog(int title, int bodyFirst, int bodySecond, final boolean isForced) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(bodyFirst, getResources().getString(R.string.app_name)))
                .append("\n").append(getString(bodySecond, getResources().getString(R.string.app_name)));
        dialog.setMessage(stringBuilder.toString());
        int posbuttonId = R.string.yes;

        if (isForced) dialog.setCancelable(false);
        else {
            dialog.setCancelable(true);
            dialog.setNegativeButton(R.string.btn_later, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    isDialogShowing = false;
                    SharedPref.setUpdateAlertShownTimeStamp(System.currentTimeMillis());
                    redirectToLaunchScreenAndTakeLaunchDecision();
                }
            });
        }
        dialog.setPositiveButton(posbuttonId, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(DeviceUtils.getPlayStoreUri()));
                if (!isForced)
                    SharedPref.setUpdateAlertShownTimeStamp(System.currentTimeMillis());
                startActivity(intent);
                isDialogShowing = false;
            }
        });

        dialog.show();
        isDialogShowing = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateRequest != null)
            updateRequest.unregisterUpdateAppListener();
    }

}
