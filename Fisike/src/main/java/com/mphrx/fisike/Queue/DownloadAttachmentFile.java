package com.mphrx.fisike.Queue;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.services.MessengerService;

import java.io.File;

/**
 * Created by ADMIN on 11/19/2015.
 */
public class DownloadAttachmentFile {
    private Context context;
    private String attachmentId;
    private ChatMessageMO chatMessageMo;
    private File file;
    private MessengerService mService;
    private ChatConversationMO chatConversationMO;
    private String fileType;
    private String encodeFile;
    private String appName;

    public DownloadAttachmentFile(Context context, String attachmentId, ChatMessageMO message, ChatConversationMO chatConversationMO,
                                  MessengerService mService, String fileType) {
        this.context = context;
        this.attachmentId = attachmentId;
        this.chatMessageMo = message;
        this.chatConversationMO = chatConversationMO;
        this.mService = mService;
        this.fileType = fileType;
        appName = context.getString(R.string.app_name);

        mService.saveAttachmentPath(chatMessageMo, null, TextConstants.ATTACHMENT_STATUS_WAITING, null, 0, null);

        chatMessageMo.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_WAITING);

        Intent intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED).putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMo).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public String getPersistenceKey() {
        return (chatMessageMo.getPersistenceKey() + "");
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public ChatMessageMO getChatMessageMo() {
        return chatMessageMo;
    }

    public void setChatMessageMo(ChatMessageMO chatMessageMo) {
        this.chatMessageMo = chatMessageMo;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public MessengerService getmService() {
        return mService;
    }

    public void setmService(MessengerService mService) {
        this.mService = mService;
    }

    public ChatConversationMO getChatConversationMO() {
        return chatConversationMO;
    }

    public void setChatConversationMO(ChatConversationMO chatConversationMO) {
        this.chatConversationMO = chatConversationMO;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getEncodeFile() {
        return encodeFile;
    }

    public void setEncodeFile(String encodeFile) {
        this.encodeFile = encodeFile;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
