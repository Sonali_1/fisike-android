package com.mphrx.fisike.Queue;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.services.MessengerService;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ADMIN on 11/19/2015.
 */
public class AttachmentQueue {

    private static final AttachmentQueue attachmentQueue = new AttachmentQueue();
    private LinkedHashMap<String, Object> arrayAttachment;
    private AsyncTask<Void, Void, Void> attachmentAsyn;
    private boolean isDownloaded;

    private AttachmentQueue() {

    }

    public static AttachmentQueue getInstance() {
        return attachmentQueue;
    }

    public void addTask(String persistenceKey, Object object) {
        if (arrayAttachment == null) {
            arrayAttachment = new LinkedHashMap<String, Object>();
        }
        arrayAttachment.put(persistenceKey, object);
        if (attachmentAsyn == null || !attachmentAsyn.getStatus().equals(AsyncTask.Status.RUNNING)) {
            poll(persistenceKey);
        }
    }

    public void poll(String persistenceKey) {
        if (!arrayAttachment.isEmpty()) {
            Iterator<Map.Entry<String, Object>> iterator = arrayAttachment.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                Object object = arrayAttachment.get(key);
                if (object instanceof SendAttachmentFile) {
                    SendAttachmentFile sendAttachmentFile = (SendAttachmentFile) object;
                    if (persistenceKey == null || persistenceKey.equals(sendAttachmentFile.getPersistenceKey())) {
                        attachmentAsyn = new SendAttachmentFileBackground(sendAttachmentFile, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        arrayAttachment.remove(key);
                        isDownloaded = false;
                        return;
                    }
                } else if (object instanceof DownloadAttachmentFile) {
                    DownloadAttachmentFile downloadAttachmentFile = (DownloadAttachmentFile) object;
                    if (persistenceKey == null || persistenceKey.equals(downloadAttachmentFile.getPersistenceKey())) {
                        attachmentAsyn = new DownloadFileBackground((DownloadAttachmentFile) object, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        arrayAttachment.remove(key);
                        isDownloaded = true;
                        return;
                    }
                }
            }
        }
    }

    public boolean cancelRequest(String persistenceKey) {
        if (isDownloaded && attachmentAsyn != null && ((DownloadFileBackground) attachmentAsyn).getPersistenceKey().equals(persistenceKey)) {
            final ChatMessageMO chatMessageMO = ((DownloadFileBackground) attachmentAsyn).getChatMsgMO();
            final ChatConversationMO chatConversationMO = ((DownloadFileBackground) attachmentAsyn).getChatConversationMO();
            final MessengerService mService = ((DownloadFileBackground) attachmentAsyn).getmService();
            final Context context = ((DownloadFileBackground) attachmentAsyn).getContext();
            attachmentAsyn.cancel(true);
            attachmentAsyn = null;
            poll(null);
            cancelRequest(chatMessageMO, mService, chatConversationMO, context, isDownloaded);
            return true;
        } else if (!isDownloaded && attachmentAsyn != null && ((SendAttachmentFileBackground) attachmentAsyn).getPersistenceKey().equals(persistenceKey)) {
            final ChatMessageMO chatMessageMO = ((SendAttachmentFileBackground) attachmentAsyn).getChatMsgMO();
            final ChatConversationMO chatConversationMO = ((SendAttachmentFileBackground) attachmentAsyn).getChatConversationMO();
            final MessengerService mService = ((SendAttachmentFileBackground) attachmentAsyn).getmService();
            final Context context = ((SendAttachmentFileBackground) attachmentAsyn).getContext();
            attachmentAsyn.cancel(true);
            attachmentAsyn = null;
            poll(null);
            cancelRequest(chatMessageMO, mService, chatConversationMO, context, isDownloaded);

            return true;
        } else if (arrayAttachment.isEmpty()) {
            Iterator<Map.Entry<String, Object>> iterator = arrayAttachment.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                if (key.equals(persistenceKey)) {
                    Object object = arrayAttachment.get(key);
                    if (object instanceof SendAttachmentFileBackground) {
                        SendAttachmentFile sendAttachmentFile = ((SendAttachmentFile) object);
                        cancelRequest(sendAttachmentFile.getChatMsgMO(), sendAttachmentFile.getmService(), sendAttachmentFile.getChatConversationMO(), sendAttachmentFile.getContext(), false);
                    } else if (object instanceof DownloadAttachmentFile) {
                        DownloadAttachmentFile downloadAttachmentFile = ((DownloadAttachmentFile) object);
                        cancelRequest(downloadAttachmentFile.getChatMessageMo(), downloadAttachmentFile.getmService(), downloadAttachmentFile.getChatConversationMO(), downloadAttachmentFile.getContext(), false);
                    }
                    arrayAttachment.remove(key);
                    return true;
                }
            }
        }
        return false;
    }

    private void cancelRequest(final ChatMessageMO chatMessageMO, final MessengerService mService, final ChatConversationMO chatConversationMO, final Context context, boolean isDownloadApi) {
        if (isDownloadApi) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    chatMessageMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
                    mService.saveAttachmentPath(chatMessageMO, null, TextConstants.ATTACHMENT_STATUS_RECEIVED, null,
                            VariableConstants.RECORED_SYNC_STATUS_DEFAULT, null);
                    Intent intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                            .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                            .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMO).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, true);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
            }).start();
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    chatMessageMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
                    mService.saveAttachmentUploaded(chatConversationMO, chatMessageMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                            VariableConstants.RECORED_SYNC_STATUS_CANCEL);

                    //update
                    //TODO Right now commenting this because attachment stattus is not store in chat Message MO
                    //TODO if proble arise in cancel request we will update  lateron

 //                    mService.storeChat(chatMessageMO, chatConversationMO, false, true);
                    Intent intent1 = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                            .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                            .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMO).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent1);
                }
            }).start();
        }


    }

    public void removeTaskAllTask() {
        if (attachmentAsyn != null) {
            attachmentAsyn.cancel(true);
            attachmentAsyn = null;
        } else if (arrayAttachment != null && !arrayAttachment.isEmpty()) {
            Iterator<Map.Entry<String, Object>> iterator = arrayAttachment.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                arrayAttachment.remove(key);
            }
        }
    }

    /**
     * Check it the task is running or is in queue
     *
     * @param persistenseKey
     * @return
     */
    public int taskQueueProgress(String persistenseKey) {
        if (attachmentAsyn != null && attachmentAsyn.getStatus().equals(AsyncTask.Status.RUNNING)) {
            if (isDownloaded) {
                DownloadFileBackground downloadFileBackground = (DownloadFileBackground) attachmentAsyn;
                if (downloadFileBackground.getPersistenceKey().equals(persistenseKey)) {
                    return downloadFileBackground.getPercentage();
                }
            } else {
                SendAttachmentFileBackground sendAttachmentFileBackground = (SendAttachmentFileBackground) attachmentAsyn;
                if (sendAttachmentFileBackground.getPersistenceKey().equals(persistenseKey)) {
                    return ((SendAttachmentFileBackground) attachmentAsyn).getPercentage();
                }
            }
        }
        return 0;
    }

    /**
     * Check it the task is running or is in queue
     *
     * @param persistenseKey
     * @return
     */
    public boolean ismTaskQueueExist(String persistenseKey) {
        if (attachmentAsyn != null && attachmentAsyn.getStatus().equals(AsyncTask.Status.RUNNING)) {
            if (isDownloaded) {
                DownloadFileBackground downloadFileBackground = (DownloadFileBackground) attachmentAsyn;
                if (downloadFileBackground.getPersistenceKey().equals(persistenseKey)) {
                    return true;
                }
            } else {
                SendAttachmentFileBackground sendAttachmentFileBackground = (SendAttachmentFileBackground) attachmentAsyn;
                if (sendAttachmentFileBackground.getPersistenceKey().equals(persistenseKey)) {
                    return true;
                }
            }
        }

        if (arrayAttachment == null) {
            return false;
        }
        Iterator<Map.Entry<String, Object>> iterator = arrayAttachment.entrySet().iterator();
        if (iterator != null && iterator.hasNext()) {
            String key = iterator.next().getKey();
            Object object = arrayAttachment.get(key);
            if (object instanceof SendAttachmentFile) {
                if (((SendAttachmentFile) object).getPersistenceKey().equals(persistenseKey)) {
                    return true;
                }
            } else if (object instanceof DownloadAttachmentFile) {
                if (((DownloadAttachmentFile) object).getPersistenceKey().equals(persistenseKey)) {
                    return true;
                }
            }
        }
        return false;
    }
}
