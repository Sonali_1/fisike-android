package com.mphrx.fisike.Queue;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.connection.CustomMultiPartEntity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.SendAttachmentGson;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.AttchmentDBAdapter;
import com.mphrx.fisike.persistence.ChatMessageDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;

/**
 * Created by ADMIN on 11/19/2015.
 */
public class SendAttachmentFileBackground extends AsyncTask<Void, Void, Void> {

    private Context context;
    private File file;
    private ChatMessageMO chatMsgMO;
    private ChatConversationMO chatConversationMO;
    private MessengerService mService;
    private String attachmentType;
    private long fileSize;
    private boolean isNetworkAvailable = true;
    private String attachmentPath;
    private byte[] byteArray;
    private String thumbImagePath;
    private String jsonString;
    private int percentage;
    private AttachmentQueue attachmentQueue;
    private HttpResponse response;
    private static final int NETWORK = 1;
    private static final int ERROR = 2;
    private static final int FINALLY = 3;
    private int executionCode = 0;

    public SendAttachmentFileBackground(SendAttachmentFile sendAttachmentFile, AttachmentQueue attachmentQueue) {
        this.context = sendAttachmentFile.getContext();
        this.file = sendAttachmentFile.getFile();
        this.chatMsgMO = sendAttachmentFile.getChatMsgMO();
        this.chatConversationMO = sendAttachmentFile.getChatConversationMO();
        this.attachmentType = sendAttachmentFile.getAttachmentType();
        this.attachmentPath = sendAttachmentFile.getAttachmentPath();
        this.fileSize = sendAttachmentFile.getFileSize();
        this.isNetworkAvailable = sendAttachmentFile.isNetworkAvailable();
        this.byteArray = sendAttachmentFile.getByteArray();
        this.thumbImagePath = sendAttachmentFile.getThumbImagePath();
        this.attachmentQueue = attachmentQueue;
        this.mService = sendAttachmentFile.getmService();
    }

    public String getPersistenceKey() {
        return (chatMsgMO.getPersistenceKey() + "");
    }


    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String attachmentStatus = TextConstants.ATTACHMENT_STATUS_IN_PROGRESS;
        chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_IN_PROGRESS);
        if (null != chatMsgMO) {
            mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, attachmentStatus, false, null,
                    VariableConstants.RECORED_SYNC_STATUS_CANCEL);
        } else {
            String messageBody = attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE) ? TextConstants.ATTACHMENT_IMAGE_MESSAGE
                    : TextConstants.ATTACHMENT_VIDEO_MESSAGE;
            chatMsgMO = mService.saveMessage(messageBody, attachmentType, chatConversationMO, System.currentTimeMillis());
            saveAttachment(file.getAbsolutePath(), attachmentType, chatConversationMO, chatMsgMO, attachmentStatus, fileSize, false, null, null, 0);
        }

        setChatMsgMO(chatMsgMO);
        chatMsgMO.setAttachmentPath(attachmentPath);

        Intent intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO).putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMsgMO)
                .putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        if (!Utils.isNetworkAvailable(context)) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            executionCode = NETWORK;
            executeSave();
            return null;
        }
        try {
            final byte[] decodFile = Citadel.decodFile(file.getAbsolutePath(), context);

            String fileName = "SendFileTemp";
            if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)) {
                fileName += ".jpg";
            } else {
                fileName += ".mp4";
            }
            chatMsgMO.setPercentage(-1);

            int messageTye = attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE) ? Utils.MEDIA_TYPE_IMAGE : Utils.MEDIA_TYPE_VIDEO;
            File saveToInternalSorage = Utils.saveToInternalSorage(context, messageTye, fileName);
            Utils.copyFile1(context, decodFile, saveToInternalSorage);

            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(APIManager.createMinervaBaseUrl() + "/attachment/sendAttachment");

                   /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
            String authToken = SharedPref.getAccessToken();

            if (authToken != null) {
                httpPost.setHeader("x-auth-token", authToken);
            }

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            // MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            CustomMultiPartEntity entity = new CustomMultiPartEntity(new CustomMultiPartEntity.ProgressListener() {
                @Override
                public boolean transferred(long num) throws InterruptedIOException {
                    percentage = ((int) (num / ((float) decodFile.length) * 100));
                    int chatPercentage = chatMsgMO.getPercentage();
                    if ((percentage == 0 && chatPercentage != percentage) || (percentage == 100 && chatPercentage < 100) || (chatPercentage <= (percentage - 4))) {
                        publishProgress();
                    }
                    return false;
                }
            }, false);
            // int totalSize = (int) entity.getContentLength();

            String fisikeServerIp = SettingManager.getInstance().getSettings().getFisikeServerIp();
            entity.addPart("receiver", new StringBody(Utils.getFisikeUserName(chatConversationMO.getJId(), fisikeServerIp)));
            entity.addPart("type", new StringBody(attachmentType));

            // FileBody fileBody = new FileBody(file);
            FileBody fileBody = new FileBody(saveToInternalSorage);
            entity.addPart("attachment", fileBody);
            httpPost.setEntity(entity);
            response = httpclient.execute(httpPost, new BasicHttpContext());

            // response.addHeader(HTTP.CONTENT_LEN, decodFile.length + "");

            BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            jsonString = sb.toString();

            String attachmentID = null;
            int status = response.getStatusLine().getStatusCode();
            if (status != 200) {
                executionCode = FINALLY;
                return null;
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, SendAttachmentGson.class);

            if (jsonToObjectMapper instanceof SendAttachmentGson) {
                SendAttachmentGson sendAttachmentGson = ((SendAttachmentGson) jsonToObjectMapper);

                if (sendAttachmentGson.getId() != 0) {
                    attachmentID = sendAttachmentGson.getId() + "";
                    chatMsgMO.setAttachmentID(attachmentID);
                    chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_UPLOADED);
                    mService.sendMessage(chatMsgMO.getMessageText(), chatMsgMO.getAttachmentID(), chatMsgMO.getAttachmentType(),
                            chatMsgMO.getLasUpdatedTimeUTC(), chatConversationMO, chatMsgMO, true, false);
                    attachmentStatus = TextConstants.ATTACHMENT_STATUS_UPLOADED;
                    mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, attachmentStatus, true, attachmentID,
                            VariableConstants.RECORED_SYNC_STATUS_DEFAULT);


                    ContentValues values = new ContentValues();
                    values.put(ChatMessageDBAdapter.getATTACHMENTID(), chatMsgMO.getAttachmentID());

                    mService.storeChatMessage(chatMsgMO, chatConversationMO, false, true, values);
                }
            } else {
                chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
                mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                        VariableConstants.RECORED_SYNC_STATUS_CANCEL);
                // TODO status is not any field in chat message Mo so will be updating if needed
                //    mService.storeChat(chatMsgMO, chatConversationMO, false, true);
            }

        } catch (Exception e) {
            executionCode = ERROR;
            executeSave();
        } finally {
            if (null != chatMsgMO && context instanceof MessageActivity) {
                Intent intent1 = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                        .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                        .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMsgMO).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent1);
            }
        }
        return null;
    }

    public Context getContext() {
        return context;
    }

    public void executeSave() {
        if (executionCode == NETWORK) {
            mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                    VariableConstants.RECORED_SYNC_STATUS_CANCEL);

            chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);

            Intent intent1 = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                    .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                    .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMsgMO).putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent1);
        } else if (executionCode == ERROR) {
            chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
            mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                    VariableConstants.RECORED_SYNC_STATUS_CANCEL);
            //update
            //  mService.storeChat(chatMsgMO, chatConversationMO, false, true);
        } else if (executionCode == FINALLY) {
            chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
            mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                    VariableConstants.RECORED_SYNC_STATUS_CANCEL);
            //update
            //    mService.storeChat(chatMsgMO, chatConversationMO, false, true);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        Intent intent = new Intent(TextConstants.BROADCAST_ATTACHMENT_STATUS_CHANGED)
                .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, chatConversationMO)
                .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMsgMO)
                .putExtra(TextConstants.ATTACHMENT_IS_TO_REFRESH_VIEW, false).putExtra(TextConstants.DOWNLOAD_PERCENTAGE, percentage);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        attachmentQueue.poll(null);
        super.onPostExecute(aVoid);
    }

    @Override
    public String toString() {
        return attachmentType + "Chat Message Mo PK : " + chatMsgMO.getPersistenceKey() + " Atachmnet Id : " + chatMsgMO.getAttachmentID();
    }

    public ChatMessageMO getChatMsgMO() {
        return chatMsgMO;
    }

    public ChatConversationMO getChatConversationMO() {
        return chatConversationMO;
    }

    public MessengerService getmService() {
        return mService;
    }

    public void setChatMsgMO(ChatMessageMO chatMsgMO) {
        this.chatMsgMO = chatMsgMO;
    }

    public int getPercentage() {
        return percentage;
    }


    /**
     * Set attachmentMo object
     *
     * @param path
     * @param attachmentType
     * @param chatConversationMO
     * @param chatMsgMO
     * @param attachmentStatus
     * @param isUplodedSucessfully
     * @param thumbImage
     * @param thumbImagePath
     * @param thumbImageStaus
     * @return
     */
    public AttachmentMo saveAttachment(String path, String attachmentType, ChatConversationMO chatConversationMO, ChatMessageMO chatMsgMO,
                                       String attachmentStatus, long fileSize, boolean isUplodedSucessfully, byte[] thumbImage, String thumbImagePath, int thumbImageStaus) {

        UserMO userMO = SettingManager.getInstance().getUserMO();

        attachmentPath = path;

        AttachmentMo attachmentMo = new AttachmentMo();
        attachmentMo.setAttachmentType(attachmentType);
        attachmentMo.setAttachmentPath(path);
        attachmentMo.setFileSize(fileSize);
        attachmentMo.setChatConversationMOPKey(chatConversationMO.getPersistenceKey() + "");
        attachmentMo.setChatMessageMOPKey(chatMsgMO.getPersistenceKey() + "");
        String nickName = chatConversationMO.getNickName();
        attachmentMo.setReceiverUserName((null != nickName && !"".equals(nickName)) ? nickName : chatConversationMO.getJId());
        String realName = userMO.getRealName();
        attachmentMo.setSenderUserName((null != realName && !"".equals(realName)) ? realName : userMO.getUsername());
        attachmentMo.setStatus(attachmentStatus);

        if (isUplodedSucessfully) {
            attachmentMo.generatePersistenceKey(chatConversationMO.getJId());
        }

        attachmentMo.setImageThumbnailPath(thumbImagePath);
        attachmentMo.setImageThumbnail(thumbImage);
        // Save Message to DB
        return mService.insertAttachmentMo(attachmentMo) ? attachmentMo : null;
    }

    public void cancelRequest() {

        ContentValues chatMessageUpdate = new ContentValues();
        chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED);
        mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED, false, null,
                VariableConstants.RECORED_SYNC_STATUS_CANCEL);
        //update
        /*        TODO Right now removed call because
                  it is not updating anything in chat Message Mo
                  if a problem identified while cancelling request  need to update it

        */

        //  mService.storeChatMessage(chatMsgMO, chatConversationMO, false, true);

    }
}
