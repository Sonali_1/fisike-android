package com.mphrx.fisike.Queue;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.util.Log;

import com.mphrx.fisike.background.DownloadUploadedFileTask;
import com.mphrx.fisike.utils.ThreadPoolExecutorProfilePic;
import com.mphrx.fisike_physician.utils.AppLog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kailash Khurana on 3/30/2016.
 */
public class UploadDocumentTaskManager {

    // Sets the amount of time an idle thread will wait for a task before
    // terminating
    private static final int KEEP_ALIVE_TIME = 60;

    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    private static final int CORE_POOL_SIZE = 1;

    private static final int MAXIMUM_POOL_SIZE = 10;
    private static UploadDocumentTaskManager sInstance = null;
    // A queue of Runnable for the image download pool
    private final BlockingQueue<Runnable> mTaskQueue;
    // A managed pool of background download threads
    private final ThreadPoolExecutorProfilePic mTaskThreadPool;
    private Handler mHandler;

    // A static block that sets class fields

    static {
        // The time unit for "keep alive" is in seconds
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

        // Creates a single static instance of PhotoManager
        if (sInstance == null) {
            sInstance = new UploadDocumentTaskManager();
            AppLog.d("init", "new TaskManager");
        }

    }

    @SuppressLint("HandlerLeak")
    private UploadDocumentTaskManager() {
        mTaskQueue = new LinkedBlockingQueue<Runnable>();

        mTaskThreadPool = new ThreadPoolExecutorProfilePic(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mTaskQueue);
    }

    public static UploadDocumentTaskManager getInstance() {
        return sInstance;
    }

    public Handler getHandler() {
        return mHandler;
    }

    /**
     * Check it the task is running or is in queue
     *
     * @param docId
     * @return
     */
    public boolean ismTaskQueueExist(int docId) {
        BlockingQueue<Runnable> queue = mTaskThreadPool.getQueue();
        Iterator<Runnable> iterator2 = queue.iterator();
        while (iterator2.hasNext()) {
            Runnable task = iterator2.next();
            if (null == task) {
                continue;
            }
            DownloadUploadedFileTask next = (DownloadUploadedFileTask) task;
            if (docId == next.getDocId()) {
                return true;
            }
        }

        ArrayList<Runnable> activeThread = mTaskThreadPool.getActiveThread();
        for (int i = 0; null != activeThread && i < activeThread.size(); i++) {
            Runnable task = activeThread.get(i);
            if (null == task) {
                continue;
            }
            DownloadUploadedFileTask next = (DownloadUploadedFileTask) task;
            if (docId == next.getDocId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove the task if exist in the queue for profile pic updation
     *
     * @param docId
     */
    public void removeTaskIfExist(int docId) {
        BlockingQueue<Runnable> queue = mTaskThreadPool.getQueue();
        Iterator<Runnable> iterator2 = queue.iterator();
        while (iterator2.hasNext()) {
            Runnable task = iterator2.next();
            DownloadUploadedFileTask next = (DownloadUploadedFileTask) task;
            if (docId == next.getDocId()) {
                queue.remove(next);
                return;
            }
        }

        ArrayList<Runnable> activeThread = mTaskThreadPool.getActiveThread();
        for (int i = 0; null != activeThread && i < activeThread.size(); i++) {
            Runnable task = activeThread.get(i);
            DownloadUploadedFileTask next = (DownloadUploadedFileTask) task;
            if (docId == (next.getDocId())) {
                return;
            }
        }
        return;

    }

    /**
     * Remove all the task in the queue and active thread if exist
     */
    public void removeTaskAllTask() {
        try {

            while (mTaskQueue != null && !mTaskQueue.isEmpty()) {
                mTaskThreadPool.remove(mTaskQueue.element());
            }
            if (mTaskQueue != null) {
                mTaskQueue.clear();
            }
            if (mTaskThreadPool != null && !mTaskThreadPool.isShutdown()) {
                mTaskThreadPool.purge();
                mTaskThreadPool.removeActiveThreadArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addTask(Runnable task) {
        int docId = 0;
        docId = ((DownloadUploadedFileTask) task).getDocId();
        if (0 == docId) {
            return;
        }
        if (ismTaskQueueExist(docId)) {
            sInstance.mTaskQueue.remove(task);
            return;
        }
        sInstance.mTaskQueue.offer(task);
    }

    public void startTask() {
        if (!sInstance.mTaskQueue.isEmpty()) {
            sInstance.mTaskThreadPool.execute(sInstance.mTaskQueue.poll());
        }
        return;
    }
}
