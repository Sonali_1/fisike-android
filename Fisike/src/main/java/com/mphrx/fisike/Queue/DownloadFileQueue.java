package com.mphrx.fisike.Queue;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.mphrx.fisike.background.LoadContactsProfilePic;
import com.mphrx.fisike.background.LoadGroupContactProfilePic;
import com.mphrx.fisike.utils.ThreadPoolExecutorProfilePic;
import com.mphrx.fisike_physician.utils.AppLog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kailash Khurana on 3/30/2016.
 */
public class DownloadFileQueue {

    // Sets the amount of time an idle thread will wait for a task before
    // terminating
    private static final int KEEP_ALIVE_TIME = 60;

    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    private static final int CORE_POOL_SIZE = 1;

    private static final int MAXIMUM_POOL_SIZE = 1;
    private static DownloadFileQueue sInstance = null;
    // A queue of Runnable for the image download pool
    private final BlockingQueue<Runnable> mTaskQueue;
    // A managed pool of background download threads
    private final ThreadPoolExecutorProfilePic mTaskThreadPool;
    private Handler mHandler;

    // A static block that sets class fields

    static {
        // The time unit for "keep alive" is in seconds
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

        // Creates a single static instance of PhotoManager
        if (sInstance == null) {
            sInstance = new DownloadFileQueue();
            AppLog.d("init", "new TaskManager");
        }

    }

    @SuppressLint("HandlerLeak")
    private DownloadFileQueue() {
        mTaskQueue = new LinkedBlockingQueue<Runnable>();

        mTaskThreadPool = new ThreadPoolExecutorProfilePic(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mTaskQueue);
    }

    public static DownloadFileQueue getInstance() {
        return sInstance;
    }

    public Handler getHandler() {
        return mHandler;
    }

    /**
     * Check it the task is running or is in queue
     *
     * @param emailId
     * @return
     */
    public boolean ismTaskQueueExist(String emailId) {
        BlockingQueue<Runnable> queue = mTaskThreadPool.getQueue();
        Iterator<Runnable> iterator2 = queue.iterator();
        while (iterator2.hasNext()) {
            Runnable task = iterator2.next();
            if (null == task) {
                continue;
            }
            if (task instanceof LoadContactsProfilePic) {
                LoadContactsProfilePic next = (LoadContactsProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return true;
                }
            } else if (task instanceof LoadGroupContactProfilePic) {
                LoadGroupContactProfilePic next = (LoadGroupContactProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return true;
                }
            }
        }

        ArrayList<Runnable> activeThread = mTaskThreadPool.getActiveThread();
        for (int i = 0; null != activeThread && i < activeThread.size(); i++) {
            Runnable task = activeThread.get(i);
            if (null == task) {
                continue;
            }
            if (task instanceof LoadContactsProfilePic) {
                LoadContactsProfilePic next = (LoadContactsProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return true;
                }
            } else if (task instanceof LoadGroupContactProfilePic) {
                LoadGroupContactProfilePic next = (LoadGroupContactProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Remove the task if exist in the queue for profile pic updation
     *
     * @param emailId
     */
    public void removeTaskIfExist(String emailId) {
        BlockingQueue<Runnable> queue = mTaskThreadPool.getQueue();
        Iterator<Runnable> iterator2 = queue.iterator();
        while (iterator2.hasNext()) {
            Runnable task = iterator2.next();
            if (task instanceof LoadContactsProfilePic) {
                LoadContactsProfilePic next = (LoadContactsProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    queue.remove(next);
                    return;
                }
            } else if (task instanceof LoadGroupContactProfilePic) {
                LoadGroupContactProfilePic next = (LoadGroupContactProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    queue.remove(next);
                    return;
                }
            }
        }

        ArrayList<Runnable> activeThread = mTaskThreadPool.getActiveThread();
        for (int i = 0; null != activeThread && i < activeThread.size(); i++) {
            Runnable task = activeThread.get(i);
            if (task instanceof LoadContactsProfilePic) {
                LoadContactsProfilePic next = (LoadContactsProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return;
                }
            } else if (task instanceof LoadGroupContactProfilePic) {
                LoadGroupContactProfilePic next = (LoadGroupContactProfilePic) task;
                if (emailId.equals(next.getEmailString())) {
                    return;
                }
            }
        }
        return;

    }

    /**
     * Remove all the task in the queue and active thread if exist
     */
    public void removeTaskAllTask() {
        try {

            while (mTaskQueue != null && !mTaskQueue.isEmpty()) {
                mTaskThreadPool.remove(mTaskQueue.element());
            }
            if (mTaskQueue != null) {
                mTaskQueue.clear();
            }
            if (mTaskThreadPool != null && !mTaskThreadPool.isShutdown()) {
                mTaskThreadPool.purge();
                mTaskThreadPool.removeActiveThreadArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addTask(Runnable task) {
        String emailString = null;
        if (task instanceof LoadContactsProfilePic) {
            emailString = ((LoadContactsProfilePic) task).getEmailString();
        } else if (task instanceof LoadGroupContactProfilePic) {
            emailString = ((LoadGroupContactProfilePic) task).getEmailString();
        }
        if (null == emailString) {
            return;
        }
        if (ismTaskQueueExist(emailString)) {
            sInstance.mTaskQueue.remove(task);
            return;
        }
        sInstance.mTaskQueue.offer(task);
    }

    public void startTask() {
        if (!sInstance.mTaskQueue.isEmpty()) {
            sInstance.mTaskThreadPool.execute(sInstance.mTaskQueue.poll());
        }
        return;
    }

}
