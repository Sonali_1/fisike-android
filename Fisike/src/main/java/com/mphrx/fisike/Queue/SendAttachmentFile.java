package com.mphrx.fisike.Queue;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by Kailash on 11/19/2015.
 */
public class SendAttachmentFile {

    private Context context;
    private File file;
    private ChatMessageMO chatMsgMO;
    private ChatConversationMO chatConversationMO;
    private MessengerService mService;
    private String attachmentType;
    private long fileSize;
    private boolean isNetworkAvailable = true;
    private String attachmentPath;
    private byte[] byteArray;
    private String thumbImagePath;

    public SendAttachmentFile(Context context, ChatMessageMO chatMessageMO, final ChatConversationMO chatConversationMO, File file,
                              MessengerService mService, final String attachmentType, final long fileSize, boolean isNetworkAvailable) {
        this.context = context;
        boolean isUpdateAttachmentMo = false;
        this.chatConversationMO = chatConversationMO;
        this.file = file;
        this.mService = mService;
        this.attachmentType = attachmentType;
        this.chatMsgMO = chatMessageMO;
        this.fileSize = fileSize;
        this.attachmentType = attachmentType;

        final String attachmentStatus = isNetworkAvailable ? TextConstants.ATTACHMENT_STATUS_WAITING : TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED;
        if (null != chatMsgMO) {
            chatMsgMO.setAttachmentStatus(attachmentStatus);
            mService.saveAttachmentUploaded(chatConversationMO, chatMsgMO, attachmentStatus, false, null,
                    VariableConstants.RECORED_SYNC_STATUS_CANCEL);
        } else {
            isUpdateAttachmentMo = true;
            String messageBody;
            Bitmap thumbImage = MyApplication.getImageThumb();

            if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)) {
                messageBody = TextConstants.ATTACHMENT_IMAGE_MESSAGE;
                // thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getAbsolutePath()),
                // MediaStore.Images.Thumbnails.MICRO_KIND, MediaStore.Images.Thumbnails.MICRO_KIND);
            } else {
                messageBody = TextConstants.ATTACHMENT_VIDEO_MESSAGE;
                // thumbImage = ThumbnailUtils.createVideoThumbnail(file.getAbsolutePath(), MediaStore.Images.Thumbnails.MICRO_KIND);
            }
            chatMsgMO = mService.saveMessage(messageBody, attachmentType, chatConversationMO, System.currentTimeMillis());
            chatMsgMO.setAttachmentStatus(attachmentStatus);
            byteArray = null;
            thumbImagePath = null;
            if (thumbImage != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                thumbImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                byteArray = stream.toByteArray();
                if (context instanceof MessageActivity) {
                    File thumbFile = Utils.createFile(Environment.getExternalStorageDirectory(),
                            File.separator + context.getString(R.string.app_name) + File.separator + VariableConstants.THUMB_PATH_STRING,
                            System.currentTimeMillis() + "");
                    ((MessageActivity) context).encriptThumb(thumbFile);
                    thumbImagePath = thumbFile.getAbsolutePath();
                    chatMsgMO.setImageThumbnailPath(thumbImagePath);
                    chatMsgMO.setImageRecordSyncStatus(VariableConstants.RECORED_SYNC_STATUS_DEFAULT);
                }
            }
            attachmentPath = file.getAbsolutePath();

            chatMsgMO.setAttachmentPath(attachmentPath);

            try {
                if (context instanceof MessageActivity) {
                    ((MessageActivity) context).refreshAttachment(chatConversationMO, chatMsgMO, false, -1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            new Thread(new Runnable() {
                public void run() {
                    saveAttachment(attachmentPath, attachmentType, chatConversationMO, chatMsgMO, attachmentStatus, fileSize, false, byteArray,
                            thumbImagePath, VariableConstants.RECORED_SYNC_STATUS_DEFAULT);
                }
            }).start();

        }
    }

    public String getPersistenceKey() {
        return (chatMsgMO.getPersistenceKey() + "");
    }

    /**
     * Set attachmentMo object
     *
     * @param path
     * @param attachmentType
     * @param chatConversationMO
     * @param chatMsgMO
     * @param attachmentStatus
     * @param isUplodedSucessfully
     * @param thumbImage
     * @param thumbImagePath
     * @param thumbImageStaus
     * @return
     */
    public AttachmentMo saveAttachment(String path, String attachmentType, ChatConversationMO chatConversationMO, ChatMessageMO chatMsgMO,
                                       String attachmentStatus, long fileSize, boolean isUplodedSucessfully, byte[] thumbImage, String thumbImagePath, int thumbImageStaus) {

        UserMO userMO = SettingManager.getInstance().getUserMO();

        attachmentPath = path;


        AttachmentMo attachmentMo = new AttachmentMo();
        attachmentMo.setAttachmentType(attachmentType);
        attachmentMo.setAttachmentPath(path);
        attachmentMo.setFileSize(fileSize);
        attachmentMo.setChatConversationMOPKey(chatConversationMO.getPersistenceKey() + "");
        attachmentMo.setChatMessageMOPKey(chatMsgMO.getPersistenceKey() + "");
        String nickName = chatConversationMO.getNickName();
        attachmentMo.setReceiverUserName((null != nickName && !"".equals(nickName)) ? nickName : chatConversationMO.getJId());
        String realName = userMO.getRealName();
        attachmentMo.setSenderUserName((null != realName && !"".equals(realName)) ? realName : userMO.getUsername());
        attachmentMo.setStatus(attachmentStatus);

        if (isUplodedSucessfully) {
            attachmentMo.generatePersistenceKey(chatConversationMO.getJId());
        }

        attachmentMo.setImageThumbnailPath(thumbImagePath);
        attachmentMo.setImageThumbnail(thumbImage);
        // Save Message to DB
        return mService.insertAttachmentMo(attachmentMo) ? attachmentMo : null;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ChatMessageMO getChatMsgMO() {
        return chatMsgMO;
    }

    public void setChatMsgMO(ChatMessageMO chatMsgMO) {
        this.chatMsgMO = chatMsgMO;
    }

    public ChatConversationMO getChatConversationMO() {
        return chatConversationMO;
    }

    public MessengerService getmService() {
        return mService;
    }

    public void setmService(MessengerService mService) {
        this.mService = mService;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public boolean isNetworkAvailable() {
        return isNetworkAvailable;
    }

    public void setIsNetworkAvailable(boolean isNetworkAvailable) {
        this.isNetworkAvailable = isNetworkAvailable;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    public String getThumbImagePath() {
        return thumbImagePath;
    }

    public void setThumbImagePath(String thumbImagePath) {
        this.thumbImagePath = thumbImagePath;
    }
}
