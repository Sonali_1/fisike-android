package com.mphrx.fisike.Queue;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by ADMIN on 11/18/2015.
 */
public class UploadRecordObject {
    private Context context;
    private Bitmap bitmap;
    private String attachmentPath;
    private String category;
    private boolean isToDecode;
    private String actualFilePath;
    private int mimeType;

    public UploadRecordObject(Context context, Bitmap bitmap, String attachmentPath, String category, boolean isToDecode, String actualFilePath, int mimeType) {
        this.context = context;
        this.bitmap = bitmap;
        this.attachmentPath = attachmentPath;
        this.category = category;
        this.isToDecode = isToDecode;
        this.actualFilePath = actualFilePath;
        this.mimeType = mimeType;
    }

    public int getMimeType() {
        return mimeType;
    }

    public void setMimeType(int mimeType) {
        this.mimeType = mimeType;
    }

    public String getActualFilePath() {
        return actualFilePath;
    }

    public void setActualFilePath(String actualFilePath) {
        this.actualFilePath = actualFilePath;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isToDecode() {
        return isToDecode;
    }

    public void setIsToDecode(boolean isToDecode) {
        this.isToDecode = isToDecode;
    }
}