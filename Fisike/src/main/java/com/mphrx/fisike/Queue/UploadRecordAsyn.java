package com.mphrx.fisike.Queue;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.UpdateDatabaseAsyncTask;
import com.mphrx.fisike.connection.CustomMultiPartEntityUploadRecords;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.DocumentReferenceResponse;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by ADMIN on 11/18/2015.
 */
public class UploadRecordAsyn extends AsyncTask<Void, Void, Void> {
    private static final String DOCUMENT_REFERENCE = "/DocumentReference/uploadAndSave";
    private static final String PATIENT_ID = "patientId";
    private static final String TYPE = "type";
    private static final String CREATED = "created";
    private static final String APPLICATIONID = "applicationId";
    private static final String FILETYPE = "fileType";
    private final String actualFilePath;
    private final int mimeType;


    private UploadRecordQueue uploadRecordQueue;
    private Context context;
    private byte[] fileData;
    private String jsonString;
    private DocumentReferenceResponse documentReferenceResponse;
    private Bitmap bitmap;
    private String attachmentPath;
    private String category;
    private boolean isToDecode;
    private int percentage;
    private boolean isToRun;
    private DocumentReferenceModel executeMultipartRequest;

    public UploadRecordAsyn(UploadRecordObject uploadRecordObject, UploadRecordQueue uploadRecordQueue) {
        this.context = uploadRecordObject.getContext();
        this.bitmap = uploadRecordObject.getBitmap();
        this.attachmentPath = uploadRecordObject.getAttachmentPath();
        this.category = uploadRecordObject.getCategory();
        this.isToDecode = uploadRecordObject.isToDecode();
        this.uploadRecordQueue = uploadRecordQueue;
        this.actualFilePath = uploadRecordObject.getActualFilePath();
        this.mimeType = uploadRecordObject.getMimeType();
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (isToDecode) {
            fileData = Citadel.decodFile(attachmentPath, context);
        } else if (mimeType == VariableConstants.MIME_TYPE_FILE || mimeType == VariableConstants.MIME_TYPE_DOC || mimeType == VariableConstants.MIME_TYPE_DOCX) {
            File file = new File(actualFilePath);
            int size = (int) file.length();
            fileData = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                buf.read(fileData, 0, fileData.length);
                buf.close();
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
        }
        APIManager apiManager = APIManager.getInstance();
        String documentReferenceURL = apiManager.getMinervaPlatformURL(context, DOCUMENT_REFERENCE);
        executeMultipartRequest = executeMultipartRequest(documentReferenceURL);
        return null;
    }

    public DocumentReferenceModel executeMultipartRequest(String url) {
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
                    /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
            String authToken = SharedPref.getAccessToken();
            httpPost.setHeader("x-auth-token", authToken);

            // MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            CustomMultiPartEntityUploadRecords entity = new CustomMultiPartEntityUploadRecords(new CustomMultiPartEntityUploadRecords.ProgressListener() {

                @Override
                public boolean transferred(long num) throws InterruptedIOException {
                    int tempPercentage = percentage;
                    percentage = ((int) (num / ((float) fileData.length) * 100));
                    if (tempPercentage != percentage) {
                        publishProgress();
                    }
                    return false;
                }

            }, false);
            entity.addPart(PATIENT_ID, new StringBody(Utils.getPatientId() + ""));
            entity.addPart(TYPE, new StringBody(category));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.US);
            String strDate = sdf.format(cal.getTime());
            entity.addPart(CREATED, new StringBody(strDate));
            long currentTimeStamp = System.currentTimeMillis();
            entity.addPart(APPLICATIONID, new StringBody("" + currentTimeStamp));
            entity.addPart(FILETYPE, new StringBody(TextConstants.NON_DICOM));


            if (null != bitmap) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                fileData = stream.toByteArray();
            }
            if (fileData != null) {
                String extention = null;
                switch (mimeType) {
                    case VariableConstants.MIME_TYPE_IMAGE:
                        extention = ".jpg";
                        break;
                    case VariableConstants.MIME_TYPE_FILE:
                        extention = ".pdf";
                        break;
                    case VariableConstants.MIME_TYPE_DOC:

                        extention = ".doc";
                        break;
                    case VariableConstants.MIME_TYPE_DOCX:
                        extention = ".docx";
                        break;
                    default:
                        extention = ".jpg";
                }
                entity.addPart("file", new InputStreamBody(new ByteArrayInputStream(fileData), System.currentTimeMillis() + extention));
            }
            httpPost.setEntity(entity);

            HttpResponse response = httpclient.execute(httpPost, new BasicHttpContext());

            BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            jsonString = sb.toString();
            documentReferenceResponse = (DocumentReferenceResponse) GsonUtils.jsonToObjectMapper(jsonString, DocumentReferenceResponse.class);
            int id = documentReferenceResponse.getId();
            String type = documentReferenceResponse.getType().getText();
            String MID = documentReferenceResponse.getMasterIdentifier().getValue();
            DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
            documentReferenceModel.setDocumentID(id);
            documentReferenceModel.setDocumentType(type);
            documentReferenceModel.setDocumentMasterIdentifier(MID);
            documentReferenceModel.setPatientID(Utils.getPatientId());
            documentReferenceModel.setDocumentLocalPath(attachmentPath);
            return documentReferenceModel;
        } catch (
                Exception e
                )

        {
            e.getStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        ContentValues updatedValues = new ContentValues();
        updatedValues.put(DocumentReferenceDBAdapter.getPERCENTAGE(), percentage);
        DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
        documentReferenceModel.setDocumentLocalPath(attachmentPath);
        new UpdateDatabaseAsyncTask(documentReferenceModel, DatabaseTables.DOCUMENT_REFERENCE_TABLE, updatedValues, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (executeMultipartRequest == null) {
            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.error_upload_record),
                    Toast.LENGTH_SHORT).show();
            ContentValues updatedValues = new ContentValues();
            updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_REDDY);
            updatedValues.put(DocumentReferenceDBAdapter.getPERCENTAGE(), 0);
            DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
            documentReferenceModel.setDocumentLocalPath(attachmentPath);
            new UpdateDatabaseAsyncTask(documentReferenceModel, DatabaseTables.DOCUMENT_REFERENCE_TABLE, updatedValues)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            ContentValues updatedValues = new ContentValues();
            updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTID(), executeMultipartRequest.getDocumentID());
            updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTMASTERIDENTIFIER(), executeMultipartRequest.getDocumentMasterIdentifier());
            updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_DOWNLOADED);
            updatedValues.put(DocumentReferenceDBAdapter.getPERCENTAGE(), 100);
            new UpdateDatabaseAsyncTask(executeMultipartRequest, DatabaseTables.DOCUMENT_REFERENCE_TABLE, updatedValues)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        uploadRecordQueue.poll();

        super.onPostExecute(aVoid);
    }
}
