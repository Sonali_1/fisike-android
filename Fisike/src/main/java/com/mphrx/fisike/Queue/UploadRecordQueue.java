package com.mphrx.fisike.Queue;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.asynctaskmanger.AbstractAsyncTask;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatConversationMO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash on 11/18/2015.
 */
public class UploadRecordQueue {

    private static UploadRecordQueue uploadRecordQueue = new UploadRecordQueue();
    private LinkedHashMap<String, UploadRecordObject> recordObjectArrayList;
    private AsyncTask<Void, Void, Void> uploadRecordAsyn;

    private UploadRecordQueue() {
    }

    public static UploadRecordQueue getInstance() {
        return uploadRecordQueue;
    }

    public void addTask(String attachmentpath, UploadRecordObject uploadRecordObject) {
        if (recordObjectArrayList == null) {
            recordObjectArrayList = new LinkedHashMap<String, UploadRecordObject>();
        }
        recordObjectArrayList.put(attachmentpath, uploadRecordObject);
        if (uploadRecordAsyn == null || !uploadRecordAsyn.getStatus().equals(AsyncTask.Status.RUNNING)) {
            poll();
        }
    }

    public void poll() {
        if (!recordObjectArrayList.isEmpty()) {
            Iterator<Map.Entry<String, UploadRecordObject>> iterator = recordObjectArrayList.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                UploadRecordObject uploadRecordObject = recordObjectArrayList.get(key);
                uploadRecordAsyn = new UploadRecordAsyn(uploadRecordObject, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                recordObjectArrayList.remove(key);
                return;
            }
        }
    }

    public void cancelRequest(String attachmentPath) {
        if (uploadRecordAsyn != null && ((UploadRecordAsyn) uploadRecordAsyn).getAttachmentPath().equals(attachmentPath)) {
            uploadRecordAsyn.cancel(true);
            uploadRecordAsyn = null;
            poll();
        } else if (recordObjectArrayList!=null && recordObjectArrayList.isEmpty()) {
            Iterator<Map.Entry<String, UploadRecordObject>> iterator = recordObjectArrayList.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                if (key.equals(attachmentPath)) {
                    recordObjectArrayList.remove(key);
                    return;
                }
            }
        }
    }

    public void removeTaskAllTask() {
        if (uploadRecordAsyn != null) {
            uploadRecordAsyn.cancel(true);
            uploadRecordAsyn = null;
        } else if (recordObjectArrayList!=null && !recordObjectArrayList.isEmpty()) {
            Iterator<Map.Entry<String, UploadRecordObject>> iterator = recordObjectArrayList.entrySet().iterator();
            while (iterator != null && iterator.hasNext()) {
                String key = iterator.next().getKey();
                recordObjectArrayList.remove(key);
            }
        }
    }
}