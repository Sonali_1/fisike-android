package com.mphrx.fisike;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.persistence.AgreementFileDBAdapter;
import com.mphrx.fisike.persistence.PhysicianDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowRequest;
import com.mphrx.fisike.update_user_profile.volleyRequest.PractionerShowResponse;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.background.FetchVitalConfigRequest;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.activity.ContactActivity;
import com.mphrx.fisike_physician.activity.MessengerServiceBindActivity;
import com.mphrx.fisike_physician.network.request.FetchAllAgreementsRequest;
import com.mphrx.fisike_physician.network.request.SetAgreementReadStatus;
import com.mphrx.fisike_physician.network.response.FetchAllAgreementsResponse;
import com.mphrx.fisike_physician.network.response.SetAgreementReadStatusResponse;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import appointment.model.LinkedPatientInfoMO;
import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;
import appointment.request.FetchSelfAndAssociatedPatients;
import appointment.request.FetchSelfAndAssociatedPatientsResponse;
import appointment.utils.CommonTasks;
import searchpatient.activity.SearchPatientFilterActivity;

public class AgreementsActivity extends MessengerServiceBindActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private WebView tvAgreement_content;
    private CustomFontTextView tvReadDateTxt;
    private CustomFontButton btDecline, btAccept;
    private IconTextView bt_toolbar_right;
    private NestedScrollView scrollView;
    private Context context;
    private ArrayList<AgreementModel> agreementModels = new ArrayList<>();
    ProgressBar pbLoading;
    private boolean fromLogin, fromSignup;
    private int agreementNo, totalAgreements;
    private boolean fromSupport;
    private boolean bottomReached;
    private String readDate;
    private long mTransactionId;
    private boolean isScrollable, isShownDialog;
    private String indexes;
    private int updatedAgreementNo;
    private String[] updatedAgreements;
    private String BASE_URL = "http://mytestap.testurl";
    private FrameLayout bottomLayout;
    private RelativeLayout acceptLayout;
    private AgreementModel currentAgreementModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreements);
        fromLogin = getIntent().getBooleanExtra(TextConstants.FROM_LOGIN, false);
        fromSignup = getIntent().getBooleanExtra(TextConstants.FROM_SIGNUP, false);
        fromSupport = getIntent().getBooleanExtra(TextConstants.FROM_SUPPORT, false);
        indexes = getIntent().getStringExtra(TextConstants.INDEXES);
        context = AgreementsActivity.this;
        findView();
        if (fromSupport)
            initViewSupport();
        else
            initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!fromLogin && !fromSupport && !isShownDialog && !fromSignup)
            showDialogforUpdatedAgreements(getResources().getString(R.string.agreement_updated));
    }

    private void initViewSupport() {
        acceptLayout.setVisibility(View.GONE);
        readDate = getIntent().getStringExtra(TextConstants.READ_DATE);
        if (!readDate.equals("")) {
            setReadDate(readDate);
        } else {
            tvReadDateTxt.setVisibility(View.GONE);
        }
        tvAgreement_content.loadDataWithBaseURL(BASE_URL, getIntent().getStringExtra(TextConstants.TEXT), "text/html", "UTF-8", null);
        toolbar_title.setText(getIntent().getStringExtra(TextConstants.NAME_AGREEMENT));
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    private void initView() {
        btAccept.setOnClickListener(this);
        btDecline.setOnClickListener(this);
        acceptLayout.setVisibility(View.VISIBLE);
        tvReadDateTxt.setVisibility(View.GONE);
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
                // if diff is zero, then the bottom has been reached
                if (diff == 0) {
                    bottomReached = true;
                    changeAcceptButtonToBlue();
                }
            }
        });
        ViewTreeObserver observer = scrollView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int viewHeight = scrollView.getMeasuredHeight();
                int contentHeight = scrollView.getChildAt(0).getHeight();
                if (viewHeight - contentHeight < 0) {
                    isScrollable = true;
                    changeAcceptButtonToWhite();
                } else {
                    isScrollable = false;
                    changeAcceptButtonToBlue();
                }
            }
        });
        if (fromSignup) {
            initializeAgreementsSignup();
        } else
            initializeAgreements();
    }

    private void initializeAgreementsSignup() {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        agreementModels = userMO.getAgreements();
        agreementNo = 0;
        totalAgreements = agreementModels.size();
        showAgreement(agreementNo);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void changeAcceptButtonToWhite() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btAccept.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancel_selector));
        } else {
            btAccept.setBackground(getResources().getDrawable(R.drawable.cancel_selector));
        }
        btAccept.setTextColor(getResources().getColor(R.color.black));
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void changeAcceptButtonToBlue() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btAccept.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_button_selectore));
            ;
        } else {
            btAccept.setBackground(getResources().getDrawable(R.drawable.blue_button_selectore));
        }
        btAccept.setTextColor(getResources().getColor(R.color.white));
    }

    private void initializeAgreements() {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        agreementModels = userMO.getAgreements();
        agreementNo = 0;
        updatedAgreementNo = 0;
        totalAgreements = agreementModels.size();
        if (!fromLogin) {
            clearSharedPrefs();
            for (int i = 0; i < agreementModels.size(); i++) {
                SharedPref.setAgreementIds(String.valueOf(agreementModels.get(i).getAgreementId()));
                SharedPref.setContents(agreementModels.get(i).getContentResult().get(0).getContent());
                SharedPref.setUpdatedDates(agreementModels.get(i).getLastUpdated());
            }
        }
        if (fromLogin) {
            showAgreement(agreementNo);
        } else if (!fromLogin) {
            updatedAgreements = indexes.split(",");
            totalAgreements = updatedAgreements.length;
            showAgreement(Integer.parseInt(updatedAgreements[updatedAgreementNo]));
        }
    }

    private void clearSharedPrefs() {
        SharedPref.setContents(null);
        SharedPref.setReadDates(null);
        SharedPref.setAgreementIds(null);
        SharedPref.setUpdatedDates(null);
    }


    private void showAgreement(int i) {
        AgreementModel agreementModel = agreementModels.get(i);
        if (fromSignup)
            currentAgreementModel = agreementModel;
        tvAgreement_content.loadDataWithBaseURL(BASE_URL, agreementModel.getContentResult().get(0).getContent(), "text/html", "UTF-8", null);
        toolbar_title.setText(agreementModel.getContentResult().get(0).getName());
        if (!fromSupport) {
            tvReadDateTxt.setVisibility(View.GONE);
        } else {
            setReadDate(agreementModel.getReadDate());
        }
        bottomReached = false;
        isScrollable = false;
    }

    public void findView() {
        initializeToolbar();
        pbLoading = new ProgressBar(context);
        bottomLayout = (FrameLayout) findViewById(R.id.layoutAction);
        tvAgreement_content = (WebView) findViewById(R.id.agreementContent);
        tvReadDateTxt = (CustomFontTextView) findViewById(R.id.readDate);
        acceptLayout = (RelativeLayout) findViewById(R.id.ll_accept_decline);
        btDecline = (CustomFontButton) findViewById(R.id.btnDecline);
        btAccept = (CustomFontButton) findViewById(R.id.btnAccept);
        scrollView = (NestedScrollView) findViewById(R.id.scroll_view);
        initializewebview();
    }

    private void initializewebview() {
        tvAgreement_content.getSettings().setJavaScriptEnabled(true);
        tvAgreement_content.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (!fromSupport)
                    view.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.contains(BASE_URL)) {
                    url = BASE_URL + url;
                }
                String urlWithoutBaseUrl = url.substring(BASE_URL.length());
                if (urlWithoutBaseUrl != null && url.contains(BASE_URL)) {
                    view.loadUrl(urlWithoutBaseUrl);
                    return true;
                }
                return false;
            }

            /*@Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }*/
        });


    }

    private void initializeToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbarAgreement);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);


        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.INVISIBLE);


        View toolbarBack = null;
        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof ImageButton) {
                toolbarBack = mToolbar.getChildAt(i);
                if (toolbarBack != null) {
                    // change arrow direction
                    if (Utils.isRTL(this)) {
                        toolbarBack.setRotationY(180);
                    } else {
                        toolbarBack.setRotationY(0);
                    }
                }
                break;
            }
        }

        setSupportActionBar(mToolbar);

        if (fromSupport) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (20 * scale + 0.5f);
            if (Utils.isRTL(getApplicationContext())) {
                toolbar_title.setPadding(0, 0, dpAsPixels, 0);
            } else {
                toolbar_title.setPadding(dpAsPixels, 0, 0, 0);
            }
        }

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAccept:
                if (Utils.showDialogForNoNetwork(context, false)) {
                    pbLoading.setVisibility(View.VISIBLE);
                    if (bottomReached || !isScrollable) {
                        showProgressDialog(context.getResources().getString(R.string.accepting));
                        ThreadManager.getDefaultExecutorService().submit(new SetAgreementReadStatus(getTransactionId(), context, TextConstants.STATUS_ACCEPTED,
                                getPayLoad(TextConstants.STATUS_ACCEPTED)));
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.read_entire), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnDecline:
                if (Utils.showDialogForNoNetwork(context, false)) {
                    showProgressDialog(context.getResources().getString(R.string.declining));
                    ThreadManager.getDefaultExecutorService().submit(new
                            SetAgreementReadStatus(getTransactionId(), context, TextConstants.STATUS_DECLINED,
                            getPayLoad(TextConstants.STATUS_DECLINED)));
                }

                break;

            default:
                break;
        }
    }

    private String getPayLoad(String statusAccepted) {
        JSONObject jsonObject = new JSONObject();
        try {
            String date = getCurrentDate();

            jsonObject.put("readDate", date);
            jsonObject.put("status", statusAccepted);
            jsonObject.put("agreementId", agreementModels.get(agreementNo).getAgreementId());
            if (!fromLogin) {
                SharedPref.setReadDates(date);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onBackPressed() {
        if (fromSupport) {
            if (tvAgreement_content.canGoBack()) {
                tvAgreement_content.goBack();
            } else {
                finish();
            }
        } else if (fromLogin) {
            mService.logoutUser(AgreementsActivity.this);
            finish();
        }
    }


    @Subscribe
    public void onAgreementResponse(SetAgreementReadStatusResponse response) throws Exception {
        if (response.getTransactionId() != mTransactionId)
            return;
        if (response.isSuccessful()) {

            if (response.getStatus().equals(TextConstants.STATUS_ACCEPTED)) {
                    /*TODO - insert read date in db for signup*/
                agreementNo++;
                if (fromSignup) {
                    currentAgreementModel.setReadDate(getCurrentDate());
                    AgreementFileDBAdapter.getInstance(context).insertAgreementData(currentAgreementModel);
                }
                fetchOrShowNextAgreemnet(agreementNo, totalAgreements);
            } else {
                dismissAndfinish();
                mService.logoutUser(AgreementsActivity.this);
            }
        } else {
            dismissProgressDialog();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.agreement_fail), Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchOrShowNextAgreemnet(int agreementNo, int totalAgreements) {

        if (agreementNo == totalAgreements && fromSignup) {

            ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(this, getTransactionId(), false));
        } else if (agreementNo == totalAgreements) {
            ThreadManager.getDefaultExecutorService().submit(new FetchAllAgreementsRequest(mTransactionId, context));
        } else if (agreementNo < totalAgreements) {
            dismissProgressDialog();
            tvAgreement_content.setVisibility(View.GONE);
            changeAcceptButtonToWhite();
            if (fromLogin || fromSignup)
                showAgreement(agreementNo);
            else if (!fromLogin)
                showAgreement(Integer.parseInt(updatedAgreements[agreementNo]));
        }
    }

    @Subscribe
    public void onFetchAgreementsResponse(FetchAllAgreementsResponse response) {
        if (response.getTransactionId() != mTransactionId)
            return;
        if (response.isSuccessful()) {
            ArrayList<AgreementModel> arrayList = response.getAgreementModelsList();
            for (int i = 0; i < arrayList.size(); i++) {
                AgreementFileDBAdapter.getInstance(context).insertAgreementData(arrayList.get(i));
            }
            if (BuildConfig.isPatientApp && fromLogin) {
               /* UserMO userMo = SettingManager.getInstance().getUserMO();
                if (userMo.getPatientId() != 0)
                    new getPatientShowDetails(this, userMo.getPatientId() + "").execute();
                else {
                    dismissAndfinish();
                    mService.logoutUser(context);
                }*/
                ThreadManager.getDefaultExecutorService().submit(new FetchSelfAndAssociatedPatients(getTransactionId(), TextConstants.FETCH_SELF));
            } else if (!BuildConfig.isPatientApp && fromLogin) {
                ThreadManager.getDefaultExecutorService().submit(new PractionerShowRequest(getTransactionId(), null));
            } else if (!fromLogin) {
                dismissAndfinish();
            }
        } else {
            if (!fromLogin) {
                String ids[] = SharedPref.getAgreementIds().split("\\^");
                String contents[] = SharedPref.getCONTENTS().split("\\^");
                String readDates[] = SharedPref.getReadDates().split("\\^");
                String updatedDates[] = SharedPref.getUpdatedDates().split("\\^");
                for (int i = 0; i < ids.length; i++) {
                    AgreementFileDBAdapter.getInstance(context).updateAgreementData(Integer.parseInt(ids[i]), readDates[i], contents[i], updatedDates[i], Utils.getUserSelectedLanguage(context));
                }
                clearSharedPrefs();
                dismissAndfinish();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.fetch_fail), Toast.LENGTH_SHORT).show();
                dismissAndfinish();
                mService.logoutUser(AgreementsActivity.this);

            }
        }
    }

    @Subscribe
    public void onGetLinkedPatientsResponse(FetchSelfAndAssociatedPatientsResponse fetchSelfAndAssociatedPatientsResponse) {
        if (mTransactionId != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;

        long persistenceKey = SettingManager.getInstance().getUserMO().getPersistenceKey();
        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful()) {
            ArrayList<LinkedPatientInfoMO> linkedPatients = fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList();
            ArrayList<Coverage> coverages = linkedPatients.get(0).getCoverage();
            ArrayList<FetchUpdateTelecom> phoneList = linkedPatients.get(0).getPhoneList();
            ArrayList<FetchUpdateTelecom> emailList = linkedPatients.get(0).getEmailList();
            ArrayList<FetchUpdateIdentifier> identifiers = linkedPatients.get(0).getIdentifier();


            ContentValues contentValues = new ContentValues();
            if (coverages != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(coverages);
                    contentValues.put(UserDBAdapter.getCOVERAGE(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing phone list in UserMo
            if (phoneList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(phoneList);
                    contentValues.put(UserDBAdapter.getPHONELIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing emailList in usermo
            if (emailList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(emailList);
                    contentValues.put(UserDBAdapter.getEMAILLIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }
            //writing identifierlist in usermo
            if (identifiers != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(identifiers);
                    contentValues.put(UserDBAdapter.getIDENTIFIER(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }
            String maritalStaus=linkedPatients.get(0).getMaritalStatus();
            if(Utils.isValueAvailable(maritalStaus))
                contentValues.put(UserDBAdapter.getMARITALSTATUS(),maritalStaus);

            UserDBAdapter.getInstance(this).updateUserInfo(persistenceKey, contentValues);


            if(Utils.isValueAvailable(linkedPatients.get(0).getProfilePic())) {
                Bitmap profile = Utils.convertBase64ToBitmap(linkedPatients.get(0).getProfilePic());
                if(profile!=null)
                UserProfilePic.saveToInternalStorage(profile, MyApplication.getAppContext());
            }
            if(SharedPref.getIsCoverageUpdated())
            {
                dismissProgressDialog();
                lauchHomeScreen();
            } else {
                UserDBAdapter.getInstance(this).updateSalutation(persistenceKey, TextConstants.NO);
                dismissProgressDialog();
                Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
                mService.logoutUser(this);
            }
        } else {

            UserDBAdapter.getInstance(this).updateSalutation(persistenceKey, TextConstants.NO);
            dismissProgressDialog();
            Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
            mService.logoutUser(this);
        }
    }

    private void dismissAndfinish() {
        dismissProgressDialog();
        finish();
    }

    public String getCurrentDate() {
        SimpleDateFormat df2 = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        return df2.format(new Date());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public long getTransactionId() {
        if (mTransactionId == 0)
            mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    public void executePatientProfileResponse(PatientMO patientMO) {
        UserMO userMo = SettingManager.getInstance().getUserMO();
        if (patientMO != null) {
            UserDBAdapter.getInstance(this).updateSalutation(userMo.getPersistenceKey(), TextConstants.YES);
            lauchHomeScreen();
        } else {
            dismissProgressDialog();
            Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
            mService.logoutUser(context);
        }

    }

    public void lauchHomeScreen() {
        dismissProgressDialog();
        Utils.revertForgetPasswordInitiation();
        SharedPref.setLoginAttempts(0);
        SharedPref.setUserLogout(false);
        Utils.setUserLoggedOut(false);
        if(fromLogin)
            Utils.checkAlternateContactDetails(SettingManager.getInstance().getUserMO().getAlternateContact());
        //new UserProfilePic(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        if (BuildConfig.isPatientApp) {
            // Open Patient Activity
            UserMO userMo = SettingManager.getInstance().getUserMO();

            Intent i = new Intent(this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            if (userMo.getHasSignedUp() != null && userMo.getHasSignedUp())
                i.putExtra(VariableConstants.LAUNCH_UPLOAD_SCREEN, true);

            startActivity(i);
        } else if (BuildConfig.isSearchPatient) {
            openActivity(SearchPatientFilterActivity.class, null, true);

        } else {
            openActivity(ContactActivity.class, null, true);

        }
    }

    @Subscribe
    public void onPhysicianDetailResponse(PractionerShowResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();

        UserMO userMO = SettingManager.getInstance().getUserMO();
        try {
            ContentValues contentValues = new ContentValues();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(context);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytes);
            out.writeObject(response.getPractitioneMOFromResponse().getAddress());
            contentValues.put(userDBAdapter.getADDRESS(), bytes.toByteArray());
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response.isSuccessful() && response.getPractitioneMOFromResponse() != null) {
            try {
                PhysicianDBAdapter.getInstance(this).createOrUpdatePhysicianMo(response.getPractitioneMOFromResponse());
            } catch (Exception e) {
                e.printStackTrace();
            }
            lauchHomeScreen();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            mService.logoutUser(context);
        }
    }

    @Override
    public void onMessengerServiceBind() {

    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void setReadDate(String readDate) {
        tvReadDateTxt.setVisibility(View.VISIBLE);
        tvReadDateTxt.setText(getResources().getString(R.string.agreement_accepted_on) + " " + CommonTasks.formateDateFromstring
                (DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime, readDate));
    }

    private void showDialogforUpdatedAgreements(String updatedAgreement) {
        // TODO Auto-generated method stub
        isShownDialog = true;
        AlertDialog.Builder dialog = new AlertDialog.Builder(AgreementsActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage(updatedAgreement);
        dialog.setPositiveButton(MyApplication.getAppContext().getResources().getString(R.string.ok_got_it), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dia, int arg1) {
                dia.cancel();
            }
        });
        dialog.show();
    }

    @Subscribe
    public void onVitalsConfigResponse(VitalsConfigResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;


        if (response.isSuccessful()) {
            if (response.isValidJson()) // success API JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION_SUCCESS,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS, GoogleAnalyticsConstants.VITALS_API_NAME);
            else //success API but blank JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS_INCORRECT_JSON, GoogleAnalyticsConstants.VITALS_API_NAME);

        } else
            MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                            GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                    GoogleAnalyticsConstants.VITALS_VALUE_FAILURE, GoogleAnalyticsConstants.VITALS_API_NAME);

        if(fromSignup && BuildConfig.isPatientApp)
            ThreadManager.getDefaultExecutorService().submit(new FetchSelfAndAssociatedPatients(getTransactionId(),TextConstants.FETCH_SELF));

    }

}
