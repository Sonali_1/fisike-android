package com.mphrx.fisike;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mphrx.fisike.adapter.settingsAdapter;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.SettingItemData;
import com.mphrx.fisike.lock_screen.LockScreenActivity;
import com.mphrx.fisike.lock_screen.SetUpdateMPIN;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.MyHealthTabItemsHolder;
import com.mphrx.fisike_physician.activity.SettingActivity;
import com.mphrx.fisike_physician.network.response.LanguagePreferenceResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Locale;

public class Settings extends Fragment implements settingsAdapter.clickListener {

    private View myFragmentView;
    private UserMO userMO;
    private ConfigMO config;
    private RecyclerView settingRecyclerView;
    private settingsAdapter settingAdapter;
    private LinearLayoutManager settingLayoutManager;
    private ArrayList<SettingItemData> settingData;
    private String[] title;
    private String[] subTitle;
    private String remove;
    private String add;

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.SETTING_FRAGMENT);
        initializeDataForSetting(settingData, title, subTitle);
        settingRecyclerView.getRecycledViewPool().clear();
        settingAdapter.notifyDataSetChanged();
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.preference_activity, container, false);
        BusProvider.getInstance().register(this);
        remove = getResources().getString(R.string.remove);
        add = getResources().getString(R.string.add_small_letters);

        setValues();
        setHasOptionsMenu(true);
        if (BuildConfig.isPatientApp) {
            ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.txt_string));
        }
        findViews();

        return myFragmentView;
    }


    private void CheckThanAddAndRemove(ArrayList<SettingItemData> temp, String value, String subtitle, String action) {
        int i = 0;
        boolean isContain = false;
        for (SettingItemData data : temp) {
            if (data.getTitle().equals(value)) {
                isContain = true;
                break;
            }
            i++;
        }

        if (isContain && action.equals(remove))
            settingData.remove(i);
        else if (!isContain && action.equals(add)) {
            SettingItemData data = new SettingItemData();
            data.setTitle(value);
            data.setSubTitle(subtitle);
            settingData.add(data);
        }
    }

    private void initializeDataForSetting(ArrayList<SettingItemData> settingData, String[] Title, String[] subTitle) {
        userMO = SettingManager.getInstance().getUserMO();
        String mPin = userMO.getmPIN();
        for (int i = 0; i < Title.length; i++) {
            boolean isMpinTitle = (((Title[i].equalsIgnoreCase(getResources().getString(R.string.add_mpin)))
                    || (Title[i].equalsIgnoreCase(getResources().getString(R.string.security)))
                    || (Title[i].equalsIgnoreCase(getResources().getString(R.string.change_mpin)))
                    || (Title[i].equalsIgnoreCase(getResources().getString(R.string.remove_mpin)))));
            SettingItemData data = new SettingItemData();
            if (!BuildConfig.isMPINEnabled && isMpinTitle) {
                CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                UserDBAdapter.getInstance(getActivity()).updateMPIN(null, userMO);

            } else {
                if (mPin == null || mPin.equals("")) {
                    if (!(Title[i].equals(getResources().getString(R.string.change_mpin)) || Title[i].equals(getResources().getString(R.string.remove_mpin)))) {
                        data.setTitle(Title[i]);
                        data.setSubTitle(subTitle[i]);
                        if (BuildConfig.isPhysicianApp && Title[i].equals(getString(R.string.notifications))) {
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                        } else if (!BuildConfig.isMedicationEnabled && !BuildConfig.isChatEnabled && (Title[i].equals(getString(R.string.notifications)))) {
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                        } else
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], add);
                    } else {
                        CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                    }
                } else {
                    if (!(Title[i].equals(getResources().getString(R.string.add_mpin)))) {
                        data.setTitle(Title[i]);
                        data.setSubTitle(subTitle[i]);
                        if (BuildConfig.isPhysicianApp && Title[i].equals(getString(R.string.notifications))) {
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                        }else if (!BuildConfig.isMedicationEnabled && !BuildConfig.isChatEnabled && (Title[i].equals(getString(R.string.notifications)))) {
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                        } else
                            CheckThanAddAndRemove(settingData, title[i], subTitle[i], add);

                    } else {
                        CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
                    }
                }
            }

            if(config==null){
                config=ConfigManager.getInstance().getConfig();
            }

            if(Title[i].equals(getResources().getString(R.string.change_language)) && config.getLanguageList()!=null && config.getLanguageList().size()<2){
                CheckThanAddAndRemove(settingData, title[i], subTitle[i], remove);
            }

        }
    }

    private void setValues() {
        userMO = SettingManager.getInstance().getUserMO();
        findViews();
        String deviceActivationDate = userMO.getDeviceActivationDate();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    private void findViews() {
        settingRecyclerView = (android.support.v7.widget.RecyclerView) myFragmentView.findViewById(R.id.my_recycler_view);
        settingLayoutManager = new LinearLayoutManager(getActivity());
        settingRecyclerView.setLayoutManager(settingLayoutManager);
        settingData = new ArrayList<SettingItemData>();
        title = getActivity().getResources().getStringArray(R.array.setting_title);
        subTitle = getActivity().getResources().getStringArray(R.array.setting_subTitle);
        initializeDataForSetting(settingData, title, subTitle);
        settingAdapter = new settingsAdapter(settingData, getActivity());
        settingAdapter.setClickListener(Settings.this);
        settingRecyclerView.setAdapter(settingAdapter);

        userMO = SettingManager.getInstance().getUserMO();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        config = ConfigManager.getInstance().getConfig();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relativeLayout:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void itemClicked(View view, int position) {
        // TODO Auto-generated method stub
        showView(settingAdapter.getTitle(position));
    }


    public void showView(String text) {
        Intent i = null;

        if (BuildConfig.isPhysicianApp)
            ((SettingActivity) getActivity()).setCurrentItem(-1);

        if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.add_mpin))) {
            i = new Intent(getActivity(), SetUpdateMPIN.class);
            startActivity(i);
        } else if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.change_mpin))) {
            i = new Intent(getActivity(), SetUpdateMPIN.class);
            startActivity(i);
        } else if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.remove_mpin))) {
            i = new Intent(getActivity(), SetUpdateMPIN.class);
            i.putExtra(SetUpdateMPIN.ISREMOVEMPIN, true);
            startActivity(i);
        } else if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.settings_notification))) {
            i = new Intent(getActivity(), NotificationActivity.class);
            startActivity(i);
        } else if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.Support))) {
            i = new Intent(getActivity(), SupportActivity.class);
            startActivity(i);
        } else if (text.equalsIgnoreCase(getActivity().getResources().getString(R.string.change_language))) {
            i = new Intent(getActivity(), LanguageSelectActivity.class);
            startActivityForResult(i, VariableConstants.FRAGMENT_SETTINGS);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

}


 