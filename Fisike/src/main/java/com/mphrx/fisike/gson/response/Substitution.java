package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;

public class Substitution {

	@Expose
	private Reason_ reason;
	@Expose
	private Type type;

	/**
	 * 
	 * @return The reason
	 */
	public Reason_ getReason() {
		return reason;
	}

	/**
	 * 
	 * @param reason
	 *  The reason
	 */
	public void setReason(Reason_ reason) {
		this.reason = reason;
	}

	/**
	 * 
	 * @return The type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *  The type
	 */
	public void setType(Type type) {
		this.type = type;
	}

}
