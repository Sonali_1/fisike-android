package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhysicianSaveGson {

	@Expose
	private Boolean onlyReturnId;
	@Expose
	private Object address;
	@Expose
	private Object birthDate;
	@Expose
	private Object communication;
	@Expose
	private Object extension;
	@Expose
	private Object gender;
	@Expose
	private Integer id;
	@Expose
	private Object identifier;
	@Expose
	private List<Location> location = new ArrayList<Location>();
	@Expose
	private Name name;
	@Expose
	private Organization organization;
	@Expose
	private Object period;
	@Expose
	private Object photo;
	@Expose
	private Object qualification;
	@SerializedName("class")
	@Expose
	private String _class;
	@Expose
	private Object role;
	@Expose
	private Object speciality;
	@Expose
	private Object telecom;
	
	/**
	 * 
	 * @return The onlyReturnId
	 */
	public Boolean getOnlyReturnId() {
		return onlyReturnId;
	}

	/**
	 * 
	 * @param onlyReturnId
	 *  The onlyReturnId
	 */
	public void setOnlyReturnId(Boolean onlyReturnId) {
		this.onlyReturnId = onlyReturnId;
	}

	/**
	 * 
	 * @return The address
	 */
	public Object getAddress() {
		return address;
	}

	/**
	 * 
	 * @param address
	 *  The address
	 */
	public void setAddress(Object address) {
		this.address = address;
	}

	/**
	 * 
	 * @return The birthDate
	 */
	public Object getBirthDate() {
		return birthDate;
	}

	/**
	 * 
	 * @param birthDate
	 *  The birthDate
	 */
	public void setBirthDate(Object birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * 
	 * @return The communication
	 */
	public Object getCommunication() {
		return communication;
	}

	/**
	 * 
	 * @param communication
	 *  The communication
	 */
	public void setCommunication(Object communication) {
		this.communication = communication;
	}

	/**
	 * 
	 * @return The extension
	 */
	public Object getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(Object extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The gender
	 */
	public Object getGender() {
		return gender;
	}

	/**
	 * 
	 * @param gender
	 * The gender
	 */
	public void setGender(Object gender) {
		this.gender = gender;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The identifier
	 */
	public Object getIdentifier() {
		return identifier;
	}

	/**
	 * 
	 * @param identifier
	 *   The identifier
	 */
	public void setIdentifier(Object identifier) {
		this.identifier = identifier;
	}

	/**
	 * 
	 * @return The location
	 */
	public List<Location> getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 *  The location
	 */
	public void setLocation(List<Location> location) {
		this.location = location;
	}

	/**
	 * 
	 * @return The name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 * The name
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * 
	 * @param organization
	 *  The organization
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * 
	 * @return The period
	 */
	public Object getPeriod() {
		return period;
	}

	/**
	 * 
	 * @param period
	 *  The period
	 */
	public void setPeriod(Object period) {
		this.period = period;
	}

	/**
	 * 
	 * @return The photo
	 */
	public Object getPhoto() {
		return photo;
	}

	/**
	 * 
	 * @param photo
	 * The photo
	 */
	public void setPhoto(Object photo) {
		this.photo = photo;
	}

	/**
	 * 
	 * @return The qualification
	 */
	public Object getQualification() {
		return qualification;
	}

	/**
	 * 
	 * @param qualification
	 * The qualification
	 */
	public void setQualification(Object qualification) {
		this.qualification = qualification;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 * The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

	/**
	 * 
	 * @return The role
	 */
	public Object getRole() {
		return role;
	}

	/**
	 * 
	 * @param role
	 * The role
	 */
	public void setRole(Object role) {
		this.role = role;
	}

	/**
	 * 
	 * @return The speciality
	 */
	public Object getSpeciality() {
		return speciality;
	}

	/**
	 * 
	 * @param speciality
	 * The speciality
	 */
	public void setSpeciality(Object speciality) {
		this.speciality = speciality;
	}

	/**
	 * 
	 * @return The telecom
	 */
	public Object getTelecom() {
		return telecom;
	}

	/**
	 * 
	 * @param telecom
	 * The telecom
	 */
	public void setTelecom(Object telecom) {
		this.telecom = telecom;
	}

}
