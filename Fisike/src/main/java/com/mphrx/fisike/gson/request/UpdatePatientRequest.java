package com.mphrx.fisike.gson.request;

import com.mphrx.fisike.mo.PatientMO;

public class UpdatePatientRequest {

	private String userType;
	private User user;
	private PatientMO patient;
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public PatientMO getPatient() {
		return patient;
	}
	public void setPatient(PatientMO patient) {
		this.patient = patient;
	}
	
}
