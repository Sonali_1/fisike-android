package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.vital_submit.request.DiagnosticOrderId;

public class Extension implements Serializable {
	private static final long serialVersionUID = 6428313811662654915L;

	@Expose
	private String url;
	@Nullable
	private ArrayList<DiagnosticOrderId> value;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<DiagnosticOrderId> getValue() {
		return value;
	}

	public void setValue(ArrayList<DiagnosticOrderId> value) {
		this.value = value;
	}

}
