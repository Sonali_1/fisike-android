package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;

/**
 * Created by Kailash Khurana on 3/15/2016.
 */
public class Code {
    @Expose
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
