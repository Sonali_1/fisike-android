package com.mphrx.fisike.gson.response;

/**
 * Created by Neha on 10-03-2016.
 */
public class GetOrganizationInformationLogoResponse {

    Organization organization;
    byte[] logo;

    public Organization getOrganization() {
        return organization;
    }

    public byte[] getLogo() {
        return logo;
    }
}
