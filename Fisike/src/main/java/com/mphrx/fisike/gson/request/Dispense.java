package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Dispense {

    @Expose
    private Medication medication;
    @Expose
    private Quantity quantity;
    @Expose
    private Integer numberOfRepeatsAllowed;

    /**
     * 
     * @return The medication
     */
    public Medication getMedication() {
        return medication;
    }

    /**
     * 
     * @param medication
     *            The medication
     */
    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    /**
     * 
     * @return The quantity
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * 
     * @param quantity
     *            The quantity
     */
    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    /**
     * 
     * @return The numberOfRepeatsAllowed
     */
    public Integer getNumberOfRepeatsAllowed() {
        return numberOfRepeatsAllowed;
    }

    /**
     * 
     * @param numberOfRepeatsAllowed
     *            The numberOfRepeatsAllowed
     */
    public void setNumberOfRepeatsAllowed(Integer numberOfRepeatsAllowed) {
        this.numberOfRepeatsAllowed = numberOfRepeatsAllowed;
    }

}