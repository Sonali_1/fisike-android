package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.mphrx.fisike.mo.ChatContactMO;

public class ContactListArray {

	private ArrayList<ChatContactMO> resultList;

	public ArrayList<ChatContactMO> getResultList() {
		return resultList;
	}

	public void setResultList(ArrayList<ChatContactMO> resultList) {
		this.resultList = resultList;
	}
}
