package com.mphrx.fisike.gson.response;

import com.mphrx.fisike.mo.UserMO;

public class SignUpResponse {

	private String msg;
	private String status;
	private String httpStatusCode;
	private UserMO user;
	private String verificationCode;
	private Boolean emailVerified;
	private String token;

	public Boolean getEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getMsg() {
		return msg;
	}

	public UserMO getUser() {
		return user;
	}

	public void setUser(UserMO user) {
		this.user = user;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "msg= " + getMsg() + " status= " + getStatus() + " httpstatuscode= " + getHttpStatusCode() +"user"+getUser();
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
