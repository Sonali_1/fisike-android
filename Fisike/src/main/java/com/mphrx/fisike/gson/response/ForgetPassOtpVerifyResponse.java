package com.mphrx.fisike.gson.response;

/**
 * Created by administrate on 12/23/2015.
 */
public class ForgetPassOtpVerifyResponse {
    String msg;
    String status;
    int httpStatusCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
