package com.mphrx.fisike.gson.response;

import java.util.ArrayList;

import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Weight;

public class User {

	long id;
	String username;
	String email;
	String firstName;
	String lastName;
	String phoneNo;
	String gender;
	String dob;
	boolean enabled;
	Weight weight;
	Height height;
	boolean accountLocked;
	long physicianId;
	long patientId;
	ArrayList<Authority> authorities;
	UserType userType;
	ArrayList<UserGroup> userGroups;
	ArrayList<MenuItem> menuItems;
	MenuItem defaultMenuItem;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}

	public Height getHeight() {
		return height;
	}

	public void setHeight(Height height) {
		this.height = height;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public long getPhysicianId() {
		return physicianId;
	}

	public void setPhysicianId(long physicianId) {
		this.physicianId = physicianId;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public ArrayList<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(ArrayList<Authority> authorities) {
		this.authorities = authorities;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public ArrayList<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(ArrayList<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(ArrayList<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public MenuItem getDefaultMenuItem() {
		return defaultMenuItem;
	}

	public void setDefaultMenuItem(MenuItem defaultMenuItem) {
		this.defaultMenuItem = defaultMenuItem;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "userid "+getId();
	}
}
