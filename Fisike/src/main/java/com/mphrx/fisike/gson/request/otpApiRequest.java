package com.mphrx.fisike.gson.request;

public class otpApiRequest {

	String contact;
	String objectType;
	String deviceUID;
	int otp;
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getDeviceUID() {
		return deviceUID;
	}
	public void setDeviceUID(String deviceUID) {
		this.deviceUID = deviceUID;
	}
	public int getOtp() {
		return otp;
	}
	public void setOtp(int otp) {
		this.otp = otp;
	}
	
	
}
