package com.mphrx.fisike.gson.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.utils.Utils;

public class Height implements Parcelable {

	private String value;
	private String unit;

	public String getValue() {
		return Utils.isValueAvailable(value)?value:"0";
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Height() {
	}

	protected Height(Parcel in) {
		value = in.readString();
		unit = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(value);
		dest.writeString(unit);
	}
	
	public static final Parcelable.Creator<Height> CREATOR = new Parcelable.Creator<Height>() {
	    public Height createFromParcel(Parcel in) {
	        return new Height(in);
	    }

	    public Height[] newArray(int size) {
	        return new Height[size];
	    }
	};

}
