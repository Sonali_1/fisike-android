package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicineNameResponse {
	@Expose
	private Code code;
	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private Integer id;
	@Expose
	private IsBrand isBrand;
	@Expose
	private String kind;
	@Expose
	private Object manufacturer;
	@Expose
	private Object medPackage;
	@Expose
	private Object medProduct;
	@Expose
	private String name;
	@SerializedName("class")
	@Expose
	private String _class;

	/**
	 * 
	 * @return The code
	 */
	public Code getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 *  The code
	 */
	public void setCode(Code code) {
		this.code = code;
	}

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The isBrand
	 */
	public IsBrand getIsBrand() {
		return isBrand;
	}

	/**
	 * 
	 * @param isBrand
	 *  The isBrand
	 */
	public void setIsBrand(IsBrand isBrand) {
		this.isBrand = isBrand;
	}

	/**
	 * 
	 * @return The kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * 
	 * @param kind
	 *  The kind
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * 
	 * @return The manufacturer
	 */
	public Object getManufacturer() {
		return manufacturer;
	}

	/**
	 * 
	 * @param manufacturer
	 *  The manufacturer
	 */
	public void setManufacturer(Object manufacturer) {
		this.manufacturer = manufacturer;
	}

	/**
	 * 
	 * @return The medPackage
	 */
	public Object getMedPackage() {
		return medPackage;
	}

	/**
	 * 
	 * @param medPackage
	 *  The medPackage
	 */
	public void setMedPackage(Object medPackage) {
		this.medPackage = medPackage;
	}

	/**
	 * 
	 * @return The medProduct
	 */
	public Object getMedProduct() {
		return medProduct;
	}

	/**
	 * 
	 * @param medProduct
	 *  The medProduct
	 */
	public void setMedProduct(Object medProduct) {
		this.medProduct = medProduct;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *  The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

}
