package com.mphrx.fisike.gson.request;


import com.mphrx.fisike.mo.UserMO;

public class OTPApiGson {
    Boolean userExist;
    Boolean resetPassword;
    Boolean verified;
    Boolean isSent;
    String contact;
    String contactType;
    UserMO user;

    public Boolean getUserExist() {
        return userExist;
    }

    public void setUserExist(Boolean userExist) {
        this.userExist = userExist;
    }

    public Boolean getResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(Boolean resetPassword) {
        this.resetPassword = resetPassword;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Boolean getIsSent() {
        return isSent;
    }

    public void setIsSent(Boolean isSent) {
        this.isSent = isSent;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public UserMO getUser()
    {
        return user;
    }

    public void setUser(UserMO user)
    {
        this.user = user;
    }
}
