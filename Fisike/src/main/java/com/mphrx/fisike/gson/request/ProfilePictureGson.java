
package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfilePictureGson {

	@SerializedName("profilePicture")
	private ArrayList<ProfilePicture> profilePictureArray;

	@SerializedName("error")
	private String error;

	@SerializedName("status")
	private boolean status;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ArrayList<ProfilePicture> getProfilePictureArray() {
		return profilePictureArray;
	}

	public void setProfilePictureArray(ArrayList<ProfilePicture> profilePictureArray) {
		this.profilePictureArray = profilePictureArray;
	}

	public class ProfilePicture {

		@Expose
		private String username;

		@Expose
		private byte[] profilePic;

		private int id;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public byte[] getProfilePic() {
			return profilePic;
		}

		public void setProfilePic(byte[] profilePic) {
			this.profilePic = profilePic;
		}
	}
}
