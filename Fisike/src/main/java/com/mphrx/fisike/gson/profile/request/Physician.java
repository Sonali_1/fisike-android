
package com.mphrx.fisike.gson.profile.request;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Physician {

    @Expose
private List<PractitionerRole> practitionerRole = new ArrayList<PractitionerRole>();
    @Expose
    private List<Object> communication = new ArrayList<Object>();
    @Expose private List<Object> location = new ArrayList<Object>();
    @Expose private String _class;
    @Expose private Object actionTriggerService;
    @Expose private List<Object> photo = new ArrayList<Object>();
    @Expose private List<Extension> extension = new ArrayList<Extension>();
    @Expose private Integer id;
    @Expose private List<Object> qualification = new ArrayList<Object>();
    @Expose private ArrayList<com.mphrx.fisike.gson.request.Address> address = new ArrayList<com.mphrx.fisike.gson.request.Address>();
    @Expose private Name name;
    @Expose private LowerCaseName lowerCaseName;
    @Expose private List<Telecom> telecom = new ArrayList<Telecom>();
    @Expose private Object gender;
    @Expose private Object active;
    @Expose private Object birthDate;
    @Expose private List<Object> identifier = new ArrayList<Object>();

    public List<PractitionerRole> getPractitionerRole() {
        return practitionerRole;
    }

    public void setPractitionerRole(List<PractitionerRole> practitionerRole) {
        this.practitionerRole = practitionerRole;
    }

    public List<Object> getCommunication() {
        return communication;
    }

    public void setCommunication(List<Object> communication) {
        this.communication = communication;
    }

    public List<Object> getLocation() {
        return location;
    }

    public void setLocation(List<Object> location) {
        this.location = location;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Object getActionTriggerService() {
        return actionTriggerService;
    }

    public void setActionTriggerService(Object actionTriggerService) {
        this.actionTriggerService = actionTriggerService;
    }

    public List<Object> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Object> photo) {
        this.photo = photo;
    }

    public List<Extension> getExtension() {
        return extension;
    }

    public void setExtension(List<Extension> extension) {
        this.extension = extension;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getQualification() {
        return qualification;
    }

    public void setQualification(List<Object> qualification) {
        this.qualification = qualification;
    }

    public ArrayList<com.mphrx.fisike.gson.request.Address> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<com.mphrx.fisike.gson.request.Address> address) {
        this.address = address;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public LowerCaseName getLowerCaseName() {
        return lowerCaseName;
    }

    public void setLowerCaseName(LowerCaseName lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public List<Telecom> getTelecom() {
        return telecom;
    }

    public void setTelecom(List<Telecom> telecom) {
        this.telecom = telecom;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getActive() {
        return active;
    }

    public void setActive(Object active) {
        this.active = active;
    }

    public Object getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Object birthDate) {
        this.birthDate = birthDate;
    }

    public List<Object> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<Object> identifier) {
        this.identifier = identifier;
    }

}
