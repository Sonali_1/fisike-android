package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Coding {

	@Expose
	private String code;
	@Expose
	private String display;
	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private Object id;
	@Expose
	private Object primary;
	@Expose
	private String system;
	@Expose
	private Object version;

	/**
	 * 
	 * @return The code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 *  The code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return The display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * 
	 * @param display
	 *  The display
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The primary
	 */
	public Object getPrimary() {
		return primary;
	}

	/**
	 * 
	 * @param primary
	 *  The primary
	 */
	public void setPrimary(Object primary) {
		this.primary = primary;
	}

	/**
	 * 
	 * @return The system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * 
	 * @param system
	 *  The system
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * 
	 * @return The version
	 */
	public Object getVersion() {
		return version;
	}

	/**
	 * 
	 * @param version
	 *  The version
	 */
	public void setVersion(Object version) {
		this.version = version;
	}

}
