package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class EncounterSearchRequest {

	@Expose
	private Constraints constraints;

	/**
	* 
	* @return
	* The constraints
	*/
	public Constraints getConstraints() {
	return constraints;
	}

	/**
	* 
	* @param constraints
	* The constraints
	*/
	public void setConstraints(Constraints constraints) {
	this.constraints = constraints;
	}

	}

