package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DosageInstruction {

	@Expose
	private String text;
//	@SerializedName("timing")
//	private TimingPeriod timingPeriod;
	@Expose
	private Timing timing;
	@Expose
	private AdditionalInstructions additionalInstructions;
	@Expose
	private Boolean asNeeded;
	@Expose
	private DoseQuantity doseQuantity;

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *            The text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 
	 * @return The timingPeriod
	 *//*
	public TimingPeriod getTimingPeriod() {
		return timingPeriod;
	}

	*//**
	 * 
	 * @param timingPeriod
	 *            The timingPeriod
	 *//*
	public void setTimingPeriod(TimingPeriod timingPeriod) {
		this.timingPeriod = timingPeriod;
	}
*/

	public Timing getTiming() {
		return timing;
	}

	public void setTiming(Timing timing) {
		this.timing = timing;
	}

	/**
	 * 
	 * @return The additionalInstructions
	 */
	public AdditionalInstructions getAdditionalInstructions() {
		return additionalInstructions;
	}

	/**
	 * 
	 * @param additionalInstructions
	 *            The additionalInstructions
	 */
	public void setAdditionalInstructions(AdditionalInstructions additionalInstructions) {
		this.additionalInstructions = additionalInstructions;
	}

	/**
	 * 
	 * @return The asNeeded
	 */
	public Boolean getAsNeeded() {
		return asNeeded;
	}

	/**
	 * 
	 * @param asNeeded
	 *            The asNeeded
	 */
	public void setAsNeeded(Boolean asNeeded) {
		this.asNeeded = asNeeded;
	}

	public DoseQuantity getDoseQuantity() {
		return doseQuantity;
	}

	public void setDoseQuantity(DoseQuantity doseQuantity) {
		this.doseQuantity = doseQuantity;
	}

}