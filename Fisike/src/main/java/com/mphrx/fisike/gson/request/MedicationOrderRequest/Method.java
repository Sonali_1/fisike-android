package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Method {

    @SerializedName("fhir_comments")
    @Expose
    public List<String> fhirComments = new ArrayList<String>();

    public List<String> getFhirComments() {
        return fhirComments;
    }

    public void setFhirComments(List<String> fhirComments) {
        this.fhirComments = fhirComments;
    }
}
