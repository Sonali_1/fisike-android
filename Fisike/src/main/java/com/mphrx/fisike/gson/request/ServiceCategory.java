package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class ServiceCategory {
	@Expose
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
