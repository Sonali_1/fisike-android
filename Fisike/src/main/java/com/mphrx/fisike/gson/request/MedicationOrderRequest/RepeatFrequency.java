package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by Kailash Khurana on 3/17/2016.
 */
public class RepeatFrequency {

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
