package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member {

	@SerializedName("jid")
	@Expose
	private String jid;
	@SerializedName("availability")
	@Expose
	private Availability availability;
	@SerializedName("role")
	@Expose
	private String role;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("userType")
	@Expose
	private String userType;

	/**
	 * 
	 * @return The jid
	 */
	public String getJid() {
		return jid;
	}

	/**
	 * 
	 * @param jid
	 *            The jid
	 */
	public void setJid(String jid) {
		this.jid = jid;
	}

	/**
	 * 
	 * @return The availability
	 */
	public Availability getAvailability() {
		return availability;
	}

	/**
	 * 
	 * @param availability
	 *            The availability
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	/**
	 * 
	 * @return The role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * 
	 * @param role
	 *            The role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * 
	 * @return The isActive
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * 
	 * @param isActive
	 *            The isActive
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * 
	 * @return The userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * 
	 * @param userType
	 *            The userType
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

}
