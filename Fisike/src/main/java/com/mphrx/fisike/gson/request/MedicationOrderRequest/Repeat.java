package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xmb2nc on 16-02-2016.
 */
public class Repeat {
    private RepeatFrequency frequency;
    private RepeatPeriod period;
    private String periodUnits;
    private Bounds bounds;

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public String getPeriodUnits() {
        return periodUnits;
    }

    public void setPeriodUnits(String periodUnits) {
        this.periodUnits = periodUnits;
    }

    public RepeatFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(RepeatFrequency frequency) {
        this.frequency = frequency;
    }

    public RepeatPeriod getPeriod() {
        return period;
    }

    public void setPeriod(RepeatPeriod period) {
        this.period = period;
    }
}
