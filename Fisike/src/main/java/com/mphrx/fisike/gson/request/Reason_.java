package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Reason_ {

	@Expose
	private Object coding;
	@Expose
	private Object extension;
	@Expose
	private Object id;
	@Expose
	private String text;

	/**
	 * 
	 * @return The coding
	 */
	public Object getCoding() {
		return coding;
	}

	/**
	 * 
	 * @param coding
	 *  The coding
	 */
	public void setCoding(Object coding) {
		this.coding = coding;
	}

	/**
	 * 
	 * @return The extension
	 */
	public Object getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(Object extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *  The text
	 */
	public void setText(String text) {
		this.text = text;
	}

}