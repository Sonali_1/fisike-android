package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class pat {
	String active;
	ArrayList<Address> address;
	String animal;
	BirthDate birthDate;
	ArrayList<String> careProvider;
	ArrayList<String> communication;
	ArrayList<Contact> contact;
	Deceased deceased;
	ArrayList<Extension> extension;
	CodingExtensionTextFormat gender;
	long id;
	LinkingList linkList;
	ManagingOrganization managingOrganization;
	CodingExtensionTextFormat maritalStatus;
	MultipleBirth multipleBirth;
	ArrayList<Contact_name> name;
	@SerializedName("class")
	String patclass;
	ArrayList<patIdentifier> identifier;
	ArrayList<telecom> telecom;

	public ArrayList<patIdentifier> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(ArrayList<patIdentifier> identifier) {
		this.identifier = identifier;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public ArrayList<Address> getAddress() {
		return address;
	}

	public void setAddress(ArrayList<Address> address) {
		this.address = address;
	}

	public String getAnimal() {
		return animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public BirthDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(BirthDate birthDate) {
		this.birthDate = birthDate;
	}

	public ArrayList<String> getCareProvider() {
		return careProvider;
	}

	public void setCareProvider(ArrayList<String> careProvider) {
		this.careProvider = careProvider;
	}

	public ArrayList<String> getCommunication() {
		return communication;
	}

	public void setCommunication(ArrayList<String> communication) {
		this.communication = communication;
	}

	public ArrayList<Contact> getContact() {
		return contact;
	}

	public void setContact(ArrayList<Contact> contact) {
		this.contact = contact;
	}

	public Deceased getDeceased() {
		return deceased;
	}

	public void setDeceased(Deceased deceased) {
		this.deceased = deceased;
	}

	/*public ArrayList<Extension> getExtension() {
		return extension;
	}

	public void setExtension(ArrayList<Extension> extension) {
		this.extension = extension;
	}

	public CodingExtensionTextFormat getGender() {
		return gender;
	}

	public void setGender(CodingExtensionTextFormat gender) {
		this.gender = gender;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LinkingList getLinkList() {
		return linkList;
	}

	public void setLinkList(LinkingList linkList) {
		this.linkList = linkList;
	}

	public ManagingOrganization getManagingOrganization() {
		return managingOrganization;
	}

	public void setManagingOrganization(ManagingOrganization managingOrganization) {
		this.managingOrganization = managingOrganization;
	}

	public CodingExtensionTextFormat getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(CodingExtensionTextFormat maritalStatus) {
		this.maritalStatus = maritalStatus;
	}*/

	public MultipleBirth getMultipleBirth() {
		return multipleBirth;
	}

	public void setMultipleBirth(MultipleBirth multipleBirth) {
		this.multipleBirth = multipleBirth;
	}

	public ArrayList<Contact_name> getName() {
		return name;
	}

	public void setName(ArrayList<Contact_name> name) {
		this.name = name;
	}

	/*public String getPatclass() {
		return patclass;
	}

	public void setPatclass(String patclass) {
		this.patclass = patclass;
	}

	public ArrayList<telecom> getTelecom() {
		return telecom;
	}

	public void setTelecom(ArrayList<telecom> telecom) {
		this.telecom = telecom;
	}*/
}
