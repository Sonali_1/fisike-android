package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class Contact_name implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6874488075963252871L;
	ArrayList<String> family;
	ArrayList<String> given;
	String period;
	ArrayList<String> prefix;
	ArrayList<String> suffix;
	String text;
	private String useCode;
	public ArrayList<String> getFamily() {
		return family;
	}
	public void setFamily(ArrayList<String> family) {
		this.family = family;
	}
	public ArrayList<String> getGiven() {
		return given;
	}
	public void setGiven(ArrayList<String> given) {
		this.given = given;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public ArrayList<String> getPrefix() {
		return prefix;
	}
	public void setPrefix(ArrayList<String> prefix) {
		this.prefix = prefix;
	}
	public ArrayList<String> getSuffix() {
		return suffix;
	}
	public void setSuffix(ArrayList<String> suffix) {
		this.suffix = suffix;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUseCode() {
		return useCode;
	}

	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}
}
