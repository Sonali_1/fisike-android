package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xmb2nc on 23-11-2015.
 */
public class MedicationPrescriptionSaveListRequest {
    @SerializedName("medicationPrescriptionList")
    @Expose
    private List<MedicationPrescriptionSaveGson> medicationPrescriptionList = new ArrayList<MedicationPrescriptionSaveGson>();
    @SerializedName("sendPushNotification")

    @Expose
    private Boolean sendPushNotification;

    /**
     * @return The medicationPrescriptionList
     */
    public List<MedicationPrescriptionSaveGson> getMedicationPrescriptionList() {
        return medicationPrescriptionList;
    }

    /**
     * @param medicationPrescriptionList The medicationPrescriptionList
     */
    public void setMedicationPrescriptionList(List<MedicationPrescriptionSaveGson> medicationPrescriptionList) {
        this.medicationPrescriptionList = medicationPrescriptionList;
    }

    /**
     * @return The sendPushNotification
     */
    public Boolean getSendPushNotification() {
        return sendPushNotification;
    }

    /**
     * @param sendPushNotification The sendPushNotification
     */
    public void setSendPushNotification(Boolean sendPushNotification) {
        this.sendPushNotification = sendPushNotification;
    }

}
