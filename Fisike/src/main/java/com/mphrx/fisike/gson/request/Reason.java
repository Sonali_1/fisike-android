package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Reason {

@Expose
private List<Coding> coding = new ArrayList<Coding>();
@Expose
private String text;

/**
* 
* @return
* The coding
*/
public List<Coding> getCoding() {
return coding;
}

/**
* 
* @param coding
* The coding
*/
public void setCoding(List<Coding> coding) {
this.coding = coding;
}

/**
* 
* @return
* The text
*/
public String getText() {
return text;
}

/**
* 
* @param text
* The text
*/
public void setText(String text) {
this.text = text;
}

}