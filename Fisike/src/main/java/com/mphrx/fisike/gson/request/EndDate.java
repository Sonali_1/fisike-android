package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class EndDate {

@Expose
private String value;

/**
* 
* @return
* The value
*/
public String getValue() {
return value;
}

/**
* 
* @param value
* The value
*/
public void setValue(String value) {
this.value = value;
}

}