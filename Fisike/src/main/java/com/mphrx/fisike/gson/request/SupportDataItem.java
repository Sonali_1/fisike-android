package com.mphrx.fisike.gson.request;

public class SupportDataItem {

    private String ResIcon;
    private String text;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResIcon() {
        return ResIcon;
    }

    public void setResIcon(String resIcon) {
        ResIcon = resIcon;
    }
}
