package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetupChatResponse {

	@SerializedName("list")
	@Expose
	private java.util.List<com.mphrx.fisike.gson.response.RosterMemberList> list = new ArrayList<com.mphrx.fisike.gson.response.RosterMemberList>();

	/**
	 * 
	 * @return The list
	 */
	public java.util.List<com.mphrx.fisike.gson.response.RosterMemberList> getList() {
		return list;
	}

	/**
	 * 
	 * @param list
	 *            The list
	 */
	public void setList(java.util.List<com.mphrx.fisike.gson.response.RosterMemberList> list) {
		this.list = list;
	}

}
