package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class DeregisterApiGson {
	
	@Expose
	private String msg;
	@Expose
	private String status;
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
