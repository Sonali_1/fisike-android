package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Participant {

	@Expose
	private Individual individual;
	@Expose
	private Object type;

	/**
	 * 
	 * @return The individual
	 */
	public Individual getIndividual() {
		return individual;
	}

	/**
	 * 
	 * @param individual
	 *  The individual
	 */
	public void setIndividual(Individual individual) {
		this.individual = individual;
	}

	/**
	 * 
	 * @return The type
	 */
	public Object getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *  The type
	 */
	public void setType(Object type) {
		this.type = type;
	}

}