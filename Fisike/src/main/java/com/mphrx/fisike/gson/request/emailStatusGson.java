package com.mphrx.fisike.gson.request;

public class emailStatusGson {

	String verificationCode;
	Boolean emailVerified;
	String msg;
	String status;
	Boolean linkExpired;
	
	public String getVerificationCode() {
		return verificationCode;
	}
	public Boolean getLinkExpired() {
		return linkExpired;
	}
	public void setLinkExpired(Boolean linkExpired) {
		this.linkExpired = linkExpired;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	
}
