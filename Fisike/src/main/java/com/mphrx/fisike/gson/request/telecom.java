package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class telecom implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6436782483912923007L;
	ArrayList<PatientExtension> extension;
	String id;
	String period;
	String system;
	String useCode;
	String value;
	public ArrayList<PatientExtension> getExtension() {
		return extension;
	}
	public void setExtension(ArrayList<PatientExtension> extension) {
		this.extension = extension;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getUseCode() {
		return useCode;
	}
	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
