package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSettings {
    @Expose
    @SerializedName("userLanguage")
    private String userLanguage;


    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }
}
