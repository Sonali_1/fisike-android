package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.SerializedName;

public class RegisterDeviceResponse {
	
	@SerializedName("msg")
	String Message;
	
	String status;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
