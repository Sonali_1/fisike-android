package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class Timing {

    @Expose
    private ArrayList<TimingEvent> event;


    public ArrayList<TimingEvent> getEvent() {
        return event;
    }

    public void setEvent(ArrayList<TimingEvent> event) {
        this.event = event;
    }
}
