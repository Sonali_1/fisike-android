package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class NumberOfRepeatsAllowed {

	@Expose
	private Object extension;
	@Expose
	private Object id;
	@Expose
	private Integer value;

	/**
	 * 
	 * @return The extension
	 */
	public Object getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(Object extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 *  The value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

}
