package com.mphrx.fisike.gson.request;

/**
 * Created by xmb2nc on 18-11-2015.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadDeviceTokenRequest {

    @SerializedName("deviceUID")
    @Expose
    private String deviceUID;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("osType")
    @Expose
    private String osType;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;

    /**
     *
     * @return
     * The deviceUID
     */
    public String getDeviceUID() {
        return deviceUID;
    }

    /**
     *
     * @param deviceUID
     * The deviceUID
     */
    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     * The osType
     */
    public String getOsType() {
        return osType;
    }

    /**
     *
     * @param osType
     * The osType
     */
    public void setOsType(String osType) {
        this.osType = osType;
    }

    /**
     *
     * @return
     * The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     *
     * @param deviceType
     * The deviceType
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     *
     * @return
     * The appVersion
     */
    public String getAppVersion() {
        return appVersion;
    }

    /**
     *
     * @param appVersion
     * The appVersion
     */
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

}
