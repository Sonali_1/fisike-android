
package com.mphrx.fisike.gson.profile.request;

import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

public class Value {

    @Expose
    private String noOfExp;

    public String getNoOfExp() {
        return noOfExp;
    }

    public void setNoOfExp(String noOfExp) {
        this.noOfExp = noOfExp;
    }

}
