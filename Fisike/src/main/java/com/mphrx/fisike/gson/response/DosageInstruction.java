package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.Timing;
import com.mphrx.fisike.gson.request.TimingPeriod;

public class DosageInstruction {

	@Expose
	private AdditionalInstructions additionalInstructions;
	@Expose
	private AsNeeded asNeeded;
	@Expose
	private DoseQuantity doseQuantity;
	@Expose
	private Object maxDosePerPeriod;
	@Expose
	private Object method;
	@Expose
	private Object rate;
	@Expose
	private Object route;
	@Expose
	private Object site;
	@Expose
	private String text;
	@Expose
	private Timing timing;
	@Expose
	private TimingPeriod timingPeriod;

	/**
	 * 
	 * @return The additionalInstructions
	 */
	public AdditionalInstructions getAdditionalInstructions() {
		return additionalInstructions;
	}

	/**
	 * 
	 * @param additionalInstructions
	 *  The additionalInstructions
	 */
	public void setAdditionalInstructions(AdditionalInstructions additionalInstructions) {
		this.additionalInstructions = additionalInstructions;
	}

	/**
	 * 
	 * @return The asNeeded
	 */
	public AsNeeded getAsNeeded() {
		return asNeeded;
	}

	/**
	 * 
	 * @param asNeeded
	 *  The asNeeded
	 */
	public void setAsNeeded(AsNeeded asNeeded) {
		this.asNeeded = asNeeded;
	}

	/**
	 * 
	 * @return The doseQuantity
	 */
	public Object getDoseQuantity() {
		return doseQuantity;
	}

	/**
	 * 
	 * @param doseQuantity
	 *  The doseQuantity
	 */
	public void setDoseQuantity(Object doseQuantity) {
		this.doseQuantity = (DoseQuantity) doseQuantity;
	}

	/**
	 * 
	 * @return The maxDosePerPeriod
	 */
	public Object getMaxDosePerPeriod() {
		return maxDosePerPeriod;
	}

	/**
	 * 
	 * @param maxDosePerPeriod
	 *  The maxDosePerPeriod
	 */
	public void setMaxDosePerPeriod(Object maxDosePerPeriod) {
		this.maxDosePerPeriod = maxDosePerPeriod;
	}

	/**
	 * 
	 * @return The method
	 */
	public Object getMethod() {
		return method;
	}

	/**
	 * 
	 * @param method
	 *  The method
	 */
	public void setMethod(Object method) {
		this.method = method;
	}

	/**
	 * 
	 * @return The rate
	 */
	public Object getRate() {
		return rate;
	}

	/**
	 * 
	 * @param rate
	 *  The rate
	 */
	public void setRate(Object rate) {
		this.rate = rate;
	}

	/**
	 * 
	 * @return The route
	 */
	public Object getRoute() {
		return route;
	}

	/**
	 * 
	 * @param route
	 *  The route
	 */
	public void setRoute(Object route) {
		this.route = route;
	}

	/**
	 * 
	 * @return The site
	 */
	public Object getSite() {
		return site;
	}

	/**
	 * 
	 * @param site
	 *  The site
	 */
	public void setSite(Object site) {
		this.site = site;
	}

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *  The text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 
	 * @return The timing
	 */
	public Timing getTiming() {
		return timing;
	}

	/**
	 * 
	 * @param timing
	 *  The timing
	 */
	public void setTiming(Timing timing) {
		this.timing = timing;
	}

	/**
	 * 
	 * @return The timingPeriod
	 */
	public TimingPeriod getTimingPeriod() {
		return timingPeriod;
	}

	/**
	 * 
	 * @param timingPeriod
	 *  The timingPeriod
	 */
	public void setTimingPeriod(TimingPeriod timingPeriod) {
		this.timingPeriod = timingPeriod;
	}

}
