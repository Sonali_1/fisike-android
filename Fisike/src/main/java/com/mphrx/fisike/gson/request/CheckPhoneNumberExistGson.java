package com.mphrx.fisike.gson.request;

public class CheckPhoneNumberExistGson {
	
	
	String phoneNumber;
	String userType;

	public void setUserType(String userType) {
		this.userType = userType;
	}

	String msg;
	String status;
	String httpStatusCode;
	
	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
	

}
