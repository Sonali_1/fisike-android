package com.mphrx.fisike.gson.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public class GsonUtils {
	
	public static Object jsonToObjectMapper(String jsonString, Class<?> object) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);
		Gson gson = gsonBuilder.create();
		return gson.fromJson(jsonString, object);
	}
}
