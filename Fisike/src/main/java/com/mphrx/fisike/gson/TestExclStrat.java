package com.mphrx.fisike.gson;

import android.support.annotation.Nullable;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Reason;

/**
 * Created by Kailash Khurana on 3/2/2017.
 */

public class TestExclStrat implements ExclusionStrategy {

    private final Class<?> typeToSkip;

    public TestExclStrat(Class<?> typeToSkip) {
        this.typeToSkip = typeToSkip;
    }

    public boolean shouldSkipClass(Class<?> clazz) {
        return (clazz == typeToSkip);
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(Nullable.class) != null;
    }
}
