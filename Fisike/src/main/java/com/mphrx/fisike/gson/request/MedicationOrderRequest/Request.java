package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request {

//    @SerializedName("fhir_comments")
//    @Expose
//    public List<String> fhirComments = new ArrayList<String>();
    @SerializedName("method")
    @Expose
    public String method;
    @SerializedName("_method")
    @Expose
    public com.mphrx.fisike.gson.request.MedicationOrderRequest.Method Method;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("_url")
    @Expose
    public com.mphrx.fisike.gson.request.MedicationOrderRequest.Url Url;


//    public List<String> getFhirComments() {
//        return fhirComments;
//    }
//
//    public void setFhirComments(List<String> fhirComments) {
//        this.fhirComments = fhirComments;
//    }

    public String getMethod() {
        return method;
    }

    public void setMethod(com.mphrx.fisike.gson.request.MedicationOrderRequest.Method method) {
        Method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(com.mphrx.fisike.gson.request.MedicationOrderRequest.Url url) {
        Url = url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
