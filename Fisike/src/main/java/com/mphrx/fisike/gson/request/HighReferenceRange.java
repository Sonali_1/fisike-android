package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class HighReferenceRange {

	@Expose
	private String units;
	@Expose
	private String value;

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
