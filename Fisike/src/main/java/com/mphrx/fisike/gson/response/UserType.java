package com.mphrx.fisike.gson.response;

import android.os.Parcel;
import android.os.Parcelable;

public class UserType implements Parcelable{

	private long id;
	private String name;
	private String type;

	public UserType() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		if (name == null) {
			return "";
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	protected UserType(Parcel in){
		id = in.readLong();
		name = in.readString();
		type = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(type);
	}
	
	public static final Parcelable.Creator<UserType> CREATOR = new Parcelable.Creator<UserType>() {
	    public UserType createFromParcel(Parcel in) {
	        return new UserType(in);
	    }

	    public UserType[] newArray(int size) {
	        return new UserType[size];
	    }
	};

}
