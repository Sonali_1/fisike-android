package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Quantity {

@Expose
private String units;
@Expose
private String value;

/**
* 
* @return
* The units
*/
public String getUnits() {
return units;
}

/**
* 
* @param units
* The units
*/
public void setUnits(String units) {
this.units = units;
}

/**
* 
* @return
* The value
*/
public String getValue() {
return value;
}

/**
* 
* @param value
* The value
*/
public void setValue(String value) {
this.value = value;
}

}