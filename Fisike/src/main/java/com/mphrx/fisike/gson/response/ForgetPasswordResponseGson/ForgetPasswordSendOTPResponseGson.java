package com.mphrx.fisike.gson.response.ForgetPasswordResponseGson;

/**
 * Created by Neha on 18-02-2016.
 */
public class ForgetPasswordSendOTPResponseGson {

    String msg;
    String status;
    String alternateContact;
    boolean messageSent;

    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public boolean isMessageSent() {
        return messageSent;
    }
}
