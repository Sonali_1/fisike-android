package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class IsBrand {

@Expose
private Boolean value;

/**
* 
* @return
* The value
*/
public Boolean getValue() {
return value;
}

/**
* 
* @param value
* The value
*/
public void setValue(Boolean value) {
this.value = value;
}

}