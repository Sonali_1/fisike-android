package com.mphrx.fisike.gson.response;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class MedicationListingResponse {

	@Expose
	private java.util.List<com.mphrx.fisike.gson.response.List> list = new ArrayList<com.mphrx.fisike.gson.response.List>();
	@Expose
	private Integer totalCount;

	/**
	 * 
	 * @return The list
	 */
	public java.util.List<com.mphrx.fisike.gson.response.List> getList() {
		return list;
	}

	/**
	 * 
	 * @param list
	 *  The list
	 */
	public void setList(java.util.List<com.mphrx.fisike.gson.response.List> list) {
		this.list = list;
	}

	/**
	 * 
	 * @return The totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * 
	 * @param totalCount
	 *  The totalCount
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}