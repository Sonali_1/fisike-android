package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coding {

    @SerializedName("display")
    @Expose
    public String display;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("system")
    @Expose
    public String system;

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }
}
