package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Encounter {
	@Expose
	private Integer id;
	@SerializedName("class")
	@Expose
	private String _class;

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *            The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

}