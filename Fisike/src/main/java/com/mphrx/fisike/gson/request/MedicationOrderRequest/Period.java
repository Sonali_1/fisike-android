package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;

/**
 * Created by xmb2nc on 16-02-2016.
 */
public class Period {
    private StartDate startDate;

    public StartDate getStartDate() {
        return startDate;
    }

    public void setStartDate(StartDate startDate) {
        this.startDate = startDate;
    }
}
