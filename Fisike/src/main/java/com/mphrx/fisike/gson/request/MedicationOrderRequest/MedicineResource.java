package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 2/25/2016.
 */
public class MedicineResource {
    @Expose
    private ArrayList<ExtensionMedicine> extension;
    @Expose
    @SerializedName("class")
    private String _class;
    @Expose
    private String resourceType;
    @Expose
    private int id;
    @Expose
    private Code code;


    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<ExtensionMedicine> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<ExtensionMedicine> extension) {
        this.extension = extension;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }
}
