package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class AuthenticationGson {

    @Expose
    private String username;
    @Expose
    private List<String> roles = new ArrayList<String>();
    @Expose
    private String token;
    @Expose
    private Boolean firstLogin;
    @Expose
    private Boolean passwordExpired;

    /**
     * 
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *            The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return The roles
     */
    public List<String> getRoles() {
        return roles;
    }

    /**
     * 
     * @param roles
     *            The roles
     */
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    /**
     * 
     * @return The token
     */
    public String getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *            The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return The firstLogin
     */
    public Boolean getFirstLogin() {
        return firstLogin;
    }

    /**
     * 
     * @param firstLogin
     *            The firstLogin
     */
    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    /**
     * 
     * @return The passwordExpired
     */
    public Boolean getPasswordExpired() {
        return passwordExpired;
    }

    /**
     * 
     * @param passwordExpired
     *            The passwordExpired
     */
    public void setPasswordExpired(Boolean passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

}