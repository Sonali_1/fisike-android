
package com.mphrx.fisike.gson.profile.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.Expose;

public class Address {

    @Expose
    private List<Object> extension = new ArrayList<Object>();
    @Expose
    private Object id;
    @Expose
    private String text;
    @Expose
    private Object postalCode;
    @Expose
    private Object state;
    @Expose
    private List<Object> line = new ArrayList<Object>();
    @Expose
    private Object useCode;
    @Expose
    private Object district;
    @Expose
    private Object period;
    @Expose
    private Object type;
    @Expose
    private Object city;
    @Expose
    private Object country;

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Object postalCode) {
        this.postalCode = postalCode;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public List<Object> getLine() {
        return line;
    }

    public void setLine(List<Object> line) {
        this.line = line;
    }

    public Object getUseCode() {
        return useCode;
    }

    public void setUseCode(Object useCode) {
        this.useCode = useCode;
    }

    public Object getDistrict() {
        return district;
    }

    public void setDistrict(Object district) {
        this.district = district;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }
}
