package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class Type implements Serializable {

@Expose
private String text;

/**
* 
* @return
* The text
*/
public String getText() {
return text;
}

/**
* 
* @param text
* The text
*/
public void setText(String text) {
this.text = text;
}

}