package com.mphrx.fisike.gson.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.utils.Utils;

public class Weight implements Parcelable {

	private String value;
	private String unit;
	
	
	public Weight() {
		super();
	}

	public String getValue() {
		return Utils.isValueAvailable(value)?value:"0";
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	protected Weight(Parcel in) {
		value = in.readString();
		unit = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(value);
		dest.writeString(unit);
	}
	
	public static final Parcelable.Creator<Weight> CREATOR = new Parcelable.Creator<Weight>() {
	    public Weight createFromParcel(Parcel in) {
	        return new Weight(in);
	    }

	    public Weight[] newArray(int size) {
	        return new Weight[size];
	    }
	};


}
