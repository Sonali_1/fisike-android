package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetUpChatRequest {

	@SerializedName("jid")
	@Expose
	private String jid;

	/**
	 * 
	 * @return The jid
	 */
	public String getJid() {
		return jid;
	}

	/**
	 * 
	 * @param jid
	 *            The jid
	 */
	public void setJid(String jid) {
		this.jid = jid;
	}

}
