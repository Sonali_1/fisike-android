package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resource {

    @Expose
    public String resourceType;
    @Expose
    public Text text;
    @Expose
    public List<DosageInstruction> dosageInstruction = new ArrayList<DosageInstruction>();
    @Expose
    public Object encounter;
    @Expose
    public Object medication;
    @Expose
    public Patient patient;
    @Expose
    public Object prescriber;
    @Nullable
    public Reason reason;
    @Expose
    public String dateWritten;
    @Expose
    public String dateEnded;
    @Expose
    public List<Extension> extension = new ArrayList<Extension>();

    @Expose
    private ClassCode classCode;
    @Expose
    private List<Identifier> identifier;
    @Expose
    private Status status;
    @Expose
    private Period period;
    @SerializedName("class")
    public String _class;
    @Expose
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public List<Identifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public ClassCode getClassCode() {
        return classCode;
    }

    public void setClassCode(ClassCode classCode) {
        this.classCode = classCode;
    }


    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public List<DosageInstruction> getDosageInstruction() {
        return dosageInstruction;
    }

    public void setDosageInstruction(List<DosageInstruction> dosageInstruction) {
        this.dosageInstruction = dosageInstruction;
    }

    public Object getMedication() {
        return medication;
    }

    public void setMedication(Object medication) {
        this.medication = medication;
    }

    public Object getEncounter() {
        return encounter;
    }

    public void setEncounter(Object encounter) {
        this.encounter = encounter;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDateWritten() {
        return dateWritten;
    }

    public void setDateWritten(String dateWritten) {
        this.dateWritten = dateWritten;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public Object getPrescriber() {
        return prescriber;
    }

    public void setPrescriber(Object prescriber) {
        this.prescriber = prescriber;
    }

    public String getDateEnded() {
        return dateEnded;
    }

    public void setDateEnded(String dateEnded) {
        this.dateEnded = dateEnded;
    }

    public List<Extension> getExtension() {
        return extension;
    }

    public void setExtension(List<Extension> extension) {
        this.extension = extension;
    }

}