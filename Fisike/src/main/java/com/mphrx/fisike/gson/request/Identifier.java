package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Identifier {

    @Expose
    private Object extension;
    @Expose
    private Object id;
    @Expose
    private Label label;
    @Expose
    private Object period;
    @Expose
    private Object system;
    @Expose
    private UseCode useCode;
    @Expose
    private Value value;

    /**
     * 
     * @return The extension
     */
    public Object getExtension() {
        return extension;
    }

    /**
     * 
     * @param extension
     *            The extension
     */
    public void setExtension(Object extension) {
        this.extension = extension;
    }

    /**
     * 
     * @return The id
     */
    public Object getId() {
        return id;
    }

    /**
     * 
     * @param id
     *            The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * 
     * @return The period
     */
    public Object getPeriod() {
        return period;
    }

    /**
     * 
     * @param period
     *            The period
     */
    public void setPeriod(Object period) {
        this.period = period;
    }

    /**
     * 
     * @return The system
     */
    public Object getSystem() {
        return system;
    }

    /**
     * 
     * @param system
     *            The system
     */
    public void setSystem(Object system) {
        this.system = system;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public UseCode getUseCode() {
        return useCode;
    }

    public void setUseCode(UseCode useCode) {
        this.useCode = useCode;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
