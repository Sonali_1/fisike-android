package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;

public class DoseQuantity {
	@Expose
	private String units;
	
	@Expose
	private Double value;

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
