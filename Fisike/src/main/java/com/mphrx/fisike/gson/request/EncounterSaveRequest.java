package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class EncounterSaveRequest {

	@Expose
	private Boolean onlyReturnId;
	@Expose
	private ClassCode classCode;
	@Expose
	private List<Identifier> identifier = new ArrayList<Identifier>();
	@Expose
	private Status status;
	@Expose
	private Patient patient;
	@Expose
	private Period period;
	@Expose
	private List<Participant> participant = new ArrayList<Participant>(); 
	@Expose
	private int id;

	/**
	 * 
	 * @return The onlyReturnId
	 */
	public Boolean getOnlyReturnId() {
		return onlyReturnId;
	}

	/**
	 * 
	 * @param onlyReturnId
	 *  The onlyReturnId
	 */
	public void setOnlyReturnId(Boolean onlyReturnId) {
		this.onlyReturnId = onlyReturnId;
	}

	/**
	 * 
	 * @return The classCode
	 */
	public ClassCode getClassCode() {
		return classCode;
	}

	/**
	 * 
	 * @param classCode
	 *  The classCode
	 */
	public void setClassCode(ClassCode classCode) {
		this.classCode = classCode;
	}

	/**
	 * 
	 * @return The identifier
	 */
	public List<Identifier> getIdentifier() {
		return identifier;
	}

	/**
	 * 
	 * @param identifier
	 *  The identifier
	 */
	public void setIdentifier(List<Identifier> identifier) {
		this.identifier = identifier;
	}

	/**
	 * 
	 * @return The status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *  The status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * 
	 * @return The period
	 */
	public Period getPeriod() {
		return period;
	}

	/**
	 * 
	 * @param period
	 *  The period
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}

	/**
	 * 
	 * @return The participant
	 */
	public List<Participant> getParticipant() {
		return participant;
	}

	/**
	 * 
	 * @param participant
	 *  The participant
	 */
	public void setParticipant(List<Participant> participant) {
		this.participant = participant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
