package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Khurana on 2/25/2016.
 */
public class ExtensionMedicine {
    @Expose
    public String url;
    @Expose
    public ArrayList<Verified> value;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Verified> getValue() {
        return value;
    }

    public void setValue(ArrayList<Verified> value) {
        this.value = value;
    }
}
