package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Coding {

	@Expose
	private String system;
	@Expose
	private String code;
	@Expose
	private String display;

	/**
	 * 
	 * @return The system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * 
	 * @param system
	 *  The system
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * 
	 * @return The code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 *  The code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return The display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * 
	 * @param display
	 *  The display
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

}
