package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class BirthDate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4851533663032506258L;
	
	ArrayList<PatientExtension> extension;
	String id;
	String value;
	public ArrayList<PatientExtension> getExtension() {
		return extension;
	}
	public void setExtension(ArrayList<PatientExtension> extension) {
		this.extension = extension;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
