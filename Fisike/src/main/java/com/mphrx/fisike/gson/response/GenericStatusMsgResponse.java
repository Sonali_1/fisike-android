package com.mphrx.fisike.gson.response;

/**
 * Created by administrate on 12/8/2015.
 */
public class GenericStatusMsgResponse {

    String msg;
    int status;
    int httpStatusCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
