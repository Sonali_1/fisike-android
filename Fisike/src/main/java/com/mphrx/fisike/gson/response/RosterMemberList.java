package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RosterMemberList {
	@SerializedName("_id")
	@Expose
	private String Id;
	@SerializedName("owner")
	@Expose
	private String owner;
	@SerializedName("subject")
	@Expose
	private String subject;
	@SerializedName("isDeleted")
	@Expose
	private Boolean isDeleted;
	@SerializedName("dateCreated")
	@Expose
	private String dateCreated;
	@SerializedName("members")
	@Expose
	private java.util.List<Member> members = new ArrayList<Member>();

	/**
	 * 
	 * @return The Id
	 */
	public String getId() {
		return Id;
	}

	/**
	 * 
	 * @param Id
	 *            The _id
	 */
	public void setId(String Id) {
		this.Id = Id;
	}

	/**
	 * 
	 * @return The owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * 
	 * @param owner
	 *            The owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * 
	 * @return The subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 *            The subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return The isDeleted
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * 
	 * @param isDeleted
	 *            The isDeleted
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * 
	 * @return The dateCreated
	 */
	public String getDateCreated() {
		return dateCreated;
	}

	/**
	 * 
	 * @param dateCreated
	 *            The dateCreated
	 */
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * 
	 * @return The members
	 */
	public java.util.List<Member> getMembers() {
		return members;
	}

	/**
	 * 
	 * @param members
	 *            The members
	 */
	public void setMembers(java.util.List<Member> members) {
		this.members = members;
	}

}
