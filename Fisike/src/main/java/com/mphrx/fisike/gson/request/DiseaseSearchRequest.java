package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

/**
 * Created by Aastha on 31/12/2015.
 */
public class DiseaseSearchRequest {

    @Expose
    private Constraints constraints;

    /**
     *
     * @return The constraints
     */
    public Constraints getConstraints() {
        return constraints;
    }

    /**
     *
     * @param constraints
     *  The constraints
     */
    public void setConstraints(Constraints constraints) {
        this.constraints = constraints;
    }

}
