
package com.mphrx.fisike.gson.profile.request;

import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.User;

public class UserProfile {
    @Expose
    private String userType;
    @Expose
    private User user;
    @Expose
    private Physician physician;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Physician getPhysician() {
        return physician;
    }

    public void setPhysician(Physician physician) {
        this.physician = physician;
    }

}
