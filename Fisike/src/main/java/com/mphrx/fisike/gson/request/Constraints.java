package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Constraints {

	@Expose
	private Integer subject;
	@Expose
	private Integer encounter;
	@Expose
	private String name;

	@SerializedName("_skip")
	@Expose
	private Integer Skip;

	@SerializedName("_count")
	@Expose
	private Integer Count;
	@Expose
	private Integer patient;
	@Expose
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQuery() {
        return Query;
    }

    public void setQuery(String query) {
        Query = query;
    }

    @SerializedName("_query")
    @Expose
    private String Query;


    /**
	 * 
	 * @return The subject
	 */
	public Integer getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 *  The subject
	 */
	public void setSubject(Integer subject) {
		this.subject = subject;
	}
	
	/**
	 * 
	 * @return The subject
	 */
	public Integer getEncounter() {
		return encounter;
	}

	/**
	 * 
	 * @param subject
	 *  The subject
	 */
	public void setEncounter(Integer encounter) {
		this.encounter = encounter;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *  The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The Skip
	 */
	public Integer getSkip() {
		return Skip;
	}

	/**
	 * 
	 * @param Skip
	 *  The _skip
	 */
	public void setSkip(Integer Skip) {
		this.Skip = Skip;
	}

	/**
	 * 
	 * @return The Count
	 */
	public Integer getCount() {
		return Count;
	}

	/**
	 * 
	 * @param Count
	 *  The _count
	 */
	public void setCount(Integer Count) {
		this.Count = Count;
	}

	/**
	 * 
	 * @return The patient
	 */
	public Integer getPatient() {
		return patient;
	}

	/**
	 * 
	 * @param patient
	 *  The patient
	 */
	public void setPatient(Integer patient) {
		this.patient = patient;
	}

}