package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.SerializedName;

public class UploadProfilePicGson {
	
	@SerializedName("error")
	private String error;

	@SerializedName("status")
	private boolean status;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}