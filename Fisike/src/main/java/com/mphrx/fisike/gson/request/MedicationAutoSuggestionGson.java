package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class MedicationAutoSuggestionGson {

	@Expose
	private java.util.List<com.mphrx.fisike.gson.request.List> list = new ArrayList<com.mphrx.fisike.gson.request.List>();
	@Expose
	private Integer totalCount;

	/**
	 * 
	 * @return The list
	 */
	public java.util.List<com.mphrx.fisike.gson.request.List> getList() {
		return list;
	}

	/**
	 * 
	 * @param list
	 *  The list
	 */
	public void setList(java.util.List<com.mphrx.fisike.gson.request.List> list) {
		this.list = list;
	}

	/**
	 * 
	 * @return The totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * 
	 * @param totalCount
	 *  The totalCount
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
