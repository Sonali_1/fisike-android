package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class patIdentifier implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7445253889336428683L;
    ArrayList<PatientExtension> extension;
    String id;
    String label;
    String period;
    String system;
    String useCode;
    String value;
    String use;
    Object type;

    public ArrayList<PatientExtension> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<PatientExtension> extension) {
        this.extension = extension;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }
}
