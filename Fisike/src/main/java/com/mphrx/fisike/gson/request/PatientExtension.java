package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class PatientExtension implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8875436610416511306L;
	private String url;
	private ArrayList<Object> value;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public ArrayList<Object> getValue() {
		return value;
	}
	public void setValue(ArrayList<Object> value) {
		this.value = value;
	}
}
