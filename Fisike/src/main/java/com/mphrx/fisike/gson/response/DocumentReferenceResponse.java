package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentReferenceResponse {

	@Expose
	private Object authenticator;
	@Expose
	private List<Author> author = new ArrayList<Author>();
	@Expose
	private Object confidentiality;
	@Expose
	private Object contextDocument;
	@Expose
	private Object created;
	@Expose
	private Object custodian;
	@Expose
	private String description;
	@Expose
	private Object docStatus;
	@Expose
	private Object documentClass;
	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private List<Object> format = new ArrayList<Object>();
	@Expose
	private Object hash;
	@Expose
	private Integer id;
	@Expose
	private List<Object> identifier = new ArrayList<Object>();
	@Expose
	private Indexed indexed;
	@Expose
	private Object location;
	@Expose
	private MasterIdentifier masterIdentifier;
	@Expose
	private String mimeType;
	@Expose
	private Object policyManager;
	@Expose
	private Object primaryLanguage;
	@Expose
	private List<Object> relatesTo = new ArrayList<Object>();
	@SerializedName("class")
	@Expose
	private String _class;
	@Expose
	private ServiceDocument serviceDocument;
	@Expose
	private Size size;
	@Expose
	private String status;
	@Expose
	private Subject subject;
	@Expose
	private Type_ type;

	/**
	 * 
	 * @return The authenticator
	 */
	public Object getAuthenticator() {
		return authenticator;
	}

	/**
	 * 
	 * @param authenticator
	 *  The authenticator
	 */
	public void setAuthenticator(Object authenticator) {
		this.authenticator = authenticator;
	}

	/**
	 * 
	 * @return The author
	 */
	public List<Author> getAuthor() {
		return author;
	}

	/**
	 * 
	 * @param author
	 *  The author
	 */
	public void setAuthor(List<Author> author) {
		this.author = author;
	}

	/**
	 * 
	 * @return The confidentiality
	 */
	public Object getConfidentiality() {
		return confidentiality;
	}

	/**
	 * 
	 * @param confidentiality
	 *  The confidentiality
	 */
	public void setConfidentiality(Object confidentiality) {
		this.confidentiality = confidentiality;
	}

	/**
	 * 
	 * @return The contextDocument
	 */
	public Object getContextDocument() {
		return contextDocument;
	}

	/**
	 * 
	 * @param contextDocument
	 *  The contextDocument
	 */
	public void setContextDocument(Object contextDocument) {
		this.contextDocument = contextDocument;
	}

	/**
	 * 
	 * @return The created
	 */
	public Object getCreated() {
		return created;
	}

	/**
	 * 
	 * @param created
	 *  The created
	 */
	public void setCreated(Object created) {
		this.created = created;
	}

	/**
	 * 
	 * @return The custodian
	 */
	public Object getCustodian() {
		return custodian;
	}

	/**
	 * 
	 * @param custodian
	 *  The custodian
	 */
	public void setCustodian(Object custodian) {
		this.custodian = custodian;
	}

	/**
	 * 
	 * @return The description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *  The description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return The docStatus
	 */
	public Object getDocStatus() {
		return docStatus;
	}

	/**
	 * 
	 * @param docStatus
	 *  The docStatus
	 */
	public void setDocStatus(Object docStatus) {
		this.docStatus = docStatus;
	}

	/**
	 * 
	 * @return The documentClass
	 */
	public Object getDocumentClass() {
		return documentClass;
	}

	/**
	 * 
	 * @param documentClass
	 *  The documentClass
	 */
	public void setDocumentClass(Object documentClass) {
		this.documentClass = documentClass;
	}

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The format
	 */
	public List<Object> getFormat() {
		return format;
	}

	/**
	 * 
	 * @param format
	 *  The format
	 */
	public void setFormat(List<Object> format) {
		this.format = format;
	}

	/**
	 * 
	 * @return The hash
	 */
	public Object getHash() {
		return hash;
	}

	/**
	 * 
	 * @param hash
	 *  The hash
	 */
	public void setHash(Object hash) {
		this.hash = hash;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The identifier
	 */
	public List<Object> getIdentifier() {
		return identifier;
	}

	/**
	 * 
	 * @param identifier
	 *  The identifier
	 */
	public void setIdentifier(List<Object> identifier) {
		this.identifier = identifier;
	}

	/**
	 * 
	 * @return The indexed
	 */
	public Indexed getIndexed() {
		return indexed;
	}

	/**
	 * 
	 * @param indexed
	 *  The indexed
	 */
	public void setIndexed(Indexed indexed) {
		this.indexed = indexed;
	}

	/**
	 * 
	 * @return The location
	 */
	public Object getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 *  The location
	 */
	public void setLocation(Object location) {
		this.location = location;
	}

	/**
	 * 
	 * @return The masterIdentifier
	 */
	public MasterIdentifier getMasterIdentifier() {
		return masterIdentifier;
	}

	/**
	 * 
	 * @param masterIdentifier
	 *  The masterIdentifier
	 */
	public void setMasterIdentifier(MasterIdentifier masterIdentifier) {
		this.masterIdentifier = masterIdentifier;
	}

	/**
	 * 
	 * @return The mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * 
	 * @param mimeType
	 *  The mimeType
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * 
	 * @return The policyManager
	 */
	public Object getPolicyManager() {
		return policyManager;
	}

	/**
	 * 
	 * @param policyManager
	 *  The policyManager
	 */
	public void setPolicyManager(Object policyManager) {
		this.policyManager = policyManager;
	}

	/**
	 * 
	 * @return The primaryLanguage
	 */
	public Object getPrimaryLanguage() {
		return primaryLanguage;
	}

	/**
	 * 
	 * @param primaryLanguage
	 *  The primaryLanguage
	 */
	public void setPrimaryLanguage(Object primaryLanguage) {
		this.primaryLanguage = primaryLanguage;
	}

	/**
	 * 
	 * @return The relatesTo
	 */
	public List<Object> getRelatesTo() {
		return relatesTo;
	}

	/**
	 * 
	 * @param relatesTo
	 *  The relatesTo
	 */
	public void setRelatesTo(List<Object> relatesTo) {
		this.relatesTo = relatesTo;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

	/**
	 * 
	 * @return The serviceDocument
	 */
	public ServiceDocument getServiceDocument() {
		return serviceDocument;
	}

	/**
	 * 
	 * @param serviceDocument
	 *  The serviceDocument
	 */
	public void setServiceDocument(ServiceDocument serviceDocument) {
		this.serviceDocument = serviceDocument;
	}

	/**
	 * 
	 * @return The size
	 */
	public Size getSize() {
		return size;
	}

	/**
	 * 
	 * @param size
	 *  The size
	 */
	public void setSize(Size size) {
		this.size = size;
	}

	/**
	 * 
	 * @return The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *  The status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return The subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 *  The subject
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return The type
	 */
	public Type_ getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *  The type
	 */
	public void setType(Type_ type) {
		this.type = type;
	}

}
