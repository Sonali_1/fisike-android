package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.Coding;

public class Reason {

	@Expose
	private List<Coding> coding = new ArrayList<Coding>();
	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private Object id;
	@Expose
	private String text;

	/**
	 * 
	 * @return The coding
	 */
	public List<Coding> getCoding() {
		return coding;
	}

	/**
	 * 
	 * @param coding
	 *  The coding
	 */
	public void setCoding(List<Coding> coding) {
		this.coding = coding;
	}

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *  The text
	 */
	public void setText(String text) {
		this.text = text;
	}

}
