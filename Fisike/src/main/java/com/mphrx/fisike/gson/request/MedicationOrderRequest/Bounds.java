package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xmb2nc on 16-02-2016.
 */
public class Bounds {

    @SerializedName("periodUnits")
    @Expose
    private StartDate startDate;

    public StartDate getStartDate() {
        return startDate;
    }

    public void setStartDate(StartDate startDate) {
        this.startDate = startDate;
    }
}
