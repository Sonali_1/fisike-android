package com.mphrx.fisike.gson.response;

/**
 * Created by xmb2nc on 18-11-2015.
 */
    import com.google.gson.annotations.Expose;
    import com.google.gson.annotations.SerializedName;

    public class UploadDeviceTokenResponse {

        @SerializedName("msg")
        @Expose
        private String msg;
        @SerializedName("status")
        @Expose
        private String status;

        /**
         *
         * @return
         * The msg
         */
        public String getMsg() {
            return msg;
        }

        /**
         *
         * @param msg
         * The msg
         */
        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         *
         * @return
         * The status
         */
        public String getStatus() {
            return status;
        }

        /**
         *
         * @param status
         * The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

    }
