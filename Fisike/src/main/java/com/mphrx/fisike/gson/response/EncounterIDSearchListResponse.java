package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class EncounterIDSearchListResponse {
	@Expose
	List<EncounterList> encounterList = new ArrayList<EncounterList>();

	public List<EncounterList> getEncounterList() {
		return encounterList;
	}

	public void setEncounterList(List<EncounterList> encounterList) {
		this.encounterList = encounterList;
	}

}
