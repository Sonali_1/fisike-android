package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class Period {

	@Expose
	private EndDate endDate;
	@Expose
	private Object extension;
	@Expose
	private Object id;
	@Expose
	private StartDate startDate;

	/**
	 * 
	 * @return The endDate
	 */
	public EndDate getEndDate() {
		return endDate;
	}

	/**
	 * 
	 * @param endDate
	 *  The endDate
	 */
	public void setEndDate(EndDate endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 * @return The extension
	 */
	public Object getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(Object extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The startDate
	 */
	public StartDate getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 *  The startDate
	 */
	public void setStartDate(StartDate startDate) {
		this.startDate = startDate;
	}

}
