package com.mphrx.fisike.gson.response;

import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;

public class updatePatientResponse {
	String msg;
	String status;
	String httpStatusCode;
	UserMO user;
	String verificationCode;
	
	PatientMO patient;
	
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getStatus() {
		return status;
	}
	public String getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public UserMO getUser() {
		return user;
	}
	public void setUser(UserMO user) {
		this.user = user;
	}
	public PatientMO getPatient() {
		return patient;
	}
	public void setPatient(PatientMO patient) {
		this.patient = patient;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
