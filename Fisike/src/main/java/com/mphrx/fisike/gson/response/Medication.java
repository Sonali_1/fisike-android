package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Medication {

	@Expose
	private String ref;
	@Expose
	private Integer id;
	@SerializedName("class")
	@Expose
	private String _class;

	/**
	 * 
	 * @return The ref
	 */
	public String getRef() {
		return ref;
	}

	/**
	 * 
	 * @param ref
	 *  The ref
	 */
	public void setRef(String ref) {
		this.ref = ref;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

}
