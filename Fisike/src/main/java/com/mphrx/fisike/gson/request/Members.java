package com.mphrx.fisike.gson.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class Members implements Parcelable{
	
	public Members() {
	}

	@Expose
	private int organizationId;

	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	@Override
	public int describeContents() {
		return 0;
	}
	
	protected Members(Parcel in) {
		organizationId = in.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(organizationId);		
	}
	
	public static final Parcelable.Creator<Members> CREATOR = new Parcelable.Creator<Members>() {
	    public Members createFromParcel(Parcel in) {
	        return new Members(in);
	    }

	    public Members[] newArray(int size) {
	        return new Members[size];
	    }
	};

}
