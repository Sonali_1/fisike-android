package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xmb2nc on 16-02-2016.
 */
public class Name {
    @SerializedName("text")
    @Expose
    private  String text;

    @Expose
    private String useCode;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }
}
