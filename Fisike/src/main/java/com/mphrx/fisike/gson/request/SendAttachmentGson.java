package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class SendAttachmentGson {

	@Expose
	private String extension;
	@Expose
	private int id;
	@Expose
	private String type;

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
