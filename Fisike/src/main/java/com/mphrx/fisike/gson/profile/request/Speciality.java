
package com.mphrx.fisike.gson.profile.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.Expose;


public class Speciality {

    @Expose
private List<Object> extension = new ArrayList<Object>();
    @Expose
    private Object id;
    @Expose
    private String text;
    @Expose
    private List<Object> coding = new ArrayList<Object>();

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Object> getCoding() {
        return coding;
    }

    public void setCoding(List<Object> coding) {
        this.coding = coding;
    }

}
