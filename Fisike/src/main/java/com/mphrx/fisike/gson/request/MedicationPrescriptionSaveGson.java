package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicationPrescriptionSaveGson {

	@Expose
	private Boolean onlyReturnId;
	@Expose
	private Reason reason;
	@Expose
	private IsBrand isBrand;
	@Expose
	private String status;
	@Expose
	private Prescriber prescriber;
	@Expose
	private Patient patient;
	@Expose
	private Medication medication;
	@Expose
	private Encounter encounter;
	@Expose
	private List<DosageInstruction> dosageInstruction = new ArrayList<DosageInstruction>();
	@Expose
	private Substitution substitution;
	@Expose
	private Dispense dispense;
	@SerializedName("dateWritten")
	private StartDate dateWritten;
	@SerializedName("dateEnded")
	private EndDate dateEnded;

	@Expose
	private int id;


	public StartDate getDateWritten() {
		return dateWritten;
	}

	public void setDateWritten(StartDate dateWritten) {
		this.dateWritten = dateWritten;
	}

	public EndDate getDateEnded() {
		return dateEnded;
	}

	public void setDateEnded(EndDate dateEnded) {
		this.dateEnded = dateEnded;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The onlyReturnId
	 */
	public Boolean getOnlyReturnId() {
		return onlyReturnId;
	}

	/**
	 * 
	 * @param onlyReturnId
	 *            The onlyReturnId
	 */
	public void setOnlyReturnId(Boolean onlyReturnId) {
		this.onlyReturnId = onlyReturnId;
	}

	/**
	 * 
	 * @return The reason
	 */
	public Reason getReason() {
		return reason;
	}

	/**
	 * 
	 * @param reason
	 *            The reason
	 */
	public void setReason(Reason reason) {
		this.reason = reason;
	}

	/**
	 * 
	 * @return The isBrand
	 */
	public IsBrand getIsBrand() {
		return isBrand;
	}

	/**
	 * 
	 * @param isBrand
	 *            The isBrand
	 */
	public void setIsBrand(IsBrand isBrand) {
		this.isBrand = isBrand;
	}

	/**
	 * 
	 * @return The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *            The status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return The prescriber
	 */
	public Prescriber getPrescriber() {
		return prescriber;
	}

	/**
	 * 
	 * @param prescriber
	 *            The prescriber
	 */
	public void setPrescriber(Prescriber prescriber) {
		this.prescriber = prescriber;
	}

	/**
	 * 
	 * @return The patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * 
	 * @param patient
	 *            The patient
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * 
	 * @return The medication
	 */
	public Medication getMedication() {
		return medication;
	}

	/**
	 * 
	 * @param medication
	 *            The medication
	 */
	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	/**
	 * 
	 * @return The encounter
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	/**
	 * 
	 * @param encounter
	 *            The encounter
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * 
	 * @return The dosageInstruction
	 */
	public List<DosageInstruction> getDosageInstruction() {
		return dosageInstruction;
	}

	/**
	 * 
	 * @param dosageInstruction
	 *            The dosageInstruction
	 */
	public void setDosageInstruction(List<DosageInstruction> dosageInstruction) {
		this.dosageInstruction = dosageInstruction;
	}

	/**
	 * 
	 * @return The substitution
	 */
	public Substitution getSubstitution() {
		return substitution;
	}

	/**
	 * 
	 * @param substitution
	 *            The substitution
	 */
	public void setSubstitution(Substitution substitution) {
		this.substitution = substitution;
	}

	/**
	 * 
	 * @return The dispense
	 */
	public Dispense getDispense() {
		return dispense;
	}

	/**
	 * 
	 * @param dispense
	 *            The dispense
	 */
	public void setDispense(Dispense dispense) {
		this.dispense = dispense;
	}

}
