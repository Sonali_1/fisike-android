package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class ServiceDocument {

	@Expose
	private String address;
	@Expose
	private List<Object> parameterDoc = new ArrayList<Object>();
	@Expose
	private Type type;

	/**
	 * 
	 * @return The address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 
	 * @param address
	 *            The address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 
	 * @return The parameterDoc
	 */
	public List<Object> getParameterDoc() {
		return parameterDoc;
	}

	/**
	 * 
	 * @param parameterDoc
	 *            The parameterDoc
	 */
	public void setParameterDoc(List<Object> parameterDoc) {
		this.parameterDoc = parameterDoc;
	}

	/**
	 * 
	 * @return The type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *            The type
	 */
	public void setType(Type type) {
		this.type = type;
	}

}
