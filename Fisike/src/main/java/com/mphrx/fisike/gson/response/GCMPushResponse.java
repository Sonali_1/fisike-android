package com.mphrx.fisike.gson.response;

/**
 * Created by xmb2nc on 20-11-2015.
 */

    import com.google.gson.annotations.Expose;
    import com.google.gson.annotations.SerializedName;

    public class GCMPushResponse {

        @SerializedName("msgCount")
        @Expose
        private String msgCount;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("info")
        @Expose
        private String info;

        /**
         *
         * @return
         * The msgCount
         */
        public String getMsgCount() {
            return msgCount;
        }

        /**
         *
         * @param msgCount
         * The msgCount
         */
        public void setMsgCount(String msgCount) {
            this.msgCount = msgCount;
        }

        /**
         *
         * @return
         * The type
         */
        public String getType() {
            return type;
        }

        /**
         *
         * @param type
         * The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         *
         * @return
         * The info
         */
        public String getInfo() {
            return info;
        }

        /**
         *
         * @param info
         * The info
         */
        public void setInfo(String info) {
            this.info = info;
        }

    }

