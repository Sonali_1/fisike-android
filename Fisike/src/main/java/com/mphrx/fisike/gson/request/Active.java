package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class Active implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9009495039733686696L;

	ArrayList<PatientExtension> extension;
	String id;
	boolean value;

	public ArrayList<PatientExtension> getExtension() {
		return extension;
	}

	public void setExtension(ArrayList<PatientExtension> extension) {
		this.extension = extension;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

}
