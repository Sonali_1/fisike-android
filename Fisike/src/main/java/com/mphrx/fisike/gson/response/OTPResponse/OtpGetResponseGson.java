package com.mphrx.fisike.gson.response.OTPResponse;

/**
 * Created by Neha on 04-03-2016.
 */
public class OtpGetResponseGson
{

    String msg;
    String status;
    boolean messageSent;
    int httpStatusCode;
    boolean userExist;
    private boolean adminCreatedPasswordNeverChanged;

    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }

    public boolean isMessageSent() {
        return messageSent;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public boolean isUserExist() {
        return userExist;
    }
    public boolean isAdminCreatedPasswordNeverChanged() {
        return adminCreatedPasswordNeverChanged;
    }


}

