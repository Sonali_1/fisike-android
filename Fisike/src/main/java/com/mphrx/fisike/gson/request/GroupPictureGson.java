package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class GroupPictureGson {

	@SerializedName("groupChatDisplayPic")
	private ArrayList<GroupProfilePicture> groupChatDisplayPic;

	@SerializedName("error")
	private String error;

	@SerializedName("status")
	private boolean status;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ArrayList<GroupProfilePicture> getGroupChatDisplayPic() {
		return groupChatDisplayPic;
	}

	public void setGroupChatDisplayPic(ArrayList<GroupProfilePicture> groupChatDisplayPic) {
		this.groupChatDisplayPic = groupChatDisplayPic;
	}

	public class GroupProfilePicture {

		@SerializedName("groupId")
		private String groupId;

		@SerializedName("groupDisplayPic")
		private byte[] groupDisplayPic;

		public String getGroupId() {
			return groupId;
		}

		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}

		public byte[] getGroupDisplayPic() {
			return groupDisplayPic;
		}

		public void setGroupDisplayPic(byte[] groupDisplayPic) {
			this.groupDisplayPic = groupDisplayPic;
		}
	}
}
