package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.gson.request.*;

public class MedicationOrder {

    @SerializedName("resourceType")
    @Expose
    public String resourceType;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("meta")
    @Expose
    public Meta meta;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("entry")
    @Expose
    public List<Entry> entry = new ArrayList<Entry>();

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }

}