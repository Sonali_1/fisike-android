package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class DoseQuantity_ {

	@Expose
	private Object code;
	@Expose
	private Object comparator;
	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private Object id;
	@Expose
	private Object system;
	@Expose
	private String units;
	@Expose
	private String value;

	/**
	 * 
	 * @return The code
	 */
	public Object getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 *  The code
	 */
	public void setCode(Object code) {
		this.code = code;
	}

	/**
	 * 
	 * @return The comparator
	 */
	public Object getComparator() {
		return comparator;
	}

	/**
	 * 
	 * @param comparator
	 *  The comparator
	 */
	public void setComparator(Object comparator) {
		this.comparator = comparator;
	}

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The system
	 */
	public Object getSystem() {
		return system;
	}

	/**
	 * 
	 * @param system
	 *  The system
	 */
	public void setSystem(Object system) {
		this.system = system;
	}

	/**
	 * 
	 * @return The units
	 */
	public String getUnits() {
		return units;
	}

	/**
	 * 
	 * @param units
	 *  The units
	 */
	public void setUnits(String units) {
		this.units = units;
	}

	/**
	 * 
	 * @return The value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 *  The value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
