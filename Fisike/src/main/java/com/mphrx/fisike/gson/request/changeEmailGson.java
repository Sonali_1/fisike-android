package com.mphrx.fisike.gson.request;

/**
 * Created by administrate on 11/21/2015.
 */
public class changeEmailGson
{

    String email;
    String phoneNo;
    String deviceUID;
    String otp;


    boolean updateEmail;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getDeviceUID() {
        return deviceUID;
    }

    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean isUpdateEmail() {
        return updateEmail;
    }

    public void setUpdateEmail(boolean updateEmail) {
        this.updateEmail = updateEmail;
    }
}

