package com.mphrx.fisike.gson.response;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicineList {

	@Expose
	private Code code;
	@Expose
	private java.util.List<Object> extension = new ArrayList<Object>();
	@Expose
	private Integer id;
	@Expose
	private Object isBrand;
	@Expose
	private Object kind;
	@Expose
	private Object manufacturer;
	@Expose
	private Object medPackage;
	@Expose
	private Object medProduct;
	@SerializedName("class")
	@Expose
	private String _class;

	/**
	 * 
	 * @return The code
	 */
	public Code getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 * The code
	 */
	public void setCode(Code code) {
		this.code = code;
	}

	/**
	 * 
	 * @return The extension
	 */
	public java.util.List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(java.util.List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The isBrand
	 */
	public Object getIsBrand() {
		return isBrand;
	}

	/**
	 * 
	 * @param isBrand
	 *  The isBrand
	 */
	public void setIsBrand(Object isBrand) {
		this.isBrand = isBrand;
	}

	/**
	 * 
	 * @return The kind
	 */
	public Object getKind() {
		return kind;
	}

	/**
	 * 
	 * @param kind
	 *  The kind
	 */
	public void setKind(Object kind) {
		this.kind = kind;
	}

	/**
	 * 
	 * @return The manufacturer
	 */
	public Object getManufacturer() {
		return manufacturer;
	}

	/**
	 * 
	 * @param manufacturer
	 *  The manufacturer
	 */
	public void setManufacturer(Object manufacturer) {
		this.manufacturer = manufacturer;
	}

	/**
	 * 
	 * @return The medPackage
	 */
	public Object getMedPackage() {
		return medPackage;
	}

	/**
	 * 
	 * @param medPackage
	 *  The medPackage
	 */
	public void setMedPackage(Object medPackage) {
		this.medPackage = medPackage;
	}

	/**
	 * 
	 * @return The medProduct
	 */
	public Object getMedProduct() {
		return medProduct;
	}

	/**
	 * 
	 * @param medProduct
	 *  The medProduct
	 */
	public void setMedProduct(Object medProduct) {
		this.medProduct = medProduct;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

}