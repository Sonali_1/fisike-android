package com.mphrx.fisike.gson.response.ForgetPasswordResponseGson;

/**
 * Created by Neha on 19-02-2016.
 */
public class SetPasswordResponseGson {

    String msg;
    String status;
    String httpStatusCode;

    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }

    public String gethttpStatusCode() {
        return httpStatusCode;
    }
}
