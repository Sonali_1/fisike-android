package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class CodingExtensionTextFormat implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2611843146777748979L;
	private ArrayList<Coding> coding;
	private ArrayList<PatientExtension> extension;
	private String id;
	private String text;
	public ArrayList<Coding> getCoding() {
		return coding;
	}
	public void setCoding(ArrayList<Coding> coding) {
		this.coding = coding;
	}
	public ArrayList<PatientExtension> getExtension() {
		return extension;
	}
	public void setExtension(ArrayList<PatientExtension> extension) {
		this.extension = extension;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
}
