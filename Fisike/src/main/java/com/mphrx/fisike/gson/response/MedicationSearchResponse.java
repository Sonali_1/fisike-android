package com.mphrx.fisike.gson.response;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class MedicationSearchResponse {

	@Expose
	private java.util.List<MedicineList> list = new ArrayList<MedicineList>();
	@Expose
	private Integer totalCount;

	/**
	 * 
	 * @return The list
	 */
	public java.util.List<MedicineList> getList() {
		return list;
	}

	/**
	 * 
	 * @param list
	 *  The list
	 */
	public void setList(java.util.List<MedicineList> list) {
		this.list = list;
	}

	/**
	 * 
	 * @return The totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * 
	 * @param totalCount
	 *  The totalCount
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}