package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class TimingPeriod {

@Expose
private StartDate startDate;
@Expose
private EndDate endDate;

/**
* 
* @return
* The startDate
*/
public StartDate getStartDate() {
return startDate;
}

/**
* 
* @param startDate
* The startDate
*/
public void setStartDate(StartDate startDate) {
this.startDate = startDate;
}

/**
* 
* @return
* The endDate
*/
public EndDate getEndDate() {
return endDate;
}

/**
* 
* @param endDate
* The endDate
*/
public void setEndDate(EndDate endDate) {
this.endDate = endDate;
}

}
