package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.SerializedName;

public class UserLoggedInGson extends CommonApiResponse {

	@SerializedName("loggedInDetails")
	private LoggedInDetails loggedInDetails;

	public LoggedInDetails getLoggedInDetails() {
		return loggedInDetails;
	}

	public void setLoggedInDetails(LoggedInDetails loggedInDetails) {
		this.loggedInDetails = loggedInDetails;
	}

	public class LoggedInDetails {
		@SerializedName("deviceUID")
		private String deviceUID;

		@SerializedName("registrationStatus")
		private boolean registrationStatus;

		@SerializedName("deviceActivationDate")
		private String deviceActivationDate;

		public String getDeviceUID() {
			return deviceUID;
		}

		public void setDeviceUID(String deviceUID) {
			this.deviceUID = deviceUID;
		}

		public boolean isRegistrationStatus() {
			return registrationStatus;
		}

		public void setRegistrationStatus(boolean registrationStatus) {
			this.registrationStatus = registrationStatus;
		}

		public String getDeviceActivationDate() {
			return deviceActivationDate;
		}

		public void setDeviceActivationDate(String deviceActivationDate) {
			this.deviceActivationDate = deviceActivationDate;
		}
	}

}
