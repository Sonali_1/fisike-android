package com.mphrx.fisike.gson.response;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

	@Expose
	private Object dateWritten;
	@Expose
	private Dispense dispense;
	@Expose
	private java.util.List<DosageInstruction> dosageInstruction = new ArrayList<DosageInstruction>();
	@Expose
	private Encounter encounter;
	@Expose
	private java.util.List<Object> extension = new ArrayList<Object>();
	@Expose
	private Integer id;
	@Expose
	private java.util.List<Object> identifier = new ArrayList<Object>();
	@Expose
	private Medication_ medication;
	@Expose
	private Patient patient;
	@Expose
	private Prescriber prescriber;
	@Expose
	private Reason reason;
	@SerializedName("class")
	@Expose
	private String _class;
	@Expose
	private String status;
	@Expose
	private Substitution substitution;

	/**
	 * 
	 * @return The dateWritten
	 */
	public Object getDateWritten() {
		return dateWritten;
	}

	/**
	 * 
	 * @param dateWritten
	 *  The dateWritten
	 */
	public void setDateWritten(Object dateWritten) {
		this.dateWritten = dateWritten;
	}

	/**
	 * 
	 * @return The dispense
	 */
	public Dispense getDispense() {
		return dispense;
	}

	/**
	 * 
	 * @param dispense
	 *  The dispense
	 */
	public void setDispense(Dispense dispense) {
		this.dispense = dispense;
	}

	/**
	 * 
	 * @return The dosageInstruction
	 */
	public java.util.List<DosageInstruction> getDosageInstruction() {
		return dosageInstruction;
	}

	/**
	 * 
	 * @param dosageInstruction
	 *  The dosageInstruction
	 */
	public void setDosageInstruction(java.util.List<DosageInstruction> dosageInstruction) {
		this.dosageInstruction = dosageInstruction;
	}

	/**
	 * 
	 * @return The encounter
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	/**
	 * 
	 * @param encounter
	 *  The encounter
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * 
	 * @return The extension
	 */
	public java.util.List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(java.util.List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The identifier
	 */
	public java.util.List<Object> getIdentifier() {
		return identifier;
	}

	/**
	 * 
	 * @param identifier
	 *  The identifier
	 */
	public void setIdentifier(java.util.List<Object> identifier) {
		this.identifier = identifier;
	}

	/**
	 * 
	 * @return The medication
	 */
	public Medication_ getMedication() {
		return medication;
	}

	/**
	 * 
	 * @param medication
	 *  The medication
	 */
	public void setMedication(Medication_ medication) {
		this.medication = medication;
	}

	/**
	 * 
	 * @return The patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * 
	 * @param patient
	 *  The patient
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * 
	 * @return The prescriber
	 */
	public Prescriber getPrescriber() {
		return prescriber;
	}

	/**
	 * 
	 * @param prescriber
	 *  The prescriber
	 */
	public void setPrescriber(Prescriber prescriber) {
		this.prescriber = prescriber;
	}

	/**
	 * 
	 * @return The reason
	 */
	public Reason getReason() {
		return reason;
	}

	/**
	 * 
	 * @param reason
	 *  The reason
	 */
	public void setReason(Reason reason) {
		this.reason = reason;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

	/**
	 * 
	 * @return The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *  The status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return The substitution
	 */
	public Substitution getSubstitution() {
		return substitution;
	}

	/**
	 * 
	 * @param substitution
	 *  The substitution
	 */
	public void setSubstitution(Substitution substitution) {
		this.substitution = substitution;
	}

}