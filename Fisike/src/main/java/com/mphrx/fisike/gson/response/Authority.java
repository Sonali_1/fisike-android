package com.mphrx.fisike.gson.response;

import java.io.Serializable;

public class Authority implements Serializable {
	long id;
	String authority;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
