package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.Medication;
import com.mphrx.fisike.gson.request.NumberOfRepeatsAllowed;
import com.mphrx.fisike.gson.request.Quantity;

public class Dispense {

	@Expose
	private Object expectedSupplyDuration;
	@Expose
	private Medication medication;
	@Expose
	private NumberOfRepeatsAllowed numberOfRepeatsAllowed;
	@Expose
	private Quantity quantity;
	@Expose
	private Object validityPeriod;

	/**
	 * 
	 * @return The expectedSupplyDuration
	 */
	public Object getExpectedSupplyDuration() {
		return expectedSupplyDuration;
	}

	/**
	 * 
	 * @param expectedSupplyDuration
	 *            The expectedSupplyDuration
	 */
	public void setExpectedSupplyDuration(Object expectedSupplyDuration) {
		this.expectedSupplyDuration = expectedSupplyDuration;
	}

	/**
	 * 
	 * @return The medication
	 */
	public Medication getMedication() {
		return medication;
	}

	/**
	 * 
	 * @param medication
	 *            The medication
	 */
	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	/**
	 * 
	 * @return The numberOfRepeatsAllowed
	 */
	public NumberOfRepeatsAllowed getNumberOfRepeatsAllowed() {
		return numberOfRepeatsAllowed;
	}

	/**
	 * 
	 * @param numberOfRepeatsAllowed
	 *            The numberOfRepeatsAllowed
	 */
	public void setNumberOfRepeatsAllowed(NumberOfRepeatsAllowed numberOfRepeatsAllowed) {
		this.numberOfRepeatsAllowed = numberOfRepeatsAllowed;
	}

	/**
	 * 
	 * @return The quantity
	 */
	public Quantity getQuantity() {
		return quantity;
	}

	/**
	 * 
	 * @param quantity
	 *            The quantity
	 */
	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	/**
	 * 
	 * @return The validityPeriod
	 */
	public Object getValidityPeriod() {
		return validityPeriod;
	}

	/**
	 * 
	 * @param validityPeriod
	 *            The validityPeriod
	 */
	public void setValidityPeriod(Object validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

}
