package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;

public class IDSaveResponse {

	@Expose
	private Integer id;

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}
