package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class CommonApiResponse {

	@Expose
	private String success;

	@Expose
	private String error;

	@Expose
	private boolean status;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}