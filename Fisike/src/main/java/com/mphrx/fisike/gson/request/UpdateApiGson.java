package com.mphrx.fisike.gson.request;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class UpdateApiGson {

	@SerializedName("success")
	private ArrayList<Success> success;

	@SerializedName("error")
	private ArrayList<Error> error;

	public ArrayList<Success> getSuccess() {
		return success;
	}

	public void setSuccess(ArrayList<Success> success) {
		this.success = success;
	}

	public ArrayList<Error> getError() {
		return error;
	}

	public void setError(ArrayList<Error> error) {
		this.error = error;
	}

	public class Success {

		@SerializedName("code")
		private int code;

		@SerializedName("text")
		private String text;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
	}
	
	public class Error {

		@SerializedName("code")
		private int code;

		@SerializedName("text")
		private String text;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
	}
}