package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 2/21/2016.
 */
public class AdditionalInstructions {

    @Expose
    private ArrayList<Coding> coding;

    public ArrayList<Coding> getCoding() {
        return coding;
    }

    public void setCoding(ArrayList<Coding> coding) {
        this.coding = coding;
    }
}
