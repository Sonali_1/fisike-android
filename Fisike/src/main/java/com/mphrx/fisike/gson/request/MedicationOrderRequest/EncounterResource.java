package com.mphrx.fisike.gson.request.MedicationOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Khurana on 2/24/2016.
 */
public class EncounterResource {
    @Expose
    public String resourceType;
    @Expose
    public Patient patient;
    @Expose
    public List<Extension> extension = new ArrayList<Extension>();
    @Expose
    private ClassCode classCode;
    @Expose
    private Status status;
    @Expose
    private Period period;
    @Expose
    private List<Identifier> identifier;
    @Expose
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Identifier> getIdentifier() {
        return identifier;
    }
    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }
    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Extension> getExtension() {
        return extension;
    }

    public void setExtension(List<Extension> extension) {
        this.extension = extension;
    }

    public ClassCode getClassCode() {
        return classCode;
    }

    public void setClassCode(ClassCode classCode) {
        this.classCode = classCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }
}
