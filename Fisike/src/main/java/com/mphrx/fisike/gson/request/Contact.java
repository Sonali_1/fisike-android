package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;

public class Contact implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1734915112310756396L;
	Address address;
	ArrayList<PatientExtension> extension;
	String gender;
	String id;
	Name name;
	String organization;
	ArrayList<CodingExtensionTextFormat> relationship;
	ArrayList<telecom> telecom;
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ArrayList<PatientExtension> getExtension() {
		return extension;
	}
	public void setExtension(ArrayList<PatientExtension> extension) {
		this.extension = extension;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Name getName() {
		return name;
	}
	public void setName(Name name) {
		this.name = name;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public ArrayList<CodingExtensionTextFormat> getRelationship() {
		return relationship;
	}
	public void setRelationship(ArrayList<CodingExtensionTextFormat> relationship) {
		this.relationship = relationship;
	}
	public ArrayList<telecom> getTelecom() {
		return telecom;
	}
	public void setTelecom(ArrayList<telecom> telecom) {
		this.telecom = telecom;
	}
	
}
