package com.mphrx.fisike.gson.response;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class EncounterSearchResponse {
	@Expose
	private java.util.List<EncounterList> list = new ArrayList<EncounterList>();
	@Expose
	private Integer totalCount;

	/**
	* 
	* @return
	* The list
	*/
	public java.util.List<EncounterList> getList() {
	return list;
	}

	/**
	* 
	* @param list
	* The list
	*/
	public void setList(java.util.List<EncounterList> list) {
	this.list = list;
	}

	/**
	* 
	* @return
	* The totalCount
	*/
	public Integer getTotalCount() {
	return totalCount;
	}

	/**
	* 
	* @param totalCount
	* The totalCount
	*/
	public void setTotalCount(Integer totalCount) {
	this.totalCount = totalCount;
	}
}
