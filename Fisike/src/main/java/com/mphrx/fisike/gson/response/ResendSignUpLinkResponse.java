package com.mphrx.fisike.gson.response;

/**
 * Created by administrate on 11/19/2015.
 */
public class ResendSignUpLinkResponse
{
String msg;
    String status;
    String verificationCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
