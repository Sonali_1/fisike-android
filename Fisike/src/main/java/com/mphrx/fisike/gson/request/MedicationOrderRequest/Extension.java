package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Extension implements Serializable {

    private static final long serialVersionUID = 8423953301270531736L;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("value")
    @Expose
    public ArrayList<String> value;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<String> getValue() {
        return value;
    }

    public void setValue(ArrayList<String> value) {
        this.value = value;
    }
}
