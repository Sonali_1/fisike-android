package com.mphrx.fisike.gson.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public abstract class AbstractGsonObjectParser {

	public abstract Class<?> objectType();

	public Object getObject(String jsonString) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);
		Gson gson = gsonBuilder.create();

		return gson.fromJson(jsonString, objectType());
	}

}
