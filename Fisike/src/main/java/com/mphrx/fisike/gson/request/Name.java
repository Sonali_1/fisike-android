package com.mphrx.fisike.gson.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Name implements Serializable{

	private static final long serialVersionUID = 6007443889224258274L;
	@Expose
	private List<String> family = new ArrayList<String>();
	@Expose
	private List<String> given = new ArrayList<String>();
	@Expose
	private Object period;
	@Expose
	private List<Object> prefix = new ArrayList<Object>();
	@Expose
	private List<Object> suffix = new ArrayList<Object>();
	@Expose
	private String text;
	@Expose
	private String useCode;

	/**
	 * 
	 * @return The family
	 */
	public List<String> getFamily() {
		return family;
	}

	/**
	 * 
	 * @param family
	 *  The family
	 */
	public void setFamily(List<String> family) {
		this.family = family;
	}

	/**
	 * 
	 * @return The given
	 */
	public List<String> getGiven() {
		return given;
	}

	/**
	 * 
	 * @param given
	 * The given
	 */
	public void setGiven(List<String> given) {
		this.given = given;
	}

	/**
	 * 
	 * @return The period
	 */
	public Object getPeriod() {
		return period;
	}

	/**
	 * 
	 * @param period
	 *  The period
	 */
	public void setPeriod(Object period) {
		this.period = period;
	}

	/**
	 * 
	 * @return The prefix
	 */
	public List<Object> getPrefix() {
		return prefix;
	}

	/**
	 * 
	 * @param prefix
	 *  The prefix
	 */
	public void setPrefix(List<Object> prefix) {
		this.prefix = prefix;
	}

	/**
	 * 
	 * @return The suffix
	 */
	public List<Object> getSuffix() {
		return suffix;
	}

	/**
	 * 
	 * @param suffix
	 *            The suffix
	 */
	public void setSuffix(List<Object> suffix) {
		this.suffix = suffix;
	}

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *  The text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 
	 * @return The useCode
	 */
	public String getUseCode() {
		return useCode;
	}

	/**
	 * 
	 * @param useCode
	 *  The useCode
	 */
	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}

}
