package com.mphrx.fisike.gson.response;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.mo.DiseaseMO;

import java.util.ArrayList;

/**
 * Created by Aastha on 31/12/2015.
 */
public class DiseaseSearchResponse {

    @Expose
    private java.util.List<DiseaseMO> list = new ArrayList<DiseaseMO>();
    @Expose
    private Integer totalCount;

    /**
     *
     * @return The list
     */
    public java.util.List<DiseaseMO> getList() {
        return list;
    }

    /**
     *
     * @param list
     *  The list
     */
    public void setList(java.util.List<DiseaseMO> list) {
        this.list = list;
    }

    /**
     *
     * @return The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     *  The totalCount
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

}
