package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class DoseQuantity {
	@Expose
	private String unit;
	
	@Expose
	private Double value;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
