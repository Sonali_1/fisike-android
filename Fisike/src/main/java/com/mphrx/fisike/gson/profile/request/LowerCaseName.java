
package com.mphrx.fisike.gson.profile.request;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LowerCaseName {

    @Expose
    private String text;
    @Expose
    private List<String> given = new ArrayList<String>();
    @Expose
    private List<String> family = new ArrayList<String>();
    @Expose
    private List<Object> prefix = new ArrayList<Object>();
    @Expose
    private String useCode;
    @Expose
    private Object period;
    @Expose
    private List<Object> suffix = new ArrayList<Object>();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getGiven() {
        return given;
    }

    public void setGiven(List<String> given) {
        this.given = given;
    }

    public List<String> getFamily() {
        return family;
    }

    public void setFamily(List<String> family) {
        this.family = family;
    }

    public List<Object> getPrefix() {
        return prefix;
    }

    public void setPrefix(List<Object> prefix) {
        this.prefix = prefix;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public List<Object> getSuffix() {
        return suffix;
    }

    public void setSuffix(List<Object> suffix) {
        this.suffix = suffix;
    }

}
