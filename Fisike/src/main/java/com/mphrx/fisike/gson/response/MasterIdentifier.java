package com.mphrx.fisike.gson.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class MasterIdentifier {

	@Expose
	private List<Object> extension = new ArrayList<Object>();
	@Expose
	private Object id;
	@Expose
	private String label;
	@Expose
	private Object period;
	@Expose
	private Object system;
	@Expose
	private String useCode;
	@Expose
	private String value;

	/**
	 * 
	 * @return The extension
	 */
	public List<Object> getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *            The extension
	 */
	public void setExtension(List<Object> extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The id
	 */
	public Object getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * 
	 * @param label
	 *            The label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * 
	 * @return The period
	 */
	public Object getPeriod() {
		return period;
	}

	/**
	 * 
	 * @param period
	 *            The period
	 */
	public void setPeriod(Object period) {
		this.period = period;
	}

	/**
	 * 
	 * @return The system
	 */
	public Object getSystem() {
		return system;
	}

	/**
	 * 
	 * @param system
	 *            The system
	 */
	public void setSystem(Object system) {
		this.system = system;
	}

	/**
	 * 
	 * @return The useCode
	 */
	public String getUseCode() {
		return useCode;
	}

	/**
	 * 
	 * @param useCode
	 *            The useCode
	 */
	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}

	/**
	 * 
	 * @return The value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 *            The value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
