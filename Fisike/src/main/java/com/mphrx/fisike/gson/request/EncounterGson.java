package com.mphrx.fisike.gson.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EncounterGson {

	@Expose
	private String classCode;
	@Expose
	private Object extension;
	@Expose
	private Object hospitalization;
	@Expose
	private Integer id;
	@Expose
	private List<Identifier> identifier = new ArrayList<Identifier>();
	@Expose
	private Object indication;
	@Expose
	private Object length;
	@Expose
	private Object location;
	@Expose
	private Object partOf;
	@Expose
	private List<Participant> participant = new ArrayList<Participant>();
	@Expose
	private Period period;
	@Expose
	private Object priority;
	@Expose
	private Object reason;
	@SerializedName("class")
	@Expose
	private String _class;
	@Expose
	private Object serviceProvider;
	@Expose
	private String status;
	@Expose
	private Subject subject;
	@Expose
	private Object type;

	/**
	 * 
	 * @return The classCode
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * 
	 * @param classCode
	 *  The classCode
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * 
	 * @return The extension
	 */
	public Object getExtension() {
		return extension;
	}

	/**
	 * 
	 * @param extension
	 *  The extension
	 */
	public void setExtension(Object extension) {
		this.extension = extension;
	}

	/**
	 * 
	 * @return The hospitalization
	 */
	public Object getHospitalization() {
		return hospitalization;
	}

	/**
	 * 
	 * @param hospitalization
	 *  The hospitalization
	 */
	public void setHospitalization(Object hospitalization) {
		this.hospitalization = hospitalization;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *  The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The identifier
	 */
	public List<Identifier> getIdentifier() {
		return identifier;
	}

	/**
	 * 
	 * @param identifier
	 *  The identifier
	 */
	public void setIdentifier(List<Identifier> identifier) {
		this.identifier = identifier;
	}

	/**
	 * 
	 * @return The indication
	 */
	public Object getIndication() {
		return indication;
	}

	/**
	 * 
	 * @param indication
	 *  The indication
	 */
	public void setIndication(Object indication) {
		this.indication = indication;
	}

	/**
	 * 
	 * @return The length
	 */
	public Object getLength() {
		return length;
	}

	/**
	 * 
	 * @param length
	 *  The length
	 */
	public void setLength(Object length) {
		this.length = length;
	}

	/**
	 * 
	 * @return The location
	 */
	public Object getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 *  The location
	 */
	public void setLocation(Object location) {
		this.location = location;
	}

	/**
	 * 
	 * @return The partOf
	 */
	public Object getPartOf() {
		return partOf;
	}

	/**
	 * 
	 * @param partOf
	 *  The partOf
	 */
	public void setPartOf(Object partOf) {
		this.partOf = partOf;
	}

	/**
	 * 
	 * @return The participant
	 */
	public List<Participant> getParticipant() {
		return participant;
	}

	/**
	 * 
	 * @param participant
	 *  The participant
	 */
	public void setParticipant(List<Participant> participant) {
		this.participant = participant;
	}

	/**
	 * 
	 * @return The period
	 */
	public Period getPeriod() {
		return period;
	}

	/**
	 * 
	 * @param period
	 *  The period
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}

	/**
	 * 
	 * @return The priority
	 */
	public Object getPriority() {
		return priority;
	}

	/**
	 * 
	 * @param priority
	 *  The priority
	 */
	public void setPriority(Object priority) {
		this.priority = priority;
	}

	/**
	 * 
	 * @return The reason
	 */
	public Object getReason() {
		return reason;
	}

	/**
	 * 
	 * @param reason
	 *  The reason
	 */
	public void setReason(Object reason) {
		this.reason = reason;
	}

	/**
	 * 
	 * @return The _class
	 */
	public String getClass_() {
		return _class;
	}

	/**
	 * 
	 * @param _class
	 *  The class
	 */
	public void setClass_(String _class) {
		this._class = _class;
	}

	/**
	 * 
	 * @return The serviceProvider
	 */
	public Object getServiceProvider() {
		return serviceProvider;
	}

	/**
	 * 
	 * @param serviceProvider
	 *  The serviceProvider
	 */
	public void setServiceProvider(Object serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	/**
	 * 
	 * @return The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *  The status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return The subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 *  The subject
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return The type
	 */
	public Object getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *  The type
	 */
	public void setType(Object type) {
		this.type = type;
	}

}
