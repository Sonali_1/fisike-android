package com.mphrx.fisike.gson.request;

import com.google.gson.annotations.Expose;

public class AdditionalInstructions {

	@Expose
	private String text;

	/**
	 * 
	 * @return The text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *  The text
	 */
	public void setText(String text) {
		this.text = text;
	}

}