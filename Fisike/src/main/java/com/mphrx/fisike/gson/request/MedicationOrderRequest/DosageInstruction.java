package com.mphrx.fisike.gson.request.MedicationOrderRequest;

/**
 * Created by xmb2nc on 15-02-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DosageInstruction {

    @Expose
    private DoseQuantity doseQuantity;
    @Expose
    private String text;
    @Expose
    public Timing timing;
    @Expose
    private AdditionalInstructions additionalInstructions;

    public DoseQuantity getDoseQuantity() {
        return doseQuantity;
    }

    public void setDoseQuantity(DoseQuantity doseQuantity) {
        this.doseQuantity = doseQuantity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public AdditionalInstructions getAdditionalInstructions() {
        return additionalInstructions;
    }

    public void setAdditionalInstructions(AdditionalInstructions additionalInstructions) {
        this.additionalInstructions = additionalInstructions;
    }
}