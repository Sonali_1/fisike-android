package com.mphrx.fisike.gson.response.OTPResponse;

/**
 * Created by Neha on 15-02-2016.
 */
public class VerifyOTPResponseGson {

    String msg;
    String status;
    boolean verified;

    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
