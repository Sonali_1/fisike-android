
package com.mphrx.fisike.gson.profile.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PractitionerRole {

    @Expose
  private Object managingOrganization;
    @Expose
    private Object healthcareService;
    //specialty,speciality
    @SerializedName("specialty")
    @Expose
    private List<Speciality> speciality = new ArrayList<Speciality>();
    @Expose
    private Role role;
    @Expose
    private Object period;

    public Object getManagingOrganization() {
        return managingOrganization;
    }

    public void setManagingOrganization(Object managingOrganization) {
        this.managingOrganization = managingOrganization;
    }

    public Object getHealthcareService() {
        return healthcareService;
    }

    public void setHealthcareService(Object healthcareService) {
        this.healthcareService = healthcareService;
    }

    public List<Speciality> getSpeciality() {
        return speciality;
    }

    public void setSpeciality(List<Speciality> speciality) {
        this.speciality = speciality;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }
}
