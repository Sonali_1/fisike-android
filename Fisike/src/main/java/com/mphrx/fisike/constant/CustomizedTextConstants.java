
package com.mphrx.fisike.constant;

/**
 * Created by Neha on 14-10-2016.
 */

public class CustomizedTextConstants
{
	//Fisike Settings Changes

	public static final String EMAIL_TO = "support.intcs@agfa.com";
	// email for AGFA-"support.intcs@agfa.com";
	// email id to send mails  //fisike : "care@fisike.com"

	public static final CharSequence SMS_SENDER ="FISIKE" ; //SMS sender Name to read Message Recieved
	public static final String FAQ_URL_PATIENT = "/webconnect/#/faqs/"; // faq url for patient ,initial url automatically added in Api Manager to support multiple server
	public static final String FAQ_URL_PHY = "/webconnect/#/phy/faqs/";  // faq url for physician ,initial url automatically added in Api Manager to support multiple server
	public static final String TOU_URL_PATIENT = "/webconnect/#/termsofuse/";  // TOU url for patient,initial url automatically added in Api Manager to support multiple server
	public static final String TOU__URL_PHY =  "/webconnect/#/phy/termsofuse/"; // TOU url for physician ,initial url automatically added in Api Manager to support multiple server
	public static final String PP_URL_PATIENT = "/webconnect/#/privacypolicy/"; // Privacy policy  url for patient,initial url automatically added in Api Manager to support multiple server
	public static final String PP_URL_PHY = "/webconnect/#/privacypolicy/";  // Privacy policy url for physician ,initial url automatically added in Api Manager to support multiple server

	public static boolean IS_ICOMOON_TEXT_ON_SPLASH = false;  //config to determine weather Splash Screen is with icomoon text or not
	public static long PASSWORD_LENGTH= 6; 


	// static urls for minerva
	public static final String STATIC_FAQ_PATIENT="file:///android_asset/faqs.html";
	public static final String STATIC_FAQ_PHYSICIAN = "file:///android_asset/faqs-physician.html";
	public static final String STATIC_FAQ_PATIENT_FR="file:///android_asset/faqs-fr.html";
	public static final String STATIC_FAQ_PHYSICIAN_FR = "file:///android_asset/faqs-physician-fr.html";
	public static final String STATIC_TOU_PATIENT="file:///android_asset/termsOfUse.html";
	public static final String STATIC_TOU_PHYSICIAN="file:///android_asset/termsOfUse-physician.html";
	public static final String STATIC_PRIVACY_POLICY="file:///android_asset/privacyPolicy.html";


	//default title in case of context is null
	public static final String UPLOADS_TITLE="Documents";
	public static final String VITALS_TITLE="Vitals";
	public static final String MEDICATION_TITLE="Medications";
	public static final String RECORDS_TITLE="Results";

}
