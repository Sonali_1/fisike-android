package com.mphrx.fisike.constant;

import org.jivesoftware.smackx.pubsub.PublishItem;

public interface VariableConstants {


    public static final String FIRST_LOGIN_HIPPA = "FIRST_LOGIN_HIPPA";
    public static final String PASSWORD_EXPIRED_HIPPA = "PASSWORD_EXPIRED_HIPPA";
    public static final String SENDER_ID = "SENDER_ID";
    public static final String AVAILABLE = "AVAILABLE";
    public static final int PROCEED_TO_HOME = 1;
    public static final String ONLINE = "ONLINE";
    public static final String ADD_SCREEN = "ADD_SCREEN";
    public static final String PHONE_NUMBER = "MOBILE_NUMBER";
    public static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    public static final int CHAT_NOTIFICATION_NUMBER = 1001;
    public static final String USER_DETAILS_FILE = "UserDetailsFile";
    public static final String APPOINTMENT_SHARED_PREFERENCE = "AppointmentSharedPreference";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String IS_LOGOUT = "is_logout";
    public static final String IS_LOGOUT_DIALOG_SHOWING = "is_logout_dialog_showing";
    public static final String IS_PENDING_TIMER_RUNNING = "isPendingTimerStarted";
    public static final String IS_USER_LOGGED_IN = "isUserLoggedIn";
    public static final String IS_TIMER_STOPPED = "isTimerStopped";
    public static final String IS_GROUP_LIST_FETCHED = "isGroupListFetched";
    public static final String MUTE_CHAT_OPTION_ENABLED = "muteChatOptionEnabled";

    public static final String GERNAL_NOTIFICATON_SOUND = "GernalNotificationSound";
    public static final String DEFAULT_NOTIFICATION_SOUND = "content://settings/system/notification_sound";
    public static final String DB_KEY = "MphRx@FISIKE";

    public static final float MIN_SCROLL = 30;
    public static final int MENU_LIST = 1001;
    public static final String IS_TO_SET_PASSWORD = "is_to_set_password";
    public static final String LOCK_PIN_CODE = "Lock_pin_code";
    public static final String IS_TO_SHOW_PIN = "Is_to_show_pin";
    public static final String SENDER_NICK_NAME = "SENDER_NICK_NAME";
    public static final String MSG_STATUS_NOTIFICATION = "broadcastRefreshDB";
    public static final String OFFLINE_MSG = "OfflineMsgPK";
    public static final String OFFLINE_MSG_TIME = "OfflineMsgTime";
    public static final String OFFLINE_SENDER_USER_ID = "OfflineSenderId";
    public static final String NEW_MSG_BROADCAST = "NewMsgBroadCast";
    public static final String CONNECTION_ESTABLISH_TASK = "ConnectionEstablishedTask";
    public static final String GROUP_INVITE_SENT = "GroupInviteSent";
    public static final String REMOVE_MEBER_FROM_GROUP_SUCCES = "memberRemoved";
    public static final String LOGOUT_SESSTION = "LogoutSession";
    public static final String REGISTER_ERROR = "RegisterError";
    public static final String INCORRECT_PIN = "INCORRECT_PIN";
    public static final String BROADCAST_MSG_OBJ = "BroadcastMsgObj";
    public static final String IS_FROM_LOGIN_PAGE = "IS_FROM_LOGIN_PAGE";
    public static final String DELIMITER = "|$|$|$|$|$|$|";
    public static final String OFFLINE_MSG_ID = "OFFLINE_MSG_ID";
    public static final String BROADCAST_MSG_DELIVER = "BROADCAST_MSG_DELIVER";
    public static final String BROADCAST_DELIVERY_STATUS = "BROADCAST_DELIVERY_STATUS";
    public static final String BROADCAST_MSG_DELIVER_ID = "BROADCAST_MSG_DELIVER_ID";
    public static final String IS_PUSH_NOTIFICATION_MESSAGE = "IS_PUSH_NOTIFICATION_MESSAGE";
    public static final String IS_LOCAL_NOTIFICATION_MESSAGE = "IS_LOCAL_NOTIFICATION_MESSAGE";
    public static final String GCM_SENDER_ID = "27513946436";
    public static final String PUSH_NOTIFICATION_MESSAGE = "You have a new message";
    public static final String REVOKE_USER = "REVOKE_USER";
    public static final String LAST_AUTENTICATED_TIME = "LAST_AUTENTICATED_TIME";

    // Olympus Arch Changes
    public static final String BROADCAST_CHAT_MSG_MO = "BroadcastChatMsgMO";
    public static final String BROADCAST_CHAT_CONV_MO = "BroadcastChatConvMO";
    public static final String BROADCAST_CONV_LIST_CHANGED = "BroadcastConvListChanged";
    public static final String SENDER_UNREAD_COUNT = "SENDER_UNREAD_COUNT";
    public static final String IS_OFFLINE_MESSAGE = "IS_OFFLINE_MESSAGE";
    public static final String BROADCAST_MSG_CHANGE_LISTNER = "BROADCAST_MSG_CHANGE_LISTNER";
    public static final String BROADCAST_CONTACT_DETAIL_CHANGE = "BROADCAST_CONTACT_DETAIL_CHANGE";
    public static final String BROADCAST_GROUP_CONV_LIST_CHANGED = "BROADCAST_GROUP_CONV_LIST_CHANGED";
    public static final String SENDER_ORG_ID = "SENDER_ORG_ID";

	/*
     * Sender Id : 27513946436 API Key : AIzaSyCuXeRSJc5l3Bh7CQNxW95BdASAq55_1aQ
	 */

    /**
     *
     */
    public static final String VIDEO_URI = "video_uri";
    public static final String IS_ATTACHMENT_PREVIEW_BEFORE_SEND = "IS_ATTACHMENT_PREVIEW_BEFORE_SEND";
    public static final String ATTACHMENT_TYPE = "ATTACHMENT_TYPE";
    public static final String NAME_CAN_NOT_BLANK = "Name cannot be blank.";
    public static final String ERROR_MESSAGE_LOGIN = "ERROR_MESSAGE_LOGIN";
    public static final String SERVER_DOWN_ERROR = "We were unable to complete this action at this time. Please try again later.";
    public static final String IS_CAPTURED_IMAGE = "IS_CAPTURED_IMAGE";
    public static final String SHARE_IMAGE_INTENT = "attachmentImageShare";

    public static final String ATTACHMENT_IMAGE = "Image";
    public static final String ATTACHMENT_VIDEO = "Video";
    public static final String RESULT_CODE = "RESULT_CODE";

    public static final String ACK_MESSAGE_SERVER_RECEIVED = "ACK_MESSAGE_SERVER_RECEIVED";

    /**
     * Group constants
     */
    public static final int conversationTypeChat = 0;
    public static final int conversationTypeGroup = 1;

    public static final int chatConversationMyStatusActive = 0;
    public static final int chatConversationMyStatusInactive = 1;

    public static final String groupConferenceName = "conference.";
    public static final String ADMIN = "admin";
    public static final String IS_GROUP_CONTACT_UPDATION = "IS_GROUP_CONTACT_UPDATION";
    public static final String IS_GROUP_CHAT = "IS_GROUP_CHAT";
    public static final String SENDER_USER_NAME_IN_GROUP = "SENDER_USER_NAME_IN_GROUP";
    public static final String PATIENT = "PATIENT";
    public static final String PHYSICIAN = "Physician";
    public static final String CONVERSATION_ITEM = "CONVERSATION";
    public static final String GROUP_INFO = "GROUP";
    public static final String GROUP_DETAILS_TITLE = "Group Information";
    public static final int MAX_MEMBERS = 200;
    public static final String YOU = "You";
    public static final String MY_STATUS_ACTIVE = "MY_STATUS_ACTIVE";
    public static final String BROADCAST_GROUP_CHAT_REMOVE_MEMBER = "BROADCAST_GROUP_CHAT_REMOVE_MEMBER";
    public static final String GROUP_NAME_BLANK_ERROR = "Group name can not be blank";
    public static final String GROUP_NAME_SAME_ERROR = "Group name can not be same";
    public static final String IS_GROUP_DETAIL = "IS_GROUP_DETAIL";
    public static final String UpdatedPicContactId = "UpdatedPicContactId";
    public static String PROFILE_PIC_CHANGED_BROADCAST = "BROADCAST_PROFILE_IMAGE_CHANGED";

    public static final String LAUNCHED_FROM_GROUP_DETAIL_ACTIVITY = "launched_from_group_detail_activity";
    public static final String LAUNCH_GROUP_ROSTER = "launch_group_roster";
    public static final String REMOVE_GROUP_MEMBER_MESSAGE = "Removing member.Please wait!!";
    public static final String EXIT_GROUP_MESSAGE = "Exiting group.Please wait!!";
    public static final String GROUP_CREATION_ERROR = "groupCreationError";
    public static final String INVITE_LIST_ADDED_MEMBERS = "INVITE_LIST_ADDED_MEMBERS";
    public static final String ALREADY_ADDED_CONTACTS = "ALREADY_ADDED_CONTACTS";

    public static final String GROUP_ID_INTENT = "groupId";
    public static final String UPDATED_CONTACT = "UPDATED_CONTACT";

    public static final int ACTION_GROUP_EXIT = 0;
    public static final int ACTION_GROUP_REMOVE = 1;
    public static final int ACTION_GROUP_UPDATE_PIC = 2;
    public static final int ACTION_GROUP_UPDATE_NAME = 3;

    public static final String CHAT_CONTACT_MO = "ChatContactMO";
    public static final String GROUP_TEXT_CHAT_MO = "GroupTextChatMo";

    public static final String NETWORK_UNAVALIAVLE_MESSAGE = "You are currently offline. Please try again once you are connected to the internet.";
    public static final String NETWORK_UNAVALIAVLE_TITLE = "Device Offline";
    public static final String UNEXPECTED_TITLE = "Unexpected Error";
    public static final String UNEXPECTED_MESSAGE = "We were unable to complete this action. Please try again later.";
    public static final String IS_Login_Screen = "IS_Login_Screen";
    public static final String QB_ATTACHMENT_SUCESS = "QB_ATTACHMENT_SUCESS";
    public static final int STATUS_CODE_INVALID_USERNAME = 401;
    public static final String MANAGER = "MANAGER";
    public static final String INTERNAL_PARTICIPANT = "INTERNAL_PARTICIPANT";
    public static final String PRACTITIONER = "PRACTITIONER";
    public static final String EXTERNAL_PARTICIPANT = "EXTERNAL_PARTICIPANT";
    public static final String CUSTOM_SELECTED_LIST = "CUSTOM_SELECTED_LIST";
    public static final String IS_SQL_CIPHER_MIGRATED = "IS_SQL_CIPHER_MIGRATED";
    //  public static final String SIGN_OUT_FAILED = "Sign out failed";
    //  public static final String SIGN_OUT_ERROR = "We could not sign you out. There was an error communicating with the server.";

    public static final int GROUP_NAME_CHARACTER_LIMIT = 25;

    public static final String STATUS_CHANGED_SUCESSFULLY = "STATUS_CHANGED_SUCESSFULLY ";
    public static final String STATUS_CODE = "STATUS_CODE";
    public static final String PRESENCE_STATUS_CHANGE = "PRESENCE_STATUS_CHANGE";
    public static final String BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED = "BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED";

    public static final String PERSISTENCE_KEY = "PERSISTENCE_KEY";
    public static final String PRESENCE_TYPE = "PRESENCE_TYPE";
    public static final String PRESENCE_MESSAGE = "PRESENCE_MESSAGE";

    public static final String GET_SCREEN_VISIBLE = "GET_SCREEN_VISIBLE";
    public static final String USER_MO = "USER_MO";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String IS_REGISTED_USER = "IS_REGISTED_USER";
    public static final String DIGITIZATION_REMINDER = "DIGITIZATION_REMINDER";
    public static final String DIGITIZATION_MESSAGE_COUNT = "MESSAGE_COUNT";
    public static final String DIGITIZATION_MESSAGE_TYPE = "MESSAGE_TYPE";
    public static final String DIGITIZATION_MEDICATION_PRECRIPTION = "DIGITIZATION_MEDICATION_PRECRIPTION";
    /**
     * Attachment status for sync in the records
     */

    public static final String ALARM_NOTIFICATION_DOCUMENT_RECORD = "ALARM_NOTIFICATION_DOCUMENT_RECORD";
    public static final String ALARM_NOTIFICATION_APPOINTMENT = "ALARM_NOTIFICATION_APPOINTMENT";

    public static final String MRN_NOTIFICATION = "MRNNOTIFICATION";


    public static final String MEDICATION_DIGITIZATION_NOTIFICATION = "MEDICATION_DIGITIZATION_NOTIFICATION";
    public static final String DOCUMENT_REPORT_CANCELLED = "DOCUMENT_REPORT_CANCELLED";
    public static final String DIAGNOSTIC_ORDER = "Diagnostic Order";
    public static final String NOTIFICATION_GENRATE = "NOTIFICATION_GENRATE";
    public static final String BULK_DOC_LINKAGE_NOTIFICATION = "BULK_DOC_LINKAGE_NOTIFICATION";


    public static final String IS_ACCEPTED = "IS_ACCEPTED";
    public static final String IS_TO_SET_CONFIG = "IS_TO_SET_CONFIG";
    public static final String OTP_ALREADY_VERIFIED = "OTP_ALREADY_VERIFIED";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String MEDICATION_PRESCRIPTION_MODEL = "MEDICATION_PRESCRIPTION_MODEL";
    public static final String MEDICATION_PRESCRIPTION_AlARM_RESET = "MEDICATION_PRESCRIPTION_AlARM_RESET";
    public static final String QUANTITY = "QUANTITY";
    public static final String UNIT = "UNIT";
    public static final String MEDICIAN_NAME = "MEDICIAN_NAME";
    public static final String PRACTIONER_NAME = "PRACTIONER_NAME";
    public static final String TIMMING = "TIMMING";
    public static final String TIMMINGENUM = "TIMMINGENUM";
    public static final String NOTIFICATION_REMINDER = "NOTIFICATION_REMINDER";
    public static final int DOCUMENT_STATUS_REDDY = 0;
    public static final int DOCUMENT_STATUS_IN_PROCESS = 1;
    public static final int DOCUMENT_STATUS_DOWNLOADED = 2;
    public static final int DOCUMENT_STATUS_DIGITIZED = 3;
    public static final int DOCUMENT_STATUS_REJECTED = 4;
    public static final int DOCUMENT_STATUS_UNKNOWN = 5;
    public static final String BROADCAST_UPDATED_RECORD = "BROADCAST_UPDATED_RECORD";
    public static final String OPEN_CAMERA = "OPEN_CAMERA";
    public static final String SCREEN_UPLOAD = "SCREEN_UPLOAD";

    /**
     * OTP Screen constant variables
     */

    public static final int OTP_LENGTH = 4;
    public static final String OTP_SEND_TIME = "OTP_SEND_TIME";
    public static final String OTP_CODE = "OTP";
    public static final String IS_MOBILE_ENABLED = "Mobile enabled";
    public static final String OBSERVATION_SELECTED = "OBSERVATION_SELECTED";
    public static final String OBSERVATION_TEXT = "OBSERVATION_TEXT";
    public static final String UPLOAD_IMAGE = "UPLOAD_IMAGE";
    public static final String REFRESH_RECORDS = "REFRESH_RECORDS";
    public static final String ATTACHMENT_PERCENTAGE = "ATTACHMENT_PERCENTAGE";
    public static final String ATTACHMENT_PATH = "ATTACHMENT_PATH";

    public static final int DELETE_API_SUCCESSFUL_RESPONSE = 204;
    public static final String BROADCST_REFRESH_DATA = "BROADCST_REFRESH_DATA";

    public static final String ISEDITABLE = "IS_EDITABLE";
    public static final String IS_CHANGE_PASSWORD = "IS_CHANGE_PASSWORD";
    public static final String IS_CHANGE_EMAIL = "IS_CHANGE_EMAIL";
    public static final String THUMB_PATH_STRING = "Thumb";

    /**
     * Attachment status for sync in the records
     */
    public static final int RECORED_SYNC_STATUS_DEFAULT = 1;
    public static final int RECORED_SYNC_STATUS_CANCEL = 0;
    public static final int RECORED_SYNC_STATUS_SUBMITED = 2;
    public static final String FIRST_LOGIN = "FIRST_LOGIN";
    public static final String PASSWORD_EXPIRED = "PASSWORD_EXPIRED";

    public static final String IMAGE_THUMP_PATH = "IMAGE_THUMP_PATH";
    public static final String ITEM_SYNC_MESSAGE = "ITEM_SYNC_MESSAGE";

    public static final String HAS_VERIFIED_OTP = "HAS_VERIFIED_OTP";
    public static final String VERIFIED_USER = "VERIFIED_USER";
    public static final String REMOVE_PIC = "REMOVE_PIC";
    public static final String NEW_EMAIL = "NEW_EMAIL";
    public static final String USER_EXIST = "USER_EXIST";
    public static final String IS_SIGNUP_SCREEN_PASSED = "IS_SIGNUP_SCREEN_PASSED";
    public static final String MAIL_VERIFICATION_MSG = "A verification email has been sent to";
    public static final String TO_LOGOUT = "TO_LOGOUT";
    public static final String TITLE_BAR = "TITLE_BAR";
    public static final String WEB_URL = "WEB_URL";
    public static final String LOGIN_PAGE_TEXT = "LOGIN_PAGE_TEXT";

    public static final String SINGLE_CHAT_IDENTIFIER = "singlechat";
    public static final String MPIN_SET = "MPIN_SET";
    public static final String MSG_NOTIFICATION_ENABLED = "MSG_MUTE";
    public static final String MEDICATION_NOTIFICATION_ENABLED = "MEDICATION_MUTE";
    public static final String UPLOADS_NOTIFICATION_ENABLED = "UPLOADS_MUTE";
    public static final String EDITED_EMAIL = "EDITED_EMAIL";
    public static final String APP_TOUR_SHOWN = "APP_TOUR_SHOWN";
    public static final String SENDER_SECOUNDRY_ID = "SENDER_SECOUNDRY_ID";
    public static final String SENDER_USER_TYPE = "SENDER_USER_TYPE";
    public static final String ALARM_NOTIFICATION_MEDICATION = "ALARM_NOTIFICATION_MEDICATION";
    public static final String ALARM_READ = "ALARM_READ";
    public static final String ALARM_COUNT_UNREAD = "ALARM_COUNT_UNREAD";
    public static final String BROADCAST_REFRESH_ALARM = "BROADCAST_REFRESH_ALARM";
    public static final String NEW_PASSWORD = "NEW_PASSWORD";

    public static final String USER_KEY = "USER_KEY";
    public static final String SECONDRY_EMAIL = "SECONDRY_EMAIL";
    public static final String SECONDRY_EMAIL_STATUS = "SECONDRY_EMAIL_STATUS";
    public static final String VERIFICATION_CODE = "VERIFICATION_CODE";
    public static final String UPDATE_RESULT = "UPDATE_RESULT";

    public static final String GROUP_INVITE_SENT_FOR_SINGLE_CHAT = "GROUP_INVITE_SENT_FOR_SINGLE_CHAT";
    public static final long SPLASH_TIME_OUT = 3; //time in seconds
    public static final long SPLASH_TIME_OUT_IMMEDIATE = 1;

    public static final String BOLD = "bold";
    public static final String REGULAR = "regular";
    public static final String ITALIC = "italic";
    public static final String MEDIUM = "medium";
    public static final String LIGHT = "light";
    public static final String SEMI_BOLD = "semi";
    public static final String CRASH = "CRASH";
    public static final String EMAIL = "EMAIL";
    public static final String SIGNED_UP = "SIGNED_UP";
    public static final String REPORT_DIGITIZATION_NOTIFICATION = "REPORT_DIGITIZATION_NOTIFICATION";

    public static final String ALARM_COUNT_MEDICATION = "ALARM_COUNT_MEDICATION";
    public static final String Blood_Pressure = "BloodPressure";
    public static final String Glucose = "Glucose";
    public static final String BMI = "BMI";

    public static final String BROADCAST_GROUP_CHAT_ADD_MEMBER = "BROADCAST_GROUP_CHAT_ADD_MEMBER";
    public static final String NUMBER_OF_TIME_APPLICATION_OPEN = "NUMBER_OF_TIME_APPLICATION_OPEN";
    public static final String HAS_RATED_APP = "HAS_RATED_APP";

    public static final String LOGGED_OUT_EMAIL = "LOGGED_OUT_EMAIL";
    public static final String TIMMING_INDEX = "TIMMING_INDEX";
    public static final int DIFFERENCE_BETWEEN_START_AND_END_DATE = 2;
//    public  static final String MEDICATION_PRESCRIPTION_ID="MEDICATION_PRESCRIPTION_ID";

    public static final String ENCOUNTER_ID = "ENCOUNTER_ID";
    //    public static final String DOCUMENT_ID = "DOCUMENT_ID";
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String LAUNCH_UPLOAD_SCREEN = "LAUNCH_UPLOAD_SCREEN";
    public static final String LAUNCH_MEDICATION_SCREEN = "LAUNCH_MEDICATION_SCREEN";
    public static final String LAUNCH_RECORDS_SCREEN = "LAUNCH_RECORDS_SCREEN";

    public static final String DEVICEUID = "DEVICEUID";
    public static final String OPEN_GALLERY = "OPEN_GALLERY";
    public static final String FORGET_PASS_OTP = "FORGET_PASS_OTP";
    public static final String FORGET_PASS_OTP_SEND_TIME = "FORGET_PASS_OTP_SEND_TIME";

    public static final String PDF_FILE_PATH = "PDF_FILE_PATH";

    public static final String PDF_FILE_STATUS = "PDF_FILE_STATUS";
    public static final String MIME_TYPE = "MIME_TYPE";
    public static final int MIME_TYPE_NOTHING = -1;
    public static final int MIME_TYPE_IMAGE = 0;
    public static final int MIME_TYPE_FILE = 1;
    public static final int MIME_TYPE_DOC = 2;
    public static final int MIME_TYPE_ZIP = 3;
    public static final int MIME_TYPE_UNKNOWN = 4;
    public static final int MIME_TYPE_DOCX = 5;
    public static final int MIME_TYPE_TEXT = 6;
    public static final String REFRENCE_ID = "REFRENCE_ID";
    public static final String IS_CAMERA_INTENT = "IS_CAMERA_INTENT";

    public static final String MEDICATION_TYPE_NEW = "new";
    public static final String IS_USERMO_UPDATED = "IS_USERMO_UPDATED";

    public static final String MEDICATION_PRESCRIPTION_ID = "MEDICATION_PRESCRIPTION_ID";
    public static final String DOSE_INSTRUCTION_CODE = "DOSE_INSTRUCTION_CODE";
    public static final String DECRIPTED_FILE = "DECRIPTED_FILE";
    public static final String DOCUMENT_DOWNLOADED = "DOCUMENT_DOWNLOADED";
    public static final String DOCUMENT_DOWNLOADED_MODEL = "DOCUMENT_DOWNLOADED_MODEL";
    public static final String BROADCAST_DOCUMENT = "BROADCAST_DOCUMENT";
    public static final String ADDRESS = "ADDRESS";
    public static final String ADDRESS_INDEX = "ADDRESS_INDEX";
    public static final String BROADCAST_NO_CONV_TO_LOAD = "BROADCAST_NO_CONV_TO_LOAD";
    public static final String TIMESTAMP = "TIMESTAMP";
    public final static String ORGANISATION_ID = "ORGANISATION_ID";
    public final static String OTHER = "Other";
    public final static String ERROR_MESSAGE = "ERROR_MESSAGE";
    public final static String VACCINATION_PROFILE = "VACCINATION_PROFILE";
    public final static String VACCINATION_PROFILE_DOB = "VACCINATION_PROFILE_DOB";
    public final static String IS_VACCINATION_PARSED = "IS_VACCINATION_PARSED";
    public final static String VACCINATION_MODEL = "VACCINATION_MODEL";

    public static final String DEEP_LINK_FOR_RECORDS_CLICKED = "DEEP_LINK_FOR_RECORDS_CLICKED";
    public static final String DEEP_LINK_FOR_MEDICATION_CLICKED = "DEEP_LINK_FOR_MEDICATION_CLICKED";
    public static final String ADMIN_CREATED_PASSWORD_NEVER_CHANGED = "adminCreatedPasswordNeverChanged";
    public static final String DEFAULT_PASSWORD_TYPE = "DEFAULT_PASSWORD_TYPE";
    public static final String START_INSIDE_ACTIVITY = "START_INSIDE_ACTIVITY";

    public static final String TIME_ZONE_CHANGED = "android.intent.action.TIMEZONE_CHANGED";
    public static final String TIME_CHANGED = "android.intent.action.TIME_CHANGED";

    public static final int GROUP_MEMBER_SIZE_LIMIT = 50;
    public static final String DOCUMENT_TYPE = "DOCUMENT TYPE";

    public static final String LAUNCH_BLOCK_VITAL_SCREEN = "LAUNCH_BLOCK_VITAL_SCREEN";
    public static final String LAUNCH_DOCUMENTS = "DOCUMENTS";
    public static final String LOGOUT_IF_AUTH_TOKEN_NULL = "LOGOUT_IF_AUTH_TOKEN_NULL";

    public static final String INTERVAL = "Interval";
    public static final String REPEAT_TIME = "Repeat Time";
    public static final String END_DATE = "End Date";
    public static final String START_DATE = "Start Date";
    public static final String REPORT_ID = "REPORT_ID";

    public static final int FRAGMENT_SETTINGS = 2004;
    public static final String RELOAD_SETTINGS = "RELOAD_SETTINGS";
    public static final int RESULT_ORDERDATE_ASC = 0;
    public static final int RESULT_ORDERDATE_DESC = 1;
    public static final int RESULT_LAST_UPDATED_ASC = 2;
    public static final int RESULT_LAST_UPDATED_DESC = 3;

    public static final int BTG_SESSION_TIMOUT = 5;
    public static final String FROM_PATIENT_PROFILE_VIEW = "FROM_PATIENT_PROFILE_VIEW";
}
