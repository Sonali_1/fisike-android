package com.mphrx.fisike.constant;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.mphrx.fisike_physician.utils.AppLog;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by xmb2nc on 29-12-2015.
 */
public class SharedPreferencesConstant {

    public static final String RECORD_SYNC_TIME = "RECORD_SYNC_TIME";
    public static final String ALL_RECORDS_LOADED = "ALL_RECORDS_LOADED";
    public static final String TOTAL_RECORD = "TOTAL_RECORD";
    public static final String RECORD_PAGE_NUMBER = "RECORD_PAGE_NUMBER";
    public static final String TIME_FOR_LOAD_RECORDS = "TIME_FOR_LOAD_RECORDS";
    public static final String SYNC_COMPLETE_AFTER_DIGITIZATION = "SYNC_COMPLETE_AFTER_DIGITIZATION";
    private static final String LOGIN_ATTEMPTS = "LOGIN_ATTEMPTS";


    public static final String MEDICATION_LAST_SYNC_TIME_SERVER = "MEDICATION_LAST_SYNC_TIME_SERVER";
    public static final String ALL_MEDICATION_ORDER_LOADED = "ALL_MEDICATION_ORDER_LOADED";
    public static final String MEDICATION_ORDER_TOTAL_COUNT = "MEDICATION_ORDER_TOTAL_COUNT";
    public static final String MEDICATION_SYNC_TIME_LOCAL = "MEDICATION_SYNC_TIME";
    public static final String SYNC_COMPLETE_AFTER_DIGITIZATION_MEDICATION = "SYNC_COMPLETE_AFTER_DIGITIZATION_MEDICATION";
    public static final String UPLOAD_LAST_SYNC_TIME_SERVER = "UPLOAD_LAST_SYNC_TIME_SERVER ";
    public static final String UPLOAD_SYNC_TIME_LOCAL = "UPLOAD_SYNC_TIME_LOCAL";
    public static final String UPLOAD_TOTAL_COUNT = "UPLOAD_TOTAL_COUNT";
    public static final String ALL_UPLOADED_LOADED = "ALL_UPLOADED_LOADED";


    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor mPreferenceEditor;

    private static Map<String, Set<SharedPreferences.OnSharedPreferenceChangeListener>> mListeners;

    public static void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE);
        mPreferenceEditor = mSharedPreferences.edit();
        mPreferenceEditor.apply();
        mListeners = new ConcurrentHashMap<>();
    }

    public static void register(String preferenceKey, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (mListeners.containsKey(preferenceKey)) {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(preferenceKey);
            listeners.add(listener);
        } else {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = new HashSet<>();
            listeners.add(listener);
            mListeners.put(preferenceKey, listeners);
        }
    }

    public static void unregister(String preferenceKey, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (mListeners.containsKey(preferenceKey)) {
            Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(preferenceKey);
            listeners.remove(listener);
            if (listeners.size() == 0) {
                mListeners.remove(preferenceKey);
            }
        } else {
            AppLog.showError(SharedPreferencesConstant.class.getSimpleName(), "Can't find the listener to unregister");
        }
    }

    public static void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (mListeners.containsKey(key)) {
                Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(key);
                for (SharedPreferences.OnSharedPreferenceChangeListener listener : listeners) {
                    listener.onSharedPreferenceChanged(sharedPreferences, key);
                }
            }
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (mListeners.containsKey(key)) {
                        Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners = mListeners.get(key);
                        for (SharedPreferences.OnSharedPreferenceChangeListener listener : listeners) {
                            listener.onSharedPreferenceChanged(sharedPreferences, key);
                        }
                    }
                }
            });
        }
    }

    private static void apply() {
        mPreferenceEditor.apply();
    }

    private static void updatePreferences(String key, String value) {
        if (value == null || value.isEmpty()) {
            mPreferenceEditor.remove(key);
        } else {
            mPreferenceEditor.putString(key, value);
        }
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    public static void updatePreferences(String key, boolean value) {
        mPreferenceEditor.putBoolean(key, value);
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    public static void updatePreferences(String key, long value) {
        mPreferenceEditor.putLong(key, value);
        apply();
        onSharedPreferenceChanged(mSharedPreferences, key);
        AppLog.showInfo("Update Preferences", "Key = " + key + " Value = " + value);
    }

    public static void setLoginAttempts(int value) {
        updatePreferences(LOGIN_ATTEMPTS, value);
    }

    public static int getLoginAttempts() {
        return mSharedPreferences.getInt(LOGIN_ATTEMPTS, 0);
    }


}
