package com.mphrx.fisike.constant;

/**
 * Created by laxmansingh on 4/6/2017.
 */

public class MyHealthConstants {


    public static final String SYSTOLIC = "Systolic";
    public static final String DIASTOLIC = "Diastolic";
    public static final String RANDOM = "Random";
    public static final String FASTING = "Fasting";
    public static final String AFTERMEAL = "After Meal";
    public static final String WEIGHT = "Weight";
    public static final String HEIGHT = "Height";


}
