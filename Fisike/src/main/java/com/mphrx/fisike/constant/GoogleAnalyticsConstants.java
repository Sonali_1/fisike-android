package com.mphrx.fisike.constant;

/**
 * Created by Aastha on 22/12/2015.
 */
public interface GoogleAnalyticsConstants {

    public static final String MY_HEALTH_FRAGMENT = "My Health Fragment";
    public static final String CHAT_FRAGMENT = "Chat Fragment";
    public static final String NOTIFICATION_CENTRE_FRAGMENT = "Notification Fragment";
    public static final String SETTING_FRAGMENT = "Setting Fragment";
    public static final String MEDICATION_FRAGMENT = "Medication Fragment";
    public static final String UPLOADS_FRAGMENT = "Uploads Fragment";
    public static final String VACCINATION_FRAGMENT = "Vaccination Fragment";
    public static final String BY_DATE = "Schedule vaccination by date";
    public static final String BY_VACCINATION = "Schedule vaccination by vaccination";

    public static final String VITALS_API_NAME="getVitalsConfig";
    public static final String VITALS_EVENT_CATEGORY="API";
    public static final String VITALS_EVENT_ACTION="API FAILURE";
    public static final String VITALS_EVENT_ACTION_SUCCESS="API SUCCESS";
    public static final long VITALS_VALUE_SUCCESS=0;
    public static final long VITALS_VALUE_FAILURE=0;
    public static final long VITALS_VALUE_SUCCESS_INCORRECT_JSON=1;

    public static final String RESULT_SEARCH_FRAGMENT = "Result Serch Fragment";
    public static final String RESULT_LOOKUP_FRAGMENT = "RESULT_LOOKUP_FRAGMENT";
}
