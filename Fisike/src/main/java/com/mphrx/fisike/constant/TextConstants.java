package com.mphrx.fisike.constant;


import com.mphrx.fisike.utils.DateTimeUtil;

public interface TextConstants {

    /**
     * Login errors
     */
    public static final String SERVICE_UNAVAILABLE = "Unable to contact server\nServer Error. Please try again later";
    public static final String INVALID_LOGIN = "Incorrect Password. Please try again.";
    public static final String UNEXPECTED_ERROR = "Something went wrong. Please try again.";
    /**
     * User details api call
     */
    public static final String SUCESSFULL_USERDETAILS_CALL = "Sucessfull userdetails call";

    /**
     * Common text constants
     */
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final String LOADING_CONTACTS = "Loading Contacts";
    public static final String CANCEL = "Cancel";


    public static final int MAX_PHONENOENTER_LENGTH = 16;
    public static final int INDIA_COUNTRYCODE = 91;
    public static final String IN_COUNTRYNAME = "IN";
    public static final int US_COUNTRYCODE = 1;
    public static final String US_COUNTRYNAME = "US";
    public static final boolean DEFAULT_COUNTRY_INDIA = false;
    public static final int MAX_PHONENO_LENGTH = 16;
    public static final int MIN_PHONENO_LENGTH = 6;
    public static final int INDIA_PHONENO_LENGTH = 10;


    public static final String SPECIALITY = "Speciality";
    public static final String DESIGNATION = "Designation";
    public static final String ABOUT_ME = "About Me";
    public static final String SETTING_EDIT = "Edit";
    public static final String SETTING_UPDATE = "Update";

    public static final String NAME = "Name";
    public static final String EMAIL = "Email Address";
    public static final String DEVICE_ACTIVATION_DATE = "Device activation Date";
    public static final String GROUP_NAME = "Group Name";
    public static final String VERSION = "Version";


    /**
     * Activities
     */
    public static final String LOCK_SCREEN = "com.mphrx.fisike.lock_screen.LockScreenActivity";
    public static final String LOGIN_SCREEN = "com.mphrx.fisike_physician.activity.LoginActivity";
    public static final String UPDATE_PIN = "com.mphrx.fisike.lock_screen.SetUpdateMPIN";
    public static final String LIST_ACTIVITY = ".ListPersonsActivity";
    public static final String OTP_SCREEN = "com.mphrx.fisike_physician.activity.OtpActivity";
    public static final String REGIONALBLOCK_SCREEN = "com.mphrx.fisike.regionalBlock.RegionalBlockActivity";
    public static final String SEARCH_PATIENT_SCREEN = "searchpatient.activity.SearchPatientActivity";

    /**
     * TODO replace contains to JSON parsing
     */
    public static final String ERROR_INVALID_USER_NAME = "Oops! Invalid User Credentials";

    /**
     * Login Setting check not needed
     */
    public static final String ERROR_INVALID_NUMBER = "Invalid input error";

    /**
     * Attachment status type
     */
    public static final String ATTACHMENT_STATUS_UPLOADED = "ATTACHMENT_STATUS_UPLOADED";
    public static final String ATTACHMENT_STATUS_WAITING = "ATTACHMENT_STATUS_WAITING";
    public static final String ATTACHMENT_STATUS_IN_PROGRESS = "ATTACHMENT_STATUS_IN_PROGRESS";
    public static final String ATTACHMENT_STATUS_NOT_UPLOADED = "ATTACHMENT_STATUS_NOT_UPLOADED";
    public static final String ATTACHMENT_STATUS_RECEIVED = "ATTACHMENT_STATUS_RECEIVED";// Attachment received but not downloaded
    public static final String ATTACHMENT_STATUS_DOWNLOADING = "ATTACHMENT_STATUS_DOWNLOADING";// Attachment download in process
    public static final String ATTACHMENT_STATUS_DOWNLOADED = "ATTACHMENT_STATUS_DOWNLOADED";// Attachment file downloaded

    /**
     * Feedback screen messsages
     */
    public static final String FEEDBACK_SCREEN = "FEEDBACK_SCREEN";
    public static final String LIKE = "LIKE";
    public static final String DISLIKE = "DISLIKE";
    public static final String RECENT_CAHAT_SCREEN = "RecentChatScreen";
    public static final String SETTING_SCREEN = "SettingScreen";
    public static final String MESSAGE_SCREEN = "MessageScreen";
    public static final String ADD_CONTACT_SCREEN = "AddContactScreen";
    public static final String CONTACT_DETAIL_SCREEN = "ContactDetailScreen";

    public static final String UPDATE_SUCESSFULLY = "Profile sucessfully updated.";
    public static final String UPDATE_ERROR = "Error in updating profile. Please try again later";
    public static final String SERVER_ERROR_PROFILE = "Server error. Please try again later";
    public static final String CHAT_DETAIL = "CHAT_DETAIL";
    public static final String IS_RECENT_CHAT = "IS_RECENT_CHAT";
    public static final String INVITE_SCREEN = "InviteScreen";
    public static final int btnChat = 1001;
    public static final String USER_API_TIME = "USER_API_TIME";
    public static final String CUSTOMSELECT_API_TIME = "CUSTOMSELECT_API_TIME";
    public static final String WARNING = "Warning!!!";
    public static final String FAILURE_SAME_EMAIL = "Sorry, no selfies :-)";
    public static final String MANAGER_CONTENT_DESCRIPTION = "Managers can invite other clinical users, including external users. Managers can also invite patients. As a manager, this person can chat with any other clinical user or patient in your group.";
    public static final String PRACTITIONER_CONTENT_DESCRIPTION = "Practitioners can invite patients but not other clinical users. Practitioners can chat with any other user or patient in your fisike group.";
    public static final String INTERNAL_PARTICIPANT_CONTENT_DESCRIPTION = "Internal Participants can not invite other users, but they can chat with any user or patient in your fisike group.";
    public static final String EXTERNAL_PARTICIPANT_CONTENT_DESCRIPTION = "External Participants can not invite any other users or patients. They can chat with only other clinical users or with patients that they have explicitly been granted access to.";
    public static final String PATIENT_CONTENT_DESCRIPTION = "If your organization has enabled access for patients, then you can invite patients to Fisike as well. Patients have limited access to only their data, and can communicate with specific clinical users. Patients cannot invite other users or see or communicate with other patients.";
    public static final String OPTIONAL_TYPE = "Optional";

    /**
     *
     */
    public static final String ACTIVITY_STATUS_COMPLETED = "ACTIVITY_STATUS_COMPLETED";
    public static final String ACTIVITY_STATUS_PARTIAL = "ACTIVITY_STATUS_PARTIAL";
    public static final String ACTIVITY_STATUS_NOT_STARTED = "ACTIVITY_STATUS_NOT_STARTED";
    public static final String ACTIVITY_STATUS_COMPLETE_NOT_SUBMITTED = "ACTIVITY_STATUS_COMPLETE_NOT_SUBMITTED";

    public static final String ACTIVITY_RECEIVED = "Received";
    public static final String ACTIVITY_VIEW = "View";
    public static final String MESSAGE_TEXT = "Message_Text";
    public static final String SUBMIT = "Submit";
    public static final String NEXT = "Next";
    public static final String READDY = "RETRY";
    public static final String START = "Start";

    public static final String QB_SEND_MESSAGE = "Please answer these questions";
    public static final String CONPLETED_ACTIVITY_TEXT_BODY = "I've completed the activity";

    public static final String CONNECTION_ERROR_MESSAGE = "You are currently offline. Please try again once you are connected to internet.";
    public static final String CONNECTION_ERROR_TITLE = "Connection error!";
    public static final CharSequence ROLE_PATIENT = "ROLE_PATIENT";
    public static final String QB_START_PAGE = "QuestionBuilderStartScreen";
    public static final String QB_QUESTION_PAGE = "QuestionBuilderQuestionAnswerScreen";
    public static final String QB_FINISH_PAGE = "QuestionBuilderDismissScreen";
    public static final String QB_RESPONSE_SCREEN = "QuestionBuilderResponseScreen";
    public static final String ACTIVITY_SHEET = "Activity Sheet";
    public static final String IS_SUBCRIPTION_SEND = "IS_SUBCRIPTION_SEND";
    public static final String PATIENT = "Patient";
    public static final String PATIENT_CAPS = "PATIENT";
    // public static final String FAIL_TO_UPLOAD_IMAGE = "Fail to upload profile pic. Please try again.";

    /**
     * Attachment send and received constants
     */
    public static final String ATTACHMENT_ID = "ATTACHMENT_ID";
    public static final String ATTACHMENT_ACTVITY = "Activity";
    public static final String ATTACHMENT_IMAGE_MESSAGE = "Image attached";
    public static final String ATTACHMENT_VIDEO_MESSAGE = "Video attached";
    public static final String ATTACHMENT_TEXT = "Text";
    public static final String ATTACHMENT_STATUS_TEXT_READDY = "Retry";
    public static final String ATTACHMENT_STATUS_TEXT_SIZE = "Size";
    public static final String BROADCAST_ATTACHMENT_STATUS_CHANGED = "BROADCAST_ATTACHMENT_STATUS_CHANGED";
    public static final String ATTACHMENT_IS_TO_REFRESH_VIEW = "ATTACHMENT_IS_TO_REFRESH_VIEW";
    public static final String BROADCAST_ATTACHMENT_DOWNLOAD_STATUS_CHANGED = "BROADCAST_ATTACHMENT_DOWNLOAD_STATUS_CHANGED";
    public static final String IS_CAMERA_INTENT = "IS_CAMERA_INTENT";
    public static final String DOWNLOAD_PERCENTAGE = "DOWNLOAD_PERCENTAGE";
    public static final String ATTACHMENT_QUESTIONARY_RESULT = "ATTACHMENT_QUESTIONARY_RESULT";
    public static final String API_ERROR = "API_ERROR";
    public static final int STATUS_AVAILABLE = 0;
    public static final int STATUS_BUSY = 1;
    public static final int STATUS_UNAVAILABLE = 2;
    public static final String STATUS_AVAILABLE_TEXT = "Available";
    public static final String STATUS_UNAVAILABLE_TEXT = "Unavailable";
    public static final String STATUS_BUSY_TEXT = "Busy";

    public static final int SELECT_PHOTO = 1;
    public static final int CROP_RESULT = 2;
    public static final int GALLERY_RESULT = 4;
    public static final int CAMERA_RESULT = 5;
    public static final int INVITE_RESULT = 3;
    public static final String REMOVE_PIC = "REMOVE_PIC";

    public static final int CAMERA_IMAGE_INTENT = 0;
    public static final int GALLERY_IMAGE_INTENT = 1;
    public static final int MEDICINE_FREQUNCY_INTET = 1008;
    public static final int MEDICINE_SEARCH_INTENT = 1009;
    public static final int MEDICAL_CONDITIONS_SEARCH_INTET = 1110;

    public static final int EDIT_TEXT_HINT_TEXT_SIZE = 16;
    public static final int EDIT_TEXT_SIZE = 20;


    public static final String STATUS_RESPONSE = "STATUS_RESPONCE";
    public static final int MAX_RETRY = 2;
    public static final int MENU_ITEM_NEW_GROUP = 0;
    public static final int MENU_ITEM_SETTINGS = 1;

    public static final int MENU_ITEM_LOGOUT = 2;
    public static final int MENU_ITEM_PROFILE = 3;
    public static final int MENU_ITEM_FILTERS = 4;
    public static final int MENU_ITEM_SEARCH = 5;

    public static final int MENU_ITEM_PROFILE_UPDATE = 1;
    public static final int MENU_ITEM_DISCLAMER = 2;


    /***
     * OTP Screen error and messages
     **/

    //	public static final int patientId = 115784;//test
//	public static final int patientId = 116242;//demo
    public static final int OTP_EXPIRE_TIME = 30;  //otp expire time in minutes

    public static final int OTP_MATCHED = 0;
    public static final int OTP_EXPIRED = 1;
    public static final int OTP_NOT_MATCHED = 2;
    public static final String RESEND = "RESEND";
    public static final int OTP_AUTOMATIC_VERIFICATION_TIME = 30;//otp sms automatic verification time in seconds
    //   public static final String OTP_API_FAILURE = "Something went wrong.Please try again.";


    /*************************************
     * SIGNUP SCREEN
     **************************************/


    public static int SIGNUP_IMAGE_WIDTH = 107;

    public static int SIGNUP_IMAGE_HEIGHT = 107;

    //public static String DONE = "Done";
    public static String MISMATCH_OLD_PASS = "Invalid password , Please enter a valid password.";
    public static String CONFIRM_EXIT_WITHOUT_SAVE_MSG = "Would you like to exit without saving?";
    public static final String UNAUTHORIZED_ACCESS = "Unauthorized access";
    //   public static final String FETCHING_STATUS = "Verifying your email account";
    public static final String SENDING_OTP_STATUS = "Hang on! Sending OTP.";

    public static final int CHANGE_EMAIL_VERIFICATION_TIME = 15;//time in minutes to get changes email status
    public static String SIGNUP_EMAIL_SENT_MESSAGE = "An email is sent to validate your email ID. Please check your email and click on validate button. \n ";
    public static int SIGNUP_MAIL_VERIFICATION_TIME = 2;//time in minutes to verify email
    public static String OBJECT_TYPE = "mobile";
    public static final CharSequence AWAITING_VERIFICATION_TEXT = "Awaiting email verification to allow Sign In.";
    public static final CharSequence UNABLE_TO_LOAD = "Unable to Load.Please try after sometime.";
    public static final String dd_mmm_yyyy = "dd MMM yyyy";
    public static final int NAVIGATION_DRAWER_IMAGE_HEIGHT = 40;
    public static final int NAVIGATION_DRAWER_IMAGE_WIDTH = 40;
    public static final String NOTIFICATIONS = "Notifications";
    public static final String SUCESSFULL_API_CALL = "SUCESSFULL_API_CALL";
    public static final String SHARE_APP = "Share App";
    public static final String NOTHING_UPDATED_TEXT = "Seems like you didn't update anything";
    public static final String AM_DONE = "Am done";
    public static final String EDIT_AGAIN = "Edit again";
    public static final String mPIN = "mPIN";
    public static final String PHYSICIAN_EXCEPTION = "You are not a Patient user.Please sign In using Patients credential.";
    public static final String HOURS_3 = "3 HOURS";
    public static final String HOURS_8 = "8 HOURS";
    public static final String DAY_1 = "1 DAY";
    public static final String YEAR_1 = "1 YEAR";
    public static final String WEEK_1 = "1 WEEK";

    public static int NUM_PAGES_APP_TOUR = 4;
    public static final int GROUP_NOT_CREATED = 0;
    public static final int GROUP_CREATED = 1;
    public static final int MEMBER_INVITATION_SENT = 2;
    public static final String PHONE_NO_NOT_UNIQUE = "PHONE_NO_NOT_UNIQUE";
    public static final String EMAIL_NOT_UNIQUE = "EMAIL_NOT_UNIQUE";
    public static final String VALID_PHONE_NUMBER = "VALID_PHONE_NUMBER";
    public static final String SENDER_ID_PATIENT = "27513946436";
    public static final String SENDER_ID_PHYSICIAN = "397522795587";
    public static final String OS_TYPE = "android";
    public static final String DEVICE_TYPE = "android";
    public static final String APP_VERSION = "0.1";

    public static final String UNDERSCORE = "_";
    public static final int PERMISSION_REQUEST_CODE = 1;

    public static final String NON_DICOM = "NON_DICOM";
    public static String USER_REVOKED = "USER_IS_REVOKED";
    public static String Forever = "Forever";
    public static final int PERMISSION_REQUEST_CODE_SINGLE_REQUEST = 2;
    public static final int PERMISSION_STORAGE_CODE = 3;
    public static final int FORGET_PASS_SCREEN_TIME = 30; //time in minutes for forgetPassword screen
    public static final String RESET_PASS = "RESET_PASS";
    public static final char[] passwordAllowedChars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '@', '.', '_', '#', '$', '%', '&', '*', '-', '+', '(', ')', '!', '"', '\'',
            ';', '/', '?', ',', '|', '\\', '^', '<', '>', '{', '}', '[', ']', '=', '.'};

    public static final char[] alphaNumericAllowedChars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    public static final char[] mobilenoAllowedChars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-'};

    public static final String EIGHT_AM = "8";
    public static final String TEN_AM = "10";
    public static final String ELEVEN_AM = "11";
    public static final String TWELVE_PM = "12";
    public static final String ONE_PM = "13";
    public static final String THREE_PM = "15";
    public static final String FOUR_PM = "16";
    public static final String FIVE_PM = "17";
    public static final String SIX_PM = "18";
    public static final String TWO_PM = "14";
    public static final String EIGHT_PM = "20";

    public static final String ONE = "1";
    public static final String CODE_BMI = "39156-5";
    public static final String CODE_SYSTOLIC = "8480-6";
    public static final String CODE_DIASTOLIC = "8462-4";
    public static final String CODE_GLUCOSE_FASTING = "1558-6";
    public static final String CODE_GLUCOSE_RANDOM = "2345-7";
    public static final String CODE_GLUCOSE_AFTERMEAL = "1521-4";
    public static final String CODE_HEIGHT = "8302-2";
    public static final String CODE_WEIGHT = "3141-9";
    public static final String CODE_HEART_RATE = "8867-4 ";
    public static final String CODE_RESPIRATORY_RATE = "9279-1";
    public static final String CODE_TEMPERATURE = "8310-5";
    public static final String SOS = "0";
    public static final String USERNAME_NOT_UNIQUE = "USERNAME_NOT_UNIQUE";

    public static final String OTP_TIME_EXPIRED = "OTP_TIME_EXPIRED";
    public static final String OTP_NOT_FOUND = "OTP_NOT_FOUND";
    public static final String DISABLE_USER = "disableAccount";
    public static final String SUCCESSFUL = "SUCCESSFUL";

    //handling errors in "USER_DEVICE_TOKEN_NOT_FOUND"s
    public static final String HTTP_STATUS_401 = "401";
    public static final String HTTP_STATUS_L01 = "L01";
    public static final String HTTP_STATUS_423 = "423";
    public static final String HTTP_STATUS_422 = "422";
    public static final String HTTP_STATUS_427 = "427";
    public static final String HTTP_STATUS_424 = "424";
    public static final String HTTP_STATUS_425 = "425";
    public static final String HTTP_STATUS_426 = "426";
    public static final String HTTP_STATUS_428 = "428";
    public static final String HTTP_STATUS_UV15 = "UV15";
    public static final String HTTP_STATUS_UV09 = "UV09";
    public static final String HTTP_STATUS_UV14 = "UV14";
    public static final String STATUS_CODE_460 = "460";
    public static final String STATUS_CODE_461 = "461";
    public static final String STATUS_CODE_IN02 = "IN02";
    public static final String USER_DEVICE_TOKEN_NOT_FOUND = "USER_DEVICE_TOKEN_NOT_FOUND";

    public static final int USER_INTERACTION_TIME = 30; //Time to measure inactivity of user in minutes
    public static final String PHYSICIAN = "PRACTITIONER";


    public static final String MULTIPLE_USERS_FOUND = "MULTIPLE_USERS_FOUND";

    public static final String ALTERNATE_TYPE_PHONE = "Mobile";
    public static final String ALTERNATE_TYPE_EMAIL = "email";


    public static int ALTER_CONTACT_DAYS_DIFF = 3;
    public static int SESSION_DIFF = 1;


    public static final String FROM_CAMERA = "camera";
    public static final String FROM_GALLERY = "gallery";


    public static final String ACTIVE_DEVICE_AND_ALTERNATE_CONTACT_NOT_FOUND = "ACTIVE_DEVICE_AND_ALTERNATE_CONTACT_NOT_FOUND";

    public static final int ADD_MEDICINE = 1990;
    public static final int UPDATE_MEDICINE = 1990;
    /*[     quest constannts      ]*/

    public static final int INTENT_CONSTANT = 1008;
    public static final int REFERESH_CONSTANT = 1009;
    public static final int REBOOK_CONSTANT = 1100;


    public static final String FINISH_ACTIVITY = "finish_activity";
    public static final String REFRESH_SMALLCASE = "refresh";
    public static final String BOTH = "both";
    public static final String UPCOMING = "Upcoming";
    public static final String RECENT = "Recent";
    public static final String SINGLE = "Single";

    public static final String SCHEDULE = "schedule";
    public static final String SLOT = "slot";


    public static final String RESCHEDULE = "reschedule";
    public static final String CANCEL_TEXT = "cancel";
    public static final String REBOOK = "Rebook";

    public static final String FROM = "from";
    public static final String TEST_TYPE = "Test Type";
    public static final String HOME_SAMPLE = "Home Sample Collection";
    public static final String LAB_TEST = "Lab Tests";
    public static final String KEY = "KEY";
    public static final String SERVICES = "Services";
    public static final String FROM_APPOINTMENT = "appointment";
    public static final String FROM_SERVICES = "services";


    public static final String TYPE_DOCTOR = "DOCTOR";
    public static final String TYPE_SPECIALITY = "SPECIALITY";
    public static final String TYPE_TESTS = "TESTS";
    public static final String TYPE_HSC = "HSC";

    public static final String DOCTORS = "Doctors";


    /*[     End of quest constannts      ]*/

    //MPIN SCREEN ERRORS
    public static final int PIN_LOCK_TIME = 0;


    // Deep link url
    public static final String DEEP_LINK_PATH = "/webconnect/mobile.fisikeForPatient.html";


    int TIME_TO_DISPLAY_DRAWER_DEMO = 3;//Time in seconds to display Drawer for first time app install
    public static final String IS_SECOND_LEVEL = "Is second level";
    public static final int SELECTTIME_REQUESTCODE = 1010;
    public static final int OTP_SENT_ON_EMAIL_AND_MOBILE = 1;
    public static final int OTP_SENT_ON_MOBILE_ONLY = 0;
    public static final int OTP_SENT_ON_EMAIL_ONLY = -1;

    public static final String REASON_BOOK = "AppointmentBooking";
    public static final String REASON_CANCEL = "AppointmentCancel";
    public static final String REASON_RESCHEDULE = "AppointmentReschedule";
    public static final String CHARACTER_SET_CAPITAL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String CHARACTER_SET_SMALL = "abcdefghijklmnopqrstuvwxyz";
    public static final String CHARACTER_SET_DIGITS = "0123456789";

    public static final int NUM_OF_COLS = 2;
    public static final String USER_PASSWORD_INCORRECT_LENGTH = "USER_PASSWORD_INCORRECT_LENGTH";
    public static final String USER_PASSWORD_INCORRECT_PATTERN = "USER_PASSWORD_INCORRECT_PATTERN";
    public static final String OLD_PASSWORD_INVALID = "OLD_PASSWORD_INVALID";
    public static final String USER_PASSWORD_SAME = "USER_PASSWORD_SAME";
    public static final String NEW_PASSWORD_INVALID = "NEW_PASSWORD_INVAID";
    public static final String ROLE_CARE_COORDINATOR = "ROLE_CARE_COORDINATOR";
    public static final String ROLE_CARE_MANAGER = "ROLE_CARE_MANAGER";


    //*Configs for DO,DR status - FIS-9170
    public static final String STATUS_FINAL = "Final";
    public static final String STATUS_PENDING = "Pending";
    public static final String DO_STATUS = "status";
    public static final String DR_STATUS = "reportStatus";

    public static final int READ_MEDICATION = 113;
    public static final String MEDICATION_READ_MODE = "medicationReadMode";
    public static final String CHILD_OFFERING_MODEL = "childOfferringModel";
    public static final String SPECIALITY_DOCTORS = "Doctors";
    public static final String SPECIALITY_SERVICES = "Services";

    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String COUNTRY = "country";

    public static final String BARRED_LOCATION_ERROR_CODE = "402";
    public static final String REGIONAL_BLOCK = "regional_block";

    public static final String PREFILL_NUM = "PREFILL_NUM";
    public static final String MARKET_URL = "market://details?id=";
    public static final String CHANGE_NUMBER = "CHANGE_NUMBER";
    public static final String LOGIN_FLOW = "LOGIN_FLOW";
    public static final String LAUNCH_MODE = "LAUNCH_MODE";
    public static final String OTP_INITIALIZATION = "OTP_INITIALIZATION";
    public static final String OTP_VERIFICATION = "OTP_VERIFICATION";

    public static String search_category_status = "status";
    public static String search_param = "param";
    public static String param_value = "param_value";
    public static final String PATIENT_MODEL = "PATIENT_MODEL";
    public static final String ERROR = "Error";
    public static final String REFRESH = "Refresh";
    public static final String LAUNCHED_FROM_GROUP_DETAIL_ACTIVITY = "launchedFromGroupDetail";
    public static final String MEDICATION_PRESCRIPTION_INTENT = "medicationPrescriptionIntent";


    public static final String GROUP_CHAT_IDENTIFIER = "groupchat_";
    public static final String TITLE = "title";
    public static String PATIENT_ID = "patientId";

    public static final String NEW_GROUP_NAME_INTENT = "groupName";
    public static final String NEW_GROUP_IMAGE_INTENT = "groupProfilePic";
    public static final String SELECTED_INVITE_INTENT = "selectedInvite";

    public static final String IS_REPORT = "IS_REPORT";

    public static final String BASE_URL = "app.fisike.com:443/fisike/userApi/getUserProfilePicture?";
    public static final String DISEASE_SEARCH_URL_STRING = "/patientCondition/search";
    public static final String MEDICATION_SEARCH_URL_STRING = "/medication/searchMedication";
    public static final String MEDICATION_PRESCRIPTION_DELETE = "/medicationOrder/deleteWithEncounter";
    public static final String GENDER = "Gender";

    public static final String CHAT = "Chat";
    public static final String GROUP_CHAT = "Group Chat";
    public static final String FRAGMENT_KEY = "FRAGMENT_KEY";


    public final String NORMAL = "Normal";
    public final String OUT_OF_RANGE = "Out Of Range";
    public final String ELEVATED = "Elevated";
    public final String LOW = "Low";

    public static final String CHANGE_EMAIL = "CHANGE_EMAIL";

    public static final String USER_EXISTED = "USER_EXISTED";

    public static final String TRANSACTION_ID = "transaction_id";
    public static final String CACHE_RESPONSE = "cache_response";

    public static final String KG_SPINNER = "KG_SPINNER";
    public static final String LBS_SPINNER = "LBS_SPINNER";

    public static final String USER_TYPE = "user_type";
    public static final String ALL_CONTACT = "all_contact";
    public static final String PATIENT_CONTACT = "patient_contact";
    public static final String OTHER_CONTACT = "other_contact";
    public static final String BACK_PRESSED = "BACK_PRESSED";
    public static final String EXPERIENCE = "EXPERIENCE";
    public static final String MICROBIOLOGY_REPORT_TYPE = "Microbiology";
    public static final String BMI_SEPARATOR = "^";
    public static final String BMI_KGM = "kg/m2";
    public static final String BMI_KGM2 = "kg/m^2";
    public static final String OBSERVATION_STATUS_CANCELLED = "Cancelled";


    public static final String NH_HELP_LINE_NO = "180042504250";


    public static final String FROM_LOGIN = "fromLogin";
    public static final String FROM_SUPPORT = "fromSupport";
    public static final String STATUS_ACCEPTED = "ACCEPTED";
    public static final String STATUS_DECLINED = "REJECTED";
    public static final String READ_DATE = "readDate";
    public static final String TEXT = "text";
    public static final String NAME_AGREEMENT = "name";
    //TODO - Text to be taken
    public static final String INDEXES = "INDEXES";
    public static final String IS_AGREEMENTS_UPDATED = "Agreement Updated";

    public static final String AM = "AM";
    public static final String PM = "PM";


    public static final String BLOOD_PRESSURE = "Blood Pressure";
    public static final String SYSTOLIC = "Systolic";
    public static final String DIASTOLIC = "Diastolic";
    public static final String GLUCOSE = "Glucose";

    public static final String DATE_TEXT = "date";
    public static final String TIME_TEXT = "time";
    public static final String DEFALUT_LANGUAGE_TITLE = "English";
    public static final String DEFALUT_LANGUAGE_KEY = "en";
    public static int NO_OF_UPCOMING_RECENT_ELEMENTS_TO_SHOW = 2;
    public static final String PHLOOBER_HELP_LINE_NO = "180042504250";
    public static final String MEDICARE = "medicare";
    public static final String FREE_SLOT_TEXT = "free";

    public static final String SCRIPT_ORIGINAL = "ORIGINAL";
    public static final String SCRIPT_THUMBNAIL = "THUMBNAIL";
    public static final String LOGIN_ID = "LOGIN_ID";
    public static final String SELF_SIGN_UP_MOBILE = "SELF_SIGN_UP_MOBILE";
    public static final String FORGOT_PASSWORD = "FORGOT_PASSWORD";
    public static final String FROM_SIGNUP = "SIGNUP";
    public static final String FETCH_SELF = "SELF";
    public static final String FETCH_ALL = "ALL";
    public static final String CODE_64 = "64";
    public static final String CODE_65 = "65";
    public static final String IS_SKIP = "IS_SKIP";
    public static final String MRN= "MRN";
    public static final String ENCODED_FORMATE = "UTF-8";
    
    public static final String STATUS_200="200";
    public static final String RESULT_ORDER_KEY_DEFAULT = "orderNumber";
    public static final String SUCCESS_MESSAGE="SUCCESS_MESSAGE";
}
