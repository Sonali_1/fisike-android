package com.mphrx.fisike.constant;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

/**
 * Created by Kailash Khurana on 4/11/2017.
 */

public class MedicationConstants {

    public static final String BEFORE_FOOD = MyApplication.getAppContext().getString(R.string.before_food_key);
    public static final String AFTER_FOOD = MyApplication.getAppContext().getString(R.string.after_food_key);

    public static final String AM = MyApplication.getAppContext().getString(R.string.time_am_key);
    public static final String PM = MyApplication.getAppContext().getString(R.string.time_pm_key);

    public static final String MONDAY ="Monday";
    public static final String TUESDAY="Tuesday";
    public static final String WEDNESDAY="Wednesday";
    public static final String THURSDAY="Thursday";
    public static final String FRIDAY="Friday";
    public static final String SATURDAY="Saturday";
    public static final String SUNDAY="Sunday";

}
