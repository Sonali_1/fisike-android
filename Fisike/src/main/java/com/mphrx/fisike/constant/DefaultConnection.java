package com.mphrx.fisike.constant;

import com.mphrx.fisike.BuildConfig;

public class DefaultConnection {

    /*
       case LAYOUT_DIRECTION_RTL: setPadding(end, top, start, bottom);
       case LAYOUT_DIRECTION_LTR: setPadding(start, top, end, bottom);
 */
    public enum Environment {
        DEV1, TEST1, TEST4, STAG1, PROD, MOBQA2, MOBDEV1, FISIKE4, WEBQA1
    }

 /* public final static Environment sEnvironment = Environment.DEV1;
    public static final String FISIKE_HOST = "mobdev2.mphrx.com";
    public static final String FISIKE_PORT = "5242";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "mobdev2";
    public static final String SERVER = "mobdev2.mphrx.com";
*/

/*    public final static Environment sEnvironment = Environment.WEBQA1;
    public static final String FISIKE_HOST = "webqa1.mphrx.com";
    public static final String FISIKE_PORT = "5242";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "webqa1";
    public static final String SERVER = "mobdev1.mphrx.com";*/


   /* public final static Environment sEnvironment = Environment.DEV1;
    public static final String FISIKE_HOST = "fisiketest1.mphrx.com";
    public static final String FISIKE_PORT = "5241";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "fisiketest1";
    public static final String SERVER = "fisiketest1.mphrx.com";
*/

/*      public final static Environment sEnvironment = Environment.TEST4;
      public static final String FISIKE_HOST = "test4.mphrx.com";
      public static final String FISIKE_PORT = "5834";
      public static final String FISIKE_SERVICE = "Smack";
      public static final String FISIKE_SERVER = "test4";
      public static final String SERVER = "test4.mphrx.com";*/

/*
    public final static Environment sEnvironment = Environment.PROD;
    public static final String FISIKE_HOST = "get.fisike.com";
    public static final String FISIKE_PORT = "5980";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "fisike2";
    public static final String SERVER = "get.fisike.com";
*/

   /* public final static Environment sEnvironment = Environment.MOBQA2;
    public static final String FISIKE_HOST = "mobqa2.mphrx.com";
    public static final String FISIKE_PORT = "5298";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "mobqa2";
    public static final String SERVER = "mobqa2.mphrx.com";
*/




/*    public final static Environment sEnvironment = Environment.MOBDEV1;
    public static final String FISIKE_HOST = "webqa1.mphrx.com";
    public static final String FISIKE_PORT = "5298";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "webqa1";
    public static final String SERVER = "mobdev2.mphrx.com";*/


    // public final/ static Environment sEnvironment = Environment.STAG1;
    // public static final String FISIKE_HOST = "fisikestaging.mphrx.com";
    // public static final String FISIKE_PORT = "5262";
    // public static final String FISIKE_SERVICE = "Smack";
    // public static final String FISIKE_SERVER = "fisikestag1";
    // public static final String SERVER = "fisikestaging.mphrx.com";


    // public static final String FISIKE_PORT = "5222";
    // public static final String FISIKE_SERVICE = "Smack";
    // public static final String FISIKE_SERVER = "fisike1";
    // public static final String SERVER = "app.fisike.com";


    public final static Environment sEnvironment = Environment.WEBQA1;
    public static final String FISIKE_HOST = "mobdev1.mphrx.com";
    public static final String FISIKE_PORT = "5298";
    public static final String FISIKE_SERVICE = "Smack";
    public static final String FISIKE_SERVER = "mobdev1";

    public static final String SERVER = "demominervaus.mphrx.com";

    // public static final String SERVER = "192.168.0.83:8080";

    public static final int STATE_NOT_CONNECTED = 0;
    public static final int STATE_NOT_AUTHENTICATED = 1;
    public static final int STATE_AUTHENTICATED = 2;

    // Ejabberd / Openfire Changes
    // Default is openfire

    public static final String XMPP_SERVER = "ejabberd";

    public static final String PORT = "8080";
    public static final float AUTO_SCREEN_TIME_OUT = 1;
    // These are the message statuses that are specified in the ChatMessageMO object
    public static final int MESSAGE_SENT = 0;
    public static final int MESSAGE_RECEIVED = 1;
    public static final int MESSAGE_NOT_SENT = 2;
    public static final int MESSAGE_DELIVERED = 3;
    public static final int MESSAGE_SENT_TO_SERVER = 4;
    // These are the message statuses contentDespription
    public static final String MESSAGE_SENT_CONTENT = "MESSAGE_SENT";
    public static final String MESSAGE_RECEIVED_CONTENT = "MESSAGE_RECEIVED";
    public static final String MESSAGE_NOT_SENT_CONTENT = "MESSAGE_NOT_SENT";
    public static final String MESSAGE_DELIVERED_CONTENT = "MESSAGE_DELIVERED";
    public static final int MAX_TRY = 3;
    public static int MESSAGE_SIZE = 10;

    /**
     * Default config parameters
     */
    public static final boolean CONFIG_IS_CHAT_ENABLE = true;
    public static final boolean CONFIG_IS_SHOW_PIN = !BuildConfig.isFisike;
    public static final boolean CAN_EDIT_PIN_STATUS = true;
}