package com.mphrx.fisike.constant;

/**
 * Created by xmb2nc on 25-02-2016.
 */
public class CachedUrlConstants {

    public static final String DIAGNOSTICREPORTSEARCH = "MoDiagnosticReport/search";
    public static final String OBSERVATIONSEARCH = "/Observation/search";//not in use

    public static final String GETTASKDETAIL = "/workflow/getTask";
    public static final String DISEASE_SEARCH_URL_STRING = "/patientCondition/search";
    public static final String MEDICATION_PRESCRIPTION = "/MedicationOrder/searchWithSync";
    public static final String postfix = "/condition/search";
    public static final String UPLOAD = "/DocumentReference/searchWithSync";
    public static final String vitals_postfix = "/MoObservation/getVitals";
    public static final String LanguagePreference_postfix = "/user/updateSettings";
}
