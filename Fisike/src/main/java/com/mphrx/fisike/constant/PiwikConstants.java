package com.mphrx.fisike.constant;

public interface PiwikConstants {

    // Categories
    public static final String KLOGIN_CATEGORY = "Login";
    public static final String KCHAT_CATEGORY = "Chat";
    public static final String KQB_CATEGORY = "QuestionBuilder";
    public static final String KCONTACT_CATEGORY = "Contact";
    public static final String KINVITE_CATEGORY = "Invite";
    public static final String KPROFILE_CATEGORY = "Profile";
    public static final String KPIN_CATEGORY = "PIN";
    public static final String KGROUP_CATEGORY = "Group";

    // Event actions
    public static final String KLOGIN_ACTION = "LoginButtonPressed";
    public static final String KLOGOUT_ACTION = "LogOutButtonPressed";
    public static final String KCHAT_DETAILS_ACTION = "ChatDetailsPressed";
    public static final String KCHAT_MSG_SENT_ACTION = "ChatMsgSent";
    public static final String KCHAT_OFFLINE_ACTION = "OfflineMsgSent";
    public static final String KQB_MSG_RECEIVED_ACTION = "QBMsgReceived";
    public static final String KQB_MSG_OPENED_ACTION = "QBMsgOpened";// start
    public static final String KQB_SUBMITTED_ACTION = "QBSubmitPressed";
    public static final String KQB_REVIEWED_ACTION = "QBReviewed";// on click
    public static final String KCONTACT_SEARCHED_ACTION = "ContactSearched";
    public static final String KCONTACT_DETAILS_ACTION = "ContactDetailsViewed";
    public static final String KINVITE_USER_ACTION = "UserInvited";
    public static final String KPROFILE_UPDATED_ACTION = "ProfileUpdated";
    public static final String KPROFILE_VIEWED_ACTION = "ProfileViewed";
    public static final String KPIN_ERROR_ACTION = "PINError";
    public static final String KGROUP_DETAIL_VIEWD_ACTION = "GroupDetailViewed";
    public static final String KGROUP_DETAIL_UPDATE_ACTION = "GroupDetailUpdate";
    public static final String KGROUP_EXIT_GROUP_ACTION = "GroupExit";
    public static final String KGROUP_REMOVE_MEMBER_ACTION = "GroupMemberRemoved";
    public static final String KGROUP_UPDATE_NAME_ACTION = "GroupNameUpdated";
    public static final String KGROUP_UPDATE_PIC_ACTION = "GroupPicUpdated";
    public static final String KGROUP_CREATED_ACTION = "GroupCreated";
    public static final String KGROUP_MEMBER_ADDED_ACTION = "GroupAdded";

    // Event text
    public static final String KLOGIN_FAILED_TXT = "Login Failed";
    public static final String KLOGIN_SUCCESSFUL_TXT = "Successful Login";
    public static final String KLOGOUT_TXT = "Logout";
    public static final String KCHAT_DETAILS_TXT = "Chat details viewed";
    public static final String KCHAT_MSGSENT_TXT = "Chat message sent";
    public static final String KCHAT_OFFLINE_TXT = "Offline msg sent";
    public static final String KQB_RECEIVED_TXT = "QB message received";
    public static final String KQB_LAUNCHED_TXT = "QB launched";
    public static final String KQB_RESPONDED_TXT = "QB responded";
    public static final String KQB_REVIEWED_TXT = "QB response viewed";
    public static final String KCONTACT_SEARCHED_TXT = "Contact searched";
    public static final String KCONTACT_DETAILS_TXT = "Contact detail viewed";
    public static final String KINVITE_USER_TXT = "User invited";
    public static final String KPROFILE_UPDATED_TXT = "Profile updated";
    public static final String KPROFILE_VIEWED_TXT = "Profile viewed";
    public static final String KPIN_ERROR_TXT = "Wrong PIN entered";
    public static final String KGROUP_DETAIL_VIEWD_TXT = "Group Detail Viewed ";
    public static final String KGROUP_EXIT_GROUP_TXT = "Member Exited from Group";
    public static final String KGROUP_EXIT_MEMBER_TXT = "Group Member Removed";
    public static final String KGROUP_UPDATE_NAME_TXT = "Group Name Updated";
    public static final String KGROUP_UPDATE_PIC_TXT = "Group Pic Updated";
    public static final String KGROUP_CREATE_TXT = "Group Created";
    public static final String KGROUP_MEMBER_ADDED_TXT = "Group Member Added";

    // Custom Attributes
    public static final String KGROUP_NUMBER_GROUP_LABLE = "Number of Groups per Tenant";
    public static final String KGROUP_NUMBER_MEMBERS_LABLE = "Number of Members per Group";
    //
    // Custom Variable
    public static final String ChatMessageType = "ChatMessageType";
    public static final String ChatMessageTypeIndividual = "Individual";
    public static final String ChatMessageTypeGroup = "Group";
    // , Group, Broadcast etc
    public static final String AttachmentType = "AttachmentType";

    public static final String AttachmentTypeQB = "Question Builder";
    public static final String AttachmentTypeImage = "Image";
    public static final String AttachmentTypeVideo = "Video";

  //  public static final int VAR_ONE = 1;
    public static final int VAR_TWO = 2;
}
