package com.mphrx.fisike.fragments.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.ChatFragment;
import com.mphrx.fisike.fragments.GroupChatFragment;
import com.mphrx.fisike.fragments.SingleChatFragment;
import com.mphrx.fisike.view.FragmentStatePagerAdapter;

public class MyPagerAdapter extends FragmentStatePagerAdapter {
    private static int NUM_ITEMS = 1;
    private boolean isPatient;
    private ChatFragment chatFragment;

    public MyPagerAdapter(FragmentManager fragmentManager, boolean isPatient, ChatFragment chatFragment) {
        super(fragmentManager);
        this.isPatient = isPatient;
        this.chatFragment = chatFragment;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        if (isPatient) {
            return 1;
        }
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                Fragment singleChatFragment = (Fragment) SingleChatFragment.newInstance(position);
                chatFragment.setItem(singleChatFragment);
                return singleChatFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title
                Fragment groupChatFragment = (Fragment) GroupChatFragment.newInstance(position);
                chatFragment.setItem(groupChatFragment);
                return groupChatFragment;
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        if (isPatient || NUM_ITEMS == 1) {
            return null;
        }
        if (position == 0) {
            return TextConstants.CHAT;
        } else {
            return TextConstants.GROUP_CHAT;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commitAllowingStateLoss();

        super.destroyItem(container, position, object);
    }

}