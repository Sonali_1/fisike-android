package com.mphrx.fisike.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MedicationActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.adapter.MedicationAdapter;
import com.mphrx.fisike.adapter.VitalsAdapter;
import com.mphrx.fisike.asynctask.DeleteMedication;
import com.mphrx.fisike.asynctask.SaveMedicationOrder;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.customview.MySwipeRefreshLayout;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.MedicationPrescriptionModel;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.WrapContentLinearLayoutManager;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;
import com.mphrx.fisike_physician.network.request.MedicationEncounterRequest;
import com.mphrx.fisike_physician.network.response.MedicationEncounterResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kailash Khurana on 2/17/2016.
 */
public class MedicationOrderFragment extends Fragment implements View.OnClickListener, MedicationAdapter.clickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int ADD_MEDICATION = 110;
    private static final int UPDATE_MEDICATION = 111;
    private static final int PAGINATION_RECORD_COUNT = 10;
    private static final int DOWNLOAD_FILE = 115;
    private View myFragmentView;
    private FloatingActionMenu btnHidePopup;
    private RecyclerView listMedication;
    private MedicationAdapter medicationAdapter;
    private ArrayList<PrescriptionModel> itemList;
    private LinearLayout layoutMedicineEntry;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectedPosition;
    //  private String medicationSyncTime;
    private boolean isLoading;
    private long mTransactionId;
    //    private WrapContentLinearLayoutManager linearLayoutManager;
    private boolean isAllOrdersLoaded;
    private FloatingActionMenu fabMenu;
    private int medicationOrderTotalCount = -1;

    // medication last server sync time
    private String medicationServerSyncTime;
    private boolean isApiCallMade;
    private PrescriptionModel tempPrescriptionModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_medication_detail, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        medicationOrderTotalCount = sharedPreferences.getInt(SharedPreferencesConstant.MEDICATION_ORDER_TOTAL_COUNT, -1);
        medicationServerSyncTime = sharedPreferences.getString(SharedPreferencesConstant.MEDICATION_LAST_SYNC_TIME_SERVER, null);
        isAllOrdersLoaded = sharedPreferences.getBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, false);
        BusProvider.getInstance().register(this);
        initView();
        if (BuildConfig.isPatientApp && !isLoading && (isSyncTimeExpired())) {
            syncApiCall();
        }

        myFragmentView.setFocusableInTouchMode(true);
        myFragmentView.requestFocus();
        myFragmentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (fabMenu.isOpened()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fabMenu.close(true);
                            }
                        });
                    } else {
                        return false;
                    }
                    return true;
                }
                return false;
            }
        });
        return myFragmentView;
    }

    public void setItemList(ArrayList<PrescriptionModel> itemList) {
        this.itemList = itemList;
    }

    private void initView() {
        if (BuildConfig.isPatientApp) {
            fabMenu = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);
            MyHealth myHealthFragment = (MyHealth) getParentFragment();
            String medicationFragment = Utils.getMyHealthTabNameBasedOnPosition(myHealthFragment.getCurrentFragmentVisible());
            if (myHealthFragment.getMyHelthAdapter().getFragment(medicationFragment) instanceof MedicationOrderFragment) {
                if (fabMenu.getVisibility() != View.VISIBLE) {
                    fabMenu.setVisibility(View.VISIBLE);
                }
            }
            fabMenu.setOnClickHandler(new FloatingActionMenu.OnClickHandler() {

                @Override
                public void onClickHandle(boolean isToHandle) {
                    if (!isToHandle) {
                        UserMO userMO = SettingManager.getInstance().getUserMO();
                        if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                            Intent intent = new Intent(getActivity(), MedicationActivity.class);
                            getActivity().startActivityForResult(intent, ADD_MEDICATION);
                        } else {
                            Toast.makeText(getActivity(),
                                    getActivity().getResources().getString(R.string.physician_operation_not_allowed), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });


            btnHidePopup = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);
            //btnHidePopup.setOnClickListener(this);

        }

        listMedication = (RecyclerView) myFragmentView.findViewById(R.id.listMedicationOrder);

        itemList = new ArrayList<>();
        medicationAdapter = new MedicationAdapter(itemList, getActivity());
        WrapContentLinearLayoutManager linearLayoutManager = new WrapContentLinearLayoutManager(getActivity());
        listMedication.setLayoutManager(linearLayoutManager);
        medicationAdapter.setClickListener(MedicationOrderFragment.this);
        listMedication.setAdapter(medicationAdapter);

        layoutMedicineEntry = (LinearLayout) myFragmentView.findViewById(R.id.ll_medicine_entry);
        swipeRefreshLayout = (SwipeRefreshLayout) myFragmentView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);

        listMedication.setOnScrollListener(new EndLessScrollingListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // if all the the records are loaded then there is no need to hit the pagination call
                if (isAllOrdersLoaded) {
                    return;
                }
                if (!isLoading) {
                    paginationApiCall();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView == null || recyclerView.getChildCount() > 0) {
                    if (recyclerView.getChildAt(0).getTop() == 0) {
                        swipeRefreshLayout.setEnabled(true);
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                    }
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        dbListMedicationShow();

        // check if sync time has expired
        // add the check whether currently api call is running
        if (isSyncTimeExpired()) {
            // make the api call
        }

        Bundle data = getArguments();

        if (data != null && data.containsKey(VariableConstants.NOTIFICATION_REMINDER)) {
            double quantityDouble = data.getDouble(VariableConstants.QUANTITY);
            String quantity;
            boolean isPlural;
            if (quantityDouble == (int) quantityDouble) {
                quantity = ((int) quantityDouble) + "";
            } else {
                quantity = Utils.formatQuantity(quantityDouble, -1) + "";
            }
            if (Float.parseFloat(quantity) > 1)
                isPlural = true;
            else
                isPlural = false;
            String display = data.getString(VariableConstants.UNIT);
            String unitCode = "";

            try {
                unitCode = MedicationUnitsEnum.getCodeFromDisplayedValue(display);
            } catch (Exception e) {

            }
            String unit = Utils.calculateUnitToDisplay(unitCode, isPlural);
           /* if (unit.contains("tablet") && Float.parseFloat(quantity) > 1) {
                unit = "tablets";
            } else if (unit.contains("capsule") && Float.parseFloat(quantity) > 1) {
                unit = "capsules";
            }*/

            String physicianName = data.getString(VariableConstants.PRACTIONER_NAME);
            String docName = "";
            if (physicianName != null) {
                docName = physicianName.trim().toLowerCase();
            }

            if (!docName.equals("") && (!docName.startsWith(getResources().getString(R.string.conditional_doc)) && !docName.startsWith(getResources().getString(R.string.conditional_dr)) && !docName.startsWith(getResources().getString(R.string.conditional_dr_dot)) && !docName.startsWith(getResources().getString(R.string.conditional_doctor_space)) && !docName.startsWith(getResources().getString(R.string.conditional_doctor))))
                physicianName = getResources().getString(R.string.txt_dr) + physicianName.trim();


            final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View v = layoutInflater.inflate(R.layout.dialog_medication_reminder, null, false);
            alertDialog.setView(v);

            CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
            tvTitle.setText(getResources().getString(R.string.txt_reminder) + (physicianName != null && !physicianName.equals("") ? (getResources().getString(R.string.txt_from_with_space) + physicianName) : ""));

            CustomFontTextView txtTimeToHave = (CustomFontTextView) v.findViewById(R.id.txtTimeToHave);

            Spannable word = new SpannableString(getActivity().getResources().getString(R.string.time_to_have));
            word.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.dusky_blue)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtTimeToHave.setText(word);
            Spannable wordTwo = new SpannableString(" \"" + data.getString(VariableConstants.MEDICIAN_NAME) + "\"");
            wordTwo.setSpan(new RelativeSizeSpan(1.20f), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            wordTwo.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.action_bar_bg)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtTimeToHave.append(wordTwo);

            CustomFontTextView txtQuantity = (CustomFontTextView) v.findViewById(R.id.txtQuantity);

            Spannable dosageText = new SpannableString(quantity.trim() + " " + unit + getResources().getString(R.string.txt_at));
            dosageText.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.dusky_blue)), 0, dosageText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtQuantity.setText(dosageText);
            String time = data.getString(VariableConstants.TIMMING);
            boolean isAmPm = time.toUpperCase().contains(TextConstants.AM) || time.toUpperCase().contains(TextConstants.PM) ? true : false;

            String formatedTime="";
            try {
                if (DateFormat.is24HourFormat(getContext())) {
                    if (isAmPm) {
                        formatedTime = DateTimeUtil.calculateDateLocle(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.sourceTimeFormat);
                    } else {
                        formatedTime = DateTimeUtil.calculateDateLocle(time, DateTimeUtil.sourceTimeFormat, DateTimeUtil.sourceTimeFormat);
                    }
                } else {
                    if (isAmPm) {
                        formatedTime = DateTimeUtil.calculateDateLocle(time, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    } else {
                        formatedTime = DateTimeUtil.calculateDateLocle(time, DateTimeUtil.sourceTimeFormat, DateTimeUtil.destinationTimeFormat);
                    }

                }
            } catch (Exception ex) {
            }


            Spannable timming = new SpannableString(" " + formatedTime);
            timming.setSpan(new RelativeSizeSpan(1.20f), 0, timming.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            timming.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.action_bar_bg)), 0, timming.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtQuantity.append(timming);

            v.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();
        }
    }

    private void paginationApiCall() {
        if (medicationOrderTotalCount > 0 && medicationAdapter != null && medicationAdapter.getItemCount() >= medicationOrderTotalCount) {
            isAllOrdersLoaded = true;
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, true);
            editor.commit();
            medicationAdapter.setFooterVisible(false);
            medicationAdapter.notifyDataSetChanged();
            return;
        }
        if (!isLoading) {
            ThreadManager.getDefaultExecutorService().submit(new MedicationEncounterRequest(paginationJson(),
                    getTransactionId(), getActivity(), false));
            isLoading = true;
            if (medicationAdapter != null) {
                medicationAdapter.setFooterVisible(true);
                medicationAdapter.notifyDataSetChanged();
            }
        }
    }

    private void syncApiCall(boolean isAlarmResetRequired) {
        if (!isLoading) {
            swipeRefreshLayout.setRefreshing(true);
            ThreadManager.getDefaultExecutorService().submit(new MedicationEncounterRequest(syncJson(),
                    getTransactionId(), getActivity(), true,
                    isAlarmResetRequired));
            isLoading = true;
        }
    }

    private void syncApiCall() {
        if (!isLoading) {
            swipeRefreshLayout.setRefreshing(true);
            ThreadManager.getDefaultExecutorService().submit(new MedicationEncounterRequest(syncJson(), getTransactionId(), getActivity(), false));
            isLoading = true;
        }
    }

    private boolean containsEncounter(int encounterId) {
        for (int index = 0; index < itemList.size(); index++) {
            if (itemList.get(index).getEncounterID() == encounterId) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == ADD_MEDICATION) {
            if (data != null && data.getExtras() != null && data.getExtras().containsKey(VariableConstants.MEDICATION_PRESCRIPTION_MODEL)) {

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                medicationServerSyncTime = sharedPreferences.getString(SharedPreferencesConstant.MEDICATION_LAST_SYNC_TIME_SERVER, null);

                if (medicationServerSyncTime == null) {
                    paginationApiCall();
                } else {
                    syncApiCall();
                }

                listMedication.scrollToPosition(0);
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == UPDATE_MEDICATION) {
            if (data != null && data.getExtras() != null && data.getExtras().containsKey(VariableConstants.MEDICATION_PRESCRIPTION_MODEL)) {
                boolean isAlarmResetRequired = data.getExtras().getBoolean(VariableConstants.MEDICATION_PRESCRIPTION_AlARM_RESET);

                syncApiCall(isAlarmResetRequired);
            }
        } else if (requestCode == ADD_MEDICATION || requestCode == UPDATE_MEDICATION) {
            String errorMessage = null;
            if (data != null && data.getExtras() != null && data.getExtras().containsKey(VariableConstants.ERROR_MESSAGE)) {
                errorMessage = data.getExtras().getString(VariableConstants.ERROR_MESSAGE);
            }
            errorInSavingResult(errorMessage);
        }
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    @Override
    public void onResume() {
        super.onResume();

        // digitization flow handling w.r.t new changes
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isSyncAfteDigitizationDone = sharedPreferences.getBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION_MEDICATION, true);
        if (!isSyncAfteDigitizationDone) {
            if (medicationServerSyncTime == null && !isLoading) {
                paginationApiCall();
            } else {
                syncApiCall();
            }
        }
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.MEDICATION_FRAGMENT);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /**
     * Request Json for pagination
     *
     * @return
     */
    @Subscribe
    public void onMedicationEncounterResponse(MedicationEncounterResponse response) {

        if (mTransactionId != response.getTransactionId())
            return;

        isLoading = false;
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        if (response.isSuccessful()) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (response.isMedicationSyncApi()) {
                medicationOrderTotalCount += response.getMedicationOrdersTotalCount();
                editor.putBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION_MEDICATION, true);
                editor.commit();
            } else {
                medicationOrderTotalCount = response.getMedicationOrdersTotalCount();
            }

            if (response.getMedicationSyncTime() != null) {
                medicationServerSyncTime = response.getMedicationSyncTime();
            }

            // update the all records loaded
            if (response.getMedicationOrdersTotalCount() <= itemList.size()) {
                isAllOrdersLoaded = true;
                editor.putBoolean(SharedPreferencesConstant.ALL_MEDICATION_ORDER_LOADED, true);
                editor.commit();
            }
            dbListMedicationShow();
        }

        /*this particular call will take some time in that caswe we will be setting the visibilty to gone*/
        if (itemList == null || itemList.size() == 0) {
            layoutMedicineEntry.setVisibility(View.VISIBLE);
            listMedication.setVisibility(View.GONE);
        }
    }

    private JSONObject paginationJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("_count", PAGINATION_RECORD_COUNT);
            jsonObject.put("_skip", itemList == null ? 0 : itemList.size());
            //commenting this as order need to be in ascending order of created date
            jsonObject.put("_sort:desc", "lastUpdated");
            JSONArray includeArray = new JSONArray();
            includeArray.put("MedicationOrder:prescriber");
            includeArray.put("MedicationOrder:medication");
            includeArray.put("MedicationOrder:patient");
            jsonObject.put("_include", includeArray);

            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * o
     * Request Json for Sync
     *
     * @return
     */
    private JSONObject syncJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            medicationServerSyncTime = medicationServerSyncTime.replaceAll("\\+", "%2B");
            jsonObject.put("_lastUpdated", ">" + medicationServerSyncTime);
            JSONArray includeArray = new JSONArray();
            includeArray.put("MedicationOrder:prescriber");
            includeArray.put("MedicationOrder:medication");
            includeArray.put("MedicationOrder:patient");
            jsonObject.put("_include", includeArray);

            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case BaseActivity.RECORDS_MANUAL:
                UserMO userMO = SettingManager.getInstance().getUserMO();
                if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                    Intent intent = new Intent(getActivity(), MedicationActivity.class);
                    //intent.putExtra(VariableConstants.START_INSIDE_ACTIVITY,true); //changed by Aastha for side drawe FIS-6382
                    getActivity().startActivityForResult(intent, ADD_MEDICATION);
                } else {
                    Toast.makeText(getActivity(),
                            getActivity().getResources().getString(R.string.physician_operation_not_allowed), Toast.LENGTH_LONG).show();
                }
                ((FloatingActionMenu) getActivity().findViewById(R.id.menu1)).close(true);
                break;
        }

    }

    public void handleMedicationPrescriptionDelete(int medicationPrescriptionId) {
        deleteMedicationPrescriptionFromDb(medicationPrescriptionId); //When API linked it needs to integrate and response will be there
    }

    public void handleMedicineNotDeleted(int medicationPrescriptionId) {
        Toast.makeText(getActivity(), getResources().getString(R.string.xmpp_error_title), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        if (!isLoading && (itemList == null || itemList.size() == 0)) {
            paginationApiCall();
        } else if (!isLoading) {
            syncApiCall();
        }
        // sync api call
        // add the logic to check whether to make a sync api call/or not
        doSearch();
    }


    public void doSearch() {

        dbListMedicationShow();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }

    public void saveOrUpdatePrescriptioninDB(PrescriptionModel prescriptionModel, int tempPosition) {
        if (prescriptionModel.getAutoReminderSQLBool() == 0) {
            for (MedicineDoseFrequencyModel medicineDoseFrequencyModel : prescriptionModel.getArrayDose()) {
                Utils.cancelAlarm(getActivity(), medicineDoseFrequencyModel.getAlarmManagerUniqueKey());
            }
            EncounterMedicationDBAdapter encounterMedicationDBAdapter = EncounterMedicationDBAdapter.getInstance(getActivity());
            ContentValues contentValues = new ContentValues();
            contentValues.put(encounterMedicationDBAdapter.getISALARMSET(), prescriptionModel.getAutoReminderSQLBool());
            try {
                encounterMedicationDBAdapter.updateMedicationPrescription(prescriptionModel.getID(), contentValues);
                itemList.set(tempPosition, prescriptionModel);
            } catch (Exception e) {
            }
        } else if (prescriptionModel.getAutoReminderSQLBool() == 1) {
            ArrayList<MedicineDoseFrequencyModel> medicineDoseFrequencyModelsArray = prescriptionModel.getArrayDose();
            for (int i = 0; i < medicineDoseFrequencyModelsArray.size(); i++) {
                MedicineDoseFrequencyModel medicineDoseFrequencyModel = medicineDoseFrequencyModelsArray.get(i);
                int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;

                medicineDoseFrequencyModel.setAlarmManagerUniqueKey((int) System.currentTimeMillis());
                Utils.startAlarmMedication(getActivity(), medicineDoseFrequencyModel.getAlarmManagerUniqueKey(), prescriptionModel, interval, medicineDoseFrequencyModel);
                medicineDoseFrequencyModelsArray.set(i, medicineDoseFrequencyModel);
            }
            try {
                EncounterMedicationDBAdapter encounterMedicationDBAdapter = EncounterMedicationDBAdapter.getInstance(getActivity());
                ContentValues contentValues = new ContentValues();
                contentValues.put(encounterMedicationDBAdapter.getISALARMSET(), prescriptionModel.getAutoReminderSQLBool());
                encounterMedicationDBAdapter.updateMedicationPrescription(prescriptionModel.getID(), contentValues);

                itemList.set(tempPosition, prescriptionModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void errorInSavingResult(String errorMessage) {
        if (errorMessage == null || errorMessage.equals("")) {
            errorMessage = TextConstants.UNEXPECTED_ERROR;
        }
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.WHITE);
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.RED);
        snackbar.show();

    }

    class FetchAllMedication extends AsyncTask<Void, Void, ArrayList<PrescriptionModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PrescriptionModel> doInBackground(Void... params) {
            try {
                ArrayList<PrescriptionModel> medicationPrescriptionList = EncounterMedicationDBAdapter.getInstance(getActivity()).fetchAllMedication();
                return medicationPrescriptionList;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<PrescriptionModel> medicationPrescriptionList) {

            try {
                ArrayList<Integer> encounterList = new ArrayList<Integer>();
                ArrayList tempItemList = new ArrayList<PrescriptionModel>();
                if (medicationPrescriptionList != null && medicationPrescriptionList.size() > 0) {
                    for (int prescriberIndex = 0; prescriberIndex < medicationPrescriptionList.size(); prescriberIndex++) {
                        PrescriptionModel encounterModel = medicationPrescriptionList.get(prescriberIndex);
                        int encounterID2 = encounterModel.getEncounterID();
                        encounterList.add(encounterID2);
                        tempItemList.add(medicationPrescriptionList.get(prescriberIndex));
                    }
                    itemList.clear();
                    itemList.addAll(tempItemList);
                    setMedicationList();
                } else {

                    //    setMedicationList();

                    if (!isApiCallMade) {
                        paginationApiCall();
                        isApiCallMade = true;
                    } else {
                        layoutMedicineEntry.setVisibility(View.VISIBLE);
                        listMedication.setVisibility(View.GONE);
                    }
                }
                if (getArguments() != null && getArguments().containsKey(VariableConstants.ENCOUNTER_ID)) {
                    int encounterId = getArguments().getInt(VariableConstants.ENCOUNTER_ID);
                    if (itemList != null && containsEncounter(encounterId)) {
                        super.onPostExecute(medicationPrescriptionList);
                        setArguments(null);
                        return;
                    }
                    setArguments(null);
                    syncApiCall();
                }
            } catch (Exception e) {
                listMedication.setVisibility(View.GONE);
            }
            super.onPostExecute(medicationPrescriptionList);
        }

    }

    private JSONObject getEncounterPayload(int encounterId) {
        try {
            JSONObject jsonObject = new JSONObject();
            JSONObject jsonConstraints = new JSONObject();
            jsonConstraints.put("encounter", encounterId);
            jsonObject.put("constraints", jsonConstraints);
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    private void setMedicationList() {
        // add a null check here
//            medicationAdapter.setPrescriptionModelList(itemList);
        medicationAdapter.setFooterVisible(false);
        medicationAdapter.notifyDataSetChanged();

        layoutMedicineEntry.setVisibility(View.GONE);
        listMedication.setVisibility(View.VISIBLE);
    }

    /**
     * Show data from db
     */
    public void dbListMedicationShow() {
        new FetchAllMedication().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void itemClicked(View view, int position) {

        View v = view;
        switch (v.getId()) {

            case R.id.rl_medication_prescription:
                selectedPosition = position;
                try {
                    if (itemList.size() > 0) {
                        PrescriptionModel clickedPrescriptionModel = itemList.get(selectedPosition);
                        if (clickedPrescriptionModel != null) {
                            Intent medicationPrescriptionActivityIntent = new Intent(getActivity(), MedicationActivity.class);
                            medicationPrescriptionActivityIntent.putExtra(TextConstants.MEDICATION_PRESCRIPTION_INTENT, clickedPrescriptionModel);
                            getActivity().startActivityForResult(medicationPrescriptionActivityIntent, UPDATE_MEDICATION);
                            break;
                        }
                    }
                } catch (Exception ex) {
                    listMedication.getRecycledViewPool().clear();
                    medicationAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.imgOverflow:
                if (itemList.size() > 0)
                    showPopUpMenu((IconTextView) v, itemList.get(position), position);
                break;


            default:
                break;
        }
    }

    @Override
    public void itemLongClicked(View view, int position) {
        showPopUpMenu((IconTextView) view.findViewById(R.id.imgOverflow), itemList.get(position), position);
    }

    private void showPopUpMenu(final IconTextView imgOverFlow, final PrescriptionModel prescriptionModel, final int tempPosition) {
        final Resources resources = getActivity().getResources();
        final PopupMenu popup = new PopupMenu(getActivity(), imgOverFlow);
        if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(getResources().getString(R.string.conditional_sos))) {
            popup.getMenu().add(resources.getString(R.string.remove_medicine));
            popup.getMenu().add(resources.getString(R.string.update_medicine));
            popup.getMenu().add(resources.getString(R.string.clone_medicine));
        } else if (prescriptionModel.getAutoReminderSQLBool() == 0) {
            popup.getMenu().add(resources.getString(R.string.remove_medicine));
            popup.getMenu().add(resources.getString(R.string.update_medicine));
            popup.getMenu().add(resources.getString(R.string.turn_on_reminder));
            popup.getMenu().add(resources.getString(R.string.clone_medicine));
        } else {
            popup.getMenu().add(resources.getString(R.string.remove_medicine));
            popup.getMenu().add(resources.getString(R.string.update_medicine));
            popup.getMenu().add(resources.getString(R.string.turn_off_reminder));
            popup.getMenu().add(resources.getString(R.string.clone_medicine));
        }

        if (prescriptionModel != null && (prescriptionModel.getMimeType() == VariableConstants.MIME_TYPE_FILE || prescriptionModel.getMimeType() == VariableConstants.MIME_TYPE_DOC) || prescriptionModel.getMimeType() == VariableConstants.MIME_TYPE_DOCX) {
            popup.getMenu().add(resources.getString(R.string.view_prescription));
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                String itemClicked = item.getTitle().toString();
                if (itemClicked.equalsIgnoreCase(resources.getString(R.string.remove_medicine))) {
                    if (Utils.showDialogForNoNetwork(getActivity(), false))
                        showConfirmationDialog(imgOverFlow, tempPosition);
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.update_medicine))) {
                    selectedPosition = tempPosition;
                    openMedicationPrescriptionPopUp(Integer.parseInt(imgOverFlow.getTag().toString()), false);
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.turn_off_reminder))) {
                    if (prescriptionModel.getAutoReminderSQLBool() == 1) {
                        saveOrUpdateMedication(prescriptionModel, 0, tempPosition);
                    }
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.turn_on_reminder))) {
                    if (prescriptionModel.getAutoReminderSQLBool() == 0) {
                        saveOrUpdateMedication(prescriptionModel, 1, tempPosition);
                    }
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.clone_medicine))) {
                    openMedicationPrescriptionPopUp(Integer.parseInt(imgOverFlow.getTag().toString()), true);
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.view_prescription))) {
                    String payload = "{\"medicationOrderId\":" + prescriptionModel.getID() + "}";
                    if (prescriptionModel.getMimeType() == VariableConstants.MIME_TYPE_DOC || prescriptionModel.getMimeType() == VariableConstants.MIME_TYPE_DOCX) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setType("application/msword");
                        if (intent.resolveActivity(getActivity().getPackageManager()) == null) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.txt_file_can_not_open), Toast.LENGTH_SHORT).show();
                        }
                        if (Utils.isNetworkAvailable(getActivity())) {
                            if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), DOWNLOAD_FILE)) {
                                new DownloadRecordFileRequest(getActivity(), prescriptionModel.getID(), prescriptionModel.getMimeType(), MphRxUrl.getMeciationReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                tempPrescriptionModel = prescriptionModel;
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.txt_no_network), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (Utils.isNetworkAvailable(getActivity())) {
                            if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), DOWNLOAD_FILE)) {
                                new DownloadRecordFileRequest(getActivity(), prescriptionModel.getID(), prescriptionModel.getMimeType(), MphRxUrl.getMeciationReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                tempPrescriptionModel = prescriptionModel;
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.txt_no_network), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                popup.dismiss();
                return true;
            }
        });
        popup.show();//showing popup menu
    }

    private boolean checkPermission(final String permissionType, String message, final int requestCode) {
        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), "Permission", message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            requestPermissions(new String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, requestCode);
            }
            return false;
        }

        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == DOWNLOAD_FILE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (tempPrescriptionModel != null) {
                String payload = "{\"medicationOrderId\":" + tempPrescriptionModel.getID() + "}";
                new DownloadRecordFileRequest(getActivity(), tempPrescriptionModel.getID(), tempPrescriptionModel.getMimeType(), MphRxUrl.getMeciationReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            onPermissionNotGranted("");
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Permission_needs_granted_access), Toast.LENGTH_SHORT).show();
    }

    private void saveOrUpdateMedication(PrescriptionModel prescriptionModel, int switchValue, int tempPosition) {
        new SaveMedicationOrder(MedicationOrderFragment.this, getActivity(), prescriptionModel, true, switchValue, tempPosition).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showConfirmationDialog(final IconTextView imgOverFlow, final int position) {
        DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getResources().getString(R.string.alert), getActivity().getResources().getString(R.string.delete_prescription), getResources().getString(R.string.txt_yes), getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;

                    case Dialog.BUTTON_POSITIVE:
                        dialog.dismiss();
                        PrescriptionModel prescriptionModel = itemList.get(position);
                        new DeleteMedication(getActivity(), prescriptionModel, MedicationOrderFragment.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        break;
                }
            }
        });
    }


    private void openMedicationPrescriptionPopUp(int prescriptionId, boolean isClone) {
        //medicationFragment.editEncounterFromOverflow(encounterId);
        PrescriptionModel clickedPrescriptionModel = null;
        try {
            clickedPrescriptionModel = EncounterMedicationDBAdapter.getInstance(getActivity()).fetchMedicationPrescription(prescriptionId);
            if (isClone) {
                clickedPrescriptionModel.setMedicianId(0);
                clickedPrescriptionModel.setMedicineName(null);
                clickedPrescriptionModel.setID(-1);

                //need to remove fields which should not be shown while updating
            }
            if (clickedPrescriptionModel != null) {
                Intent medicationPrescriptionActivityIntent = new Intent(getActivity(), MedicationActivity.class);
                medicationPrescriptionActivityIntent.putExtra(TextConstants.MEDICATION_PRESCRIPTION_INTENT, clickedPrescriptionModel);
                medicationPrescriptionActivityIntent.putExtra(getResources().getString(R.string.txt_CLONE), isClone);
                if (isClone)
                    getActivity().startActivityForResult(medicationPrescriptionActivityIntent, ADD_MEDICATION);
                else
                    getActivity().startActivityForResult(medicationPrescriptionActivityIntent, UPDATE_MEDICATION);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteMedicationPrescriptionFromDb(int medicationPrescriptionId) {
        try {
            EncounterMedicationDBAdapter.getInstance(getActivity()).deleteMedicationPrescription(medicationPrescriptionId);
            // disbale the reminder as well
            dbListMedicationShow();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // called from onCreateView if last sync was before 24 hours then true is returned and sync is called.

    private boolean isSyncTimeExpired() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        long val = sharedPreferences.getLong(SharedPreferencesConstant.MEDICATION_SYNC_TIME_LOCAL, 0);
        if (val == 0) {
            return false;
        }
        if ((System.currentTimeMillis() - val >= TimeUnit.DAYS.toMillis(1))) {
            return true;
        }
        return false;
    }
}