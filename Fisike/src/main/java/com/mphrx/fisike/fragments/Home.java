package com.mphrx.fisike.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.HomeGridAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;

import appointment.phlooba.WhosPatientActivity;


public class Home extends Fragment implements View.OnClickListener {

    private View myFragmentView;
    private int numberOfColumns = 2;
    private RecyclerView mRv_home_grid;
    private HomeGridAdapter adapterTiles;
    public static final int PERMISSION_REQUEST_CODE = 1;

    public Home() {
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.home, container, false);
        ((BaseActivity) getActivity()).setToolbarTitle("My Home");

        findView();
        initView();

        return myFragmentView;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
    }


    private void findView() {
        mRv_home_grid = (RecyclerView) myFragmentView.findViewById(R.id.rv_home_grid);
    }


    private void initView() {
        mRv_home_grid.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        adapterTiles = new HomeGridAdapter(getActivity());
        mRv_home_grid.setAdapter(adapterTiles);
        BaseActivity.ivToolbarCall.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_call: {
                if (!checkPermission(Manifest.permission.CALL_PHONE, ""))
                    return;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + TextConstants.NH_HELP_LINE_NO));
                startActivity(intent);
            }
            break;
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public boolean checkPermission(final String permissionType, String message) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), "Permission", message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                            } else
                                onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionNotGranted(permissions[0]);
            }
        }
    }

    public void onPermissionGranted(String permission) {
        throw new RuntimeException("You must override this function if you use ask Runtime Permission.");
    }

    public void onPermissionNotGranted(String permission) {
        throw new RuntimeException("You must override this function if you use ask Runtime Permission.");
    }

}