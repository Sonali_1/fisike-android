package com.mphrx.fisike.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.ImageGestureAnimation.GestureImageView;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.DecriptFile;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.UploadTaggingEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;

import appointment.phlooba.HomeDrawStatusActivity;

public class OpenImageFragment extends BaseActivity {

    private GestureImageView imgPreview;
    private String attachmentPath;
    private ProgressDialog progressDialog;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private int mimeType;
    private WebView wvDocumentViewer;
    private TextView txtRefId;
    private TextView prescription, dateTime;
    private RelativeLayout rel_imageViewer, rel_lyt_image;
    String timestamp;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.fragment_image_decript);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.fragment_image_decript, frameLayout);

        initializeCustomActionBar();

        imgPreview = (GestureImageView) findViewById(R.id.imgPreviw);
        wvDocumentViewer = (WebView) findViewById(R.id.wvDocumentViewer);
        txtRefId = (TextView) findViewById(R.id.txtRefId);
        prescription = (TextView) findViewById(R.id.prescription_header);
        dateTime = (TextView) findViewById(R.id.txtDateTime);
        rel_imageViewer = (RelativeLayout) findViewById(R.id.rel_imageViewer);
        rel_lyt_image = (RelativeLayout) findViewById(R.id.rel_lyt_image);
        attachmentPath = getIntent().getExtras().getString(VariableConstants.VIDEO_URI);
        mimeType = getIntent().getExtras().getInt(VariableConstants.MIME_TYPE);


        String refId = getIntent().getExtras().getString(VariableConstants.REFRENCE_ID);
        String docType = getIntent().getExtras().getString(VariableConstants.DOCUMENT_TYPE);
        Context context=this;
        LinkedHashMap<String, UploadTaggingEnum> uploadTaggingDisplayValueMap = UploadTaggingEnum.getUploadTaggingEnumLinkedHashMap();
        String documentTypeDisplayableValue= docType.equalsIgnoreCase(context.getResources().getString(R.string.conditional_check_unknown)) || docType.equalsIgnoreCase(context.getResources().getString(R.string.conditional_check_others))
                ? context.getResources().getString(R.string.document) : docType.equalsIgnoreCase(context.getResources().getString(R.string.Radiology_Images)) ? context.getResources().getString(R.string.Radiology_Image) : docType;

        if(uploadTaggingDisplayValueMap!=null && uploadTaggingDisplayValueMap.containsKey(documentTypeDisplayableValue))
            documentTypeDisplayableValue=uploadTaggingDisplayValueMap.get(documentTypeDisplayableValue).getValue();
        prescription.setText(documentTypeDisplayableValue);
        timestamp = getIntent().getExtras().getString(VariableConstants.TIMESTAMP);
        if (refId != null && !refId.equals("null") && !refId.equals("0")) {
            txtRefId.setVisibility(View.VISIBLE);
            if (timestamp != null)
                dateTime.setText(DateTimeUtil.calculateDateTimeFromTimeStampUploads(Long.parseLong(timestamp), context));
            txtRefId.setText(getString(R.string.ref_id) + refId);
            prescription.setVisibility(View.VISIBLE);
            dateTime.setVisibility(View.VISIBLE);
        } else {
//            showFile(attachmentPath);
//            wvDocumentViewer.setVisibility(View.VISIBLE);
//            return;
        }

        if (getIntent().getExtras().containsKey(VariableConstants.DECRIPTED_FILE)) {
            Bitmap decodeByteArray = BitmapFactory.decodeFile(attachmentPath);
            imgPreview.setImageBitmap(decodeByteArray);
            return;
        }

        if (getIntent().getStringExtra(VariableConstants.DOCUMENT_TYPE).equalsIgnoreCase("script")) {
            toolbar_title.setText(getResources().getString(R.string.script));
            imgPreview.setImageBitmap(Utils.convertBase64ToBitmap(HomeDrawStatusActivity.scriptImage));

          /*  if(Utils.isValueAvailable(getIntent().getStringExtra("scriptThumbImage")))
            {
                byte[] imageBytes = Base64.decode(getIntent().getStringExtra("scriptThumbImage"), Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            }*/
            return;
        }


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(this.getResources().getString(R.string.progress_loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        new DecriptFile(this, attachmentPath, "Image").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showFile(String attachmentpath) {
        wvDocumentViewer.getSettings().setJavaScriptEnabled(true);
        wvDocumentViewer.getSettings().setAllowFileAccess(true);
    }

    private void initializeCustomActionBar() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_documents));
    }

    public void showDecriptedFile(byte[] decodFile) {
        switch (mimeType) {
            case VariableConstants.MIME_TYPE_IMAGE:
                // byte[] resized= resizeImage(decodFile)
                rel_imageViewer.setVisibility(View.GONE);
                rel_lyt_image.setVisibility(View.VISIBLE);
                if(decodFile!=null)
                {
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decodFile, 0, decodFile.length);
                    imgPreview.setImageBitmap(decodeByteArray);
                }
                dateTime.setText(DateTimeUtil.calculateDateTimeFromTimeStampUploads(Long.parseLong(timestamp), OpenImageFragment.this));
                break;
            case VariableConstants.MIME_TYPE_FILE:
                rel_imageViewer.setVisibility(View.VISIBLE);
                rel_lyt_image.setVisibility(View.GONE);
                wvDocumentViewer.setVisibility(View.VISIBLE);
                WebSettings settings = wvDocumentViewer.getSettings();
                settings.setJavaScriptEnabled(true);

                //The default value is true for API level android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1 and below,
                //and false for API level android.os.Build.VERSION_CODES.JELLY_BEAN and above.
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                    settings.setAllowUniversalAccessFromFileURLs(true);

                settings.setBuiltInZoomControls(true);
                setWebClientForLoading();

                File file = Utils.saveToInternalSorage(OpenImageFragment.this, "report.pdf");
                FileOutputStream output = null;
                try {
                    output = new FileOutputStream(file);
                    output.write(decodFile, 0, decodFile.length);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                wvDocumentViewer.loadUrl("file:///android_asset/pdfviewer/index.html?file=file://" + file.getAbsolutePath());
                break;
            case VariableConstants.MIME_TYPE_DOC:

                break;
        }


        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    byte[] resizeImage(byte[] input) {
        Bitmap original = BitmapFactory.decodeByteArray(input, 0, input.length);
        Bitmap resized = Bitmap.createScaledBitmap(original, 100, 100, true);

        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 100, blob);

        return blob.toByteArray();
    }

    /**
     * Web view client handle the web events
     */
    private void setWebClientForLoading() {
        wvDocumentViewer.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            public void onPageFinished(WebView view, String url) {
                wvDocumentViewer.clearCache(true);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(OpenImageFragment.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
            //super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
