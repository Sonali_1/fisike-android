package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.AppTour.AppTourActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import java.util.concurrent.TimeUnit;

public class MinervaSettings extends Fragment {

    private TextView txtGroupName;
    private TextView txtVersion;
    private TextView txtDeviceActivation;
    private View myFragmentView;
    private UserMO userMO;
    private Activity activity;
    private LinearLayout layoutPin;
    private ConfigMO config;
    private CheckBox checkPin;
    private MessengerService mService;
    private boolean mBound;
    private Button btnChangePin, btnAppTour;

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;
        mBound = ((BaseActivity) activity).ismBound();
        mService = ((BaseActivity) activity).getmService();
        super.onAttach(activity);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.layout_minerva_settings, container, false);
        setValues();
        ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.menuOptionLabelSettings));

        config = ConfigManager.getInstance().getConfig();

        if (config.isCanChangePinStatus()) {
            layoutPin.setVisibility(View.VISIBLE);
        }

        setHasOptionsMenu(true);

        checkPin.setChecked(config.isShowPin());
        btnChangePin.setEnabled(true);

        layoutPin.setVisibility(View.GONE);

        checkPin.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    launchLockScreen();
                    return;
                } else {
                    Editor edit = activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0).edit();
                    edit.putString(VariableConstants.LOCK_PIN_CODE, "").commit();
                    btnChangePin.setEnabled(isChecked);
                }
                config.setShowPin(isChecked);
                config = ConfigManager.getInstance().updateConfig(config);
                if (mBound) {
                    mService.startPinScreenTimer();
                }
            }
        });

        return myFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

    }

    private void setValues() {
        userMO = SettingManager.getInstance().getUserMO();
        findViews();

        String deviceActivationDate = userMO.getDeviceActivationDate();
        if (null != deviceActivationDate && !deviceActivationDate.equals("") && !deviceActivationDate.equals(getActivity().getResources().getString(R.string.txt_null))) {
            txtDeviceActivation.setText(Utils.getFormattedDate(deviceActivationDate));
        }

        txtGroupName.setText(userMO.getGroupName());

        PackageInfo pkg;
        try {
            pkg = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            String appVersion = pkg.versionName;
            txtVersion.setText(appVersion);
        } catch (NameNotFoundException e) {
            txtVersion.setText("");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    private void findViews() {
        txtGroupName = (TextView) myFragmentView.findViewById(R.id.txtGroupName);
        txtDeviceActivation = (TextView) myFragmentView.findViewById(R.id.txtDeviceActivation);
        txtVersion = (TextView) myFragmentView.findViewById(R.id.txtVersion);
        layoutPin = (LinearLayout) myFragmentView.findViewById(R.id.layoutPin);
        checkPin = (CheckBox) myFragmentView.findViewById(R.id.checkPin);
        btnChangePin = (Button) myFragmentView.findViewById(R.id.btnChangePin);
        btnChangePin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                launchLockScreen();
            }

        });

        btnAppTour = (Button) myFragmentView.findViewById(R.id.btnAppTour);
        btnAppTour.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                launchAppTour();
            }

        });
    }

    private void launchLockScreen() {
        startActivity(new Intent(activity, com.mphrx.fisike.lock_screen.LockScreenActivity.class).putExtra(VariableConstants.IS_TO_SET_PASSWORD, true).putExtra(VariableConstants.IS_TO_SET_CONFIG, true));
    }

    private void launchAppTour() {
        startActivity(new Intent(activity, AppTourActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        config = ConfigManager.getInstance().getConfig();
        checkPin.setChecked(config.isShowPin());
        btnChangePin.setEnabled(config.isShowPin());
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}