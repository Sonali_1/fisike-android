package com.mphrx.fisike.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.MyHealthItemsEnum;
import com.mphrx.fisike.fragments.adapter.MyHealthAdapter;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.record_screen.fragment.GenralFragment;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.CustomViewPager;
import com.mphrx.fisike.utils.MyHealthTabItemsHolder;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.SlidingTabLayout;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * @MyHealth
 */
public class MyHealth extends Fragment implements OnPageChangeListener, OnClickListener {
    private View myFragmentView;
    public static CustomViewPager mViewPager;
    private AppCompatActivity activity;
    private MyHealthAdapter myHelthAdapter;
    private SlidingTabLayout tabs;
    private FloatingActionMenu fabMenu;
    private FloatingActionButton fabRecordsCamera;
    private FloatingActionButton fabRecordsReport;
    private FloatingActionButton fabUploadCamera;
    private FloatingActionButton fabMedicationCamera;
    private FloatingActionButton fabUploadGallery;
    private FloatingActionButton fabUploadManual;
    private List<ResolveInfo> infos;
    private static LinkedHashMap<String, Integer> healthItemsHashMap = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().getMyHealthTabItemsMap();

    public MyHealth() {

    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = (AppCompatActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.MY_HEALTH_FRAGMENT);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.fragment_my_health, container, false);

        if (BuildConfig.isPatientApp) {
            //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("My Health");
            ((BaseActivity) getActivity()).setToolbarTitle(getActivity().getResources().getString(R.string.title_my_health));
            if (getArguments() != null &&
                    (getArguments().containsKey(VariableConstants.LAUNCH_MEDICATION_SCREEN)
                            || getArguments().containsKey(VariableConstants.LAUNCH_RECORDS_SCREEN)
                            || (getArguments().containsKey(VariableConstants.LAUNCH_UPLOAD_SCREEN) && getArguments().getBoolean(VariableConstants.LAUNCH_UPLOAD_SCREEN)))) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (BuildConfig.isPatientApp) {
                            if((BaseActivity) getActivity()!=null)
                            ((BaseActivity) getActivity()).setToolbarTitle(getActivity().getResources().getString(R.string.title_my_health));
                        }
                    }
                }, 600);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (BuildConfig.isPatientApp) {
                            if((BaseActivity) getActivity()!=null)
                            ((BaseActivity) getActivity()).setToolbarTitle("My Health");
                        }
                    }
                }, 200);
            }
        }
        addFabs();
        initView();

        setHasOptionsMenu(true);

        setAdapter();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(getActivity());
        ContentValues values = new ContentValues();
        if (userMO != null) {
            userMO.setHasSignedUp(false);
            values.put(userDBAdapter.getHASSIGNEDUP(), userMO.getHasSignedUp());
            try {
                userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    private void initView() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT)
            intent.setType("image/*");
        else {
            intent.setType("**//*");
            String[] mimetypes = {"image/*", "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        }
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra("requestCode", -1);
        PackageManager manager = getActivity().getPackageManager();
        infos = manager.queryIntentActivities(intent, 0);

        tabs = (SlidingTabLayout) myFragmentView.findViewById(R.id.sliding_tabs);
        mViewPager = (CustomViewPager) myFragmentView.findViewById(R.id.pager);

        if (BuildConfig.isPatientApp) {
            fabMenu = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);
        }

        mViewPager.setOffscreenPageLimit(3);


        // setting indicator and divider color
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white); // define any color in xml resources and set it here, I have used white
            }

            @Override
            public int getDividerColor(int position) {
                // return getResources().getColor(R.color.white);
                return 0;
            }
        });

        mViewPager.addOnPageChangeListener(this);
    }

    private void setAdapter() {
        myHelthAdapter = new MyHealthAdapter(getChildFragmentManager(), tabs, this);
        mViewPager.setAdapter(myHelthAdapter);


        if (getArguments() != null && getArguments().containsKey(VariableConstants.LAUNCH_RECORDS_SCREEN)) {
            if (getArguments().getBoolean(VariableConstants.LAUNCH_RECORDS_SCREEN)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Records.getTabItemName()));
                    }
                }, 300);

            }
        } else if (getArguments() != null && getArguments().containsKey(VariableConstants.LAUNCH_MEDICATION_SCREEN)) {
            if (getArguments().getBoolean(VariableConstants.LAUNCH_MEDICATION_SCREEN)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Medications.getTabItemName()));
                    }
                }, 300);

            }
        } else if (getArguments() != null && getArguments().containsKey(VariableConstants.LAUNCH_UPLOAD_SCREEN)) {
            if (getArguments().getBoolean(VariableConstants.LAUNCH_UPLOAD_SCREEN)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
                    }
                }, 300);

            }
        } else if (getArguments() != null && getArguments().getBoolean(VariableConstants.DEEP_LINK_FOR_RECORDS_CLICKED, false)) {
            // open records screen
            int recordsIndex = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().
                    getMyHealthTabItemsMap().get(MyHealthItemsEnum.Records.getTabItemName());
            setPage(recordsIndex);
        } else if (getArguments() != null && getArguments().getBoolean(VariableConstants.DEEP_LINK_FOR_MEDICATION_CLICKED, false)) {
            // open medication screen
            int medicationIndex = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().
                    getMyHealthTabItemsMap().get(MyHealthItemsEnum.Medications.getTabItemName());
            mViewPager.setCurrentItem(medicationIndex);
            setPage(medicationIndex);
        }
        if (getArguments() != null && getArguments().containsKey(VariableConstants.NOTIFICATION_REMINDER)) {
            // open medication screen
            int medicationIndex = getTabIndex(MyHealthItemsEnum.Medications);
            mViewPager.setCurrentItem(medicationIndex);
            myHelthAdapter.setMedicationFragment(getArguments());
        } else if (getArguments() != null && getArguments().containsKey(VariableConstants.DIGITIZATION_REMINDER) && getArguments().containsKey(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION)) {
            // open records screen
            int recordsIndex = getTabIndex(MyHealthItemsEnum.Records);
            mViewPager.setCurrentItem(recordsIndex);
            myHelthAdapter.setRecordFragment(getArguments().getString(VariableConstants.DIGITIZATION_MESSAGE_COUNT), getArguments().getString(VariableConstants.DIGITIZATION_MESSAGE_TYPE), getArguments().getInt(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION), getArguments().getBoolean(VariableConstants.DIGITIZATION_REMINDER));
        } else if (getArguments() != null && getArguments().containsKey(VariableConstants.DIGITIZATION_REMINDER)) {
            // open uploads screen
            int uploadsIndex = getTabIndex(MyHealthItemsEnum.Uploads);
            String refId = getArguments().getString(VariableConstants.REFRENCE_ID);
            mViewPager.setCurrentItem(uploadsIndex);
            myHelthAdapter.setUploadFragment(refId);
        } else if (getArguments() != null && getArguments().containsKey(VariableConstants.SCREEN_UPLOAD)
                && getArguments().getBoolean(VariableConstants.SCREEN_UPLOAD)) {
            // open uploads screen
            int uploadsIndex = getTabIndex(MyHealthItemsEnum.Uploads);
            mViewPager.setCurrentItem(uploadsIndex);
            if (getArguments().containsKey(VariableConstants.OPEN_CAMERA)) {
                myHelthAdapter.isOpenCamera(getArguments().getBoolean(VariableConstants.OPEN_CAMERA));
            }
        } else {
            // open the first tab
            // no changes here
            setPage(0);
        }
        tabs.setViewPager(mViewPager);


    }

    private int getTabIndex(MyHealthItemsEnum myHealthItemsEnum) {
        return healthItemsHashMap.get(myHealthItemsEnum.getTabItemName());
    }

    public static CustomViewPager getmViewPager() {
        return mViewPager;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {

            } else {
                Bundle extras = data.getExtras();
                if (extras != null && extras.containsKey(VariableConstants.UPLOAD_IMAGE)) {
                    if (extras.getBoolean(VariableConstants.UPLOAD_IMAGE)) {
                        if (openCamera()) {
                            return;
                        }
                    }
                }
            }
        }
        int currentItem = mViewPager.getCurrentItem();
        Fragment fragment = myHelthAdapter.getFragment(Utils.getMyHealthTabNameBasedOnPosition(currentItem));
        if (fragment instanceof UploadsFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
            fabMenu.close(false);
        } else if (fragment instanceof GenralFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else if (fragment instanceof MedicationOrderFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else if (fragment instanceof VitalsFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public boolean openCamera() {
        myHelthAdapter.isOpenCamera(true);
        mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
        final Fragment fragment = myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName());
        if (fragment instanceof UploadsFragment) {
            mViewPager.postDelayed(new Runnable() {
                public void run() {
                    ((UploadsFragment) fragment).setOpenCamera();
                }
            }, 50);
            return true;
        }
        return false;
    }

    public boolean openUploadGallery() {
        myHelthAdapter.isOpenGalley(true);
        int currentItemPosition = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().getMyHealthTabItemsMap().
                get(MyHealthItemsEnum.Uploads.getTabItemName());
        mViewPager.setCurrentItem(currentItemPosition);
        final Fragment fragment = myHelthAdapter.getFragment(Utils.getMyHealthTabNameBasedOnPosition(currentItemPosition));
        if (fragment instanceof UploadsFragment) {
            mViewPager.postDelayed(new Runnable() {
                public void run() {
                    ((UploadsFragment) fragment).openGalleryIntent();
                }
            }, 50);
            return true;
        }
        return false;
    }


    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int page) {
        setPage(page);
    }

    public void setPage(int tabNumber) {
        String pageName = Utils.getMyHealthTabNameBasedOnPosition(tabNumber);
        if (BuildConfig.isPhysicianApp)
            return;

        removeAllFab();
        if (pageName.equals(MyHealthItemsEnum.Vitals.getTabItemName()) || pageName.equals(MyHealthItemsEnum.Records.getTabItemName())) {
            // vitals tab
            fabMenu.setVisibility(View.GONE);
        } else if (pageName.equals(MyHealthItemsEnum.Records.getTabItemName())) {
            // records screen
            fabMenu.setMenu(true);
            if (infos.size() > 0 && BuildConfig.isUploadsEnabled) {
                fabMenu.addMenuButton(fabRecordsReport);
            }
            if (BuildConfig.isUploadsEnabled)
                fabMenu.addMenuButton(fabRecordsCamera);
            if (myHelthAdapter.getFragment(((MyHealthItemsEnum.Records.getTabItemName()))) == null) {
                mViewPager.setCurrentItem(getTabIndex(MyHealthItemsEnum.Records));
            }
            fabRecordsCamera.setOnClickListener(this);
            if (infos.size() > 0 && BuildConfig.isUploadsEnabled)
                fabRecordsReport.setOnClickListener(this);
            if (fabMenu.getVisibility() != View.VISIBLE && BuildConfig.isUploadsEnabled) {
                fabMenu.setVisibility(View.VISIBLE);
            }
            if (!BuildConfig.isUploadsEnabled) {
                fabMenu.setVisibility(View.GONE);
            }
        } else if (pageName.equals(MyHealthItemsEnum.Medications.getTabItemName())) {
            // medication screen
            fabMenu.setMenu(true);
            fabMenu.addMenuButton(fabUploadManual);
            if (BuildConfig.isUploadsEnabled) {
                fabMenu.addMenuButton(fabMedicationCamera);
                fabMedicationCamera.setOnClickListener(this);
            }

            fabUploadManual.setOnClickListener(this);

            if (fabMenu.getVisibility() != View.VISIBLE) {
                fabMenu.setVisibility(View.VISIBLE);
            }
        } else if (pageName.equals(MyHealthItemsEnum.Uploads.getTabItemName())) {
            // uploads screen
            fabMenu.setMenu(true);
            if (infos.size() > 0)
                fabMenu.addMenuButton(fabUploadGallery);
            fabMenu.addMenuButton(fabUploadCamera);
            if (fabMenu.getVisibility() != View.VISIBLE) {
                fabMenu.setVisibility(View.VISIBLE);
            }
            if (myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName()) == null) {
                mViewPager.setCurrentItem(getTabIndex(MyHealthItemsEnum.Uploads));
            }
            if (getActivity() instanceof BaseActivity && ((BaseActivity) getActivity()).isUploadFragmentRefresh()) {
                ((BaseActivity) getActivity()).setUploadFragmentRefresh(false);
                if (myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName()) != null && myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName()) instanceof UploadsFragment) {
                    ((UploadsFragment) myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName())).onResume();
                }
            }
            if (infos.size() > 0)
                fabUploadGallery.setOnClickListener(this);

            fabUploadCamera.setOnClickListener(this);

        }
    }

    private void addFabs() {
        if (BuildConfig.isPhysicianApp)
            return;
        fabRecordsReport = newFloatingActionBtn(getResources().getString(R.string.upload_file), BaseActivity.RECORDS_REPORT, R.drawable.ic_fab_attachment); // records 1
        fabRecordsCamera = newFloatingActionBtn(getResources().getString(R.string.click_and_upload_file), BaseActivity.RECORDS_CAMERA_FILE, R.drawable.ic_fab_camera); //records 2
        fabMedicationCamera = newFloatingActionBtn(getResources().getString(R.string.click_upload_prescription), BaseActivity.RECORDS_CAMERA_FILE, R.drawable.ic_fab_camera); //medications 2
        fabUploadCamera = newFloatingActionBtn(getResources().getString(R.string.click_and_upload_file), BaseActivity.UPLOAD_CAMERA_FILE, R.drawable.ic_fab_camera);//uploads 2
        fabUploadGallery = newFloatingActionBtn(getResources().getString(R.string.upload_file), BaseActivity.UPLOAD_GALLERY_FILE, R.drawable.ic_fab_attachment); //uploads 1
        fabUploadManual = newFloatingActionBtn(getResources().getString(R.string.add_manually), BaseActivity.RECORDS_MANUAL, R.drawable.ic_fab_add_medicine);// medications 1


    }

    private FloatingActionButton newFloatingActionBtn(String lable, int id, int resourceId) {
        FloatingActionButton floatingActionButton = new FloatingActionButton(getActivity(), id, lable.replaceAll(" ", "_"));
        floatingActionButton.setButtonSize(FloatingActionButton.SIZE_MINI);
        floatingActionButton.setLabelText(lable);
        floatingActionButton.setImageResource(resourceId);
        floatingActionButton.setColorRipple(getResources().getColor(R.color.blue_grey));
        floatingActionButton.setColorNormal(getResources().getColor(R.color.blue_grey));
        floatingActionButton.setColorPressed(getResources().getColor(R.color.blue_grey));
        return floatingActionButton;
    }

    private void removeAllFab() {
        if (BuildConfig.isPhysicianApp)
            return;

        ArrayList<FloatingActionButton> fabList = fabMenu.getFabList();
        for (int i = 0; fabList != null && i < fabList.size(); ) {
            FloatingActionButton floatingActionButton = fabList.get(i);
            fabMenu.removeMenuButton(floatingActionButton);
        }
    }

    @Override
    public void onClick(View view) {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (!userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
            Toast.makeText(getActivity(),
                    getActivity().getResources().getString(R.string.physician_operation_not_allowed), Toast.LENGTH_LONG).show();
            return;
        }
        switch (view.getId()) {
            case BaseActivity.RECORDS_CAMERA_FILE:
                //record - click upload a report
                //medication - click and upload
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), TextConstants.PERMISSION_STORAGE_CODE)) {
                    fabMenu.close(false);
                    openCamera();
                }
                break;


            case BaseActivity.RECORDS_REPORT:
                //records -upload a report
                if (mViewPager.getCurrentItem() != healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName())) {
                    setPage(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
                    mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
                }
                fabMenu.close(false);
                ((UploadsFragment) myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName())).onClick(BaseActivity.UPLOAD_GALLERY_FILE);
                break;


            case BaseActivity.UPLOAD_CAMERA_FILE:
                ((UploadsFragment) myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName())).onClick(BaseActivity.UPLOAD_CAMERA_FILE);
                break;

            case BaseActivity.UPLOAD_GALLERY_FILE:
                //Upload a file (Uploads )
                if (mViewPager.getCurrentItem() != healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName())) {
                    setPage(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
                    mViewPager.setCurrentItem(healthItemsHashMap.get(MyHealthItemsEnum.Uploads.getTabItemName()));
                }
                ((UploadsFragment) myHelthAdapter.getFragment(MyHealthItemsEnum.Uploads.getTabItemName())).onClick(BaseActivity.UPLOAD_GALLERY_FILE);
                break;

            case BaseActivity.RECORDS_MANUAL:
                ((MedicationOrderFragment) myHelthAdapter.getFragment(MyHealthItemsEnum.Medications.getTabItemName())).onClick(view);
                break;

            default:
                break;
        }
    }

    public boolean checkPermission(final String permissionType, String message, final int PERMISSION_REQUEST_CODE) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(getActivity(), new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted(permissions[0], requestCode);
        } else {
            onPermissionNotGranted(permissions[0]);
        }

    }

    public void onPermissionGranted(String permission, int requestCode) {
        switch (requestCode) {
            case TextConstants.PERMISSION_STORAGE_CODE:
                if (BuildConfig.isPatientApp)
                    fabMenu.close(false);
                openCamera();
                break;

            case UploadsFragment.RESULT_GALLERY:
                openUploadGallery();

                break;
            case UploadsFragment.CAMERA_INTENT:
                openCamera();
                break;
        }

    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.permission_uniterrupted_service), Toast.LENGTH_SHORT).show();
    }

    //this will close  advance search form if it is open
    public void hideAdvanceSearchFormIfOpen() {
        int currentIndex = mViewPager.getCurrentItem();
        Fragment fragment = myHelthAdapter.getFragment
                (Utils.getMyHealthTabNameBasedOnPosition(currentIndex));
        if (fragment != null && fragment instanceof GenralFragment)
            ((GenralFragment) fragment).hideAdvanceSearchForm();
    }

    public MyHealthAdapter getMyHelthAdapter() {
        return myHelthAdapter;
    }

    public int getCurrentFragmentVisible() {
        return mViewPager.getCurrentItem();
    }

    public void setBlockVitalScreen() {
        int currentIndex = mViewPager.getCurrentItem();
        Fragment fragment = myHelthAdapter.getFragment
                (Utils.getMyHealthTabNameBasedOnPosition(currentIndex));
    }
}