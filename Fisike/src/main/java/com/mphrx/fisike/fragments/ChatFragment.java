package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.mphrx.fisike.AddContactsActivity;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.NewGroupActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.adapter.MyPagerAdapter;
import com.mphrx.fisike.interfaces.NotifyFabIconToDisplay;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.SlidingTabLayout;

import java.util.ArrayList;

public class ChatFragment extends Fragment implements OnClickListener, NotifyFabIconToDisplay {

    private View myFragmentView;
    private ViewPager vpPager;
    private MyPagerAdapter adapterViewPager;
    private AppCompatActivity activity;
    private SlidingTabLayout tabs;
    private Switch switch1;
    private ProgressBar prToggleStatus;
    private FloatingActionButton btnAddContacts;
    private RelativeLayout layoutBusyStatus;
    private CustomFontTextView txtStatusBusy;
    private LinearLayout layoutList;
    private int dp65;
    private int dp15;
    protected MessengerService mService;
    protected UserMO userMO;

    private Fragment fragmentSelected;

    private boolean mBound;
    private boolean isRegisteredBroadcast;
    private boolean isPatient;
    private Menu menu;

    public ChatFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = (AppCompatActivity) activity;
        mBound = ((BaseActivity) activity).ismBound();
        mService = ((BaseActivity) activity).getmService();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.chat_fragment, container, false);
        initViews();

        registerBroadcastListeners();
        ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.messages));
        setHasOptionsMenu(true);

        setAdapter();

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        vpPager.setPageMargin(pageMargin);

        tabs.setViewPager(vpPager);

        updateStatus();
        Utils.fabInterface = this;
        return myFragmentView;
    }

    @Override
    public void onResume() {
        setAdapter();
        super.onResume();
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.CHAT_FRAGMENT);
    }


    public MyPagerAdapter getAdapterViewPager() {
        return adapterViewPager;
    }

    private void setAdapter() {
        if (adapterViewPager == null) {
            adapterViewPager = new MyPagerAdapter(activity.getSupportFragmentManager(), isPatient, this);
            vpPager.setAdapter(adapterViewPager);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onDestroy() {
        if (!activity.isFinishing()) {
            ArrayList<Fragment> arrayFragment = adapterViewPager.mFragments;

            if (arrayFragment != null) {
                FragmentManager mFragmentMgr = getFragmentManager();
                android.support.v4.app.FragmentTransaction mTransaction = mFragmentMgr.beginTransaction();
                for (Fragment fragment : arrayFragment) {
                    mTransaction.remove(fragment);
                }
                if (getActivity() instanceof HomeActivity) {
                    ((BaseActivity) getActivity()).setPage();
                }

                mTransaction.commitAllowingStateLoss();
            }
        }
        super.onDestroy();
    }

    /**
     * This method registers the broadcast listeners for new messages and delivery status notifications
     */
    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
            broadcastManager.registerReceiver(statusChangeBroadcast, new IntentFilter(VariableConstants.STATUS_CHANGED_SUCESSFULLY));
            isRegisteredBroadcast = true;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (!isPatient) {
            menu.add(Menu.NONE, TextConstants.MENU_ITEM_NEW_GROUP, Menu.NONE, getActivity().getResources().getString(R.string.New_Group)).setIcon(R.drawable.ic_ab_edit_profile_add_member)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        //menu.add(Menu.NONE, TextConstants.MENU_ITEM_PROFILE, Menu.NONE, "Profile").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        //	menu.add(Menu.NONE, TextConstants.MENU_ITEM_LOGOUT, Menu.NONE, "Sign Out").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
//		inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
//		setSearchMenu();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case TextConstants.MENU_ITEM_NEW_GROUP:
                Intent groupChatActivityIntent = new Intent(activity, NewGroupActivity.class);
                startActivityForResult(groupChatActivityIntent, 1);
                break;
            case TextConstants.MENU_ITEM_LOGOUT:
                Utils.menuClickAction(activity, TextConstants.MENU_ITEM_LOGOUT, false, ((HomeActivity) activity).getmService());
                break;
            case TextConstants.MENU_ITEM_PROFILE:
                //Utils.menuClickAction(activity, TextConstants.MENU_ITEM_PROFILE, false, ((HomeActivity) activity).getmService());
                Intent userProfileActivity = new Intent(getActivity(), UserProfileActivity.class);
                // settingsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivity(userProfileActivity);
                break;
        }
        return true;
    }

    /**
     * This method unregisters the two XMPP receivers - for new messages and delivery reciepts
     */
    private void unregisterBroadcastListeners() {
        // Unregister Msg Received listener
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(statusChangeBroadcast);
        // Unregister Delivery Status Listener
        isRegisteredBroadcast = false;
    }

    @Override
    public void onDetach() {
        unregisterBroadcastListeners();
        super.onDetach();
    }

    private void initViews() {
        userMO = SettingManager.getInstance().getUserMO();
        isPatient = (userMO.getUserType().trim()).equalsIgnoreCase(VariableConstants.PATIENT);
        findViews();
        dp65 = (int) Utils.convertDpToPixel(65, activity);
        dp15 = (int) Utils.convertDpToPixel(15, activity);

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    if (!Utils.showDialogForNoNetwork(activity, true)) {
                        switch1.setChecked(true);
                        return;
                    }
                    if (mBound) {
                        String statusMessage = userMO.getStatusMessage() != null ? userMO.getStatusMessage() : "";
                        mService.changeStatus(TextConstants.STATUS_AVAILABLE, statusMessage);
                        switch1.setVisibility(View.GONE);
                        prToggleStatus.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void findViews() {
        tabs = (SlidingTabLayout) myFragmentView.findViewById(R.id.tabs);
        vpPager = (ViewPager) myFragmentView.findViewById(R.id.vpPager);

        switch1 = (Switch) myFragmentView.findViewById(R.id.switch1);

        prToggleStatus = (ProgressBar) myFragmentView.findViewById(R.id.pr_status_update);

        btnAddContacts = (FloatingActionButton) myFragmentView.findViewById(R.id.fab_btn_load_contacts);
        btnAddContacts.setOnClickListener(this);

        layoutBusyStatus = (RelativeLayout) myFragmentView.findViewById(R.id.layoutBusyStatus);
        txtStatusBusy = (CustomFontTextView) myFragmentView.findViewById(R.id.txtStatusBusy);

        layoutList = (LinearLayout) myFragmentView.findViewById(R.id.layoutList);

        // setting indicator and divider color
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white); // define any color in xml resources and set it here, I have used white
            }

            @Override
            public int getDividerColor(int position) {
                // return getResources().getColor(R.color.white);
                return 0;
            }
        });

        // if (isPatient) {
        tabs.setMTabStripInvisible();
        // }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.fab_btn_load_contacts:
                if (Utils.showDialogForNoNetwork(activity, false)) {
                    startActivity(new Intent(activity, AddContactsActivity.class));
                }
                break;
        }
    }

    private BroadcastReceiver statusChangeBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isSucessfullyUpdated = intent.getExtras().getBoolean(TextConstants.STATUS_RESPONSE);
            switch1.setVisibility(View.VISIBLE);
            prToggleStatus.setVisibility(View.GONE);
            if (isSucessfullyUpdated) {
                // ivStatusIndicator.setImageResource(R.drawable.ic_status_available);
                updateStatus();
            } else {
                switch1.setChecked(true);
            }
        }
    };

    private void updateStatus() {
        // setStatus();

        if (userMO.getStatus() != null
                && (userMO.getStatus().equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT) || userMO.getStatus().equalsIgnoreCase(
                TextConstants.STATUS_UNAVAILABLE_TEXT))) {
            try {
                if (!switch1.isChecked()) {
                    switch1.setChecked(true);
                }
            } catch (Exception e) {

            }

            layoutBusyStatus.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, dp15, dp65);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            btnAddContacts.setLayoutParams(params);

            // myFragmentView.findViewById(R.id.layoutListPersonsChat).invalidate();

            Spannable word = new SpannableString(getResources().getString(R.string.your_status) + " ");
            word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtStatusBusy.setText(word);

            if (userMO.getStatus().equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
                Spannable wordTwo = new SpannableString(getResources().getString(R.string.your_status_unavailable));
                wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtStatusBusy.append(wordTwo);
            } else {
                Spannable wordTwo = new SpannableString(getResources().getString(R.string.your_status_busy));
                wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtStatusBusy.append(wordTwo);
            }

            int height = (int) Utils.convertDpToPixel(55, activity);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, height);
            layoutList.setLayoutParams(layoutParams);

        } else {
            layoutBusyStatus.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.setMargins(0, 0, dp15, dp15);
            btnAddContacts.setLayoutParams(params);

            // myFragmentView.findViewById(R.id.layoutListPersonsChat).invalidate();

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            layoutList.setLayoutParams(layoutParams);
        }
    }

    public void setItem(Fragment fragmentSelected) {
        this.fragmentSelected = fragmentSelected;
//		setSearchMenu();
    }

    private void setSearchMenu() {
        if (menu == null || fragmentSelected == null) {
            return;
        }
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            // searchView = (SearchView) searchItem.getActionView();
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.getComponentName()));
        }

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                ((SingleChatFragment) ChatFragment.this.fragmentSelected).getAdapter().getFilter().filter(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                ((SingleChatFragment) ChatFragment.this.fragmentSelected).getAdapter().getFilter().filter(query);
                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
    }

    @Override
    public void setFabIconVisibility(boolean value) {

        if (value)
            btnAddContacts.setVisibility(View.VISIBLE);
        else
            btnAddContacts.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        setFabIconVisibility(false);
        super.onDestroyView();
    }
}