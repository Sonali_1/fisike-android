package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.NewGroupActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.RecentContactListAdapter;
import com.mphrx.fisike.asynctask.contactListAsyncTask;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.ContactListArray;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class SingleChatFragment extends Fragment implements OnItemClickListener {

    private ArrayList<ChatConversationMO> contactList;
    private ListView listPersons;
    public static boolean isMessageListActivityRunning;

    private RecentContactListAdapter adapter;
    private UserMO userMO;
    private static boolean isRegisteredBroadcast = false;
    private static boolean hasContactListApiDone = false;

    private MessengerService mService;
    private boolean mBound = false;

    private HashMap<String, ChatContactMO> chatContactList;
    private AsyncTask<Void, Void, Void> refreshList;

    // private ArrayList<ChatConversationMO> groupList;

    // private ArrayList<ChatConversationMO> comboList;
    private HashMap<String, GroupTextChatMO> groupChatContactList;

    private Activity activity;

    private View myFragmentView;
    private CustomFontTextView txtNoMoreHistory;

    private static final int NEW_CHAT_ICON_STARTING_INDEX = 43;
    private static final int NEW_CHAT_ICON_END_INDEX = 44;
    private ArrayList<ChatContactMO> contacts;
    private int size;
    private RelativeLayout rl_progress;

    private boolean isGroupNamesLoaded;
    private RelativeLayout progressSetupChat;

    public SingleChatFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;

        userMO = SettingManager.getInstance().getUserMO();

        mService = ((BaseActivity) activity).getmService();
        mBound = ((BaseActivity) activity).ismBound();

//        loadGroupNames();

        super.onAttach(activity);
    }

    class LoadingList extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (!isGroupNamesLoaded) {
//                loadGroupNames();
            }
            mService.loadChatConversationsFromDB();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            refreshListBG();
            super.onPostExecute(result);
        }

    }

    private void showProgressDialog() {
       /* AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view1 = layoutInflater.inflate(R.layout.progressdialoglayout, null);
        dialog.setView(view1);
        alert=dialog.show();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));*/
        rl_progress.setVisibility(View.VISIBLE);
        setFabIconVisibility(false);

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.single_chat, container, false);
        findViews();
        setHasOptionsMenu(true);
        lookForContactList();
        if (SharedPref.getSetupChatApiCall()) {
            progressSetupChat.setVisibility(View.VISIBLE);
        }
        return myFragmentView;
    }

    Menu menu;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        if (BuildConfig.isPhysicianApp) {
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        setSearchMenu();
//        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (contactList == null || contactList.size() == 0) {
            if (menu != null && menu.findItem(R.id.action_search) != null)
                menu.findItem(R.id.action_search).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);

    }

    private void setSearchMenu() {
        if (menu == null || getActivity() == null) {
            return;
        }
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            // searchView = (SearchView) searchItem.getActionView();
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.getComponentName()));
        }

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                getAdapter().getFilter().filter(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                getAdapter().getFilter().filter(query);
                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
    }

    private void lookForContactList() {
        if (BuildConfig.isPatientApp) {
            //showProgressDialog();
            txtNoMoreHistory.setVisibility(View.GONE);
            new contactListAsyncTask("", 0, getActivity(), this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onResume() {
        initViews();
        registerBroadcastListeners();
        isMessageListActivityRunning = true;

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        isMessageListActivityRunning = false;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        isMessageListActivityRunning = isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    private void initViews() {

        initDataLoad();
    }


    private void initDataLoad() {
        if (mBound) {
            new LoadingList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void findViews() {
        txtNoMoreHistory = (CustomFontTextView) myFragmentView.findViewById(R.id.txtNoMoreHistory);
        listPersons = (ListView) myFragmentView.findViewById(R.id.listContacts);
        listPersons.setOnItemClickListener(this);
        rl_progress = (RelativeLayout) myFragmentView.findViewById(R.id.rl_progress);
        progressSetupChat = (RelativeLayout) myFragmentView.findViewById(R.id.progressSetupChat);
    }

    public RecentContactListAdapter getAdapter() {
        return adapter;
    }

    /**
     * Update the last user used
     */
    public void setUserInteraction() {
        userMO.setLastAuthenticatedTime(System.currentTimeMillis());
        try {
            //SettingManager.getInstance().updateUserMO(userMO);
            UserDBAdapter.getInstance(getActivity()).updateLastAuthenticatedTime(userMO.getPersistenceKey(), userMO.getLastAuthenticatedTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is fired when the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (refreshList != null && refreshList.isCancelled()) {
            refreshList.cancel(true);
            refreshList = null;
        }
        unregisterBroadcastListeners();
        super.onDestroy();
    }

    /**
     * This method registers the broadcast listeners for new messages and delivery status notifications
     */
    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
            broadcastManager.registerReceiver(updateConversationListener, new IntentFilter(VariableConstants.BROADCAST_CONV_LIST_CHANGED));
            broadcastManager.registerReceiver(updateNoConversationListener, new IntentFilter(VariableConstants.BROADCAST_NO_CONV_TO_LOAD));
            broadcastManager.registerReceiver(msgReceivedListener, new IntentFilter(VariableConstants.NEW_MSG_BROADCAST));
            broadcastManager.registerReceiver(deliveryStatusListener, new IntentFilter(VariableConstants.BROADCAST_DELIVERY_STATUS));
            broadcastManager.registerReceiver(refreshData, new IntentFilter(VariableConstants.BROADCST_REFRESH_DATA));
            broadcastManager.registerReceiver(updateGroupConversationListener, new IntentFilter(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED));
            broadcastManager.registerReceiver(statusChangeBroadcast, new IntentFilter(VariableConstants.STATUS_CHANGED_SUCESSFULLY));
            isRegisteredBroadcast = true;
        }
    }

    /**
     * Broadcast for handling conversation list changes
     */
    private BroadcastReceiver updateGroupConversationListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh list when this notification is received
            refreshGroupListBG();
        }
    };

    private BroadcastReceiver statusChangeBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isSucessfullyUpdated = intent.getExtras().getBoolean(TextConstants.STATUS_RESPONSE);
            mService.dismissDialog();
        }
    };

    private void refreshGroupListBG() {
        refreshList = new RefreshList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    /**
     * This method unregisters the two XMPP receivers - for new messages and delivery reciepts
     */
    public void unregisterBroadcastListeners() {
        // Unregister Msg Received listener
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(msgReceivedListener);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(deliveryStatusListener);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(updateConversationListener);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(refreshData);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(updateGroupConversationListener);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(statusChangeBroadcast);
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(updateNoConversationListener);
        isRegisteredBroadcast = false;
    }

    /**
     * Broadcast for handling conversation list changes
     */
    private BroadcastReceiver updateConversationListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh list when this notification is received
            refreshList();
        }
    };

    private BroadcastReceiver updateNoConversationListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (!SharedPref.getSetupChatApiCall() && progressSetupChat != null && progressSetupChat.getVisibility() == View.VISIBLE) {
                        progressSetupChat.setVisibility(View.GONE);
                    }
                }
            });
        }
    };

    /**
     * Broadcast for handling received message notifications
     */
    private BroadcastReceiver msgReceivedListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // This will directly refresh the list once its received
            refreshList();
        }
    };

    /**
     * RefreshData
     */
    private BroadcastReceiver refreshData = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            mService = ((BaseActivity) activity).getmService();
            mBound = ((BaseActivity) activity).ismBound();
            initDataLoad();
        }
    };

    /**
     * Broadcast Receiver for handling delivery status notifications
     */
    private BroadcastReceiver deliveryStatusListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // We will get only the Chat Conversation MO - as it only matters to us if the last message was updated
            ChatConversationMO chatConvMO = (ChatConversationMO) intent.getExtras().get(VariableConstants.BROADCAST_CHAT_CONV_MO);
            if (chatConvMO.getLastMsgStatus() == DefaultConnection.MESSAGE_DELIVERED) {
                // Only update the list if the last message status was delivered
                refreshList();
            }
        }
    };

    private void refreshListBG() {
        refreshList = new RefreshList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    /**
     * Refresh contacts list and sort -- now gets the list from in-memory MessengerService
     */
    private void refreshList() {
        Comparator<ChatConversationMO> comparator = new Comparator<ChatConversationMO>() {

            public int compare(ChatConversationMO object1, ChatConversationMO object2) {
                if (object1 == null || object1.getCreatedTimeUTC() > object2.getCreatedTimeUTC()) {
                    return -1;
                } else if (object2 == null || object1.getCreatedTimeUTC() < object2.getCreatedTimeUTC()) {
                    return 1;
                }
                return 0;
            }
        };

        if (mBound) {
            // Get the latest list from the MessengerService
            contactList = mService.getConversationsList();
            contactList.size();

            for (int i = contactList.size() - 1; i >= 0; i--) {
                if (contactList.get(i).getConversationType() == VariableConstants.conversationTypeChat && (contactList.get(i) == null || contactList.get(i).getChatContactMoKeys() == null || contactList.get(i).getIsChatInitiated() == 0)
                        ) {
                    contactList.remove(i);
                }
            }

            ArrayList<ChatConversationMO> groupList = new ArrayList<ChatConversationMO>();

            for (int i = 0; contactList != null && i < contactList.size(); i++) {
                ChatConversationMO chatConversationMO = contactList.get(i);

                String nickName = chatConversationMO.getNickName();
//                String jId = contactList.get(i).getJId();

                if (TextUtils.isEmpty(nickName)) {
                    if (!TextUtils.isEmpty(chatConversationMO.getGroupName())) {
                        chatConversationMO.setNickName(chatConversationMO.getGroupName());
                    }
                }
//                if (null == nickName || nickName.contains("@" + VariableConstants.groupConferenceName)) {
//                    if (jId.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
//                        chatConversationMO.setGroupName(VariableConstants.SINGLE_CHAT_IDENTIFIER);
//                    } else {
//                        mService.getGroupName(jId);
//                    }
//                }
                if (chatConversationMO.getConversationType() == VariableConstants.conversationTypeGroup) {
                    groupList.add(chatConversationMO);
                }
            }
            if (contactList != null && contactList.size() > 1) {
                Collections.sort(contactList, comparator);
            }

            chatContactList = mService.getContactList(contactList, VariableConstants.conversationTypeChat);
            groupChatContactList = mService.getGroupList(groupList, VariableConstants.conversationTypeGroup);

            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (contactList == null || contactList.size() == 0) {
                        // txtNoMoreHistory.setText(SingleChatFragment.this.getResources().getString(R.string.no_chat_found));
                        rl_progress.setVisibility(View.GONE);
                        txtNoMoreHistory.setVisibility(View.VISIBLE);
                        listPersons.setVisibility(View.GONE);
                        setEmptyRosterText();
                    } else {
                        txtNoMoreHistory.setVisibility(View.GONE);
                        listPersons.setVisibility(View.VISIBLE);
                    }
                    activity.invalidateOptionsMenu();
                    if (null == adapter) {
                        adapter = new RecentContactListAdapter(contactList, chatContactList, groupChatContactList, activity, userMO.getId() + "",
                                userMO.getUserType());

                        listPersons.setAdapter(adapter);
                    } else {
                        // adapter.clear();
                        adapter.setChatContactList(chatContactList);
                        adapter.setContactList(contactList);
                    }
                    if (BuildConfig.isPhysicianApp) {
                        rl_progress.setVisibility(View.GONE);
                    }
                    // listPersons.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    listPersons.setOnItemClickListener(SingleChatFragment.this);
                }
            });
        }
    }

    private void loadGroupNames() {

        if (mService != null) {

            isGroupNamesLoaded = true;
            ChatConversationDBAdapter mHelper = ChatConversationDBAdapter.getInstance(getActivity());

            ArrayList<String> chatConversationKeyList = userMO.getChatConversationKeyList();

            for (int index = 0; chatConversationKeyList != null && index < chatConversationKeyList.size(); index++) {
                String persistenceKey = chatConversationKeyList.get(index);
                ChatConversationMO chatConvMO = null;
                try {
                    chatConvMO = mHelper.fetchTargettedConversationMO(persistenceKey);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (chatConvMO == null || chatConvMO.getJId() == null) {
                    continue;
                }
                String user = chatConvMO.getJId();
//                if(TextUtils.isEmpty(chatConvMO.getGroupName()) || chatConvMO.getGroupName().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER))
                mService.getGroupName(user);
            }
        }

    }

    class RefreshList extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            refreshList();

            return null;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            ChatConversationMO chatConversationMO = adapter.getContactList().get(position);
            int unReadCount = chatConversationMO.getUnReadCount();
            chatConversationMO.setReadStatus(true);
            adapter.notifyDataSetChanged();

            boolean isGroupChat = chatConversationMO.getConversationType() == VariableConstants.conversationTypeGroup ? true : false;

            String userName = Utils.cleanXMPPUserName(chatConversationMO.getJId());
            String phoneNumber = chatConversationMO.getPhoneNumber();
            unregisterBroadcastListeners();

            Intent intent = new Intent(activity, MessageActivity.class);

            String nickName = chatConversationMO.getNickName();
            if (TextUtils.isEmpty(nickName)) {
                if (!TextUtils.isEmpty(chatConversationMO.getGroupName())) {
                    nickName = chatConversationMO.getGroupName();
                }
            }

            if (isGroupChat) {
                int myStatus = chatConversationMO.getMyStatus();
                String subString = new String(userName);
                GroupTextChatMO groupTextChatMO = new GroupTextChatMO();
                subString = Utils.extractGroupId(subString);
                groupTextChatMO.generatePersistenceKey(subString, userMO.getId() + "");
                userName = chatConversationMO.getJId();
                groupTextChatMO = GroupChatDBAdapter.getInstance(getActivity()).fetchGroupTextChatMO(groupTextChatMO.getPersistenceKey() + "");
                intent.putExtra(VariableConstants.GROUP_INFO, groupTextChatMO).putExtra(VariableConstants.MY_STATUS_ACTIVE, myStatus);
                intent.putExtra(VariableConstants.SENDER_SECOUNDRY_ID, chatConversationMO.getJId())
                        .putExtra("groupName", chatConversationMO.getGroupName());
            } else {
                intent.putExtra(VariableConstants.MY_STATUS_ACTIVE, chatConversationMO.getMyStatus());
            }
            intent.putExtra(VariableConstants.SENDER_ID, userName).putExtra(VariableConstants.PHONE_NUMBER, phoneNumber)
                    .putExtra(VariableConstants.SENDER_NICK_NAME, nickName)
                    .putExtra(VariableConstants.SENDER_UNREAD_COUNT, unReadCount).putExtra(TextConstants.IS_RECENT_CHAT, true)
                    .putExtra(VariableConstants.IS_GROUP_CHAT, isGroupChat);
            if (!isGroupChat) {
                intent.putExtra(VariableConstants.SENDER_SECOUNDRY_ID, chatConversationMO.getSecoundryJid());
            }
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case TextConstants.MENU_ITEM_NEW_GROUP:
                Intent groupChatActivityIntent = new Intent(activity, NewGroupActivity.class);
                startActivity(groupChatActivityIntent);
                break;
            case TextConstants.MENU_ITEM_LOGOUT:
                Utils.menuClickAction(activity, TextConstants.MENU_ITEM_LOGOUT, false, mService);
                break;
            case TextConstants.MENU_ITEM_PROFILE:
                Utils.menuClickAction(activity, TextConstants.MENU_ITEM_PROFILE, false, mService);
                break;
            case R.id.action_search:
                handleIntent(activity.getIntent());
        }
        return true;
    }

    private void handleIntent(Intent intent) {
//        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
        String query = intent.getStringExtra(SearchManager.QUERY);
        getAdapter().getFilter().filter(query);
//            Toast.makeText(activity, "Inside Search view : " + query, Toast.LENGTH_LONG).show();
//        }
    }

    public static Fragment newInstance(int i) {
        SingleChatFragment fragmentFirst = new SingleChatFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private void setEmptyRosterText() {
        try {
            String text;
            if (BuildConfig.isPatientApp) {
                text = getActivity().getResources().getString(R.string.still_to_start_chat);
                SpannableString ss = new SpannableString(text);
                Drawable d = getActivity().getResources().getDrawable(R.drawable.ic_chat_small);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
                ss.setSpan(span, NEW_CHAT_ICON_STARTING_INDEX, NEW_CHAT_ICON_END_INDEX, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                txtNoMoreHistory.setText(ss);
                rl_progress.setVisibility(View.GONE);
            } else {
                text = getActivity().getResources().getString(R.string.still_to_start_chat_physician);
                txtNoMoreHistory.setText(text);
            }


            if (hasContactListApiDone) {
                if (size == 0) {
                    setFabIconVisibility(false);
                    setNoContactText();
                } else {
                    setFabIconVisibility(true);
                    txtNoMoreHistory.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {

        }

    }

    private void setFabIconVisibility(boolean value) {
        if (Utils.fabInterface != null)
            Utils.fabInterface.setFabIconVisibility(value);

    }

    public void loadContacts(String jsonString) {
        try {

            txtNoMoreHistory.setVisibility(View.VISIBLE);

            /*alert.dismiss();*/
            rl_progress.setVisibility(View.GONE);
            if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
                size = 0;
                return;
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ContactListArray.class);
            if (jsonToObjectMapper instanceof ContactListArray) {
                if (((ContactListArray) jsonToObjectMapper).getResultList() != null)
                    size = ((ContactListArray) jsonToObjectMapper).getResultList().size();
                else
                    size = 0;

            }
            if (size == 0) {
                setFabIconVisibility(false);
                setNoContactText();
            } else {
                setFabIconVisibility(true);
                txtNoMoreHistory.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            // FIXME
            try {
                e.printStackTrace();
                Toast.makeText(getActivity(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                setFabIconVisibility(true);
                setEmptyRosterText();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        hasContactListApiDone = true;
    }

    private void setNoContactText() {
        String text;
        if (BuildConfig.isPatientApp) {
            text = getActivity().getResources().getString(R.string.no_contacts);
        } else {
            text = getActivity().getResources().getString(R.string.still_to_start_chat_physician);

        }
        txtNoMoreHistory.setText(text);

    }


}
