package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.PdfPreview;
import com.mphrx.fisike.Queue.UploadRecordObject;
import com.mphrx.fisike.Queue.UploadRecordQueue;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UploadsTaggingActivity;
import com.mphrx.fisike.asynctask.StoreUplodedRecordAsyncTask;
import com.mphrx.fisike.background.EncriptFile;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.enums.UploadTaggingEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.models.TypeModel;
import com.mphrx.fisike.utils.Citadel;
import com.mphrx.fisike.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class UploadTaggingFragment extends Fragment implements OnCheckedChangeListener, OnClickListener {


    private View myFragmentView;
    private ImageView imgThumb;
    private String attachmentPath;
    private Bitmap attachmentBitmap;
    private com.mphrx.fisike.customview.CustomFontButton btnSubmit;
    private String category = "";
    private ProgressDialog dialog;
    private boolean isToScanAnotherDoc;
    private byte[] decodFile;
    private int itemPosition;
    private boolean hasTappedScanAnother = false;
    private int mimeType;
    private String toEncriptFilePath;
    private boolean isToDecode;

    private List<TypeModel> list = new ArrayList<TypeModel>();
    private int currentTypeSelected;
    private IconTextView img_cross;
    private RelativeLayout btn_cross;
    private CustomFontButton btnType1, btnType2, btnType3, btnType4, btnType5, btnType6, btnType7;
    private String cameraOrGallery = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.upload_tagging_fragment, container, false);
        setHasOptionsMenu(true);
        String[] myResArray = getResources().getStringArray(R.array.document_type_array);
        for (String type : myResArray) {
            TypeModel typeModel = new TypeModel();
            typeModel.setType(type.equals(getActivity().getResources().getString(R.string.Others)) ? getActivity().getResources().getString(R.string.Document) : type);
            typeModel.setViewType(type.equals(getActivity().getResources().getString(R.string.Radiology_Images)) ? getActivity().getResources().getString(R.string.Radiology_Image) : type);
            typeModel.setSelected(false);
            list.add(typeModel);
        }


        initView();
        if (mimeType == VariableConstants.MIME_TYPE_DOCX || mimeType == VariableConstants.MIME_TYPE_DOC || mimeType == VariableConstants.MIME_TYPE_FILE) {
            setDocText();
        } else {
            setTextImage();
        }

        myFragmentView.findViewById(R.id.layoutParent).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        return myFragmentView;
    }

    /**
     * Set the text to show for doc file
     */
    private void setDocText() {
        int color = getActivity().getResources().getColor(R.color.bg_play_color);
        File file = new File(toEncriptFilePath);
        Spannable word = new SpannableString(file.getName());
        word.setSpan(new ForegroundColorSpan(color), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        word.setSpan(new AbsoluteSizeSpan((int) Utils.convertDpToPixel(17, getActivity())), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        Spannable wordTwo = new SpannableString("\n\n" + getFolderSize(file));
        wordTwo.setSpan(new ForegroundColorSpan(Color.GRAY), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordTwo.setSpan(new AbsoluteSizeSpan((int) Utils.convertDpToPixel(14, getActivity())), 0, wordTwo.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * Calculate size of file
     *
     * @param file
     * @return
     */

    private String getFolderSize(File file) {
        String value = null;
        long fileSize = file.length() / 1024;
        if (fileSize >= 1024) {
            return fileSize / 1024 + " Mb";
        } else {
            return fileSize + " Kb";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hasTappedScanAnother = false;
    }

    private void handleDocandDocx() {
        myFragmentView.findViewById(R.id.layoutPreview).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(Uri.parse("file://" + toEncriptFilePath), "application/msword");
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    PackageManager packageManager1 = getActivity().getPackageManager();
                    List<ResolveInfo> listGall = packageManager1.queryIntentActivities(intent, 0);
                    int size = listGall.size();
                    for (int i = 0; i < size; i++) {
                        ResolveInfo res = listGall.get(i);
                        if (res.activityInfo.packageName.equals("com.google.android.apps.docs.editors.docs")) {
                            intent.setPackage(res.activityInfo.packageName);
                            startActivity(intent);
                            return;
                        }
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void initView() {

        btnType1 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType1);
        btnType1.setOnClickListener(this);
        btnType2 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType2);
        btnType2.setOnClickListener(this);
        btnType3 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType3);
        btnType3.setOnClickListener(this);
        btnType4 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType4);
        btnType4.setOnClickListener(this);
        btnType5 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType5);
        btnType5.setOnClickListener(this);
        btnType6 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType6);
        btnType6.setOnClickListener(this);
        btnType7 = (CustomFontButton) myFragmentView.findViewById(R.id.btnType7);
        btnType7.setOnClickListener(this);

        img_cross = (IconTextView) myFragmentView.findViewById(R.id.img_cross);
        img_cross.setOnClickListener(this);

        btn_cross = (RelativeLayout) myFragmentView.findViewById(R.id.btn_crosss);
        btn_cross.setOnClickListener(this);
        GradientDrawable bgShape = (GradientDrawable) btn_cross.getBackground();
        bgShape.setColor(getActivity().getResources().getColor(R.color.overflow_color));

        setType();

        cameraOrGallery = getArguments().getString("fromWhere");
        imgThumb = (ImageView) myFragmentView.findViewById(R.id.imgThumb);
        btnSubmit = (com.mphrx.fisike.customview.CustomFontButton) myFragmentView.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        attachmentPath = getArguments().getString(VariableConstants.VIDEO_URI);
        mimeType = getArguments().getInt(VariableConstants.MIME_TYPE);
        toEncriptFilePath = getArguments().getString(VariableConstants.PDF_FILE_PATH);

        switch (mimeType) {
            case VariableConstants.MIME_TYPE_IMAGE:
                if (getActivity() instanceof UploadsTaggingActivity) {

                    attachmentBitmap = MyApplication.getTempImagePreview();
                    attachmentBitmap = Utils.getCurvedCornerBitmap(attachmentBitmap, 50);
                    System.gc();

                    if (attachmentBitmap != null) {
                        Drawable d = new BitmapDrawable(getResources(), attachmentBitmap);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            imgThumb.setBackground(d);
                        } else {
                            imgThumb.setBackgroundDrawable(d);
                        }
                    }
                } else {
                    itemPosition = getArguments().getInt(VariableConstants.ITEM_SYNC_MESSAGE);
                    String imgThumbPath = getArguments().getString(VariableConstants.IMAGE_THUMP_PATH);

                    byte[] decodFile = Citadel.decodFile(imgThumbPath, getActivity());
                    if (decodFile != null) {
                        attachmentBitmap = BitmapFactory.decodeByteArray(decodFile, 0, decodFile.length);
                    }
                    if (attachmentBitmap != null) {
                        imgThumb.setImageBitmap(attachmentBitmap);
                    }

                }
                break;
            case VariableConstants.MIME_TYPE_FILE:
                if (toEncriptFilePath == null) {
                    isToDecode = true;
                }
                encriptFile();
                imgThumb.setBackgroundResource(R.drawable.doc_type_pdf);
                myFragmentView.findViewById(R.id.layoutPreview).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setDataAndType(Uri.parse("file://" + toEncriptFilePath), "application/pdf");
                        intent.addCategory("android.intent.category.DEFAULT");
                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                            PackageManager packageManager1 = getActivity().getPackageManager();
                            List<ResolveInfo> listGall = packageManager1.queryIntentActivities(intent, 0);
                            int size = listGall.size();
                            for (int i = 0; i < size; i++) {
                                ResolveInfo res = listGall.get(i);
                                if (res.activityInfo.packageName.equals("com.google.android.apps.docs")) {
                                    intent.setPackage(res.activityInfo.packageName);
                                    startActivity(intent);
                                    return;
                                }
                            }
                            startActivity(intent);
                        } else {
                            getActivity().startActivity(new Intent(getActivity(), PdfPreview.class).putExtra(VariableConstants.PDF_FILE_PATH, toEncriptFilePath).putExtra(VariableConstants.MIME_TYPE, mimeType));
                        }
                    }
                });
                break;
            case VariableConstants.MIME_TYPE_DOC:
                if (toEncriptFilePath == null) {
                    isToDecode = true;
                }
                encriptFile();
                imgThumb.setBackgroundResource(R.drawable.doc_type_doc);
                handleDocandDocx();
                break;
            case VariableConstants.MIME_TYPE_DOCX:
                if (toEncriptFilePath == null) {
                    isToDecode = true;
                }
                encriptFile();
                imgThumb.setBackgroundResource(R.drawable.doc_type_docx);
                handleDocandDocx();

                break;

        }


    }

    private void setTextImage() {

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                if (category.equals("") || category == null || category.equals(getActivity().getResources().getString(R.string.txt_null))) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.pls_select_one_type), Toast.LENGTH_SHORT).show();
                } else {
                    isToScanAnotherDoc = false;
                    submitDocument();
                }
                break;

            case R.id.img_cross:
                Intent intent = new Intent();
                intent.putExtra("fromWhere", cameraOrGallery);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                if (getActivity() != null && getActivity() instanceof UploadsTaggingActivity)
                    ((UploadsTaggingActivity) getActivity()).deleteFile();
                break;

            case R.id.btnType1:
                setSelectedType(0);
                break;

            case R.id.btnType2:
                setSelectedType(1);
                break;

            case R.id.btnType3:
                setSelectedType(2);
                break;

            case R.id.btnType4:
                setSelectedType(3);
                break;

            case R.id.btnType5:
                setSelectedType(4);
                break;

            case R.id.btnType6:
                setSelectedType(5);
                break;

            case R.id.btnType7:
                setSelectedType(6);
                break;
            default:
                break;
        }
    }

    private void encriptFile() {
        EncriptFile encriptBitmapFile = new EncriptFile(getActivity(), null, this, toEncriptFilePath);
        encriptBitmapFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setAttachmentBitmap(Bitmap attachmentBitmap) {
        this.attachmentBitmap = attachmentBitmap;
    }

    private void submitDocument() {
        DocumentReferenceModel documentReferenceModel = new DocumentReferenceModel();
        documentReferenceModel.setDocumentLocalPath(attachmentPath);
        documentReferenceModel.setPatientID(Utils.getPatientId());
        documentReferenceModel.setDocumentType(category);
        documentReferenceModel.setMimeType(mimeType);
        documentReferenceModel.setTimeStamp(System.currentTimeMillis() + "");

        switch (mimeType) {
            case VariableConstants.MIME_TYPE_IMAGE:
                if (getActivity() instanceof MessageActivity) {
                    ((MessageActivity) getActivity()).setRecordUploaded(itemPosition, VariableConstants.RECORED_SYNC_STATUS_SUBMITED);

                    getActivity().getSupportFragmentManager().beginTransaction().remove(UploadTaggingFragment.this).commit();
                }
                if (attachmentBitmap != null) {
                    Bitmap imageBitmap = ThumbnailUtils.extractThumbnail(attachmentBitmap, 150, 150);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageData = baos.toByteArray();
                    documentReferenceModel.setThumbImage(imageData);
                }
                break;
            case VariableConstants.MIME_TYPE_FILE:
                break;
        }
        if (Utils.isNetworkAvailable(getActivity())) {
            documentReferenceModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_IN_PROCESS);
            new StoreUplodedRecordAsyncTask(getActivity(), documentReferenceModel, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            MyApplication.setUploadStaus(VariableConstants.DOCUMENT_STATUS_IN_PROCESS);
            UploadRecordQueue uploadRecordQueue = UploadRecordQueue.getInstance();
            UploadRecordObject uploadRecordObject;
            if (decodFile != null) {
                isToDecode = true;
            }
            uploadRecordObject = new UploadRecordObject(getActivity(), attachmentBitmap, attachmentPath, category, isToDecode, toEncriptFilePath, mimeType);
            uploadRecordQueue.addTask(uploadRecordObject.getAttachmentPath(), uploadRecordObject);
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_upload_record), Toast.LENGTH_SHORT).show();
            documentReferenceModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_REDDY);
            new StoreUplodedRecordAsyncTask(getActivity(), documentReferenceModel, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    public void setAttachmentPath(String attachmentPath, String toEncriptFilePath) {
        this.attachmentPath = attachmentPath;
        this.toEncriptFilePath = toEncriptFilePath;
    }

    public void dismissProgressDialog(DocumentReferenceModel documentReferenceModel) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if (getActivity() instanceof UploadsTaggingActivity) {
            Intent intent = new Intent();
            intent.putExtra(TextConstants.FRAGMENT_KEY, isToScanAnotherDoc);
            intent.putExtra(VariableConstants.IS_CAMERA_INTENT, getArguments().getBoolean(VariableConstants.IS_CAMERA_INTENT));
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.show();
    }

    public void setDecodedFile(byte[] decodFile) {
        this.decodFile = decodFile;
    }


    public void setSelectedType(int position) {
        //category = "Lab Report";
        //  category = "Note";
        // category = "Prescription";
        list.get(currentTypeSelected).setSelected(false);
        currentTypeSelected = position;
        category = list.get(position).getType();
        list.get(position).setSelected(true);
        setType();
    }

    public void setType() {
        LinkedHashMap<String, UploadTaggingEnum> uploadTaggingDisplayValueMap = UploadTaggingEnum.getUploadTaggingEnumLinkedHashMap();
        for (int i = 0; i < list.size(); i++) {
            String btnText = (uploadTaggingDisplayValueMap != null &&
                    uploadTaggingDisplayValueMap.containsKey(list.get(i).getViewType())) ?
                    uploadTaggingDisplayValueMap.get(list.get(i).getViewType()).getValue() :
                    list.get(i).getViewType();
            switch (i) {
                case 0:
                    btnType1.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType1.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType1.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType1.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType1.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 1:
                    btnType2.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType2.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType2.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType2.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType2.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 2:
                    btnType3.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType3.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType3.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType3.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType3.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 3:
                    btnType4.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType4.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType4.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType4.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType4.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 4:
                    btnType5.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType5.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType5.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType5.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType5.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 5:
                    btnType6.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType6.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType6.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType6.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType6.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;

                case 6:
                    btnType7.setText(btnText);
                    if (list.get(i).isSelected()) {
                        btnType7.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_blue_selector));
                        btnType7.setTextColor(getActivity().getResources().getColor(R.color.white));
                    } else {
                        btnType7.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.cancel_selector));
                        btnType7.setTextColor(getActivity().getResources().getColor(R.color.overflow_color));
                    }
                    break;
            }
        }

    }





   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            {
                if(getActivity()instanceof HomeActivity)
                {
                    getActivity().onBackPressed();
                return true;
                }
            }
        }
        return false;
    }*/


}