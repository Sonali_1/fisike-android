package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.VitalsAdapter;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.customview.MySwipeRefreshLayout;
import com.mphrx.fisike.enums.VitalLionCodeEnum;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.persistence.VitalsDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.WrapContentLinearLayoutManager;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike_physician.network.request.VitalsRequest;
import com.mphrx.fisike_physician.network.response.VitalsResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 7/28/2016.
 */
public class VitalsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private MySwipeRefreshLayout swipeRefreshLayout;
    private long mTransactionId;
    private View myFragmentView;
    private RecyclerView recyclerview_grid_vitals;
    private Map<String, VitalsModel> vitalsModelList = new HashMap<String, VitalsModel>();
    private List<VitalsModel> list_bloodglucose = new ArrayList<VitalsModel>();
    private VitalsAdapter vitalsAdapter;
    private Activity activity;
    private boolean isRegisteredBroadcast;
    private boolean allVitalNull;
    VitalsConfigMO vitalsConfigMO;
    boolean isToShowContactAdminScreen = false;

    public VitalsFragment() {
        try {
            vitalsConfigMO = VitalsConfigDBAdapter.getInstance(getActivity()).fetchVitalsConfigMO();
        } catch (Exception e) {
            vitalsConfigMO = null;
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedPref.getRefreshRecords() && BuildConfig.isPatientApp) {
            SharedPref.setRefreshRecords(false);
            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                swipeRefreshLayout.setRefreshing(true);
                fetchVitalsFromServer();
            }
        }
    }

    private BroadcastReceiver timeZoneChangedBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (BuildConfig.isPatientApp) {
                    fetchVitalsfromDB(true, null);
                } else if (BuildConfig.isPhysicianApp) {
                    if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                        fetchVitalsFromServer();
                    }
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.vitals_fragment, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        BusProvider.getInstance().register(this);

        findView();
        if (BuildConfig.isPatientApp) {
            fetchVitalsfromDB(true, null);
        }
        initView();
        registerBroadcast();
        return myFragmentView;
    }

    private void registerBroadcast() {
        if (isRegisteredBroadcast) {
            return;
        }
        getActivity().registerReceiver(timeZoneChangedBroadcast, new IntentFilter(VariableConstants.TIME_ZONE_CHANGED));
        isRegisteredBroadcast = true;
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        if (timeZoneChangedBroadcast != null && isRegisteredBroadcast) {
            try {
                getActivity().unregisterReceiver(timeZoneChangedBroadcast);
            } catch (Exception ex) {
            }
        }
        super.onDestroyView();
    }


    private void findView() {
        recyclerview_grid_vitals = (RecyclerView) myFragmentView.findViewById(R.id.recyclerview_grid_vitals);
        swipeRefreshLayout = (MySwipeRefreshLayout) myFragmentView.findViewById(R.id.swipe_container);
        vitalsAdapter = new VitalsAdapter(getActivity(), vitalsModelList, list_bloodglucose);
        recyclerview_grid_vitals.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
        recyclerview_grid_vitals.setAdapter(vitalsAdapter);
    }


    private void initView() {

        // sets the colors used in the refresh animation
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            if (BuildConfig.isPhysicianApp) {
                swipeRefreshLayout.setRefreshing(true);
            } else {
                swipeRefreshLayout.setRefreshing(true);
            }
            fetchVitalsFromServer();
        }
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }


    @Subscribe
    public void onVitalsResponse(final VitalsResponse response) {

        try {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (mTransactionId != response.getTransactionId())
                return;

            if (response.isSuccessful() && response.getVitalsModelArrayList() != null && response.getVitalsModelArrayList().size() > 0) {

                new AsyncTask<Void, Void, Void>() {
                    VitalsDBAdapter vitalsinstance = VitalsDBAdapter.getInstance(getActivity());

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (BuildConfig.isPatientApp) {
                            ArrayList<VitalsModel> vitalsModelArrayList = response.getVitalsModelArrayList();
                            for (int i = 0; i < vitalsModelArrayList.size(); i++) {
                                {
                                    final VitalsModel vitalsModel = vitalsModelArrayList.get(i);
                                    try {
                                        vitalsinstance.insertOrUpdateVitalsModel(vitalsModel);
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        if (BuildConfig.isPatientApp) {
                            fetchVitalsfromDB(true, null);
                        } else if (BuildConfig.isPhysicianApp) {
                            if (response.isSuccessful() && response.getVitalsModelArrayList() != null && response.getVitalsModelArrayList().size() > 0) {
                                fetchVitalsfromDB(false, response.getVitalsModelArrayList());
                            }
                        }
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (!response.isSuccessful() || response.getVitalsModelArrayList() == null) {
                recyclerview_grid_vitals.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), getResources().getString(R.string.api_error), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private boolean isGlucose(String paramId) {
        if (paramId.equals(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode()) ||
                paramId.equals(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode()) ||
                paramId.equals(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode()))
            return true;
        else return false;
    }

    private boolean isBMI(String paramId) {
        if (paramId.equals(VitalLionCodeEnum.BMI_CODE.getCode()))
            return true;
        else return false;
    }

    private boolean isBP(String paramId) {
        if (paramId.equals(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()) || paramId.equals(VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode()))
            return true;
        else return false;
    }

    private void addElementInVitalModelList(VitalsModel vitalsModel, boolean isToAddBMI, boolean isToAddBP, boolean isToAddGlucosr) {
        if ((BuildConfig.isPhysicianApp)) {
            vitalsModelList.put(vitalsModel.getParamId(), vitalsModel);

            if (vitalsModel.getParamId().equals(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode()) && !vitalsModel.getDate().equals("")) {
                list_bloodglucose.add(vitalsModel);
            }

            if (vitalsModel.getParamId().equals(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode()) && !vitalsModel.getDate().equals("")) {
                list_bloodglucose.add(vitalsModel);
            }

            if (vitalsModel.getParamId().equals(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode()) && !vitalsModel.getDate().equals("")) {
                list_bloodglucose.add(vitalsModel);
            }

            return;
        }

        if (isToAddGlucosr && isGlucose(vitalsModel.getParamId())) {
            vitalsModelList.put(vitalsModel.getParamId(), vitalsModel);
            if (!vitalsModel.getDate().equals(""))
                list_bloodglucose.add(vitalsModel);
        } else if (isToAddBMI && isBMI(vitalsModel.getParamId()))
            vitalsModelList.put(vitalsModel.getParamId(), vitalsModel);
        else if (isToAddBP && isBP(vitalsModel.getParamId()))
            vitalsModelList.put(vitalsModel.getParamId(), vitalsModel);
    }

    public void fetchVitalsfromDB(boolean isFetchFromDB, ArrayList<VitalsModel> vitalsModelArrayList) {
        vitalsConfigMO = VitalsConfigMO.getInstance();
        if (vitalsAdapter == null) {
            return;
        }
        VitalsDBAdapter vitalsinstance = VitalsDBAdapter.getInstance(getActivity());
        vitalsModelList.clear();
        list_bloodglucose.clear();
        boolean isToAddBMI = vitalsConfigMO != null ? vitalsConfigMO.isToShowAddBMIButton() : false;
        boolean isToAddGlucose = vitalsConfigMO != null ? vitalsConfigMO.isToShowAddGlucoseButton() : false;
        boolean isToAddBP = vitalsConfigMO != null ? vitalsConfigMO.isToShowAddBPButton() : false;


        if (isFetchFromDB) {
            for (VitalLionCodeEnum vitalLionCodeEnum : VitalLionCodeEnum.values()) {
                try {
                    synchronized (this) {
                        VitalsModel vitalsModel = (VitalsModel) vitalsinstance.fetchVitals(vitalLionCodeEnum.getCode());
                        VitalsModel vitalsModelNew = new VitalsModel();
                        if (vitalsModel != null)
                            addElementInVitalModelList(vitalsModel, isToAddBMI, isToAddBP, isToAddGlucose);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (int i = 0; i < vitalsModelArrayList.size(); i++) {
                addElementInVitalModelList(vitalsModelArrayList.get(i), isToAddBMI, isToAddBP, isToAddGlucose);
            }
        }

        Collections.sort(list_bloodglucose, new Comparator<VitalsModel>() {
            @Override
            public int compare(VitalsModel o1, VitalsModel o2) {
                try {
                    if ((o1.getDate().equals("") || o1.getDate().equals(getActivity().getResources().getString(R.string.txt_null)) || o1.getDate() == null) && (o2.getDate().equals("") || (o2.getDate().equals(getActivity().getResources().getString(R.string.txt_null)) || o2.getDate() == null)))
                        return 0;

                    if ((o1.getDate().equals("") || o1.getDate().equals(getActivity().getResources().getString(R.string.txt_null)) || o1.getDate() == null))
                        return 1;

                    if ((o2.getDate().equals("") || o2.getDate().equals(getActivity().getResources().getString(R.string.txt_null)) || o2.getDate() == null))
                        return -1;

                    SimpleDateFormat ft = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd_hh_mm_ss_sss_a, Locale.US);
                    SimpleDateFormat ft1 = new SimpleDateFormat(DateTimeUtil.YYYY_MM_dd_hh_mm_ss_sss_a, Locale.US);
                    Date date1 = null, date2 = null;
                    try {
                        date1 = ft.parse(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.YYYY_MM_dd_hh_mm_ss_sss_a, o1.getDate().toString().trim()));
                        date2 = ft1.parse(CommonTasks.formatDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.YYYY_MM_dd_hh_mm_ss_sss_a, o2.getDate().toString().trim()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return date1.compareTo(date2);
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });

        try {
            Collections.reverse(list_bloodglucose);
        } catch (Exception e) {

        }
        recyclerview_grid_vitals.getRecycledViewPool().clear();
        vitalsAdapter.notifyDataSetChanged();

        if (BuildConfig.isPatientApp) {
            VitalsConfigMO instance = VitalsConfigMO.getInstance();
            if (instance == null || (isAllVitalNull(instance))) {
                if (isToShowContactAdminScreen)
                    showContactAdminScreen();
                else
                    showTryAgainScreen();
            } else {
                hideTryAgainScreen();
            }
        }

    }

    /**
     * Check if the vital response is null for retying the api
     *
     * @param instance
     * @return
     */
    private boolean isAllVitalNull(VitalsConfigMO instance) {
        if (instance == null || (instance.getBMI() == null || !instance.isToShowAddBMIButton()) && (instance.getBloodPressure() == null || !instance.isToShowAddBPButton()) && (instance.getGlucose() == null || !instance.isToShowAddGlucoseButton())) {
            return true;
        }
        return false;
    }

    @Override
    public void onRefresh() {

        boolean isInternetAvailable = Utils.showDialogForNoNetwork(getActivity(), false);
        if (isInternetAvailable) {
            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                fetchVitalsFromServer();
            }
        } else {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void fetchVitalsFromServer() {
        if (BuildConfig.isPatientApp) {
            swipeRefreshLayout.setRefreshing(true);
            ThreadManager.getDefaultExecutorService().submit(new VitalsRequest(getTransactionId(), getActivity()));
        } else if (BuildConfig.isPhysicianApp) {
            ThreadManager.getDefaultExecutorService().submit(new VitalsRequest(getTransactionId(), getActivity(), String.valueOf(getArguments().getString("patientId"))));
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VitalsAdapter.OBSERVATION_SELECT) {
            if (resultCode == getActivity().RESULT_OK) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                        boolean isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                        if (isToRefresh) {
                            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                                swipeRefreshLayout.setRefreshing(true);
                                fetchVitalsFromServer();
                            }
                        }
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initializeView(String messageText, String buttonText) {
        CustomFontTextView textMessage = ((CustomFontTextView) myFragmentView.findViewById(R.id.block_msg));
        CustomFontButton btnRetry = (CustomFontButton) myFragmentView.findViewById(R.id.btn_retry);
        myFragmentView.findViewById(R.id.layout_block_vital).setVisibility(View.VISIBLE);
        myFragmentView.findViewById(R.id.scrollLayout).setBackgroundColor(getResources().getColor(R.color.card_bg));
        textMessage.setTextColor(getResources().getColor(R.color.bluey_grey));
        textMessage.setText(messageText);
        recyclerview_grid_vitals.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setEnabled(false);
        btnRetry.setText(buttonText);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClick((CustomFontButton) v);
            }
        });
    }

    /*
     *
     * If the data is API failure and no data in ConfigMo
     */
    public void showTryAgainScreen() {
        initializeView(getString(R.string.vital_retry_text, getString(R.string.app_name)),
                getString(R.string.try_again));
    }

    private void setOnClick(CustomFontButton btnRetry) {

        /*
        remove network check at this level to resolve
        multiple dialog on try again click
        */
        if (btnRetry.getText().toString().equals(getString(R.string.contact_us))) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(getActivity(), SettingManager.getInstance().getUserMO()));
            intent.setData(Uri.parse(getActivity().getResources().getString(R.string.mailto)));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
            if (Utils.showDialogForNoNetwork(getActivity(), false) && intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        } else if (Utils.showDialogForNoNetwork(getActivity(), false))
            ((HomeActivity) getActivity()).checkVitalsConfigResponse();

    }

    /*
    If the data validation data for all the vital fail
     */
    public void showContactAdminScreen() {
        isToShowContactAdminScreen = true;
        initializeView(getString(R.string.vital_contact_admin, getString(R.string.app_name)), getString(R.string.contact_us));
    }

    /**
     * If the vital api is sucessfull then fetch from db
     */
    public void getVitalConfig() {
        hideTryAgainScreen();
        if (BuildConfig.isPatientApp)
        {
            fetchVitalsFromServer();
        }
    }

    /**
     * Show recycleview and hide the block screen, if not null then find from db
     */
    private void hideTryAgainScreen() {
        /**
         * If the find view had not run
         */
        if (recyclerview_grid_vitals == null) {
            return;
        }
        myFragmentView.findViewById(R.id.layout_block_vital).setVisibility(View.INVISIBLE);
        recyclerview_grid_vitals.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setEnabled(true);
        if(vitalsAdapter!=null)
            recyclerview_grid_vitals.getRecycledViewPool().clear();
            vitalsAdapter.notifyDataSetChanged();
    }

}
