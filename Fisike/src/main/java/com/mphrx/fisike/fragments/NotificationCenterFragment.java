package com.mphrx.fisike.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.NotificationCenterAdapter;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.persistence.NotificationCenterDBAdapter;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.view.DividerItemDecoration;

import java.util.ArrayList;

import appointment.OrderStatusActivity;
import appointment.phlooba.HomeDrawStatusActivity;

/**
 * This fragment is for showing the notification list for recent medication and document shared
 *
 * @author Kailash
 */
public class NotificationCenterFragment extends Fragment {

    private View myFragmentView;
    private RecyclerView listNotification;
    private CustomFontTextView txtNoAlarm;
    private boolean isRegisteredBroadcast;

    private BroadcastReceiver alarmListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh list when this notification is received
            refreshList();
        }
    };

    public NotificationCenterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.notification_center_fragment, container, false);
        listNotification = (RecyclerView) myFragmentView.findViewById(R.id.listNotification);
        txtNoAlarm = (CustomFontTextView) myFragmentView.findViewById(R.id.txtNoAlarm);

        ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.txt_notification));

        if (getArguments() != null && getArguments().containsKey(VariableConstants.DIGITIZATION_MESSAGE_COUNT)) {
            // push notification handling
        }

        setHasOptionsMenu(true);

        refreshList();

        return myFragmentView;
    }

    private void refreshList() {
        getNotificationData();

        SharedPreferences preferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        Editor edit = preferences.edit();
        edit.putBoolean(VariableConstants.ALARM_READ, true);
        edit.putInt(VariableConstants.ALARM_COUNT_UNREAD, 0);
        edit.putBoolean(VariableConstants.ALARM_COUNT_MEDICATION, false);
        edit.commit();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutFragment:
                break;
        }
    }

    @Override
    public void onResume() {
        registerBroadcastListeners();
        super.onResume();
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.NOTIFICATION_CENTRE_FRAGMENT);
    }

    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getActivity());
            broadcastManager.registerReceiver(alarmListener, new IntentFilter(VariableConstants.BROADCAST_REFRESH_ALARM));
            isRegisteredBroadcast = true;
        }
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastListeners();
        super.onDestroy();
    }

    private void unregisterBroadcastListeners() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(alarmListener);
    }

    /**
     * Get the notification data form the database
     */
    private void getNotificationData() {
        new Thread(new Runnable() {
            public void run() {
                NotificationCenterDBAdapter notificationCenterdbAdapter = NotificationCenterDBAdapter.getInstance(getActivity());
                try {
                    ArrayList<NotificationCenter> fetchNotificationCenter = notificationCenterdbAdapter.fetchNotificationCenter();
                    setAdapter(fetchNotificationCenter);
                } catch (Exception e) {
                }
            }
        }).start();
    }

    /**
     * Set list adapter to the notification list view
     *
     * @param fetchNotificationCenter
     */
    private void setAdapter(final ArrayList<NotificationCenter> fetchNotificationCenter) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                listNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
                listNotification.addItemDecoration(new DividerItemDecoration(getActivity()));
                if (fetchNotificationCenter.size() > 0) {
                    txtNoAlarm.setVisibility(View.INVISIBLE);
                } else {
                    txtNoAlarm.setVisibility(View.VISIBLE);
                }
                NotificationCenterAdapter adapter = new NotificationCenterAdapter(getActivity(), fetchNotificationCenter);
                adapter.setClickListener(new NotificationCenterAdapter.clickListener() {
                    @Override
                    public void itemClicked(View view, int position) {
                        NotificationCenter notificationCenter = fetchNotificationCenter.get(position);

                        if (notificationCenter.getDocumentType() != null && notificationCenter.getDocumentType().equalsIgnoreCase(VariableConstants.DIAGNOSTIC_ORDER)) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(VariableConstants.DIGITIZATION_REMINDER, true);
                            if (notificationCenter.getDocumentId() != null && !notificationCenter.getDocumentId().trim().equals("")) {
                                bundle.putInt(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION, Integer.parseInt(notificationCenter.getDocumentId()));
                            }
                            bundle.putString(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION, notificationCenter.getDocumentType());
                            bundle.putString(VariableConstants.ORGANISATION_ID, notificationCenter.getOrganizationId());

                            ((BaseActivity) getActivity()).openFragmentRecord(bundle);
                            return;
                        } else if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_MEDICATION)) {
                            new FetchMedicationBG(Integer.parseInt(notificationCenter.getDocumentId()), notificationCenter.getMedicationTime()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            return;
                        } else if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_APPOINTMENT)) {
                            Intent resultIntent = new Intent(getActivity(), OrderStatusActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putLong("appointmentID", Long.parseLong(notificationCenter.getDocumentId()));
                            resultIntent.putExtra("bundle", bundle);
                            startActivity(resultIntent);
                            return;
                        } else if (notificationCenter.getNotificationType().equals(VariableConstants.MRN_NOTIFICATION)){
                            return;
                        }
                        ((BaseActivity) getActivity()).openFragmentRecord(notificationCenter.getRefId(), notificationCenter.getDocumentType());
                    }
                });
                listNotification.setAdapter(adapter);
            }
        });
    }

    private class FetchMedicationBG extends AsyncTask<Void, Void, Void> {

        private PrescriptionModel medicationPrescriptionModel;
        private int id;
        private String timming;
        private ProgressDialog progressDialog;

        public FetchMedicationBG(int id, String timming) {
            this.id = id;
            this.timming = timming;
        }


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                medicationPrescriptionModel = EncounterMedicationDBAdapter.getInstance(getActivity()).fetchMedicationPrescription(id);
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (medicationPrescriptionModel != null) {
                ArrayList<MedicineDoseFrequencyModel> arrayDose = medicationPrescriptionModel.getArrayDose();
                for (MedicineDoseFrequencyModel doseFrequencyModel : arrayDose) {
                    int hr = Integer.parseInt(doseFrequencyModel.getDoseHours());
                    if (doseFrequencyModel.getDoseTimeAMorPM()!=null && doseFrequencyModel.getDoseTimeAMorPM().equalsIgnoreCase(getResources().getString(R.string.txt_PM))) {
                        hr += 12;
                    }
                    String tempTimming = hr + ":" + doseFrequencyModel.getDoseMinutes() + ":" + " 00";
                    if (tempTimming.equalsIgnoreCase(timming)) {
                        if (medicationPrescriptionModel != null) {
                            ((BaseActivity) getActivity()).openMedicationFragment(medicationPrescriptionModel, doseFrequencyModel);
                        }
                    }
                }
            } else {
                Toast.makeText(NotificationCenterFragment.this.getActivity(), MyApplication.getAppContext().getResources().getString(R.string.seems_medicine_deleted), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
    }
}