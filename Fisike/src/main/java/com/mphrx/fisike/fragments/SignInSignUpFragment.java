package com.mphrx.fisike.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.CreateNewAccountActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.VolleyRequest.ForgetPasswordRequest.ForgetPasswordSendOTPApi;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.network.request.LoginRequest;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

/**
 * Created by Aastha on 16/02/2016.
 */
public class SignInSignUpFragment extends BaseFragment {

    CustomEditTextWhite edPassword;
    Button btNext;
    Button btForgetPassword;
    Button btNewAccount;
    long mTransactionId;

    TextView tvAccSecure;
    private boolean hasTouched;
    String loginId;
    public OnGetTransactionIdListener mListener;

    public interface OnGetTransactionIdListener {
        public void setIdInActivity(long id);

        public void setDefaultPassword(String password);
    }

    public static SignInSignUpFragment getInstance(Bundle bundle) {
        SignInSignUpFragment fragment = new SignInSignUpFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.passwordscreen;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mListener = (OnGetTransactionIdListener) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hasTouched = false;
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");
        else {
            edPassword = (CustomEditTextWhite) getView().findViewById(R.id.et_pass);
            edPassword.requestFocus();
            btNext = (Button) getView().findViewById(R.id.btn_next);
            btNext.setText(getResources().getString(R.string.sign_in));
            btForgetPassword = (Button) getView().findViewById(R.id.btn_forget_password);
            btNewAccount = (Button) getView().findViewById(R.id.btn_bottom);
            tvAccSecure = (TextView) getView().findViewById(R.id.tv_acc_secure);
            if (BuildConfig.isForgetPassword) {
                btForgetPassword.setVisibility(View.VISIBLE);
            } else {
                btForgetPassword.setVisibility(View.GONE);
            }
            if (SharedPref.getIsMobileEnabled()) {
                btNewAccount.setVisibility(View.VISIBLE);
                btNewAccount.setText(getActivity().getString(R.string.not_having_acnt, getActivity
                        ().getString(R.string.app_title)));
            } else if ((!SharedPref.getIsMobileEnabled())) {
                btNewAccount.setVisibility(View.GONE);
            }

            if (!SharedPref.getIsSelfSignUpEnabled())
                btNewAccount.setVisibility(View.GONE);
            tvAccSecure.setText(R.string.acc_secure);
        }
    }

    @Override
    public void initView() {
        if (SharedPref.getAdminCreatedPasswordNeverChanged()) {
            edPassword.setHint(SharedPref.getDefaultPasswordType());
        } else if (SharedPref.getUserType().equals(SharedPref.UserType.PARTIALLY_REGISTERED.getTypeString
                ()) && !BuildConfig.isPatientApp) {
            edPassword.setHint(R.string.forgot_password_new_password);
        } else if (SharedPref.getUserType().equals(SharedPref.UserType.REGISTERED.getTypeString())) {
            edPassword.setHint(getActivity().getResources().getString(R.string.password));
        } else if ((SharedPref.isSignedUpUser() || !SharedPref.isVerifiedUser()) && BuildConfig.isPatientApp) {
            edPassword.setHint(getActivity().getResources().getString(R.string.password));
        }
    }

    @Override
    public void bindView() {

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.showDialogForNoNetwork(getActivity(), false))
                    onClickSubmit();
            }
        });

        btForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasTouched) {
                    hasTouched = true;
                    hideKeyboard();
                    String keyNumber = new String();
                    if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                        showProgressDialog(getResources().getString(R.string.progress_loading));
                        if (SharedPref.getIsMobileEnabled())
                            keyNumber = SharedPref.getMobileNumber();
                        else if (!SharedPref.getIsMobileEnabled())
                            keyNumber = SharedPref.getEmailAddress();
                        ThreadManager.getDefaultExecutorService().submit(new
                                ForgetPasswordSendOTPApi(keyNumber, SharedPref.getDeviceUid(), getTransactionId(),SharedPref.getCountryCode()));

                    } else
                        hasTouched = false;
                }
            }

        });

        edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        edPassword.setPasswordKeyListener();

        btNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasTouched) {
                    hasTouched = true;
                    Intent newAccount = new Intent(getActivity(), CreateNewAccountActivity.class);
                    startActivity(newAccount);
                }
            }
        });

    }

    @Override
    protected void onShowKeyboard(int keyboardHeight) {
        super.onShowKeyboard(keyboardHeight);
        btNewAccount.setVisibility(View.GONE);
    }

    @Override
    protected void onHideKeyboard() {
        super.onHideKeyboard();
        btNewAccount.setVisibility(View.VISIBLE);

    }

    private void onClickSubmit() {
        hideKeyboard();

        if (!checkPermission(Manifest.permission.READ_PHONE_STATE, getActivity().getResources().getString(R.string.We_need_your_device_ID)))
            return;
        loginWithNumberAndPassword();
    }


    private void loginWithNumberAndPassword() {
        loginId = new String();
        String password = edPassword.getText().toString().trim();
        if (password.equals("")) {
            edPassword.setError(MyApplication.getAppContext().getResources().getString(R.string.enter_yourpassword));
            return;
        } else
            mListener.setDefaultPassword(password);
        showProgressDialog(getResources().getString(R.string.progress_loading));
        mTransactionId = getTransactionId();

        if (SharedPref.getIsMobileEnabled())
            loginId = SharedPref.getMobileNumber();
        else if (SharedPref.getIsUserNameEnabled())
            loginId = SharedPref.getLoginUserName();
        else if (!SharedPref.getIsMobileEnabled())
            loginId = SharedPref.getEmailAddress();
        ThreadManager.getDefaultExecutorService().submit(new LoginRequest(loginId, password,
                mTransactionId));
        mListener.setIdInActivity(mTransactionId);
    }

    public void setError(String error) {
        edPassword.setError(error);
    }
}


