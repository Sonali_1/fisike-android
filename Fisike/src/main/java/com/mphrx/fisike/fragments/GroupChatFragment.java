package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.NewGroupActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.RecentContactListAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class GroupChatFragment extends Fragment implements OnItemClickListener {
    private ArrayList<ChatConversationMO> groupList;
    private ListView listGroup;

    private RecentContactListAdapter adapterGroup;
    private UserMO userMO;
    private static boolean isRegisteredBroadcast = false;

    private MessengerService mService;
    private boolean mBound = false;

    private HashMap<String, GroupTextChatMO> groupChatConatList;
    private AsyncTask<Void, Void, Void> refreshGroup;

    private Activity activity;

    public GroupChatFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;

        userMO = SettingManager.getInstance().getUserMO();

        mService = ((BaseActivity) activity).getmService();
        mBound = ((BaseActivity) activity).ismBound();

        super.onAttach(activity);
    }

    private View myFragmentView;
    private CustomFontTextView txtNoMoreHistory;

    class LoadingList extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            mService.loadChatConversationsFromDB();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    refreshGroupListBG();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (groupList == null || groupList.size() == 0) {
                txtNoMoreHistory.setText(GroupChatFragment.this.getResources().getString(R.string.no_chat_found));
                txtNoMoreHistory.setVisibility(View.VISIBLE);
            } else {
                txtNoMoreHistory.setVisibility(View.GONE);
            }
            super.onPostExecute(result);
        }
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.group_chat, container, false);

        return myFragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onResume() {
        initViews();

        registerBroadcastListeners();
        super.onResume();
    }

    private void initViews() {
        findViews();

        initDataLoad();
    }

    private void initDataLoad() {
        // refreshGroupList();
        new LoadingList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void findViews() {
        txtNoMoreHistory = (CustomFontTextView) myFragmentView.findViewById(R.id.txtNoMoreHistory);

        listGroup = (ListView) myFragmentView.findViewById(R.id.groupContacts);
        listGroup.setOnItemClickListener(GroupChatFragment.this);
    }

    /**
     * Update the last user used
     */
    public void setUserInteraction() {
        userMO.setLastAuthenticatedTime(System.currentTimeMillis());
        try {
//            SettingManager.getInstance().updateUserMO(userMO);
            UserDBAdapter.getInstance(getActivity()).updateLastAuthenticatedTime(userMO.getPersistenceKey(),userMO.getLastAuthenticatedTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is fired when the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (refreshGroup != null && refreshGroup.isCancelled()) {
            refreshGroup.cancel(true);
            refreshGroup = null;
        }
        unregisterBroadcastListeners();
        super.onDestroy();
    }

    /**
     * This method registers the broadcast listeners for new messages and delivery status notifications
     */
    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
            broadcastManager.registerReceiver(updateGroupConversationListener, new IntentFilter(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED));
            isRegisteredBroadcast = true;
        }

    }

    /**
     * This method unregisters the two XMPP receivers - for new messages and delivery reciepts
     */
    public void unregisterBroadcastListeners() {
        // Unregister Msg Received listener
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(updateGroupConversationListener);
        isRegisteredBroadcast = false;
    }

    /**
     * Broadcast for handling conversation list changes
     */
    private BroadcastReceiver updateGroupConversationListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh list when this notification is received
            refreshGroupListBG();
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            ChatConversationMO chatConversationMO = adapterGroup.getContactList().get(position);
            int unReadCount = chatConversationMO.getUnReadCount();
            chatConversationMO.setReadStatus(true);
            adapterGroup.notifyDataSetChanged();

            int myStatus = chatConversationMO.getMyStatus();

            String userName = chatConversationMO.getJId();// 1432544588066_phy1%40abis.33mail.com@conference.dev1
            String phoneNumber = chatConversationMO.getPhoneNumber();
            unregisterBroadcastListeners();

            String subString = new String(userName);

            GroupTextChatMO chatContactMO = new GroupTextChatMO();
            subString = Utils.extractGroupId(subString);
            chatContactMO.generatePersistenceKey(subString, userMO.getId()+"");

            chatContactMO = groupChatConatList.get(chatContactMO.getPersistenceKey() + "");// -1153091850

            startActivity(new Intent(activity, MessageActivity.class).putExtra(VariableConstants.SENDER_ID, userName)
                    .putExtra(VariableConstants.PHONE_NUMBER, phoneNumber)
                    .putExtra(VariableConstants.SENDER_NICK_NAME, chatConversationMO.getNickName())
                    .putExtra(VariableConstants.SENDER_UNREAD_COUNT, unReadCount).putExtra(TextConstants.IS_RECENT_CHAT, true)
                    .putExtra(VariableConstants.IS_GROUP_CHAT, true).putExtra(VariableConstants.MY_STATUS_ACTIVE, myStatus)
                    .putExtra(VariableConstants.GROUP_INFO, chatContactMO));
        } catch (Exception e) {
        }
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
        case TextConstants.MENU_ITEM_NEW_GROUP:
            Intent groupChatActivityIntent = new Intent(activity, NewGroupActivity.class);
            startActivity(groupChatActivityIntent);
            break;
        case TextConstants.MENU_ITEM_SETTINGS:
            Utils.menuClickAction(activity, TextConstants.MENU_ITEM_SETTINGS, false, mService);
            break;
        case TextConstants.MENU_ITEM_LOGOUT:
            Utils.menuClickAction(activity, TextConstants.MENU_ITEM_LOGOUT, false, mService);
            break;
        case TextConstants.MENU_ITEM_PROFILE:
            Utils.menuClickAction(activity, TextConstants.MENU_ITEM_PROFILE, false, mService);
            break;
        }
        return true;
    }

    private void refreshGroupList() {
        Comparator<ChatConversationMO> comparator = new Comparator<ChatConversationMO>() {

            public int compare(ChatConversationMO object1, ChatConversationMO object2) {
                if (object2 == null) {
                    return 1;
                }
                if (object1 == null) {
                    return -1;
                }
                if (object1.getCreatedTimeUTC() > object2.getCreatedTimeUTC()) {
                    return -1;
                } else if (object1.getCreatedTimeUTC() < object2.getCreatedTimeUTC()) {
                    return 1;
                }
                return 0;
            }
        };

        if (mBound) {
            // Get the latest list from the MessengerService
            groupList = mService.getConversationsList();
            groupList.size();

            for (int i = 0; i < groupList.size(); i++) {
                String groupname = groupList.get(i).getNickName();
                if (null == groupname || groupname.contains("@" + VariableConstants.groupConferenceName)) {
                    mService.getGroupName(groupList.get(i).getJId());
                }
            }

            // Sorting the list of conversations
            Collections.sort(groupList, comparator);

            groupChatConatList = mService.getGroupList(groupList, VariableConstants.conversationTypeGroup);

            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (groupList == null || groupList.size() == 0) {
                        txtNoMoreHistory.setText(GroupChatFragment.this.getResources().getString(R.string.no_chat_found));
                        txtNoMoreHistory.setVisibility(View.VISIBLE);
                    } else {
                        txtNoMoreHistory.setVisibility(View.GONE);
                    }
                    /*if (null == adapterGroup) {
                        adapterGroup = new RecentContactListAdapter(groupList, groupChatConatList, true, activity, userMO.getUserName());
                        listGroup.setAdapter(adapterGroup);
                    } else {
                        adapterGroup.setGroupChatContactList(groupChatConatList);
                        adapterGroup.setContactList(groupList);
                    }*/
                    adapterGroup.notifyDataSetChanged();
                    listGroup.setOnItemClickListener(GroupChatFragment.this);
                }
            });
        }
    }

    private void refreshGroupListBG() {
        refreshGroup = new RefreshGroup().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    class RefreshGroup extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            refreshGroupList();
            return null;
        }
    }

    public static Fragment newInstance(int i) {
        GroupChatFragment fragmentFirst = new GroupChatFragment();
        // Bundle args = new Bundle();
        // args.putInt("someInt", i);
        // fragmentFirst.setArguments(args);
        return fragmentFirst;
    }
}
