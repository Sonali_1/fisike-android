package com.mphrx.fisike.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.PdfPreview;
import com.mphrx.fisike.Queue.UploadRecordObject;
import com.mphrx.fisike.Queue.UploadRecordQueue;
import com.mphrx.fisike.R;
import com.mphrx.fisike.UploadsTaggingActivity;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.adapter.UploadedRecyclerAdapter;
import com.mphrx.fisike.asynctask.UpdateDatabaseAsyncTask;
import com.mphrx.fisike.background.DecriptFile;
import com.mphrx.fisike.background.EncriptFile;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.dbasyn.FetchUploadedData;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.enums.MyHealthItemsEnum;
import com.mphrx.fisike.imageCropping.Crop;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.CustomViewPager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.SlidingTabLayout;
import com.mphrx.fisike.view.WrapContentLinearLayoutManager;
import com.mphrx.fisike_physician.network.request.DownloadFileRequest;
import com.mphrx.fisike_physician.network.request.UploadRequest;
import com.mphrx.fisike_physician.network.response.UploadResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ExifUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class UploadsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener, UploadedRecyclerAdapter.clickListener {
    public static final int CAMERA_INTENT = 100;
    public static final int RESULT_GALLERY = 101;
    private static final int FRAGMENT_TAGING = 102;
    private static final int PDF_PREVIEW_INTENT = 103;
    private static final int DOC_PREVIEW_INTENT = 104;
    private static final int PAGINATION_RECORD_COUNT = 10;
    private static final int UPLOAD_REQUEST_CODE = 11;
    public static final int DOWNLOAD_FILE = 105;
    private static boolean isHandled = true;
    private View myFragmentView;
    private int CAMERA_RESULT = 2;
    private com.mphrx.fisike.customview.CustomFontButton btnUploadNow;
    private com.mphrx.fisike.customview.CustomFontButton btnUploadFile;
    private Activity activity;
    private String imageFileName;
    private File cameraImageFile;
    private LinearLayout layoutUploadData;
    private RecyclerView listRecords;
    private UploadedRecyclerAdapter uplodedRecordsAdapter;
    private boolean isRegisteredBroadcast;
    private boolean isCameraUploading;
    private boolean isScanAnotherCamera;
    private FloatingActionMenu fabMenu;
    private MyObserver observer;
    private SlidingTabLayout tabs;
    private int mimeType = VariableConstants.MIME_TYPE_IMAGE;

    private boolean isLoading;
    private SwipeRefreshLayout swipeRefreshLayout;
    private long mTransactionId;
    private ListView recordListView;
    private View loadingView;
    private LinearLayout loadingViewContainer;
    private boolean isAllUploadLoaded;
    private int uploadTotalCount;
    private String uploadServerSyncTime;
    private boolean isNotificationHandled;
    private boolean isSyncApi;
    private ArrayList<DocumentReferenceModel> documentReferenceModelList;
    private boolean isToHitPaginationSyncCallAfterFetchData = false;

    private BroadcastReceiver uploadRecordBroadCast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.hasExtra(VariableConstants.ATTACHMENT_PERCENTAGE)) {
                int percentage = intent.getExtras().getInt(VariableConstants.ATTACHMENT_PERCENTAGE);
                String attachmentPath = intent.getExtras().getString(VariableConstants.ATTACHMENT_PATH);
                int position = uplodedRecordsAdapter.getPosition(attachmentPath, percentage);
            } else {
                new FetchUploadedData(activity, UploadsFragment.this, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                if (!isLoading) {
                    swipeRefreshLayout.setRefreshing(true);
                    paginationSyncApiCall();
                }

            }
        }
    };

    private BroadcastReceiver uploadRecordDownloadedBroadCast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.hasExtra(VariableConstants.DOCUMENT_DOWNLOADED_MODEL)) {
                DocumentReferenceModel documentReferenceModel = (DocumentReferenceModel) intent.getExtras().get(VariableConstants.DOCUMENT_DOWNLOADED_MODEL);
                uplodedRecordsAdapter.refreshRow(documentReferenceModel);
            }
        }
    };
    private CustomFontTextView tv_physician_no_text;
    private LinearLayout ll_patient_defaut;
    private String cameraOrGallery;
    private int positionClicked;


    public UploadsFragment(SlidingTabLayout tabs) {
        this.tabs = tabs;
    }

    public UploadsFragment() {

    }

    public static UploadsFragment getInstance() {
        UploadsFragment fragment = new UploadsFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    private void setViewPagerSwipeEnabled(boolean isSwipeable) {
        CustomViewPager customViewPager = MyHealth.mViewPager;
        if (customViewPager != null) {
            customViewPager.setPagingEnabled(isSwipeable);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.UPLOADS_FRAGMENT);
        Utils.deleteDir(new File(Environment.getExternalStorageDirectory(), "/" + getString(R.string.app_name) + "/.temp"));
        setViewPagerSwipeEnabled(true);
        if (SharedPref.isToRefreshUploads())
            paginationSyncApiCall();
    }

    private void paginationApiCall() {
        if (uploadTotalCount > 0 && uplodedRecordsAdapter != null && uplodedRecordsAdapter.getItemCount() >= uploadTotalCount) {
            isAllUploadLoaded = true;
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(SharedPreferencesConstant.ALL_UPLOADED_LOADED, true);
            editor.commit();
            uplodedRecordsAdapter.setFooterVisible(false);
            uplodedRecordsAdapter.notifyDataSetChanged();
            return;
        }
        if (!isLoading) {
            ThreadManager.getDefaultExecutorService().submit(new UploadRequest(paginationJson(), getTransactionId(), getActivity(), false));
            uplodedRecordsAdapter.setFooterVisible(true);
            uplodedRecordsAdapter.notifyDataSetChanged();
            isLoading = true;
        }
    }

    private void syncApiCall() {
        if (!isLoading) {
            swipeRefreshLayout.setRefreshing(true);
            ThreadManager.getDefaultExecutorService().submit(new UploadRequest(syncJson(), getTransactionId(),
                    getActivity(), false));
            isLoading = true;
            isSyncApi = true;
        }
    }

    @Subscribe
    public void onUploadResponse(UploadResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        isLoading = false;
        uplodedRecordsAdapter.setFooterVisible(false);
        uplodedRecordsAdapter.notifyDataSetChanged();

        if (response.isSuccessful() && response.getDocumentReferenceModel() != null && response.getDocumentReferenceModel().size() > 0) {
            if (uplodedRecordsAdapter == null || uplodedRecordsAdapter.getItemCount() == 0) {

                ArrayList<DocumentReferenceModel> documentReferenceModel = response.getDocumentReferenceModel();
                documentReferenceModelList.clear();
                documentReferenceModelList.addAll(documentReferenceModel);
                uplodedRecordsAdapter.notifyDataSetChanged();

                listRecords.setVisibility(View.VISIBLE);
                layoutUploadData.setVisibility(View.GONE);


                  /*[   Edited by laxman   ]*/
                if (BuildConfig.isPatientApp) {
                    MyHealth myHealthFragment = (MyHealth) getParentFragment();
                    String uploadFragment = Utils.getMyHealthTabNameBasedOnPosition(myHealthFragment.getCurrentFragmentVisible());
                    if (myHealthFragment.getMyHelthAdapter().getFragment(uploadFragment) instanceof UploadsFragment) {
                        if (fabMenu.getVisibility() != View.VISIBLE) {
                            fabMenu.setVisibility(View.VISIBLE);
                        }
                    }
                    new InsertData(response.getDocumentReferenceModel()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                /*[   Edited by laxman   ]*/
            } else {
                /*[   Edited by laxman   ]*/

                new InsertData(uplodedRecordsAdapter.addRecords(response.getDocumentReferenceModel(), isSyncApi)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*[   Edited by laxman   ]*/
                uplodedRecordsAdapter.notifyDataSetChanged();
            }
            isSyncApi = false;

            if (response.getUploadTotalCount() <= uplodedRecordsAdapter.getItemCount()) {
                isAllUploadLoaded = true;
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(SharedPreferencesConstant.ALL_UPLOADED_LOADED, true);
                editor.commit();
            }
            uploadServerSyncTime = response.getUploadSyncTime();

            ArrayList<Integer> documentsToDownload = response.getDocumentsToDownload();

        } else if (uplodedRecordsAdapter == null || uplodedRecordsAdapter.getItemCount() == 0) {
            listRecords.setVisibility(View.GONE);
            layoutUploadData.setVisibility(View.VISIBLE);
            // fabMenu.setVisibility(View.VISIBLE);
        } else if (uplodedRecordsAdapter != null && uplodedRecordsAdapter.getItemCount() == uplodedRecordsAdapter.getItemCount()) {
            uplodedRecordsAdapter.setFooterVisible(false);
            uplodedRecordsAdapter.notifyDataSetChanged();
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        uploadTotalCount = sharedPreferences.getInt(SharedPreferencesConstant.UPLOAD_TOTAL_COUNT, -1);
        uploadServerSyncTime = sharedPreferences.getString(SharedPreferencesConstant.UPLOAD_LAST_SYNC_TIME_SERVER, null);
    }


    public void setDocumentRefresh(DocumentReferenceModel documentReferenceModel, int position) {
        uplodedRecordsAdapter.setDocumentReferenceModelAtPosition(documentReferenceModel, position);
    }

    @Override
    public void itemClicked(View view, int position) {

        if (view.getId() == R.id.layoutReport) {
            this.positionClicked = position;
            downloadUploadedFileAndDisplayResult(position);
        }
    }


    private void downloadUploadedFileAndDisplayResult(int position) {
        Context context = getActivity();
        final DocumentReferenceModel documentReferenceModel = uplodedRecordsAdapter.getItemAtPosition(position);
        if (BuildConfig.isPhysicianApp && documentReferenceModel.getDocumentLocalPath() != null) {
            documentReferenceModel.setDocumentLocalPath(null);
        }
        //taking permission
        if (!(this.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " "
                + getActivity().getString(R.string.permission_storage), DOWNLOAD_FILE))) {
            return;
        }

        //download and display image
        startDownload(documentReferenceModel, context, position);
    }

    private boolean checkPermission(final String permissionType, String message, final int requestCode) {
        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), "Permission", message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(getActivity(), new String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permissionType}, requestCode);
            }
            return false;
        }

        return true;

    }

    private void startDownload(DocumentReferenceModel documentReferenceModel, Context context, int position) {
        if (documentReferenceModel.getDocumentLocalPath() == null || !(new File(documentReferenceModel.getDocumentLocalPath())).exists()) {
            if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_IMAGE || documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_FILE || documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_DOC || documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_DOCX) {
                if (Utils.isNetworkAvailable(context)) {
                    new DownloadFileRequest(context, documentReferenceModel.getDocumentID(), documentReferenceModel.getMimeType(), this, documentReferenceModel, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    Toast.makeText(context, "No network", Toast.LENGTH_SHORT).show();
                }
            } else if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_ZIP) {
                Toast.makeText(context, "The .zip file format is not supported.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, context.getString(R.string.unsupported_format_not_supported), Toast.LENGTH_SHORT).show();
            }
            return;
        }
        File mediaFile = new File(documentReferenceModel.getDocumentLocalPath());

        if (!mediaFile.exists() || documentReferenceModel.isDirty()) {
            //deleting file from media storage if exists.
            if (Utils.isNetworkAvailable(context)) {
                if (mediaFile.exists() && documentReferenceModel.isDirty())
                    mediaFile.delete();
                new DownloadFileRequest(context, documentReferenceModel.getDocumentID(), documentReferenceModel.getMimeType(), this, documentReferenceModel, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                Toast.makeText(context, "No network ", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_DOC || documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_DOCX) {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + documentReferenceModel.getDocumentLocalPath()), "application/msword");
            PackageManager packageManager1 = context.getPackageManager();
            List<ResolveInfo> listGall = packageManager1.queryIntentActivities(intent, 0);
            int size = listGall.size();
            for (int i = 0; i < size; i++) {
                ResolveInfo res = listGall.get(i);
                if (listGall.size() == 1) {
                    System.out.println(res.activityInfo.packageName);
                }
            }
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                new DecriptFile(context, documentReferenceModel.getDocumentLocalPath(), "application/msword").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                Toast.makeText(context, "Can not open document", Toast.LENGTH_SHORT).show();
            }
        } else if (documentReferenceModel.getMimeType() == VariableConstants.MIME_TYPE_FILE) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + documentReferenceModel.getDocumentLocalPath()), "application/pdf");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                new DecriptFile(context, documentReferenceModel.getDocumentLocalPath(), "application/pdf").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                context.startActivity(new Intent(context, OpenImageFragment.class).putExtra(VariableConstants.DOCUMENT_TYPE, documentReferenceModel.getDocumentType()).putExtra(VariableConstants.VIDEO_URI,
                        documentReferenceModel.getDocumentLocalPath()).putExtra(VariableConstants.MIME_TYPE, documentReferenceModel.getMimeType()).putExtra(VariableConstants.REFRENCE_ID, documentReferenceModel.getDocumentMasterIdentifier()).putExtra(VariableConstants.TIMESTAMP, documentReferenceModel.getTimeStamp()));//changed by Aastha for side drawe FIS-6382
            }
        } else {
            context.startActivity(new Intent(context, OpenImageFragment.class).putExtra(VariableConstants.DOCUMENT_TYPE, documentReferenceModel.getDocumentType()).putExtra(VariableConstants.VIDEO_URI,
                    documentReferenceModel.getDocumentLocalPath()).putExtra(VariableConstants.MIME_TYPE, documentReferenceModel.getMimeType()).putExtra(VariableConstants.REFRENCE_ID, documentReferenceModel.getDocumentMasterIdentifier()).putExtra(VariableConstants.TIMESTAMP, documentReferenceModel.getTimeStamp()));//changed by Aastha for side drawe FIS-6382
        }
    }

    public class InsertData extends AsyncTask<Void, Void, Void> {

        private ArrayList<DocumentReferenceModel> documentReferenceModelsList;

        public InsertData(ArrayList<DocumentReferenceModel> documentReferenceModelArrayList) {
            this.documentReferenceModelsList = documentReferenceModelArrayList;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (BuildConfig.isPatientApp) {
                DocumentReferenceDBAdapter.getInstance(getActivity()).insertDocumentReferenceList(documentReferenceModelsList);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    private JSONObject paginationJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("_count", PAGINATION_RECORD_COUNT);
            jsonObject.put("_skip", listRecords == null ? 0 : (uplodedRecordsAdapter.getItemCount()));
            jsonObject.put("_sort:desc", "lastUpdated");
            if (BuildConfig.isPhysicianApp) {
                jsonObject.put("subject", getArguments().getString("patientId"));
            }
            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject syncJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            uploadServerSyncTime = uploadServerSyncTime.replaceAll("\\+", "%2B");
            jsonObject.put("_lastUpdated", ">" + uploadServerSyncTime);
            JSONObject constraintsObject = new JSONObject();
            constraintsObject.put("constraints", jsonObject);
            if (BuildConfig.isPhysicianApp) {
                jsonObject.put("subject", getArguments().getString("patientId"));
            }
            return constraintsObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_uploads, container, false);
        //loadingView = inflater.inflate(R.layout.loading_view, null);
        if (BuildConfig.isPatientApp)
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getActivity().getResources().getString(R.string.title_my_health));
        setHasOptionsMenu(true);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        uploadTotalCount = sharedPreferences.getInt(SharedPreferencesConstant.UPLOAD_TOTAL_COUNT, -1);
        uploadServerSyncTime = sharedPreferences.getString(SharedPreferencesConstant.UPLOAD_LAST_SYNC_TIME_SERVER, null);
        isAllUploadLoaded = sharedPreferences.getBoolean(SharedPreferencesConstant.ALL_UPLOADED_LOADED, false);

        BusProvider.getInstance().register(this);

        initView();

        registerBoadCast();
        if (BuildConfig.isPatientApp) {
            setViewPagerSwipeEnabled(true);
        } else {
            setViewPagerSwipeEnabled(false);
        }

        observer = new MyObserver(new Handler());
        if (!Build.MANUFACTURER.equals(getActivity().getResources().getString(R.string.samsung))) {
            getActivity().getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, observer);
        }

        if (getArguments() != null && getArguments().containsKey(VariableConstants.OPEN_CAMERA) && getArguments().getBoolean(VariableConstants.OPEN_CAMERA)) {
            if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                    getString(R.string.permission_camera), UploadsFragment.CAMERA_INTENT))
                launchCamera();
        }

        if (getArguments() != null && getArguments().containsKey(VariableConstants.OPEN_GALLERY) && getArguments().getBoolean(VariableConstants.OPEN_GALLERY)) {
            openGalleryIntent();
        }

        if (BuildConfig.isPatientApp) {
            isToHitPaginationSyncCallAfterFetchData = true;
            new FetchUploadedData(activity, this, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } else {
            paginationSyncApiCall();
        }

        myFragmentView.setFocusableInTouchMode(true);
        myFragmentView.requestFocus();
        myFragmentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (fabMenu.isOpened()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fabMenu.close(true);
                            }
                        });
                    } else {
                        return false;
                    }
                    return true;
                }
                return false;
            }
        });

        return myFragmentView;
    }

    private void getArgumentValue() {
        if (!isNotificationHandled && getArguments() != null && getArguments().containsKey(VariableConstants.REFRENCE_ID)) {
            String refId = (getArguments().getString(VariableConstants.REFRENCE_ID));
            if (refId == null || refId.equals("")) {
                return;
            }
            isNotificationHandled = true;
            int position = uplodedRecordsAdapter.getPositionForRecord(refId);
            if (position > -1) {
                listRecords.scrollToPosition(position);
                return;
            }

        }
    }


    private void registerBoadCast() {
        if (isRegisteredBroadcast) {
            return;
        }
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getActivity());
        broadcastManager.registerReceiver(uploadRecordBroadCast, new IntentFilter(VariableConstants.BROADCAST_UPDATED_RECORD));
        broadcastManager.registerReceiver(uploadRecordDownloadedBroadCast, new IntentFilter(VariableConstants.BROADCAST_DOCUMENT));
        getActivity().registerReceiver(timeZoneChangedBroadcast, new IntentFilter(VariableConstants.TIME_ZONE_CHANGED));
        isRegisteredBroadcast = true;
    }

    /**
     * Get view and initData
     */
    private void initView() {
        fabMenu = (FloatingActionMenu) activity.findViewById(R.id.menu1);
        layoutUploadData = (LinearLayout) myFragmentView.findViewById(R.id.layoutUploadData);
        tv_physician_no_text = (CustomFontTextView) myFragmentView.findViewById(R.id.tv_physician_no_text);
        tv_physician_no_text.setText(getString(R.string.physician_no_upload));
        ll_patient_defaut = (LinearLayout) myFragmentView.findViewById(R.id.ll_patient_defaut);

        if (BuildConfig.isPhysicianApp || BuildConfig.hideStatus) {
            ll_patient_defaut.setVisibility(View.GONE);
            tv_physician_no_text.setVisibility(View.VISIBLE);
        } else {
            ll_patient_defaut.setVisibility(View.VISIBLE);
            tv_physician_no_text.setVisibility(View.GONE);
        }

        listRecords = (RecyclerView) myFragmentView.findViewById(R.id.listRecords);
//        listRecords.setHasFixedSize(true);
        listRecords.setVisibility(View.GONE);

       /* [    Edited by laxman       ]*/
        if (BuildConfig.isPatientApp) {
            MyHealth myHealthFragment = (MyHealth) getParentFragment();
            String uploadFragment = Utils.getMyHealthTabNameBasedOnPosition(myHealthFragment.getCurrentFragmentVisible());
            if (myHealthFragment.getMyHelthAdapter().getFragment(uploadFragment) instanceof UploadsFragment) {
                if (fabMenu.getVisibility() != View.VISIBLE) {
                    fabMenu.setVisibility(View.VISIBLE);
                }
            }
        }

        documentReferenceModelList = new ArrayList<>();
        uplodedRecordsAdapter = new UploadedRecyclerAdapter(activity, documentReferenceModelList, this);
        WrapContentLinearLayoutManager linearLayoutManager = new WrapContentLinearLayoutManager(getActivity());
        listRecords.setLayoutManager(linearLayoutManager);
        listRecords.setAdapter(uplodedRecordsAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) myFragmentView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);
        listRecords.setOnScrollListener(new EndLessScrollingListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // if all the the records are loaded then there is no need to hit the pagination call
                if (isAllUploadLoaded) {
                    return;
                }
                if (!isLoading) {
                    paginationApiCall();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getChildAt(0).getTop() == 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        if (BuildConfig.isPatientApp) {
            new FetchUploadedData(activity, this, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void decriptFile(DocumentReferenceModel documentReferenceModel) {
        File mediaFile = new File(documentReferenceModel.getDocumentLocalPath());
        if (mediaFile.exists()) {
            MyApplication.setUploadStaus(VariableConstants.DOCUMENT_STATUS_IN_PROCESS);

            ContentValues updatedValues = new ContentValues();
            updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_IN_PROCESS);
            updatedValues.put(DocumentReferenceDBAdapter.getPERCENTAGE(), 0);
            new UpdateDatabaseAsyncTask(documentReferenceModel, DatabaseTables.DOCUMENT_REFERENCE_TABLE, updatedValues)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            documentReferenceModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_IN_PROCESS);

            UploadRecordQueue uploadRecordQueue = UploadRecordQueue.getInstance();
            UploadRecordObject uploadRecordObject = new UploadRecordObject(getActivity(), null, documentReferenceModel.getDocumentLocalPath(), documentReferenceModel.getDocumentType(), true, null, documentReferenceModel.getMimeType());
            uploadRecordQueue.addTask(uploadRecordObject.getAttachmentPath(), uploadRecordObject);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.unregisterReceiver(uploadRecordBroadCast);
        localBroadcastManager.unregisterReceiver(uploadRecordDownloadedBroadCast);
        getActivity().unregisterReceiver(timeZoneChangedBroadcast);
        isRegisteredBroadcast = false;
        isCameraUploading = false;
        isScanAnotherCamera = false;
        getActivity().getContentResolver().unregisterContentObserver(observer);

        super.onDestroy();
    }

    /**
     * Set list view adapter for listview
     *
     * @param isToRefresh
     */
    public void setListView(ArrayList<DocumentReferenceModel> arrayDocumentReferenceModels, boolean isToRefresh) {
        if (arrayDocumentReferenceModels != null && arrayDocumentReferenceModels.size() > 0) {
            uplodedRecordsAdapter.setArrayDocumentReferenceModels(arrayDocumentReferenceModels);
            uplodedRecordsAdapter.notifyDataSetChanged();
            getArgumentValue();

            if (isToRefresh) {
                return;
            }
            listRecords.setVisibility(View.VISIBLE);
            layoutUploadData.setVisibility(View.GONE);
            //fabMenu.setVisibility(View.VISIBLE);
            if (BuildConfig.isPatientApp) {
                fabMenu.close(false);
            } else {

            }
            paginationSyncApiCall();
        } else {
            if (!isLoading) {
                paginationApiCall();
                listRecords.setVisibility(View.VISIBLE);
                layoutUploadData.setVisibility(View.GONE);
                if (BuildConfig.isPatientApp) {
                    MyHealth myHealthFragment = (MyHealth) getParentFragment();
                    String uploadFragment = Utils.getMyHealthTabNameBasedOnPosition(myHealthFragment.getCurrentFragmentVisible());
                    if (myHealthFragment.getMyHelthAdapter().getFragment(uploadFragment) instanceof UploadsFragment) {
                        if (fabMenu.getVisibility() != View.VISIBLE) {
                            fabMenu.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    private BroadcastReceiver timeZoneChangedBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                AppLog.d("broadcast", "timezone broadcast received");

                if (BuildConfig.isPatientApp)
                    new FetchUploadedData(activity, UploadsFragment.this, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                else if (BuildConfig.isPhysicianApp)
                    updateDateInModel(documentReferenceModelList);
            }
        }
    };

    private void updateDateInModel(ArrayList<DocumentReferenceModel> documentReferenceModelList) {
        if (uplodedRecordsAdapter != null)
            uplodedRecordsAdapter.notifyDataSetChanged();
    }

    public int getItemCount() {
//        if (uplodedRecordsAdapter == null) {
        return 0;
//        }
//        return uplodedRecordsAdapter.getItemCount();
    }


    public void onClick(int id) {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (!userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.physician_operation_not_allowed), Toast.LENGTH_LONG).show();
            return;
        }
        switch (id) {
            case BaseActivity.UPLOAD_CAMERA_FILE:
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), CAMERA_INTENT)) {
                    fabMenu.open(true);
                    cameraOrGallery = TextConstants.FROM_CAMERA;
                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_camera), UploadsFragment.CAMERA_INTENT))
                        launchCamera();
                }
                break;
            case BaseActivity.UPLOAD_GALLERY_FILE:
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), RESULT_GALLERY)) {
                    fabMenu.open(true);
                    launchGallery();
                }
                break;
            default:
                break;
        }
    }

    private void launchCamera() {
        String timeStamp = new SimpleDateFormat(DateTimeUtil.yyyyMMdd_HHmmss).format(new java.util.Date());
        String appName = getString(R.string.app_name);
        imageFileName = appName + timeStamp + "_";
        try {
            cameraImageFile = File.createTempFile(imageFileName, /* prefix */
                    ".jpg", /* suffix */
                    activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES) /* directory */);
            cameraImageFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isHandled = false;
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraImageFile));
        startActivityForResult(intent, CAMERA_INTENT);
    }


    public void launchGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT)
            intent.setType("image/*");
        else {
            intent.setType("**//*");
            String[] mimetypes = {"image/*", "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        }
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra("requestCode", RESULT_GALLERY);
        PackageManager manager = getActivity().getPackageManager();
        cameraOrGallery = TextConstants.FROM_GALLERY;
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            startActivityForResult(intent, RESULT_GALLERY);
        } else {
            // show some toast message
            Toast.makeText(getActivity(), getString(R.string.no_supported_file_explorer), Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FRAGMENT_TAGING) {
            if (tabs != null && tabs.getVisibility() == View.GONE) {
                tabs.setVisibility(View.VISIBLE);
            }
            setViewPagerSwipeEnabled(true);
            if (fabMenu != null && (fabMenu.getVisibility() == View.GONE || fabMenu.getVisibility() == View.INVISIBLE)) {
                if (fabMenu.getVisibility() != View.VISIBLE) {
                    fabMenu.setVisibility(View.VISIBLE);
                }
            }
            if (fabMenu.isOpened()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fabMenu.close(true);
                    }
                });

            }
        }
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_INTENT:
                    isCameraUploading = true;
                    isScanAnotherCamera = true;
                    mimeType = VariableConstants.MIME_TYPE_IMAGE;
                    beginCrop(Uri.fromFile(cameraImageFile));
                    break;
                case RESULT_GALLERY:
                    String mimeType;
                    if (data != null && data.getData() != null) {
                        if (data.getData().toString().startsWith("file:///")) {
                            File file = new File(data.getData().getPath());
                            String strFileName = file.getAbsolutePath();
                            mimeType = strFileName.contains(".") ? "application/" + strFileName.substring(strFileName.lastIndexOf(".") + 1, strFileName.length()) : getActivity().getResources().getString(R.string.txt_null);
                            if (mimeType.equals("application/doc") || mimeType.equals("application/docx")) {
                                mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                            }
                        } else {
                            mimeType = getActivity().getContentResolver().getType(data.getData());
                        }
                        if (mimeType == null || mimeType.equals("*/*")) {
                            fileNotSupported();
                        } else if (mimeType.startsWith("image") || mimeType.endsWith("jpg") || mimeType.endsWith("jpeg") || mimeType.endsWith("png")) {
                            this.mimeType = VariableConstants.MIME_TYPE_IMAGE;
                            isCameraUploading = false;
                            beginCrop(data.getData());
                        } else {
                            String extention = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);
                            Uri uri = Uri.parse(data.getData().toString());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && isExternalStorageDocument(uri)) {
                                final String docId = DocumentsContract.getDocumentId(uri);
                                final String[] split = docId.split(":");
                                final String type = split[0];

                                if ("primary".equalsIgnoreCase(type)) {
                                    try {
                                        if (split[1].endsWith(".pdf")) {
                                            this.mimeType = VariableConstants.MIME_TYPE_FILE;
                                            openFile(null, new File(Environment.getExternalStorageDirectory() + "/" + split[1]).getAbsolutePath());
                                        } else if (split[1].endsWith(".doc")) {
                                            this.mimeType = VariableConstants.MIME_TYPE_DOC;
                                            openFile(null, new File(Environment.getExternalStorageDirectory() + "/" + split[1]).getAbsolutePath());
                                        } else if (split[1].endsWith(".docx")) {
                                            this.mimeType = VariableConstants.MIME_TYPE_DOCX;
                                            openFile(null, new File(Environment.getExternalStorageDirectory() + "/" + split[1]).getAbsolutePath());
                                        } else {
                                            fileNotSupported();
                                        }
                                    } catch (Exception e) {
                                        fileNotSupported();
                                    }
                                }
                            } else if (uri.toString().startsWith("content://")) {
                                if (extention == null) {
                                    fileNotSupported();
                                    return;
                                }
                                if (extention.equals("pdf")) {
                                    this.mimeType = VariableConstants.MIME_TYPE_FILE;
                                } else if (extention.equals("doc")) {
                                    this.mimeType = VariableConstants.MIME_TYPE_DOC;
                                } else if (extention.equals("docx")) {
                                    this.mimeType = VariableConstants.MIME_TYPE_DOCX;
                                } else {
                                    fileNotSupported();
                                    return;
                                }
                                String[] filePathColumn = new String[]{"_display_name"};
                                Cursor cursor = null;
                                String fileName = null;
                                cursor = getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);

                                if (cursor != null && cursor.moveToFirst()) {
                                    int ignored = cursor.getColumnIndex("_display_name");
                                    if (ignored != -1) {
                                        fileName = cursor.getString(ignored);
                                    }
                                }
                                if (fileName == null) {
                                    fileName = ".tempFile." + extention;
                                }

                                InputStream inputStream = null;
                                OutputStream outputStream = null;
                                try {
                                    inputStream = getActivity().getContentResolver().openInputStream(Uri.parse(uri.toString()));//, filePathColumn, (String) null, (String[]) null, (String) null);

                                    File file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + getActivity().getString(R.string.app_name), fileName + "");
                                    outputStream = new FileOutputStream(file);
                                    int read = 0;
                                    byte[] bytes = new byte[1024];

                                    while ((read = inputStream.read(bytes)) != -1) {
                                        outputStream.write(bytes, 0, read);
                                    }
                                    openFile(null, file.getAbsolutePath());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (inputStream != null) {
                                            inputStream.close();
                                        }
                                        if (outputStream != null) {
                                            outputStream.close();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else if (extention != null && extention.equals("pdf")) {
                                this.mimeType = VariableConstants.MIME_TYPE_FILE;
                                openFile(null, new File(data.getData().getPath()).getAbsolutePath());
                            } else if (extention != null && extention.equals("doc")) {
                                this.mimeType = VariableConstants.MIME_TYPE_DOC;
                                openFile(null, new File(data.getData().getPath()).getAbsolutePath());
                            } else if (extention != null && extention.equals("docx")) {
                                this.mimeType = VariableConstants.MIME_TYPE_DOCX;
                                openFile(null, new File(data.getData().getPath()).getAbsolutePath());
                            } else {
                                fileNotSupported();
                            }
                        }
                    }
                    break;
                case FRAGMENT_TAGING:
                    if (data != null) {
                        if (fabMenu.isOpened()) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (fabMenu != null && fabMenu.isOpened()) {
                                        fabMenu.close(true);
                                    }
                                }
                            });
                        }
                        if (data.hasExtra("fromWhere")) {
                            if (data.getStringExtra("fromWhere").equals(TextConstants.FROM_CAMERA)) {
                                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), CAMERA_INTENT)) {
                                    cameraOrGallery = TextConstants.FROM_CAMERA;
                                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                                            getString(R.string.permission_camera), UploadsFragment.CAMERA_INTENT))
                                        launchCamera();
                                }
                            } else if (data.getStringExtra("fromWhere").equals(TextConstants.FROM_GALLERY)) {
                                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), RESULT_GALLERY)) {
                                    launchGallery();
                                }
                            }
                        } else {
                            boolean isToScanAnother = data.getBooleanExtra(TextConstants.FRAGMENT_KEY, false);
                            boolean isOpenCamera = data.getBooleanExtra(VariableConstants.IS_CAMERA_INTENT, false);
                            refreshList(isToScanAnother, isOpenCamera);
                            listRecords.scrollToPosition(0);
                        }
                    }
                    break;
                case Crop.REQUEST_CROP:
                    handleCrop(resultCode, data);
                    break;
                case PDF_PREVIEW_INTENT:
                    if (data.hasExtra(VariableConstants.PDF_FILE_STATUS) && data.getExtras().getBoolean(VariableConstants.PDF_FILE_STATUS)) {
                        encriptPDFFile(data.getExtras().getString(VariableConstants.PDF_FILE_PATH));
                    }
                    break;
                case DOC_PREVIEW_INTENT:
                    if (data.hasExtra(VariableConstants.PDF_FILE_STATUS) && data.getExtras().getBoolean(VariableConstants.PDF_FILE_STATUS)) {
                        encriptPDFFile(data.getExtras().getString(VariableConstants.PDF_FILE_PATH));
                    }
                    break;
                default:
                    break;
            }
            return;
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (fabMenu != null && !fabMenu.isOpened())
                fabMenu.open(false);
            switch (requestCode) {
                case FRAGMENT_TAGING:
                    break;
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * Encript file bg
     *
     * @param pdfFilePath
     */
    private void encriptPDFFile(String pdfFilePath) {
        EncriptFile encriptBitmapFile = new EncriptFile(activity, null, this, pdfFilePath);
        encriptBitmapFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * For the file not suppoted
     */
    private void fileNotSupported() {
        Toast.makeText(getActivity(), getResources().getString(R.string.unsupported_format_not_supported), Toast.LENGTH_SHORT).show();
    }

    /**
     * handle pdf file
     */
    private void handlePdfFile(String filePath) {
        getActivity().startActivityForResult(new Intent(getActivity(), PdfPreview.class).putExtra(VariableConstants.PDF_FILE_PATH, filePath).putExtra(VariableConstants.MIME_TYPE, mimeType), PDF_PREVIEW_INTENT);
    }


    private void saveCroppedImage(Uri uri) {
        Bitmap imageBitmap = ExifUtils.rotateBitmap(uri.getPath(), Utils.getBitmapFromUri(getActivity(), uri, 1000, 700));

        MyApplication.setTempImagePreview(imageBitmap);
        EncriptFile encriptBitmapFile = new EncriptFile(activity, imageBitmap, this, null);
        encriptBitmapFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        if (isCameraUploading) {
            isCameraUploading = false;
            Utils.deleteExternalStoragePrivatePicture(cameraImageFile);
        }
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            saveCroppedImage(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            AppLog.d("Fisike", "error while cropping upload image");
        }
    }

    private void beginCrop(Uri source) {
        File file = new File(getActivity().getCacheDir(), "cropped");
        Uri destination = Uri.fromFile(file);
        Crop.of(source, destination).start(getActivity(), UploadsFragment.this);
    }

    @Override
    public void onRefresh() {
        if (!isLoading) {
            swipeRefreshLayout.setRefreshing(true);
            paginationSyncApiCall();
        }
    }

    private void paginationSyncApiCall() {
        if (!isLoading && (uplodedRecordsAdapter == null || uplodedRecordsAdapter.getItemCount() == 0 || uploadServerSyncTime == null)) {
            paginationApiCall();
        } else if (!isLoading) {
            syncApiCall();
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        boolean enable = false;
        if (view != null && view.getChildCount() > 0) {
            // check if the first item of the list is visible
            boolean firstItemVisible = view.getFirstVisiblePosition() > 0;
            // check if the top of the first item is visibleon
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() < view.getPaddingTop();
            // enabling or disabling the refresh layout
            enable = firstItemVisible || topOfFirstItemVisible;
        }
        swipeRefreshLayout.setEnabled(!enable);

        if (isAllUploadLoaded) {
            return;
        }

        if (firstVisibleItem == visibleItemCount) {
            return;
        }

        if (!isLoading && ((firstVisibleItem + visibleItemCount) == totalItemCount)) {
            if (Utils.isNetworkAvailable(getActivity())) {
                paginationApiCall();
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

    }

    class MyObserver extends ContentObserver {

        public MyObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            if (!isHandled) {
                isHandled = true;
                String mediaId = MediaStore.Images.Media._ID;
                String mediaData = MediaStore.Images.Media.DATA;
                Uri uri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

                final String[] imageColumns = {mediaId};
                final String imageOrderBy = mediaId + " DESC";
                final String imageWhere = null;
                final String[] imageArguments = null;
                Cursor imageCursor = getActivity().getContentResolver().query(uri1, imageColumns, imageWhere, imageArguments, imageOrderBy);
                if (imageCursor.moveToFirst()) {

                    int id = imageCursor.getInt(imageCursor.getColumnIndex(mediaId));

                    String[] projection = {mediaData};
                    String whereClause = mediaId + "=?";
                    Cursor cursor = getActivity().getContentResolver().query(uri1, projection, whereClause, new String[]{id + ""}, null);
                    int column_index = cursor.getColumnIndexOrThrow(mediaData);
                    String string = "";
                    if (cursor.moveToFirst()) {
                        string = cursor.getString(column_index);
                        Utils.deleteFile(getActivity(), string, uri1, "1");
                    }
                    imageCursor.close();
                }
            }
        }

    }

    public void postEncription(File moveToInternalStorage, int requestCode, String filePath) {
        Uri mImageCaptureUri = null;
        if (moveToInternalStorage.exists()) {
            filePath = new String(moveToInternalStorage.getAbsolutePath());
        }
        if (null != filePath && !"".equals(filePath)) {
            mImageCaptureUri = Uri.fromFile(new File(filePath));
        }
        if (null != mImageCaptureUri) {
            String realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, activity);
            if (null == realPathFromURI) {
                realPathFromURI = mImageCaptureUri.getPath();
                if (null == realPathFromURI) {
                    return;
                }
            }
        }
    }

    /**
     * File exist in the the path (file already downloaded)
     *
     * @param attachmentPath
     * @param toEncriptFilePath
     */
    public void openFile(String attachmentPath, String toEncriptFilePath) {
        tabs.setVisibility(View.GONE);

        setViewPagerSwipeEnabled(false);
        Intent uploadIntent = new Intent(getActivity(), UploadsTaggingActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("fromWhere", cameraOrGallery);
        bundle.putString(VariableConstants.VIDEO_URI, attachmentPath);
        bundle.putInt(VariableConstants.MIME_TYPE, mimeType);
        bundle.putString(VariableConstants.PDF_FILE_PATH, toEncriptFilePath);
        bundle.putBoolean(VariableConstants.IS_CAMERA_INTENT, isScanAnotherCamera);
        uploadIntent.putExtras(bundle);
        startActivityForResult(uploadIntent, FRAGMENT_TAGING);
    }

    public void setOpenCamera() {
        if (fabMenu != null && !fabMenu.isOpened())
            fabMenu.open(true);
        if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                getString(R.string.permission_camera), UploadsFragment.CAMERA_INTENT))
            launchCamera();
    }


    public void refreshList(boolean isToScanAnothor, boolean isOpenCamera) {
        new FetchUploadedData(activity, this, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        if (isToScanAnothor && isOpenCamera) {
            if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                    getString(R.string.permission_camera), UploadsFragment.CAMERA_INTENT))
                launchCamera();
        } else if (isToScanAnothor) {
            onClick(BaseActivity.UPLOAD_GALLERY_FILE);
        }
        isScanAnotherCamera = false;
    }


    public void openGalleryIntent() {
        fabMenu.close(false);
        IntentUtils.attachImageFile(getActivity(), 0, RESULT_GALLERY, null, null, false, false);


    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Permission_needs_granted_access), Toast.LENGTH_SHORT).show();
    }

    public void onPermissionGranted(String permission) {
        Toast.makeText(getActivity(), "Permission have been granted ", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted(permissions[0], requestCode);
        } else {
            onPermissionNotGranted(permissions[0]);
        }

    }

    private void onPermissionGranted(String permission, int requestCode) {
        if (requestCode == DOWNLOAD_FILE) {
            downloadUploadedFileAndDisplayResult(positionClicked);
        } else if (requestCode == UploadsFragment.CAMERA_INTENT) {
            launchCamera();
        }
    }

}

