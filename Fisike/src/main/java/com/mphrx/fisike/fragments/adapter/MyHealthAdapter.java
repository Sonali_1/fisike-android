package com.mphrx.fisike.fragments.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.MyHealthItemsEnum;
import com.mphrx.fisike.fragments.MedicationOrderFragment;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.fragments.VitalsFragment;
import com.mphrx.fisike.record_screen.fragment.GenralFragment;
import com.mphrx.fisike.utils.MyHealthTabItemsHolder;
import com.mphrx.fisike.view.FragmentStatePagerAdapter;
import com.mphrx.fisike.view.SlidingTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MyHealthAdapter extends FragmentStatePagerAdapter {
    private static final String Fragment = null;
    private static int NUM_ITEMS;
    private SlidingTabLayout tabs;
    private MyHealth myHealth;
    private boolean isToOpenCamera;
    private HashMap<String, Fragment> mPageReferenceMap = new HashMap<String, Fragment>();
    private String digitiztionMessageCount;
    private String digiticationMessageType;
    private int digitizationMedicationPrecription;
    private boolean digitizationPriscription;
    private int encounterId;
    private boolean isToOpenGallery;
    private Bundle bundle;
    private String refId;
    private LinkedHashMap<String, Integer> myHealthTabItemsMap;
    ArrayList<String> pageTitles = new ArrayList<>();
    ArrayList<String> displayPageTitles = new ArrayList<>();

    public MyHealthAdapter(FragmentManager fragmentManager, SlidingTabLayout tabs, MyHealth myHealth) {
        super(fragmentManager);
        this.tabs = tabs;

        myHealthTabItemsMap = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().getMyHealthTabItemsMap();
        NUM_ITEMS = myHealthTabItemsMap.size();
        int i = 0;
        for (String key : myHealthTabItemsMap.keySet()) {
            pageTitles.add(i, key);

            if (key.equals(MyHealthItemsEnum.Vitals.getTabItemName())) {
                displayPageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_vitals_txt));
            } else if (key.equals(MyHealthItemsEnum.Records.getTabItemName())) {
                displayPageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_records));
            } else if (key.equals(MyHealthItemsEnum.Medications.getTabItemName())) {
                displayPageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_medication));
            } else if (key.equals(MyHealthItemsEnum.Uploads.getTabItemName())) {
                displayPageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_uploads));
            }

            i++;
        }
        this.myHealth = myHealth;
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        String tabName = pageTitles.get(position);

        if (tabName.equals(MyHealthItemsEnum.Vitals.getTabItemName())) {
            VitalsFragment vitalsFragment = new VitalsFragment();
            vitalsFragment.setArguments(myHealth.getArguments());
            mPageReferenceMap.put(tabName, vitalsFragment);
            return vitalsFragment;
        } else if (tabName.equals(MyHealthItemsEnum.Records.getTabItemName())) {

            GenralFragment genralFragment = new GenralFragment();
            if (digitizationMedicationPrecription != 0) {
                if (myHealth != null) {
                    genralFragment.setArguments(myHealth.getArguments());
                } else {
                    Bundle data = new Bundle();
                    data.putString(VariableConstants.DIGITIZATION_MESSAGE_COUNT, digitiztionMessageCount);
                    data.putString(VariableConstants.DIGITIZATION_MESSAGE_TYPE, digiticationMessageType);
                    data.putInt(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION, digitizationMedicationPrecription);
                    data.putBoolean(VariableConstants.DIGITIZATION_REMINDER, digitizationPriscription);
                    genralFragment.setArguments(data);
                }
            } else {
                genralFragment.setArguments(myHealth.getArguments());
            }
            mPageReferenceMap.put(tabName, genralFragment);
            return genralFragment;
        } else if (tabName.equals(MyHealthItemsEnum.Medications.getTabItemName())) {
            if (BuildConfig.isPatientApp) {
                MedicationOrderFragment medicationFragment = new MedicationOrderFragment();
                if (encounterId != 0) {
                    Bundle data = new Bundle();
                    data.putInt(VariableConstants.ENCOUNTER_ID, encounterId);
                    medicationFragment.setArguments(data);
                } else if (bundle != null) {
                    medicationFragment.setArguments(bundle);
                }
                mPageReferenceMap.put(tabName, medicationFragment);
                return medicationFragment;
            } else {
                com.mphrx.fisike_physician.fragment.MedicationFragment f = new com.mphrx.fisike_physician.fragment.MedicationFragment();
                f.setArguments(myHealth.getArguments());
                mPageReferenceMap.put(tabName, f);
                return f;
            }
        } else if (tabName.equals(MyHealthItemsEnum.Uploads.getTabItemName())) {
            UploadsFragment uploadsFragment = new UploadsFragment(tabs);
            if (isToOpenGallery) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(VariableConstants.OPEN_GALLERY, isToOpenGallery);
                uploadsFragment.setArguments(bundle);
            } else if (refId != null) {
                Bundle data = new Bundle();
                data.putString(VariableConstants.REFRENCE_ID, refId);
                uploadsFragment.setArguments(data);
            }
            if (BuildConfig.isPhysicianApp) {
                uploadsFragment.setArguments(myHealth.getArguments());
            }
            mPageReferenceMap.put(tabName, uploadsFragment);
            return uploadsFragment;
        } else
            return null;

    }

    public void setMedicationFragment(Bundle bundle) {
        this.bundle = bundle;
    }

    public void setRecordFragment(String digitiztionMessageCount, String digiticationMessageType, int digitizationMedicationPrecription, boolean digitizationPriscription) {
        this.digitiztionMessageCount = digitiztionMessageCount;
        this.digiticationMessageType = digiticationMessageType;
        this.digitizationMedicationPrecription = digitizationMedicationPrecription;
        this.digitizationPriscription = digitizationPriscription;
    }

    public void setMedicationFragment(boolean digitizationPriscription, int encounterId) {
        this.digitizationPriscription = digitizationPriscription;
        this.encounterId = encounterId;
    }

    public void setUploadFragment(String refId) {
        this.refId = refId;
    }

    public void isOpenCamera(boolean isToOpenCamera) {
        this.isToOpenCamera = isToOpenCamera;
    }

    public Fragment getFragment(String key) {
        return mPageReferenceMap.get(key);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {

        return displayPageTitles.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commitAllowingStateLoss();

        super.destroyItem(container, position, object);
    }

    public void isOpenGalley(boolean isToOpenGallery) {
        this.isToOpenGallery = isToOpenGallery;
    }
}
