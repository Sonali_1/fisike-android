package com.mphrx.fisike.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.EncriptFile;
import com.mphrx.fisike.imageCropping.Crop;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.ExifUtils;

import java.io.File;

import appointment.utils.SupportBaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CameraFragment.OnFragmentInteractionListener} interface
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class CameraFragment extends SupportBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final int SUCESS_RESULT = 1, CAMERA_RESULT = 2, GALLERY_RESULT = 3, CROP_RESULT = 4,
                           READ_WRITE_INTENT = 5;
    private File photoFilePath;
    private Uri mImageCaptureUri = null;
    public CameraIntegration cameraIntegrationListener;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String realPathFromURI;


    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CameraFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraFragment newInstance(String param1, String param2) {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public void findView() {

    }

    @Override
    public void initView() {

    }

    @Override
    public void bindView() {

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void takePhoto() {
        try {
            photoFilePath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    System.currentTimeMillis() + ".jpg");
            photoFilePath.setWritable(true);
            mImageCaptureUri = Uri.fromFile(photoFilePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentUtils.onClickTakePhoto(getActivity(), CAMERA_RESULT, GALLERY_RESULT, photoFilePath, null, true, true);
    }

    public void openGallery() {
        photoFilePath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                System.currentTimeMillis() + ".jpg");
        photoFilePath.setWritable(true);
        mImageCaptureUri = Uri.fromFile(photoFilePath);
        IntentUtils.onClickChooseImage(getActivity(), CAMERA_RESULT, GALLERY_RESULT, photoFilePath, null, true, true);
    }


    private void fileNotSupported() {
        Toast.makeText(getActivity(), getResources().getString(R.string.unsupported_format_not_supported), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_RESULT:
                String mimeType;
                if (data != null && data.getData() != null) {
                    if (data.getData().toString().startsWith("file:///")) {
                        File file = new File(data.getData().getPath());
                        String strFileName = file.getAbsolutePath();
                        mimeType = strFileName.contains(".") ? "application/" + strFileName.substring(strFileName.lastIndexOf(".") + 1, strFileName.length()) : getActivity().getResources().getString(R.string.txt_null);
                        if (mimeType.equals("application/doc") || mimeType.equals("application/docx")) {
                            mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        }
                    } else {
                        mimeType = getActivity().getContentResolver().getType(data.getData());
                    }
                    if (mimeType == null || mimeType.equals("*/*")) {
                        fileNotSupported();
                    } else if (mimeType.startsWith("image") || mimeType.endsWith("jpg") || mimeType.endsWith("jpeg") || mimeType.endsWith("png")) {
                        beginCrop(data.getData());
                    }
                }
                break;

            case CAMERA_RESULT:
                if(photoFilePath != null) {
                    beginCrop(Uri.fromFile(photoFilePath));
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.api_error), Toast.LENGTH_LONG).show();
                }
                break;

            case Crop.REQUEST_CROP:
                handleCrop(resultCode, data);
                break;

        }
    }

    private void beginCrop(Uri source) {
        File file = new File(getActivity().getCacheDir(), "cropped");
        Uri destination = Uri.fromFile(file);
        Crop.of(source, destination).start(getActivity());
    }

    private void saveCroppedImage(Uri uri) {
        Bitmap imageBitmap = ExifUtils.rotateBitmap(uri.getPath(), Utils.getBitmapFromUri(getActivity(), uri, 1000, 700));
        cameraIntegrationListener.setExtractedBitmap(imageBitmap);

        MyApplication.setTempImagePreview(imageBitmap);
        EncriptFile encriptBitmapFile = new EncriptFile(getActivity(), imageBitmap, this, null);
        encriptBitmapFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
      /*  if (isCameraUploading) {
            isCameraUploading = false;
            Utils.deleteExternalStoragePrivatePicture(cameraImageFile);
        }*/
    }

    public interface CameraIntegration{
        void setExtractedBitmap(Bitmap bitmap);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            saveCroppedImage(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            AppLog.d("Fisike", "error while cropping upload image");
        }
    }

}

