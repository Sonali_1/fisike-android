package com.mphrx.fisike;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.mphrx.fisike.utils.BaseActivity;


public class XeroViewerWebview extends BaseActivity {

	private WebView myBrowser;

	private ProgressDialog progressDialog;
	private Toolbar addToolbar;

	private ProgressBar progressBar;
	private FrameLayout frameLayout;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setting toolbar to radiology report
		frameLayout = (FrameLayout) findViewById(R.id.frame_container);
		getLayoutInflater().inflate(R.layout.xeroviewer_webview, frameLayout);
//		setContentView(R.layout.xeroviewer_webview);

		addToolbar = (Toolbar) findViewById(R.id.toolbar_add_contact);
		setSupportActionBar(addToolbar);
		addToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
		getSupportActionBar().setTitle(getResources().getString(R.string.Xero_Viewer));

		myBrowser = (WebView) findViewById(R.id.webView1);

		progressBar = (ProgressBar) findViewById(R.id.pbar);


		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage(getResources().getString(R.string.Loading_Xero_Viewer));
		progressDialog.setCancelable(false);

		myBrowser.getSettings().setJavaScriptEnabled(true);

		// Msg = (EditText)findViewById(R.id.msg);
		// btnSendMsg = (Button)findViewById(R.id.sendmsg);
		// btnSendMsg.setOnClickListener(new Button.OnClickListener(){
		//
		// @Override
		// public void onClick(View arg0) {
		// // TODO Auto-generated method stub
		// String msgToSend = Msg.getText().toString();
		// myBrowser.loadUrl("javascript:callFromActivity(\""+msgToSend+"\")");
		//
		// }});

		myBrowser.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);

				progressBar.setVisibility(View.VISIBLE);
//				if (!Utils.isNetworkAvailable(getActivity())) {
//					Toast.makeText(RecordDetailScreen.this, "Network Error...", 1).show();
//				}

			}

			@Override
			public void onPageFinished(WebView view, String url)

			{
				progressBar.setVisibility(View.GONE);
			}
		});
		myBrowser.loadUrl(getIntent().getStringExtra("targetUrl"));
//		myBrowser.loadUrl(getIntent().getStringExtra("targetUrl").replace("https","http"));

		// System.out.println(msgToSend);
		// myBrowser.loadUrl("javascript:callFromActivity(\""+msgToSend+"\")");

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		else {
			return super.onOptionsItemSelected(item);
		}
	}
}