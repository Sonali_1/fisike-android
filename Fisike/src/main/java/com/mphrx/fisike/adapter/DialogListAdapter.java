package com.mphrx.fisike.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.util.ArrayList;

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.DialogViewHolder > {
    Context context;
    private String title;
    private ArrayList<String> ListData;
    public clickListener listener;

    public DialogListAdapter(ArrayList<String> listData) {
        // TODO Auto-generated constructor stub
        this.ListData = listData;

    }

    //view holder for supportAdapter
    public class DialogViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        CustomFontTextView tvTitle;
        RadioButton radioButton;
        public DialogViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.title);
            this.radioButton=(RadioButton)v.findViewById(R.id.rb);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                //handling Item Click
//                radioButton.setChecked(true);
                listener.itemClicked(v, getAdapterPosition());
            }
        }

    }

    @Override
    public void onBindViewHolder(DialogViewHolder arg0, int position) {
        // TODO Auto-generated method stub
        title = ListData.get(position);
        arg0.tvTitle.setText(title);

    }

    @Override
    public int getItemCount() {
        return ListData.size();
    }

    @Override
    public DialogViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.speciality_row, arg0, false);
        DialogViewHolder settingHolder = new DialogViewHolder(row);
        return settingHolder;
    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }


    public void setClickListener(clickListener listener) {
        // TODO Auto-generated method stub
        this.listener = listener;

    }
}
