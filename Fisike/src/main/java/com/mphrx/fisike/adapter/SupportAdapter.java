package com.mphrx.fisike.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SupportDataItem;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.ArrayList;

public class SupportAdapter extends RecyclerView.Adapter<SupportAdapter.supportViewHolder> {
    Context context;
    private String title;
    private ArrayList<SupportDataItem> ListData;
    public clickListener listener;

    public SupportAdapter(ArrayList<SupportDataItem> listData) {
        // TODO Auto-generated constructor stub
        this.ListData = listData;

    }

    //view holder for supportAdapter
    public class supportViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        CustomFontTextView tvTitle;
        IconTextView ivIcon;
        View temp_line;

        public supportViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.title);
            this.ivIcon = (IconTextView) v.findViewById(R.id.iv_icon);
            this.temp_line = (View) v.findViewById(R.id.temp_line);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                //handling Item Click
                listener.itemClicked(v, getAdapterPosition());
            }
        }
    }

    @Override
    public int getItemCount() {
        return ListData.size();
    }

    @Override
    public void onBindViewHolder(supportViewHolder arg0, int position) {
        arg0.tvTitle.setText(ListData.get(position).getText());
        arg0.ivIcon.setText(ListData.get(position).getResIcon());

        if (position == ListData.size() - 1) {
            arg0.temp_line.setVisibility(View.GONE);
        } else {
            arg0.temp_line.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public supportViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.support_row, arg0, false);
        supportViewHolder settingHolder = new supportViewHolder(row);
        return settingHolder;
    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }


    public void setClickListener(clickListener listener) {
        // TODO Auto-generated method stub
        this.listener = listener;

    }
}
