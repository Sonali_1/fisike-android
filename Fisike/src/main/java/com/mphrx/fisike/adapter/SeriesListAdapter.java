package com.mphrx.fisike.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.beans.Series;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.lazyloading.ListImageDownloader;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.interfaces.RSAKey;
import java.util.ArrayList;
import java.util.List;


public class SeriesListAdapter extends BaseAdapter {
    private ArrayList<Series> seriesList;
    private Context context;
    ListImageDownloader downloader;
    private String token;
    private LayoutInflater inflater;


    public SeriesListAdapter(Context context, List<Series> seriesList, String token) {
        this.context = context;
        this.seriesList = (ArrayList<Series>) seriesList;
        this.token = token;
        downloader = new ListImageDownloader(context);
        inflater = LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return seriesList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return seriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;
        //convertView = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.series_list_cell, null);
            holder.numberOfImages = (CustomFontTextView) convertView.findViewById(R.id.TextView01);
            holder.tests = (CustomFontTextView) convertView.findViewById(R.id.TextView02);
            holder.ids = (CustomFontTextView) convertView.findViewById(R.id.TextView03);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.imageView1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


//		Series item = seriesList.get(position);
//		View view = convertView;
//		ViewHolder holder;
//		view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.series_list_cell, parent, false);
//
//		TextView patientNameAgeSex = (TextView) view.findViewById(R.id.TextView01);
//		TextView tests = (TextView) view.findViewById(R.id.TextView02);
//		TextView dateOfTest = (TextView) view.findViewById(R.id.TextView03);
//
//		ImageView rad = (ImageView) view.findViewById(R.id.imageView1);
//
//		holder = new ViewHolder(patientNameAgeSex, tests, dateOfTest, rad);
//		view.setTag(holder);

        if (seriesList.get(position).numberOfInstances > 1)
            holder.numberOfImages.setText(String.valueOf(seriesList.get(position).numberOfInstances) + " " + context.getResources().getString(R.string.images));
        else
            holder.numberOfImages.setText(String.valueOf(seriesList.get(position).numberOfInstances) + " " + context.getResources().getString(R.string.image));


        if (seriesList.get(position).description.equals(context.getResources().getString(R.string.txt_null)) || seriesList.get(position).description.equals(""))
            holder.tests.setText(context.getResources().getString(R.string.no_desc));
        else
            holder.tests.setText(seriesList.get(position).description);

        //holder.ids.setText("Instance Id : "+String.valueOf(seriesList.get(position).instanceId)+"\nSeries Id : "+String.valueOf(seriesList.get(position).seriesId)+"\nStudy Id : "+String.valueOf(seriesList.get(position).imagingStudyId));


        try {
            downloader.download(seriesList.get(position).thumbUrl, holder.thumbnail, token, new JSONObject().put("instanceId", seriesList.get(position).instanceId).put("imageSize", "TNAIL"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertView;
    }

    private class ViewHolder {
        private CustomFontTextView numberOfImages;
        private CustomFontTextView tests;
        private CustomFontTextView ids;
        private ImageView thumbnail;

        // private TextView status;

//		public ViewHolder(TextView patientNameAgeSex, TextView tests, TextView dateOfTest, ImageView rad) {
//			this.patientNameAgeSex = patientNameAgeSex;
//			this.tests = tests;
//			this.dateOfTest = dateOfTest;
//			this.rad = rad;
//
//		}
    }

}