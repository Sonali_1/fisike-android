package com.mphrx.fisike.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.IGroupImageViewLoader;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * This is the Data Adapter that is used to power the Recent Chats Screen
 *
 * @author kkhurana
 */
public class RecentContactListAdapter extends BaseAdapter implements Filterable {
    private ArrayList<ChatConversationMO> contactList;
    private Context context;
    private SettingMO settings;
    private HashMap<String, ChatContactMO> chatContactList;
    private ArrayList<ChatConversationMO> mStringFilterList;
    // private LayoutInflater inflater;
    private ValueFilter valueFilter;
    private boolean isGroupChat;
    private HashMap<String, GroupTextChatMO> groupTextChatList;
    private String userName;
    private boolean isUserPatient;
    String myId;

    public RecentContactListAdapter(ArrayList<ChatConversationMO> contactList, HashMap<String, ChatContactMO> chatContactList,
                                    HashMap<String, GroupTextChatMO> groupChatContactList, Context context, String userName, String userType) {
        this.contactList = new ArrayList<>();
        for (int i = 0; i < contactList.size(); i++) {
            ChatConversationMO person = contactList.get(i);
            String nickName = person.getNickName();
            String groupName = person.getGroupName();
            if ((nickName != null && !nickName.equals("")) || !TextUtils.isEmpty(groupName)) {
                this.contactList.add(person);
            }
        }

//        this.contactList = contactList;
        this.chatContactList = chatContactList;
        this.context = context;
        this.userName = userName;
        this.isGroupChat = true;
        this.groupTextChatList = groupChatContactList;

        isUserPatient = userType.equalsIgnoreCase(TextConstants.PATIENT) ? true : false;

        mStringFilterList = this.contactList;
        getFilter();
        myId = SettingManager.getInstance().getUserMO().getId() + "";
    }

    public ArrayList<ChatConversationMO> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<ChatConversationMO> contactList) {
        this.contactList = new ArrayList<>();
        for (int i = 0; i < contactList.size(); i++) {
            ChatConversationMO person = contactList.get(i);
            String nickName = person.getNickName();
            String groupName = person.getGroupName();
            if ((nickName != null && !nickName.equals("")) || !TextUtils.isEmpty(groupName)) {
                this.contactList.add(person);
            }
        }
//        this.contactList = contactList;
        mStringFilterList = this.contactList;
    }

    public HashMap<String, ChatContactMO> getChatContactList() {
        return chatContactList;
    }

    public void setChatContactList(HashMap<String, ChatContactMO> chatContactList) {
        this.chatContactList = chatContactList;
    }

    public void setGroupChatContactList(HashMap<String, GroupTextChatMO> groupTextChatList) {
        this.groupTextChatList = groupTextChatList;
    }

    @Override
    public int getCount() {
        if (contactList != null) {
            return contactList.size();
        }
        return 0;
    }

    @Override
    public ChatConversationMO getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void findContact(String id, String status) {
    }

    /**
     * This method renders each of the list items on the recent chat screen
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatConversationMO person = contactList.get(position);
        String nickName = person.getNickName();
        String groupName = person.getGroupName();
        if ((nickName == null || nickName.equals("")) && TextUtils.isEmpty(groupName)) {
            AppLog.d("TAG", "getView: conversation null");
            return null;
        }
        try {
            settings = SettingManager.getInstance().getSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }

        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.contact_list, parent, false);
            CustomFontTextView txtUserName = (CustomFontTextView) view.findViewById(R.id.txtName);
            CustomFontTextView txtMsg = (CustomFontTextView) view.findViewById(R.id.txtMsg);
            CustomFontTextView txtTime = (CustomFontTextView) view.findViewById(R.id.txtTime);
            ImageView imgMsgStatus = (ImageView) view.findViewById(R.id.imgMsgStatus);
            CustomFontTextView txtUnReadCount = (CustomFontTextView) view.findViewById(R.id.txtUnReadCount);
            CustomProfileVIewLayout imgPerson = (CustomProfileVIewLayout) view.findViewById(R.id.imgPerson);
            ImageView imgStatusType = (ImageView) view.findViewById(R.id.imgStatus);
            LinearLayout layoutContacts = (LinearLayout) view.findViewById(R.id.layoutContacts);
            holder = new ViewHolder(txtUserName, txtMsg, txtTime, layoutContacts, imgMsgStatus, txtUnReadCount, imgPerson, imgStatusType, position);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final String userId = person.getJId();
        if (nickName != null && !nickName.equals("")) {
            holder.txtUserName.setText(nickName);
        }
//        else if (userId.contains("@" + settings.getFisikeServerIp())) {
//            int indexOf = userId.lastIndexOf("@" + settings.getFisikeServerIp());
//            String substring = userId.substring(0, indexOf);
//            try {
//                substring = URLDecoder.decode(substring, "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//            holder.txtUserName.setText(substring);
//        }
        else {
            if (!TextUtils.isEmpty(groupName)) {
                nickName = groupName;
                holder.txtUserName.setText(nickName);
            } else {
                holder.txtUserName.setText(userId);
            }
        }

        int unReadCount = person.getUnReadCount();
        if (unReadCount <= 0) {
            holder.txtUnReadCount.setVisibility(View.INVISIBLE);
        } else {
            holder.txtUnReadCount.setVisibility(View.VISIBLE);
            holder.txtUnReadCount.setText(person.getUnReadCount() + "");
        }

        String lastMsg = person.getLastMsg();
        if (lastMsg == null || lastMsg.equals("")) {
            lastMsg = "-----";
            holder.imgMsgStatus.setBackgroundResource(0);
            holder.txtMsg.setText("");
        } else {
            holder.txtMsg.setText(lastMsg);
            int lastMsgStatus = person.getLastMsgStatus();
            if (lastMsgStatus == DefaultConnection.MESSAGE_SENT || lastMsgStatus == DefaultConnection.MESSAGE_SENT_TO_SERVER) {
                holder.imgMsgStatus.setBackgroundResource(R.drawable.message_sent);
            } else if (lastMsgStatus == DefaultConnection.MESSAGE_NOT_SENT) {
                holder.imgMsgStatus.setBackgroundResource(R.drawable.process_msg);
            } else if (lastMsgStatus == DefaultConnection.MESSAGE_DELIVERED) {
                holder.imgMsgStatus.setBackgroundResource(R.drawable.delivered_image);
            } else {
                holder.imgMsgStatus.setBackgroundResource(0);
            }
        }
        if (person.getCreatedTimeUTC() == 0) {
            holder.txtTime.setText("");
        } else {
            long time = person.getCreatedTimeUTC();
            Date date = new Date(time);
            String timeFormated;
            long diff = Utils.diff(time, Calendar.DAY_OF_YEAR); // 0 - today, 1 - tomorrow, -1 - yesterday
            if (-1 == diff) {
                holder.txtTime.setText(context.getResources().getString(R.string.Yesterday));
            } else if (0 == diff) {
                java.text.DateFormat dateFormat = android.text.format.DateFormat.getTimeFormat(context);
                holder.txtTime.setText(dateFormat.format(date));
            } else {
                timeFormated = DateTimeUtil.calculateDateWithOuTime(time);
                holder.txtTime.setText(timeFormated);
            }
        }
        if (unReadCount <= 0) {
            holder.layoutContacts.setBackgroundResource(R.drawable.mybox_read_selector);
        } else {
            holder.layoutContacts.setBackgroundResource(R.drawable.mybox_unread_selector);
        }

        String groupExtractedId = new String(userId);

//        final GroupTextChatMO chatContactMO = new GroupTextChatMO();
//
        groupExtractedId = Utils.extractGroupId(groupExtractedId);
//
//        chatContactMO.generatePersistenceKey(groupExtractedId, userName);

        ArrayList<String> contactIds = new ArrayList<>();

        ArrayList<String> moKeys = person.getChatContactMoKeys();
        for (int i = 0; i < moKeys.size(); i++) {
            ChatContactMO mo = null;
            try {
                mo = chatContactDBAdapter.getInstance(context).fetchChatContactMo(moKeys.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mo != null) {
                contactIds.add(TextPattern.getUserId(mo.getId()));
            }
        }

        holder.imgStatusType.setVisibility(View.GONE);

        if (groupExtractedId.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            groupExtractedId = null;
        }
        holder.imgPerson.displayContactImages(contactIds, groupExtractedId);

//        holder.imgPerson.setTag(new Integer(position));

        holder.imgPerson.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                int itemPosition = (Integer) v.getTag();
//
//                ChatConversationMO chatConversationMO =person;
//
//                boolean isGroupChat = contactList.get(itemPosition).getConversationType() == 1;
//                if (isGroupChat || (chatConversationMO != null && chatConversationMO.getChatContactMoKeys() != null && chatConversationMO.getChatContactMoKeys().size() > 2)) {
//
//                    GroupTextChatMO groupTextChatMO = groupTextChatList.get(chatContactMO.getPersistenceKey() + "");
//                    context.startActivity(new Intent(context, BuildConfig.isPatientApp ? GroupDetailActivity.class : com.mphrx.fisike_physician.activity.GroupDetailActivity.class).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO).putExtra(
//                            VariableConstants.GROUP_INFO, groupTextChatMO).putExtra("groupName", chatConversationMO.getGroupName()));
//                } else {
//
//                    if (BuildConfig.isPatientApp) {
//                        context.startActivity(new Intent(context, ContactDetailActivity.class).putExtra(VariableConstants.SENDER_ID, person.getSecoundryJid())
//                                .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
//                    } else {
//
//                        if (chatContactMO.getUserType().getName().equalsIgnoreCase("patient")) {
//                            context.startActivity(new Intent(context, ViewProfile.class).putExtra(VariableConstants.SENDER_ID, person.getSecoundryJid())
//                                    .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
//                        } else {
//                            context.startActivity(new Intent(context, com.mphrx.fisike_physician.activity.ProfileActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
//                                    .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, chatConversationMO));
//                        }
//
//                    }
//                }
//                if () {
//                    GroupTextChatMO groupTextChatMO = groupTextChatList.get(chatContactMO.getPersistenceKey() + "");
//                    context.startActivity(new Intent(context, GroupDetailActivity.class).putExtra(VariableConstants.CONVERSATION_ITEM,
//                            contactList.get(itemPosition)).putExtra(VariableConstants.GROUP_INFO, groupTextChatMO));
//                    return;
//                }
//                String secoundryJid = Utils.cleanXMPPServer(person.getSecoundryJid());
//                context.startActivity(new Intent(context, ContactDetailActivity.class).putExtra(VariableConstants.SENDER_ID, secoundryJid).putExtra(
//                        TextConstants.IS_RECENT_CHAT, true));
            }
        });
        return view;
    }

    private void setGrupContactImage(ImageView imgPerson, String subString, GroupTextChatMO chatContactMO) {
        chatContactMO = groupTextChatList.get(chatContactMO.getPersistenceKey() + "");

        if (null != chatContactMO) {
            byte[] profilePic = chatContactMO.getImageData();
            if (null != profilePic && profilePic.length != 0) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
                imgPerson.setImageBitmap(bitmap);
            } else if (null != chatContactMO) {
                imgPerson.setImageResource(R.drawable.cg_profile_picturedefault);
            }
        } else {
            imgPerson.setImageResource(R.drawable.cg_profile_picturedefault);
        }
    }

    private void setUserStatus(IGroupImageViewLoader imgPerson, String subString, ImageView imgStatusType) {
        if (subString != null) {
            if (subString.contains("@" + settings.getFisikeServerIp())) {
                int indexOf = subString.lastIndexOf("@" + settings.getFisikeServerIp());
                subString = subString.substring(0, indexOf);
            }
            try {
                subString = URLDecoder.decode(subString, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
            ChatContactMO chatContactMO = new ChatContactMO();
            chatContactMO.generatePersistenceKey(subString);

            if (chatContactList == null) {
                return;
            }

            chatContactMO = chatContactList.get(chatContactMO.getPersistenceKey() + "");

            if (null != chatContactMO) {
                if (chatContactMO.getUserType() == null || chatContactMO.getUserType().getName() == null
                        || chatContactMO.getUserType().getName().equalsIgnoreCase(TextConstants.PATIENT)) {
                    imgStatusType.setVisibility(View.INVISIBLE);
                } else {
                    String presenceType = chatContactMO.getPresenceType();
                    imgStatusType.setVisibility(View.VISIBLE);
                    if (presenceType == null || presenceType.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
                        imgStatusType.setImageResource(R.drawable.ic_status_available);
                    } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
                        imgStatusType.setImageResource(R.drawable.ic_status_busy);
                    } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
                        imgStatusType.setImageResource(R.drawable.ic_status_away);
                    } else {
                        imgStatusType.setImageResource(R.drawable.ic_status_available);
                    }
                }
            }
        }

    }

    public void clear() {
        contactList.clear();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }

    private class ViewHolder {
        private CustomFontTextView txtUserName;
        private CustomFontTextView txtMsg;
        private CustomFontTextView txtTime;
        private LinearLayout layoutContacts;
        private ImageView imgMsgStatus;
        private CustomFontTextView txtUnReadCount;
        private IGroupImageViewLoader imgPerson;
        private int position;
        private ImageView imgStatusType;

        public ViewHolder(CustomFontTextView txtUserName, CustomFontTextView txtMsg, CustomFontTextView txtTime, LinearLayout layoutContacts, ImageView imgStatus,
                          CustomFontTextView txtUnReadCount, CustomProfileVIewLayout imgPerson, ImageView imgStatusType, int position) {
            this.txtUserName = txtUserName;
            this.txtMsg = txtMsg;
            this.txtTime = txtTime;
            this.layoutContacts = layoutContacts;
            this.imgMsgStatus = imgStatus;
            this.txtUnReadCount = txtUnReadCount;
            this.imgPerson = imgPerson;
            this.imgStatusType = imgStatusType;
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

    private class ValueFilter extends Filter {

        // Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<ChatConversationMO> filterList = new ArrayList<ChatConversationMO>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if (mStringFilterList.get(i).getNickName() != null
                            && (mStringFilterList.get(i).getNickName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(mStringFilterList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                contactList = (ArrayList<ChatConversationMO>) results.values;
                if (contactList.size() <= 0) {

                }
                notifyDataSetChanged();
            } catch (Exception e) {
            }
        }
    }


}
