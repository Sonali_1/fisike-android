package com.mphrx.fisike.adapter;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HeaderActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.Queue.UploadRecordQueue;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.UpdateDatabaseAsyncTask;
import com.mphrx.fisike.background.DecriptFile;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.enums.UploadTaggingEnum;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.lazyloading.ImageLoader;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.MasterLayout;
import com.mphrx.fisike_physician.network.request.DownloadFileRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

import static com.mphrx.fisike.fragments.UploadsFragment.RESULT_GALLERY;

/**
 * Created by Aastha on 06/05/2016.  //
 */
public class UploadedRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final Pattern DocTypeLabReport = Pattern.compile("(?i)Lab Report");
    private static final Pattern DocTypeXRay = Pattern.compile("(?i)x.ray");
    private static final Pattern DocTypeNote = Pattern.compile("(?i).*note.*");
    private static final Pattern DocTypePrescription = Pattern.compile("(?i)prescription");
    public static final int RESULT_GALLERY = 101;

    private Context context;
    private ArrayList<DocumentReferenceModel> arrayDocumentReferenceModels = new ArrayList<DocumentReferenceModel>();
    private UploadsFragment uploadsFragment;
    private ImageLoader imgLoader;

    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 0;
    private boolean isVisible;
    private int position;
    private clickListener listener;

    public UploadedRecyclerAdapter(Context context, ArrayList<DocumentReferenceModel> arrayDocumentReferenceModels, UploadsFragment uploadsFragment) {
        this.context = context;
        imgLoader = new ImageLoader(context);
        this.arrayDocumentReferenceModels = arrayDocumentReferenceModels;
        this.uploadsFragment = uploadsFragment;
        Comparator<DocumentReferenceModel> comparator = new Comparator<DocumentReferenceModel>() {

            public int compare(DocumentReferenceModel object1, DocumentReferenceModel object2) {
                if (Long.parseLong(object1.getTimeStamp()) < Long.parseLong(object2.getTimeStamp())) {
                    return 1;
                } else if (Long.parseLong(object1.getTimeStamp()) > Long.parseLong(object2.getTimeStamp())) {
                    return -1;
                }
                return 0;
            }
        };
        Collections.sort(arrayDocumentReferenceModels, comparator);
        setClickListener(uploadsFragment);
    }

    public void setArrayDocumentReferenceModels(ArrayList<DocumentReferenceModel> arrayDocumentReferenceModels) {
        this.arrayDocumentReferenceModels = arrayDocumentReferenceModels;
        Comparator<DocumentReferenceModel> comparator = new Comparator<DocumentReferenceModel>() {

            public int compare(DocumentReferenceModel object1, DocumentReferenceModel object2) {
                if (object1 == null || object2 == null || object1.getTimeStamp() == null || object2.getTimeStamp() == null || object1.getTimeStamp().equals(context.getResources().getString(R.string.txt_null)) || object2.getTimeStamp().equals(context.getResources().getString(R.string.txt_null))) {
                    return 0;
                } else if (Long.parseLong(object1.getTimeStamp()) < Long.parseLong(object2.getTimeStamp())) {
                    return 1;
                } else if (Long.parseLong(object1.getTimeStamp()) > Long.parseLong(object2.getTimeStamp())) {
                    return -1;
                }
                return 0;
            }
        };
        Collections.sort(arrayDocumentReferenceModels, comparator);
    }

    public int getPosition(String attachmentPath, int percentage) {
        for (int i = 0; i < arrayDocumentReferenceModels.size(); i++) {
            if (attachmentPath.equals(arrayDocumentReferenceModels.get(i).getDocumentLocalPath())) {
                arrayDocumentReferenceModels.get(i).setPercentage(percentage);
                notifyDataSetChanged();
                return i;
            }
        }
        return -1;
    }

    private int getPostion(int documentId) {
        for (int i = 0; i < arrayDocumentReferenceModels.size(); i++) {
            if (documentId == arrayDocumentReferenceModels.get(i).getDocumentID()) {
                return i;
            }
        }
        return -1;
    }

    public int getPositionForRecord(String refId) {
        for (int i = 0; arrayDocumentReferenceModels != null && i < arrayDocumentReferenceModels.size(); i++) {
            if (arrayDocumentReferenceModels.get(i).getDocumentMasterIdentifier() != null && arrayDocumentReferenceModels.get(i).getDocumentMasterIdentifier().equals(refId)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<DocumentReferenceModel> addRecords(ArrayList<DocumentReferenceModel> documentReferenceModelArrayList, boolean isSyncApi) {
        ArrayList<DocumentReferenceModel> tempDocumentReferenceModels = new ArrayList<>();
        for (int i = 0; i < documentReferenceModelArrayList.size(); i++) {
            DocumentReferenceModel documentReferenceModel = documentReferenceModelArrayList.get(i);
            int index = containElement(documentReferenceModel.getDocumentID());
            if (index == -1) {
                if (isSyncApi) {
                    arrayDocumentReferenceModels.add(0, documentReferenceModel);
                } else {
                    arrayDocumentReferenceModels.add(documentReferenceModel);
                }
                tempDocumentReferenceModels.add(documentReferenceModel);
            } else {
                documentReferenceModel.setToUpdate(true);
                DocumentReferenceModel tempDocumentReferenceModel = arrayDocumentReferenceModels.get(index);
                if (tempDocumentReferenceModel.getDocumentLocalPath() != null) {
                    documentReferenceModel.setDocumentLocalPath(tempDocumentReferenceModel.getDocumentLocalPath());
                }

                // neeed to test thumbnail chnges or not
                if (documentReferenceModel.getThumbImage() != null && documentReferenceModel.getThumbImage().length > 0) {
                    documentReferenceModel.setThumbImage(documentReferenceModel.getThumbImage());
                } else if (tempDocumentReferenceModel.getThumbImage() != null) {
                    documentReferenceModel.setThumbImage(tempDocumentReferenceModel.getThumbImage());
                }

                if (tempDocumentReferenceModel.getNoOfAttachment() < documentReferenceModel.getNoOfAttachment()) {
                    documentReferenceModel.setDirty(true);
                } else {
                    documentReferenceModel.setDirty(false);
                }

                if (documentReferenceModel.getDocumentUploadedStatus() == VariableConstants.DOCUMENT_STATUS_UNKNOWN)
                    documentReferenceModel.setDocumentUploadedStatus(tempDocumentReferenceModel.getDocumentUploadedStatus());

                arrayDocumentReferenceModels.set(index, documentReferenceModel);
                tempDocumentReferenceModels.add(documentReferenceModel);
            }
        }
        return tempDocumentReferenceModels;
    }


    private int containElement(int documentId) {
        for (int i = 0; i < arrayDocumentReferenceModels.size(); i++) {
            if (arrayDocumentReferenceModels.get(i).getDocumentID() == documentId) {
                return i;
            }
        }
        return -1;
    }

    public void setDocumentReferenceModelAtPosition(DocumentReferenceModel documentReferenceModel, int position) {
        arrayDocumentReferenceModels.set(position, documentReferenceModel);
        Comparator<DocumentReferenceModel> comparator = new Comparator<DocumentReferenceModel>() {

            public int compare(DocumentReferenceModel object1, DocumentReferenceModel object2) {
                if (Long.parseLong(object1.getTimeStamp()) < Long.parseLong(object2.getTimeStamp())) {
                    return 1;
                } else if (Long.parseLong(object1.getTimeStamp()) > Long.parseLong(object2.getTimeStamp())) {
                    return -1;
                }
                return 0;
            }
        };
        Collections.sort(arrayDocumentReferenceModels, comparator);
        notifyDataSetChanged();
    }

    public void refreshRow(DocumentReferenceModel documentReferenceModel) {
        int position = getPostion(documentReferenceModel.getDocumentID());
        //todo 5509 (position>=0 )
        if (position >= 0) {

            arrayDocumentReferenceModels.set(position, documentReferenceModel);
            notifyDataSetChanged();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        if (arg1 == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.uploaded_record_list, parent, false);
            UploadedRecordsViewHolder viewHolder = new UploadedRecordsViewHolder(view);
            return viewHolder;
        } else if (arg1 == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_view, parent, false);
            FooterHolder viewHolder = new FooterHolder(view);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder recycler, int position) {
        if (recycler instanceof UploadedRecordsViewHolder) {

            int drawable = R.drawable.ic_doc_type_prec_nor;
            final UploadedRecordsViewHolder holder = (UploadedRecordsViewHolder) recycler;

            if (position == 0) {
                holder.temp_view.setVisibility(View.VISIBLE);
            } else {
                holder.temp_view.setVisibility(View.GONE);
            }

            final DocumentReferenceModel documentReferenceModel = arrayDocumentReferenceModels.get(position);
            String documentType = documentReferenceModel.getDocumentType();
            if (documentType != null && documentType.equals(context.getResources().getString(R.string.conditional_check_Note))) {
                documentType = context.getResources().getString(R.string.Discharge_Note);
            }
            if (documentReferenceModel.getSourceName() != null && !documentReferenceModel.getSourceName().equals("")) {
                holder.lyt_source.setVisibility(View.VISIBLE);
                holder.tvSource.setText(documentReferenceModel.getSourceName());
            } else {
                holder.lyt_source.setVisibility(View.GONE);
            }
            if (documentType == null) {
                return;
            }

            LinkedHashMap<String, UploadTaggingEnum> uploadTaggingDisplayValueMap = UploadTaggingEnum.getUploadTaggingEnumLinkedHashMap();
            String documentTypeDisplayableValue = documentType.equalsIgnoreCase(context.getResources().getString(R.string.conditional_check_unknown)) || documentType.equalsIgnoreCase(context.getResources().getString(R.string.conditional_check_others))
                    ? context.getResources().getString(R.string.document) :
                    documentType.equalsIgnoreCase(context.getResources().getString(R.string.Radiology_Images)) ?
                            context.getResources().getString(R.string.Radiology_Image) : documentType;

            if (uploadTaggingDisplayValueMap != null && uploadTaggingDisplayValueMap.containsKey(documentTypeDisplayableValue))
                documentTypeDisplayableValue = uploadTaggingDisplayValueMap.get(documentTypeDisplayableValue).getValue();

            holder.txtCategory.setText(documentTypeDisplayableValue);
            holder.txtTimeStamp.setText(DateTimeUtil.calculateDateTimeFromTimeStampUploads(Long.parseLong(documentReferenceModel.getTimeStamp()), context));
            switch (documentReferenceModel.getMimeType()) {
                case VariableConstants.MIME_TYPE_FILE:
                    holder.imgReport.setImageResource(R.drawable.doc_type_pdf);
                    break;
                case VariableConstants.MIME_TYPE_DOC:
                    holder.imgReport.setImageResource(R.drawable.doc_type_doc);
                    break;
                case VariableConstants.MIME_TYPE_DOCX:
                    holder.imgReport.setImageResource(R.drawable.doc_type_docx);
                    break;
                case VariableConstants.MIME_TYPE_IMAGE:
                    byte[] thumb = documentReferenceModel.getThumbImage();
                    String tempDocumentType = arrayDocumentReferenceModels.get(position).getDocumentType();
                    if (thumb != null && thumb.length > 0) {
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(thumb, 0, thumb.length);
                        decodeByteArray = Utils.getCurvedCornerBitmap(decodeByteArray, 15);
                        holder.imgReport.setImageBitmap(decodeByteArray);
                    } else if (DocTypeLabReport.matcher(tempDocumentType).matches()) {
                        drawable = R.drawable.ic_doc_type_lab_nor;
                        holder.imgReport.setImageResource(R.drawable.ic_doc_type_lab_nor);
                    } else if (DocTypeXRay.matcher(tempDocumentType).matches()) {
                        drawable = R.drawable.ic_doc_type_xray_nor;
                        holder.imgReport.setImageResource(R.drawable.ic_doc_type_xray_nor);
                    } else if (DocTypeNote.matcher(tempDocumentType).matches()) {
                        drawable = R.drawable.ic_doc_type_dn_nor;
                        holder.imgReport.setImageResource(R.drawable.ic_doc_type_dn_nor);
                    } else if (DocTypePrescription.matcher(tempDocumentType).matches()) {
                        drawable = R.drawable.ic_doc_type_prec_nor;
                        holder.imgReport.setImageResource(R.drawable.ic_doc_type_prec_nor);
                    } else {
                        drawable = R.drawable.ic_doc_type_prec_nor;
                        holder.imgReport.setImageResource(R.drawable.doc_type_unknown);
                    }

                    break;
                case VariableConstants.MIME_TYPE_ZIP:
                    holder.imgReport.setImageResource(R.drawable.doc_type_zip);
                    break;
                case VariableConstants.MIME_TYPE_UNKNOWN:
                    holder.imgReport.setImageResource(R.drawable.doc_type_unknown);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            switch (documentReferenceModel.getDocumentUploadedStatus()) {
                case VariableConstants.DOCUMENT_STATUS_DOWNLOADED:
                    holder.imgUploadedStatus.setVisibility(View.GONE);
                    // holder.imgUploadedStatus.setImageResource(R.drawable.ic_record_uploaded);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.cusview.setupprogress(View.GONE);
                    holder.masterLayout.setVisibility(View.GONE);
                    holder.txtGettingDig.setVisibility(View.VISIBLE);
                    holder.txtGettingDig.setText(context.getResources().getString(R.string.Getting_Digitized_) + " ");
                    holder.txtGettingDig.setTextColor(ContextCompat.getColor(context, R.color.orange));
                    holder.txtUploadStatus.setVisibility(View.VISIBLE);
                    holder.txtUploadStatus.setText(context.getResources().getString(R.string.in_2_days));
                    holder.lyt_refId.setVisibility(View.VISIBLE);
                    holder.txtRefId.setText(context.getString(R.string.ref_id) + documentReferenceModel.getDocumentMasterIdentifier());
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;
                case VariableConstants.DOCUMENT_STATUS_IN_PROCESS:
                    int percentage = documentReferenceModel.getPercentage();
                    holder.progressDownloading.setVisibility(View.VISIBLE);
                    holder.txtGettingDig.setVisibility(View.GONE);
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    holder.imgUploadedStatus.setImageResource(R.drawable.ic_doc_uploading_stop);
                    if (percentage != 0) {
                        holder.imgUploadedStatus.setVisibility(View.VISIBLE);
                        holder.progressDownloading.setVisibility(View.GONE);
                        holder.masterLayout.setVisibility(View.VISIBLE);
                        holder.masterLayout.cusview.setupprogress(percentage);
                        holder.masterLayout.cusview.invalidate();
                    }
                    if (percentage == 100) {
                        holder.masterLayout.cusview.setupprogress(View.GONE);
                        holder.masterLayout.setVisibility(View.GONE);
                        holder.txtGettingDig.setVisibility(View.VISIBLE);
                        holder.txtGettingDig.setText(context.getResources().getString(R.string.Getting_Digitized_) + " ");
                        holder.txtGettingDig.setTextColor(ContextCompat.getColor(context, R.color.orange));
                        holder.txtUploadStatus.setVisibility(View.VISIBLE);
                        holder.txtUploadStatus.setText(context.getResources().getString(R.string.in_2_days));
                        holder.lyt_refId.setVisibility(View.VISIBLE);
                        holder.imgUploadedStatus.setVisibility(View.GONE);
                    } else {
                        holder.lyt_refId.setVisibility(View.GONE);
                    }
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;
                case VariableConstants.DOCUMENT_STATUS_REDDY:
                    holder.imgUploadedStatus.setVisibility(View.VISIBLE);
                    holder.imgUploadedStatus.setImageResource(R.drawable.ic_refresh_black_24dp);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.setVisibility(View.GONE);
                    holder.masterLayout.cusview.setupprogress(View.GONE);
                    holder.txtGettingDig.setVisibility(View.VISIBLE);
                    holder.txtGettingDig.setText(context.getResources().getString(R.string.lets_try_again));
                    holder.txtGettingDig.setTextColor(ContextCompat.getColor(context, R.color.dusky_blue));
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    holder.lyt_refId.setVisibility(View.GONE);
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;
                case VariableConstants.DOCUMENT_STATUS_DIGITIZED:
                    holder.imgUploadedStatus.setVisibility(View.GONE);
                    //holder.imgUploadedStatus.setImageResource(R.drawable.ic_record_uploaded);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.cusview.setupprogress(View.GONE);
                    holder.txtGettingDig.setVisibility(View.VISIBLE);
                    holder.txtGettingDig.setText(context.getResources().getString(R.string.Digitized));
                    holder.txtGettingDig.setTextColor(ContextCompat.getColor(context, R.color.action_bar_bg));
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    holder.lyt_refId.setVisibility(View.VISIBLE);
                    holder.txtRefId.setText(context.getString(R.string.ref_id) + documentReferenceModel.getDocumentMasterIdentifier());
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;
                case VariableConstants.DOCUMENT_STATUS_REJECTED:
                    holder.imgUploadedStatus.setVisibility(View.GONE);
//                holder.imgUploadedStatus.setImageResource(R.drawable.ic_record_uploaded);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.txtGettingDig.setVisibility(View.VISIBLE);
                    holder.txtGettingDig.setText(context.getResources().getString(R.string.Rejected_) + " ");
                    holder.txtGettingDig.setTextColor(ContextCompat.getColor(context, R.color.red_500));
                    holder.txtUploadStatus.setVisibility(View.VISIBLE);
                    holder.txtUploadStatus.setText("( " + documentReferenceModel.getReason() + " )");
                    holder.lyt_refId.setVisibility(View.VISIBLE);
                    holder.txtRefId.setText(context.getString(R.string.ref_id) + documentReferenceModel.getDocumentMasterIdentifier());
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;
                case VariableConstants.DOCUMENT_STATUS_UNKNOWN:
                    holder.imgUploadedStatus.setVisibility(View.GONE);
                    //holder.imgUploadedStatus.setImageResource(R.drawable.ic_record_uploaded);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.cusview.setupprogress(View.GONE);
                    holder.txtGettingDig.setVisibility(View.GONE);
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    holder.lyt_refId.setVisibility(View.VISIBLE);
                    holder.txtRefId.setText(context.getString(R.string.Ref_id) + documentReferenceModel.getDocumentMasterIdentifier());
                    holder.txtUploadStatus.setVisibility(View.GONE);
                    if (Utils.isRTL(context)) {
                        layoutParams.setMargins(50, 0, 0, 0);
                    } else {
                        layoutParams.setMargins(0, 0, 50, 0);
                    }
                    break;

                default:
                    holder.txtGettingDig.setVisibility(View.GONE);
                    holder.txtUploadStatus.setVisibility(View.GONE);
            }

            if (BuildConfig.hideStatus) {
                holder.layoutUploadStatus.setVisibility(View.GONE);
            }
            if (BuildConfig.hideRefId) {
                holder.lyt_refId.setVisibility(View.GONE);
            }
            holder.layoutReport.setLayoutParams(layoutParams);
        } else {
            FooterHolder holder = (FooterHolder) recycler;
            holder.tvFooterText.setText(context.getResources().getString(R.string.Hang_on_tight_Fetching_records));
        }

    }

    @Override
    public int getItemCount() {
        if (arrayDocumentReferenceModels == null) {
            return 0;
        }
        if (isVisible) {
            return arrayDocumentReferenceModels.size() + 1;
        } else {
            return arrayDocumentReferenceModels.size();
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public DocumentReferenceModel getItemAtPosition(int position) {
        return arrayDocumentReferenceModels.get(position);
    }

    public class UploadedRecordsViewHolder extends RecyclerView.ViewHolder {

        private CustomFontTextView txtCategory, tvSource;
        private FrameLayout temp_view;
        private ImageView imgReport;
        private CustomFontTextView txtTimeStamp;
        private CustomFontTextView txtGettingDig;
        private ImageButton imgUploadedStatus;
        private ProgressBar progressDownloading;
        private LinearLayout layoutReport;
        private CustomFontTextView txtUploadStatus;
        private MasterLayout masterLayout;
        private CustomFontTextView txtRefId;
        private LinearLayout layoutUploadStatus;
        private RelativeLayout lyt_timestamp, lyt_refId, lyt_source;

        public UploadedRecordsViewHolder(View itemView) {
            super(itemView);
            txtCategory = (CustomFontTextView) itemView.findViewById(R.id.txtCategory);
            // sourceName
            // source view
            tvSource = (CustomFontTextView) itemView.findViewById(R.id.tv_source);
            txtTimeStamp = (CustomFontTextView) itemView.findViewById(R.id.txtTimeStamp);
            imgReport = (ImageView) itemView.findViewById(R.id.imgReport);
            imgUploadedStatus = (ImageButton) itemView.findViewById(R.id.imgUploadedStatus);
            progressDownloading = (ProgressBar) itemView.findViewById(R.id.progressDownloading);
            lyt_timestamp = (RelativeLayout) itemView.findViewById(R.id.lyt_timestamp);
            lyt_refId = (RelativeLayout) itemView.findViewById(R.id.lyt_refId);
            lyt_source = (RelativeLayout) itemView.findViewById(R.id.lyt_source);
            progressDownloading.getIndeterminateDrawable().setColorFilter(
                    context.getResources().getColor(R.color.action_bar_bg),
                    android.graphics.PorterDuff.Mode.SRC_IN);
            layoutReport = (LinearLayout) itemView.findViewById(R.id.layoutReport);
            txtGettingDig = (CustomFontTextView) itemView.findViewById(R.id.txtGettingDig);
            txtUploadStatus = (CustomFontTextView) itemView.findViewById(R.id.txtUploadStatus);
            masterLayout = (MasterLayout) itemView.findViewById(R.id.masterLayout);
            txtRefId = (CustomFontTextView) itemView.findViewById(R.id.txtRefId);
            temp_view = (FrameLayout) itemView.findViewById(R.id.temp_view);
            layoutUploadStatus = (LinearLayout) itemView.findViewById(R.id.layoutUploadStatus);

            //moved click
            layoutReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position == RecyclerView.NO_POSITION) {
                        position = getLayoutPosition();
                    }
                    if (listener != null)
                        listener.itemClicked(v, position);

                }

            });
            imgUploadedStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position == RecyclerView.NO_POSITION) {
                        position = getLayoutPosition();
                    }
                    final DocumentReferenceModel documentReferenceModel = arrayDocumentReferenceModels.get(position);
                    if (documentReferenceModel.getDocumentUploadedStatus() == VariableConstants.DOCUMENT_STATUS_REDDY) {
                        if (Utils.isNetworkAvailable(MyApplication.getAppContext())) {
                            uploadsFragment.decriptFile(documentReferenceModel);
                        }
                    } else if (documentReferenceModel.getDocumentUploadedStatus() == VariableConstants.DOCUMENT_STATUS_IN_PROCESS) {
                        UploadRecordQueue uploadRecordQueue = UploadRecordQueue.getInstance();
                        uploadRecordQueue.cancelRequest(documentReferenceModel.getDocumentLocalPath());
                        documentReferenceModel.setDocumentUploadedStatus(VariableConstants.DOCUMENT_STATUS_REDDY);
                        documentReferenceModel.setPercentage(0);
                        ContentValues updatedValues = new ContentValues();
                        updatedValues.put(DocumentReferenceDBAdapter.getPERCENTAGE(), 0);
                        updatedValues.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_REDDY);
                        new UpdateDatabaseAsyncTask(documentReferenceModel, DatabaseTables.DOCUMENT_REFERENCE_TABLE, updatedValues, 2)
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        imgUploadedStatus.setVisibility(View.VISIBLE);
                        imgUploadedStatus.setImageResource(R.drawable.ic_refresh_black_24dp);
                        progressDownloading.setVisibility(View.GONE);
                        txtGettingDig.setVisibility(View.VISIBLE);
                        txtGettingDig.setText(context.getResources().getString(R.string.lets_try_again));
                        txtUploadStatus.setVisibility(View.GONE);
                        masterLayout.setVisibility(View.GONE);

                    }
                }
            });
        }


    }

    public class FooterHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvFooterText;

        public FooterHolder(View itemView) {
            super(itemView);
            tvFooterText = (CustomFontTextView) itemView.findViewById(R.id.tv_footer_text);

        }
    }

    public void setFooterVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    private boolean isPositionFooter(int position) {
        return position == arrayDocumentReferenceModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }
}
