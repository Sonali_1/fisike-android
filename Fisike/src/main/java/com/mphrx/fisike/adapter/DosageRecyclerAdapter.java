package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.Medicinefrequency_Activity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.beans.DoseBean;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Aastha on 27/01/2016.
 */
public class DosageRecyclerAdapter extends RecyclerView.Adapter<DosageRecyclerAdapter.ViewHolder> {

    private ArrayList<DoseBean> dosageList;
    private Context context;
    private ViewHolder viewHolder;

    private boolean isPlural;


    public DosageRecyclerAdapter(Context mContext, ArrayList<DoseBean> dosageList) {
        this.dosageList = dosageList;
        context = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dosage_listitem, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        DoseBean doseBean = dosageList.get(position);
        if (doseBean.getTime() != null && !doseBean.isSOS()) {
            //     viewHolder.tvMin.setText(":" + doseBean.getMin());
            //int hourInt = Integer.parseInt(doseBean.getTime());


            if (!DateFormat.is24HourFormat(context)) {
                viewHolder.tvTime.setText(" " + DateTimeUtil.getHoursIn12Or24HrFormatLocale(doseBean.getTime() + ":" + doseBean.getMin(), DateTimeUtil.HH_mm, DateTimeUtil.destinationTimeFormat) + " ");
            } else {
                viewHolder.tvTime.setText(" " + DateTimeUtil.getHoursIn12Or24HrFormatLocale(doseBean.getTime() + ":" + doseBean.getMin(), DateTimeUtil.HH_mm, DateTimeUtil.HH_mm) + " ");
            }

            String before_after_food_text = doseBean.getIsAfterOrBefore();
            before_after_food_text = MedicationFrequancyEnum.getDisplayedValuefromCode(before_after_food_text);
            viewHolder.tvIsAfterOrBefore.setText(before_after_food_text);
            viewHolder.tvTime.setVisibility(View.VISIBLE);
            //setSOS(true);
        } else {
            viewHolder.tvTime.setVisibility(View.GONE);
            String before_after_food_text = doseBean.getIsAfterOrBefore();
            before_after_food_text = MedicationFrequancyEnum.getDisplayedValuefromCode(before_after_food_text);
            viewHolder.tvIsAfterOrBefore.setText(before_after_food_text);
        }
        double amountDouble = Double.parseDouble(doseBean.getAmount());
        if ((int) amountDouble == amountDouble) {
            viewHolder.tvDosage.setText((int) amountDouble + "");
        } else {
            viewHolder.tvDosage.setText(doseBean.getAmount());
        }
        if (amountDouble > 1) {
            isPlural = true;
        } else {
            isPlural = false;
        }
        setDosageImageandUnitType(viewHolder, doseBean.getUnits(), isPlural);

        setViewHolder(viewHolder);
    }


    private void setDosageImageandUnitType(ViewHolder viewHolder, String units, boolean isPlural) {
        //unit is language specific here
        String unitsCode = units;
        try {
            //getting standard code from language
            unitsCode = MedicationUnitsEnum.getCodeFromDisplayedValue(units);
        } catch (Exception e) {
        }

        String displayUnit = Utils.calculateUnitToDisplay(unitsCode, isPlural);
        viewHolder.tvUnit.setText(displayUnit);
        GradientDrawable bgShape = (GradientDrawable) viewHolder.imgDosageType.getBackground();

        if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_tablet))) {
            viewHolder.imgDosageType.setText(R.string.fa_tablet);
            bgShape.setColor(context.getResources().getColor(R.color.tablet));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_capsule))) {
            viewHolder.imgDosageType.setText(R.string.fa_capsule);
            bgShape.setColor(context.getResources().getColor(R.color.capsule));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_syrup))) {
            viewHolder.imgDosageType.setText(R.string.fa_syrup);
            bgShape.setColor(context.getResources().getColor(R.color.syrup));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_injection))) {
            viewHolder.imgDosageType.setText(R.string.fa_injection);
            bgShape.setColor(context.getResources().getColor(R.color.injection));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_spray))) {
            viewHolder.imgDosageType.setText(R.string.fa_spray);
            bgShape.setColor(context.getResources().getColor(R.color.spray));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_inhaler))) {
            viewHolder.imgDosageType.setText(R.string.fa_inhaler);
            bgShape.setColor(context.getResources().getColor(R.color.mixture));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_drop))) {
            viewHolder.imgDosageType.setText(R.string.fa_drops);
            bgShape.setColor(context.getResources().getColor(R.color.drop));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_powder))) {
            viewHolder.imgDosageType.setText(R.string.fa_powder);
            bgShape.setColor(context.getResources().getColor(R.color.powder));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_granule))) {
            viewHolder.imgDosageType.setText(R.string.fa_granule);
            bgShape.setColor(context.getResources().getColor(R.color.granule));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_cream))) {
            viewHolder.imgDosageType.setText(R.string.fa_creme);
            bgShape.setColor(context.getResources().getColor(R.color.creame));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_gum_paint))) {
            viewHolder.imgDosageType.setText(R.string.fa_gum_paint);
            bgShape.setColor(context.getResources().getColor(R.color.gum_paint));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_mouth_wash))) {
            viewHolder.imgDosageType.setText(R.string.fa_moutwash);
            bgShape.setColor(context.getResources().getColor(R.color.mouth_wash));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_oil))) {
            viewHolder.imgDosageType.setText(R.string.fa_oil);
            bgShape.setColor(context.getResources().getColor(R.color.oil));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_ointment))) {
            viewHolder.imgDosageType.setText(R.string.fa_ointment);
            bgShape.setColor(context.getResources().getColor(R.color.ointment));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_sache))) {
            viewHolder.imgDosageType.setText(R.string.fa_sachets);
            bgShape.setColor(context.getResources().getColor(R.color.sachet));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_suspension))) {
            viewHolder.imgDosageType.setText(R.string.fa_suspension);
            bgShape.setColor(context.getResources().getColor(R.color.suspension));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_toothpaste))) {
            viewHolder.imgDosageType.setText(R.string.fa_toothpaste);
            bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_astringent))) {
            viewHolder.imgDosageType.setText(R.string.fa_gel);
            bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_gel))) {
            viewHolder.imgDosageType.setText(R.string.fa_gel);
            bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_expectorant))) {
            viewHolder.imgDosageType.setText(R.string.fa_syrup);
            bgShape.setColor(context.getResources().getColor(R.color.tablet));
        } else if (unitsCode.toLowerCase().contains(context.getResources().getString(R.string.conditional_respule))) {
            viewHolder.imgDosageType.setText(R.string.fa_respule);
            bgShape.setColor(context.getResources().getColor(R.color.tablet));
        } else {
            viewHolder.imgDosageType.setText(R.string.fa_others);
            bgShape.setColor(context.getResources().getColor(R.color.mixture));
        }
    }

    @Override
    public int getItemCount() {
        return (null != dosageList ? dosageList.size() : 0);
    }

    public void updateList(ArrayList<DoseBean> dosageList) {
        this.dosageList = dosageList;
        this.notifyDataSetChanged();
    }

    public void setViewHolder(ViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    public ViewHolder getViewHolder() {
        return this.viewHolder;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CustomFontTextView tvDosage;
        private CustomFontTextView tvUnit;
        private CustomFontTextView tvTime;
        private CustomFontTextView tvIsAfterOrBefore;
        private IconTextView imgDosageType, imgEdit;
        private int clickedItemPosition;

        public ViewHolder(View itemView) {
            super(itemView);

            tvDosage = (CustomFontTextView) itemView.findViewById(R.id.tv_dosage_frequency);
            tvUnit = (CustomFontTextView) itemView.findViewById(R.id.tv_tablet);
            tvTime = (CustomFontTextView) itemView.findViewById(R.id.tv_time);
            tvIsAfterOrBefore = (CustomFontTextView) itemView.findViewById(R.id.tv_afterOrbefore);
            imgDosageType = (IconTextView) itemView.findViewById(R.id.imgDosageType);
            imgEdit = (IconTextView) itemView.findViewById(R.id.imgEdit);
            if (BuildConfig.isPatientApp) {
                itemView.setOnClickListener(this);
            } else {
                //read mode for physician (Aastha)
                itemView.setClickable(false);
                imgEdit.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            clickedItemPosition = getPosition();
            //    showNumberPickerDialog(clickedItemPosition, getViewHolder().isSOS);
            ((Activity) context).startActivityForResult(new Intent(context, Medicinefrequency_Activity.class).putExtra("dosebean", dosageList.get(clickedItemPosition)).putExtra("clicked_position", clickedItemPosition).putExtra("is_sos", dosageList.get(clickedItemPosition).isSOS()), TextConstants.MEDICINE_FREQUNCY_INTET);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                ((Activity) context).overridePendingTransition(R.anim.bottom_up, R.anim.no_change);
            }
        }
    }

    public ArrayList<DoseBean> getDosageList() {
        return dosageList;
    }

}