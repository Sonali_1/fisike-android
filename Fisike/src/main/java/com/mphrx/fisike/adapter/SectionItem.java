package com.mphrx.fisike.adapter;

public class SectionItem implements MessageItem {

    private String title;

    public SectionItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean isSection() {
        return true;
    }

}
