package com.mphrx.fisike.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;

import java.util.List;

/**
 * Created by laxmansingh on 12/26/2016.
 */

public class CountryListAdapter extends ArrayAdapter<String> {

    private LayoutInflater mLayoutInflater;
    private List<String> country_list;
    private Context mContext;
    private String color;
    private Typeface font_regular, font_semibold;

    public CountryListAdapter(Context context, List country_list, String color) {
        super(context, 0, country_list);
        this.country_list = country_list;
        this.mContext = context;
        this.color = color;
        mLayoutInflater = LayoutInflater.from(context);
        font_semibold = Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.opensans_ttf_semibold));
        font_regular = Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.opensans_ttf_regular));

    }


    public int getCount() {
        return country_list.size();
    }

    public String getItem(int position) {
        return country_list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        Utils.hideKeyboard(mContext, parent);

        final CountryListAdapter.CountryViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.countrycode_spinner_item, parent, false);
            holder = new CountryListAdapter.CountryViewHolder();
            holder.mNameView = (CustomFontTextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (CountryListAdapter.CountryViewHolder) convertView.getTag();
        }

        holder.mNameView.setText(country_list.get(position));
        if (color.equals("black")) {
            holder.mNameView.setTextColor(mContext.getResources().getColor(R.color.black));
        } else if (color.equals("white")) {
            holder.mNameView.setTextColor(mContext.getResources().getColor(R.color.white));
        } else if (color.equals("dusky_blue")) {
            holder.mNameView.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        }

        if (position == 0) {
            holder.mNameView.setTextSize(15);
            holder.mNameView.setTypeface(font_semibold);
        } else {
            holder.mNameView.setTextSize(14);
            holder.mNameView.setTypeface(font_regular);
        }


        return convertView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.countrycode_spinner_item, null);
        }
        CustomFontTextView text1 = (CustomFontTextView) convertView.findViewById(R.id.text1);
        if (position == 0) {
            text1.setTextColor(mContext.getResources().getColor(R.color.dusky_blue_50));
        } else {
            text1.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        }
        text1.setTextSize(13);
        text1.setText(country_list.get(position).equals(mContext.getResources().getString(R.string.Select_Country)) ? mContext.getResources().getString(R.string.Country) : country_list.get(position));
        return convertView;
    }

    private static class CountryViewHolder {
        public CustomFontTextView mNameView;
    }
}
