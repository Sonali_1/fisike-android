package com.mphrx.fisike.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import appointment.utils.CustomTypefaceSpan;

/**
 * Created by Kailash Khurana on 2/17/2016.
 */
public class MedicationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 0;
    private ArrayList<PrescriptionModel> prescriptionModelList;

    private String medicianName, patientName = "";
    private clickListener listener;
    private Context context;
    private boolean isVisible;
    private Typeface font_regular, font_semibold;
    private SpannableString dose_string;
    private boolean isPlural;

    public MedicationAdapter(ArrayList<PrescriptionModel> prescriptionModelList, Context context) {
        this.prescriptionModelList = prescriptionModelList;
        this.context = context;
        font_regular = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
        font_semibold = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_semibold));
    }

    public void pushInfoFront(PrescriptionModel prescriptionModel) {
        prescriptionModelList.add(0, prescriptionModel);
        notifyDataSetChanged();
    }

    public void setPrescriptionModelList(ArrayList<PrescriptionModel> prescriptionModelList) {
        this.prescriptionModelList = prescriptionModelList;
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    public class PrescriberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private IconTextView imgMedician;
        private CustomFontTextView txtMedicationName;
        private CustomFontTextView txtDoses;
        private CustomFontTextView txtNumberOfTimes;
        private CustomFontTextView txtNextMedicine;
        private IconTextView imgOverflow;
        private CustomFontTextView txtPatientName, tvSourceName;
        private LinearLayout lyn_lyt_medicine_time, lyn_lyt_patient_name, llSourceName;
        private FrameLayout temp_view;


        public PrescriberViewHolder(View v) {
            super(v);

            this.txtMedicationName = (CustomFontTextView) v.findViewById(R.id.txtMedicianName);
            this.txtDoses = (CustomFontTextView) v.findViewById(R.id.txtDoses);
            this.txtNumberOfTimes = (CustomFontTextView) v.findViewById(R.id.txtNumberOfTimes);
            this.txtNextMedicine = (CustomFontTextView) v.findViewById(R.id.txtNextMedicine);
            this.imgMedician = (IconTextView) v.findViewById(R.id.imgMedician);
            this.lyn_lyt_medicine_time = (LinearLayout) v.findViewById(R.id.lyn_lyt_medicine_time);
            this.lyn_lyt_patient_name = (LinearLayout) v.findViewById(R.id.lyn_lyt_patient_name);
            temp_view = (FrameLayout) v.findViewById(R.id.temp_view);
            // source name view
            this.llSourceName = (LinearLayout) v.findViewById(R.id.ll_source_name);
            this.tvSourceName = (CustomFontTextView) v.findViewById(R.id.tv_source_name);

            // source name view
            this.llSourceName = (LinearLayout) v.findViewById(R.id.ll_source_name);
            this.tvSourceName = (CustomFontTextView) v.findViewById(R.id.tv_source_name);

            v.setOnClickListener((View.OnClickListener) this);
            v.setOnLongClickListener(new View.OnLongClickListener() {
                                         @Override
                                         public boolean onLongClick(View v) {
                                             if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                                                 listener.itemLongClicked(v, getAdapterPosition());
                                                 return true;
                                             }
                                             return false;
                                         }
                                     }
            );
            this.imgOverflow = (IconTextView) v.findViewById(R.id.imgOverflow);
            this.txtPatientName = (CustomFontTextView) v.findViewById(R.id.txtPatientName);

            if (BuildConfig.isPhysicianApp) {
                this.imgOverflow.setVisibility(View.GONE);
                this.lyn_lyt_medicine_time.setVisibility(View.GONE);
            } else
                this.imgOverflow.setOnClickListener((View.OnClickListener) this);
        }


        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition == RecyclerView.NO_POSITION) {
                adapterPosition = getLayoutPosition();
            }
            if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
                listener.itemClicked(v, adapterPosition);
            }
        }

    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgMedician;
        private CustomFontTextView txtMedicationName;
        private CustomFontTextView txtDoses;
        private CustomFontTextView txtNumberOfTimes;
        private CustomFontTextView txtNextMedicine;
        private ImageButton imgOverflow;
        private CustomFontTextView txtPatientName;


        public FooterViewHolder(View v) {
            super(v);

        }
    }


    @Override
    public int getItemCount() {
        if (prescriptionModelList == null) {
            return 0;
        }
        if (isVisible) {
            return prescriptionModelList.size() + 1;
        } else {
            return prescriptionModelList.size();
        }
    }

    public int getPrescriptionModelListCount() {
        if (prescriptionModelList == null) {
            return 0;
        }
        return prescriptionModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    public void setFooterVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public boolean isFooterViewVisible() {
        return false;
    }

    private boolean isPositionFooter(int position) {
        return position == prescriptionModelList.size();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder recyclerViewHolder, int position) {
        if (recyclerViewHolder instanceof PrescriberViewHolder) {
            PrescriberViewHolder holder = (PrescriberViewHolder) recyclerViewHolder;
            if (position == 0) {
                holder.temp_view.setVisibility(View.VISIBLE);
            } else {
                holder.temp_view.setVisibility(View.GONE);
            }

            PrescriptionModel prescriptionModel = prescriptionModelList.get(position);
            medicianName = prescriptionModel.getMedicineName();
            if (prescriptionModel.getPatientName() != null) {
                patientName = prescriptionModel.getPatientName();
            }

            if (patientName.equals("")) {
                //    holder.txtPatientName.setText("");
                holder.lyn_lyt_patient_name.setVisibility(View.GONE);
            } else {
                holder.txtPatientName.setText(patientName);
                holder.lyn_lyt_patient_name.setVisibility(View.VISIBLE);
            }

            if (medicianName != null)
                holder.txtMedicationName.setText(medicianName.toLowerCase());
            if (prescriptionModel.getSourceName() != null && !prescriptionModel.getSourceName().equals("")) {
                holder.llSourceName.setVisibility(View.VISIBLE);
                holder.tvSourceName.setText(prescriptionModel.getSourceName());
            } else {
                holder.llSourceName.setVisibility(View.GONE);
            }

            if (prescriptionModel.getSourceName() != null && !prescriptionModel.getSourceName().equals("")) {
                holder.llSourceName.setVisibility(View.VISIBLE);
                holder.tvSourceName.setText(prescriptionModel.getSourceName());
            } else {
                holder.llSourceName.setVisibility(View.GONE);
            }

            boolean isToShowDoses = true;
            double doseQuantity = 0;
            double previousDoseQuantity = 0;
            boolean isNextDateToCalculate = checkDatePassed(prescriptionModel.getEndDate(), prescriptionModel.getStartDate());
            boolean isNextDate = false,istoshowNextDateinUI = false;
            String nextDate = "";
            String imgMedicineUnit = "", displayUnit = "";
            String nextUnit = null;
            for (int index = 0; prescriptionModel.getArrayDose() != null && index < prescriptionModel.getArrayDose().size(); index++) {

                MedicineDoseFrequencyModel medicineDoseFrequencyModel = prescriptionModel.getArrayDose().get(index);
                doseQuantity = medicineDoseFrequencyModel.getDoseQuantity();
                if (doseQuantity > 1) {
                    isPlural = true;
                } else {
                    isPlural = false;
                }
                nextUnit = Utils.calculateUnitToDisplay(medicineDoseFrequencyModel.getDoseUnit(), isPlural);
                if (index > 0) {

                    if (isToShowDoses && ((medicineDoseFrequencyModel.getDoseUnit() != null && !displayUnit.equals(nextUnit)) || doseQuantity != previousDoseQuantity)) {
                        isToShowDoses = false;
                        if (!isNextDateToCalculate) {
                            break;
                        }
                    }
                }

                if (isNextDateToCalculate ) {
                    String ampm = medicineDoseFrequencyModel.getDoseTimeAMorPM();
                    if (!Utils.isValueAvailable(ampm)) {
                        ampm = "";
                    }
                    String tempDate = Integer.parseInt(medicineDoseFrequencyModel.getDoseHours()) + ":" + Integer.parseInt(medicineDoseFrequencyModel.getDoseMinutes()) + " " + ampm;
                    isNextDate = calculateNearTime(tempDate);
                    if (isNextDate && (nextDate.equals("") || calculateNearTime(nextDate, tempDate))) {
                        nextDate = tempDate.trim();
                        istoshowNextDateinUI = true;
                    }
                }

                imgMedicineUnit = medicineDoseFrequencyModel.getDoseUnit();

                displayUnit = nextUnit;
                previousDoseQuantity = medicineDoseFrequencyModel.getDoseQuantity();
            }

            if (isToShowDoses) {

                if (doseQuantity == (int) doseQuantity) {
                    dose_string = new SpannableString(context.getResources().getString(R.string.txt_DOSE_space) + (int) doseQuantity + " " + MedicationUnitsEnum.getDisplayedValuefromCode(displayUnit) + " ");
                } else {
                    dose_string = new SpannableString(context.getResources().getString(R.string.txt_space_DOSE_space) + doseQuantity + " " + MedicationUnitsEnum.getDisplayedValuefromCode(displayUnit) + " ");
                }
                dose_string.setSpan(new CustomTypefaceSpan("", font_semibold), 0, 4, 0);
                try {
                    dose_string.setSpan(new CustomTypefaceSpan("", font_regular), 5, dose_string.length(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.txtDoses.setVisibility(View.VISIBLE);
                holder.txtDoses.setText(dose_string);
                holder.txtNumberOfTimes.setText(" (" + MedicationFrequancyEnum.getDisplayedValuefromCode(prescriptionModel.getDoseInstruction()) + ")");
            } else {
                holder.txtDoses.setVisibility(View.GONE);
                holder.txtNumberOfTimes.setText("(" + MedicationFrequancyEnum.getDisplayedValuefromCode(prescriptionModel.getDoseInstruction()) + ")");
            }


            String day = getDay(new Date());
            boolean today = false;

            if (prescriptionModel.getArrayDose() != null && prescriptionModel.getArrayDose().size() > 0 &&
                    prescriptionModel.getArrayDose().get(0) != null && prescriptionModel.getArrayDose().get(0).getDoseWeekdaysList() != null) {
                for (String days : prescriptionModel.getArrayDose().get(0).getDoseWeekdaysList()) {
                    if (day.toLowerCase().equals(days.toLowerCase())) {
                        today = true;
                        break;
                    }
                }
            }

            if (BuildConfig.isPatientApp && isNextDateToCalculate && istoshowNextDateinUI && today) {
                if (DateFormat.is24HourFormat(context)) {
                    if (nextDate.toUpperCase().contains(TextConstants.AM) || nextDate.toUpperCase().contains(TextConstants.PM)) {
                        nextDate = DateTimeUtil.convertSourceDestinationDate(nextDate, DateTimeUtil.destinationTimeFormat, DateTimeUtil.sourceTimeFormat);
                    } else {
                        nextDate = DateTimeUtil.convertSourceDestinationDate(nextDate, DateTimeUtil.sourceTimeFormat, DateTimeUtil.sourceTimeFormat);
                    }
                } else {
                    if (nextDate.toUpperCase().contains(TextConstants.AM) || nextDate.toUpperCase().contains(TextConstants.PM)) {
                        nextDate = DateTimeUtil.convertSourceDestinationDateLocale(nextDate, DateTimeUtil.destinationTimeFormat, DateTimeUtil.destinationTimeFormat);
                    } else {
                        nextDate = DateTimeUtil.convertSourceDestinationDateLocale(nextDate, DateTimeUtil.sourceTimeFormat, DateTimeUtil.destinationTimeFormat);
                    }
                }
                holder.txtNextMedicine.setText(context.getResources().getString(R.string.txt_next_at) + nextDate);
                holder.lyn_lyt_medicine_time.setVisibility(View.VISIBLE);
            } else {
                holder.lyn_lyt_medicine_time.setVisibility(View.GONE);
            }

            GradientDrawable bgShape = (GradientDrawable) holder.imgMedician.getBackground();

            if (imgMedicineUnit == null || imgMedicineUnit.equals("")) {
                holder.imgMedician.setText(R.string.fa_tablet);
                bgShape.setColor(context.getResources().getColor(R.color.tablet));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_tablet))) {
                holder.imgMedician.setText(R.string.fa_tablet);
                bgShape.setColor(context.getResources().getColor(R.color.tablet));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_capsule))) {
                holder.imgMedician.setText(R.string.fa_capsule);
                bgShape.setColor(context.getResources().getColor(R.color.capsule));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_syrup))) {
                holder.imgMedician.setText(R.string.fa_syrup);
                bgShape.setColor(context.getResources().getColor(R.color.syrup));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_injection))) {
                holder.imgMedician.setText(R.string.fa_injection);
                bgShape.setColor(context.getResources().getColor(R.color.injection));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_spray))) {
                holder.imgMedician.setText(R.string.fa_spray);
                bgShape.setColor(context.getResources().getColor(R.color.spray));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_inhaler))) {
                holder.imgMedician.setText(R.string.fa_inhaler);
                bgShape.setColor(context.getResources().getColor(R.color.mixture));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_drop))) {
                holder.imgMedician.setText(R.string.fa_drops);
                bgShape.setColor(context.getResources().getColor(R.color.drop));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_powder))) {
                holder.imgMedician.setText(R.string.fa_powder);
                bgShape.setColor(context.getResources().getColor(R.color.powder));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_granule))) {
                holder.imgMedician.setText(R.string.fa_granule);
                bgShape.setColor(context.getResources().getColor(R.color.granule));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_cream))) {
                holder.imgMedician.setText(R.string.fa_creme);
                bgShape.setColor(context.getResources().getColor(R.color.creame));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_gum_paint))) {
                holder.imgMedician.setText(R.string.fa_gum_paint);
                bgShape.setColor(context.getResources().getColor(R.color.gum_paint));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_mouth_wash))) {
                holder.imgMedician.setText(R.string.fa_moutwash);
                bgShape.setColor(context.getResources().getColor(R.color.mouth_wash));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_oil))) {
                holder.imgMedician.setText(R.string.fa_oil);
                bgShape.setColor(context.getResources().getColor(R.color.oil));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_ointment))) {
                holder.imgMedician.setText(R.string.fa_ointment);
                bgShape.setColor(context.getResources().getColor(R.color.ointment));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_sache))) {
                holder.imgMedician.setText(R.string.fa_sachets);
                bgShape.setColor(context.getResources().getColor(R.color.sachet));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_suspension))) {
                holder.imgMedician.setText(R.string.fa_suspension);
                bgShape.setColor(context.getResources().getColor(R.color.suspension));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_toothpaste))) {
                holder.imgMedician.setText(R.string.fa_toothpaste);
                bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_astringent))) {
                holder.imgMedician.setText(R.string.fa_gel);
                bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_gel))) {
                holder.imgMedician.setText(R.string.fa_gel);
                bgShape.setColor(context.getResources().getColor(R.color.toothpaste));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_expectorant))) {
                holder.imgMedician.setText(R.string.fa_syrup);
                bgShape.setColor(context.getResources().getColor(R.color.tablet));
            } else if (imgMedicineUnit.toLowerCase().contains(context.getResources().getString(R.string.conditional_respule))) {
                holder.imgMedician.setText(R.string.fa_respule);
                bgShape.setColor(context.getResources().getColor(R.color.tablet));
            } else {
                holder.imgMedician.setText(R.string.fa_others);
                bgShape.setColor(context.getResources().getColor(R.color.mixture));
            }
            holder.txtNumberOfTimes.setText("(" + MedicationFrequancyEnum.getDisplayedValuefromCode(prescriptionModel.getDoseInstruction()) + ")");
            holder.imgOverflow.setTag(((PrescriptionModel) prescriptionModelList.get(position)).getID());
        }

    }

    private String getDay(Date date) {

        //setting Locale to US so that day return in generic form like monday tuesday...
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateTimeUtil.eeee_format, Locale.US);
        return simpleDateFormat.format(date).toUpperCase();
    }


    private boolean calculateNearTime(String calculateTime) {
        try {
            Calendar calendar = Calendar.getInstance();
            String amPm = (calendar.get(Calendar.AM_PM) == 0) ? context.getResources().getString(R.string.txt_AM) : context.getResources().getString(R.string.txt_PM);
            String currentTime = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + " " + amPm;
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat, Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat, Locale.US);
            if (calculateTime.contains(TextConstants.AM) || calculateTime.contains(TextConstants.PM))
                sdf = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);
            if (currentTime.contains(TextConstants.AM) || currentTime.contains(TextConstants.PM))
                sdf1 = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);

            Date start = sdf1.parse(currentTime.toLowerCase());
            Date end = sdf.parse(calculateTime.toLowerCase().trim());
            if (calculateTime != null && start != null && start.compareTo(end) < 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean calculateNearTime(String nextDate, String tempDate) {
        try {
            SimpleDateFormat sdf  = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat, Locale.US);
            SimpleDateFormat sdf1  = new SimpleDateFormat(DateTimeUtil.sourceTimeFormat, Locale.US);
            if (nextDate.contains(TextConstants.AM) || nextDate.contains(TextConstants.PM))
                sdf1 = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);
            if (tempDate.contains(TextConstants.AM) || tempDate.contains(TextConstants.PM))
                sdf1 = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat, Locale.US);

            Date temp = sdf.parse(tempDate);
            Date next = sdf1.parse(nextDate);
            if (nextDate != null && temp != null && temp.compareTo(next) < 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkDatePassed(String endDate, String startDate) {
        try {
            Calendar calendar = Calendar.getInstance();
            int mm = calendar.get(Calendar.MONTH) + 1;
            String currentDate = calendar.get(Calendar.YEAR) + "-" + mm + "-" + calendar.get(Calendar.DATE);
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, Locale.getDefault());
            Date current = sdf.parse(currentDate);
            Date end = sdf.parse(endDate);
            Date start = sdf.parse(startDate);
            if ((endDate != null && current != null && (end.compareTo(current) > 0 || end.compareTo(current) == 0)) && (start != null && current != null && (start.compareTo(current) <= 0))) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        if (arg1 == TYPE_ITEM) {
            View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.layout_medications, arg0, false);
            //    View footerView = LayoutInflater.from(arg0.getContext()).inflate(R.layout.loading_view, arg0, false);
            PrescriberViewHolder prescriberHolder = new PrescriberViewHolder(row);
            return prescriberHolder;
        } else if (arg1 == TYPE_FOOTER) {
            View footerView = LayoutInflater.from(arg0.getContext()).inflate(R.layout.loading_view, arg0, false);
            FooterViewHolder footerViewHolder = new FooterViewHolder(footerView);
            return footerViewHolder;
        }
        return null;
    }

    public interface clickListener {
        public void itemClicked(View view, int position);

        public void itemLongClicked(View view, int position);
    }
}
