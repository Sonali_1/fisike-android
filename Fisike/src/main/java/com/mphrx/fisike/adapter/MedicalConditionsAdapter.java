package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.SearchMedicalConditionActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.util.List;

import appointment.utils.CommonTasks;


/**
 * Created by laxmansingh on 10/26/2016.
 */

public class MedicalConditionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<DiseaseMO> list;
    private final int VIEW_TYPE_ITEM = 0;
    private int paddingDp;


    public MedicalConditionsAdapter(Context context, List<DiseaseMO> list) {
        this.mContext = context;
        this.list = list;
        paddingDp = CommonTasks.getDensityPixel(mContext, 40);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.medical_condition_list_item, null);
            MedicalConditionsAdapter.MedicalConditionsViewHolder viewHolder = new MedicalConditionsAdapter.MedicalConditionsViewHolder(view, mContext);
            return viewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int i) {
        if (customViewHolder instanceof MedicalConditionsAdapter.MedicalConditionsViewHolder) {
            MedicalConditionsAdapter.MedicalConditionsViewHolder medicalConditionsViewHolder = (MedicalConditionsAdapter.MedicalConditionsViewHolder) customViewHolder;
            if (i == list.size()) {
                medicalConditionsViewHolder.et_medicalCondition.getEditText().setText("");
                medicalConditionsViewHolder.btnCross.setVisibility(View.GONE);
            } else {
                DiseaseMO diseaseMO = list.get(i);
                medicalConditionsViewHolder.et_medicalCondition.getEditText().setText(diseaseMO.getText().toString().trim());
                if (BuildConfig.isPatientApp)
                    medicalConditionsViewHolder.btnCross.setVisibility(View.VISIBLE);
                else {   //read mode for physician
                    medicalConditionsViewHolder.et_medicalCondition.getEditText().setClickable(false);
                }
            }


            /*[For maintaining the top margin of list item]*/
            RelativeLayout relativeLayout = new RelativeLayout(mContext);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            if (i == 0) {
                params.setMargins(0, 40, 0, 0);
                medicalConditionsViewHolder.rel_lyt.setLayoutParams(params);
            } else {
                params.setMargins(0, 2, 0, 0);
                medicalConditionsViewHolder.rel_lyt.setLayoutParams(params);
            }
                /*[End of For maintaining the top margin of list item]*/
        }
    }

    @Override
    public int getItemCount() {
        if (BuildConfig.isPatientApp)
            return (null != list ? list.size() + 1 : 0 + 1);
        else
            return (null != list ? list.size() : 0);
    }


    public class MedicalConditionsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        ClearableEditText et_medicalCondition;
        IconTextView btnCross;
        RelativeLayout rel_lyt;


        public MedicalConditionsViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            et_medicalCondition = (ClearableEditText) view.findViewById(R.id.et_medicalCondition);
            btnCross = (IconTextView) view.findViewById(R.id.btnCross);
            rel_lyt = (RelativeLayout) view.findViewById(R.id.rel_lyt);
            et_medicalCondition.setIsAddTxtVisible(false);
            et_medicalCondition.setHint(mContext.getResources().getString(R.string.enter_medical_condition));
            et_medicalCondition.getclearTextButton().setVisibility(View.GONE);
            et_medicalCondition.setIsCrossVisible(false);
            et_medicalCondition.setTextSize(14);
            if (Utils.isRTL(context)) {
                et_medicalCondition.setMargins(paddingDp, 0, 14, 22);
            } else {
                et_medicalCondition.setMargins(14, 0, paddingDp, 22);
            }
            et_medicalCondition.getEditText().setFocusable(false);
            et_medicalCondition.getEditText().setFocusableInTouchMode(false);
            et_medicalCondition.getEditText().setMaxLines(1);
            et_medicalCondition.getEditText().setSingleLine(true);

            if (BuildConfig.isPatientApp) {
                et_medicalCondition.getEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int adapter_position = getAdapterPosition();
                        Intent i = new Intent(mContext, SearchMedicalConditionActivity.class);
                        i.putExtra("whichSearch", mContext.getResources().getString(R.string.search_conditions));
                        i.putExtra("position", adapter_position);
                        i.putExtra("condition_name", et_medicalCondition.getEditText().getText().toString().trim());
                        ((Activity) mContext).startActivityForResult(i, TextConstants.MEDICAL_CONDITIONS_SEARCH_INTET);
                    }
                });
                btnCross.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.btnCross:
                    showDeleteMedicineDialog(adapter_position);
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }


    private void showDeleteMedicineDialog(final int position) {
        DialogUtils.showAlertDialogCommon(mContext, mContext.getResources().getString(R.string.alert), mContext.getResources().getString(R.string.delete_medical_condition), mContext.getResources().getString(R.string.txt_ok), mContext.getResources().getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;

                    case Dialog.BUTTON_POSITIVE:
                        list.remove(position);
                        notifyDataSetChanged();
                        dialog.dismiss();
                        break;
                }
            }
        });
    }

    public void showError(RecyclerView recyclerView, boolean isshow) {
        try {
            MedicalConditionsAdapter.MedicalConditionsViewHolder medicalConditionsViewHolder = (MedicalConditionsViewHolder) recyclerView.findViewHolderForLayoutPosition(0);
            if (isshow) {
                if (Utils.isRTL(mContext)) {
                    medicalConditionsViewHolder.et_medicalCondition.setMargins(CommonTasks.getDensityPixel(mContext, 10), 0, 14, 22);
                } else {
                    medicalConditionsViewHolder.et_medicalCondition.setMargins(14, 0, CommonTasks.getDensityPixel(mContext, 10), 22);
                }
                medicalConditionsViewHolder.et_medicalCondition.getEditText().setError(mContext.getResources().getString(R.string.enter_condition));
                Toast.makeText(mContext, mContext.getResources().getString(R.string.please_enter_medical_condition), Toast.LENGTH_SHORT).show();
            } else {
                if (Utils.isRTL(mContext)) {
                    medicalConditionsViewHolder.et_medicalCondition.setMargins(paddingDp, 0, 14, 22);
                } else {
                    medicalConditionsViewHolder.et_medicalCondition.setMargins(14, 0, paddingDp, 22);
                }
                medicalConditionsViewHolder.et_medicalCondition.getEditText().setError(null);
            }
        } catch (Exception ex) {

        }
    }
}
