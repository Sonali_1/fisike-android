package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.utils.DateTimeUtil;

import java.util.ArrayList;

public class NotificationCenterAdapter extends RecyclerView.Adapter<NotificationCenterAdapter.ViewHolder> {
    private Context context;
    private ArrayList<NotificationCenter> mDataset;
    public clickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CustomFontTextView mTextView;
        public CustomFontTextView txtRefId;
        public LinearLayout layoutRow;
        private FrameLayout temp_view;

        public ViewHolder(View v) {
            super(v);
            mTextView = (CustomFontTextView) v.findViewById(R.id.title);
            txtRefId = (CustomFontTextView) v.findViewById(R.id.txtRefId);
            temp_view = (FrameLayout) v.findViewById(R.id.temp_view);
            layoutRow = (LinearLayout) v.findViewById(R.id.layoutRow);
            layoutRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.itemClicked(view, getAdapterPosition());
            }
        }
    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public NotificationCenterAdapter(Context context, ArrayList<NotificationCenter> myDataset) {
        this.context = context;
        this.mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NotificationCenterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_row, parent, false);
        ViewHolder vh = new ViewHolder(row);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if (position == 0) {
            holder.temp_view.setVisibility(View.VISIBLE);
        } else {
            holder.temp_view.setVisibility(View.GONE);
        }
        NotificationCenter notificationCenter = mDataset.get(position);
        if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_MEDICATION)) {
            String object = context.getResources().getString(R.string.Time_to_have) + " " + notificationCenter.getMedicianName();
            holder.mTextView.setText(object);
            holder.txtRefId.setVisibility(View.VISIBLE);
            String date_display = DateTimeUtil.convertSourceDestinationDateLocale(notificationCenter.getMedicationDate() + ", " + notificationCenter.getMedicationTime(),
                    DateTimeUtil.dd_MMM_yyyy_comma_HH_mm_ss, DateTimeUtil.dd_MMM_yyyy_comma_hh_mm_a);
            holder.txtRefId.setText(context.getResources().getString(R.string.Due_at) + " " + date_display);
        } else if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_DOCUMENT_RECORD)) {
            String type = notificationCenter.getDocumentType();
            if (type.equalsIgnoreCase(VariableConstants.REPORT_DIGITIZATION_NOTIFICATION)) {
                type = context.getResources().getString(R.string.lab_report).toLowerCase();
            } else if (type.equalsIgnoreCase(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION)) {
                type = context.getResources().getString(R.string.prescription).toLowerCase();
            } else if (type.equalsIgnoreCase(VariableConstants.DIAGNOSTIC_ORDER)) {
                String object = context.getResources().getString(R.string.Your_lab_report_has_been_updated);
                setView(holder, object, notificationCenter.getRefId());
                return;
            } else {
                String object = context.getResources().getString(R.string.Your_document_has_been_rejected);
                setView(holder, object, notificationCenter.getRefId());
                return;
            }

            String object = context.getResources().getString(R.string.Your) + " " + type + " " + context.getResources().getString(R.string.has_been_digitized);
            setView(holder, object, notificationCenter.getRefId());
        } else if (notificationCenter.getNotificationType().equals(VariableConstants.ALARM_NOTIFICATION_APPOINTMENT)) {
            String type = notificationCenter.getDocumentType();
            if (type.equalsIgnoreCase("APPOINTMENT_STATUS_CHANGED")) {
                String object = notificationCenter.getDocumentStatus();
                setView(holder, object, notificationCenter.getRefId());
                return;
            }
        } else if (notificationCenter.getNotificationType().equals(VariableConstants.MRN_NOTIFICATION)) {
            // MRN notification
            holder.mTextView.setText(context.getResources().getString(R.string.mrn_linking_required));
        }
    }

    private String formateTime(String medicationTime) {
        if (medicationTime != null && medicationTime.contains(":")) {
            String[] timeArray = medicationTime.split(":");
            if (timeArray.length > 2) {
                int hr = Integer.parseInt(timeArray[0].trim());
                String amPm = hr > 12 ? context.getResources().getString(R.string.txt_PM) : context.getResources().getString(R.string.txt_AM);
                hr = hr > 12 ? hr - 12 : hr;
                return hr + ":" + timeArray[1] + " " + amPm;
            }

        }
        return medicationTime;
    }

    private void setView(ViewHolder holder, String object, String refId) {
        holder.mTextView.setText(object);
        holder.txtRefId.setVisibility(View.VISIBLE);
        if (refId.equalsIgnoreCase("0")) {
            holder.txtRefId.setVisibility(View.INVISIBLE);
        } else {
            holder.txtRefId.setText(context.getResources().getString(R.string.appointment_id) + " " + refId);
            holder.txtRefId.setVisibility(View.VISIBLE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}