package com.mphrx.fisike.adapter;

import java.util.ArrayList;

public class OptionImageObj {

    private String imgURL;
    private ArrayList<Byte> imgData;
    private String optionText;

    public OptionImageObj(String imgURL, ArrayList<Byte> imgData, String optionText) {
        this.imgURL = imgURL;
        this.imgData = imgData;
        this.optionText = optionText;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public ArrayList<Byte> getImgData() {
        return imgData;
    }

    public void setImgData(ArrayList<Byte> imgData) {
        this.imgData = imgData;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }
}