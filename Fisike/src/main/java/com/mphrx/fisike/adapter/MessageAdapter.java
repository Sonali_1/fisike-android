package com.mphrx.fisike.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.MessageActivity;
import com.mphrx.fisike.Queue.AttachmentQueue;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.UploadTaggingFragment;
import com.mphrx.fisike.lazyloading.DecriptThumbnail;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.MasterLayout;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * MessageAdapter is a Custom class to implement custom row in ListView
 */
public class MessageAdapter extends BaseAdapter {
    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;
    private Context mContext;
    private ArrayList<MessageItem> mMessages;
    private UserMO userMO;
    private boolean isGroupChat;
    private DecriptThumbnail imgLoader;
    private boolean isPatient;
    private boolean isSyncTapped;
    private boolean isToShowChatContactNames;

    public MessageAdapter(Context context, ArrayList<MessageItem> messages, boolean isGroupChat,
                          boolean isPatient, boolean isToShowChatContactNames) {
        super();
        this.mContext = context;
        this.mMessages = messages;
        this.isGroupChat = isGroupChat;
        this.isPatient = isPatient;
        userMO = SettingManager.getInstance().getUserMO();
        imgLoader = new DecriptThumbnail(context);
        this.isToShowChatContactNames = isToShowChatContactNames;
    }

    public static String convertToStringRepresentation(final long value) {
        final long[] dividers = new long[]{T, G, M, K, 1};
        final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"};
        if (value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            final long divider = dividers[i];
            if (value >= divider) {
                result = format(value, divider, units[i]);
                break;
            }
        }
        return result;
    }

    private static String format(final long value, final long divider, final String unit) {
        final double result = divider > 1 ? (double) value / (double) divider : (double) value;
        return new DecimalFormat("#,##0.#").format(result) + " " + unit;
    }

    @Override
    public int getCount() {
        return mMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessages.get(position);
    }

    public void setMessageItem(int position, int imageRecordSyncStatus) {
        ChatMessageMO chatMessageMO = (ChatMessageMO) mMessages.get(position);
        chatMessageMO.setImageRecordSyncStatus(imageRecordSyncStatus);
        mMessages.set(position, chatMessageMO);
        notifyDataSetChanged();
    }

    public int getPosition(long pk) {
        for (int i = 0; mMessages != null && i < mMessages.size(); i++) {
            MessageItem item = (MessageItem) this.getItem(i);

            if (item != null) {
                if (!item.isSection()) {
                    ChatMessageMO message = (ChatMessageMO) item;
                    if (message.getPersistenceKey() == pk) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MessageItem item = (MessageItem) this.getItem(position);

        if (item != null) {
            if (item.isSection()) {
                // Section header for message
                SectionItem message = (SectionItem) item;
                convertView = LayoutInflater.from(mContext).inflate(R.layout.section_header, parent, false);
                CustomFontTextView txtSectionHeader = (CustomFontTextView) convertView.findViewById(R.id.txtSectionHeader);
                txtSectionHeader.setText(message.getTitle());
                return convertView;
            } else {
                /**
                 * Display chat messages
                 */
                final ChatMessageMO message = (ChatMessageMO) this.getItem(position);

                ViewHolder holder;
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.sms_row, parent, false);
                holder.message = (CustomFontTextView) convertView.findViewById(R.id.message_text);
                holder.layoutMessage = (LinearLayout) convertView.findViewById(R.id.layoutMessage);
                try {
                    holder.layoutSubMessage = (LinearLayout) convertView.findViewById(R.id.layoutSubMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.imgMsgStatus = (ImageView) convertView.findViewById(R.id.imgMsgStatus);
                holder.time = (CustomFontTextView) convertView.findViewById(R.id.message_time);
                holder.messageStatus = (CustomFontTextView) convertView.findViewById(R.id.messageStatus);
                holder.imgActivity = (ImageView) convertView.findViewById(R.id.imgActivity);
                holder.imgImageStatus = (ImageView) convertView.findViewById(R.id.imgImageStatus);
                holder.masterLayout = (MasterLayout) convertView.findViewById(R.id.masterLayout);
                holder.progressDownloading = (ProgressBar) convertView.findViewById(R.id.progressDownloading);
                holder.relativeAttachmentLayout = (RelativeLayout) convertView.findViewById(R.id.relativeAttachmentLayout);
                holder.txtAttachmentStatus = (CustomFontTextView) convertView.findViewById(R.id.txtAttachmentStatus);

                holder.senderName = (CustomFontTextView) convertView.findViewById(R.id.senderName);
                holder.timeLayout = (LinearLayout) convertView.findViewById(R.id.time);
                holder.attachmentLayout = (LinearLayout) convertView.findViewById(R.id.attachmentLayout);
                holder.imgAttachmentType = (ImageView) convertView.findViewById(R.id.imgAttachmentType);
                holder.dividerDigitize = convertView.findViewById(R.id.dividerDigitize);
                holder.layoutDigitize = (LinearLayout) convertView.findViewById(R.id.layoutDigitize);
                holder.btnLater = (com.mphrx.fisike.customview.CustomFontButton) convertView.findViewById(R.id.btnLater);
                holder.btnSync = (com.mphrx.fisike.customview.CustomFontButton) convertView.findViewById(R.id.btnSync);

                if (null == message) {
                    return null;
                }

                Date date = new Date(message.getLasUpdatedTimeUTC());

                boolean isMine = message.getSenderUserId().equals(userMO.getId() + "");
               if (isToShowChatContactNames || isGroupChat) {
                    holder.senderName.setVisibility(View.VISIBLE);
                    holder.senderName.setText(message.getSenderNickName());
                    if (isMine) {
                        holder.senderName.setTextColor(mContext.getResources().getColor(R.color.btn_bg_normal));
                    } else {
                        holder.senderName.setTextColor(mContext.getResources().getColor(R.color.color_888888));
                    }
               }

                holder.message.setText(message.getMessageText());

                String attachmentType = message.getAttachmentType();
                /**
                 * Check for message Type
                 */
                /**
                 * Normal message
                 */
                if (null == attachmentType || "".equals(attachmentType) || "null".equalsIgnoreCase(attachmentType)) {
                    holder.messageStatus.setVisibility(View.GONE);
                    holder.imgActivity.setVisibility(View.GONE);
                    holder.imgImageStatus.setVisibility(View.GONE);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.setVisibility(View.GONE);
                }
                /**
                 * Image attachment or Video attachment
                 */
                else if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE)
                        || attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)) {
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.attachmentLayout.getLayoutParams();
                    lp.height = mContext.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size);
                    lp.width  = 2 * mContext.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size);
                    RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) holder.imgImageStatus.getLayoutParams();
                    lp1.height = LayoutParams.MATCH_PARENT;
                    lp1.width  = 2 * mContext.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size);

                    String attachmentStatus = message.getAttachmentStatus();

                    holder.attachmentLayout.setOrientation(LinearLayout.VERTICAL);

                    holder.imgActivity.setVisibility(View.VISIBLE);
                    holder.imgImageStatus.setVisibility(View.VISIBLE);

                    holder.relativeAttachmentLayout.setLayoutParams(lp);
                    holder.imgImageStatus.setLayoutParams(lp1);
                    holder.imgActivity.setLayoutParams(lp1);

                    int attachmentDrawable = attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE) ? R.drawable.icon_att_image
                            : R.drawable.icon_att_video;

                    String color = "#00f0f0f5";
                    holder.progressDownloading.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
//                            TaskManager instance = TaskManager.getInstance();
//                            boolean ismTaskQueueExist = instance.isQueueThread(message.getPersistenceKey() + "");
//                            Toast.makeText(mContext, "Queue : " + ismTaskQueueExist, Toast.LENGTH_SHORT).show();
                            AttachmentQueue attachmentQueue = AttachmentQueue.getInstance();
                            attachmentQueue.cancelRequest(message.getPersistenceKey() + "");

                        }
                    });

                    byte[] imageThumb = message.getImageThumb();

                    imgLoader.DisplayImage(message.getImageThumbnailPath(), holder.imgActivity, attachmentDrawable, imageThumb,
                            holder.progressDownloading, null);

                    holder.timeLayout.setPadding(0, 10, 0, 0);
                    if (isPatient
                            && (message.getImageRecordSyncStatus() == VariableConstants.RECORED_SYNC_STATUS_DEFAULT)
                            && !attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)
                            && (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_DOWNLOADED) || attachmentStatus
                            .equals(TextConstants.ATTACHMENT_STATUS_UPLOADED))) {
                        holder.dividerDigitize.setVisibility(View.VISIBLE);
                        holder.layoutDigitize.setVisibility(View.VISIBLE);

                        holder.message.setText(mContext.getResources().getString(R.string.digitize_msg));
                        holder.message.setWidth(2 * mContext.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size));

                        holder.btnLater.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                ((MessageActivity) mContext).setRecordUploaded(position, VariableConstants.RECORED_SYNC_STATUS_CANCEL);
                            }
                        });
                        isSyncTapped = false;
                        holder.btnSync.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (isSyncTapped) {
                                    return;
                                }
                                isSyncTapped = true;
                                ((MessageActivity) mContext).findViewById(R.id.layoutChat).setVisibility(View.INVISIBLE);

                                UploadTaggingFragment uploadTaggingFragment = new UploadTaggingFragment();
                                Bundle bundle = new Bundle();
                                bundle.putInt(VariableConstants.ITEM_SYNC_MESSAGE, position);
                                bundle.putString(VariableConstants.VIDEO_URI, message.getAttachmentPath());
                                bundle.putString(VariableConstants.IMAGE_THUMP_PATH, message.getImageThumbnailPath());
                                uploadTaggingFragment.setArguments(bundle);

                                FragmentTransaction fragmentTransaction = ((MessageActivity) mContext).getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.add(R.id.frame_container, uploadTaggingFragment);
                                fragmentTransaction.commitAllowingStateLoss();
                            }
                        });

                    } else {
                        int attachmentInticatorDrawable = attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE) ? 0
                                : R.drawable.ic_send_video_msg;
                        holder.imgAttachmentType.setImageResource(attachmentInticatorDrawable);

                        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) holder.time.getLayoutParams();
                        lp2.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                        lp2.width = 2 * mContext.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size);
                        holder.timeLayout.setLayoutParams(lp2);
                        holder.time.setGravity(Gravity.RIGHT);
                        holder.imgAttachmentType.setVisibility(View.VISIBLE);
                        if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)) {
                            holder.timeLayout.setBackgroundResource(R.color.Video_strip_bar);
                            holder.timeLayout.setGravity(Gravity.CENTER_VERTICAL);
                            holder.time.setTextColor(Color.WHITE);
                        }
                        holder.message.setVisibility(View.GONE);
                    }
                    // holder.message.setVisibility(View.VISIBLE);

                    if (null != attachmentStatus
                            && (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_IN_PROGRESS)
                            || attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_DOWNLOADING) || attachmentStatus
                            .equals(TextConstants.ATTACHMENT_STATUS_WAITING))) {
                        color = "#B3000000";
                        holder.txtAttachmentStatus.setVisibility(View.VISIBLE);
                        holder.txtAttachmentStatus.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_att_cancel, 0, 0);
                        holder.txtAttachmentStatus.setText("");
                        int percentage = message.getPercentage();
                        // int percentage = holder.progressDownloading.cusview.getProgress();
                        if (percentage > 0) {
                            holder.masterLayout.setVisibility(View.VISIBLE);
                            // holder.progressDownloading.stopAnimation();
                            holder.masterLayout.cusview.setupprogress(percentage);
                            holder.masterLayout.cusview.invalidate();
                            holder.progressDownloading.setVisibility(View.INVISIBLE);

                            holder.masterLayout.setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
//                                    TaskManager instance = TaskManager.getInstance();
//                                    boolean ismTaskQueueExist = instance.isActiveThread(message.getPersistenceKey() + "");
//                                    Toast.makeText(mContext, "Queue : " + ismTaskQueueExist, Toast.LENGTH_SHORT).show();
                                    AttachmentQueue attachmentQueue = AttachmentQueue.getInstance();
                                    attachmentQueue.cancelRequest(message.getPersistenceKey() + "");

//                                    uiUpdate
                                }
                            });

                        } else {
                            holder.progressDownloading.setVisibility(View.VISIBLE);
                            holder.masterLayout.setVisibility(View.INVISIBLE);
                            // holder.progressDownloading.startAnimation();
                        }
                    } else {
                        // holder.progressDownloading.setVisibility(View.GONE);
                        holder.masterLayout.setVisibility(View.GONE);
                        if (null != attachmentStatus) {
                            int drawableResource = 0;
                            // long fileSize = message.getFileSize();
                            // String downloadStatus = fileSize <= 0 ? "" : convertToStringRepresentation(fileSize);
                            String downloadStatus = "";
                            if (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_RECEIVED)) {
                                drawableResource = R.drawable.icon_att_download;
                                // downloadStatus = TextConstants.ATTACHMENT_STATUS_TEXT_SIZE;
                                color = "#B3000000";
                            } else if (attachmentStatus.equals(TextConstants.ATTACHMENT_STATUS_NOT_UPLOADED)) {
                                drawableResource = R.drawable.icon_att_upload;
                                downloadStatus = TextConstants.ATTACHMENT_STATUS_TEXT_READDY;
                                color = "#B3000000";
                            } else {
                                holder.attachmentLayout.setBackgroundColor(Color.parseColor("#99000000"));
                            }
                            holder.txtAttachmentStatus.setVisibility(View.VISIBLE);
                            holder.txtAttachmentStatus.setCompoundDrawablesWithIntrinsicBounds(0, drawableResource, 0, 0);
                            holder.txtAttachmentStatus.setText(downloadStatus);
                        }
                    }
                    holder.imgImageStatus.setBackgroundColor(Color.parseColor(color));
                }

                /**
                 * Questionary
                 */
                else if (attachmentType.equalsIgnoreCase(TextConstants.ATTACHMENT_ACTVITY)) {
                    holder.attachmentLayout.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.attachmentLayout.getLayoutParams();
                    lp.width = LayoutParams.WRAP_CONTENT;
                    lp.height = LayoutParams.WRAP_CONTENT;
                    holder.attachmentLayout.setLayoutParams(lp);
                    holder.messageStatus.setVisibility(View.VISIBLE);
                    String messageStatus = isMine ? TextConstants.ACTIVITY_VIEW : TextConstants.ACTIVITY_RECEIVED;
                    holder.messageStatus.setText(messageStatus);
                    RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) holder.imgActivity.getLayoutParams();
                    lp1.width = LayoutParams.WRAP_CONTENT;
                    lp1.height = LayoutParams.WRAP_CONTENT;
                    holder.imgActivity.setLayoutParams(lp1);
                    if (message.getMessageText().equalsIgnoreCase(TextConstants.CONPLETED_ACTIVITY_TEXT_BODY)) {
//                        holder.imgActivity.setImageResource(R.drawable.activity_received);
                    } else {
//                        holder.imgActivity.setImageResource(R.drawable.qb_send);
                    }
                    holder.imgActivity.setVisibility(View.VISIBLE);
                    holder.message.setPadding(10, 0, 0, 0);
                    holder.imgImageStatus.setVisibility(View.GONE);
                    holder.progressDownloading.setVisibility(View.GONE);
                    holder.masterLayout.setVisibility(View.GONE);
                }
                java.text.DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(mContext);
                //holder.time.setText(timeFormat.format(date));
                holder.time.setText(DateTimeUtil.getFormattedTime(date));
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.message.getLayoutParams();

                // holder.layoutSubMessage.setGravity(Gravity.LEFT);
                /**
                 * Show bubble in the screen -Left if receive - Right if send
                 */
                if (isMine) {
                    holder.layoutMessage.setBackgroundResource(R.drawable.speech_bubble_green);
                    lp.gravity = Gravity.RIGHT;
                } else {
                    holder.layoutMessage.setBackgroundResource(R.drawable.speech_bubble_white);
                    lp.gravity = Gravity.LEFT;
                }
                holder.layoutMessage.setLayoutParams(lp);

                /**
                 * Show the status of the message
                 */
                int msgSentStatus = message.getMsgStatus();
                if (!isMine) {
                    holder.imgMsgStatus.setBackgroundResource(0);
                    holder.imgMsgStatus.setContentDescription(DefaultConnection.MESSAGE_RECEIVED_CONTENT);
                } else if (msgSentStatus == DefaultConnection.MESSAGE_SENT || msgSentStatus == DefaultConnection.MESSAGE_SENT_TO_SERVER) {
                    holder.imgMsgStatus.setBackgroundResource(R.drawable.message_sent);
                    holder.imgMsgStatus.setContentDescription(DefaultConnection.MESSAGE_SENT_CONTENT);
                } else if (msgSentStatus == DefaultConnection.MESSAGE_NOT_SENT) {
                    holder.imgMsgStatus.setBackgroundResource(R.drawable.process_msg);
                    holder.imgMsgStatus.setContentDescription(DefaultConnection.MESSAGE_NOT_SENT_CONTENT);
                } else if (msgSentStatus == DefaultConnection.MESSAGE_DELIVERED) {
                    holder.imgMsgStatus.setBackgroundResource(R.drawable.delivered_image);
                    holder.imgMsgStatus.setContentDescription(DefaultConnection.MESSAGE_DELIVERED_CONTENT);
                } else {
                    holder.imgMsgStatus.setBackgroundResource(0);
                    holder.imgMsgStatus.setContentDescription(DefaultConnection.MESSAGE_RECEIVED_CONTENT);
                }
                return convertView;
            }
        }
        return null;
    }

    /**
     * Clear the imageLoader memory
     */
    public void clearSDCard() {
        imgLoader.clearSdCard();
    }

    @Override
    public long getItemId(int position) {
        // Unimplemented, because we aren't using Sqlite.
        return 0;
    }

    public void clear() {
        mMessages.clear();
    }

    public void setmMessages(ArrayList<MessageItem> mMessages) {
        this.mMessages = mMessages;
    }

    public void setmMessages(ChatMessageMO chatMessageMO, int position) {
        if (position == -1) {
            return;
        }
        MessageItem item = mMessages.get(position);
        if (!item.isSection() && ((ChatMessageMO) item).getPersistenceKey() == chatMessageMO.getPersistenceKey()) {
            mMessages.set(position, chatMessageMO);
        }
    }

    private static class ViewHolder {
        private CustomFontTextView message;
        private CustomFontTextView messageStatus;
        private CustomFontTextView time;
        private LinearLayout layoutMessage;
        private LinearLayout layoutSubMessage;
        private ImageView imgMsgStatus;
        private ImageView imgActivity;
        private LinearLayout attachmentLayout;
        private ImageView imgImageStatus;
        private MasterLayout masterLayout;
        private ProgressBar progressDownloading;
        private RelativeLayout relativeAttachmentLayout;
        private CustomFontTextView txtAttachmentStatus;
        private LinearLayout timeLayout;
        private CustomFontTextView senderName;
        private ImageView imgAttachmentType;
        private View dividerDigitize;
        private LinearLayout layoutDigitize;
        private com.mphrx.fisike.customview.CustomFontButton btnLater;
        private com.mphrx.fisike.customview.CustomFontButton btnSync;
    }
}
