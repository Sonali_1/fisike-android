package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ChatContactMO;

import java.util.ArrayList;
import java.util.List;

public class SelectedInviteeAdapter extends BaseAdapter {

    private ArrayList<ChatContactMO> memberList = null;
    private Context context;
    private GroupInviteAdapter adapter;
    private  com.mphrx.fisike.customview.CustomFontButton addMemberButton;
    private ActionBar actionSelectedInviteCount;


    public SelectedInviteeAdapter(Context context, List<ChatContactMO> inviteeList, GroupInviteAdapter groupInviteAdapter, 
    		                       com.mphrx.fisike.customview.CustomFontButton addMemberButton, ActionBar actionSelectionCount) {
        this.context = context;
        this.memberList = (ArrayList<ChatContactMO>) inviteeList;
        this.addMemberButton = addMemberButton;
        this.actionSelectedInviteCount = actionSelectionCount;
        adapter = groupInviteAdapter;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.group_list_cell, null);
            holder = new ViewHolder();
            holder.tvMemberName = (CustomFontTextView) convertView.findViewById(R.id.name);
            holder.remove = (ImageView) convertView.findViewById(R.id.img_remove);
            holder.ivProfilePic = (ImageView) convertView.findViewById(R.id.img_group_member);
            holder.tvMemberOccupation = (CustomFontTextView) convertView.findViewById(R.id.description);

            holder.remove.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImageView ivRemove = (ImageView) v;
                    ChatContactMO mo = (ChatContactMO) ivRemove.getTag();
                    int i = adapter.getInviteList().indexOf(mo);
                    if (i != -1) {
                        adapter.getInviteList().get(i).setIsSelected(false);
                        if (adapter.getInvitees().indexOf(mo) != -1) {
                            adapter.getInvitees().remove(mo);
                        }
                    } else if (adapter.getInvitees().indexOf(mo) != -1) {
                        adapter.getInvitees().remove(mo);
                    }
                    if (!(addMemberButton.getVisibility() == View.VISIBLE)) {
                        addMemberButton.setVisibility(View.VISIBLE);
                    }
                    actionSelectedInviteCount.setSubtitle(context.getResources().getQuantityString(R.plurals.number_of_contacts_selected, 
                                                          adapter.getSelectedMembersCount(),
                                                          adapter.getSelectedMembersCount()));
                    adapter.notifyDataSetChanged();

                    memberList.remove(mo);
                    SelectedInviteeAdapter.this.notifyDataSetChanged();
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ChatContactMO invitedMember = memberList.get(position);
        holder.tvMemberName.setText(invitedMember.getRealName());
        if (invitedMember.getSpeciality() != null && !invitedMember.getSpeciality().equals(context.getResources().getString(R.string.txt_null))) {
            holder.tvMemberOccupation.setText(invitedMember.getSpeciality());
        } else {
            holder.tvMemberOccupation.setText(invitedMember.getUserType().getName());
        }
        holder.remove.setTag(invitedMember);
        return convertView;

    }

    @Override
    public int getCount() {
        return memberList == null ? 0 : memberList.size();
    }

    @Override
    public Object getItem(int position) {
        return memberList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private List<ChatContactMO> removeDuplicates(List<ChatContactMO> list) {
        list.size();
        List<ChatContactMO> finalList = new ArrayList<ChatContactMO>();
        for (ChatContactMO contactMO : list) {
            if (!finalList.contains(contactMO)) {
                finalList.add(contactMO);
            }
        }
        finalList.size();
        return finalList;
    }

    public class ViewHolder {
        public CustomFontTextView tvMemberName;
        public CustomFontTextView tvMemberOccupation;
        public ImageView remove;
        public ImageView ivProfilePic;
    }


}
