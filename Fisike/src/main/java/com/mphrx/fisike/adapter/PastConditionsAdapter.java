package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike_physician.models.PastConditionsModel;

import java.util.List;

/**
 * Created by laxmansingh on 5/25/2016.
 */
public class PastConditionsAdapter extends RecyclerView.Adapter<PastConditionsAdapter.PastConditionsViewHolder> {

    Context context;
    List<PastConditionsModel> list;
    CustomFontTextView tv_view_more;

    public PastConditionsAdapter(Context context, List<PastConditionsModel> list, CustomFontTextView tv_view_more) {
        this.list = list;
        this.context = context;
        this.tv_view_more = tv_view_more;
    }

    @Override
    public PastConditionsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.past_conditions_list_item, null);
        PastConditionsViewHolder viewHolder = new PastConditionsViewHolder(view, context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PastConditionsViewHolder customViewHolder, final int i) {
        PastConditionsModel pastConditionsModel = (PastConditionsModel) list.get(i);
        customViewHolder.tv_title.setText(pastConditionsModel.getDisplay().toString().trim());


        if (pastConditionsModel.getDuration().equals("") && pastConditionsModel.getDuration() == null && pastConditionsModel.getDuration().equals(context.getResources().getString(R.string.txt_null))) {
            customViewHolder.tv_period_val.setText(context.getResources().getString(R.string.unavailable));
        } else {
            customViewHolder.tv_period_val.setText(pastConditionsModel.getDuration().toString().trim());
        }


        if (i == getItemCount() - 1) {
            customViewHolder.temp_view1.setVisibility(View.GONE);
        } else {
            customViewHolder.temp_view1.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (tv_view_more.getTag().equals(context.getResources().getString(R.string.tag_more))) {
            return (null != list ? 2 : 0);
        } else if (tv_view_more.getTag().equals(context.getResources().getString(R.string.tag_less))) {
            return (null != list ? list.size() : 0);
        }
        return (null != list ? list.size() : 0);
    }


    public class PastConditionsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_title, tv_period_val, tv_status_val, tv_severity_val, tv_location_val;
        View temp_view1;
        IconTextView menu_overflow;

        public PastConditionsViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);
            tv_title = (CustomFontTextView) view.findViewById(R.id.tv_title);
            tv_period_val = (CustomFontTextView) view.findViewById(R.id.tv_period_val);
            tv_status_val = (CustomFontTextView) view.findViewById(R.id.tv_status_val);
            tv_severity_val = (CustomFontTextView) view.findViewById(R.id.tv_severity_val);
            tv_location_val = (CustomFontTextView) view.findViewById(R.id.tv_location_val);
            menu_overflow = (IconTextView) view.findViewById(R.id.menu_overflow);
            menu_overflow.setOnClickListener(this);
            temp_view1 = (View) view.findViewById(R.id.temp_view1);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.menu_overflow:
                    showPopup(menu_overflow, position);
                    break;
            }
        }
    }


    public void showPopup(View v, final int position) {
        final PopupMenu popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();

        inflater.inflate(R.menu.menu_pastconditions, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.cancel:
                        popup.dismiss();
                        break;

                    default:
                        break;
                }

                return true;
            }
        });
    }


}