package com.mphrx.fisike.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;

/**
 * Created by kkhurana on 4/22/2015.
 */
public class StatusAdapter extends ArrayAdapter<String> {
    private String[] objects;
    private Context context;

    public StatusAdapter(Context context, int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomFontTextView txtStatus;
        View view = convertView;
        view = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.text_status, parent, false);
        view.setEnabled(false);
        txtStatus = (CustomFontTextView) view.findViewById(R.id.txtStatus);
        txtStatus.setText(objects[position]);
        return view;
    }

    @Override
    public String getItem(int position) {
        return objects[position];
    }
}
