package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SelectLanguageItemData;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;

public class SelectLanguageAdapter extends RecyclerView.Adapter<SelectLanguageAdapter.LanguageViewHolder> {
    private final String languageSlected;
    private ArrayList<SelectLanguageItemData> languageItemList;
    private String title;
    public clickListener listener;
    private Context context;
    private RadioButton lastChecked;
    private int lastRadioPos;

    public SelectLanguageAdapter(ArrayList<SelectLanguageItemData> languageItemList, Context context) {
        this.languageItemList = languageItemList;
        this.context = context;
        languageSlected = SharedPref.getLanguageSelected();
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    public class LanguageViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        // each data item is just a string in this case
        CustomFontTextView tvTitle;
        RadioButton rb_select_lang;
        View temp_line;

        public LanguageViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.tv_notificationTitle);
            rb_select_lang = (RadioButton) v.findViewById(R.id.rb_select_lang);
            rb_select_lang.setOnClickListener(this);
            temp_line = (View) v.findViewById(R.id.temp_line);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                RadioButton cb = (RadioButton) v;
                int clickedPos = ((Integer) cb.getTag()).intValue();
                if (cb.isChecked()) {
                    if (lastChecked != null) {
                        lastChecked.setChecked(false);
                    }
                    if(!cb.isChecked()){
                        cb.setChecked(true);
                    }
                    lastChecked = cb;
                    lastRadioPos = clickedPos;
                    listener.itemClicked(v, getAdapterPosition());
                } else {
                    cb.setChecked(true);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return languageItemList.size();
    }

    @Override
    public void onBindViewHolder(LanguageViewHolder holder, int position) {
        title = languageItemList.get(position).getTitle();
        holder.tvTitle.setText(title);
        if (title.equalsIgnoreCase(languageSlected)) {
            holder.rb_select_lang.setChecked(true);
        } else {
            holder.rb_select_lang.setChecked(false);
        }

        if (position == languageItemList.size() - 1) {
            holder.temp_line.setVisibility(View.GONE);
        } else {
            holder.temp_line.setVisibility(View.VISIBLE);
        }
        holder.rb_select_lang.setTag(new Integer(position));

        if (holder.rb_select_lang.isChecked()) {
            lastChecked = holder.rb_select_lang;
            lastRadioPos = position;
        }
    }


    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.language_setting_row, arg0, false);
        LanguageViewHolder settingHolder = new LanguageViewHolder(row);
        return settingHolder;

    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

    public String getItemAtPosition(int position) {
        return languageItemList.get(position).getTitle();
    }

}
