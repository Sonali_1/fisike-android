package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.VitalLionCodeEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.models.ObservationList;
import com.mphrx.fisike.models.VitalsModel;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.record_screen.activity.ObservationHistoryActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.UnitsEnum;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike.vital_submit.activity.EnterObservationActivity;

import java.util.List;
import java.util.Map;

/**
 * Created by laxmansingh on 7/28/2016.
 */
public class VitalsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Map<String, VitalsModel> list;
    private List<VitalsModel> list_bloodglucose;
    private final int VIEW_TYPE_HEADER = 0;
    private final int VIEW_TYPE_ITEM = 1;
    public static final int OBSERVATION_SELECT = 1111;
    private VitalsConfigMO vitalsConfigMO;

    public VitalsAdapter(Context context, final Map<String, VitalsModel> list, List<VitalsModel> list_bloodglucose) {
        this.list = list;
        this.mContext = context;
        this.list_bloodglucose = list_bloodglucose;
        try {
            vitalsConfigMO = VitalsConfigDBAdapter.getInstance(context).fetchVitalsConfigMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vitals_list_item, null);
            VitalsViewHolder vitalsViewHolder = new VitalsViewHolder(view, mContext);
            return vitalsViewHolder;
        } else if (viewType == VIEW_TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vital_default_list_item, null);
            VitalsDefaultViewHolder vitalsDefaultViewHolder = new VitalsDefaultViewHolder(view, mContext);
            return vitalsDefaultViewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int position) {
        vitalsConfigMO = VitalsConfigMO.getInstance();
        if (customViewHolder instanceof VitalsViewHolder) {
            if (isToAddBMI() && position == getBMIVitalPosition()) {
                //initializing view of BMI
                initializeBMIDataFilledView(customViewHolder);
            } else if (isToAddBP() && position == getBPVitalPostion()) {
                initializeBPDataFilledView(customViewHolder);
            } else if (isToAddGlucose() && position == getGlucoseVitalPostion()) {
                initializeGlucoseDataFilledView(customViewHolder);
            }
            if (BuildConfig.isPhysicianApp) {
                ((VitalsViewHolder) customViewHolder).img_add.setVisibility(View.INVISIBLE);
            }
        } else if (customViewHolder instanceof VitalsDefaultViewHolder) {
            if (isToAddBMI() && position == getBMIVitalPosition()) {
                ((VitalsDefaultViewHolder) customViewHolder).temp_view.setVisibility(View.VISIBLE);
                ((VitalsDefaultViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_bmi));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setText(mContext.getResources().getString(R.string.fa_observation_bmi));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setTextColor(mContext.getResources().getColor(R.color.fill3));

            } else if (isToAddBP() && position == getBPVitalPostion()) {

                ((VitalsDefaultViewHolder) customViewHolder).temp_view.setVisibility(View.GONE);
                ((VitalsDefaultViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_blood_pressure));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setText(mContext.getResources().getString(R.string.fa_observation_glucose));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setTextColor(mContext.getResources().getColor(R.color.fill1));

            } else if (isToAddGlucose() && position == getGlucoseVitalPostion()) {
                ((VitalsDefaultViewHolder) customViewHolder).temp_view.setVisibility(View.GONE);
                ((VitalsDefaultViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_blood_glucose));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setText(mContext.getResources().getString(R.string.fa_observation_bp));
                ((VitalsDefaultViewHolder) customViewHolder).img_type.setTextColor(mContext.getResources().getColor(R.color.fill2));
            }

            //setting button visibility, update them on the basis of validation list
            if (BuildConfig.isPhysicianApp) {
                ((VitalsDefaultViewHolder) customViewHolder).img_add.setVisibility(View.INVISIBLE);
                ((VitalsDefaultViewHolder) customViewHolder).btn_add.setVisibility(View.GONE);
                ((VitalsDefaultViewHolder) customViewHolder).tv_no_record_uploaded_msg.setVisibility(View.VISIBLE);
            } else if (BuildConfig.isPatientApp) {
                ((VitalsDefaultViewHolder) customViewHolder).img_add.setVisibility(View.GONE);
                ((VitalsDefaultViewHolder) customViewHolder).btn_add.setVisibility(View.VISIBLE);
                ((VitalsDefaultViewHolder) customViewHolder).tv_no_record_uploaded_msg.setVisibility(View.GONE);

            }
        }
    }

    private void initializeGlucoseDataFilledView(RecyclerView.ViewHolder customViewHolder) {


        ((VitalsViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_blood_glucose));
        ((VitalsViewHolder) customViewHolder).temp_view.setVisibility(View.GONE);
        ((VitalsViewHolder) customViewHolder).tv_glucose_type.setVisibility(View.VISIBLE);

        if (list_bloodglucose.size() > 0) {
            ((VitalsViewHolder) customViewHolder).tv_val.setText(list_bloodglucose.get(0).getValue());
            ((VitalsViewHolder) customViewHolder).tv_unit.setText(list_bloodglucose.get(0).getUnit());
            ((VitalsViewHolder) customViewHolder).tv_username.setText(list_bloodglucose.get(0).getPatientName());


            if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode())) {
                ((VitalsViewHolder) customViewHolder).tv_glucose_type.setText("(" + mContext.getResources().getString(R.string.fasting_camelcase) + ")");
            } else if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode())) {
                ((VitalsViewHolder) customViewHolder).tv_glucose_type.setText("(" + mContext.getResources().getString(R.string.after_meal_camelcase) + ")");
            } else if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode())) {
                ((VitalsViewHolder) customViewHolder).tv_glucose_type.setText("(" + mContext.getResources().getString(R.string.random_camelcase) + ")");
            }


            if (list_bloodglucose.get(0).getRange_status().equals(mContext.getResources().getString(R.string.conditional_check_Normal))) {
                ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
            } else {
                if (BuildConfig.isVitalOutOfRange)
                    ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.red_radical));
                else
                    ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
            }

            if (!list_bloodglucose.get(0).getLabName().equals("") && list_bloodglucose.get(0).getLabName() != null && !list_bloodglucose.get(0).getLabName().equals(mContext.getResources().getString(R.string.txt_null))) {
                ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.VISIBLE);
                ((VitalsViewHolder) customViewHolder).tv_lab_name.setText(list_bloodglucose.get(0).getLabName());
            } else {
                ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.GONE);
            }

            String time0 = "";
            if(DateFormat.is24HourFormat(mContext))
                time0 = DateTimeUtil.calculateDateLocle(list_bloodglucose.get(0).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destination24HourDateTimeWithoutSecond);
            else
                time0 = DateTimeUtil.calculateDateLocle(list_bloodglucose.get(0).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithTimeOld);

            ((VitalsViewHolder) customViewHolder).tv_time.setText(time0 == null ? "" : time0);
        }
    }

    private void initializeBPDataFilledView(RecyclerView.ViewHolder customViewHolder) {
        ((VitalsViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_blood_pressure));
        ((VitalsViewHolder) customViewHolder).temp_view.setVisibility(View.GONE);
        ((VitalsViewHolder) customViewHolder).tv_glucose_type.setVisibility(View.GONE);
        ((VitalsViewHolder) customViewHolder).tv_val.setText(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getValue() + "/" + list.get(VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode()).getValue());
        ((VitalsViewHolder) customViewHolder).tv_unit.setText(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getUnit());
        ((VitalsViewHolder) customViewHolder).tv_username.setText(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getPatientName());

        if (list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getRange_status().equals(mContext.getResources().getString(R.string.conditional_check_Normal)) && list.get(VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode()).getRange_status().equals(mContext.getResources().getString(R.string.conditional_check_Normal))) {
            ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        } else {
            if (BuildConfig.isVitalOutOfRange)
                ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.red_radical));
            else
                ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        }

        if (!list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getLabName().equals("") && list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getLabName() != null && !list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getLabName().equals(mContext.getResources().getString(R.string.txt_null))) {
            ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.VISIBLE);
            ((VitalsViewHolder) customViewHolder).tv_lab_name.setText(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getLabName());
        } else {
            ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.GONE);
        }

        String time2 = "";
        if(DateFormat.is24HourFormat(mContext))
            time2=DateTimeUtil.calculateDateLocle(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destination24HourDateTimeWithoutSecond);
        else
            time2=DateTimeUtil.calculateDateLocle(list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithTimeOld);
        ((VitalsViewHolder) customViewHolder).tv_time.setText(time2 == null ? "" : time2);

    }

    private void initializeBMIDataFilledView(RecyclerView.ViewHolder customViewHolder) {
        ((VitalsViewHolder) customViewHolder).tv_header_title.setText(mContext.getResources().getString(R.string.vitals_bmi));
        ((VitalsViewHolder) customViewHolder).temp_view.setVisibility(View.VISIBLE);
        ((VitalsViewHolder) customViewHolder).tv_glucose_type.setVisibility(View.GONE);

        ((VitalsViewHolder) customViewHolder).tv_val.setText(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getValue());

        String unit = list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getUnit();
        if(unit.equalsIgnoreCase(TextConstants.BMI_KGM))
            unit = TextConstants.BMI_KGM2;
        if (unit.contains(TextConstants.BMI_SEPARATOR)){
            String[] parts = unit.split("\\^");
            String part0value=parts[0];
            String part1value=parts.length>=2?parts[1]:"";
            ((VitalsViewHolder) customViewHolder).tv_unit.setText(Html.fromHtml(UnitsEnum.getValueFrom(part0value) + "<sup><small>"
                    + part1value+ "</small></sup>"));
        } else
        {
             unit= UnitsEnum.getValueFrom(unit);
            ((VitalsViewHolder) customViewHolder).tv_unit.setText(unit);
        }
        ((VitalsViewHolder) customViewHolder).tv_unit.setVisibility(View.VISIBLE);
        ((VitalsViewHolder) customViewHolder).tv_username.setText(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getPatientName());

        if (list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getRange_status().equals(mContext.getResources().getString(R.string.conditional_check_Normal))) {
            ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        } else {
            if (BuildConfig.isVitalOutOfRange)
                ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.red_radical));
            else
                ((VitalsViewHolder) customViewHolder).tv_val.setTextColor(mContext.getResources().getColor(R.color.dusky_blue));
        }

        if (!list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getLabName().equals("") && list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getLabName() != null && !list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getLabName().equals(mContext.getResources().getString(R.string.txt_null))) {
            ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.VISIBLE);
            ((VitalsViewHolder) customViewHolder).tv_lab_name.setText(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getLabName());
        } else {
            ((VitalsViewHolder) customViewHolder).ll_labname_container.setVisibility(View.GONE);
        }
        String time1="";
        if(!DateFormat.is24HourFormat(mContext))
           time1 = DateTimeUtil.calculateDateLocle(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithTimeOld);
        else
            time1 = DateTimeUtil.calculateDateLocle(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destination24HourDateTimeWithoutSecond);

        ((VitalsViewHolder) customViewHolder).tv_time.setText(time1 == null ? "" : time1);
    }

    @Override
    public int getItemCount() {
        int count = 3;

/*
        if (!list.containsKey(VitalLionCodeEnum.BMI_CODE.getCode())) {
            count--;
        }
        if (!list.containsKey(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode())) {
            count--;
        }
        if (!list.containsKey(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode()) && !list.containsKey(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode()) && !list.containsKey(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode())) {
            count--;
        }
*/
        if (BuildConfig.isPatientApp) {
            try {
                vitalsConfigMO = VitalsConfigDBAdapter.getInstance(mContext).fetchVitalsConfigMO();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(vitalsConfigMO==null)
                return 0;

            if (!vitalsConfigMO.isToShowAddBMIButton()) {
                count--;
            }
            if (!vitalsConfigMO.isToShowAddBPButton()) {
                count--;
            }
            if (!vitalsConfigMO.isToShowAddGlucoseButton()) {
                count--;
            }
        }
        return count;
    }


    //return true if to add BP cell
    private boolean isToAddBP() {

        if (BuildConfig.isPhysicianApp)
            return true;

        else {
            if (vitalsConfigMO.isToShowAddBPButton())
                return true;
            else
                return false;
        }
    }

    //return postition to add BP
    private int getBPVitalPostion() {
        if (isToAddBP()) {
            return getBMIVitalPosition() + 1;
        } else
            return getBMIVitalPosition() + 0;
    }


    public class VitalsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_header_title, tv_val, tv_unit, tv_username, tv_time, tv_lab_name, tv_glucose_type;
        IconTextView img_add;
        LinearLayout user_details_lyt, ll_date_container, ll_labname_container;
        FrameLayout temp_view;
        RelativeLayout rel_lyt_vital, rl_parent_header;

        public VitalsViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_header_title = (CustomFontTextView) view.findViewById(R.id.tv_header_title);
            tv_val = (CustomFontTextView) view.findViewById(R.id.tv_val);
            tv_unit = (CustomFontTextView) view.findViewById(R.id.tv_unit);
            tv_username = (CustomFontTextView) view.findViewById(R.id.tv_username);
            tv_time = (CustomFontTextView) view.findViewById(R.id.tv_time);
            tv_lab_name = (CustomFontTextView) view.findViewById(R.id.tv_lab_name);
            tv_glucose_type = (CustomFontTextView) view.findViewById(R.id.tv_glucose_type);

            img_add = (IconTextView) view.findViewById(R.id.img_add);
            img_add.setOnClickListener(this);

            rel_lyt_vital = (RelativeLayout) view.findViewById(R.id.rel_lyt_vital);
            rel_lyt_vital.setOnClickListener(this);

            user_details_lyt = (LinearLayout) view.findViewById(R.id.user_details_lyt);
            ll_date_container = (LinearLayout) view.findViewById(R.id.ll_date_container);
            ll_labname_container = (LinearLayout) view.findViewById(R.id.ll_labname_container);

            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);
            rl_parent_header = (RelativeLayout) view.findViewById(R.id.rl_parentHeader);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {

                case R.id.img_add:
                    initializeClickListener(adapter_position);
                    break;
                case R.id.rel_lyt_vital:
                    String paramId = null, patientId = null,
                            vitalText = null, subtitle = null, secoundryParamId = null;

                    if (BuildConfig.isPatientApp) {
                        patientId = String.valueOf(SettingManager.getInstance().getUserMO().getPatientId());
                    } else if (BuildConfig.isPhysicianApp) {
                        patientId = ((Activity) context).getIntent().getStringExtra("patientId");
                    }

                    if (list.containsKey(VitalLionCodeEnum.BMI_CODE.getCode()) && adapter_position == getBMIVitalPosition()) {
                        paramId = list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getParamId();
                        vitalText = mContext.getResources().getString(R.string.bmi);
                        subtitle = mContext.getResources().getString(R.string.bmi);
                        secoundryParamId = null;
                    } else if (list.containsKey(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()) && adapter_position == getBPVitalPostion()) {
                        paramId = list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getParamId();
                        vitalText = TextConstants.BLOOD_PRESSURE;
                        subtitle = TextConstants.SYSTOLIC;
                        secoundryParamId = list.get(VitalLionCodeEnum.BP_DIASTOLIC_CODE.getCode()).getParamId();
                    } else if (list_bloodglucose != null && list_bloodglucose.size() > 0 && adapter_position == getGlucoseVitalPostion()) {
                        paramId = list_bloodglucose.get(0).getParamId();
                        vitalText = TextConstants.GLUCOSE;

                        if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode())) {
                            subtitle = mContext.getResources().getString(R.string.fasting);
                        } else if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode())) {
                            subtitle = mContext.getResources().getString(R.string.after_meal);
                        } else if (list_bloodglucose.get(0).getParamId().equals(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode())) {
                            subtitle = mContext.getResources().getString(R.string.random);
                        }
                        secoundryParamId = null;
                    }
                    context.startActivity(ObservationHistoryActivity.newIntent(context,
                            paramId, patientId,
                            vitalText, subtitle, secoundryParamId, true));
                    break;

                default:
                    break;
            }
        }
    }

    public class VitalsDefaultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        CustomFontTextView tv_header_title;
        IconTextView img_add, img_type;
        RelativeLayout btn_add, rl_parent_header_default;
        FrameLayout temp_view;
        IconTextView img_inner_add;
        CustomFontTextView tv_lets_do_it, tv_no_record_uploaded_msg;

        public VitalsDefaultViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_header_title = (CustomFontTextView) view.findViewById(R.id.tv_header_title);

            img_add = (IconTextView) view.findViewById(R.id.img_add);
            img_add.setOnClickListener(this);

            img_type = (IconTextView) view.findViewById(R.id.img_type);

            btn_add = (RelativeLayout) view.findViewById(R.id.btn_add);
            btn_add.setOnClickListener(this);

            temp_view = (FrameLayout) view.findViewById(R.id.temp_view);
            tv_lets_do_it = (CustomFontTextView) view.findViewById(R.id.tv_lets_do_it);
            tv_no_record_uploaded_msg = (CustomFontTextView) view.findViewById(R.id.tv_no_record_uploaded_msg);
            img_inner_add = (IconTextView) view.findViewById(R.id.img_inner_add);
            rl_parent_header_default = (RelativeLayout) view.findViewById(R.id.rl_parent_header_default);
        }


        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();

            switch (v.getId()) {
                case R.id.btn_add:
                case R.id.img_add:
                    initializeClickListener(adapter_position);

            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (list.size() > 0) {
            if (list.containsKey(VitalLionCodeEnum.BMI_CODE.getCode()) && position == getBMIVitalPosition()) {
                if (!(list.get(VitalLionCodeEnum.BMI_CODE.getCode()).getValue().equals(""))) {
                    return VIEW_TYPE_ITEM;
                } else {
                    return VIEW_TYPE_HEADER;
                }
            } else if (list.containsKey(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()) && position == getBPVitalPostion()) {
                if (!list.get(VitalLionCodeEnum.BP_SYSTOLIC_CODE.getCode()).getValue().equals("")) {
                    return VIEW_TYPE_ITEM;
                } else {
                    return VIEW_TYPE_HEADER;
                }
            } else if (position == getGlucoseVitalPostion() && isGlucoseValueAvailable()) {
                return VIEW_TYPE_ITEM;
            } else {
                return VIEW_TYPE_HEADER;
            }
        }
        return VIEW_TYPE_HEADER;
    }

    boolean isGlucoseValueAvailable() {
        if (((list.containsKey(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode())
                && !list.get(VitalLionCodeEnum.GLUCOSE_AFTERMEAL_CODE.getCode()).getValue().equals(""))
                || (list.containsKey(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode())
                && !list.get(VitalLionCodeEnum.GLUCOSE_FASTING_CODE.getCode()).getValue().equals(""))
                || (list.containsKey(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode())
                && !list.get(VitalLionCodeEnum.GLUCOSE_RANDOM_CODE.getCode()).getValue().equals(""))))
            return true;
        else
            return false;
    }

    public void openObservationActivity(ObservationList observationList) {
        ((Activity) mContext).startActivityForResult(
                new Intent(mContext, EnterObservationActivity.class).putExtra(VariableConstants.OBSERVATION_SELECTED,
                        observationList.getObservationResource()).putExtra(VariableConstants.OBSERVATION_TEXT, observationList.getObservationText()),
                OBSERVATION_SELECT);
    }


    //return true if to add BMI cell
    private boolean isToAddBMI() {
        if (BuildConfig.isPhysicianApp)
            return true;

        else {
            if (vitalsConfigMO.isToShowAddBMIButton())
                return true;
            else
                return false;
        }
    }

    //return postion where to add BMI
    private int getBMIVitalPosition() {
        if (isToAddBMI())
            return 0;
        else return -1;
    }

    //return true if to add glucose cell
    private boolean isToAddGlucose() {
        if (BuildConfig.isPhysicianApp)
            return true;

        else {
            if (vitalsConfigMO.isToShowAddGlucoseButton())
                return true;
            else return false;
        }
    }

    //return position where to add Glucose
    private int getGlucoseVitalPostion() {
        if (isToAddGlucose()) {
            return getBPVitalPostion() + 1;
        } else
            return getBPVitalPostion() + 0;
    }

    private void initializeClickListener(int adapter_position) {
        if (BuildConfig.isPatientApp && isToAddBMI() && adapter_position == getBMIVitalPosition()) {
            openObservationActivity(new ObservationList(R.string.fa_observation_bmi, VariableConstants.BMI, R.color.fill3));
        } else if (BuildConfig.isPatientApp && isToAddBP() && adapter_position == getBPVitalPostion()) {
            openObservationActivity(new ObservationList(R.string.fa_observation_bp, VariableConstants.Blood_Pressure, R.color.fill1));
        } else if (list_bloodglucose != null && isToAddGlucose() && adapter_position == getGlucoseVitalPostion()) {
            openObservationActivity(new ObservationList(R.string.fa_observation_glucose, VariableConstants.Glucose, R.color.fill2));
        }
    }


}
