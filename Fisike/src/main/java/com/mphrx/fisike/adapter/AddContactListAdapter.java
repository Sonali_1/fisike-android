package com.mphrx.fisike.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.lazyloading.ImageLoader;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.activity.ViewPatientProfile;

import java.util.ArrayList;

public class AddContactListAdapter extends BaseAdapter {

    private ArrayList<ChatContactMO> contactList;
    private Context context;
    private ImageLoader imgLoader;

    public AddContactListAdapter(Context context, ArrayList<ChatContactMO> contactList) {
        this.context = context;
        this.contactList = contactList;
        imgLoader = new ImageLoader(context);
    }

    public void setContactList(ArrayList<ChatContactMO> contactList) {
        this.contactList = contactList;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public ChatContactMO getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatContactMO person = contactList.get(position);
        int drawable = R.drawable.physician_generic;
        View view = convertView;
        ViewHolder holder;
        view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.new_contact_list, parent, false);
        CustomFontTextView txtUserName = (CustomFontTextView) view.findViewById(R.id.txtName);
        CustomFontTextView txtUserType = (CustomFontTextView) view.findViewById(R.id.type);
        ImageView imgPerson = (ImageView) view.findViewById(R.id.iv_contact);
        ImageView ivContactStatus = (ImageView) view.findViewById(R.id.iv_contact_status);
        holder = new ViewHolder(txtUserName, txtUserType, imgPerson, ivContactStatus);
        view.setTag(holder);
        holder.txtUserName.setText(person.getRealName());

        if (person.getSpeciality() != null && !person.getSpeciality().equals(context.getResources().getString(R.string.txt_null))) {
            holder.txtUserType.setText(person.getSpeciality());
        } else {
            holder.txtUserType.setText(person.getUserType().getName());
        }
        if ((person.getUserType().equals(context.getResources().getString(R.string.conditional_check_Patient)))) {
            drawable = R.drawable.patient_generic;
            holder.imgPerson.setImageResource(drawable);
        }
        imgLoader.DisplayImage(person.getId(), holder.imgPerson, drawable);

        byte[] profilePic = person.getProfilePic();
        if (null != profilePic && profilePic.length > 0) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
            if (bitmap != null) {
                holder.imgPerson.setImageBitmap(bitmap);
            }
        }

        holder.imgPerson.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //  context.startActivity(new Intent(context, ContactDetailActivity.class).putExtra(TextConstants.CHAT_DETAIL, person).putExtra(TextConstants.IS_RECENT_CHAT, false));
                String senderSecondaryId = person.getId() + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
                if (person.getUserType().getName().equalsIgnoreCase(context.getResources().getString(R.string.conditional_check_patient))) {

                    context.startActivity(new Intent(context, ViewPatientProfile.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                            .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, person));
                } else {
                    context.startActivity(new Intent(context, com.mphrx.fisike_physician.activity.ProfileActivity.class).putExtra(VariableConstants.SENDER_ID, senderSecondaryId)
                            .putExtra(TextConstants.IS_RECENT_CHAT, true).putExtra(VariableConstants.CONVERSATION_ITEM, person));
                }
            }
        });

        if (person.getUserType() == null || person.getUserType().getName().equalsIgnoreCase(TextConstants.PATIENT)) {
            holder.ivContactStatus.setVisibility(View.INVISIBLE);
        } else {
            String presenceType = person.getPresenceType();
            holder.ivContactStatus.setVisibility(View.VISIBLE);
            if (presenceType == null || presenceType.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_available);
            } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_busy);
            } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_away);
            } else {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_available);
            }
        }

        return view;
    }

    public void clearSDCard() {
        imgLoader.clearSdCard();
    }

    private class ViewHolder {
        private CustomFontTextView txtUserName, txtUserType;
        private ImageView imgPerson;
        private ImageView ivContactStatus;

        public ViewHolder(CustomFontTextView txtUserName, CustomFontTextView txtUserType, ImageView imgPerson, ImageView ivContactStatus) {
            this.txtUserName = txtUserName;
            this.txtUserType = txtUserType;
            this.imgPerson = imgPerson;
            this.ivContactStatus = ivContactStatus;

        }
    }
}