package com.mphrx.fisike.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerAdapter extends BaseAdapter {
    private Context context;
    private List<NavDrawerItem> navDrawerItem;
    int mSelectedPostion = -1;

    public NavigationDrawerAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItem) {
        this.context = context;
        this.navDrawerItem = navDrawerItem;
    }

    @Override
    public int getCount() {
        return navDrawerItem.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItem.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflator.inflate(R.layout.navigation_drawer_item, null);
            holder = new ViewHolder();
            holder.ivNavigationDrawerItemIcon = (IconTextView) view.findViewById(R.id.iv_navigation_drawer_item_icon);
            holder.tvNavigationDrawerItemTitle = (CustomFontTextView) view.findViewById(R.id.tv_navigation_drawer_item_title);
            holder.tvNavigationDrawerItemCounter = (CustomFontTextView) view.findViewById(R.id.tv_item_count);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.ivNavigationDrawerItemIcon.setText(navDrawerItem.get(position).getResId());
        holder.tvNavigationDrawerItemTitle.setText(navDrawerItem.get(position).getTitle());
        holder.tvNavigationDrawerItemTitle.setContentDescription(navDrawerItem.get(position).getTitle());

        // String count = navDrawerItem.get(position).getCount() != null ? navDrawerItem.get(position).getCount() : "";
        if (navDrawerItem.get(position).getTitle().equalsIgnoreCase(TextConstants.NOTIFICATIONS)) {
            // holder.tvNavigationDrawerItemCounter.setVisibility(View.VISIBLE);
            // holder.tvNavigationDrawerItemCounter.setText(count);

            SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            boolean isRead = preferences.getBoolean(VariableConstants.ALARM_READ, true);
            int unReadCount = preferences.getInt(VariableConstants.ALARM_COUNT_UNREAD, 0);

            if (!isRead && unReadCount > 0) {
                holder.tvNavigationDrawerItemCounter.setVisibility(View.VISIBLE);
                holder.tvNavigationDrawerItemCounter.setText(unReadCount + "");
            } else {
                holder.tvNavigationDrawerItemCounter.setVisibility(View.INVISIBLE);
            }

        }
        if(getmSelectedPostion() == position){
            view.setSelected(true);
        }else{
            view.setSelected(false);
        }

        return view;
    }

    public void setSelectedPosition(int position) {
        mSelectedPostion = position;
    }

    public int getmSelectedPostion() {
        return mSelectedPostion;
    }

    private class ViewHolder {
        public IconTextView ivNavigationDrawerItemIcon;
        public CustomFontTextView tvNavigationDrawerItemTitle;
        public CustomFontTextView tvNavigationDrawerItemCounter;
    }

}
