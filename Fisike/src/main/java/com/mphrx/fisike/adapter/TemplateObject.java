package com.mphrx.fisike.adapter;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.gson.request.CommonApiResponse;

import java.io.Serializable;

public class TemplateObject extends CommonApiResponse implements Serializable {

    private static final long serialVersionUID = 6909549540080775729L;

    @SerializedName("templateId")
    private String templateId;
    
    @SerializedName("templateName")
    private String templateName;
    
    @SerializedName("estimatedTime")
    private String estimatedTime;
    
    @SerializedName("description")
    private String description;
    
    @SerializedName("category")
    private String category;
    
    @SerializedName("numberOfQuestions")
    private String numberOfQuestions;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(String numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }
}
