package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.TextPattern;
import com.mphrx.fisike_physician.views.CustomProfileVIewLayout;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class GroupMembersAdapter extends RecyclerView.Adapter<GroupMembersAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ChatContactMO> arrayChatContactMo;
    private GroupTextChatMO groupTextChatMO;
    private String id;
    public clickListener listener;
    private ChatConversationMO chatConversationMo;

    public GroupMembersAdapter(Context context, ArrayList<ChatContactMO> arrayOptionImageObj, String id) {
        this.context = context;
        this.arrayChatContactMo = arrayOptionImageObj;
        try {
            this.id = URLDecoder.decode(id, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            this.id = id;
        }
    }


    private void setPresence(String presenceType, ImageView imgMemberStatus) {
        if (presenceType == null || presenceType.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
            imgMemberStatus.setImageResource(R.drawable.ic_status_available);
        } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
            imgMemberStatus.setImageResource(R.drawable.ic_status_busy);
        } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
            imgMemberStatus.setImageResource(R.drawable.ic_status_away);
        } else {
            imgMemberStatus.setImageResource(R.drawable.ic_status_available);
        }
    }

    public boolean isElementYou(int position) {
        if (null != id && arrayChatContactMo.get(position).getEmail().equalsIgnoreCase(id)) {
            return true;
        }
        return false;

    }

    private boolean isAdmin(ChatContactMO chatContactMO) {
        if (null == groupTextChatMO) {
            return false;
        }
        ArrayList<String> admin = groupTextChatMO.getAdmin();
        if (null == admin) {
            return false;
        }
        for (int i = 0; i < admin.size(); i++) {
            if (admin.get(i).equals(chatContactMO.getPersistenceKey() + "")) {
                return true;
            }
        }
        return false;
    }

    private boolean isAdminFromAddedColleague(ChatContactMO chatContactMO) {
        if (chatConversationMo == null) return false;

        String userId = String.valueOf(chatContactMO.getId());
        userId = TextPattern.getUserId(userId);
        String extractedId = Utils.getAdminId(chatConversationMo.getJId());
        if (userId.equals(extractedId)) {
            return true;
        }
        return false;

    }


    public ChatContactMO getItem(int position) {
        return arrayChatContactMo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public void setGroupTextChatMo(GroupTextChatMO groupTextChatMO) {
        this.groupTextChatMO = groupTextChatMO;

    }

    public void setChatConversationMo(ChatConversationMO chatConversationMo) {
        this.chatConversationMo = chatConversationMo;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CustomFontTextView txtName;
        private CustomFontTextView txtDescription;
        private ImageView imgRemove;
        private CustomProfileVIewLayout imgGroupMember;
        private CustomFontTextView txtRole;
        private ImageView imgMemberStatus;

        public ViewHolder(View view) {
            super(view);
            txtName = (CustomFontTextView) view.findViewById(R.id.name);
            txtDescription = (CustomFontTextView) view.findViewById(R.id.description);
            imgRemove = (ImageView) view.findViewById(R.id.img_remove);
            txtRole = (CustomFontTextView) view.findViewById(R.id.txtRole);
            imgGroupMember = (CustomProfileVIewLayout) view.findViewById(R.id.img_group_member);
            imgMemberStatus = (ImageView) view.findViewById(R.id.imgStatus);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.itemClicked(v, getAdapterPosition());
            }
        }

    }

    public void setChatContactList(ArrayList<ChatContactMO> chatConatList) {
        arrayChatContactMo = chatConatList;
    }


    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(context).inflate(R.layout.group_list_cell, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        viewHolder.imgRemove.setVisibility(View.GONE);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatContactMO chatContactMO = arrayChatContactMo.get(position);

        if (isAdmin(chatContactMO) || isAdminFromAddedColleague(chatContactMO)) {
            holder.txtRole.setVisibility(View.VISIBLE);
        }

        holder.imgGroupMember.setViewProfileImage(chatContactMO.getId());


        String presenceType = chatContactMO.getPresenceType();
        setPresence(presenceType, holder.imgMemberStatus);

        String realName = chatContactMO.getRealName();
        String id = chatContactMO.getId();
        if (this.id.equals(id)) {
            holder.txtName.setText(VariableConstants.YOU);
            UserMO userMO = SettingManager.getInstance().getUserMO();
            presenceType = userMO.getStatus();
            setPresence(presenceType, holder.imgMemberStatus);
        } else if (null == realName || realName.equals("")) {
            holder.txtName.setText(id);
        } else {
            holder.txtName.setText(realName);
        }

        if (chatContactMO.getSpeciality() != null && !chatContactMO.getSpeciality().equals(context.getResources().getString(R.string.txt_null))) {
            holder.txtDescription.setText(chatContactMO.getSpeciality());
        } else {
            String userType = chatContactMO.getUserType().getName();
            if (null != userType && !"".equals(userType)) {
                holder.txtDescription.setText(chatContactMO.getUserType().getName());
            }
        }
    }


    public interface clickListener {
        void itemClicked(View view, int position);
    }

    @Override
    public int getItemCount() {
        return arrayChatContactMo.size();
    }
}