package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Constraints;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Text;
import com.mphrx.fisike.gson.request.MedicationSearchRequest;
import com.mphrx.fisike.gson.response.MedicationSearchResponse;
import com.mphrx.fisike.gson.response.MedicineList;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

import appointment.utils.CommonControls;

/**
 * Created by laxmansingh on 10/19/2016.
 */

public class SearchMedicationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context mContext;
    private List<MedicineList> list;


    private RelativeLayout mNo_data_lyt;
    private ImageView mNo_data_image;
    private CustomFontTextView mTv_no_data_msg;
    private CustomFontButton mBtnAddMedicine;
    private CardView card_rv;
    private EditText mEt_search;
    private IconTextView mBtnCross;
    private ProgressBar mSearch_progress_bar;

    private final int VIEW_TYPE_ITEM = 0;

    public SearchMedicationAdapter(Context context, List<MedicineList> list, EditText mEt_search, RelativeLayout mNo_data_lyt, ImageView mNo_data_image, CustomFontTextView mTv_no_data_msg, CustomFontButton mBtnAddMedicine, CardView card_rv, IconTextView mBtnCross, ProgressBar mSearch_progress_bar) {
        this.mContext = context;
        this.list = list;
        this.mEt_search = mEt_search;
        this.mNo_data_lyt = mNo_data_lyt;
        this.mNo_data_image = mNo_data_image;
        this.mTv_no_data_msg = mTv_no_data_msg;
        this.mBtnAddMedicine = mBtnAddMedicine;
        this.card_rv = card_rv;
        this.mBtnCross = mBtnCross;
        this.mSearch_progress_bar = mSearch_progress_bar;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.searched_list_item, null);
            SearchMedicationAdapter.SearchMedicationViewHolder viewHolder = new SearchMedicationAdapter.SearchMedicationViewHolder(view, mContext);
            return viewHolder;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder customViewHolder, int i) {
        if (customViewHolder instanceof SearchMedicationAdapter.SearchMedicationViewHolder) {
            SearchMedicationAdapter.SearchMedicationViewHolder searchMedicationViewHolder = (SearchMedicationAdapter.SearchMedicationViewHolder) customViewHolder;

            if (i == list.size() - 1) {
                searchMedicationViewHolder.line_view.setVisibility(View.GONE);
            } else {
                searchMedicationViewHolder.line_view.setVisibility(View.VISIBLE);
            }

            searchMedicationViewHolder.textViewItem.setText(list.get(i).getCode().getText());
        }
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }


    public class SearchMedicationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView textViewItem;
        View line_view;


        public SearchMedicationViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            textViewItem = (TextView) view.findViewById(R.id.textViewItem);
            line_view = (View) view.findViewById(R.id.line_view);
        }

        @Override
        public void onClick(View v) {
            int adapter_position = getAdapterPosition();
            Intent intent = new Intent();
            intent.putExtra("medicine_name", list.get(adapter_position).getCode().getText().toString().trim());
            intent.putExtra("medicine_id", list.get(adapter_position).getId());
            CommonControls.closeKeyBoard(mContext);
            ((Activity) mContext).setResult(TextConstants.MEDICINE_SEARCH_INTENT, intent);
            ((Activity) mContext).finish();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraints) {
                final FilterResults filterResults = new FilterResults();

                if (constraints.toString().trim().length() == 0) {
                    Handler mainHandler = new Handler(mContext.getMainLooper());
                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            mBtnCross.setVisibility(View.GONE);
                            mSearch_progress_bar.setVisibility(View.GONE);
                        }
                    };
                    mainHandler.post(myRunnable);
                    filterResults.count = 0;
                    return filterResults;
                } else if (constraints.toString().trim().length() < 3 && constraints.toString().trim().length() > 0) {
                    Handler mainHandler = new Handler(mContext.getMainLooper());
                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            mBtnCross.setVisibility(View.VISIBLE);
                            mSearch_progress_bar.setVisibility(View.GONE);
                        }
                    };
                    mainHandler.post(myRunnable);
                    filterResults.count = 0;
                    return filterResults;
                } else if (constraints.toString().trim().length() > 2) {
                    if (!Utils.showDialogForNoNetwork(mContext, false)) {
                        Handler mainHandler = new Handler(mContext.getMainLooper());
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                mBtnCross.setVisibility(View.VISIBLE);
                                mSearch_progress_bar.setVisibility(View.GONE);
                                CommonControls.closeKeyBoard(mContext);
                            }
                        };
                        mainHandler.post(myRunnable);
                        filterResults.count = 0;
                        return filterResults;
                    } else {
                        Handler progressHandler = new Handler(mContext.getMainLooper());
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                mBtnCross.setVisibility(View.GONE);
                                mSearch_progress_bar.setVisibility(View.VISIBLE);
                            }
                        };
                        progressHandler.post(myRunnable);

                        MedicationSearchResponse response = null;
                        APIManager apiManager = APIManager.getInstance();
                        String medicineSearchURL = apiManager.getMinervaPlatformURL(mContext, TextConstants.MEDICATION_SEARCH_URL_STRING);
                        MedicationSearchRequest searchRequestJson = new MedicationSearchRequest();
                        Constraints constraint = new Constraints();
                        constraint.setCode(constraints.toString().trim());
                        constraint.setSkip(0);
                        constraint.setCount(10);
                        searchRequestJson.setConstraints(constraint);

                        String json = new Gson().toJson(searchRequestJson);
                        Gson gson = new Gson();
                        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
                        JsonObject jsonObj = element.getAsJsonObject();
                                /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
                        String authToken = SharedPref.getAccessToken();
                        String executeHttpPost = apiManager.executeHttpPost(medicineSearchURL, jsonObj, authToken, mContext);
                        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(executeHttpPost, MedicationSearchResponse.class);
                        if (jsonToObjectMapper instanceof MedicationSearchResponse) {
                            response = ((MedicationSearchResponse) jsonToObjectMapper);
                        }

                        list = (ArrayList<MedicineList>) response.getList();
                        List<MedicineList> medicineResult = list;
                        // Assign the data to the FilterResults
                        filterResults.values = list;
                        filterResults.count = medicineResult.size();
                        return filterResults;
                    }
                } else {
                    filterResults.count = 0;
                    return filterResults;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count == -1) {
                    list.clear();
                    setUIAndData();
                    notifyDataSetChanged();
                } else if (results != null && results.count > 0 && mEt_search.getText().toString().trim().length() > 2) {
                    setUIAndData();
                    ArrayList<MedicineList> data = (ArrayList<MedicineList>) results.values;
                    notifyDataSetChanged();
                    MyApplication.getInstance().trackResultCondition(results.count, constraint.toString());
                } else if (results.count == 0 && constraint != null) {
                    list.clear();
                    setUIAndData();
                    notifyDataSetChanged();
                    MyApplication.getInstance().trackNoResultCondition(0, constraint.toString());
                } else if (mEt_search.getText().toString().trim().length() < 3) {
                    list.clear();
                    setUIAndData();
                    notifyDataSetChanged();
                }
            }
        };
        return filter;
    }


    public void setUIAndData() {
        Handler mainHandler = new Handler(mContext.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                mSearch_progress_bar.setVisibility(View.GONE);
                if (mEt_search.getText().toString().trim().length() == 0) {
                    mBtnCross.setVisibility(View.GONE);
                } else {
                    mBtnCross.setVisibility(View.VISIBLE);
                }
                if (list.size() > 0) {
                    card_rv.setVisibility(View.VISIBLE);
                    mNo_data_lyt.setVisibility(View.GONE);
                    notifyDataSetChanged();
                } else {
                    if (mEt_search.getText().toString().trim().length() > 2) {
                        mNo_data_image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_no_medication));
                        mTv_no_data_msg.setText(mContext.getResources().getString(R.string.no_result_autocomplete_medicine));
                        mTv_no_data_msg.setTextColor(mContext.getResources().getColor(R.color.light_green));
                        mBtnAddMedicine.setVisibility(View.VISIBLE);
                    } else {
                        mNo_data_image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seach_medicatio));
                        mTv_no_data_msg.setText(mContext.getResources().getString(R.string.search_your_medicine));
                        mTv_no_data_msg.setTextColor(mContext.getResources().getColor(R.color.orange_2));
                        mBtnAddMedicine.setVisibility(View.GONE);
                    }
                    card_rv.setVisibility(View.GONE);
                    mNo_data_lyt.setVisibility(View.VISIBLE);
                }
            }
        };
        mainHandler.post(myRunnable);
    }
}
