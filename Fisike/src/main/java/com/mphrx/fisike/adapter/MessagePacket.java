package com.mphrx.fisike.adapter;

import java.util.regex.Pattern;

import com.mphrx.fisike.constant.VariableConstants;

/**
 * Message is a Custom Object to encapsulate message information/fields
 */
public class MessagePacket {
    /**
     * The content of the message
     */
    private String message;

    /**
     * boolean to determine, who is sender of this message
     */
    private boolean isMine;

    /**
     * boolean to determine, whether the message is a status message or not. it reflects the changes/updates about the sender is writing, have entered text etc
     */
    private boolean isStatusMessage;

    private long time;

    private int messageReceived;

    private String msgId;

    /**
     * Constructor to make a Message object
     *
     * @param messageReceived
     */
    public MessagePacket(String message, boolean isMine, long time, int messageReceived, String msgId) {
        super();
        this.message = message;
        this.isMine = isMine;
        this.time = time;
        this.messageReceived = messageReceived;
        this.msgId = msgId;
        this.isStatusMessage = false;
    }

    public MessagePacket(String message, boolean isMine, long time, int messageReceived) {
        super();
        String tempMessage = message;
        if (tempMessage.contains(VariableConstants.DELIMITER)) {
            String[] split = tempMessage.split(Pattern.quote(VariableConstants.DELIMITER));
            this.msgId = split[0];
            this.message = split[1];
        } else {
            this.msgId = time + "";
            this.message = tempMessage;
        }
        this.isMine = isMine;
        this.time = time;
        this.messageReceived = messageReceived;
        this.isStatusMessage = false;
    }

    /**
     * Constructor to make a status Message object consider the parameters are swaped from default Message constructor, not a good approach but have to go with it.
     */
    public MessagePacket(boolean status, String message) {
        super();
        this.message = message;
        this.isMine = false;
        this.isStatusMessage = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean isMine) {
        this.isMine = isMine;
    }

    public boolean isStatusMessage() {
        return isStatusMessage;
    }

    public void setStatusMessage(boolean isStatusMessage) {
        this.isStatusMessage = isStatusMessage;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getMessageReceived() {
        return messageReceived;
    }

    public void setMessageReceived(int messageReceived) {
        this.messageReceived = messageReceived;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
