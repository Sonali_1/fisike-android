package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mphrx.fisike.R;

import java.util.List;


/**
 * Created by laxmansingh on 10/10/2016.
 */

public class PastConditionsStatusAdapter extends ArrayAdapter<String> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<String> list;
    LayoutInflater mInflater;
    private Typeface font_regular, font_semibold;

    public PastConditionsStatusAdapter(Context context, int textViewResourceId,
                                       List<String> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        font_semibold = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_semibold));
        font_regular = Typeface.createFromAsset(context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_regular));
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PastConditionsStatusAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            holder = new PastConditionsStatusAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (PastConditionsStatusAdapter.ViewHolder) convertView.getTag();
        }

        holder.txt_item.setTextSize(12);
        holder.txt_item.setTypeface(font_semibold);
        holder.txt_item.setTextColor(context.getResources().getColor(R.color.overflow_color));
        holder.txt_item.setText(list.get(position).toString().trim());
        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        final PastConditionsStatusAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_text_view, null);
            holder = new PastConditionsStatusAdapter.ViewHolder();
            holder.txt_item = (TextView) convertView
                    .findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (PastConditionsStatusAdapter.ViewHolder) convertView.getTag();
        }

        holder.txt_item.setTextSize(14);
        holder.txt_item.setTypeface(font_regular);
        holder.txt_item.setTextColor(context.getResources().getColor(R.color.dusky_blue));
        holder.txt_item.setText(list.get(position).toString().trim());
        return convertView;
    }

    public static class ViewHolder {
        TextView txt_item;
    }
}
