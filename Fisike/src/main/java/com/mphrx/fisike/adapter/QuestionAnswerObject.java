package com.mphrx.fisike.adapter;

import com.google.gson.annotations.SerializedName;

public class QuestionAnswerObject {

    @SerializedName("question")
    private String question;
    
    @SerializedName("response")
    private String answer;
    
    @SerializedName("questionType")
    private String questionType;
    
    @SerializedName("description")
    private String description;
    
    @SerializedName("questionId")
    private String questionId;
    
    @SerializedName("responseReceivedAt")
    private String responseReceivedAt;

    public QuestionAnswerObject(String question, String answer, String responseReceivedAt) {
        this.question = question;
        this.answer = answer;
        this.responseReceivedAt = responseReceivedAt;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getResponseReceivedAt() {
        return responseReceivedAt;
    }

    public void setResponseReceivedAt(String responseReceivedAt) {
        this.responseReceivedAt = responseReceivedAt;
    }
}