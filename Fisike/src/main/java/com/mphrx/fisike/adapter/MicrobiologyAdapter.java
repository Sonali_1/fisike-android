package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.record_screen.adapter.ObservationAdapter;
import com.mphrx.fisike.record_screen.model.Observation;
import com.mphrx.fisike.record_screen.response.MicrobiologyObservation;

import java.util.List;

/**
 * Created by xmb2nc on 11-04-2017.
 */

public class MicrobiologyAdapter extends RecyclerView.Adapter<MicrobiologyAdapter.MicrobiologyViewHolder> {
    private Context context;
    private List<MicrobiologyObservation> microbiologyObservationsList;
    private clickListener listener;

    public MicrobiologyAdapter(Context context, List<MicrobiologyObservation> microbiologyObservationsList){
        this.context = context;
        this.microbiologyObservationsList = microbiologyObservationsList;
    }

    @Override
    public MicrobiologyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_microbiology, parent, false);
        MicrobiologyAdapter.MicrobiologyViewHolder microbiologyViewHolder = new MicrobiologyAdapter.MicrobiologyViewHolder(row);
        return microbiologyViewHolder;
    }

    @Override
    public void onBindViewHolder(MicrobiologyViewHolder holder, int position) {
        MicrobiologyObservation microbiologyObservation = microbiologyObservationsList.get(position);
         renderMicrobiologyRow(holder, microbiologyObservation);

    }

    private void renderMicrobiologyRow(MicrobiologyViewHolder microbiologyViewHolder,
                                       MicrobiologyObservation microbiologyObservation){
        microbiologyViewHolder.tvMicroBiologyComponentName.setText(microbiologyObservation.getTestType());

    }

    @Override
    public int getItemCount() {
        return microbiologyObservationsList.size();
    }

    // view holder class
    public class MicrobiologyViewHolder extends ViewHolder {
        private CustomFontTextView tvMicroBiologyComponentName;
        private CustomFontButton btnOverFlow;
        public MicrobiologyViewHolder(View itemView) {
            super(itemView);
            tvMicroBiologyComponentName = (CustomFontTextView) itemView.findViewById(R.id.tv_component_name);
            btnOverFlow = (CustomFontButton) itemView.findViewById(R.id.btn_microbiology_component_overflow);
            btnOverFlow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null)
                        listener.itemClicked(view,getAdapterPosition(),false);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.itemClicked(view, getAdapterPosition(), true);
                    }

                }
            });
        }
    }

    public MicrobiologyObservation getObservationAtPostition(int position)
    {
        return microbiologyObservationsList.get(position);
    }

    public interface clickListener {
        public void itemClicked(View view, int position, boolean isHistoryScreenNeedsToBeLaunched);
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;

    }
}
