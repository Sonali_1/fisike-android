package com.mphrx.fisike.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MedicationActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Days;
import com.mphrx.fisike.utils.Utils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by Aastha on 28/01/2016.
 */
public class GridButtonAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Days> mDays;
    // this is used to display
    private String days[] = new String[7];

    public GridButtonAdapter(Context context, ArrayList<Days> days) {
        mContext = context;
        this.days = Utils.getShortWeekDays(Utils.getUserSelectedLanguage(mContext));
        mDays = days;
    }

    @Override
    public int getCount() {
        return mDays.size();
    }

    @Override
    public Button getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_days, null);
            viewHolder.button = (CustomFontTextView) convertView.findViewById(R.id.bt_days);
            viewHolder.buttonLayout = (LinearLayout) convertView.findViewById(R.id.button_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.button.setText(days[position]);
        viewHolder.button.setTextColor(Color.WHITE);
        setButtonState(mDays.get(position).isSelected(), viewHolder.button, viewHolder.buttonLayout);
        final View finalConvertView = convertView;
        if (BuildConfig.isPatientApp)
            viewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((GridView) parent).performItemClick(finalConvertView, position, 0);
                    ((MedicationActivity) mContext).setDaysType(mDays);
                }
            });

        return convertView;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setButtonState(Boolean isSelected, CustomFontTextView daysButton, LinearLayout buttonLayout) {
        if (!isSelected) {
            GradientDrawable bgShape = (GradientDrawable) daysButton.getBackground();
            bgShape.setColor(mContext.getResources().getColor(R.color.blue_grey));
        } else {
            GradientDrawable bgShape = (GradientDrawable) daysButton.getBackground();
            bgShape.setColor(mContext.getResources().getColor(R.color.action_bar_bg));
        }
    }

    public void setWeekDays(ArrayList<Days> days) {
        mDays.clear();
        mDays.addAll(days);
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        private CustomFontTextView button;
        private LinearLayout buttonLayout;
    }
}
