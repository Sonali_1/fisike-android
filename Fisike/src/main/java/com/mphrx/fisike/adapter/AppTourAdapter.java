package com.mphrx.fisike.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.mphrx.fisike.customview.CustomFontButton;

import java.util.List;

public class AppTourAdapter extends FragmentStatePagerAdapter{

	Fragment myfragment;
	List<Fragment> fragments;
	View view;
	CustomFontButton skip;
	public AppTourAdapter(FragmentManager fm,View view, List<Fragment> fragments) {
		super(fm);
		this.view=view;
		this.fragments = fragments;
		skip=( com.mphrx.fisike.customview.CustomFontButton )view;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		return this.fragments.get(arg0);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return fragments.size();
	}

}
