package com.mphrx.fisike.adapter;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.lazyloading.ImageLoader;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;

import java.util.ArrayList;
import java.util.List;

public class GroupInviteAdapter extends BaseAdapter {
    private static final String BASE_URL = "dev1.mphrx.com:443/fisike/userApi/getUserProfilePicture?";
    private ArrayList<ChatContactMO> memberList = null;
    private Context context;
    private List<ChatContactMO> selectedInvites = null;
    private ImageLoader imgLoader;
    private int alreadyAddedContactsSize = 0;
    private ActionBar actionSelectedInviteCount;

    public GroupInviteAdapter(Context context, List<ChatContactMO> inviteList, int alreadyAddedContacts, ActionBar actionSelectionCount) {
        this.context = context;
        this.memberList = (ArrayList<ChatContactMO>) inviteList;
        imgLoader = new ImageLoader(context);
        this.alreadyAddedContactsSize = alreadyAddedContacts;
        this.actionSelectedInviteCount = actionSelectionCount;
    }

    public ArrayList<ChatContactMO> getInviteList() {
        return memberList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.invite_list, null);
            holder = new ViewHolder();
            holder.tvMemberName = (CustomFontTextView) convertView.findViewById(R.id.tv_member_name);
            holder.chequeBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.ivProfilePic = (ImageView) convertView.findViewById(R.id.iv_contact);
            holder.tvMemberOccupation = (CustomFontTextView) convertView.findViewById(R.id.tv_occupation);
            holder.ivContactStatus = (ImageView) convertView.findViewById(R.id.iv_contact_status);

            // handle click on check box
            holder.chequeBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    ChatContactMO member = (ChatContactMO) cb.getTag();
                    ArrayList<ChatContactMO> list = new ArrayList<ChatContactMO>();
                    member.generatePersistenceKey(member.getId() + "");
                    list.add(member);
                    if (!cb.isChecked()) {
                        selectedInvites.remove(member);

                        actionSelectedInviteCount.setSubtitle(context.getResources().getQuantityString(R.plurals.number_of_contacts_selected,
                                getSelectedMembersCount(),
                                getSelectedMembersCount()));
                    } else {
                        if (selectedInvites == null) {
                            setSelectedInvites(list);
                        } else {
                            // check for number of added  members and set selected to false explicitly
                            if (selectedInvites != null && selectedInvites.size() + alreadyAddedContactsSize < VariableConstants.GROUP_MEMBER_SIZE_LIMIT - 1) {
                                setSelectedInvites(list);
                            } else {
                                cb.setChecked(false);
                                Toast.makeText(context, context.getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG).show();
                            }
                        }
                        actionSelectedInviteCount.setSubtitle(context.getResources().getQuantityString(R.plurals.number_of_contacts_selected,
                                getSelectedMembersCount(),
                                getSelectedMembersCount()));
                    }
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ChatContactMO invitedMember = memberList.get(position);
        if (selectedInvites != null && selectedInvites.size() > 0) {
            if (selectedInvites.contains(invitedMember)) {
                holder.chequeBox.setChecked(true);
            } else {
                holder.chequeBox.setChecked(invitedMember.getIsSelected());
            }
        } else {
            holder.chequeBox.setChecked(invitedMember.getIsSelected());
        }


        holder.tvMemberName.setText(invitedMember.getRealName());
        if (invitedMember.getSpeciality() != null && !invitedMember.getSpeciality().equals(context.getResources().getString(R.string.txt_null))) {
            holder.tvMemberOccupation.setText(invitedMember.getSpeciality());
        } else {
            holder.tvMemberOccupation.setText(invitedMember.getUserType().getName());
        }
        imgLoader.DisplayImage(getUrl(invitedMember.getEmail()), holder.ivProfilePic, R.drawable.physician_generic);
        holder.chequeBox.setTag(invitedMember);
        if (invitedMember.getUserType() == null || invitedMember.getUserType().getName().equalsIgnoreCase(TextConstants.PATIENT)) {
            holder.ivContactStatus.setVisibility(View.INVISIBLE);
        } else {
            String presenceType = invitedMember.getPresenceType();
            holder.ivContactStatus.setVisibility(View.VISIBLE);
            if (presenceType == null || presenceType.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_available);
            } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_busy);
            } else if (presenceType.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_away);
            } else {
                holder.ivContactStatus.setImageResource(R.drawable.ic_status_available);
            }
        }

        return convertView;
    }

    public int getSelectedMembersCount() {
        if (selectedInvites != null) {
            return selectedInvites.size() + alreadyAddedContactsSize;
        }
        return alreadyAddedContactsSize;
    }

    public void setInviteMemberListAdapter(ArrayList<ChatContactMO> inviteList) {
        memberList = (ArrayList<ChatContactMO>) inviteList;
        memberList.size();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return memberList == null ? 0 : memberList.size();
    }

    @Override
    public Object getItem(int position) {
        return memberList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setSelectedInvites(List<ChatContactMO> selectedInvites) {
        if (this.selectedInvites != null && this.selectedInvites.size() > 0) {
            this.selectedInvites.addAll(selectedInvites);
            return;
        } else {
            this.selectedInvites = selectedInvites;
        }
    }

    public void clearSelectedInvites() {
        try {
            this.selectedInvites.clear();
        } catch (Exception e) {

        }
    }

    public List<ChatContactMO> getInvitees() {
        return selectedInvites;
    }

    private String getUrl(String email) {
        SettingManager settingManager = SettingManager.getInstance();
        SettingMO setMO = settingManager.getSettings();
        UserMO userMO = settingManager.getUserMO();
        boolean useHTTPS = setMO.isUseHTTPS();
        String httpStr = "https://";
        if (!useHTTPS)
            httpStr = "http://";
        String url = httpStr + BASE_URL + "username=" + userMO.getUsername() + "&password=" + userMO.getPassword() + "&email=" + email;
        return url;
    }

    public void clearSDCard() {
        imgLoader.clearSdCard();
    }

    public class ViewHolder {
        public CustomFontTextView tvMemberName;
        public CustomFontTextView tvMemberOccupation;
        public CheckBox chequeBox;
        public ImageView ivProfilePic;
        public ImageView ivContactStatus;
    }
}
