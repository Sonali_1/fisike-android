package com.mphrx.fisike.adapter;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.xbill.DNS.DClass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SettingItemData;
import com.mphrx.fisike.lock_screen.LockScreenActivity;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.utils.BaseActivity;

public class mPINAdapter extends RecyclerView.Adapter<mPINAdapter.SettingViewHolder> {
    private ArrayList<SettingItemData> SettingList;
    private String title;
    public clickListener listener;
    Context context;
    ConfigMO config;

    public mPINAdapter(ArrayList<SettingItemData> settingList, Context context) {
        this.SettingList = settingList;
        this.context=context;
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    public class SettingViewHolder extends RecyclerView.ViewHolder  {
        // each data item is just a string in this case
        CustomFontTextView tvTitle;
        CustomFontTextView tvSubTitle;
        CheckBox cbSettings;

        public SettingViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.tv_fieldTitle);
            this.tvSubTitle = (CustomFontTextView) v.findViewById(R.id.tv_fieldSubTitle);
            cbSettings = (CheckBox) v.findViewById(R.id.cb_settings);
            config = ConfigManager.getInstance().getConfig();

            v.setOnClickListener(new OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    // TODO Auto-generated method stub
                    if(tvTitle.getText().toString().equalsIgnoreCase(TextConstants.mPIN))
                    {
                        if(cbSettings.isChecked())
                            cbSettings.setChecked(false);
                        else
                            cbSettings.setChecked(true);
                    }
                    else
                    {
                        if(config.isShowPin())
                            launchLockScreen(true, false);
                        else
                            Toast.makeText(context, context.getResources().getString(R.string.Please_enable_mPIN_first), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            cbSettings.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    // TODO Auto-generated method stub
                    SharedPreferences sharedpref = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                    Editor edit=sharedpref.edit();
                    edit.putBoolean(VariableConstants.MPIN_SET, cbSettings.isChecked());
                    edit.commit();

                    if (isChecked)
                    {
                        launchLockScreen( true, true);
                        return;
                    }
                    else
                    {
                        edit.putString(VariableConstants.LOCK_PIN_CODE, "").commit();
                        // btnChangePin.setEnabled(isChecked);
                    }

                    config.setShowPin(isChecked);
                    config = ConfigManager.getInstance().updateConfig(config);
                    if (((BaseActivity) context).ismBound())
                    {
                        ((BaseActivity) context).getmService().startPinScreenTimer();
                    }
                }
            });

        }

//		@Override
//		public void onClick(View v) {
//			if (listener != null) {
//				listener.itemClicked(v, getAdapterPosition());
//			}
//		}

    }

    @Override
    public int getItemCount() {
        return SettingList.size();
    }

    @Override
    public void onBindViewHolder(SettingViewHolder arg0, int arg1) {
        title = SettingList.get(arg1).getTitle();
        arg0.tvTitle.setText(title);
        arg0.tvSubTitle.setText(SettingList.get(arg1).getSubTitle());
        if (title.equalsIgnoreCase(TextConstants.mPIN)) {
            arg0.cbSettings.setVisibility(View.VISIBLE);
            SharedPreferences sharedpref = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            arg0.cbSettings.setChecked(sharedpref.getBoolean(VariableConstants.MPIN_SET, false));
        }
    }

    @Override
    public SettingViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.setting_row, arg0, false);
        SettingViewHolder settingHolder = new SettingViewHolder(row);
        return settingHolder;

    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

    private void launchLockScreen(boolean is_pass, boolean is_conf) {
        context.startActivity(new Intent(context, LockScreenActivity.class).putExtra(VariableConstants.IS_TO_SET_PASSWORD, is_pass).putExtra(VariableConstants.IS_TO_SET_CONFIG, is_conf));
    }

}
