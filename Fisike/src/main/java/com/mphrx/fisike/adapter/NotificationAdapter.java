package com.mphrx.fisike.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SettingItemData;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> implements CompoundButton.OnCheckedChangeListener {
    private ArrayList<SettingItemData> SettingList;
    private String title;
    public clickListener listener;
    Context context;

    public NotificationAdapter(ArrayList<SettingItemData> settingList, Context context) {
        this.SettingList = settingList;
        this.context = context;
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        // each data item is just a string in this case
        CustomFontTextView tvTitle;
        SwitchCompat cbSettings;
        View temp_line;
        public NotificationViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.tv_notificationTitle);
            cbSettings = (SwitchCompat) v.findViewById(R.id.cb_notificationsettings);
            cbSettings.setOnClickListener(this);
            temp_line = (View) v.findViewById(R.id.temp_line);
        }

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (listener != null) {
                listener.itemClicked(v, getAdapterPosition());
            }
        }

    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return SettingList.size();
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder arg0, int arg1) {
        // TODO Auto-generated method stub
        title = SettingList.get(arg1).getTitle();
        arg0.tvTitle.setText(title);
        setCheckBox(title, arg0);
        if (arg1 == SettingList.size() - 1) {
            arg0.temp_line.setVisibility(View.GONE);
        } else {
            arg0.temp_line.setVisibility(View.VISIBLE);
        }
    }

    private void setCheckBox(String title, NotificationViewHolder arg0) {
        SharedPreferences shPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean value = false;
        if(title.equals(context.getResources().getString(R.string.Messages))) {
            value = shPreferences.getBoolean(VariableConstants.MSG_NOTIFICATION_ENABLED, true);
            arg0.cbSettings.setChecked(value);
        }
        else if(title.equals(context.getResources().getString(R.string.DocumentUploaded))) {
            value = shPreferences.getBoolean(VariableConstants.UPLOADS_NOTIFICATION_ENABLED, true);
            arg0.cbSettings.setChecked(value);
        }
        else if (title.equals(context.getResources().getString(R.string.Medications))){
            value = shPreferences.getBoolean(VariableConstants.MEDICATION_NOTIFICATION_ENABLED, true);
            arg0.cbSettings.setChecked(value);
        }
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.notification_setting_row, arg0, false);
        NotificationViewHolder settingHolder = new NotificationViewHolder(row);
        return settingHolder;

    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

	public String getItemAtPosition(int position)
	{
		return SettingList.get(position).getTitle();
	}

}
