package com.mphrx.fisike.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SettingItemData;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;

import appointment.utils.CustomTypefaceSpan;

public class settingsAdapter extends RecyclerView.Adapter<settingsAdapter.SettingViewHolder> {
    private ArrayList<SettingItemData> SettingList;
    private String title;
    public clickListener listener;
    Context context;
    UserMO userMO;
    private Typeface font_bold, font_italic;

    public settingsAdapter(ArrayList<SettingItemData> settingList, Context context) {
        this.SettingList = settingList;
        this.context = context;
        userMO = SettingManager.getInstance().getUserMO();
        font_bold = Typeface.createFromAsset(this.context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_bold));
        font_italic = Typeface.createFromAsset(this.context.getAssets(), this.context.getResources().getString(R.string.opensans_ttf_italic));
    }

    public void setClickListener(clickListener listener) {
        this.listener = listener;
    }

    public class SettingViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        // each data item is just a string in this case
        CustomFontTextView tvTitle;
        CustomFontTextView tvSubTitle;
        CustomFontTextView tv_heading;
        CheckBox cbSettings;
        RelativeLayout rl_header;
        CardView rl_row_items;

        public SettingViewHolder(View v) {
            super(v);
            this.tvTitle = (CustomFontTextView) v.findViewById(R.id.tv_fieldTitle);
            this.tvSubTitle = (CustomFontTextView) v.findViewById(R.id.tv_fieldSubTitle);
            cbSettings = (CheckBox) v.findViewById(R.id.cb_settings);
            rl_header = (RelativeLayout) v.findViewById(R.id.layout_section);
            rl_row_items = (CardView) v.findViewById(R.id.card);
            tv_heading = (CustomFontTextView) v.findViewById(R.id.tv_heading);
            v.setOnClickListener(this);
            cbSettings.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.itemClicked(v, getAdapterPosition());
            }
        }

    }

    @Override
    public int getItemCount() {
        return SettingList.size();
    }

    @Override
    public void onBindViewHolder(SettingViewHolder arg0, int arg1) {

        title = SettingList.get(arg1).getTitle();

        if (SettingList.get(arg1).getSubTitle().equals(context.getResources().getString(R.string.section))) {
            arg0.tv_heading.setText(title);
            arg0.rl_row_items.setVisibility(View.GONE);
            arg0.rl_header.setVisibility(View.VISIBLE);
        } else {

            arg0.tvSubTitle.setText(SettingList.get(arg1).getSubTitle());

            if (title.equals(context.getResources().getString(R.string.change_language))) {
                String title_string = title + "  (" + SharedPref.getLanguageSelected() + ")";
                SpannableString text = new SpannableString(title_string);
                text.setSpan(new CustomTypefaceSpan("", font_italic), context.getResources().getString(R.string.change_language).length() + 1, text.length(), 12);
                arg0.tvTitle.setText(text);
            } else {
                arg0.tvTitle.setText(title);
            }

            arg0.rl_row_items.setVisibility(View.VISIBLE);
            arg0.rl_header.setVisibility(View.GONE);
        }

    }

    @Override
    public SettingViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.setting_row, arg0, false);
        SettingViewHolder settingHolder = new SettingViewHolder(row);
        return settingHolder;

    }

    public interface clickListener {
        public void itemClicked(View view, int position);
    }

    public String getTitle(int position) {
        return SettingList.get(position).getTitle();
    }
}
