package com.mphrx.fisike.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.MyHealthItemsEnum;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.provider.ChatIQ;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.MyHealthTabItemsHolder;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by laxmansingh on 7/3/2017.
 */

public class HomeGridAdapter extends RecyclerView.Adapter<HomeGridAdapter.HomeGridViewHolder> {

    private Context context;
    private LinkedHashMap<String, Integer> myHealthTabItemsMap;
    private ArrayList<String> pageTitles = new ArrayList<>();
    ArrayList<String> displayPageTitles = new ArrayList<>();


    public HomeGridAdapter(FragmentActivity context) {
        this.context = context;
        myHealthTabItemsMap = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().getMyHealthTabItemsMap();
        int i = 0;
        for (String key : myHealthTabItemsMap.keySet()) {
            if (key.equals(MyHealthItemsEnum.Vitals.getTabItemName())) {
                pageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_vitals_txt));
            } else if (key.equals(MyHealthItemsEnum.Records.getTabItemName())) {
                pageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_records));
            } else if (key.equals(MyHealthItemsEnum.Medications.getTabItemName())) {
                pageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_medication));
            } else if (key.equals(MyHealthItemsEnum.Uploads.getTabItemName())) {
                pageTitles.add(i, MyApplication.getAppContext().getResources().getString(R.string.tab_uploads));
            }
            i++;
        }
    }

    @Override
    public HomeGridAdapter.HomeGridViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_tiles_list_item, null);
        HomeGridAdapter.HomeGridViewHolder viewHolder = new HomeGridAdapter.HomeGridViewHolder(view, context);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return pageTitles.size();
    }


    @Override
    public void onBindViewHolder(final HomeGridViewHolder customViewHolder, final int position) {
        customViewHolder.tv_title.setText(pageTitles.get(position));

        if (pageTitles.get(position).equals(context.getResources().getString(R.string.tab_vitals_txt))) {

            customViewHolder.img_title_icon.setText(context.getResources().getString(R.string.fa_observation_glucose));
            customViewHolder.img_title_icon.setTextColor(context.getResources().getColor(R.color.fill1));

        } else if (pageTitles.get(position).equals(context.getResources().getString(R.string.tab_records))) {

            customViewHolder.img_title_icon.setText(context.getResources().getString(R.string.fa_services_ico));
            customViewHolder.img_title_icon.setTextColor(context.getResources().getColor(R.color.fill2));

        } else if (pageTitles.get(position).equals(context.getResources().getString(R.string.tab_medication))) {

            customViewHolder.img_title_icon.setText(context.getResources().getString(R.string.fa_capsule));
            customViewHolder.img_title_icon.setTextColor(context.getResources().getColor(R.color.blue_sky));

        } else if (pageTitles.get(position).equals(context.getResources().getString(R.string.tab_uploads))) {

            customViewHolder.img_title_icon.setText(context.getResources().getString(R.string.fa_discharge_note));
            customViewHolder.img_title_icon.setTextColor(context.getResources().getColor(R.color.orange));

        }

        boolean isRtl = Utils.isRTL(context);

        if (position % 2 == 0) {

            if (isRtl)
                customViewHolder.rl_header.setPadding((int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context), 0, (int) Utils.convertDpToPixel(5, context));
            else
                customViewHolder.rl_header.setPadding(0, (int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context));

        } else {

            if (isRtl)
                customViewHolder.rl_header.setPadding(0, (int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context));
            else
                customViewHolder.rl_header.setPadding((int) Utils.convertDpToPixel(5, context), (int) Utils.convertDpToPixel(5, context), 0, (int) Utils.convertDpToPixel(5, context));

        }

    }


    public class HomeGridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        private CustomFontTextView tv_title;
        private IconTextView img_title_icon;
        private RelativeLayout clicked_view, rl_header;

        public HomeGridViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            view.setClickable(true);
            view.setOnClickListener(this);

            tv_title = (CustomFontTextView) view.findViewById(R.id.tv_title);
            clicked_view = (RelativeLayout) view.findViewById(R.id.clicked_view);
            rl_header = (RelativeLayout) view.findViewById(R.id.rl_header);
            img_title_icon = (IconTextView) view.findViewById(R.id.img_title_icon);
            clicked_view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            onClickRequest(v);
        }

        public void onClickRequest(View v) {
            int adapter_position = getAdapterPosition();

            Fragment fragment = new MyHealth();
            Bundle bundle = new Bundle();
            if (pageTitles.get(adapter_position).equals(context.getResources().getString(R.string.tab_vitals_txt))) {
                // [***  No need to place bundle as vitals is handled in @MYHealh  ***]
            } else if (pageTitles.get(adapter_position).equals(context.getResources().getString(R.string.tab_records))) {
                bundle.putBoolean(VariableConstants.LAUNCH_RECORDS_SCREEN, true);
            } else if (pageTitles.get(adapter_position).equals(context.getResources().getString(R.string.tab_medication))) {
                bundle.putBoolean(VariableConstants.LAUNCH_MEDICATION_SCREEN, true);
            } else if (pageTitles.get(adapter_position).equals(context.getResources().getString(R.string.tab_uploads))) {
                bundle.putBoolean(VariableConstants.LAUNCH_UPLOAD_SCREEN, true);
            }
            ((BaseActivity) context).selectItem(NavigationDrawerActivity.MYHEALTH, bundle);
        }
    }
}
