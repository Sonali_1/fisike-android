package com.mphrx.fisike.adapter;

public class NavDrawerItem {

	private String title;
	private String navIcon;
	private boolean isCountVisible;
	private String count;
	private int identifier;
	private String classname;

	public NavDrawerItem(String title, String navIcon, int identifier,String className){
		this.title = title;
		this.navIcon = navIcon;
		this.identifier = identifier;
		this.classname = className;
	}


	public NavDrawerItem(String title, String navIcon, boolean isCountVisible, String count,int identifier){
		this.title = title;
		this.navIcon = navIcon;
		this.isCountVisible = isCountVisible;
		this.count = count;
		this.identifier = identifier;
	}

	public int getIdentifier() {
		return identifier;
	}

	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResId() {
		return navIcon;
	}

	public void setResId(String resId) {
		this.navIcon = navIcon;
	}

	public boolean isCountVisible() {
		return isCountVisible;
	}

	public void setCountVisible(boolean isCountVisible) {
		this.isCountVisible = isCountVisible;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}
}
