package com.mphrx.fisike.createUserProfile.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.createUserProfile.CreatePhysicianProfile;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.views.CircleImageView;

/**
 * Created by Neha on 05-05-2016.
 */
public class SetProfilePictureFragment extends BaseFragment {

    CircleImageView iv_profile_pic;
    CustomFontButton btnNext, btnSkip;
    IconTextView iconCamera;
    Bitmap bitmap;

    public static SetProfilePictureFragment getInstance(Bundle bundle) {
        SetProfilePictureFragment fragment = new SetProfilePictureFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_profile_picture;
    }

    @Override
    public void findView() {
        iv_profile_pic = (CircleImageView) getView().findViewById(R.id.iv_profile_pic);
        btnNext = (CustomFontButton) getView().findViewById(R.id.btnNext);
        btnSkip = (CustomFontButton) getView().findViewById(R.id.btnSkip);
        iconCamera = (IconTextView) getView().findViewById(R.id.itv_icon_camera);
    }

    @Override
    public void initView() {
        setProfilePicture(null);
    }

    @Override
    public void bindView() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                    if (BuildConfig.isPatientApp)
                        ((CreateProfileActivity) getActivity()).setProfilePictureAndSendApiForSignUp(bitmap);
                    else
                        ((CreatePhysicianProfile) getActivity()).setProfilePictureAndSendApiForSignUp(bitmap);
                }

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                    if (BuildConfig.isPatientApp)
                        ((CreateProfileActivity) getActivity()).setProfilePictureAndSendApiForSignUp(null);
                    else
                        ((CreatePhysicianProfile) getActivity()).setProfilePictureAndSendApiForSignUp(null);
                }
            }
        });

        iv_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BuildConfig.isPatientApp)
                    ((CreateProfileActivity) getActivity()).showBottomSheet();
                else
                    ((CreatePhysicianProfile) getActivity()).showBottomSheet();
            }
        });


    }

    public void setProfilePicture(Bitmap bitmap) {
        this.bitmap = bitmap;
        if (bitmap != null) {
            iv_profile_pic.setImageBitmap(Utils.scaleBitmapAndKeepRation(bitmap,bitmap.getHeight(),bitmap.getWidth() ));
            iconCamera.setVisibility(View.INVISIBLE);
        } else {
            iv_profile_pic.setImageBitmap(null);
            iv_profile_pic.setBackgroundResource(R.drawable.circle_bluegrey);
            iconCamera.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Bundle args = new Bundle();
        args.putString("origin", SetProfilePictureFragment.class.getName());
        if (BuildConfig.isPatientApp)
            ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
        else
            ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(args);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
    }
}
