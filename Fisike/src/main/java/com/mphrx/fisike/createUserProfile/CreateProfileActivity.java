package com.mphrx.fisike.createUserProfile;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.apache.commons.codec.binary.Base64;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.CreateHeightWeightSpinnerValue;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.VolleyRequest.SignUpRequest;
import com.mphrx.fisike.createUserProfile.VolleyResponse.SignUpResponse;
import com.mphrx.fisike.createUserProfile.fragment.InputDetailAgeActivity;
import com.mphrx.fisike.createUserProfile.fragment.SetGenderFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetHeightFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetPasswordFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetProfilePictureFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetUserNameFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetWeightFragment;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.SignUpGsonRequest;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.imageCropping.Crop;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.BmiSignupData;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.background.FetchVitalConfigRequest;
import com.mphrx.fisike.vital_submit.request.ObservationValueUnit;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.network.request.RegisterUserDeviceRequest;
import com.mphrx.fisike_physician.network.response.RegisterUserDeviceResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.ExifUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CreateProfileActivity extends NetworkActivity implements FragmentManager.OnBackStackChangedListener {
    protected static final int SUCESS_RESULT = 1, CAMERA_RESULT = 2, GALLERY_RESULT = 3, CROP_RESULT = 4;
    private String unit;
    private Float number;
    String firstName, otp;
    String lastName;
    String password;
    String dob;
    Height height;
    String heightUnit;
    Weight weight;
    String weightUnit;
    private String gender;
    private Toolbar toolbar;
    private BottomSheetDialog mBottomSheetDialog;
    private File photoUri;
    private String realPathFromURI;
    private Uri mImageCaptureUri = null;
    private boolean isCameraImage;
    private Bitmap tempBitmap;
    private String encodedImage, ImageName;
    SignUpGsonRequest request;
    private boolean isDisableAccount;
    private UserMO signUpUserMo;
    private Bundle mMainFragmentArgs;
    private String heightUnitType, weightUnitType;
    private String actualHeightValue;
    private float actualWeightValue;
    private ArrayList<String> itemKgSpinner, itemLBSSpinner;
    private boolean isToLaunchVitalsBlockScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.create_profile_layout_patient);
        findView();
        if (savedInstanceState == null)
            addOrInitSetPasswordFragment();
        //  addFragment(SetPasswordFragment.getInstance(null), R.id.fragment_container, false);

        otp = getIntent().getStringExtra(VariableConstants.OTP_CODE);
        isDisableAccount = getIntent().getBooleanExtra(TextConstants.DISABLE_USER, false);
        request = new SignUpGsonRequest();
        getFragmentManager().addOnBackStackChangedListener(this);
        new CreateHeightWeightSpinnerValue(this).execute();
    }


    private void findView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBackPress();
            }
        });
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    public void onClick(View v) {
        int id = v.getId();
    }


    public void addOrInitSetPasswordFragment() {
        setTitle(getResources().getString(R.string.set_password));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
     /*   fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
     */
        fragmentTransaction.add(R.id.fragment_container, SetPasswordFragment.getInstance(null));

        fragmentTransaction.addToBackStack(SetPasswordFragment.class.getName());
        fragmentTransaction.commit();
    }

    public void addOrInitAgeFragment() {
        setTitle(getResources().getString(R.string.more_abt_u));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        /*if (fragment == null || !fragment.isVisible())
        {
*/
        fragmentTransaction.replace(R.id.fragment_container, InputDetailAgeActivity.getInstance(null));
        fragmentTransaction.addToBackStack(InputDetailAgeActivity.class.getName());
        fragmentTransaction.commit();
    }

    private void addOrInitUserNameFragment() {
        //SetUserNameFragment fragment = (SetUserNameFragment) getFragmentByKey(SetUserNameFragment.class);
        setTitle(getResources().getString(R.string.create_your_profile));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        /*if (fragment == null || !fragment.isVisible())
        {
*/
        fragmentTransaction.replace(R.id.fragment_container, SetUserNameFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetUserNameFragment.class.getName());
        fragmentTransaction.commit();
        //  replaceFragment(SetUserNameFragment.getInstance(null), R.id.fragment_container, false);
  /*      }
        else
        {
            fragment.initView();
        }
 */
    }


    private void addOrInitGenderFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        fragmentTransaction.replace(R.id.fragment_container, SetGenderFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetGenderFragment.class.getName());
        fragmentTransaction.commit();

    }

    private void addOrInitHeightFragment(String gender) {
        this.gender = gender;
       /* SetHeightFragment fragment = (SetHeightFragment) getFragmentByKey(SetHeightFragment.class);
        if (fragment == null || !fragment.isVisible())
        {
            replaceFragment(SetHeightFragment.getInstance(null), R.id.fragment_container, false);
        }
        else
        {
            fragment.initView();
        }*/
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        /*if (fragment == null || !fragment.isVisible())
        {
*/
        Bundle extras = new Bundle();
        extras.putString(TextConstants.GENDER, gender);
        fragmentTransaction.replace(R.id.fragment_container, SetHeightFragment.getInstance(extras));
        fragmentTransaction.addToBackStack(SetHeightFragment.class.getName());
        fragmentTransaction.commit();

    }

    private void addOrInitProfilePictureFragment() {
      /*  SetProfilePictureFragment fragment = (SetProfilePictureFragment) getFragmentByKey(SetProfilePictureFragment.class);
        if (fragment == null || !fragment.isVisible())
        {
            replaceFragment(SetProfilePictureFragment.getInstance(null), R.id.fragment_container, false);
        }
        else
        {
            fragment.initView();
        }*/
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);        /*if (fragment == null || !fragment.isVisible())
        {
*/
        fragmentTransaction.replace(R.id.fragment_container, SetProfilePictureFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetProfilePictureFragment.class.getName());
        fragmentTransaction.commit();

    }

    private void addOrInitWeightFragment() {
     /*   SetWeightFragment fragment = (SetWeightFragment) getFragmentByKey(SetWeightFragment.class);
        if (fragment == null || !fragment.isVisible())
        {
            replaceFragment(SetWeightFragment.getInstance(null), R.id.fragment_container, false);
        }
        else
        {
            fragment.initView();
        }*/

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        /*if (fragment == null || !fragment.isVisible())
        {
*/
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(TextConstants.KG_SPINNER, itemKgSpinner);
        bundle.putStringArrayList(TextConstants.LBS_SPINNER, itemLBSSpinner);
        fragmentTransaction.replace(R.id.fragment_container, SetWeightFragment.getInstance(bundle));
        fragmentTransaction.addToBackStack(SetWeightFragment.class.getName());
        fragmentTransaction.commit();

    }


    public void launchGetAgeDetailsFragment(String fname, String lname) {
        this.firstName = fname;
        this.lastName = lname;
        request.setFirstName(fname);
        request.setLastName(lname);
        addOrInitAgeFragment();
    }

    public void setPasswordAndOpenUserNameFragment(String password) {
        request.setPassword(password);
        this.password = password;
        addOrInitUserNameFragment();

    }

    public void setDobAndOpenGenderFragment(String dob) {
        request.setDob(DateTimeUtil.convertSourceDestinationDate(dob, DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_z_appended));
        this.dob = dob;
        addOrInitGenderFragment();
    }

    public void setGenderAndOpenHeightFragment(String gender) {
        if (gender != null && !gender.trim().equals(""))
            request.setGender(gender);

        this.gender = gender;
        addOrInitHeightFragment(gender);

    }

    public void setHeightAndOpenWeightFragment(Height height, String unit_type, String value) {
        if (height != null) {
            request.setHeight(height);
            this.height = height;

        } else {
            Height Height = new Height();
            Height.setValue("0");
            Height.setUnit("cm");
            request.setHeight(Height);
            this.height = Height;

        }
        this.heightUnitType = unit_type;
        this.actualHeightValue = value;
        addOrInitWeightFragment();
    }


    public void handleBackPress() {
        if (getFragmentManager().getBackStackEntryCount() == 1) {

            //EDITED BY NEHA   setGetStartedScreenView();

            Intent i = new Intent(this, OtpActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(TextConstants.PREFILL_NUM, true);
            startActivity(i);
        } else {
            getFragmentManager().popBackStack();
        }

    }

    @Override
    public void onBackPressed() {
        handleBackPress();
    }

    @Override
    public void onBackStackChanged() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (f != null)
            updateTitle(f);
    }

    private void updateTitle(Fragment f) {
        String className = f.getClass().getName();

        if (className.equals(SetPasswordFragment.class.getName()))
            setToolbarTitle(getResources().getString(R.string.set_password));

        else if (className.equals(SetUserNameFragment.class.getName()))
            setToolbarTitle(getResources().getString(R.string.create_your_profile));

        else
            setToolbarTitle(getResources().getString(R.string.more_abt_u));
    }


    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void setWeightAndOpenProfilePictureFragment(Weight h, String unit, float actualWeightValue) {


        if (h != null) {
            request.setWeight(h);
            this.weight = h;
        } else {
            Weight wt = new Weight();
            wt.setValue("0");
            wt.setUnit("Kg");
            request.setWeight(wt);
            this.weight = wt;
        }

        this.actualWeightValue = actualWeightValue;
        this.weightUnitType = unit;
        addOrInitProfilePictureFragment();
    }

    private void removeImage() {
        dismissBottomSheet();
        tempBitmap = null;
        setProfilePicture(null);

    }

    /**
     * Dismiss the bottom sheet
     */
    private void dismissBottomSheet() {
        if (mBottomSheetDialog.isShowing()) {
            mBottomSheetDialog.dismiss();
        }
    }

    private void openGallery() {
        dismissBottomSheet();
        photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                System.currentTimeMillis() + ".jpg");
        photoUri.setWritable(true);
        mImageCaptureUri = Uri.fromFile(photoUri);
        IntentUtils.onClickChooseImage(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }


    public void showOrHideRemoveImageMenu(View view) {
        Utils.hideKeyboard(this);
        if (tempBitmap != null)
            view.findViewById(R.id.remove_image).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.remove_image).setVisibility(View.GONE);
    }

    /**
     * Show bottom sheet for sending the attachment
     */
    public void showBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.change_image_layout, null);
        showOrHideRemoveImageMenu(view);
        float peekHeight = view.getMeasuredHeight();
        view.findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), CAMERA_RESULT))
                    takePhoto();

            }
        });

        view.findViewById(R.id.choose_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg)+ " " + getString(R.string.permission_storage), GALLERY_RESULT))
                    openGallery();

            }
        });

        view.findViewById(R.id.remove_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                removeImage();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALLERY_RESULT:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(photoUri);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        realPathFromURI = ExifUtils.getPath(getApplicationContext(), data.getData());
                        inputStream.close();
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    AppLog.d("Image File Path", "" + realPathFromURI);
                    isCameraImage = false;
                    startCropImage();
                    break;

                case CAMERA_RESULT:
                    isCameraImage = true;
                    captureAndCropImage(data);
                    break;

                case CROP_RESULT:
                    Uri croppedUri = Crop.getOutput(data);
                    try {
                        tempBitmap = ExifUtils.rotateBitmap(croppedUri.getPath().toString(), MediaStore.Images.Media.getBitmap(this.getContentResolver(), croppedUri));
                        setProfilePicture(tempBitmap);
                    } catch (IOException e) {
                        // FIXME there should atleast be a toast here ... but there wasn't one before so not adding
                    }
                    if (isCameraImage) {
                        // FIXME @Rajan probably not needed anymore
                        String croppedImagePath = realPathFromURI.toString().replace(".jpg", "~2.jpg");
                        File croppedImageFile = new File(croppedImagePath);
                        if (croppedImageFile.exists()) {
                            croppedImageFile.delete();
                        }

                        File file = new File(realPathFromURI);
                        Uri uri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        try {
                            ContentResolver resolver = this.getContentResolver();

                            // change type to image, otherwise nothing will
                            // be deleted
                            ContentValues contentValues = new ContentValues();
                            int media_type = 1;
                            contentValues.put("media_type", media_type);
                            resolver.update(Uri.parse(realPathFromURI), contentValues, null, null);
                            resolver.delete(Uri.parse(realPathFromURI), null, null);
                        } catch (Exception e) {

                        }
                        Utils.deleteFile(this, realPathFromURI, uri1, VariableConstants.ATTACHMENT_IMAGE);
                        if (file.exists()) {
                            boolean deleted = file.delete();
                        }
                    }


                    break;

                default:
                    break;
            }
        }
    }

    private void takePhoto() {
        dismissBottomSheet();
        try {
            photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    System.currentTimeMillis() + ".jpg");
            photoUri.setWritable(true);
            mImageCaptureUri = Uri.fromFile(photoUri);
            realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentUtils.onClickTakePhoto(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }

    private void captureAndCropImage(Intent data) {
        if (null != photoUri && !"".equals(photoUri)) {
            mImageCaptureUri = Uri.fromFile(photoUri);
        }
        mImageCaptureUri = (null == data || null == data.getData()) ? mImageCaptureUri : data.getData();
        if (null != mImageCaptureUri) {
            realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
            if (null == realPathFromURI) {
                realPathFromURI = mImageCaptureUri.getPath();
                if (null == realPathFromURI) {
                    return;
                }
            }

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(realPathFromURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
/*
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
*/
            startCropImage();
        }
    }

    private void startCropImage() {

        DisplayMetrics display = getResources().getDisplayMetrics();
        int screenWidth = display.widthPixels;
        int screenHeight = display.heightPixels;
        int gcd = GCD(screenWidth, screenHeight);
        int aspect_x = screenHeight / gcd;
        int aspect_y = screenWidth / gcd;
        if (screenWidth > 400)
            screenWidth = 400;
        int output_x = 400;
        int output_y = 400;
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped-profile-pic"));
        startActivityForResult(Crop.of(Uri.fromFile(photoUri), destination)
                .asSquare()
                .withMaxSize(output_x, output_y)
                .getIntent(this), CROP_RESULT);
    }

    private int GCD(int a, int b) {
        return (b == 0 ? a : GCD(b, a % b));
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }


    private void setProfilePicture(Bitmap bitmap) {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment.getClass().getName().equals(SetProfilePictureFragment.class.getName()))
            ((SetProfilePictureFragment) fragment).setProfilePicture(bitmap);
    }


    public void setProfilePictureAndSendApiForSignUp(Bitmap bitmap) {
        tempBitmap = bitmap;

        if (bitmap != null) {
            encodedImage = Base64.encodeBase64String(Utils.convertBitmapToByteArray(bitmap));
            ImageName = firstName + "_" + lastName + "_" + System.currentTimeMillis();
            request.setImageBase64(encodedImage);
            request.setImageName(ImageName);
        } else {
            request.setImageBase64(null);
            request.setImageName(null);
        }

        if (Utils.showDialogForNoNetwork(this, false))
            if (checkPermission(Manifest.permission.READ_PHONE_STATE, getString(R.string.permission_msg)+ " " + getString(R.string.permission_phone_state), TextConstants.PERMISSION_REQUEST_CODE))
                sendApiForSignUp();

    }

    private void sendApiForSignUp() {
        request.setOtp(otp);
        request.setDisableOtherAccount(isDisableAccount);
        request.setDeviceUID(SharedPref.getDeviceUid());
        request.setUserType(Utils.getUserType());
        if (SharedPref.getIsMobileEnabled())
            request.setPhoneNo(SharedPref.getMobileNumber());
        else if (!SharedPref.getIsMobileEnabled())
            request.setEmail(SharedPref.getEmailAddress());
        showProgressDialog();
        ThreadManager.getDefaultExecutorService().submit(new SignUpRequest(getTransactionId(), request, this));
    }


    @Subscribe
    public void onSignUpResponse(SignUpResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful()) {
            SharedPref.setAccessToken(response.getResponseGson().getToken());
            signUpUserMo = response.getResponseGson().getUser();
            signUpUserMo.setPassword(password);

            DecimalFormat format = new DecimalFormat(".##");
            //call for bmi on signup
            if (Utils.isValueAvailable(response.getResponseGson().getToken())) {
                if (Double.parseDouble(weight.getValue()) > 0 && Double.parseDouble(height.getValue()) > 0) {
                    ArrayList<ObservationValueUnit> observationArray = new ArrayList<ObservationValueUnit>();
                    observationArray.add(new ObservationValueUnit(String.valueOf(weight.getValue()), weight.getUnit().toLowerCase(), "Weight"));
                    observationArray.add(new ObservationValueUnit(String.valueOf(height.getValue()), height.getUnit().toLowerCase(), "Height"));
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                    String formattedDate = df.format(c.getTime());
                    BmiSignupData bmiSignupData = BmiSignupData.getInstance();
                    bmiSignupData.setDate(formattedDate);
                    bmiSignupData.setNumber(0);
                    bmiSignupData.setObservationArray(observationArray);
                    bmiSignupData.setPatientId((int) signUpUserMo.getPatientId());
                    ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(this, getTransactionId(), true));

                } else
                    ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(this, getTransactionId(), false));
            }

            //TODO add password field in SignUpApi

        } else {
            dismissProgressDialog();
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe
    public void onRegisterUserDeviceResponse(RegisterUserDeviceResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {

              /*
              TODO if app crash in profile then uncomment this and hanldle accordingly
                    UserMO userMo = SettingManager.getInstance().getUserMO();
                    new getPatientShowDetails(this, userMo.getPatientId() + "").execute();
              */

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    saveUserInfoAndLaunchHomeScreen(signUpUserMo);
                }
            }, 1500);

        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            isToLaunchVitalsBlockScreen = false;
        }
    }

    void saveUserInfoAndLaunchHomeScreen(UserMO user) {
        //user.setProfilePic(signUpUserMo.getProfilePic());
        user.setSalutation(TextConstants.YES);
        user.setPassword(password);
        if (user.getDateOfBirth() != null)
            user.setDateOfBirth(Utils.getFormattedDate(user.getDateOfBirth(),
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z,
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));
        try {
            long userKey = SharedPref.setUserName(user.getId());
            user.setPersistenceKey(userKey);
            UserDBAdapter.getInstance(this).insert(user);
            SharedPref.setUserLogout(false);
            Utils.setUserLoggedOut(false);
            if (tempBitmap != null)
                UserProfilePic.saveToInternalStorage(tempBitmap, MyApplication.getAppContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent i = new Intent(this, HomeActivity.class);
        i.putExtra(VariableConstants.LAUNCH_UPLOAD_SCREEN, false);
        i.putExtra(VariableConstants.LAUNCH_BLOCK_VITAL_SCREEN, isToLaunchVitalsBlockScreen);
        startActivity(i);
        this.finish();

    }

    public void saveMainFragmentState(Bundle args) {
        mMainFragmentArgs = args;
    }

    public Bundle getSavedMainFragmentState() {

        if (mMainFragmentArgs != null) {
            String origin = mMainFragmentArgs.getString("origin");
            if (origin.equals(SetUserNameFragment.class.getName())) {
                mMainFragmentArgs.putString("Password", this.password);
            } else if (origin.equals(InputDetailAgeActivity.class.getName())) {
                mMainFragmentArgs.putString("firstname", this.firstName);
                mMainFragmentArgs.putString("lastname", this.lastName);
            } else if (origin.equals(SetGenderFragment.class.getName())) {
            } else if (origin.equals(SetHeightFragment.class.getName())) {
                mMainFragmentArgs.putString(TextConstants.GENDER, this.gender);
            } else if (origin.equals(SetWeightFragment.class.getName())) {
                mMainFragmentArgs.putString("height", String.valueOf(height.getValue()));
                mMainFragmentArgs.putString("heightUnit", height.getUnit());
                mMainFragmentArgs.putString("unit_type_height", heightUnitType);
                mMainFragmentArgs.putString("actual_value_height", String.valueOf(actualHeightValue));
            } else if (origin.equals(SetProfilePictureFragment.class.getName())) {
                mMainFragmentArgs.putString("weight", String.valueOf(weight.getValue()));
                mMainFragmentArgs.putString("weightUnit", weight.getUnit());
                mMainFragmentArgs.putString("unit_type_weight", weightUnitType);
                mMainFragmentArgs.putString("actual_value_weight", String.valueOf(actualWeightValue));
            }
            return
                    mMainFragmentArgs;
        } else
            return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_RESULT:
                    takePhoto();
                    break;
                case GALLERY_RESULT:
                    openGallery();
                    break;
                case TextConstants.PERMISSION_REQUEST_CODE:
                    sendApiForSignUp();
                    break;
            }
        } else
            onPermissionNotGranted();
    }

    public void executeSetHeightWeightSpinnerValue(String s, ArrayList<String> item, ArrayList<String> itemLbs) {
        if (s.equals(TextConstants.SUCESSFULL_API_CALL)) {
            itemKgSpinner = item;
            itemLBSSpinner = itemLbs;
        } else {
            itemKgSpinner = null;
            itemLBSSpinner = null;
        }
    }


    private boolean isNotValidBMI(ArrayList<ObservationValueUnit> observationArray) {
        double formatQuantity1 = Double.parseDouble(observationArray.get(0).getValue());
        double formatQuantity2 = Double.parseDouble(observationArray.get(1).getValue());

        if (observationArray.get(0).getUnit().equalsIgnoreCase("lb")) {
            if (observationArray.get(1).getUnit().equalsIgnoreCase("ft/in")) {
                formatQuantity2 = formatQuantity2 * 12;
            } else if (observationArray.get(1).getUnit().equalsIgnoreCase("cm")) {
                formatQuantity2 = formatQuantity2 * 0.393701;
            }
            unit = "lb/in2";
        } else {
            if (observationArray.get(1).getUnit().equalsIgnoreCase("ft/in")) {
                formatQuantity2 = formatQuantity2 * 0.3048;
            } else if (observationArray.get(1).getUnit().equalsIgnoreCase("cm")) {
                formatQuantity2 = formatQuantity2 * 0.01;
            }
            unit = "kg/m2";
        }


        observationArray.get(0).setUnit(unit);


        if (unit.equals("lb/in2")) {
            number = Float.parseFloat(Utils.formatQuantity((formatQuantity1 / Math.pow(formatQuantity2, 2)) * 703, 1));
        } else {
            number = Float.parseFloat(Utils.formatQuantity(formatQuantity1 / Math.pow(formatQuantity2, 2), 1));
        }

        if (number >= 1 && number <= 99) {
            return false;
        }
        return true;
    }

    @Subscribe
    public void onVitalsConfigResponse(VitalsConfigResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            if (response.isValidJson()) // success API JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION_SUCCESS,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS, GoogleAnalyticsConstants.VITALS_API_NAME);
            else //success API but blank JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS_INCORRECT_JSON, GoogleAnalyticsConstants.VITALS_API_NAME);

        } else
            MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                            GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                    GoogleAnalyticsConstants.VITALS_VALUE_FAILURE, GoogleAnalyticsConstants.VITALS_API_NAME);

        ThreadManager.getDefaultExecutorService().submit(new RegisterUserDeviceRequest(getTransactionId()));
    }
}
