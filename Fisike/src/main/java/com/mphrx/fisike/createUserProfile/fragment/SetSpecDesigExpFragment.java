package com.mphrx.fisike.createUserProfile.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.createUserProfile.CreatePhysicianProfile;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DesignationEnum;
import com.mphrx.fisike.enums.ExperienceEnum;
import com.mphrx.fisike.enums.SpecialityEnum;
import com.mphrx.fisike_physician.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mphrx.fisike.R.style.AppStyle_themeWhite;

/**
 * Created by Neha on 14-07-2016.
 */
public class SetSpecDesigExpFragment extends BaseFragment implements View.OnClickListener {
    CustomFontTextView speciality, role, year;
    CustomFontButton btnNext;
    public static String SPECIALITY = "SPECIALITY", ROLE = "ROLE";
    private ArrayList<String> specialityList, roleList, yearList;
    private String selectedSpeciality, selectedRole, selectedYearsOfExperience;
    private AlertDialog alertDialog_Speciality, alertDialog_Role, alertDialog_Years;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_set_speciality;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTheme(AppStyle_themeWhite);
        Bundle args = ((CreatePhysicianProfile) getActivity()).getSavedMainFragmentState();
        if (args != null) {
            selectedSpeciality = args.getString("specialitySelected");
            selectedRole = args.getString("roleSelected");
            selectedYearsOfExperience = args.getString("yearSelected");
            if (selectedSpeciality != null)
                speciality.setText(selectedSpeciality);
            if (selectedRole != null)
                role.setText(selectedRole);
            if (selectedYearsOfExperience != null)
                year.setText(selectedYearsOfExperience);

        } else if (savedInstanceState != null) {

        } else {
            args = getArguments();
            specialityList = new ArrayList<String>();
            roleList = new ArrayList<String>();

            if (args != null) {
                specialityList = args.getStringArrayList(SPECIALITY);
                roleList = args.getStringArrayList(ROLE);
            }

        }
    }

    public static SetSpecDesigExpFragment getInstance(Bundle bundle) {
        SetSpecDesigExpFragment fragment = new SetSpecDesigExpFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void findView() {

        speciality = (CustomFontTextView) getView().findViewById(R.id.speciality);
        role = (CustomFontTextView) getView().findViewById(R.id.role);
        year = (CustomFontTextView) getView().findViewById(R.id.year);
        btnNext = (CustomFontButton) getView().findViewById(R.id.btnNext);
    }

    @Override
    public void initView() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            if (specialityList == null || specialityList.size() == 0) {
                specialityList = new ArrayList<String>();
                specialityList = bundle.getStringArrayList(SPECIALITY);
            }
            if (roleList == null || roleList.size() == 0) {
                roleList = new ArrayList<String>();
                roleList = bundle.getStringArrayList(ROLE);
            }
        }

        List<String> temp = new ArrayList<>();
        yearList = new ArrayList<String>();
        temp = Arrays.asList(getResources().getStringArray(R.array.year_of_experience));
        for (String s : temp) {
            yearList.add(s);
        }


    }

    @Override
    public void bindView() {
        getView().findViewById(R.id.speciality_drop_down).setOnClickListener(this);
        getView().findViewById(R.id.role_drop_down).setOnClickListener(this);
        getView().findViewById(R.id.years_drop_down).setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.speciality_drop_down:
                if (alertDialog_Speciality == null)
                    alertDialog_Speciality = createSpecialityDialog(specialityList, speciality.getText().toString(), getString(R.string.speciality));

                showSpecialityDialog(specialityList, speciality.getText().toString(), getString(R.string.speciality), alertDialog_Speciality);
                break;
            case R.id.role_drop_down:
                if (alertDialog_Role == null)
                    alertDialog_Role = createSpecialityDialog(roleList, role.getText().toString(), getString(R.string.role));

                showSpecialityDialog(roleList, role.getText().toString(), getString(R.string.role), alertDialog_Role);
                break;
            case R.id.years_drop_down:
                if (alertDialog_Years == null)
                    alertDialog_Years = createSpecialityDialog(yearList, role.getText().toString(), getString(R.string.years_of_exp));

                showSpecialityDialog(yearList, role.getText().toString(), getString(R.string.years_of_exp), alertDialog_Years);
                break;
            case R.id.btnNext:
                sendToNextScreen();
                break;
        }
    }

    private void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void sendToNextScreen() {


        selectedSpeciality = SpecialityEnum.getCodeFromValue(speciality.getText().toString().trim());
        selectedRole = DesignationEnum.getCodeFromValue(role.getText().toString().trim());
        selectedYearsOfExperience = ExperienceEnum.getCodeFromValue(year.getText().toString().trim());

        if (!selectedSpeciality.equals("")) {
            if (!selectedRole.equals("")) {
                if (!selectedYearsOfExperience.equals("")) {
                    launchProfilePictureFragment();
                } else
                    showToast(getString(R.string.blank_years_of_exp));
            } else
                showToast(getString(R.string.blank_role));
        } else
            showToast(getString(R.string.blank_speciality));

    }

    private void launchProfilePictureFragment() {

        ((CreatePhysicianProfile) getActivity()).setSpecialityDesignationYearsAndLaunchProfilePictureFragments(selectedSpeciality, selectedRole, selectedYearsOfExperience);

    }


    private AlertDialog createSpecialityDialog(final ArrayList<String> list, final String selected, final String title) {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        final String[] itemSelect = new String[1];
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View v = layoutInflater.inflate(R.layout.speciality_dialog_ui, null, false);
        alertDialog.setView(v);
        alertDialog.setCancelable(false);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.title);
        CustomFontButton btnCancel = (CustomFontButton) v.findViewById(R.id.cancel);
        CustomFontButton done = (CustomFontButton) v.findViewById(R.id.done);
        tvTitle.setText(title);
        RadioGroup radioGroup = (RadioGroup) v.findViewById(R.id.rgp_spec);
        for (int i = 0; i < list.size(); i++) {
            RadioButton radioButton = new RadioButton(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(params);
            radioButton.setText(list.get(i));
            radioButton.setId(i);
            radioButton.setPadding(30, 30, 5, 30);
            radioButton.setTextSize(14);
            radioButton.setTextColor(R.color.dusky_blue);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), getActivity().getResources().getString(R.string.opensans_ttf_regular));
            radioButton.setTypeface(font);
            radioGroup.addView(radioButton);

        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                itemSelect[0] = list.get(checkedId).toString();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.equals(getString(R.string.speciality))) {
                    selectedSpeciality = itemSelect[0];
                    speciality.setText(selectedSpeciality);
                } else if (title.equals(getString(R.string.role))) {
                    selectedRole = itemSelect[0];
                    role.setText(selectedRole);
                } else {
                    selectedYearsOfExperience = itemSelect[0];
                    year.setText(selectedYearsOfExperience);
                }
                alertDialog.dismiss();
            }
        });
        return alertDialog;
    }


    private void showSpecialityDialog(final ArrayList<String> list, final String selected, final String title, AlertDialog alertDialog) {
        if (alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();
            DisplayMetrics display = getResources().getDisplayMetrics();
            int screenWidth = display.widthPixels;
            int screenHeight = display.heightPixels;
            if (title.equals(getString(R.string.role)))
                alertDialog.getWindow().setLayout((int) (screenWidth * 0.90f), (int) (screenHeight * 0.80f));
            else
                alertDialog.getWindow().setLayout((int) (screenWidth * 0.90f), (int) (screenHeight * 0.80f));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Bundle args = new Bundle();
        getActivity().setTheme(AppStyle_themeWhite);
        args.putString("origin", SetSpecDesigExpFragment.class.getName());
        if (BuildConfig.isPatientApp) {
            ((CreateProfileActivity) getActivity()).saveMainFragmentState(null);
            ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
        } else {
            ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(null);
            ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(args);

        }
    }

}
