package com.mphrx.fisike.createUserProfile.VolleyRequest;

/**
 * Created by Neha on 14-07-2016.
 */
public class SignUpReqGsonPhysician {
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNo;
    private String email;
    private String address;
    private String speciality;
    private String role;
    private String noOfExp;
    private String otp;
    private String deviceUID;
    private String userType;
    private String alternateContact;
    private String imageName;
    private String imageBase64;
    private boolean disableOtherAccount;

    public boolean getDisableOtherAccount() {
        return disableOtherAccount;
    }

    public void setDisableOtherAccount(boolean disableOtherAccount) {
        this.disableOtherAccount = disableOtherAccount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNoOfExp() {
        return noOfExp;
    }

    public void setNoOfExp(String noOfExp) {
        this.noOfExp = noOfExp;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getDeviceUID() {
        return deviceUID;
    }

    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public void setAlternateContact(String alternateContact) {
        this.alternateContact = alternateContact;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }
}
