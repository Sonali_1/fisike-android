package com.mphrx.fisike.createUserProfile.fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.customview.SpinnerWheelView;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Neha on 05-05-2016.
 */
public class SetHeightFragment extends BaseFragment {

    CustomFontButton btnNext, btnSkip;
    String heightUnit;
    double heightValue;
    CustomFontTextView feet_unit, feet_value, inches_unit, inches_value, value_type;
    String value;
    private String gender;
    ArrayList<String> item;
    ArrayList<String> itemInCM;
    SpinnerWheelView spwheel_height, spwheel_height_cm;
    String ft, inch, cm;
    String feet_symbol, inch_symbol;
    private String[] list_decimal;


    public static SetHeightFragment getInstance(Bundle bundle) {
        SetHeightFragment fragment = new SetHeightFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.setheight_layout;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        gender = getActivity().getResources().getString(R.string.gender_male).toLowerCase();

        Bundle args = ((CreateProfileActivity) getActivity()).getSavedMainFragmentState();
        //to do -implement logic from back press
        if (args != null) {
            heightUnit = args.getString("unit_type_height");


            if (heightUnit == null) {
                setFeetInchesSpinnerWheel("4.0");
                return;
            }

            heightValue = Float.parseFloat(args.getString("height"));

            if (heightValue == 0) {
                setFeetInchesSpinnerWheel(args.getString("actual_value_height"));
            } else {
                if (heightUnit.equals(getResources().getString(R.string.feet_inch)))
                    setFeetInchesSpinnerWheel(args.getString("actual_value_height"));
                else {
/*
                    String tempVal = df.format(Float.parseFloat(args.getString("actual_value_height")));
*/
                    value = args.getString("actual_value_height");
                    setCMSpinnerWheel(value);
                }
            }

        } else if (savedInstanceState != null) {

        } else {
            args = getArguments();
        }
    }

    @Override
    public void findView() {
        feet_value = (CustomFontTextView) getView().findViewById(R.id.tv_ft);
        feet_unit = (CustomFontTextView) getView().findViewById(R.id.tv_ft_unit);
        inches_value = (CustomFontTextView) getView().findViewById(R.id.tv_in);
        inches_unit = (CustomFontTextView) getView().findViewById(R.id.tv_in_unit);
        value_type = (CustomFontTextView) getView().findViewById(R.id.unit_type);
        btnNext = (CustomFontButton) getView().findViewById(R.id.btnNext);
        btnSkip = (CustomFontButton) getView().findViewById(R.id.btnSkip);
        spwheel_height = (SpinnerWheelView) getView().findViewById(R.id.spwheel_height);
        spwheel_height_cm = (SpinnerWheelView) getView().findViewById(R.id.spwheel_height_inches);
    }

    void setFeetInchesSpinnerWheel(String value) {
        int elementPosition;
        for (int i = 0; i < 10; i = i + 1) {
            for (String f : list_decimal) {
                String sum = i + "" + f;
                if (!(i == 0 && f.equals(".0"))) {
                    String itemValue = "" + sum;
                    String ft_inch_array[] = itemValue.split("\\.");
                    itemValue = "" + ft_inch_array[0] + feet_symbol;
                    if (ft_inch_array.length >= 2)
                        itemValue = itemValue + ft_inch_array[1] + inch_symbol;
                    item.add(itemValue);

                }
            }
        }
        spwheel_height.setItems(item);
        spwheel_height.setAdditionCenterMark("");
        heightUnit = getString(R.string.feet_inch);
        String tempValue = value;
        String ft_inch_array1[] = tempValue.split("\\.");
        tempValue = "" + ft_inch_array1[0] + feet_symbol;
        if (ft_inch_array1.length >= 2)
            tempValue = tempValue + ft_inch_array1[1] + inch_symbol;
        elementPosition = item.indexOf(tempValue);
        spwheel_height.selectIndex(elementPosition);
        inches_unit.setVisibility(View.VISIBLE);
        inches_value.setVisibility(View.VISIBLE);
        spwheel_height_cm.setVisibility(View.GONE);
        spwheel_height.setVisibility(View.VISIBLE);
        setValueForUser(elementPosition);
        value_type.setText(heightUnit);

    }

    void setCMSpinnerWheel(String value) {
        int elementPosition;
        Locale locale  = new Locale("en", "US");
        String pattern = "#.#";
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        df.applyPattern(pattern);


        for (float i1 = 1f; i1 < 300; i1 = i1 + 1)
            itemInCM.add("" + df.format(i1));

        inches_value.setVisibility(View.GONE);
        inches_unit.setVisibility(View.GONE);
        spwheel_height.setVisibility(View.GONE);
        spwheel_height_cm.setVisibility(View.VISIBLE);
        spwheel_height_cm.setItems(itemInCM);
        spwheel_height_cm.setAdditionCenterMark("");
        heightUnit = getString(R.string.CM);
/*        this.value = Float.parseFloat(df.format(value));*/
        elementPosition = itemInCM.indexOf(value);
        spwheel_height_cm.selectIndex(elementPosition);
        setValueForUser(elementPosition);
        value_type.setText(heightUnit);
    }

    @Override
    public void initView() {

        list_decimal = getActivity().getResources().getStringArray(R.array.decimal_numbers_array);

        feet_symbol = getString(R.string.feet_symbol);
        inch_symbol = getString(R.string.inch_symbol);

        gender = getArguments().getString(TextConstants.GENDER);
        if (gender == null || gender.equals(""))
            gender = getActivity().getResources().getString(R.string.gender_male).toLowerCase();

        item = new ArrayList<>();
        itemInCM = new ArrayList<>();

        spwheel_height.setOnWheelItemSelectedListener(new SpinnerWheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(SpinnerWheelView wheelView, int position) {

                //value=Float.parseFloat(item.get(position));
                setValueForUser(position);
            }

            @Override
            public void onWheelItemChanged(SpinnerWheelView wheelView, int position) {
                //value=Float.parseFloat(item.get(position));
                setValueForUser(position);
            }
        });


        spwheel_height_cm.setOnWheelItemSelectedListener(new SpinnerWheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(SpinnerWheelView wheelView, int position) {

                //value=Float.parseFloat(item.get(position));
                setValueForUser(position);
            }

            @Override
            public void onWheelItemChanged(SpinnerWheelView wheelView, int position) {
                //value=Float.parseFloat(item.get(position));
                setValueForUser(position);
            }
        });

        setFeetInchesSpinnerWheel("4.0");

    }


    private void setValueForUser(int position) {
        if (heightUnit.equals(getString(R.string.feet_inch))) {
            String value_Generated = item.get(position);
            if (value_Generated.length() > 2)
                value_Generated = value_Generated.replaceAll("\\" + feet_symbol, "\\.");
            else
                value_Generated = value_Generated.replaceAll("\\" + feet_symbol, "");

            value_Generated = value_Generated.replaceAll("\\" + inch_symbol, "");
            //value = Float.parseFloat(value_Generated);
            value = value_Generated;
            setValueInFeetAndInches(value_Generated);
        } else {
            value = /*Float.parseFloat(*/itemInCM.get(position);
            setValueCM(value);
        }

    }

    private void setValueCM(String value) {
        feet_value.setText(value + "");
        feet_unit.setText(heightUnit);
        inches_unit.setVisibility(View.GONE);
        inches_value.setVisibility(View.GONE);
    }

    private void setValueInFeetAndInches(String value) {
        String valueInString = value + "";
        valueInString = valueInString.replaceAll("\\" + feet_symbol, "\\.");
        String[] val = valueInString.split("\\.");
        feet_value.setText(val[0]);
        if (val.length > 1)
            inches_value.setText(val[1].replace(getString(R.string.inch_symbol), ""));
        else
            inches_value.setText(0);
        feet_unit.setText(getActivity().getResources().getString(R.string.ft));
        inches_unit.setText(getActivity().getResources().getString(R.string.in));
    }

    @Override
    public void bindView() {

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Height h = new Height();
                if (heightUnit.equals(getResources().getString(R.string.feet_inch))) {
                    convertFeetInchesToCM(Float.parseFloat(feet_value.getText().toString()), Float.parseFloat(inches_value.getText().toString()));
                } else {
                    cm = feet_value.getText().toString();
                }

                h.setValue("" + Float.parseFloat(cm));
                h.setUnit(getResources().getString(R.string.CM));
                ((CreateProfileActivity) getActivity()).setHeightAndOpenWeightFragment(h, heightUnit, value);
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateProfileActivity) getActivity()).setHeightAndOpenWeightFragment(null, heightUnit, value);
            }
        });

        value_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenuCRUD(R.menu.menu_height_options, v);

            }
        });

    }

/*
   private  void setImageForUser(float value)
   {
       if(gender.equalsIgnoreCase("male"))
           setMaleImage(value);

       else
           setFemaleImage(value);

   }
*/

/*
    private void setMaleImage(float value) {
        if(value<4)
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.man_1));

        else if(value>6)
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.man_3));

        else
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.man_2));


    }


    private void setFemaleImage(float value) {
        if(value<4)
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.woman_1));

        else if(value>6)
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.woman_2));

        else
            heightIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.woman_3));


    }
*/

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Bundle args = new Bundle();
        args.putString("origin", SetHeightFragment.class.getName());
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
    }


    public void showPopupMenuCRUD(final int menuId, View view) {
        final PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.option_height_ft:
                        setFeetInchesSpinnerWheel("4.0");
                        break;

                    case R.id.option_height_cm:
                        setCMSpinnerWheel("150");
                        break;
                }

                popup.dismiss();
                return true;
            }
        });


        popup.show();//showing popup menu
    }


    private void convertFeetInchesToCM(float ft, float in) {
        float totalInches = ft * 12 + in;
        cm = (2.54f * totalInches) + "";
    }

}
