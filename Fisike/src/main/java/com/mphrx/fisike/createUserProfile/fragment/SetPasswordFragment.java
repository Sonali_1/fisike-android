package com.mphrx.fisike.createUserProfile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.SettingWebView;
import com.mphrx.fisike.background.changePasswordBG;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.CreatePhysicianProfile;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.SharedPref;

/**
 * Created by Neha on 18-04-2016.
 */
public class SetPasswordFragment extends BaseFragment {

    LinearLayout ll_container_pass;
    CustomFontButton btn_forget_password;
    private static String password;
    public String getPassword() {
        return password;
    }

    private CustomEditTextWhite etPassword;
    private String mDefaultPassword;

    public OnGetTransactionIdListener mListener;

    public interface OnGetTransactionIdListener {

        public void setPassword(String password);
    }

    public static SetPasswordFragment getInstance(Bundle bundle) {
        SetPasswordFragment fragment = new SetPasswordFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_set_pass;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args;


        if (BuildConfig.isPatientApp && (getActivity() instanceof CreateProfileActivity))
            args = ((CreateProfileActivity) getActivity()).getSavedMainFragmentState();
        else if (BuildConfig.isPhysicianApp && getActivity() instanceof CreatePhysicianProfile)
            args = ((CreatePhysicianProfile) getActivity()).getSavedMainFragmentState();
        else
            args = null;

        if (args != null && !SharedPref.getAdminCreatedPasswordNeverChanged()) {
            etPassword.setText(args.getString("Password"));

        } else if (savedInstanceState != null) {

        } else {
            args = getArguments();
            if (args != null)
                mDefaultPassword = args.getString("defaultpass");
        }
    }

    @Override
    public void findView() {
        ll_container_pass = (LinearLayout) getView().findViewById(R.id.ll_container_pass);
        btn_forget_password = (CustomFontButton) getView().findViewById(R.id.btn_forget_password);
        etPassword = (CustomEditTextWhite) getView().findViewById(R.id.et_pass);
    }

    @Override
    public void initView() {

        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        etPassword.setPasswordKeyListener();
        etPassword.setCursorVisible(true);
        etPassword.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        showSetPasswordView();

    }

    @Override
    public void bindView() {

        getView().findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit(v);
            }
        });


    }


    public void submit(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                next();
                break;
        }
    }

    private void showSetPasswordView() {
        btn_forget_password.setVisibility(View.GONE);
        ll_container_pass.setVisibility(View.VISIBLE);
    }

    private void next() {
        Utils.hideKeyboard(getActivity());
        password = etPassword.getText().toString().trim();

        String passError;

        if (password.equals(""))
            passError = getActivity().getResources().getString(R.string.please_enter_password);
        else
            passError = Utils.validatePassword(password, getActivity());

        if (!passError.equals("")) {
            etPassword.setError(passError);
            return;
        }

         /* MERGED ASTHA"S CODE edited by Aastha*/
        /* end of edited*/

        if (SharedPref.getAdminCreatedPasswordNeverChanged()) {
            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                mListener.setPassword(password);
                new changePasswordBG(true, mDefaultPassword, password, getActivity()).execute();
            }
        } else if (BuildConfig.isPatientApp)
            ((CreateProfileActivity) getActivity()).setPasswordAndOpenUserNameFragment(password);
        else
            ((CreatePhysicianProfile) getActivity()).setPasswordAndOpenUserNameFragment(password);

    }

    private void showError(String s) {

        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            if (activity instanceof LoginActivity)
                mListener = (OnGetTransactionIdListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Bundle args = new Bundle();
        args.putString("origin", SetPasswordFragment.class.getName());

        if (!SharedPref.getAdminCreatedPasswordNeverChanged()) {
            if (BuildConfig.isPatientApp) {
                ((CreateProfileActivity) getActivity()).saveMainFragmentState(null);
                ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
            } else {
                ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(null);
                ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(args);

            }
        }
    }
}
