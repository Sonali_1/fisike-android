package com.mphrx.fisike.createUserProfile.VolleyResponse;

import com.android.volley.VolleyError;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by Neha on 12-05-2016.
 */
public class SignUpResponse extends BaseResponse {

    com.mphrx.fisike.gson.response.SignUpResponse responseGson;

    public com.mphrx.fisike.gson.response.SignUpResponse getResponseGson() {
        return responseGson;
    }

    public SignUpResponse(VolleyError error, long mTransactionId) {
        super(error, mTransactionId);
    }

    public SignUpResponse(JSONObject response, long mTransactionId) {

        super(response, mTransactionId);
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.toString(), com.mphrx.fisike.gson.response.SignUpResponse.class);
        responseGson=((com.mphrx.fisike.gson.response.SignUpResponse) jsonToObjectMapper);
    }
}
