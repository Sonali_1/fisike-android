package com.mphrx.fisike.createUserProfile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import login.activity.LoginActivity;

public class ContactAdminActivity extends NetworkActivity implements View.OnClickListener {

    private CustomFontButton BtnEmailNow, BtnNoThanks;
    CustomFontTextView tvContactAdmin;
    private IconTextView imgHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_admin);
        findViews();
        bindView();
        initView();
    }

    private void initView() {
        BtnEmailNow.setText(getResources().getString(R.string.email_now));
        if (BuildConfig.isIcomoonForSplashEnabled)
        {
            if(BuildConfig.isPatientApp)
            {imgHeader.setText(R.string.fa_fisike_icon);
                imgHeader.setTextSize(100f);}
            else
            {imgHeader.setText(R.string.fa_fisike_spec_icon);
                         imgHeader.setTextSize(70f);}
        }
        else
        {
                imgHeader.setBackgroundResource(R.drawable.fisike_icon);
            imgHeader.setText("");
            imgHeader.setTextSize(0);
        }
    }

	@Override
	public void fetchInitialDataFromServer() {

	}

	@Override
	public void fetchInitialDataFromCache(String cacheResponse) {

	}

    private void findViews() {
        BtnEmailNow = (CustomFontButton) findViewById(R.id.btn_email_now);

        BtnNoThanks = (CustomFontButton) findViewById(R.id.no_thanks_btn);
        tvContactAdmin = (CustomFontTextView) findViewById(R.id.tv_contact_admin);
        imgHeader=(IconTextView)findViewById(R.id.img_header);
        tvContactAdmin.setText(getString(R.string.contact_admin, getString(R.string.app_name)));
    }

    private void bindView() {

        BtnEmailNow.setOnClickListener(this);
        BtnNoThanks.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_email_now:
                openEmail();
                break;
            case R.id.no_thanks_btn:
                openActivity(LoginActivity.class, null, true);
                break;
        }
    }

    private void openEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",
                CustomizedTextConstants.EMAIL_TO, null));
        emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.admin_mail_body)+" "+createMailBody());
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.admin_mail_subject,
                createMailHeader()));
        startActivity(Intent.createChooser(emailIntent, "Send email"));
    }

    private String createMailHeader() {
        if (SharedPref.getIsUserNameEnabled()) {
            return getString(R.string.username_txt_email) ;
        } else if (SharedPref.getIsMobileEnabled()) {
            return getString(R.string.mobile_number);
        } else if(SharedPref.getIsEmailEnabled()){
            return getString(R.string.email) ;
        }
        return getString(R.string.username_txt_email);
    }
    private String createMailBody() {
        if (SharedPref.getIsUserNameEnabled()) {
            return getString(R.string.username_txt_email) + " " + SharedPref.getLoginUserName() + "\n" + Utils
                    .getEmailBodyFooterInfo(this, null) + "\n" + getString(R.string.regards);
        } else if (SharedPref.getIsMobileEnabled()) {
            return getString(R.string.mobile_number) + " " + SharedPref.getMobileNumber() + "\n" + Utils
                    .getEmailBodyFooterInfo(this, null) + "\n" + getString(R.string.regards);
        } else if(SharedPref.getIsEmailEnabled()){
            return getString(R.string.email) + " " + SharedPref.getEmailAddress() + "\n" + Utils
                    .getEmailBodyFooterInfo(this, null) + "\n" + getString(R.string.regards);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        openActivity(LoginActivity.class, null, true);
    }
}
