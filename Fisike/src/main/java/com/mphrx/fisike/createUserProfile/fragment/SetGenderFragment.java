
package com.mphrx.fisike.createUserProfile.fragment;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike_physician.fragment.BaseFragment;

/**
 * Created by Neha on 05-05-2016.
 */
public class SetGenderFragment extends BaseFragment {


    ImageView ib_male, ib_female;
    ImageView rl_idont_know;
    private String gender;
    private CustomFontButton btnNext;
    private boolean hasSelectedFemale = false, hasSelectedMale = false, hasSelectedIdontKnow = false;


    public static SetGenderFragment getInstance(Bundle bundle) {
        SetGenderFragment fragment = new SetGenderFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_signup_gender;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = ((CreateProfileActivity) getActivity()).getSavedMainFragmentState();
        hasSelectedMale = false;
        hasSelectedFemale = false;
        hasSelectedIdontKnow = false;
        if (args != null) {
            gender = args.getString(TextConstants.GENDER);
            if ((gender != null) && gender.equals(getActivity().getResources().getString(R.string.gender_male_key))) {
                setBackGroundImages(true, false, false);
            } else if ((gender != null) && gender.equals(getActivity().getResources().getString(R.string.gender_female_key))) {
                setBackGroundImages(false, true, false);
            } else if ((gender != null) && gender.equals("")) {
                setBackGroundImages(false, false, true);
            }


        } else if (savedInstanceState != null) {

        } else {
            args = getArguments();
        }
    }

    @Override
    public void findView() {

        ib_male = (ImageView) getView().findViewById(R.id.ib_male);
        ib_female = (ImageView) getView().findViewById(R.id.ib_female);
        rl_idont_know = (ImageView) getView().findViewById(R.id.ib_idont_want_to_tell);
        btnNext = (CustomFontButton) getView().findViewById(R.id.btnNext);
    }

    @Override
    public void initView() {

    }

    @Override
    public void bindView() {

        ib_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackGroundImages(true, false, false);
            }
        });

        ib_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackGroundImages(false, true, false);
            }
        });

        rl_idont_know.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackGroundImages(false, false, true);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasSelectedMale && !hasSelectedFemale && !hasSelectedIdontKnow)
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Please_select_an_option), Toast.LENGTH_SHORT).show();
                else
                    ((CreateProfileActivity) getActivity()).setGenderAndOpenHeightFragment(gender);
            }
        });


    }


    private void setBackGroundImages(boolean male, boolean female, boolean iDontWantToTell) {
        //setting image for male
        setMaleGender(male);
        //setting image for female
        setFemaleGender(female);
        //setting image for idontknow
        setIdontKnowGender(iDontWantToTell);


    }

    private void setMaleGender(boolean isMale) {
        if (isMale && !hasSelectedMale) {
            ib_male.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.boy_gender_green));
            gender = getActivity().getResources().getString(R.string.Male);
            hasSelectedMale = true;
        } else {
            hasSelectedMale = false;
            ib_male.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.boy_gender_normal));
        }
    }

    private void setFemaleGender(boolean isFemale) {
        if (isFemale && !hasSelectedFemale) {
            ib_female.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.girl_gender_green));
            gender = getActivity().getResources().getString(R.string.Female);
            hasSelectedFemale = true;
        } else {
            ib_female.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.girl_gender_nor));
            hasSelectedFemale = false;
        }
    }

    private void setIdontKnowGender(boolean isdontKnow) {
        if (isdontKnow && !hasSelectedIdontKnow) {
            rl_idont_know.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.i_don_wan_green));
            gender = "";
            hasSelectedIdontKnow = true;
        } else {
            rl_idont_know.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.i_don_wan_nor));
            hasSelectedIdontKnow = false;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(null);
        Bundle args = new Bundle();
        args.putString("origin", SetGenderFragment.class.getName());
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
    }
}
