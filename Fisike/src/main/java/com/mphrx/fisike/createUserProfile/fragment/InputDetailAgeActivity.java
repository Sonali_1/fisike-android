package com.mphrx.fisike.createUserProfile.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.numberpicker.DatePickerCustom;
import com.mphrx.fisike_physician.fragment.BaseFragment;

import java.util.Calendar;

/**
 * Created by iappstreet on 01/04/16.
 */
public class InputDetailAgeActivity extends BaseFragment {

    DatePickerCustom edDob;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public static InputDetailAgeActivity getInstance(Bundle bundle) {
        InputDetailAgeActivity fragment = new InputDetailAgeActivity();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.enter_age_detail;
    }

    public void findView() {
        edDob = (DatePickerCustom) getView().findViewById(R.id.dp_dob);
        Utils.hideKeyboard(getActivity());
    }

    @Override
    public void initView() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -18);
        edDob.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        Calendar minDate = Calendar.getInstance();
        minDate.add(Calendar.YEAR, -100);
        edDob.setMinDate(minDate.getTimeInMillis());
        edDob.setMaxDate(System.currentTimeMillis());

    }

    @Override
    public void bindView() {

        getView().findViewById(R.id.dob_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateSelected = edDob.getDateInDD_MMM_YYYY();
                int age = Utils.calculateAge(dateSelected);
                if (age >= 18)
                    ((CreateProfileActivity) getActivity()).setDobAndOpenGenderFragment(dateSelected);
                else
                    Toast.makeText(getActivity(), MyApplication.getAppContext().getResources().getString(R.string.must_18_old_above), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(null);
        Bundle args = new Bundle();
        args.putString("origin", InputDetailAgeActivity.class.getName());
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
    }
}
