package com.mphrx.fisike.createUserProfile.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.createUserProfile.CreatePhysicianProfile;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;

/**
 * Created by Neha on 05-05-2016.
 */
public class SetUserNameFragment extends BaseFragment {

    private CustomEditTextWhite firstNameEditText, lastNameEditText;
    CustomFontTextView header;
    String firstName, lastName;
    private ImageView backButton;

    public static SetUserNameFragment getInstance(Bundle bundle)
    {
        SetUserNameFragment fragment = new SetUserNameFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_profile;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args;
        if(BuildConfig.isPatientApp)
            args = ((CreateProfileActivity) getActivity()).getSavedMainFragmentState();
        else
            args = ((CreatePhysicianProfile) getActivity()).getSavedMainFragmentState();

        if(args!=null){
            firstNameEditText.setText(args.getString("firstname"));
            lastNameEditText.setText(args.getString("lastname"));
        }else{
            args=getArguments();
        }
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
        firstNameEditText = (CustomEditTextWhite) getView().findViewById(R.id.first_name);
        lastNameEditText = (CustomEditTextWhite) getView().findViewById(R.id.last_name);
        firstNameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        firstNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                    lastNameEditText.requestFocus();
                    return true;
            }

        });
    }

    @Override
    public void initView()
    {
        firstNameEditText.setCursorVisible(true);
        firstNameEditText.requestFocus();
        getActivity().getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE| WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void bindView() {

        getView().findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });



    }

    private void next()
    {
        firstName = firstNameEditText.getText().toString().trim();
        lastName = lastNameEditText.getText().toString().trim();
        if (isValidated())
        {
            Utils.hideKeyboard(getActivity());
            if(BuildConfig.isPatientApp)
                ((CreateProfileActivity)getActivity()).launchGetAgeDetailsFragment(firstName,lastName);
            else
                ((CreatePhysicianProfile)getActivity()).launchGetAgeDetailsFragment(firstName,lastName);
        }

    }

    private boolean isValidated()
    {

        if (firstName != null && !firstName.equals("")) {
            firstNameEditText.setErrorEnabled(false);
            if(lastName !=null && !lastName.equals(""))
            {
                lastNameEditText.setErrorEnabled(false);
            }
        }
        else {
            firstNameEditText.setError(getResources().getString(R.string.please_enter_first_name));
            return false;
        }
        return true;


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void setInitialSavedState(SavedState state) {
        super.setInitialSavedState(state);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Bundle args=new Bundle();
        args.putString("origin", SetUserNameFragment.class.getName());

        if(BuildConfig.isPatientApp){
                ((CreateProfileActivity) getActivity()).saveMainFragmentState(null);
                ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
        }
        else
        {
            ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(null);
            ((CreatePhysicianProfile) getActivity()).saveMainFragmentState(args);

        }
    }
}
