package com.mphrx.fisike.createUserProfile.VolleyRequest;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.verifyOTPResponse;
import com.mphrx.fisike.createUserProfile.VolleyResponse.SignUpResponse;
import com.mphrx.fisike.gson.request.SignUpGsonRequest;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

/**
 * Created by Neha on 12-05-2016.
 */
public class SignUpRequest  extends BaseObjectRequest {


    private long mTransactionId;
    SignUpGsonRequest request;
    Context context;

    public SignUpRequest(long mTransactionId, SignUpGsonRequest request, Context context)
    {
        this.mTransactionId=mTransactionId;
        this.request=request;
        this.context=context;
    }

    @Override
    public void doInBackground() {
        APIManager apiManager = APIManager.getInstance();
        String signUpUrl = apiManager.getSignUpUrl(context);
        AppLog.showInfo(getClass().getSimpleName(), signUpUrl);
        String json = new Gson().toJson(request);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, signUpUrl,json, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SignUpResponse(error, mTransactionId));

    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SignUpResponse(response, mTransactionId));
    }
}
