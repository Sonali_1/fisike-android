package com.mphrx.fisike.createUserProfile.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.SeekBar;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.customview.SpinnerWheelView;
import com.mphrx.fisike.fragments.HeaderFragment;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Neha on 05-05-2016.
 */
public class SetWeightFragment extends BaseFragment {


    CustomFontTextView weight, weightUnit, unit_type;
    float value;
    CustomFontButton btnNext, btnSkip;
    SpinnerWheelView spwheel_weight, spwheel_weight_lbs;
    private ArrayList<String> item, itemLbs;
    String unit;
    private DecimalFormat df;
    boolean hasloadedKgSpinner, hasloadedLBSSpinner;

    public static SetWeightFragment getInstance(Bundle bundle) {
        SetWeightFragment fragment = new SetWeightFragment();
        if (bundle != null)
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.setweight_layout;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = ((CreateProfileActivity) getActivity()).getSavedMainFragmentState();
        //to do -implement logic from back press

        if (args != null) {
            unit = args.getString("unit_type_weight");
            if (unit == null) {
                setKgSpinnerValue(40f);
                return;
            }
            value = Float.parseFloat(args.getString("weight"));
            DecimalFormat df = new DecimalFormat("#.#");
            String tempVal = df.format(Float.parseFloat(args.getString("actual_value_weight")));
            value = Float.parseFloat(tempVal);


            if (unit.equals(getResources().getString(R.string.LBS)))
                setLBSpinnerValue(value);
            else
                setKgSpinnerValue(value);

        } else if (savedInstanceState != null) {

        } else {
            args = getArguments();
            if (args != null) {
                item = args.getStringArrayList(TextConstants.KG_SPINNER);
                itemLbs = args.getStringArrayList(TextConstants.LBS_SPINNER);
            }
            setSpinnerValue();


        }
    }


    @Override
    public void findView() {
        weight = (CustomFontTextView) getView().findViewById(R.id.tv_wt_value);
        weightUnit = (CustomFontTextView) getView().findViewById(R.id.tv_wt_unit);
        unit_type = (CustomFontTextView) getView().findViewById(R.id.unit_type);
        btnNext = (CustomFontButton) getView().findViewById(R.id.btnNext);
        btnSkip = (CustomFontButton) getView().findViewById(R.id.btnSkip);
        spwheel_weight = (SpinnerWheelView) getView().findViewById(R.id.spwheel_weight);
        spwheel_weight_lbs = (SpinnerWheelView) getView().findViewById(R.id.spwheel_weight_lbs);
    }

    @Override
    public void initView() {


        df = new DecimalFormat("#.#");
        Bundle bundle = getArguments();
        if (bundle != null) {
            item = bundle.getStringArrayList(TextConstants.KG_SPINNER);
            itemLbs = bundle.getStringArrayList(TextConstants.LBS_SPINNER);
        }
        setSpinnerValue();
        spwheel_weight.setItems(item);
        spwheel_weight.setAdditionCenterMark("");
        setKgSpinnerValue(40f);
        spwheel_weight_lbs.setItems(itemLbs);
        spwheel_weight_lbs.setAdditionCenterMark("");

    }

    public void showPopupMenuCRUD(final int menuId, View view) {
        final PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.option_weight_kg:
                        setKgSpinnerValue(40f);
                        break;

                    case R.id.option_weight_lbs:
                        setLBSpinnerValue(40f);
                        break;
                }

                popup.dismiss();
                return true;
            }
        });


        popup.show();//showing popup menu
    }

    private void setKgSpinnerValue(float value) {

        spwheel_weight_lbs.setVisibility(View.GONE);
        spwheel_weight.setVisibility(View.VISIBLE);
        int position = item.indexOf(df.format(value));
        spwheel_weight.selectIndex(position);
        this.value = value;/*Float.parseFloat(item.get(spwheel_weight.getSelectedPosition()))*/
        ;
        weight.setText(value + "");
        unit = getResources().getString(R.string.KG);
        unit_type.setText(unit);
        weightUnit.setText(unit);
        setValueForPostition(position);
        Utils.hideKeyboard(getActivity());
    }

    private void setLBSpinnerValue(float value) {
        spwheel_weight.setVisibility(View.GONE);
        spwheel_weight_lbs.setVisibility(View.VISIBLE);
        int position = itemLbs.indexOf(df.format(value));
        spwheel_weight_lbs.selectIndex(position);
        this.value = value;/*Float.parseFloat(item.get(spwheel_weight.getSelectedPosition()))*/
        ;
        weight.setText(value + "");
        unit = getResources().getString(R.string.LBS);
        unit_type.setText(unit);
        weightUnit.setText(unit);
        setValueForPostition(position);
        Utils.hideKeyboard(getActivity());
    }


    private void setSpinnerValue() {
        float i, j;
        if (item == null || item.size() == 0) {
            item = new ArrayList<>();
            for (i = 0.1f; i < 600; )
                item.add("" + df.format(i));
            if (i < 200)
                i = i + 0.1f;
            else
                i = i + 1f;
        }
        if (itemLbs == null || itemLbs.size() == 0) {
            itemLbs = new ArrayList<>();
            for (j = 0.1f; j < 1323f; )
                itemLbs.add("" + df.format(j));
            if (j < 200)
                j = j + 0.1f;
            else
                j = j + 1f;
        }
    }

    @Override
    public void bindView() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Weight h = new Weight();
                double result;
                if (unit.equals(getResources().getString(R.string.LBS)))
                    result = value * 0.454f;
                else
                    result = value;

                Locale locale  = new Locale("en", "US");
                String pattern = "#.#";
                DecimalFormat formater = (DecimalFormat) NumberFormat.getNumberInstance(locale);
                formater.applyPattern(pattern);
                result = Double.parseDouble(formater.format(result));
                h.setValue(result + "");
                h.setUnit("Kg");
                ((CreateProfileActivity) getActivity()).setWeightAndOpenProfilePictureFragment(h, unit, value);

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateProfileActivity) getActivity()).setWeightAndOpenProfilePictureFragment(null, unit, 40);
            }
        });
        spwheel_weight.setOnWheelItemSelectedListener(new SpinnerWheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(SpinnerWheelView wheelView, int position) {

                setValueForPostition(position);
            }

            @Override
            public void onWheelItemChanged(SpinnerWheelView wheelView, int position) {
                setValueForPostition(position);
            }
        });

        spwheel_weight_lbs.setOnWheelItemSelectedListener(new SpinnerWheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(SpinnerWheelView wheelView, int position) {

                setValueForPostition(position);
            }

            @Override
            public void onWheelItemChanged(SpinnerWheelView wheelView, int position) {
                setValueForPostition(position);
            }
        });


        unit_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenuCRUD(R.menu.weight_option_menu, unit_type);
            }
        });
    }


    private void setValueForPostition(int postition) {
        if (unit.equals(getResources().getString(R.string.LBS)))
            value = Float.parseFloat(itemLbs.get(postition));
        else
            value = Float.parseFloat(item.get(postition));

        weight.setText(value + "");
        weightUnit.setText(unit);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Bundle args = new Bundle();
        args.putString("origin", SetWeightFragment.class.getName());
        ((CreateProfileActivity) getActivity()).saveMainFragmentState(args);
    }
}
