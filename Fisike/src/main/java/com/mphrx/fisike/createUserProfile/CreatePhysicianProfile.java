package com.mphrx.fisike.createUserProfile;

import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.apache.commons.codec.binary.Base64;
import com.google.gson.Gson;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.VolleyRequest.SignUpReqGsonPhysician;
import com.mphrx.fisike.createUserProfile.fragment.SetPasswordFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetProfilePictureFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetSpecDesigExpFragment;
import com.mphrx.fisike.createUserProfile.fragment.SetUserNameFragment;
import com.mphrx.fisike.imageCropping.Crop;
import com.mphrx.fisike.utils.IntentUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.ApprovalStatusActivity;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.network.request.SignUpRequest;
import com.mphrx.fisike_physician.network.request.SpecialitiesAndDesignationsRequest;
import com.mphrx.fisike_physician.network.response.SignUpResponse;
import com.mphrx.fisike_physician.network.response.SpecialitiesAndDesignationsResponse;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ExifUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class CreatePhysicianProfile extends NetworkActivity implements FragmentManager.OnBackStackChangedListener {
    protected static final int SUCESS_RESULT = 1, CAMERA_RESULT = 2, GALLERY_RESULT = 3, CROP_RESULT = 4;
    private BottomSheetDialog mBottomSheetDialog;
    private File photoUri;
    private String realPathFromURI;
    private Uri mImageCaptureUri = null;
    private boolean isCameraImage;
    private Bitmap tempBitmap;
    String firstName, otp;
    String lastName;
    String password;
    private boolean isdisableAccount;
    private Toolbar toolbar;
    SignUpReqGsonPhysician request;
    private String encodedImage;
    private String ImageName;
    private ArrayList<String> roles;
    private ArrayList<String> speciality;
    String selectedSpeciality, selectedRole, selectedYearsOfExp;
    private Bundle mMainFragmentArgs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_create_physician_profile);
        isdisableAccount = getIntent().getBooleanExtra(TextConstants.DISABLE_USER, false);
        otp = getIntent().getStringExtra(VariableConstants.OTP_CODE);
        getFragmentManager().addOnBackStackChangedListener(this);
        request = new SignUpReqGsonPhysician();
        findView();
        ThreadManager.getDefaultExecutorService().submit(new SpecialitiesAndDesignationsRequest(getTransactionId()));
        addOrInitSetPasswordFragment();
    }

    private void findView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBackPress();
            }
        });
    }

    public void handleBackPress() {
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            setGetStartedScreenView();
            Intent i = new Intent(this, OtpActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(TextConstants.PREFILL_NUM, true);
            startActivity(i);
        } else {
            getFragmentManager().popBackStack();
        }

    }

    private void setGetStartedScreenView() {
        if (BuildConfig.isPhoneEnabled && BuildConfig.isEmailEnabled) {
            SharedPref.setEmailAndMobile(true);
            SharedPref.setMobileEnabled(false);
        } else if (BuildConfig.isPhoneEnabled && !BuildConfig.isEmailEnabled) {
            SharedPref.setMobileEnabled(true);
            SharedPref.setEmailAndMobile(false);
        } else if (!BuildConfig.isPhoneEnabled && BuildConfig.isEmailEnabled) {
            SharedPref.setMobileEnabled(false);
            SharedPref.setEmailAndMobile(false);
        }
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    /**
     * Show bottom sheet for sending the attachment
     */
    public void showBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.change_image_layout, null);
        showOrHideRemoveImageMenu(view);
        float peekHeight = view.getMeasuredHeight();
        view.findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), CAMERA_RESULT))
                    takePhoto();

            }
        });

        view.findViewById(R.id.choose_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), GALLERY_RESULT))
                    openGallery();

            }
        });

        view.findViewById(R.id.remove_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                removeImage();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALLERY_RESULT:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(photoUri);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    isCameraImage = false;
                    startCropImage();
                    break;

                case CAMERA_RESULT:
                    isCameraImage = true;
                    captureAndCropImage(data);
                    break;

                case CROP_RESULT:
                    Uri croppedUri = Crop.getOutput(data);
                    try {
                        tempBitmap = ExifUtils.rotateBitmap(croppedUri.getPath().toString(), MediaStore.Images.Media.getBitmap(this.getContentResolver(), croppedUri));

                        setProfilePicture(tempBitmap);
                    } catch (IOException e) {
                        // FIXME there should atleast be a toast here ... but there wasn't one before so not adding
                    }
                    if (isCameraImage) {
                        // FIXME @Rajan probably not needed anymore
                        String croppedImagePath = realPathFromURI.toString().replace(".jpg", "~2.jpg");
                        File croppedImageFile = new File(croppedImagePath);
                        if (croppedImageFile.exists()) {
                            croppedImageFile.delete();
                        }

                        File file = new File(realPathFromURI);
                        Uri uri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        try {
                            ContentResolver resolver = this.getContentResolver();

                            // change type to image, otherwise nothing will
                            // be deleted
                            ContentValues contentValues = new ContentValues();
                            int media_type = 1;
                            contentValues.put("media_type", media_type);
                            resolver.update(Uri.parse(realPathFromURI), contentValues, null, null);
                            resolver.delete(Uri.parse(realPathFromURI), null, null);
                        } catch (Exception e) {

                        }
                        Utils.deleteFile(this, realPathFromURI, uri1, VariableConstants.ATTACHMENT_IMAGE);
                        if (file.exists()) {
                            boolean deleted = file.delete();
                        }
                    }


                    break;

                default:
                    break;
            }
        }
    }

    private void takePhoto() {
        dismissBottomSheet();
        try {
            photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    System.currentTimeMillis() + ".jpg");
            photoUri.setWritable(true);
            mImageCaptureUri = Uri.fromFile(photoUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentUtils.onClickTakePhoto(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }

    private void captureAndCropImage(Intent data) {
        if (null != photoUri && !"".equals(photoUri)) {
            mImageCaptureUri = Uri.fromFile(photoUri);
        }
        mImageCaptureUri = (null == data || null == data.getData()) ? mImageCaptureUri : data.getData();
        if (null != mImageCaptureUri) {
            realPathFromURI = Utils.getRealPathFromURI(mImageCaptureUri, this);
            if (null == realPathFromURI) {
                realPathFromURI = mImageCaptureUri.getPath();
                if (null == realPathFromURI) {
                    return;
                }
            }

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(realPathFromURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
/*
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
*/
            startCropImage();
        }
    }

    private void startCropImage() {

        DisplayMetrics display = getResources().getDisplayMetrics();
        int screenWidth = display.widthPixels;
        int screenHeight = display.heightPixels;
        int gcd = GCD(screenWidth, screenHeight);
        int aspect_x = screenHeight / gcd;
        int aspect_y = screenWidth / gcd;
        if (screenWidth > 400)
            screenWidth = 400;
        int output_x = 400;
        int output_y = 400;
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped-profile-pic"));
        startActivityForResult(Crop.of(Uri.fromFile(photoUri), destination)
                .asSquare()
                .withMaxSize(output_x, output_y)
                .getIntent(this), CROP_RESULT);
    }

    private int GCD(int a, int b) {
        return (b == 0 ? a : GCD(b, a % b));
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private void openGallery() {
        dismissBottomSheet();
        photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                System.currentTimeMillis() + ".jpg");
        photoUri.setWritable(true);
        mImageCaptureUri = Uri.fromFile(photoUri);
        IntentUtils.onClickChooseImage(this, CAMERA_RESULT, GALLERY_RESULT, photoUri, null, true, true);
    }

    public void showOrHideRemoveImageMenu(View view) {
        Utils.hideKeyboard(this);
        if (tempBitmap != null)
            view.findViewById(R.id.remove_image).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.remove_image).setVisibility(View.GONE);
    }

    /**
     * Dismiss the bottom sheet
     */
    private void dismissBottomSheet() {
        if (mBottomSheetDialog.isShowing()) {
            mBottomSheetDialog.dismiss();
        }
    }

    private void removeImage() {
        dismissBottomSheet();
        tempBitmap = null;
        setProfilePicture(null);

    }

    private void setProfilePicture(Bitmap bitmap) {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment.getClass().getName().equals(SetProfilePictureFragment.class.getName()))
            ((SetProfilePictureFragment) fragment).setProfilePicture(bitmap);
    }

    public void setPasswordAndOpenUserNameFragment(String password) {
        request.setPassword(password);
        this.password = password;
        addOrInitUserNameFragment();

    }

    public void launchGetAgeDetailsFragment(String fname, String lname) {
        this.firstName = fname;
        this.lastName = lname;
        request.setFirstName(fname);
        request.setLastName(lname);
        addOrInitSetSpecicalityDesignationFragment();

    }

    private void addOrInitSetSpecicalityDesignationFragment() {
        setTitle(getResources().getString(R.string.more_abt_u));
        setTheme(R.style.AppStyle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(SetSpecDesigExpFragment.SPECIALITY, speciality);
        bundle.putStringArrayList(SetSpecDesigExpFragment.ROLE, roles);
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);

        fragmentTransaction.replace(R.id.fragment_container, SetSpecDesigExpFragment.getInstance(bundle));
        fragmentTransaction.addToBackStack(SetSpecDesigExpFragment.class.getName());
        fragmentTransaction.commit();
    }

    public void addOrInitSetPasswordFragment() {
        setTitle(getResources().getString(R.string.set_password));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, SetPasswordFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetPasswordFragment.class.getName());
        fragmentTransaction.commit();
    }

    private void addOrInitUserNameFragment() {
        setTitle(getResources().getString(R.string.create_profile));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);

        fragmentTransaction.replace(R.id.fragment_container, SetUserNameFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetUserNameFragment.class.getName());
        fragmentTransaction.commit();
    }

    private void addOrInitProfilePictureFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter, R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter, R.animator.fragment_slide_right_exit);
        fragmentTransaction.replace(R.id.fragment_container, SetProfilePictureFragment.getInstance(null));
        fragmentTransaction.addToBackStack(SetProfilePictureFragment.class.getName());
        fragmentTransaction.commit();

    }

    public void setProfilePictureAndSendApiForSignUp(Bitmap bitmap) {
        tempBitmap = bitmap;

        if (bitmap != null) {
            encodedImage = Base64.encodeBase64String(Utils.convertBitmapToByteArray(bitmap));
            ImageName = firstName + "_" + lastName + "_" + System.currentTimeMillis();
            request.setImageBase64(encodedImage);
            request.setImageName(ImageName);
        } else {
            request.setImageBase64(null);
            request.setImageName(null);
        }

        if (Utils.showDialogForNoNetwork(this, false))
            if (checkPermission(Manifest.permission.READ_PHONE_STATE, getString(R.string.permission_msg) + " " +getString(R.string.permission_phone_state), TextConstants.PERMISSION_REQUEST_CODE))
                sendApiForSignUp();

    }

    private void sendApiForSignUp() {
        hitSignUpService();
    }

    @Override
    public void onBackStackChanged() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_container);

        if (f != null)
            updateTitle(f);
    }

    private void updateTitle(Fragment f) {
        String className = f.getClass().getName();

        if (className.equals(SetSpecDesigExpFragment.class.getName()))
            setTheme(R.style.AppStyle);
        else
            setTheme(R.style.AppStyle_themeWhite);


        if (className.equals(SetPasswordFragment.class.getName()))
            setToolbarTitle(getResources().getString(R.string.set_password));

        else if (className.equals(SetUserNameFragment.class.getName()))
            setToolbarTitle(getResources().getString(R.string.create_profile));

        else
            setToolbarTitle(getResources().getString(R.string.more_abt_u));


    }


    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Subscribe
    public void onSpecialitiesAndDesignationsResponse(SpecialitiesAndDesignationsResponse response) {
        roles = response.getDesignations();
        speciality = response.getSpecialities();

    }

    public void setSpecialityDesignationYearsAndLaunchProfilePictureFragments(String selectedSpeciality, String selectedRole, String selectedYearsOfExperience) {
        setTheme(R.style.AppStyle_themeWhite);
        this.selectedSpeciality = selectedSpeciality;
        this.selectedRole = selectedRole;
        this.selectedYearsOfExp = selectedYearsOfExperience;
        request.setSpeciality(selectedSpeciality);
        request.setRole(selectedRole);
        request.setNoOfExp(selectedYearsOfExperience.substring(0, selectedYearsOfExperience.length()-1));
        addOrInitProfilePictureFragment();
    }

    public void saveMainFragmentState(Bundle args) {
        mMainFragmentArgs = args;
    }

    public Bundle getSavedMainFragmentState() {

        if (mMainFragmentArgs != null) {
            String origin = mMainFragmentArgs.getString("origin");
            if (origin.equals(SetUserNameFragment.class.getName())) {
                mMainFragmentArgs.putString("Password", this.password);
            } else if (origin.equals(SetSpecDesigExpFragment.class.getName())) {
                mMainFragmentArgs.putString("firstname", this.firstName);
                mMainFragmentArgs.putString("lastname", this.lastName);
            } else if (origin.equals(SetProfilePictureFragment.class.getName())) {
                mMainFragmentArgs.putString("specialitySelected", this.selectedSpeciality);
                mMainFragmentArgs.putString("roleSelected", this.selectedRole);
                mMainFragmentArgs.putString("yearSelected", this.selectedYearsOfExp);
            }
            return
                    mMainFragmentArgs;
        } else
            return null;
    }

    private void hitSignUpService() {
        showProgressDialog();
        String deviceUID = SharedPref.getDeviceUid();
        request.setDeviceUID(deviceUID);
        request.setDisableOtherAccount(isdisableAccount);
        if (SharedPref.getIsMobileEnabled())
            request.setPhoneNo(SharedPref.getMobileNumber());
        else if (!SharedPref.getIsMobileEnabled())
            request.setEmail(SharedPref.getEmailAddress());
        request.setOtp(otp);
        request.setUserType(Utils.getUserType());
        String json = new Gson().toJson(request);
        ThreadManager.getDefaultExecutorService().submit(new SignUpRequest(json, getTransactionId()));
    }

    @Subscribe
    public void onSignUpResponse(SignUpResponse response) throws Exception {

        dismissProgressDialog();
        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            showMessageDialog();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void showMessageDialog() {
        DialogUtils.showAlertDialogCommon(this, getResources().getString(R.string.Thanks), getResources().getString(R.string.we_have_received_your_details), getResources().getString(R.string.btn_ok), null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    SharedPref.setUserWaitingForApproval(true);
                    openActivity(ApprovalStatusActivity.class, null, true);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_RESULT:
                    takePhoto();
                    break;
                case GALLERY_RESULT:
                    openGallery();
                    break;
                case TextConstants.PERMISSION_REQUEST_CODE:
                    sendApiForSignUp();
                    break;
            }
        } else
            onPermissionNotGranted();
    }

}


