package com.mphrx.fisike;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mphrx.fisike.adapter.SelectLanguageAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.SelectLanguageItemData;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.request.LanguagePreferenceRequest;
import com.mphrx.fisike_physician.network.response.LanguagePreferenceResponse;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.text.Bidi;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import static com.mphrx.fisike.R.attr.title;

public class LanguageSelectActivity extends BaseActivity implements SelectLanguageAdapter.clickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private RecyclerView languageRecyclerView;
    private SelectLanguageAdapter languagetAdapter;
    private RecyclerView.LayoutManager languageLayoutManager;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private ArrayList<SelectLanguageItemData> itemList;
    private FrameLayout frameLayout;
    private String languageKey = TextConstants.DEFALUT_LANGUAGE_KEY;
    private String languageTitle;
    private Dialog mProgressDialog;
    private CustomFontButton mBtn_done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initializing language title to value stored in db
        languageTitle = SharedPref.getLanguageSelected();
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_select_language, frameLayout);
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
        setValue();
    }

    private void setValue() {
        toolbar_title.setText(R.string.language);
        Set<String> listLanguage = ConfigManager.getInstance().getConfig().getLanguageList().keySet();

        String languageSelected = SharedPref.getLanguageSelected();

        languageRecyclerView = (RecyclerView) this.findViewById(R.id.my_recycler_view);
        mBtn_done = (CustomFontButton) this.findViewById(R.id.btn_done);
        mBtn_done.setText(getResources().getString(R.string.done));
        languageLayoutManager = new LinearLayoutManager(this);
        languageRecyclerView.setLayoutManager(languageLayoutManager);
        itemList = new ArrayList<SelectLanguageItemData>();
        Iterator<String> iterator = listLanguage.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            SelectLanguageItemData itemData = new SelectLanguageItemData();
            itemData.setTitle(next);
            if (languageSelected.equalsIgnoreCase(next)) {
                itemData.setSelected(true);
            }
            itemList.add(itemData);
        }

        languagetAdapter = new SelectLanguageAdapter(itemList, this);
        languagetAdapter.setClickListener(this);
        languageRecyclerView.setAdapter(languagetAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initView() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void itemClicked(View view, int position) {
        languageTitle = itemList.get(position).getTitle();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_done:
                if (Utils.showDialogForNoNetwork(this, false)) {
                    if (SharedPref.getLanguageSelected().equalsIgnoreCase(languageTitle)) {
                        finish();
                        return;
                    }
                    mProgressDialog = DialogUtils.showProgressDialog(this, getResources().getString(R.string.updating_language_progress));
                    mProgressDialog.show();
                    long transactionId = getTransactionId();
                    languageKey = Utils.getLanguageKey(this, languageTitle);
                    ThreadManager.getDefaultExecutorService().submit(new LanguagePreferenceRequest(transactionId, this, languageKey));
                }
                break;
        }
    }

    @Subscribe
    public void onLanguagePreferenceResponse(LanguagePreferenceResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful()) {
            if (response.getHttpStatusCodes() == 200 && response.getStatus() == 200 && response.getSuccessfullyChanged().equals("Successful")) {
                Utils.setLocalLanguage(this, languageKey, languageTitle);
                onBackPressed();
            } else {
                Toast.makeText(this, this.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SharedPref.LANGUAGE_SELECTED)) {
            languageTitle = SharedPref.getLanguageSelected();
            languageKey = SharedPref.getLanguageKey();
            setValue();
        }
    }

    @Override
    public void onBackPressed() {
        if (BuildConfig.isPatientApp) {
            setResult(RESULT_OK, new Intent().putExtra(VariableConstants.RELOAD_SETTINGS, true));
            finish();
            return;
        }
        super.onBackPressed();
    }

}
