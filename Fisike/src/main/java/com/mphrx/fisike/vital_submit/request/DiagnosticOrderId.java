package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class DiagnosticOrderId implements Serializable {
	private static final long serialVersionUID = 2678465280486244538L;
	@Expose
	private String diagnosticOrderID;

	public String getDiagnosticOrderID() {
		return diagnosticOrderID;
	}

	public void setDiagnosticOrderID(String diagnosticOrderID) {
		this.diagnosticOrderID = diagnosticOrderID;
	}

}
