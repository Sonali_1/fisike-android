package com.mphrx.fisike.vital_submit.background;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike.vital_submit.activity.EnterObservationActivity;
import com.mphrx.fisike.vital_submit.request.DiagnosticIdentifier;
import com.mphrx.fisike.vital_submit.request.DiagnosticObservationCode;
import com.mphrx.fisike.vital_submit.request.DiagnosticObservations;
import com.mphrx.fisike.vital_submit.request.DiagnosticSubject;
import com.mphrx.fisike.vital_submit.request.DiagnosticType;
import com.mphrx.fisike.vital_submit.request.ObservationGSON;
import com.mphrx.fisike.vital_submit.request.ObservationValue;
import com.mphrx.fisike.vital_submit.request.ObservationValueUnit;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

import java.util.ArrayList;

public class ObservationSumitTask extends AsyncTask<Void, Void, Boolean> {


    private ProgressDialog progressDialog;
    private Context context;
    private int patientId;
    private String date;
    private String observationType;
    private ArrayList<ObservationValueUnit> observationArray;
    private ArrayList<ObservationValue> responseObservationValue;
    private ArrayList<DiagnosticObservations> observations = new ArrayList<>();
    private Float number;
    private String responseObservationStatus;
    boolean isshowprogressdialog = true;
    String convertedUnit, convertedValue, highRange, lowRange;
    VitalsConfigMO vitalsConfigMo;

    public ObservationSumitTask(Context context, int patientId, String date, String observationType, ArrayList<ObservationValueUnit> observationArray, Float number) {
        this.context = context;
        this.patientId = patientId;
        this.date = date;
        this.observationType = observationType;
        this.observationArray = observationArray;
        this.number = number;
        try {
            vitalsConfigMo = VitalsConfigDBAdapter.getInstance(context).fetchVitalsConfigMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isshowprogressdialog)
            initProgressDialog();
    }

    public ObservationSumitTask(Context context, int patientId, String date, String observationType, ArrayList<ObservationValueUnit> observationArray, Float number, boolean isshowprogressdialog) {
        this.context = context;
        this.patientId = patientId;
        this.date = date;
        this.observationType = observationType;
        this.observationArray = observationArray;
        this.number = number;
        this.isshowprogressdialog = isshowprogressdialog;
        if (isshowprogressdialog)
            initProgressDialog();
        try {
            vitalsConfigMo = VitalsConfigDBAdapter.getInstance(context).fetchVitalsConfigMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        if (isshowprogressdialog && !progressDialog.isShowing())
            progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        APIManager apiManager = APIManager.getInstance();
        String otpUrl = apiManager.getObservationApi(context);

        ObservationGSON observationGSON = new ObservationGSON();


       /* observations = new ArrayList<DiagnosticObservations>();
        responseObservationValue = new ArrayList<>();*/

        if (observationType.equalsIgnoreCase(vitalsConfigMo.getBMI().getName())) {
            observations = setBMIParameter();
        } else {
            observations = setObservationParameter();
        }

        observationGSON.setObservations(observations);
        DiagnosticSubject subject = new DiagnosticSubject();
        subject.setId(patientId);
        observationGSON.setSubject(subject);
        observationGSON.setTestName(observationType);
        observationGSON.setCategory("Vital");
        observationGSON.setIssuedDate(date);
        observationGSON.setSource("Self");

        Gson gson = new Gson();
        String json = gson.toJson(observationGSON);

        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        //SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
        String authToken = SharedPref.getAccessToken();
        String executeHttpPost = apiManager.executeHttpPost(otpUrl, jsonObj, authToken, context);

        if (executeHttpPost == null) {
            return false;
        } else {
            JSONObject success = null;
            Boolean successful = false;
            try {
                success = new JSONObject(executeHttpPost);
                successful = success.getBoolean("success");
                JSONObject info = success.optJSONObject("info");
                JSONObject bmi = info.optJSONObject("BMI");


                if (bmi != null) {
                    if (bmi.optJSONObject("referenceRange") != null && bmi.optJSONObject("referenceRange").optJSONObject("high") != null) {
                        highRange = bmi.optJSONObject("referenceRange").optJSONObject("high").optString("value");
                    }
                    if (bmi.optJSONObject("referenceRange") != null && bmi.optJSONObject("referenceRange").optJSONObject("low") != null) {
                        lowRange = bmi.optJSONObject("referenceRange").optJSONObject("low").optString("value");
                    }
                    convertedUnit = bmi.optString("convertedUnit");
                    convertedValue = bmi.optString("convertedValue");
                }

                return successful;
            } catch (Exception e) {
                return successful;
            }
        }
    }

    public void setResponseObservationStatus(String responseObservationStatus) {
        if (responseObservationStatus.equals("L")) {
            if (this.responseObservationStatus == null) {
                responseObservationStatus = TextConstants.LOW;
            } else if (this.responseObservationStatus.equals(TextConstants.NORMAL) || this.responseObservationStatus.equals(TextConstants.ELEVATED)) {
                responseObservationStatus = TextConstants.OUT_OF_RANGE;
            } else {
                responseObservationStatus = TextConstants.LOW;
            }
        } else if (responseObservationStatus.equals(TextConstants.NORMAL)) {
            if (this.responseObservationStatus == null) {
                responseObservationStatus = TextConstants.NORMAL;
            } else if (this.responseObservationStatus.equals(TextConstants.LOW) || this.responseObservationStatus.equals(TextConstants.ELEVATED)) {
                responseObservationStatus = TextConstants.OUT_OF_RANGE;
            } else {
                responseObservationStatus = TextConstants.NORMAL;
            }
        } else {
            if (this.responseObservationStatus == null) {
                responseObservationStatus = TextConstants.ELEVATED;
            } else if (this.responseObservationStatus.equals(TextConstants.LOW) || this.responseObservationStatus.equals(TextConstants.NORMAL)) {
                responseObservationStatus = TextConstants.OUT_OF_RANGE;
            } else {
                responseObservationStatus = TextConstants.ELEVATED;
            }
        }
        this.responseObservationStatus = responseObservationStatus;
    }

    private ArrayList<DiagnosticObservations> setObservationParameter() {
        observations.clear();
        Resources resources = context.getResources();
        for (int i = 0; i < observationArray.size(); i++) {

            DiagnosticObservations diagnosticObservations = new DiagnosticObservations();

            //code
            DiagnosticObservationCode name = new DiagnosticObservationCode();
            // name.setText(observationType);
            name.setSystem("loinc"); //will be fetched from json config

            ArrayList<DiagnosticIdentifier> identifier = new ArrayList<>();
            DiagnosticIdentifier observationIdentifier = new DiagnosticIdentifier();
            DiagnosticType type = new DiagnosticType();
            type.setText("paramId");
            observationIdentifier.setType(type);
            observationIdentifier.setUseCode("usual");

            ObservationValueUnit observationValueUnit = observationArray.get(i);

            /*ObservationValue obsValue = new ObservationValue();*/
            //adding Glucose
            if (vitalsConfigMo.isToShowAddGlucoseButton()) {
                if (observationValueUnit.getObservationCat().equalsIgnoreCase(vitalsConfigMo
                        .getGlucose().getGlucoseRandom().getObservationName())) {
                    observationIdentifier.setValue(TextConstants.CODE_GLUCOSE_RANDOM);
                    //obsValue.setObservationCategory(observationValueUnit.getObservationCat());
                    name.setValue(TextConstants.CODE_GLUCOSE_RANDOM);
                    name.setText(vitalsConfigMo.getGlucose().getGlucoseRandom().getObservationName());

                } else if (observationValueUnit.getObservationCat().equalsIgnoreCase(
                        vitalsConfigMo.getGlucose().getGlucoseFasting().getObservationName())) {
                    observationIdentifier.setValue(TextConstants.CODE_GLUCOSE_FASTING);
                    name.setValue(TextConstants.CODE_GLUCOSE_FASTING);
                    name.setText(vitalsConfigMo.getGlucose().getGlucoseFasting().getObservationName());

                } else if (observationValueUnit.getObservationCat().replaceAll(" ", "").equalsIgnoreCase(
                        vitalsConfigMo.getGlucose().getGlucoseAfterMeal().getObservationName())) {
                    observationIdentifier.setValue(TextConstants.CODE_GLUCOSE_AFTERMEAL);
                    name.setValue(TextConstants.CODE_GLUCOSE_AFTERMEAL);
                    name.setText(vitalsConfigMo.getGlucose().getGlucoseAfterMeal().getObservationName());
                }
            }

            if (vitalsConfigMo.isToShowAddBPButton()) {
                if (observationValueUnit.getObservationCat().equalsIgnoreCase(
                        vitalsConfigMo.getBloodPressure().getBPSYstolic().getObservationName())) {
                    observationIdentifier.setValue(TextConstants.CODE_SYSTOLIC);
                    name.setValue(TextConstants.CODE_SYSTOLIC);
                    name.setText(vitalsConfigMo.getBloodPressure().getBPSYstolic().getObservationName());

                } else if (observationValueUnit.getObservationCat().equalsIgnoreCase(
                        vitalsConfigMo.getBloodPressure().getBPDystolic().getObservationName())) {
                    observationIdentifier.setValue(TextConstants.CODE_DIASTOLIC);
                    name.setValue(TextConstants.CODE_DIASTOLIC);
                    name.setText(vitalsConfigMo.getBloodPressure().getBPDystolic().getObservationName());

                }
            }
            //Value
            ObservationValue value = new ObservationValue();
            value.setUnit(observationValueUnit.getUnit());
            value.setValue(observationValueUnit.getValue());

            diagnosticObservations.setCode(name);
            identifier.add(0, observationIdentifier);
            diagnosticObservations.setIdentifier(identifier);
            diagnosticObservations.setIssuedDate(date);
            diagnosticObservations.setValue(value);

            observations.add(i, diagnosticObservations);
        }
        return observations;
    }

    private ArrayList<DiagnosticObservations> setBMIParameter() {

        observations.clear();
        for (int i = 0; i < observationArray.size(); i++) {
            DiagnosticObservations diagnosticObservations = new DiagnosticObservations();

            //code
            DiagnosticObservationCode name = new DiagnosticObservationCode();
            //name.setText(observationType);
            name.setSystem("loinc");
            if (i == 0) {
                name.setText(vitalsConfigMo.getBMI().getBMIWeight().getObservationName());
                name.setValue(TextConstants.CODE_WEIGHT);
            } else {
                name.setText(vitalsConfigMo.getBMI().getBMIHeight().getObservationName());
                name.setValue(TextConstants.CODE_HEIGHT);
            }
            diagnosticObservations.setCode(name);

            //identifier
            ArrayList<DiagnosticIdentifier> identifier = new ArrayList<>();

            DiagnosticIdentifier observationIdentifier = new DiagnosticIdentifier();
            DiagnosticType type = new DiagnosticType();
            type.setText("paramId");
            observationIdentifier.setType(type);
            observationIdentifier.setUseCode("usual");
            if (i == 0)
                observationIdentifier.setValue(TextConstants.CODE_WEIGHT);
            else
                observationIdentifier.setValue(TextConstants.CODE_HEIGHT);
            identifier.add(observationIdentifier);
            diagnosticObservations.setIdentifier(identifier);

            //issued date
            diagnosticObservations.setIssuedDate(date);

            String unit = observationArray.get(i).getUnit();
            String number = observationArray.get(i).getValue();

            //value
            ObservationValue value = new ObservationValue();
            value.setUnit(unit);
            value.setValue(number + "");
            diagnosticObservations.setValue(value);
            observations.add(0, diagnosticObservations);
        }
        return observations;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (isshowprogressdialog) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }

        }

        //
        if (context instanceof EnterObservationActivity) {
            ((EnterObservationActivity) context).setObservationGsonResult(result, convertedUnit, convertedValue, highRange, lowRange);
        } else {
            SharedPref.setRefreshRecords(true);
        }

        super.onPostExecute(result);
    }

    private void initProgressDialog() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.progress_submitting));

    }

}