package com.mphrx.fisike.vital_submit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.vital_submit.background.FetchVitalConfigRequest;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

public class BlockVitalsScreenActivity extends NetworkActivity {

    CustomFontButton btnTryAgain;
    private IconTextView imgHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_vitals_screen);
        initView();
    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }

    private void initView() {
        btnTryAgain = (CustomFontButton) findViewById(R.id.btn_retry);
        btnTryAgain.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showProgressDialog(getString(R.string.please_wait));
                        ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(BlockVitalsScreenActivity.this, getTransactionId()));
                    }
                });

        imgHeader = (IconTextView) findViewById(R.id.img_header);

        if (BuildConfig.isIcomoonForSplashEnabled) {
            if(BuildConfig.isPatientApp)
            {imgHeader.setText(R.string.fa_fisike_icon);
                imgHeader.setTextSize(100f);}
            else
            {imgHeader.setText(R.string.fa_fisike_spec_icon);
                imgHeader.setTextSize(70f);}
        } else {
            imgHeader.setBackgroundResource(R.drawable.fisike_icon);
            imgHeader.setText("");
            imgHeader.setTextSize(0);
        }
    }


    @Subscribe
    public void onVitalsConfigResponse(VitalsConfigResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;

        dismissProgressDialog();

    }

}
