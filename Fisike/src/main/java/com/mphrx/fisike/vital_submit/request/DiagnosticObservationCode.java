package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticObservationCode {

	@SerializedName("text")
	private String text;
	@SerializedName("system")
	private String system;
	@SerializedName("value")
	private String value;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
