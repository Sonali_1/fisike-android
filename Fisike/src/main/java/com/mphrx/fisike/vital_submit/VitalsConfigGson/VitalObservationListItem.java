package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Neha on 02-12-2016.
 */

public class VitalObservationListItem implements Parcelable{

    private String observationName;
    private ArrayList<String> optionalUnits;
    private String defaultUnit;
    private String systemCode;
    private HashMap<String, ValidationListItemParams> validationList;

    private String observationName_key="observationName";
    private String optionalUnits_key="optionalUnits";
    private String defaultUnit_key="defaultUnit";
    private String systemCode_key="systemCode";
    private String validationList_key="validationList";
    private String bmiUnitRestriction_key="bmiUnitRestrictions";
    private JSONObject bmiUnitRestrictionsKey;


    public VitalObservationListItem(String JsonObject) {
        // TODO Auto-generated constructor stub
        parseJson(JsonObject);
    }

    public VitalObservationListItem(Parcel in) {

        defaultUnit=in.readString();
        observationName=in.readString();
        in.readStringList(optionalUnits);
        systemCode=in.readString();
        validationList=in.readHashMap(ClassLoader.getSystemClassLoader());
    }

    private void parseJson(String JsonObject){
        try
        {
            JSONObject jsonString=new JSONObject(JsonObject);
            observationName=jsonString.optString(observationName_key);
            defaultUnit=jsonString.optString(defaultUnit_key);
            bmiUnitRestrictionsKey =  jsonString.optJSONObject(bmiUnitRestriction_key);

            JSONArray optionalUnitsArray=jsonString.getJSONArray(optionalUnits_key);
            this.optionalUnits=new ArrayList<String>();
            for(int i=0;i<optionalUnitsArray.length();i++)
                this.optionalUnits.add(optionalUnitsArray.get(i).toString());

            systemCode=jsonString.getString(systemCode_key);
            validationList=new HashMap<>();

            JSONObject validationObject =new JSONObject(jsonString.getString(validationList_key));
            for(Iterator<String> valiationListIterator = validationObject.keys(); valiationListIterator.hasNext();)
            {
                String key=valiationListIterator.next();
                validationList.put(key,new ValidationListItemParams(validationObject.getString(key)));
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getObservationName() {
        return observationName;
    }
    public void setObservationName(String observationName) {
        this.observationName = observationName;
    }
    public ArrayList<String> getOptionalUnits() {
        return optionalUnits;
    }
    public void setOptionalUnits(ArrayList<String> optionalUnits) {
        this.optionalUnits = optionalUnits;
    }
    public String getDefaultUnit() {
        return defaultUnit;
    }
    public void setDefaultUnit(String defaultUnit) {
        this.defaultUnit = defaultUnit;
    }
    public String getSystemCode() {
        return systemCode;
    }
    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }
    public HashMap<String, ValidationListItemParams> getValidationList() {
        return validationList;
    }
    public void setValidationList(HashMap<String, ValidationListItemParams> validationList) {
        this.validationList = validationList;
    }

    @Override
    public String toString() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(defaultUnit_key, getDefaultUnit());
            jsonObject.put(observationName_key,getObservationName());
            JSONArray units=new JSONArray(getOptionalUnits());
            jsonObject.put(optionalUnits_key,units);
            jsonObject.put(systemCode_key,getSystemCode());
            JSONObject object=new JSONObject();
            for(Map.Entry<String,ValidationListItemParams> entry:getValidationList().entrySet())
            {
                object.put(entry.getKey(),entry.getValue());
            }
            jsonObject.put(validationList_key,object);
        return jsonObject.toString();
        }
        catch (Exception e)
        {
            return null;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {

        dest.writeString(getDefaultUnit());
        dest.writeString(getObservationName());
        dest.writeStringList(getOptionalUnits());
        dest.writeString(getSystemCode());
        dest.writeMap(getValidationList());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public VitalObservationListItem createFromParcel(Parcel in) {
            return new VitalObservationListItem(in);
        }

        public VitalObservationListItem[] newArray(int size) {
            return new VitalObservationListItem[size];
        }
    };


    /*
     * return true if valid observationList is there
     @isToCheckOptionalList checks weather to optional unit or not
     *
     */
    public boolean checkObservationList(boolean isToCheckOptionalList)
    {
        try{
           //checking validation List
            if(getValidationList()!=null && !getValidationList().isEmpty() && Utils.isValueAvailable(getObservationName())){
               //checking optional list based on @param
                if(isToCheckOptionalList && getOptionalUnits()!=null
                       && !getOptionalUnits().isEmpty())
                   return true;
               else if(!isToCheckOptionalList)
                   return true;
               else return false;
            }
            else return false;
        }catch (Exception e){
            return false;
        }
    }
}
