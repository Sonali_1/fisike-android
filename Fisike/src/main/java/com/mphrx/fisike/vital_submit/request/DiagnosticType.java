package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticType{

	@SerializedName("text")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}