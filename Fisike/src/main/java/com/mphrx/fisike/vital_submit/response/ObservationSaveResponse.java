package com.mphrx.fisike.vital_submit.response;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.vital_submit.request.ObservationValue;

import java.util.ArrayList;

public class ObservationSaveResponse {

	@Expose
	private ArrayList<Observations> observations;
	private String diagnosticOrderId;
	private String observationType;
	private String observationStatus;
	private ArrayList<ObservationValue> observationValue;
	private int pk;

	public ArrayList<Observations> getObservations() {
		return observations;
	}

	public void setObservations(ArrayList<Observations> observations) {
		this.observations = observations;
	}

	public String getDiagnosticOrderId() {
		return diagnosticOrderId;
	}

	public void setDiagnosticOrderId(String diagnosticOrderId) {
		this.diagnosticOrderId = diagnosticOrderId;
	}

	public String getObservationType() {
		return observationType;
	}

	public void setObservationType(String observationType) {
		this.observationType = observationType;
	}

	public String getObservationStatus() {
		return observationStatus;
	}

	public void setObservationStatus(String observationStatus) {
		this.observationStatus = observationStatus;
	}

	public ArrayList<ObservationValue> getObservationValue() {
		return observationValue;
	}

	public void setObservationValue(ArrayList<ObservationValue> observationValue) {
		this.observationValue = observationValue;
	}

	public int getPk() {
		return pk;
	}

	public void setPk(int pk) {
		this.pk = pk;
	}
}