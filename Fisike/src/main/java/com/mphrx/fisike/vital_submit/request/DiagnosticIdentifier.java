package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticIdentifier {

	@SerializedName("type")
	private DiagnosticType type;
	@SerializedName("useCode")
	private String useCode;
	@SerializedName("value")
	private String value;


	public DiagnosticType getType() {
		return type;
	}

	public void setType(DiagnosticType type) {
		this.type = type;
	}

	public String getUseCode() {
		return useCode;
	}

	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}