package com.mphrx.fisike.vital_submit.request;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObservationGSON  {

	@SerializedName("observations")
	private ArrayList<DiagnosticObservations> observations;
	@SerializedName("subject")
	private DiagnosticSubject subject;
	@SerializedName("testName")
	private String testName;
	@SerializedName("category")
	private String category;
	@SerializedName("issuedDate")
	private String issuedDate;
	@SerializedName("source")
	private String source;

    public ArrayList<DiagnosticObservations> getObservations() {
        return observations;
    }

    public void setObservations(ArrayList<DiagnosticObservations> observations) {
        this.observations = observations;
    }

    public DiagnosticSubject getSubject() {
        return subject;
    }

    public void setSubject(DiagnosticSubject subject) {
        this.subject = subject;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
