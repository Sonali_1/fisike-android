package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Neha on 02-12-2016.
 */

public class ValidationListItemParams implements Parcelable {

    private double high;
    private double highest;
    private double low;
    private double lowest;

    public static String high_key = "high";
    public static String highest_key = "highest";
    public static String low_key = "low";
    public static String lowest_key = "lowest";

    public ValidationListItemParams(String validationString) {
        // TODO Auto-generated constructor stub
        try {
            JSONObject validationObject = new JSONObject(validationString);
            low = validationObject.optDouble(low_key);
            lowest = validationObject.optDouble(lowest_key);
            high = validationObject.optDouble(high_key);
            highest = validationObject.optDouble(highest_key);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public ValidationListItemParams(Parcel in)
    {
        low = in.readDouble();
        lowest = in.readDouble();
        high = in.readDouble();
        highest = in.readDouble();
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getHighest() {
        return highest;
    }

    public void setHighest(double highest) {
        this.highest = highest;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getLowest() {
        return lowest;
    }

    public void setLowest(double lowest) {
        this.lowest = lowest;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        try {
            JSONObject validateItemparams = new JSONObject();
            validateItemparams.put(low_key, low + "");
            validateItemparams.put(lowest_key, lowest + "");
            validateItemparams.put(high_key, high + "");
            validateItemparams.put(highest_key, highest + "");
            return validateItemparams.toString();
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(low);
        dest.writeDouble(lowest);
        dest.writeDouble(high);
        dest.writeDouble(highest);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ValidationListItemParams createFromParcel(Parcel in) {
            return new ValidationListItemParams(in);
        }

        public ValidationListItemParams[] newArray(int size) {
            return new ValidationListItemParams[size];
        }
    };
}
