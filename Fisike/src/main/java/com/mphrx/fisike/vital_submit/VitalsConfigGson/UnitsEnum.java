package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha on 05-12-2016.
 */

public enum UnitsEnum {
    FT(MyApplication.getAppContext().getString(R.string.ft_key), R.string.ft_value),
    INCH(MyApplication.getAppContext().getString(R.string.in_key), R.string.in_value),
    METER(MyApplication.getAppContext().getString(R.string.meter_key), R.string.meter_value),
    CENTI(MyApplication.getAppContext().getString(R.string.centi_meter_key), R.string.centi_meter_value),
    FEETINCH(MyApplication.getAppContext().getString(R.string.ft_in_key),R.string.ft_in_value),
    KG(MyApplication.getAppContext().getString(R.string.kg_key),R.string.kg_value),
    POUND(MyApplication.getAppContext().getString(R.string.lb_key),R.string.lb_value),
    MMHG(MyApplication.getAppContext().getString(R.string.mmhg_key), R.string.mmhg_value),
    CMH2O(MyApplication.getAppContext().getString(R.string.cmH2O_key), R.string.cmH2O_value),
    MGPERDL(MyApplication.getAppContext().getString(R.string.mg_pedl_key),R.string.mg_pedl_value),
//    KGPM(MyApplication.getAppContext().getString(R.string.bmi_kgperm_key),R.string.bmi_kgperm_value),
    MMOLPERL(MyApplication.getAppContext().getString(R.string.mmol_perl_key),R.string.mmol_perl_value);

    private String code ;
    int description;
    public static LinkedHashMap<String, UnitsEnum> unitsMap = null;

    private UnitsEnum(String code, int description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        try {
            return MyApplication.getAppContext().getString(description);
        } catch (Exception e) {
            return "";
        }
    }

    public void setDescription(int description) {
        this.description = description;
    }


    public static LinkedHashMap<String, UnitsEnum> getMap() {
        if (unitsMap == null) {
            unitsMap = new LinkedHashMap<String, UnitsEnum>();
            for (UnitsEnum interpretation : UnitsEnum.values()) {
                unitsMap.put(interpretation.code, interpretation);
            }
        }
        return unitsMap;
    }

    public static String getCode(String value) {
        String code = "";
        getMap();
        for (Map.Entry<String, UnitsEnum> entry : unitsMap.entrySet())
        {
            if (entry.getValue().getDescription().equalsIgnoreCase(value)) {
                code = entry.getValue().getCode();
                break;
            }
        }
        return code;
    }

    //return value matching to particular value
    public static String getValueFrom(String code)
    {
        getMap();
        if(unitsMap!=null && unitsMap.containsKey(code))
        {
            return unitsMap.get(code).getDescription();
        }
        return code;
    }
}
