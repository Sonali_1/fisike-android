package com.mphrx.fisike.vital_submit.request;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObservationValue {
	//private static final long serialVersionUID = 8407901929289734698L;


	@SerializedName("unit")
	private String unit;
	@SerializedName("value")
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/*public String getObservationCategory() {
		return observationCategory;
	}

	public void setObservationCategory(String observationCategory) {
		this.observationCategory = observationCategory;
	}*/
}
