package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class DiagnosticSubject implements Serializable {

	@Expose
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
