package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Kailash on 27-02-2017.
 */

public class BMIListParams implements Parcelable {


    private ArrayList<String> validationObjectArray;

    public BMIListParams(String validationString) {
        // TODO Auto-generated constructor stub
        try {
            JSONArray validationObject = new JSONArray(validationString);
            this.validationObjectArray=new ArrayList<String>();
            for(int i=0;i<validationObject.length();i++)
                this.validationObjectArray.add(validationObject.get(i).toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public BMIListParams(Parcel in)
    {
        in.readStringList(validationObjectArray);
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        try {
            JSONArray jsonArray=new JSONArray();
            for(String s:validationObjectArray)
                jsonArray.put(s);
            return jsonArray.toString();
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(validationObjectArray);
    }

    public static final Creator CREATOR = new Creator() {
        public BMIListParams createFromParcel(Parcel in) {
            return new BMIListParams(in);
        }

        public BMIListParams[] newArray(int size) {
            return new BMIListParams[size];
        }
    };

    public ArrayList<String> getValidationObjectArray() {
        return validationObjectArray;
    }
}
