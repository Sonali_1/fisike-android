package com.mphrx.fisike.vital_submit.response;

import com.google.gson.annotations.Expose;

public class Observations {
	
	@Expose
	private String diagnosticOrderID;

	public String getDiagnosticOrderID() {
		return diagnosticOrderID;
	}

	public void setDiagnosticOrderID(String diagnosticOrderID) {
		this.diagnosticOrderID = diagnosticOrderID;
	}
}
