package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.utils.Utils;

import org.json.JSONObject;

public class VitalsConfigMO implements Parcelable {
    private String defaultSystem;
    private Vital BMI;
    private Vital BloodPressure;
    private Vital Glucose;
    private static VitalsConfigMO vitalsConfigMO;

    private VitalsConfigMO(String jsonString) {
        // TODO Auto-generated constructor stub
        try {
            JSONObject vitalsJson = new JSONObject(jsonString);
            defaultSystem = vitalsJson.optString("defaultSystem");
            BMI = new Vital(vitalsJson.optString("BMI"));
            BloodPressure = new Vital(vitalsJson.optString("BloodPressure"));
            Glucose = new Vital(vitalsJson.optString("Glucose"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public VitalsConfigMO(Parcel in) {
        defaultSystem = in.readString();
        BMI = in.readParcelable(ClassLoader.getSystemClassLoader());
        BloodPressure = in.readParcelable(ClassLoader.getSystemClassLoader());
        Glucose = in.readParcelable(ClassLoader.getSystemClassLoader());

    }


    public static VitalsConfigMO parseObject(String jsonString) {
        vitalsConfigMO = new VitalsConfigMO(jsonString);
        return vitalsConfigMO;
    }


    public static VitalsConfigMO getInstance() {
        if (null == vitalsConfigMO) {
            synchronized (VitalsConfigMO.class) {
                if (vitalsConfigMO == null)
                    vitalsConfigMO = new VitalsConfigMO();
            }
        }
        return vitalsConfigMO;
    }

    public static void setInstance(VitalsConfigMO vitalsConfig) {
        vitalsConfigMO = vitalsConfig;
    }

    private VitalsConfigMO() {

    }

    public String getDefaultSystem() {
        return defaultSystem;
    }

    public void setDefaultSystem(String defaultString) {
        this.defaultSystem = defaultString;
    }

    public Vital getBMI() {
        return BMI;
    }

    public void setBMI(Vital bMI) {
        BMI = bMI;
    }

    public Vital getBloodPressure() {
        return BloodPressure;
    }

    public void setBloodPressure(Vital bloodPressure) {
        BloodPressure = bloodPressure;
    }

    public Vital getGlucose() {
        return Glucose;
    }

    public void setGlucose(Vital glucose) {
        Glucose = glucose;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getDefaultSystem());
        dest.writeParcelable(getBMI(), flags);
        dest.writeParcelable(getBloodPressure(), flags);
        dest.writeParcelable(getGlucose(), flags);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public VitalsConfigMO createFromParcel(Parcel in) {
            return new VitalsConfigMO(in);
        }

        public VitalsConfigMO[] newArray(int size) {
            return new VitalsConfigMO[size];
        }
    };


    //return true if to display AddBMI Button
    public boolean isToShowAddBMIButton() {
        try {
            //BMI is null
            if (getBMI() == null || getBMI().getBmiListRestrictionMap() == null || !Utils.isValueAvailable(getBMI().getName()))
                return false;

                //BMI restrictionMap is empty
            else if (getBMI().getBmiListRestrictionMap() != null
                    && getBMI().getBmiListRestrictionMap().isEmpty()) return false;

                //BMI height or weight is not null and checking its units
            else if (getBMI().getBMIHeight() != null && getBMI().getBMIWeight() != null) {
                //checking Observation Height& weight
                if (getBMI().getBMIHeight().checkObservationList(false)
                        && getBMI().getBMIWeight().checkObservationList(false))
                    return true;

                    //not meeting any above information
                else return false;

            } else return false;

        } catch (Exception e) {
            return false;
        }
    }

    //return true if to display AddBP Button
    public boolean isToShowAddBPButton() {
        try {
            //BP is null
            if (getBloodPressure() == null || !Utils.isValueAvailable(getBloodPressure().getName()))
                return false;

                //BP Systolic & diastolic is not null and checking its units
            else if (getBloodPressure().getBPSYstolic() != null && getBloodPressure().getBPDystolic() != null) {

                //checking Observation Systolic & Diastolic
                if (getBloodPressure().getBPSYstolic().checkObservationList(true)
                        && getBloodPressure().getBPDystolic().checkObservationList(true))
                    return true;

                    //not meeting any above information
                else return false;

            } else return false;

        } catch (Exception e) {
            return false;
        }
    }

    //return true if to display Add Glucose Button
    public boolean isToShowAddGlucoseButton() {
        try {
            //Glucose is null
            if (getGlucose() == null || !Utils.isValueAvailable(getGlucose().getName()))
                return false;

                //Glucose fasting,random & aftermeal is not null and checking its units
            else if (getGlucose().getGlucoseRandom() != null && getGlucose().getGlucoseFasting() != null
                    && getGlucose().getGlucoseAfterMeal() != null) {

                //checking Observation fasting,random & aftermeal
                if (getGlucose().getGlucoseRandom().checkObservationList(true)
                        && getGlucose().getGlucoseFasting().checkObservationList(true)
                        && getGlucose().getGlucoseAfterMeal().checkObservationList(true))
                    return true;

                    //not meeting any above information
                else return false;

            } else return false;

        } catch (Exception e) {
            return false;
        }
    }

    //return true if all the validation pass otherwise false
    public boolean isValidVitalsFetched() {
        if (getBMI() == null && getBloodPressure() == null && getGlucose() == null)
            return false;

        else if (!isToShowAddBMIButton() && !isToShowAddBPButton() && !isToShowAddGlucoseButton())
            return false;
        else
            return true;
    }
}
