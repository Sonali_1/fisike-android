package com.mphrx.fisike.vital_submit.response;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;


/**
 * Created by Neha on 19-12-2016.
 */

public class VitalsConfigResponse extends BaseResponse {

    private boolean isToLaunchContactAdminScreen;
    private boolean isValidJson;

    public VitalsConfigResponse(JSONObject response, long transactionId,boolean isSuccess,boolean isValidJson) {
        super(response, transactionId);
        this.isToLaunchContactAdminScreen=isSuccess;
        this.isValidJson=isValidJson;
    }

    public VitalsConfigResponse(Exception exception, long transactionId,boolean isSuccess) {
        super(exception, transactionId);
        this.isToLaunchContactAdminScreen=isSuccess;
    }

    public boolean getIsTolaunchContactAdminScreen()
    {
        return isToLaunchContactAdminScreen;
    }
    public boolean isValidJson(){
        return  isValidJson;
    }
}
