package com.mphrx.fisike.vital_submit.request;

public class ObservationValueUnit {
	private String observationCat;
	private String value;
	private String unit;

	public ObservationValueUnit(String observationVal, String observatonUnit, String observationCat) {
		value = observationVal;
		unit = observatonUnit;
		this.observationCat = observationCat;
	}

	public String getObservationCat() {
		return observationCat;
	}

	public void setObservationCat(String observationCat) {
		this.observationCat = observationCat;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}
