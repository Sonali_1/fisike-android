package com.mphrx.fisike.vital_submit.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.method.DigitsKeyListener;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.SuperscriptSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.MyHealthConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.UnitsEnum;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalObservationListItem;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike.vital_submit.background.ObservationSumitTask;
import com.mphrx.fisike.vital_submit.request.ObservationValueUnit;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import appointment.utils.CustomTypefaceSpan;

import static com.mphrx.fisike.constant.VariableConstants.BMI;

/**
 * Enter the details for the observation clicked
 * 256144705846
 *
 * @author KAILASH
 */
public class EnterObservationActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private CustomFontTextView txtDate;
    private CustomFontTextView txtTime;
    private CustomFontTextView txtObservationType;
    private String observation;
    private CustomFontEditTextView edValue1;
    private CustomFontEditTextView edValue2;
    private CustomFontEditTextView edValue3;
    private String stringValue1;
    private String stringValue2;
    private String stringValue3;
    private String stringValue4;
    private CustomFontTextView spinnerUnit1;
    private CustomFontTextView spinnerUnit2;
    private float number;
    private String unit;
    private String observationTempVal;
    private boolean isSetHeightUserMo;
    private boolean isSetWeightUserMo;
    private boolean isRandomFirst = true, isAfterMealFirst = true, isFastingFirst = true;
    private CustomFontEditTextView edValue4;
    private String stringUnit1;
    private String observationTempLable;
    private RadioButton rbValue1;
    private RadioButton rbValue2;
    private RadioButton rbValue3;
    private double minValue;
    private LinearLayout layoutUnit;

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    private LinearLayout ll_date, ll_time;

    private String observationType;
    private VitalsConfigMO vitalsConfig;
    private double low, lowest, high, highest;
    private ArrayList<String> listHeight;
    private ArrayList<String> list;
    private int seconds;

    public int maxMinute;
    private boolean isTodayDate;
    public int maxHour;

    /**
     * Set the content view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_enter_observation, frameLayout);

        try {
            vitalsConfig = VitalsConfigDBAdapter.getInstance(this).fetchVitalsConfigMO();
            VitalsConfigMO.setInstance(vitalsConfig);
        } catch (Exception e) {
            AppLog.d("Vitals Config", "null vitals config");
        }

        initToolbar();
        initViews();
        setObservationValues();
    }

    private void initToolbar() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.txt_enterdetail));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
    }


    /**
     * Initialized views
     */
    private void initViews() {
        txtDate = (CustomFontTextView) findViewById(R.id.txtDate);
        txtTime = (CustomFontTextView) findViewById(R.id.txtTime);
        txtObservationType = (CustomFontTextView) findViewById(R.id.txtObservationType);
        edValue1 = (CustomFontEditTextView) findViewById(R.id.edValue1);
        edValue2 = (CustomFontEditTextView) findViewById(R.id.edValue2);
        edValue3 = (CustomFontEditTextView) findViewById(R.id.edValue3);
        edValue4 = (CustomFontEditTextView) findViewById(R.id.edValue4);
        spinnerUnit1 = (CustomFontTextView) findViewById(R.id.spinerUnit1);
        spinnerUnit2 = (CustomFontTextView) findViewById(R.id.spinerUnit2);
        txtDate.setText(String.format(getResources().getString(R.string.date), DateTimeUtil.getCurrentDate(new Date(System.currentTimeMillis()))));
        if (!DateFormat.is24HourFormat(this))
            txtTime.setText(String.format(getResources().getString(R.string.time), DateTimeUtil.getFormattedTime(new Date(System.currentTimeMillis()))));
        else
            txtTime.setText(String.format(getResources().getString(R.string.time), DateTimeUtil.getFormattedTimeIn24Hour(new Date(System.currentTimeMillis()))));

        seconds = Calendar.getInstance().get(Calendar.SECOND);
        maxHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        maxMinute = Calendar.getInstance().get(Calendar.MINUTE);
        isTodayDate = true;

        rbValue1 = (RadioButton) findViewById(R.id.rbValue1);
        rbValue2 = (RadioButton) findViewById(R.id.rbValue2);
        rbValue3 = (RadioButton) findViewById(R.id.rbValue3);
        layoutUnit = (LinearLayout) findViewById(R.id.layoutUnit);
        ll_date = (LinearLayout) findViewById(R.id.ll_date);
        ll_time = (LinearLayout) findViewById(R.id.ll_time);
        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

    }

    /**
     * Set views value for the observation type
     */
    private void setObservationValues() {
        Bundle extras = getIntent().getExtras();
        observation = extras.getString(VariableConstants.OBSERVATION_TEXT);
        Resources resources = getResources();
        if (observation.equals(vitalsConfig.getBloodPressure().getName())) {
            txtObservationType.setText(getResources().getString(R.string.blood_pressure_screen_title));
            txtObservationType.setTag(observation);
            edValue1.setHint(resources.getString(R.string.systotic));
            edValue1.setTag(MyHealthConstants.SYSTOLIC);
            edValue2.setHint(resources.getString(R.string.diasystotic));
            edValue2.setTag(MyHealthConstants.DIASTOLIC);
            setSpinner();
            onPopUpMenuItemClick(String.valueOf(spinnerUnit1.getText()));
        } else if (observation.equals(vitalsConfig.getGlucose().getName())) {
            txtObservationType.setText(getResources().getString(R.string.glucose_screen_title));
            txtObservationType.setTag(observation);
            edValue3.setVisibility(View.GONE);
            edValue4.setVisibility(View.VISIBLE);
            edValue1.setHint(resources.getString(R.string.random));
            edValue1.setTag(MyHealthConstants.RANDOM);
            edValue2.setHint(resources.getString(R.string.fasting));
            edValue2.setTag(MyHealthConstants.FASTING);
            edValue4.setHint(resources.getString(R.string.after_meal));
            edValue4.setTag(MyHealthConstants.AFTERMEAL);

            setSpinner();
            setRadioButtonListner();
            onPopUpMenuItemClick(String.valueOf(spinnerUnit1.getText()));
            rbValue1.setChecked(true);
        } else if (observation.equals(vitalsConfig.getBMI().getName())) {
            txtObservationType.setText((getResources().getString(R.string.bmi_screen_title)));
            txtObservationType.setTag(observation);
            edValue1.setHint(resources.getString(R.string.weight));
            edValue1.setTag(MyHealthConstants.WEIGHT);
            edValue2.setHint(resources.getString(R.string.height));
            edValue2.setTag(MyHealthConstants.HEIGHT);
            final UserMO userMO = SettingManager.getInstance().getUserMO();
            spinnerUnit2.setVisibility(View.VISIBLE);
            setSpinner();
            onPopUpMenuItemClick(String.valueOf(spinnerUnit1.getText()));
            onPopUpMenuItemClick(String.valueOf(spinnerUnit2.getText()));
        }
    }

    private void setRadioButtonListner() {
        rbValue1.setVisibility(View.VISIBLE);
        rbValue2.setVisibility(View.VISIBLE);
        rbValue3.setVisibility(View.VISIBLE);
        rbValue1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    edValue4.setText("");
                    edValue2.setText("");
                    edValue1.getEditText().setEnabled(true);
                    edValue2.getEditText().setEnabled(false);
                    edValue4.getEditText().setEnabled(false);
                    rbValue2.setChecked(false);
                    rbValue3.setChecked(false);
                    edValue1.getEditText().setFocusable(true);
                    edValue1.getEditText().setFocusableInTouchMode(true);
                    edValue1.getEditText().requestFocus();
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) EnterObservationActivity.this.getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(
                            EnterObservationActivity.this.getCurrentFocus(), 0);
                }
            }
        });
        rbValue2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    edValue1.setText("");
                    edValue4.setText("");
                    edValue1.getEditText().setEnabled(false);
                    edValue2.getEditText().setEnabled(true);
                    edValue4.getEditText().setEnabled(false);
                    rbValue1.setChecked(false);
                    rbValue3.setChecked(false);
                    edValue2.getEditText().setFocusableInTouchMode(true);
                    edValue2.getEditText().setFocusable(true);
                    edValue2.getEditText().requestFocus();
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) EnterObservationActivity.this.getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(
                            EnterObservationActivity.this.getCurrentFocus(), 0);
                }
            }
        });
        rbValue3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    edValue1.setText("");
                    edValue2.setText("");
                    edValue1.getEditText().setEnabled(false);
                    edValue2.getEditText().setEnabled(false);
                    edValue4.getEditText().setEnabled(true);
                    rbValue1.setChecked(false);
                    rbValue2.setChecked(false);
                    edValue1.setFocusable(false);
                    edValue4.getEditText().setFocusableInTouchMode(true);
                    edValue4.getEditText().setFocusable(true);
                    edValue4.getEditText().requestFocus();
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) EnterObservationActivity.this.getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(
                            EnterObservationActivity.this.getCurrentFocus(), 0);
                }
            }
        });
    }


    private void setFeetInchesView(String unit) {
        //checking weather list of ft-in to be show
        if (unit.toLowerCase().equals(getString(R.string.ft_in_key))) {
            edValue3.setVisibility(View.VISIBLE);
            layoutUnit.setVisibility(View.VISIBLE);
        } else if (unit.toLowerCase().equals(getString(R.string.kg_key))) {
            edValue3.setVisibility(View.GONE);
            layoutUnit.setVisibility(View.GONE);
        }

    }

    private void setSpinner() {
        list = new ArrayList<>();
        listHeight = new ArrayList<>();
        String defaultUnit, defaultUnit1;
        if (observation.equalsIgnoreCase(VariableConstants.Glucose)) {
            list = vitalsConfig.getGlucose().getGlucoseAfterMeal().getOptionalUnits();
            defaultUnit = (Utils.isValueAvailable(vitalsConfig.getGlucose().getGlucoseRandom().getDefaultUnit()) ? vitalsConfig.getGlucose().getGlucoseRandom().getDefaultUnit() : list.get(0));
            defaultUnit = UnitsEnum.getValueFrom(defaultUnit);
            spinnerUnit1.setText(defaultUnit);
        } else if (observation.equalsIgnoreCase(BMI)) {
            Set<String> strings = vitalsConfig.getBMI().getBmiListRestrictionMap().keySet();
            list = new ArrayList<String>(strings);
            try {
                defaultUnit = Utils.isValueAvailable(vitalsConfig.getBMI().getBMIWeight().getDefaultUnit()) ?
                        vitalsConfig.getBMI().getBMIWeight().getDefaultUnit() : list.get(0);
            } catch (Exception e) {
                defaultUnit = list.get(0);
            }

            if (vitalsConfig.getBMI().getBmiListRestrictionMap().get(defaultUnit) == null)
                defaultUnit = list.get(0);
            listHeight = vitalsConfig.getBMI().getBmiListRestrictionMap().get(defaultUnit).getValidationObjectArray();
            defaultUnit1 = Utils.isValueAvailable(vitalsConfig.getBMI().getBMIHeight().getDefaultUnit()) ?
                    vitalsConfig.getBMI().getBMIHeight().getDefaultUnit() : listHeight.get(0);
            defaultUnit = UnitsEnum.getValueFrom(defaultUnit);
            defaultUnit1 = UnitsEnum.getValueFrom(defaultUnit1);
            spinnerUnit1.setText(defaultUnit);
            spinnerUnit2.setText(defaultUnit1);
            setFeetInchesView(defaultUnit1);
        } else {
            list = vitalsConfig.getBloodPressure().getBPSYstolic().getOptionalUnits();
            defaultUnit = Utils.isValueAvailable(vitalsConfig.getBloodPressure().getBPSYstolic().getDefaultUnit()) ?
                    vitalsConfig.getBloodPressure().getBPSYstolic().getDefaultUnit() : list.get(0);
            defaultUnit = UnitsEnum.getValueFrom(defaultUnit);
            spinnerUnit1.setText(defaultUnit);
        }

        spinnerUnit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(list, spinnerUnit1, observation);
            }
        });

        spinnerUnit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (observation.equalsIgnoreCase(BMI)) {
                    showPopupMenu(listHeight, spinnerUnit2, observation);
                    return;
                }
                showPopupMenu(listHeight, spinnerUnit2, observation);
            }
        });
    }

    public void showPopupMenu(ArrayList<String> menuId, final CustomFontTextView view, final String type) {
        final PopupMenu popup = new PopupMenu(EnterObservationActivity.this, view);
        for (int i = 0; i < menuId.size(); i++) {
            popup.getMenu().add(Menu.NONE, i, Menu.NONE, UnitsEnum.getValueFrom(menuId.get(i)));
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                String itemTitle = "";
                if (item.getTitle().toString().equalsIgnoreCase(view.getText().toString())) {
                    return true;
                }
                view.setText(item.getTitle());
                itemTitle = UnitsEnum.getCode(item.getTitle().toString());
                if (type.equals(VariableConstants.Glucose)) {
                    if (view == spinnerUnit1) {
                        onPopUpMenuItemClick(itemTitle);
                        edValue1.setText("");
                        edValue2.setText("");
                        edValue4.setText("");

                        if (isRandomFirst) {
                            isRandomFirst = false;
                        } else {
                            isRandomFirst = true;
                        }
                    }
                } else if (type.equalsIgnoreCase(VariableConstants.BMI)) {
                    if (view == spinnerUnit1) {
                        ArrayList<String> validationObjectArray = vitalsConfig.getBMI().getBmiListRestrictionMap().get(itemTitle).getValidationObjectArray();
                        String[] heightArray = validationObjectArray.toArray(new String[validationObjectArray.size()]);
                        listHeight = new ArrayList<>();
                        for (int i = 0; heightArray != null && i < heightArray.length; i++) {
                            listHeight.add(UnitsEnum.getValueFrom(heightArray[i]));
                        }
                        //setting default unit of BMI based on BMI value
                        String defaultUnit1 = "";
                        defaultUnit1 = UnitsEnum.getValueFrom((Utils.isValueAvailable(vitalsConfig.getBMI().getBMIHeight().getDefaultUnit()) ?
                                vitalsConfig.getBMI().getBMIHeight().getDefaultUnit() : heightArray[0]));

                        if (!listHeight.contains(defaultUnit1))
                            defaultUnit1 = UnitsEnum.getValueFrom(heightArray[0]);

                        spinnerUnit2.setText(defaultUnit1);
                    }

                    onClickBMI(itemTitle, view);
                    onPopUpMenuItemClick(itemTitle);
                } else if (type.equalsIgnoreCase(VariableConstants.Blood_Pressure)) {
                    onPopUpMenuItemClick(itemTitle);
                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();
    }

    private void onClickBMI(String title, CustomFontTextView view) {
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (view == spinnerUnit2) {
            clearEditText();
            setFeetInchesView(title);
        } else if (view == spinnerUnit1) {
            clearEditText();
            setFeetInchesView(title);
        }
    }

    /**
     * Clear edit text for change BMI height unit
     */
    private void clearEditText() {
        edValue2.setText("");
        edValue3.setText("");
    }


    private void validateBMIandSubmitTask() {

        String unit1 = UnitsEnum.getCode(spinnerUnit1.getText().toString());
        String unit2 = UnitsEnum.getCode(spinnerUnit2.getText().toString());

        if (isValidValues(edValue1, unit1)) //valid height
        {
            if (isValidValues(edValue2, unit2))   //valid weight
            {
                boolean isSoftValidationWeightPass = isValuePassSoftValidation(observation,
                        edValue1.getTag().toString(),
                        unit1,
                        Double.parseDouble(edValue1.getText().toString()));


                boolean isSoftValidationHeightPass = isValuePassSoftValidation(observation,
                        edValue2.getTag().toString(),
                        unit2.equals(getString(R.string.ft_in_key)) ? getString(R.string.in_key) :
                                unit2,
                        spinnerUnit2.getText().toString().equals(getString(R.string.ft_in_key)) ?
                                (Float.parseFloat(edValue2.getText().toString()) * 12 + (edValue3.getText().toString().equals("") ? Float.parseFloat("0.00") : Float.parseFloat(edValue3.getText().toString()))) :
                                Double.parseDouble(edValue2.getText().toString()));


                if (isSoftValidationHeightPass && isSoftValidationWeightPass) //soft validation
                    submitTask(true);
                else {
                    submitTask(false);
                }

            }
        }
    }


    private void validateBPandSubmitTask() {
        String unit1 = UnitsEnum.getCode(spinnerUnit1.getText().toString());

        if (isValidValues(edValue1, unit1)) {
            if (isValidValues(edValue2, unit1)) {
                boolean isSoftValidationSystolicPass = isValuePassSoftValidation(observation,
                        edValue1.getTag().toString(),
                        unit1,
                        Double.parseDouble(edValue1.getText().toString()));
                boolean isSoftValidationDiastolicPass = isValuePassSoftValidation(observation,
                        edValue2.getTag().toString(),
                        unit1,
                        Double.parseDouble(edValue2.getText().toString()));
                if (isSoftValidationSystolicPass && isSoftValidationDiastolicPass)
                    submitTask(true);
                else {
                    submitTask(false);
                }
            }
        }
    }


    private void validateGlucoseandSubmitTask() {
        String unit1 = UnitsEnum.getCode(spinnerUnit1.getText().toString());

        if (rbValue1.isChecked() && isValidValues(edValue1, unit1)) {
            if (isValuePassSoftValidation(observation,
                    edValue1.getTag().toString(),
                    unit1,
                    Double.parseDouble(edValue1.getText().toString())))
                submitTask(true);
            else
                submitTask(false);
        } else if (rbValue2.isChecked() && isValidValues(edValue2, unit1)) {
            if (isValuePassSoftValidation(observation,
                    edValue2.getTag().toString(),
                    unit1,
                    Double.parseDouble(edValue2.getText().toString())))
                submitTask(true);
            else
                submitTask(false);
        } else if (rbValue3.isChecked() && isValidValues(edValue4, unit1)) {
            if (isValuePassSoftValidation(observation, edValue4.getTag().toString(), unit1, Double.parseDouble(edValue4.getText().toString()))) {
                submitTask(true);
            } else {
                submitTask(false);
            }
        }
    }

    /**
     * Handle  com.mphrx.fisike.customview.CustomFontButton clicks
     *
     * @param view
     */
    public void onClick(View view) {
        Utils.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.btnSubmit:
                stringValue1 = edValue1.getText().toString();
                stringValue2 = edValue2.getText().toString();
                stringValue3 = edValue3.getText().toString();
                stringValue4 = edValue4.getText().toString();
                if (observation.equals(vitalsConfig.getBMI().getName())) {
                    validateBMIandSubmitTask();
                } else if (observation.equals(vitalsConfig.getBloodPressure().getName())) {
                    validateBPandSubmitTask();
                } else if (observation.equals(vitalsConfig.getGlucose().getName())) {
                    validateGlucoseandSubmitTask();
                }
                break;
            case R.id.ll_date:
                showDatePickerDialog();
                break;
            case R.id.ll_time:
                //showDatePickerDialog();
                showTimePickerDialog();
                break;

            default:
                break;
        }
    }

    private boolean isValidValues(CustomFontEditTextView editTextView, String unitType) {

        String unit = unitType;
        //validating blank values
        if (!validateEmptyData(stringValue1, stringValue2, stringValue4)) {
            return false;
        } else {
            if (isValuePassHardValidation(observation, editTextView.getTag().toString(), unit.equals(getString(R.string.ft_in_key)) ? getString(R.string.in_key) :
                    unit, unit.equals(getString(R.string.ft_in_key)) ? ((Float.parseFloat(edValue2.getText().toString()) * 12) + (edValue3.getText().toString().equals("") ? Float.parseFloat("0.00") : Float.parseFloat(edValue3.getText().toString()))) : Double.parseDouble(editTextView.getText().toString()), unit.equals(getString(R.string.ft_in_key)) ? true : false, editTextView.getHint().toString())) {
                return true;
            }
        }


        return false;
    }

    private void submitTask(boolean isSoftValidationPass) {

        String time = "";
        //if time in 12 hr covert it to 24 h
        if (!DateFormat.is24HourFormat(this))
            time = DateTimeUtil.convertSourceDestinationDateEnglish(txtTime.getText().toString().trim(), DateTimeUtil.destinationTimeFormat, DateTimeUtil.sourceTimeFormat);
        else
            time = txtTime.getText().toString().trim();

        time = time + ":" + seconds;
        final String calculateDate = DateTimeUtil.convertSourceDestinationDateEnglish((txtDate.getText().toString().trim() + " " + time), DateTimeUtil.destinationDateFormatWithTime24Hour, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        this.observationType = observation;
        stringUnit1 = UnitsEnum.getCode(spinnerUnit1.getText().toString());
        final ArrayList<ObservationValueUnit> observationArray = new ArrayList<ObservationValueUnit>();
        if (stringValue1 != null && !stringValue1.equals("")) {
            observationArray.add(new ObservationValueUnit(stringValue1, stringUnit1, edValue1.getTag().toString()));
        }
        if (observation.equals(vitalsConfig.getBMI().getName())) {
            if (stringValue2 != null && !stringValue2.equals("")) {
                if (stringValue3 != null && !stringValue3.equals("") && !stringValue3.equals("0")) {
                    stringValue2 = String.valueOf(((Float.parseFloat(stringValue2) * 12) + Float.parseFloat(stringValue3)));
                } else {
                    String stringUnit2 = UnitsEnum.getCode(spinnerUnit2.getText().toString());
                    if (stringUnit2.equals(getString(R.string.ft_in_key))) {
                        stringValue2 = String.valueOf((Float.parseFloat(stringValue2) * 12));
                    }
                }
                String stringUnit2 = UnitsEnum.getCode(spinnerUnit2.getText().toString());
                observationArray.add(new ObservationValueUnit(stringValue2, stringUnit2.equals(getString(R.string.ft_in_key)) ? getString(R.string.in_key) : stringUnit2, edValue2.getTag().toString()));
            }
        } else {
            if (stringValue2 != null && !stringValue2.equals("")) {
                observationArray.add(new ObservationValueUnit(stringValue2, stringUnit1, edValue2.getTag().toString()));
            }
        }
        if (stringValue4 != null && !stringValue4.equals("")) {
            observationArray.add(new ObservationValueUnit(stringValue4, stringUnit1, edValue4.getTag().toString()));
        }

        if (observation.equals("BMI")) {
        } else if (observation.equals(VariableConstants.Blood_Pressure)) {
            //observationArray.get(0).setUnit(unit);
            double systolicval = Double.parseDouble(edValue1.getText().toString().trim());
            double diastolicval = Double.parseDouble(edValue2.getText().toString().trim());

            if (systolicval <= diastolicval) {
                DialogUtils.showAlertDialogCommon(this, this.getResources().getString(R.string.alert), this.getResources().getString(R.string.systolic_greater_msg), this.getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Dialog.BUTTON_POSITIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
                return;
            }

        }

        //        removed local validation on observations
        if (!isSoftValidationPass) {
            DialogUtils.showAlertDialogCommon(this, this.getResources().getString(R.string.alert), getString(R.string.hard_validation_error), getResources().getString(R.string.Continue), getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Dialog.BUTTON_POSITIVE:
                            submitTask(Utils.getPatientId(), observationType, calculateDate, observationArray, number);
                            dialog.dismiss();
                            break;
                        case Dialog.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            return;
        }
        submitTask(Utils.getPatientId(), observationType, calculateDate, observationArray, number);

    }

    @Override
    public boolean onSupportNavigateUp() {
        //setResult(RESULT_OK);
        finish();
        return super.onSupportNavigateUp();
    }

    private void submitTask(int patientId, final String observationType, final String calculateDate, final ArrayList<ObservationValueUnit> observationArray, final float number) {
        if (Utils.showDialogForNoNetwork(EnterObservationActivity.this, false)) {
            new ObservationSumitTask(EnterObservationActivity.this, Utils.getPatientId(), calculateDate, observationType, observationArray, number).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    private boolean validateData() {
        try {
            if (txtObservationType.getTag().toString().equalsIgnoreCase("Glucose")) {
                return true;
            }
            Float value1 = Float.parseFloat(stringValue1);
            Float value2 = Float.parseFloat(stringValue2);
            if (value1 > 0 && value2 > 0) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    /**
     * @param observationVal1
     * @param observationVal2
     * @return
     */
    private boolean validateEmptyData(String observationVal1, String observationVal2, String observationVal3) {
        if (txtObservationType.getTag().toString().equalsIgnoreCase("Blood Glucose") || txtObservationType.getTag().toString().equalsIgnoreCase("Glucose")) {
            if (observationVal1 != null && observationVal1.equals("") && observationVal2 != null && observationVal2.equals("") && observationVal3 != null && observationVal3.equals("")) {
                if (edValue1.hasFocus()) {
                    edValue1.setError(getResources().getString(R.string.enter_a_valid_value));
                    edValue2.getEditText().setEnabled(false);
                    edValue4.getEditText().setEnabled(false);
                    spinnerUnit2.setEnabled(false);
                } else if (edValue2.hasFocus()) {
                    edValue2.setError(getResources().getString(R.string.enter_a_valid_value));
                    edValue1.getEditText().setEnabled(false);
                    edValue4.getEditText().setEnabled(false);
                    spinnerUnit1.setEnabled(false);
                } else if (edValue4.hasFocus()) {
                    edValue4.setError(getResources().getString(R.string.enter_a_valid_value));
                    edValue1.getEditText().setEnabled(false);
                    edValue2.getEditText().setEnabled(false);
                    spinnerUnit1.setEnabled(false);
                    spinnerUnit2.setEnabled(false);
                }
                return false;
            }
            return true;
        }
        if (observationVal1 == null || observationVal1.equals("") || observationVal2 == null || observationVal2.equals("")) {
            if (observationVal1 != null && observationVal1.equals("")) {
                edValue1.setError(getResources().getString(R.string.enter_a_valid_value));
                return false;
            } else if (observationVal2 != null && observationVal2.equals("")) {
                edValue2.setError(getResources().getString(R.string.enter_a_valid_value));
                if (observation.equals(vitalsConfig.getBMI().getName()) && spinnerUnit2.getText().equals(getString(R.string.ft_in_key))) {
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Show Date Picker
     */
    public void showDatePickerDialog() {
        // Use the current date as the default date in the picker
        //  final Calendar c = Calendar.getInstance();
        Calendar c = Calendar.getInstance();// Aug28, 2015
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            c.setTime(sdf.parse(txtDate.getText().toString().trim()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, this, year, month, day);
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if (datePicker.isShown()) {
            txtDate.setText(String.format(getResources().getString(R.string.date), DateTimeUtil.
                    getFormattedDateWithoutUTCLocale(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated)));
            if (!isTodayDate) {
                if (DateFormat.is24HourFormat(EnterObservationActivity.this))
                    txtTime.setText(maxHour + ":" + maxMinute);
                else
                    txtTime.setText(DateTimeUtil.convertSourceDestinationDate(maxHour + ":" + maxMinute, DateTimeUtil.sourceTimeFormat, DateTimeUtil.destinationTimeFormat));
            }

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, day);
            isTodayDate = DateUtils.isToday(cal.getTimeInMillis()) ? true : false;
        }
    }

    /**
     * Show Time Picker
     */
    public void showTimePickerDialog() {
        Calendar c;
        String time;
        String Time = txtTime.getText().toString().replace(",", "").trim();
        time = Time.replace(", ", "");

        if (time != null || time != "")
            c = DateTimeUtil.createCalenderObjectFromTime(time);
        else
            c = Calendar.getInstance();

        // Use the current date as the default date in the picker
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        boolean is24Hour = DateFormat.is24HourFormat(EnterObservationActivity.this) ? true : false;

        timePickerDialog = new TimePickerDialog(this, this, hour, minute, is24Hour);
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        if (isTodayDate && (hourOfDay > maxHour || hourOfDay == maxHour && minute > maxMinute)) {
            Toast.makeText(EnterObservationActivity.this, getResources().getString(R.string.input_valid_time), Toast.LENGTH_SHORT).show();
            return;
        }
        if (timePicker.isShown()) {
            String displayHour = "" + hourOfDay;
            String displayMinute = "" + minute;
            if (hourOfDay < 10)
                displayHour = "0" + displayHour;
            if (minute < 10)
                displayMinute = "0" + displayMinute;

            if (DateFormat.is24HourFormat(EnterObservationActivity.this))
                txtTime.setText(displayHour + ":" + displayMinute);
            else
                txtTime.setText(DateTimeUtil.convertSourceDestinationDate(displayHour + ":" + displayMinute, DateTimeUtil.sourceTimeFormat, DateTimeUtil.destinationTimeFormat));
        }
    }

    /**
     * Handle the observation result
     *
     * @param result
     * @param convertedUnit
     * @param convertedValue
     * @param highRange
     * @param lowRange
     */
    public void setObservationGsonResult(boolean result, String convertedUnit, String convertedValue, String highRange, String lowRange) {
        if (result == false) {
            Toast.makeText(this, getResources().getString(R.string.api_error), Toast.LENGTH_SHORT).show();
        } else {
            //if (result.getObservationType() != null && result.getObservationType().equalsIgnoreCase("BMI")) {
            if (observationType.equalsIgnoreCase("BMI")) {

                showBMIAlertDialog(result, convertedUnit, convertedValue, highRange, lowRange);
                return;
            }
        }
        refreshRecords();
    }


    /**
     * Refresh records for activity result
     */
    private void refreshRecords() {
        Intent putExtra = getIntent().putExtra(VariableConstants.REFRESH_RECORDS, true);
        setResult(RESULT_OK, putExtra);
        finish();
    }

    /**
     * Show alert dialog for the observation submitted successfully
     *
     * @param result
     */
    private void showBMIAlertDialog(boolean result, String convertedUnit, String convertedValue, String highRange, String lowRange) {
        AlertDialog.Builder frequancydialog = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.observation_bmi_alert, null);
        CustomFontTextView txtBMIValue = (CustomFontTextView) view.findViewById(R.id.txtBMIValue);
        CustomFontTextView txtDescription = (CustomFontTextView) view.findViewById(R.id.txtDescription);
        CustomFontButton btnOK = (CustomFontButton) view.findViewById(R.id.continue_action);

        Resources resources = getResources();
        float number = Float.parseFloat(convertedValue);

        // txtBMIValue.setText(number + "");
        txtBMIValue.setVisibility(View.GONE);

        Typeface font_bold = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.opensans_ttf_bold));
        String firstPart = new String();
        String secondPart = new String();
        String newUnit = new String();

        if (convertedUnit.equalsIgnoreCase(TextConstants.BMI_KGM))
            convertedUnit = TextConstants.BMI_KGM2;
        if (convertedUnit.contains(TextConstants.BMI_SEPARATOR)) {
            String[] parts = convertedUnit.split("\\^");
            firstPart = parts[0];
            secondPart = parts[1];
            newUnit = firstPart + secondPart;
        } else
            newUnit = convertedUnit;
        String textString = getResources().getString(R.string.bmi_msg) + " " + number + " " + newUnit;
        SpannableString desc_styledString = new SpannableString(getResources().getString(R.string.bmi_msg) + " " + number + " " + newUnit);
        desc_styledString.setSpan(new AbsoluteSizeSpan(16, true), getResources().getString(R.string.bmi_msg).length(), desc_styledString.length(), 0);
        desc_styledString.setSpan(new CustomTypefaceSpan("", font_bold), getResources().getString(R.string.bmi_msg).length(), desc_styledString.length(), 0);
        if (convertedUnit.contains(TextConstants.BMI_SEPARATOR)) {
            desc_styledString.setSpan(new SuperscriptSpan(), textString.length() - 1, textString.length(), 0);
            desc_styledString.setSpan(new AbsoluteSizeSpan(12, true), textString.length() - 1, textString.length(), 0);
        }
        txtDescription.setText(desc_styledString);
      /*  if (number < Float.parseFloat(lowRange)) {
            txtDescription.setText(resources.
            getString(R.string.bmi_underweight));
        } else if (number >= Float.parseFloat(lowRange) && number <= Float.parseFloat(highRange)) {
            txtDescription.setText(resources.getString(R.string.bmi_healthy));
        } else if (number > Float.parseFloat(highRange)) {
            txtDescription.setText(resources.getString(R.string.bmi_overweight));
        }*/ /*else {
            imgBMIResult.setBackgroundResource(R.drawable.bmi_obese);
            txtDescription.setText(String.format(resources.getString(R.string.bmi_overweight), getMaxValue(number, (float) 24.9)));
            txtBMIValue.setTextColor(Color.RED);
        }*/

        frequancydialog.setView(view);
        frequancydialog.setCancelable(false);
        final AlertDialog show = frequancydialog.show();

        btnOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                show.cancel();
                refreshRecords();
            }
        });
    }

    private String getMinValue(float number, float min) {
        float minWeight = Float.parseFloat(stringValue1);
        if (stringUnit1.toString().equalsIgnoreCase("lb")) {
            minWeight = (float) (minWeight * ((float) 0.45));
        }
        return Utils.formatQuantity(Math.abs(((float) ((min / number) * minWeight) - minWeight)), 0);
    }

    private String getMaxValue(float number, float max) {
        float minWeight = Float.parseFloat(stringValue1);
        if (stringUnit1.toString().equalsIgnoreCase("lb")) {
            minWeight = (float) (Float.parseFloat(stringValue1) * 0.45);
        }
        return Utils.formatQuantity(Math.abs(((float) ((max / number) * minWeight) - minWeight)), 0);
    }

    private void initHardSoftValidationRange(String observationType, String observationParamName, String UnitType) {

        ArrayList<VitalObservationListItem> observationList;
        if (observationType.equalsIgnoreCase(vitalsConfig.getBMI().getName())) {
            observationList = vitalsConfig.getBMI().getObservationList();
        } else if (observationType.equalsIgnoreCase(vitalsConfig.getGlucose().getName())) {
            observationList = vitalsConfig.getGlucose().getObservationList();
        } else {
            observationList = vitalsConfig.getBloodPressure().getObservationList();
        }
        if (observationList != null && observationList.size() != 0)
            for (int i = 0; i < observationList.size(); i++) {
                if (observationList.get(i).getObservationName().equalsIgnoreCase(observationParamName.replaceAll(" ", ""))) {
                    try {
                        low = observationList.get(i).getValidationList().get(UnitType).getLow();
                        lowest = observationList.get(i).getValidationList().get(UnitType).getLowest();
                        high = observationList.get(i).getValidationList().get(UnitType).getHigh();
                        highest = observationList.get(i).getValidationList().get(UnitType).getHighest();
                        break;
                    } catch (Exception e) {
                        AppLog.d("matching", "no corresponding value" + "unittype");
                    }
                }
            }
    }

    private boolean isValuePassHardValidation(String observationType, String observationParamName, String UnitType, double value, boolean isftInch, String displayedParam) {

        initHardSoftValidationRange(observationType, observationParamName, UnitType);

        if (value < lowest) {
            showError(getString(isftInch ? R.string.soft_ft_invalidation_error : R.string.soft_validation_error, displayedParam, isftInch ? convertValuesInchtoFeet((int) lowest) : lowest, isftInch ? convertValuesInchtoFeet((int) highest) : highest));
            return false;
        } else if (value > highest) {
            showError(getString(isftInch ? R.string.soft_ft_invalidation_error : R.string.soft_validation_error, displayedParam, isftInch ? convertValuesInchtoFeet((int) lowest) : lowest, isftInch ? convertValuesInchtoFeet((int) highest) : highest));
            return false;
        } else
            return true;

    }

    private boolean isValuePassSoftValidation(String observationType, String observationParamName, String unit, double value) {
        String UnitType = unit;
        initHardSoftValidationRange(observationType, observationParamName, UnitType);
        if (value >= lowest && value < low) {
            return false;
        } else if (value <= highest && value > high) {
            return false;
        } else
            return true;

    }

    void showError(String errorMsg) {
        Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
    }


    public void setDecimalPlacesForEdvalue(final CustomFontEditTextView edValue, boolean isSet, final int decimalplaces, String selectedUnit) {
        if (isSet) {
            edValue.setKeyListener(DigitsKeyListener.getInstance(false, true));
            edValue.setFilters(new InputFilter[]{new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                int afterDecimal = decimalplaces;

                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    String temp = edValue.getText() + source.toString();

                    if (temp.equals(".")) {
                        return "0.";
                    } else if (temp.contains(".")) {
                        temp = temp.substring(temp.indexOf(".") + 1);
                        if (temp.length() > afterDecimal) {
                            return "";
                        }
                    }
                    return super.filter(source, start, end, dest, dstart, dend);
                }
            }});
        } else {
            edValue.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
            edValue.setFilters(new InputFilter[]{});

            if (selectedUnit.equals(getString(R.string.ft_in_key))) {
                edValue3.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
                edValue3.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (Integer.parseInt(s.toString().trim()) > 11) {
                                edValue3.setText(String.valueOf(s.charAt(0)));
                                edValue3.setSelection(1);
                                Toast.makeText(EnterObservationActivity.this, getString(R.string.inch_validation), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (s.toString().trim().length() == 0) {
                            try {
                                if (Integer.parseInt(s.toString().trim()) > 11) {
                                    edValue3.setText(String.valueOf(s.charAt(0)));
                                    edValue3.setSelection(1);
                                    Toast.makeText(EnterObservationActivity.this, getString(R.string.inch_validation), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {

                            }
                        }
                    }
                });
            }
        }
    }


    public void onPopUpMenuItemClick(String item) {
        switch (item) {
                          /*[    Blood Glucose units setup   ]*/
            case "mg/dL":
                setDecimalPlacesForEdvalue(edValue1, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue4, false, 0, String.valueOf(item));
                break;

            case "mmol/L":
                setDecimalPlacesForEdvalue(edValue1, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue4, false, 0, String.valueOf(item));
                break;

            case "mg/dl":
                setDecimalPlacesForEdvalue(edValue1, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue4, false, 0, String.valueOf(item));
                break;

            case "mmol/l":
                setDecimalPlacesForEdvalue(edValue1, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue4, false, 0, String.valueOf(item));
                break;
                          /*[    End of Blood Glucose units setup   ]*/



              /*[    Weight units setup   ]*/
            case "kg":
                setDecimalPlacesForEdvalue(edValue1, true, 1, String.valueOf(item));
                break;

            case "lb":
                setDecimalPlacesForEdvalue(edValue1, true, 1, String.valueOf(item));
                break;
              /*[    End of Weight units setup   ]*/




                     /*[    Height units setup   ]*/
            case "m":
                setDecimalPlacesForEdvalue(edValue2, true, 2, String.valueOf(item));
                break;

            case "ft":
                setDecimalPlacesForEdvalue(edValue2, true, 2, String.valueOf(item));
                break;

            case "in":
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                break;

            case "cm":
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                break;

            case "ft-in":
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));  // for feet
                setDecimalPlacesForEdvalue(edValue3, false, 0, String.valueOf(item));  // for inch
                break;
                        /*[    End of Height units setup   ]*/

             /*[    Blood Pressure units setup   ]*/
            case "mmHg":
                setDecimalPlacesForEdvalue(edValue1, false, 0, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, false, 0, String.valueOf(item));
                break;

            case "cmH2O":
                setDecimalPlacesForEdvalue(edValue1, true, 1, String.valueOf(item));
                setDecimalPlacesForEdvalue(edValue2, true, 1, String.valueOf(item));
                break;
                        /*[    End of Blood Pressure units setup   ]*/
             /*default handling*/
            default:
                break;
        }
    }

    public String convertValuesInchtoFeet(int anyInches) {
        int inches = anyInches;
        int feet = inches / 12;
        int leftoverInches = inches % 12;
        String convertd = feet + "'" + leftoverInches + "\"";
        System.out.println(inches + " inches =    " + feet + "'" + leftoverInches + "\"");
        return convertd;
    }
}