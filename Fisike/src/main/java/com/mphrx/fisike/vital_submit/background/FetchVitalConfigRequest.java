package com.mphrx.fisike.vital_submit.background;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.models.BmiSignupData;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONObject;

/**
 * Created by Neha on 02-12-2016.
 */

public class FetchVitalConfigRequest extends BaseObjectRequest {

    VitalsConfigMO configMO;
    private Context context;
    private long transactionId;
    private boolean isToSendBMICall=false;
    private boolean isToShowContactAdminScreen=false;
    private boolean isValidJson;

    public FetchVitalConfigRequest(Context context, long transactionId) {
        this.context = context;
        this.transactionId = transactionId;
    }

    public FetchVitalConfigRequest(Context context, long transactionId,boolean isToSendBMICall) {
        this.context = context;
        this.transactionId = transactionId;
        this.isToSendBMICall=isToSendBMICall;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getVitalsConfigUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url, null, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        SharedPref.setVitalConfigExecuted(false);
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        //firing Google Analytics event
        configMO=VitalsConfigMO.getInstance();
        if(configMO!=null)
            isToShowContactAdminScreen=false;
        else
            isToShowContactAdminScreen=true;

        BusProvider.getInstance().post(new VitalsConfigResponse(error, transactionId,isToShowContactAdminScreen));
    }

    private void submitBMIData() {
        BmiSignupData bmiSignupData = BmiSignupData.getInstance();
        //sending BMI call if recieved full BMI data
        if (isToSendBMICall && configMO!=null
                && configMO.getBMI()!=null && configMO.isToShowAddBMIButton()) {
            new ObservationSumitTask(context, bmiSignupData.getPatientId(), bmiSignupData.getDate(), configMO.getBMI().getName(),
                    bmiSignupData.getObservationArray(), bmiSignupData.getNumber(), false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        try {

            if(Utils.isValueAvailable(response.toString()))
                isValidJson=true;
            else
                isValidJson=false;

            configMO = VitalsConfigMO.parseObject(response.toString());
            if (isToSendBMICall) {
                submitBMIData();
            }
                VitalsConfigDBAdapter.getInstance(context).insertVitalConfig(configMO);
                SharedPref.setVitalConfigExecuted(true);
                if(configMO.isValidVitalsFetched())
                    isToShowContactAdminScreen=false;
                else
                    isToShowContactAdminScreen=true;

        } catch (Exception e) {
            //success but error in response
             if(configMO.isValidVitalsFetched())
                isToShowContactAdminScreen=false;
            else
                isToShowContactAdminScreen=true;
            e.printStackTrace();
        }
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new VitalsConfigResponse(response, transactionId,isToShowContactAdminScreen,isValidJson));
    }


}
