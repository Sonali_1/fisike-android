package com.mphrx.fisike.vital_submit.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DiagnosticObservations  {

    @SerializedName("code")
    private DiagnosticObservationCode code;
    @SerializedName("identifier")
    private ArrayList<DiagnosticIdentifier> identifier;
    @SerializedName("issuedDate")
    private String issuedDate;
    @SerializedName("value")
    private ObservationValue value;

    public DiagnosticObservationCode getCode() {
        return code;
    }

    public void setCode(DiagnosticObservationCode code) {
        this.code = code;
    }

    public ArrayList<DiagnosticIdentifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(ArrayList<DiagnosticIdentifier> identifier) {
        this.identifier = identifier;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public ObservationValue getValue() {
        return value;
    }

    public void setValue(ObservationValue value) {
        this.value = value;
    }
}
