package com.mphrx.fisike.vital_submit.VitalsConfigGson;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class Vital implements Parcelable {

    private String name;
    private String diplayUnit;
    private String bmiSystemCode;
    private ArrayList<VitalObservationListItem> observationList;
    private String name_key = "name";
    private String diplayUnit_key = "displayUnit";
    private String bmiSystemCode_key = "bmiSystemCode";
    private LinkedHashMap<String, BMIListParams> bmiListRestrictionMap;
    private String bmiUnitRestrictions_key = "bmiUnitRestrictions";
    private String observationList_key = "observationList";
    private String height_key = "height";
    private String weight_key = "weight";
    private String systolic_key = "Systolic";
    private String diastolic_key = "Diastolic";
    private String glucose_random_key = "Random";
    private String glucose_fasting_key = "Fasting";
    private String glucose_after_meal_key = "AfterMeal";


    public Vital(String jsonToParse) {
        // TODO Auto-generated constructor stub
        if (!(jsonToParse == null || jsonToParse.equals("null") || jsonToParse.equals("")))
            parseObservation(jsonToParse);
    }

    public Vital(Parcel in) {
        name = in.readString();
        diplayUnit = in.readString();
        bmiSystemCode = in.readString();
        observationList = in.readArrayList(ClassLoader.getSystemClassLoader());
        bmiListRestrictionMap = (LinkedHashMap<String, BMIListParams>) in.readHashMap(ClassLoader.getSystemClassLoader());
    }

    private void parseObservation(String jsonToParse) {
        JSONObject observationJson;
        try {
            observationJson = new JSONObject(jsonToParse);
            name = observationJson.getString(name_key);
            diplayUnit = observationJson.optString(diplayUnit_key);
            bmiSystemCode = observationJson.optString(bmiSystemCode_key);
            JSONObject bmiUnitKeys = observationJson.optJSONObject(bmiUnitRestrictions_key);
            if (bmiUnitKeys != null) {
                bmiListRestrictionMap = new LinkedHashMap<>();
                for (Iterator<String> valiationListIterator = bmiUnitKeys.keys(); valiationListIterator.hasNext(); ) {
                    String key = valiationListIterator.next();
                    bmiListRestrictionMap.put(key, new BMIListParams(bmiUnitKeys.getString(key)));
                }
            }
            JSONArray observationListArray = observationJson.getJSONArray(observationList_key);
            observationList = new ArrayList<>();
            for (int i = 0; i < observationListArray.length(); i++) {
                observationList.add(new VitalObservationListItem(observationListArray.get(i).toString()));
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiplayUnit() {
        return diplayUnit;
    }

    public void setDiplayUnit(String diplayUnit) {
        this.diplayUnit = diplayUnit;
    }

    public String getBmiSystemCode() {
        return bmiSystemCode;
    }

    public void setBmiSystemCode(String bmiSystemCode) {
        this.bmiSystemCode = bmiSystemCode;
    }

    public ArrayList<VitalObservationListItem> getObservationList() {
        return observationList;
    }

    public void setObservationList(ArrayList<VitalObservationListItem> observationList) {
        this.observationList = observationList;
    }


    @Override
    public String toString() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(name_key, getName());
            jsonObject.put(diplayUnit_key, getDiplayUnit());
            jsonObject.put(bmiSystemCode_key, getBmiSystemCode());
            JSONArray array = new JSONArray();
            for (int i = 0; i < getObservationList().size(); i++) {
                array.put(getObservationList().get(i));
            }
            jsonObject.put(observationList_key, array);
            JSONObject object = new JSONObject();
            int count = 0;
            if (getBmiListRestrictionMap() != null) {
                for (Map.Entry<String, BMIListParams> entry : getBmiListRestrictionMap().entrySet()) {
                    count = count + 1;
                    object.put(entry.getKey(), entry.getValue());
                }
            }
            if (count > 0)
                jsonObject.put(bmiUnitRestrictions_key, object);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }

    public VitalObservationListItem getObservationFromDisplayName(String value) {
        VitalObservationListItem item = null;
        for (int i = 0; i < getObservationList().size(); i++) {
            if (getObservationList().get(i).getObservationName().equalsIgnoreCase(value)) {
                item = getObservationList().get(i);
                break;
            }
        }
        return item;
    }

    public VitalObservationListItem getBMIHeight() {
        return getObservationFromDisplayName(height_key);
    }

    public VitalObservationListItem getBPSYstolic() {
        return getObservationFromDisplayName(systolic_key);
    }

    public VitalObservationListItem getBPDystolic() {
        return getObservationFromDisplayName(diastolic_key);
    }

    public VitalObservationListItem getGlucoseFasting() {
        return getObservationFromDisplayName(glucose_fasting_key);
    }

    public VitalObservationListItem getGlucoseRandom() {
        return getObservationFromDisplayName(glucose_random_key);
    }

    public VitalObservationListItem getGlucoseAfterMeal() {
        return getObservationFromDisplayName(glucose_after_meal_key);
    }

    public VitalObservationListItem getBMIWeight() {
        return getObservationFromDisplayName(weight_key);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getName());
        dest.writeString(getDiplayUnit());
        dest.writeString(getBmiSystemCode());
        dest.writeList(getObservationList());
        dest.writeMap(getBmiListRestrictionMap());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Vital createFromParcel(Parcel in) {
            return new Vital(in);
        }

        public Vital[] newArray(int size) {
            return new Vital[size];
        }
    };

    public LinkedHashMap<String, BMIListParams> getBmiListRestrictionMap() {
        return bmiListRestrictionMap;
    }

    public void setBmiListRestrictionMap(LinkedHashMap<String, BMIListParams> bmiListRestrictionMap) {
        this.bmiListRestrictionMap = bmiListRestrictionMap;
    }
}
