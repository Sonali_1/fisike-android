package com.mphrx.fisike.interfaces;

/**
 * Created by administrate on 11/27/2015.
 */
public interface NotifyFabIconToDisplay {

    public void setFabIconVisibility(boolean value);
}
