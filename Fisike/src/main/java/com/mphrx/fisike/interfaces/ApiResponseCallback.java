package com.mphrx.fisike.interfaces;

public interface ApiResponseCallback<Result> {
    public void onSuccessRespone(Result result);

    public void onFailedResponse(Exception exception);

}
