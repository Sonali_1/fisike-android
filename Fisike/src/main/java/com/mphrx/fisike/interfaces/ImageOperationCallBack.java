package com.mphrx.fisike.interfaces;

import java.io.File;

import android.content.Intent;

public interface ImageOperationCallBack {
    public void removeProfilePic();
    public void updateProfilePic(Intent intent, int returnCode, File uri);
}
