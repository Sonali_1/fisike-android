package com.mphrx.fisike.interfaces;

import com.mphrx.fisike.utils.EndlessScrollView;

/**
 * Created by laxmansingh on 6/3/2016.
 */
public interface EndlessScrollListener {
    void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy);
}
