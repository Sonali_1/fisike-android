package com.mphrx.fisike.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

import android.util.Log;

import com.mphrx.fisike.provider.ChatIQ.From;
import com.mphrx.fisike.provider.ChatIQ.Set;
import com.mphrx.fisike.provider.ChatIQ.To;
import com.mphrx.fisike_physician.utils.AppLog;

public class ChatIQProvider implements IQProvider {

    public ChatIQProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {
        AppLog.d("CHAT IQ PROVIDER", String.format("Received iq packet, namespace[%s], name[%s]", parser.getNamespace(), parser.getName()));
        ChatIQ iq = new ChatIQ();
        ChatIQ.Set set = new Set();
        boolean done = false;

        From from = new From();
        To to = new To();
        String secs = "", jid = "";
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getName().equals("from")) {
                    secs = parser.getAttributeValue("", "secs");
                    jid = parser.getAttributeValue("", "jid");

                    to = null;
                    from = new From();
                    from.setJid(jid);
                    from.setSecs(secs);

                    iq.addFrom(from);
                } else if (parser.getName().equals("to")) {
                    secs = parser.getAttributeValue("", "secs");
                    jid = parser.getAttributeValue("", "jid");

                    from = null;
                    to = new To();
                    to.setJid(jid);
                    to.setSecs(secs);

                    iq.addTos(to);
                } else if (parser.getName().equals("body")) {
                    ChatIQ.Body body = new ChatIQ.Body(parser.nextText());
                    if (from != null) {
                        from.setBody(body);
                    } else if (to != null) {
                        to.setBody(body);
                    }
                } else if (parser.getName().equals("first")) {
                    int index = parseInt(parser.getAttributeValue("", "index"));
                    set.setIndexAtt(index);
                    int first = parseInt(parser.nextText());
                    set.setFirst(first);
                } else if (parser.getName().equals("last")) {
                    int last = parseInt(parser.nextText());
                    set.setLast(last);
                } else if (parser.getName().equals("count")) {
                    int count = parseInt(parser.nextText());
                    set.setCount(count);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("chat")) {
                    iq.setSet(set);
                    done = true;
                }
            }
        }
        return iq;
    }

    private int parseInt(String integer) {
        return Integer.parseInt((integer != null ? integer : "0"));
    }
}
