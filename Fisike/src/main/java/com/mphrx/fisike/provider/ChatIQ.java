package com.mphrx.fisike.provider;

import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.packet.IQ;

public class ChatIQ extends IQ {

    private String xmlns;
    private String with;
    private String start;

    private List<From> froms;
    private List<To> tos;
    private Set set;

    public ChatIQ() {
        this.froms = new ArrayList<ChatIQ.From>();
        this.tos = new ArrayList<ChatIQ.To>();
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getWith() {
        return with;
    }

    public void setWith(String with) {
        this.with = with;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void addFrom(From from) {
        froms.add(from);
    }

    public List<From> getFroms() {
        return froms;
    }

    public void addTos(To to) {
        tos.add(to);
    }

    public List<To> getTos() {
        return tos;
    }

    public Set getSet() {
        return set;
    }

    public void setSet(Set set) {
        this.set = set;
    }

    @Override
    public String getChildElementXML() {
        StringBuilder builder = new StringBuilder("<chat xmlns=\"urn:xmpp:archive\"");
        builder.append("with=\"").append(with).append("\"");
        builder.append(" start=\"");
        builder.append(start);
        builder.append("\">");

        int messagesSize = froms.size() + tos.size();
        int fromIndex = -1, toIndex = -1;
        int fromSize = froms.size(), toSize = tos.size();

        if (froms.size() > 0) {
            fromIndex = 0;
        }
        if (tos.size() > 0) {
            toIndex = 0;
        }

        for (int i = 0; i < messagesSize; i++) {
            if (fromSize == fromIndex - 1 && toSize == toIndex - 1) {
                break;
            }
            From from;
            To to;
            // from is not there
            if (fromSize == 0 || (fromSize <= fromIndex && fromIndex > -1)) {
                to = tos.get(toIndex);
                builder.append(to.toXml());
                toIndex++;
                continue;
            }
            // to is not available
            if (toSize == 0 || (toSize <= toIndex && toIndex > -1)) {
                from = froms.get(fromIndex);
                builder.append(from.toXml());
                fromIndex++;
                continue;
            }

            from = froms.get(fromIndex);
            to = tos.get(toIndex);

            if (Integer.parseInt(from.getSecs()) > Integer.parseInt(to.getSecs())) {
                builder.append(to.toXml());
                toIndex++;
            } else {
                builder.append(from.toXml());
                fromIndex++;
            }
        }

        // for (From from : froms) {
        // builder.append(from.toXml());
        // }
        // for (To to : tos) {
        // builder.append(to.toXml());
        // }

        builder.append(set.toXml());
        builder.append("</chat>");
        return builder.toString();
    }

    public static class From {
        private String secs;
        private String jid;
        private Body body;

        public String getSecs() {
            return secs;
        }

        public void setSecs(String secs) {
            this.secs = secs;
        }

        public String getJid() {
            return jid;
        }

        public void setJid(String jid) {
            this.jid = jid;
        }

        public Body getBody() {
            return body;
        }

        public void setBody(Body body) {
            this.body = body;
        }

        public String toXml() {
            StringBuilder builder = new StringBuilder("<from ");
            builder.append("secs=\"").append(secs).append("\" ");
            builder.append("jid=\"").append(jid).append("\" >");
            builder.append(body.toXml());
            builder.append("</from>");
            return builder.toString();
        }
    }

    public static class To {
        private String secs;
        private String jid;
        private Body body;

        public String getSecs() {
            return secs;
        }

        public void setSecs(String secs) {
            this.secs = secs;
        }

        public String getJid() {
            return jid;
        }

        public void setJid(String jid) {
            this.jid = jid;
        }

        public Body getBody() {
            return body;
        }

        public void setBody(Body body) {
            this.body = body;
        }

        public String toXml() {
            StringBuilder builder = new StringBuilder("<to ");
            builder.append("secs=\"").append(secs).append("\" ");
            builder.append("jid=\"").append(jid).append("\" >");
            builder.append(body.toXml());
            builder.append("</to>");
            return builder.toString();
        }
    }

    public static class Body {
        private String message;

        public Body(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object toXml() {
            StringBuilder builder = new StringBuilder("<body>");
            builder.append(message);
            builder.append("</body>");
            return builder.toString();
        }
    }

    public static class Set {
        private int last;
        private int count;
        private int indexAtt;
        private int first;

        public Set() {
        }

        public int getLast() {
            return last;
        }

        public void setLast(int last) {
            this.last = last;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getIndexAtt() {
            return indexAtt;
        }

        public void setIndexAtt(int indexAtt) {
            this.indexAtt = indexAtt;
        }

        public int getFirst() {
            return first;
        }

        public void setFirst(int first) {
            this.first = first;
        }

        public String toXml() {
            StringBuilder builder = new StringBuilder("<set xmlns=\"http://jabber.org/protocol/rsm\">");
            builder.append("<first index=\"").append(indexAtt).append("\">").append(first).append("</first>");
            builder.append("<last>").append(last).append("</last>");
            builder.append("<count>").append(count).append("</count>");
            builder.append("</set>");
            return builder.toString();
        }
    }

}