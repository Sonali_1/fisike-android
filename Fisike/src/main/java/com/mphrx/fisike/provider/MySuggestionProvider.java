package com.mphrx.fisike.provider;

import android.content.SearchRecentSuggestionsProvider;

import com.mphrx.fisike.BuildConfig;

import java.util.ArrayList;

public class MySuggestionProvider extends SearchRecentSuggestionsProvider
 {

    public final static String AUTHORITY = getAuthority();

    public final static int MODE = DATABASE_MODE_QUERIES;

    public MySuggestionProvider()
    {
        setupSuggestions(AUTHORITY, MODE);

    }

     private static String getAuthority() {

         String authority = ".provider.MySuggestionProvider";;

         if(BuildConfig.DEBUG)
         {
             String[] list = (BuildConfig.APPLICATION_ID).split(".debug");
             return list[0] + authority;
         }
         else
         {
             return BuildConfig.APPLICATION_ID + authority;
         }
     }
}
