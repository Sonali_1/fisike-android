package com.mphrx.fisike.provider;

import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;

public class AttachmentTag implements PacketExtension {
    public static final String NAMESPACE = "urn:xmpp:receipts";
    public static final String ELEMENT = "received";

    private String id; // / original ID of the delivered message

    public AttachmentTag(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String toXML() {
        return "<received xmlns='" + NAMESPACE + "' id='" + id + "'/>";
    }

    /**
     * This Provider parses and returns DeliveryReceipt packets.
     */
    public static class Provider extends EmbeddedExtensionProvider {

        @Override
        protected PacketExtension createReturnExtension(String currentElement, String currentNamespace, Map<String, String> attributeMap, List<? extends PacketExtension> content) {
            return new AttachmentTag(attributeMap.get("id"));
        }
    }
}