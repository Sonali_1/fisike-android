package com.mphrx.fisike.provider;

import java.io.StringWriter;

import org.jivesoftware.smack.packet.Packet;

import android.util.Log;

import com.mphrx.fisike_physician.utils.AppLog;

public class ExtendedPackect extends Packet {

    @Override
    public String toXML() {
        StringWriter writer = new StringWriter();
        // Serializer serializer = new Persister();
        try {
            // serializer.write(iq, writer);
            return writer.getBuffer().toString();
        } catch (Exception e) {
            AppLog.e("COMPANY", "Error serializing xml", e);
        }
        return null;
    }

}
