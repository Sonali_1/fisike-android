package com.mphrx.fisike.mo;

public class NotificationCenter {
	private int _Id;
	private String notificationType;
	private String medicianName;
	private String medicationTime;
	private String medicationDate;
	private String medicianQuantity;
	private String encounterId;
	private String documentId;
	private String documentStatus;
	private String documentType;
	private boolean isRead;
	private int unreadCount;
	private String refId;
	private String organizationId;
	private String organizationName;

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public int get_Id() {
		return _Id;
	}

	public void set_Id(int _Id) {
		this._Id = _Id;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getMedicianName() {
		return medicianName;
	}

	public void setMedicianName(String medicianName) {
		this.medicianName = medicianName;
	}

	public String getMedicationTime() {
		return medicationTime;
	}

	public void setMedicationTime(String medicationTime) {
		this.medicationTime = medicationTime;
	}

	public String getMedicationDate() {
		return medicationDate;
	}

	public void setMedicationDate(String medicationDate) {
		this.medicationDate = medicationDate;
	}

	public String getMedicianQuantity() {
		return medicianQuantity;
	}

	public void setMedicianQuantity(String medicianQuantity) {
		this.medicianQuantity = medicianQuantity;
	}

	public String getEncounterId() {
		return encounterId;
	}

	public void setEncounterId(String encounterId) {
		this.encounterId = encounterId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public int getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(int unreadCount) {
		this.unreadCount = unreadCount;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
}
