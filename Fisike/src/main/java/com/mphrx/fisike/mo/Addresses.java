package com.mphrx.fisike.mo;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.Line;

import org.xbill.DNS.Serial;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aastha on 7/13/2017.
 */

class Addresses implements Serializable{

    @Expose
    private String state;
    @Expose
    private String country;
    @Expose
    private ArrayList<String> line;
    @Expose
    private String text;
    @Expose
    private String city;
    @Expose
    private String postalCode;
    @Expose
    private String useCode;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<String> getLine() {
        return line;
    }

    public void setLine(ArrayList<String> line) {
        this.line = line;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }
}
