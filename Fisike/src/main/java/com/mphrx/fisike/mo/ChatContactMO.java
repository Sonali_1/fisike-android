package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike_physician.models.Experience;
import com.mphrx.fisike_physician.models.Role;
import com.mphrx.fisike_physician.models.Speciality;
import com.mphrx.fisike_physician.utils.TextPattern;

import java.util.ArrayList;

public class ChatContactMO extends MobileObject {

    @Expose
    private String id;
    @Expose
    private String username;
    @Expose
    private String email;
    @Expose
    private String firstName;
    @Expose
    private String lastName;
    @Expose
    private String phoneNo;
    @Expose
    private String gender;
    @Expose
    private String dob;
    @Expose
    private Boolean enabled;
    @Expose
    private Weight weight;
    @Expose
    private Height height;
    @Expose
    private Boolean accountLocked;
    @Expose
    private long physicianId;
    @Expose
    private long patientId;
    // @Expose
    // private ArrayList<Authority> authorities;
    // @Expose
    // private ArrayList<UserGroup> userGroups;
    @Expose
    private UserType userType;

    // same as persitence key of group
    private ArrayList<String> joinedGroupMOPK;
    private Boolean isAdmin; // only for showing data

    /**
     * Old attributes
     */

    private ArrayList<Speciality> speciality;
    private String designation;
    private Boolean isSelected;
    private String membershipStatus;
    private String aboutMe;
    private String presenceStatus;
    private String presenceType;
    private boolean isFisikeContact;
    private ArrayList<Experience> experience;
    private Role role;

    public ChatContactMO() {
        super();
    }

    public static String getPersistenceKey(String fromUserName) {
        // FIXME CRITICAL: should PKEY id have @ip attached or not
        String uniqueString = "ChatContactMO" + TextPattern.getUserId(fromUserName);

        return uniqueString.hashCode() + "";
    }

    public ArrayList<String> getJoinedGroupMOPK() {
        return joinedGroupMOPK;
    }

    public void setJoinedGroupMOPK(ArrayList<String> joinedGroupMOPK) {
        this.joinedGroupMOPK = joinedGroupMOPK;
    }

    public String generateJoinedGroupMOPK(String groupID) {
        String uniqueString = "GroupTextChatMo" + groupID;
        return uniqueString.hashCode() + "";
    }

    public Boolean getIsSelected() {
        if (isSelected == null) {
            isSelected = false;
        }
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public byte[] getProfilePic() {
        return null;
    }

    public void setProfilePic(byte[] profilePic) {
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        // if (email != null) {
        // try {
        // this.jid = URLEncoder.encode(email, "UTF-8") + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
        // } catch (UnsupportedEncodingException e) {
        // }
        // }
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    public String getRealName() {
        if (lastName == null || lastName.equals("null") || lastName.equals("")) {
            return firstName;
        }
        return firstName + " " + lastName;
    }

    // public void setRealName(String realName) {
    // this.realName = realName;
    // }

    public String getSpeciality() {
        if (getSpecialityList().size() == 0)
            return "";

        return getSpecialityList().get(0).getText();
    }

    public ArrayList<Speciality> getSpecialityList() {
        if (this.speciality == null)
            return Speciality.EMPTY;

        return speciality;
    }

    public void setSpeciality(ArrayList<Speciality> speciality) {
        this.speciality = speciality;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public void generatePersistenceKey(String fromUserName) {
        // FIXME CRITICAL: should PKEY id have @ip attached or not
        String uniqueString = "ChatContactMO" + TextPattern.getUserId(fromUserName);
        setPersistenceKey(uniqueString.hashCode());
    }

    public String getPresenceStatus() {
        return presenceStatus;
    }

    public void setPresenceStatus(String presenceStatus) {
        if (presenceStatus == null || presenceStatus.equals("null")) {
            presenceStatus = "";
        }
        this.presenceStatus = presenceStatus;
    }

    public String getPresenceType() {
        return presenceType;
    }

    public void setPresenceType(String presenceType) {
        if (presenceType == null || presenceType.equals("null")) {
            presenceType = TextConstants.STATUS_AVAILABLE_TEXT;
        }
        this.presenceType = presenceType;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        } else {
            ChatContactMO chatContactMO = (ChatContactMO) object;
            String id = TextPattern.getUserId(chatContactMO.getId());
            if (id.equals(TextPattern.getUserId(this.getId()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getEmail().hashCode();
    }

    public Boolean equalsProfilePic(byte[] profilePic) {
        return false;
    }

    public Boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if (lastName == null || lastName.equalsIgnoreCase("null")) {
            return "";
        }
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null || lastName.equalsIgnoreCase("null")) {
            this.lastName = "";
            return;
        }
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Height getHeight() {
        return height;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public Boolean isAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public long getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(long physicianId) {
        this.physicianId = physicianId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    // public ArrayList<Authority> getAuthorities() {
    // return authorities;
    // }
    //
    // public void setAuthorities(ArrayList<Authority> authorities) {
    // this.authorities = authorities;
    // }
    //
    // public ArrayList<UserGroup> getUserGroups() {
    // return userGroups;
    // }
    //
    // public void setUserGroups(ArrayList<UserGroup> userGroups) {
    // this.userGroups = userGroups;
    // }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    protected ChatContactMO(Parcel in) {
        super(in);
        id = in.readString();
        username = in.readString();
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        phoneNo = in.readString();
        gender = in.readString();
        dob = in.readString();
        byte isEnableVal = in.readByte();
        enabled = isEnableVal == 0x02 ? false : isEnableVal != 0x00;
        weight = in.readParcelable(Weight.class.getClassLoader());
        height = in.readParcelable(Height.class.getClassLoader());
        byte accountLockedVal = in.readByte();
        accountLocked = accountLockedVal == 0x02 ? false : accountLockedVal != 0x00;
        physicianId = in.readLong();
        patientId = in.readLong();

        aboutMe = in.readString();
        membershipStatus = in.readString();
        speciality = in.readArrayList(Speciality.class.getClassLoader());
        designation = in.readString();
        byte isAdminVal = in.readByte();
        isAdmin = isAdminVal == 0x02 ? false : isAdminVal != 0x00;
        presenceType = in.readString();
        presenceStatus = in.readString();

        byte isSelecetdVal = in.readByte();
        isSelected = isSelecetdVal == 0x02 ? false : isSelecetdVal != 0x00;
        if (in.readByte() == 0x01) {
            joinedGroupMOPK = new ArrayList<String>();
            in.readList(joinedGroupMOPK, String.class.getClassLoader());
        } else {
            joinedGroupMOPK = null;
        }

        userType = in.readParcelable(UserType.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(phoneNo);
        dest.writeString(gender);
        dest.writeString(dob);
        if (enabled == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (enabled ? 0x01 : 0x00));
        }
        dest.writeParcelable(new Weight(), flags);
        dest.writeParcelable(new Height(), flags);
        if (accountLocked == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (accountLocked ? 0x01 : 0x00));
        }
        dest.writeLong(physicianId);
        dest.writeLong(patientId);
        dest.writeString(aboutMe);
        dest.writeString(membershipStatus);
        dest.writeList(speciality);
        dest.writeString(designation);
        if (isAdmin == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isAdmin ? 0x01 : 0x00));
        }
        dest.writeString(presenceType);
        dest.writeString(presenceStatus);
        if (isSelected == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isSelected ? 0x01 : 0x00));
        }
        if (joinedGroupMOPK == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(joinedGroupMOPK);
        }
        dest.writeParcelable(new UserType(), flags);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ChatContactMO> CREATOR = new Parcelable.Creator<ChatContactMO>() {
        @Override
        public ChatContactMO createFromParcel(Parcel in) {
            return new ChatContactMO(in);
        }

        @Override
        public ChatContactMO[] newArray(int size) {
            return new ChatContactMO[size];
        }
    };

    public String getDisplayName() {
        if (!TextUtils.isEmpty(firstName)) {
            String out = firstName;
            if (!TextUtils.isEmpty(lastName)) {
                out = out + " " + lastName;
            }
            return out;
        } else if (!TextUtils.isEmpty(username)) {
            return username;
        } else if (!TextUtils.isEmpty(email)) {
            return email;
        }
        return "";
    }

    public void setFisikeContact(boolean flag) {
        isFisikeContact = flag;
    }

    public boolean isFisikeContact() {
        return isFisikeContact;
    }

    public ArrayList<Experience> getExperience() {
        return experience;
    }

    public void setExperience(ArrayList<Experience> experience) {
        this.experience = experience;
    }

    public String getExperienceSingle() {
        if (getExperience() == null || getExperience().isEmpty())
            return "";

        return getExperience().get(0).getNoOfExp();
    }

    public void setExperienceFromSingle(String string) {
        if (TextUtils.isEmpty(string))
            return;
        Experience sp = new Experience();
        sp.setNoOfExp(string);
        ArrayList<Experience> arr = new ArrayList<>(1);
        arr.add(sp);
        setExperience(arr);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getRoleSingle() {
        if (getRole() == null || getRole().equals("null") || getRole().equals(""))
            return "";

        return getRole().getText();
    }

    public void setRoleFromSingle(String string) {
        if (TextUtils.isEmpty(string))
            return;
        Role sp = new Role();
        sp.setText(string);
//        ArrayList<Role> arr = new ArrayList<>(1);
//        arr.add(sp);
        setRole(sp);
    }

    public void setSpecialityFromSingle(String string) {
        if (TextUtils.isEmpty(string))
            return;
        Speciality sp = new Speciality();
        sp.setText(string);
        ArrayList<Speciality> arr = new ArrayList<>(1);
        arr.add(sp);
        setSpeciality(arr);
    }
}
