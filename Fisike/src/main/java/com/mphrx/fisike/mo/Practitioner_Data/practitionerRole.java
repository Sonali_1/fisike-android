package com.mphrx.fisike.mo.Practitioner_Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Neha on 23-12-2016.
 */

public class practitionerRole implements Serializable {
    private static final long serialVersionUID = 7006459162908038561L;
    Object healthcareService;
    Object period;
    ArrayList<Speciality> specialty;
    Object managingOrganization;
    Role role;

    public Object getHealthcareService() {
        return healthcareService;
    }

    public Object getPeriod() {
        return period;
    }

    public ArrayList<Speciality> getSpecialty() {
        return specialty;
    }

    public Object getManagingOrganization() {
        return managingOrganization;
    }

    public Role getRole() {
        return role;
    }

    public void setHealthcareService(Object healthcareService) {
        this.healthcareService = healthcareService;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public void setSpecialty(ArrayList<Speciality> specialty) {
        this.specialty = specialty;
    }

    public void setManagingOrganization(Object managingOrganization) {
        this.managingOrganization = managingOrganization;
    }

    public void setRole(Role role) {
        this.role = role;
    }


/*    protected practitionerRole(Parcel in) {
        period=in.readValue(ClassLoader.getSystemClassLoader());
        specialty = in.createTypedArrayList(Speciality.CREATOR);
        role = in.readParcelable(Role.class.getClassLoader());
        healthcareService=in.readValue(ClassLoader.getSystemClassLoader());
        managingOrganization=in.readValue(ClassLoader.getSystemClassLoader());
    }

    public static final Creator<practitionerRole> CREATOR = new Creator<practitionerRole>() {
        @Override
        public practitionerRole createFromParcel(Parcel in) {
            return new practitionerRole(in);
        }

        @Override
        public practitionerRole[] newArray(int size) {
            return new practitionerRole[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(period);
        dest.writeTypedList(specialty);
        dest.writeParcelable(role, flags);
        dest.writeValue(healthcareService);
        dest.writeValue(managingOrganization);
    }*/
}
