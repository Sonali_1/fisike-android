package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class DiseaseMO implements Serializable, Parcelable {
    private static final long serialVersionUID = -8634562629296862567L;

    private Object desc;
    private Object code;
    private Object display;
    private Integer id;
    private Object text;
    private Object version;
    private Object system;

    public DiseaseMO(Object diseaseName, Object diseaseCode, Object diseaseCodingSystem) {
        this.desc = diseaseName;
        this.system = diseaseCodingSystem;
        this.code = diseaseCode;
        this.text = diseaseName;
    }


    public DiseaseMO(Parcel in) {
        readFromParcel(in);
    }


    public Object getSystem() {
        return system;
    }

    public void setSystem(Object system) {
        this.system = system;
    }

    public Object getVersion() {
        return version;
    }

    public void setVersion(Object version) {
        this.version = version;
    }

    public Object getDesc() {
        return desc;
    }

    public void setDesc(Object desc) {
        this.desc = desc;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getDisplay() {
        return display;
    }

    public void setDisplay(Object display) {
        this.display = display;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getText() {
        return text;
    }

    public void setText(Object text) {
        this.text = text;
    }


    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeValue(desc);
        dest.writeValue(code);
        dest.writeValue(display);
        dest.writeInt(id);
        dest.writeValue(text);
        dest.writeValue(version);
        dest.writeValue(system);


    }

    private void readFromParcel(Parcel in) {
        desc = in.readValue(DiseaseMO.class.getClassLoader());
        code = in.readValue(DiseaseMO.class.getClassLoader());
        display = in.readValue(DiseaseMO.class.getClassLoader());
        id = in.readInt();
        text = in.readValue(DiseaseMO.class.getClassLoader());
        version = in.readValue(DiseaseMO.class.getClassLoader());
        system = in.readValue(DiseaseMO.class.getClassLoader());
    }

    public static final Parcelable.Creator<DiseaseMO> CREATOR = new Parcelable.Creator<DiseaseMO>() {
        public DiseaseMO createFromParcel(Parcel in) {
            return new DiseaseMO(in);
        }

        public DiseaseMO[] newArray(int size) {
            return new DiseaseMO[size];
        }
    };
}
