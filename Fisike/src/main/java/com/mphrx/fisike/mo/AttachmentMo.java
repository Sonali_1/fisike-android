package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

public class AttachmentMo extends MobileObject {
	private String attachmentId;
	private String attachmentType;
	private String attachmentPath;
	private String chatConversationMOPKey;
	private String chatMessageMOPKey;
	private String receiverUserName;
	private String senderUserName;
	private String status;
	private long fileSize;
	private byte[] imageThumbnail;
	private String imageThumbnailPath;
	private int imageRecordSyncStatus;

	public AttachmentMo() {
		super();
	}

	public String getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	public String getChatConversationMOPKey() {
		return chatConversationMOPKey;
	}

	public void setChatConversationMOPKey(String chatConversationMOPKey) {
		this.chatConversationMOPKey = chatConversationMOPKey;
	}

	public String getChatMessageMOPKey() {
		return chatMessageMOPKey;
	}

	public void setChatMessageMOPKey(String chatMessageMOPKey) {
		this.chatMessageMOPKey = chatMessageMOPKey;
	}

	public String getReceiverUserName() {
		return receiverUserName;
	}

	public void setReceiverUserName(String receiverUserName) {
		this.receiverUserName = receiverUserName;
	}

	public String getSenderUserName() {
		return senderUserName;
	}

	public void setSenderUserName(String senderUserName) {
		this.senderUserName = senderUserName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public byte[] getImageThumbnail() {
		return imageThumbnail;
	}

	public void setImageThumbnail(byte[] imageThumbnail) {
		this.imageThumbnail = imageThumbnail;
	}

	public void generatePersistenceKey(String senderId) {
		String uniqueString = "AttachmentMo" + attachmentId + senderId;

		setPersistenceKey(uniqueString.hashCode());
	}

	protected AttachmentMo(Parcel in) {
		super(in);
		attachmentId = in.readString();
		attachmentType = in.readString();
		attachmentPath = in.readString();
		chatConversationMOPKey = in.readString();
		chatMessageMOPKey = in.readString();
		receiverUserName = in.readString();
		senderUserName = in.readString();
		status = in.readString();
		fileSize = in.readLong();
		if (imageThumbnail == null) {
			imageThumbnail = new byte[0];
		}
		this.imageThumbnail = new byte[in.readInt()];
		in.readByteArray(this.imageThumbnail);
		imageThumbnailPath = in.readString();
		imageRecordSyncStatus = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public String getImageThumbnailPath() {
		return imageThumbnailPath;
	}

	public void setImageThumbnailPath(String imageThumbnailPath) {
		this.imageThumbnailPath = imageThumbnailPath;
	}

	public int getImageRecordSyncStatus() {
		return imageRecordSyncStatus;
	}

	public void setImageRecordSyncStatus(int imageRecordSyncStatus) {
		this.imageRecordSyncStatus = imageRecordSyncStatus;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(attachmentId);
		dest.writeString(attachmentType);
		dest.writeString(attachmentPath);
		dest.writeString(chatConversationMOPKey);
		dest.writeString(chatMessageMOPKey);
		dest.writeString(receiverUserName);
		dest.writeString(senderUserName);
		dest.writeString(status);
		dest.writeLong(fileSize);
		if (imageThumbnail == null) {
			imageThumbnail = new byte[0];
		}
		dest.writeInt(imageThumbnail.length);
		dest.writeByteArray(imageThumbnail);
		dest.writeString(imageThumbnailPath);
		dest.writeInt(imageRecordSyncStatus);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<AttachmentMo> CREATOR = new Parcelable.Creator<AttachmentMo>() {
		@Override
		public AttachmentMo createFromParcel(Parcel in) {
			return new AttachmentMo(in);
		}

		@Override
		public AttachmentMo[] newArray(int size) {
			return new AttachmentMo[size];
		}
	};
}