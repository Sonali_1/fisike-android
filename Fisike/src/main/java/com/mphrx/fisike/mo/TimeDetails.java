package com.mphrx.fisike.mo;

import java.util.ArrayList;

public class TimeDetails {

    private ArrayList<TimeReminder> timeReminders;

    public ArrayList<TimeReminder> getTimeReminders() {
        return timeReminders;
    }

    public void setTimeReminders(ArrayList<TimeReminder> timeReminders) {
        this.timeReminders = timeReminders;
    }

}
