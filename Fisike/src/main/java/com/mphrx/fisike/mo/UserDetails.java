package com.mphrx.fisike.mo;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.gson.request.CommonApiResponse;

public class UserDetails extends CommonApiResponse {

	@SerializedName("user")
	private UserMO userDetails;

	public UserMO getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserMO userDetails) {
		this.userDetails = userDetails;
	}
}
