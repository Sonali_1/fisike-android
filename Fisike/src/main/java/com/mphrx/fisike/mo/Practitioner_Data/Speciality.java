package com.mphrx.fisike.mo.Practitioner_Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Neha on 23-12-2016.
 */

public class Speciality implements Serializable{
    private static final long serialVersionUID = 7329178706276519410L;
    Object[] coding;
    Object[] extension;
    String id;
    String text;

    public Object[] getCoding() {
        return coding;
    }

    public void setCoding(Object[] coding) {
        this.coding = coding;
    }

    public void setExtension(Object[] extension) {
        this.extension = extension;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object[] getExtension() {
        return extension;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }
    /*    protected Speciality(Parcel in) {
        id = in.readString();
        text = in.readString();
        coding=in.readArray(ClassLoader.getSystemClassLoader());
        extension=in.readArray(ClassLoader.getSystemClassLoader());
    }

    public static final Creator<Speciality> CREATOR = new Creator<Speciality>() {
        @Override
        public Speciality createFromParcel(Parcel in) {
            return new Speciality(in);
        }

        @Override
        public Speciality[] newArray(int size) {
            return new Speciality[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(text);
        dest.writeArray(coding);
        dest.writeArray(extension);
    }*/
}
