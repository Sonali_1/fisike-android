package com.mphrx.fisike.mo;

import com.mphrx.fisike.models.DoseModel;
import com.mphrx.fisike.models.MedicationPrescriptionModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MedicationDetailsMO extends MobileObject {

	private String toDate;
	private String fromDate;

	private ArrayList<DiseaseMO> diseaseList;
	private int prescriberID;
	private String prescriberName;
	private ArrayList<DrugDetails> drugDetails;
	private boolean isAllReminders;
	private int encounterID;

	public int getEncounterID() {
		return encounterID;
	}

	public void setEncounterID(int encounterID) {
		this.encounterID = encounterID;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	public String getPrescriberName() {
		return prescriberName;
	}

	public void setPrescriberName(String prescriberName) {
		this.prescriberName = prescriberName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public int getPrescriberID() {
		return prescriberID;
	}

	public void setPrescriberID(int prescriberID) {
		this.prescriberID = prescriberID;
	}

	public ArrayList<DrugDetails> getDrugDetails() {
		return drugDetails;
	}

	public void setDrugDetails(ArrayList<DrugDetails> drugDetails) {
		this.drugDetails = drugDetails;
	}

	public boolean isAllReminders() {
		return isAllReminders;
	}

	public void setAllReminders(boolean isAllReminders) {
		this.isAllReminders = isAllReminders;
	}

	public static MedicationDetailsMO createFromMedicationPrescriptionModel(int encounterID, List<MedicationPrescriptionModel> models) {
		if (models == null || models.size() == 0)
			return null;

		MedicationDetailsMO out = new MedicationDetailsMO();
		out.setEncounterID(encounterID);
		out.setPrescriberID(models.get(0).getPractitionarID());
		out.setFromDate(models.get(0).getFromDate());
		out.setToDate(models.get(0).getToDate());
		out.setDrugDetails(new ArrayList<DrugDetails>());
        out.setDiseaseName(models.get(0).getDiseaseMoList());

		for (MedicationPrescriptionModel model : models) {
			DrugDetails drugDetail = new DrugDetails();
			if (model.getAutoReminderSQLBool() > 0) {
				out.setAllReminders(true);
				drugDetail.setReminderAdded(true);
			} else {
				out.setAllReminders(false);
				drugDetail.setReminderAdded(false);
			}
			drugDetail.setMedicationPrescriptionID(model.getID());
			drugDetail.setMedicineId(model.getMedicianId());
			drugDetail.setEncounterID(model.getEncounterID());


//				drugDetail.setDiseaseCode(model.getDiseaseCode());
//				drugDetail.setDiseaseCodeingSystem(model.getDiseaseCodingSystem());
//				drugDetail.setDiseaseName(model.getDiseaseName());

			drugDetail.setEndDate(model.getToDate());
			drugDetail.setMedicineUnits(model.getUnit());
			drugDetail.setPrescriberID(model.getPractitionarID());
			drugDetail.setStartDate(model.getFromDate());


			LinkedHashMap<String, Double> arrayDosageInfo = new LinkedHashMap<String, Double>();
			ArrayList<String> doseTime = new ArrayList<String>();
			int medicinePerDayCount = 0;
			for (DoseModel doseModel : model.getArrayDose()) {
				arrayDosageInfo.put(doseModel.getDescription(), doseModel.getQuntity());
				doseTime.add(doseModel.getDescription());
				medicinePerDayCount += doseModel.getQuntity();
			}
			drugDetail.setMedicinePerDayCount(medicinePerDayCount);
			drugDetail.setDoseTime(doseTime);
			drugDetail.setDosageInfo(arrayDosageInfo);
			drugDetail.setMedicineName(model.getMedicineName());
			out.getDrugDetails().add(drugDetail);
//			out.setDiseaseName(model.getDiseaseName());
			out.setPrescriberName(model.getPractitionerName());
		}

		return out;
	}

	public void setDiseaseName(ArrayList<DiseaseMO> diseaseName) {
		this.diseaseList = diseaseName;
	}

	public ArrayList<DiseaseMO> getDiseaseList() {
		return diseaseList;
	}
}
