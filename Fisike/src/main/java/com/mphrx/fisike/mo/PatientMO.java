package com.mphrx.fisike.mo;

import java.io.Serializable;
import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.Map;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.Active;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.BirthDate;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.Contact;
import com.mphrx.fisike.gson.request.Contact_name;
import com.mphrx.fisike.gson.request.Deceased;
import com.mphrx.fisike.gson.request.Extension;
import com.mphrx.fisike.gson.request.Identifier;
import com.mphrx.fisike.gson.request.LinkingList;
import com.mphrx.fisike.gson.request.ManagingOrganization;
import com.mphrx.fisike.gson.request.MultipleBirth;
import com.mphrx.fisike.gson.request.PatientExtension;
import com.mphrx.fisike.gson.request.patIdentifier;
import com.mphrx.fisike.gson.request.telecom;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.BMIListParams;

public class PatientMO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7278902171281055026L;

	private Active active;
	private ArrayList<Address> address;
	private String animal;
	private BirthDate birthDate;
	private ArrayList<Object> careProvider;
	private ArrayList<String> communication;
	private ArrayList<Contact> contact;
	private Deceased deceased;
	private ArrayList<Extension> extension;
	//	CodingExtensionTextFormat gender;
	private String gender;
	private long id;
	private ArrayList<LinkingList> linkList;
	private ManagingOrganization managingOrganization;
	private CodingExtensionTextFormat maritalStatus;
	//	private MultipleBirth multipleBirth;
	private ArrayList<Contact_name> name;
	@SerializedName("class")
	private String patclass;
	private ArrayList<patIdentifier> identifier;
	private ArrayList<telecom> telecom;
	private String resourceType;

	private String alternateContact;
	private ArrayList<Object> photo;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public void setLinkList(ArrayList<LinkingList> linkList) {
		this.linkList = linkList;
	}

	public ArrayList<patIdentifier> getIdentifier() {
		return identifier;
	}

	public ArrayList<LinkingList> getLinkList() {
		return linkList;
	}

	public void setIdentifier(ArrayList<patIdentifier> identifier) {
		this.identifier = identifier;
	}

	public Active getActive() {
		return active;
	}

	public void setActive(Active active) {
		this.active = active;
	}

	public ArrayList<Object> getPhoto() {
		return photo;
	}

	public void setPhoto(ArrayList<Object> photo) {
		this.photo = photo;
	}

	public ArrayList<Address> getAddress() {
		return address;
	}

	public void setAddress(ArrayList<Address> address) {
		this.address = address;
	}

	public String getAnimal() {
		return animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public BirthDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(BirthDate birthDate) {
		this.birthDate = birthDate;
	}

	public ArrayList<Object> getCareProvider() {
		return careProvider;
	}

	public void setCareProvider(ArrayList<Object> careProvider) {
		this.careProvider = careProvider;
	}

	public ArrayList<String> getCommunication() {
		return communication;
	}

	public void setCommunication(ArrayList<String> communication) {
		this.communication = communication;
	}

	public ArrayList<Contact> getContact() {
		return contact;
	}

	public void setContact(ArrayList<Contact> contact) {
		this.contact = contact;
	}

	public Deceased getDeceased() {
		return deceased;
	}

	public void setDeceased(Deceased deceased) {
		this.deceased = deceased;
	}

	public ArrayList<Extension> getExtension() {
		return extension;
	}

	public void setExtension(ArrayList<Extension> extension) {
		this.extension = extension;
	}

//	public CodingExtensionTextFormat getGender() {
//		return gender;
//	}
//
//	public void setGender(CodingExtensionTextFormat gender) {
//		this.gender = gender;
//	}


	public CodingExtensionTextFormat getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(CodingExtensionTextFormat maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getAlternateContact() {
		return alternateContact;
	}

	public void setAlternateContact(String alternateContact) {
		this.alternateContact = alternateContact;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public ManagingOrganization getManagingOrganization() {
		return managingOrganization;
	}

	public void setManagingOrganization(ManagingOrganization managingOrganization) {
		this.managingOrganization = managingOrganization;
	}

//	public MultipleBirth getMultipleBirth() {
//		return multipleBirth;
//	}
//
//	public void setMultipleBirth(MultipleBirth multipleBirth) {
//		this.multipleBirth = multipleBirth;
//	}

	public ArrayList<Contact_name> getName() {
		return name;
	}

	public void setName(ArrayList<Contact_name> name) {
		this.name = name;
	}

	public String getPatclass() {
		return patclass;
	}

	public void setPatclass(String patclass) {
		this.patclass = patclass;
	}

	public ArrayList<telecom> getTelecom() {
		return telecom;
	}

	public void setTelecom(ArrayList<telecom> telecom) {
		this.telecom = telecom;
	}

	public String getPrimaryEmail()
	{
		try {

		String primaryEmail="";
		ArrayList<telecom> telecomArrayList=getTelecom();
		for(telecom telecomElement:telecomArrayList)
		{
			if(telecomElement.getExtension()!=null && telecomElement.getExtension().size()>0
					&& Utils.isValueAvailable(telecomElement.getSystem())
					&& telecomElement.getSystem().equals("email"))
			{
									if(isPrimaryValue(telecomElement.getExtension()))
										primaryEmail=telecomElement.getValue();
			}
		}

		return primaryEmail;}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private boolean isPrimaryValue(ArrayList<PatientExtension> extensions)
	{
		try {
		for(PatientExtension extension:extensions)
			{
				try {
					if (extension.getUrl().equalsIgnoreCase("primary") && extension.getValue().get(0).toString().equalsIgnoreCase("true")) {
						return true;
					}
				}
				catch (Exception e)
				{
					return false;
				}
			}
		return false;}catch (Exception e)
		{
			return false;
		}
	}

	public String getPrimaryPhone()
	{
		try{
		String primaryPhone="";
		ArrayList<telecom> telecomArrayList=getTelecom();
		for(telecom telecomElement:telecomArrayList)
		{
			if(telecomElement.getExtension()!=null && telecomElement.getExtension().size()>0
					&& Utils.isValueAvailable(telecomElement.getSystem())
					&& telecomElement.getSystem().equals("phone"))
			{
				if(isPrimaryValue(telecomElement.getExtension()))
					primaryPhone=telecomElement.getValue();
			}
		}
		return primaryPhone;}catch (Exception e)
		{}
		return null;
	}

	public void setPrimaryEmail(String email, String primaryEmail)
	{
		try {
		boolean isfound=false;
		ArrayList<telecom> telecomArrayList=getTelecom();
		for(telecom telecomElement:telecomArrayList)
		{
			if(telecomElement.getExtension()!=null && telecomElement.getExtension().size()>0
					&& Utils.isValueAvailable(telecomElement.getSystem())
					&& telecomElement.getSystem().equals("email"))
			{
				if(isPrimaryValue(telecomElement.getExtension()))
				{	isfound=true;
					telecomElement.setValue(email);
				}

			}
		}
		if(!isfound)
		{
			telecomArrayList.add(createTelecomObject("email","home",email));
		}}
		catch (Exception e)
		{

		}
	}

	public void setPrimaryPhone(String phone, String primaryPhone)
	{
		boolean isfound=false;
		ArrayList<telecom> telecomArrayList=getTelecom();
		for(telecom telecomElement:telecomArrayList)
		{
			if(telecomElement.getExtension()!=null && telecomElement.getExtension().size()>0
					&& Utils.isValueAvailable(telecomElement.getSystem())
					&& telecomElement.getSystem().equals("phone"))
			{
				if(isPrimaryValue(telecomElement.getExtension()))
				{	isfound=true;
					telecomElement.setValue(phone);
				}

			}
		}
		if(!isfound)
		{
			telecomArrayList.add(createTelecomObject("phone","mobile",phone));
		}
	}

	private telecom createTelecomObject(String system,String useCode,String value)
	{
		com.mphrx.fisike.gson.request.telecom patientTelecom=new telecom();
		patientTelecom.setSystem(system);
		patientTelecom.setValue(value);
		patientTelecom.setUseCode(useCode);
		ArrayList<PatientExtension> extensions=new ArrayList<>();
		PatientExtension extension=new PatientExtension();
		extension.setUrl("primary");
		ArrayList<Object> primaryValue=new ArrayList<>();
		primaryValue.add(String.valueOf(true));
		extension.setValue(primaryValue);
		extensions.add(extension);
		patientTelecom.setExtension(extensions);
		return patientTelecom;
	}

	//return aadhar number fetched from identifier
    public String getAadharNumber() {
		return getValueFromIdentifierBasedOnKey(MyApplication.getAppContext().getString(R.string.aadhar_key));
    }

	//return MRN  number fetched from identifier
	public String getMRNNumber() {
		return getValueFromIdentifierBasedOnKey(MyApplication.getAppContext().getString(R.string.mrn_key));
	}

	private String getValueFromIdentifierBasedOnKey(String key)
	{
		String text="";
		patIdentifier keyIdentifier=getIdentifierObjectBasedOnKey(key);
		if(keyIdentifier!=null)
			text=keyIdentifier.getValue();

		return text;
	}

	//return identifier object if key matches in type String else return null
	public patIdentifier getIdentifierObjectBasedOnKey(String key)
	{
		try {
		if(getIdentifier() == null) return null;

		for( patIdentifier identifierObject:getIdentifier())
		{
			if(identifierObject.getType()!=null)
			{
				if(identifierObject.getType().toString().contains(key))
					return identifierObject;
			}
		}}
		catch (Exception e)
		{}
		return null;
	}


	//return identifier object if key matches in type String else return null
	public int getIdentifierObjectIndexOnKey(String key)
	{
		if(getIdentifier() == null) return -1;

		patIdentifier keyIdentifier=getIdentifierObjectBasedOnKey(key);
		return getIdentifier().indexOf(keyIdentifier);
	}
}
