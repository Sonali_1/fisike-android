package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.adapter.MessageItem;

public class ChatMessageMO extends MobileObject implements MessageItem {
	private String attachmentID;
	private String attachmentPKey;
	private String attachmentType;

	private String messageText;
	private String msgId;
	/*
	 * The msgStatus is an integer from the DefaultConnection class MESSAGE_SENT = 0; MESSAGE_RECEIVED = 1; MESSAGE_NOT_SENT = 2; MESSAGE_DELIVERED =
	 * 3;
	 */
	private int msgStatus;

	private String senderUserId;
	private String subjectText;
	private long lastUpdatedTimeUTC;

	// This is a new field which tracks the persistence key of the chat conversation MO
	private long convPersistenceKey;

	// These are two new flags that have been created to keep track if the server has received the message
	// and if the recipient has received the message
	private boolean serverReceivedMsg = false;
	private boolean recipientReceivedMsg = false;

	private boolean isAttachmentRecived;
	private String attachmentStatus;
	private long fileSize;
	private int percentage;

	private int chatMessageMOType;
	private String groupID;

	private String senderNickName;// for adapter only not store in db
	private int subType;

	private byte[] imageThumb;
	private String imageThumbnailPath;
	private int imageRecordSyncStatus;
	private String attachmentPath;

	public ChatMessageMO() {
		super();
	}

	/**
	 * This method is to check the persistence key without creating a new object
	 *
	 * @param senderId
	 * @param messageId
	 * @return persistenceKey
	 */
	public static int checkPersistenceKey(String senderId, String messageId) {
		String uniqueString = "ChatMessageMo" + senderId + messageId;
		return uniqueString.hashCode();
	}

	public int getChatMessageMOType() {
		return chatMessageMOType;
	}

	public void setChatMessageMOType(int chatMessageMOType) {
		this.chatMessageMOType = chatMessageMOType;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getAttachmentID() {
		return attachmentID;
	}

	public void setAttachmentID(String attachmentID) {
		this.attachmentID = attachmentID;
	}

	public String getAttachmentPKey() {
		return attachmentPKey;
	}

	public void setAttachmentPKey(String attachmentPKey) {
		this.attachmentPKey = attachmentPKey;
	}

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getSenderUserId() {
		return senderUserId;
	}

	public void setSenderUserId(String senderUserName) {
		this.senderUserId = senderUserName;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public void setLastUpdatedTimeUTC(long lastUpdatedTimeUTC) {
		this.lastUpdatedTimeUTC = lastUpdatedTimeUTC;
	}

	public long getLasUpdatedTimeUTC() {
		return lastUpdatedTimeUTC;
	}

	public void generatePersistenceKey() {
		String uniqueString = "ChatMessageMo" + senderUserId + msgId;

		setPersistenceKey(uniqueString.hashCode());
	}

	public String getSubjectText() {
		return subjectText;
	}

	public void setSubjectText(String subjectText) {
		this.subjectText = subjectText;
	}

	public int getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(int msgStatus) {
		this.msgStatus = msgStatus;
	}

	public boolean isServerReceivedMsg() {
		return serverReceivedMsg;
	}

	public void setServerReceivedMsg(boolean serverReceivedMsg) {
		this.serverReceivedMsg = serverReceivedMsg;
	}

	public boolean isRecipientReceivedMsg() {
		return recipientReceivedMsg;
	}

	public void setRecipientReceivedMsg(boolean recipientReceivedMsg) {
		this.recipientReceivedMsg = recipientReceivedMsg;
	}

	public long getConvPersistenceKey() {
		return convPersistenceKey;
	}

	public void setConvPersistenceKey(long convPersistenceKey) {
		this.convPersistenceKey = convPersistenceKey;
	}

	public byte[] getImageThumb() {
		return imageThumb;
	}

	public void setImageThumb(byte[] imageThumb) {
		this.imageThumb = imageThumb;
	}
	
	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	@Override
	public boolean isSection() {
		return false;
	}

	public boolean isAttachmentRecived() {
		return isAttachmentRecived;
	}

	public void setAttachmentRecived(boolean isAttachmentRecived) {
		this.isAttachmentRecived = isAttachmentRecived;
	}

	public String getAttachmentStatus() {
		return attachmentStatus;
	}

	public void setAttachmentStatus(String attachmentStatus) {
		this.attachmentStatus = attachmentStatus;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getSenderNickName() {
		return senderNickName;
	}

	public void setSenderNickName(String senderNickName) {
		this.senderNickName = senderNickName;
	}

	public String getImageThumbnailPath() {
		return imageThumbnailPath;
	}

	public void setImageThumbnailPath(String imageThumbnailPath) {
		this.imageThumbnailPath = imageThumbnailPath;
	}

	public int getImageRecordSyncStatus() {
		return imageRecordSyncStatus;
	}

	public void setImageRecordSyncStatus(int imageRecordSyncStatus) {
		this.imageRecordSyncStatus = imageRecordSyncStatus;
	}

	protected ChatMessageMO(Parcel in) {
		super(in);
		attachmentID = in.readString();
		attachmentPKey = in.readString();
		attachmentType = in.readString();
		messageText = in.readString();
		msgId = in.readString();
		msgStatus = in.readInt();
		senderUserId = in.readString();
		subjectText = in.readString();
		lastUpdatedTimeUTC = in.readLong();
		convPersistenceKey = in.readLong();
		serverReceivedMsg = in.readByte() != 0x00;
		recipientReceivedMsg = in.readByte() != 0x00;
		isAttachmentRecived = in.readByte() != 0x00;
		attachmentStatus = in.readString();
		fileSize = in.readLong();
		percentage = in.readInt();
		chatMessageMOType = in.readInt();
		groupID = in.readString();
		senderNickName = in.readString();
		subType = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(attachmentID);
		dest.writeString(attachmentPKey);
		dest.writeString(attachmentType);
		dest.writeString(messageText);
		dest.writeString(msgId);
		dest.writeInt(msgStatus);
		dest.writeString(senderUserId);
		dest.writeString(subjectText);
		dest.writeLong(lastUpdatedTimeUTC);
		dest.writeLong(convPersistenceKey);
		dest.writeByte((byte) (serverReceivedMsg ? 0x01 : 0x00));
		dest.writeByte((byte) (recipientReceivedMsg ? 0x01 : 0x00));
		dest.writeByte((byte) (isAttachmentRecived ? 0x01 : 0x00));
		dest.writeString(attachmentStatus);
		dest.writeLong(fileSize);
		dest.writeInt(percentage);
		dest.writeInt(chatMessageMOType);
		dest.writeString(groupID);
		dest.writeString(senderNickName);
		dest.writeInt(subType);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<ChatMessageMO> CREATOR = new Parcelable.Creator<ChatMessageMO>() {
		@Override
		public ChatMessageMO createFromParcel(Parcel in) {
			return new ChatMessageMO(in);
		}

		@Override
		public ChatMessageMO[] newArray(int size) {
			return new ChatMessageMO[size];
		}
	};
}