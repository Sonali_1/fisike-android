package com.mphrx.fisike.mo;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class QuestionActivityMO extends MobileObject {

	@SerializedName("numberOfQuestions")
	private int numberOfQuestions;// number

	@SerializedName("title")
	private String activityTitle;

	@SerializedName("estimatedTime")
	private String estimatedTime;

	@SerializedName("questions")
	private ArrayList<QuestionMO> questionsList;

	private String activityID;
	private String activityStatus;
	private String submissionTimeUTC;
	private boolean activitySubmitted;
	private String chatConversationMOPKey;
	private String chatMessageMOPKey;

	private ArrayList<String> questionMOPKeys;// id
	private String receiverUsername;
	private String senderUsername;
	

	public QuestionActivityMO(){
		super();
	}
	public String getActivityID() {
		return activityID;
	}

	public void setActivityID(String activityID) {
		this.activityID = activityID;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	public String getSubmissionTimeUTC() {
		return submissionTimeUTC;
	}

	public void setSubmissionTimeUTC(String submissionTimeUTC) {
		this.submissionTimeUTC = submissionTimeUTC;
	}

	public boolean getActivitySubmitted() {
		return activitySubmitted;
	}

	public void setActivitySubmitted(boolean activitySubmitted) {
		this.activitySubmitted = activitySubmitted;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String getChatConversationMOPKey() {
		return chatConversationMOPKey;
	}

	public void setChatConversationMOPKey(String chatConversationMOPKey) {
		this.chatConversationMOPKey = chatConversationMOPKey;
	}

	public String getChatMessageMOPKey() {
		return chatMessageMOPKey;
	}

	public void setChatMessageMOPKey(String chatMessageMOPKey) {
		this.chatMessageMOPKey = chatMessageMOPKey;
	}

	public String getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}

	public ArrayList<String> getQuestionMOPKeys() {
		return questionMOPKeys;
	}

	public void setQuestionMOPKeys(ArrayList<String> questionMOPKeys) {
		this.questionMOPKeys = questionMOPKeys;
	}

	public String getReceiverUsername() {
		return receiverUsername;
	}

	public void setReceiverUsername(String receiverUsername) {
		this.receiverUsername = receiverUsername;
	}

	public String getSenderUsername() {
		return senderUsername;
	}

	public void setSenderUsername(String senderUsername) {
		this.senderUsername = senderUsername;
	}
	public void generatePersistenceKey(String patEmailId) {
		String uniqueString = "QuestionActivityMO" + activityID + patEmailId;
		setPersistenceKey(uniqueString.hashCode());
	}

    protected QuestionActivityMO(Parcel in) {
    	super(in);
        activityID = in.readString();
        activityStatus = in.readString();
        submissionTimeUTC = in.readString();
        activitySubmitted = in.readByte() != 0x00;
        activityTitle = in.readString();
        chatConversationMOPKey = in.readString();
        chatMessageMOPKey = in.readString();
        estimatedTime = in.readString();
        numberOfQuestions = in.readInt();
        if (in.readByte() == 0x01) {
            questionMOPKeys = new ArrayList<String>();
            in.readList(questionMOPKeys, String.class.getClassLoader());
        } else {
            questionMOPKeys = null;
        }
        receiverUsername = in.readString();
        senderUsername = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    	super.writeToParcel(dest, flags);
        dest.writeString(activityID);
        dest.writeString(activityStatus);
        dest.writeString(submissionTimeUTC);
        dest.writeByte((byte) (activitySubmitted ? 0x01 : 0x00));
        dest.writeString(activityTitle);
        dest.writeString(chatConversationMOPKey);
        dest.writeString(chatMessageMOPKey);
        dest.writeString(estimatedTime);
        dest.writeInt(numberOfQuestions);
        if (questionMOPKeys == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(questionMOPKeys);
        }
        dest.writeString(receiverUsername);
        dest.writeString(senderUsername);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<QuestionActivityMO> CREATOR = new Parcelable.Creator<QuestionActivityMO>() {
        @Override
        public QuestionActivityMO createFromParcel(Parcel in) {
            return new QuestionActivityMO(in);
        }

        @Override
        public QuestionActivityMO[] newArray(int size) {
            return new QuestionActivityMO[size];
        }
    };
	public ArrayList<QuestionMO> getQuestionsList() {
		return questionsList;
	}

	public void setQuestionsList(ArrayList<QuestionMO> questionsList) {
		this.questionsList = questionsList;
	}

}
