package com.mphrx.fisike.mo;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.CommonApiResponse;

public class UserProfile extends CommonApiResponse {

	@Expose
	private ArrayList<ChatContactMO> resultList;
//	@Expose
//	private int totalCount;
//	@Expose
//	private String status;

	public ArrayList<ChatContactMO> getResultList() {
		return resultList;
	}

	public void setResultList(ArrayList<ChatContactMO> profiles) {
		this.resultList = profiles;
	}

/*
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
*/
//	}
}