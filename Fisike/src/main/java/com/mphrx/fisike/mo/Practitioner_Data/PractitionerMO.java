package com.mphrx.fisike.mo.Practitioner_Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.profile.request.PractitionerRole;
import com.mphrx.fisike.gson.request.Active;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.BirthDate;
import com.mphrx.fisike.gson.request.CodingExtensionTextFormat;
import com.mphrx.fisike.gson.request.Contact;
import com.mphrx.fisike.gson.request.Contact_name;
import com.mphrx.fisike.gson.request.Deceased;
import com.mphrx.fisike.gson.request.LinkingList;
import com.mphrx.fisike.gson.request.ManagingOrganization;
import com.mphrx.fisike.gson.request.patIdentifier;
import com.mphrx.fisike.gson.request.telecom;
import com.mphrx.fisike.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Neha on 23-12-2016.
 */

public class PractitionerMO implements Serializable{


    private static final long serialVersionUID = -6844012373528514499L;
    private ArrayList<patIdentifier> identifier;
    private ArrayList<Extension> extension;
    private ArrayList<Address> address;
    private String gender;
    ArrayList<practitionerRole> practitionerRole;
    private Active active;
    ArrayList<Object> photo;
    private BirthDate birthDate;
    private Contact_name lowerCaseName;
    private Contact_name name;
    private ArrayList<Object> qualification;
    private ArrayList<Object> location;
    private ArrayList<com.mphrx.fisike.gson.request.telecom> telecom;
    private long id;
    private ArrayList<Object> communication;
    private String alternateContact;
    private CodingExtensionTextFormat maritalStatus;

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    private String _class;


    public ArrayList<patIdentifier> getIdentifier() {
        return identifier;
    }

    public ArrayList<Extension> getExtension() {
        return extension;
    }

    public ArrayList<Address> getAddress() {
        return address;
    }

    public String getGender() {
        return gender;
    }

    public ArrayList<com.mphrx.fisike.mo.Practitioner_Data.practitionerRole> getPractitionerRole() {
        return practitionerRole;
    }

    public Active getActive() {
        return active;
    }

    public ArrayList<Object> getPhoto() {
        return photo;
    }

    public BirthDate getBirthDate() {
        return birthDate;
    }

    public Contact_name getLowerCaseName() {
        return lowerCaseName;
    }

    public Contact_name getName() {
        return name;
    }

    public ArrayList<Object> getLocation() {
        return location;
    }

    public ArrayList<com.mphrx.fisike.gson.request.telecom> getTelecom() {
        return telecom;
    }

    public long getId() {
        return id;
    }

    public ArrayList<Object> getCommunication() {
        return communication;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public CodingExtensionTextFormat getMaritalStatus() {
        return maritalStatus;
    }


    public void setIdentifier(ArrayList<patIdentifier> identifier) {
        this.identifier = identifier;
    }

    public void setExtension(ArrayList<Extension> extension) {
        this.extension = extension;
    }

    public ArrayList<Object> getQualification() {
        return qualification;
    }

    public void setQualification(ArrayList<Object> qualification) {
        this.qualification = qualification;
    }

    public void setAddress(ArrayList<Address> address) {
        this.address = address;

    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPractitionerRole(ArrayList<com.mphrx.fisike.mo.Practitioner_Data.practitionerRole> practitionerRole) {
        this.practitionerRole = practitionerRole;
    }

    public void setActive(Active active) {
        this.active = active;
    }

    public void setPhoto(ArrayList<Object> photo) {
        this.photo = photo;
    }

    public void setBirthDate(BirthDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setLowerCaseName(Contact_name lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public void setName(Contact_name name) {
        this.name = name;
    }

    public void setLocation(ArrayList<Object> location) {
        this.location = location;
    }

    public void setTelecom(ArrayList<com.mphrx.fisike.gson.request.telecom> telecom) {
        this.telecom = telecom;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCommunication(ArrayList<Object> communication) {
        this.communication = communication;
    }

    public void setAlternateContact(String alternateContact) {
        this.alternateContact = alternateContact;
    }

    public void setMaritalStatus(CodingExtensionTextFormat maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getYearsOfExperience()
    {
        String experience=null;
        for(Extension extension:getExtension())
        {
            if(Utils.isValueAvailable(extension.getUrl())&&extension.getUrl().equalsIgnoreCase("Experience"))
            {
                experience=extension.getValue().get(0).getNoOfExp();
                break;
            }
        }
    return experience;
    }

/*    protected PractitionerMO(Parcel in) {
        gender = in.readString();
        practitionerRole = in.createTypedArrayList(com.mphrx.fisike.mo.Practitioner_Data.practitionerRole.CREATOR);
        id = in.readLong();
        communication = in.createStringArrayList();
        alternateContact = in.readString();
    }

    public static final Creator<PractitionerMO> CREATOR = new Creator<PractitionerMO>() {
        @Override
        public PractitionerMO createFromParcel(Parcel in) {
            return new PractitionerMO(in);
        }

        @Override
        public PractitionerMO[] newArray(int size) {
            return new PractitionerMO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(gender);
        dest.writeTypedList(practitionerRole);
        dest.writeLong(id);
        dest.writeStringList(communication);
        dest.writeString(alternateContact);
    }*/
}
