package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

public class SettingMO extends MobileObject {
	public static final long SETTING_KEY = "MphRxSetting".hashCode();
    private String serverIP;
    private String serverPort;
    private boolean useHTTPS;
    private float autoLogoutTimeInterval;
    private boolean isAutoLogout;
    private String fisikeServerIp;
    private String fisikeServerPort;
    private boolean fisikeUseHTTPS;
    private int noOfMsgsToStoreLocally;
    private int noOfDaysOfMsgsToStoreLocally;
    private String fisikeResource;
    private String fisikeServerHost;
    
    protected SettingMO(Parcel in) {
		super(in);
		serverIP = in.readString();
		serverPort = in.readString();
		useHTTPS = in.readByte() != 0; 
		autoLogoutTimeInterval = in.readFloat();
		isAutoLogout = in.readByte() != 0;
		fisikeServerIp = in.readString();
		fisikeServerPort = in.readString();
		fisikeUseHTTPS = in.readByte() != 0;
		noOfDaysOfMsgsToStoreLocally = in.readInt();
		noOfDaysOfMsgsToStoreLocally = in.readInt();
		fisikeResource = in.readString();
		fisikeServerHost = in.readString();
	}
    
    public SettingMO(){
    	super();
    }
    
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeString(serverIP);
        out.writeString(serverPort);
        out.writeByte((byte) (useHTTPS ? 1 : 0));  
        out.writeFloat(autoLogoutTimeInterval);
        out.writeByte((byte) (isAutoLogout ? 1 : 0));
        out.writeString(fisikeServerIp);
        out.writeString(fisikeServerPort);
        out.writeByte((byte) (fisikeUseHTTPS ? 1 : 0));
        out.writeInt(noOfMsgsToStoreLocally);
        out.writeInt(noOfDaysOfMsgsToStoreLocally);
        out.writeString(fisikeResource);
        out.writeString(fisikeServerHost);
    }
    
    public static final Parcelable.Creator<SettingMO> CREATOR = new Parcelable.Creator<SettingMO>() {

		@Override
		public SettingMO createFromParcel(Parcel source) {
			return new SettingMO(source);
		}

		@Override
		public SettingMO[] newArray(int size) {
			return new SettingMO[size];
		}
	};

    //Added ejabberd / openfire config
    private String xmppServerType;

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public boolean isUseHTTPS() {
        return useHTTPS;
    }

    public void setUseHTTPS(boolean useHTTPS) {
        this.useHTTPS = useHTTPS;
    }

    public boolean isAutoLogout() {
        return isAutoLogout;
    }

    public void setAutoLogout(boolean isAutoLogout) {
        this.isAutoLogout = isAutoLogout;
    }

    public float getAutoLogoutTimeInterval() {
        return autoLogoutTimeInterval;
    }

    public void setAutoLogoutTimeInterval(float autoLogoutTimeInterval) {
        this.autoLogoutTimeInterval = autoLogoutTimeInterval;
    }

    public String getFisikeServerIp() {
        return fisikeServerIp;
    }

    public void setFisikeServerIp(String fisikeServerIp) {
        this.fisikeServerIp = fisikeServerIp;
    }

    public String getFisikeServerPort() {
        return fisikeServerPort;
    }

    public void setFisikeServerPort(String fisikeServerPort) {
        this.fisikeServerPort = fisikeServerPort;
    }

    public boolean isFisikeUseHTTPS() {
        return fisikeUseHTTPS;
    }

    public void setFisikeUseHTTPS(boolean fisikeUseHTTPS) {
        this.fisikeUseHTTPS = fisikeUseHTTPS;
    }

    public int getNoOfMsgsToStoreLocally() {
        return noOfMsgsToStoreLocally;
    }

    public void setNoOfMsgsToStoreLocally(int noOfMsgsToStoreLocally) {
        this.noOfMsgsToStoreLocally = noOfMsgsToStoreLocally;
    }

    public int getNoOfDaysOfMsgsToStoreLocally() {
        return noOfDaysOfMsgsToStoreLocally;
    }

    public void setNoOfDaysOfMsgsToStoreLocally(int noOfDaysOfMsgsToStoreLocally) {
        this.noOfDaysOfMsgsToStoreLocally = noOfDaysOfMsgsToStoreLocally;
    }

    public String getFisikeResource() {
        return fisikeResource;
    }

    public void setFisikeResource(String fisikeResource) {
        this.fisikeResource = fisikeResource;
    }

    public String getFisikeServerHost() {
        return fisikeServerHost;
    }

    public void setFisikeServerHost(String fisikeServerHost) {
        this.fisikeServerHost = fisikeServerHost;
    }

    public String getXmppServerType() {
        return xmppServerType;
    }

    public void setXmppServerType(String xmppServerType) {
        this.xmppServerType = xmppServerType;
    }
}