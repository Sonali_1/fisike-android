package com.mphrx.fisike.mo.Practitioner_Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Neha on 23-12-2016.
 */

public class Extension implements Serializable
{

    private static final long serialVersionUID = -6784847013424855275L;
    String  url;
    ArrayList<Value> value;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Value> getValue() {
        return value;
    }

    public void setValue(ArrayList<Value> value) {
        this.value = value;
    }
}


