package com.mphrx.fisike.mo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.constant.VariableConstants;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * This is the class that stores details for every converstaion that is shown in the recent chat screen
 *
 * @author kkhurana
 */
public class ChatConversationMO extends MobileObject {
    private String nickName;
    @SerializedName("_id")
    private String jid;
    @SerializedName("subject")
    private String groupName;
    private String lastMsg;
    private int lastMsgStatus;
    private String phoneNumber;
    private boolean read;
    private int unReadCount;

    // Added a new field to store the last messageId in the conversation
    private String lastMsgId;

    // This is the list of messages in this conversation (that have been sent or recieved)
    private Vector<String> chatMessageKeyList;

    // This is the list of messages in this conversation that have not been sent yet - because the user is offline
    private Vector<String> offlineChatMessageKeyList;

    private long createdTimeUTC;

    private ArrayList<String> chatContactMoKeys;
    private int conversationType;
    private int myStatus;
    private String typeDataMOPK;
    private String secoundryJid;

    private int isNotificationEnabled;

    public int getIsChatInitiated() {
        return isChatInitiated;
    }

    public void setIsChatInitiated(int isChatInitiated) {
        this.isChatInitiated = isChatInitiated;
    }

    private int isChatInitiated;
    private long muteChatTillTime;

    // 0 corresponds to group not created
    // 1 corresponds to group created
    // 2 corresponds to group created as well as invite sent
    private int groupStatus;

    public ChatConversationMO() {
        super();
    }

    public ArrayList<String> getChatContactMoKeys() {
        return chatContactMoKeys;
    }

    public void setChatContactMoKeys(ArrayList<String> chatContactMoKeys) {
//		// FIXME HACK .... make set to remove duplicates
//		Set<String> uniq = new HashSet<>(chatContactMoKeys);
//		ArrayList<String> tmp = new ArrayList<>(uniq);
//
//		this.chatContactMoKeys = tmp;
        this.chatContactMoKeys = chatContactMoKeys;
    }

    public int getConversationType() {
        return conversationType;
    }

    public void setConversationType(int conversationType) {
        this.conversationType = conversationType;
    }

    public int getMyStatus() {
        return myStatus;
    }

    public void setMyStatus(int myStatus) {
        this.myStatus = myStatus;
    }

    public String getTypeDataMOPK() {
        return typeDataMOPK;
    }

    public void setTypeDataMOPK(String typeDataMOPK) {
        this.typeDataMOPK = typeDataMOPK;
    }

    public int getLastMsgStatus() {
        return lastMsgStatus;
    }

    public void setLastMsgStatus(int lastMsgStatus) {
        this.lastMsgStatus = lastMsgStatus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public String getJId() {
        return jid;
    }

    // emailwith%40@fisikeServerIp
    public void setJId(String jid) {
        this.jid = jid;
    }

    public String getSecoundryJid() {
        return secoundryJid;
    }

    public void setSecoundryJid(String secoundryJid) {
        this.secoundryJid = secoundryJid;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        if (TextUtils.isEmpty(this.groupName) || !this.groupName.equals(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            this.groupName = groupName;
            if (!TextUtils.isEmpty(groupName) && !groupName.equals(VariableConstants.SINGLE_CHAT_IDENTIFIER))
                setNickName(groupName);
        }
    }

    public long getCreatedTimeUTC() {
        return createdTimeUTC;
    }

    public void setCreatedTimeUTC(long createdTimeUTC) {
        this.createdTimeUTC = createdTimeUTC;
    }

    public int getIsNotificationEnabled() {
        return isNotificationEnabled;
    }

    public void setIsNotificationEnabled(int isNotificationEnabled) {
        this.isNotificationEnabled = isNotificationEnabled;
    }

    public long getMuteChatTillTime() {
        return muteChatTillTime;
    }

    public void setMuteChatTillTime(long muteChatTillTime) {
        this.muteChatTillTime = muteChatTillTime;
    }

    public void generatePersistenceKey(String currentUserId) {
        setPersistenceKey(generatePersistenceKey(jid, currentUserId));
    }

    /**
     * For group send groupId to generate the chat conversation mo
     *
     * @param jid           id of second user in single chat OR group id in case of group
     * @param currentUserId if one to one chat then sender username and for group name then group id
     */
    public static int generatePersistenceKey(String jid, String currentUserId) {
        if (!jid.contains("@")) {
            throw new RuntimeException("jid doesn't contain '@' : " + jid);
        }

        String uniqueString = "ChatConversationMo" + jid + currentUserId;
        return uniqueString.hashCode();
    }

    public Vector<String> getChatMessageKeyList() {
        return chatMessageKeyList;
    }

    public void setChatMessageKeyList(Vector<String> chatMessageKeyList) {
        this.chatMessageKeyList = chatMessageKeyList;
    }

    public boolean getReadStatus() {
        return read;
    }

    public void setReadStatus(boolean read) {
        this.read = read;
    }

    public String getNickName() {
        if (this.conversationType == VariableConstants.conversationTypeGroup)
            return groupName;
        else
            return nickName;
    }

    public void setNickName(String nickName) {
        if (TextUtils.isEmpty(this.groupName)
                || this.groupName.equals(VariableConstants.SINGLE_CHAT_IDENTIFIER))
            this.nickName = nickName;
    }

    public Vector<String> getOfflineChatMessageKeyList() {
        return offlineChatMessageKeyList;
    }

    public void setOfflineChatMessageKeyList(Vector<String> offlineChatMessageKeyList) {
        this.offlineChatMessageKeyList = offlineChatMessageKeyList;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    /**
     * Method to increment unread count
     */
    public synchronized void incrementUnreadCount() {
        setUnReadCount(++unReadCount);
    }

    public String getLastMsgId() {
        return lastMsgId;
    }

    public void setLastMsgId(String lastMsgId) {
        this.lastMsgId = lastMsgId;
    }

    public int getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(int groupStatus) {
        this.groupStatus = groupStatus;
    }

    protected ChatConversationMO(Parcel in) {
        super(in);
        nickName = in.readString();
        jid = in.readString();
        groupName = in.readString();
        lastMsg = in.readString();
        lastMsgStatus = in.readInt();
        phoneNumber = in.readString();
        read = in.readByte() != 0x00;
        unReadCount = in.readInt();
        lastMsgId = in.readString();
        if (in.readByte() == 0x01) {
            chatMessageKeyList = new Vector<String>();
            in.readList(chatMessageKeyList, String.class.getClassLoader());
        } else {
            chatMessageKeyList = null;
        }
        if (in.readByte() == 0x01) {
            offlineChatMessageKeyList = new Vector<String>();
            in.readList(offlineChatMessageKeyList, String.class.getClassLoader());
        } else {
            offlineChatMessageKeyList = null;
        }
        createdTimeUTC = in.readLong();
        if (in.readByte() == 0x01) {
            chatContactMoKeys = new ArrayList<String>();
            in.readList(chatContactMoKeys, String.class.getClassLoader());
        } else {
            chatContactMoKeys = null;
        }
        conversationType = in.readInt();
        myStatus = in.readInt();
        typeDataMOPK = in.readString();

        isNotificationEnabled = in.readInt();
        muteChatTillTime = in.readLong();
        groupStatus = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(nickName);
        dest.writeString(jid);
        dest.writeString(groupName);
        dest.writeString(lastMsg);
        dest.writeInt(lastMsgStatus);
        dest.writeString(phoneNumber);
        dest.writeByte((byte) (read ? 0x01 : 0x00));
        dest.writeInt(unReadCount);
        dest.writeString(lastMsgId);
        if (chatMessageKeyList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(chatMessageKeyList);
        }
        if (offlineChatMessageKeyList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(offlineChatMessageKeyList);
        }
        dest.writeLong(createdTimeUTC);
        if (chatContactMoKeys == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(chatContactMoKeys);
        }
        dest.writeInt(conversationType);
        dest.writeInt(myStatus);
        dest.writeString(typeDataMOPK);

        dest.writeInt(isNotificationEnabled);
        dest.writeLong(muteChatTillTime);
        dest.writeInt(groupStatus);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ChatConversationMO> CREATOR = new Parcelable.Creator<ChatConversationMO>() {
        @Override
        public ChatConversationMO createFromParcel(Parcel in) {
            return new ChatConversationMO(in);
        }

        @Override
        public ChatConversationMO[] newArray(int size) {
            return new ChatConversationMO[size];
        }
    };
}
