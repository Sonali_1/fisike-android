package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.gson.request.Height;
import com.mphrx.fisike.gson.request.Identifier;
import com.mphrx.fisike.gson.request.Members;
import com.mphrx.fisike.gson.request.UserSettings;
import com.mphrx.fisike.gson.request.Weight;
import com.mphrx.fisike.gson.response.Authority;
import com.mphrx.fisike.gson.response.UserGroup;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;

public class UserMO extends MobileObject {
    public static final String KEY = "MphRxUser";

    private String salutation; // This is YES for user logged in and NO for user logged out

    @Expose
    private long id;
    @Expose
    private String username;
    @Expose
    private String email;
    @Expose
    private String firstName;

    @Expose
    private String middleName;

    @Expose
    private String lastName;
    @Expose
    @SerializedName("phoneNo")
    private String phoneNumber;
    @Expose
    private String countryCode;
    @Expose
    private String gender;
    @Expose
    private String dob;
    @Expose
    private Boolean enabled;
    @Expose
    private Boolean accountLocked;
    @Expose
    private UserSettings userSettings;
    @Expose
    private long physicianId;
    @Expose
    private long patientId;

    @Expose
    private ArrayList<Coverage> coverage;
    @Expose
    private ArrayList<Authority> authorities;
    @Expose
    private ArrayList<UserGroup> userGroups;
    @Expose
    private String userType;
    @Expose
    private String password;
    @Expose
    private String roles;
    private String realName;
    @Expose
    private String deviceUID;
    @Expose
    private Boolean registrationStatus;
    @Expose
    private String deviceActivationDate;
    @Expose
    private String groupName;
    @Expose
    private String privilegeGroup;
    @Expose
    private String aboutMe;
    @Expose
    private String specialty;
    @Expose
    private String designation;
    @Expose
    private ArrayList<String> userPrivileges;


    @Expose
    private Height height;
    @Expose
    private Weight weight;
    @Expose
    private ArrayList<Members> members;
    private Boolean hasSignedUp;
    private String otpCode;
    private String mPIN;
    private String dependentPatientsIds;
    @Expose
    private boolean correctAgreementStatus;
    @Expose
    private String correctAgreementMessage;

    @Expose
    private String token;
    @Expose
    private ArrayList<AgreementModel> agreements;

    @Expose
    private ArrayList<Integer> pendingDiagnosticOrders;
    @SerializedName("inviteUserPriGroupList")
    private ArrayList<InviteUserPriGroupObject> inviteUserPriGroupObject;

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    private LinkedHashMap<String, String> inviteUserPriGroupMap;

    @SerializedName("profilePic")
    private byte[] profilePic;

    @SerializedName("activationStatus")
    private String fisikeCurrentStatus;

    @SerializedName("specialities")
    private ArrayList<String> specialitiesArray;

    @SerializedName("designations")
    private ArrayList<String> designationArray;

    @SerializedName("age")
    private int age;

    private String status;
    private String statusMessage;

    private ArrayList<String> chatConversationKeyList;
    private long lastAuthenticatedTime;
    private long createdTimeUTC;
    private String apnsDeviceToken;
    private Boolean profilePicUpdateStatus;
    private Boolean SignUpStatus;

    private Long SignUpTime;
    @SerializedName("addresses")
    private ArrayList<Address> addressUserMos;
    private String alternateContact;
    private String maritalStatus;
    private ArrayList<FetchUpdateIdentifier> identifierArrayList;
    private ArrayList<FetchUpdateTelecom> phoneList;
    private ArrayList<FetchUpdateTelecom> emailList;


    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Long getSignUpTime() {
        return SignUpTime;
    }

    public void setSignUpTime(Long signUpTime) {
        SignUpTime = signUpTime;
    }

    public Boolean getProfilePicUpdateStatus() {
        if (profilePicUpdateStatus == null) {
            return false;
        }
        return profilePicUpdateStatus;
    }

    public ArrayList<Address> getAddressUserMos() {
        return addressUserMos;
    }

    public void setAddressUserMos(ArrayList<Address> addressUserMos) {
        this.addressUserMos = addressUserMos;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public void setAlternateContact(String alternateContact) {
        this.alternateContact = alternateContact;
    }


    public Boolean getHasSignedUp() {
        if (hasSignedUp == null) {
            return false;
        }
        return hasSignedUp;
    }

    public void setHasSignedUp(Boolean hasSignedUp) {
        if (hasSignedUp == null) {
            this.hasSignedUp = false;
            return;
        }
        this.hasSignedUp = hasSignedUp;
    }

    public void setProfilePicUpdateStatus(Boolean profilePicUpdateStatus) {
        if (profilePicUpdateStatus == null) {
            this.profilePicUpdateStatus = false;
            return;
        }
        this.profilePicUpdateStatus = profilePicUpdateStatus;
    }

    public Boolean getSignUpStatus() {
        if (SignUpStatus == null) {
            return false;
        }
        return SignUpStatus;
    }

    public void setSignUpStatus(Boolean signUpStatus) {
        if (signUpStatus == null) {
            this.SignUpStatus = false;
            return;
        }
        SignUpStatus = signUpStatus;
    }

    public Boolean isEnabled() {
        if (enabled == null) {
            return false;
        }
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        if (enabled == null) {
            this.enabled = false;
            return;
        }
        this.enabled = enabled;
    }

    public Boolean isAccountLocked() {
        if (accountLocked == null) {
            return false;
        }
        return accountLocked;
    }

    public void setAccountLocked(Boolean accountLocked) {
        if (accountLocked == null) {
            this.accountLocked = false;
            return;
        }
        this.accountLocked = accountLocked;
    }

    public String getDependentPatientsIds() {
        return dependentPatientsIds;
    }


    public List<Long> getLinkedPatients() {
        List<Long> linkedPatientsList = new ArrayList<Long>();
        try {
            String linkedPatients = getDependentPatientsIds();
            if (linkedPatients != null && !linkedPatients.equals("")) {
                JSONArray ja = new JSONArray(linkedPatients);
                for (int i = 0; i < ja.length(); i++) {
                    try {
                        linkedPatientsList.add(Long.parseLong(String.valueOf(ja.get(i))));
                    } catch (Exception ex) {
                    }
                }
            }
        } catch (Exception ex) {
        }

        return linkedPatientsList;
    }


    public void setDependentPatientsIds(String dependentPatientsIds) {
        this.dependentPatientsIds = dependentPatientsIds;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public long getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(long physicianId) {
        this.physicianId = physicianId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public ArrayList<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(ArrayList<Authority> authorities) {
        this.authorities = authorities;
    }

    public ArrayList<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(ArrayList<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public LinkedHashMap<String, String> getInviteUserPriGroupMap() {
        return inviteUserPriGroupMap;
    }

    public void setInviteUserPriGroupMap(LinkedHashMap<String, String> inviteUserPriGroupMap) {
        this.inviteUserPriGroupMap = inviteUserPriGroupMap;
    }

    public UserMO() {
        super();
    }

    public ArrayList<InviteUserPriGroupObject> getInviteUserPriGroupObject() {
        return inviteUserPriGroupObject;
    }

    public void setInviteUserPriGroupObject(ArrayList<InviteUserPriGroupObject> inviteUserPriGroupObject) {
        this.inviteUserPriGroupObject = inviteUserPriGroupObject;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public ArrayList<String> getUserPrivileges() {
        return userPrivileges;
    }

    public void setUserPrivileges(ArrayList<String> userPrivileges) {
        this.userPrivileges = userPrivileges;
    }

    public LinkedHashMap<String, String> getInviteUserPriGroupList() {
        return inviteUserPriGroupMap;
    }

    public void setInviteUserPriGroupList(LinkedHashMap<String, String> inviteUserPriGroupList) {
        this.inviteUserPriGroupMap = inviteUserPriGroupList;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateOfBirth() {
        return dob;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dob = dateOfBirth;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getDeviceUID() {
        return deviceUID;
    }

    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    public String getFisikeCurrentStatus() {
        return fisikeCurrentStatus;
    }

    public void setFisikeCurrentStatus(String fisikeCurrentStatus) {
        this.fisikeCurrentStatus = fisikeCurrentStatus;
    }

    public Boolean isRegistrationStatus() {
        if (registrationStatus == null) {
            return false;
        }
        return registrationStatus;
    }

    public void setRegistrationStatus(Boolean registrationStatus) {
        if (registrationStatus == null) {
            this.registrationStatus = false;
            return;
        }
        this.registrationStatus = registrationStatus;
    }

    public String getDeviceActivationDate() {
        return deviceActivationDate;
    }

    public void setDeviceActivationDate(String deviceActivationDate) {
        this.deviceActivationDate = deviceActivationDate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPrivilegeGroup() {
        return privilegeGroup;
    }

    public void setPrivilegeGroup(String privilegeGroup) {
        this.privilegeGroup = privilegeGroup;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public byte[] getProfilePic() {
        return profilePic;
    }

    public String getUsername() {
        return username;
    }

    // pat1%40khura.33mail.com
    public void setUsername(String userName) {
        this.username = userName;
    }

    public void setProfilePic(byte[] profilePic) {
        this.profilePic = profilePic;
    }

    public ArrayList<String> getChatConversationKeyList() {
        return chatConversationKeyList;
    }

    public void setChatConversationKeyList(ArrayList<String> chatConversationKeyList) {
        this.chatConversationKeyList = chatConversationKeyList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if (lastName == null || lastName.equals("null")) {
            return "";
        }
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getLastAuthenticatedTime() {
        return lastAuthenticatedTime;
    }

    public void setLastAuthenticatedTime(long lastAuthenticatedTime) {
        this.lastAuthenticatedTime = lastAuthenticatedTime;
    }

    public long getCreatedTimeUTC() {
        return createdTimeUTC;
    }

    public void setCreatedTimeUTC(long createdTimeUTC) {
        this.createdTimeUTC = createdTimeUTC;
    }

    public String getApnsDeviceToken() {
        return apnsDeviceToken;
    }

    public void setApnsDeviceToken(String apnsDeviceToken) {
        this.apnsDeviceToken = apnsDeviceToken;
    }

    public ArrayList<String> getSpecialitiesArray() {
        return specialitiesArray;
    }

    public void setSpecialitiesArray(ArrayList<String> specialitiesArray) {
        this.specialitiesArray = specialitiesArray;
    }

    public ArrayList<String> getDesignationArray() {
        return designationArray;
    }

    public void setDesignationArray(ArrayList<String> designationArray) {
        this.designationArray = designationArray;
    }

    public boolean containAuthoritie(String role_access_patient_btg) {
        if(authorities!=null ){
            Iterator<Authority> iterator = authorities.iterator();
            while (iterator!=null&& iterator.hasNext()){
                Authority next = iterator.next();
                if(next.getAuthority().equalsIgnoreCase(role_access_patient_btg)){
                    return true;
                }
            }
        }
        return false;
    }

    public class InviteUserPriGroupObject {
        @SerializedName("MANAGER")
        private String manager;

        @SerializedName("PRACTITIONER")
        private String practitioner;

        @SerializedName("INTERNAL_PARTICIPANT")
        private String internalParticipant;

        @SerializedName("EXTERNAL_PARTICIPANT")
        private String externalParticipant;

        @SerializedName("PATIENT")
        private String patient;

        public String getManager() {
            return manager;
        }

        public void setManager(String manager) {
            this.manager = manager;
        }

        public String getPractitioner() {
            return practitioner;
        }

        public void setPractitioner(String practitioner) {
            this.practitioner = practitioner;
        }

        public String getInternalParticipant() {
            return internalParticipant;
        }

        public void setInternalParticipant(String internalParticipant) {
            this.internalParticipant = internalParticipant;
        }

        public String getExternalParticipant() {
            return externalParticipant;
        }

        public void setExternalParticipant(String externalParticipant) {
            this.externalParticipant = externalParticipant;
        }

        public String getPatient() {
            return patient;
        }

        public void setPatient(String patient) {
            this.patient = patient;
        }
    }

    protected UserMO(Parcel in) {
        super(in);
        id = in.readLong();
        username = in.readString();
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        phoneNumber = in.readString();
        countryCode = in.readString();
        gender = in.readString();
        dob = in.readString();
        byte isEnableVal = in.readByte();
        enabled = isEnableVal == 0x02 ? false : isEnableVal != 0x00;
        byte accountLockedVal = in.readByte();
        accountLocked = accountLockedVal == 0x02 ? false : accountLockedVal != 0x00;
        physicianId = in.readLong();
        patientId = in.readLong();
        userType = in.readString();
        password = in.readString();
        roles = in.readString();
        realName = in.readString();
        deviceUID = in.readString();
        byte registrationStatusVal = in.readByte();
        registrationStatus = registrationStatusVal == 0x02 ? false : registrationStatusVal != 0x00;
        deviceActivationDate = in.readString();
        groupName = in.readString();
        privilegeGroup = in.readString();
        aboutMe = in.readString();
        specialty = in.readString();
        designation = in.readString();
        if (in.readByte() == 0x01) {
            userPrivileges = new ArrayList<String>();
            in.readList(userPrivileges, String.class.getClassLoader());
        } else {
            userPrivileges = null;
        }
        height = in.readParcelable(Height.class.getClassLoader());
        weight = in.readParcelable(Weight.class.getClassLoader());
        members = in.readParcelable(Members.class.getClassLoader());
        byte hasSignedUpVal = in.readByte();
        hasSignedUp = hasSignedUpVal == 0x02 ? false : hasSignedUpVal != 0x00;

        if (in.readByte() == 0x01) {
            inviteUserPriGroupObject = new ArrayList<InviteUserPriGroupObject>();
            in.readList(inviteUserPriGroupObject, InviteUserPriGroupObject.class.getClassLoader());
        } else {
            inviteUserPriGroupObject = null;
        }
        inviteUserPriGroupMap = (LinkedHashMap) in.readValue(LinkedHashMap.class.getClassLoader());
        if (profilePic == null) {
            profilePic = new byte[0];
        }
        this.profilePic = new byte[in.readInt()];
        in.readByteArray(this.profilePic);
        fisikeCurrentStatus = in.readString();
        if (in.readByte() == 0x01) {
            specialitiesArray = new ArrayList<String>();
            in.readList(specialitiesArray, String.class.getClassLoader());
        } else {
            specialitiesArray = null;
        }
        if (in.readByte() == 0x01) {
            designationArray = new ArrayList<String>();
            in.readList(designationArray, String.class.getClassLoader());
        } else {
            designationArray = null;
        }
        age = in.readInt();
        status = in.readString();
        statusMessage = in.readString();

        if (in.readByte() == 0x01) {
            chatConversationKeyList = new ArrayList<String>();
            in.readList(chatConversationKeyList, String.class.getClassLoader());
        } else {
            chatConversationKeyList = null;
        }
        lastAuthenticatedTime = in.readLong();
        createdTimeUTC = in.readLong();
        apnsDeviceToken = in.readString();

        byte profilePicUpdateStatusVal = in.readByte();
        profilePicUpdateStatus = profilePicUpdateStatusVal == 0x02 ? false : profilePicUpdateStatusVal != 0x00;
        byte SignUpStatusVal = in.readByte();
        SignUpStatus = SignUpStatusVal == 0x02 ? false : SignUpStatusVal != 0x00;
        SignUpTime = in.readLong();
        salutation = in.readString();
        addressUserMos = (ArrayList<Address>) in.readSerializable();
        alternateContact = in.readString();
        maritalStatus = in.readString();
        middleName=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(id);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(phoneNumber);
        dest.writeString(countryCode);
        dest.writeString(gender);
        dest.writeString(dob);
        if (enabled == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (enabled ? 0x01 : 0x00));
        }
        if (accountLocked) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (accountLocked ? 0x01 : 0x00));
        }
        dest.writeLong(physicianId);
        dest.writeLong(patientId);
        dest.writeString(userType);
        dest.writeString(password);
        dest.writeString(roles);
        dest.writeString(realName);
        dest.writeString(deviceUID);
        if (registrationStatus == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (registrationStatus ? 0x01 : 0x00));
        }
        dest.writeString(deviceActivationDate);
        dest.writeString(groupName);
        dest.writeString(privilegeGroup);
        dest.writeString(aboutMe);
        dest.writeString(specialty);
        dest.writeString(designation);
        if (userPrivileges == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(userPrivileges);
        }
        dest.writeParcelable(new Height(), flags);
        dest.writeParcelable(new Weight(), flags);
        dest.writeParcelable(new Members(), flags);

        if (hasSignedUp == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (hasSignedUp ? 0x01 : 0x00));
        }
        if (inviteUserPriGroupObject == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(inviteUserPriGroupObject);
        }
        dest.writeValue(inviteUserPriGroupMap);

        if (profilePic == null) {
            profilePic = new byte[0];
        }
        dest.writeInt(profilePic.length);
        dest.writeByteArray(profilePic);
        dest.writeString(fisikeCurrentStatus);
        if (specialitiesArray == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(specialitiesArray);
        }
        if (designationArray == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(designationArray);
        }
        dest.writeInt(age);
        dest.writeString(status);
        dest.writeString(statusMessage);
        if (chatConversationKeyList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(chatConversationKeyList);
        }
        dest.writeLong(lastAuthenticatedTime);
        dest.writeLong(createdTimeUTC);
        dest.writeString(apnsDeviceToken);
        if (profilePicUpdateStatus) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (profilePicUpdateStatus ? 0x01 : 0x00));
        }
        if (SignUpStatus) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (SignUpStatus ? 0x01 : 0x00));
        }
        dest.writeLong(SignUpTime);
        dest.writeString(salutation);
        dest.writeSerializable(new ArrayList<Address>());
        dest.writeString(alternateContact);
        dest.writeString(maritalStatus);
        dest.writeString(middleName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserMO> CREATOR = new Parcelable.Creator<UserMO>() {
        @Override
        public UserMO createFromParcel(Parcel in) {
            return new UserMO(in);
        }

        @Override
        public UserMO[] newArray(int size) {
            return new UserMO[size];
        }
    };

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Height getHeight() {
        return height;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public ArrayList<Members> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Members> members) {
        this.members = members;
    }

    public void copyValues(UserMO src, UserMO dest) {
        if (src != null && dest != null) {
        }
    }

    public String getmPIN() {
        return mPIN;
    }

    public void setmPIN(String mPIN) {
        this.mPIN = mPIN;
    }


    public String getUserFullName() {

        return firstName + (Utils.isValueAvailable(middleName)?" "+middleName:"")+
                (Utils.isValueAvailable(lastName) ? " " + lastName : "");
    }

    public String getDob() {
        return dob;
    }

    public String getAadharNumber() {
        String aadharNumber = "";
        try {
            if (identifierArrayList != null && identifierArrayList.size() > 0) {
                for (int i = 0; i < identifierArrayList.size(); i++) {
                    FetchUpdateIdentifier telecom = identifierArrayList.get(i);
                    if (Utils.isValueAvailable(telecom.getText()) &&
                            telecom.getText().equalsIgnoreCase(MyApplication.getAppContext().getString(R.string.aadhar_key))) {
                        aadharNumber = telecom.getValue();
                        return aadharNumber;
                    }

                }
            }
        } catch (Exception e) {

        }
        return aadharNumber;

    }

    public boolean isCorrectAgreementStatus() {
        return correctAgreementStatus;
    }

    public void setCorrectAgreementStatus(boolean correctAgreementStatus) {
        this.correctAgreementStatus = correctAgreementStatus;
    }

    public String getCorrectAgreementMessage() {
        return correctAgreementMessage;
    }

    public void setCorrectAgreementMessage(String correctAgreementMessage) {
        this.correctAgreementMessage = correctAgreementMessage;
    }

    public ArrayList<AgreementModel> getAgreements() {

        return agreements;
    }

    public void setAgreements(ArrayList<AgreementModel> agreements) {
        this.agreements = agreements;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<Coverage> getCoverage() {
        return coverage;
    }

    public void setCoverage(ArrayList<Coverage> coverage) {
        this.coverage = coverage;
    }

    public ArrayList<Integer> getPendingDiagnosticOrders() {
        return pendingDiagnosticOrders;
    }

    public void setPendingDiagnosticOrders(ArrayList<Integer> pendingDiagnosticOrders) {
        this.pendingDiagnosticOrders = pendingDiagnosticOrders;
    }

    public ArrayList<FetchUpdateIdentifier> getIdentifierArrayList() {
        return identifierArrayList;
    }

    public void setIdentifierArrayList(ArrayList<FetchUpdateIdentifier> identifierArrayList) {
        this.identifierArrayList = identifierArrayList;
    }

    public ArrayList<FetchUpdateTelecom> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(ArrayList<FetchUpdateTelecom> phoneList) {
        this.phoneList = phoneList;
    }

    public ArrayList<FetchUpdateTelecom> getEmailList() {
        return emailList;
    }

    public void setEmailList(ArrayList<FetchUpdateTelecom> emailList) {
        this.emailList = emailList;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}