package com.mphrx.fisike.mo;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.constant.VariableConstants;

public class GroupTextChatMO extends MobileObject {
    private String chatConversationMOPK;
    private ArrayList<String> admin;
    private byte[] imageData;

    public GroupTextChatMO() {
        super();
    }

    public String getChatConversationMOPK() {
        return chatConversationMOPK;
    }

    public void setChatConversationMOPK(String chatConversationMOPK) {
        this.chatConversationMOPK = chatConversationMOPK;
    }

    public void generateChatConversationMOPK(String groupID, String userEmail) {
        // username
        String uniqueString = "ChatConversationMO" + groupID + userEmail;
        setChatConversationMOPK(uniqueString.hashCode() + "");
    }

    public ArrayList<String> getAdmin() {
        return admin;
    }

    public void setAdmin(ArrayList<String> admin) {
        this.admin = admin;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public void generatePersistenceKey(String groupID, String userEmail) {
        // useremail
        String uniqueString = VariableConstants.GROUP_TEXT_CHAT_MO + groupID + userEmail;
        setPersistenceKey(uniqueString.hashCode());
    }

    public boolean equalsProfilePic(byte[] imageData) {
        if (null == this.imageData && null != imageData) {
            return false;
        } else if ((null != this.imageData) && (null != imageData && this.imageData.equals(imageData))) {
            return true;
        }
        return false;
    }

    protected GroupTextChatMO(Parcel in) {
        super(in);
        chatConversationMOPK = in.readString();
        if (in.readByte() == 0x01) {
            admin = new ArrayList<String>();
            in.readList(admin, String.class.getClassLoader());
        } else {
            admin = null;
        }
        if (imageData == null) {
            imageData = new byte[0];
        }
        this.imageData = new byte[in.readInt()];
        in.readByteArray(this.imageData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(chatConversationMOPK);
        if (admin == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(admin);
        }
        if (imageData == null) {
            imageData = new byte[0];
        }
        dest.writeInt(imageData.length);
        dest.writeByteArray(imageData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GroupTextChatMO> CREATOR = new Parcelable.Creator<GroupTextChatMO>() {
        @Override
        public GroupTextChatMO createFromParcel(Parcel in) {
            return new GroupTextChatMO(in);
        }

        @Override
        public GroupTextChatMO[] newArray(int size) {
            return new GroupTextChatMO[size];
        }
    };
}