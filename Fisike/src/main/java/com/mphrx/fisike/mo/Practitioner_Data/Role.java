package com.mphrx.fisike.mo.Practitioner_Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Neha on 23-12-2016.
 */

public class Role implements Serializable {
    private static final long serialVersionUID = 4041924330940772189L;
    Object[] coding;
    Object[] extension;
    String id;
    String text;

    public void setCoding(Object[] coding) {
        this.coding = coding;
    }

    public void setExtension(Object[] extension) {
        this.extension = extension;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object[] getCoding() {
        return coding;
    }

    public Object[] getExtension() {
        return extension;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

/*
    protected Role(Parcel in) {
        id = in.readString();
        text = in.readString();
        coding = in.readArray(ClassLoader.getSystemClassLoader());
        extension = in.readArray(ClassLoader.getSystemClassLoader());

    }

    public static final Creator<Role> CREATOR = new Creator<Role>() {
        @Override
        public Role createFromParcel(Parcel in) {
            return new Role(in);
        }

        @Override
        public Role[] newArray(int size) {
            return new Role[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(text);
        dest.writeArray(coding);
        dest.writeArray(extension);

    }
*/


}
