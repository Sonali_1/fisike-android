package com.mphrx.fisike.mo;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.Streams;

import java.util.LinkedHashMap;

public class ConfigMO extends MobileObject {

    public static final long CONFIG_KEY = "MphRxConfig".hashCode();

    @SerializedName("isChatEnabled")
    private boolean isChatEnabled;

    @SerializedName("isShowPin")
    private boolean isShowPin;

    private long autoLockMinute;

    public int getPinLockTime() {
        return pinLockTime;
    }

    public void setPinLockTime(int pinLockTime) {
        this.pinLockTime = pinLockTime;
    }

    private int pinLockTime;

    private LinkedHashMap<String,String> languageList;

    @SerializedName("enableChangePinStatus")
    private boolean canChangePinStatus;

    private long noOfDaysForAlternateContact;

    private String otpAttemptsLeft;
    private String passwordRegex;

    public boolean isChatEnabled() {
        return isChatEnabled;
    }

    public void setChatEnabled(boolean isChatEnabled) {
        this.isChatEnabled = isChatEnabled;
    }

    public boolean isShowPin() {
        return isShowPin;
    }

    public void setShowPin(boolean isShowPin)
    {
        this.isShowPin = isShowPin;
    }

    public boolean isCanChangePinStatus() {
        return canChangePinStatus;
    }

    public void setCanChangePinStatus(boolean canChangePinStatus) {
        this.canChangePinStatus = canChangePinStatus;
    }


    public long getNoOfDaysForAlternateContact() {
        return noOfDaysForAlternateContact;
    }

    public void setNoOfDaysForAlternateContact(long value) {
        this.noOfDaysForAlternateContact=value;
    }

    public long getAutoLockMinute() {
        return 0;
    }

    public void setAutoLockMinute(long autoLockMinute) {
        this.autoLockMinute = autoLockMinute;
    }

    public LinkedHashMap<String, String> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(LinkedHashMap<String, String> languageList) {
        this.languageList = languageList;
    }

    public String getOtpAttemptsLeft() {
        return otpAttemptsLeft;
    }

    public void setOtpAttemptsLeft(String otpAttemptsLeft) {
        this.otpAttemptsLeft = otpAttemptsLeft;
    }

    public String getPasswordRegex() {
        return passwordRegex;
    }

    public void setPasswordRegex(String passwordRegex) {
        this.passwordRegex = passwordRegex;
    }
}