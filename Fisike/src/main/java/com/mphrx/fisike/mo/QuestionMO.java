package com.mphrx.fisike.mo;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class QuestionMO extends MobileObject {

	private ArrayList<String> ansTextArray;

	@SerializedName("optionsLink")
	private ArrayList<String> imageUrlArray;

	@SerializedName("options")
	private ArrayList<String> optionsArray;

	@SerializedName("description")
	private String questionDescription;

	@SerializedName("questionId")
	private String questionID;

	@SerializedName("question")
	private String questionText;

	@SerializedName("questionType")
	private String questionType;

	    
	    public QuestionMO(){
	    	super();
	    }

	    public ArrayList<String> getAnsTextArray() {
	        return ansTextArray;
	    }

	    public void setAnsTextArray(ArrayList<String> ansTextArray) {
	        this.ansTextArray = ansTextArray;
	    }


	    public ArrayList<String> getImageUrlArray() {
	        return imageUrlArray;
	    }

	    public void setImageUrlArray(ArrayList<String> imageUrlArray) {
	        this.imageUrlArray = imageUrlArray;
	    }

	    public ArrayList<String> getOptionsArray() {
	        return optionsArray;
	    }

	    public void setOptionsArray(ArrayList<String> optionsArray) {
	        this.optionsArray = optionsArray;
	    }

	    public String getQuestionDescription() {
	        return questionDescription;
	    }

	    public void setQuestionDescription(String questionDescription) {
	        this.questionDescription = questionDescription;
	    }

	    public String getQuestionID() {
	        return questionID;
	    }

	    public void setQuestionID(String questionID) {
	        this.questionID = questionID;
	    }

	    public String getQuestionText() {
	        return questionText;
	    }

	    public void setQuestionText(String questionText) {
	        this.questionText = questionText;
	    }

	    public String getQuestionType() {
	        return questionType;
	    }

	    public void setQuestionType(String questionType) {
	        this.questionType = questionType;
	    }

	    public void generatePersistenceKey(String activityId, String sender) {
	        String uniqueString = "QuestionMO" + activityId + questionID + sender;

	        setPersistenceKey(uniqueString.hashCode());
	    }

	    protected QuestionMO(Parcel in) {
	    	super(in);
	        if (in.readByte() == 0x01) {
	            ansTextArray = new ArrayList<String>();
	            in.readList(ansTextArray, String.class.getClassLoader());
	        } else {
	            ansTextArray = null;
	        }
	        if (in.readByte() == 0x01) {
	            imageUrlArray = new ArrayList<String>();
	            in.readList(imageUrlArray, String.class.getClassLoader());
	        } else {
	            imageUrlArray = null;
	        }
	        if (in.readByte() == 0x01) {
	            optionsArray = new ArrayList<String>();
	            in.readList(optionsArray, String.class.getClassLoader());
	        } else {
	            optionsArray = null;
	        }
	        questionDescription = in.readString();
	        questionID = in.readString();
	        questionText = in.readString();
	        questionType = in.readString();
	    }

	    @Override
	    public int describeContents() {
	        return 0;
	    }

	    @Override
	    public void writeToParcel(Parcel dest, int flags) {
	    	super.writeToParcel(dest, flags);
	        if (ansTextArray == null) {
	            dest.writeByte((byte) (0x00));
	        } else {
	            dest.writeByte((byte) (0x01));
	            dest.writeList(ansTextArray);
	        }
	        if (imageUrlArray == null) {
	            dest.writeByte((byte) (0x00));
	        } else {
	            dest.writeByte((byte) (0x01));
	            dest.writeList(imageUrlArray);
	        }
	        if (optionsArray == null) {
	            dest.writeByte((byte) (0x00));
	        } else {
	            dest.writeByte((byte) (0x01));
	            dest.writeList(optionsArray);
	        }
	        dest.writeString(questionDescription);
	        dest.writeString(questionID);
	        dest.writeString(questionText);
	        dest.writeString(questionType);
	    }

	    @SuppressWarnings("unused")
	    public static final Parcelable.Creator<QuestionMO> CREATOR = new Parcelable.Creator<QuestionMO>() {
	        @Override
	        public QuestionMO createFromParcel(Parcel in) {
	            return new QuestionMO(in);
	        }

	        @Override
	        public QuestionMO[] newArray(int size) {
	            return new QuestionMO[size];
	        }
	    };
	}
