package com.mphrx.fisike.mo;

import android.os.Parcel;
import android.os.Parcelable;

public class MobileObject implements Parcelable {
	private long persistenceKey;

	public MobileObject(){
		
	}
    public long getPersistenceKey() {
        return persistenceKey;
    }

    public void setPersistenceKey(long persistenceKey) {
        this.persistenceKey = persistenceKey;
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {	
		dest.writeLong(persistenceKey);
	}
	
	protected MobileObject(Parcel in){
		persistenceKey = in.readLong();
	}
}