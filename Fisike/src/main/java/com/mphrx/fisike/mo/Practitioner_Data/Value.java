package com.mphrx.fisike.mo.Practitioner_Data;

import java.io.Serializable;

/**
 * Created by Neha on 26-12-2016.
 */

public class Value implements Serializable
{
  private static final long serialVersionUID = -6626496484766166333L;
  String noOfExp;

  public String getNoOfExp() {
    return noOfExp;
  }

  public void setNoOfExp(String noOfExp) {
    this.noOfExp = noOfExp;
  }
}
