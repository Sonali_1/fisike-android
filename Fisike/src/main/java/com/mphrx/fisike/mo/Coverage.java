package com.mphrx.fisike.mo;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.Utils;

import java.io.Serializable;

/**
 * Created by aastha on 7/13/2017.
 */

public class Coverage implements Serializable {

    private static final long serialVersionUID = -4823682150452602231L;
    @Expose
    private int id;
    @Expose
    private int subscriberRefId;
    @Expose
    private String policyNumber;
    @Expose
    private String insuranceType;
    @Expose
    private String status;
    @Expose
    private String policyHolderName;
    @Expose
    private String primaryInsuranceCompany;
    @Expose
    private String backImage;
    @Expose
    private String frontImage;
    @Expose
    private String backImageMimeType;


    @Expose

    private String frontImageMimeType;

    private String groupId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubscriberRefId() {
        return subscriberRefId;
    }

    public void setSubscriberRefId(int subscriberRefId) {
        this.subscriberRefId = subscriberRefId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPolicyHolderName() {
        return policyHolderName;
    }

    public void setPolicyHolderName(String policyHolderName) {
        this.policyHolderName = policyHolderName;
    }

    public String getPrimaryInsuranceCompany() {
        return primaryInsuranceCompany;
    }

    public void setPrimaryInsuranceCompany(String primaryInsuranceCompany) {
        this.primaryInsuranceCompany = primaryInsuranceCompany;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        this.frontImage = frontImage;
    }

    public String getBackImageMimeType() {
        return backImageMimeType;
    }

    public void setBackImageMimeType(String backImageMimeType) {
        this.backImageMimeType = backImageMimeType;
    }

    public String getFrontImageMimeType() {
        return frontImageMimeType;
    }

    public void setFrontImageMimeType(String frontImageMimeType) {
        this.frontImageMimeType = frontImageMimeType;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    //return true if manual insurance
    public boolean isManualInsurance(Context context)
    {
        try {
            if(Utils.isValueAvailable(insuranceType) && !insuranceType.equalsIgnoreCase(context.getString(R.string.insurance_type_picture)))
                return true;

        }
        catch (Exception e)
        {

        }
    return false;
    }


    //return true if medicare insurance type
    public boolean isMedicareInsurance(Context context)
    {
        try{
        if(Utils.isValueAvailable(insuranceType) && insuranceType.equalsIgnoreCase(context.getString(R.string.insurance_type_medicare)))
            return true; }
        catch (Exception e)
        {}
        return false;
    }

    //return true if medicaid insurance type
    public boolean isMedicaidInsurance(Context context)
    {
        try{
            if(Utils.isValueAvailable(insuranceType) && insuranceType.equalsIgnoreCase(context.getString(R.string.insurance_type_medicaid)))
                return true; }
        catch (Exception e)
        {}
        return false;
    }

    //return true if other insurance type
    public boolean isOtherInsurance(Context context)
    {
        try{
            if(Utils.isValueAvailable(insuranceType) && insuranceType.equalsIgnoreCase(context.getString(R.string.insurance_type_other)))
                return true; }
        catch (Exception e)
        {}
        return false;
    }
}

