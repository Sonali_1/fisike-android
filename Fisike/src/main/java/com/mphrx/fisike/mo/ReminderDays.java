package com.mphrx.fisike.mo;

import java.util.ArrayList;

public class ReminderDays {
    private ArrayList<String> reminderDay;

    public ArrayList<String> getDay() {
        return reminderDay;
    }

    public void setDay(ArrayList<String> day) {
        this.reminderDay = day;
    }

}
