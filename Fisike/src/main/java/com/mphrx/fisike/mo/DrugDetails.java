package com.mphrx.fisike.mo;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DrugDetails {
	// medicine Id
	private int medicineId;
	private String medicineName;
	// prescriber Id
	private int prescriberID;
	
	private String prescriberName;
	
	// encounter ID
	private int encounterID;

	private int dosePerDay;
	private String doseTimeDescription;
	private double medicinePerDayCount;
	private String unit;
	private ArrayList<String> doseTime;
	private ArrayList<DiseaseMO> diseaseMoList;

	private String startDate;
	private String endDate;
	private boolean isReminderAdded;
	
	private int medicationPrescriptionID;

	public int getMedicationPrescriptionID() {
		return medicationPrescriptionID;
	}

	public void setMedicationPrescriptionID(int medicationPrescriptionID) {
		this.medicationPrescriptionID = medicationPrescriptionID;
	}

	public boolean isReminderAdded() {
		return isReminderAdded;
	}

	public void setReminderAdded(boolean isReminderAdded) {
		this.isReminderAdded = isReminderAdded;
	}

	private LinkedHashMap<String, Double> dosageInfo = new LinkedHashMap<String, Double>();

	public LinkedHashMap<String, Double> getDosageInfo() {
		return dosageInfo;
	}
	
	public String getPrescriberName() {
		return prescriberName;
	}


	public void setPrescriberName(String prescriberName) {
		this.prescriberName = prescriberName;
	}

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setDosageInfo(LinkedHashMap<String, Double> dosageInfo) {
		this.dosageInfo = dosageInfo;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getEncounterID() {
		return encounterID;
	}

	public void setEncounterID(int encounterID) {
		this.encounterID = encounterID;
	}

	public int getPrescriberID() {
		return prescriberID;
	}

	public void setPrescriberID(int prescriberID) {
		this.prescriberID = prescriberID;
	}

	public ArrayList<String> getDoseTime() {
		return doseTime;
	}

	public void setDoseTime(ArrayList<String> doseTime) {
		this.doseTime = doseTime;
	}

	public String getMedicineUnits() {
		return unit;
	}

	public void setMedicineUnits(String medicineUnits) {
		this.unit = medicineUnits;
	}

	public Double getMedicinePerDayCount() {
		return medicinePerDayCount;
	}

	public void setMedicinePerDayCount(int medicinePerDayCount) {
		this.medicinePerDayCount = medicinePerDayCount;
	}

	public String getDoseTimeDescription() {
		return doseTimeDescription;
	}

	public void setDoseTimeDescription(String doseTimeDescription) {
		this.doseTimeDescription = doseTimeDescription;
	}

	public int getMedicineId() {
		return medicineId;
	}

	public void setMedicineId(int medicineName) {
		this.medicineId = medicineName;
	}

	public int getDosePerDay() {
		return dosePerDay;
	}

	public void setDosePerDay(int dosePerDay) {
		this.dosePerDay = dosePerDay;
	}
	
	public void setDiseaseMoList(ArrayList<DiseaseMO> diseaseMoList) {
		this.diseaseMoList = diseaseMoList;
	}

	public ArrayList<DiseaseMO> getDiseaseMoList() {
		return diseaseMoList;
	}

}
