package com.mphrx.fisike.asynctaskmanger;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;

public class StoreChatConversation extends AbstractAsyncTask<Void, Void, Boolean> {

    private ChatMessageMO chatMsgMO;
    private ChatConversationMO chatConvMO;
    private boolean isToShowNotification;
    private Context context;
    private ContentValues contentValues;

    /**
     * TODO store chat conversation and chat message diffrently TODO manage single instance
     *
     * @param context
     * @param isToShowProgress
     * @param chatConvMO
     * @param chatMsgMO
     * @param isToShowNotification
     */

    public StoreChatConversation(ApiResponseCallback<Boolean> callback, Context context, boolean isToShowProgress, ChatConversationMO chatConvMO, ChatMessageMO chatMsgMO, boolean isToShowNotification) {
        super(callback, context, isToShowProgress);
        this.chatConvMO = chatConvMO;
        this.chatMsgMO = chatMsgMO;
        this.isToShowNotification = isToShowNotification;
        this.context = context;
    }

    public StoreChatConversation(ApiResponseCallback<Boolean> callback, Context context, ChatConversationMO chatConvMO, ContentValues contentValues) {
        super(callback, context, false);
        this.chatConvMO = chatConvMO;
        this.context = context;
        this.contentValues = contentValues;
    }

    @Override
    protected Boolean getResult(Void... params) {
        runStoreChatTask(chatMsgMO, chatConvMO);
        return isToShowNotification;
    }

    /**
     * This method stores the chat message and conversation synchronously
     *
     * @param chatMsgMO
     * @param chatConversationMO
     */
    private boolean runStoreChatTask(ChatMessageMO chatMsgMO, ChatConversationMO chatConversationMO) {
//        boolean msgSaved = true;
//        boolean convSaved = true;
//        if (null != chatMsgMO) {
//            msgSaved = DBAdapter.getInstance(context).createOrUpdateChatMessageMO(chatMsgMO, false);
//        }
//        if (null != chatConversationMO) {
//            convSaved = ChatConversationDBAdapter.getInstance(context).createOrUpdateChatConversationMO(chatConversationMO, chatConversationMO.getReadStatus());
//        }
//
//        if (msgSaved && convSaved) {
//            return true;
//        } else {
//            return false;
//        }

        if (contentValues != null) {
            ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).updateChatConversation(chatConvMO.getPersistenceKey(), contentValues);
            return true;
        } else {
            return ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).insertChatConversationMo(chatConvMO);
        }


    }
}
