package com.mphrx.fisike.asynctaskmanger;

import java.util.HashMap;
import java.util.Iterator;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smackx.packet.DiscoverItems;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.asmack.wrappers.GroupRosterPacket;
import com.mphrx.fisike.background.LoadGroupContactProfilePic;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.ProfilePicTaskManager;
import com.mphrx.fisike.utils.Utils;

public class GroupListBG extends AbstractAsyncTask<DiscoverItems, Void, HashMap<String, ChatConversationMO>> {

    private DBAdapter mHelper;
    private Context context;
    private UserMO userMO;
    private HashMap<String, ChatConversationMO> conversationsGroup;
    private MessengerService mService;

    public GroupListBG(ApiResponseCallback<HashMap<String, ChatConversationMO>> callback, Context context,
                       HashMap<String, ChatConversationMO> conversationsGroup, MessengerService mService) {
        super(callback, context, false);
        this.context = context;
        this.conversationsGroup = conversationsGroup;
        this.mService = mService;
        mHelper = DBAdapter.getInstance(context);
        userMO = SettingManager.getInstance().getUserMO();
    }

    @Override
    protected HashMap<String, ChatConversationMO> getResult(DiscoverItems... discoverItems) {
        processGroupsListNames(discoverItems[0]);
        return conversationsGroup;
    }

    /**
     * Iterate items for groupNames
     *
     * @param discoverItems
     */
    private void processGroupsListNames(DiscoverItems discoverItems) {
        Iterator<org.jivesoftware.smackx.packet.DiscoverItems.Item> items = discoverItems.getItems();

        while (items.hasNext()) {
            DiscoverItems.Item item = items.next();
            final String entityID = item.getEntityID();
            String itemName = item.getName();

            saveGroupItem(entityID, itemName);
        }
    }

    /**
     * Save group item/name received as stanza
     *
     * @param entityID
     * @param groupName
     */
    private void saveGroupItem(String entityID, String groupName) {
        ChatConversationMO chatConversationMO = new ChatConversationMO();
        chatConversationMO.setJId(entityID);
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");
        boolean isUpdate = true;
        try {
            chatConversationMO = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(chatConversationMO.getPersistenceKey() + "");
        } catch (Exception e) {
        }

        if (null == chatConversationMO) {
            chatConversationMO = new ChatConversationMO();//create call otherwise update
            isUpdate = false;
        }

        chatConversationMO.setJId(entityID);
        if (null != groupName) {
            chatConversationMO.setGroupName(groupName);
            if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                chatConversationMO.setNickName(groupName);
            }
        }
        chatConversationMO.setReadStatus(false);
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");// 1421047816.969098_pat1%40khura.33mail.com@conference.dev1
        int conversationType = groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER) ? VariableConstants.conversationTypeChat
                : VariableConstants.conversationTypeGroup;
        chatConversationMO.setConversationType(conversationType);
        chatConversationMO.setMyStatus(VariableConstants.chatConversationMyStatusActive);

        GroupTextChatMO groupTextChatMo = new GroupTextChatMO();
        String extractGroupId = Utils.extractGroupId(entityID);
        groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
        boolean isGroupdetailsExist = true;
        try {
            groupTextChatMo = GroupChatDBAdapter.getInstance(context).fetchGroupTextChatMO(groupTextChatMo.getPersistenceKey() + "");
        } catch (Exception e) {
        }
        if (null == groupTextChatMo) {
            isGroupdetailsExist = false;
            groupTextChatMo = new GroupTextChatMO();
        }
        groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
        groupTextChatMo.setChatConversationMOPK(chatConversationMO.getPersistenceKey() + "");
        try {

            if (isUpdate) {
                ContentValues values = new ContentValues();
                values.put(ChatConversationDBAdapter.getPERSISTENCEKEY(), chatConversationMO.getPersistenceKey());
                values.put(ChatConversationDBAdapter.getGROUPNAME(), chatConversationMO.getGroupName());
                values.put(ChatConversationDBAdapter.getNICKNAME(), chatConversationMO.getNickName());
                values.put(ChatConversationDBAdapter.getREADSTATUS(), chatConversationMO.getReadStatus());
                values.put(ChatConversationDBAdapter.getCONVERSATIONTYPE(), chatConversationMO.getConversationType());
                values.put(ChatConversationDBAdapter.getMYSTATUS(), chatConversationMO.getMyStatus());
                ChatConversationDBAdapter.getInstance(context).updateChatConversation(chatConversationMO.getPersistenceKey(), values);
            } else {
                ChatConversationDBAdapter.getInstance(context).insertChatConversationMo(chatConversationMO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if(isGroupdetailsExist)
                {
                ContentValues values=new ContentValues();
                values.put(GroupChatDBAdapter.getCHATCONVERSATIONMOPK(),groupTextChatMo.getChatConversationMOPK());
                }
            else
                GroupChatDBAdapter.getInstance(context).insert(groupTextChatMo);

            }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (null == conversationsGroup) {
            conversationsGroup = new HashMap<String, ChatConversationMO>();
        }

        conversationsGroup.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);

        getGroupMembers(entityID);
        if (!isGroupdetailsExist && null == groupTextChatMo.getImageData()) {
            if (Utils.showDialogForNoNetwork(context, false)) {
                ProfilePicTaskManager.getInstance().addTask(new LoadGroupContactProfilePic(mService, extractGroupId));
                ProfilePicTaskManager.getInstance().startTask();
            }
        }
    }

    /**
     * Get the list of group members
     */
    public void getGroupMembers(String jid) {
        XMPPConnection connection = ConnectionInfo.getInstance().getXmppConnection();
        if (null != connection && connection.isConnected() && connection.isAuthenticated()) {
            try {
                GroupRosterPacket groupRosterPacket = new GroupRosterPacket();
                groupRosterPacket.setTo(jid);
                groupRosterPacket.setFrom(Utils.cleanXMPPUserName(connection.getUser()));

                connection.sendPacket(groupRosterPacket);
            } catch (Exception e) {
            }
        }
    }

}
