package com.mphrx.fisike.asynctaskmanger;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.persistence.ChatMessageDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;

public class StoreChatMessage extends AbstractAsyncTask<Void, Void, Boolean> {

    private ChatMessageMO chatMsgMO;
    private ChatConversationMO chatConvMO;
    private boolean isToShowNotification;
    private Context context;
    private ContentValues contentValues;

    /**
     *
     * @param context
     * @param isToShowProgress
     * @param chatConvMO
     * @param chatMsgMO
     * @param isToShowNotification
     */

    public StoreChatMessage(ApiResponseCallback<Boolean> callback, Context context, boolean isToShowProgress, ChatConversationMO chatConvMO, ChatMessageMO chatMsgMO, boolean isToShowNotification) {
        super(callback, context, isToShowProgress);
        this.chatConvMO = chatConvMO;
        this.chatMsgMO = chatMsgMO;
        this.isToShowNotification = isToShowNotification;
        this.context = context;
    }

    public StoreChatMessage(ApiResponseCallback<Boolean> callback, Context context, boolean isToShowProgress, ChatConversationMO chatConvMO, ChatMessageMO chatMsgMO, boolean isToShowNotification,ContentValues values) {
        super(callback, context, isToShowProgress);
        this.chatConvMO = chatConvMO;
        this.chatMsgMO = chatMsgMO;
        this.isToShowNotification = isToShowNotification;
        this.context = context;
        this.contentValues=values;
    }

    public StoreChatMessage(ApiResponseCallback<Boolean> callback, Context context, ChatConversationMO chatConvMO, ContentValues contentValues) {
        super(callback, context, false);
        this.chatConvMO = chatConvMO;
        this.context = context;
        this.contentValues = contentValues;
    }

    @Override
    protected Boolean getResult(Void... params) {
        runStoreChatTask(chatMsgMO, chatConvMO,contentValues);
        return isToShowNotification;
    }

    /**
     * This method stores the chat message and conversation synchronously
     *
     * @param chatMsgMO
     * @param chatConversationMO
     */
    private boolean runStoreChatTask(ChatMessageMO chatMsgMO, ChatConversationMO chatConversationMO,ContentValues contentValues) {
//        boolean msgSaved = true;
//        boolean convSaved = true;
        if (null != chatMsgMO) {


            if(contentValues!=null)
               return ChatMessageDBAdapter.getInstance(context).updateChatMessageMo(chatMsgMO.getPersistenceKey(), contentValues);
            else
               return ChatMessageDBAdapter.getInstance(context).insert(chatMsgMO);
        }
//        if (null != chatConversationMO) {
//            convSaved = ChatConversationDBAdapter.getInstance(context).createOrUpdateChatConversationMO(chatConversationMO, chatConversationMO.getReadStatus());
//        }
//
//        if (msgSaved && convSaved) {
//            return true;
//        } else {
//            return false;
//        }
        return false;
    }
}
