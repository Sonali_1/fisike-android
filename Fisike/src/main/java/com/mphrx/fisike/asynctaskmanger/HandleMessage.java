package com.mphrx.fisike.asynctaskmanger;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.DelayInformation;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.asynctask.DownloadGroupImageAsyncTask;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.PiwikConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.QuestionActivityMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.AttchmentDBAdapter;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.ChatMessageDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.TextPattern;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class HandleMessage extends AbstractAsyncTask<Packet, Void, Void> {

    private DBAdapter mHelper;
    private Context context;
    private UserMO userMO;
    private MessengerService mService;
    private String senderId;

    public HandleMessage(ApiResponseCallback<Void> callback, Context context, boolean isToShowProgress, MessengerService mService, String senderId) {
        super(callback, context, isToShowProgress);
        this.context = context;
        this.senderId = senderId;
        this.mService = mService;
        mHelper = DBAdapter.getInstance(context);
        this.mService = mService;
        userMO = SettingManager.getInstance().getUserMO();
    }

    @Override
    protected Void getResult(Packet... params) {
        Packet packet = params[0];
        if (((Message) packet).getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            String packetXml = packet.toXML();
            if (packetXml.contains("<subject>") && !packetXml.contains("<image") && !packetXml.contains("<body>")) {
                final String subject = ((Message) packet).getSubject();
                String from = packet.getFrom();
                String senderJid = Utils.getSenderJid(from);
                final String groupId = Utils.cleanXMPPUserName(from);
                mService.changeUpdateGroupName(subject, groupId);
                mService.getGroupMembers(groupId);
            } else {
                handleChat(packet, false);
            }
        } else if (((Message) packet).getType().equals(Message.Type.groupchat)) {
            String packetXml = packet.toXML();
            if (packetXml.contains("<body>") && !packetXml.contains("<subject>") && !packetXml.contains("<image>")) {
                // handle group chat message
                handleChat(packet, true);
            } else if (packetXml.contains("<subject>") && !packetXml.contains("<image") && !packetXml.contains("<body>")) {
                String subject = ((Message) packet).getSubject();
                String from = packet.getFrom();
                String senderJid = Utils.getSenderJid(from);
                String groupId = Utils.cleanXMPPUserName(from);
                mService.changeUpdateGroupName(subject, groupId);
            } else if (packetXml.contains("<image") && !packetXml.contains("<subject>") && !packetXml.contains("<body>")) {
                // handle change groupImage
                String from = packet.getFrom();
                String senderJid = Utils.getSenderJid(from);
                String groupId = Utils.cleanXMPPUserName(from);

                groupId = Utils.extractGroupId(groupId);
                //manohar
//                new DownloadGroupImageAsyncTask(mService, groupId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else if (((Message) packet).getType().equals(Message.Type.chat)) {
            handleChat(packet, false);
        }
        return null;
    }


    private void handleChat(Packet packet, boolean isGroupChat) {
        final Message message = (Message) packet;
        boolean isOfflineMessage = false;
        boolean isUpdateChatConversationMo=true;

        DelayInformation inf = null;
        try {
            inf = (DelayInformation) packet.getExtension("x", "jabber:x:delay");
            if (inf != null) {
                isOfflineMessage = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null == message.getBody()) {
            return;
        }
        ChatMessageMO chatMsgMO = new ChatMessageMO();

        // if (isGroupChat) {
        String from = !isGroupChat ? Utils.getSenderJid(message.getFrom()) : Utils.getGroupIdFrom(message.getFrom());
        chatMsgMO.setGroupID(Utils.cleanXMPPUserName(message.getFrom()));
        if (!isGroupChat) {
            from = Utils.cleanXMPPUserName(from);
        }
        // } else {
        // from = Utils.cleanXMPPUserName(message.getFrom());
        // }
        if (isGroupChat) {
            chatMsgMO.setSenderUserId(Utils.getSenderJid(message.getFrom()));
        } else {
            chatMsgMO.setSenderUserId(from);

            if (from != null) {
                String userId = from.contains("@") ? from.split("@")[0] : from;
                if (!chatContactDBAdapter.getInstance(context).isChatContactMoAvailable(userId)) {
                    mService.getGroupChatUsersDetails(userId);
                }
            }
        }
        chatMsgMO.setMsgId(packet.getPacketID());
        chatMsgMO.setLastUpdatedTimeUTC(System.currentTimeMillis());
        chatMsgMO.generatePersistenceKey();

        int conversationType = packet.getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) ? VariableConstants.conversationTypeChat
                : VariableConstants.conversationTypeGroup;

        // Get the chat conversation
        ChatConversationMO chatConvMO = null;
        if (isGroupChat) {
            chatConvMO = mService.getGroupConversationMO(Utils.cleanXMPPUserName(packet.getFrom()), from, null, true, false);

            String fromId = Utils.getSenderJid(message.getFrom());

            if (chatConvMO != null && chatConvMO.getChatContactMoKeys() == null) {

                ArrayList<String> chatContactMoKeys = ChatConversationDBAdapter.getInstance(context).addContactMoToConversation(chatConvMO, ChatContactMO.getPersistenceKey(TextPattern.getUserId(fromId)), "" + SettingManager.getInstance().getUserMO().getId());
                mService.updateChatContactMoKeysInChatConversationCacheObject(chatContactMoKeys, chatConvMO.getPersistenceKey() + "");

            }

        } else {
            chatConvMO = mService.getConversationMO(Utils.cleanXMPPUserName(packet.getFrom()), Utils.genrateSecoundryId(chatMsgMO.getGroupID()), null, true, false, chatMsgMO.getGroupID());
        }

        if (null == chatConvMO) {
            isUpdateChatConversationMo=false;
            chatConvMO = new ChatConversationMO();
            // chatConvMO.setConversationType(conversationType);
            chatConvMO.setSecoundryJid(Utils.genrateSecoundryId(chatMsgMO.getGroupID()));
            String tempGroupId = chatMsgMO.getGroupID();
            chatConvMO.setJId(tempGroupId);
            // chatConvMO.generatePersistenceKey(userMO.getUserName());
            chatConvMO.generatePersistenceKey(userMO.getId() + "");
            chatConvMO.setUnReadCount(0);
            if (tempGroupId.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                chatConvMO.setGroupName(VariableConstants.SINGLE_CHAT_IDENTIFIER);
            }
            mService.getGroupName(tempGroupId);
            mService.getGroupMembers(tempGroupId);
        }

        String attachmentId = message.getAttachmentId();
        String attachmentType = message.getAttachmentType();
        if (null != attachmentId && !"".equals(attachmentId)) {
            chatMsgMO.setAttachmentID(attachmentId);
            chatMsgMO.setAttachmentType(attachmentType);

            if (TextConstants.ATTACHMENT_ACTVITY.equalsIgnoreCase(attachmentType) && !userMO.getRoles().contains(TextConstants.ROLE_PATIENT)) {
                QuestionActivityMO questionActivityMO = new QuestionActivityMO();
                questionActivityMO.setActivityID(attachmentId);
                questionActivityMO.generatePersistenceKey(Utils.getFisikeUserName(from, SettingManager.getInstance().getSettings()
                        .getFisikeServerIp()));
                questionActivityMO.setActivitySubmitted(true);
                mService.saveQuestion(questionActivityMO, null);

				/*PiwikUtils.sendEvent(mService.getApplication(), PiwikConstants.KQB_CATEGORY, PiwikConstants.KQB_MSG_RECEIVED_ACTION,
                        PiwikConstants.KQB_RECEIVED_TXT);*/
            }
            /**
             * Attachment type is image , video or text
             */
            else {
                AttachmentMo attachmentMo = new AttachmentMo();
                attachmentMo.setAttachmentId(attachmentId);
                attachmentMo.setAttachmentType(attachmentType);
                attachmentMo.setChatMessageMOPKey(chatMsgMO.getPersistenceKey() + "");
                attachmentMo.setChatConversationMOPKey(chatConvMO.getPersistenceKey() + "");
                String nickName = chatConvMO.getNickName();
                attachmentMo.setSenderUserName((null != nickName && !"".equals(nickName)) ? nickName : chatConvMO.getJId());
                String realName = userMO.getRealName();
                attachmentMo.setReceiverUserName((null != realName && !"".equals(realName)) ? realName : userMO.getUsername());
                attachmentMo.generatePersistenceKey(chatConvMO.getJId());
                attachmentMo.setStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);
                chatMsgMO.setAttachmentStatus(TextConstants.ATTACHMENT_STATUS_RECEIVED);

                try {
                 //   mHelper.createOrUpdateAttachmentMO(attachmentMo);
                    AttchmentDBAdapter.getInstance(context).insert(attachmentMo);
                    }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            ChatMessageMO fetchChatMessageMO =  ChatMessageDBAdapter.getInstance(context).fetchChatMessageMO(chatMsgMO.getPersistenceKey() + "");
            if (fetchChatMessageMO != null) {
                // This message has already been received, so we can ignore this packet
                return;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        chatMsgMO.setMsgStatus(DefaultConnection.MESSAGE_RECEIVED);
        chatMsgMO.setMessageText(Utils.cleanMessageText(message.getBody()));

        chatConvMO.setLastMsg(chatMsgMO.getMessageText());
        chatConvMO.setLastMsgStatus(DefaultConnection.MESSAGE_RECEIVED);
        chatConvMO.setLastMsgId(chatMsgMO.getMsgId());
        chatConvMO.setCreatedTimeUTC(chatMsgMO.getLasUpdatedTimeUTC());
        chatConvMO.setReadStatus(false);
        chatConvMO.setConversationType(conversationType);

        chatMsgMO.setChatMessageMOType(conversationType);
        if (isGroupChat) {
            chatMsgMO.setGroupID(Utils.extractGroupId(from));
        }
        boolean isToIncrement = false;
        if (senderId == null || !senderId.equals(chatConvMO.getJId()) || Utils.isAppbackground(context)) {
            chatConvMO.incrementUnreadCount();
            isToIncrement = true;
            chatConvMO.setReadStatus(false);
        }
        ContentValues values = null;
        //update and insert
        if(isUpdateChatConversationMo)
        {
            mService.copyMessageKeyListFromChatMessageMoToChatConversationMo(chatMsgMO, chatConvMO);
            values=new ContentValues();
            values.put(ChatConversationDBAdapter.getLASTMSG(),chatConvMO.getLastMsg());
            values.put(ChatConversationDBAdapter.getLASTMSGSTATUS(),chatConvMO.getLastMsgStatus());
            values.put(ChatConversationDBAdapter.getLASTMSGID(),chatConvMO.getLastMsgId());
            values.put(ChatConversationDBAdapter.getCREATEDTIMEUTC(),chatConvMO.getCreatedTimeUTC());
            values.put(ChatConversationDBAdapter.getREADSTATUS(), chatConvMO.getReadStatus());
            values.put(ChatConversationDBAdapter.getCONVERSATIONTYPE(), chatConvMO.getConversationType());
            values.put(ChatConversationDBAdapter.getLASTMSG(), chatConvMO.getLastMsg());
            values.put(ChatConversationDBAdapter.getUNREADCOUNT(), chatConvMO.getUnReadCount());

            try{
                if (chatConvMO.getChatMessageKeyList() != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(chatConvMO.getChatMessageKeyList());
                    values.put(ChatConversationDBAdapter.getCHATMESSAGEKEYLIST(), bytes.toByteArray());
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }



        mService.updateChatConversation(chatConvMO,true,values);
        mService.storeChatMessage(chatMsgMO, chatConvMO, true, true,null);
        Intent intent = new Intent(VariableConstants.NEW_MSG_BROADCAST).putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMsgMO).putExtra(
                VariableConstants.BROADCAST_CHAT_CONV_MO, chatConvMO);
        intent.putExtra(VariableConstants.IS_OFFLINE_MESSAGE, isOfflineMessage);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
