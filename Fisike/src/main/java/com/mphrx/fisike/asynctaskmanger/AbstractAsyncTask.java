package com.mphrx.fisike.asynctaskmanger;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.R;
import com.mphrx.fisike.exception.UnexpectedException;
import com.mphrx.fisike.interfaces.ApiResponseCallback;

public abstract class AbstractAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private Context context;
    private ProgressDialog progressDialog;
    private ApiResponseCallback<Result> callback;

    private int titleResId;
    private boolean isToShowProgress;

    public AbstractAsyncTask(ApiResponseCallback<Result> callback, Context context, boolean isToShowProgress) {
        this(R.string.text_processing_request, callback, context, isToShowProgress);
    }

    public AbstractAsyncTask(int titleResId, ApiResponseCallback<Result> callback, Context context, boolean isToShowProgress) {
        this.context = context;
        this.callback = callback;
        this.titleResId = titleResId;
        this.isToShowProgress = isToShowProgress;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isToShowProgress) {
            progressDialog = ProgressDialog.show(context, null, context.getString(titleResId));
        }
    }

    @Override
    protected Result doInBackground(Params... params) {
        try {
            return getResult(params);
        } catch (Exception exception) {
            return (Result) new UnexpectedException();
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if (isToShowProgress && progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (result==null || result instanceof Exception) {
            callback.onFailedResponse((Exception) result);
        } else {
            callback.onSuccessRespone(result);
        }
    }

    protected abstract Result getResult(Params... params);

}
