package com.mphrx.fisike.asynctaskmanger;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.utils.Utils;

public class HandlePresenceStatus extends AbstractAsyncTask<Packet, Void, Boolean> {

	private Context context;
	private DBAdapter mHelper;

	public HandlePresenceStatus(ApiResponseCallback<Boolean> callback, Context context, boolean isToShowProgress) {
		super(callback, context, isToShowProgress);
		this.context = context;
		mHelper = DBAdapter.getInstance(context);
	}

	@Override
	protected Boolean getResult(Packet... params) {
		Packet packet = params[0];
		if (packet == null) {
			return false;
		}
		Presence presencePacket = (Presence) packet;
		String senderId = presencePacket.getFrom();
		String generateChatContactPK = Utils.generateChatContactPK(senderId);
		if (generateChatContactPK == null) {
			return false;
		}
		String presenceType = "";
		String presenceMessage = presencePacket.getStatus();

		Type type = presencePacket.getType();
		if (type.equals(Presence.Type.available)) {
			presenceType = TextConstants.STATUS_AVAILABLE_TEXT;
		} else if (type.equals(Presence.Type.unavailable)) {
			presenceType = TextConstants.STATUS_UNAVAILABLE_TEXT;
		} else if (type.equals(Presence.Type.busy)) {
			presenceType = TextConstants.STATUS_BUSY_TEXT;
		}

		ChatContactMO fetchChatContactMo;
		try {
			fetchChatContactMo = chatContactDBAdapter.getInstance(context).fetchChatContactMo(generateChatContactPK);
			if (fetchChatContactMo != null)
            {
				fetchChatContactMo.setPresenceType(presenceType);
				fetchChatContactMo.setPresenceStatus(presenceMessage);
                ContentValues values=new ContentValues();
                values.put(chatContactDBAdapter.getPRESENCETYPE(),fetchChatContactMo.getPresenceType());
                values.put(chatContactDBAdapter.getPRESENCESTATUS(), fetchChatContactMo.getPresenceStatus());
               // mHelper.createOrUpdateChatContactMO(fetchChatContactMo);
                chatContactDBAdapter.getInstance(context).updateChatContactMo(fetchChatContactMo.getPersistenceKey(),values);


				Intent intent = new Intent(VariableConstants.BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED);
				intent.putExtra(VariableConstants.PERSISTENCE_KEY, fetchChatContactMo.getPersistenceKey() + "");
				intent.putExtra(VariableConstants.PRESENCE_TYPE, fetchChatContactMo.getPresenceType());
				intent.putExtra(VariableConstants.PRESENCE_MESSAGE, fetchChatContactMo.getPresenceStatus());
				LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

				return true;
			} else {
				fetchChatContactMo = new ChatContactMO();
				fetchChatContactMo.setId(senderId);
				String cleanId = Utils.cleanXMPPServer(senderId);
				fetchChatContactMo.generatePersistenceKey(cleanId);
				fetchChatContactMo.setPresenceType(presenceType);
				fetchChatContactMo.setPresenceStatus(presenceMessage);
				chatContactDBAdapter.getInstance(context).insert(fetchChatContactMo);

			}

			// Intent intent = new Intent(VariableConstants.PRESENCE_STATUS_CHANGE);
			// LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		} catch (Exception e) {
		}
		return false;
	}
}
