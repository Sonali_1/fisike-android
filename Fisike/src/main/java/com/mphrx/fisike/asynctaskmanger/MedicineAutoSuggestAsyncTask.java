package com.mphrx.fisike.asynctaskmanger;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.Constraints;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.MedicationSearchRequest;
import com.mphrx.fisike.gson.response.MedicationSearchResponse;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.utils.SharedPref;


public class MedicineAutoSuggestAsyncTask extends AbstractAsyncTask<Void, Void, MedicationSearchResponse> {
	private static final String MEDICATION_SEARCH_URL_STRING = "medication/search";
	private Context context;
	private String name;
	private int skip, count;
	


	public MedicineAutoSuggestAsyncTask(ApiResponseCallback<MedicationSearchResponse> apiResponseCallback,
			                            Context context, boolean isToShowProgress, String name,
			                            int skip, int count) {
		super(apiResponseCallback, context, isToShowProgress);
		this.context = context;
		this.name = name;
		this.skip = skip;
		this.count = count;
	}

	@Override
	protected MedicationSearchResponse getResult(Void... params) {
	   MedicationSearchResponse response = null;
       APIManager apiManager = APIManager.getInstance();
	   String medicineSearchURL = apiManager.getMinervaPlatformURL(context, MEDICATION_SEARCH_URL_STRING);
       MedicationSearchRequest searchRequestJson = new MedicationSearchRequest();
       Constraints constraint = new Constraints();
       constraint.setName(name);
       constraint.setSkip(skip);
       constraint.setCount(count);
       searchRequestJson.setConstraints(constraint);
       
       String json = new Gson().toJson(searchRequestJson);
       Gson gson = new Gson();
       JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
       JsonObject jsonObj = element.getAsJsonObject();
/*       SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
       String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
		// SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
		// String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
		String authToken = SharedPref.getAccessToken();
       String executeHttpPost = apiManager.executeHttpPost(medicineSearchURL, jsonObj, authToken,context);

       Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(executeHttpPost, MedicationSearchResponse.class);
       if (jsonToObjectMapper instanceof MedicationSearchResponse) {
           return ((MedicationSearchResponse) jsonToObjectMapper);
       }

	   return response;
	}

}
