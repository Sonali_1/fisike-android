package com.mphrx.fisike.asynctaskmanger;

import com.mphrx.fisike.mo.ChatConversationMO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class RoasterEmailObject {

	private ArrayList<String> roasterContacts;
	private HashMap<String, ChatConversationMO> chatConversationMO;

	public RoasterEmailObject(HashMap<String, ChatConversationMO> arrayChatConversation, HashSet<String> contactList) {
		this.chatConversationMO = arrayChatConversation;
		ArrayList<String> arrayContactList = new ArrayList<String>();
		arrayContactList.addAll(contactList);
		roasterContacts = new ArrayList<String>();
		roasterContacts.addAll(arrayContactList);
	}

	public String getRoasterContacts() {
		String commaSepratedIds = "";
		for (int i = 0; roasterContacts != null && i < roasterContacts.size(); i++) {
			commaSepratedIds += roasterContacts.get(i) + ",";
		}
		return commaSepratedIds;
	}

	public void setRoasterContacts(ArrayList<String> roasterContacts) {
		this.roasterContacts = roasterContacts;
	}

	public HashMap<String, ChatConversationMO> getChatConversationMO() {
		return chatConversationMO;
	}

	public void setChatConversationMO(HashMap<String, ChatConversationMO> chatConversationMO) {
		this.chatConversationMO = chatConversationMO;
	}
}
