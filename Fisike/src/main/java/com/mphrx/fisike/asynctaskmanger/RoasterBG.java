package com.mphrx.fisike.asynctaskmanger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.Member;
import com.mphrx.fisike.gson.response.RosterMemberList;
import com.mphrx.fisike.gson.response.SetupChatResponse;
import com.mphrx.fisike.gson.response.UserType;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;

public class RoasterBG extends AbstractAsyncTask<Void, Void, RoasterEmailObject> {

    private Context context;
    private UserMO userMO;
    private final String ROASTER_URL = "/chat/setUpChat";
    private String userJId;
    private DBAdapter mHelper;

    public RoasterBG(ApiResponseCallback<RoasterEmailObject> callback, Context context, boolean isToShowProgress) {
        super(callback, context, false);
        this.context = context;
        userMO = SettingManager.getInstance().getUserMO();
        mHelper = DBAdapter.getInstance(context);
    }

    @Override
    protected RoasterEmailObject getResult(Void... params) {
        JSONObject jsonObject = new JSONObject();
        userJId = userMO.getId() + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();

        try {
            jsonObject.put("jid", userJId);
        } catch (JSONException e) {
        }
        String executeHttpPost = APIManager.getInstance().executeHttpPost(APIManager.createMinervaBaseUrl() + ROASTER_URL, "", jsonObject, context);
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(executeHttpPost, SetupChatResponse.class);
        if (jsonToObjectMapper instanceof SetupChatResponse) {
            SetupChatResponse response = ((SetupChatResponse) jsonToObjectMapper);
            HashSet<String> contactList = new HashSet<String>();
            List<RosterMemberList> arrayContactList = response.getList();
            HashMap<String, ChatConversationMO> arrayChatConversation = new HashMap<String, ChatConversationMO>();
            for (int i = 0; arrayContactList != null && i < arrayContactList.size(); i++) {
                GroupTextChatMO groupTextChatMo = new GroupTextChatMO();
                RosterMemberList roasterMemberList = arrayContactList.get(i);
                ChatConversationMO chatConversationMO = new ChatConversationMO();
                chatConversationMO.setGroupName(roasterMemberList.getSubject());
                chatConversationMO.setJId(roasterMemberList.getId());
                // chatConversationMO.setNickName(nickName);

//                int conversationType = roasterMemberList.getId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) ? VariableConstants.conversationTypeChat
//                        : VariableConstants.conversationTypeGroup;
                int conversationType = roasterMemberList.getSubject().equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER) ? VariableConstants.conversationTypeChat
                        : VariableConstants.conversationTypeGroup;
                chatConversationMO.setConversationType(conversationType);
                int myStatus = VariableConstants.chatConversationMyStatusInactive;

                List<Member> members = roasterMemberList.getMembers();

                ArrayList<String> chatContactMoKeys = new ArrayList<String>();
                for (int j = 0; j < members.size(); j++) {
                    ChatContactMO chatContactMO = new ChatContactMO();
                    Member member = members.get(j);
                    String id = Utils.cleanXMPPServer(member.getJid());
                    chatContactMO.setId(member.getJid());
                    UserType userType = new UserType();
                    userType.setName(member.getUserType());
                    chatContactMO.setUserType(userType);
                    if (member.getJid().equalsIgnoreCase(userJId)) {
                        myStatus = VariableConstants.chatConversationMyStatusActive;
                    } else if (conversationType == VariableConstants.conversationTypeChat) {
                        chatConversationMO.setSecoundryJid(Utils.genrateSecoundryId(roasterMemberList.getId()));
                    }
                    // FIXME CRITICAL: should PKEY id have @ip attached or not
                    chatContactMO.generatePersistenceKey(id);
                    chatContactMoKeys.add(chatContactMO.getPersistenceKey() + "");
                    contactList.add(id);
                    String role = member.getRole();
                    if (conversationType == VariableConstants.conversationTypeGroup && role.equalsIgnoreCase("admin")) {
                        ArrayList<String> admin = new ArrayList<String>();
                        admin.add(chatContactMO.getPersistenceKey() + "");
                        groupTextChatMo.setAdmin(admin);
                    }
                }
                chatConversationMO.setChatContactMoKeys(chatContactMoKeys);
                chatConversationMO.setMyStatus(myStatus);
                chatConversationMO.generatePersistenceKey(userMO.getId() + "");
                chatConversationMO.setGroupStatus(TextConstants.GROUP_CREATED);
                if (conversationType == VariableConstants.conversationTypeGroup) {
                    chatConversationMO.setIsChatInitiated(1);
                }

                arrayChatConversation.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
                try {
                    ChatConversationDBAdapter.getInstance(context).insertChatConversationMo(chatConversationMO);

                } catch (Exception e) {
                }


                if (conversationType == VariableConstants.conversationTypeGroup) {
                    String extractGroupId = Utils.extractGroupId(roasterMemberList.getId());
                    groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
                    groupTextChatMo.setChatConversationMOPK(chatConversationMO.getPersistenceKey() + "");
                    try {
                        GroupChatDBAdapter.getInstance(context).insert(groupTextChatMo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            ArrayList<String> arrayList = new ArrayList<String>();
            arrayList.addAll(contactList);
            RoasterEmailObject roasterEmailObject = new RoasterEmailObject(arrayChatConversation, contactList);
            return roasterEmailObject;
        }
        return null;
    }
}
