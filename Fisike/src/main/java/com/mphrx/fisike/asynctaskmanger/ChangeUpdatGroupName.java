package com.mphrx.fisike.asynctaskmanger;

import java.util.ArrayList;
import java.util.Iterator;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverInfo.Identity;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.platform.SettingManager;

public class ChangeUpdatGroupName extends AbstractAsyncTask<Packet, Void, ArrayList<ChatConversationMO>> {

    private DBAdapter mHelper;
    private UserMO userMO;
    private String subject;
    private String groupId;
    private Context context;

    public ChangeUpdatGroupName(ApiResponseCallback<ArrayList<ChatConversationMO>> callback, Context context, boolean isToShowProgress,
                                String subject, String groupId) {
        super(callback, context, isToShowProgress);
        this.subject = subject;
        this.groupId = groupId;
        this.context = context;
        mHelper = DBAdapter.getInstance(context);
        userMO = SettingManager.getInstance().getUserMO();
    }

    @Override
    protected ArrayList<ChatConversationMO> getResult(Packet... params) {
        ArrayList<ChatConversationMO> chatConversationMOs = new ArrayList<ChatConversationMO>();
        Packet packet = params[0];
        if (packet == null && subject != null && groupId != null) {
            chatConversationMOs.add(changeUpdateGroupName(subject, groupId));
            return chatConversationMOs;
        }
        DiscoverInfo discoverpacket = (DiscoverInfo) packet;
        Iterator<Identity> iterator = discoverpacket.getIdentities();
        while (iterator.hasNext()) {
            Identity member = iterator.next();
            if (member.getName().equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                return null;
            }
            ChatConversationMO changeUpdateGroupName = changeUpdateGroupName(member.getName(), packet.getFrom());
            if (changeUpdateGroupName != null) {
                chatConversationMOs.add(changeUpdateGroupName);
            }
        }
        return chatConversationMOs;
    }

    private ChatConversationMO changeUpdateGroupName(String groupName, String from) {
        ChatConversationMO fetchChatconversationMo = null;
        ChatConversationMO chatConversationMo = new ChatConversationMO();
        if (groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatConversationMo.setConversationType(VariableConstants.conversationTypeChat);
        }else{
            chatConversationMo.setIsChatInitiated(1);
            chatConversationMo.setNickName(groupName);
            chatConversationMo.setConversationType(VariableConstants.conversationTypeGroup);
        }
        chatConversationMo.setGroupName(groupName);
        chatConversationMo.setJId(from);

        chatConversationMo.generatePersistenceKey(userMO.getId() + "");

        String pk = chatConversationMo.getPersistenceKey() + "";
        try {
            fetchChatconversationMo = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(pk);
        } catch (Exception e) {
        }

        if (null == fetchChatconversationMo) {
            /*
			 * chatconversationMo = new ChatConversationMO(); chatconversationMo.setNickName(groupName); chatconversationMo.setGroupName(groupName);
			 * chatconversationMo.setUserName(from); chatconversationMo.generatePersistenceKey(userMO.getUserName());
			 */
            //if null then insert else updat
            ChatConversationDBAdapter.getInstance(context).insertChatConversationMo(chatConversationMo);
            return chatConversationMo;
        }


        // mHelper.updateChatConversationMo((ChatConversationMO) chatconversationMo, contentValues);
        if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            fetchChatconversationMo.setNickName(groupName);
        }
        fetchChatconversationMo.setGroupName(groupName);
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatConversationDBAdapter.getGROUPNAME(), fetchChatconversationMo.getGroupName());
        contentValues.put(ChatConversationDBAdapter.getNICKNAME(), fetchChatconversationMo.getNickName());
        ChatConversationDBAdapter.getInstance(context).updateChatConversation(fetchChatconversationMo.getPersistenceKey(), contentValues);
        //ChatConversationDBAdapter.getInstance(context).createOrUpdateChatConversationMO(chatconversationMo, false);
        return fetchChatconversationMo;
    }
}
