package com.mphrx.fisike.asynctaskmanger;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.asynctaskmanger.GroupMemberListBG.GroupMemberListObject;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.TextPattern;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.DiscoverItems;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class GroupMemberListBG extends AbstractAsyncTask<Packet, Void, GroupMemberListObject> {

    private DBAdapter mHelper;
    Context context;
    private UserMO userMO;
    private boolean isToUpdate = true;

    public GroupMemberListBG(ApiResponseCallback<GroupMemberListObject> callback, Context context,
                             Map<String, ChatConversationMO> conversationsGroup) {
        super(callback, context, false);
        this.context = context;
        mHelper = DBAdapter.getInstance(context);
        userMO = SettingManager.getInstance().getUserMO();
    }

    @Override
    protected GroupMemberListObject getResult(Packet... packet) {
        return processGroupMembersList(((DiscoverItems) packet[0]), packet[0]);
    }

    private GroupMemberListObject processGroupMembersList(DiscoverItems discoverItems, Packet packet) {
        Iterator<org.jivesoftware.smackx.packet.DiscoverItems.Item> items = discoverItems.getItems();
        boolean isUpdateContactMo = true;
        boolean isUpdateGroupTextChatMo = true;
        String fromJid = packet.getFrom();// 1421047816.969098_pat1%40khura.33mail.com@conference.dev1
        ChatConversationMO chatConversationMO = new ChatConversationMO();
        // fromJid = Utils.extractGroupId(fromJid, settingMO.getFisikeServerIp());
        chatConversationMO.setJId(fromJid);
        // chatConversationMO.generatePersistenceKey(userMO.getUserName());
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");

        GroupTextChatMO groupTextChatMo = new GroupTextChatMO();
        String extractGroupId = Utils.extractGroupId(fromJid);
        groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
        try {
            chatConversationMO = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(chatConversationMO.getPersistenceKey() + "");
            groupTextChatMo = GroupChatDBAdapter.getInstance(context).fetchGroupTextChatMO(groupTextChatMo.getPersistenceKey() + "");
        } catch (Exception e) {
        }

        boolean isGroupInvited = false;
        if (null == chatConversationMO) {
            isToUpdate = false;
            chatConversationMO = new ChatConversationMO();
            chatConversationMO.setJId(fromJid);
            chatConversationMO.generatePersistenceKey(userMO.getId() + "");
            isGroupInvited = true;
            chatConversationMO.setNickName("");
            String adminId = TextPattern.getUserId(Utils.getAdminId(packet.getFrom()));
            if (fromJid.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                try {
                    ChatContactMO mo = chatContactDBAdapter.getInstance(context).fetchChatContactMo(ChatContactMO.getPersistenceKey(adminId));
                    if (mo != null) {
                        chatConversationMO.setNickName(mo.getDisplayName());
                    } else {
                        chatConversationMO.setNickName("");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (null == groupTextChatMo) {
            groupTextChatMo = new GroupTextChatMO();
            groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
            isUpdateGroupTextChatMo = false;
        }

        ArrayList<String> chatContactArrayList = new ArrayList<String>();
        ArrayList<String> groupAdminRoleArrayList = new ArrayList<String>();
        int myStatus = VariableConstants.chatConversationMyStatusInactive;

        if (null != chatConversationMO && null != groupTextChatMo) {
            String emailContacts = "";
            // HashMap<String, String> emailGroupMap = new HashMap<String, String>();
            while (items.hasNext()) {
                DiscoverItems.Item item = items.next();
                String entityID = item.getEntityID();
                ChatContactMO chatContactMO = new ChatContactMO();
                String emailString = Utils.cleanXMPPServer(entityID).replace("%40", "@");
                chatContactMO.generatePersistenceKey(emailString);
                chatContactMO = getChatContactMo(chatContactMO.getPersistenceKey() + "");
                if (entityID.startsWith(userMO.getUsername())) {
                    myStatus = VariableConstants.chatConversationMyStatusActive;
                } else if (chatConversationMO.getGroupName() != null && VariableConstants.SINGLE_CHAT_IDENTIFIER.equals(chatConversationMO.getGroupName())) {
                    if (chatConversationMO.getChatContactMoKeys() == null || chatConversationMO.getChatContactMoKeys().size() <= 2) {
                        chatConversationMO.setNickName(chatContactMO.getDisplayName());
                        chatConversationMO.setGroupName(VariableConstants.SINGLE_CHAT_IDENTIFIER);
                    }

                } else if (packet.getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) && sizeOfIterator(discoverItems.getItems()) <= 2) {
                    chatConversationMO.setNickName(chatContactMO.getDisplayName());
                    chatConversationMO.setGroupName(VariableConstants.SINGLE_CHAT_IDENTIFIER);
                }
                if (null == chatContactMO) {
                    chatContactMO = new ChatContactMO();
                    chatContactMO.setEmail("");
                    chatContactMO.generatePersistenceKey(emailString);
                    chatContactMO.setId(entityID);
                    isUpdateContactMo = false;
                }
                if (null == chatContactMO.getRealName() || "".equals(chatContactMO.getRealName())) {
                    emailContacts += Utils.getFisikeUserName(Utils.cleanXMPPServer(entityID), SettingManager.getInstance().getSettings()
                            .getFisikeServerIp())
                            + ",";
                }
                ArrayList<String> joinedGroupArray = chatContactMO.getJoinedGroupMOPK();
                if (null == joinedGroupArray) {
                    joinedGroupArray = new ArrayList<String>();
                }
                String generateJoinedGroupMOPK = chatContactMO.generateJoinedGroupMOPK(Utils.extractGroupId(packet.getFrom()));
                if (!joinedGroupArray.contains(generateJoinedGroupMOPK)) {
                    joinedGroupArray.add(generateJoinedGroupMOPK);
                    // emailGroupMap.put(emailString, generateJoinedGroupMOPK);
                }

                chatContactMO.setJoinedGroupMOPK(joinedGroupArray);
                if (isToUpdate) {

                    ContentValues contentValues = new ContentValues();
                    try {
                        if (chatContactMO.getJoinedGroupMOPK() != null) {
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            ObjectOutputStream out = new ObjectOutputStream(bytes);
                            out.writeObject(chatContactMO.getJoinedGroupMOPK());
                            contentValues.put(chatContactDBAdapter.getJOINEDGROUPMOPK(), bytes.toByteArray());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    updateChatContactMO(chatContactMO, contentValues);


                } else
                    insertChatContactMo(chatContactMO);


                ArrayList<String> chatContactMoKeys = chatConversationMO.getChatContactMoKeys();
                if (null == chatContactMoKeys || !chatContactMoKeys.contains(chatContactMO.getPersistenceKey())) {
                    chatContactArrayList.add(chatContactMO.getPersistenceKey() + "");
                }

                String role = item.getRole();
                if (VariableConstants.ADMIN.equalsIgnoreCase(role)) {
                    groupAdminRoleArrayList.add(chatContactMO.getPersistenceKey() + "");
                    if (fromJid.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) && !(userMO.getId() + "").equals(emailString)) {
                        chatConversationMO.setSecoundryJid(Utils.genrateSecoundryId(fromJid));
                        chatConversationMO.generatePersistenceKey(userMO.getId() + "");
                    }
                }
            }
            if (isGroupInvited && myStatus == VariableConstants.chatConversationMyStatusActive) {
                String tempPk = ("ChatContactMO" + userMO.getId()).hashCode() + "";
                if (!chatContactArrayList.contains(tempPk)) {
                    chatContactArrayList.add(tempPk);
                }
            }
            chatConversationMO.setMyStatus(myStatus);
            chatConversationMO.setChatContactMoKeys(chatContactArrayList);
            chatConversationMO.setGroupStatus(TextConstants.GROUP_CREATED);

            if (chatContactArrayList.size() > 2) {
                chatConversationMO.setConversationType(VariableConstants.conversationTypeGroup);
            } else if (fromJid.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                chatConversationMO.setConversationType(VariableConstants.conversationTypeChat);
            } else {
                chatConversationMO.setConversationType(VariableConstants.conversationTypeGroup);
            }

            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put(ChatConversationDBAdapter.getMYSTATUS(), myStatus);
                if (chatConversationMO.getChatContactMoKeys() != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);

                    out.writeObject(chatConversationMO.getChatContactMoKeys());

                    contentValues.put(ChatConversationDBAdapter.getCHATCONTACTMOKEYS(), bytes.toByteArray());
                } else {

                }
                contentValues.put(ChatConversationDBAdapter.getGROUPSTATUS(), TextConstants.GROUP_CREATED);
                long updated = ChatConversationDBAdapter.getInstance(context).updateChatConversation(chatConversationMO.getPersistenceKey(), contentValues);
                if (updated <= 0) {
                    ChatConversationDBAdapter.getInstance(context).insertChatConversationMo(chatConversationMO);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            groupTextChatMo.setAdmin(groupAdminRoleArrayList);

            try {
                if (isUpdateGroupTextChatMo) {
                    GroupChatDBAdapter.getInstance(context).updateAdminKeyList(groupTextChatMo.getPersistenceKey(), groupAdminRoleArrayList);
                } else {
                    GroupChatDBAdapter.getInstance(context).insert(groupTextChatMo);
                }
                // mHelper.createOrUpdateGroupChatTextMo(groupTextChatMo);
            } catch (Exception e) {
            }

            return new GroupMemberListObject(chatConversationMO, emailContacts);
        }
        return null;

    }

    /**
     * Replace Gauave
     * @param items
     * @return
     */
    private int sizeOfIterator(Iterator<DiscoverItems.Item> items) {
        int count = 0;
        while (items != null && items.hasNext()) {
            items.next();
            count++;
        }
        return count;
    }

    public class GroupMemberListObject {
        private ChatConversationMO chatConversationMO;
        private String emailSting;

        public GroupMemberListObject(ChatConversationMO chatConversationMO, String emailString) {
            this.chatConversationMO = chatConversationMO;
            this.emailSting = emailString;
        }

        public ChatConversationMO getChatConversationMO() {
            return chatConversationMO;
        }

        public String getEmailSting() {
            return emailSting;
        }

    }

    /**
     * This method is called when a class wants to update chat contact
     *
     * @param chatContactMO
     */
    public void updateChatContactMO(ChatContactMO chatContactMO, ContentValues values) {
        try {
/*
            mHelper.createOrUpdateChatContactMO(chatContactMO);
*/

            chatContactDBAdapter.getInstance(context).updateChatContactMo(chatContactMO.getPersistenceKey(), values);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertChatContactMo(ChatContactMO chatContactMO) {
        try {
            chatContactDBAdapter.getInstance(context).insert(chatContactMO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method get the chat contact detail
     *
     * @param persistenceKey
     * @return
     */
    public ChatContactMO getChatContactMo(String persistenceKey) {
        try {
            return chatContactDBAdapter.getInstance(context).fetchChatContactMo(persistenceKey);
        } catch (Exception e) {
            return null;
        }
    }
}