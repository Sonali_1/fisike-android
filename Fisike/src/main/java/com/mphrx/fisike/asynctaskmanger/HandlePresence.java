package com.mphrx.fisike.asynctaskmanger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.asynctaskmanger.HandlePresence.HandlePresenceStanza;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.TextPattern;

public class HandlePresence extends AbstractAsyncTask<Packet, Void, HandlePresenceStanza> {


    private DBAdapter mHelper;
    private Context context;
    private UserMO userMO;
    private SettingMO settingMO;
    private String senderJid;
    private String groupName;

    public HandlePresence(ApiResponseCallback<HandlePresenceStanza> callback, Context context, boolean isToShowProgress, String jid, String userName) {
        super(callback, context, isToShowProgress);
        this.context = context;
        this.senderJid = jid;
        this.groupName = userName;
        mHelper = DBAdapter.getInstance(context);
        userMO = SettingManager.getInstance().getUserMO();
        settingMO = SettingManager.getInstance().getSettings();
    }

    @Override
    protected HandlePresenceStanza getResult(Packet... params) {
        if (params.length <= 0) {
            if (senderJid != null && groupName != null) {
                return handlePresenceGroupMemberAdded(senderJid, groupName);
            }
            return null;
        }
        Packet packet = params[0];
        if (packet == null && senderJid != null && groupName != null) {
            return handlePresenceGroupMemberAdded(senderJid, groupName);
        }
        Presence presencePacket = (Presence) packet;
        if (presencePacket.getType().equals(Presence.Type.unavailable)) {
            // handle exit group
            Intent intent = new Intent(VariableConstants.REMOVE_MEBER_FROM_GROUP_SUCCES);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            String from = presencePacket.getFrom();
            presencePacket.getStatusCode();
            String senderJid = "";
            if (from.contains("/")) {
                senderJid = Utils.getSenderJid(from);
            } else if (null != presencePacket.getItem() && null != presencePacket.getItem().getJid()) {
                senderJid = presencePacket.getItem().getJid();
            }
            String groupId = Utils.cleanXMPPUserName(from);
            return handleExitGroupDB(groupId, senderJid, presencePacket.getStatusCode());
        } else {
            String from = packet.getFrom();
            String senderJid = "";
            if (from.contains("/")) {
                senderJid = Utils.getSenderJid(from);
                String groupId = Utils.cleanXMPPUserName(from);
                return handlePresenceGroupMemberAdded(senderJid, groupId);
            }
        }
        return null;
    }

    protected HandlePresenceStanza handleExitGroupDB(String groupId, String senderJid, int statusCode) {
        ChatConversationMO chatConversationMO = new ChatConversationMO();
        chatConversationMO.setJId(groupId);
        boolean isToUpdateContactMo=true;

        // chatConversationMO.generatePersistenceKey(userMO.getUserName());
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");
        ChatConversationMO fetchchatConversationMo = null;
        try {
            fetchchatConversationMo = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(chatConversationMO.getPersistenceKey() + "");

            if (null != fetchchatConversationMo) {

                String fetchChatConversationPK = fetchchatConversationMo.getPersistenceKey() + "";
                ChatContactMO chatContactMO = new ChatContactMO();
                String id = URLDecoder.decode(Utils.getFisikeUserName(senderJid, settingMO.getFisikeServerIp()), "UTF-8");
                chatContactMO.generatePersistenceKey(id);
                chatContactMO = chatContactDBAdapter.getInstance(context).fetchChatContactMo(chatContactMO.getPersistenceKey() + "");
                if (null == chatContactMO) {
                    chatContactMO = new ChatContactMO();
                    chatContactMO.generatePersistenceKey(id);
                    isToUpdateContactMo=false;
                }
                ArrayList<String> chatContactMoKeys = fetchchatConversationMo.getChatContactMoKeys();
                // if (statusCode == 110) {// || statusCode == 307
                if (TextPattern.getUserId(id).equalsIgnoreCase(URLDecoder.decode(userMO.getId() + "", "UTF-8"))) {
                    fetchchatConversationMo.setMyStatus(VariableConstants.chatConversationMyStatusInactive);
                    ArrayList<String> joinedGroupMOPK = chatContactMO.getJoinedGroupMOPK();

                    if (null != joinedGroupMOPK && joinedGroupMOPK.contains(fetchChatConversationPK)) {
                        joinedGroupMOPK.remove(fetchChatConversationPK);
                        chatContactMO.setJoinedGroupMOPK(joinedGroupMOPK);
                      //  mHelper.createOrUpdateChatContactMO(chatContactMO);
                        chatContactDBAdapter.getInstance(context).updateJOINEDGROUPMOPK(chatContactMO.getPersistenceKey(),chatContactMO.getJoinedGroupMOPK());
                    }

                }

                while (null != chatContactMoKeys && chatContactMoKeys.contains(chatContactMO.getPersistenceKey() + "")) {
                    chatContactMoKeys.remove(chatContactMO.getPersistenceKey() + "");
                    fetchchatConversationMo.setChatContactMoKeys(chatContactMoKeys);
                }
/*
                ContentValues contentValues =new ContentValues();
                    tChatContactMoKeys
                       fetchchatConversationMo.setMyStatus(VariableConstants.chatConversationMyStatusInactive);


*/
                //            ChatConversationDBAdapter.getInstance(context).createOrUpdateChatConversationMO(fetchchatConversationMo, fetchchatConversationMo.getReadStatus());

                return new HandlePresenceStanza(false, true, null, fetchchatConversationMo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * GroupMembersUpdate
     * <p/>
     * TODO
     * fetch and update if problem
     */
    public HandlePresenceStanza handlePresenceGroupMemberAdded(String addedMemberJid, String groupId) {
        //update
        ContentValues contentValues = new ContentValues();
        boolean isToUpdateContactMo=true;
        String fromJid = new String(groupId);// 1421047816.969098_pat1%40khura.33mail.com@conference.dev1
        ChatConversationMO chatConversationMO = new ChatConversationMO();
        // fromJid = Utils.extractGroupId(fromJid, settingMO.getFisikeServerIp());
        chatConversationMO.setJId(fromJid);
        if (groupId.contains(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatConversationMO.setSecoundryJid(Utils.genrateSecoundryId(fromJid));
        }
        // chatConversationMO.generatePersistenceKey(userMO.getUserName());
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");

        GroupTextChatMO groupTextChatMo = new GroupTextChatMO();
        String extractGroupId = Utils.extractGroupId(fromJid);
        groupTextChatMo.generatePersistenceKey(extractGroupId, userMO.getId() + "");
        try {
            chatConversationMO = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(chatConversationMO.getPersistenceKey() + "");
            groupTextChatMo = GroupChatDBAdapter.getInstance(context).fetchGroupTextChatMO(groupTextChatMo.getPersistenceKey() + "");
        } catch (Exception e) {
        }

        if (null != chatConversationMO && null != groupTextChatMo) {
            String emailContacts = "";

            ChatContactMO chatContactMO = new ChatContactMO();
            String emailString = Utils.cleanXMPPServer(addedMemberJid).replace("%40", "@");
            chatContactMO.generatePersistenceKey(emailString);
            try {
                chatContactMO = chatContactDBAdapter.getInstance(context).fetchChatContactMo(chatContactMO.getPersistenceKey() + "");
            } catch (Exception e) {

            }

            if (null == chatContactMO) {
                chatContactMO = new ChatContactMO();
                chatContactMO.setEmail(emailString);
                chatContactMO.generatePersistenceKey(emailString);
                chatContactMO.setId(addedMemberJid);
                isToUpdateContactMo=false;
            }
            if (null == chatContactMO.getRealName() || "".equals(chatContactMO.getRealName())) {
                emailContacts += Utils.getFisikeUserName(Utils.cleanXMPPServer(addedMemberJid), settingMO.getFisikeServerIp()) + ",";
            }
            ArrayList<String> joinedGroupArray = chatContactMO.getJoinedGroupMOPK();
            if (null == joinedGroupArray) {
                joinedGroupArray = new ArrayList<String>();
            }
            String generateJoinedGroupMOPK = chatContactMO.generateJoinedGroupMOPK(Utils.extractGroupId(groupId));
            if (joinedGroupArray.contains(generateJoinedGroupMOPK)) {
                joinedGroupArray.add(generateJoinedGroupMOPK);
            }

            chatContactMO.setJoinedGroupMOPK(joinedGroupArray);

            try {
                if(isToUpdateContactMo)
                    chatContactDBAdapter.getInstance(context).updateJOINEDGROUPMOPK(chatContactMO.getPersistenceKey(),chatContactMO.getJoinedGroupMOPK());

                chatContactDBAdapter.getInstance(context).insert(chatContactMO);
            } catch (Exception e) {
            }

            ArrayList<String> chatContactMoKeys = chatConversationMO.getChatContactMoKeys();
            if (null == chatContactMoKeys) {
                chatContactMoKeys = new ArrayList<String>();
            }
            if (!chatContactMoKeys.contains(String.valueOf(chatContactMO.getPersistenceKey()))) {
                chatContactMoKeys.add(chatContactMO.getPersistenceKey() + "");
            }
            chatConversationMO.setChatContactMoKeys(chatContactMoKeys);
            try {
                if (chatConversationMO.getChatContactMoKeys() != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(chatConversationMO.getChatContactMoKeys());
                    contentValues.put(ChatConversationDBAdapter.getCHATCONTACTMOKEYS(), bytes.toByteArray());
                }
                ChatConversationDBAdapter.getInstance(context).updateChatConversation(chatConversationMO.getPersistenceKey(), contentValues);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new HandlePresenceStanza(true, false, emailContacts, chatConversationMO);
        }
        return null;
    }

    public class HandlePresenceStanza {
        private boolean isAddMember;
        private boolean isRemoveMember;
        private String emailContacts;
        private ChatConversationMO chatConversationMO;

        public HandlePresenceStanza(boolean isAddMember, boolean isRemoveMember, String emailContacts, ChatConversationMO chatchatConversationMO) {
            this.isAddMember = isAddMember;
            this.isRemoveMember = isRemoveMember;
            this.emailContacts = emailContacts;
            chatConversationMO = chatchatConversationMO;
        }

        public boolean isAddMember() {
            return isAddMember;
        }

        public boolean isRemoveMember() {
            return isRemoveMember;
        }

        public String getEmailContacts() {
            return emailContacts;
        }

        public ChatConversationMO getChatConversationMO() {
            return chatConversationMO;
        }

    }
}
