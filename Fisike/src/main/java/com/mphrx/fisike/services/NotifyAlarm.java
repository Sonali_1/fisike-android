package com.mphrx.fisike.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NotifyAlarm extends BroadcastReceiver {
    private Date startDate;
    private Date currentDate;
    private String pract_name;
    private static final int DELAY_TIME = 17;
    private int medicationPrescriptionId, alarmManagerUniqueKey, interval;
    private String alarmDateAndTime, endDate, medicianName, timming;
    private Context context;
    private PrescriptionModel prescriptionModel = null;
    private  MedicineDoseFrequencyModel medicineDoseFrequencyModel = null;
    private Calendar calculateDateTime, cal;
    private long now, timeInMillis, delayLimitTime;

    @Override
    public void onReceive(Context context, Intent intent) {
        // For our recurring task, we'll just display a message
        Bundle extras = intent.getExtras();
        this.context = context;
        medicationPrescriptionId = extras.getInt(VariableConstants.MEDICATION_PRESCRIPTION_ID);
        alarmManagerUniqueKey = extras.getInt(VariableConstants.DOSE_INSTRUCTION_CODE);
        interval = extras.getInt(VariableConstants.INTERVAL);
        alarmDateAndTime = extras.getString(VariableConstants.REPEAT_TIME);
        endDate = extras.getString(VariableConstants.END_DATE);

        resetAlarmForHandlingDozeMode();

        try {
            prescriptionModel = EncounterMedicationDBAdapter.getInstance(context).fetchMedicationPrescription(medicationPrescriptionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (prescriptionModel == null) {
            return;
        }
        medicineDoseFrequencyModel = prescriptionModel.getElement((int) alarmManagerUniqueKey);
        if (medicineDoseFrequencyModel == null) {
            return;
        }

        medicianName = prescriptionModel.getMedicineName();
        int hours = Integer.parseInt(medicineDoseFrequencyModel.getDoseHours());
        if(medicineDoseFrequencyModel.getDoseTimeAMorPM() != null && medicineDoseFrequencyModel.getDoseTimeAMorPM().equalsIgnoreCase("PM")) {
            hours = hours + 12;
        }
        timming = hours + ":" + medicineDoseFrequencyModel.getDoseMinutes() + ": " + "00";
        calculateDateTime = Utils.calculateDateTime(prescriptionModel.getStartDate() + " " + timming);
        // check if the end date has expired
        if (endDateExpired(prescriptionModel.getEndDate())) {
            return;
        }
        // if alarm is disabled return
        if (prescriptionModel.getAutoReminderSQLBool() != 1) {
            return;
        }
        now = System.currentTimeMillis();
        cal = Calendar.getInstance();
        calculateDateTime = getCurrentDateAlarmTime(calculateDateTime);
        timeInMillis = calculateDateTime.getTimeInMillis();
        delayLimitTime = timeInMillis + TimeUnit.MINUTES.toMillis(DELAY_TIME);

        generateAlarmNotification();
    }

    private void generateAlarmNotification(){
        ArrayList<String> weekDays = medicineDoseFrequencyModel.getDoseWeekdaysList();
        if (now == timeInMillis || (now > timeInMillis && delayLimitTime >= now) &&
                (weekDays == null || weekDays.size() == 0 || weekDays.size() > 6 ||
                        hasElement(weekDays, cal.get(Calendar.DAY_OF_WEEK) - 1))) {

            NotificationCenter notificationObject = new NotificationCenter();
            notificationObject.setEncounterId(prescriptionModel.getEncounterID() + "");
            notificationObject.setNotificationType(VariableConstants.ALARM_NOTIFICATION_MEDICATION);
            notificationObject.setMedicianName(medicianName);
            notificationObject.setMedicationTime(timming);
            notificationObject.setDocumentId(prescriptionModel.getID() + "");
            Calendar medicationNotificationCalender = Calendar.getInstance();
            medicationNotificationCalender.setTimeInMillis(calculateDateTime.getTimeInMillis());
            if (calculateDateTime.getTimeInMillis() > now) {
                medicationNotificationCalender.add(Calendar.DAY_OF_MONTH, (int) (Utils.diff(calculateDateTime.getTimeInMillis(), now)));
            }
            notificationObject.setMedicationDate(DateTimeUtil.getCurrentDateEnglish(new Date()));
            notificationObject.setMedicianQuantity(medicineDoseFrequencyModel.getDoseQuantity() + "");
            notificationObject.setRead(false);

            SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            Editor edit = preferences.edit();
            edit.putBoolean(VariableConstants.ALARM_COUNT_MEDICATION, true);
            edit.commit();
            new DatabaseAsyncTask(notificationObject, DatabaseTables.NOTIFICATION_CENTER_TABLE)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            pract_name=(prescriptionModel.getPractitionerName()!=null?(prescriptionModel.getPractitionerName()):"");
            String docName=pract_name.trim().toLowerCase();

            if(!docName.equals("")&&(!docName.startsWith("doc ")&& !docName.startsWith("dr ") && !docName.startsWith("dr.") && !docName.startsWith("doctor ") && !docName.startsWith("doctor")))
                pract_name=context.getResources().getString(R.string.Dr_)+pract_name.trim();

            if(!docName.equals(""))
                pract_name=context.getResources().getString(R.string.Reminder_from_)+" "+pract_name+" : ";

            Utils.generateNotification(context, pract_name + context.getResources().getString(R.string.Its_time_to_have_) + medicianName, prescriptionModel, medicineDoseFrequencyModel);
        }
    }

    private boolean hasElement(ArrayList<String> weekDays, int dayOfWeek) {
        String[] days = context.getResources().getStringArray(R.array.day_array);
        for (int i = 0; i < weekDays.size(); i++) {
            String day = weekDays.get(i);
            if (day.equalsIgnoreCase(days[dayOfWeek])) {
                return true;
            }
        }
        return false;
    }

    private boolean endDateExpired(String medicationPrescriptionEndDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated);
        Date endDate = null;

        try {

            endDate = sdf.parse(medicationPrescriptionEndDate +" "+ context.getResources().getString(R.string._20_30_00));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        currentDate = new Date();
        // compare the date
        if (endDate != null && endDate.compareTo(currentDate) >= 0) {
            return false;
        }
        return true;
    }

    private void resetAlarmForHandlingDozeMode(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && interval!=0 && !endDateExpired(endDate)) {
            Calendar calendar = getDozeModeAlarmTime(alarmDateAndTime, interval);
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New, Locale.getDefault());


            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent myIntent = new Intent(context, NotifyAlarm.class);
            myIntent.putExtra(VariableConstants.MEDICATION_PRESCRIPTION_ID, medicationPrescriptionId);
            myIntent.putExtra(VariableConstants.DOSE_INSTRUCTION_CODE, alarmManagerUniqueKey);
            myIntent.putExtra(VariableConstants.INTERVAL, interval);
            myIntent.putExtra(VariableConstants.REPEAT_TIME, sdf.format(calendar.getTime()));
            myIntent.putExtra(VariableConstants.END_DATE, endDate);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmManagerUniqueKey, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        }

        private Calendar getDozeModeAlarmTime(String alarmTime, int interval){
            Calendar calendar = Calendar.getInstance();

                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New,
                                                            Locale.getDefault());
                try {
                    calendar.setTime(sdf.parse(alarmTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(interval == 1 ){
                    calendar = getCurrentDateAlarmTime(calendar);
                }
                //If current time is greater than the expected alarm time ,add interval
                if(System.currentTimeMillis() > calendar.getTimeInMillis() )
                   calendar.add(Calendar.DATE, interval);
            return calendar;
        }

        private Calendar getCurrentDateAlarmTime(Calendar alarmStartDateTime){
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            alarmStartDateTime.set(year, month, day);
            return  alarmStartDateTime;
        }
}
