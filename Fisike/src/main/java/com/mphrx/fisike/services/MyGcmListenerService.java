package com.mphrx.fisike.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appointment.OrderStatusActivity;
import appointment.phlooba.HomeDrawStatusActivity;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private boolean medicationSyncRequired = false;
    private boolean recordsSyncRequired = false;


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        medicationSyncRequired = false;
        recordsSyncRequired = false;
        try {
            if (message != null && !message.equals("")) {
                JSONObject jsonNotification = new JSONObject(message);
                String type = jsonNotification.getString("type");
                if ((type != null && (type.equalsIgnoreCase(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION)
                        || type.equalsIgnoreCase(VariableConstants.REPORT_DIGITIZATION_NOTIFICATION)
                        || type.equalsIgnoreCase(VariableConstants.BULK_DOC_LINKAGE_NOTIFICATION)))) {
                    JSONObject info = jsonNotification.getJSONObject("info");
                    if (info != null) {
                        String medicationPrescriptionId = null;
                        if (type.equalsIgnoreCase(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION)) {
                            String jsonMedication = info.optString("ENCOUNTER_ID");
//                            medicationPrescriptionId = Integer.parseInt(new JSONArray(jsonMedication).getString(0));
                            try {
                                if (jsonMedication != null) {
                                    medicationPrescriptionId = new JSONArray(jsonMedication).optString(0);
                                }
                            } catch (Exception e) {
                            }
                            medicationSyncRequired = true;
                            // mark shared pref for medication as sync required
                            if (info.toString().contains("DIAGNOSTIC_ORDER_ID")) {
                                // mark shared pref for records as sync required
                                recordsSyncRequired = true;
                            }
                            updateSyncStatusForRecordOrMedication(recordsSyncRequired, medicationSyncRequired);
                        } else if (type.equalsIgnoreCase(VariableConstants.BULK_DOC_LINKAGE_NOTIFICATION)) {
                            String attachmentId = info.optString("DOCUMENT_REFERENCE_ID");
                            String refId = info.optString("REFERENCE_CODE");
                            String alertMsg = info.optString("alertMsg");
                            String title = info.optString("TITLE");
                            sendNotification(medicationPrescriptionId, type, refId, alertMsg, true, title);
                            SharedPref.setIsToRefreshUploads(true);
                            Intent alarmIntent = new Intent(VariableConstants.BROADCAST_UPDATED_RECORD);
                            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(alarmIntent);
                            return;


                        } else {
                            String jsonMedication = info.getString("DIAGNOSTIC_ORDER_ID");
//                            medicationPrescriptionId = Integer.parseInt(new JSONArray(jsonMedication).getString(0));
                            try {
                                medicationPrescriptionId = new JSONArray(jsonMedication).getString(0);
                            } catch (Exception e) {

                            }
                            recordsSyncRequired = true;
                            if (info.toString().contains(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION)) {
                                medicationSyncRequired = true;
                            }
                            updateSyncStatusForRecordOrMedication(recordsSyncRequired, medicationSyncRequired);
                        }

                        String attachmentId = info.optString("DOCUMENT_REFERENCE_ID");
                        String refId = info.optString("REFERENCE_CODE");
                        String alertMsg = info.optString("alertMsg");
                        String title = info.optString("TITLE");
                        sendNotification(medicationPrescriptionId, type, refId, alertMsg, true, title);


                        ContentValues values = new ContentValues();
                        values.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_DIGITIZED);
                        SharedPref.setIsToRefreshUploads(true);
                        Intent alarmIntent = new Intent(VariableConstants.BROADCAST_UPDATED_RECORD);
                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(alarmIntent);

                        try {
                            DocumentReferenceDBAdapter.getInstance(getApplicationContext()).updateDocumentReferenceDigitized(Integer.parseInt(attachmentId), values);
                        } catch (Exception e) {

                        }
                    }
                } else if (type != null && type.equalsIgnoreCase(VariableConstants.DOCUMENT_REPORT_CANCELLED)) {
                    JSONObject info = jsonNotification.getJSONObject("info");
                    String reason = info.getString("REASON");
                    String attachmentId = info.getString("DOCUMENT_REFERENCE_ID");
                    String refId = info.getString("REFERENCE_CODE");
                    String alertMsg = info.getString("alertMsg");
                    String title = info.optString("TITLE");
                    SharedPref.setIsToRefreshUploads(true);
                    rejectedDocument(type, attachmentId, reason, refId, alertMsg, title);
                } else if (type != null && type.equalsIgnoreCase(VariableConstants.DIAGNOSTIC_ORDER)) {
                    JSONObject info = jsonNotification.getJSONObject("info");
                    String reportName = info.getString("reportName");
                    String patientName = info.getString("patientName");
                    String organizationName = info.getString("organizationName");
                    int diagnosticOrderId = info.getInt("diagnosticOrderId");
                    JSONArray array = info.optJSONArray("diagnosticReportIds");
                    int diagnosticReportOrderId = -1;
                    if (array != null && array.length() > 0)
                        diagnosticReportOrderId = array.getInt(0);
                    String organisationId = info.getString("organizationId");
                    String alertMsg = info.getString("alertMsg");
                    String title = info.optString("TITLE");
                    sendNotification(type, reportName, patientName, organizationName, diagnosticReportOrderId,
                            organisationId, alertMsg, title);
                } else if (type != null && type.equalsIgnoreCase("APPOINTMENT_STATUS_CHANGED")) {
                    JSONObject info = jsonNotification.getJSONObject("info");

                    System.out.println("APPOINTMENT_STATUS_CHANGED");

//                    String[] arr = info.split("##");
//                    String appointmentId = arr[0].split("[|]")[1];
//                    String appointmentType = arr[1].split("[|]")[1];
//                    String appointmentActor = arr[2].split("[|]")[1];
//                    String appointmentStatus = arr[3].split("[|]")[1];

                    String appointmentId = info.optString("APPOINTMENT_ID");
                    String appointmentType = info.optString("TYPE_OF_APPOINTMENT");
                    String appointmentActor = info.optString("NAME_OF_ACTOR");
                    String appointmentStatus = info.optString("ACTION_TYPE");


                    String appointmentMsg = "";

                    if (info.has("NOTIFICATION_MSG") && !info.optString("NOTIFICATION_MSG").equals("")) {
                        appointmentMsg = info.optString("NOTIFICATION_MSG");
                    } else if (info.has("alertMsg") && !info.optString("alertMsg").equals("")) {
                        appointmentMsg = info.optString("alertMsg");
                    }


                    sendNotification(type, appointmentId, appointmentMsg);
                    return;
                } else if (type != null && type.equalsIgnoreCase("ACTIVITI")) {

                    JSONObject info = jsonNotification.getJSONObject("info");
                    String taskId = info.optString("taskId");

                    String alertMsg = info.getString("alertMsg");

                    generateNotification(this, alertMsg, taskId, type);

                    return;
                }
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    private void sendNotification(String documentType, String id, String msg) {


        generateNotification(this, msg, id, documentType);

        NotificationCenter notificationObject = new NotificationCenter();

        notificationObject.setDocumentId(id);
        notificationObject.setDocumentType(documentType);
        notificationObject.setDocumentStatus(msg);
        notificationObject.setNotificationType(VariableConstants.ALARM_NOTIFICATION_APPOINTMENT);
        notificationObject.setRead(false);
        notificationObject.setRefId("0");

        new DatabaseAsyncTask(notificationObject, DatabaseTables.NOTIFICATION_CENTER_TABLE)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    public void generateNotification(Context context, String notificationText, String appointmentId, String type) {
        try {
            NotificationCompat.Builder mBuilder;
            mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(notificationText)
                    .setWhen(System.currentTimeMillis())
                    .setTicker(notificationText)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText));
            mBuilder.setAutoCancel(true);
            mBuilder.setDefaults(Notification.DEFAULT_ALL);

            Intent resultIntent;

            if (type.equalsIgnoreCase("APPOINTMENT_STATUS_CHANGED")) {
                resultIntent = new Intent(context, OrderStatusActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong("appointmentID", Long.parseLong(appointmentId));
                resultIntent.putExtra("bundle", bundle);

            } else {
                resultIntent = new Intent(context, HomeActivity.class);
                resultIntent.putExtra("careTaskActiviti", type);
            }


            int id = (int) System.currentTimeMillis();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, id, resultIntent, 0);

            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(id, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    private void updateSyncStatusForRecordOrMedication(boolean isRecordsSyncRequired,
                                                       boolean isMedicationSyncRequired) {
        SharedPreferences sharedPreferences = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (isRecordsSyncRequired) {
            editor.putBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION, false);
        }

        if (isMedicationSyncRequired) {
            editor.putBoolean(SharedPreferencesConstant.SYNC_COMPLETE_AFTER_DIGITIZATION_MEDICATION, false);
        }
        editor.commit();
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param medicationPrescriptionId GCM message received.
     * @param refId
     */


    /**
     * save rejected document status
     *
     * @param type
     * @param attachmentId
     * @param reason
     */
    private void rejectedDocument(String type, String attachmentId, String reason, String refId,
                                  String alertMsg, String title) {
        try {

            sendNotification(attachmentId, type, refId, alertMsg, true, title);

            ContentValues values = new ContentValues();
            values.put(DocumentReferenceDBAdapter.getDOCUMENTUPLOADEDSTATUS(), VariableConstants.DOCUMENT_STATUS_REJECTED);
            values.put(DocumentReferenceDBAdapter.getREASON(), reason);

            Intent alarmIntent = new Intent(VariableConstants.BROADCAST_UPDATED_RECORD);
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(alarmIntent);

            try {
                DocumentReferenceDBAdapter.getInstance(getApplicationContext()).updateDocumentReferenceDigitized(Integer.parseInt(attachmentId), values);
            } catch (Exception e) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(int appointmentId, String type, String appointmentActor, String appointmentStatus) {
        String record = "Your Appointment for " + appointmentActor + " has been " + appointmentStatus;
        generateNotification(this, record, "" + appointmentId, type);

    }


    private void sendNotification(String medicationPrescriptionId, String documentType, String refId, String alertMsg,
                                  boolean isAlertFromServer, String title) {
        NotificationCenter notificationObject = new NotificationCenter();
        int documtentStauts;
        if (documentType.equals(VariableConstants.DOCUMENT_REPORT_CANCELLED)) {
            documtentStauts = VariableConstants.DOCUMENT_STATUS_REJECTED;
            String record;
            record = alertMsg;
            Utils.generateDigitizationNotification(this, record, medicationPrescriptionId, documentType, refId, title);
            notificationObject.setDocumentId(medicationPrescriptionId + "");
        } else if (documentType.equals(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION)) {
            String record;

            record = alertMsg;
            Utils.generateDigitizationNotification(this, record, medicationPrescriptionId, documentType, refId, title);
            notificationObject.setEncounterId(medicationPrescriptionId + "");
            documtentStauts = VariableConstants.DOCUMENT_STATUS_DIGITIZED;
        } else {
            String record;
            record = alertMsg;
            Utils.generateDigitizationNotification(this, record, medicationPrescriptionId, documentType, refId, title);
            notificationObject.setDocumentId(medicationPrescriptionId + "");
            if (!documentType.equalsIgnoreCase(VariableConstants.BULK_DOC_LINKAGE_NOTIFICATION))
                documtentStauts = VariableConstants.DOCUMENT_STATUS_DIGITIZED;
            else
                documtentStauts = VariableConstants.DOCUMENT_STATUS_DOWNLOADED;
        }
        notificationObject.setDocumentType(documentType);
        notificationObject.setDocumentStatus(documtentStauts + "");
        notificationObject.setNotificationType(VariableConstants.ALARM_NOTIFICATION_DOCUMENT_RECORD);
        notificationObject.setRead(false);
        notificationObject.setRefId(refId);
        new DatabaseAsyncTask(notificationObject, DatabaseTables.NOTIFICATION_CENTER_TABLE)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // hl7 notification
    private void sendNotification(String documentType, String reportName, String patientName, String organizationName, int diagnosticOrderId,
                                  String organisationId, String alertMsg, String title) {
        NotificationCenter notificationObject = new NotificationCenter();
        int documtentStauts = 0;
        if (documentType.equals(VariableConstants.DIAGNOSTIC_ORDER)) {
            Utils.generateLabReportNotification(this, documentType, reportName, patientName, organizationName,
                    diagnosticOrderId, organisationId, alertMsg, title);
            notificationObject.setDocumentId(diagnosticOrderId + "");
            documtentStauts = VariableConstants.DOCUMENT_STATUS_DIGITIZED;
        }
        notificationObject.setDocumentType(documentType);
        notificationObject.setDocumentStatus(documtentStauts + "");
        notificationObject.setNotificationType(VariableConstants.ALARM_NOTIFICATION_DOCUMENT_RECORD);
        notificationObject.setRead(false);
        notificationObject.setRefId("0");
        notificationObject.setOrganizationId(organisationId);
        notificationObject.setOrganizationName(organizationName);
        new DatabaseAsyncTask(notificationObject, DatabaseTables.NOTIFICATION_CENTER_TABLE)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


}
