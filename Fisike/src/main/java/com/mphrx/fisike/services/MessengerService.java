package com.mphrx.fisike.services;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.Queue.AttachmentQueue;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.MessageItem;
import com.mphrx.fisike.adapter.SectionItem;
import com.mphrx.fisike.asmack.wrappers.CreateGroupPacket;
import com.mphrx.fisike.asmack.wrappers.ErrorPacketTypeFilter;
import com.mphrx.fisike.asmack.wrappers.GroupDeletionPacket;
import com.mphrx.fisike.asmack.wrappers.GroupImageChangePacket;
import com.mphrx.fisike.asmack.wrappers.GroupNamePacket;
import com.mphrx.fisike.asmack.wrappers.GroupNameTypeFilter;
import com.mphrx.fisike.asmack.wrappers.GroupRosterPacket;
import com.mphrx.fisike.asmack.wrappers.GroupRosterPacketFilter;
import com.mphrx.fisike.asmack.wrappers.InvitationTypeFilter;
import com.mphrx.fisike.asmack.wrappers.NewGroupIQ;
import com.mphrx.fisike.asmack.wrappers.NewGroupSuccessTypeFilter;
import com.mphrx.fisike.asmack.wrappers.RemoveGroupMemberTypeFilter;
import com.mphrx.fisike.asmack.wrappers.SpoolPacketIQ;
import com.mphrx.fisike.asmack.wrappers.SpoolTypeFilter;
import com.mphrx.fisike.asynctask.DatabaseAsyncTask;
import com.mphrx.fisike.asynctask.SendInviteAsyncTak;
import com.mphrx.fisike.asynctask.SendPacketAsyncTask;
import com.mphrx.fisike.asynctask.UpdateDatabaseAsyncTask;
import com.mphrx.fisike.asynctask.UploadGroupPicAsyncTask;
import com.mphrx.fisike.asynctaskmanger.ChangeUpdatGroupName;
import com.mphrx.fisike.asynctaskmanger.GroupMemberListBG;
import com.mphrx.fisike.asynctaskmanger.GroupMemberListBG.GroupMemberListObject;
import com.mphrx.fisike.asynctaskmanger.HandleMessage;
import com.mphrx.fisike.asynctaskmanger.HandlePresence;
import com.mphrx.fisike.asynctaskmanger.HandlePresence.HandlePresenceStanza;
import com.mphrx.fisike.asynctaskmanger.HandlePresenceStatus;
import com.mphrx.fisike.asynctaskmanger.RoasterBG;
import com.mphrx.fisike.asynctaskmanger.RoasterEmailObject;
import com.mphrx.fisike.asynctaskmanger.StoreChatConversation;
import com.mphrx.fisike.asynctaskmanger.StoreChatMessage;
import com.mphrx.fisike.background.CheckUserLoggedInBG;
import com.mphrx.fisike.background.CustomSelectedListTask;
import com.mphrx.fisike.background.LoadContactsProfilePic;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.beans.HashPresence;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.connection.DeRegisterUserDeviceBG;
import com.mphrx.fisike.connection.LoginConnection;
import com.mphrx.fisike.connection.UserDetailsBackground;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.PiwikConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.gson.request.GroupPictureGson;
import com.mphrx.fisike.gson.request.GroupPictureGson.GroupProfilePicture;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.ProfilePictureGson;
import com.mphrx.fisike.gson.request.ProfilePictureGson.ProfilePicture;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.AttachmentMo;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.QuestionActivityMO;
import com.mphrx.fisike.mo.QuestionMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.mo.UserProfile;
import com.mphrx.fisike.persistence.AttchmentDBAdapter;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.ChatMessageDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.ProfilePicTaskManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.TextPattern;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;
import org.jivesoftware.smackx.packet.DiscoverItems;
import org.jivesoftware.smackx.packet.MUCAdmin;
import org.jivesoftware.smackx.receipts.AckReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;


//import com.google.android.gcm.GCMRegistrar;

/**
 * -- PART OF OLYMPUS ARCH CHANGground process that handles the following:
 * <p/>
 * - Maintains XMPP connection - Maintains freshest copy of conversations in memory - Provides interface to retrieve messages - Provides interface to
 * retrieve conversations - Providers interface to send message - Provides interface to login - Implements broadcast for new messages
 *
 * @author varunanand
 */
public class MessengerService extends Service implements ConnectionListener {

    private static final int NUMBER_OF_PACKETS_TO_BE_REMOVED = 50;

    private final IBinder mBinder = new MessengerBinder();
    public static XMPPConnection connection;

    // Load DTO for settings, UserMo, and XMPP connection
    private SettingManager settingManager;
    private SettingMO settingMO;
    private UserMO userMO;
    private ConnectionInfo connectionInfo;

    // For Db query
    private DBAdapter mHelper;
    ChatConversationDBAdapter chatConversationDBAdapter;
    chatContactDBAdapter chatContactDBAdapter;

    // Conversation list
    private ConcurrentHashMap<String, ChatConversationMO> conversations;
    // private HashMap<String, ChatConversationMO> conversations;

    // Packet listeners for receiving the information
    private PacketListener incomingPacketListener;
    private PacketListener memberRemovedFromGroupListener;
    private PacketListener errorListener;
    private PacketListener groupRosterListner;
    private PacketListener groupCreatedPacketListener;
    private PacketListener presenceListener;
    private PacketListener groupNamePacketListener;
    private PacketListener groupInviteListener;
    private PacketListener spoolPacketListener;
    private PacketListener groupDeletePacketListener;

    // Handlers for receiving the data
    private Handler networkTimerHandler;
    private Handler pinScreenHandler;
    private Handler logoutUserHandler;
    private Handler profileUpdateHandler;
    private Handler foregroundPinScreenHandler;

    // Handler interval long
    private long networkTimerInterval;
    private long profileUpdateInterval;
    private long updateUserProfileInterval;
    private long pinscreenInterval;

    // TODO remove the stack after implementing queue
    private Stack<String> deliveryStatusStack = new Stack<String>();
    private Stack<String> ackStatusStack = new Stack<String>();

    // Temp variables
    private List<ChatContactMO> inviteList;
    boolean loggedIn = false;
    private String senderId;
    private Bitmap groupProfilePic;
    private String groupName;
    private String groupID;

    private boolean isToLogout;

    // Shared preference
    private SharedPreferences store;
    private Editor edit;

    private ProgressDialog dialog;

    private static int toggle = 0;
    private String statusPacketId;
    private String statusText;
    private String statusMessage;
    private int statusCode;

    private Context activity;

    private boolean stopProgressBarBroadcast = true;

    private boolean isGroupCreatedFromSingleChat = false;

    private ChatConversationMO firstTimeGroupchatConversationMO;

    private ChatMessageMO firstTimeGroupMessage;
    private long logoutInterval;
    private long customSelectApiInterval;
    private Handler customSelectApiHandler;

    // This method overrides the service onCreate
    @Override
    public void onCreate() {
        super.onCreate();
        AppLog.d("Fisike", "com.mphrx.fisike.services.onCreate");

        // Initialize DB Helper
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        chatConversationDBAdapter = ChatConversationDBAdapter.getInstance(MyApplication.getAppContext());
        chatContactDBAdapter = chatContactDBAdapter.getInstance(MyApplication.getAppContext());
        Utils.timersRunningStatus(MyApplication.getAppContext(), false);
        Utils.pendingMessageTimerStatus(MyApplication.getAppContext(), true);

        // Load up settings
        settingManager = SettingManager.getInstance();
        settingMO = settingManager.getSettings();

        // Get the userMO object
        userMO = settingManager.getUserMO();

        if (null != userMO) {
            // Utils.logInUser(MessengerService.this, userMO.getUserName(), userMO.getPassword());
            //
            postSuccessfulLogin();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (!Utils.isTimerRunning(getApplicationContext()) && Utils.isNetworkAvailable(getApplicationContext())) {
            initiateTimers();
        } else if (ConfigManager.getInstance().getConfig().getPinLockTime() != 0) {
            startPinScreenTimer();
        }

        AppLog.d("Fisike", "com.mphrx.fisike.services.onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    /**
     * This method starts the auto logout service
     */
    private void startLogoutService() {
        try {
            ConfigMO configMO = ConfigManager.getInstance().getConfig();
            //checking to show auto logout
            if (configMO.getAutoLockMinute() != 0) {
                try {
                    SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                    Editor edit = preferences.edit();
                    userMO = SettingManager.getInstance().getUserMO();
                    if (SharedPref.getAccessToken() == null) {
                        return;
                    }

                    // Check for time difference with last Authentication time
                    if (Utils.checkIsToLogoutUser()) {
                        try {
                            if (!Utils.isAppbackground(MessengerService.this) && !Utils.checkTopActivity(this, TextConstants.OTP_SCREEN)) {
                                SharedPref.setLastInteractionTime(System.currentTimeMillis());
                                logoutUser(this);
                            }
                        } catch (Exception e) {
                            AppLog.d("Fisike", "com.mphrx.fisike.services.startLogoutService | Timmer exception " + e.getMessage());
                            e.printStackTrace();
                            return;
                        }
                    }
                } catch (Exception ex) {
                    AppLog.d("Fisike", "com.mphrx.fisike.services.startLogoutService | Exception " + ex.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


    private void startPinService(boolean hasToDisplayPinScreenAfterRemainingTime) {
        try {
            userMO = SettingManager.getInstance().getUserMO();

            if (SharedPref.getAccessToken() == null || userMO == null || userMO.getSalutation().equalsIgnoreCase("NO")) {
                return;
            }
            long inActiveTime =((System.currentTimeMillis() - SharedPref.getLastInteractionTime()));
            if(Utils.checkToDisplayPINScreen(userMO))
            {
                if (!Utils.isAppbackground(MessengerService.this) && !Utils.isTopActivityLockScreen(this) && !Utils.checkTopActivity(this, TextConstants.UPDATE_PIN))
                {
                    AppLog.d("Fisike", "Lock Screen Thread 3 thread Id : " + Thread.currentThread().getId() + " Thread Name : "
                            + Thread.currentThread().getName());
                    startActivity(new Intent(this, com.mphrx.fisike.lock_screen.LockScreenActivity.class).putExtra(VariableConstants.IS_TO_SET_PASSWORD, false).addFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            } else if (!Utils.checkIsToLogoutUser() && (SharedPref.getLastInteractionTime() != 0 && inActiveTime < pinscreenInterval)) {
                hasToDisplayPinScreenAfterRemainingTime = true;
                long pinInterval = pinscreenInterval - inActiveTime;
                stopForegroundTimer();
                if (foregroundPinScreenHandler == null) {
                    foregroundPinScreenHandler = new Handler();
                    final boolean finalHasToDisplayPinScreenAfterRemainingTime = hasToDisplayPinScreenAfterRemainingTime;
                    foregroundPinScreenHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            startPinTask(finalHasToDisplayPinScreenAfterRemainingTime);
                        }
                    }, pinInterval);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


    /**
     * This method returns a list object of the latest conversations
     *
     * @return List<ChatConversationMO>
     */
    public ArrayList<ChatConversationMO> getConversationsList() {
        if (null == conversations) {
            loadChatConversationsFromDB();
        }
        if (conversations == null) {
            return new ArrayList<ChatConversationMO>();
        }
        return new ArrayList<ChatConversationMO>(conversations.values());
    }

    /**
     * This method returns a list object of the latest conversations
     *
     * @return List<ChatConversationMO>
     */
    public AttachmentMo getAttachmentMo(String persistenceKey) {
        try {
            return AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).fetchAttachmentMO_ChatMessageMo(persistenceKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * get the one to one contact list
     *
     * @param contactList
     * @param conversationType
     * @return
     */
    public HashMap<String, ChatContactMO> getContactList(ArrayList<ChatConversationMO> contactList, int conversationType) {
        HashMap<String, ChatContactMO> chatContactList = new HashMap<String, ChatContactMO>();

        for (int index = 0; index < contactList.size(); index++) {
            if (contactList.get(index).getConversationType() != conversationType) {// TODO crash
                continue;
            }
            ChatContactMO chatContactMO = new ChatContactMO();
            try {
                // FIXME if is irrelevent .... else is wrong
                if (conversationType == VariableConstants.conversationTypeChat &&
                        contactList.get(index).getSecoundryJid() != null) {
                    String userName = contactList.get(index).getSecoundryJid();
                    userName = Utils.getFisikeUserName(userName, settingMO.getFisikeServerIp());
                    userName = URLDecoder.decode(userName, "UTF-8");
                    if (userName.equals(userMO.getId())) {
                        continue;
                    }
                    chatContactMO.generatePersistenceKey(userName);
                    chatContactMO = chatContactDBAdapter.fetchChatContactMo(chatContactMO.getPersistenceKey() + "");
                } else {
                    String userName = contactList.get(index).getJId();
                    userName = Utils.getFisikeUserName(userName, settingMO.getFisikeServerIp());
                    userName = URLDecoder.decode(userName, "UTF-8");
                    chatContactMO.generatePersistenceKey(userName);
                    chatContactMO = chatContactDBAdapter.fetchChatContactMo(chatContactMO.getPersistenceKey() + "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (chatContactMO == null) {
                continue;
            }
            chatContactList.put(chatContactMO.getPersistenceKey() + "", chatContactMO);
        }

        return chatContactList;
    }

    /**
     * Get the groupContactList
     *
     * @param contactListPKs
     * @return
     */
    public ArrayList<ChatContactMO> getGroupContactList(ArrayList<String> contactListPKs) {
        // HashMap<String, ChatContactMO> chatContactList = new HashMap<String, ChatContactMO>();
        ArrayList<ChatContactMO> chatContactList = new ArrayList<ChatContactMO>();

        for (int index = 0; index < contactListPKs.size(); index++) {
            ChatContactMO chatContactMO = new ChatContactMO();
            String pk = contactListPKs.get(index);
            try {
                chatContactMO = chatContactDBAdapter.fetchChatContactMo(pk);
            } catch (Exception e) {
            }
            if (chatContactMO == null) {
                continue;
            }
            chatContactList.add(chatContactMO);
        }

        return chatContactList;
    }

    /**
     * get the list of groups
     *
     * @param contactList
     * @param conversationType
     * @return
     */
    public HashMap<String, GroupTextChatMO> getGroupList(ArrayList<ChatConversationMO> contactList, int conversationType) {
        HashMap<String, GroupTextChatMO> chatContactList = new HashMap<String, GroupTextChatMO>();

        for (int index = 0; index < contactList.size(); index++) {
            if (contactList.get(index).getConversationType() != conversationType) {
                continue;
            }
            GroupTextChatMO groupTextChatMO = new GroupTextChatMO();
            try {
                String userName = contactList.get(index).getJId();
                userName = Utils.extractGroupId(userName);
                groupTextChatMO.generatePersistenceKey(userName, userMO.getId() + "");
                groupTextChatMO = GroupChatDBAdapter.getInstance(MyApplication.getAppContext()).fetchGroupTextChatMO(groupTextChatMO.getPersistenceKey() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (groupTextChatMO == null) {
                continue;
            }
            chatContactList.put(groupTextChatMO.getPersistenceKey() + "", groupTextChatMO);
        }
        return chatContactList;
    }

    /**
     * This method initiates the timers - Pending Message Sending Timer - Connection Check Timer
     */
    public void initiateTimers() {
        AppLog.d("Fisike", "com.mphrx.fisike.services.initiateTimers");
        Utils.timersRunningStatus(getApplicationContext(), true);
        startNetworkTimer();
        userMO = SettingManager.getInstance().getUserMO();
        if(userMO!=null && userMO.getSalutation()!=null && userMO.getSalutation().equals(TextConstants.YES))
        {
            if(ConfigManager.getInstance().getConfig().getPinLockTime()!=0 && Utils.checkToDisplayPINScreen(userMO))
                startPinScreenTimer();
            startLogoutTimer();
            startGetProfileUpdatesTimer();
            if(!BuildConfig.isPatientApp)
            startCustomSelectApiTimer();
        }
        else{
            Utils.timersRunningStatus(getApplicationContext(), false);
        }
    }

    private void startCustomSelectApiTimer() {
        customSelectApiInterval = TimeUnit.HOURS.toMillis(5);
        store = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        edit = store.edit();
        edit.putLong(TextConstants.CUSTOMSELECT_API_TIME, System.currentTimeMillis());
        edit.commit();
        if (null == customSelectApiHandler) {
            customSelectApiHandler = new Handler();
            customSelectApiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Utils.isNetworkAvailable(getApplicationContext())) {
                         runStartCustomSelectApiTimer();
                    }
                    customSelectApiHandler.postDelayed(this,customSelectApiInterval );
                }
            }, customSelectApiInterval);
        }
    }

    public void startPinScreenTimer() {
        clearPinScreenTimer();
        ConfigMO configMO = ConfigManager.getInstance().getConfig();
        pinscreenInterval = TimeUnit.SECONDS.toMillis(configMO.getPinLockTime() * 60);
        if(configMO.getPinLockTime() == 0)
            return;

        if (userMO == null || (userMO != null && userMO.getSalutation() != null && userMO.getSalutation().equals(TextConstants.NO)) || SharedPref.getAccessToken() == null)
            return;

        //checking mPIN is disabled and password pin lock is not enabled
        if ((userMO.getmPIN() == null && BuildConfig.isPasswordPinLockEnabled == false))
            return;
        else {
            if (pinScreenHandler == null) {
                pinScreenHandler = new Handler();
                pinScreenHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startPinTask(false);
                    }
                }, pinscreenInterval);
            }
        }
    }

    public void clearPinScreenTimer() {
        userMO = SettingManager.getInstance().getUserMO();
        if (userMO != null && !Utils.isValueAvailable(userMO.getmPIN()) && !BuildConfig.isPasswordPinLockEnabled) {
            if (pinScreenHandler != null) {
                pinScreenHandler.removeCallbacksAndMessages(null);
                pinScreenHandler = null;
            }
            return;
        }
    }
    public void clearProfileUpdatesTimer(){
        if (userMO != null && userMO.getSalutation().equals(TextConstants.YES)) {
            if (profileUpdateHandler != null) {
                profileUpdateHandler.removeCallbacksAndMessages(null);
                profileUpdateHandler = null;
            }
            return;
        }
    }


    public void startLogoutTimer()
    {
        ConfigMO configMO=ConfigManager.getInstance().getConfig();
        if (userMO == null || (userMO != null && userMO.getSalutation() != null && userMO.getSalutation().equals(TextConstants.NO)) || SharedPref.getAccessToken() == null)
            return ;
        if (configMO.getAutoLockMinute()==0 || !Utils.isValueAvailable(SharedPref.getAccessToken()))
        {
            if (logoutUserHandler != null) {
                logoutUserHandler.removeCallbacksAndMessages(null);
                logoutUserHandler = null;
            }
            return;
        }
        logoutInterval = TimeUnit.SECONDS.toMillis(configMO.getAutoLockMinute() * 60);
        if (logoutUserHandler == null) {
            logoutUserHandler = new Handler();
            logoutUserHandler.post(new Runnable() {
                @Override
                public void run() {
                    startLogoutTask();
                    if (logoutUserHandler != null) {
                        logoutUserHandler.postDelayed(this, logoutInterval);
                    }
                }
            });
        }
    }

    private void startNetworkTimer() {
        networkTimerInterval = TimeUnit.SECONDS.toMillis(30);
        if (networkTimerHandler == null) {
            networkTimerHandler = new Handler();
            networkTimerHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (networkTimerHandler != null) {
                        if (toggle % 2 == 0) {
                            // delete spool 1 min
                            runDeleteSpool();

                            // checkUserLoggedIn 1 min
                            if (SharedPref.isUserLogout() || SharedPref.getAccessToken() == null) {
                                networkTimerHandler.removeCallbacks(this);
                                stopSelf();
                            } else {
                                runCheckedUserLoggedInTimer();
                            }
                        }
                        // connection 30 sec
                        runConnectionTimer();

                        // pending message timer 30 sec
                        if (Utils.isPendingMessageTimerRunning(MyApplication.getAppContext())) {
                            sendPendingMessages();
                        }
                        toggle++;
                        if (networkTimerHandler != null) {
                            networkTimerHandler.postDelayed(this, networkTimerInterval);
                        }
                    }

                }
            });
        }
    }

    private void deleverPackert() {
        ContentValues chatMessageContent = null;
        if (deliveryStatusStack.isEmpty()) {
            return;
        }
        String packetId = deliveryStatusStack.pop();
        if (null != packetId) {
            ChatMessageMO chatMsgMO = new ChatMessageMO();
            // String user = Utils.cleanXMPPUserName(userMO.getUserName());
            chatMsgMO.setMsgId(packetId);
            // chatMsgMO.setSenderUserId(user);
            chatMsgMO.setSenderUserId(userMO.getId() + "");
            chatMsgMO.generatePersistenceKey();
            try {
                // If either the Chat Message or chat conversation is unavailable in DB (due to synchronization) then push the
                // packetId back into queue and move on
                ChatMessageMO fetchChatMessageMO = ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).fetchChatMessageMO(chatMsgMO.getPersistenceKey() + "");
                if (null == fetchChatMessageMO) {
                    deliveryStatusStack.push(packetId);
                    return;
                }
                ChatConversationMO fetchChatConversationMO = chatConversationDBAdapter.fetchChatConversationMO(fetchChatMessageMO.getConvPersistenceKey() + "");
                if (null == fetchChatConversationMO) {
                    deliveryStatusStack.push(packetId);
                    return;
                }

                // For the message already delivered
                int msgStatus = fetchChatMessageMO.getMsgStatus();
                if (DefaultConnection.MESSAGE_DELIVERED == msgStatus) {
                    return;
                }
                fetchChatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_DELIVERED);
                fetchChatMessageMO.setRecipientReceivedMsg(true);
                fetchChatMessageMO.setServerReceivedMsg(true);

                // Check if the last sent message in the conversation was the message that has been delivered
                if (fetchChatConversationMO.getLastMsgId().equals(packetId)) {
                    // The last message in the conversation has been delivered
                    fetchChatConversationMO.setLastMsgStatus(DefaultConnection.MESSAGE_DELIVERED);
                }

                ///update
                ContentValues values = new ContentValues();
                values.put(ChatConversationDBAdapter.getLASTMSGSTATUS(), fetchChatConversationMO.getLastMsgStatus());
                if (fetchChatMessageMO != null) {
                    chatMessageContent = new ContentValues();
                    chatMessageContent.put(ChatMessageDBAdapter.getMSGSTATUS(), fetchChatMessageMO.getMsgStatus());
                    chatMessageContent.put(ChatMessageDBAdapter.getRECIPIENTRECEIVEDMSG(), fetchChatMessageMO.isRecipientReceivedMsg());
                    chatMessageContent.put(ChatMessageDBAdapter.getSERVERRECEIVEDMSG(), fetchChatMessageMO.isServerReceivedMsg());

                    String msgPk = chatMsgMO.getPersistenceKey() + "";
                    Vector<String> chatMsgKeyList = fetchChatConversationMO.getChatMessageKeyList();
                    if (null == chatMsgKeyList) {
                        chatMsgKeyList = new Vector<String>();
                    }
                    if (!chatMsgKeyList.contains(msgPk)) {
                        chatMsgKeyList.add(msgPk);
                    }
                    fetchChatConversationMO.setChatMessageKeyList(chatMsgKeyList);
                    // Also add link to conversation to the messageMO
                }

                try {
                    if (fetchChatConversationMO.getChatMessageKeyList() != null) {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        ObjectOutputStream out = new ObjectOutputStream(bytes);
                        out.writeObject(fetchChatConversationMO.getChatMessageKeyList());
                        values.put(ChatConversationDBAdapter.getCHATMESSAGEKEYLIST(), bytes.toByteArray());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                updateChatConversation(fetchChatConversationMO, false, values);
                storeChatMessage(fetchChatMessageMO, fetchChatConversationMO, false, false, chatMessageContent);

                Intent intent = new Intent(VariableConstants.BROADCAST_DELIVERY_STATUS)
                        .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, fetchChatConversationMO)
                        .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, fetchChatMessageMO)
                        .putExtra(VariableConstants.ACK_MESSAGE_SERVER_RECEIVED, false);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    protected void ackUpdate() {
        ContentValues values;
        if (ackStatusStack.isEmpty()) {
            return;
        }
        String packetId = ackStatusStack.pop();
        if (null != packetId) {
            ChatMessageMO chatMsgMO = new ChatMessageMO();
            // String user = Utils.cleanXMPPUserName(userMO.getUserName());
            String id = userMO.getId() + "";
            chatMsgMO.setMsgId(packetId);
            chatMsgMO.setSenderUserId(id);
            chatMsgMO.generatePersistenceKey();
            try {
                // If either the Chat Message or chat conversation is unavailable in DB (due to synchronization) then push the
                // packetId back into queue and move on
                ChatMessageMO fetchChatMessageMO = ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).fetchChatMessageMO(chatMsgMO.getPersistenceKey() + "");
                if (null == fetchChatMessageMO) {
                    // ackStatusStack.push(packetId);
                    return;
                } else if (fetchChatMessageMO.getMsgStatus() == DefaultConnection.MESSAGE_DELIVERED) {
                    return;
                }
                ChatConversationMO fetchChatConversationMO = chatConversationDBAdapter.fetchChatConversationMO(fetchChatMessageMO.getConvPersistenceKey() + "");
                if (null == fetchChatConversationMO) {
                    ackStatusStack.push(packetId);
                    return;
                }
                fetchChatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_SENT_TO_SERVER);
                fetchChatMessageMO.setRecipientReceivedMsg(true);
                fetchChatMessageMO.setServerReceivedMsg(true);
                {
                    values = new ContentValues();
                    values.put(ChatMessageDBAdapter.getMSGSTATUS(), fetchChatMessageMO.getMsgStatus());
                    values.put(ChatMessageDBAdapter.getRECIPIENTRECEIVEDMSG(), fetchChatMessageMO.isRecipientReceivedMsg());
                    values.put(ChatMessageDBAdapter.getSERVERRECEIVEDMSG(), fetchChatMessageMO.isServerReceivedMsg());
                }
                ContentValues contentValues = null;
                // Check if the last sent message in the conversation was the message that has been delivered
                if (null == fetchChatConversationMO.getLastMsgId() || fetchChatConversationMO.getLastMsgId().equals(packetId)) {
                    // The last message in the conversation has been delivered
                    fetchChatConversationMO.setLastMsgStatus(DefaultConnection.MESSAGE_SENT_TO_SERVER);
                    contentValues = new ContentValues();
                    contentValues.put(ChatConversationDBAdapter.getLASTMSGSTATUS(), DefaultConnection.MESSAGE_SENT_TO_SERVER);

                    if (null != chatMsgMO && null != fetchChatConversationMO) {
                        String msgPk = chatMsgMO.getPersistenceKey() + "";
                        Vector<String> chatMsgKeyList = fetchChatConversationMO.getChatMessageKeyList();
                        if (null == chatMsgKeyList) {
                            chatMsgKeyList = new Vector<String>();
                        }
                        if (!chatMsgKeyList.contains(msgPk)) {
                            chatMsgKeyList.add(msgPk);
                        }
                        fetchChatConversationMO.setChatMessageKeyList(chatMsgKeyList);

                        // Also add link to conversation to the messageMO
                        chatMsgMO.setConvPersistenceKey(fetchChatConversationMO.getPersistenceKey());
                    }

                    if (fetchChatConversationMO.getChatMessageKeyList() != null) {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        ObjectOutputStream out = new ObjectOutputStream(bytes);
                        out.writeObject(fetchChatConversationMO.getChatMessageKeyList());
                        contentValues.put(ChatConversationDBAdapter.getCHATMESSAGEKEYLIST(), bytes.toByteArray());
                    }

                }

                // Now store the message update
                //update
                updateChatConversation(fetchChatConversationMO, false, contentValues);

                storeChatMessage(fetchChatMessageMO, fetchChatConversationMO, false, false, values);
                // storeChat(fetchChatMessageMO, null, false, false);

                Intent intent = new Intent(VariableConstants.BROADCAST_DELIVERY_STATUS)
                        .putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO, fetchChatConversationMO)
                        .putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, fetchChatMessageMO)
                        .putExtra(VariableConstants.ACK_MESSAGE_SERVER_RECEIVED, true);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void startGetProfileUpdatesTimer() {

        profileUpdateInterval = TimeUnit.HOURS.toMillis(24);
        if (null == profileUpdateHandler) {
            profileUpdateHandler = new Handler();
            profileUpdateHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                        if(!SharedPref.getIsAgreementsUpdated()) {
                            checkUserDetailApi(false);
                            setUserAPITime();
                    }
                }
            }, profileUpdateInterval);
        }
        else {
            if(profileUpdateHandler!=null){
                if(SharedPref.getIsAgreementsUpdated()){
                    profileUpdateHandler.post(new Runnable() {
                        @Override
                        public void run() {
                                 checkUserDetailApi(true);
                                 setUserAPITime();
                        }
                    });
                }
            }
        }
    }

    private void setUserAPITime() {
        store = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        edit = store.edit();
        edit.putLong(TextConstants.USER_API_TIME, System.currentTimeMillis());
        edit.commit();
    }

    protected boolean isStartUserDetailsCall(boolean isPending){
        store = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        long userAPITime = store.getLong(TextConstants.USER_API_TIME, -1);
        if (0 < userAPITime) {
            edit = store.edit();
            edit.putLong(TextConstants.USER_API_TIME, System.currentTimeMillis());
            edit.commit();
        }

        Calendar startDateTime = Calendar.getInstance();
        startDateTime.setTime(new Date(userAPITime));

        Calendar endDateTime = Calendar.getInstance();
        long currentTimeStamp = Calendar.getInstance().getTime().getTime();
        endDateTime.setTime(new Date(currentTimeStamp));

        long milliseconds1 = startDateTime.getTimeInMillis();
        long milliseconds2 = endDateTime.getTimeInMillis();
        long diff = milliseconds2 - milliseconds1;

        Log.d("Fisike", "com.mphrx.fisike.services.run UserProfile  Diff : " + diff + " Profile : " + updateUserProfileInterval);
        if ((diff >= updateUserProfileInterval ) && null != userMO && userMO.getSalutation().equals(TextConstants.YES) && !isPending) {
            Log.d("Fisike", "com.mphrx.fisike.services.run UserProfile Run");

            return true;
        }
        return false;
    }
    public void checkUserDetailApi(boolean isPending) {

        /*//Pending means - userdetails was hit while app was in background , so agreements might have been updated but not shown.
             In this case pending is true
        * userdetails should be hit in that case always when user relaunches the app.
        * userdetails will also be hit if app is in foreground and timer for userdetails is up.In this case pending is false - Aastha(MNNV-8921)*/
            if(isPending || isStartUserDetailsCall(isPending)) {
                new UserDetailsBackground(getApplicationContext(),this,isPending).execute();

                ProfilePicTaskManager profilePicTaskManager = ProfilePicTaskManager.getInstance();
                String emailString = "";
                ArrayList<String> chatConversationKeyList = userMO.getChatConversationKeyList();
                for (int i = 0; chatConversationKeyList != null && i < chatConversationKeyList.size(); i++) {
                    String conversation = chatConversationKeyList.get(i);
                    try {
                        ChatConversationMO chatConversationMO = chatConversationDBAdapter.fetchChatConversationMO(conversation);
                        String fisikeUserName = Utils.cleanXMPPServer(chatConversationMO.getJId());
                        emailString += fisikeUserName + ",";

                        LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(this, fisikeUserName);
                        profilePicTaskManager.addTask(loadContactsProfilePic);
                        profilePicTaskManager.startTask();
                    } catch (Exception e) {
                    }
                }
                loadContactDetails(emailString, null);
            }
    }

    protected void startLogoutTask() {
        ConfigMO configMO=ConfigManager.getInstance().getConfig();
        if (Utils.isValueAvailable(SharedPref.getAccessToken()) && configMO.getAutoLockMinute()!=0)
            {
                startLogoutService();
            }
    }


    protected void startPinTask(boolean hasToDisplayPinScreenAfterRemainingTime) {
        if (null != userMO) {
            String salutation = userMO.getSalutation();
            if (null != salutation && salutation.equals(TextConstants.YES)) {
                startPinService(hasToDisplayPinScreenAfterRemainingTime);
            }
        }
    }

    private void sendCreateGroupPacket(String groupName, String groupID) {
        CreateGroupPacket newGroupPacket = new CreateGroupPacket();
        newGroupPacket.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        newGroupPacket.setId(newGroupPacket.getPacketID());
        newGroupPacket.setSubject(groupName);
        this.groupID = groupID;
        if (userMO == null) {
            userMO = SettingManager.getInstance().getUserMO();
        }
        if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
            newGroupPacket.setUserRole(userMO.getUserType());
        } else {
            newGroupPacket.setUserRole(VariableConstants.PHYSICIAN);
        }
        // newGroupPacket.setTo(groupID + "@" + VariableConstants.groupConferenceName + settingMO.getFisikeServerIp());
        newGroupPacket.setTo(groupID);
        newGroupPacket.toXML();
        new SendPacketAsyncTask(connection, newGroupPacket).execute();
    }

    public void sendDeleteGroupPacket(String groupID) {
        GroupDeletionPacket groupDeletionPacket = new GroupDeletionPacket();
        groupDeletionPacket.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        groupDeletionPacket.setId(groupDeletionPacket.getPacketID());
        this.groupID = groupID;
        groupDeletionPacket.setTo(groupID);
        groupDeletionPacket.toXML();
        new SendPacketAsyncTask(connection, groupDeletionPacket).execute();
    }

    public void createNewGroup(String groupName, List<ChatContactMO> invitelist, Bitmap profilePic, boolean stopBroadcast, String groupID,
                               boolean isGroupCreatedFromSingleChat) {
        this.groupName = groupName;
        this.inviteList = invitelist;
        this.isGroupCreatedFromSingleChat = isGroupCreatedFromSingleChat;
        // this boolean determines whether to stop the loading progress bar,
        // which appears only when we create a group
        this.stopProgressBarBroadcast = stopBroadcast;
        invitelist.size();
        groupProfilePic = profilePic;
        sendCreateGroupPacket(groupName, groupID);
    }

    public void sendInvite(String groupName, boolean stopProgressBarBroadcast) {
        if (connection != null && connection.isConnected() && connection.isAuthenticated() && inviteList != null) {
            int size = 0;
            if (null != conversations) {
                size = conversations.size();
            }
            if (isGroupCreatedFromSingleChat) {
                new SendInviteAsyncTak(inviteList, groupName, getApplicationContext(), true, getApplication(), size, connection,
                        stopProgressBarBroadcast, firstTimeGroupchatConversationMO, firstTimeGroupMessage, this).execute();
            } else {
                new SendInviteAsyncTak(inviteList, groupName, getApplicationContext(), true, getApplication(), size, connection,
                        stopProgressBarBroadcast, isGroupCreatedFromSingleChat).execute();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
        }
        if (groupProfilePic != null && groupID != null && Utils.isNetworkAvailable(getApplicationContext())) {
            new UploadGroupPicAsyncTask(this, groupProfilePic, groupID).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void sendInvite(List<ChatContactMO> inviteList, String groupName) {
        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
            int size = 0;
            if (null != conversations) {
                size = conversations.size();
            }
            new SendInviteAsyncTak(inviteList, groupName, getApplicationContext(), false, getApplication(), size, connection,
                    stopProgressBarBroadcast, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    // call when packet is processed
    public void sendGroupPresence(String groupId) {
        sendGroupPresencePacket(groupId);
    }

    private void sendGroupPresencePacket(String groupId) {
        Presence presence = new Presence(Presence.Type.available);
        // presence.setFrom(userMO.getUserName() + "@" + settingMO.getFisikeServerIp());
        presence.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        presence.setTo(groupId);
        // new SendPacketAsyncTask(connection, presence).execute();
        connection.sendPacket(presence);
    }

    private void runStartCustomSelectApiTimer() {
        userMO = settingManager.getUserMO();
        if (userMO != null) {
            new CustomSelectedListTask(userMO.getUsername(), userMO.getPassword()).execute();
        }
    }

    private void runCheckedUserLoggedInTimer() {
        userMO = settingManager.getUserMO();
        if (userMO != null && TextConstants.YES.equals(userMO.getSalutation()) && !CheckUserLoggedInBG.isUserDetailCall
                && !UserDetailsBackground.isUserDetailCall) {
            String password = userMO.getPassword();
            new CheckUserLoggedInBG(getApplicationContext(), userMO.getUsername(), password).execute();
        }
    }

    /**
     * This will fetch the chatCovertion and update the chat Message and chat conversation
     *
     * @param chatMessageMO
     */
    public void handleMessageDeliveryReceipt(ChatMessageMO chatMessageMO) {
        try {
            chatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_DELIVERED);
            ChatConversationMO fetchChatConversationMO = chatConversationDBAdapter.fetchChatConversationMO(chatMessageMO.getConvPersistenceKey() + "");

            ContentValues messageMoUpdate = new ContentValues();

            chatMessageMO.setMsgStatus(DefaultConnection.MESSAGE_DELIVERED);
            chatMessageMO.setRecipientReceivedMsg(true);
            chatMessageMO.setServerReceivedMsg(true);

            messageMoUpdate.put(ChatMessageDBAdapter.getMSGSTATUS(), chatMessageMO.getMsgStatus());
            messageMoUpdate.put(ChatMessageDBAdapter.getRECIPIENTRECEIVEDMSG(), chatMessageMO.isRecipientReceivedMsg());
            messageMoUpdate.put(ChatMessageDBAdapter.getSERVERRECEIVEDMSG(), chatMessageMO.isServerReceivedMsg());
            // Check if the last sent message in the conversation was the message that has been delivered
            if (fetchChatConversationMO.getLastMsgId().equals(chatMessageMO.getMsgId())) {
                // The last message in the conversation has been delivered
                fetchChatConversationMO.setLastMsgStatus(DefaultConnection.MESSAGE_DELIVERED);
            }
            // Now store the message update

            //update call

            ContentValues values = new ContentValues();
            values.put(ChatConversationDBAdapter.getLASTMSGSTATUS(), fetchChatConversationMO.getLastMsgStatus());
            updateChatConversation(fetchChatConversationMO, false, values);
            storeChatMessage(chatMessageMO, fetchChatConversationMO, false, false, messageMoUpdate);

            Intent intent = new Intent(VariableConstants.BROADCAST_DELIVERY_STATUS).putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO,
                    fetchChatConversationMO).putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMO);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        } catch (Exception e) {

        }
    }

    private void runDeleteSpool() {
        List<String> packetIdList = MyApplication.getInstance().getMessageIdList();

        if (packetIdList != null && packetIdList.size() > 0) {
            // to, from needs to be set
            if (packetIdList.size() > 50) {

                packetIdList = packetIdList.subList(0, NUMBER_OF_PACKETS_TO_BE_REMOVED);
                MyApplication.getInstance().lightenSpoolPacket(NUMBER_OF_PACKETS_TO_BE_REMOVED);
            }
            createSpoolPacket(packetIdList);
        }
    }

    private void runConnectionTimer() {
        AppLog.d("Fisike", "com.mphrx.fisike.services.run Time : " + System.currentTimeMillis());
        // If the userMO is null, then just don't do anything
        if (null == userMO) {
            shutdownService();
            AppLog.d("Fisike", "com.mphrx.fisike.services.connectionTimer User Mo is null");
        } else {
            try {
                AppLog.d("Fisike", "com.mphrx.fisike.services.run : check XMPP connection");
                if (!checkXMPPConnection()) {
                    AppLog.d("Fisike", "com.mphrx.fisike.services.run");
                    restartXMPPConnection();
                }
            } catch (Exception e) {
                // Do nothing - wait for the next turn to come
            }
        }
    }

    /**
     * This method sends the pending messages
     */
    protected void sendPendingMessages() {

        if (connection == null || !connection.isConnected() || !connection.isAuthenticated()) {
            return;
        }

        List<ChatMessageMO> pendingMessages = ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).fetchPendingChatMessages();

        if (pendingMessages == null || pendingMessages.size() <= 0) {
            Utils.pendingMessageTimerStatus(getApplicationContext(), false);
            return;
        }

        if (null != pendingMessages) {
            Iterator<ChatMessageMO> msgIte = pendingMessages.iterator();

            while (msgIte.hasNext()) {
                try {
                    ChatMessageMO chatMessageMO = msgIte.next();
                    // sendMessage(chatMessageMO);
                    ChatConversationMO chatConvMO = chatConversationDBAdapter.fetchChatConversationMO(chatMessageMO.getConvPersistenceKey() + "");
                    AppLog.d("Fisike", "com.mphrx.fisike.services.sendPendingMessages ");

                    String attachmentID = chatMessageMO.getAttachmentID();
                    String attachmentType = chatMessageMO.getAttachmentType();
                    long lasUpdatedTimeUTC = chatMessageMO.getLasUpdatedTimeUTC();

                    /**
                     * Try sending the message again if :
                     *
                     * @author Case attachment id is received : AttachmentId is not empty or
                     * @author Case Plain text : AttachmentId and AttachmentType both are empty
                     */
                    if ((null != attachmentID && !"".equals(attachmentID))
                            || (((null == attachmentID || "".equals(attachmentID))) && (null == attachmentType || "".equals(attachmentType)))) {
                        sendMessage(chatMessageMO.getMessageText(), attachmentID, attachmentType, lasUpdatedTimeUTC, chatConvMO, chatMessageMO,
                                false, true);
                    } else if (null != attachmentType && (TextConstants.ATTACHMENT_ACTVITY).equalsIgnoreCase(attachmentType)) {

                    }

                } catch (Exception e) {
                    // There was an issue sending the message - not to worry as it will be picked up later
                }
            }
        }
    }

    /**
     * This method restarts the XMPP Connection
     */
    public void restartXMPPConnection() {

        if (ConnectionInfo.getInstance().getXmppConnection() == null && LoginConnection.isConnecting)
            return;

        if (null != userMO && TextConstants.YES.equals(userMO.getSalutation())) {
            AppLog.d("Fisike", "com.mphrx.fisike.services.restartXMPPConnection");
            MyApplication.getInstance().clearSpool();
            Utils.logInUser(MessengerService.this, userMO.getUsername(), userMO.getPassword());
        }
    }

    public void shutDownNetworkTimers() {
        Utils.timersRunningStatus(getApplicationContext(), false);
        if (networkTimerHandler != null) {
            networkTimerHandler.removeCallbacksAndMessages(null);
            networkTimerHandler = null;
            toggle = 0;
        }
    }

    /**
     * This method shuts down the timers
     */
    public void shutdownTimers() {
        shutDownNetworkTimers();
        if (null != profileUpdateHandler) {
            profileUpdateHandler.removeCallbacksAndMessages(null);
            profileUpdateHandler = null;
        }

        if (pinScreenHandler != null) {
            pinScreenHandler.removeCallbacksAndMessages(null);
            pinScreenHandler = null;
        }
        if (logoutUserHandler != null) {
            logoutUserHandler.removeCallbacksAndMessages(null);
            logoutUserHandler = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Thread(new Runnable() {
            @Override
            public void run() {
                shutdownService();
            }
        }).start();
    }

    @Override
    public boolean stopService(Intent name) {
        AppLog.d("Fisike", "com.mphrx.fisike.services.stopService");
        shutdownService();
        return super.stopService(name);
    }

    /**
     * This method handles the shutting down of the service
     */
    private void shutdownService() {
        AppLog.d("Fisike", "com.mphrx.fisike.services.shutdownService");
        shutdownTimers();
        disconnectXMPPHooks();
    }

    /**
     * This method is called to clean up the XMPP Connections
     */
    private void disconnectXMPPHooks() {
        if (null != connection) {
            connection.disconnect();
        }
    }

    /**
     * This method loads up the XMPP Connection
     */
    private void loadXMPPConnection() {
        if (loggedIn) {
            connectionInfo = ConnectionInfo.getInstance();
            connection = connectionInfo.getXmppConnection();
            if (BuildConfig.DEBUG)
                Connection.DEBUG_ENABLED = true;
        }
    }

    /**
     * This method initiates the settings of the user in the system
     */
    private void initiateUser() {
        loadUserMo();
        // Loading conversations from the DB
        loadChatConversationsFromDB();
    }

    private void loadUserMo() {
        // Load up settings
        settingManager = SettingManager.getInstance();
        settingMO = settingManager.getSettings();

        // Get the userMO object
        userMO = settingManager.getUserMO();
    }

    /**
     * This method loads all the recent conversations from the DB
     */
    public void loadChatConversationsFromDB() {
        // Get the conversations list from userMO
        if (userMO == null && settingManager != null) {
            userMO = settingManager.getUserMO();
            if (userMO == null) {
                return;
            }
        }

        ArrayList<ChatConversationMO> chatConversationMOs = chatConversationDBAdapter.getAllChatConversationMo();

        HashMap<String, ChatConversationMO> tempconversations = new HashMap<String, ChatConversationMO>();

        String emailString = "";

        ProfilePicTaskManager profilePicTaskManager = ProfilePicTaskManager.getInstance();

        for (int index = 0; chatConversationMOs != null && index < chatConversationMOs.size(); index++) {
            ChatConversationMO chatConvMO = chatConversationMOs.get(index);

            if (chatConvMO == null || chatConvMO.getJId() == null) {
                continue;
            }
            String user = chatConvMO.getJId();
            chatConvMO.setJId(user.toLowerCase());
            String nickName = chatConvMO.getNickName();

            ArrayList<String> chatContactMoKeys = chatConvMO.getChatContactMoKeys();

            if (chatContactMoKeys != null) {
                for (int i = 0; i < chatContactMoKeys.size(); i++) {
                    ChatContactMO mo = getChatContactMo(chatContactMoKeys.get(i));
                    if (mo != null && mo.getId() != null) {
                        LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(this, TextPattern.getUserId(mo.getId()));
                        profilePicTaskManager.addTask(loadContactsProfilePic);
                        profilePicTaskManager.startTask();
                    }

                }
            }
            if (nickName == null || nickName.equals("")) {
                String fisikeUserName = Utils.getFisikeUserName(user, settingMO.getFisikeServerIp());
                // FIXME CRITICAL: should PKEY id have @ip attached or not
                emailString += TextPattern.getUserId(fisikeUserName) + ",";
                LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(this, fisikeUserName);
                profilePicTaskManager.addTask(loadContactsProfilePic);
                profilePicTaskManager.startTask();
            }
            tempconversations.put(chatConvMO.getPersistenceKey() + "", chatConvMO);
        }

        // if (VariableConstants.conversationTypeChat == conversationType) {
        conversations = new ConcurrentHashMap<String, ChatConversationMO>();
        conversations.putAll(tempconversations);
        // updateConversationsFromRemoteRoster();
        notifyConversationsListChange();
        // } else if (VariableConstants.conversationTypeGroup == conversationType) {
        // conversations = new HashMap<String, ChatConversationMO>();
        // conversations.putAll(tempconversations);
        // notifyGroupConversationsListChange();
        // }

        if (!emailString.equals("")) {
            loadContactDetails(emailString, null);
        }
    }

    /**
     * This method get the chat contact detail
     *
     * @param persistenceKey
     * @return
     */
    public ChatContactMO getChatContactMo(String persistenceKey) {
        try {
            return chatContactDBAdapter.fetchChatContactMo(persistenceKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Refresh db state when the real name is updated
     *
     * @param jsonString
     * @param emailPresence
     */
    private void refreshDB(String jsonString, HashMap<String, HashPresence> emailPresence) {
        try {
            boolean isUpdateChatContactMo = false;
            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, UserProfile.class);
            if (jsonToObjectMapper instanceof UserProfile) {
                UserProfile userProfile = (UserProfile) jsonToObjectMapper;

                ArrayList<ChatContactMO> profiles = userProfile.getResultList();

                for (int i = 0; i < profiles.size(); i++) {
                    ChatContactMO gsonChatContactMO = profiles.get(i);
                    String email = gsonChatContactMO.getEmail();
                    String id = gsonChatContactMO.getId();
                    String userNickName = gsonChatContactMO.getRealName();

                    ChatContactMO chatContactMO = chatContactDBAdapter.fetchChatContactMo(ChatContactMO.getPersistenceKey(id) + "");
                    if (chatContactMO != null) {
                        isUpdateChatContactMo = true;
                        if (chatContactMO.getProfilePic() != null) {
                            gsonChatContactMO.setProfilePic(chatContactMO.getProfilePic());
                        }
                        if (chatContactMO.getPresenceType() != null) {
                            gsonChatContactMO.setPresenceType(chatContactMO.getPresenceType());
                        }
                        if (chatContactMO.getPresenceStatus() != null) {
                            gsonChatContactMO.setPresenceStatus(chatContactMO.getPresenceStatus());
                        }
                    }
                    gsonChatContactMO.setId(gsonChatContactMO.getId() + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp());
                    if (emailPresence != null && emailPresence.containsKey(email)) {
                        gsonChatContactMO.setPresenceType(emailPresence.get(email).getPresenceType());
                        gsonChatContactMO.setPresenceStatus(emailPresence.get(email).getPresenceStatus());
                    }
                    gsonChatContactMO.generatePersistenceKey(id + "");
                    gsonChatContactMO.setDob(DateTimeUtil.convertSourceDestinationDate(gsonChatContactMO.getDob(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));

                    if (isUpdateChatContactMo) {
                        chatContactDBAdapter.updateChatContactMoUserProfile(gsonChatContactMO.getPersistenceKey(), gsonChatContactMO, false);
                    } else {
                        chatContactDBAdapter.insert(gsonChatContactMO);
                    }
                    if (conversations == null) {
                        return;
                    }
                    Iterator<Entry<String, ChatConversationMO>> iterator = conversations.entrySet().iterator();
                    while (iterator != null && iterator.hasNext()) {
                        String persistenceKey = iterator.next().getKey();
                        ChatConversationMO chatConvMO = null;
                        try {
                            chatConvMO = chatConversationDBAdapter.fetchChatConversationMO(persistenceKey);
                        } catch (Exception e) {
                        }

                        if (null == chatConvMO) {
                            continue;
                        }

                        String userName = chatConvMO.getJId();
                        // String tempEmail = URLEncoder.encode(email, "UTF-8") + "@" + settingMO.getFisikeServerIp();
                        String tempEmail = id + "@" + settingMO.getFisikeServerIp();
                        //TODO if (VariableConstants.SINGLE_CHAT_IDENTIFIER.equals(chatConvMO.getGroupName())
                        if (userName.startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)
                                && tempEmail.equalsIgnoreCase(chatConvMO.getSecoundryJid())) {
                            chatConvMO.setNickName(userNickName);
                            conversations.put(chatConvMO.getPersistenceKey() + "", chatConvMO);

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(ChatConversationDBAdapter.getNICKNAME(), chatConvMO.getNickName());
                            chatConversationDBAdapter.updateChatConversation(chatConvMO.getPersistenceKey(), contentValues);
                            break;
                        }
                        if (tempEmail.equalsIgnoreCase(userName)) {
                            // FIXME is this still FUCKING needed
                            if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
                                chatConvMO.setNickName(userNickName);
                            }
                            conversations.put(chatConvMO.getPersistenceKey() + "", chatConvMO);

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(ChatConversationDBAdapter.getNICKNAME(), chatConvMO.getNickName());
                            chatConversationDBAdapter.updateChatConversation(chatConvMO.getPersistenceKey(), contentValues);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Refresh db state when the profilePic is updated
     *
     * @param jsonString
     */
    private void refreshDBProfilePic(String jsonString) {
        if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
            return;
        }
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ProfilePictureGson.class);
        if (jsonToObjectMapper instanceof ProfilePictureGson) {
            ProfilePictureGson profilePictureGson = ((ProfilePictureGson) jsonToObjectMapper);
            ArrayList<ProfilePicture> profilePictureArray = profilePictureGson.getProfilePictureArray();
            for (int i = 0; i < profilePictureArray.size(); i++) {
                ProfilePicture profilePicture = profilePictureArray.get(i);
                String id = profilePicture.getId() + "";
                byte[] profilePic = profilePicture.getProfilePic();
                if ((null != id || !"".equals(id)) && (null != profilePic)) {
                    updateChatContactProfilePic(profilePic, id);
                }
            }
        }
    }

    /**
     * Refresh db state when the profilePic is updated
     *
     * @param jsonString
     * @param emailString
     */
    private void refreshGroupDBProfilePic(String jsonString, String emailString) {
        try {
            if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
                return;
            }

            Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, GroupPictureGson.class);

            if (jsonToObjectMapper instanceof GroupPictureGson) {
                GroupPictureGson profilePictureGson = ((GroupPictureGson) jsonToObjectMapper);
                ArrayList<GroupProfilePicture> groupChatDisplayPic = profilePictureGson.getGroupChatDisplayPic();
                for (int i = 0; i < groupChatDisplayPic.size(); i++) {
                    GroupProfilePicture profilePicture = groupChatDisplayPic.get(i);
                    String email = profilePicture.getGroupId();
                    byte[] profilePic = profilePicture.getGroupDisplayPic();
                    if ((null != email || !"".equals(email)) && (null != profilePic)) {
                        updateGroupProfilePic(profilePic, email);
                    }
                }
            }
        } catch (Exception e) {
        }
    }


    /**
     * Update the chat contact details
     *
     * @param chatContactMO
     */
/*
    public void updateChatContactDetails(ChatContactMO chatContactMO) {

        try {
            ChatConversationMO chatConvMO = new ChatConversationMO();
            chatConvMO.setSecoundryJid(Utils.genrateSecoundryId(chatContactMO.getId()));
            chatConvMO.generatePersistenceKey(userMO.getId() + "");
            ChatConversationMO fetchChatConversationMO = chatConversationDBAdapter.fetchChatConversationMO(chatConvMO.getPersistenceKey() + "");
            if (null != fetchChatConversationMO) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(ChatConversationDBAdapter.getNICKNAME(), chatContactMO.getRealName());
                chatConversationDBAdapter.updateChatConversation(chatConvMO.getPersistenceKey(), contentValues);

                fetchChatConversationMO.setNickName(chatContactMO.getRealName());
                conversations.put(fetchChatConversationMO.getPersistenceKey() + "", fetchChatConversationMO);
            }
        } catch (Exception e) {
        }
        Intent intent = new Intent(VariableConstants.BROADCAST_CONTACT_DETAIL_CHANGE).putExtra(VariableConstants.IS_GROUP_CONTACT_UPDATION, false);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }
*/

    /**
     * Update the chat contact profilePic
     *
     * @param profilePic
     * @param id
     */
    public void updateChatContactProfilePic(byte[] profilePic, String id) {
        boolean isUpdateChatContactMo = true;

        ChatContactMO fetchChatContactMO = new ChatContactMO();
        try {
            fetchChatContactMO.generatePersistenceKey(id);
            fetchChatContactMO = chatContactDBAdapter.fetchChatContactMo(fetchChatContactMO.getPersistenceKey() + "");
        } catch (Exception e) {

        }
        if (null == fetchChatContactMO || (null != fetchChatContactMO && !fetchChatContactMO.equalsProfilePic(profilePic))) {
            if (null == fetchChatContactMO) {
                isUpdateChatContactMo = false;
                fetchChatContactMO = new ChatContactMO();
                fetchChatContactMO.generatePersistenceKey(id);
            }
            fetchChatContactMO.setProfilePic(profilePic);

            try {
                if (isUpdateChatContactMo) {
                    chatContactDBAdapter.updateProfilePic(fetchChatContactMO.getPersistenceKey(), fetchChatContactMO.getProfilePic());
                } else {
                    chatContactDBAdapter.insert(fetchChatContactMO);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(VariableConstants.BROADCAST_CONTACT_DETAIL_CHANGE)
                    .putExtra(VariableConstants.IS_GROUP_CONTACT_UPDATION, false);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    /**
     * Update the group profilePic
     *
     * @param profilePic
     * @param email
     */
    public void updateGroupProfilePic(byte[] profilePic, String email) {
        GroupTextChatMO fetchGroupChatMo = new GroupTextChatMO();
        GroupChatDBAdapter groupChatDBAdapter = GroupChatDBAdapter.getInstance(MyApplication.getAppContext());
        boolean isUpdateGroupChatMo = true;

        try {
            fetchGroupChatMo.generatePersistenceKey(email, userMO.getId() + "");
            fetchGroupChatMo = groupChatDBAdapter.fetchGroupTextChatMO(fetchGroupChatMo.getPersistenceKey() + "");
        } catch (Exception e) {

        }
        if (null == fetchGroupChatMo || (null != fetchGroupChatMo && !fetchGroupChatMo.equalsProfilePic(profilePic))) {
            if (null == fetchGroupChatMo) {
                isUpdateGroupChatMo = false;
                fetchGroupChatMo = new GroupTextChatMO();
                fetchGroupChatMo.generatePersistenceKey(email, userMO.getId() + "");
            }
            fetchGroupChatMo.setImageData(profilePic);

            if (isUpdateGroupChatMo) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(groupChatDBAdapter.getIMAGEDATA(), profilePic);
                storeGroupContactMo(fetchGroupChatMo, contentValues);
            } else {
                storeGroupContactMo(fetchGroupChatMo, null);
            }


        }
    }

    // /**
    // * This method is called to update the roster from the XMPP Server
    // */
    // private void updateConversationsFromRemoteRoster() {
    // if (null == connection) {
    // return;
    // }
    //
    // HashMap<String, HashPresence> emailPresence = new HashMap<String, HashPresence>();
    //
    // Roster roster = connection.getRoster();
    // Collection<RosterEntry> entries = roster.getEntries();
    // String emailString = "";
    //
    // ProfilePicTaskManager profilePicTaskManager = ProfilePicTaskManager.getInstance();
    // for (RosterEntry entry : entries) {
    // String user = Utils.cleanXMPPUserName(entry.getUser());
    //
    // ChatConversationMO oldChatConvMO = new ChatConversationMO();
    // oldChatConvMO.setJId(user.toLowerCase());
    // // oldChatConvMO.generatePersistenceKey(userMO.getUserName());
    // if (userMO == null || userMO.getId() == 0) {
    // return;
    // }
    // oldChatConvMO.generatePersistenceKey(userMO.getId() + "");
    //
    // if (conversations == null) {
    // conversations = new HashMap<String, ChatConversationMO>();
    // }
    // if (entry.getPresenceType() == null) {
    // entry.setPresenceType("available");
    // }
    // if (entry.getPresenceStatus() == null) {
    // entry.setPresenceStatus("");
    // }
    // if (!conversations.containsKey(oldChatConvMO.getPersistenceKey() + "")) {
    // // Only if the conversation does not have this chat should we add this entry
    // ChatConversationMO chatConvMO = new ChatConversationMO();
    // chatConvMO.setJId(user);
    // chatConvMO.setCreatedTimeUTC(0);
    // chatConvMO.generatePersistenceKey(userMO.getId() + "");
    // chatConvMO.setPhoneNumber("");
    // chatConvMO.setNickName("");
    // chatConvMO.setUnReadCount(0);
    // chatConvMO.setReadStatus(true);
    // // Since its a new chat conversation, we create a new string for the chat message key
    // chatConvMO.setChatMessageKeyList(new Vector<String>());
    //
    // String fisikeUserName = Utils.getFisikeUserName(user, settingMO.getFisikeServerIp());
    // emailString += fisikeUserName + ",";
    // try {
    // emailPresence.put(URLDecoder.decode(fisikeUserName, "UTF-8"),
    // new HashPresence(entry.getPresenceStatus(), entry.getPresenceType()));
    // } catch (UnsupportedEncodingException e) {
    // }
    //
    // LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(this, fisikeUserName);
    // profilePicTaskManager.addTask(loadContactsProfilePic);
    // profilePicTaskManager.startTask();
    //
    // // updateChatContactMO(chatConvMO);
    //
    // // Call for updating the chatconversationMO
    // updateChatConversationMO(chatConvMO);
    // } else {
    // // Check if the conversation has the nickname populated correctly or not
    // ChatConversationMO chatConvMO = conversations.get(oldChatConvMO.getPersistenceKey());
    //
    // if (null != chatConvMO) {
    // String nickName = chatConvMO.getNickName();
    // if (nickName == null || nickName.equals("")) {
    // String fisikeUserName = Utils.getFisikeUserName(user, settingMO.getFisikeServerIp());
    // emailString += fisikeUserName + ",";
    // LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(this, fisikeUserName);
    // profilePicTaskManager.addTask(loadContactsProfilePic);
    // profilePicTaskManager.startTask();
    // }
    // }
    // }
    // }
    //
    // // If there are any missing nicknames - then load all of them..
    // if (!emailString.equals("")) {
    // loadContactDetails(emailString, emailPresence);
    // }
    //
    // }

    /**
     * This method sends out a notification to the ListPersonsActivity that the conversation List has been updated
     */
    private void notifyConversationsListChange() {
        Intent intent = new Intent(VariableConstants.BROADCAST_CONV_LIST_CHANGED);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        // Ensure that the userMO is updated with the latest conversations
        syncUserMOConversations();
    }

    public void notifyNoConversationBroadCast() {
        Intent intent = new Intent(VariableConstants.BROADCAST_NO_CONV_TO_LOAD);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * This method sends out a notification to the ListPersonsActivity that the conversation List has been updated
     */
    public void notifyConversationsListChange(ChatConversationMO chatConversationMO) {
        Intent intent = new Intent(VariableConstants.BROADCAST_GROUP_CHAT_REMOVE_MEMBER).putExtra(VariableConstants.CONVERSATION_ITEM,
                chatConversationMO);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Ensure that the userMO is updated with the latest conversations
        syncUserMOConversations();
    }

    /**
     * This method sends out a notification to the ListPersonsActivity that the conversation List has been updated
     */
    public void notifyConversationsAdd(ChatConversationMO chatConversationMO) {
        Intent intent = new Intent(VariableConstants.BROADCAST_GROUP_CHAT_ADD_MEMBER).putExtra(VariableConstants.CONVERSATION_ITEM,
                chatConversationMO);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * This method sends out a notification to the ListPersonsActivity that the conversation List has been updated
     */
    public void notifyGroupConversationsListChange() {
        Intent intent = new Intent(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * This method sends out a notification to the ListPersonsActivity that the conversation List has been updated
     */
    private void notifyChatMessage(ChatConversationMO chatConversationMO, ChatMessageMO chatMessageMO) {
        Intent intent = new Intent(VariableConstants.BROADCAST_MSG_CHANGE_LISTNER).putExtra(VariableConstants.BROADCAST_CHAT_CONV_MO,
                chatConversationMO).putExtra(VariableConstants.BROADCAST_CHAT_MSG_MO, chatMessageMO);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * This method will sync the userMO conversations with what is stored in the DB
     */
    private void syncUserMOConversations() {
        boolean userMOChanged = false;
        if (null != conversations) {
            Iterator<ChatConversationMO> convIte = conversations.values().iterator();
            List<String> convKeyList = userMO.getChatConversationKeyList();
            if (convKeyList == null) {
                convKeyList = new Vector<String>();
                userMO.setChatConversationKeyList(new ArrayList<String>());
            }

            while (convIte.hasNext()) {
                String key = convIte.next().getPersistenceKey() + "";

                if (!convKeyList.contains(key)) {
                    userMOChanged = true;
                    userMO.getChatConversationKeyList().add(key);
                }
            }
        }
        if (null == conversations) {
            return;
        }

        Iterator<ChatConversationMO> convGroupIte = conversations.values().iterator();
        List<String> convKeyGroupList = userMO.getChatConversationKeyList();

        boolean userMOChangedGroup = false;
        while (null != convKeyGroupList && convGroupIte.hasNext()) {
            String key = convGroupIte.next().getPersistenceKey() + "";

            if (!convKeyGroupList.contains(key)) {
                userMOChangedGroup = true;
                userMO.getChatConversationKeyList().add(key);
            }
        }

        if (userMOChanged || userMOChangedGroup) {
            try {
                //   settingManager.updateUserMO(userMO);
                UserDBAdapter.getInstance(MyApplication.getAppContext()).updateConversationKeyList(userMO.getPersistenceKey(), userMO.getChatConversationKeyList());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is called on a successful login
     */
    public void postSuccessfulLogin() {
        AppLog.d("MessengerService", "Now setting up XMPP connections in the service");
        loggedIn = true;
        final SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isGroupListFetched = preferences.getBoolean(VariableConstants.IS_GROUP_LIST_FETCHED, false);
        loadXMPPConnection();
        if (connection == null || !connection.isConnected() && !connection.isAuthenticated()) {
            return;
        }
        initiateMessageListeners();
        initiateConnectionListener();
        // if there is value in group or not
        // if (userMO != null && !userMO.getUserType().getName().equalsIgnoreCase(VariableConstants.PATIENT)) {
        if (conversations == null) {
            getConversationsList();
        }
        if (!isGroupListFetched || conversations == null || conversations.size() <= 0) {
            // getGroupList();
            SharedPref.setProgressForChat(true);
            getRoaster();
        } else {
            SharedPref.setProgressForChat(false);
            notifyNoConversationBroadCast();
        }
        // }
        initGroupListListner();
        groupCreatedListener();
        presencePacketListener();
        groupNamePacketListener();
        initgroupInviteListener();
//        initGroupDeletionListener();
        initErrorListener();
        initSpoolTypeListener();
        initRemoveGroupMemberTypeFilter();
        initiateUser();
        initalizeDeliveryReceiptManager();
        initalizeMessageAckReceiptManager();

        Utils.pendingMessageTimerStatus(getApplicationContext(), true);
    }

    private void initiateConnectionListener() {
        connection.addConnectionListener(new ConnectionListener() {

            @Override
            public void reconnectionSuccessful() {
                // Add code to re-initialize the listeners
                // /getGroupList();
                initiateMessageListeners();
                // initGroupListListner();
                groupCreatedListener();
                presencePacketListener();
                groupNamePacketListener();
//                initGroupDeletionListener();
                initgroupInviteListener();
                initRemoveGroupMemberTypeFilter();
                initiateUser();
                initalizeDeliveryReceiptManager();
                initalizeMessageAckReceiptManager();
            }

            @Override
            public void reconnectionFailed(Exception arg0) {

            }

            @Override
            public void reconnectingIn(int arg0) {

            }

            @Override
            public void connectionClosedOnError(Exception error) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.connectionClosedOnError : " + error.getMessage());
                error.printStackTrace();
                if (connection != null && connection.isConnected()) {
                    connection.disconnect();
                }
            }

            @Override
            public void connectionClosed() {
                AppLog.d("Fisike", "com.mphrx.fisike.services.connectionClosed");
                if (connection != null) {
                    connection = null;
                }
            }
        });
    }

    /**
     * This method call registers this device with the GCM
     *
     * @see - Login Activity onCreate
     * @see - MessegerService postSuccessfulLogin
     */
    public void registerWithGCM() {
        // GCMRegistrar.checkDevice(this);
        // GCMRegistrar.checkManifest(this);
        // String regId = GCMRegistrar.getRegistrationId(this);
        // if (regId.equals("")) {
        // GCMRegistrar.register(this, VariableConstants.GCM_SENDER_ID); // Note: get
        // } else {
        // AppLog.d("LOG_MANAGER", "Already registered, regId ");
        // }
    }

    /**
     * This method uploads the device token to the server
     */
    public void uploadDeviceToken() {
        Thread uploadToken = new Thread() {
            public void run() {
                try {
                    APIManager jsonManager = APIManager.getInstance();
                    jsonManager.uploadDeviceToken(MessengerService.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        uploadToken.start();
    }

    /**
     * This method initiates the delivery receipt manager
     */
    private void initalizeDeliveryReceiptManager() {
        try {
            if (null != connection && connection.isConnected() && connection.isAuthenticated())
                DeliveryReceiptManager.getInstanceFor(connection).enableAutoReceipts();
            DeliveryReceiptManager.getInstanceFor(connection).addReceiptReceivedListener(new ReceiptReceivedListener() {
                @Override
                public void onReceiptReceived(String senderUserId, String arg1, String packetId) {
                    // Change - basically just push into the delivery status stack so that another thread takes care of this
                    deliveryStatusStack.push(packetId);
                    deleverPackert();
                    CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();
                    list.add(packetId);
                    MyApplication.getInstance().setMessageIdList(list);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method initiates the delivery receipt manager
     */
    private void initalizeMessageAckReceiptManager() {
        try {
            if (null != connection && connection.isConnected() && connection.isAuthenticated())
                AckReceiptManager.getInstanceFor(connection).enableAutoReceipts();
            AckReceiptManager.getInstanceFor(connection).addReceiptReceivedListener(new ReceiptReceivedListener() {
                @Override
                public void onReceiptReceived(String senderUserId, String arg1, String packetId) {
                    // Change basically just push into the delivery status stack so that another thread takes care of this
                    if (statusPacketId != null && statusPacketId.equals(packetId)) {
//                        if (logoutUserHandler != null) {
//                            logoutUserHandler.removeCallbacksAndMessages(null);
//                            logoutUserHandler = null;
//                        }
                        changeStatusResponce(true);
                    } else {
                        ackStatusStack.push(packetId);
                        ackUpdate();
                        // storing in spool
                        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();
                        list.add(packetId);
                        MyApplication.getInstance().setMessageIdList(list);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void changeStatusResponce(boolean isChangeStutusSucessully) {
        if (isChangeStutusSucessully && !isToLogout) {
            ContentValues values = new ContentValues();
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(MyApplication.getAppContext());
            values.put(userDBAdapter.getSTATUS(), statusText);
            values.put(userDBAdapter.getSTATUSMESSAGE(), statusMessage);
            userMO.setStatus(statusText);
            userMO.setStatusMessage(statusMessage);

            try {
                //     SettingManager.getInstance().updateUserMO(userMO);
                userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!isChangeStutusSucessully) {
            Resources resources = getApplicationContext().getResources();
            String message = resources.getString(R.string.xmpp_error_message);
            // show dialog for broken Xmpp connection
//            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

        if (isToLogout) {
            deRegisterUser(isChangeStutusSucessully, 0);
        }
        Intent intent = new Intent(VariableConstants.STATUS_CHANGED_SUCESSFULLY);
        intent.putExtra(VariableConstants.STATUS_CODE, statusCode);
        intent.putExtra(TextConstants.STATUS_RESPONSE, isChangeStutusSucessully);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Truncate user while logout
     */
    private void truncateUserLogoutSucessfully() {
        conversations = null;
        conversations = null;
        if (BuildConfig.isPatientApp) {
            /** FIXME see FIXME in {@link logoutUser(Context context)} */
            //mHelper.truncateDB();
        }
        //PiwikUtils.sendEvent(getApplication(), PiwikConstants.KLOGIN_CATEGORY, PiwikConstants.KLOGOUT_ACTION, PiwikConstants.KLOGOUT_TXT);
    }

    /**
     * Disconnect XMPP connection
     */
    protected void disconnectXMPPConnection() {
        try {
            new Thread(new Runnable() {
                public void run() {
                    XMPPConnection connection = ConnectionInfo.getInstance().getXmppConnection();
                    if (connection != null)
                        connection.disconnect();

                    connection = null;
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Logging out user
     *
     * @param context
     */
    public void logoutUser(Context context) {
            try {
                userMO = SettingManager.getInstance().getUserMO();
                userMO.setSalutation(TextConstants.NO);
                UserDBAdapter.getInstance(MyApplication.getAppContext()).updateSalutation(userMO.getPersistenceKey(), TextConstants.NO);
            } catch (Exception e) {
                e.printStackTrace();
            }

        disconnectXMPPConnection();
        shutdownTimers();

        SharedPref.setUserLogout(true);

        Intent mIntent;
        stopSelf();

        SharedPref.setAccessToken(null);
        SharedPref.setLong(SharedPref.SEARCH_PATIENT_BTG_TIME, 0);
        // config check for hard logout
        if (!BuildConfig.isSoftSignOut) {
            Utils.truncateDB();

        }
        return;
    }


    private void clearSharedPref() {

        SharedPreferences preferences = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        String phoneNumber = preferences.getString(VariableConstants.PHONE_NUMBER, "");
        String countryCode = preferences.getString(VariableConstants.COUNTRY_CODE, "");
        String otpSavedCode = preferences.getString(VariableConstants.OTP_CODE, "");
        long otp_send_time = preferences.getLong(VariableConstants.OTP_SEND_TIME, 0);
        boolean isOTPVerified = preferences.getBoolean(VariableConstants.HAS_VERIFIED_OTP, false);
        boolean isUserExist = preferences.getBoolean(VariableConstants.USER_EXIST, false);
        boolean isSignUpScreenPassed = preferences.getBoolean(VariableConstants.IS_SIGNUP_SCREEN_PASSED, false);
        boolean isAppTourPassed = preferences.getBoolean(VariableConstants.APP_TOUR_SHOWN, false);
        Editor edit = preferences.edit();
        edit.clear().commit();
        edit = preferences.edit();
        edit.putString(VariableConstants.PHONE_NUMBER, phoneNumber);
        edit.putString(VariableConstants.COUNTRY_CODE, countryCode);
        edit.putString(VariableConstants.OTP_CODE, otpSavedCode);
        edit.putLong(VariableConstants.OTP_SEND_TIME, otp_send_time);
        edit.putBoolean(VariableConstants.HAS_VERIFIED_OTP, isOTPVerified);
        edit.putBoolean(VariableConstants.USER_EXIST, isUserExist);
        edit.putBoolean(VariableConstants.IS_SIGNUP_SCREEN_PASSED, isSignUpScreenPassed);
        edit.putBoolean(VariableConstants.APP_TOUR_SHOWN, isAppTourPassed);
        edit.commit();
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(false);
        config = ConfigManager.getInstance().updateConfig(config);
    }

    public void deRegisterUser(boolean isChangeStutusSucessully, int retryAttempts) {
        if (retryAttempts > TextConstants.MAX_RETRY) {
            return;
        }
        if (isChangeStutusSucessully) {
            try {
                new DeRegisterUserDeviceBG(activity, MessengerService.this, false, retryAttempts).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } catch (Exception e) {
                Utils.showAlert(getApplicationContext(), VariableConstants.UNEXPECTED_TITLE, VariableConstants.UNEXPECTED_MESSAGE);
            }
        } else {
            // dismissDialog();
        }
    }

    public void setPresenceStatus(int statusCode) {
        this.statusCode = statusCode;
    }

    private void groupCreatedListener() {
        if (null == connection) {
            return;
        }
        if (groupCreatedPacketListener != null) {
            connection.removePacketListener(groupCreatedPacketListener);
        }
        groupCreatedPacketListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                sendGroupPresence(packet.getFrom());
                try {
                    if (inviteList == null || inviteList.size() <= 0) {
                        return;
                    }

                    updateGroupChatTextMO(packet);
                    processGroupCreationPresencePacket(packet);

                    updateChatConversationMOGroupCreation(packet, inviteList);
                    // send invitation

                    sendInvite(packet.getFrom(), stopProgressBarBroadcast);

                   /* ArrayList<PiwikVar> customVar = new ArrayList<PiwikUtils.PiwikVar>();
                    if (null != conversations && conversations.size() > 0) {
                        customVar.add(new PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.KGROUP_NUMBER_GROUP_LABLE, conversations.size() + ""));
                    }*/
                    // PiwikUtils.sendEvent(getApplication(), PiwikConstants.KGROUP_CATEGORY, PiwikConstants.KGROUP_CREATED_ACTION,
                    // PiwikConstants.KGROUP_CREATE_TXT, customVar);

                } catch (Exception e) {
                    e.printStackTrace();
                    Intent intent = new Intent(VariableConstants.GROUP_INVITE_SENT);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            }
        };
        PacketFilter packetFilter = new NewGroupSuccessTypeFilter(NewGroupIQ.Type.result);
        connection.addPacketListener(groupCreatedPacketListener, packetFilter);
    }

    private void groupNamePacketListener() {
        if (null == connection) {
            return;
        }
        if (groupNamePacketListener != null) {
            connection.removePacketListener(groupNamePacketListener);
        }
        groupNamePacketListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                new ChangeUpdatGroupName(new ApiResponseCallback<ArrayList<ChatConversationMO>>() {

                    @Override
                    public void onSuccessRespone(ArrayList<ChatConversationMO> result) {
                        if (conversations == null) {
                            conversations = new ConcurrentHashMap<String, ChatConversationMO>();
                        }
                        for (int i = 0; i < result.size(); i++) {
                            ChatConversationMO chatConversationMO = result.get(i);
                            conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
                            notifyConversationsListChange(chatConversationMO);
                        }
                        notifyGroupConversationsListChange();
                    }

                    @Override
                    public void onFailedResponse(Exception exception) {
                        // TODO Auto-generated method stub

                    }
                }, getApplicationContext(), false, null, null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, packet);
            }

        };
        PacketFilter packetFilter = new GroupNameTypeFilter();
        connection.addPacketListener(groupNamePacketListener, packetFilter);
    }

    public void changeUpdateGroupName(String groupName, String from) {
        ChatConversationMO chatconversationMo = new ChatConversationMO();
        if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatconversationMo.setNickName(groupName);
        }
        chatconversationMo.setGroupName(groupName);
        // String extractGroupId = Utils.extractGroupId(packet.getFrom(), settingMO.getFisikeServerIp());
        // extractGroupId = Utils.extractGroupId(extractGroupId, settingMO.getFisikeServerIp());
        chatconversationMo.setJId(from);
        // chatconversationMo.generatePersistenceKey(userMO.getUserName());
        chatconversationMo.generatePersistenceKey(userMO.getId() + "");

        String pk = chatconversationMo.getPersistenceKey() + "";
        try {
            chatconversationMo = chatConversationDBAdapter.fetchChatConversationMO(pk);
        } catch (Exception e) {
        }

        if (null == chatconversationMo) {
            chatconversationMo = new ChatConversationMO();
            chatconversationMo.setConversationType(VariableConstants.conversationTypeChat);
            chatconversationMo.setJId(from);
            chatconversationMo.generatePersistenceKey(userMO.getId() + "");
            chatconversationMo.setUnReadCount(0);
            chatconversationMo.setGroupName(groupName);
            chatconversationMo.setNickName(groupName);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("groupName", groupName);
        contentValues.put("nickName", groupName);
        // update these values in table in background thread.
        // new UpdateDatabaseAsyncTask(chatconversationMo, DatabaseTables.ChatConversationMO_TABLE, contentValues).execute();
        chatConversationDBAdapter.updateChatConversation(chatconversationMo.getPersistenceKey(), contentValues);
        // }

        chatconversationMo.setGroupName(groupName);
        if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatconversationMo.setNickName(groupName);
        }

        // if (!conversations.containsKey(chatconversationMo.getPersistenceKey() + "")) {
        conversations.put(pk, chatconversationMo);

        notifyGroupConversationsListChange();
        notifyConversationsListChange(chatconversationMo);
        // }
    }

    private void initGroupDeletionListener() {
        if (null == connection) {
            return;
        }
        if (groupDeletePacketListener != null) {
            connection.removePacketListener(groupDeletePacketListener);
        }
        groupDeletePacketListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {

//                DBAdapter.getInstance(MessengerService.this).deleteChatConversationMo(packet.getFrom());
//                notifyConversationsListChange();
//                notifyGroupConversationsListChange();
            }
        };
        PacketFilter packetFilter = new InvitationTypeFilter();
        connection.addPacketListener(groupDeletePacketListener, packetFilter);
    }

    private void initgroupInviteListener() {
        // groupInviteListener
        if (null == connection) {
            return;
        }
        if (groupInviteListener != null) {
            connection.removePacketListener(groupInviteListener);
        }
        groupInviteListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                handleInvitation(packet);
            }
        };
        PacketFilter packetFilter = new InvitationTypeFilter();
        connection.addPacketListener(groupInviteListener, packetFilter);
    }

    // error packet that indicates group creation failed
    private void initErrorListener() {
        if (null == connection) {
            return;
        }
        if (errorListener != null) {
            connection.removePacketListener(errorListener);
        }
        errorListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                Intent intent = new Intent(VariableConstants.GROUP_INVITE_SENT);
                intent.putExtra(VariableConstants.GROUP_CREATION_ERROR, true);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            }
        };
        PacketFilter packetFilter = new ErrorPacketTypeFilter();
        connection.addPacketListener(errorListener, packetFilter);
    }

    private void initSpoolTypeListener() {
        if (null == connection) {
            return;
        }
        if (spoolPacketListener != null) {
            connection.removePacketListener(spoolPacketListener);
        }
        spoolPacketListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                // delete entry from local datastructure
                SpoolPacketIQ spoolPacket = (SpoolPacketIQ) packet;
                MyApplication.getInstance().removeSuccessFullyDeletedMessageId(spoolPacket.getPacketIDList());
            }
        };
        PacketFilter packetFilter = new SpoolTypeFilter();
        connection.addPacketListener(spoolPacketListener, packetFilter);
    }

    /**
     * TODO check if it comes in this
     */
    private void initRemoveGroupMemberTypeFilter() {
        if (null == connection) {
            return;
        }
        if (memberRemovedFromGroupListener != null) {
            connection.removePacketListener(memberRemovedFromGroupListener);
        }
        memberRemovedFromGroupListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                CopyOnWriteArrayList list = new CopyOnWriteArrayList<String>();
                list.add(packet.getPacketID());
                MyApplication.getInstance().setMessageIdList(list);
            }
        };
        PacketFilter packetFilter = new RemoveGroupMemberTypeFilter();
        connection.addPacketListener(memberRemovedFromGroupListener, packetFilter);
    }

    private void presencePacketListener() {
        if (null == connection) {
            return;
        }
        if (presenceListener != null) {
            connection.removePacketListener(presenceListener);
        }

        presenceListener = new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                // adding packets in local ds which will be send to server after every 5sec

                CopyOnWriteArrayList list = new CopyOnWriteArrayList<String>();
                list.add(packet.getPacketID());
                MyApplication.getInstance().setMessageIdList(list);

                Presence presencePacket = (Presence) packet;

                if (!presencePacket.getFrom().contains("@" + VariableConstants.groupConferenceName + settingMO.getFisikeServerIp())
                        && presencePacket.getTo().equalsIgnoreCase(userMO.getId() + "@" + settingMO.getFisikeServerIp())
                        && presencePacket.toXML().contains("<status")) {
                    new HandlePresenceStatus(new ApiResponseCallback<Boolean>() {

                        @Override
                        public void onSuccessRespone(Boolean result) {
                            if (result) {
                                notifyConversationsListChange();
                            }
                        }

                        @Override
                        public void onFailedResponse(Exception exception) {

                        }
                    }, getApplicationContext(), false).execute(packet);
                } else if (presencePacket.getType().equals(Presence.Type.unavailable) || packet.getFrom().contains("/")) {
                    // new HandlePresense(packet).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    new HandlePresence(new ApiResponseCallback<HandlePresenceStanza>() {

                        @Override
                        public void onSuccessRespone(HandlePresenceStanza result) {
                            if (result != null && result.isAddMember()) {
                                if (null == conversations) {
                                    conversations = new ConcurrentHashMap<String, ChatConversationMO>();
                                }
                                ChatConversationMO chatConversationMO = result.getChatConversationMO();
                                String emailContacts = result.getEmailContacts();
                                conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
                                notifyGroupConversationsListChange();
                                if (!emailContacts.equals("")) {
                                    loadContactDetails(emailContacts, null);
                                }
                                if (chatConversationMO.getChatContactMoKeys() != null && chatConversationMO.getChatContactMoKeys().size() > 2) {
                                    notifyConversationsAdd(chatConversationMO);
                                }
                            } else if (result != null && result.isRemoveMember()) {
                                ChatConversationMO chatConversationMO = result.getChatConversationMO();
                                conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
                                notifyGroupConversationsListChange();
                                notifyConversationsListChange(chatConversationMO);
                            }
                        }

                        @Override
                        public void onFailedResponse(Exception exception) {
                        }
                    }, getApplicationContext(), false, null, null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, packet);
                }
            }

        };
        PacketTypeFilter packetFilter = new PacketTypeFilter(Presence.class);
        connection.addPacketListener(presenceListener, packetFilter);
    }

    private void updateGroupChatTextMO(Packet packet) {
        GroupTextChatMO groupTextChat = new GroupTextChatMO();
        String groupId = Utils.extractGroupId(packet.getFrom());
        String id = Utils.cleanXMPPServer(packet.getTo());
        groupTextChat.generatePersistenceKey(groupId, id);

        groupTextChat.setAdmin(new ArrayList<String>(Arrays.asList((VariableConstants.CHAT_CONTACT_MO + userMO.getId()).hashCode() + "")));

        if (groupProfilePic != null) {
            groupTextChat.setImageData(Utils.convertBitmapToByteArray(groupProfilePic));
        } else {
            groupTextChat.setImageData(null);
        }

        groupTextChat.generateChatConversationMOPK(groupId, id);
        storeGroupContactMo(groupTextChat, null);
    }

    public void processGroupCreationPresencePacket(Packet packet) {
        for (ChatContactMO chatContact : inviteList) {
            try {
                // ideal way out is to update the joined group table
                ChatContactMO existingContact = chatContactDBAdapter.fetchChatContactMo(chatContact.getPersistenceKey() + "");
                if (existingContact == null) {
                    // create a new object and set the values in it
                    ChatContactMO newContact = new ChatContactMO();
                    newContact = chatContact;
                    ArrayList<String> joinedGroupArray = new ArrayList<String>();
                    newContact.setJoinedGroupMOPK(joinedGroupArray);
                    new DatabaseAsyncTask(newContact, DatabaseTables.ChatContactMo_TABLE).execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * create new chatconversation for creating group
     *
     * @param packet
     * @param inviteList
     */
    public void updateChatConversationMOGroupCreation(Packet packet, List<ChatContactMO> inviteList) {
        ChatConversationMO chatConversationMO = new ChatConversationMO();
        chatConversationMO.setJId(packet.getFrom());
        chatConversationMO.setGroupName(groupName);
        if (!groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatConversationMO.setNickName(groupName);
        }
        chatConversationMO.setUnReadCount(0);
        chatConversationMO.setReadStatus(true);
        if (groupName != null && groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            String secoundryId = "";
            for (int i = 0; i < inviteList.size(); i++) {
                if (!(userMO.getId() + "").equals(inviteList.get(i).getId())) {
                    secoundryId = inviteList.get(i).getId();
                    break;
                }
            }
            chatConversationMO.setSecoundryJid(Utils.genrateSecoundryId(packet.getFrom()));
        }
        chatConversationMO.generatePersistenceKey(userMO.getId() + "");

        chatConversationMO.setMyStatus(VariableConstants.chatConversationMyStatusActive);
        // FIXME verify if this is still needed ....
        chatConversationMO.setGroupStatus(TextConstants.GROUP_CREATED);

        ArrayList<String> contatMoPK = new ArrayList<String>();
        for (int i = 0; i < inviteList.size(); i++) {
            // contatMoPK.add(ChatContactMO.getPersistenceKey(inviteList.get(i).getId() + ""));
            contatMoPK.add(ChatContactMO.getPersistenceKey(inviteList.get(i).getId()) + "");
        }

        ChatContactMO chatContactMO = new ChatContactMO();
        chatContactMO.generatePersistenceKey(userMO.getId() + "");

        contatMoPK.add(chatContactMO.getPersistenceKey() + "");

        chatConversationMO.setChatContactMoKeys(contatMoPK);

        if (groupName.equalsIgnoreCase(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            chatConversationMO.setConversationType(VariableConstants.conversationTypeChat);
        } else {
            chatConversationMO.setConversationType(VariableConstants.conversationTypeGroup);
        }
        if (conversations.containsKey(chatConversationMO.getPersistenceKey() + "")) {
            ChatConversationMO tempChatConversationMO = conversations.get(chatConversationMO.getPersistenceKey() + "");
            tempChatConversationMO.setChatContactMoKeys(contatMoPK);
            ContentValues contentValues = new ContentValues();
            if (contatMoPK != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out;
                    out = new ObjectOutputStream(bytes);
                    out.writeObject(contatMoPK);
                    contentValues.put("chatContactMoKeys", bytes.toByteArray());
                    conversations.put(chatConversationMO.getPersistenceKey() + "", tempChatConversationMO);

                    chatConversationDBAdapter.updateChatConversation(chatConversationMO.getPersistenceKey(), contentValues);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
            notifyGroupConversationsListChange();
            //insert
            chatConversationDBAdapter.insertChatConversationMo(chatConversationMO);
//            saveChatConversationMO(chatConversationMO);
        }
        ArrayList<String> temp = userMO.getChatConversationKeyList();
        temp.add(chatConversationMO.getPersistenceKey() + "");
        userMO.setChatConversationKeyList(temp);
        try {
            //  SettingManager.getInstance().updateUserMO(userMO);
            UserDBAdapter.getInstance(MyApplication.getAppContext()).updateConversationKeyList(userMO.getPersistenceKey(), userMO.getChatConversationKeyList());

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!chatConversationMO.getJId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) || chatConversationMO.getChatContactMoKeys().size() > 2) {

            chatConversationDBAdapter.updateIsChatInitiated(ChatConversationMO.generatePersistenceKey(chatConversationMO.getJId(), userMO.getId() + ""));
            notifyGroupConversationsListChange();

        }
    }

    /**
     * This method is to get all the group information
     */
    private void initGroupListListner() {
        if (null == connection) {
            return;
        }
        if (groupRosterListner != null) {
            connection.removePacketListener(groupRosterListner);
        }

        groupRosterListner = new PacketListener() {

            @Override
            public synchronized void processPacket(Packet packet) {
                DiscoverItems discoverItems = ((DiscoverItems) packet);

                if (null != packet && null != packet.getFrom()) {
                    /**
                     * List of Group members
                     */
                    if ((packet.getFrom().contains(VariableConstants.groupConferenceName + settingMO.getFisikeServerIp()))) {
                        String checkGroupJid = Utils.checkGroupJid(packet.getFrom(), conversations);
                        if (checkGroupJid != null) {
                            updateChatConversationMo(checkGroupJid, packet.getFrom());
                        }

                        new GroupMemberListBG(new ApiResponseCallback<GroupMemberListObject>() {

                            @Override
                            public void onSuccessRespone(final GroupMemberListObject groupMemberListObject) {
                                if (groupMemberListObject == null) {
                                    return;
                                }
                                getGroupName(groupMemberListObject.getChatConversationMO().getJId());

                                // shared pref entry to mark group has been received
                                SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                                Editor edit = preferences.edit();
                                edit.putBoolean(VariableConstants.IS_GROUP_LIST_FETCHED, true);
                                edit.commit();
                                final ChatConversationMO chatConversationMO = groupMemberListObject.getChatConversationMO();
                                if (chatConversationMO != null) {
                                    if (null == conversations) {
                                        conversations = new ConcurrentHashMap<String, ChatConversationMO>();
                                    }
                                    ChatConversationMO namedchatConverstaChatConversationMO = conversations.get(chatConversationMO
                                            .getPersistenceKey() + "");
                                    if (namedchatConverstaChatConversationMO != null && namedchatConverstaChatConversationMO.getGroupName() != null) {
                                        chatConversationMO.setGroupName(namedchatConverstaChatConversationMO.getGroupName());
                                    }

                                    String groupName = groupMemberListObject.getChatConversationMO().getGroupName();
                                    String nickName = groupMemberListObject.getChatConversationMO().getNickName();

                                    if (TextUtils.isEmpty(groupName) && TextUtils.isEmpty(nickName) && chatConversationMO.getJId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {

                                        String adminId = TextPattern.getUserId(Utils.getAdminId(chatConversationMO.getJId()));

                                        if (adminId.equals(String.valueOf(userMO.getId()))) {
                                            adminId = TextPattern.getUserId(Utils.getPateintIdFromJid(chatConversationMO.getJId()));
                                        }
                                        ChatContactMO chatContactMO = null;
                                        try {
                                            chatContactMO = chatContactDBAdapter.getInstance(MyApplication.getAppContext()).fetchChatContactMo(ChatContactMO.getPersistenceKey(adminId));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (chatContactMO != null) {
                                            chatConversationMO.setNickName(chatContactMO.getDisplayName());
                                            groupMemberListObject.getChatConversationMO().setNickName(chatContactMO.getDisplayName());
                                        }

                                    }

                                    /**
                                     * If it is group or has more than 2 members in a chat it should be visible on list.
                                     */

                                    if (!chatConversationMO.getJId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) || chatConversationMO.getChatContactMoKeys().size() > 2) {
                                        chatConversationMO.setIsChatInitiated(1);
                                    }
                                    conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
                                }
                                if (!groupMemberListObject.getEmailSting().equals("")) {
                                    new LoadContactsRealName(MessengerService.this, groupMemberListObject.getEmailSting()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }
//                                String groupName = groupMemberListObject.getChatConversationMO().getGroupName();
//                                updateChatConversationMO(groupMemberListObject.getChatConversationMO());

                                updateChatConversationMO(groupMemberListObject.getChatConversationMO());

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!groupMemberListObject.getChatConversationMO().getJId().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER) || chatConversationMO.getChatContactMoKeys().size() > 2) {
                                            chatConversationDBAdapter.updateIsChatInitiated(ChatConversationMO.generatePersistenceKey(groupMemberListObject.getChatConversationMO().getJId(), userMO.getId() + ""));
                                            notifyGroupConversationsListChange();
                                            notifyConversationsListChange();

                                        }
                                    }
                                }, 1000);


                            }

                            @Override
                            public void onFailedResponse(Exception exception) {

                            }
                        }, getApplicationContext(), conversations).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, packet);
                    }
                }
            }
        };
        PacketFilter packetFilter = new GroupRosterPacketFilter();
        connection.addPacketListener(groupRosterListner, packetFilter);
    }

    /**
     * Update the group with the received group
     *
     * @param checkGroupJid
     * @param groupId
     */
    private void updateChatConversationMo(String checkGroupJid, String groupId) {
        ChatConversationMO chatConversationMO = conversations.get(checkGroupJid);
        chatConversationMO.setJId(groupId);
        conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
        ContentValues updatedValues = new ContentValues();
        updatedValues.put("userName", groupId);
        new UpdateDatabaseAsyncTask(chatConversationMO, DatabaseTables.ChatConversationMO_TABLE, updatedValues)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadContactDetails(String emailContacts, HashMap<String, HashPresence> emailPresence) {
        new LoadContactsRealName(MessengerService.this, emailContacts, emailPresence).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Store the groupTextChat Mo and broadcast the group
     *
     * @param groupTextChatMo
     */
    private void storeGroupContactMo(GroupTextChatMO groupTextChatMo, ContentValues contentValues) {
        try {

            GroupChatDBAdapter groupChatDBAdapter = GroupChatDBAdapter.getInstance(MyApplication.getAppContext());
            if (contentValues != null)
                groupChatDBAdapter.updateGroupTextChatMO(groupTextChatMo.getPersistenceKey(), contentValues);

            else
                groupChatDBAdapter.insert(groupTextChatMo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(VariableConstants.BROADCAST_CONTACT_DETAIL_CHANGE).putExtra(VariableConstants.IS_GROUP_CONTACT_UPDATION, true);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * This method initiates all the XMPP Listeners (for new messages, etc.)
     */
    private void initiateMessageListeners() {
        if (null != incomingPacketListener) {
            connection.removePacketListener(incomingPacketListener);
        }
        incomingPacketListener = new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                new HandleMessage(new ApiResponseCallback<Void>() {

                    @Override
                    public void onSuccessRespone(Void result) {
                    }

                    @Override
                    public void onFailedResponse(Exception exception) {
                    }
                }, getApplicationContext(), false, MessengerService.this, senderId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, packet);

                CopyOnWriteArrayList list = new CopyOnWriteArrayList<String>();
                list.add(packet.getPacketID());
                MyApplication.getInstance().setMessageIdList(list);
            }

        };
        // Now attach this listener to the XMPP Connection
        PacketFilter filter = new MessageTypeFilter();
        connection.addPacketListener(incomingPacketListener, filter);
    }

    private void handleInvitation(final Packet packet) {

        if (packet.getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            String adminId = TextPattern.getUserId(Utils.getAdminId(packet.getFrom()));
            if (adminId.equals(String.valueOf(userMO.getId()))) {
                adminId = TextPattern.getUserId(Utils.getPateintIdFromJid(packet.getFrom()));
            }
            try {
                if (chatContactDBAdapter.getInstance(MyApplication.getAppContext()).fetchChatContactMo(ChatContactMO.getPersistenceKey(adminId)) == null) {
                    new LoadContactsRealName(MessengerService.this, adminId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // adding packets in local ds which will be send to server after every 5sec
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();
        list.add(packet.getPacketID());
        MyApplication.getInstance().setMessageIdList(list);
        // send the presence
//        if (!packet.getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
        getGroupMembers(packet.getFrom());
//        }


        sendGroupPresence(packet.getFrom());
        if (!packet.getFrom().startsWith(VariableConstants.SINGLE_CHAT_IDENTIFIER)) {
            getGroupName(packet.getFrom());
        }
        String groupId = Utils.extractGroupId(packet.getFrom());
    }

    /**
     * This service method is called to send a message to the XMPP Server
     *
     * @param text
     * @param attachmentId
     * @param attachmentType
     * @param timeStamp
     * @param chatConvMO
     * @param chatMessageMO
     * @param isApiSendMessage
     * @param isPendingMessages
     * @return ChatMessageMO object
     */
    public ChatMessageMO sendMessage(String text, String attachmentId, String attachmentType, long timeStamp, ChatConversationMO chatConvMO,
                                     ChatMessageMO chatMessageMO, boolean isApiSendMessage, boolean isPendingMessages) {
        String messageText = text.trim();

        if (messageText.length() <= 0) {
            return null;
        }
        String originalText = messageText;
        Message msg = null;
        String chatType = PiwikConstants.ChatMessageTypeIndividual;
        msg = new Message(chatConvMO.getJId(), Message.Type.groupchat);
        if (VariableConstants.conversationTypeGroup == chatConvMO.getConversationType()) {
            chatType = PiwikConstants.ChatMessageTypeGroup;
        }

        if (null != chatMessageMO) {
            // This means this is resending a failed message - hence manually updating the packetId
            msg.setPacketID(chatMessageMO.getMsgId());
        }

        if (timeStamp == 0) {
            timeStamp = System.currentTimeMillis();
        }

        messageText = msg.getPacketID() + VariableConstants.DELIMITER + messageText;
        msg.setBody(messageText);
        msg.setAttachmentId(attachmentId);
        msg.setAttachmentType(attachmentType);
        msg.setTo(chatConvMO.getJId());

        DeliveryReceiptManager.addDeliveryReceiptRequest(msg);
        int msgSendStatus = DefaultConnection.MESSAGE_NOT_SENT;

        if (checkXMPPConnection()) {
            try {
                connection.sendPacket(msg);

               /* ArrayList<PiwikUtils.PiwikVar> customPiwikVar = new ArrayList<PiwikUtils.PiwikVar>();
                customPiwikVar.add(new PiwikUtils.PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.ChatMessageType, chatType));*/

                if (null != attachmentType) {
                    if (VariableConstants.ATTACHMENT_IMAGE.equalsIgnoreCase(attachmentType)) {
                       /* customPiwikVar.add(new PiwikUtils.PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.AttachmentType,
                                PiwikConstants.AttachmentTypeImage));*/
                    } else if (VariableConstants.ATTACHMENT_VIDEO.equalsIgnoreCase(attachmentType)) {
                       /* customPiwikVar.add(new PiwikUtils.PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.AttachmentType,
                                PiwikConstants.AttachmentTypeVideo));*/
                    } else if (TextConstants.ATTACHMENT_ACTVITY.equalsIgnoreCase(attachmentType)) {
                        /*customPiwikVar.add(new PiwikUtils.PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.AttachmentType,
                                PiwikConstants.AttachmentTypeQB));*/
                    }
                }

                if (isPendingMessages) {
                   /* PiwikUtils.sendEvent(getApplication(), PiwikConstants.KCHAT_CATEGORY, PiwikConstants.KCHAT_OFFLINE_ACTION,
                            PiwikConstants.KCHAT_OFFLINE_TXT, customPiwikVar);*/
                } else {
                   /* PiwikUtils.sendEvent(getApplication(), PiwikConstants.KCHAT_CATEGORY, PiwikConstants.KCHAT_MSG_SENT_ACTION,
                            PiwikConstants.KCHAT_MSGSENT_TXT, customPiwikVar);*/
                }

                if (chatMessageMO != null) {
                    AppLog.d("Fisike", "com.mphrx.fisike.services.sendMessage Msg Text : " + chatMessageMO.getMessageText() + " Msg Id : "
                            + chatMessageMO.getMsgId() + " Msg status : " + msgSendStatus + " PK : " + chatMessageMO.getPersistenceKey()
                            + " Thread Id : " + Thread.currentThread().getId() + " Thread Name : " + Thread.currentThread().getName()
                            + " PendingMessag : " + isPendingMessages + " Api Message : " + isApiSendMessage);
                } else {
                    AppLog.d("Fisike", "com.mphrx.fisike.services.sendMessage pk will genrate Msg text :" + msg.getBody() + " Msg Status : "
                            + msgSendStatus + " Thread Id : " + Thread.currentThread().getId() + " Thread Name : " + Thread.currentThread().getName()
                            + " PendingMessag : " + isPendingMessages + " Api Message : " + isApiSendMessage);
                }
            } catch (Exception e) {
                AppLog.d("Fisike", "MessengerService | Sending message packet error pk : " + e.getMessage());
                e.printStackTrace();
                msgSendStatus = DefaultConnection.MESSAGE_NOT_SENT;
            }
        } else {
            AppLog.d("Fisike", "MessengerService | Sending message packet error : Connection error");
            msgSendStatus = DefaultConnection.MESSAGE_NOT_SENT;
        }

        if (isPendingMessages || isApiSendMessage) {
            // Update the chat message status
            chatMessageMO.setMsgStatus(msgSendStatus);
            updateStoreMessageStatus(chatConvMO, chatMessageMO, msgSendStatus, isPendingMessages);
            notifyChatMessage(chatConvMO, chatMessageMO);
        } else {
            chatMessageMO = storeSentMessage(msg, chatConvMO, originalText, msgSendStatus, timeStamp);
        }

        return chatMessageMO;
    }

    /**
     * This service method is called to send a message to the XMPP Server
     *
     * @param text
     * @param attachmentType
     * @param chatConvMO
     * @param timeStampUTC
     */
    public ChatMessageMO saveMessage(String text, String attachmentType, ChatConversationMO chatConvMO, long timeStampUTC) {
        String messageText = text.trim();

        if (messageText.length() <= 0) {
            return null;
        }
        String originalText = messageText;
        Message msg = null;

        msg = new Message(chatConvMO.getJId(), Message.Type.chat);

        messageText = msg.getPacketID() + VariableConstants.DELIMITER + messageText;
        msg.setBody(messageText);
        // msg.setFrom(userMO.getUserName() + "@" + settingMO.getFisikeServerIp() + "/" + settingMO.getFisikeResource());
        msg.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp() + "/" + settingMO.getFisikeResource());
        msg.setTo(chatConvMO.getJId());
        msg.setAttachmentType(attachmentType);

        int msgSendStatus = DefaultConnection.MESSAGE_NOT_SENT;

        // Save Message to DB
        return storeSentMessage(msg, chatConvMO, originalText, msgSendStatus, timeStampUTC);
    }

    /**
     * This method stores the new sent message into the persistence layer - it also updates the information in the corresponding chat conversationMO
     *
     * @param msg
     * @param chatConvMO
     * @param messageText
     * @param msgSendStatus
     * @param timeStamp
     */
    public ChatMessageMO storeSentMessage(Message msg, ChatConversationMO chatConvMO, String messageText, int msgSendStatus, long timeStamp) {
        // Firstly create the ChatMsgMO object
        ChatMessageMO chatMsgMO = new ChatMessageMO();

        // chatMsgMO.setSenderUserId(userMO.getUserName());
        chatMsgMO.setSenderUserId(userMO.getId() + "");
        chatMsgMO.setMessageText(messageText);
        chatMsgMO.setMsgStatus(msgSendStatus);
        chatMsgMO.setLastUpdatedTimeUTC(System.currentTimeMillis());
        chatMsgMO.setMsgId(msg.getPacketID());
        chatMsgMO.generatePersistenceKey();
        chatMsgMO.setConvPersistenceKey(chatConvMO.getPersistenceKey());
        chatMsgMO.setAttachmentID(msg.getAttachmentId());
        chatMsgMO.setAttachmentType(msg.getAttachmentType());

        // Update the conversation with last message data
        chatConvMO.setLastMsg(messageText);
        chatConvMO.setLastMsgStatus(msgSendStatus);
        chatConvMO.setLastMsgId(chatMsgMO.getMsgId());
        chatConvMO.setCreatedTimeUTC(chatMsgMO.getLasUpdatedTimeUTC());

        // Update unread count
        chatConvMO.setUnReadCount(0);
        chatConvMO.setReadStatus(true);


        // Store the New Message
        //update

        ContentValues values = new ContentValues();
        values.put(ChatConversationDBAdapter.getLASTMSG(), chatConvMO.getLastMsg());
        values.put(ChatConversationDBAdapter.getLASTMSGSTATUS(), chatConvMO.getLastMsgStatus());
        values.put(ChatConversationDBAdapter.getLASTMSGID(), chatConvMO.getLastMsgId());
        values.put(ChatConversationDBAdapter.getCREATEDTIMEUTC(), chatConvMO.getCreatedTimeUTC());
        values.put(ChatConversationDBAdapter.getUNREADCOUNT(), chatConvMO.getUnReadCount());
        values.put(ChatConversationDBAdapter.getREADSTATUS(), chatConvMO.getReadStatus());
        if (chatMsgMO != null) {
            String msgPk = chatMsgMO.getPersistenceKey() + "";
            Vector<String> chatMsgKeyList = chatConvMO.getChatMessageKeyList();
            if (null == chatMsgKeyList) {
                chatMsgKeyList = new Vector<String>();
            }
            if (!chatMsgKeyList.contains(msgPk)) {
                chatMsgKeyList.add(msgPk);
            }
            chatConvMO.setChatMessageKeyList(chatMsgKeyList);
            // Also add link to conversation to the messageMO
        }

        try {
            if (chatConvMO.getChatMessageKeyList() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(chatConvMO.getChatMessageKeyList());
                values.put(ChatConversationDBAdapter.getCHATMESSAGEKEYLIST(), bytes.toByteArray());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Update Chat Conversation Mo
        updateChatConversation(chatConvMO, false, values);

        //store ChatMessage Mo
        /*
        *
        * TODO this is called to store new message so creating insert call
        * */
        storeChatMessage(chatMsgMO, chatConvMO, false, false, null);


        return chatMsgMO;
    }


    public void copyMessageKeyListFromChatMessageMoToChatConversationMo(ChatMessageMO chatMessageMO, ChatConversationMO chatConvMO) {
        if (chatMessageMO != null) {
            String msgPk = chatMessageMO.getPersistenceKey() + "";
            Vector<String> chatMsgKeyList = chatConvMO.getChatMessageKeyList();
            if (null == chatMsgKeyList) {
                chatMsgKeyList = new Vector<String>();
            }
            if (!chatMsgKeyList.contains(msgPk)) {
                chatMsgKeyList.add(msgPk);
            }
            chatConvMO.setChatMessageKeyList(chatMsgKeyList);
            // Also add link to conversation to the messageMO
        }

    }

    /**
     * Update the message status for pending messages and message send again
     *
     * @param chatConvMO
     * @param chatMessageMO
     * @param msgSendStatus
     * @param isPendingMessages
     */
    private void updateStoreMessageStatus(ChatConversationMO chatConvMO, ChatMessageMO chatMessageMO, int msgSendStatus, boolean isPendingMessages) {
        if (null == chatMessageMO || (msgSendStatus != chatMessageMO.getMsgStatus() && !isPendingMessages)) {
            return;
        }
        int msgStatus = chatMessageMO.getMsgStatus();
        ContentValues updatedChatMessageInfo = null;
        // Update if message delivered or message sent to server
        if ((DefaultConnection.MESSAGE_DELIVERED == msgSendStatus)
                || (DefaultConnection.MESSAGE_SENT == msgSendStatus || msgStatus == DefaultConnection.MESSAGE_SENT_TO_SERVER) || (isPendingMessages)) {
            chatMessageMO.setMsgStatus(msgSendStatus);
            if (chatMessageMO.getLasUpdatedTimeUTC() == chatConvMO.getCreatedTimeUTC()) {
                chatConvMO.setLastMsgStatus(msgSendStatus);
            }
            // Store the New Message
            ChatMessageMO fetchMessageMo = null;
            try {
                fetchMessageMo = ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).fetchChatMessageMO(chatMessageMO.getPersistenceKey() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (fetchMessageMo != null) {
                updatedChatMessageInfo = new ContentValues();
                updatedChatMessageInfo.put(ChatMessageDBAdapter.getMSGSTATUS(), chatMessageMO.getMsgStatus());
            }
            //update
            ContentValues values = new ContentValues();
            values.put(ChatConversationDBAdapter.getLASTMSGSTATUS(), chatConvMO.getLastMsgStatus());

            //copy MessageKeyList from ChatMessage to ChatConversation

            copyMessageKeyListFromChatMessageMoToChatConversationMo(chatMessageMO, chatConvMO);
            try {
                if (chatConvMO.getChatMessageKeyList() != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(chatConvMO.getChatMessageKeyList());
                    values.put(ChatConversationDBAdapter.getCHATMESSAGEKEYLIST(), bytes.toByteArray());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            updateChatConversation(chatConvMO, false, values);

            storeChatMessage(chatMessageMO, chatConvMO, false, false, updatedChatMessageInfo);
        }
    }

    /**
     * This method checks if the XMPP Connection is valid
     *
     * @return boolean status
     */
    private boolean checkXMPPConnection() {
        if (null != userMO && null != connection && connection.isConnected() && connection.isAuthenticated()) {
            try {
                String status = userMO.getStatus();
                Presence.Type type = Presence.Type.available;
                if (status == null) {
                    type = Presence.Type.available;
                } else if (status.equalsIgnoreCase(TextConstants.STATUS_AVAILABLE_TEXT)) {
                    type = Presence.Type.available;
                } else if (status.equalsIgnoreCase(TextConstants.STATUS_BUSY_TEXT)) {
                    type = Presence.Type.busy;
                } else if (status.equalsIgnoreCase(TextConstants.STATUS_UNAVAILABLE_TEXT)) {
                    type = Presence.Type.unavailable;
                }
                Presence presence = new Presence(type);
                connection.sendPacket(presence);
            } catch (Exception e) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.checkXMPPConnection Connected : false");
            }
            AppLog.d("Fisike", "com.mphrx.fisike.services.checkXMPPConnection Connected : " + connection.isAuthenticated());
            return true;
        }
        AppLog.d("Fisike", "com.mphrx.fisike.services.checkXMPPConnection Connected : false");
        return false;
    }


    public String getUserStatus(String userId) {
        if (checkXMPPConnection()) {
            return connection.getRoster().getPresence(userId).getStatus();
        }

        return "";
    }

    /**
     * This method is called when a class wants to update a conversation
     */
    public void updateChatConversationMO(ChatConversationMO chatConversationMO) {
        //update
        storeChat(null, chatConversationMO, false, false);
    }

    /**
     * This method is called when a class wants to update chat contact
     *
     * @param chatContactMO
     */
    /*public void updateChatContactMO(ChatContactMO chatContactMO) {
        storeChat(chatContactMO);
    }*/


    /**
     * This method provides the activity the ability to get all messages in a conversation
     *
     * @param chatConvMO
     * @param senderNickNameHash
     * @return
     */
    public ArrayList<MessageItem> getMessagesInConversation(ChatConversationMO chatConvMO, boolean isGroupUser,
                                                            HashMap<String, String> senderNickNameHash) {
        ArrayList<MessageItem> messagesList = new ArrayList<MessageItem>();
        try {
            List<ChatMessageMO> messages = ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).fetchMessagesInConversation(chatConvMO.getPersistenceKey());

            Iterator<ChatMessageMO> msgIte = messages.iterator();
            ChatMessageMO preMsgMO = null;
            while (msgIte.hasNext()) {
                ChatMessageMO currMsg = msgIte.next();
                currMsg = setUserNickName(currMsg, isGroupUser, senderNickNameHash);
                String attachmentType = currMsg.getAttachmentType();
                if (null != attachmentType
                        && (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_IMAGE) || attachmentType
                        .equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO))) {
                    AttachmentMo attachmentMo = AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).fetchAttachmentMO_ChatMessageMo(currMsg.getPersistenceKey() + "");
                    if (null != attachmentMo) {
                        currMsg.setAttachmentStatus(attachmentMo.getStatus());
                        currMsg.setFileSize(attachmentMo.getFileSize());
//                currMsg.setPercentage(TaskManager.getInstance().taskQueueProgress(currMsg.getPersistenceKey() + ""));
                        currMsg.setPercentage(AttachmentQueue.getInstance().taskQueueProgress(currMsg.getPersistenceKey() + ""));
                    }
                }
                if (null != currMsg) {
                    if (preMsgMO == null || !Utils.isSameDate(preMsgMO.getLasUpdatedTimeUTC(), currMsg.getLasUpdatedTimeUTC())) {
                        Date date = new Date(currMsg.getLasUpdatedTimeUTC());
                        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
                        messagesList.add(new SectionItem(dateFormat.format(date).toString()));
                    }
                    messagesList.add(currMsg);
                    preMsgMO = currMsg;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return messagesList;
    }

    /**
     * Set userNick name for the users
     *
     * @param chatMessageMO
     * @return
     */
    public ChatMessageMO setUserNickName(ChatMessageMO chatMessageMO, boolean isGroupChat, HashMap<String, String> senderNickNameHash) {
//        if (isGroupChat) {
        String senderId = chatMessageMO.getSenderUserId();

        if (null != senderNickNameHash && senderNickNameHash.containsKey(senderId)) {
            chatMessageMO.setSenderNickName(senderNickNameHash.get(senderId));
            return chatMessageMO;
        }

        ChatContactMO contactMO = new ChatContactMO();

        senderId = Utils.cleanXMPPServer(senderId);
        try {
            senderId = URLDecoder.decode(senderId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }

        contactMO.generatePersistenceKey(senderId);

        ChatContactMO chatContactMo = getChatContactMo(contactMO.getPersistenceKey() + "");

        if (null != chatContactMo && null != chatContactMo.getRealName()) {
            senderId = chatContactMo.getRealName();
        } else {
            String name = chatContactDBAdapter.getChatSenderName(senderId);
            if (!name.equals("") && !name.startsWith("null")) {
                senderId = name;
            }
            if (name.startsWith("null")) {
                getGroupChatUsersDetails(senderId);
            }
        }
        chatMessageMO.setSenderNickName(senderId);
//        }

        return chatMessageMO;
    }

    /**
     * This method provides the activity the ability to get all messages in a conversation for all attachmnets status in process
     *
     * @param chatConvMO
     * @return
     */
    public ArrayList<AttachmentMo> getMessagesInConversationForAttachment(ChatConversationMO chatConvMO) {
        try {
            return AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).fetchMessagesInConversationForAttachments(chatConvMO.getPersistenceKey());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This method provides the activity the ability to get all messages in a conversation for all attachmnets status in process
     *
     * @param attachmentMoList
     * @return
     */
    public void storeAttachmentStatus(ArrayList<AttachmentMo> attachmentMoList) {
        for (int i = 0; attachmentMoList != null && i < attachmentMoList.size(); i++) {
            AttachmentMo attachmentMO = attachmentMoList.get(i);
            try {
                //TODO update call if problem exist in changeStatus
                // mHelper.createOrUpdateAttachmentMO(attachmentMO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method gets the conversation
     *
     * @param jid
     * @param otherUserId
     * @param nickName
     * @param isProcessPacket
     * @param groupID
     * @return
     */
    public ChatConversationMO getConversationMO(String jid, String otherUserId, String nickName, boolean isProcessPacket, boolean isAddContactScreen,
                                                String groupID) {
        jid = Utils.cleanXMPPUserName(jid);
        String convMOPersistenceKey = ChatConversationMO.generatePersistenceKey(jid, userMO.getId() + "") + "";
        ChatConversationMO chatConvMO = null;
        if (conversations == null) {
            getConversationsList();
        }
        if (conversations != null && conversations.containsKey(convMOPersistenceKey)) {
            // There is already a chat conversation that exists in memory
            chatConvMO = conversations.get(convMOPersistenceKey);
            // Increment Unread count
            if (senderId != null) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO : sender user id : " + senderId);
            }
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO : User Name : " + chatConvMO.getJId() + " PacketProcessd : "
                    + isProcessPacket + " Background : " + Utils.isAppbackground(getApplicationContext()) + " last msg : " + chatConvMO.getLastMsg());
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO thread Id : " + Thread.currentThread().getId() + " Thread Name : "
                    + Thread.currentThread().getName());
            if (senderId == null || (isProcessPacket && !senderId.equals(chatConvMO.getSecoundryJid()))
                    || Utils.isAppbackground(getApplicationContext())) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO count : " + chatConvMO.getUnReadCount());
                chatConvMO.setReadStatus(false);
            }

            if (null == nickName || nickName.equals("")) {
                String fisikeUserName = Utils.getFisikeUserName(otherUserId, settingMO.getFisikeServerIp());
                loadContactDetails(fisikeUserName, null);
                ProfilePicTaskManager.getInstance().addTask(new LoadContactsProfilePic(this, fisikeUserName));
                ProfilePicTaskManager.getInstance().startTask();
            }
        } else if (!isAddContactScreen) {
            chatConvMO = new ChatConversationMO();

            if (!jid.equals("")) {
                chatConvMO.setJId(jid);
            } else {
                chatConvMO.setJId(groupID);
            }
            if (VariableConstants.conversationTypeChat == chatConvMO.getConversationType()) {
                chatConvMO.setGroupName(VariableConstants.SINGLE_CHAT_IDENTIFIER);
                chatConvMO.setSecoundryJid(Utils.genrateSecoundryId(groupID));
            } else {
                // FIXME
                chatConvMO.setSecoundryJid(Utils.genrateSecoundryIdFromContact(otherUserId));
            }

            // chatConvMO.generatePersistenceKey(userMO.getUserName());
            chatConvMO.generatePersistenceKey(userMO.getId() + "");
            chatConvMO.setPhoneNumber("");
            chatConvMO.setNickName(nickName);


            if (isProcessPacket) {
                chatConvMO.setCreatedTimeUTC(System.currentTimeMillis());
                chatConvMO.setReadStatus(false);
            } else {
                chatConvMO.setCreatedTimeUTC(0);
                chatConvMO.setReadStatus(true);
            }
            if (conversations == null) {
                conversations = new ConcurrentHashMap<>();
            }
            // Since its a new chat conversation, we create a new string for the chat message key
            chatConvMO.setChatMessageKeyList(new Vector<String>());


            if (nickName != null && !"".equals(nickName)) {
                chatConvMO.setNickName(nickName);
            } else {
                String fisikeUserName = Utils.getFisikeUserName(otherUserId, settingMO.getFisikeServerIp());
                loadContactDetails(fisikeUserName, null);
                ProfilePicTaskManager.getInstance().addTask(new LoadContactsProfilePic(this, fisikeUserName));
                ProfilePicTaskManager.getInstance().startTask();
            }
            chatConvMO.setReadStatus(false);
            chatConversationDBAdapter.addContactMoToConversation(chatConvMO, ChatContactMO.getPersistenceKey(TextPattern.getUserId(otherUserId)), userMO.getId() + "");

            try {
                ChatConversationMO tempChatConvetsationMo = chatConversationDBAdapter.fetchChatConversationMO(chatConvMO.getPersistenceKey() + "");
                if (tempChatConvetsationMo != null) {
                    //TODO check for insert phone number
                    tempChatConvetsationMo.setNickName(nickName);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(ChatConversationDBAdapter.getNICKNAME(), nickName);
                    chatConversationDBAdapter.updateChatConversation(tempChatConvetsationMo.getPersistenceKey(), contentValues);
                } else {
                    chatConversationDBAdapter.insertChatConversationMo(chatConvMO);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            conversations.put(chatConvMO.getPersistenceKey() + "", chatConvMO);
            if (isProcessPacket) {
                updateRoster(otherUserId);
            }
        } else {
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO converstion not found");
        }
        return chatConvMO;
    }


    public void updateChatContactMoKeysInChatConversationCacheObject(ArrayList<String> convsMoKeys, String convMOPersistenceKey) {

        if (conversations != null && conversations.containsKey(convMOPersistenceKey)) {
            // There is already a chat conversation that exists in memory
            ChatConversationMO chatConvMO = conversations.get(convMOPersistenceKey);

            chatConvMO.setChatContactMoKeys(convsMoKeys);

            conversations.put(chatConvMO.getPersistenceKey() + "", chatConvMO);
        }
    }

    public void updateLocalConversationMo(HandlePresenceStanza result) {
        if (result != null && result.isAddMember()) {
            if (null == conversations) {
                conversations = new ConcurrentHashMap<>();
            }
            ChatConversationMO chatConversationMO = result.getChatConversationMO();
            String emailContacts = result.getEmailContacts();
            conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
            notifyGroupConversationsListChange();
            if (!emailContacts.equals("")) {
                loadContactDetails(emailContacts, null);
            }
        } else if (result != null && result.isRemoveMember()) {
            ChatConversationMO chatConversationMO = result.getChatConversationMO();
            conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
            notifyGroupConversationsListChange();
            notifyConversationsListChange(chatConversationMO);
        }
    }


    public void getGroupChatUsersDetails(String userId) {
        loadContactDetails(userId, null);

    }

    /**
     * This method gets the conversation
     *
     * @param otherUserId
     * @param nickName
     * @param isProcessPacket
     * @return
     */
    public synchronized ChatConversationMO getGroupConversationMO(String jid, String otherUserId, String nickName, boolean isProcessPacket,
                                                                  boolean isAddContactScreen) {
        // otherUserId = Utils.extractGroupId(otherUserId, settingMO.getFisikeServerIp());

        String convMOPersistenceKey = ChatConversationMO.generatePersistenceKey(jid, userMO.getId() + "") + "";
        ChatConversationMO chatConvMO = null;
        if (conversations == null) {
            getConversationsList();
        }
        if (conversations.containsKey(convMOPersistenceKey)) {
            // There is already a chat conversation that exists in memory
            chatConvMO = conversations.get(convMOPersistenceKey);
            // Increment Unread count
            if (senderId != null) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO : sender user id : " + senderId);
            }
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO : User Name : " + chatConvMO.getJId() + " PacketProcessd : "
                    + isProcessPacket + " Background : " + Utils.isAppbackground(getApplicationContext()) + " last msg : " + chatConvMO.getLastMsg());
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO thread Id : " + Thread.currentThread().getId() + " Thread Name : "
                    + Thread.currentThread().getName());
            if (senderId == null || (isProcessPacket && !senderId.equals(chatConvMO.getJId())) || Utils.isAppbackground(getApplicationContext())) {
                AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO count : " + chatConvMO.getUnReadCount());
                chatConvMO.setReadStatus(false);
            }
        } else {
            AppLog.d("Fisike", "com.mphrx.fisike.services.getConversationMO converstion not found");
        }
        return chatConvMO;
    }

    /**
     * This method is called to update the roster when a new conversation is added
     *
     * @param otherUserId
     */
    public void updateRoster(String otherUserId) {
        // If connection is not available
        // if (connection == null || !connection.isConnected() || !connection.isAuthenticated()) {
        // return;
        // }
        // // Section to update the Roster on the XMPP Server
        // Roster roster = connection.getRoster();
        // Collection<RosterEntry> entries = roster.getEntries();
        // roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
        //
        // // Checking if the user already exists in the roster
        // for (RosterEntry rosterEntry : entries) {
        // if (rosterEntry.getUser().equals(otherUserId)) {
        // return;
        // }
        // }
        // // Update the roster
        // try {
        // roster.createEntry(otherUserId, "", null);
        // } catch (XMPPException e) {
        // e.printStackTrace();
        // }

    }

    /**
     * checks if the user is already added if not creates a group and adds the user to roster
     *
     * @param groupID
     */
    public void updateGroupRoster(String receiverUserId, List<ChatContactMO> invite, String groupID, ChatConversationMO chatConversationMo,
                                  ChatMessageMO message) {
        // db call to check if added to roster
        // post implemention of fetchroster call api will implement this
        firstTimeGroupchatConversationMO = chatConversationMo;
        firstTimeGroupMessage = message;
        stopProgressBarBroadcast = false;
        createNewGroup(VariableConstants.SINGLE_CHAT_IDENTIFIER, invite, null, false, groupID, true);
    }

    /**
     * This method stores a new message into the persistence layer -- it also updates the information in the chat conversationMO
     *
     * @param chatMsgMO
     * @param chatConvMO
     */
    /**
     * This method stores a new message into the persistence layer -- it also updates the information in the chat conversationMO
     *
     * @param chatMsgMO
     * @param chatConvMO
     */
    public void storeChat(ChatMessageMO chatMsgMO, final ChatConversationMO chatConvMO, boolean isToShowNotification, boolean isRunningFromThread) {
        if (null != chatMsgMO && null != chatConvMO) {
            // synchronized (MessengerService.class) {
            String msgPk = chatMsgMO.getPersistenceKey() + "";
            Vector<String> chatMsgKeyList = chatConvMO.getChatMessageKeyList();
            if (null == chatMsgKeyList) {
                chatMsgKeyList = new Vector<String>();
            }
            if (!chatMsgKeyList.contains(msgPk)) {
                chatMsgKeyList.add(msgPk);
            }
            chatConvMO.setChatMessageKeyList(chatMsgKeyList);

            // Also add link to conversation to the messageMO
            chatMsgMO.setConvPersistenceKey(chatConvMO.getPersistenceKey());
            // }
        }

        saveAndNotifyConversation(chatConvMO);

        //   updateChatConversation(chatConvMO,isRunningFromThread,conte);
        if (isRunningFromThread) {
            isToShowNotification = (isToShowNotification && (Utils.isAppbackground(getApplicationContext()) || (null == senderId || (""
                    .equals(senderId))))) ? true : false;
            runStoreChatTask(chatMsgMO, chatConvMO, isToShowNotification, null);
        } else {
            new StoreChatMessage(new ApiResponseCallback<Boolean>() {
                @Override
                public void onSuccessRespone(Boolean result) {
                    if (result && (Utils.isAppbackground(getApplicationContext()) || (null == senderId || ("".equals(senderId))))) {
                        Utils.generateNotification(getApplicationContext(), VariableConstants.PUSH_NOTIFICATION_MESSAGE, false, chatConvMO);
                    }
                }

                @Override
                public void onFailedResponse(Exception exception) {

                }
            }, getApplicationContext(), false, chatConvMO, chatMsgMO, isToShowNotification).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    public void storeChatMessage(ChatMessageMO chatMsgMO, final ChatConversationMO chatConvMO, boolean isToShowNotification, boolean isRunningFromThread, ContentValues vaues) {
        if (null != chatMsgMO && null != chatConvMO) {
            // synchronized (MessengerService.class) {
            String msgPk = chatMsgMO.getPersistenceKey() + "";
            Vector<String> chatMsgKeyList = chatConvMO.getChatMessageKeyList();
            if (null == chatMsgKeyList) {
                chatMsgKeyList = new Vector<String>();
            }
            if (!chatMsgKeyList.contains(msgPk)) {
                chatMsgKeyList.add(msgPk);
            }
            chatConvMO.setChatMessageKeyList(chatMsgKeyList);

            // Also add link to conversation to the messageMO
            chatMsgMO.setConvPersistenceKey(chatConvMO.getPersistenceKey());
            // }
        }

        saveAndNotifyConversation(chatConvMO);

        //   updateChatConversation(chatConvMO,isRunningFromThread,conte);
        if (isRunningFromThread) {
            isToShowNotification = (isToShowNotification && (Utils.isAppbackground(getApplicationContext()) || (null == senderId || (""
                    .equals(senderId))))) ? true : false;
            runStoreChatTask(chatMsgMO, chatConvMO, isToShowNotification, vaues);
        } else {
            new StoreChatMessage(new ApiResponseCallback<Boolean>() {
                @Override
                public void onSuccessRespone(Boolean result) {
                    if (result && (Utils.isAppbackground(getApplicationContext()) || (null == senderId || ("".equals(senderId))))) {
                        Utils.generateNotification(getApplicationContext(), VariableConstants.PUSH_NOTIFICATION_MESSAGE, false, chatConvMO);
                    }
                }

                @Override
                public void onFailedResponse(Exception exception) {

                }
            }, getApplicationContext(), false, chatConvMO, chatMsgMO, isToShowNotification, vaues).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    public void updateChatConversation(final ChatConversationMO chatConvMO, boolean isRunningFromThread, final ContentValues contentValues) {
//        saveAndNotifyConversation(chatConvMO);
        if (isRunningFromThread) {
            if (contentValues != null) {
                ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).updateChatConversation(chatConvMO.getPersistenceKey(), contentValues);
            } else {
                ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).insertChatConversationMo(chatConvMO);
            }

        } else {
            new StoreChatConversation(new ApiResponseCallback<Boolean>() {
                @Override
                public void onSuccessRespone(Boolean result) {

                }

                @Override
                public void onFailedResponse(Exception exception) {

                }
            }, getApplicationContext(), chatConvMO, contentValues).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    private void saveAndNotifyConversation(ChatConversationMO chatConvMO) {
        // Notify the conversation list has changed
        if (null != chatConvMO) {
            String convMOPersistenceKey = chatConvMO.getPersistenceKey() + "";
            // Update the conversations object in memory
            if (chatConvMO.getConversationType() == VariableConstants.conversationTypeGroup) {
                conversations.put(convMOPersistenceKey, chatConvMO);
                notifyGroupConversationsListChange();
            } else if (chatConvMO.getConversationType() == VariableConstants.conversationTypeChat) {
                if (conversations.containsKey(convMOPersistenceKey)) {
                    ChatConversationMO chatConversationMO = conversations.get(convMOPersistenceKey);
                    if (chatConvMO.getNickName() == null || chatConvMO.getNickName().equals("")) {
                        chatConvMO.setNickName(chatConversationMO.getNickName());
                    }
                }
                conversations.put(convMOPersistenceKey, chatConvMO);
                notifyConversationsListChange();
            }
        }
    }


    /**
     * This method stores the chat message and conversation synchronously
     *
     * @param chatMsgMO
     */
    private boolean runStoreChatTask(ChatMessageMO chatMsgMO, ContentValues contentValues) {
//        boolean msgSaved = true;
//        boolean convSaved = true;
        if (null != chatMsgMO) {
//            msgSaved = DBAdapter.getInstance(getApplicationContext()).createOrUpdateChatMessageMO(chatMsgMO, false);
            if (contentValues != null)
                return ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).updateChatMessageMo(chatMsgMO.getPersistenceKey(), contentValues);

            else
                return ChatMessageDBAdapter.getInstance(MyApplication.getAppContext()).insert(chatMsgMO);
        }
//        if (null != chatConversationMO) {
//            convSaved = ChatConversationDBAdapter.getInstance(getApplicationContext()).createOrUpdateChatConversationMO(chatConversationMO,
//                    chatConversationMO.getReadStatus());
//        }
//
//        if (msgSaved && convSaved) {
//            return true;
//        } else {
//            return false;
//        }
        return false;
    }

    /**
     * This method stores the chat message and conversation synchronously
     *
     * @param chatMsgMO
     * @param chatConversationMO
     * @param isToShowNotification
     */
    private boolean runStoreChatTask(ChatMessageMO chatMsgMO, ChatConversationMO chatConversationMO, boolean isToShowNotification, ContentValues contentValues) {
        boolean isStoreChat = runStoreChatTask(chatMsgMO, contentValues);
        if (isStoreChat && isToShowNotification) {
            Utils.generateNotification(getApplicationContext(), VariableConstants.PUSH_NOTIFICATION_MESSAGE, false, chatConversationMO);
        }
        return isStoreChat;
    }

    /**
     * This is the db action that saves the chat conversation MO
     *
     * @param chatConversationMO
     * @return boolean status
     *//*
    private boolean saveChatConversationMO(ChatConversationMO chatConversationMO) {
        try {
            chatConversationDBAdapter.createOrUpdateChatConversationMO(chatConversationMO, chatConversationMO.getReadStatus());
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/

    /**
     * This is the db action that saves the QuestionActivityMO
     *
     * @param questionActivityMO
     * @param queArrayListMO
     * @return boolean status
     */
    public boolean saveQuestion(QuestionActivityMO questionActivityMO, ArrayList<QuestionMO> queArrayListMO) {
        try {
            mHelper.createOrUpdateQuestionActivityMO(questionActivityMO);
            for (int i = 0; null != queArrayListMO && i < queArrayListMO.size(); i++) {
                QuestionMO questionMO = queArrayListMO.get(i);
                saveQuestionMO(questionMO);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This is the db action that saves the QuestionActivityMO
     *
     * @param questionMO
     * @return boolean status
     */
    public boolean saveQuestionMO(QuestionMO questionMO) {
        try {
            mHelper.createOrUpdateQuestionMO(questionMO);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This is the db action that return QuestionActivityMO
     *
     * @param persistenceKey
     * @return
     */
    public QuestionActivityMO getQuestionActivityMO(String persistenceKey) {
        try {
            return mHelper.fetchQuestionActivityMO(persistenceKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This is the db action that return QuestionMO
     *
     * @param persistenceKey
     * @return
     */
    public QuestionMO getQuestionMO(String persistenceKey) {
        try {
            return mHelper.fetchQuestionMO(persistenceKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This method sets the senderId for the current user
     *
     * @param senderId
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (!Utils.isTimerRunning(getApplicationContext())) {
            initiateTimers();
        } else if (ConfigManager.getInstance().getConfig().getPinLockTime() != 0) {
            startPinScreenTimer();
        }
        return mBinder;
    }

    /**
     * Update the Real name for the user email ids
     *
     * @param jsonString
     * @param emailPresence
     */
    public void updateContactRealName(String jsonString, HashMap<String, HashPresence> emailPresence) {
        try {
            if (jsonString != null && !("".equals(jsonString))) {
                refreshDB(jsonString, emailPresence);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppLog.d("Fisike", "Now updating the conv list screen");
        notifyConversationsListChange();
    }

    /**
     * Update the profilePic
     *
     * @param jsonString
     */
    public void updateContactProfilePic(String jsonString) {
        try {
            if (jsonString != null && !("".equals(jsonString)) && !(jsonString.contains("not found"))) {
                refreshDBProfilePic(jsonString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppLog.d("Fisike", "Now updating the conv list screen");
        notifyConversationsListChange();
    }

    public void updateContactProfilePic(byte[] byteArray, String id) {
        if ((null != id || !"".equals(id)) && (null != byteArray)) {
            updateChatContactProfilePic(byteArray, id);
        }
        notifyConversationsListChange();
    }

    /**
     * Update the profilePic
     *
     * @param jsonString
     * @param emailString
     */
    public void updateGroupContactProfilePic(String jsonString, String emailString) {
        try {
            if (jsonString != null && !("".equals(jsonString))) {
                refreshGroupDBProfilePic(jsonString, emailString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppLog.d("Fisike", "Now updating the conv list screen");
        notifyGroupConversationsListChange();
    }

    /**
     * Send and update the message to be send again for pending messages and messages of single tick for verify it reached to the server TODO not used
     * now its previous
     *
     * @param chatMessageMO
     */
    public void sendMessage(ChatMessageMO chatMessageMO) {
        ChatConversationMO chatConvMO;
        try {
            chatConvMO = chatConversationDBAdapter.fetchChatConversationMO(chatMessageMO.getConvPersistenceKey() + "");
            sendMessage(chatMessageMO.getMessageText(), chatMessageMO.getAttachmentID(), chatMessageMO.getAttachmentType(),
                    chatMessageMO.getLasUpdatedTimeUTC(), chatConvMO, chatMessageMO, true, false);
        } catch (Exception e) {
        }
    }

    /**
     * Set attachmentMo object
     *
     * @param chatConversationMO
     * @param chatMsgMO
     * @param attachmentStatus
     * @param isUplodedSucessfully
     * @param attachmentId
     * @param recoredSyncStatus
     * @return
     */
    public AttachmentMo saveAttachmentUploaded(ChatConversationMO chatConversationMO, ChatMessageMO chatMsgMO, String attachmentStatus,
                                               boolean isUplodedSucessfully, String attachmentId, int recoredSyncStatus) {

        boolean isUpdateAttachmentMo = false;
        AttachmentMo attachmentMo;
        try {
            attachmentMo = AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).fetchAttachmentMO_attachmentPath(chatMsgMO.getAttachmentPath() + "");
            if (attachmentMo != null)
                isUpdateAttachmentMo = true;

            attachmentMo.setStatus(attachmentStatus);
            if (isUplodedSucessfully) {
                attachmentMo.setAttachmentId(attachmentId);
                attachmentMo.generatePersistenceKey(chatConversationMO.getJId());
            }
            attachmentMo.setImageRecordSyncStatus(recoredSyncStatus);
            if (isUpdateAttachmentMo) {
                ContentValues values = new ContentValues();
                values.put(AttchmentDBAdapter.getSTATUS(), attachmentMo.getStatus());
                values.put(AttchmentDBAdapter.getPERSISTENCEKEY(), attachmentMo.getPersistenceKey());
                values.put(AttchmentDBAdapter.getATTACHMENTID(), attachmentMo.getAttachmentId());
                values.put(AttchmentDBAdapter.getIMAGERECORDSYNCSTATUS(), attachmentMo.getImageRecordSyncStatus());
                updateAttachmentMo(attachmentMo, values);
                AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).updateAttachmentMo_AttachmentPatg(attachmentMo.getAttachmentPath(), values);
            }

/*            storeAttachmentMo(attachmentMo);*/
            else
                insertAttachmentMo(attachmentMo);

            return attachmentMo;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Set attachment path
     *
     * @param chatMsgMO
     * @param attachmentPath
     * @param recoredSyncStatus
     * @param imageThumb
     * @return
     */
    public AttachmentMo saveAttachmentPath(ChatMessageMO chatMsgMO, String attachmentPath, String status, byte[] imageThumb, int recoredSyncStatus,
                                           String imageThumbnailPath) {

        boolean isUpdateAttachment = false;
        AttachmentMo attachmentMo;
        try {
            attachmentMo = AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).fetchAttachmentMO_ChatMessageMo(chatMsgMO.getPersistenceKey() + "");
            if (attachmentMo != null)
                isUpdateAttachment = true;

            attachmentMo.setAttachmentPath(attachmentPath);
            attachmentMo.setStatus(status);
            attachmentMo.setImageThumbnail(imageThumb);
            attachmentMo.setImageRecordSyncStatus(recoredSyncStatus);
            attachmentMo.setImageThumbnailPath(imageThumbnailPath);

            if (isUpdateAttachment) {
                ContentValues values = new ContentValues();
                values.put(AttchmentDBAdapter.getATTACHMENTPATH(), attachmentMo.getAttachmentPath());
                values.put(AttchmentDBAdapter.getSTATUS(), attachmentMo.getStatus());
                values.put(AttchmentDBAdapter.getIMAGETHUMBNAIL(), attachmentMo.getImageThumbnail());
                values.put(AttchmentDBAdapter.getIMAGERECORDSYNCSTATUS(), attachmentMo.getImageRecordSyncStatus());
                values.put(AttchmentDBAdapter.getIMAGETHUMBNAILPATH(), attachmentMo.getImageThumbnailPath());

                updateAttachmentMo(attachmentMo, values);

            } else
                insertAttachmentMo(attachmentMo);
            // storeAttachmentMo(attachmentMo);
            return attachmentMo;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Set attachment size
     *
     * @param chatMsgMO
     * @param fileSize
     * @return boolean
     */
    public boolean isSaveAttachmentSize(ChatMessageMO chatMsgMO, long fileSize) {

        AttachmentMo attachmentMo;
        AttchmentDBAdapter attchmentDBAdapter = AttchmentDBAdapter.getInstance(MyApplication.getAppContext());
        try {
            attachmentMo = attchmentDBAdapter.fetchAttachmentMO_ChatMessageMo(chatMsgMO.getPersistenceKey() + "");
            long size = attachmentMo.getFileSize();
            if (size == 0 || size != fileSize) {
                attachmentMo.setFileSize(fileSize);
                ContentValues values = new ContentValues();
                values.put(AttchmentDBAdapter.getFILESIZE(), fileSize);
                attchmentDBAdapter.updateAttachmentMo(attachmentMo.getPersistenceKey(), values);
                //storeAttachmentMo(attachmentMo);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }


    public boolean insertAttachmentMo(AttachmentMo attachmentMo) {
        try {
            AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).insert(attachmentMo);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateAttachmentMo(AttachmentMo attachmentMo, ContentValues values) {
        try {
            AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).updateAttachmentMo(attachmentMo.getPersistenceKey(), values);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Get the list of groups for the user
     */
    public void getGroupList() {

        userMO = SettingManager.getInstance().getUserMO();
        //unco
        if (null != userMO && null != userMO.getUserType() && !VariableConstants.PATIENT.equalsIgnoreCase(userMO.getUserType())
                && null != connection && connection.isConnected() && connection.isAuthenticated()) {
            try {
                GroupRosterPacket groupRosterPacket = new GroupRosterPacket();
                groupRosterPacket.setTo(VariableConstants.groupConferenceName + settingMO.getFisikeServerIp());
                groupRosterPacket.setFrom(Utils.cleanXMPPUserName(connection.getUser()));
                connection.sendPacket(groupRosterPacket);
            } catch (Exception e) {
            }
        }
    }

    /**
     * Get the list of group members
     */
    public void getGroupMembers(String jid) {
        //textint der
        if (null != connection && connection.isConnected() && connection.isAuthenticated()) {
            try {

                GroupRosterPacket groupRosterPacket = new GroupRosterPacket();
                groupRosterPacket.setTo(jid);
                // @conference.dev1
                groupRosterPacket.setFrom(Utils.cleanXMPPUserName(connection.getUser()));
                connection.sendPacket(groupRosterPacket);
            } catch (Exception e) {
            }

        }
    }

    // from - userjid to groupId@conference.dev1
    public void getGroupName(String groupId) {
        if (null != connection && connection.isConnected() && connection.isAuthenticated()) {
            try {
                GroupNamePacket groupNamePacket = new GroupNamePacket();
                groupNamePacket.setTo(groupId);
                groupNamePacket.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
                connection.sendPacket(groupNamePacket);
            } catch (Exception e) {
            }
        }
    }

    // groupId - 'groupId@conference...'
    public void changeGroupName(String changedName, String groupId) {
        Message changeGroupNameMessage = new Message();
        changeGroupNameMessage.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        changeGroupNameMessage.setTo(groupId);
        changeGroupNameMessage.setType(Message.Type.groupchat);
        changeGroupNameMessage.setSubject(changedName);
        changeGroupNameMessage.toXML();
        new SendPacketAsyncTask(connection, changeGroupNameMessage).execute();
    }

    // groupId - 'groupId@conference...'
    public void changeGroupImage(String groupId) {
        GroupImageChangePacket groupImageChange = new GroupImageChangePacket();
        groupImageChange.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        groupImageChange.setTo(groupId);
        groupImageChange.setType(Message.Type.groupchat);
        groupImageChange.toXML();
        new SendPacketAsyncTask(connection, groupImageChange).execute();
    }

    // groupId - 'groupId@conference...'
    public void exitGroup(String groupId) {
        Presence exitGroupPresence = new Presence(Presence.Type.unavailable);
        String to = groupId + "/" + userMO.getId() + "@" + settingMO.getFisikeServerIp();
        exitGroupPresence.setTo(to);
        exitGroupPresence.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        exitGroupPresence.toXML();
        connection.sendPacket(exitGroupPresence);
    }

    public void removeMemberFromGroup(MUCAdmin packet) {
        new SendPacketAsyncTask(connection, packet).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /*
     * <iq type='set' id='32075' from='abc@dev1' to='dev1' > <query xmlns='jabber:iq:private' action='delete'> <item from='spool'>messageID1</item>
    * <item from='spool'>messageID2</item> </query> </iq>
    */
    public synchronized void createSpoolPacket(List<String> packetIDList) {
        SpoolPacketIQ spoolPacket = new SpoolPacketIQ();
        spoolPacket.setType(IQ.Type.SET);
        spoolPacket.setTo(settingMO.getFisikeServerIp());
        spoolPacket.setFrom(userMO.getId() + "@" + settingMO.getFisikeServerIp());
        spoolPacket.setPacketIDList(packetIDList);
        spoolPacket.toXML();
        new SendPacketAsyncTask(connection, spoolPacket).execute();
    }

    public void getRoaster() {
        final SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        new RoasterBG(new ApiResponseCallback<RoasterEmailObject>() {

            @Override
            public void onSuccessRespone(RoasterEmailObject result) {

                HashMap<String, ChatConversationMO> chatConversationMO = result.getChatConversationMO();
                if (chatConversationMO != null) {
                    conversations.putAll(chatConversationMO);
                    Editor store = preferences.edit();
                    store.putBoolean(VariableConstants.IS_GROUP_LIST_FETCHED, true);
                    store.commit();

                }
                notifyConversationsListChange();
                if (result.getRoasterContacts().equals("")) {
                    SharedPref.setProgressForChat(false);
                    notifyNoConversationBroadCast();
                } else {
                    new LoadContactsRealName(MessengerService.this, result.getRoasterContacts(), true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

                ProfilePicTaskManager profilePicTaskManager = ProfilePicTaskManager.getInstance();
                Collection<ChatConversationMO> values = chatConversationMO.values();
                Iterator<ChatConversationMO> iterator = values.iterator();
                while (iterator.hasNext()) {
                    ChatConversationMO profilePicChatConversationMO = iterator.next();

                    String fisikeUserName = Utils.cleanXMPPServer(profilePicChatConversationMO.getSecoundryJid());

                    LoadContactsProfilePic loadContactsProfilePic = new LoadContactsProfilePic(MessengerService.this, fisikeUserName);
                    profilePicTaskManager.addTask(loadContactsProfilePic);
                    profilePicTaskManager.startTask();
                }
            }

            @Override
            public void onFailedResponse(Exception exception) {
                if (connection != null && connection.isConnected() && connection.isAuthenticated() && !preferences.getBoolean(VariableConstants.IS_GROUP_LIST_FETCHED, false)) {
                    getRoaster();
                }
            }
        }, getApplicationContext(), false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * This is the Local Binder that is used for other activities to connect with this service
     *
     * @author varunanand
     */
    public class MessengerBinder extends Binder {
        public MessengerService getService() {
            return MessengerService.this;
        }
    }

    public void changeUpdateGroupSucessfull(ArrayList<ChatConversationMO> result) {
        if (conversations == null) {
            conversations = new ConcurrentHashMap<>();
        }
        for (int i = 0; i < result.size(); i++) {
            ChatConversationMO chatConversationMO = result.get(i);
            conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
            notifyConversationsListChange(chatConversationMO);
        }
        notifyGroupConversationsListChange();
    }

    public void changeStatus(int position, String statusMessage) {
        isToLogout = false;
        statusText = TextConstants.STATUS_AVAILABLE_TEXT;
        if (statusMessage == null) {
            statusMessage = "";
        }
        this.statusMessage = statusMessage;
        setPresenceStatus(position);
        Presence presence = null;
        switch (position) {
            case TextConstants.STATUS_AVAILABLE:
                statusText = TextConstants.STATUS_AVAILABLE_TEXT;
                presence = new Presence(Type.available);
                break;
            case TextConstants.STATUS_UNAVAILABLE:
                statusText = TextConstants.STATUS_UNAVAILABLE_TEXT;
                presence = new Presence(Type.unavailable);
                break;
            case TextConstants.STATUS_BUSY:
                statusText = TextConstants.STATUS_BUSY_TEXT;
                presence = new Presence(Type.busy);
                break;
            default:
                break;
        }
        if (Utils.isXMPPConnectionAvailable()
                && presence != null
                && (userMO.getStatus() == null || !userMO.getStatus().equalsIgnoreCase(statusText) || !userMO.getStatusMessage().equalsIgnoreCase(
                statusMessage))) {
            String fisikeServerIp = settingMO.getFisikeServerIp();
            // presence.setFrom(userMO.getUserName() + "@" + fisikeServerIp);
            presence.setFrom(userMO.getId() + "@" + fisikeServerIp);
            presence.setTo(fisikeServerIp);
            presence.setStatus(statusMessage);
            if (connection != null)
                connection.sendPacket(presence);
            statusPacketId = presence.getPacketID();
            startTimmerChangeStatus(TimeUnit.SECONDS.toMillis(15));
        } else {
            startTimmerChangeStatus(TimeUnit.SECONDS.toMillis(0));
        }
    }

    private void startTimmerChangeStatus(long delay) {
        if (logoutUserHandler == null) {
            logoutUserHandler = new Handler();
            logoutUserHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    changeStatusResponce(false);
                }
            }, delay);
        }
    }

    public void showDialog(Context activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
        dialog.setMessage(activity.getResources().getString(R.string.signing_out));
        dialog.setCancelable(false);
        dialog.show();
    }


    @Override
    public void connectionClosed() {
        // TODO Auto-generated method stub

    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Toast.makeText(getApplicationContext(), "Connection broken exception e :::::" + e.toString(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void reconnectingIn(int seconds) {
        // TODO Auto-generated method stub

    }

    @Override
    public void reconnectionSuccessful() {
        // TODO Auto-generated method stub

    }

    @Override
    public void reconnectionFailed(Exception e) {
        // TODO Auto-generated method stub

    }

    public void setChatConversation(ChatConversationMO chatConversationMO) {
        conversations.put(chatConversationMO.getPersistenceKey() + "", chatConversationMO);
        syncUserMOConversations();
    }

    public void sendMessageOnFirstTimeGroupCreated(ChatMessageMO firstTimeMessage) {
        sendMessage(firstTimeMessage.getMessageText(), null, null, 0, firstTimeGroupchatConversationMO, firstTimeMessage, false, false);

    }

    public void stopPinTimer() {
        if (pinScreenHandler != null) {
            pinScreenHandler.removeCallbacksAndMessages(null);
            pinScreenHandler = null;
        }
        stopForegroundTimer();
    }

    public void stopForegroundTimer() {
        if (foregroundPinScreenHandler != null) {
            foregroundPinScreenHandler.removeCallbacksAndMessages(null);
            foregroundPinScreenHandler = null;
        }
    }

    public void stopLogoutTimer() {
        if (logoutUserHandler != null) {
            logoutUserHandler.removeCallbacksAndMessages(null);
            logoutUserHandler = null;
        }
    }
}
