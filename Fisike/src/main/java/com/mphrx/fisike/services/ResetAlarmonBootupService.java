package com.mphrx.fisike.services;

import android.app.IntentService;
import android.content.Intent;

import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;

import java.util.ArrayList;

/**
 * Created by xmb2nc on 09-12-2015.
 */
public class ResetAlarmonBootupService extends IntentService {

    public ResetAlarmonBootupService() {
        super(ResetAlarmonBootupService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            ArrayList<PrescriptionModel> medicationPrescriptionModelsList = EncounterMedicationDBAdapter.getInstance(this).getMedicationPrescriptionsWhoseAlarmIsActive();

            for (int i = 0; medicationPrescriptionModelsList != null && i < medicationPrescriptionModelsList.size(); i++) {
                PrescriptionModel prescriptionModel = medicationPrescriptionModelsList.get(i);
                ArrayList<MedicineDoseFrequencyModel> arrayMedicineDoseFrequencyModels = prescriptionModel.getArrayDose();
                for (int j = 0; arrayMedicineDoseFrequencyModels != null && j < arrayMedicineDoseFrequencyModels.size(); j++) {
                    MedicineDoseFrequencyModel medicineDoseFrequencyModel = arrayMedicineDoseFrequencyModels.get(j);
                    int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;
                    Utils.startAlarmMedication(getApplicationContext(), medicineDoseFrequencyModel.getAlarmManagerUniqueKey(), prescriptionModel, interval, medicineDoseFrequencyModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ArrayList<VaccinationModel> vaccinationModelArrayList = VaccinationDBAdapter.getInstance(this).fetchAllVaccinationMO(true);

            for (int i = 0; vaccinationModelArrayList != null && i < vaccinationModelArrayList.size(); i++) {
                VaccinationModel vaccinationModel = vaccinationModelArrayList.get(i);
                if (!vaccinationModel.isVaccinationTaken()) {
                    VaccinationAlarm.setAlarm(vaccinationModel, this, false);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
