package com.mphrx.fisike;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mphrx.fisike.add_alternate_contact.AddAlternateContact;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.fragments.VitalsFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.persistence.VitalsConfigDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.record_screen.adapter.ResultListAdapter;
import com.mphrx.fisike.record_screen.view_holder.DiagnosticOrderViewHolder;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.VitalsConfigGson.VitalsConfigMO;
import com.mphrx.fisike.vital_submit.activity.BlockVitalsScreenActivity;
import com.mphrx.fisike.vital_submit.background.FetchVitalConfigRequest;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * This is the activity that is providing the list of open chats in Fisike
 *
 * @author kkhurana
 */
public class HomeActivity extends BaseActivity implements OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener, ResultListAdapter.DownloadListner {

    private static final int ADD_ALTERNATE_CONTACT = 101;
    private static final int DOWNLOAD_RESULT = 10001;
    VitalsConfigMO vitalsConfigMO;
    public static IconTextView bt_toolbar_right;

    android.support.v7.app.AlertDialog alertDialogAddRateUs, alertDialogAlternateContact;
    private boolean isStartService;
    private String orderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);
        try {
            vitalsConfigMO = VitalsConfigDBAdapter.getInstance(this).fetchVitalsConfigMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (SettingManager.getInstance().getUserMO() != null &&
                SettingManager.getInstance().getUserMO().getSalutation().equalsIgnoreCase(TextConstants.YES)) {
            checkVitalsConfigResponse();
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    public void checkVitalsConfigResponse() {
        if (BuildConfig.isVitalEnabled) {
            //fetching vitals config from server if any validation fails
            if (!SharedPref.isVitalConfigApiExecuted() || vitalsConfigMO == null || !(vitalsConfigMO.isToShowAddBPButton()
                    && vitalsConfigMO.isToShowAddGlucoseButton() && vitalsConfigMO.isToShowAddBMIButton())) {
                showProgressDialog();
                ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(this, getTransactionId()));
            }
        }

    }

    private void showDialogToAddAlternateContact() {

        SharedPref.setLastDisplayedAlternateContactDate(System.currentTimeMillis());

        if (alertDialogAlternateContact == null) {
            alertDialogAlternateContact = DialogUtils.returnAlertDialog(this, getString(R.string.alternate_contact_title), getString(R.string.alternate_contact_message), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        dialog.dismiss();
                        startActivityForResult(new Intent(HomeActivity.this, AddAlternateContact.class).putExtra(VariableConstants.START_INSIDE_ACTIVITY, true), ADD_ALTERNATE_CONTACT);

                    } else {
                        dialog.dismiss();
                    }
                    Utils.checkAlternateContactDetails(SettingManager.getInstance().getUserMO().getAlternateContact());
                }
            });


        }
        if (alertDialogAlternateContact != null && !alertDialogAlternateContact.isShowing())
            alertDialogAlternateContact.show();
    }

    private boolean showDialogToRateApp() {
        final SharedPreferences sharedPreferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        Resources res = getResources();

        if ((sharedPreferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0) >= 5)
                && sharedPreferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0) % 5 == 0
                && !sharedPreferences.getBoolean(VariableConstants.HAS_RATED_APP, false)) {

            if (alertDialogAddRateUs == null) {
                alertDialogAddRateUs = DialogUtils.showAlertDialogThreeButtons(HomeActivity.this, res.getString(R.string.rate_us_title, getString(R.string.app_name)), res.getString(R.string.rate_us_msg), res.getString(R.string.btn_lets_do_it), res.getString(R.string.btn_report_issue), res.getString(R.string.btn_later), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            dialog.dismiss();
                            String pckg = HomeActivity.this.getPackageName();
                            if (pckg.endsWith(".debug"))
                                pckg = pckg.replace(".debug", "");

                            Uri uri = Uri.parse("market://details?id=" + pckg);
                            Intent i = new Intent(Intent.ACTION_VIEW, uri);
                            // To count with Play market backstack, After pressing back button,
                            // to taken back to our application, we need to add following flags to intent.
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            try {
                                startActivity(i);
                            } catch (ActivityNotFoundException e) {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("http://play.google.com/store/apps/details?id=" + pckg)));
                            }

                            SharedPreferences sharedPreferences1 = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                            Editor editor = sharedPreferences1.edit();
                            editor.putBoolean(VariableConstants.HAS_RATED_APP, true);
                            editor.commit();

                        } else if (which == Dialog.BUTTON_NEGATIVE) {
                            dialog.dismiss();
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.parse(getResources().getString(R.string.mailto))); // only email apps should handle this
                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
                            intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(HomeActivity.this, userMO));
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                            }

                            int numberOfTimeApplicationOpened = sharedPreferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0);
                            Editor edit = sharedPreferences.edit();
                            if (!sharedPreferences.getBoolean(VariableConstants.HAS_RATED_APP, false)) {
                                edit.putInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, (numberOfTimeApplicationOpened + 1));
                                edit.commit();
                            }

                        } else {
                            dialog.dismiss();
                            int numberOfTimeApplicationOpened = sharedPreferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0);
                            Editor edit = sharedPreferences.edit();
                            if (!sharedPreferences.getBoolean(VariableConstants.HAS_RATED_APP, false)) {
                                edit.putInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, (numberOfTimeApplicationOpened + 1));
                                edit.commit();
                            }
                        }
                    }
                });
            }
            if (alertDialogAddRateUs != null && !alertDialogAddRateUs.isShowing())
                alertDialogAddRateUs.show();
            return true;
        } else {
            return false;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onMessengerServiceBind() {
        if (mService != null && !isStartService && !SharedPref.getIsAgreementsUpdated()) {
            mService.initiateTimers();
        } else if (mService != null && SharedPref.getIsAgreementsUpdated()) {
            mService.startGetProfileUpdatesTimer();
        }
        super.onMessengerServiceBind();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isStartService = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!showDialogToRateApp() && SharedPref.getIsToShowAlternateContactDialog())
            showDialogToAddAlternateContact();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == ADD_ALTERNATE_CONTACT) {
            SharedPref.setIsToShowAlternateContactDialog(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showDecriptedFile(byte[] decodFile, String attachment_type) {
        String extention = "";
        if (attachment_type.equals("application/msword")) {
            extention = ".doc";
        } else if (attachment_type.equals("application/pdf")) {
            extention = ".pdf";
        }
        if (extention.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
            return;
        }
        File file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + getString(R.string.app_name) + "/.temp", "fileName" + extention);
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(file);
            output.write(decodFile, 0, decodFile.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), attachment_type);
        if (intent.resolveActivity(this.getPackageManager()) != null) {
            PackageManager packageManager1 = getPackageManager();
            List<ResolveInfo> listGall = packageManager1.queryIntentActivities(intent, 0);
            int size = listGall.size();
            for (int i = 0; i < size; i++) {
                ResolveInfo res = listGall.get(i);
                if (res.activityInfo.packageName.equals("com.google.android.apps.docs.editors.docs") || res.activityInfo.packageName.equals("com.google.android.apps.docs")) {
                    intent.setPackage(res.activityInfo.packageName);
                    startActivity(intent);
                    return;
                }
            }
            startActivity(intent);
        } else {
            Toast.makeText(this, getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onVitalsConfigResponse(VitalsConfigResponse response) {
        if (response.getTransactionId() != getTransactionId())
            return;
        vitalsConfigMO = VitalsConfigMO.getInstance();
        dismissProgressDialog();

        //API failure
        if (!response.isSuccessful()) {

            MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                            GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                    GoogleAnalyticsConstants.VITALS_VALUE_FAILURE, GoogleAnalyticsConstants.VITALS_API_NAME);

            /*Launcher change*/
            if (getCurrentFragmentVisible() instanceof MyHealth) {
                MyHealth myHealth = (MyHealth) getCurrentFragmentVisible();
                Fragment fragment = myHealth.getMyHelthAdapter().getFragment(
                        Utils.getMyHealthTabNameBasedOnPosition(myHealth.mViewPager.getCurrentItem()));
                if (fragment instanceof VitalsFragment) {
                    if (response.getIsTolaunchContactAdminScreen())
                        ((VitalsFragment) fragment).showContactAdminScreen();
                    else
                        ((VitalsFragment) fragment).showTryAgainScreen();
                }
            }

        } else if (getCurrentFragmentVisible() instanceof MyHealth) {
            if (response.isValidJson()) // success API JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION_SUCCESS,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS, GoogleAnalyticsConstants.VITALS_API_NAME);
            else //success API but blank JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS_INCORRECT_JSON, GoogleAnalyticsConstants.VITALS_API_NAME);

            MyHealth myHealth = (MyHealth) getCurrentFragmentVisible();
            Fragment fragment = myHealth.getMyHelthAdapter().getFragment(
                    Utils.getMyHealthTabNameBasedOnPosition(myHealth.mViewPager.getCurrentItem()));
            if (fragment instanceof VitalsFragment) {
                if (vitalsConfigMO.isValidVitalsFetched())
                    ((VitalsFragment) fragment).getVitalConfig();
                else {
                    if (response.getIsTolaunchContactAdminScreen())
                        ((VitalsFragment) fragment).showContactAdminScreen();
                    else
                        ((VitalsFragment) fragment).showTryAgainScreen();
                }

            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SharedPref.LANGUAGE_SELECTED)) {
            recreate();
        }
    }

    @Override
    public void itemClicked(String orderId, IconTextView imgOverFlow) {
        showPopupMenu(imgOverFlow, R.menu.menu_file_download, orderId);
    }

    public void showPopupMenu(View view, final int menuId, final String orderId) {
        this.orderId = orderId;
        final android.support.v7.widget.PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.report_download:
                        if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " + getString(R.string.permission_storage), DOWNLOAD_RESULT))
                            fileDownload(orderId);
                        break;
                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void fileDownload(String orderId) {
        String payload = "{\"constraints\":{\"diagnosticOrderId\":\"" + orderId + "\"}}";
        if (Utils.isNetworkAvailable(this)) {
            new DownloadRecordFileRequest(this, 0, VariableConstants.MIME_TYPE_FILE, MphRxUrl.getReportFile(), payload).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPermissionNotGranted() {
        super.onPermissionNotGranted();
    }

    @Override
    public void onPermissionGranted(String permission) {
        fileDownload(orderId);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //   super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case DOWNLOAD_RESULT:
                    onPermissionGranted(permissions[0]);
                    break;
                case UploadsFragment.CAMERA_INTENT:
                    MyHealth myHealth = (MyHealth) getCurrentFragmentVisible();
                    Fragment fragment = myHealth.getMyHelthAdapter().getFragment(
                            Utils.getMyHealthTabNameBasedOnPosition(myHealth.mViewPager.getCurrentItem()));
                    ((UploadsFragment) fragment).setOpenCamera();
                    break;

                case UploadsFragment.RESULT_GALLERY:
                    MyHealth myHealth1 = (MyHealth) getCurrentFragmentVisible();
                    Fragment fragment1 = myHealth1.getMyHelthAdapter().getFragment(
                            Utils.getMyHealthTabNameBasedOnPosition(myHealth1.mViewPager.getCurrentItem()));
                    ((UploadsFragment) fragment1).launchGallery();
                    break;

                case TextConstants.PERMISSION_STORAGE_CODE:
                    MyHealth myHealth2 = (MyHealth) getCurrentFragmentVisible();
                    myHealth2.openCamera();
                    break;

            }
        } else
            onPermissionNotGranted();
    }
}