package com.mphrx.fisike;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.mphrx.fisike.background.DecriptFile;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ExifUtils;

import java.io.File;

public class VideoViewActivity extends BaseActivity {

    private String attachmentPath;
    private ImageView imgFile;
    private String attachment_type;
    private boolean isAttachmentPreviewBeforSend;
    private boolean isCapturedImage;
    private VideoView video_player_view;
    private MediaController media_Controller;
    private FrameLayout video_frame;
    private File saveToInternalSorage;
    private Bitmap attachmentBitmap;
    private Toolbar toolBarVideoview;

    public static int getThumbMiniSize() {
        return MyApplication.context.getResources().getDimensionPixelSize(R.dimen.attachment_icon_size);
    }

    public static int getThumbSize() {
        return MyApplication.context.getResources().getDimensionPixelSize(R.dimen.attachment_thumb_size);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        Bundle extras = getIntent().getExtras();
        attachmentPath = extras.getString(VariableConstants.VIDEO_URI);

        attachment_type = extras.getString(VariableConstants.ATTACHMENT_TYPE);
        isAttachmentPreviewBeforSend = extras.getBoolean(VariableConstants.IS_ATTACHMENT_PREVIEW_BEFORE_SEND);

        imgFile = (ImageView) findViewById(R.id.videoPlay);

        isCapturedImage = extras.getBoolean(VariableConstants.IS_CAPTURED_IMAGE);

        if (isCapturedImage) {
            attachmentBitmap = MyApplication.getTempImagePreview();
        }

        toolBarVideoview = (Toolbar) findViewById(R.id.toolbar_video_view);
        setSupportActionBar(toolBarVideoview);
        toolBarVideoview.setNavigationIcon(R.drawable.ic_back_w_shadow);

        if (attachment_type.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)) {
            getSupportActionBar().setTitle(getResources().getString(R.string.video_select));
        } else {
            getSupportActionBar().setTitle(getResources().getString(R.string.image_select));
        }

        video_player_view = (VideoView) findViewById(R.id.video_player_view);
        video_frame = (FrameLayout) findViewById(R.id.video_frame);

        findViewById(R.id.btnPlay).setVisibility(View.GONE);

        if (!isAttachmentPreviewBeforSend) {
            findViewById(R.id.layoutSendAttachment).setVisibility(View.GONE);
        }

        System.gc();
        if (attachmentBitmap != null && VariableConstants.ATTACHMENT_IMAGE.equalsIgnoreCase(attachment_type)) {
            imgFile.setImageBitmap(attachmentBitmap);
            Bitmap thumbnail = ExifUtils.extractThumbnail(attachmentBitmap);
            Bitmap icon = ExifUtils.extractThumbnailMini(thumbnail);
            MyApplication.setImageThumb(icon);
            MyApplication.setPreviewBitmap(thumbnail);
        } else {
            decryptFile(attachmentPath, attachment_type);
        }
    }

    private void decryptFile(String path, String attachmentType) {
        DecriptFile decriptFile = new DecriptFile(this, attachmentPath, attachment_type);
        if (Build.VERSION.SDK_INT >= 11) {
            decriptFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            decriptFile.execute();
        }
    }

    public void postDecriptFile(byte[] bitmapBytes) {
        if (null == bitmapBytes) {
            return;
        }
        Bitmap imgPreview = null;
        Bitmap imageThumb = null;
        Bitmap imageThumbMini = null;
        if (VariableConstants.ATTACHMENT_IMAGE.equalsIgnoreCase(attachment_type)) {
            imgPreview = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
            imageThumb = ExifUtils.extractThumbnail(imgPreview);
            imageThumbMini = ExifUtils.extractThumbnailMini(imageThumb);
        } else {
//			saveToInternalSorage = Utils.saveToInternalSorage(this, Utils.MEDIA_TYPE_VIDEO, "playTempFile.mp4");
            saveToInternalSorage = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + getString(R.string.app_name) + "/.temp", "playTempFile.mp4");
            Utils.copyFile1(this, bitmapBytes, saveToInternalSorage);
            imgPreview = ThumbnailUtils.createVideoThumbnail(saveToInternalSorage.getAbsolutePath(), MediaStore.Images.Thumbnails.MINI_KIND);
            imageThumb = ExifUtils.extractThumbnail(imgPreview);
            imageThumbMini = ExifUtils.extractThumbnailMini(imageThumb);
            findViewById(R.id.btnPlay).setVisibility(View.VISIBLE);
        }
        if (null == imgPreview) {
            imgFile.setImageResource(R.drawable.preview_not_available);
            imgFile.setBackgroundColor(getResources().getColor(R.color.bg_play_color));
            imgFile.getLayoutParams().height = (int) Utils.convertDpToPixel(203, this);
            imgFile.getLayoutParams().width = (int) Utils.convertDpToPixel(340, this);
        } else {
            imgFile.setImageBitmap(imgPreview);
        }
        MyApplication.setImageThumb(imageThumbMini);
        MyApplication.setPreviewBitmap(imageThumb);

        System.gc();
    }

    public void getInit(File saveToInternalSorage) {
        video_frame.setVisibility(View.VISIBLE);
        imgFile.setVisibility(View.GONE);
        media_Controller = new MediaController(this);
        video_player_view.setMediaController(media_Controller);
        video_player_view.setVideoPath(saveToInternalSorage.getAbsolutePath());
        video_player_view.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (null != saveToInternalSorage && null != video_player_view && !video_player_view.isPlaying()) {
            video_player_view.resume();
        }
    }

    @Override
    protected void onResume() {
        if (null != saveToInternalSorage && null != video_player_view && !video_player_view.isPlaying()) {
            video_player_view.resume();
        }
        super.onResume();
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnPlay:
                if (null != saveToInternalSorage) {
                    getInit(saveToInternalSorage);
                }
                break;
            case R.id.btnOK:
//			MyApplication.setPreviewBitmap(null);
                setResult(Activity.RESULT_OK,
                        new Intent().putExtra(VariableConstants.VIDEO_URI, attachmentPath).putExtra(VariableConstants.ATTACHMENT_TYPE, attachment_type));
                finish();
                break;
            case R.id.btnCancel:
                MyApplication.setTempImagePreview(null);
                MyApplication.setPreviewBitmap(null);
                MyApplication.setImageThumb(null);
                Uri mediaUri;
                if (isCapturedImage) {
                    mediaUri = attachment_type.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO) ? MediaStore.Video.Media.INTERNAL_CONTENT_URI
                            : MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                } else {
                    mediaUri = attachment_type.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO) ? MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                            : MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }
                Utils.deleteFile(this, attachmentPath, mediaUri, attachment_type);
                finish();
                break;
            default:
                break;
        }

    }

    private void openFile() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(attachmentPath)), "video/*");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (saveToInternalSorage != null) {
            saveToInternalSorage.deleteOnExit();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (saveToInternalSorage != null) {
            saveToInternalSorage.deleteOnExit();
        }
        super.onDestroy();
    }
}