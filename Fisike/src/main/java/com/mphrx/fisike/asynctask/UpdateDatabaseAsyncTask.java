package com.mphrx.fisike.asynctask;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.AttchmentDBAdapter;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;

public class UpdateDatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
    private DBAdapter mHelper;
    private Object mobileObject;
    private DatabaseTables table;
    private ContentValues updatedValues;
    private int updateDBStatus;
    private int putExtra = 1;
    private int makeLocalBroadcast = 2;

    public UpdateDatabaseAsyncTask(Object mobileObject, DatabaseTables table, ContentValues updatedValues) {
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        this.mobileObject = mobileObject;
        this.table = table;
        this.updatedValues = updatedValues;
    }

    public UpdateDatabaseAsyncTask(Object mobileObject, DatabaseTables table, ContentValues updatedValues, int updateDBStatus) {
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        this.mobileObject = mobileObject;
        this.table = table;
        this.updatedValues = updatedValues;
        this.updateDBStatus = updateDBStatus;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            performDbOperation(table, mobileObject, updatedValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    private void performDbOperation(DatabaseTables tableName, Object tableObject, ContentValues updatedValue) throws Exception {
        switch (tableName) {
            case UserMO_TABLE:
                // update UserMO_TABLE
                break;
            case SettingMO_TABLE:
                // update SettingMO_TABLE
                break;
            case ChatConversationMO_TABLE:
                // update UserMO_TABLE
                ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).updateChatConversation(((ChatConversationMO) tableObject).getPersistenceKey(), updatedValue);
                // int conversationType = ((ChatConversationMO)mobileObject).getConversationType();
                // if (conversationType == VariableConstants.conversationTypeGroup) {
                // Intent intent = new Intent(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED);
                // LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                // } else if (conversationType == VariableConstants.conversationTypeChat) {
                // Intent intent = new Intent(VariableConstants.BROADCAST_CONV_LIST_CHANGED);
                // LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                // }
                break;
            case ChatContactMo_TABLE:
                // update ChatContactMo_TABLE
                break;
            case ChatMessageMO_TABLE:
                // update ChatMessageMO_TABLE
                break;
            case QuestionActivityMO_TABLE:
                // update QuestionActivityMO_TABLE
                break;
            case QuestionMO_TABLE:
                // update QuestionMO_TABLE
                break;
            case AttachmentMO_TABLE:
                AttchmentDBAdapter.getInstance(MyApplication.getAppContext()).updateAttachmentObject((String) tableObject, updatedValue);
                break;
            case GROUP_TEXT_CHAT_MO_TABLE:
         //       mHelper.updateGroupTextChatMo((GroupTextChatMO) tableObject, updatedValue);
                break;
            case DOCUMENT_REFERENCE_TABLE:
                if (tableObject == null) {
                    DocumentReferenceDBAdapter.getInstance(MyApplication.getAppContext()).updateDocumentReference();
                } else {
                    DocumentReferenceDBAdapter.getInstance(MyApplication.getAppContext()).updateDocumentReference((DocumentReferenceModel) tableObject, updatedValue);
                }
//                if (updateDBStatus == makeLocalBroadcast) {
//                    return;
//                }

                Intent intent = new Intent(VariableConstants.BROADCAST_UPDATED_RECORD);
                if (updateDBStatus == putExtra) {
                    intent.putExtra(VariableConstants.ATTACHMENT_PERCENTAGE, updatedValue.getAsInteger("percentage"));
                    intent.putExtra(VariableConstants.ATTACHMENT_PATH, ((DocumentReferenceModel) tableObject).getDocumentLocalPath());
                }
                LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                break;
            case DISEASES_TABLE:
            case DRUG_DETATILS_TABLE:
            case ENCOUNTER_TABLE:
            case MEDICATION_PRESCRIPTION_TABLE:
            case MEDICATION_TABLE:
            case PRACTITIONAR_TABLE:
            default:
                break;
        }
    }
}
