package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.fragments.UploadTaggingFragment;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;

public class StoreUplodedRecordAsyncTask extends AsyncTask<Void, Void, Void> {
	private Context context;
	private DocumentReferenceDBAdapter mHelper;
	private DocumentReferenceModel documentReferenceModel;
	private UploadTaggingFragment uploadTaggingFragment;

	public StoreUplodedRecordAsyncTask(Context context, DocumentReferenceModel documentReferenceModel, UploadTaggingFragment uploadTaggingFragment) {
		this.context = context;
		this.uploadTaggingFragment = uploadTaggingFragment;
		this.documentReferenceModel = documentReferenceModel;
		mHelper = DocumentReferenceDBAdapter.getInstance(MyApplication.getAppContext());
	}

	@Override
	protected void onPreExecute() {
		uploadTaggingFragment.showProgressDialog();
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		try {
			mHelper.insertDocumentReference(documentReferenceModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		uploadTaggingFragment.dismissProgressDialog(documentReferenceModel);
		super.onPostExecute(result);
	}

}
