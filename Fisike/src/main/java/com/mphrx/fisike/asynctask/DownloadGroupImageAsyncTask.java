package com.mphrx.fisike.asynctask;

import android.content.ContentValues;
import android.os.AsyncTask;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadGroupImageAsyncTask extends AsyncTask<Void, Void, Void> {
    private static final String GROUP_CHAT_PIC_ARRAY = "profilePicture";
    private static final String GROUP_ID = "groupId";
    private static final String GROUP_PIC = "profilePic";
    private static final String BASE_URL = DefaultConnection.SERVER + "/minerva/userApi/getProfilePicture";
    private String groupId;

    // Db Helper
    private DBAdapter mHelper;

    private UserMO userMO;
    private MessengerService messengerService;
    private String responseString;
    private boolean isGroupId;

    public DownloadGroupImageAsyncTask(MessengerService messengerService, String groupId, boolean isGroupId) {
        this.messengerService = messengerService;
        this.groupId = groupId;
        this.isGroupId = isGroupId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        URL url;
        HttpURLConnection urlConnection = null;
        JSONArray response = new JSONArray();
        try {
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO = settingManager.getSettings();
            userMO = settingManager.getUserMO();
            boolean useHTTPS = setMO.isUseHTTPS();
            String httpStr = "https://";
            if (!useHTTPS)
                httpStr = "http://";

            url = new URL(httpStr + BASE_URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(100000);
            urlConnection.setDoOutput(true);
            urlConnection.addRequestProperty("x-auth-token", SharedPref.getAccessToken());

            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            JSONObject jsonParam = new JSONObject();

            jsonParam.put(isGroupId ? "groupId" : "id", Utils.extractGroupId(groupId));

            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.writeBytes(jsonParam.toString());


            wr.flush();
            wr.close();

            urlConnection.connect();

            //+ Utils.extractGroupId(groupId));
//            urlConnection = (HttpURLConnection) url.openConnection();
            int responseCode = urlConnection.getResponseCode();

            if (responseCode == HttpStatus.SC_OK) {
                responseString = readStream(urlConnection.getInputStream());
                if (responseString.contains("error")) {
                    // show message not able to download image
                } else {
                    parseJson(responseString);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        messengerService.updateGroupContactProfilePic(responseString, groupId);
        super.onPostExecute(result);
    }

    private String parseJson(String responseString) throws Exception {
        JSONObject contactArray = new JSONObject(responseString);
        JSONArray jsonArray = contactArray.getJSONArray(GROUP_CHAT_PIC_ARRAY);
        jsonArray.length();
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        jsonObject.getString(GROUP_ID);
        // parses the json as well as updates the db
        parseprofilePic(jsonObject.getJSONArray(GROUP_PIC));

        return jsonObject.toString();
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    private void parseprofilePic(JSONArray jsonArray) {
        try {
            byte[] profilePic = new byte[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                profilePic[i] = (byte) jsonArray.getInt(i);
            }
//            updateGroupImageDB(profilePic);
        } catch (Exception e) {

        }
    }

    private void updateGroupImageDB(byte[] profilePic) {
        GroupChatDBAdapter groupChatDBAdapter = GroupChatDBAdapter.getInstance(MyApplication.getAppContext());
        GroupTextChatMO groupTextChatMo = new GroupTextChatMO();
        groupTextChatMo.setImageData(profilePic);
        groupTextChatMo.generatePersistenceKey(groupId, userMO.getId() + "");
        ContentValues updatedValue = new ContentValues();
        updatedValue.put(groupChatDBAdapter.getIMAGEDATA(), profilePic);
        groupChatDBAdapter.updateGroupTextChatMO(groupTextChatMo.getPersistenceKey(), updatedValue);
    }

}
