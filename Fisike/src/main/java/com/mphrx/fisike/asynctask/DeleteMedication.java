package com.mphrx.fisike.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.MedicationOrderFragment;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kailash Khurana on 3/2/2016.
 */
public class DeleteMedication extends AsyncTask<Void, Void, Void> {
    private final PrescriptionModel prescriptionModel;
    private Context context;

    private boolean isDeleteSuccessful = false;
    private MedicationOrderFragment callback;
    private ProgressDialog progressDialog;

    public DeleteMedication(Context context, PrescriptionModel prescriptionModel, MedicationOrderFragment callback) {
        this.context = context;
        this.prescriptionModel = prescriptionModel;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            APIManager apiManager = APIManager.getInstance();
            String medicationPrescriptionDeleteURL = apiManager.getMinervaPlatformURL(context, TextConstants.MEDICATION_PRESCRIPTION_DELETE);
            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("medicationOrderId", prescriptionModel.getID());

            String jsonString = apiManager.executeHttpPost(medicationPrescriptionDeleteURL, authToken, jsonObject, context);

            if (jsonString != null) {
                JSONObject deleteJson = new JSONObject(jsonString);
                int statusCode = deleteJson.getInt("httpStatusCode");

                if (statusCode == 204 || statusCode == 404) {
                    isDeleteSuccessful = true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (isDeleteSuccessful) {
            callback.handleMedicationPrescriptionDelete(prescriptionModel.getID());
            if (prescriptionModel.getAutoReminderSQLBool() == 1) {
                for (MedicineDoseFrequencyModel medicineDoseFrequencyModel : prescriptionModel.getArrayDose()) {
                    Utils.cancelAlarm(context, medicineDoseFrequencyModel.getAlarmManagerUniqueKey());
                }
            }
        } else {
            callback.handleMedicineNotDeleted(prescriptionModel.getID());
        }
    }
}
