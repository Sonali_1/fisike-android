package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.fragments.SingleChatFragment;
import com.mphrx.fisike.platform.APIManager;

import org.json.JSONObject;

import java.net.URLEncoder;

/**
 * Created by administrate on 11/27/2015.
 */
 public class contactListAsyncTask extends AsyncTask<Void, Void, String> {

    String searchText,jsonString;
    Context context;
    int pageNum;
    SingleChatFragment myfragment;

    public contactListAsyncTask(String searchText,int pageNum,Context context,SingleChatFragment myfragment)
    {

        this.myfragment=myfragment;
        this.searchText=searchText;
        this.pageNum=pageNum;
        this.context=context;


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... params) {
        try {
             searchText = URLEncoder.encode(searchText.trim(), "UTF-8");
            searchText = searchText.replaceAll("\\+", "%20");
            final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + "/getContactList";

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("_count", 2);
            jsonObject.put("_skip", pageNum);
            jsonObject.put("searchText", searchText);
            APIManager jsonManager = APIManager.getInstance();
             jsonString = jsonManager.executeHttpPost(url, "", jsonObject, context);
                return  jsonString;
        } catch (Exception e) {
            jsonString = null;
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        ((com.mphrx.fisike.fragments.SingleChatFragment)myfragment ).loadContacts(jsonString);
        super.onPostExecute(result);
    }
}