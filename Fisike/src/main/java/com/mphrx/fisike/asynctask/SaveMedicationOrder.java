package com.mphrx.fisike.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MedicationActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.MedicationOrderFragment;
import com.mphrx.fisike.gson.TestExclStrat;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.AdditionalInstructions;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Bounds;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.ClassCode;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Code;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Coding;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.DosageInstruction;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.DoseQuantity;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.EncounterResource;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Entry;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Event;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Extension;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.ExtensionMedicine;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Identifier;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Medication;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.MedicationOrder;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.MedicineResource;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Name;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Patient;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Period;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.PrescriptionResource;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Reason;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Repeat;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.RepeatFrequency;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.RepeatPeriod;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Request;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Resource;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.StartDate;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Timing;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Type;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Verified;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike_physician.utils.AppLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xmb2nc on 15-02-2016.
 */
public class SaveMedicationOrder extends AsyncTask<Void, Void, String> {
    private static final String practionerfullURL = "urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0a";
    private static final String encounterfullUrl = "urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0b";
    private static final String medicationFullURl = "urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0c";
    private static final String medicineFullURL = "urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0e";
    private PrescriptionModel oldPrescriptionModel;

    //    private static final String API_REQUEST_METHOD_POST = "POST";
    private String methodType;
    private boolean isEncounterChanged;
    private boolean isPrescriberChanged;
    private boolean isMedicationChanged;
    private boolean isUpdateMedication;

    private MedicationOrderFragment fragment;
    private PrescriptionModel prescriptionModel;
    private Context context;
    private ProgressDialog progressDialog;
    private int tempPosition;

    public SaveMedicationOrder(Context context, PrescriptionModel prescriptionModel) {
        this.prescriptionModel = prescriptionModel;
        this.context = context;
        methodType = "POST";
    }

    public SaveMedicationOrder(Context context, PrescriptionModel prescriptionModel, boolean isUpdateMedication, boolean isEncounterChanged, boolean isPrescriberChanged, boolean isMedicationChanged, PrescriptionModel oldPrescriptionModel) {
        this.prescriptionModel = prescriptionModel;
        this.context = context;
        this.isUpdateMedication = isUpdateMedication;
        this.isEncounterChanged = isEncounterChanged;
        this.isMedicationChanged = isMedicationChanged;
        this.isPrescriberChanged = isPrescriberChanged;
        this.oldPrescriptionModel = oldPrescriptionModel;
        methodType = "PUT";
        prescriptionModel.setPatientID(oldPrescriptionModel.getPatientID());
        prescriptionModel.setPatientName(oldPrescriptionModel.getPatientName());
    }

    public SaveMedicationOrder(MedicationOrderFragment fragment, Context context, PrescriptionModel prescriptionModel, boolean isUpdateMedication, int switchValue, int tempPosition) {
        progressDialog = new ProgressDialog(context);
        if (isUpdateMedication) {
            progressDialog.setMessage(context.getResources().getString(R.string.progress_updating));
        }
        this.fragment = fragment;
        this.prescriptionModel = prescriptionModel;
        this.tempPosition = tempPosition;
        this.prescriptionModel.setAutoReminderSQLBool(switchValue);
        this.context = context;
        this.isUpdateMedication = isUpdateMedication;
        this.oldPrescriptionModel = prescriptionModel;
        methodType = "PUT";
        prescriptionModel.setPatientID(oldPrescriptionModel.getPatientID());
        prescriptionModel.setPatientName(oldPrescriptionModel.getPatientName());
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        if (isUpdateMedication) {
            progressDialog.setMessage(context.getResources().getString(R.string.progress_updating));
        } else {
            progressDialog.setMessage(context.getResources().getString(R.string.progress_loading));
        }
        progressDialog.setCancelable(false);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String responseGson;
        try {
            responseGson = updatePatient();
            if (responseGson != null) {
                try {
                    JSONObject medicationResponceJsonObject = new JSONObject(responseGson);
                    JSONArray entryArray = medicationResponceJsonObject.getJSONArray("entry");
                    JSONObject practitionerModel = getJsonObjectbasedOnResourceType(entryArray, "Practitioner");
                    prescriptionModel.setPractitionarID(practitionerModel != null ? practitionerModel.optInt("id") : 0);

                    JSONObject medicineModel = getJsonObjectbasedOnResourceType(entryArray, "Medication");
                    JSONObject encounterModel = getJsonObjectbasedOnResourceType(entryArray, "Encounter");
                    JSONObject medicineOrderModel = getJsonObjectbasedOnResourceType(entryArray, "MedicationOrder");
                    // setting encounter id
                    prescriptionModel.setEncounterID(encounterModel.optInt("id"));

                    //setting medicationOrder id
                    prescriptionModel.setID(medicineOrderModel.optInt("id"));

                    //setting medicine id in case medicine is not selected from medicine drop down
                    if (prescriptionModel.getMedicianId() == -1) {
                        prescriptionModel.setMedicianId(medicineModel.optInt("id"));
                    }

                } catch (JSONException e) {
                    return TextConstants.UNEXPECTED_ERROR;
                }
                return TextConstants.SUCESSFULL_API_CALL;
            } else
                return TextConstants.UNEXPECTED_ERROR;
        } catch (Exception e) {
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return TextConstants.UNEXPECTED_ERROR;
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return TextConstants.SERVICE_UNAVAILABLE;
            else
                return TextConstants.UNEXPECTED_ERROR;
        }
    }

    private String updatePatient() throws Exception {
        MedicationOrder medicationOrder = medicationOrder();

        APIManager apiManager = APIManager.getInstance();
        String otpUrl = apiManager.getCompositeMedicationApi();

        Gson gson = new GsonBuilder().setExclusionStrategies(new TestExclStrat(Reason.class)).serializeNulls().create();
        String json = new Gson().toJson(medicationOrder);
        AppLog.d("medication", json);
        JsonElement element = gson.fromJson(json.toString(), JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        ApiResult apiResult = apiManager.executeHttpPost(otpUrl, jsonObj, true, context);
        String response = apiResult != null ? apiResult.getJsonString() : null;
        AppLog.d("medication", response);
        if (response != null) {
            if (apiResult.getStatusCode() == 503)
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else {
                return response;
            }
        } else {
            throw new Exception(TextConstants.UNEXPECTED_ERROR);
        }
    }

    private MedicationOrder medicationOrder() {
        MedicationOrder medicationOrder = new MedicationOrder();

        medicationOrder.setResourceType("Bundle");
        medicationOrder.setId("bundle-transaction");
        medicationOrder.setType("transaction");

        List<Entry> entry = new ArrayList<Entry>();
        if (BuildConfig.isEnablePractitionerNameOnMedication) // if config enabled create practitioner instance
            entry.add(createPractitionerEntry());
        else if (!BuildConfig.isEnablePractitionerNameOnMedication &&
                isUpdateMedication &&
                (oldPrescriptionModel.getPractitionerName() != null))//if config disabled update flow and practitioner recieved from server-
            entry.add(createPractitionerEntry());


        if (prescriptionModel.getMedicianId() == -1) {
            entry.add(createMedicineEntry());
        }
        entry.add(createEncounterEntry());
        entry.add(createMedicationOrderEntry());
        medicationOrder.setEntry(entry);

        return medicationOrder;
    }

    private Entry createPractitionerEntry() {
        Entry physicianEntry = new Entry();
        physicianEntry.setFullUrl(practionerfullURL);

        String apiType = methodType;
        PrescriptionResource practionerResource = new PrescriptionResource();
        practionerResource.setResourceType("Practitioner");
        if (prescriptionModel.getPractitionarID() == 0) {
            apiType = "POST";
        }
        Name name = new Name();
        name.setUseCode("Human Name");
        name.setText(prescriptionModel.getPractitionerName());
        practionerResource.setName(name);
        if (isUpdateMedication) {
            practionerResource.setId(prescriptionModel.getPractitionarID());
        }
        physicianEntry.setResource(practionerResource);


        Request practionerRequest = new Request();
        practionerRequest.setMethod(apiType);
        physicianEntry.setRequest(practionerRequest);

        return physicianEntry;
    }

    private Entry createMedicineEntry() {
        Entry medicationEntry = new Entry();
        medicationEntry.setFullUrl(medicineFullURL);

        MedicineResource medicineResource = new MedicineResource();
        medicineResource.setResourceType("Medication");
        ExtensionMedicine extension = new ExtensionMedicine();
        extension.setUrl("SearchTag");
        ArrayList<Verified> valueList = new ArrayList<Verified>();
        Verified verified = new Verified();
        verified.setVerified("false");
        valueList.add(verified);
        extension.setValue(valueList);
        ArrayList<ExtensionMedicine> extensionArray = new ArrayList<ExtensionMedicine>();
        extensionArray.add(extension);
        medicineResource.setExtension(extensionArray);
        medicineResource.set_class("com.mphrx.consus.resources.Medication");

        Code code = new Code();
        code.setText(prescriptionModel.getMedicineName());
        medicineResource.setCode(code);

        medicationEntry.setResource(medicineResource);

        Request medicationResource = new Request();
        medicationResource.setMethod("POST");
        medicationEntry.setRequest(medicationResource);

        return medicationEntry;
    }

    private Entry createEncounterEntry() {
        Entry encounterEntry = new Entry();
        encounterEntry.setFullUrl(encounterfullUrl);

        EncounterResource encounterResource = new EncounterResource();
        encounterResource.setResourceType("Encounter");

        ClassCode classcode = new ClassCode();
        classcode.setValue("Inpatient");
        encounterResource.setClassCode(classcode);

        List<Identifier> identifierList = new ArrayList<Identifier>();
        Identifier identifier = new Identifier();
        ArrayList<Type> typeList = new ArrayList<Type>();
        Type type = new Type();
        type.setText("patientVisitNumber");
        typeList.add(type);
        identifier.setType(typeList);
        identifier.setUseCode("usual");
        identifier.setValue(System.currentTimeMillis());
        encounterResource.setIdentifier(identifierList);

        com.mphrx.fisike.gson.request.MedicationOrderRequest.Status status = new com.mphrx.fisike.gson.request.MedicationOrderRequest.Status();
        status.setValue("IN PROGRESS");
        encounterResource.setStatus(status);

        Patient patient = new Patient();
        if (isUpdateMedication) {
            patient.setId(oldPrescriptionModel.getPatientID());
            encounterResource.setId(prescriptionModel.getEncounterID());
        } else {
            patient.setId(prescriptionModel.getPatientID());
        }
        encounterResource.setPatient(patient);

        Period period = new Period();
        StartDate startDate = new StartDate();
        startDate.setValue(DateTimeUtil.convertSourceDestinationDateEnglish(prescriptionModel.getStartDate(), DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
        period.setStartDate(startDate);
        encounterResource.setPeriod(period);

        encounterEntry.setResource(encounterResource);

        Request encounterRequest = new Request();
        encounterRequest.setMethod(methodType);
        encounterEntry.setRequest(encounterRequest);

        return encounterEntry;
    }

    private Entry createMedicationOrderEntry() {
        Entry medicationEntry = new Entry();
        medicationEntry.setFullUrl(medicationFullURl);

        Resource medicationResource = new Resource();
        medicationResource.setResourceType("MedicationOrder");


        com.mphrx.fisike.gson.request.MedicationOrderRequest.Status status = new com.mphrx.fisike.gson.request.MedicationOrderRequest.Status();
        status.setValue("active");
        medicationResource.setStatus(status);

        if (isUpdateMedication) {
            medicationResource.setId(prescriptionModel.getID());
        }

        List<DosageInstruction> dosageInstructionList = new ArrayList<DosageInstruction>();

        ArrayList<Extension> extensions = new ArrayList<>();
        ArrayList<Integer> indexAdded = new ArrayList<>();
        for (int i = 0; i < prescriptionModel.getArrayDose().size(); i++) {
            if (indexAdded.contains(i)) {
                continue;
            }
            indexAdded.add(i);
            MedicineDoseFrequencyModel medicineDoseFrequencyModel = prescriptionModel.getArrayDose().get(i);
            ArrayList<Integer> tempIndexAdded = medicineDoseFrequencyModel.compareDoseUnitQuantity(prescriptionModel.getArrayDose(), i);

            DosageInstruction dosageInstruction = new DosageInstruction();
            String weekDays = "";
            if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(context.getString(R.string.conditional_sos))) {
                dosageInstruction.setText("Take '" + medicineDoseFrequencyModel.getDoseQuantity() + "' '" + prescriptionModel.getMedicineName() + "' '" + medicineDoseFrequencyModel.getDoseUnit() + "' SOS '");
//                dosageInstruction.setText("SOS");
                Extension extension = new Extension();
                ArrayList<String> value = new ArrayList<>();
                value.add("SOS");
                extension.setValue(value);
                extension.setUrl("SOS");
                extensions.add(extension);
            } else if (medicineDoseFrequencyModel.isRepeatAfterEverySelected()) {
                dosageInstruction.setText("Take '" + medicineDoseFrequencyModel.getDoseQuantity() + "' '" + prescriptionModel.getMedicineName() + "' '" + medicineDoseFrequencyModel.getDoseUnit() + "' '" + prescriptionModel.getDoseInstruction() + "' 'after " + medicineDoseFrequencyModel.getRepeatDaysInterval() + " days'");
//                dosageInstruction.setText(medicineDoseFrequencyModel.getDoseHours()+":"+medicineDoseFrequencyModel.getDoseMinutes()+" "+medicineDoseFrequencyModel.getDoseTimeAMorPM().toUpperCase()+" "+medicineDoseFrequencyModel.getDoseIntakeTimeInstruction().toLowerCase());
            } else if (medicineDoseFrequencyModel.getDoseWeekdaysList().size() == 7) {
                dosageInstruction.setText("Take '" + medicineDoseFrequencyModel.getDoseQuantity() + "' '" + prescriptionModel.getMedicineName() + "' '" + medicineDoseFrequencyModel.getDoseUnit() + "' '" + prescriptionModel.getDoseInstruction() + "' 'everyday'");
//                dosageInstruction.setText(medicineDoseFrequencyModel.getDoseHours()+":"+medicineDoseFrequencyModel.getDoseMinutes()+" "+medicineDoseFrequencyModel.getDoseTimeAMorPM().toUpperCase()+" "+medicineDoseFrequencyModel.getDoseIntakeTimeInstruction().toLowerCase());
            } else {
                ArrayList<String> value = new ArrayList<>();
                for (int j = 0; j < medicineDoseFrequencyModel.getDoseWeekdaysList().size(); j++) {
                    if (j != medicineDoseFrequencyModel.getDoseWeekdaysList().size() - 1) {
                        weekDays += medicineDoseFrequencyModel.getDoseWeekdaysList().get(j) + ",";
                    } else {
                        weekDays += medicineDoseFrequencyModel.getDoseWeekdaysList().get(j);
                    }
                    value.add(medicineDoseFrequencyModel.getDoseWeekdaysList().get(j));
                }
                Extension extension = new Extension();
                extension.setValue(value);
                extension.setUrl("specificWeekDays");
                extensions.add(extension);
                dosageInstruction.setText("Take '" + medicineDoseFrequencyModel.getDoseQuantity() + "' '" + prescriptionModel.getMedicineName() + "' '" + medicineDoseFrequencyModel.getDoseUnit() + "' '" + prescriptionModel.getDoseInstruction() + "' '" + weekDays + "'");
//                dosageInstruction.setText(medicineDoseFrequencyModel.getDoseHours()+":"+medicineDoseFrequencyModel.getDoseMinutes()+" "+medicineDoseFrequencyModel.getDoseTimeAMorPM().toUpperCase()+" "+medicineDoseFrequencyModel.getDoseIntakeTimeInstruction().toLowerCase());
            }
            DoseQuantity doseQuantity = new DoseQuantity();
            doseQuantity.setUnits(medicineDoseFrequencyModel.getDoseUnit());
            doseQuantity.setValue(medicineDoseFrequencyModel.getDoseQuantity());
            dosageInstruction.setDoseQuantity(doseQuantity);

            if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(context.getString(R.string.conditional_sos))) {
                Timing timing = new Timing();
                List<Event> eventList = new ArrayList<Event>();
                timing.setEvent(eventList);
                timing.setExtension(extensions);
                dosageInstruction.setTiming(timing);
            } else {
                Timing timing = new Timing();
                if (prescriptionModel.getAutoReminderSQLBool() == 1) {
                    List<Event> eventList = new ArrayList<Event>();
                    Event event = new Event();
                    event.setValue(DateTimeUtil.calculateDateEnglish(medicineDoseFrequencyModel.getStartDate(), DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
                    eventList.add(event);
                    timing.setEvent(eventList);
                } else {
                    List<Event> eventList = new ArrayList<Event>();
                    timing.setEvent(eventList);
                }
                timing.setExtension(extensions);
                Repeat repeat = new Repeat();
                Bounds bound = new Bounds();
                StartDate startDate = new StartDate();
                startDate.setValue(DateTimeUtil.calculateDateEnglish(medicineDoseFrequencyModel.getStartDate(), DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
                bound.setStartDate(startDate);
                repeat.setBounds(bound);
                int frequencyValue = prescriptionModel.getArrayDose().size();
                RepeatFrequency frequency = new RepeatFrequency();
                frequency.setValue(frequencyValue);
                repeat.setFrequency(frequency);
                int periodValue = medicineDoseFrequencyModel.getRepeatDaysInterval() == 0 ? 1 : medicineDoseFrequencyModel.getRepeatDaysInterval();
                RepeatPeriod period = new RepeatPeriod();
                period.setValue(periodValue);
                repeat.setPeriod(period);
                repeat.setPeriodUnits("d");
                timing.setRepeat(repeat);
                dosageInstruction.setTiming(timing);
            }
            if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(context.getString(R.string.conditional_sos))) {
                AdditionalInstructions addionalInstruction = new AdditionalInstructions();
                ArrayList<Coding> codingList = new ArrayList<>();
                for (int j = 0; j < prescriptionModel.getArrayDose().size(); j++) {
                    MedicineDoseFrequencyModel arrayDose = prescriptionModel.getArrayDose().get(0);
                    Coding coding = new Coding();
                    coding.setDisplay(arrayDose.getDoseIntakeTimeInstruction());
                    codingList.add(coding);
                }
                addionalInstruction.setCoding(codingList);
                dosageInstruction.setAdditionalInstructions(addionalInstruction);
            } else {
                AdditionalInstructions addionalInstruction = new AdditionalInstructions();
                ArrayList<Coding> codingList = new ArrayList<>();
                for (int j = 0; j < tempIndexAdded.size(); j++) {
                    MedicineDoseFrequencyModel arrayDose = prescriptionModel.getArrayDose().get(tempIndexAdded.get(j));
                    indexAdded.add(tempIndexAdded.get(j));
                    Coding coding = new Coding();
                    int hr = Integer.parseInt(arrayDose.getDoseHours());
                    if (arrayDose.getDoseTimeAMorPM() != null && arrayDose.getDoseTimeAMorPM().equalsIgnoreCase(TextConstants.PM)) {
                        hr += 12;
                    } else if (arrayDose.getDoseTimeAMorPM() != null && arrayDose.getDoseTimeAMorPM().equalsIgnoreCase(TextConstants.AM) && hr == 12) {
                        hr -= 12;
                    }
                    coding.setDisplay(Integer.toString(hr) + ": " + arrayDose.getDoseMinutes() + "," + arrayDose.getDoseIntakeTimeInstruction());
                    codingList.add(coding);
                    if (prescriptionModel.getAutoReminderSQLBool() == 1) {
                        arrayDose.setAlarmManagerUniqueKey((int) System.currentTimeMillis());
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                        }
                        AppLog.d("TAG", "createMedicationEntry: " + j + arrayDose.getAlarmManagerUniqueKey());
                    }
                }
                addionalInstruction.setCoding(codingList);
                dosageInstruction.setAdditionalInstructions(addionalInstruction);
            }
            dosageInstructionList.add(dosageInstruction);
        }

        medicationResource.setDosageInstruction(dosageInstructionList);
        if (isUpdateMedication) {
            medicationResource.setEncounter((Integer) prescriptionModel.getEncounterID());
        } else {
            medicationResource.setEncounter(encounterfullUrl);
        }

        if (prescriptionModel.getMedicianId() != -1) {
            Medication medication = new Medication();
            medication.set_class("com.mphrx.consus.resources.Medication");
            medication.setId(prescriptionModel.getMedicianId());
            medicationResource.setMedication(medication);
        } else {
            medicationResource.setMedication(medicineFullURL);
        }

        Patient patient = new Patient();
        if (isUpdateMedication) {
            patient.setId(oldPrescriptionModel.getPatientID());
        } else {
            patient.setId(prescriptionModel.getPatientID());
        }
        patient.set_class("com.mphrx.consus.resources.Patient");

        if (isUpdateMedication && ((Integer) prescriptionModel.getPractitionarID()) > 0) {
            medicationResource.setPrescriber((Integer) prescriptionModel.getPractitionarID());
        } else {
            if (BuildConfig.isEnablePractitionerNameOnMedication) // if config enabled create practitioner instance
                medicationResource.setPrescriber(practionerfullURL);
        }
        medicationResource.setPatient(patient);

        if (BuildConfig.isEnableMedicationCondition || (prescriptionModel.getDiseaseMoList() != null && prescriptionModel.getDiseaseMoList().size() > 0)) {
            Reason medicationOrderReason = new Reason();
            String diseaseListName = "";
            List<Coding> codingListReason = new ArrayList<Coding>();
            for (int i = 0; i < prescriptionModel.getDiseaseMoList().size(); i++) {
                DiseaseMO diseaseMo = prescriptionModel.getDiseaseMoList().get(i);
                diseaseListName += (String) diseaseMo.getText() + ";";

                Coding coding = new Coding();
                coding.setDisplay((String) diseaseMo.getText());
                coding.setSystem((String) diseaseMo.getSystem());
                coding.setCode((String) diseaseMo.getCode());
                codingListReason.add(coding);
            }

            medicationOrderReason.setCoding(codingListReason);
            medicationOrderReason.setText(diseaseListName);
            medicationResource.setReason(medicationOrderReason);
        } else {
            medicationResource.setReason(null);
        }
        medicationResource.setDateWritten(DateTimeUtil.calculateDateEnglish(prescriptionModel.getStartDate(), DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
        medicationResource.setDateEnded(DateTimeUtil.calculateDateEnglish(prescriptionModel.getEndDate(), DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));

        /*
         *
         * Extention will be for future devlopment for remarks in medication add
         added extension to support at southend
         *
         *
         */

        List<Extension> extentionList = prescriptionModel.getExtensionList();
        if (extentionList != null) {
            int index = -1;
            for (int i = 0; i < extentionList.size(); i++) {
                if (extentionList.get(0).getUrl().equalsIgnoreCase("Instructions")) {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                ArrayList<String> valueList = new ArrayList<>();
                valueList.add(prescriptionModel.getRemark());
                extentionList.get(index).setValue(valueList);
            } else {
                Extension extention = new Extension();
                extention.setUrl("Instructions");
                ArrayList<String> valueList = new ArrayList<>();
                valueList.add(prescriptionModel.getRemark());
                extention.setValue(valueList);
                extentionList.add(extention);
            }
        } else {
            extentionList = new ArrayList<>();
            Extension extention = new Extension();
            extention.setUrl("Instructions");
            ArrayList<String> valueList = new ArrayList<>();
            valueList.add(prescriptionModel.getRemark());
            extention.setValue(valueList);
            extentionList.add(extention);
        }

        medicationResource.setExtension(extentionList);
        medicationEntry.setResource(medicationResource);
        Request medicationRequest = new Request();
        medicationRequest.setMethod(methodType);

        medicationEntry.setRequest(medicationRequest);
        return medicationEntry;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (aVoid.equals(TextConstants.SUCESSFULL_API_CALL) && context instanceof MedicationActivity) {
            ((MedicationActivity) context).saveOrUpdatePrescriptioninDB(context, prescriptionModel, oldPrescriptionModel);
        } else if (context instanceof MedicationActivity) {
            ((MedicationActivity) context).errorInSavingResult(aVoid);
        } else if (aVoid.equals(TextConstants.SUCESSFULL_API_CALL) && fragment != null) {
            fragment.saveOrUpdatePrescriptioninDB(prescriptionModel, tempPosition);
        } else if (fragment != null) {
            fragment.errorInSavingResult(aVoid);
        }
        super.onPostExecute(aVoid);
    }


    private JSONObject getJsonObjectbasedOnResourceType(JSONArray entryArray, String resourceType) {

        JSONObject object;
        for (int i = 0; i < entryArray.length(); i++) {
            try {
                object = entryArray.getJSONObject(i);
                String resource = object.getJSONObject("resource").getString("resourceType");
                if (resource.equalsIgnoreCase(resourceType))
                    return object.getJSONObject("resource");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}