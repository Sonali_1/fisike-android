package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.Code;
import com.mphrx.fisike.gson.response.MedicineNameResponse;
import com.mphrx.fisike.models.MedicationModel;
import com.mphrx.fisike.persistence.MedicationDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.utils.SharedPref;

public class GetMedicineNameAsyncTask extends AsyncTask<Void, Void, Void> {
    private Context context;
    private int medicineId;
    private static final String MEDICINE_BASE_URL = "/Medication/show/";
    private CustomFontTextView tvMedicineName;
    private String medicineName;

    public GetMedicineNameAsyncTask(Context context, int medicineId, CustomFontTextView tvMedicineName) {
        this.context = context;
        this.medicineId = medicineId;
        this.tvMedicineName = tvMedicineName;

    }

    @Override
    protected Void doInBackground(Void... params) {
        APIManager apiManager = APIManager.getInstance();
        String medicineSearchURL = apiManager.getMinervaPlatformURL(context, MEDICINE_BASE_URL + medicineId);
               /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
        String authToken = SharedPref.getAccessToken();
        String medicationResponse = apiManager.executeHttpGet(medicineSearchURL, authToken);
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(medicationResponse, MedicineNameResponse.class);
        if (jsonToObjectMapper instanceof MedicineNameResponse) {
            Code code = ((MedicineNameResponse) jsonToObjectMapper).getCode();
            if (code != null) {
                medicineName = code.getText();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        MedicationModel medicationModel = new MedicationModel();
        medicationModel.setMedicationID(medicineId);
        medicationModel.setMedician(medicineName);
        try {
            MedicationDBAdapter.getInstance(context).createOrUpdateMedicationModel(medicationModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvMedicineName.setText(medicineName);
    }

}
