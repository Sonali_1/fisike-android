package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.GroupDetailActivity;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;

public class UploadGroupPicAsyncTask extends AsyncTask<Void, Void, Void> {
    private static final String GROUP_ID = "groupId";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String GROUP_CHAT_URL = "/userApi/uploadProfilePicture";


    private Bitmap bitmap;
    private Context context;
    private String jsonString;
    private byte[] bitmapdata;
    private String groupId;

    public UploadGroupPicAsyncTask(Context context, Bitmap bitmap, String groupId) {
        this.context = context;
        this.bitmap = bitmap;
        this.groupId = groupId;
    }

    @Override
    protected void onPreExecute() {
        if (context instanceof GroupDetailActivity) {
            ((GroupDetailActivity) context).progressStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        postImageData();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (context instanceof com.mphrx.fisike_physician.activity.GroupDetailActivity) {
            ((com.mphrx.fisike_physician.activity.GroupDetailActivity) context).uploadProfilePic(jsonString, bitmapdata, groupId);
        }

        super.onPostExecute(result);
    }

    public void postImageData() {

        try {
            SettingManager settingManager = SettingManager.getInstance();
            SettingMO setMO = settingManager.getSettings();
            boolean useHTTPS = setMO.isUseHTTPS();

            String httpStr = "https://";

            if (!useHTTPS)
                httpStr = "http://";

            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(httpStr + setMO.getServerIP() + "/minerva" + GROUP_CHAT_URL);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//            httpPost.addHeader("Content-Type","application/json");

            String authToken = SharedPref.getAccessToken();
            httpPost.setHeader("x-auth-token", authToken);
            entity.addPart("groupId", new StringBody(Utils.extractGroupId(groupId)));

            if (null != bitmap) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                bitmapdata = stream.toByteArray();
                entity.addPart("profilePic", new ByteArrayBody(stream.toByteArray(), "image/jpeg", "groupPic.jpg"));
            }
            httpPost.setEntity(entity);
            HttpResponse response = httpclient.execute(httpPost, new BasicHttpContext());

            BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            jsonString = sb.toString();

            Thread.sleep(2500);

            Network.getImageCache().removeAllGroupPicEntries(Utils.extractGroupId(groupId));

            LocalBroadcastManager.getInstance(context).sendBroadcast(
                    new Intent(VariableConstants.PROFILE_PIC_CHANGED_BROADCAST)
                            .putExtra(VariableConstants.UpdatedPicContactId, Utils.extractGroupId(groupId)));

        } catch (Exception e) {
            e.getStackTrace();
        }
    }

}
