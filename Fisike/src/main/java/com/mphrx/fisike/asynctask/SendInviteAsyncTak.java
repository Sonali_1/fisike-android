package com.mphrx.fisike.asynctask;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.asmack.wrappers.GroupInvitePacket;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ChatMessageMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike_physician.utils.TextPattern;

import org.jivesoftware.smack.XMPPConnection;

import java.util.List;

public class SendInviteAsyncTak extends AsyncTask<Void, Void, Void> {
	private static XMPPConnection connection;
	private List<ChatContactMO> inviteList;
	// XMPP Connection objects
	private ConnectionInfo connectionInfo;
	// Setting Manager and MO loaded into memory
	private SettingManager settingManager;
	private SettingMO settingMO;
	private String groupName;

	private Context context;

	private boolean launchGroupRoster = true;
	private Application application;
	private int size;
	private boolean stopProgressBarBroadcast = true;
	// indicates invite from single chat screen
	private boolean isGroupCreatedFromSingleChat = true;
	private ChatConversationMO firstTimeChatConversationMO = null;
	private ChatMessageMO firstTimeMessage;
	private MessengerService messengerService;

	public SendInviteAsyncTak(List<ChatContactMO> inviteList, String groupName, Context context, 
			                  boolean groupRoster, Application application, int size, 
			                  XMPPConnection connection, boolean stopProgressBarBroadcast, boolean isGroupCreatedFromSingleChat) {
		this.inviteList = inviteList;
		this.groupName = groupName;
		this.context = context;
		launchGroupRoster = groupRoster;
		this.application = application;
		this.size = size;
		this.connection = connection;
		this.stopProgressBarBroadcast = stopProgressBarBroadcast;
		this.isGroupCreatedFromSingleChat = isGroupCreatedFromSingleChat;
	}
	
	public SendInviteAsyncTak(List<ChatContactMO> inviteList, String groupName, Context context, 
			                  boolean groupRoster, Application application, int size, 
			                  XMPPConnection connection, boolean stopProgressBarBroadcast, 
			                  ChatConversationMO firstChatConversationMO, ChatMessageMO firstTimeMessage,
			                  MessengerService messengerService) {
		this.inviteList = inviteList;
		this.groupName = groupName;
		this.context = context;
		launchGroupRoster = groupRoster;
		this.application = application;
		this.size = size;
		this.connection = connection;
		this.stopProgressBarBroadcast = stopProgressBarBroadcast;
		this.firstTimeChatConversationMO = firstChatConversationMO;
		this.firstTimeMessage = firstTimeMessage;
		this.messengerService = messengerService;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		// Load up settings
		settingManager = SettingManager.getInstance();
		settingMO = settingManager.getSettings();
		connectionInfo = ConnectionInfo.getInstance();
		connection = connectionInfo.getXmppConnection();
		for (ChatContactMO invite : inviteList) {
			GroupInvitePacket groupInvite = new GroupInvitePacket();
			groupInvite.setGroupName(groupName);
			groupInvite.setUserType(invite.getUserType().getName());
			groupInvite.setInvitedMemberJid(TextPattern.getUserId(invite.getId()) + "@" + settingMO.getFisikeServerIp());
			connection.sendPacket(groupInvite);
		}
		if(firstTimeChatConversationMO != null){
			// update the chat conversationMo table
			ContentValues updatedValues = new ContentValues();
			updatedValues.put("groupStatus", TextConstants.MEMBER_INVITATION_SENT);
            ChatConversationDBAdapter.getInstance(context).updateChatConversation(firstTimeChatConversationMO.getPersistenceKey(), updatedValues);
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(stopProgressBarBroadcast && isGroupCreatedFromSingleChat){
			// send broadcast to message activity that group is created
			Intent intent = new Intent(VariableConstants.GROUP_INVITE_SENT_FOR_SINGLE_CHAT);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
			
		}else if (stopProgressBarBroadcast) {
			Intent intent = new Intent(VariableConstants.GROUP_INVITE_SENT);
			intent.putExtra(VariableConstants.LAUNCH_GROUP_ROSTER, launchGroupRoster);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}
		try {
		    Thread.sleep(1000);                 //1000 milliseconds is one second.
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		//messengerService.sendMessageOnFirstTimeGroupCreated(firstTimeMessage);

		// new PiwikGroupCreateDispatch().execute();
	}

	private class PiwikGroupCreateDispatch extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			if (size > 0) {
				DBAdapter mHelper = DBAdapter.getInstance(context);
				int fetchGroupNames = chatContactDBAdapter.getInstance(context).fetchGroupMemberListSum();
				//ArrayList<PiwikVar> customVar = new ArrayList<PiwikUtils.PiwikVar>();
				int avgGroupMember = fetchGroupNames / size;
				//customVar.add(new PiwikVar(PiwikConstants.VAR_TWO, PiwikConstants.KGROUP_NUMBER_GROUP_LABLE, avgGroupMember + ""));
				//PiwikUtils.sendEvent(application, PiwikConstants.KGROUP_CATEGORY, PiwikConstants.KGROUP_NUMBER_MEMBERS_LABLE,
						//PiwikConstants.KGROUP_MEMBER_ADDED_TXT, customVar);
			}
			return null;
		}

	}
}
