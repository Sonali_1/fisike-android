package com.mphrx.fisike.asynctask;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.DatabaseTables;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.GroupTextChatMO;
import com.mphrx.fisike.mo.NotificationCenter;
import com.mphrx.fisike.mo.QuestionActivityMO;
import com.mphrx.fisike.mo.QuestionMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.DiagnosticOrderDBAdapter;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;
import com.mphrx.fisike.persistence.GroupChatDBAdapter;
import com.mphrx.fisike.persistence.NotificationCenterDBAdapter;
import com.mphrx.fisike.persistence.SettingsDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.vital_submit.response.ObservationSaveResponse;

import java.util.ArrayList;

public class DatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
    private  ContentValues contentValues;
    private DBAdapter mHelper;

    private Object mobileObject;
    private DatabaseTables table;
    private boolean isLastMessageRead = false;
    private boolean readStatus = false;
    private boolean isToClearRecords = false;
    private NotificationCenterDBAdapter notificationCenterAdapter;

    public DatabaseAsyncTask(Object mobileObject, DatabaseTables table) {
        // Initialize DB Helper
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        notificationCenterAdapter=NotificationCenterDBAdapter.getInstance(MyApplication.getAppContext());
        this.mobileObject = mobileObject;
        this.table = table;
        this.contentValues=null;
    }


    public DatabaseAsyncTask(boolean isToClearRecords , DatabaseTables table) {
        // Initialize DB Helper
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());

        this.table = table;
        this.isToClearRecords = isToClearRecords;
        this.contentValues=null;
    }

    public DatabaseAsyncTask(Object mobileObject, DatabaseTables table, ContentValues contentValues) {
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        notificationCenterAdapter=NotificationCenterDBAdapter.getInstance(MyApplication.getAppContext());
        this.mobileObject = mobileObject;
        this.table = table;
        this.contentValues=contentValues;

    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            performDbOperation(table, mobileObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void performDbOperation(DatabaseTables tableName, Object tableObject) throws Exception {
        switch (tableName) {
            case UserMO_TABLE:
                UserDBAdapter.getInstance(MyApplication.getAppContext()).createOrUpdateUserMO((UserMO) tableObject);
                break;
            case SettingMO_TABLE:
                SettingsDBAdapter.getInstance(MyApplication.getAppContext()).createOrUpdateSettingMO((SettingMO) tableObject);
                break;
            case ChatConversationMO_TABLE:
         //       boolean createOrUpdateChatConversationMO = ChatConversationDBAdapter.getInstance(MyApplication.getAppContext()).createOrUpdateChatConversationMO((ChatConversationMO) tableObject, readStatus);
                break;
            case ChatContactMo_TABLE:
                chatContactDBAdapter.getInstance(MyApplication.getAppContext()).insert((ChatContactMO) tableObject);
                break;
/*
            case ChatMessageMO_TABLE:
                mHelper.createOrUpdateChatMessageMO((ChatMessageMO) tableObject, isLastMessageRead);
                break;
*/
            case QuestionActivityMO_TABLE:
                mHelper.createOrUpdateQuestionActivityMO((QuestionActivityMO) tableObject);
                break;
            case QuestionMO_TABLE:
                mHelper.createOrUpdateQuestionMO((QuestionMO) tableObject);
                break;
            case AttachmentMO_TABLE:
                //no calls related to this so commenting it
              //  mHelper.createOrUpdateAttachmentMO((AttachmentMo) tableObject);
                break;
            case DRUG_DETATILS_TABLE:
                break;
            case GROUP_TEXT_CHAT_MO_TABLE:
                if(contentValues!=null)
                    GroupChatDBAdapter.getInstance(MyApplication.getAppContext()).updateGroupTextChatMO(((GroupTextChatMO) tableObject).getPersistenceKey(),contentValues);
                else
                    GroupChatDBAdapter.getInstance(MyApplication.getAppContext()).insert((GroupTextChatMO) tableObject);
                Intent intent = new Intent(VariableConstants.BROADCAST_GROUP_CONV_LIST_CHANGED);
                LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                break;
            case MEDICATION_TABLE:
                break;
            case PRACTITIONAR_TABLE:
                break;
            case ENCOUNTER_TABLE:
                break;
            case MEDICATION_PRESCRIPTION_TABLE:
                break;
            case DOCUMENT_REFERENCE_TABLE:
                DocumentReferenceDBAdapter.getInstance(MyApplication.getAppContext()).insertDocumentReference((DocumentReferenceModel) tableObject);
                break;
            case NOTIFICATION_CENTER_TABLE:
                boolean insertNotificationCenter =notificationCenterAdapter.insertNotificationCenter((NotificationCenter) tableObject);

                if (insertNotificationCenter) {
                    Intent alarmIntent = new Intent(VariableConstants.BROADCAST_REFRESH_ALARM);
                    LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(alarmIntent);
                }
                break;
            case PATIENT_RECORD_TABLE:
                if(isToClearRecords){
                    DiagnosticOrderDBAdapter.getInstance(MyApplication.getAppContext()).deleteRecords();
                }
                else
                    DiagnosticOrderDBAdapter.getInstance(MyApplication.getAppContext()).insertDiagnosticOrderArray((ArrayList<DiagnosticOrder>) tableObject);
                break;
            default:
                break;
        }
    }
}
