package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.createUserProfile.CreateProfileActivity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Neha on 09-06-2016.
 */
public class CreateHeightWeightSpinnerValue extends AsyncTask<Void, Void, String> {


    private ArrayList<String> item;
    private ArrayList<String> itemLbs;
    Context context;
    public CreateHeightWeightSpinnerValue(Context context)
    {
        this.context=context;
    }
    @Override
    protected String doInBackground(Void... params)
    {

        Locale locale  = new Locale("en", "US");
        String pattern = "#.#";
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        df.applyPattern(pattern);


       // DecimalFormat df = new DecimalFormat("#.#");
        try{
        item = new ArrayList<>();
        for (float i = 0.1f; i < 600; )
        {
            item.add("" + df.format(i));
            if(i<200)
                i = i + 0.1f;
            else
                i=i+1;
        }

        itemLbs = new ArrayList<>();
        for (float j = 0.1f; j < 1323f;)
        {
            itemLbs.add("" + df.format(j));
            if(j<200)
                j=j+0.1f;
            else
                j=j+1;
        }
        return TextConstants.SUCESSFULL_API_CALL;}
        catch (Exception e)
        {
            return "";
        }
    }

    @Override
    protected void onPostExecute(String s)
    {

        ((CreateProfileActivity)(context)).executeSetHeightWeightSpinnerValue(s,item,itemLbs);
    }
}