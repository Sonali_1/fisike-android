package com.mphrx.fisike.asynctask;

import android.os.AsyncTask;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Packet;

public class SendPacketAsyncTask extends AsyncTask<Void, Void, Void> {
    private Packet packet;
    private XMPPConnection connection;

    public SendPacketAsyncTask(XMPPConnection connection, Packet packet) {
        this.connection = connection;
        this.packet = packet;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            String str = packet.toXML().toString();
            if (null != connection && connection.isConnected() && connection.isAuthenticated() && null != packet) {
                connection.sendPacket(packet);
            }
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }
}
