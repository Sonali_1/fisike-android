package com.mphrx.fisike.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.response.PhysicianNameResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.utils.SharedPref;

public class GetPhysicianNameAsyncTask extends AsyncTask<Void, Void, Void>{
	private Context context;
	private int physicianId;
	private CustomFontTextView tvphysicianName;
	private String physicianName;
	private static final String PHYSICIAN_BASE_URL = "Practitioner/show/";
	public GetPhysicianNameAsyncTask(Context context, int physicianId, CustomFontTextView tvphysicianName){
		this.context = context;
		this.physicianId = physicianId;
		this.tvphysicianName = tvphysicianName;
	}

	@Override
	protected Void doInBackground(Void... params) {
		 APIManager apiManager = APIManager.getInstance();
		 String medicineSearchURL = apiManager.getMinervaPlatformURL(context, PHYSICIAN_BASE_URL + physicianId);
		         /* SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");*/
		String authToken = SharedPref.getAccessToken();
		 String physicianResponse = apiManager.executeHttpGet(medicineSearchURL, authToken);
		 
		 Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(physicianResponse, PhysicianNameResponse.class);
	       if (jsonToObjectMapper instanceof PhysicianNameResponse) {
	           physicianName = ((PhysicianNameResponse) jsonToObjectMapper).getName().getText();
	       }
		 return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		tvphysicianName.setText(physicianName);
	}

}
