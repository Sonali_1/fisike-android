package com.mphrx.fisike;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

/**
 * Created by Aastha on 17/02/2016.
 */
public class CreateNewAccountActivity extends HeaderActivity {

    Button btSignUp;
//    LinearLayout llSignIn;
    Button btSignin;
    private CustomFontTextView tv_phone_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_account_signup);
        findView();
        initView();
        bindView();
    }

    private void initView() {
        btSignin.setText(R.string.text_signin);
            tv_phone_no.setText(""+SharedPref.getMobileNumber()+".");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        SharedPref.setOldMobileNumber(SharedPref.getMobileNumber());
        SharedPref.setOldCountryCode(SharedPref.getCountryCode());

//        setTitle("Change Number");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportActionBar().setTitle(getResources().getString(R.string.create_new_account));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
       // findViewById(R.id.action_bar).setVisibility(View.VISIBLE);

    }

    private void bindView() {
//        btSignUp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent signUpIntent = new Intent(CreateNewAccountActivity.this, OtpActivity.class);
//                signUpIntent.putExtra(TextConstants.DISABLE_USER, true);
//                startActivity(signUpIntent);
//            }
//        });
//        btSignin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent loginIntent = new Intent(CreateNewAccountActivity.this, LoginActivity.class);
//                startActivity(loginIntent);
//            }
//        });
    }

    private void findView() {

        btSignUp = (Button) findViewById(R.id.btn_new_signupp);
//        llSignIn = (LinearLayout) findViewById(R.id.sign_in);
        btSignin = (Button) findViewById(R.id.btn_bottom_signin);
        tv_phone_no=(CustomFontTextView)findViewById(R.id.tv_phone_no);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom_signin:
            case R.id.btn_new_signupp:
                finish();
                break;
            case R.id.btn:
                Intent signUpIntent = new Intent(CreateNewAccountActivity.this, OtpActivity.class);
                signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                signUpIntent.putExtra(TextConstants.DISABLE_USER, true);
                startActivity(signUpIntent);
                break;
        }

    }

}
