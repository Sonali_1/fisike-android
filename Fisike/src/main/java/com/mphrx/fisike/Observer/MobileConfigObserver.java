package com.mphrx.fisike.Observer;

import java.util.Observable;

/**
 * Created by kailashkhurana on 12/07/17.
 */

public class MobileConfigObserver extends Observable {

    private static MobileConfigObserver mobileConfigObserver;
    private boolean isError;

    public static MobileConfigObserver getInstance() {
        if (mobileConfigObserver == null) {
            mobileConfigObserver = new MobileConfigObserver();
        }
        return mobileConfigObserver;
    }

    private MobileConfigObserver() {

    }

    public void callNotify(boolean isError) {
        this.isError = isError;
        setChanged();
        notifyObservers();
    }

    public boolean isError() {
        return isError;
    }
}
