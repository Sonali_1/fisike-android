package com.mphrx.fisike;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;

import com.mphrx.fisike.adapter.NavDrawerItem;
import com.mphrx.fisike.adapter.NavigationDrawerAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.ContactActivity;
import com.mphrx.fisike_physician.activity.InviteUsersActivity;
import com.mphrx.fisike_physician.activity.MessageListActivity;
import com.mphrx.fisike_physician.activity.MessengerServiceBindActivity;
import com.mphrx.fisike_physician.activity.MyScheduleActivity;
import com.mphrx.fisike_physician.activity.SettingActivity;
import com.mphrx.fisike_physician.results.activity.ResultActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;

import appointment.AppointmentActivity;
import careplan.activity.CareTaskActivity;
import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;

/**
 * Created by Neha on 05-08-2016.
 */
public abstract class NavigationDrawerActivity extends MessengerServiceBindActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    /*
    set unique identifier of each no matter
    at what position they are added in navigation drawer list
    *
    */

    public static final int SEARCHPATIENTS = 0;
    public static final int MYCONTACTS = 1;
    public static final int MESSAGES = 2;
    public static final int INVITE = 3;
    public static final int MYHEALTH = 4;
    public static final int CHAT = 5;
    public static final int NOTIFICATION = 6;
    public static final int APPOINTMENT = 7;
    public static final int VACCINATION = 8;
    public static final int WHATSNEXT = 9;
    public static final int CAREPLAN = 10;
    public static final int SETTING = 11;
    public static final int SIGNOUT = 12;
    public static final int HOME = 13;
    public static final int SEARCH_RESULT = 14;
    public static final int HOMEDRAWREQUESTS = 14;
    public static final int PROFILES = 15;
    public static final int HOMEDRAW = 16;
    public static final int MYSCHEDULE = 17;
    public static final int HOMEINDEX = MYHEALTH;

    public static String homeFragmentClassName ;

    public String[] leftSliderData;
    public String[] navMenuIcons;
    public ArrayList<NavDrawerItem> navDrawerItems;

    public NavigationDrawerAdapter navigationDrawerAdapter;
    private int item_clicked;
    private static int currentItem;

    public NavigationDrawerActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void setNavDrawerItems() {
        if (BuildConfig.isPhysicianApp)
            initializeNavigationDrawerPhysician();
        else
            InitializeNavigationDrawerPatient();

    }

    @Override
    public void fetchInitialDataFromServer() {

    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {

    }


    public abstract void setNavigationDrawerAdapter();

    public void setCurrentItem(int currentItem) {
        this.currentItem = currentItem;
    }

    private void initializeNavigationDrawerPhysician() {
        //this method will set navigation drawer items to the list
        leftSliderData = getResources().getStringArray(R.array.nav_drawer_items_physician);
        navMenuIcons = getResources().getStringArray(R.array.nav_drawer_icons_physician);


    /*[..............Adding navigation drawer items.................]*/
        navDrawerItems = new ArrayList<NavDrawerItem>();
        if (BuildConfig.isSearchPatient) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[0], navMenuIcons[10], SEARCHPATIENTS,""));
        }else {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[1], navMenuIcons[1], MYCONTACTS,""));
            navDrawerItems.add(new NavDrawerItem(leftSliderData[2], navMenuIcons[2], MESSAGES,""));
            navDrawerItems.add(new NavDrawerItem(leftSliderData[3], navMenuIcons[3], INVITE,""));
        }
        navDrawerItems.add(new NavDrawerItem(leftSliderData[8], navMenuIcons[8], SEARCH_RESULT,""));
        if (BuildConfig.isAppointmentEnabled) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[4], navMenuIcons[4], APPOINTMENT,""));
        }

        navDrawerItems.add(new NavDrawerItem(leftSliderData[9], navMenuIcons[9], MYSCHEDULE,""));

        if (BuildConfig.isWhatsNextEnabled) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[6], navMenuIcons[6], WHATSNEXT,""));
        }

        navDrawerItems.add(new NavDrawerItem(leftSliderData[5], navMenuIcons[5], SETTING,""));
        if (BuildConfig.isDrawerSignOut){
            navDrawerItems.add(new NavDrawerItem(leftSliderData[7], navMenuIcons[7], SIGNOUT,""));
        }

    }

    private void InitializeNavigationDrawerPatient() {
        navDrawerItems = new ArrayList<NavDrawerItem>();
        leftSliderData = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().getStringArray(R.array.nav_drawer_icons);

        if (BuildConfig.isAppointmentEnabled) {
           // navDrawerItems.add(new NavDrawerItem(leftSliderData[4], navMenuIcons[11], HOMEDRAW,"PhloobaHomeFragment"));
          //  navDrawerItems.add(new NavDrawerItem(leftSliderData[9], navMenuIcons[9], HOMEDRAWREQUESTS, "HomeDrawListingFragment"));
            navDrawerItems.add(new NavDrawerItem(leftSliderData[12], navMenuIcons[9], APPOINTMENT, "MyAppointmentFragment"));
        }
        // navDrawerItems.add(new NavDrawerItem(leftSliderData[10], navMenuIcons[10], PROFILES,"WhosPatientFragment" ));

        if(BuildConfig.isToShowTiles)
            navDrawerItems.add(new NavDrawerItem(leftSliderData[11],navMenuIcons[0], HOME,"Home"));

        navDrawerItems.add(new NavDrawerItem(leftSliderData[0], navMenuIcons[0], MYHEALTH,"MyHealth"));
        if (BuildConfig.isChatEnabled) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[1], navMenuIcons[1], CHAT, "ChatFragment"));
        }

        if (BuildConfig.isVaccinationEnabled && BuildConfig.isPatientApp) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[3], navMenuIcons[3], VACCINATION,"VaccinationListFragment"));
        }

        if (BuildConfig.isWhatsNextEnabled && BuildConfig.isPatientApp) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[6], navMenuIcons[6], WHATSNEXT,"CareTaskFragment"));
        }

        if (BuildConfig.isCarePlanEnabled && BuildConfig.isPatientApp) {
            navDrawerItems.add(new NavDrawerItem(leftSliderData[7], navMenuIcons[7], CAREPLAN,"CarePlanFragment"));
        }

        navDrawerItems.add(new NavDrawerItem(leftSliderData[2], navMenuIcons[2], NOTIFICATION,"NotificationCenterFragment"));
        navDrawerItems.add(new NavDrawerItem(leftSliderData[5], navMenuIcons[5], SETTING,"Settings"));
        if (BuildConfig.isDrawerSignOut){
            navDrawerItems.add(new NavDrawerItem(leftSliderData[8], navMenuIcons[8], SIGNOUT,""));
        }
        NavDrawerItem nav = null;
        for(int i=0;i<navDrawerItems.size();i++){
            if(navDrawerItems.get(i).getIdentifier()==HOMEINDEX)
            {
                nav= navDrawerItems.get(i);
                navDrawerItems.remove(i);
                break;
            }
        }
        if(nav!=null)
            navDrawerItems.add(0,nav);
        else
            nav = navDrawerItems.get(0);
        homeFragmentClassName = nav.getClassname();
    }


    public void onItemClickPatient(AdapterView<?> parent, View view, int position, long id, DrawerLayout drawerLayout, Fragment fragment, int currentFragment, Context context) {
        Utils.hideKeyboard(this);


        if (fragment != null && fragment instanceof MyHealth) {
            ((MyHealth) fragment).hideAdvanceSearchFormIfOpen();
        }
        item_clicked=navDrawerItems.get(position).getIdentifier();
        //instead of text comparing identifier
        if (currentFragment != -1 && (item_clicked == currentFragment)){
            drawerLayout.closeDrawers();
            return;
        }
         /*Launcher change add select item for launcher*/
        if (context instanceof BaseActivity)
            ((com.mphrx.fisike.utils.BaseActivity) context).selectItem(item_clicked, null);

    }

    /*
    this method will display side drawer open when app is installed for the first time
    otherwise close the drawer if it is open
    */
    public void showDrawer(final DrawerLayout drawerLayout) {
        if (!SharedPref.isSideDrawerShown()) {
            drawerLayout.openDrawer(GravityCompat.START);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawerLayout.closeDrawers();
                    SharedPref.setSideDrawerShown(true);
                }
            }, TextConstants.TIME_TO_DISPLAY_DRAWER_DEMO * 1000);

        } else if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }

    }


    public void onItemClickPhysician(AdapterView<?> parent, View view, int position, long id, DrawerLayout drawerLayout) {
        Utils.hideKeyboard(this);

        //  openActivity(com.mphrx.fisike.UserProfileActivity.class, null, false);
        item_clicked = navDrawerItems.get(position).getIdentifier();
        if (item_clicked == currentItem) {
            drawerLayout.closeDrawers();
            return;
        }
        selectItemPhysician(item_clicked);
        drawerLayout.closeDrawers();

    }


    public void selectItemPhysician(int position) {
        if(position!=SIGNOUT) {
            item_clicked = position; /*navDrawerItems.get(position).getIdentifier();*/
        }else{
            DialogUtils.showAlertDialogCommon(this, this.getResources().getString(R.string.confirm_signout), getLogoutMessageConfigBased(), getString(R.string.proceed),getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        mService.logoutUser(NavigationDrawerActivity.this);
                    } else {
                        dialog.dismiss();
                    }
                }
            });
            return;
        }
        if (!BuildConfig.isSearchPatient && !(this instanceof ContactActivity)) {
            Utils.UpdateSelectItemFromNavigationDrawer(true, item_clicked);
            this.finish();
            return;
        } else if (BuildConfig.isSearchPatient && !(this instanceof SearchPatientFilterActivity) && position!=SIGNOUT) {
            Utils.UpdateSelectItemFromNavigationDrawer(true, item_clicked);
            this.finish();
        }
        switch (item_clicked) {

            /*launcher change -add launcher fragment click*/
            case SEARCHPATIENTS:
                currentItem = SEARCHPATIENTS;
                Bundle bundle_search_patient = new Bundle();
                openActivity(SearchPatientFilterActivity.class, bundle_search_patient, false);
                break;
            case MYCONTACTS:
                currentItem = MYCONTACTS;
                Bundle bundle_mycontact = new Bundle();
                openActivity(ContactActivity.class, bundle_mycontact, false);
                break;
            case MESSAGES:
                currentItem = MESSAGES;
                Bundle bundle_message = new Bundle();
                openActivity(MessageListActivity.class, bundle_message, false);
                break;

            case INVITE:
                currentItem = INVITE;
                Bundle bundle_invite = new Bundle();
                openActivity(InviteUsersActivity.class, bundle_invite, false);
                break;

            case APPOINTMENT:
                currentItem = APPOINTMENT;
                openActivity(AppointmentActivity.class, null, false);
                break;


            case SETTING:
                currentItem = SETTING;
                openActivity(SettingActivity.class, null, false);
                break;

            case WHATSNEXT:
                currentItem = WHATSNEXT;
                Bundle bundle=new Bundle();
                bundle.putBoolean(CareTaskActivity.IS_FROM_DRAWER,true);
                openActivity(CareTaskActivity.class,bundle,false);
                break;

            case SEARCH_RESULT:
                currentItem=SEARCH_RESULT;
                openActivity(ResultActivity.class,null,false);
                break;

            case MYSCHEDULE:
                currentItem = MYSCHEDULE;
                openActivity(MyScheduleActivity.class, null, false);
                break;

        }
        Utils.UpdateSelectItemFromNavigationDrawer(false, -1);

    }


    public String getLogoutMessageConfigBased() {
        if (BuildConfig.isFisike) {
            return this.getResources().getString(R.string.logout_msg);
        } else
            return this.getResources().getString(R.string.hard_logout_msg);

    }
}