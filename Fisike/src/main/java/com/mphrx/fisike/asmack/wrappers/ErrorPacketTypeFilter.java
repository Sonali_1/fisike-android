package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;

public class ErrorPacketTypeFilter implements PacketFilter {

    @Override
    public boolean accept(Packet packet) {
        if (!(packet instanceof IQ)) {
            return false;
        } else if (null != packet.getError()) {
            if (packet.getError().toString().contains("not-acceptable"))
                return true;
        }
        return false;
    }

}
