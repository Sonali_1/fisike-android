package com.mphrx.fisike.asmack.wrappers;

import java.util.Iterator;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.MUCAdmin;
import org.jivesoftware.smackx.packet.MUCAdmin.Item;

public class RemoveGroupMemberTypeFilter implements PacketFilter {
    private static final String SET = "set";

    @Override
    public boolean accept(Packet packet) {
        if (!(packet instanceof MUCAdmin)) {
            return false;
        }
        MUCAdmin mucAdminPacket = (MUCAdmin) packet;
        if (!(mucAdminPacket.getType().toString().equals(SET))) {
            return false;
        } else {
            return performCheckOnRole(mucAdminPacket);
        }

    }

    private boolean performCheckOnRole(MUCAdmin packet) {
        Iterator<Item> iterator = packet.getItems();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            if (item.getRole().toString().equals("")) {
                return true;
            }
        }
        return false;
    }

}
