package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.Packet;

public class GroupInvitePacket extends Packet{
	private String groupName;
	private String userType;
	private String invitedMemberJid;
	
	@Override
	public String toXML() {
	   StringBuilder buf = new StringBuilder();
	   buf.append("<message id=\"");
	   buf.append(getPacketID());
	   buf.append("\"");
	   buf.append(" to=\"");
	   buf.append(getGroupName());
	   buf.append("\"");
	   buf.append(">");
	   buf.append("<x xmlns=\"").append("http://jabber.org/protocol/muc#user\"");
	   buf.append(">");
	   buf.append("<invite to=\"");
	   buf.append(getInvitedMemberJid());
	   buf.append("\"");
	   buf.append(" userType=\"");
	   buf.append(getUserType());
	   buf.append("\"");
	   buf.append("/>");
	   buf.append("</x>");
	   buf.append("</message>");
	   return buf.toString();
	}
	
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getInvitedMemberJid() {
		return invitedMemberJid;
	}

	public void setInvitedMemberJid(String invitedMemberJid) {
		this.invitedMemberJid = invitedMemberJid;
	}

}
