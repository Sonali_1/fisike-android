package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.DiscoverItems;

public class GroupRosterPacketFilter implements PacketFilter {

    public static final String type = "result";

    @Override
    public boolean accept(Packet packet) {
        if (!(packet instanceof DiscoverItems)) {
            return false;
        } else {
            return ((DiscoverItems) packet).getType().toString().equals(type.toString());
        }
    }
}
