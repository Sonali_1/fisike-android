package com.mphrx.fisike.asmack.wrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SpoolSuccessProvider implements IQProvider {

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {
        SpoolPacketIQ spoolIQ = new SpoolPacketIQ();
        spoolIQ.setPacketIDList(parsePacketId(parser));
        return new SpoolPacketIQ();
    }

    private List<String> parsePacketId(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        List<String> packetList = new ArrayList<String>();
        boolean done = false;
        String packetId = null;

        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG && "item".equals(parser.getName())) {

            } else if (eventType == XmlPullParser.TEXT) {
                packetId = parser.getText();
            } else if (eventType == XmlPullParser.END_TAG && "item".equals(parser.getName())) {
                // add into packet arraylist
                if (packetId != null) {
                    packetList.add(packetId);
                }
            } else if (eventType == XmlPullParser.END_TAG && "query".equals(parser.getName())) {
                done = true;
            }
        }
        return packetList;
    }
}
