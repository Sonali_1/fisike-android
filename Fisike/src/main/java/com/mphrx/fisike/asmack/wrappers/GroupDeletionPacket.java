package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.Packet;

/**
 * Created by manohar on 17/02/16.
 */
public class GroupDeletionPacket extends Packet {

    public String from;
    public String id;
    public String to;

    @Override
    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq");
        buf.append(" from=\"").append(getFrom()).append("\"");
        buf.append(" id=\"").append(getId()).append("\"");
        buf.append(" to=\"").append(getTo()).append("\"");
        buf.append(" type=\"set\">");
        buf.append("<query xmlns=\"").append("http://jabber.org/protocol/muc#owner\">");
        buf.append("<destroy>");
        buf.append("<reason>").append("no reason").append("</reason>");
        buf.append("</destroy>");
        buf.append("</query>");
        buf.append("</iq>");
        return buf.toString();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
