package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.packet.MUCUser;

public class InvitationTypeFilter implements PacketFilter {

    @Override
    public boolean accept(Packet packet) {
        if ((packet instanceof Message)) {
            return performChecks(packet);

        } else return false;
    }

    private boolean performChecks(Packet packet) {
        if (packet.getExtensions() != null) {
            for (PacketExtension extension : packet.getExtensions()) {
                if (!(extension instanceof MUCUser)) {
                    return false;
                }
                MUCUser user = (MUCUser) extension;
                if (user.getInvite() != null) {
                    return true;
                }
            }
        }
        return false;
    }

}
