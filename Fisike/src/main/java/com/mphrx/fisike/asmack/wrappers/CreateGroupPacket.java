package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.Packet;

public class CreateGroupPacket extends Packet {

    public String from;
    public String id;
    public String to;
    public String subject;
    public String userRole;

	@Override
    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq");
        buf.append(" from=\"").append(getFrom()).append("\"");
        buf.append(" id=\"").append(getId()).append("\"");
        buf.append(" to=\"").append(getTo()).append("\"");
        buf.append(" type=\"set\">");
        buf.append("<query xmlns=\"").append("http://jabber.org/protocol/muc#owner\">");
        buf.append("<x xmlns=\"").append("jabber:x:data\"");
        buf.append(" type=\"submit\"/>");
        buf.append("</query>");
        buf.append("<subject>");
        buf.append(getSubject());
        buf.append("</subject>");
        buf.append("<userType>");
        buf.append(getUserRole());
        buf.append("</userType>");	
        buf.append("</iq>");
        return buf.toString();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

}
