package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.Packet;

public class GroupSuccessPresence extends Packet {

    private String from;
    private String to;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toXML() {
        StringBuilder builder = new StringBuilder();
        builder.append("<presence ");
        builder.append("from=\"").append(getFrom());
        builder.append("\"");
        builder.append(" to=\"").append(getTo());
        builder.append("\"");
        builder.append("/>");
        return builder.toString();
    }

}
