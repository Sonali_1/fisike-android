package com.mphrx.fisike.asmack.wrappers;

import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;


public class SpoolPacketIQ extends IQ {
    private static final String ACTION_DELETE = "delete";
    private static final String SPOOL = "spool";
    public Type type;
    private List<String> packetIDList;

    @Override
    public String getChildElementXML() {
        return "jabber:iq:private";
    }

    @Override
    public synchronized String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq ");
        if (getPacketID() != null) {
            buf.append("id=\"" + getPacketID() + "\" ");
        }
        if (getTo() != null) {
            buf.append("to=\"").append(StringUtils.escapeForXML(getTo())).append("\" ");
        }
        if (getFrom() != null) {
            buf.append("from=\"").append(StringUtils.escapeForXML(getFrom())).append("\" ");
        }
        if (type == null) {
            buf.append("type=\"set\">");
        } else {
            buf.append("type=\"").append(getType()).append("\">");
        }
        // Add the query section if there is one.
        String queryXML = getChildElementXML();
        if (queryXML != null) {
            buf.append("<query xmlns=");
            buf.append("\"");
            buf.append(queryXML);
            buf.append("\"");
            buf.append(" action=");
            buf.append("\"");
            buf.append(ACTION_DELETE);
            buf.append("\"");
            buf.append(">");
        }	
        if (getPacketIDList() != null) {
        	Iterator<String> iter = packetIDList.iterator();
        	while (iter.hasNext()) {
                buf.append("<item from=");
                buf.append("\"");
                buf.append(SPOOL);
                buf.append("\"");
                buf.append(">");
                buf.append(iter.next());
                buf.append("</item>");
            }
        }
        buf.append("</query>");
        // add  items
        // Add the error sub-packet, if there is one.
        XMPPError error = getError();
        if (error != null) {
            buf.append(error.toXML());
        }
        buf.append("</iq>");
        return buf.toString();
    }

    public synchronized List<String> getPacketIDList() {
        return packetIDList;
    }

    public synchronized void setPacketIDList(List<String> messageIDList) {
        this.packetIDList =  messageIDList;
    }

    public synchronized void setPacketIdList(List<String> packetList) {
        this.packetIDList =  packetList;
    }

}
