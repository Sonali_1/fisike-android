package com.mphrx.fisike.asmack.wrappers;

import java.util.Iterator;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverInfo.Identity;

public class GroupNameTypeFilter implements PacketFilter {
    private static final String CONFERENCE = "conference";
    private static final String TEXT = "text";
    private static final String RESULT = "result";

    @Override
    public boolean accept(Packet packet) {
        if (!(packet instanceof DiscoverInfo)) {
            return false;
        }
        DiscoverInfo discoverpacket = (DiscoverInfo) packet;

        if (!(discoverpacket.getType().toString().equals(RESULT))) {
            return false;
        } else if (performCheckOnIdentity(discoverpacket)) {
            return true;
        }
        return false;
    }


    private boolean performCheckOnIdentity(DiscoverInfo identity) {
        Iterator<Identity> iterator = identity.getIdentities();
        while (iterator.hasNext()) {
            Identity member = iterator.next();
            if (member.getCategory().equals(CONFERENCE) && member.getType().equals(TEXT)) {
                return true;
            }
        }
        return false;
    }

}
