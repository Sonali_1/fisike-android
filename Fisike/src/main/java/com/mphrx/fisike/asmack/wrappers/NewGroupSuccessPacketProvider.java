package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class NewGroupSuccessPacketProvider implements IQProvider {

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {
        return new NewGroupIQ();
    }

}
