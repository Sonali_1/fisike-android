package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.IQ;

public class GroupRosterPacket extends IQ {

    @Override
    public String getChildElementXML() {
        return "<query xmlns='http://jabber.org/protocol/disco#items'/>";
    }

}
