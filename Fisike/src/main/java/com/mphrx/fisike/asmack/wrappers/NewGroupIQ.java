package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.IQ;

public class NewGroupIQ extends IQ {

    private String to;
    private String id;
    private String from;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    @Override
    public String getChildElementXML() {
        return "<query xmlns=\"http://jabber.org/protocol/muc#owner\"/>";
    }

    /**
     * Represents the type of a message.
     */
    public enum Type {

        /**
         * group successfully created.
         */
        result;

        public static Type fromString(String name) {
            try {
                return Type.valueOf(name);
            } catch (Exception e) {
                return result;
            }
        }
    }


}
