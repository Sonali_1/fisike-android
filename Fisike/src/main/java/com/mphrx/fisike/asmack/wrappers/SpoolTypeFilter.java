package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;

public class SpoolTypeFilter implements PacketFilter {
    @Override
    public boolean accept(Packet packet) {
        if ((packet instanceof SpoolPacketIQ)) {
            return true;
        } else {
            return false;
        }
    }
}


