package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;

public class NewGroupSuccessTypeFilter implements PacketFilter {
    private final NewGroupIQ.Type type;

    /**
     * Creates a new message type filter using the specified message type.
     *
     * @param type the message type.
     */
    public NewGroupSuccessTypeFilter(NewGroupIQ.Type type) {
        this.type = type;
    }

    @Override
    public boolean accept(Packet packet) {
        if (!(packet instanceof NewGroupIQ)) {
            return false;
        } else {
            return ((NewGroupIQ) packet).getType().toString().equals(this.type.toString());
        }
    }

}
