package com.mphrx.fisike.asmack.wrappers;

import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;


public class GroupImageChangePacket extends Packet {
    private static final String IMAGE_CHANGED = "Image has been changed";
    private String from;
    private String to;
    private Type type;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type groupchat) {
        this.type = groupchat;
    }

    @Override
    public String toXML() {

        StringBuilder buf = new StringBuilder();
        buf.append("<message");
        buf.append(" from=\"").append(getFrom()).append("\"");
        if (getPacketID() != null) {
            buf.append(" id=\"").append(getPacketID()).append("\"");
        }
        buf.append(" to=\"").append(getTo()).append("\"");
        buf.append(" type=\"");
        buf.append(getType() + "\"");
        buf.append(">");
        buf.append("<image>");
        buf.append(IMAGE_CHANGED);
        buf.append("</image>");
        buf.append("</message>");
        return buf.toString();
    }


}
