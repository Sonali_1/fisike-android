package com.mphrx.fisike;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.adapter.DosageRecyclerAdapter;
import com.mphrx.fisike.adapter.GridButtonAdapter;
import com.mphrx.fisike.adapter.MedicalConditionsAdapter;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.asynctask.SaveMedicationOrder;
import com.mphrx.fisike.beans.DoseBean;
import com.mphrx.fisike.constant.MedicationConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.MedicationDurationEnum;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Days;
import com.mphrx.fisike.utils.MapHolder;
import com.mphrx.fisike.utils.NotifyingScrollView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

import appointment.request.PlaceTestOrderRequest;
import appointment.utils.CommonTasks;

import static com.mphrx.fisike.R.array;
import static com.mphrx.fisike.R.layout;
import static com.mphrx.fisike.R.string;

/**
 * Created by Laxman Singh on 20/10/2016.
 */
public class MedicationActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener,
        RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener, NotifyingScrollView.OnScrollChangedListener {
    private Context mContext;
    private DatePickerDialog datePickerDialog;
    private String dateType;
    private View repeat_line_view;
    private int previousSelectedPosition = -1;
    private int daysRepitition = 3;
    String[] dosage_frequency;
    private CustomFontTextView st_days;
    private static final int DOCTOR_NAME_MAX_LENGTH = 50;
    public static ArrayList<Days> days = new ArrayList<Days>();

    private Toolbar mToolbar;
    private NotifyingScrollView scrollView;
    private LinearLayout llayoutRepeatContainer, llParentRepeatLayout;
    private RadioGroup rgRepitition;
    private RadioButton rbDays, rbInterval, rbEveryday;
    private IconTextView btMinus;
    private IconTextView btPlus;
    private ImageButton btnBack;
    private IconTextView btnSubmit, btnSearch;
    private SwitchCompat mSwitch;
    private TextView tvNoOfDays;
    private CustomFontTextView mSpinner;
    private ClearableEditText etDoctorName;
    private ClearableEditText etRemark;
    private RecyclerView rvRecyclerView;
    private CustomFontTextView tvSelectedDate, tvEndDate, tvEveryday, tvInterval, tvDays;
    private GridView gvRepeatDays;
    private RelativeLayout cvRepeatLayout;
    private DosageRecyclerAdapter rvDosageAdapter;

    private HashMap<String, Integer> medicineHashMap;
    private ArrayList<DiseaseMO> diseaseMoList = new ArrayList<DiseaseMO>();
    private ArrayList<DoseBean> dosageList;


    private Boolean isUpdateFlow = false, isUpdatePrescription = false;
    private Boolean isClone = false;

    private GridButtonAdapter gridButtonAdapter;
    private PrescriptionModel clickedPrescriptionModel;
    private PrescriptionModel prescriptionModel;
    private boolean isSosSelected;
    private PopupWindow mPopup;
    private LinearLayout layoutEndDate, layoutStartDate;
    private CustomFontTextView txtEndDate;
    private LinearLayout layoutToolbar;

    private Drawable mActionBarBackgroundDrawable;
    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener;
    private FrameLayout frameLayout;

    private CardView cardview_medicalcondition, cardViewPhysicianName;
    private CardView card_view_duration;
    private EditText mEtMedicineSearch;
    private RecyclerView mRv_medicalConditions;
    private MedicalConditionsAdapter mMedicalConditionsAdapter;
    private CustomFontButton btnSave;
    private IconTextView btnCross;
    private CustomFontTextView txtStartDate;
    private int medicineIDSearch;
    private View frequency_line_view;
    private View remark_line_view;
    private CustomFontTextView tvRemark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        mContext = this;
        if (BuildConfig.isPatientApp)
            getLayoutInflater().inflate(R.layout.activity_medication, frameLayout);
        else
            getLayoutInflater().inflate(R.layout.activity_medication_physician, frameLayout);

        initView();
        Drawable.Callback mDrawableCallback = new Drawable.Callback() {
            @Override
            public void invalidateDrawable(Drawable who) {
                getActionBar().setBackgroundDrawable(who);
            }

            @Override
            public void scheduleDrawable(Drawable who, Runnable what, long when) {
            }

            @Override
            public void unscheduleDrawable(Drawable who, Runnable what) {
            }
        };
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }

        initializeDoctorField(etDoctorName);
        setDaysList(true);
        initRepititionDaysLayout();
        setAllDays(true);
       /* [ End of To initialize specific days or everyday]*/
        if (BuildConfig.isPatientApp) {
            mActionBarBackgroundDrawable.setAlpha(0);
            initializeRemarkField(etRemark);
            btnSearch.setVisibility(View.VISIBLE);
            layoutEndDate.setOnClickListener(this);
            layoutStartDate.setOnClickListener(this);
            mEtMedicineSearch.setOnClickListener(this);
            btnSave.setOnClickListener(this);
            CharSequence[] arrayPopup = getResources().getTextArray(array.dosage_frequency);
            mSpinner.setText(arrayPopup[0].toString());
            mSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopUpMenu(mSpinner);
                }
            });
            rgRepitition.setOnCheckedChangeListener(this);
            btPlus.setOnClickListener(this);
            btMinus.setOnClickListener(this);
            mSwitch.setOnCheckedChangeListener(this);
            layoutEndDate.setEnabled(false);
            txtEndDate.setEnabled(false);
            setGridviewItemClickListener();
        } else {
            btnSubmit.setVisibility(View.GONE);
            mSpinner.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txtEndDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txtStartDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        isClone = getIntent().getBooleanExtra(getString(string.txt_CLONE), false);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(TextConstants.MEDICATION_PRESCRIPTION_INTENT)) {
            clickedPrescriptionModel = (PrescriptionModel) getIntent().getSerializableExtra(TextConstants.MEDICATION_PRESCRIPTION_INTENT);
            isUpdateFlow = true;
            if (isClone)
                isUpdatePrescription = false;
            else
                isUpdatePrescription = true;
            setData(clickedPrescriptionModel);

            if (isUpdatePrescription) {
                ((CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(string.toolbar_title_update_medicine);
            } else if (isClone) {
                ((CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(string.toolbar_title_clone_medicine);
            }

            invalidateOptionsMenu();
        } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(TextConstants.MEDICATION_READ_MODE)) {
            clickedPrescriptionModel = (PrescriptionModel) getIntent().getSerializableExtra(TextConstants.MEDICATION_READ_MODE);
            setData(clickedPrescriptionModel);
            ((CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.view_medication));
        } else {
            isUpdateFlow = false;
            isUpdatePrescription = false;
            dosage_frequency = getResources().getStringArray(R.array.dosage_frequency);
            dosageList = (ArrayList<DoseBean>) MapHolder.getInstance().getdoseMap(TextConstants.ADD_MEDICINE, mContext).get(dosage_frequency[0]);
            rvDosageAdapter = new DosageRecyclerAdapter(MedicationActivity.this, dosageList);
            rvRecyclerView.setAdapter(rvDosageAdapter);
            String date = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(new Date());
            tvSelectedDate.setText(date);
            mSwitch.setChecked(true);
            if (mSwitch.isChecked()) {
                tvEndDate.setText("");
                layoutEndDate.setEnabled(false);
                txtEndDate.setEnabled(false);
            } else {
                layoutEndDate.setEnabled(true);
                txtEndDate.setEnabled(true);
            }
            invalidateOptionsMenu();
        }
    }

    private void setGridviewItemClickListener() {
        gvRepeatDays.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (rgRepitition.getCheckedRadioButtonId() == rbDays.getId()) {
                    Days currentSelected = days.get(position);
                    String day = currentSelected.getDayOfWeek();
                    boolean isSelected = currentSelected.isSelected();
                    LinearLayout linearLayout = (LinearLayout) view;
                    CustomFontTextView btDays = (CustomFontTextView) linearLayout.getChildAt(0);
                    if (isSelected) {
                        previousSelectedPosition = position;
                        currentSelected.setIsSelected(false);
                    } else {
                        currentSelected.setIsSelected(true);
                        previousSelectedPosition = position;
                    }
                    gridButtonAdapter.setButtonState(currentSelected.isSelected(), btDays, linearLayout);
                } else if (rgRepitition.getCheckedRadioButtonId() == rbInterval.getId()) {
                    rbDays.setSelected(true);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setData(PrescriptionModel clickedPrescriptionModel) {
        mEtMedicineSearch.setText(clickedPrescriptionModel.getMedicineName() != null ? clickedPrescriptionModel.getMedicineName() : "");
        medicineHashMap.put(clickedPrescriptionModel.getMedicineName(), clickedPrescriptionModel.getMedicianId());

        ArrayList<MedicineDoseFrequencyModel> clickedDoseFrequencyModelList = clickedPrescriptionModel.getArrayDose();
        if (BuildConfig.isPatientApp) {
            if (Utils.isValueAvailable(clickedPrescriptionModel.getPractitionerName()))
                etDoctorName.getClearTextButton().setVisibility(View.VISIBLE);
            String remark = clickedPrescriptionModel.getRemark();
            if (Utils.isValueAvailable(remark)) {
                etRemark.setText(remark);
                etRemark.getClearTextButton().setVisibility(View.VISIBLE);
            }
            setFrequencySwitch(clickedPrescriptionModel.getEndDate().trim(), clickedPrescriptionModel.getStartDate().trim());
        } else {
            etDoctorName.getClearTextButton().setVisibility(View.GONE);
            String remark = clickedPrescriptionModel.getRemark();
            if (Utils.isValueAvailable(remark))
                tvRemark.setText(remark);
            else
                tvRemark.setText(getResources().getString(R.string.no_comments));
            setDatesForPhysician(clickedPrescriptionModel.getEndDate().trim(), clickedPrescriptionModel.getStartDate().trim());
        }
        etDoctorName.setText(clickedPrescriptionModel.getPractitionerName());

        if (clickedPrescriptionModel.getDiseaseMoList() != null) {
            for (DiseaseMO diseaseMO : clickedPrescriptionModel.getDiseaseMoList()) {
                diseaseMoList.add(diseaseMO);
            }
        }
        mMedicalConditionsAdapter.notifyDataSetChanged();

        if (clickedDoseFrequencyModelList != null) {
            if (clickedDoseFrequencyModelList.size() - 1 == 0 && clickedPrescriptionModel.getDoseInstruction() != null && clickedPrescriptionModel.getDoseInstruction().equalsIgnoreCase("SOS")) {
                isSosSelected = true;
            }
            setRepeatContainer(clickedDoseFrequencyModelList);
            dosage_frequency = getResources().getStringArray(R.array.dosage_frequency);
            populateDosageList(clickedDoseFrequencyModelList);
            rvDosageAdapter = new DosageRecyclerAdapter(MedicationActivity.this, dosageList);
            rvRecyclerView.setAdapter(rvDosageAdapter);
            if (isSosSelected) {
                mSpinner.setText(dosage_frequency[dosage_frequency.length - 1]);

            } else {
                mSpinner.setText(dosage_frequency[clickedDoseFrequencyModelList.size() - 1]);
            }
        }

    }

    private void setDatesForPhysician(String endDate, String startDate) {
        String frequency = Utils.calculateDateInWeeks(endDate, startDate);
        if (frequency != null && frequency.equals(getResources().getString(string.forever_text))) {
            txtEndDate.setText("");
            tvEndDate.setText(getResources().getString(string.forever_text));
        } else {
            String displayEndDate = DateTimeUtil.calculateDate(endDate, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate,
                    DateTimeUtil.destinationDateFormatWithoutTime);
            tvEndDate.setText(displayEndDate);
        }
        String date = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime).format(new Date());
        tvSelectedDate.setText(date);
    }

    private void populateDosageList(ArrayList<MedicineDoseFrequencyModel> clickedDoseFrequencyModelList) {
        dosageList = new ArrayList<DoseBean>();
        int i = 0;
        for (MedicineDoseFrequencyModel clickedMedicationDoseFrequencyModel : clickedDoseFrequencyModelList) {
            //setting dose bean language specific
            String displayedUnit = clickedMedicationDoseFrequencyModel.getDoseUnit();
            displayedUnit = MedicationUnitsEnum.getDisplayedValuefromCode(displayedUnit);
            String AM_PM_String = clickedMedicationDoseFrequencyModel.getDoseTimeAMorPM();
            AM_PM_String = MedicationFrequancyEnum.getDisplayedValuefromCode(AM_PM_String);
            int hr = Integer.parseInt(clickedMedicationDoseFrequencyModel.getDoseHours());
            if (AM_PM_String != null && AM_PM_String.equalsIgnoreCase(TextConstants.PM)) {
                hr += 12;
            } else if (AM_PM_String != null && AM_PM_String.equalsIgnoreCase(TextConstants.AM) && hr == 12) {
                hr -= 12;
            }
            DoseBean doseBean = new DoseBean(clickedMedicationDoseFrequencyModel.getDoseQuantity() + "",
                    displayedUnit, Integer.toString(hr),
                    getValue(Integer.parseInt(clickedMedicationDoseFrequencyModel.getDoseMinutes() == null ? "0" : clickedMedicationDoseFrequencyModel.getDoseMinutes())),
                    clickedMedicationDoseFrequencyModel.getDoseIntakeTimeInstruction(), isSosSelected);
            dosageList.add(i, doseBean);
            i++;
        }
    }

    private String getValue(int val) {
        if (val == 0)
            return getResources().getString(string.number_00);
        if (val == 1)
            return getResources().getString(string.number_01);
        if (val == 2)
            return getResources().getString(string.number_02);
        if (val == 3)
            return getResources().getString(string.number_03);
        if (val == 4)
            return getResources().getString(string.number_04);
        if (val == 5)
            return getResources().getString(string.number_05);
        if (val == 6)
            return getResources().getString(string.number_06);
        if (val == 7)
            return getResources().getString(string.number_07);
        if (val == 8)
            return getResources().getString(string.number_08);
        if (val == 9)
            return getResources().getString(string.number_09);
        return val + "";
    }

    // sets the data in repeats container
    private void setRepeatContainer(ArrayList<MedicineDoseFrequencyModel> clickedDoseFrequencyModelList) {
        MedicineDoseFrequencyModel clickedItemArrayListFirstItem = clickedDoseFrequencyModelList.get(0);
        if (clickedItemArrayListFirstItem.isRepeatAfterEverySelected() && !BuildConfig.isPatientApp) {
            llayoutRepeatContainer.setVisibility(View.VISIBLE);
            tvDays.setVisibility(View.GONE);
            tvEveryday.setVisibility(View.GONE);
            tvNoOfDays.setVisibility(View.VISIBLE);
            tvNoOfDays.setText(clickedItemArrayListFirstItem.getRepeatDaysInterval() + "");
        } else if (clickedItemArrayListFirstItem.isRepeatAfterEverySelected()) {
            (rgRepitition).clearCheck();
            rbInterval.setFocusable(true);
            rbInterval.setChecked(true);
            tvNoOfDays.setText(clickedItemArrayListFirstItem.getRepeatDaysInterval() + "");
        } else {
            if (BuildConfig.isPatientApp) {
                (rgRepitition).clearCheck();
                rbDays.setChecked(true);
            }
            ArrayList<String> doseWeekdaysList = clickedItemArrayListFirstItem.getDoseWeekdaysList();
            for (int i = 0; doseWeekdaysList != null && i < days.size(); i++) {
                if (doseWeekdaysList.contains(days.get(i).getDayOfWeek())) {
                    // set the selected status to true
                    days.get(i).setIsSelected(true);
                } else {
                    // set the selected status to false
                    days.get(i).setIsSelected(false);
                }
            }
            ArrayList<Days> tempDays = new ArrayList<>();
            tempDays.addAll(days);
            setDaysType(tempDays);
            gridButtonAdapter.setWeekDays(tempDays);
            gridButtonAdapter.notifyDataSetChanged();
            gvRepeatDays.invalidateViews();
        }

    }

    /*send the start date and end date in yyyy-mm-dd format and sets in ui in dd MMM yyyy*/
    private void setFrequencySwitch(String endDate, String startDate) {
        String frequency = Utils.calculateDateInWeeks(endDate, startDate);
        if (frequency != null && frequency.equals(MedicationDurationEnum.FOREVER.getCode())) {
            // set the goes on Forever as checked
            mSwitch.setChecked(true);
            tvEndDate.setText("");
            layoutEndDate.setEnabled(false);
            txtEndDate.setEnabled(false);
        } else {
            mSwitch.setChecked(false);
            String displayEndDate = DateTimeUtil.calculateDate(endDate,
                    DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate,
                    DateTimeUtil.destinationDateFormatWithoutTime);
            tvEndDate.setText(displayEndDate);
            layoutEndDate.setEnabled(true);
            txtEndDate.setEnabled(true);
        }
        tvSelectedDate.setText(
                (DateTimeUtil.calculateDate(startDate,
                        DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate,
                        DateTimeUtil.destinationDateFormatWithoutTime)));
    }

    private void initRepititionDaysLayout() {
        gridButtonAdapter = new GridButtonAdapter(MedicationActivity.this, days);
        gvRepeatDays.setAdapter(gridButtonAdapter);
    }

    private void initView() {
        Typeface font_regular = Typeface.createFromAsset(getAssets(), "sans/OpenSans-Regular.ttf");
        Typeface font = Typeface.createFromAsset(getAssets(), "sans/OpenSans-Semibold.ttf");

        mOnScrollChangedListener = MedicationActivity.this;
        scrollView = (NotifyingScrollView) findViewById(R.id.scroll_view);
        scrollView.setFocusable(true);
        scrollView.setFocusableInTouchMode(true);
        scrollView.fullScroll(View.FOCUS_UP);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (Utils.isKeyBoardOpen(MedicationActivity.this)) {
                    Utils.hideKeyboard(MedicationActivity.this);
                }
                return false;
            }
        });
        mActionBarBackgroundDrawable = new ColorDrawable(getResources().getColor(R.color.action_bar_bg));
        layoutToolbar = (LinearLayout) findViewById(R.id.layoutToolbar);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        ((CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.toolbar_title_add_medicine);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setBackgroundDrawable(mActionBarBackgroundDrawable);
        repeat_line_view = (View) findViewById(R.id.repeat_line_view);
        frequency_line_view = (View) findViewById(R.id.frequency_line_view);
        remark_line_view = (View) findViewById(R.id.remark_line_view);
        cardview_medicalcondition = (CardView) findViewById(R.id.cardview_medicalcondition);
        card_view_duration = (CardView) findViewById(R.id.card_view_duration);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnSearch = (IconTextView) findViewById(R.id.btnSearch);
        btnBack.setOnClickListener(this);
        btnBack.setVisibility(View.GONE);
        layoutEndDate = (LinearLayout) findViewById(R.id.layoutEndDate);
        layoutStartDate = (LinearLayout) findViewById(R.id.layoutStartDate);
        txtEndDate = (CustomFontTextView) findViewById(R.id.txtEndDate);
        txtStartDate = (CustomFontTextView) findViewById(R.id.txtStartDate);
        st_days = (CustomFontTextView) findViewById(R.id.st_days);
        mEtMedicineSearch = (EditText) findViewById(R.id.etMedicineSearch);
        btnCross = (IconTextView) findViewById(R.id.btnCross);
        mEtMedicineSearch.setTypeface(font_regular);
        llayoutRepeatContainer = (LinearLayout) findViewById(R.id.ll_repeat_container);
        llayoutRepeatContainer.setVisibility(View.GONE);
        mSpinner = (CustomFontTextView) findViewById(R.id.spinner);
        gvRepeatDays = (GridView) findViewById(R.id.gridview);
        gvRepeatDays.setVisibility(View.GONE);
        etDoctorName = (ClearableEditText) findViewById(R.id.et_doctor_name);
        tvSelectedDate = (CustomFontTextView) findViewById(R.id.tv_selected_date);
        tvNoOfDays = (TextView) findViewById(R.id.tv_number_of_days);
        tvEndDate = (CustomFontTextView) findViewById(R.id.tv_end_date);
        btnSubmit = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        btnSubmit.setVisibility(View.GONE);
        btPlus = (IconTextView) findViewById(R.id.bt_plus);
        btMinus = (IconTextView) findViewById(R.id.bt_minus);
        cardViewPhysicianName = (CardView) findViewById(R.id.physician_name_card_view);
        rvRecyclerView = (RecyclerView) findViewById(R.id.rv_dosage);
        cvRepeatLayout = (RelativeLayout) findViewById(R.id.cvRepeatLayout);
        rvRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), 1, false));
        rvRecyclerView.setNestedScrollingEnabled(false);
        medicineHashMap = new HashMap<String, Integer>();
        mRv_medicalConditions = (RecyclerView) findViewById(R.id.rv_medicalConditions);
        mRv_medicalConditions.setLayoutManager(new LinearLayoutManager(mContext));
        mRv_medicalConditions.setNestedScrollingEnabled(false);
        mMedicalConditionsAdapter = new MedicalConditionsAdapter(mContext, diseaseMoList);
        mRv_medicalConditions.setAdapter(mMedicalConditionsAdapter);
        if (BuildConfig.isPatientApp) {
            mSwitch = (SwitchCompat) findViewById(R.id.swForever);
            btnSave = (CustomFontButton) findViewById(R.id.btnSubmit);
            rgRepitition = (RadioGroup) findViewById(R.id.hour_radio_group);
            rbDays = (RadioButton) findViewById(R.id.rb_days);
            rbEveryday = (RadioButton) findViewById(R.id.rb_everyday);
            rbInterval = (RadioButton) findViewById(R.id.rb_interval);
            etRemark = (ClearableEditText) findViewById(R.id.ed_remark);
            rbDays.setTypeface(font);
            rbEveryday.setTypeface(font);
            rbInterval.setTypeface(font);
            scrollView.setOnScrollChangedListener(mOnScrollChangedListener);
        } else {
            mEtMedicineSearch.setTypeface(font);
            llParentRepeatLayout = (LinearLayout) findViewById(R.id.ll_repeatlayout);
            tvDays = (CustomFontTextView) findViewById(R.id.tv_days);
            tvEveryday = (CustomFontTextView) findViewById(R.id.tv_everyday);
            tvInterval = (CustomFontTextView) findViewById(R.id.tv_interval);
            tvRemark = (CustomFontTextView) findViewById(R.id.tv_remark);
            tvDays.setTypeface(font);
            tvEveryday.setTypeface(font);
            tvInterval.setTypeface(font);
        }

        if (!BuildConfig.isEnablePractitionerNameOnMedication || (!BuildConfig.isPatientApp && clickedPrescriptionModel != null && !Utils.isValueAvailable(clickedPrescriptionModel.getPractitionerName()))) {
            cardViewPhysicianName.setVisibility(View.GONE);
        }
        if (!BuildConfig.isEnableMedicationCondition) {
            cardview_medicalcondition.setVisibility(View.GONE);
        }
    }

    private void initializeDoctorField(ClearableEditText etDoctorName) {
        etDoctorName.setEditTextSize(14);
        etDoctorName.getEditText().setSingleLine();
        etDoctorName.getEditText().setFocusableInTouchMode(true);
        etDoctorName.getEditText().setFocusable(true);
        etDoctorName.setMaxLength(DOCTOR_NAME_MAX_LENGTH);
        etDoctorName.setIsAddTxtVisible(false);

    }

    private void initializeRemarkField(ClearableEditText clearableEditText) {
        clearableEditText.setEditTextSize(14);
        clearableEditText.getEditText().setSingleLine(false);
        clearableEditText.getEditText().setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        clearableEditText.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        clearableEditText.getEditText().setMinLines(1);
        clearableEditText.getEditText().setVerticalScrollBarEnabled(true);
        clearableEditText.getEditText().setMovementMethod(ScrollingMovementMethod.getInstance());
        clearableEditText.getEditText().setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        clearableEditText.setIsAddTxtVisible(false);

        if (Utils.isRTL(mContext)) {
            int paddingDp = CommonTasks.getDensityPixel(mContext, 30);
            clearableEditText.setMargins(paddingDp, 0, 14, 22);
        } else {
            int paddingDp = CommonTasks.getDensityPixel(mContext, 35);
            clearableEditText.setMargins(14, 0, paddingDp, 22);
        }
    }

    private boolean isDiseaseAlreadyAdded(String diseaseName) {
        String enteredDiseaseName = diseaseName.trim().toUpperCase();
        for (int i = 0; i < diseaseMoList.size(); i++) {
            if (diseaseMoList.get(i).getText().toString().trim().toUpperCase().equals(enteredDiseaseName)) {
                return true;
            }
        }
        return false;
    }

    private void showPopUpMenu(final View view) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        list.setDivider(null);
        final Resources resources = getResources();
        CharSequence[] arrayPopup = getResources().getTextArray(array.dosage_frequency);
        String[] arrayPopupString = new String[arrayPopup.length];
        for (int i = 0; i < arrayPopup.length; i++) {
            arrayPopupString[i] = arrayPopup[i].toString();
        }
        final StatusAdapter adapter = new StatusAdapter(this, R.layout.text_status, arrayPopupString);

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemClicked = adapter.getItem(position);
                mSpinner.setText(itemClicked);
                if (mPopup != null && mPopup.isShowing()) {
                    mPopup.dismiss();
                }
                onItemSelected(position);
            }
        });


        int width = CommonTasks.getDensityPixel(mContext, 210);
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setFocusable(true);
        mPopup.setOutsideTouchable(true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.showAsDropDown(view, (int) Utils.convertDpToPixel(0, this), -(int) Utils.convertDpToPixel(0, this));
    }

    public void onItemSelected(int position) {
        String item = dosage_frequency[position];
        String itemCode = item;
        try {
            itemCode = MedicationFrequancyEnum.getCodeFromValue(item);
        } catch (Exception e) {

        }

        if (itemCode.equalsIgnoreCase(getResources().getString(R.string.conditional_sos))) {
            cvRepeatLayout.setVisibility(View.GONE);
            repeat_line_view.setVisibility(View.INVISIBLE);
        } else {
            cvRepeatLayout.setVisibility(View.VISIBLE);
            repeat_line_view.setVisibility(View.VISIBLE);
            if (isSosSelected) {
                setDaysList(true);
                ArrayList<Days> tempDays = new ArrayList<>();
                tempDays.addAll(days);
                gridButtonAdapter.setWeekDays(tempDays);
                gridButtonAdapter.notifyDataSetChanged();
                isSosSelected = false;
            }
        }
        if (!isUpdateFlow) {
            // new medication order flow
            dosageList = (ArrayList<DoseBean>) MapHolder.getInstance().getdoseMap(TextConstants.ADD_MEDICINE, mContext).get(item);
        } else {
            isUpdateFlow = false;
            if (dosageList == null) {
                dosageList = (ArrayList<DoseBean>) MapHolder.getInstance().getdoseMap(TextConstants.UPDATE_MEDICINE, mContext).get(item);
            } else {
                dosageList = (ArrayList<DoseBean>) MapHolder.getInstance().getdoseMap(TextConstants.UPDATE_MEDICINE, mContext).get(item);
            }
        }
        rvDosageAdapter.updateList(dosageList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutStartDate:
                dateType = getResources().getString(string.txt_startDate);
                showDateDialog(tvSelectedDate);
                break;
            case R.id.layoutEndDate:
                dateType = getResources().getString(string.txt_endDate);
                showDateDialog(tvEndDate);
                break;
            case R.id.bt_plus:
                incrementRepitionDays();
                if (BuildConfig.isPatientApp)
                    rbInterval.setChecked(true);
                break;
            case R.id.bt_minus:
                decrementRepitionDays();
                if (BuildConfig.isPatientApp)
                    rbInterval.setChecked(true);
                break;
            case R.id.bt_toolbar_right:
                break;
            case R.id.etMedicineSearch:
                Intent i = new Intent(MedicationActivity.this, SearchMedicationActivity.class);
                i.putExtra("whichSearch", MedicationActivity.this.getResources().getString(R.string.search_medicine));
                i.putExtra("medicine_name", mEtMedicineSearch.getText().toString().trim());
                startActivityForResult(i, TextConstants.MEDICINE_SEARCH_INTENT);
                break;
            case R.id.btnSubmit:
                // perform validations
                if (performValidationsSuccessful()) {
                    showToolbarDialog();
                }
                break;

            case R.id.bt_toolbar_cross:
                onBackPressed();
                break;

        }
    }

    private boolean performValidationsSuccessful() {

     /*[validation for medicine name]*/
        if (mEtMedicineSearch.getText().toString() == null || mEtMedicineSearch.getText().toString().equals("")) {
            mEtMedicineSearch.setError(getResources().getString(R.string.please_enter_medicine_name));
            mEtMedicineSearch.requestFocus();
            Toast.makeText(MedicationActivity.this, this.getResources().getString(R.string.please_enter_medicine_name), Toast.LENGTH_SHORT).show();
            scrollView.smoothScrollTo(0, 0);
            return false;
        }
        /*[end of validation for medicine name]*/

        // start date
        if (tvSelectedDate.getText().toString() == null || tvSelectedDate.getText().toString().trim().equals("")) {
//            Utils.showDialogWithPositiveButton(this, false, "", getResources().getString(R.string.set_frequency_screen_start_date_validation), getResources().getString(R.string.ok));
            tvSelectedDate.setError(getResources().getString(R.string.please_enter_start_date_error));
            tvSelectedDate.requestFocus();
            scrollView.smoothScrollTo(0, card_view_duration.getTop());
            Toast.makeText(MedicationActivity.this, this.getResources().getString(R.string.startdate_bfore), Toast.LENGTH_SHORT).show();
            tvSelectedDate.requestFocus();
            return false;
        }
        String itemCode = (String) mSpinner.getText();
        try {
            itemCode = MedicationFrequancyEnum.getCodeFromValue(itemCode);
        } catch (Exception e) {
        }

        if (rgRepitition.getCheckedRadioButtonId() == rbInterval.getId()) {
        } else if (!itemCode.equalsIgnoreCase("SOS")) {
            if (getSelectedDays().size() == 0) {
                scrollView.smoothScrollTo(0, card_view_duration.getTop());
                Toast.makeText(this, this.getResources().getString(R.string.please_select_atleast_one_day), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        String endDate = tvEndDate.getText().toString().trim();
        // end date
        if (!mSwitch.isChecked() && ((endDate == null) || endDate.equals(""))) {
            scrollView.smoothScrollTo(0, card_view_duration.getTop());
            tvEndDate.setError(getResources().getString(string.please_enter_end_date_error));
            scrollView.smoothScrollTo(0, card_view_duration.getTop());
            Toast.makeText(MedicationActivity.this, this.getResources().getString(R.string.enddate_after), Toast.LENGTH_SHORT).show();
            tvEndDate.requestFocus();
            return false;
        }

        if (!mSwitch.isChecked() && rbInterval.isChecked()) {
            Date d1 = null;
            Date d2 = null;
            SimpleDateFormat format = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
            try {
                d1 = format.parse(tvSelectedDate.getText().toString().trim());
                d2 = format.parse(tvEndDate.getText().toString().trim());
            } catch (Exception Ex) {
            }

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);

            int daysset = Integer.parseInt(tvNoOfDays.getText().toString().trim());
            if (daysset > diffDays) {
                scrollView.smoothScrollTo(0, card_view_duration.getTop());
                DialogUtils.showErrorDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.error_rep_days_selected), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                return false;
            }
        }

        if (BuildConfig.isEnableMedicationCondition) {
            // disease name
            if (diseaseMoList.size() == 0) {
                scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        mMedicalConditionsAdapter.showError(mRv_medicalConditions, true);
                        scrollView.smoothScrollTo(0, cardview_medicalcondition.getTop());
                    }
                });
                return false;
            }
        }
        return true;
    }

    private void showToolbarDialog() {
        final boolean isEncounterChanged;
        final boolean isPrescriberChanged;
        final boolean isMedicationChanged;
        prescriptionModel = saveMedicationPrescription();
        if (isUpdatePrescription) {
            prescriptionModel.setID(clickedPrescriptionModel.getID());
            prescriptionModel.setEncounterID(clickedPrescriptionModel.getEncounterID());
            if (medicineIDSearch == -1)
                prescriptionModel.setMedicianId(-1);
            else
                prescriptionModel.setMedicianId(clickedPrescriptionModel.getMedicianId());
            prescriptionModel.setPractitionarID(clickedPrescriptionModel.getPractitionarID());
            prescriptionModel.setMimeType(clickedPrescriptionModel.getMimeType());
            prescriptionModel.setExtensionList(clickedPrescriptionModel.getExtensionList());
            isEncounterChanged = isEncounterChanged();

            //checking prescriber has changed or not based on configuration is enabled
            if (BuildConfig.isEnablePractitionerNameOnMedication)
                isPrescriberChanged = isPrescriberChanged();
            else
                isPrescriberChanged = false;

            isMedicationChanged = isMedicationChanged();
            if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(getResources().getString(R.string.conditional_sos))) {
                saveOrUpdateMedication(0, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
                return;
            }
        } else {
            isEncounterChanged = false;
            isPrescriberChanged = false;
            isMedicationChanged = false;
        }
        if (prescriptionModel.getDoseInstruction().equalsIgnoreCase(getResources().getString(R.string.conditional_sos))) {
            saveOrUpdateMedication(0, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
            return;
        }

        if (isUpdatePrescription) {
            int reminderSwitch;
            if (clickedPrescriptionModel.getDoseInstruction().equals(getResources().getString(R.string.conditional_sos)) && !prescriptionModel.getDoseInstruction().equals(getResources().getString(R.string.conditional_sos))) {
                reminderSwitch = 1;
            } else {
                reminderSwitch = clickedPrescriptionModel.getAutoReminderSQLBool();
            }

            if (reminderSwitch == 0 && !isEndDateExpire(prescriptionModel.getEndDate())) {
                DialogUtils.showAlertDialogCommon(mContext, mContext.getResources().getString(R.string.alert), mContext.getResources().getString(R.string.do_update_reminder_settings), mContext.getResources().getString(R.string.txt_yes), mContext.getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Dialog.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                saveOrUpdateMedication(clickedPrescriptionModel.getAutoReminderSQLBool(), isEncounterChanged, isPrescriberChanged, isMedicationChanged);
                                break;

                            case Dialog.BUTTON_POSITIVE:
                                dialog.dismiss();
                                int reminderswitch = 1;
                                saveOrUpdateMedication(reminderswitch, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
                                break;
                        }
                    }
                });
            } else {
                saveOrUpdateMedication(reminderSwitch, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
            }
            return;
        }
        if (isEndDateExpire(prescriptionModel.getEndDate())) {
            saveOrUpdateMedication(0, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
            return;
        }
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.toolbar_dialog, null);
        dialogBuilder.setView(dialogView);
        final SwitchCompat sw_toolbar_dialog_switch = (SwitchCompat) dialogView.findViewById(R.id.sw_toolbar_dialog_switch);
        dialogBuilder.setCancelable(true);
        final AlertDialog b = dialogBuilder.create();
        dialogView.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        dialogView.findViewById(R.id.done_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOrUpdateMedication(sw_toolbar_dialog_switch.isChecked() ? 1 : 0, isEncounterChanged, isPrescriberChanged, isMedicationChanged);
                b.dismiss();
            }
        });
        b.show();
    }

    private boolean isEndDateExpire(String medicationPrescriptionEndDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated);
        Date endDate = null;

        try {
            endDate = sdf.parse(medicationPrescriptionEndDate + " 23:59:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date currentDate = new Date();
        // compare the date
        if (endDate != null && endDate.compareTo(currentDate) >= 0) {
            return false;
        }
        return true;
    }

    private void saveOrUpdateMedication(int switchValue, boolean isEncounterChanged, boolean isPrescriberChanged, boolean isMedicationChanged) {
        prescriptionModel.setAutoReminderSQLBool(switchValue);
        prescriptionModel.setStatus(EncounterMedicationDBAdapter.status);

        if (medicineHashMap.containsKey(prescriptionModel.getMedicineName())) {
            prescriptionModel.setMedicianId(medicineHashMap.get(prescriptionModel.getMedicineName()));
        } else {
            prescriptionModel.setMedicianId(-1);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        String date = sdf.format(new Date());
        prescriptionModel.setUpdatedDate(date);

        if (isUpdateFlow) {
            prescriptionModel.setSourceName(clickedPrescriptionModel.getSourceName());
        }

        if (Utils.showDialogForNoNetwork(MedicationActivity.this, false)) {
            if (isUpdatePrescription) {
                new SaveMedicationOrder(MedicationActivity.this, prescriptionModel, isUpdatePrescription, isEncounterChanged, isPrescriberChanged, isMedicationChanged, clickedPrescriptionModel).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                //medication saved first
                new SaveMedicationOrder(MedicationActivity.this, prescriptionModel).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    private boolean isMedicationChanged() {
        if (clickedPrescriptionModel.getMedicineName().equalsIgnoreCase(prescriptionModel.getMedicineName())) {
            return false;
        }
        return true;
    }

    private boolean isPrescriberChanged() {
        if (!BuildConfig.isEnablePractitionerNameOnMedication) {
            return false;
        } else if (clickedPrescriptionModel.getPractitionerName() != null && clickedPrescriptionModel.getPractitionerName().equalsIgnoreCase(prescriptionModel.getPractitionerName())) {
            return false;
        }
        return true;
    }

    private boolean isEncounterChanged() {
        if (clickedPrescriptionModel.getArrayDose().size() != prescriptionModel.getArrayDose().size()) {
            return true;
        } else if (isDoseFrequencyModelChanged(clickedPrescriptionModel.getArrayDose(), prescriptionModel.getArrayDose())) {
            return true;
        }
        if (clickedPrescriptionModel.getStartDate().equalsIgnoreCase(prescriptionModel.getStartDate()) && clickedPrescriptionModel.getEndDate().equalsIgnoreCase(prescriptionModel.getEndDate())) {
            return false;
        }
        return true;
    }

    private boolean isDoseFrequencyModelChanged(ArrayList<MedicineDoseFrequencyModel> clickedDoseFrequencyModel,
                                                ArrayList<MedicineDoseFrequencyModel> prescriptionDoseFrequencyModel) {
        for (int i = 0; i < clickedDoseFrequencyModel.size(); i++) {
            if (!(clickedDoseFrequencyModel.get(i).getDoseHours().equals(prescriptionDoseFrequencyModel.get(i).getDoseHours())) || !(clickedDoseFrequencyModel.get(i).getDoseMinutes().equals(prescriptionDoseFrequencyModel.get(i).getDoseMinutes()))) {
                return true;
            }
        }
        return false;
    }

    public void saveOrUpdatePrescriptioninDB(Context context, PrescriptionModel prescriptionModel, PrescriptionModel oldPrescriptionModel) {
        // create the data model and add the reminder  check here
        boolean alarmReset = true;
        if (isUpdatePrescription) {
            if ((isMedicationChanged() || isPrescriberChanged()) && !isEncounterChanged()) {
                alarmReset = false;
            }
        } else {
            alarmReset = true;
        }
        Intent putExtra = getIntent().putExtra(VariableConstants.MEDICATION_PRESCRIPTION_MODEL, prescriptionModel);
        getIntent().putExtra(VariableConstants.MEDICATION_PRESCRIPTION_AlARM_RESET, alarmReset).putExtra(VariableConstants.REFRESH_RECORDS, true);
        setResult(RESULT_OK, putExtra);
        finish();
    }

    public void errorInSavingResult(String aVoid) {
        Intent putExtra = getIntent().putExtra(VariableConstants.ERROR_MESSAGE, aVoid).putExtra(VariableConstants.REFRESH_RECORDS, true);
        setResult(RESULT_CANCELED, putExtra);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, null);
        finish();
        super.onBackPressed();
    }

    private PrescriptionModel saveMedicationPrescription() {
        // create the model
        PrescriptionModel prescriptionModel = new PrescriptionModel();
        prescriptionModel.setMedicineName(mEtMedicineSearch.getText().toString().trim());

        ArrayList<MedicineDoseFrequencyModel> medicineDoseFrequencyList = new ArrayList<MedicineDoseFrequencyModel>();

        ArrayList<DoseBean> medicineDoseBeanList = rvDosageAdapter.getDosageList();

        String startDate = DateTimeUtil.calculateDate(tvSelectedDate.getText().toString(), DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        String endDate = "";
        if (mSwitch.isChecked()) {
            endDate = Utils.calculateEndDate(mContext, startDate, MedicationDurationEnum.FOREVER.getCode().toString());
        } else {
            endDate = DateTimeUtil.calculateDate(tvEndDate.getText().toString(), DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        }
        // iterate over the list and create MedicineDoseFrequencyModel from it.
        for (DoseBean medicineDoseBean : medicineDoseBeanList) {
            MedicineDoseFrequencyModel medicineDoseFrequencyModel = new MedicineDoseFrequencyModel();
            medicineDoseFrequencyModel.setDoseQuantity(Double.parseDouble(medicineDoseBean.getAmount()));
            //sending Tablet(count) if tablet is selected by default
            String unit = MedicationUnitsEnum.getCodeFromDisplayedValue(medicineDoseBean.getUnits());

            if (unit.equals(getString(R.string.tablet)))
                unit = getString(string.tablet_count_key);

            medicineDoseFrequencyModel.setDoseUnit(unit);
            medicineDoseFrequencyModel.setDoseHours(medicineDoseBean.getTime());
            medicineDoseFrequencyModel.setDoseMinutes(medicineDoseBean.getMin());
            String AM_PM_CODE = medicineDoseBean.getAM_PM();
            try {
                AM_PM_CODE = MedicationFrequancyEnum.getCodeFromValue(AM_PM_CODE);
            } catch (Exception e) {
            }
            medicineDoseFrequencyModel.setDoseTimeAMorPM(AM_PM_CODE);
            medicineDoseFrequencyModel.setDoseIntakeTimeInstruction(medicineDoseBean.getIsAfterOrBefore() == null ? "" : medicineDoseBean.getIsAfterOrBefore());
            String itemCode = (String) mSpinner.getText();
            try {
                itemCode = MedicationFrequancyEnum.getCodeFromValue(itemCode);
            } catch (Exception e) {
            }
            if ((itemCode).equalsIgnoreCase(getResources().getString(R.string.conditional_sos))) {
                medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(false);
            } else {
                if (rbDays.isChecked() || rbEveryday.isChecked()) {
                    medicineDoseFrequencyModel.setDoseWeekdaysList(getSelectedDays());
                    medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(false);
                } else if (rbInterval.isChecked()) {
                    medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(true);
                    medicineDoseFrequencyModel.setRepeatDaysInterval(Integer.parseInt(tvNoOfDays.getText().toString().trim()));
                }
            }
            medicineDoseFrequencyModel.setStartDate(startDate);
            medicineDoseFrequencyModel.setEndDate(endDate);

            medicineDoseFrequencyList.add(medicineDoseFrequencyModel);

        }
        LinkedHashMap<String, MedicationFrequancyEnum> frequancy_map = MedicationFrequancyEnum.getMedicationFrequancyEnumLinkedHashMap();

        String itemCode = (String) mSpinner.getText();
        try {
            itemCode = MedicationFrequancyEnum.getCodeFromValue(itemCode);
        } catch (Exception e) {
        }
        prescriptionModel.setDoseInstruction(itemCode);
        prescriptionModel.setArrayDose(medicineDoseFrequencyList);
        //reading practioner name if config is enabled
        if (BuildConfig.isEnablePractitionerNameOnMedication)
            prescriptionModel.setPractitionerName(etDoctorName.getText().toString().trim());
        prescriptionModel.setDiseaseMoList(diseaseMoList);
        if (medicineIDSearch == -1)
            prescriptionModel.setMedicianId(-1);
        else
            prescriptionModel.setMedicianId(prescriptionModel.getMedicianId());
        prescriptionModel.setPatientID(userMO.getPatientId());
        prescriptionModel.setEndDate(endDate);
        prescriptionModel.setStartDate(startDate);
        prescriptionModel.setRemark(etRemark.getText().toString().trim());
        prescriptionModel.setMimeType(VariableConstants.MIME_TYPE_NOTHING);

        return prescriptionModel;
    }

    private ArrayList<String> getSelectedDays() {
        ArrayList<String> daySelected = new ArrayList<>();
        for (int i = 0; i < days.size(); i++) {
            Days days = MedicationActivity.days.get(i);
            if (days.isSelected()) {
                daySelected.add(days.getDayOfWeek());
            }
        }
        return daySelected;
    }

    private void decrementRepitionDays() {
        if (daysRepitition >= 2) {
            daysRepitition = daysRepitition - 1;
        }
        tvNoOfDays.setText(daysRepitition + "");
        if (daysRepitition == 1) {
            ((CustomFontTextView) findViewById(R.id.st_days)).setText(getResources().getString(R.string.txt_day));
        }
    }

    private void incrementRepitionDays() {
        daysRepitition = daysRepitition + 1;
        tvNoOfDays.setText(daysRepitition + "");
        if (daysRepitition > 1) {
            ((CustomFontTextView) findViewById(R.id.st_days)).setText(getResources().getString(R.string.txt_days));
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (rbInterval.isChecked()) {
            setDaysList(false);
            setAllDays(false);
            gridButtonAdapter.notifyDataSetChanged();
            st_days.setTextColor(getResources().getColor(R.color.medium_green));
            tvNoOfDays.setTextColor(getResources().getColor(R.color.medium_green));
            gvRepeatDays.setVisibility(View.GONE);
            llayoutRepeatContainer.setVisibility(View.VISIBLE);
        } else if (rbEveryday.isChecked()) {
            setDaysList(true);
            setAllDays(true);
            gridButtonAdapter.notifyDataSetChanged();
            //  daysType.setText("Every day");
            st_days.setTextColor(getResources().getColor(R.color.dusky_blue_50));
            tvNoOfDays.setTextColor(getResources().getColor(R.color.dusky_blue_50));
            gvRepeatDays.setVisibility(View.GONE);
            llayoutRepeatContainer.setVisibility(View.GONE);
        } else if (rbDays.isChecked()) {
            //  daysType.setText("Specific days");
            setDaysList(false);
            setAllDays(false);
            gridButtonAdapter.notifyDataSetChanged();
            gvRepeatDays.setVisibility(View.VISIBLE);
            llayoutRepeatContainer.setVisibility(View.GONE);
        }
    }

    private void setAllDays(boolean isSelected) {
        for (int i = 0; i < 7; i++) {
            LinearLayout ll = (LinearLayout) gvRepeatDays.getChildAt(i);
            if (ll == null) {
                return;
            }
            CustomFontTextView previous = (CustomFontTextView) ll.getChildAt(0);
            previous.setSelected(isSelected);
            days.get(i).setIsSelected(isSelected);
            gridButtonAdapter.setButtonState(isSelected, previous, ll);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            tvEndDate.setError(null);
            tvEndDate.setText("");
            layoutEndDate.setEnabled(false);
            txtEndDate.setEnabled(false);
        } else {
            tvEndDate.setError(null);
            layoutEndDate.setEnabled(true);
            txtEndDate.setEnabled(true);
            String startDate = tvSelectedDate.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(startDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 2);
            SimpleDateFormat sdf1 = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
            String displayEndDate = sdf1.format(c.getTime());
            tvEndDate.setText(displayEndDate);
        }
    }

    public void setDaysList(boolean isSelected) {
        days.clear();
        days.add(new Days(getResources().getString(R.string.txt_monday), isSelected));
        days.add(new Days(getResources().getString(string.txt_tuesday), isSelected));
        days.add(new Days(getResources().getString(string.txt_wednesday), isSelected));
        days.add(new Days(getResources().getString(string.txt_thursday), isSelected));
        days.add(new Days(getResources().getString(string.txt_friday), isSelected));
        days.add(new Days(getResources().getString(string.txt_saturday), isSelected));
        days.add(new Days(getResources().getString(string.txt_sunday), isSelected));
    }

    public void setDaysType(ArrayList<Days> daysArray) {
        ArrayList<String> daySelected = daysSelected(daysArray);
        if (BuildConfig.isPatientApp)
            setRadioButtonsStatus(daySelected);
        else {
            setDaysTextStatus(daySelected);
        }
    }

    private ArrayList<String> daysSelected(ArrayList<Days> daysArray) {
        ArrayList<String> daySelected = new ArrayList<>();
        for (int i = 0; i < daysArray.size(); i++) {
            Days days = daysArray.get(i);
            if (days.isSelected()) {
                daySelected.add(days.getDayOfWeek());
            }
        }
        return daySelected;
    }

    @Override
    public void onScrollChanged(NestedScrollView who, int l, int t, int oldl, int oldt) {
        final int headerHeight = layoutToolbar.getHeight() - mToolbar.getHeight();
        final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
        final int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }

    public void setRadioButtonsStatus(ArrayList<String> daySelected) {
        if (daySelected.size() == 7) {
            rbEveryday.setChecked(true);
            setAllDays(true);
            gvRepeatDays.setVisibility(View.GONE);
            llayoutRepeatContainer.setVisibility(View.GONE);
        } else if (isSosSelected) {
            rbEveryday.setChecked(true);
            cvRepeatLayout.setVisibility(View.GONE);
        } else {
            rbDays.setChecked(true);
            gvRepeatDays.setVisibility(View.VISIBLE);
            llayoutRepeatContainer.setVisibility(View.GONE);
        }
    }

    public void setDaysTextStatus(ArrayList<String> daySelected) {
        tvEveryday.setVisibility(View.GONE);
        tvInterval.setVisibility(View.GONE);
        gvRepeatDays.setVisibility(View.VISIBLE);
        tvDays.setVisibility(View.VISIBLE);
        llayoutRepeatContainer.setVisibility(View.GONE);
        if (daySelected.size() == 7) {
            setAllDays(true);
            tvEveryday.setVisibility(View.VISIBLE);
            tvDays.setVisibility(View.GONE);
            gvRepeatDays.setVisibility(View.GONE);
            llayoutRepeatContainer.setVisibility(View.GONE);
        } else if (isSosSelected) {
            cvRepeatLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String date = null;
        if (view.isShown()) {
            date = DateTimeUtil.getFormattedDateWithoutUTCLocale(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(tvSelectedDate.getWindowToken(), 0);
            if (!compareStartAndEndDate(dateType, date)) {
                if (dateType.equals(getResources().getString(R.string.txt_endDate))) {
                    tvEndDate.setError(this.getResources().getString(R.string.enddate_after));
                    Toast.makeText(MedicationActivity.this, this.getResources().getString(R.string.enddate_after), Toast.LENGTH_SHORT).show();
                } else if (dateType.equals(getResources().getString(R.string.txt_startDate))) {
                    tvSelectedDate.setError(this.getResources().getString(R.string.startdate_bfore));
                    Toast.makeText(MedicationActivity.this, this.getResources().getString(R.string.startdate_bfore), Toast.LENGTH_SHORT).show();
                }
                //  tvSelectedDate.setText("");
            } else {
                if (dateType.equals(getResources().getString(R.string.txt_endDate))) {
                    tvEndDate.setText(date);
                    tvEndDate.setError(null);
                } else if (dateType.equals(getResources().getString(R.string.txt_startDate))) {
                    tvSelectedDate.setText(date);
                    tvSelectedDate.setError(null);
                }
            }
        }
    }


    public void showDateDialog(CustomFontTextView tv) {
        // Use the current date as the default date in the picker

        Calendar c = Calendar.getInstance();// Aug28, 2015
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            c.setTime(sdf.parse(tv.getText().toString().trim()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, this, year, month, day);
        datePickerDialog.show();
    }


    public boolean compareStartAndEndDate(String type, String date) {
        if (type.equals(getResources().getString(R.string.txt_endDate))) {
            return Utils.compareStartAndEndDate(tvSelectedDate.getText().toString().trim(), date);
        } else {
            return Utils.compareStartAndEndDate(date, tvEndDate.getText().toString().trim());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        etRemark.clearFocus();
        scrollView.smoothScrollTo(0, 0);
        if (requestCode == TextConstants.MEDICINE_FREQUNCY_INTET) {
            if (null != data) {
                Bundle bundle = data.getExtras();
                DoseBean doseBean = (DoseBean) bundle.getParcelable("dosebean");
                int clicked_position = bundle.getInt("clicked_position");

                dosageList.set(clicked_position, doseBean);
                rvDosageAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == TextConstants.MEDICINE_SEARCH_INTENT) {
            if (null != data) {
                Bundle bundle = data.getExtras();
                String medicineName = bundle.getString("medicine_name", "");
                medicineIDSearch = bundle.getInt("medicine_id", -1);
                mEtMedicineSearch.setError(null);
                mEtMedicineSearch.setText(medicineName);
                medicineHashMap.put(medicineName, medicineIDSearch);
            }

        } else if (requestCode == TextConstants.MEDICAL_CONDITIONS_SEARCH_INTET) {
            if (null != data) {
                Bundle bundle = data.getExtras();
                DiseaseMO diseaseMo = bundle.getParcelable("diseaseMo");

                String selecetdDiseaseName = diseaseMo.getText() + "";
                int position = bundle.getInt("position");
                if (!isDiseaseAlreadyAdded(selecetdDiseaseName)) {
                    try {
                        diseaseMoList.remove(position);
                    } catch (Exception Ex) {
                    }
                    diseaseMoList.add(position, diseaseMo);
                    mMedicalConditionsAdapter.notifyDataSetChanged();
                    mMedicalConditionsAdapter.showError(mRv_medicalConditions, false);
                } else {
                    showDuplicateDiseaseDialog(selecetdDiseaseName);
                }
            }
        }
    }

    private void showDuplicateDiseaseDialog(String diseaseName) {
        String duplicateMedicineName = "";
        if (diseaseName != null && !diseaseName.isEmpty()) {
            duplicateMedicineName = Utils.capitalizeFirstLetter(diseaseName);
            final SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.txt_you_added) + duplicateMedicineName + getResources().getString(R.string.txt_pls_cross_check));
            int length = 11 + duplicateMedicineName.length();
            final StyleSpan bss = new StyleSpan(Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 10, length, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make duplicate medicineName Bold
            DialogUtils.showAlertDialogCommon(mContext, mContext.getResources().getString(R.string.alert), sb.toString(), mContext.getResources().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Dialog.BUTTON_POSITIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            });
        }
    }


}
