package com.mphrx.fisike;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.ContactActivity;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

public class HeaderActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    UserMO userMO;
    private Dialog mProgressDialog;
    public static final int PERMISSION_REQUEST_CODE = 1;


    private boolean isRegisteredBroadcast;

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (SettingManager.getInstance().getUserMO() != null && key.equals(SharedPref.LANGUAGE_SELECTED)) {
            recreate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.changeAppLanguage(this);
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        super.onCreate(savedInstanceState);
        Utils.setCurrentActivityContext(this);
        Utils.restrictScreenShot(this);
        setStatusBarGradiant(this);

    }

    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.drawable_status_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        registerBroadcast();

        userMO = SettingManager.getInstance().getUserMO();
        storeLastInteractionTime(true);

    }


    @Override
    protected void onDestroy() {
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
        unregisterBroadcast();

    }

    private void unregisterBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(logoutBroadcast);
    }

    private void registerBroadcast() {
        if (isRegisteredBroadcast) {
            return;
        }
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(logoutBroadcast, new IntentFilter(VariableConstants.LOGOUT_IF_AUTH_TOKEN_NULL));
        isRegisteredBroadcast = true;
    }

    private BroadcastReceiver logoutBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                if (intent.getExtras() != null) {
                    if (intent.getExtras().containsKey(TextConstants.REGIONAL_BLOCK)) {
                        Utils.launchRegionalBlockScreen(HeaderActivity.this);
                    }
                } else {
                    Utils.launchLoginScreen(HeaderActivity.this);
                }
            } else {
            }
        }
    };


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        storeLastInteractionTime(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void storeLastInteractionTime(boolean isToLaunch) {
        if (Utils.checkToDisplayPINScreen(userMO) && ConfigManager.getInstance().getConfig().getPinLockTime() != 0) {
            if (isToLaunch && !Utils.isAppbackground(this) && !Utils.isTopActivityLockScreen(this) && !Utils.checkTopActivity(this, TextConstants.UPDATE_PIN)) {
                Intent i = new Intent(this, com.mphrx.fisike.lock_screen.LockScreenActivity.class);
                startActivity(i);
            }
        } else if (Utils.checkIsToLogoutUser()) {
            //no need to update time here as it will be handle in BaseActivity / Physician header Activity.
        } else if (getIntent().getExtras() == null && SharedPref.getLastInteractionTime() != 0 && (((System.currentTimeMillis() - SharedPref.getLastInteractionTime()) / 1000) >= (TextConstants.USER_INTERACTION_TIME * 60))) {
            SharedPref.setLastInteractionTime(System.currentTimeMillis());
            Intent i = new Intent(this, BuildConfig.isPatientApp ? SplashActivity.class : com.mphrx.fisike_physician.activity.SplashActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            SharedPref.setLastInteractionTime(System.currentTimeMillis());
        } else
            SharedPref.setLastInteractionTime(System.currentTimeMillis());
    }


    public boolean checkPermission(final String permissionType, String message) {
        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(HeaderActivity.this, new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionNotGranted(permissions[0]);
            }
        }
    }

    public void onPermissionGranted(String permission) {
        throw new RuntimeException(getResources().getString(R.string.You_must_override_ask_Runtime_Permission));
    }

    public void onPermissionNotGranted(String permission) {
        throw new RuntimeException(getResources().getString(R.string.You_must_override_ask_Runtime_Permission));
    }


    public boolean checkPermission(final String permissionType, String message, final int requestCode) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(HeaderActivity.this, new String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted();

                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
            }

            return false;
        }

        return true;
    }

    public void onPermissionNotGranted() {
        Toast.makeText(this, getResources().getString(R.string.You_need_to_add_Permission_to_continue), Toast.LENGTH_SHORT)
                .show();

    }

    public void openActivity(Class<? extends Activity> clazz, Bundle bundle, boolean finishAllActivities) {

        Intent intent = new Intent(this, clazz);
        if (bundle != null)
            intent.putExtras(bundle);
        if (finishAllActivities) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        if (this instanceof ContactActivity) {
            startActivityForResult(intent, 0);
        } else
            startActivity(intent);
        setEnterAndExitAnimation();
        if (finishAllActivities)
            finish();
    }

    public void openActivity(Class<? extends Activity> clazz, Bundle bundle, int requestCode) {
        Intent intent = new Intent(this, clazz);
        if (bundle != null)
            intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
        setEnterAndExitAnimation();
    }

    public void setEnterAndExitAnimation() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void setExitAndEnterAnimation() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();

        super.onBackPressed();
        setExitAndEnterAnimation();
    }

    public void hideKeyboard() {
        Utils.hideKeyboard(this);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null)
            mProgressDialog = DialogUtils.showProgressDialog(this);
        else
            mProgressDialog.show();
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog == null)
            mProgressDialog = DialogUtils.showProgressDialog(this, message);
        else if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    public void addFragment(BaseFragment fragment, int containerId, boolean addToBackStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(containerId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack)
            ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    public void replaceFragment(BaseFragment fragment, int containerId, boolean addToBackStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(containerId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack)
            ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commitAllowingStateLoss();
    }

    public boolean isFragmentVisible(Class<? extends BaseFragment> clazz) {
        FragmentManager fm = getFragmentManager();
        Fragment frgmt = fm.findFragmentByTag(clazz.getSimpleName());
        if (frgmt == null || !frgmt.isVisible())
            return false;
        return true;
    }

    public Fragment getFragmentByKey(Class<? extends BaseFragment> clazz) {
        FragmentManager fm = getFragmentManager();
        return fm.findFragmentByTag(clazz.getSimpleName());
    }


}
