package com.mphrx.fisike;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by administrate on 12/11/2015.
 */
    public class squareImageView extends android.support.v7.widget.AppCompatImageView {

        public squareImageView(Context context) {
            super(context);
        }

        public squareImageView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public squareImageView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

            int width = getMeasuredWidth();
            setMeasuredDimension(width, width);
        }

    }
