package com.mphrx.fisike;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.mphrx.fisike.adapter.PastConditionsAdapter;
import com.mphrx.fisike.add_address.AddAddressActivity;
import com.mphrx.fisike.add_alternate_contact.AddAlternateContact;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.DesignationEnum;
import com.mphrx.fisike.enums.ExperienceEnum;
import com.mphrx.fisike.enums.AddressTypeEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.enums.SpecialityEnum;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.update_user_profile.Enum.MaritalStatusEnum;
import com.mphrx.fisike.update_user_profile.UpdateProfilePicActivity;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.models.PastConditionsModel;
import com.mphrx.fisike_physician.network.request.PastConditionRequest;
import com.mphrx.fisike_physician.network.response.PastConditionResponse;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import appointment.phlooba.adapter.ImageGridAdapter;
import appointment.phlooba.model.ImageDataType;

/**
 * Created by Kailash Khurana on 4/4/2016.
 */
public class UserProfileActivity extends UpdateProfilePicActivity {
    private static final int UPDATE_PROFILE = 100;
    private static final int UPDATE_ADDRESS = 101;
    private static final int UPDATE_ALTERNATE_CONTACT = 102;
    public static final int CHANGE_PASSWORD_REQUEST_CODE = 103;

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private CustomFontTextView txtUserStatus, tvViewMore;
    private CustomFontTextView txtUserName;
    private CustomFontTextView txtUserGender, txtContactSubHeader;
    private CustomFontTextView txtUserDOB;
    private CustomFontTextView txtContactHeader;
    private CustomFontButton btnAddContact;
    private LinearLayout layoutAddContactFields;


    private LinearLayout layoutAddAddressFields;
    private PastConditionsAdapter pastConditionsAdapter;
    private Intent i;
    private ImageView imgContactOverflow;
    private CustomFontTextView txtSpeciality;
    private CustomFontTextView txtMyRole;
    private CustomFontTextView txtYearOfExperience;
    private CustomFontTextView txtHeader;
    private CustomFontTextView txtStatusTitle, txtStatusHeader;
    private View pastconditions_view;
    private RecyclerView rv_past_conditions;
    private java.util.List<PastConditionsModel> pastConditionsModelList = new ArrayList<PastConditionsModel>();
    private RelativeLayout temp_msg_lyt;
    private ProgressBar progress;
    private CustomFontTextView tv_msg;
    private FrameLayout frameLayout;
    private CustomFontTextView tv_view_more;
    private CardView cardViewUserAddress;
    private CustomFontTextView txtAadharHeader, txtAadhar;
    private CardView cardEmail;
    private CustomFontTextView txtEmailAddress, txtNameHeader;
    private CustomFontButton btnVerifyEmail, btnUpdateEmail;
    private CardView cardAlternateContact;
    private CardView cardinsurance;
    private RecyclerView rvInsuranceImage;
    private LinearLayout llManualInsurance;
    private ImageGridAdapter adapter;
    private ArrayList<ImageDataType> insuranceList;
    private CustomFontButton addInsuranceButton;
    private CustomFontTextView insuranceType, insurancetypeCompanyName, lblPolicyNumber, txtPolicyNo, lblGroupNo, txtGroupNo, lblPolicyHolderName, txtPolicyHolderName;
    private CustomFontButton btnDelete;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showHideToolbar(false);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        if (BuildConfig.isPatientApp)
            getLayoutInflater().inflate(R.layout.activity_user_profile, frameLayout);
        else
            getLayoutInflater().inflate(R.layout.activity_user_physician_profile, frameLayout);
        setCurrentItem(-1);
        findViews();
        setData();
        //moved this code to bas0eActivity
        //BusProvider.getInstance().register(this);
        if (BuildConfig.isPatientApp && BuildConfig.isPastConditionsEnabled) {
            pastconditions_view.setVisibility(View.VISIBLE);
            ThreadManager.getDefaultExecutorService().submit(new PastConditionRequest(getTransactionId(), this));
        } else if (BuildConfig.isPatientApp && !BuildConfig.isPastConditionsEnabled) {
            pastconditions_view.setVisibility(View.GONE);
        }

        super.userProfileData();
    }

    @Override
    protected void onDestroy() {
        //moved these calls to baseActivity
        // BusProvider.getInstance().unregister(this);
        super.onDestroy();

    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateProfileResponse response) {
        if (response.getTransactionId() != super.getTransactionId())
            return;
        super.updatePhysicianAddress(response);
        userMO = SettingManager.getInstance().getUserMO();
        setAddress();
    }


    /*
         * Activity elements
         *
         */
    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        imgContactOverflow = (ImageView) findViewById(R.id.imgContactOverflow);
        txtUserName = (CustomFontTextView) findViewById(R.id.txtUserName);
        txtContactHeader = (CustomFontTextView) findViewById(R.id.txtContactHeader);
        txtContactSubHeader = (CustomFontTextView) findViewById(R.id.txtContactSubHeader);
        layoutAddContactFields = (LinearLayout) findViewById(R.id.layoutAddContactFields);
        layoutAddAddressFields = (LinearLayout) findViewById(R.id.layoutAddAdressFields);
        btnAddContact = (CustomFontButton) findViewById(R.id.btnAddContact);
        cardViewUserAddress = (CardView) findViewById(R.id.cardViewUserAddress);
        cardAlternateContact = (CardView) findViewById(R.id.cardViewUserContact);
        txtNameHeader = (CustomFontTextView) findViewById(R.id.txtNameHeader);

        if (BuildConfig.isPatientApp) {
            txtUserGender = (CustomFontTextView) findViewById(R.id.txtUserGender);
            txtUserDOB = (CustomFontTextView) findViewById(R.id.txtUserDOB);
            txtUserStatus = (CustomFontTextView) findViewById(R.id.txtUserStatus);
            txtStatusHeader = (CustomFontTextView) findViewById(R.id.txtStatusHeader);
            tv_view_more = (CustomFontTextView) findViewById(R.id.tv_view_more);
            tv_view_more.setOnClickListener(this);

            pastconditions_view = findViewById(R.id.cardpastconditions);
            rv_past_conditions = (RecyclerView) pastconditions_view.findViewById(R.id.rv_past_conditions);
            temp_msg_lyt = (RelativeLayout) pastconditions_view.findViewById(R.id.temp_msg_lyt);
            progress = (ProgressBar) pastconditions_view.findViewById(R.id.progress);
            tv_msg = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_msg);

            rv_past_conditions.setLayoutManager(new LinearLayoutManager(this));
            rv_past_conditions.setNestedScrollingEnabled(false);
            pastConditionsAdapter = new PastConditionsAdapter(this, pastConditionsModelList, tv_view_more);
            rv_past_conditions.setAdapter(pastConditionsAdapter);
            //AadharView
            txtAadharHeader = (CustomFontTextView) findViewById(R.id.txtAadharHeader);
            txtAadhar = (CustomFontTextView) findViewById(R.id.txtAadhar);


            findEmailCardElements();
            findInsuranceCardElements();
        } else {
            txtSpeciality = (CustomFontTextView) findViewById(R.id.txtSpeciality);
            txtMyRole = (CustomFontTextView) findViewById(R.id.txtMyRole);
            txtYearOfExperience = (CustomFontTextView) findViewById(R.id.txtYearOfExperience);
            txtHeader = (CustomFontTextView) findViewById(R.id.txtHeader);
            txtStatusTitle = (CustomFontTextView) findViewById(R.id.txtStatusTitle);
            txtHeader.setOnClickListener(this);
            txtStatusTitle.setOnClickListener(this);
        }
    }

    /**
     * Initilizing the elements value
     */
    private void setData() {
        initToolbar();
        String lastname = "";
        if (userMO == null)
            return;

        if (userMO != null)
            lastname = userMO.getLastName();
        if (lastname == null)
            lastname = "";
        txtNameHeader.setText(getString(R.string.name));
        txtUserName.setText(userMO.getFirstName() + " " + lastname);
        if (BuildConfig.isPatientApp) {
            txtNameHeader.setText(getString(R.string.mobile_number));
            if (Utils.isValueAvailable(userMO.getPhoneNumber())){
                String phone=(Utils.isValueAvailable(userMO.getCountryCode())?userMO.getCountryCode()+" ":"")+userMO.getPhoneNumber();
                txtUserName.setText(phone);
            }
            else
            {
                txtNameHeader.setVisibility(View.GONE);
                txtUserName.setVisibility(View.GONE);
            }
            if (Utils.isValueAvailable(userMO.getDateOfBirth()))
                txtUserDOB.setText(Utils.getFormattedDate(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime));
            if (userMO.getGender() != null && !userMO.getGender().trim().equals("") && GenderEnum.getGenderEnumLinkedHashMap().containsKey(userMO.getGender())) {
                txtUserGender.setText(GenderEnum.getDisplayedValuefromCode(userMO.getGender()));
            } else {
                txtUserGender.setText(GenderEnum.getDisplayedValuefromCode(getString(R.string.gender_other_key)));
            }
            String maritalStatusValue = userMO.getMaritalStatus();
            String maritalStatus = (Utils.isValueAvailable(maritalStatusValue)) ?
                    (MaritalStatusEnum.getMaritalStatusMap().containsKey(maritalStatusValue) ? MaritalStatusEnum.getMaritalStatusMap().get(maritalStatusValue).getValue() : "")
                    : "";

            if (Utils.isValueAvailable(maritalStatus)) {
                txtUserStatus.setText(maritalStatus);

            } else {
                txtStatusHeader.setVisibility(View.GONE);
                txtUserStatus.setVisibility(View.GONE);

            }
            setAadharData();
        } else {
            String experince = SharedPref.getExperience();
            String role = SharedPref.getRole();
            String speciality = SharedPref.getSpeciality();
            if (Utils.isValueAvailable(experince)) {
                if (experince.contains("<") || experince.trim().equals("1")) {
                    experince = experince + " " + getResources().getString(R.string.year);
                } else {
                    experince = experince + " " + getResources().getString(R.string.years);
                }
                txtYearOfExperience.setVisibility(View.VISIBLE);
                txtYearOfExperience.setText(ExperienceEnum.getDisplayedValuefromCode(experince.trim()));
            }
            if (Utils.isValueAvailable(role)) {
                txtMyRole.setText(DesignationEnum.getDisplayedValuefromCode(role));
            }
            if (Utils.isValueAvailable(speciality)) {
                txtSpeciality.setText(SpecialityEnum.getDisplayedValuefromCode(speciality));
            }
            setStatus();
        }

        String alternateContact = userMO.getAlternateContact();

        addAlternateContact();
        if (BuildConfig.isPatientApp) {
            setAddress();
            initInsuranceData();
            if (Utils.isValueAvailable(userMO.getEmail())) {
                setEmailData(userMO.getEmail());
            }
        } else
            cardViewUserAddress.setVisibility(View.GONE);
    }


    @Subscribe
    public void onPastConditionResponse(PastConditionResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful() && response.getmPastConditionsResponseList() != null && response.getmPastConditionsResponseList().size() > 0) {
            pastconditions_view.setVisibility(View.VISIBLE);
            pastConditionsModelList.clear();
            pastConditionsModelList.addAll(response.getmPastConditionsResponseList());
            rv_past_conditions.setVisibility(View.VISIBLE);
            temp_msg_lyt.setVisibility(View.GONE);
            pastConditionsAdapter.notifyDataSetChanged();
        } else {
            rv_past_conditions.setVisibility(View.GONE);
            temp_msg_lyt.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            tv_msg.setText(getResources().getString(R.string.No_Past_Conditions));
            pastconditions_view.setVisibility(View.GONE);
        }
    }

    private void setAddress() {
        ArrayList<Address> addresses = userMO.getAddressUserMos();
        if (addresses == null || addresses.size() == 0) {
            findViewById(R.id.txtAddressSubHeader).setVisibility(View.VISIBLE);
            layoutAddAddressFields.removeAllViews();
            return;
        }
        layoutAddAddressFields.removeAllViews();
        findViewById(R.id.txtAddressSubHeader).setVisibility(View.GONE);
        for (int i = 0; userMO.getAddressUserMos() != null && i < userMO.getAddressUserMos().size(); i++) {
            Address address = userMO.getAddressUserMos().get(i);
            addAddress(address, i);
        }
    }


    private void deleteAddress(Object object) {
        ArrayList<Address> addresses = userMO.getAddressUserMos();
        addresses.remove(((Address) object));
        deleteAddress(addresses);
    }


    public void showPopupMenuCRUD(final int menuId, View view, final Object object) {
        final PopupMenu popup = new PopupMenu(UserProfileActivity.this, view);

        popup.getMenuInflater().inflate(menuId, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
//                    case R.id.menu_add_address:
//                        addAddressDialog();
//                        break;
                    case R.id.menu_delete_address:
                        if (Utils.showDialogForNoNetwork(UserProfileActivity.this, false)) {
                            showDeleteAddress(object);
                        }
                        break;
                    case R.id.menu_update_address:
                        startAddAddress(((Address) object));
                        break;
                    case R.id.menu_delete_contact:
                        if (Utils.showDialogForNoNetwork(UserProfileActivity.this, false)) {
                            showDeleteAlternateContact();
                        }
                        break;

                }
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void addAddressDialog() {
/*
        DialogUtils.showAlertDialog(UserProfileActivity.this, "Add Address", getResources().getString(R.string.home_work_address), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
*/
        startAddAddress(null);
/*
                } else {
                    dialog.dismiss();
                }
            }
        });
*/
    }

    /**
     * Initilizing and handling toolbar
     */
    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar_title.setText(getResources().getString(R.string.my_profile));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           /*     UserProfileActivity.this.finish();*/
/*                UserProfileActivity.super.onBackPressed();*/
                openDrawer();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case UPDATE_PROFILE:
                    setUserMO();
                    setData();
                    break;
                case UPDATE_ADDRESS:
                    setUserMO();
                    setAddressUserData();
                    break;
                case UPDATE_ALTERNATE_CONTACT:
                    setUserMO();
                    setAlternateContact();
                    break;
                case CHANGE_PASSWORD_REQUEST_CODE:
                    // show toast for password successfully updated
                    Toast.makeText(this, R.string.password_changed_successfully,
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public void
    setAlternateContact() {
        setUserMO();
        addAlternateContact();
    }

    public void setAddressUserData() {
        setUserMO();
        layoutAddAddressFields.removeAllViews();
        setAddress();
    }

    public void setPastConditions() {

    }

    /**
     * Add address to be shown in the UI
     *
     * @param address
     * @param index
     */
    private void addAddress(final Address address, int index) {
        int dp29 = (int) Utils.convertDpToPixel(29, UserProfileActivity.this);
        int dp12 = (int) Utils.convertDpToPixel(12, UserProfileActivity.this);
        int dp7 = (int) Utils.convertDpToPixel(7, UserProfileActivity.this);
        int dp20 = (int) Utils.convertDpToPixel(20, UserProfileActivity.this);
        CustomFontTextView txtAddressHeader = new CustomFontTextView(this);
        if (address.getUseCode() == null || address.getUseCode().equals("")) {
            txtAddressHeader.setText("");
        } else {
            String useCode = address.getUseCode();
            useCode = AddressTypeEnum.getDisplayedValuefromCode(useCode);

            txtAddressHeader.setText(/*useCode.contains(getResources().getString(R.string.Address)) ? useCode : (useCode + " " + getResources().getString(R.string._Address))*/"");
        }
        txtAddressHeader.setTextColor(getResources().getColor(R.color.dusky_blue_50));
        txtAddressHeader.setTextSize(12);
        CustomFontTextView txtAddress = new CustomFontTextView(this);

        String postal = address.getPostalCode();
        String city = address.getCity();
        String state = address.getState();
        String country = address.getCountry();
        ArrayList<String> line = address.getLine();
        String addLine = new String();
        for (int i = 0; i < line.size(); i++)
            if (!line.get(i).trim().equals(""))
                addLine += line.get(i) + ", ";

        String tempAddress = new String();
        if (addLine != null)
            tempAddress += addLine;
        if (city != null && !city.equals(getResources().getString(R.string.txt_null)) && !city.equals(""))
            tempAddress += city + ", ";
        if (state != null && !state.equals(getResources().getString(R.string.txt_null)) && !state.equals(""))
            tempAddress += state + ", ";
        if (postal != null && !postal.equals(getResources().getString(R.string.txt_null)) && !postal.equals(""))
            tempAddress += postal + ", ";
        if (country != null && !country.equals(getResources().getString(R.string.txt_null)) && !country.equals(""))
            tempAddress += country;
        txtAddress.setText(tempAddress);
        txtAddress.setTextColor(getResources().getColor(R.color.dusky_blue_70));
        txtAddress.setTextSize(14);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(dp29, 0, dp29, 0);
        txtAddress.setLayoutParams(lp);
        txtAddress.setGravity(Gravity.CENTER_VERTICAL);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        txtAddressHeader.setLayoutParams(lp);
        txtAddressHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_menu_overflow, 0);
        txtAddressHeader.setGravity(Gravity.CENTER_VERTICAL);
        relativeLayout.addView(txtAddressHeader);
        relativeLayout.setGravity(Gravity.CENTER_VERTICAL);
        final ImageView imageButton = new ImageView(this);
        imageButton.setBackgroundResource(R.drawable.ic_menu_overflow);
        RelativeLayout.LayoutParams layoutRule = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutRule.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        imageButton.setLayoutParams(layoutRule);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isReadOnly)
                    Toast.makeText(UserProfileActivity.this, "under development...", Toast.LENGTH_SHORT).show();
                else
                    showPopupMenuCRUD(R.menu.menu_address_update, imageButton, address);

            }
        });
        relativeLayout.addView(imageButton);
        LinearLayout.LayoutParams relativeLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        relativeLayoutParams.setMargins(dp29, index > 0 ? 30 : 0, dp7, 0);
        relativeLayout.setLayoutParams(relativeLayoutParams);
        if (index > 0) {
            View view = new View(UserProfileActivity.this);
            LinearLayout.LayoutParams lineView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 3);
            lineView.setMargins(0, dp12, 0, 0);
            view.setLayoutParams(lineView);
            view.setBackgroundColor(getResources().getColor(R.color.blue_grey_20));
            layoutAddAddressFields.addView(view);
        }
        layoutAddAddressFields.addView(relativeLayout);
        layoutAddAddressFields.addView(txtAddress);
    }

    /**
     * Add the user alternate contact information
     */
    private void addAlternateContact() {


        int dp12 = (int) Utils.convertDpToPixel(12, UserProfileActivity.this);
        if (layoutAddContactFields != null)
            layoutAddContactFields.removeAllViews();

        ViewGroup.LayoutParams p = layoutAddAddressFields.getLayoutParams();
        if (p instanceof LinearLayout.LayoutParams) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) p;
            lp.setMargins(0, 2, 0, 0);
            layoutAddAddressFields.setLayoutParams(lp);
        }
        CustomFontTextView txtAlternatePhoneNumberHeader = new CustomFontTextView(this);
        CustomFontTextView txtAlternatePhoneNumber = new CustomFontTextView(this);
        String alternateContact = userMO.getAlternateContact();
        txtAlternatePhoneNumberHeader.setTextColor(getResources().getColor(R.color.dusky_blue_50));
        txtAlternatePhoneNumberHeader.setTextSize(12);
        txtAlternatePhoneNumber.setTextColor(getResources().getColor(R.color.dusky_blue_70));
        txtAlternatePhoneNumber.setTextSize(14);
        if (alternateContact != null && !alternateContact.trim().equals("")) {
            if (alternateContact.contains("@"))
                txtAlternatePhoneNumberHeader.setText(getResources().getString(R.string.Email_Address));
            else
                txtAlternatePhoneNumberHeader.setText(getResources().getString(R.string.Mobile_Number));

            txtAlternatePhoneNumber.setText(alternateContact);
            txtContactSubHeader.setVisibility(View.GONE);
            txtContactHeader.setCompoundDrawables(null, null, null, null);
            btnAddContact.setText(R.string.update_contact);
            imgContactOverflow.setVisibility(View.VISIBLE);
            layoutAddContactFields.addView(txtAlternatePhoneNumberHeader);
            layoutAddContactFields.addView(txtAlternatePhoneNumber);
            ViewGroup.LayoutParams parms = txtAlternatePhoneNumber.getLayoutParams();
            if (parms instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) parms;
                lp.setMargins(0, dp12, 0, 0);
                txtAlternatePhoneNumber.setLayoutParams(lp);
            }

        } else {
            if (isReadOnly)
                cardAlternateContact.setVisibility(View.GONE);
            txtContactSubHeader.setVisibility(BuildConfig.isForgetPassword ? View.VISIBLE : View.GONE);
            btnAddContact.setText(R.string.txt_add);
            imgContactOverflow.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                txtContactHeader.setCompoundDrawables(null, getResources().getDrawable(R.drawable.ic_menu_overflow, null), null, null);
            } else
                txtContactHeader.setCompoundDrawables(null, getResources().getDrawable(R.drawable.ic_menu_overflow), null, null);
        }


    }

    /**
     * Handling the views on click
     *
     * @param v
     */
    public void onClick(View v) {
        Utils.hideKeyboard(this);
        if (isReadOnly)
            Toast.makeText(this, "under development..", Toast.LENGTH_SHORT).show();
        else {
            switch (v.getId()) {
                case R.id.txtHeader:
                case R.id.txtStatusTitle:
                    showAlertDialogPresence(UserProfileActivity.this, getResources().getString(R.string.Change_Status));
                    break;
                case R.id.btnAddContact:
                    if (((CustomFontButton) findViewById(R.id.btnAddContact)).getText().toString().equalsIgnoreCase(getResources().getString(R.string.conditional_add_contact)))
                        updateAlternateContact(false);
                    else
                        updateAlternateContact(true);
                    break;
                case R.id.btnAddAddress:
                    addAddressDialog();
                    break;
                case R.id.imgContactOverflow:
                    showPopupMenuCRUD(R.menu.menu_alternate_add, findViewById(R.id.imgContactOverflow), null);
                    break;
                default:
                    super.onClick(v);

            }
        }
    }

    private void showAlertDialogPresence(final Context context, String title) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.dialog_status_presence, null, false);
        alertDialog.setView(v);
        CustomFontTextView tvTitle = (CustomFontTextView) v.findViewById(R.id.header);
        tvTitle.setText(title);
        final Switch sw_toolbar_dialog_switch = (Switch) v.findViewById(R.id.sw_toolbar_dialog_switch);
        final CustomFontTextView txtStatus = (CustomFontTextView) v.findViewById(R.id.txtStatus);
        final CustomFontEditTextView etStatusMessage = (CustomFontEditTextView) v.findViewById(R.id.etStatusMessage);
        final CustomFontTextView txtStatusLength = (CustomFontTextView) v.findViewById(R.id.txtStatusLength);

        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                boolean isChecked = sw_toolbar_dialog_switch.isChecked();
                SharedPref.setIsUserStatus(isChecked);
                if (isChecked) {
                    SharedPref.setUserStatus(etStatusMessage.getText().toString());
                } else {
                    SharedPref.setUserStatus(etStatusMessage.getText().toString());
                }
                setStatus();
            }
        });

        sw_toolbar_dialog_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPref.setIsUserStatus(isChecked);
                if (isChecked) {
                    txtStatus.setText(getResources().getString(R.string.I_am_available));
                    txtStatus.setTextColor(context.getResources().getColor(R.color.green));
                    etStatusMessage.setText(getResources().getString(R.string.I_am_available));
                } else {
                    txtStatus.setText(getResources().getString(R.string.I_am_not_available));
                    txtStatus.setTextColor(context.getResources().getColor(R.color.red_500));
                    etStatusMessage.setText(getResources().getString(R.string.I_am_not_available));
                }
            }
        });

        etStatusMessage.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String space = " ";
                txtStatusLength.setText(getResources().getString(R.string.Max_) + (140 - s.toString().trim().length()) + getResources().getString(R.string._characters));
            }
        });

        boolean isUserStatus = SharedPref.getIsUserStatus();
        sw_toolbar_dialog_switch.setChecked(isUserStatus);

        String statusMessage = SharedPref.getUserStatus();
        if (statusMessage != null && !statusMessage.equals("")) {
            etStatusMessage.setText(SharedPref.getUserStatus());
        } else if (statusMessage == null) {
            etStatusMessage.setText(getResources().getString(R.string.I_am_available));
        }
        alertDialog.show();
    }

    private void setStatus() {
        boolean isUserStatus = SharedPref.getIsUserStatus();
        String statusMessage = SharedPref.getUserStatus();
        if (statusMessage != null && !statusMessage.equals("")) {
            txtHeader.setText(SharedPref.getUserStatus());
        } else if (statusMessage == null) {
            isUserStatus = true;
            SharedPref.setIsUserStatus(isUserStatus);
            txtHeader.setText(getResources().getString(R.string.I_am_available));
        }
        if (isUserStatus) {
            txtHeader.setTextColor(getResources().getColor(R.color.green));
        } else {
            txtHeader.setTextColor(getResources().getColor(R.color.red_500));
        }
    }


    private void updateAlternateContact(boolean isUpdate) {
        if (isUpdate) {
//            DialogUtils.showAlertDialog(this, "Update Alternate Contact", getResources().getString(R.string.update_alternate_contact_message), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    if (which == Dialog.BUTTON_POSITIVE) {
            startAddAlternateContact();
//                    } else {
//                        dialog.dismiss();
//                    }
//                }
//            });
        } else
            startAddAlternateContact();
    }

    private void startAddAlternateContact() {
        Intent i = new Intent(UserProfileActivity.this, AddAlternateContact.class);
        startActivityForResult(i, UPDATE_ALTERNATE_CONTACT);
    }

    private void showDeleteAlternateContact() {
        DialogUtils.showAlertDialog(this, getResources().getString(R.string.Remove_Alternate_Contact), getResources().getString(R.string.txt_delete_alternate_contact), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    deleteAlternateContactFields();
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    private void showDeleteAddress(final Object object) {
        DialogUtils.showAlertDialog(this, getResources().getString(R.string.Remove_Address), getResources().getString(R.string.txt_delete_address), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    deleteAddress(object);
                } else {
                    dialog.dismiss();
                }
            }
        });
    }


    private void startAddAddress(final Address address) {

        if (address != null) {
         /*   DialogUtils.showAlertDialog(this, "Update Address", getResources().getString(R.string.txt_update_address), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {*/
            Intent intent = new Intent(new Intent(UserProfileActivity.this, AddAddressActivity.class));
            if (address != null) {
                intent.putExtra(VariableConstants.ADDRESS, address);
                intent.putExtra(VariableConstants.ADDRESS_INDEX, userMO.getAddressUserMos().indexOf(address));
            }
            startActivityForResult(intent, UPDATE_ADDRESS);
                  /*  } else {
                        dialog.dismiss();
                    }
                }
            });*/
        } else {
            Intent intent = new Intent(new Intent(UserProfileActivity.this, AddAddressActivity.class));
            if (address != null) {
                intent.putExtra(VariableConstants.ADDRESS, address);
                intent.putExtra(VariableConstants.ADDRESS_INDEX, userMO.getAddressUserMos().indexOf(address));
            }
            startActivityForResult(intent, UPDATE_ADDRESS);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = (MenuItem) menu.findItem(R.id.menu_change_mobile_number);
        if (!SharedPref.getIsMobileEnabled() && item != null) {
            item.setVisible(false);
        }
        MenuItem signout = (MenuItem) menu.findItem(R.id.menu_sign_out);
        if (BuildConfig.isDrawerSignOut && menu.findItem(R.id.menu_sign_out) != null) {
            signout.setVisible(false);
        }

        if (isReadOnly) {
            if (menu.findItem(R.id.menu_update_profile) != null)
                menu.findItem(R.id.menu_update_profile).setVisible(false);

            if (menu.findItem(R.id.menu_change_password) != null)
                menu.findItem(R.id.menu_change_password).setVisible(false);

            if (menu.findItem(R.id.menu_change_mobile_number) != null)
                menu.findItem(R.id.menu_change_mobile_number).setVisible(false);
        }

        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    private void logoutUser() {
        DialogUtils.showAlertDialog(this, this.getResources().getString(R.string.confirm_signout), getLogoutMessageConfigBased(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    logout(UserProfileActivity.this);
                } else {
                    dialog.dismiss();
                }
            }
        });
    }


    protected void logout(Context context) {
        MessengerService mService = ((BaseActivity) context).getmService();
//        if (BuildConfig.isFisike)
        mService.logoutUser(context);
//        else
//            new DeRegisterUserDeviceBG(context, getmService(), false, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_update_profile:

                startActivityForResult(new Intent(UserProfileActivity.this, UpdateUserProfileActivity.class), UPDATE_PROFILE);

                break;
            case R.id.menu_change_password:
                i = new Intent(this, ChangeDetails.class);
                i.putExtra(VariableConstants.IS_CHANGE_PASSWORD, true);
                startActivityForResult(i, CHANGE_PASSWORD_REQUEST_CODE);

                break;
            case R.id.menu_change_mobile_number:
                i = new Intent(this, OtpActivity.class).putExtra(TextConstants.LAUNCH_MODE, TextConstants.CHANGE_NUMBER);
                startActivity(i);
                break;

            case R.id.menu_sign_out:
                logoutUser();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAadharData() {
        if (BuildConfig.isAadharEnabled) {
            txtAadhar.setText(Utils.isValueAvailable(userMO.getAadharNumber()) ? userMO.getAadharNumber() : getString(R.string.blank_value));
        } else {
            txtAadharHeader.setVisibility(View.GONE);
            txtAadhar.setVisibility(View.GONE);
        }
    }

    public String getLogoutMessageConfigBased() {
        if (BuildConfig.isFisike) {
            return this.getResources().getString(R.string.logout_msg);
        } else
            return this.getResources().getString(R.string.hard_logout_msg);

    }

    private void findEmailCardElements() {
        cardEmail = (CardView) findViewById(R.id.parent_card_email);
        txtEmailAddress = (CustomFontTextView) findViewById(R.id.txt_email_text);
        btnVerifyEmail = (CustomFontButton) findViewById(R.id.btn_verify_email);
        btnUpdateEmail = (CustomFontButton) findViewById(R.id.btn_update_email);
        btnVerifyEmail.setOnClickListener(this);
        btnUpdateEmail.setOnClickListener(this);
    }

    private void setEmailData(String emailAddress) {
        cardEmail.setVisibility(View.VISIBLE);
        txtEmailAddress.setText(emailAddress);
    }

    private void initInsuranceData() {
        if (userMO.getCoverage() != null && userMO.getCoverage().size() > 0) {
            cardinsurance.setVisibility(View.VISIBLE);
            addInsuranceButton.setOnClickListener(this);
            Coverage coverage = userMO.getCoverage().get(0);
            if(coverage == null){
                return;
            }

            if (coverage.getInsuranceType()!=null && coverage.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_picture))) {
                insuranceList = new ArrayList<>();
                if (coverage.getFrontImage() != null)
                    addElementInList(insuranceList, Utils.convertBase64ToBitmap(coverage.getFrontImage()), getString(R.string.front_image));
                adapter = new ImageGridAdapter(insuranceList, this, true);
                rvInsuranceImage.setLayoutManager(new GridLayoutManager(this, 2));
                rvInsuranceImage.setAdapter(adapter);
                llManualInsurance.setVisibility(View.GONE);
                if (insuranceList.size() == 0)
                    cardinsurance.setVisibility(View.GONE);
            } else {
                llManualInsurance.setVisibility(View.VISIBLE);
                rvInsuranceImage.setVisibility(View.GONE);
                if (Utils.isValueAvailable(coverage.getInsuranceType())) {
                    if (coverage.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicare)) && Utils.isValueAvailable(coverage.getPolicyNumber())) {
                        insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicare)));
                        lblPolicyNumber.setText(getString(R.string.medicare_number));
                        txtPolicyNo.setText(coverage.getPolicyNumber());
                    } else if (coverage.getInsuranceType().equalsIgnoreCase(getString(R.string.insurance_type_medicaid)) && Utils.isValueAvailable(coverage.getPolicyNumber())) {
                        insuranceType.setText(Utils.capitalizeFirstLetter(getString(R.string.insurance_type_medicaid)));
                        lblPolicyNumber.setText(getString(R.string.medicaid_number));
                        txtPolicyNo.setText(coverage.getPolicyNumber());
                    } else {
                        insuranceType.setText(coverage.getPrimaryInsuranceCompany());
                        txtPolicyNo.setText(coverage.getPolicyNumber());
                        txtGroupNo.setText(coverage.getGroupId());
                        txtPolicyHolderName.setText(coverage.getPolicyHolderName());
                        lblGroupNo.setVisibility(View.VISIBLE);
                        txtGroupNo.setVisibility(View.VISIBLE);
                        lblPolicyHolderName.setVisibility(View.VISIBLE);
                        txtPolicyHolderName.setVisibility(View.VISIBLE);
                        insurancetypeCompanyName.setVisibility(View.VISIBLE);

                    }

                }


            }
        }

    }

    private void addElementInList(ArrayList<ImageDataType> listData, Bitmap bitmap, String tag) {
        ImageDataType data = new ImageDataType(tag, bitmap);
        listData.add(data);
    }

    private void findInsuranceCardElements() {
        cardinsurance = (CardView) findViewById(R.id.card_insurance_parent);
        rvInsuranceImage = (RecyclerView) findViewById(R.id.rv_insurance);
        llManualInsurance = (LinearLayout) findViewById(R.id.ll_manual_insurance);
        addInsuranceButton = (CustomFontButton) findViewById(R.id.add_insurance_button);
        insuranceType = (CustomFontTextView) findViewById(R.id.insuranceType);
        insurancetypeCompanyName = (CustomFontTextView) findViewById(R.id.insurancetype_company_name);
        lblPolicyNumber = (CustomFontTextView) findViewById(R.id.lbl_policy_number);
        txtPolicyNo = (CustomFontTextView) findViewById(R.id.txt_policy_no);
        lblGroupNo = (CustomFontTextView) findViewById(R.id.lbl_group_number);
        txtGroupNo = (CustomFontTextView) findViewById(R.id.txt_group_no);
        lblPolicyHolderName = (CustomFontTextView) findViewById(R.id.lbl_policy_holder_name);
        txtPolicyHolderName = (CustomFontTextView) findViewById(R.id.txt_policy_holder_name);
        btnDelete = (CustomFontButton) findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(this);

    }
}
