package com.mphrx.fisike;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.mphrx.fisike.AppTour.AppTourActivity;
import com.mphrx.fisike.Observer.MobileConfigObserver;
import com.mphrx.fisike.adapter.SelectLanguageAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.SelectLanguageItemData;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class LaunchingLanguageSelectActivity extends ConfigFetchBaseActivity implements SelectLanguageAdapter.clickListener, Observer {
    public static final String IS_ERROR = "IS_ERROR";
    private RecyclerView languageRecyclerView;
    private SelectLanguageAdapter languagetAdapter;
    private RecyclerView.LayoutManager languageLayoutManager;
    private ArrayList<SelectLanguageItemData> itemList;
    private String languageKey = TextConstants.DEFALUT_LANGUAGE_KEY;
    private String languageTitle;
    private Dialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        languageRecyclerView = (RecyclerView) this.findViewById(R.id.my_recycler_view);

        languageLayoutManager = new LinearLayoutManager(this);
        languageRecyclerView.setLayoutManager(languageLayoutManager);
        itemList = new ArrayList<SelectLanguageItemData>();

        languagetAdapter = new SelectLanguageAdapter(itemList, this);
        languagetAdapter.setClickListener(this);
        languageRecyclerView.setAdapter(languagetAdapter);

        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean(IS_ERROR)) {
            findViewById(R.id.card_select_language).setVisibility(View.GONE);
            findViewById(R.id.rl).setVisibility(View.GONE);

            findViewById(R.id.layout_error).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        MobileConfigObserver.getInstance().addObserver(this);
        super.onResume();
        initView();
    }

    @Override
    protected void onPause() {
        MobileConfigObserver.getInstance().deleteObserver(this);
        super.onPause();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initView() {
        itemList.clear();
        Set<String> listLanguage = ConfigManager.getInstance().getConfig().getLanguageList().keySet();

        String languageSelected = SharedPref.getLanguageSelected();

        Iterator<String> iterator = listLanguage.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            SelectLanguageItemData itemData = new SelectLanguageItemData();
            itemData.setTitle(next);
            if (languageSelected.equalsIgnoreCase(next)) {
                itemData.setSelected(true);
            }
            itemList.add(itemData);
        }

        if (listLanguage != null && listLanguage.size() > 0) {
            languageTitle = itemList.get(0).getTitle();
        }

        languagetAdapter.notifyDataSetChanged();
    }

    @Override
    public void itemClicked(View view, int position) {
        languageTitle = itemList.get(position).getTitle();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                languageKey = Utils.getLanguageKey(this, languageTitle);
                Utils.setLocalLanguage(this, languageKey, languageTitle);
                Intent i = new Intent(this, AppTourActivity.class);
                startActivity(i);
                break;
            case R.id.btn_retry:
                mProgressDialog.show();
                requestConfig();
                break;
        }
    }

    public long getTransactionId() {
        return System.currentTimeMillis();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof MobileConfigObserver) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.cancel();
            }
            if (!((MobileConfigObserver) observable).isError()) {
                ConfigMO configMo = ConfigManager.getInstance().getConfig();
                if (getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).
                        getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() > 1) {
                    findViewById(R.id.card_select_language).setVisibility(View.VISIBLE);
                    findViewById(R.id.rl).setVisibility(View.VISIBLE);

                    findViewById(R.id.layout_error).setVisibility(View.GONE);
                    initView();
                } else if (getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).
                        getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() == 1) {
                    Set<String> strings = configMo.getLanguageList().keySet();
                    Iterator<String> iterator = strings.iterator();
                    String next = iterator.next();
                    String languageKey = configMo.getLanguageList().get(next);
                    Utils.setLocalLanguage(this, languageKey, next);
                    startActivity(new Intent(this, AppTourActivity.class));
                }
            }
        }
    }
}
