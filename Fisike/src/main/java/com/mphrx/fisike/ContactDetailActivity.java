package com.mphrx.fisike;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mphrx.fisike.customview.CustomFontTextView;

import com.mphrx.fisike.background.LoadContactsProfilePicAsyn;
import com.mphrx.fisike.background.LoadContactsRealName;
import com.mphrx.fisike.constant.PiwikConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.ProfilePicTaskManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.AppLog;

@SuppressLint("ResourceAsColor")
public class ContactDetailActivity extends BaseActivity implements OnClickListener {


    private ChatContactMO chatContactMO;
    private String email;
    private String realName;
    private boolean isRecentChat = false;
    private boolean isRegisteredBroadcast;
    private AsyncTask<Void, Void, Void> loadContactsRealName;
    private AsyncTask<Void, Void, Void> loadContactProfilePicAsyn;
    private boolean isGroupDetail = false;
    private boolean isOneAPIResponded = false;
    private ImageView ivCall, ivEmail;
    private squareImageView imgPic;
    private ProgressBar progressDialog;
    private CheckBox cbMute, cbShowNotifications;
    private com.mphrx.fisike.customview.CustomFontButton dialogCancel, dialogOK;
    private View view1;
    private DialogInterface alert;
    private ProgressBar pr_update_details;
    private CustomFontTextView tvStatusMessage, tvMyRole, tvContactProfileName, tvEmail, tvAboutMe, tvNotificationStatus, tvMobileNumber, tvSpeciality,
            tvNotificationLabel;

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolBarLayout;
    AlertDialog.Builder frequancydialog;
    private RadioGroup rgTime;
    private RadioButton rbEightHours, rbThreeHours, rbOneDay, rbOneWeek, rbOneYear;
    private int selectedIndex;
    private ChatConversationMO chatConversationMO;

    private String contactPhoneNumber;
    private CoordinatorLayout mCoOrdinatorlayout;
    private AppBarLayout myAppbar;

    private String senderId;
    /**
     * Broadcast for handling refresh contact detail
     */
    private BroadcastReceiver contactDetailBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            AppLog.d("Fisike", "com.mphrx.fisike.onReceive - Received Connection Broadcast");
            if (mBound) {
                AppLog.d("Fisike", "com.mphrx.fisike.onReceive - Now refresh the fields");
                ChatContactMO tempChatContactMO = new ChatContactMO();
                tempChatContactMO.generatePersistenceKey(senderId);
                tempChatContactMO = mService.getChatContactMo(tempChatContactMO.getPersistenceKey() + "");

                if (null != tempChatContactMO) {
                    chatContactMO = tempChatContactMO;
                }

                refreshFields();

            }
        }
    };

    private BroadcastReceiver statusChangeBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String chatPK = bundle.getString(VariableConstants.PERSISTENCE_KEY);
            if (chatContactMO != null && chatPK.equalsIgnoreCase(chatContactMO.getPersistenceKey() + "")) {
                chatContactMO.setPresenceType(bundle.getString(VariableConstants.PRESENCE_TYPE));
                chatContactMO.setPresenceStatus(bundle.getString(VariableConstants.PRESENCE_MESSAGE));
                changePresence();
            }
        }
    };
    private FrameLayout frameLayout;


    private void changePresence() {
        if (chatContactMO.getPresenceStatus() != null) {
            String presenceStatus = chatContactMO.getPresenceStatus();
            presenceStatus = presenceStatus.replace("\\n", System.getProperty("line.separator"));
            tvStatusMessage.setText(presenceStatus);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        //rendering layout inside framelayout to support side drawer
        getLayoutInflater().inflate(R.layout.activity_contact_profile, frameLayout);
        //setContentView(R.layout.activity_contact_profile);

        Bundle extras = getIntent().getExtras();

        isRecentChat = extras.getBoolean(TextConstants.IS_RECENT_CHAT);

        findViews();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            toolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        }

        try {
            isGroupDetail = extras.getBoolean(VariableConstants.IS_GROUP_DETAIL);
        } catch (Exception e) {

        }

        // Register the broadcast listener
        registerBroadcastListeners();
        refreshPage();
        /*PiwikUtils.sendEvent(getApplication(), PiwikConstants.KCONTACT_CATEGORY, PiwikConstants.KCONTACT_DETAILS_ACTION,
                PiwikConstants.KCONTACT_DETAILS_TXT);*/
    }

    private void findViews() {
        imgPic = (squareImageView) findViewById(R.id.iv_profile_picture);
        progressDialog = (ProgressBar) findViewById(R.id.progressDialog);
        pr_update_details = (ProgressBar) findViewById(R.id.pr_update_details);
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar_contact_detail);
        collapsingToolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCoOrdinatorlayout = (CoordinatorLayout) findViewById(R.id.cl_root);
        collapsingToolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        myAppbar = (AppBarLayout) findViewById(R.id.appbar);
        tvNotificationLabel = (CustomFontTextView) findViewById(R.id.tv_notifications);
        tvStatusMessage = (CustomFontTextView) findViewById(R.id.txtStatusMessage);
        tvEmail = (CustomFontTextView) findViewById(R.id.txtEmail);
        tvAboutMe = (CustomFontTextView) findViewById(R.id.txtAboutMe);
        tvNotificationStatus = (CustomFontTextView) findViewById(R.id.tv_notifications);
        tvMobileNumber = (CustomFontTextView) findViewById(R.id.tv_mobile_number);
        tvSpeciality = (CustomFontTextView) findViewById(R.id.txtSpeciality);
        cbMute = (CheckBox) findViewById(R.id.cb_mute);

    }

    private void showDialogforMuteChat() {
        frequancydialog = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view1 = layoutInflater.inflate(R.layout.mute_chat_layout, null);
        frequancydialog.setView(view1);
        dialogCancel = (com.mphrx.fisike.customview.CustomFontButton) view1.findViewById(R.id.btn_Cancel);
        dialogOK = (com.mphrx.fisike.customview.CustomFontButton) view1.findViewById(R.id.btn_Ok);
        rgTime = (RadioGroup) view1.findViewById(R.id.rg_mute_date_container);
        rbEightHours = (RadioButton) view1.findViewById(R.id.rb_8_hours);
        rbThreeHours = (RadioButton) view1.findViewById(R.id.rb_3_hours);
        rbOneDay = (RadioButton) view1.findViewById(R.id.rb_day1);
        rbOneWeek = (RadioButton) view1.findViewById(R.id.rb_week1);
        rbOneYear = (RadioButton) view1.findViewById(R.id.rb_year1);
        SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        int muteDurationIndex = preferences.getInt(VariableConstants.MUTE_CHAT_OPTION_ENABLED, 0);
        setRadioButtonSelected(muteDurationIndex);
        cbShowNotifications = (CheckBox) view1.findViewById(R.id.cb_notification);
        long muteEndDate;
        if (chatConversationMO != null)
            muteEndDate = 0;
        else
            muteEndDate = chatConversationMO.getMuteChatTillTime();
        int isShowNotificationEnabled = chatConversationMO.getIsNotificationEnabled();
        if (isShowNotificationEnabled < 0 && (muteEndDate >= System.currentTimeMillis())) {
            cbShowNotifications.setChecked(false);
        } else {
            cbShowNotifications.setChecked(true);
        }

        dialogCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ContentValues updatedValue = new ContentValues();
                tvNotificationLabel.setText(getResources().getString(R.string.chat_mute));
                updatedValue.put("muteChatTillTime", 0);
                // db call
                cbMute.setChecked(false);
                ChatConversationDBAdapter.getInstance(ContactDetailActivity.this).updateChatConversation(chatConversationMO.getPersistenceKey(), updatedValue);
                alert.dismiss();
            }
        });

        dialogOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                selectedIndex = rgTime.getCheckedRadioButtonId();
                long mutetTillDateDuration = getSelectedIndexText(selectedIndex);
                String senderId1 = senderId;
                boolean isShowNotificationEnabled = cbShowNotifications.isChecked();
                ContentValues updatedValue = new ContentValues();
                updatedValue.put("muteChatTillTime", mutetTillDateDuration);
                updatedValue.put("isNotificationEnabled", isShowNotificationEnabled ? 0 : -1);
                tvNotificationLabel.setText(getResources().getString(R.string.chat_unmute));
                // db call
                chatConversationMO.setMuteChatTillTime(mutetTillDateDuration);
                chatConversationMO.setIsNotificationEnabled(isShowNotificationEnabled ? 0 : -1);
                mService.setChatConversation(chatConversationMO);
                ChatConversationDBAdapter.getInstance(ContactDetailActivity.this).updateChatConversation(chatConversationMO.getPersistenceKey(), updatedValue);
                alert.dismiss();
            }

        });

        if (cbMute.isChecked()) {
            this.alert = frequancydialog.show();
        } else {
            // cb mute is unchecked
            ContentValues updatedValue = new ContentValues();
            tvNotificationLabel.setText(getResources().getString(R.string.chat_mute));
            updatedValue.put("muteChatTillTime", 0);
            updatedValue.put("isNotificationEnabled", 0);
            // db call
            chatConversationMO.setMuteChatTillTime(0);
            chatConversationMO.setIsNotificationEnabled(0);
            mService.setChatConversation(chatConversationMO);
            ChatConversationDBAdapter.getInstance(ContactDetailActivity.this).updateChatConversation(chatConversationMO.getPersistenceKey(), updatedValue);
        }

    }

    private void setRadioButtonSelected(int index) {
        switch (index) {
            case 0:
                // 8 hours
                rbEightHours.setChecked(true);
                break;
            case 1:
                // 3 hours
                rbThreeHours.setChecked(true);

                break;
            case 2:
                // 1 day
                rbOneDay.setChecked(true);
                break;
            case 3:
                // 1 week
                rbOneWeek.setChecked(true);

                break;
            case 4:
                // 1 year
                rbOneYear.setChecked(true);
                break;

            default:
                break;
        }

    }

    private long getSelectedIndexText(int index) {
        long mutetTillDateDuration = 0;
        String selectText = "";
        switch (index) {
            case R.id.rb_8_hours:
                mutetTillDateDuration = Utils.getMuteChatTime(8, 0, 0, 0);
                selectText = TextConstants.HOURS_8;
                storeMuteDurationInSharedPreferences(0);
                break;

            case R.id.rb_3_hours:
                selectText = TextConstants.HOURS_3;
                mutetTillDateDuration = Utils.getMuteChatTime(3, 0, 0, 0);
                storeMuteDurationInSharedPreferences(1);
                break;

            case R.id.rb_day1:
                selectText = TextConstants.DAY_1;
                mutetTillDateDuration = Utils.getMuteChatTime(0, 1, 0, 0);
                storeMuteDurationInSharedPreferences(2);
                break;

            case R.id.rb_week1:
                selectText = TextConstants.WEEK_1;
                mutetTillDateDuration = Utils.getMuteChatTime(0, 0, 7, 0);
                storeMuteDurationInSharedPreferences(3);
                break;

            case R.id.rb_year1:
                selectText = TextConstants.YEAR_1;
                mutetTillDateDuration = Utils.getMuteChatTime(0, 0, 0, 1);
                storeMuteDurationInSharedPreferences(4);
                break;

            default:
                break;
        }
        return mutetTillDateDuration;
    }

    private void storeMuteDurationInSharedPreferences(int position) {
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0).edit().putInt(VariableConstants.MUTE_CHAT_OPTION_ENABLED, position).commit();

    }

    private void refreshPage() {
        if (isRecentChat || isGroupDetail) {
            senderId = getIntent().getExtras().getString(VariableConstants.SENDER_ID);
            senderId = Utils.cleanXMPPServer(senderId);
            ChatContactMO tempChatContactMO = new ChatContactMO();
            tempChatContactMO.generatePersistenceKey(senderId);
            if (!mBound || null == mService) {
                return;
            }
            chatContactMO = mService.getChatContactMo(tempChatContactMO.getPersistenceKey() + "");

            executeApiCall();

        } else {
            chatContactMO = (ChatContactMO) getIntent().getExtras().get(TextConstants.CHAT_DETAIL);
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(contactDetailBroadcast);
            } catch (Exception e) {
            }
            getUpdatedProfilePicStatus();
            senderId = chatContactMO.getId();

        }
        refreshMute();
        refreshFields();
    }

    private void refreshMute() {
        // chatConversationMO = (ChatConversationMO) getIntent().getExtras().get(VariableConstants.CONVERSATION_ITEM);
        ChatConversationMO chatConvMO = new ChatConversationMO();
        if (senderId.contains(SettingManager.getInstance().getSettings().getFisikeServerIp())) {
            chatConvMO.setJId(senderId);
        } else {
            chatConvMO.setJId(senderId + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp());
        }
        // chatConvMO.generatePersistenceKey(userMO.getUserName());
        chatConvMO.generatePersistenceKey(userMO.getId() + "");
        try {
            chatConversationMO = ChatConversationDBAdapter.getInstance(this).fetchChatConversationMO(chatConvMO.getPersistenceKey() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (chatConversationMO != null) {
            long muteEndDate = chatConversationMO.getMuteChatTillTime();
            long now = System.currentTimeMillis();
            if (muteEndDate >= now) {
                cbMute.setChecked(true);
                tvNotificationLabel.setText(getResources().getString(R.string.chat_unmute));
            } else {
                cbMute.setChecked(false);
                tvNotificationLabel.setText(getResources().getString(R.string.chat_mute));
            }

            // don't show the notifications if the show notifications are disabled

        }
        cbMute.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                showDialogforMuteChat();
            }
        });
    }

    private void getUpdatedProfilePicStatus() {
        byte[] profilePic = chatContactMO.getProfilePic();
        if (mBound) {
            ChatContactMO tempChatContactMO = mService.getChatContactMo(chatContactMO.getPersistenceKey() + "");
            if (null != tempChatContactMO) {

                if ((null == profilePic || profilePic.length <= 0)) {
                    chatContactMO.setProfilePic(tempChatContactMO.getProfilePic());
                    chatContactDBAdapter.getInstance(ContactDetailActivity.this).updateChatContactMoUserProfile(chatContactMO.getPersistenceKey(), chatContactMO, false);
                } else {
                    chatContactDBAdapter.getInstance(ContactDetailActivity.this).updateChatContactMoUserProfile(chatContactMO.getPersistenceKey(), chatContactMO, true);

                }
                if (tempChatContactMO.getPresenceType() != null) {
                    chatContactMO.setPresenceType(tempChatContactMO.getPresenceType());
                }
                if (tempChatContactMO.getPresenceStatus() != null) {
                    chatContactMO.setPresenceStatus(tempChatContactMO.getPresenceStatus());
                }
            }
        }

        senderId = chatContactMO.getId();
        if (mBound && null != mService) {
            //mService.updateChatContactDetails(chatContactMO);
            pr_update_details.setVisibility(View.INVISIBLE);

            if (Utils.showDialogForNoNetwork(this, false)) {
                if (progressDialog != null) {
                    progressDialog.setVisibility(View.VISIBLE);
                }
                loadContactProfilePicAsyn = new LoadContactsProfilePicAsyn(mService, Utils.cleanXMPPServer(senderId), this, imgPic, progressDialog)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    private void executeApiCall() {
        if (Utils.showDialogForNoNetwork(this, false)) {
            loadContactsRealName = new LoadContactsRealName(mService, senderId, this).execute();
            loadContactProfilePicAsyn = new LoadContactsProfilePicAsyn(mService, senderId, this).execute();
            pr_update_details.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (null != loadContactsRealName && !loadContactsRealName.isCancelled()) {
            loadContactsRealName.cancel(true);
        }
        if (null != loadContactProfilePicAsyn && !loadContactProfilePicAsyn.isCancelled()) {
            loadContactProfilePicAsyn.cancel(true);
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(VariableConstants.UPDATED_CONTACT, chatContactMO);
        setResult(RESULT_OK, returnIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    /**
     * This method refresh the fields of the attributes
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void refreshFields() {

        if (null == chatContactMO) {
            return;
        }

        realName = chatContactMO.getRealName();
        realName = (null != realName && !realName.equals("") && !realName.equals(getResources().getString(R.string.txt_null))) ? realName : "";

        if (collapsingToolBarLayout != null) {
            if (!realName.trim().equals(""))
                collapsingToolBarLayout.setTitle(Utils.toCamelCase(realName));
            collapsingToolBarLayout.setExpandedTitleColor(getResources().getColor(R.color.white));
            collapsingToolBarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
            collapsingToolBarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
            collapsingToolBarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1);
            collapsingToolBarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);
            collapsingToolBarLayout.setExpandedTitleGravity(Gravity.BOTTOM | Gravity.CENTER);

        }

        changePresence();
        email = chatContactMO.getEmail();
        tvEmail.setText(email);
        contactPhoneNumber = chatContactMO.getPhoneNo();
        if (contactPhoneNumber != null || contactPhoneNumber != "")
            tvMobileNumber.setText(contactPhoneNumber);

        String speciality = chatContactMO.getSpeciality();
        speciality = (null != speciality && !speciality.equals("") && !speciality.equals(getResources().getString(R.string.txt_null))) ? speciality : "";
        tvSpeciality.setText(speciality);

        String designation = chatContactMO.getDesignation();
        designation = (null != designation && !designation.equals("") && !designation.equals(getResources().getString(R.string.txt_null))) ? designation : "";
        tvMyRole = (CustomFontTextView) findViewById(R.id.tv_my_role);
        tvMyRole.setText(designation);
        ivCall = (ImageView) findViewById(R.id.iv_icon_mobile);
        ivEmail = (ImageView) findViewById(R.id.iv_icon_email);
        ivCall.setOnClickListener(this);
        ivEmail.setOnClickListener(this);

        String aboutMe = chatContactMO.getAboutMe();
        aboutMe = (null != aboutMe && !aboutMe.equals("") && !aboutMe.equals(getResources().getString(R.string.txt_null))) ? aboutMe : "";

        tvAboutMe.setMovementMethod(new ScrollingMovementMethod());
        tvAboutMe.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        tvAboutMe.setText(aboutMe);
        byte[] profilePic = chatContactMO.getProfilePic();

        if (null != profilePic && profilePic.length != 0) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(profilePic, 0, profilePic.length);
            //imgPic.setBackground(new BitmapDrawable(getResources(), bitmap));
            imgPic.setImageBitmap(bitmap);
        } else if (null != chatContactMO && null != chatContactMO.getUserType() && !"".equals(chatContactMO.getUserType())) {
            if (chatContactMO.getUserType() == null || chatContactMO.getUserType().equals(TextConstants.PATIENT)) {
                imgPic.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.img_profile_default_full));
//				imgPic.setBackgroundResource(R.drawable.img_profile_default_full);
            } else {
//				imgPic.setBackgroundResource(R.drawable.img_profile_default_full);
                imgPic.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.img_profile_default_full));

            }
        }

        myAppbar.post(new Runnable() {
            @Override
            public void run() {
                int heightPx = imgPic.getHeight();
                setAppBarOffset(heightPx / 2);
            }
        });

    }

    private void setAppBarOffset(int offsetPix) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) myAppbar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.onNestedPreScroll(mCoOrdinatorlayout, myAppbar, null, 0, offsetPix, new int[]{0, 0});
    }

    /**
     * Register broadcast register
     */
    private void registerBroadcastListeners() {
        if (!isRegisteredBroadcast) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            broadcastManager.registerReceiver(contactDetailBroadcast, new IntentFilter(VariableConstants.BROADCAST_CONTACT_DETAIL_CHANGE));
            broadcastManager.registerReceiver(statusChangeBroadcast, new IntentFilter(VariableConstants.BROADCAST_CHAT_CONTACT_PRESENCE_CHANGED));
            isRegisteredBroadcast = true;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_icon_mobile:
                if (contactPhoneNumber != null && contactPhoneNumber != "") {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    String number = getResources().getString(R.string.tel) + contactPhoneNumber;
                    intent.setData(Uri.parse(number));
                    startActivity(intent);
                }
                break;
            case R.id.iv_icon_email:
                String emailTo = "";
                if (email != null) {
                    emailTo = email;
                }
                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.mailto) + emailTo));
                intent1.putExtra(Intent.EXTRA_SUBJECT, "");
                intent1.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(this, SettingManager.getInstance().getUserMO()));
                startActivity(intent1);
                break;

            case R.id.rb_3_hours:
            case R.id.rb_8_hours:
            case R.id.rb_day1:
            case R.id.rb_week1:
            case R.id.rb_year1:
                selectUnselectRadioButton((RadioButton) view);
                break;
            default:
                break;
        }
    }

    private void selectUnselectRadioButton(RadioButton v) {
        if (v.isChecked())
            v.setChecked(!v.isChecked());

    }

    private void chatNow() {
        String sender = "";
        if (null != chatContactMO && null != chatContactMO.getId()) {
            sender = chatContactMO.getId();
        } else {
            try {
                sender = URLEncoder.encode(senderId, "UTF-8") + "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
            } catch (UnsupportedEncodingException e) {
            }
        }
        Intent intent = new Intent(this, MessageActivity.class);
        if (null != realName && !("".equals(realName))) {
            intent.putExtra(VariableConstants.SENDER_NICK_NAME, realName);
        }
        if (!isRecentChat) {
            intent.putExtra(VariableConstants.ADD_SCREEN, true);
        }
        intent.putExtra(VariableConstants.SENDER_ID, sender);
        intent.putExtra(TextConstants.CHAT_DETAIL, chatContactMO);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == 0 && (item.getTitle().equals(TextConstants.ERROR) || item.getTitle().equals(TextConstants.REFRESH))) {
            executeApiCall();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

	/*
     * @Override public boolean onCreateOptionsMenu(Menu menu) { MenuInflater inflater = getMenuInflater();
	 * inflater.inflate(R.menu.contact_detail_menu, menu); if (isRecentChat || isGroupDetail) { if (Utils.isNetworkAvailable(this)) {
	 * pr_update_details.setVisibility(View.VISIBLE); } } return true; }
	 */

    /**
     * Close the laoding view
     *
     * @param jsonString
     * @param isProfilePicApi
     */
    public void postExecute(String jsonString, boolean isProfilePicApi) {
        if (isOneAPIResponded) {
            pr_update_details.setVisibility(View.INVISIBLE);
        }
        isOneAPIResponded = true;
        if (isProfilePicApi) {
            ProfilePicTaskManager.getInstance().removeTaskIfExist(senderId);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterBroadcastListeners();
        super.onDestroy();
    }

    private void unregisterBroadcastListeners() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(contactDetailBroadcast);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusChangeBroadcast);
    }

    @Override
    public void onMessengerServiceBind() {
        super.onMessengerServiceBind();
        refreshPage();
    }
}
