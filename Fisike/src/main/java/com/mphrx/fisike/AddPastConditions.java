package com.mphrx.fisike;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.update_user_profile.UpdateUserProfileActivity;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.util.Calendar;

import appointment.utils.CommonControls;
import appointment.utils.CommonTasks;

/**
 * Created by laxmansingh on 10/10/2016.
 */

public class AddPastConditions extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private Context mContext;
    private FrameLayout frameLayout;

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private CustomFontButton mBtnSave;
    private CustomFontEditTextView mEt_condition, mEt_status, mEt_severity, mEt_from, mEt_to;
    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.addpastconditions, frameLayout);
        mContext = this;
        initToolbar();
        findViews();
        bindViews();
    }

    private void initToolbar() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.addcondition));
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
    }


    private void findViews() {
        mBtnSave = (CustomFontButton) findViewById(R.id.btnSave);
        mEt_condition = (CustomFontEditTextView) findViewById(R.id.et_condition);
        mEt_status = (CustomFontEditTextView) findViewById(R.id.et_status);
        mEt_severity = (CustomFontEditTextView) findViewById(R.id.et_severity);
        mEt_from = (CustomFontEditTextView) findViewById(R.id.et_from);
        mEt_to = (CustomFontEditTextView) findViewById(R.id.et_to);
    }

    private void bindViews() {
        setDateLisner();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mEt_from.setText(DateTimeUtil.getFormattedDateWithoutUTC(((monthOfYear + 1) + "-" + dayOfMonth + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (checkValidations()) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.Saved), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean checkValidations() {
        String condition = mEt_condition.getText().toString().trim();
        String status = mEt_status.getText().toString().trim();
        String severity = mEt_severity.getText().toString().trim();
        String from = mEt_from.getText().toString().trim();
        String to = mEt_to.getText().toString().trim();

        if (condition.equals("")) {
            mEt_condition.setError(mContext.getResources().getString(R.string.validation_enter_condition));
            return false;
        } else if (status.equals("")) {
            mEt_status.setError(mContext.getResources().getString(R.string.validation_enter_status));
            return false;
        } else if (severity.equals("")) {
            mEt_severity.setError(mContext.getResources().getString(R.string.validation_severity));
            return false;
        } else if (from.equals("")) {
            mEt_from.setError(mContext.getResources().getString(R.string.validation_enter_from));
            return false;
        }

        return true;
    }


    /**
     * Initialize the date picker dialog and set listner on the dob field
     */
    private void setDateLisner() {
        int year, month, day;
        final Calendar c = Calendar.getInstance();
        if (!mEt_from.getText().trim().toString().equals("")) {
            String selectedfrom = CommonTasks.formateDateFromstring(DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.dd_MM_yyyy_HiphenSeperatedDate, mEt_from.getText().trim().toString());
            String[] caldate = selectedfrom.split("-");
            day = Integer.parseInt(caldate[0]);
            month = Integer.parseInt(caldate[1]);
            year = Integer.parseInt(caldate[2]);
        } else {
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        datePickerDialog = new DatePickerDialog(mContext, AddPastConditions.this, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

        mEt_from.setInputType(InputType.TYPE_NULL);
        mEt_from.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.hideKeyboard(AddPastConditions.this);
                datePickerDialog.show();
                return false;
            }
        });
        mEt_from.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(AddPastConditions.this);
                    datePickerDialog.show();
                }
            }
        });
    }

}
