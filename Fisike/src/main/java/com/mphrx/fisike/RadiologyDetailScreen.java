package com.mphrx.fisike;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.adapter.SeriesListAdapter;
import com.mphrx.fisike.beans.Series;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import careplan.activity.CareTaskActivity;
import careplan.activity.CareTaskFilterActivity;
import careplan.constants.CarePlanConstants;

public class RadiologyDetailScreen extends BaseActivity {
    private ListView listView;
    private View header;
    private List<Series> seriesList = new ArrayList<Series>();
    private Toolbar addToolbar;

    private TextView textView;

    private ProgressBar progressBar;

//	private String id = "157583", token = "81d6l3u5ultms72e6b7e8dms52hjddae", host = "minerva2";
//	private String id = "19260", token = "7mf4i9n7e9efhufm2d3qu2l8e07gaseg", host = "minerva1";
//	private String id = "679", token = "k3b47f49m1vlh7qmcf6gmkeotsud8lip", host = "minerva";


    private String jsonString, imagingStudyUrl, conclusion, id, token;
    private ProgressDialog progressDialog;

    private SeriesListAdapter adapter;

    private int arrayListLength;
    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        // setting toolbar to radiology report
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);
        id = getIntent().getStringExtra("id");
        token = getIntent().getStringExtra("token");
//    	Collections.synchronizedList(seriesList)/;


        addToolbar = (Toolbar) findViewById(R.id.toolbar_add_contact);

        progressBar = (ProgressBar) findViewById(R.id.pbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);


        listView = (ListView) findViewById(R.id.listView1);

        LayoutInflater inflater = LayoutInflater.from(this);
        header = inflater.inflate(R.layout.header, listView, false);
        textView = (TextView) header.findViewById(R.id.header_list);
        listView.addHeaderView(header, null, false);

        header.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        initializeToolbar();

        adapter = new SeriesListAdapter(this, seriesList, token);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                // TODO Auto-generated method stub
                try {
                    new ViewerAsyncTask(APIManager.getInstance().generateDiagnosticViewerURL
                            (RadiologyDetailScreen.this), new JSONObject().put("studyId", (
                            (Series) arg0.getItemAtPosition(arg2)).imagingStudyId).put("seriesId",
                            ((Series) arg0.getItemAtPosition(arg2)).seriesId)).executeOnExecutor
                            (AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Toast.makeText(RadiologyDetailScreen.this, getResources().getString(R.string
                            .no_xeroViewer), Toast.LENGTH_LONG).show();
                }
            }
        });

        new DisgnosticReportAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            addToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            addToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) addToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        ((CustomFontTextView) addToolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.Radiology_Detail));
        bt_toolbar_right = (IconTextView) addToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        addToolbar.findViewById(R.id.toolbar_right_img).setVisibility(View.GONE);
        setSupportActionBar(addToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private class ViewerAsyncTask extends AsyncTask<Void, Void, String> {

        private String url;
        private JSONObject jsonObject;

        public ViewerAsyncTask(String url, JSONObject jsonObject) {
            this.url = url;
            this.jsonObject = jsonObject;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog.setMessage(getResources().getString(R.string.loading_xeroViewer));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub


            String url = null;

            try {

                String jsonString = APIManager.getInstance().executeHttpPost(this.url, token,
                        jsonObject, RadiologyDetailScreen.this);

                JSONObject xeroJsonObject = new JSONObject(jsonString);

                url = xeroJsonObject.getString("ticketUrl");


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
//				System.out.println(url);
//				Toast.makeText(RadiologyDetailScreen.this, "Network Error",  1).show();

            }

            return url;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            progressDialog.dismiss();
            if (result != null)
                startActivity(new Intent(RadiologyDetailScreen.this, XeroViewerWebview.class)
                        .putExtra("targetUrl", result));
            else
                Toast.makeText(RadiologyDetailScreen.this, getResources().getString(R.string
                        .no_xeroViewer), Toast.LENGTH_LONG).show();


        }

    }


    private class DisgnosticReportAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
//			progressDialog.setMessage(getResources().getString(R.string.hang_on_tight));
//			progressDialog.show();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String searchDisgnosticReportUrl = APIManager.getInstance()
                        .getDiagnosticReportSearchUrl(RadiologyDetailScreen.this);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("request", id);
                JSONObject constraintsObject = new JSONObject();
                constraintsObject.put("constraints", jsonObject);

                jsonString = APIManager.getInstance().executeHttpPost(searchDisgnosticReportUrl,
                        token, constraintsObject, RadiologyDetailScreen.this);

                //loadDisgnosticReport();

                if (loadDisgnosticReport() && imagingStudyUrl != null) {
                    jsonString = APIManager.getInstance().executeHttpPost(imagingStudyUrl, token,
                            new JSONObject(), RadiologyDetailScreen.this);

                    JSONObject imagingStudyObject = new JSONObject(jsonString);

                    String studyId = imagingStudyObject.getString("uid");
                    JSONArray seriesArray = imagingStudyObject.getJSONArray("series");

                    arrayListLength = seriesArray.length();

                    for (int i = 0; i < seriesArray.length(); i++) {
                        final Series seriesObj = new Series();

                        JSONObject series = seriesArray.getJSONObject(i);

                        // String seriesId = series.getString("uid");
                        final String seriesUrl = series.optString("ref");

                        seriesObj.sortingId = series.optInt("id");
                        seriesObj.imagingStudyId = studyId;


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new SeriesAsyncTask(seriesUrl, seriesObj).executeOnExecutor
                                        (AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        });

                    }
                } else
                    jsonString = null;
            } catch (Exception e) {
                jsonString = null;
//				Toast.makeText(RadiologyDetailScreen.this, "Data Error",  1).show();
                e.printStackTrace();
            }


            return jsonString;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
//			System.out.println(result);

            if (result != null || conclusion != null) {
                if (conclusion.equals("null") || conclusion.equals(""))
                    textView.setText(getResources().getString(R.string.report_not_avaialble));
                else {
                    conclusion = conclusion.replace("\n", "<br>");
                    textView.setText(Html.fromHtml(conclusion));
                }

                if (result == null) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(RadiologyDetailScreen.this, getResources().getString(R.string.images_unavailable), Toast
                            .LENGTH_LONG).show();
                }

            } else {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(RadiologyDetailScreen.this, getResources().getString(R.string
                        .api_error), Toast.LENGTH_LONG).show();
            }

        }

        private boolean loadDisgnosticReport() {

            try {
                JSONObject reportJsonObject = new JSONObject(jsonString);
                JSONArray reportArray = reportJsonObject.getJSONArray("list");

                for (int i = 0; i < reportArray.length(); i++) {
                    JSONObject listObj = reportArray.getJSONObject(i);

                    conclusion = listObj.optString("conclusion");

                    JSONArray imagingStudyArray = listObj.getJSONArray("imagingStudy");
                    JSONObject imagingStudyObject = imagingStudyArray.getJSONObject(0);

                    imagingStudyUrl = imagingStudyObject.optString("ref");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }

            return true;

        }
    }


    private class SeriesAsyncTask extends AsyncTask<Void, Void, String> {

        String url;
        Series series;

        public SeriesAsyncTask(String url, Series series) {
            this.url = url;
            this.series = series;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub

            String jsonString = APIManager.getInstance().executeHttpPost(url, token, new
                    JSONObject(), RadiologyDetailScreen.this);

            JSONObject instanceJsonObject;
            try {
                instanceJsonObject = new JSONObject(jsonString);

                series.description = instanceJsonObject.getString("description");
                series.thumbUrl = instanceJsonObject.getString("url");
                series.seriesId = instanceJsonObject.getString("uid");

                JSONArray instanceJsonArray = instanceJsonObject.getJSONArray("instance");
                series.instanceId = instanceJsonArray.getJSONObject(0).getInt("id");

                series.numberOfInstances = instanceJsonObject.getJSONObject("numberOfInstances")
                        .getInt("value");

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                jsonString = null;
            }
            return jsonString;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (result == null) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(RadiologyDetailScreen.this, getResources().getString(R.string
                            .api_error), Toast.LENGTH_LONG).show();
                }
                return;
            }

            if (progressBar.getVisibility() == View.VISIBLE)
                progressBar.setVisibility(View.GONE);


            seriesList.add(series);
            Collections.sort(seriesList);
            adapter.notifyDataSetChanged();

            if (arrayListLength == seriesList.size()) {

            }

        }

    }
}