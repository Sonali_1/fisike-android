package com.mphrx.fisike.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import com.mphrx.fisike.AppTour.AppTourActivity;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.LaunchingLanguageSelectActivity;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import login.activity.LoginActivity;

public class LaunchingScreen {

    public static LaunchingScreen launchingScreen;
    private UserMO userMO;
    private SettingMO settings;
    private SharedPreferences preferences;
    private boolean isToSetPassword;
    private ConfigMO configMo;
    private boolean isToShowPin;
    private boolean isOTPVerified;
    private boolean isUserExist;
    private boolean isSignUpScreenPassed;
    private boolean isVerifiedUser;
    private boolean isAppTourShown;

    private LaunchingScreen() {
    }

    public static LaunchingScreen getInstance() {
        if (launchingScreen == null) {
            launchingScreen = new LaunchingScreen();
        }
        return launchingScreen;
    }

    public void launchDesion(Activity activity) {
        setLaunchingScreen(activity);
    }

    public void setLaunchingScreen(Activity activity) {
        SettingManager settingManager = SettingManager.getInstance();
        settings = settingManager.getSettings();
        configMo = ConfigManager.getInstance().getConfig();
        userMO = settingManager.getUserMO();
        preferences = activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        isAppTourShown = preferences.getBoolean(VariableConstants.APP_TOUR_SHOWN, false);
        Intent launchIntent = null;
        if (isAppTourShown) {
            if (userMO != null)
                launchIntent = decideScreenBasedOnUserMo(activity);
            else
                launchIntent = new Intent(activity, LoginActivity.class);
        } else if (activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).
                getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() > 1)
            launchIntent = new Intent(activity, LaunchingLanguageSelectActivity.class);
        else if (activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).
                getString(SharedPref.LANGUAGE_SELECTED, null) == null && configMo.getLanguageList() != null && configMo.getLanguageList().size() == 1) {
            Set<String> strings = configMo.getLanguageList().keySet();
            Iterator<String> iterator = strings.iterator();
            String next = iterator.next();
            String languageKey = configMo.getLanguageList().get(next);
            Utils.setLocalLanguage(activity, languageKey, next);
            launchIntent = new Intent(activity, AppTourActivity.class);
        } else {
            launchIntent = new Intent(activity, AppTourActivity.class);
        }
        if (launchIntent != null) {
            activity.startActivity(launchIntent);
            activity.finish();
        }
    }


    private Intent decideScreenBasedOnUserMo(Activity activity) {
        Intent launchIntent = new Intent(activity, LoginActivity.class);
        isToShowPin = configMo.isShowPin();
        if (userMO.getSalutation() != null && userMO.getSalutation().equalsIgnoreCase(TextConstants.YES))//logged in user
        {
            // checks whether to show the alternate contact prompt set the entry in shared pref viz eventually consumed by home activity.
            Utils.checkAlternateContactDetails(userMO.getAlternateContact());
            preferences.edit().putBoolean(VariableConstants.IS_LOGOUT_DIALOG_SHOWING, true).commit();
            launchIntent = new Intent(activity, HomeActivity.class);
        }
        return launchIntent;
    }


    public void notificationFlow(Activity activity, Bundle extras) {
        SettingManager settingManager = SettingManager.getInstance();
        configMo = ConfigManager.getInstance().getConfig();
        userMO = settingManager.getUserMO();

        if (!configMo.isShowPin()) {
            if (userMO != null && userMO.getSalutation().equalsIgnoreCase(TextConstants.YES)) {
                activity.startActivity(new Intent(activity, HomeActivity.class).putExtras(extras));
            } else {
                launchDesion(activity);
            }
            return;
        }
        settings = settingManager.getSettings();

        if (settings.isAutoLogout()) {
            if (userMO == null || userMO.getSalutation().equalsIgnoreCase("NO")) {
                return;
            }

            Calendar startDateTime = Calendar.getInstance();
            startDateTime.setTime(new Date(userMO.getLastAuthenticatedTime()));

            Calendar endDateTime = Calendar.getInstance();
            long currentTimeStamp = Calendar.getInstance().getTime().getTime();
            endDateTime.setTime(new Date(currentTimeStamp));

            long milliseconds1 = startDateTime.getTimeInMillis();
            long milliseconds2 = endDateTime.getTimeInMillis();
            long diff = milliseconds2 - milliseconds1;

            long diffSeconds = diff / 1000;

            long autoLogoutTimeSeconds = (long) settings.getAutoLogoutTimeInterval() * 60;

            // Check for time difference with last Authentication time

            if (diffSeconds < autoLogoutTimeSeconds) {
                setUserInteraction();
                return;
            }
        } else {
            setUserInteraction();
            return;
        }
        launchDesion(activity);
    }

    /**
     * Update the last user used
     */
    private void setUserInteraction() {
        userMO.setLastAuthenticatedTime(System.currentTimeMillis());
        try {
            SettingManager.getInstance().updateUserMO(userMO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}