package com.mphrx.fisike.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.webkit.MimeTypeMap;

import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.Entity;
import com.facebook.crypto.util.SystemNativeCryptoLibrary;
import com.mphrx.fisike_physician.utils.ProfilePicUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Citadel {
    private static Crypto crypto;


    public static byte[] getByteArray(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int imageSize = bitmap.getByteCount();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
        return bos.toByteArray();
    }

    public static byte[] getByteArrayFile(String actualFilePath) {
        File file = new File(actualFilePath);
        int size = (int) file.length();
        byte[] fileData = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(fileData, 0, fileData.length);
            buf.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        return fileData;
    }

    // context would be application context
    // need to decide upon the return type
    // maybe the file path
    public static String encodeFile(String filePath, Context _context, String appName, String fileName) {
        File mypath = null;
        try {

            mypath = createFile(Environment.getExternalStorageDirectory(), "/" + appName, fileName + System.currentTimeMillis() + "");
            crypto = setUpConceal(_context);
            if (!crypto.isAvailable()) {
                return null;
            }
            OutputStream fileStream = new BufferedOutputStream(new FileOutputStream(mypath));
            OutputStream outputStream = crypto.getCipherOutputStream(fileStream, new Entity("MphRxPassword"));

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            Uri selectedUri = Uri.fromFile(new File(filePath));
            String fileExtension
                    = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String type
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            if(type != null && type.contains("image") ){

                Bitmap saveOrientedBitmap = ProfilePicUtils.safeDecodeFile(_context, filePath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                saveOrientedBitmap.compress(Bitmap.CompressFormat.PNG, 100/*ignored for PNG*/, bos);
                outputStream.write(bos.toByteArray());
                saveOrientedBitmap.recycle();
            }else{
                FileInputStream fileinputStream = new FileInputStream(filePath);
                int read;
                byte[] buffer = new byte[1024];
                while ((read = fileinputStream.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                outputStream.write(out.toByteArray());
                fileinputStream.close();
            }

            outputStream.close();

        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return mypath.getAbsolutePath();
    }

    public static String encodeFile(Context _context, String appName, String fileName, File mypath, byte[] fileData) {
        if (fileName == null || fileName.equals("")) {
            fileName = System.currentTimeMillis() + "";
        }
        try {
            InputStream fileinputStream = new ByteArrayInputStream(fileData);
            if (mypath == null) {
                mypath = createFile(Environment.getExternalStorageDirectory(), File.separator + appName, "" + fileName + "");
            }

            crypto = setUpConceal(_context);
            if (!crypto.isAvailable()) {
                return null;
            }
            OutputStream fileStream = new BufferedOutputStream(new FileOutputStream(mypath));
            OutputStream outputStream = crypto.getCipherOutputStream(fileStream, new Entity("MphRxPassword"));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int read;
            byte[] buffer = new byte[1024];
            while ((read = fileinputStream.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            fileinputStream.close();
            outputStream.write(out.toByteArray());
            outputStream.close();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return mypath.getAbsolutePath();
    }

    public static String encodeFile(Bitmap bitmap, Context _context, String appName, String fileName, File mypath) {
        if (fileName == null || fileName.equals("")) {
            fileName = System.currentTimeMillis() + "";
        }
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int imageSize  = bitmap.getByteCount();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80 , bos);
            byte[] bitmapdata = bos.toByteArray();
            InputStream fileinputStream = new ByteArrayInputStream(bitmapdata);
            if (mypath == null) {
                mypath = createFile(Environment.getExternalStorageDirectory(), File.separator + appName, "" + fileName + "");
            }

            crypto = setUpConceal(_context);
            if (!crypto.isAvailable()) {
                return null;
            }
            OutputStream fileStream = new BufferedOutputStream(new FileOutputStream(mypath));
            OutputStream outputStream = crypto.getCipherOutputStream(fileStream, new Entity("MphRxPassword"));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int read;
            byte[] buffer = new byte[1024];
            while ((read = fileinputStream.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            fileinputStream.close();
            outputStream.write(out.toByteArray());
            outputStream.close();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return mypath.getAbsolutePath();
    }

    public static String encodeBytes(FileOutputStream fileOutputStream, Context _context, String appName) {
        File mypath = null;
        try {
            // OutputStream fileinputStream = new BufferedOutputStream(fileOutputStream);
            //
            // FileInputStream fileinputStream = new FileInputStream(filePath);
            mypath = createFile(Environment.getExternalStorageDirectory(), "/" + appName, System.currentTimeMillis() + "");

            crypto = setUpConceal(_context);
            if (!crypto.isAvailable()) {
                return null;
            }
            OutputStream fileStream = new BufferedOutputStream(fileOutputStream);
            // OutputStream outputStream = crypto.getMacOutputStream(fileOutputStream, new Entity("MphRxPassword"));
            OutputStream outputStream = crypto.getCipherOutputStream(fileStream, new Entity("MphRxPassword"));
            // ByteArrayOutputStream out = new ByteArrayOutputStream();
            // int read;
            // byte[] buffer = new byte[1024];
            // while ((read = fileinputStream.read(buffer)) != -1) {
            // out.write(buffer, 0, read);
            // }
            // fileinputStream.close();
            // outputStream.write(out.toByteArray());
            outputStream.close();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return mypath.getAbsolutePath();
    }

    public static byte[] decodFile(String filePath, Context context) {
        ByteArrayOutputStream out = null;
        byte[] decodedByteArray = null;
        try {
            crypto = setUpConceal(context);
            if (!crypto.isAvailable()) {
                return null;
            }
            FileInputStream fileStream = new FileInputStream(filePath);
            InputStream inputStream = crypto.getCipherInputStream(fileStream, new Entity("MphRxPassword"));
            out = new ByteArrayOutputStream();
            int read;
            byte[] buffer = new byte[1024];

            while ((read = inputStream.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            inputStream.close();
            decodedByteArray = out.toByteArray();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decodedByteArray;

    }

    private static Crypto setUpConceal(Context context) {
        crypto = new Crypto(new SharedPrefsBackedKeyChain(context), new SystemNativeCryptoLibrary());
        return crypto;
    }

    /*
     * input rootFolder - the storage location directoryName(/rajan)- the name of directory fileName - the name of file output myPath(File) - is
     * filepath of encrypted file location
     */
    public static File createFile(File rootFolder, String directoryName, String fileName) {
        File sdCard = rootFolder;
        File dir = new File(sdCard.getAbsolutePath() + directoryName);
        if (!dir.exists())
            dir.mkdirs();
        File myPath = new File(dir, fileName);
        return myPath;
    }
}
