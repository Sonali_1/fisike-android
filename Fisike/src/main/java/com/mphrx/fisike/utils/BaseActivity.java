package com.mphrx.fisike.utils;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.Settings;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.adapter.NavigationDrawerAdapter;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.broadcast.ConnectivityChanged;
import com.mphrx.fisike.connection.ConnectionStatus;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.ChatFragment;
import com.mphrx.fisike.fragments.Home;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.fragments.NotificationCenterFragment;
import com.mphrx.fisike.fragments.VitalsFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.record_screen.fragment.GenralFragment;
import com.mphrx.fisike.services.RegistrationIntentService;
import com.mphrx.fisike.vaccination.adapter.VaccinationListAdapter;
import com.mphrx.fisike.vaccination.background.OpenVaccinationFragmenet;
import com.mphrx.fisike.vaccination.background.VaccinationJosnParsing;
import com.mphrx.fisike.vaccination.fragments.VaccinationListFragment;
import com.mphrx.fisike.vaccination.fragments.VaccinationProfileFragment;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;
import com.mphrx.fisike_physician.activity.MessageListActivity;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;

import appointment.fragment.MyAppointmentFragment;
import appointment.phlooba.fragment.HomeDrawListingFragment;
import appointment.phlooba.fragment.PhloobaHomeFragment;
import appointment.phlooba.fragment.WhosPatientFragment;
import appointment.profile.ViewUpdateSelfProfileActivity;
import appointment.utils.CustomTypefaceSpan;
import careplan.fragments.CarePlanFragment;
import careplan.fragments.CareTaskFragment;


public class BaseActivity extends NavigationDrawerActivity implements ConnectivityChanged, AdapterView.OnItemClickListener {

    private static final long EXITTIME = 2000;
    private FloatingActionMenu fabMenu;
    protected UserMO userMO;
    private static final int LABELS_POSITION_LEFT = 0;
    private static final int LABELS_POSITION_RIGHT = 1;
    String toolBarTitle;
    private Bundle vaccinationProfileBundle;
    private ConfigMO configMO;
    private Dialog mProgressDialog;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView leftDrawerList;
    private NavigationDrawerAdapter navigationDrawerAdapter;
    private int currentFragment;
    private boolean isBlockUI;
    private FragmentManager supportFragmentManager;
    protected Fragment fragment;
    private static Toolbar mToolbar;
    public static ImageButton btnBack;
    private static CustomFontTextView toolbar_title;
    public static IconTextView bt_toolbar_right;
    public static IconTextView bt_toolbar_icon;
    private RelativeLayout rlUserDetails;
    private ImageView imgUserPic;
    private CustomFontTextView txtUserName;
    private CustomFontTextView txtPhoneNo;
    private boolean doubleBackToExitPressedOnce;
    private static Toast toast;
    private boolean isToLaunchUploadScreen;
    public final static int RECORDS_ENTER_DETAILS = 555;
    public final static int UPLOAD_CAMERA_FILE = 556;
    public final static int UPLOAD_GALLERY_FILE = 557;
    public final static int RECORDS_CAMERA_FILE = 558;
    public final static int RECORDS_MANUAL = 559;
    public final static int RECORDS_REPORT = 560;
    FrameLayout frameLayout;
    private boolean hasHome;
    private boolean uploadFragmentRefresh;
    private Intent intent;
    private LinearLayout lldrawerLayout;
    private CustomFontButton mBtn_helpline;
    private Typeface font_bold, font_semibold;
    public static IconTextView ivToolbarCall;
    private static CustomFontTextView tvHelpline;


    public void setUploadFragmentRefresh(boolean uploadFragmentRefresh) {
        this.uploadFragmentRefresh = uploadFragmentRefresh;
    }

    public boolean isUploadFragmentRefresh() {
        return uploadFragmentRefresh;
    }

    //removed code from here as it is moved to header Activity
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    public void setUserMO() {
        this.userMO = SettingManager.getInstance().getUserMO();
    }

    @Override
    protected void onStart() {
        super.onStart();

        setUserPic(imgUserPic);
        userMO = SettingManager.getInstance().getUserMO();
        if (userMO != null) {
            String userRealName = userMO.getFirstName() + " " + (userMO.getLastName() != null ? userMO.getLastName() : "");
            txtUserName.setText(userRealName);
            txtPhoneNo.setText(userMO.getPhoneNumber());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        setUserMO();
        setNavDrawerItems();
        Utils.setCurrentActivityContext(this);
        configMO = ConfigManager.getInstance().getConfig();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        font_bold = Typeface.createFromAsset(this.getAssets(), this.getResources().getString(R.string.opensans_ttf_bold));
        font_semibold = Typeface.createFromAsset(this.getAssets(), this.getResources().getString(R.string.opensans_ttf_regular));
        boolean isRegisterationStatusSent = preferences.getBoolean(VariableConstants.SENT_TOKEN_TO_SERVER, false);
        if (!isRegisterationStatusSent && userMO != null && userMO.getSalutation().equalsIgnoreCase(TextConstants.YES)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
        setContentView(R.layout.activity_base);
        toast = Toast.makeText(this, getResources().getString(R.string.exit_confirmation_message), Toast.LENGTH_SHORT);
        userMO = SettingManager.getInstance().getUserMO();
        intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(VariableConstants.LAUNCH_UPLOAD_SCREEN)) {
            isToLaunchUploadScreen = intent.getBooleanExtra(VariableConstants.LAUNCH_UPLOAD_SCREEN, false);
        }
        // Recycle the typed array
        initView();

        supportFragmentManager = getSupportFragmentManager();
        SharedPreferences sharedPreferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);

        /*Launcher change*/
        if (mToolbar != null) {
            setToolbarTitle(getResources().getString(R.string.title_my_health));
        }
        initDrawer();
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
                //add check for launcher fragment (Similar to above
                if (fragment != null && fragment.getClass().getSimpleName().equals(homeFragmentClassName)) {
                    hasHome = true;
                    if (fragment.getClass().getName().equals("com.mphrx.fisike.fragments.MyHealth")) {
                        uploadFragmentRefresh = true;
                    }

                }
                //for fragments opened from myhealth and other than myhealth-aastha
                /*Launcher change (last fragment is fragment other than home fragment and moving to home fragment)*/
                if (currentFragment == HOMEINDEX && fragment != null && !(fragment.getClass().getSimpleName().equals(homeFragmentClassName))) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                    getSupportFragmentManager().popBackStack();
                }
            }
        });

        createBlurBitmap();
        onNewIntent(intent);
        if (mService != null && ConfigManager.getInstance().getConfig().getAutoLockMinute() == 0)
            mService.stopLogoutTimer();

    }

    public void showHideToolbar(boolean value) {
        if (value)
            mToolbar.setVisibility(View.VISIBLE);
        else
            mToolbar.setVisibility(View.GONE);

    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        ivToolbarCall = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_call);

        tvHelpline = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_sub_helpline);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));


        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_viewall_orders));
        bt_toolbar_right.setTextSize(44);
        bt_toolbar_right.setVisibility(View.GONE);
        bt_toolbar_icon = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_icon);
        bt_toolbar_icon.setVisibility(View.GONE);

        View toolbarBack = null;
        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof ImageButton) {
                toolbarBack = mToolbar.getChildAt(i);
                if (toolbarBack != null) {
                    // change arrow direction
                    if (Utils.isRTL(this)) {
                        toolbarBack.setRotationY(180);
                    } else {
                        toolbarBack.setRotationY(0);
                    }
                }
                break;
            }
        }

        setSupportActionBar(mToolbar);
    }

    private void initView() {
        try {
            setNavigationDrawerAdapter();
            mBtn_helpline = (CustomFontButton) findViewById(R.id.btn_helpline);
            mBtn_helpline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!checkPermission(Manifest.permission.CALL_PHONE, ""))
                        return;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + TextConstants.PHLOOBER_HELP_LINE_NO));
                    startActivity(intent);
                }
            });


            frameLayout = (FrameLayout) findViewById(R.id.frame_container);
            leftDrawerList = (ListView) findViewById(R.id.left_drawer);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
            leftDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (isBlockUI) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.loading_user_information), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (BuildConfig.isPatientApp)
                        onItemClickPatient(parent, view, position, id, drawerLayout, fragment, currentFragment, BaseActivity.this);
                    else
                        onItemClickPhysician(parent, view, position, id, drawerLayout);

                }
            });

            leftDrawerList.setAdapter(navigationDrawerAdapter);
            rlUserDetails = (RelativeLayout) findViewById(R.id.rl_userDetails);
            imgUserPic = ((ImageView) findViewById(R.id.imgUserPic));
            txtUserName = ((CustomFontTextView) findViewById(R.id.txtUserName));
            Bitmap ProfilePicIcon = BitmapFactory.decodeResource(getResources(), R.drawable.default_profile_pic);
            imgUserPic.setImageBitmap(ProfilePicIcon);
            imgUserPic.setVisibility(View.VISIBLE);
            txtPhoneNo = (CustomFontTextView) findViewById(R.id.tv_email_id);
            rlUserDetails.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (fragment != null && fragment instanceof MyHealth) {
                        ((MyHealth) fragment).hideAdvanceSearchFormIfOpen();
                    }

                    Intent settingsActivity;
                    if (BuildConfig.isPhysicianApp)
                        settingsActivity = new Intent(BaseActivity.this, UserProfileActivity.class);
                    else
                        settingsActivity = new Intent(BaseActivity.this, ViewUpdateSelfProfileActivity.class);

                    settingsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    drawerLayout.closeDrawers();
                    startActivity(settingsActivity);

                }
            });

        } catch (Exception e) {
            System.out.println("Exception e :::::" + e.toString());
        }
        fabMenu = (com.github.clans.fab.FloatingActionMenu) findViewById(R.id.menu1);
        fabMenu.setClosedOnTouchOutside(true);



        /*[******   Change FabMenu Left or Right Based on ltr or rtl   ********]*/
        if (Utils.isRTL(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fabMenu.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            fabMenu.setLayoutParams(params);
            fabMenu.setLabelsAlignment(LABELS_POSITION_RIGHT);
        } else {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fabMenu.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            fabMenu.setLayoutParams(params);
            fabMenu.setLabelsAlignment(LABELS_POSITION_LEFT);
        }

        initToolbar();
    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_opened, R.string.drawer_closed) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideKeyboard();
                navigationDrawerAdapter.notifyDataSetChanged();
                super.onDrawerOpened(drawerView);

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        this.intent = intent;

        if (intent != null && intent.getData() != null && intent.getData().getPath().equals(TextConstants.DEEP_LINK_PATH)) {
            if (SettingManager.getInstance().getUserMO() == null) {
                startActivity(new Intent(this, OtpActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                this.finish();
                return;
            }
            Fragment fragment = new MyHealth();
            Bundle bundle = new Bundle();
            bundle.putBoolean(VariableConstants.DEEP_LINK_FOR_RECORDS_CLICKED, true);
            fragment.setArguments(intent.getExtras());
            openFragment(fragment);
            currentFragment = MYHEALTH;
        } else if (intent != null && intent.getExtras() != null && intent.hasExtra(VariableConstants.VACCINATION_MODEL)) {
            openVaccinationFragment();
        } else if (intent != null && intent.getExtras() != null && intent.hasExtra(VariableConstants.NOTIFICATION_REMINDER)) {
            Fragment fragment = new MyHealth();
            fragment.setArguments(intent.getExtras());
            openFragment(fragment);
            currentFragment = MYHEALTH;
        } else if (intent != null && intent.getExtras() != null && intent.hasExtra("careTaskActiviti")) {
            selectItem(WHATSNEXT, null);
        } else if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(VariableConstants.DIGITIZATION_REMINDER)) {
            Fragment fragment = new MyHealth();
            fragment.setArguments(intent.getExtras());
            openFragment(fragment);
            currentFragment = MYHEALTH;
            setNotificaitonCount();
        } else if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE)) {
            boolean isLocalNotification = intent.getExtras().getBoolean(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE);
            if (isLocalNotification) {
                currentFragment = CHAT;
                selectItem(currentFragment, null);
            }
        } else if (fragment != null && fragment instanceof MyHealth) {
            try {
                fragment.onActivityResult(GenralFragment.RESULT_SEARCH_QUERY, RESULT_OK, intent);
                showHideToolbar(true);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (fragment == null && (intent.getComponent().getClassName().equals("com.mphrx.fisike.HomeActivity"))) {
            currentFragment = HOMEINDEX;
            selectItem(currentFragment, null);
            Utils.UpdateSelectItemFromNavigationDrawer(false, -1);

        } else if (fragment == null && (!intent.getComponent().getClassName().equals("com.mphrx.fisike.HomeActivity"))) {
            fabMenu.setVisibility(View.GONE);
            showHideToolbar(false);
            currentFragment = -1;
            return;
        }
        super.onNewIntent(intent);
    }

    private void setNotificaitonCount() {
        SharedPreferences preferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isRead = preferences.getBoolean(VariableConstants.ALARM_READ, true);
        int unReadCount = preferences.getInt(VariableConstants.ALARM_COUNT_UNREAD, 0);
        if (!isRead && unReadCount > 0) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(VariableConstants.ALARM_COUNT_UNREAD, --unReadCount);
            if (unReadCount == 0) {
                editor.putBoolean(VariableConstants.ALARM_READ, true);
            }
            editor.commit();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUserMO();
        if (userMO != null && TextConstants.NO.equals(userMO.getSalutation())) {
            Utils.launchLoginScreen(this);
        }

        showDrawer(drawerLayout);


        if (MyApplication.getInstance().getMessengerSeviceInstance() == null)
            MyApplication.getInstance().setMessengerSeviceInstance(mService);

        //to display user profile pic and name after updating info
        setUserPic(imgUserPic);
        userMO = SettingManager.getInstance().getUserMO();
        if (userMO != null) {
            String userRealName = userMO.getFirstName() + " " + (userMO.getLastName() != null ? userMO.getLastName() : "");
            txtUserName.setText(userRealName);
            txtPhoneNo.setText(Utils.getUserName());
        }

        if (SharedPref.getIsToSelectItem()) {
            selectItem(SharedPref.getPositionToSelect(), null);
        }

        if (mService != null && Utils.checkIsToLogoutUser())
            mService.logoutUser(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRetry:
                break;

            default:
                if (fragment instanceof VaccinationProfileFragment) {
                    ((VaccinationProfileFragment) fragment).onClick(view);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        /*Launcher change*/
        if (currentFragment == HOMEINDEX) {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawers();
                return;
            }
            if (doubleBackToExitPressedOnce) {
                if (toast != null) {
                    toast.cancel();
                }
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                return;
            }

            this.doubleBackToExitPressedOnce = true;
            toast.show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, EXITTIME);
        } else {
            //onClickAppIcon();
            handleBackfromFragments();

        }

        if (fragment instanceof VaccinationProfileFragment) {
            MyApplication.getInstance().trackEvent(getString(R.string.vaccination_profile), "Back", null, null, "Vaccination Profile back hit");
        }

        return;
    }


    public void openDrawer() {
        hideKeyboard();
        int drawerLockMode = drawerLayout.getDrawerLockMode(GravityCompat.START);
        if (drawerLayout.isDrawerVisible(GravityCompat.START)
                && (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_OPEN)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
            drawerLayout.openDrawer(GravityCompat.START);
        }

    }

    private void handleBackfromFragments() {
        if (fragment == null) {
            finish();
            return;
        }
        if (currentFragment == HOMEINDEX) {
            int drawerLockMode = drawerLayout.getDrawerLockMode(GravityCompat.START);
            if (drawerLayout.isDrawerVisible(GravityCompat.START)
                    && (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_OPEN)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else if (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        } else {
            if (fragment instanceof VaccinationProfileFragment && ((VaccinationProfileFragment) fragment).isUpdateFlow()) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                VaccinationListFragment vaccinationListFragment = new VaccinationListFragment();
                //  vaccinationListFragment.setArguments();
                fragmentTransaction.add(R.id.frame_container, vaccinationListFragment);
                ((VaccinationProfileFragment) fragment).setUpdateFlow(false);
                fragmentTransaction.commit();
                currentFragment = VACCINATION;
            } else {
                /*Launcher change*/
                currentFragment = HOMEINDEX;
                selectItem(currentFragment, null);
            }
        }
    }

    private void onClickAppIcon() {
        if (fragment instanceof VaccinationProfileFragment && ((VaccinationProfileFragment) fragment).isUpdateFlow()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_container, new VaccinationListFragment());
            ((VaccinationProfileFragment) fragment).setUpdateFlow(false);
            fragmentTransaction.commit();
            currentFragment = VACCINATION;
            setNavigationBackIcon();
        } else {
            hideKeyboard();
            int drawerLockMode = drawerLayout.getDrawerLockMode(GravityCompat.START);
            if (drawerLayout.isDrawerVisible(GravityCompat.START)
                    && (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_OPEN)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else if (drawerLockMode != DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
                drawerLayout.openDrawer(GravityCompat.START);
            }


        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
    }

    public void setHasMyHealth(boolean value) {
        hasHome = value;
    }

    public void selectItem(final int position, Bundle fragment_bundle) {
        if (position == SIGNOUT) {
            DialogUtils.showAlertDialogCommon(this, this.getResources().getString(R.string.confirm_signout), getLogoutMessageConfigBased(), getString(R.string.proceed), getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        mService.logoutUser(BaseActivity.this);
                    } else {
                        dialog.dismiss();
                    }
                }
            });
            drawerLayout.closeDrawers();

        } else {
            showHideToolbar(true);
            tvHelpline.setVisibility(View.GONE);
            ivToolbarCall.setVisibility(View.GONE);
            //selecting item from 2nd layer activities
            if (!(this instanceof HomeActivity)) {
                Utils.UpdateSelectItemFromNavigationDrawer(true, position);
                this.finish();
                return;
            }
            if (SharedPref.getIsToSelectItem())
                hasHome = false;
            switch (position) {
                case MYHEALTH:
                    final Bundle bundle;
                    if (fragment_bundle == null) {
                        bundle = new Bundle();
                        bundle.putBoolean(VariableConstants.LAUNCH_UPLOAD_SCREEN, isToLaunchUploadScreen);
                    } else {
                        bundle = fragment_bundle;
                    }
                    if (fabMenu.getVisibility() != View.VISIBLE) {
                        fabMenu.setVisibility(View.VISIBLE);
                    }
                    checkIfHomePresent(position, new MyHealth(), bundle);
                    currentFragment = MYHEALTH;
                    //  toolBarTitle = "My Health";
                    break;
                case CHAT:
                    checkIfHomePresent(position, new ChatFragment(), null);
                    currentFragment = CHAT;
                    toolBarTitle = "CHAT";
                    break;
                case NOTIFICATION:
                    fabMenu.setVisibility(View.GONE);

                    checkIfHomePresent(position, new NotificationCenterFragment(), null);
                    currentFragment = NOTIFICATION;
                    toolBarTitle = "Notifications";
                    break;
                case SETTING:
//                fragment = BuildConfig.isFisike ? new Settings() : new MinervaSettings();
                    currentFragment = SETTING;
                    fabMenu.setVisibility(View.GONE);
                    checkIfHomePresent(position, new Settings(), null);
                    toolBarTitle = "Settings";
                    break;
                case HOMEDRAW:
                    currentFragment = HOMEDRAW;
                    fabMenu.setVisibility(View.GONE);
                    checkIfHomePresent(position, new PhloobaHomeFragment(), null);
                    toolBarTitle = getResources().getString(R.string.northwell_title_home);
                    break;
                case HOMEDRAWREQUESTS:
                    fragment = new HomeDrawListingFragment();
                    currentFragment = HOMEDRAWREQUESTS;
                    fabMenu.setVisibility(View.GONE);
                    checkIfHomePresent(position, new HomeDrawListingFragment(), null);
                    toolBarTitle = getResources().getString(R.string.title_homedrawrequest);
                    break;
                case APPOINTMENT:
                    currentFragment = APPOINTMENT;
                    fabMenu.setVisibility(View.GONE);
                    checkIfHomePresent(position, new MyAppointmentFragment(), null);
                    toolBarTitle = getResources().getString(R.string.appointments);
                    break;
                case PROFILES:
                    final Bundle bundless = new Bundle();
                    bundless.putBoolean(VariableConstants.FROM_PATIENT_PROFILE_VIEW, true);
                    fragment = new WhosPatientFragment();
                    currentFragment = PROFILES;
                    fabMenu.setVisibility(View.GONE);
                    checkIfHomePresent(position, new WhosPatientFragment(), bundless);
                    toolBarTitle = getResources().getString(R.string.title_profiles);
                    break;
                case VACCINATION:
                    openVaccinationFragment();
//                showHideToolbar(true);
                    return;
                case WHATSNEXT:
                    fabMenu.setVisibility(View.INVISIBLE);
                    Bundle bundle1 = new Bundle();
                    if (intent != null && intent.getExtras() != null) {
                        bundle1 = intent.getExtras();
                    }
                    checkIfHomePresent(position, new CareTaskFragment(), bundle1);
                    toolBarTitle = getResources().getString(R.string.what_next);
                    currentFragment = WHATSNEXT;
                    break;
                case CAREPLAN:
                    currentFragment = CAREPLAN;
                    fabMenu.setVisibility(View.INVISIBLE);
                    checkIfHomePresent(position, new CarePlanFragment(), null);
                    toolBarTitle = getResources().getString(R.string.care_plan);
                    break;

                case HOME:
                    fabMenu.setVisibility(View.INVISIBLE);
                    checkIfHomePresent(position, new Home(), null);
                    currentFragment = HOME;
                    toolBarTitle = getResources().getString(R.string.tiles);
                    tvHelpline.setVisibility(View.VISIBLE);
                    ivToolbarCall.setVisibility(View.VISIBLE);
                    break;


            }
            navigationDrawerAdapter.setSelectedPosition(position);
            navigationDrawerAdapter.notifyDataSetChanged();
            openFragment(fragment);
        }
    }

    private void checkIfHomePresent(int position, Fragment newFragment, Bundle bundle) {
        if (!hasHome || position != HOMEINDEX) {
            fragment = newFragment;
            if (bundle != null)
                fragment.setArguments(bundle);
            if (position == HOMEINDEX)
                setHamburgerIcon();
            else
                setNavigationBackIcon();
        } else if (position == HOMEINDEX) {
            for (Fragment f : getSupportFragmentManager().getFragments()) {
                if (f.getClass().getSimpleName().equals(homeFragmentClassName)) {
                    fragment = f;
                    break;
                }
            }
        }
    }

    /*opens from deep linking entry points ,navigation drawer selections,*/

    private void openFragment(Fragment fragment) {
        Utils.UpdateSelectItemFromNavigationDrawer(false, -1);
        initToolbar();
        setToolbarTitle(toolBarTitle);
        if (fragment != null) {
            getSupportFragmentManager().executePendingTransactions();
            if (fragment.isAdded() && homeFragmentClassName.equals(fragment.getClass().getSimpleName())) {
                getSupportFragmentManager().popBackStack();
                drawerLayout.closeDrawers();
                if (fragment instanceof MyHealth) {
                    setToolbarTitle(getResources().getString(R.string.title_my_health));
                    MyHealth myHealth = (MyHealth) fragment;
                    if (myHealth.getMyHelthAdapter() != null && myHealth.mViewPager != null && (myHealth.getMyHelthAdapter().getFragment(
                            Utils.getMyHealthTabNameBasedOnPosition(myHealth.mViewPager.getCurrentItem())) instanceof VitalsFragment || myHealth.getMyHelthAdapter().getFragment(
                            Utils.getMyHealthTabNameBasedOnPosition(myHealth.mViewPager.getCurrentItem())) instanceof GenralFragment)) {
                        fabMenu.setVisibility(View.GONE);
                    } else if (myHealth.getMyHelthAdapter() != null && myHealth.mViewPager != null &&
                            myHealth.getMyHelthAdapter().getFragment(Utils.getMyHealthTabNameBasedOnPosition
                                    (myHealth.mViewPager.getCurrentItem())) == null && myHealth.mViewPager.getCurrentItem() == 0) {
                        myHealth.setPage(myHealth.mViewPager.getCurrentItem());
                        fabMenu.setVisibility(View.VISIBLE);
                    }
                }
                return;
            }
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_container, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.commitAllowingStateLoss();
        }
        drawerLayout.closeDrawers();
    }

    private void openVaccinationFragment() {
//            setHasMyHealth(false);
        new OpenVaccinationFragmenet(new ApiResponseCallback<VaccinationProfile>() {
            @Override
            public void onSuccessRespone(VaccinationProfile vaccinationProfile) {
                if (vaccinationProfile != null) {
                    fragment = new VaccinationListFragment();
                    Bundle bundle;
                    if (getIntent() != null && getIntent().hasExtra(VariableConstants.VACCINATION_MODEL)) {
                        bundle = getIntent().getExtras();
                    } else {
                        bundle = new Bundle();
                    }
                    bundle.putParcelable(VariableConstants.VACCINATION_PROFILE, vaccinationProfile);
                    fragment.setArguments(bundle);
                    setVaccinationProfileBundle(bundle);
                } else {
                    fragment = new VaccinationProfileFragment();
                }
                currentFragment = VACCINATION;
                fabMenu.setVisibility(View.INVISIBLE);
                setNavigationBackIcon();
                toolBarTitle = getResources().getString(R.string.title_vaccination);

                navigationDrawerAdapter.setSelectedPosition(HOMEINDEX);
                navigationDrawerAdapter.notifyDataSetChanged();
                openFragment(fragment);
            }

            @Override
            public void onFailedResponse(Exception exception) {
                fragment = new VaccinationProfileFragment();
                currentFragment = VACCINATION;
                fabMenu.setVisibility(View.INVISIBLE);
                setNavigationBackIcon();
                toolBarTitle = getResources().getString(R.string.title_vaccination);

                navigationDrawerAdapter.setSelectedPosition(HOMEINDEX);
                navigationDrawerAdapter.notifyDataSetChanged();
                openFragment(fragment);
            }
        }, BaseActivity.this, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void openFragmentRecord(String refrenceId, String documentType) {
        Bundle data = new Bundle();
        data.putString(VariableConstants.DIGITIZATION_MESSAGE_COUNT, "0");
        data.putBoolean(VariableConstants.DIGITIZATION_REMINDER, true);
        data.putString(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION, documentType);
        data.putString(VariableConstants.REFRENCE_ID, refrenceId);
        Fragment fragment = new MyHealth();
        fragment.setArguments(data);
        openFragment(fragment);
        currentFragment = MYHEALTH;
    }

    public void openFragmentRecord(Bundle bundle) {
        Fragment fragment = new MyHealth();
        fragment.setArguments(bundle);
        openFragment(fragment);
        /*launcher change - to check impact*/
        currentFragment = 0;
    }

    public void openFragmentVital(Bundle bundle) {
        Fragment fragment = new MyHealth();
        fragment.setArguments(bundle);
        openFragment(fragment);
         /*launcher change - to check impact*/
        currentFragment = 0;
    }


    public void openFragmentAppointment(Bundle bundle) {
     /*   Fragment fragment = new MyAppointmentFragment();
        fragment.setArguments(bundle);
        openFragment(fragment);
         *//*launcher change - to check impact*//*
        currentFragment = APPOINTMENT;*/
        selectItem(APPOINTMENT, null);
    }


    public void openMedicationFragment(PrescriptionModel prescriptionModel, MedicineDoseFrequencyModel doseFrequencyModel) {
        Bundle data = new Bundle();
        data.putBoolean(VariableConstants.NOTIFICATION_REMINDER, true);
        data.putString(VariableConstants.MEDICIAN_NAME, prescriptionModel.getMedicineName());
        data.putDouble(VariableConstants.QUANTITY, doseFrequencyModel.getDoseQuantity());
        data.putString(VariableConstants.UNIT, doseFrequencyModel.getDoseUnit());
        String ampm = Utils.isValueAvailable(doseFrequencyModel.getDoseTimeAMorPM()) ? " " + doseFrequencyModel.getDoseTimeAMorPM() : "";
        data.putString(VariableConstants.TIMMING, doseFrequencyModel.getDoseHours() + ":" + doseFrequencyModel.getDoseMinutes() + ampm);
        data.putInt(VariableConstants.MEDICATION_PRESCRIPTION_ID, prescriptionModel.getID());
        data.putInt(VariableConstants.DOSE_INSTRUCTION_CODE, doseFrequencyModel.getAlarmManagerUniqueKey());

        Fragment fragment = new MyHealth();
        fragment.setArguments(data);
        openFragment(fragment);
        currentFragment = MYHEALTH;
    }

    public void setHamburgerIcon() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hamburger);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void setNavigationBackIcon() {
        initToolbar();
        mToolbar.setNavigationIcon(R.drawable.hamburger);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void setToolbarBack() {
        initToolbar();
        mToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    public void changeOnConnectionAvalable() {

    }

    public void changeOnConnectionDisconnected() {

    }

    public void changeOnConnectionConnecting() {

    }

    @Override
    public void onNetConnectivityBroadcast(Intent intent) {
        NetworkInfo localNetworkInfo = (NetworkInfo) intent.getExtras().get("networkInfo");
        if (Utils.isNetworkAvailable(this)) {
            ConnectionStatus.isConnectionAvailable = true;
            changeOnConnectionAvalable();
        } else {
            ConnectionStatus.isConnectionAvailable = false;
            changeOnConnectionDisconnected();
        }
    }


    public void hideKeyboard() {
        Utils.hideKeyboard(BaseActivity.this);
    }


    public void progressStarted() {
    }

    public void removeProfilePic() {
    }

    public void errorInRemoving() {
    }

    public void removeProfilePic(String json) {
    }

    public void uploadProfilePic(String jsonString, byte[] bitmapdata) {

    }

    @Override
    protected void onDestroy() {
//        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @Override
    public void onMessengerServiceBind() {
        if (BaseActivity.this instanceof HomeActivity) {

            if (((HomeActivity) BaseActivity.this).getCurrentFragmentVisible() != null
                    && ((HomeActivity) BaseActivity.this).getCurrentFragmentVisible() instanceof ChatFragment) {
                ChatFragment chatFragment = (ChatFragment) ((HomeActivity) BaseActivity.this).getCurrentFragmentVisible();
                if (chatFragment instanceof ChatFragment) {
                    Intent intent = new Intent(VariableConstants.BROADCST_REFRESH_DATA);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            }
        } else if (BuildConfig.isPhysicianApp && BaseActivity.this instanceof MessageListActivity) {
            Intent intent = new Intent(VariableConstants.BROADCST_REFRESH_DATA);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    @Override
    public void onMessengerServiceUnbind() {

    }


    public boolean checkPermission(final String permissionType, String message) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(BaseActivity.this, new String[]{permissionType}, TextConstants.PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, TextConstants.PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TextConstants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionNotGranted(permissions[0]);
            }
        }
    }

    public void onPermissionGranted(String permission) {

    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(this, getResources().getString(R.string.granted_access), Toast.LENGTH_SHORT).show();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null)
            mProgressDialog = DialogUtils.showProgressDialog(this);
        else
            mProgressDialog.show();
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog == null)
            mProgressDialog = DialogUtils.showProgressDialog(this, getString(R.string.Processing_loading));
        else if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public void setUserPic(ImageView userProfilePic) {
        Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());
        if (bitmap != null) {
            userProfilePic.setImageBitmap(bitmap);
        } else {
            Bitmap ProfilePicIcon = BitmapFactory.decodeResource(getResources(), R.drawable.default_profile_pic);
            userProfilePic.setImageBitmap(ProfilePicIcon);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    public void setToolbarTitle(String title) {
        initToolbar();

        AppLog.d("Base Activoty setToolbar title ", this.toString());

        SpannableString text;
        if (title != null && title.contains("Northwell")) {
            text = new SpannableString(title);
            text.setSpan(new CustomTypefaceSpan("", font_bold), 0, 9, 0);
            text.setSpan(new CustomTypefaceSpan("", font_semibold), 10, 16, 0);
            toolbar_title.setText(text);
        } else {
            toolbar_title.setText(title);
        }
        toolbar_title.setVisibility(View.VISIBLE);
        bt_toolbar_right.setOnClickListener(null);
        bt_toolbar_right.setVisibility(View.GONE);
        bt_toolbar_icon.setOnClickListener(null);
        bt_toolbar_icon.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.d("mvv", "home selected");
                onClickAppIcon();

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (fragment instanceof VaccinationListFragment) {
            return fragment.onOptionsItemSelected(item);
        } else if (fragment instanceof CareTaskFragment) {
            return fragment.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public Fragment getCurrentFragmentVisible() {
        return fragment;
    }

    public void setList(String dob, final VaccinationListAdapter adapter, boolean isViewByDate) {
        SharedPreferences sharedPreferences = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        final boolean isSetAlarm = !sharedPreferences.getBoolean(VariableConstants.IS_VACCINATION_PARSED, false);
        new VaccinationJosnParsing(new ApiResponseCallback<ArrayList<VaccinationModel>>() {
            @Override
            public void onSuccessRespone(final ArrayList<VaccinationModel> objects) {
                adapter.setVaccinationModelList(objects);
                adapter.notifyDataSetChanged();

                if (isSetAlarm) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; objects != null && i < objects.size(); i++) {
                                if (objects.get(i).getDoseTimeStamp() != 0) {
                                    Utils.cancelVaccinationAlarm(BaseActivity.this, objects.get(i).getReminderUniqueKey());
                                    VaccinationAlarm.setAlarm(objects.get(i), BaseActivity.this, false);
                                }
                            }
                        }
                    }).start();
                }
            }

            @Override
            public void onFailedResponse(Exception exception) {

            }
        }, BaseActivity.this, true, isViewByDate, dob).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setPage() {

        View childAt = ((FrameLayout) findViewById(R.id.frame_container)).getChildAt(0);

        if (childAt == null) {
            selectItem(0, null);
        }
    }


    @Override
    public void setNavigationDrawerAdapter() {
        navigationDrawerAdapter = new NavigationDrawerAdapter(getApplicationContext(), navDrawerItems);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && data.hasExtra(VariableConstants.RELOAD_SETTINGS)) {
            selectItem(SETTING, null);
            return;
        }
        if (fragment != null && fragment instanceof MyHealth && data != null && data.hasExtra(VariableConstants.REFRESH_RECORDS)) {
            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        } else if (fragment != null && fragment instanceof HomeDrawListingFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        } else if (fragment != null && fragment instanceof MyAppointmentFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        } else if (fragment != null && fragment instanceof CareTaskFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        }


    }

    public Bundle getVaccinationProfileBundle() {
        return vaccinationProfileBundle;
    }

    public void setVaccinationProfileBundle(Bundle vaccinationProfileBundle) {
        this.vaccinationProfileBundle = vaccinationProfileBundle;
    }

    private void createBlurBitmap() {
       /* try {
            lldrawerLayout = (LinearLayout) findViewById(R.id.only_drawer_layout);
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            Bitmap image = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888);
            image.eraseColor(getResources().getColor(R.color.drawer_bg));
            Drawable d = new BitmapDrawable(getResources(), Utils.blur(this, image, 10f));
            lldrawerLayout.setBackgroundDrawable(d);
        }
        catch (Exception e){}*/
    }

}
