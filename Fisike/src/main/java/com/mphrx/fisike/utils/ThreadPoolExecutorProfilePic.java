package com.mphrx.fisike.utils;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorProfilePic extends ThreadPoolExecutor {
    private ArrayList<Runnable> activeThread;

    public ThreadPoolExecutorProfilePic(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    public void execute(Runnable command) {
        if (command == null) {
            return;
        }
        if (null == activeThread) {
            activeThread = new ArrayList<Runnable>();
        }
        activeThread.add(command);
        super.execute(command);
    }

    @Override
    protected void afterExecute(Runnable command, Throwable t) {
        if (command == null) {
            return;
        }
        if (activeThread!=null&&activeThread.contains(command)) {
            activeThread.remove(command);
        }
        super.afterExecute(command, t);
    }

    public ArrayList<Runnable> getActiveThread() {
        return activeThread;
    }

    public void removeActiveThreadArray(){
        activeThread = null;
    }
}
