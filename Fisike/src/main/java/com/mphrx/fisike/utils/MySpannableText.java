package com.mphrx.fisike.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.mphrx.fisike.R;

/**
 * Created by laxmansingh on 12/1/2016.
 */

public class MySpannableText extends ClickableSpan {

    private boolean isUnderline = true;
    private Context mContext;

    public MySpannableText(boolean isUnderline, Context mContext) {

        this.isUnderline = isUnderline;
        this.mContext = mContext;

    }

    @Override

    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(isUnderline);
        ds.setColor(mContext.getResources().getColor(R.color.link_blue_color));
        ds.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
    }

    @Override

    public void onClick(View widget) {
    }

}
