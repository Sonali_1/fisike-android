package com.mphrx.fisike.utils;

/**
 * Created by Aastha on 12/02/2016.
 */
public class Days {

    private String dayOfWeek;
    private boolean isSelected;

    public Days(String day, boolean b) {
        dayOfWeek=day;
        isSelected=b;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


}
