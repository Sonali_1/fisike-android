package com.mphrx.fisike.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.beans.OTPBean;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OTPUtils {

    /**
     * Generate Random number to send in Message
     */
    public static String generateOTP() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int m = cal.get(Calendar.MINUTE);
        int s = cal.get(Calendar.SECOND);
        int ms = cal.get(Calendar.MILLISECOND);
        int v = (m % 10) * 1235 + s * 79 + ms * 982 + 1;
        String format = v + "";
        int otplength = VariableConstants.OTP_LENGTH;
        if (format.length() < otplength) {
            return generateOTP();
        }
        String substring = format.substring(format.length() - otplength, format.length());
        return String.format(Locale.getDefault(), "%0" + otplength + "d", Integer.parseInt(substring));
    }

    /***
     * save OTP in shared pref
     * ***/

    public static void saveOTP(OTPBean otpToSave, Context context) {
        SharedPreferences sharedpreference = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        Editor edit = sharedpreference.edit();
        edit.putString(VariableConstants.PHONE_NUMBER, otpToSave.getMobile_number());
        edit.putString(VariableConstants.COUNTRY_CODE, otpToSave.getCountryCode());
        edit.putBoolean(VariableConstants.HAS_VERIFIED_OTP, false);
        edit.putString(VariableConstants.OTP_CODE, otpToSave.getOtp_Code());
        edit.commit();

    }

    public static int verifyOTP(String otpCode, Context context) {
        SharedPreferences sharedpreference = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
//		otpCode = "1234";
        String otpSavedCode = sharedpreference.getString(VariableConstants.OTP_CODE, "");
//		String otpSavedCode = "1234";
        long otp_send_time = sharedpreference.getLong(VariableConstants.OTP_SEND_TIME, 0);
        long currentTimeStamp = Calendar.getInstance().getTime().getTime();
        boolean isOtpVerified = sharedpreference.getBoolean(VariableConstants.HAS_VERIFIED_OTP, false);
        if (!isOtpVerified) {
            if (otpSavedCode != "" && otp_send_time != 0) {
                if (otpSavedCode.equals(otpCode)) {
                    if ((currentTimeStamp - otp_send_time) / 1000 < (60 * TextConstants.OTP_EXPIRE_TIME))
                        return TextConstants.OTP_MATCHED;
                    else
                        return TextConstants.OTP_EXPIRED;
                } else
                    return TextConstants.OTP_NOT_MATCHED;
            } else {
                return -1;
            }

        } else {
            Toast.makeText(context, context.getResources().getString(R.string.OTP_already_Verified), Toast.LENGTH_SHORT).show();
            return -1;
        }

    }

    /**
     *
     * */
    public static boolean manualVerifyOTP(OTPBean OTPtoMatch, String otp) {
        return false;
    }

}
