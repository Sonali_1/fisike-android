package com.mphrx.fisike.utils;

import android.content.Context;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.beans.DoseBean;
import com.mphrx.fisike.constant.MedicationConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.utils.AppLog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Aastha on 22/01/2016.
 */
public class MapHolder {
    private static MapHolder soleInstance;

    public static LinkedHashMap<String, List<DoseBean>> dosagelinkedHashMap =
            new LinkedHashMap<String, List<DoseBean>>();

    public static MapHolder getInstance() {
        if (soleInstance == null) {
            soleInstance = new MapHolder();
        }
        return soleInstance;
    }


    public static LinkedHashMap<String, List<DoseBean>> getdoseMap(int isAddition, Context mContext) {
        if (dosagelinkedHashMap != null) {
            dosagelinkedHashMap.clear();
        }

        insertDosageDefaultData(mContext);
        return dosagelinkedHashMap;

    }


    private static void insertDosageDefaultData(Context mContext) {
        AppLog.d("Fisike", "inside (insertDosageDefaultData)");
        MapHolder mapHolder = MapHolder.getInstance();
        AppLog.d("Fisike", mapHolder.toString());
        DoseBean eightAMAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.EIGHT_AM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean eightPMAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.EIGHT_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean tenAMAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.TEN_AM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean elevenAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.ELEVEN_AM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean twelveAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.TWELVE_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean oneAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.ONE_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean twoAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.TWO_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean threeAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.THREE_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean fourAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.FOUR_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean fiveAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.FIVE_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean sixAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.SIX_PM, "00", MedicationConstants.AFTER_FOOD, false);
        DoseBean sosAfter = new DoseBean(TextConstants.ONE, MyApplication.getAppContext().getString(R.string.tablet), TextConstants.SOS, "00", MedicationConstants.AFTER_FOOD, true);
        ArrayList<DoseBean> oneTime = new ArrayList<DoseBean>();
        oneTime.add(eightAMAfter);
        ArrayList<DoseBean> sos = new ArrayList<DoseBean>();
        sos.add(sosAfter);
        ArrayList<DoseBean> twoTime = new ArrayList<DoseBean>();
        twoTime.add(eightAMAfter);
        twoTime.add(eightPMAfter);
        ArrayList<DoseBean> threeTime = new ArrayList<DoseBean>();
        threeTime.add(eightAMAfter);
        threeTime.add(eightPMAfter);
        //threeTime.add(twoBefore);
        threeTime.add(twoAfter);
        ArrayList<DoseBean> fourTime = new ArrayList<DoseBean>();
        //fourTime.add(eightAMBefore);
        fourTime.add(eightAMAfter);
        //fourTime.add(twelveBefore);
        fourTime.add(twelveAfter);
        //fourTime.add(fourbefore);
        fourTime.add(fourAfter);
        //fourTime.add(eightPMBefore);
        fourTime.add(eightPMAfter);
        ArrayList<DoseBean> fiveTime = new ArrayList<DoseBean>();
        //fiveTime.add(eightAMBefore);
        fiveTime.add(eightAMAfter);
        //fiveTime.add(elevenBefore);
        fiveTime.add(elevenAfter);
        //fiveTime.add(twoBefore);
        fiveTime.add(twoAfter);
        //fiveTime.add(fivebefore);
        fiveTime.add(fiveAfter);
        //fiveTime.add(eightPMBefore);
        fiveTime.add(eightPMAfter);
        ArrayList<DoseBean> sixTime = new ArrayList<DoseBean>();
        //sixTime.add(eightAMBefore);
        sixTime.add(eightAMAfter);
        //sixTime.add(tenAMBefore);
        sixTime.add(tenAMAfter);
        //sixTime.add(twelveBefore);
        sixTime.add(twelveAfter);
        //sixTime.add(fourbefore);
        sixTime.add(fourAfter);
        //sixTime.add(sixbefore);
        sixTime.add(sixAfter);
        //sixTime.add(eightPMBefore);
        sixTime.add(eightPMAfter);
        ArrayList<DoseBean> sevenTime = new ArrayList<>();
        //sevenTime.add(eightAMBefore);
        sevenTime.add(eightAMAfter);
        //sevenTime.add(tenAMBefore);
        sevenTime.add(tenAMAfter);
        //sevenTime.add(twelveBefore);
        sevenTime.add(twelveAfter);
        //sevenTime.add(twoBefore);
        sevenTime.add(twoAfter);
        //sevenTime.add(fourbefore);
        sevenTime.add(fourAfter);
        //sevenTime.add(sixbefore);
        sevenTime.add(sixAfter);
        //sevenTime.add(eightPMBefore);
        sevenTime.add(eightPMAfter);
        ArrayList<DoseBean> eightTime = new ArrayList<>();
        //eightTime.add(eightAMBefore);
        eightTime.add(eightAMAfter);
        //eightTime.add(tenAMBefore);
        eightTime.add(tenAMAfter);
        //eightTime.add(elevenBefore);
        eightTime.add(elevenAfter);
        //eightTime.add(oneBefore);
        eightTime.add(oneAfter);
        //eightTime.add(threeBefore);
        eightTime.add(threeAfter);
        //eightTime.add(fivebefore);
        eightTime.add(fiveAfter);
        //eightTime.add(sixbefore);
        eightTime.add(sixAfter);
        //eightTime.add(eightPMBefore);
        eightTime.add(eightPMAfter);

        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Once_a_day), oneTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Twice_a_day), twoTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Three_times_a_day), threeTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Four_times_a_day), fourTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Five_times_a_day), fiveTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Six_times_a_day), sixTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Seven_times_a_day), sevenTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.Eight_times_a_day), eightTime);
        dosagelinkedHashMap.put(mContext.getResources().getString(R.string.SOS), sos);

    }


}
