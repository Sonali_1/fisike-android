package com.mphrx.fisike.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.background.RemoveProfilePicture;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class IntentUtils {


    public static void onClickTakePhoto(Activity context, int cameraImageIntent, int galleryImageIntent, File cameraImageFile, byte[] profilePic,
                                        boolean isProfilePic, boolean isEditProfile) {
        Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*");

        List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();
        if (cameraImageIntent != 0) {

            PackageManager packageManager = context.getPackageManager();
            if (cameraImageIntent != 0) {
                List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);
                if (listCam.size() > 0) {
                    ResolveInfo res = listCam.get(0);
                    final Intent finalIntent = new Intent(camIntent);
                    finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    finalIntent.putExtra(VariableConstants.RESULT_CODE, cameraImageIntent);
                    finalIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraImageFile));
                    yourIntentsList.add(finalIntent);
                    info.add(res);
                }
            }
        }
        if (yourIntentsList.size() == 1) {
            int resultCode = yourIntentsList.get(0).getExtras().getInt(VariableConstants.RESULT_CODE);
            context.startActivityForResult(yourIntentsList.get(0), resultCode);
        } else {
            openDialog(yourIntentsList, info, context.getResources().getString(R.string.Select_Image), context, profilePic, isEditProfile);
        }

    }

    public static void onClickChooseImage(Activity context, int cameraImageIntent, int galleryImageIntent, File cameraImageFile, byte[] profilePic,
                                          boolean isProfilePic, boolean isEditProfile) {
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*");

        List<ResolveInfo> info = new ArrayList<>();
        List<Intent> yourIntentsList = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra(VariableConstants.RESULT_CODE, galleryImageIntent);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        if (yourIntentsList.size() == 1) {
            int resultCode = yourIntentsList.get(0).getExtras().getInt(VariableConstants.RESULT_CODE);
            context.startActivityForResult(yourIntentsList.get(0), resultCode);
        } else {
            openDialog(yourIntentsList, info, context.getResources().getString(R.string.Select_Image), context, profilePic, isEditProfile);
        }

    }

    /**
     * Open image File from camera or gallery
     *
     * @param context
     * @param cameraImageIntent  : intent camera return code
     * @param galleryImageIntent : intent gallery return code
     * @param cameraImageFile    : image file to be stored in
     * @param profilePic
     */
    public static void attachImageFile(Activity context, int cameraImageIntent, int galleryImageIntent, File cameraImageFile, byte[] profilePic,
                                       boolean isProfilePic, boolean isEditProfile) {
        Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*");

        // look for available intents
        List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();
        PackageManager packageManager = context.getPackageManager();
        if (cameraImageIntent != 0) {
            List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);
            if (listCam.size() > 0) {
                ResolveInfo res = listCam.get(0);
                final Intent finalIntent = new Intent(camIntent);
                finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                finalIntent.putExtra(VariableConstants.RESULT_CODE, cameraImageIntent);
                finalIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraImageFile));
                yourIntentsList.add(finalIntent);
                info.add(res);
            }
        }
        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra(VariableConstants.RESULT_CODE, galleryImageIntent);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        if (profilePic != null) {
            ResolveInfo res = listGall.get(0);
            Intent intent = new Intent();
            intent.putExtra(VariableConstants.REMOVE_PIC, true);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, "Remove pic"));
            yourIntentsList.add(intent);
            info.add(res);
        }

        openDialog(yourIntentsList, info, context.getResources().getString(R.string.Select_Image), context, profilePic, isEditProfile);
    }

    private static void openDialog(final List<Intent> yourIntentsList, List<ResolveInfo> info, String title, final Activity context,
                                   final byte[] profilePic, final boolean isEditProfile) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setAdapter(buildAdapter(context, info, profilePic), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = yourIntentsList.get(id);
                if (intent.getExtras().containsKey(TextConstants.REMOVE_PIC)) {
                    if (profilePic == null) {
                        dialog.dismiss();
                        return;
                    }
                    boolean removePic = intent.getExtras().getBoolean(TextConstants.REMOVE_PIC);
                    if (Utils.showDialogForNoNetwork(context, false)) {
                        if (removePic) {
                            new RemoveProfilePicture(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                    dialog.dismiss();
                    return;
                }
                int resultCode = intent.getExtras().getInt(VariableConstants.RESULT_CODE);
                context.startActivityForResult(intent, resultCode);
                dialog.dismiss();
            }
        });

        dialog.setNeutralButton(context.getResources().getString(R.string.cancel), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Build the list of items to show using the intent_listview_row layout.
     *
     * @param context
     * @param activitiesInfo
     * @param profilePic
     * @return
     */
    private static ArrayAdapter<ResolveInfo> buildAdapter(final Context context, final List<ResolveInfo> activitiesInfo, final byte[] profilePic) {
        return new ArrayAdapter<ResolveInfo>(context, R.layout.intent_listview_row, R.id.title, activitiesInfo) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (profilePic != null && position == activitiesInfo.size() - 1) {
                    ImageView image = (ImageView) view.findViewById(R.id.icon);
                    image.setImageResource(R.drawable.ic_pu_rem_pp_pic);
                    CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                    textview.setText(context.getResources().getString(R.string.Remove_profile_picture));
                    return view;
                }
                ResolveInfo res = activitiesInfo.get(position);
                ImageView image = (ImageView) view.findViewById(R.id.icon);
                image.setImageDrawable(res.loadIcon(context.getPackageManager()));
                CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                textview.setText(res.loadLabel(context.getPackageManager()).toString());
                return view;
            }
        };
    }

}
