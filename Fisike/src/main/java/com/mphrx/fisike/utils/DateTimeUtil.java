package com.mphrx.fisike.utils;

import android.content.Context;
import android.text.format.DateFormat;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike_physician.utils.AppLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by administrate on 1/4/2016.
 */
public class DateTimeUtil {
    public static String EEE_dd_MMM_yyyy = "EEE dd MMM yyyy";
    public static String MM_dd_yyyy = "MM_dd_yyyy";
    public static String yyyyMMdd_HHmmss = "yyyyMMdd_HHmmss";
    public static String hh_mm_aaa_comma_d_MMM_yyyy = "hh:mm aaa, d MMM yyyy";
    public static String HH_mm_comma_d_MMM_yyyy = "HH:mm, d MMM yyyy";
    public static String yyyy_MM_dd_T_HH_mm_ss_SSS = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static String yyyy_MM_dd_HH_mm_ssZ_Hiphen_Seperated = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static String yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated = "yyyy-MM-dd HH:mm:ss";
    public static String yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss";
    public static String yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_New = "yyyy-MM-dd hh:mm a";
    public static String yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm";
    public static String yyyy_MM_dd_hh_mm_aa_Hiphen_Seperated = "yyyy-MM-dd hh:mm a";
    public static String yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static String yyyy_MM_dd_T_HH_mm_ss_Hiphen_Seperated = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static String agreementformat = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static String yyyy_MM_dd_T_HH_mm_ss_z_appended = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static String MM_dd_yyyy_HH_mm_ss_Hiphen_Seperated = "MM-dd-yyyy HH:mm:ss";
    public static String destinationDateFormatWithoutTime = "dd MMM yyyy";//need to change when changin DateTime format
    public static String destinationDateFormatWithTime = "dd MMM yyyy, hh:mm:ss a";//need to change when changin DateTime format
    public static String DateFormatWithTime = "dd MMM yyyy hh:mm a";//need to change when changin DateTime format
    public static String DateFormatWithTime24hr = "dd MMM yyyy HH:mm";
    public static String DateFormatWithTimeInSeconds = "dd MMM yyy hh:mm:ss a";
    public static String destinationDateFormatWithTimeOld = "dd MMM yyyy, hh:mm a";//need to change when changin DateTime format
    public static String destination24HourDateTimeWithoutSecond = "dd MMM yyyy, HH:mm";//need to change when changin DateTime format
    public static String destinationDateFormatWithTime24hr = "dd MMM yyyy, HH:mm";
    public static String dd_MMM_yyyy = "dd MMM yyyy";
    public static String d_M_yyyy = "d M yyyy";
    public static String hh_mm_aaa = "hh:mm aaa";
    public static String HH_mm = "HH:mm";
    public static String hh_mm_aaa_comma = "hh:mm aaa,";
    public static String HH_mm_comma = "HH:mm,";
    public static String MMM_dd_comma_yyyy = "MMM dd, yyyy";
    public static String YYYY_MM_dd_hh_mm_ss_a = "yyyy-MM-dd hh:mm:ss a";
    public static String YYYY_MM_dd = "yyyy-MM-dd";
    public static String hh_mm_aaa_comma_dd_MMM_yyyy = "hh:mm aaa, dd MMM yyyy";
    public static String HH_mm_comma_dd_MMM_yyyy = "HH:mm, dd MMM yyyy";
    public static String d_MMM_yyyy = "d MMM yyyy";
    public static String YYYY_MM_dd_hh_mm_ss_sss_a = "yyyy-MM-dd hh:mm:ss.SSS a";
    public static String destinationTimeFormat = "hh:mm a";
    public static String sourceTimeFormat = "HH:mm";
    public static String mm_dd_yyyy_HiphenSeperated = "MM-dd-yyyy";
    public static String mm_dd_yyyy_slashSeperated = "MM/dd/yy";
    public static String yyyy_mm_dd_HiphenSeperatedDate = "yyyy-MM-dd";
    public static String dd_MM_yyyy_HiphenSeperatedDate = "dd-MM-yyyy";
    public static String dd_MMM_yyyy_HiphenSepertedDate = "dd-MMM-yyyy";
    public static String dd_MMM_yy_HiphenSepertedDate = "dd-MMM-yy";
    public static String destinationDateFormatWithTime24Hour = "dd MMM yyyy HH:mm:ss";//need to change when changin DateTime format
    public static String dd_MMM_yyyy_comma_HH_mm_ss = "dd MMM yyyy, HH:mm: ss";
    public static String dd_MMM_yyyy_comma_hh_mm_a = "dd MMM yyyy, hh:mm a";
    public static String eeee_format = "EEEE";
    public static String HH = "HH";
    public static String hh = "hh";
    public static String mm = "mm";
    public static String careTaskTimeFormat = "HH:mm:ss";
    public static String medicationTimeFormat = "HH: mm";
    private static String time24hour = "HH:mm";
    public static String destinationDateFormatWithTime24HourWithoutSecond = "dd MMM yyyy HH:mm";

    public static String getFormattedDateWithoutTime(String formattedDate, String sourceFormat) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat);
        formater.setTimeZone(utc);
        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationDateFormatWithoutTime);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedTimeInUTC(String formattedDate, String sourceFormat) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat);
        formater.setTimeZone(utc);
        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedDateWithoutUTC(String formattedDate, String sourceFormat) {
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.US);
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationDateFormatWithoutTime, Locale.US);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedDateWithoutUTCLocale(String formattedDate, String sourceFormat) {
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.US);
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationDateFormatWithoutTime, Locale.getDefault());
        return date_format.format(dateTime).toString();
    }

    public static String convertSourceDestinationDate(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        TimeZone utc = TimeZone.getDefault();
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.US);
        formater.setTimeZone(utc);
        //   Log.d("Date:", formattedDate);
        AppLog.d("Source format:", sourceFormat);

        AppLog.d("Destination format:", destinationFormat);
        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat, Locale.getDefault());
        return date_format.format(dateTime).toString();
    }


    public static String convertSourceDestinationDateEnglis(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        TimeZone utc = TimeZone.getDefault();
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.US);
        formater.setTimeZone(utc);
        //   Log.d("Date:", formattedDate);
        AppLog.d("Source format:", sourceFormat);

        AppLog.d("Destination format:", destinationFormat);
        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat, Locale.US);
        return date_format.format(dateTime).toString();
    }


    public static String convertSourceDestinationDateLocale(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.US);
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat, Locale.getDefault());
        return date_format.format(dateTime).toString();
    }


    public static String convertSourceLocaleToDateEnglish(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat, Locale.getDefault());
        //   Log.d("Date:", formattedDate);
        AppLog.d("Source format:", sourceFormat);

        AppLog.d("Destination format:", destinationFormat);
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat, Locale.US);
        return date_format.format(dateTime).toString();
    }


    /*[    ]*/
    public static String convertSourceDestinationDateEnglish(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        TimeZone utc = TimeZone.getDefault();
        SimpleDateFormat formater = new SimpleDateFormat(sourceFormat);
        formater.setTimeZone(utc);
        //   Log.d("Date:", formattedDate);
        AppLog.d("Source format:", sourceFormat);

        AppLog.d("Destination format:", destinationFormat);
        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat, Locale.US);
        return date_format.format(dateTime).toString();
    }

    public static String getCurrentDate(Date date) {
        SimpleDateFormat df2 = new SimpleDateFormat(destinationDateFormatWithoutTime);
        return df2.format(date);
    }

    public static String getCurrentDateEnglish(Date date) {
        SimpleDateFormat df2 = new SimpleDateFormat(destinationDateFormatWithoutTime, Locale.US);
        return df2.format(date);
    }

    public static String getFormattedTime(Date date) {
        SimpleDateFormat df2 = new SimpleDateFormat(destinationTimeFormat);
        return df2.format(date);
    }


    public static String getFormattedTimeIn24Hour(Date date) {
        SimpleDateFormat df2 = new SimpleDateFormat(time24hour);
        return df2.format(date);
    }

    public static String genralDateTime(String date, String outputFormate) {
        Calendar cal = Calendar.getInstance();// 28 Aug 2015
        SimpleDateFormat sdf;
        try {
            sdf = new SimpleDateFormat(DateFormatWithTimeInSeconds, Locale.getDefault());

            cal.setTime(sdf.parse(date));
        } catch (Exception e) {

        }

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        sdf = new SimpleDateFormat(outputFormate);
        return sdf.format(cal.getTime());
    }

    public static String calculateDate(String date, String _sourceDateFormat, String _destinationDateFormat) {
        try {
            Calendar cal = Calendar.getInstance();// Aug28, 2015

            SimpleDateFormat sdf = new SimpleDateFormat(_sourceDateFormat, Locale.getDefault());

            cal.setTime(sdf.parse(date));


            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

            sdf = new SimpleDateFormat(_destinationDateFormat);
            return sdf.format(cal.getTime());
        } catch (ParseException e) {
        }
        return null;
    }


    public static String calculateDateEnglis(String date, String _sourceDateFormat, String _destinationDateFormat) {
        try {
            Calendar cal = Calendar.getInstance();// Aug28, 2015

            SimpleDateFormat sdf = new SimpleDateFormat(_sourceDateFormat, Locale.US);

            cal.setTime(sdf.parse(date));


            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

            sdf = new SimpleDateFormat(_destinationDateFormat, Locale.US);
            return sdf.format(cal.getTime());
        } catch (ParseException e) {
        }
        return null;
    }

    public static String calculateDateLocle(String date, String _sourceDateFormat, String _destinationDateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(_sourceDateFormat, Locale.US);
            SimpleDateFormat sdf_output = new SimpleDateFormat(_destinationDateFormat, Locale.getDefault());

            Calendar cal = Calendar.getInstance();// Aug28, 2015
            cal.setTime(sdf.parse(date));
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

            return sdf_output.format(cal.getTime());
        } catch (ParseException e) {
        }
        return null;
    }

    public static String calculateDateEnglish(String date, String _sourceDateFormat, String _destinationDateFormat) {
        try {
            Calendar cal = Calendar.getInstance();// Aug28, 2015

            SimpleDateFormat sdf = new SimpleDateFormat(_sourceDateFormat, Locale.getDefault());

            cal.setTime(sdf.parse(date));


            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

            sdf = new SimpleDateFormat(_destinationDateFormat, Locale.US);
            return sdf.format(cal.getTime());
        } catch (ParseException e) {
        }
        return null;
    }

    public static String calculateDateTimeFromTimeStamp(long timeStamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp);
        SimpleDateFormat sdf = new SimpleDateFormat(destinationDateFormatWithTime, Locale.getDefault());
        return sdf.format(cal.getTime());
    }

    public static String calculateDateTimeFromTimeStampUploads(long timeStamp, Context context) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp);
        String destinationFormat;
        if (DateFormat.is24HourFormat(context)) {
            destinationFormat = destinationDateFormatWithTime24hr;
        } else {
            destinationFormat = destinationDateFormatWithTimeOld;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(destinationFormat, Locale.getDefault());
        return sdf.format(cal.getTime());

    }

    public static String calculateDateWithOuTime(long timeStamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp);
        SimpleDateFormat sdf = new SimpleDateFormat(destinationDateFormatWithTime, Locale.getDefault());
        String time = sdf.format(cal.getTime());
        return time.substring(0, time.indexOf(',')).trim();
    }


    public static String changeTimeRemoveHiphen(String timeStamp) {
        String format = yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        try {
            return calculateDateTimeFromTimeStamp(simpleDateFormat.parse(timeStamp).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static Calendar createCalenderObjectFromTime(String time) {
        SimpleDateFormat sdf;
        if (!DateFormat.is24HourFormat(MyApplication.getAppContext()))
            sdf = new SimpleDateFormat(DateFormatWithTime);
        else
            sdf = new SimpleDateFormat(destinationDateFormatWithTime24HourWithoutSecond);

        Date date = null;
        try {
            date = sdf.parse(getCurrentDate(new Date()) + " " + time);
        } catch (ParseException e) {
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }


    public static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }


    public static String getFormattedAMPM(String formattedDate) {
        SimpleDateFormat formater = new SimpleDateFormat("aa");
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();
        SimpleDateFormat date_format = new SimpleDateFormat("aa", Locale.getDefault());
        return date_format.format(dateTime).toString();
    }


    //return current date time in Z format as per given time zone
    public static String getFormattedDateTimeInZfFormatWithTimeZone(Calendar calendar, String timeZoneFormat) {
        calendar.setTimeZone(TimeZone.getTimeZone(timeZoneFormat));
        Date date = calendar.getTime();
        //current date and time in UTC
        SimpleDateFormat df = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        df.setTimeZone(TimeZone.getTimeZone(timeZoneFormat)); //format in given timezone
        return df.format(date);
    }

    public static String convertDateFromOneFormatToAnother(String sourceTimeFormat, String destinationFormat, String sourceDate, String timeZone) {
        SimpleDateFormat formater = new SimpleDateFormat(sourceTimeFormat);
        formater.setTimeZone(TimeZone.getTimeZone(timeZone));
        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
        try {
            cal.setTime(formater.parse(sourceDate));
            Date dateTime = cal.getTime();
            SimpleDateFormat date_format = new SimpleDateFormat(destinationFormat);
            return date_format.format(dateTime).toString();
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    //compare only dates of two calender
    public static int compare(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
        return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
    }


    public static String[] getAMPMWithLocale() {
        final String am = DateTimeUtil.convertSourceDestinationDateLocale("10", "HH", "a");
        final String pm = DateTimeUtil.convertSourceDestinationDateLocale("14", "HH", "a");
        String[] amPmStringArray = {am, pm};
        return amPmStringArray;
    }

    public static String getAMOrPM(String time) {
        String time24 = "";
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("HH:mm", Locale.US);
            SimpleDateFormat outFormat = new SimpleDateFormat("a", Locale.US);
            time24 = outFormat.format(inFormat.parse(time));
            System.out.println("time in 24 hour format : " + time24);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return time24;
    }


    public static String getHoursIn24HrFormat(String time) {
        String time24 = "";
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
            SimpleDateFormat outFormat = new SimpleDateFormat("HH", Locale.US);
            time24 = outFormat.format(inFormat.parse(time));
            System.out.println("time in 24 hour format : " + time24);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return time24;
    }


    public static String getHoursIn12HrFormat(String time) {
        String time12 = "";
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("HH:mm", Locale.US);
            SimpleDateFormat outFormat = new SimpleDateFormat("hh:mm a", Locale.US);
            time12 = outFormat.format(inFormat.parse(time));
            System.out.println("time in 24 hour format : " + time12);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return time12;
    }


    public static String getHoursIn12Or24HrFormatLocale(String time, String sourceFormat, String destinationFormat) {
        String time12 = "";
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(sourceFormat, Locale.US);
            SimpleDateFormat outFormat = new SimpleDateFormat(destinationFormat, Locale.getDefault());
            time12 = outFormat.format(inFormat.parse(time));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return time12;
    }

}
