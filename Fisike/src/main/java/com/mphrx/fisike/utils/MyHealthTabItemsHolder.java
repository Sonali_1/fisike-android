package com.mphrx.fisike.utils;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.enums.MyHealthItemsEnum;

import java.util.LinkedHashMap;

/**
 * Created by xmb2nc on 15-12-2016.
 */

public class MyHealthTabItemsHolder {
    // name would be the case and value would be the address
    // create a singleton class for this
    // create a hash map
    // check if hashmap is null
    // create a new one
    public static MyHealthTabItemsHolder myHealthTabItemsHolderInstance;
    private LinkedHashMap<String, Integer> myHealthTabItemsMap = new LinkedHashMap<String, Integer>();

    public static MyHealthTabItemsHolder getMyHealthTabItemsHolderInstance() {
        if (myHealthTabItemsHolderInstance == null) {
            myHealthTabItemsHolderInstance = new MyHealthTabItemsHolder();
        }
        return myHealthTabItemsHolderInstance;
    }

    public LinkedHashMap<String, Integer> getMyHealthTabItemsMap() {
        if (myHealthTabItemsMap == null || myHealthTabItemsMap.size() == 0) {
            return generateMapBasedOnConfig();
        }
        return myHealthTabItemsMap;
    }

    private LinkedHashMap<String, Integer> generateMapBasedOnConfig() {
        int count = 0;
        if (BuildConfig.isVitalEnabled) {
            myHealthTabItemsMap.put(MyHealthItemsEnum.Vitals.getTabItemName(), count);
            count++;
        }

        if (BuildConfig.isRecordsEnabled) {
            myHealthTabItemsMap.put(MyHealthItemsEnum.Records.getTabItemName(), count);
            count++;
        }

        if (BuildConfig.isMedicationEnabled) {
            myHealthTabItemsMap.put(MyHealthItemsEnum.Medications.getTabItemName(), count);
            count++;
        }

        if (BuildConfig.isUploadsEnabled) {
            myHealthTabItemsMap.put(MyHealthItemsEnum.Uploads.getTabItemName(), count);
            count++;
        }
        return myHealthTabItemsMap;
    }


    public void setmyHealthTabItemsMapClear() {
        myHealthTabItemsMap.clear();
    }


}
