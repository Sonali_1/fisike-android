package com.mphrx.fisike.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;

import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by Kailash Khurana on 11/8/2016.
 */

public class CountryCodeUtils {

    public static String getCountryId(Context context) {
        String CountryID = null;
        try {
            CountryID = getUserCountry(context).toString().toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
            if(TextConstants.DEFAULT_COUNTRY_INDIA)
            {
                CountryID = TextConstants.IN_COUNTRYNAME;
            }
            else
            {
                CountryID = TextConstants.US_COUNTRYNAME;
            }
        }
        return CountryID;
    }

    public static String getCountryZipCode(Context context,String CountryID) {
        String CountryZipCode = null;
        try {
            String[] rl = context.getResources().getStringArray(
                    R.array.CountryCodes);
            for (int i = 0; i < rl.length; i++) {
                String[] g = rl[i].split(",");
                if (g[1].trim().equals(CountryID.trim())) {
                    CountryZipCode = g[0];
                    break;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return CountryZipCode;
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country
                // code is
                // available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static int getCountryPosition(Context context, int country_code, String countryId) {

        String[] list = context.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < list.length; i++) {
            StringTokenizer stringTokenizer = new StringTokenizer(list[i].toString().trim(), ",");
            //  String codeval =
            stringTokenizer.nextToken();
            String valuecode = stringTokenizer.nextToken();
            if (valuecode.equals(countryId)) {
                return i;
            }
        }
        return 0;
    }
}
