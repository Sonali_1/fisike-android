package com.mphrx.fisike.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.widget.EditText;

import com.mphrx.fisike.constant.TextConstants;

public class GenericTextWatcher implements TextWatcher {

	private EditText view;

	public GenericTextWatcher(EditText view) {
		this.view = view;
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		if (arg0.length() == 0) {
			// No entered text so will show hint
			view.setTextSize(TypedValue.COMPLEX_UNIT_SP, TextConstants.EDIT_TEXT_HINT_TEXT_SIZE);
		} else {
			view.setTextSize(TypedValue.COMPLEX_UNIT_SP, TextConstants.EDIT_TEXT_SIZE);
		}

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

}
