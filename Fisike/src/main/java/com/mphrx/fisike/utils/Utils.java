package com.mphrx.fisike.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.text.TextUtilsCompat;
import android.support.v4.view.ViewCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.Settings;
import com.mphrx.fisike.SplashActivity;
import com.mphrx.fisike.UserProfileActivity;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.connection.LoginConnection;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.MedicationDurationEnum;
import com.mphrx.fisike.enums.MedicationUnitsEnum;
import com.mphrx.fisike.enums.MyHealthItemsEnum;
import com.mphrx.fisike.fragments.SingleChatFragment;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.interfaces.ImageOperationCallBack;
import com.mphrx.fisike.interfaces.NotifyFabIconToDisplay;
import com.mphrx.fisike.mo.ChatConversationMO;
import com.mphrx.fisike.mo.ConfigMO;
import com.mphrx.fisike.mo.Practitioner_Data.PractitionerMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.ChatConversationDBAdapter;
import com.mphrx.fisike.persistence.DBAdapter;
import com.mphrx.fisike.persistence.EncounterDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.regionalBlock.RegionalBlockActivity;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.services.NotifyAlarm;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;
import com.mphrx.fisike.view.RoundedImageView;
import com.mphrx.fisike_physician.activity.MessageListActivity;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import appointment.utils.CommonTasks;
import login.activity.LoginActivity;

public class Utils {

    private static int notificationNumber;
    private static ProgressDialog dialog;
    public static NotifyFabIconToDisplay fabInterface;
    static String Year = "y";
    static String month = "m";
    static String week = "w";
    static String day = "d";
    static String hour = "hrs";
    static String min = "min";
    private static Context currentActivityContext;
    private static String number;
    private static boolean isMobile;
    private static String indexes;

    public static String getNumber() {
        return number;
    }

    public static boolean isMobile() {
        return isMobile;
    }

    public static boolean checkIfEncounterExists(int encounterId, Context context) {
        try {
            return EncounterDBAdapter.getInstance(context).checkIfEncounterExists(encounterId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static boolean menuClickAction(final Activity activity, int id, boolean isFirstLogin, final MessengerService messengerService) {
        if (id == R.layout.lock_screen_activity) {
            try {

                SharedPreferences preferences = activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                String pincode = preferences.getString(VariableConstants.LOCK_PIN_CODE, "");
                boolean isToSetPin = false;
                if (pincode == null || pincode.equals("")) {
                    isToSetPin = true;
                }
                // ((Activity) activity).overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
                if (!Utils.isAppbackground(activity) && !Utils.isTopActivityLockScreen(activity) && !Utils.checkTopActivity(activity, TextConstants.UPDATE_PIN)) {
                    AppLog.d("Fisike", "Lock Screen Thread 2 thread Id : " + Thread.currentThread().getId() + " Thread Name : "
                            + Thread.currentThread().getName());
                    activity.startActivity(new Intent(activity, com.mphrx.fisike.lock_screen.LockScreenActivity.class).putExtra(VariableConstants.IS_TO_SET_PASSWORD, isToSetPin)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                activity.finish();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else if (id == TextConstants.btnChat) {
            try {
                UserMO userMO = SettingManager.getInstance().getUserMO();
                SharedPreferences preferences = activity.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);

                boolean is_logout = preferences.getBoolean(VariableConstants.IS_LOGOUT, false);
                boolean isLogoutDialogShowing = preferences.getBoolean(VariableConstants.IS_LOGOUT_DIALOG_SHOWING, false);

                if (!is_logout && !isLogoutDialogShowing) {
                    userMO.setLastAuthenticatedTime(System.currentTimeMillis());
                    try {
                        //SettingManager.getInstance().updateUserMO(userMO);
                        UserDBAdapter.getInstance(activity).updateLastAuthenticatedTime(userMO.getPersistenceKey(), userMO.getLastAuthenticatedTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Intent settingsActivity = new Intent(activity, HomeActivity.class);
                settingsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(settingsActivity);
                activity.finish();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        if (id == TextConstants.MENU_ITEM_PROFILE) {
            try {
                Intent settingsActivity = new Intent(activity, UserProfileActivity.class);
                settingsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                activity.startActivity(settingsActivity);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else if (id == TextConstants.MENU_ITEM_SETTINGS) {
            try {
                Intent settingsActivity = new Intent(activity, Settings.class);
                settingsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                activity.startActivity(settingsActivity);
                // activity.finish();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else if (id == TextConstants.MENU_ITEM_LOGOUT) {
            DialogUtils.showAlertDialogCommon(activity, activity.getResources().getString(R.string.confirm_signout),
                    activity.getResources().getString(R.string.logout_msg),
                    activity.getResources().getString(R.string.txt_yes),
                    activity.getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case Dialog.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;

                                case Dialog.BUTTON_POSITIVE:
                                    messengerService.showDialog(activity);
/*                                messengerService.logout(TextConstants.STATUS_UNAVAILABLE_TEXT, TextConstants.STATUS_UNAVAILABLE, "", activity);*/
                                    messengerService.logoutUser(activity);
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
        }
        return false;
    }


    public static void revertForgetPasswordInitiation() {
        SharedPref.setForgetPassOTP(null); //reverting forget pass
        SharedPref.setForgetPassOTPSendTime(0);//reverting forget pass
    }

    public static void enableSoundInNotification(NotificationCompat.Builder mBuilder, Context context) {
        try {
            SharedPreferences store = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            String uriString = store.getString(VariableConstants.GERNAL_NOTIFICATON_SOUND, VariableConstants.DEFAULT_NOTIFICATION_SOUND);
            mBuilder.setSound(Uri.parse(uriString));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method generates an in-app notification Depending
     *
     * @param context
     * @param notificationText
     * @param isPushNotification
     */
    public static void generateNotification(Context context, String notificationText, boolean isPushNotification,
                                            ChatConversationMO chatConversationMo) {

        if (SharedPref.isUserLogout()) {
            return;
        }
        if (BuildConfig.isPhysicianApp) {
            if (!SharedPref.getIsShowNotification()) {
                return;
            }
        }
        long currentDate;
        // add the check for chat notification muteSIgn
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean isMessageNotificationEnabled = preferences.getBoolean(VariableConstants.MSG_NOTIFICATION_ENABLED, true);
        // if message notifications are disabled then return
        if (!isMessageNotificationEnabled) {
            return;
        }
        int notificationNumber = 0;
        DBAdapter mHelper;
        ChatConversationMO receivedMessageChatConversationMO = null;
        try {
            receivedMessageChatConversationMO = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(ChatConversationMO.generatePersistenceKey(chatConversationMo.getJId(), SettingManager.getInstance().getUserMO().getId() + "") + "");
        } catch (Exception e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

        if (receivedMessageChatConversationMO == null) {
            return;
        }
        long muteEndDate = receivedMessageChatConversationMO.getMuteChatTillTime();
        int isShowNotificationEnabled = receivedMessageChatConversationMO.getIsNotificationEnabled();

        currentDate = System.currentTimeMillis();
        // don't show the notifications if the show notifications are disabled

        if (isShowNotificationEnabled < 0 && (muteEndDate >= currentDate)) {
            return;
        }

        if (!isPushNotification) {
            UserMO userMO = null;
            try {
                userMO = SettingManager.getInstance().getUserMO();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (userMO != null) {
                mHelper = DBAdapter.getInstance(MyApplication.getAppContext());

                ArrayList<String> chatConversationKeyList = userMO.getChatConversationKeyList();

                for (int i = 0; chatConversationKeyList != null && i < chatConversationKeyList.size(); i++) {
                    String persistenceKey = chatConversationKeyList.get(i);
                    ChatConversationMO fetchChatConversationMO = null;
                    try {
                        fetchChatConversationMO = ChatConversationDBAdapter.getInstance(context).fetchChatConversationMO(persistenceKey);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (fetchChatConversationMO == null) {
                        continue;
                    } else if (0 == fetchChatConversationMO.getCreatedTimeUTC()) {
                        continue;
                    } else if (fetchChatConversationMO.getUnReadCount() > 0) {
                        notificationNumber += fetchChatConversationMO.getUnReadCount();
                    }
                }
            }
        }
        NotificationCompat.Builder mBuilder;
        if (!isPushNotification) {
            String msgNotification = "";
            if (notificationNumber < 2) {
                msgNotification = context.getResources().getString(R.string.You_have) + (notificationNumber) + context.getResources().getString(R.string.unread_conversation);
            } else {
                msgNotification = context.getResources().getString(R.string.You_have) + (notificationNumber) + context.getResources().getString(R.string.unread_conversations);
            }
            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon).setContentTitle(context.getResources().getString(R.string.New_Message))
                    .setContentText(msgNotification).setWhen(System.currentTimeMillis()).setTicker(msgNotification);
        } else {
            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon).setContentTitle(context.getResources().getString(R.string.New_Message))
                    .setContentText(notificationText).setWhen(System.currentTimeMillis()).setTicker(notificationText);
        }

        // enables sound to notification


        if (BuildConfig.isPhysicianApp) {
            if (SharedPref.getIsMute()) {
                long muteTime = SharedPref.getMuteTimeInMills();
                long now = System.currentTimeMillis();

                if (now > muteTime) {
                    enableSoundInNotification(mBuilder, context);
                }
            } else {
                enableSoundInNotification(mBuilder, context);
            }
        } else {
            if (!(muteEndDate >= currentDate)) {
                enableSoundInNotification(mBuilder, context);
            }
        }

        mBuilder.setAutoCancel(true);

        try {
            // Creates an explicit intent for an Activity in your app
            // if (isTopActivityRecentChat(context)) {
            // resultIntent = new Intent();
            // } else {


            Class<?> activity;

            Intent resultIntent = new Intent();
            if (BuildConfig.isPhysicianApp) {
                if (SingleChatFragment.isMessageListActivityRunning) {
                    return;
                }
                activity = MessageListActivity.class;
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                activity = SplashActivity.class;
            }
            resultIntent.setClass(context, activity);

            // }
            if (isPushNotification) {
                resultIntent.putExtra(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE, false);
                resultIntent.putExtra(VariableConstants.IS_PUSH_NOTIFICATION_MESSAGE, true);
            } else {
                resultIntent.putExtra(VariableConstants.IS_LOCAL_NOTIFICATION_MESSAGE, true);
                resultIntent.putExtra(VariableConstants.IS_PUSH_NOTIFICATION_MESSAGE, false);
            }

			/*
             * // The stack builder object will contain an artificial back stack for the // started Activity. // This ensures that navigating backward
			 * from the Activity leads out of // your application to the Home screen. TaskStackBuilder stackBuilder =
			 * TaskStackBuilder.create(context); // Adds the back stack for the Intent (but not the Intent itself)
			 * stackBuilder.addParentStack(ListPersonsActivity.class); // Adds the Intent that starts the Activity to the top of the stack
			 * stackBuilder.addNextIntent(resultIntent);
			 */
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            // resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // mId allows you to update the notification later on.
            mNotificationManager.notify(com.mphrx.fisike.constant.VariableConstants.CHAT_NOTIFICATION_NUMBER, mBuilder.build());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /**
     * This method generates an in-app notification Depending
     *
     * @param context
     * @param notificationText
     * @param prescriptionModel
     */
    public static void generateNotification(Context context, String notificationText, PrescriptionModel prescriptionModel, MedicineDoseFrequencyModel doseInstructionCode) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        try {
            NotificationCompat.Builder mBuilder;

            String text[] = notificationText.split(":");
            String titletext, displayString;
            titletext = context.getResources().getString(R.string.app_name);
            displayString = notificationText;
            if (text.length == 2) {
                titletext = text[0];
                displayString = text[1];
            }
            if (titletext.trim().equals(""))
                titletext = context.getResources().getString(R.string.app_name);
            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle(titletext).setContentText(displayString).setWhen(System.currentTimeMillis())
                    .setTicker(notificationText);

            NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
            style.addLine(displayString);
            mBuilder.setStyle(style);
            // if medication notifications are turned off in setting then return
            boolean isMedicationNotificationEnabled = preferences.getBoolean(VariableConstants.MEDICATION_NOTIFICATION_ENABLED, true);
            if (!isMedicationNotificationEnabled) {
                return;
            }
            mBuilder.setAutoCancel(true);
            if (Utils.isAppbackground(context)) {
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            }
            Intent resultIntent;
            resultIntent = new Intent(context, HomeActivity.class);
            resultIntent.putExtra(VariableConstants.NOTIFICATION_REMINDER, true);
            resultIntent.putExtra(VariableConstants.PRACTIONER_NAME, prescriptionModel.getPractitionerName());
            resultIntent.putExtra(VariableConstants.MEDICIAN_NAME, prescriptionModel.getMedicineName());
            resultIntent.putExtra(VariableConstants.QUANTITY, doseInstructionCode.getDoseQuantity());
            resultIntent.putExtra(VariableConstants.UNIT, doseInstructionCode.getDoseUnit());
            String ampm = doseInstructionCode.getDoseTimeAMorPM() != null ? " " + doseInstructionCode.getDoseTimeAMorPM() : "";
            resultIntent.putExtra(VariableConstants.TIMMING, doseInstructionCode.getDoseHours() + ":" + doseInstructionCode.getDoseMinutes() + ampm);
            resultIntent.putExtra(VariableConstants.TIMMING, doseInstructionCode.getDoseHours() + ":" + doseInstructionCode.getDoseMinutes() + " " + doseInstructionCode.getDoseTimeAMorPM());
            resultIntent.putExtra(VariableConstants.MEDICATION_PRESCRIPTION_ID, prescriptionModel.getID());
            resultIntent.putExtra(VariableConstants.DOSE_INSTRUCTION_CODE, doseInstructionCode.getAlarmManagerUniqueKey());
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int id = (int) System.currentTimeMillis();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, id, resultIntent, 0);

            // resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(prescriptionModel.getID(), mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void generateDigitizationNotification(Context context, String notificationText, String medicationPrescriptionId,
                                                        String documentType, String refId, String title) {

        if (refId == null) {
            refId = "";
        }
        try {
            SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            boolean isUploadNotificationEnabled = preferences.getBoolean(VariableConstants.UPLOADS_NOTIFICATION_ENABLED, true);
            if (!isUploadNotificationEnabled) {
                return;
            }
            String notificationTitle = "";
            if (title == null || title.equalsIgnoreCase(context.getResources().getString(R.string.txt_null))) {
                notificationTitle = context.getResources().getString(R.string.app_name);
            } else {
                notificationTitle = title;
            }
            NotificationCompat.Builder mBuilder;
            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle(notificationTitle).setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText)).
                            setContentText(notificationText).setWhen(System.currentTimeMillis())
                    .setTicker(notificationText);

            // if medication notifications are turned off in setting then return
            mBuilder.setAutoCancel(true);
//            if (Utils.isAppbackground(context)) {
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
//            }
            Intent resultIntent;
            resultIntent = new Intent(context, HomeActivity.class);
            resultIntent.putExtra(VariableConstants.DIGITIZATION_REMINDER, true);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            resultIntent.putExtra(VariableConstants.REFRENCE_ID, refId);
            resultIntent.putExtra(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION, documentType);
            int id = (int) System.currentTimeMillis();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, id, resultIntent, 0);

            // resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(id, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hl7 notification
    public static void generateLabReportNotification(Context context, String documentType, String reportName, String patientName,
                                                     String organizationName, int diagnosticOrderId, String organisationId,
                                                     String alertMsg, String title) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            boolean isUploadNotificationEnabled = preferences.getBoolean(VariableConstants.UPLOADS_NOTIFICATION_ENABLED, true);
            if (!isUploadNotificationEnabled) {
                return;
            }
            NotificationCompat.Builder mBuilder;
            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon).
                    setContentTitle(title).setStyle(new NotificationCompat.BigTextStyle().bigText(alertMsg)).
                    setContentText(alertMsg).setWhen(System.currentTimeMillis());


            mBuilder.setAutoCancel(true);
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
            Intent resultIntent;
            resultIntent = new Intent(context, HomeActivity.class);
            resultIntent.putExtra(VariableConstants.DIGITIZATION_REMINDER, true);
            resultIntent.putExtra(VariableConstants.DIGITIZATION_MEDICATION_PRECRIPTION, diagnosticOrderId);
            resultIntent.putExtra(VariableConstants.ORGANISATION_ID, organisationId);
            resultIntent.putExtra(VariableConstants.MEDICATION_DIGITIZATION_NOTIFICATION, documentType);
            int id = (int) System.currentTimeMillis();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, id, resultIntent, 0);

            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(id, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFormattedDate(String formattedDate) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            SimpleDateFormat formater1 = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated);
            formater1.setTimeZone(utc);
            try {
                cal.setTime(formater1.parse(formattedDate));
            } catch (ParseException e1) {
                return "";
            }
        }
        Date dateTime = cal.getTime();

        SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.dd_MMM_yyyy);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedDate_Setting(String formattedDate) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_Hiphen_Seperated);
        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();

        SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.MM_dd_yyyy);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedDate(String formattedDate, String sourceFormat, String destinationFormat) {
        try {
            if (formattedDate == null || formattedDate.equals("")) {
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
            SimpleDateFormat sdf1 = new SimpleDateFormat(destinationFormat);
            sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            java.util.Date dateObj = null;

            dateObj = sdf.parse(formattedDate);
            return sdf1.format(dateObj).toString();
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }

    }

    public static String getFormattedDateLocale(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(destinationFormat, Locale.getDefault());
        sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        java.util.Date dateObj = null;
        try {
            dateObj = sdf.parse(formattedDate);
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        return sdf1.format(dateObj).toString();
    }


    public static String getFormattedDateEnglish(String formattedDate, String sourceFormat, String destinationFormat) {
        if (formattedDate == null || formattedDate.equals("")) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(destinationFormat, Locale.US);
        sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        java.util.Date dateObj = null;
        try {
            dateObj = sdf.parse(formattedDate);
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        return sdf1.format(dateObj).toString();
    }



   /* public static String getFormattedDateMonth(String formattedDate) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat( DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();

        SimpleDateFormat date_format = new SimpleDateFormat("MMM dd, yyyy");
        return date_format.format(dateTime).toString();
    }*/

    public static String getFormattedDate_MMDDYYYY(String formattedDate) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();

        SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
        return date_format.format(dateTime).toString();
    }

    public static String getFormattedDateApi(String formattedDate) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            // e.printStackTrace();
            return "";
        }
        Date dateTime = cal.getTime();

        SimpleDateFormat date_format = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
        return date_format.format(dateTime).toString();
    }

    public static Date getDate(String formattedDate) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formater = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_Hiphen_Seperated);

        formater.setTimeZone(utc);

        GregorianCalendar cal = new GregorianCalendar(utc);
        try {
            cal.setTime(formater.parse(formattedDate));
        } catch (ParseException e) {
            // e.printStackTrace();
            return null;
        }
        return cal.getTime();
    }

    public static String getDateFormateForSave(long dateMilliSecound) {
        DateFormat formatter = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_T_HH_mm_ss_Hiphen_Seperated);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateMilliSecound);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(calendar.getTime());
    }

    /**
     * This method check if the top activity is recent chat view
     *
     * @param context
     * @return
     */
    public static boolean isTopActivityRecentChat(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            String packageName = context.getPackageName();
            if (packageName == null) {
                return false;
            } else if (topActivity.getPackageName().equals(packageName) && topActivity.getShortClassName().equals(TextConstants.LIST_ACTIVITY)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * This method check if the top activity is Lock screen
     *
     * @param context
     * @return
     */
    public static boolean isTopActivityLockScreen(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            String packageName = context.getPackageName();
            if (packageName == null) {
                return false;
            } else if (topActivity.getPackageName().equals(packageName) && topActivity.getShortClassName().equals(TextConstants.LOCK_SCREEN)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * This method check if the top activity is Login Activity
     *
     * @param context
     * @return
     */
    public static boolean isTopActivityLoginScreen(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            String packageName = context.getPackageName();
            if (packageName == null) {
                return false;
            } else if (topActivity.getPackageName().equals(packageName) && topActivity.getShortClassName().equals(TextConstants.LOGIN_SCREEN)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    /**
     * This method check if the application is in background or forground
     *
     * @param context
     * @return
     */
    public static boolean isAppbackground(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            /**
             * For lollipop
             */
            List<RunningAppProcessInfo> runningAppProcesses = am.getRunningAppProcesses();
            if (null == runningAppProcesses) {
                return false;
            }
            Iterator<RunningAppProcessInfo> runningProcess = runningAppProcesses.iterator();
            while (runningProcess.hasNext()) {
                RunningAppProcessInfo info = runningProcess.next();
                if (info.processName.equalsIgnoreCase(context.getPackageName())) {
                    if (info.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            return false;
        } else {
            /**
             * upto 19 api
             */
            ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
            List<RunningTaskInfo> tasks = am.getRunningTasks(1);
            if (!tasks.isEmpty()) {
                ComponentName topActivity = tasks.get(0).topActivity;
                String packageName = context.getPackageName();
                if (packageName == null) {
                    return true;
                } else if (topActivity.getPackageName().equals(packageName)) {
                    return false;
                } else {
                    return true;
                }
            }
            return true;
        }
    }

    /**
     * Cancel notification on chating screen
     *
     * @param ctx
     * @param notifyId
     */
    public static void cancelNotification(Context ctx, int notifyId) {
        try {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
            nMgr.cancel(notifyId);
        } catch (Exception e) {

        }
    }

    /**
     * This method display lock screen if the logout time crossed - on Recent chat screen - on background to forground activity
     *
     * @param context
     * @return
     */
    public static boolean checkSession(Context context) {
        /*SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        boolean is_logout = preferences.getBoolean(VariableConstants.IS_LOGOUT, false);
        boolean isLogoutDialogShowing = preferences.getBoolean(VariableConstants.IS_LOGOUT_DIALOG_SHOWING, false);

        if (is_logout && !isLogoutDialogShowing) {
            Editor edit = preferences.edit();
            edit.putBoolean(VariableConstants.IS_LOGOUT_DIALOG_SHOWING, true);
            edit.commit();
*/
        // ((Activity) context).overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
        if (!Utils.isAppbackground(context) && !Utils.checkTopActivity(context, TextConstants.OTP_SCREEN)) {
            AppLog.d("Fisike", "Lock Screen Thread 3 thread Id : " + Thread.currentThread().getId() + " Thread Name : "
                    + Thread.currentThread().getName());
/*                context.startActivity(new Intent(context, com.mphrx.fisike.lock_screen.LockScreenActivity.class).putExtra(VariableConstants.IS_TO_SET_PASSWORD, false).addFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK));*/
            SharedPref.setAccessToken(null);
        }
        return true;
//        }
  /*      if (isLogoutDialogShowing) {
            return true;
        }
        return false;
*/
    }

    /**
     * This method is to clean the XMPP Username so that it can be used within the application -- basically remove the /Smack or /xmpp that comes at
     * the end of the XML response
     *
     * @param name
     * @return
     */
    public static String cleanXMPPUserName(String name) {
        try {
            StringBuilder temp = new StringBuilder(name);
            temp.replace(name.lastIndexOf("/"), name.length(), "");
            name = temp.toString();
        } catch (Exception e) {
            return name;
        }
        return name;
    }

    /**
     * This method is sender jid in case of group chat group/senderjid
     *
     * @param name
     * @return
     */
    public static String getSenderJid(String name) {
        String[] split = name.split(Pattern.quote("/"));
        if (split.length > 0) {
            return split[split.length - 1];
        }
        return name;
    }

    public static String getGroupIdFrom(String name) {
        String[] split = name.split(Pattern.quote("/"));
        if (split.length > 0) {
            return split[0];
        }
        return name;
    }

    /**
     * This method is to clean the XMPP Username so that it can be used within the application -- basically remove the @servername
     *
     * @param name
     * @return
     */
    public static String cleanXMPPServer(String name) {
        try {
            StringBuilder temp = new StringBuilder(name);
            temp.replace(name.indexOf("@"), name.length(), "");
            name = temp.toString();
        } catch (Exception e) {
            return name;
        }
        return name;
    }

    /**
     * This method cleans up the message text that is being recieved from the XMPP Server
     *
     * @param text
     * @return
     */
    public static String cleanMessageText(String text) {
        if (text.contains(VariableConstants.DELIMITER)) {
            String[] split = text.split(Pattern.quote(VariableConstants.DELIMITER));
            return split[1];
        } else {
            return text;
        }
    }

    /**
     * This method converts the xmpp username into the fisike username -- this is WITHOUT decoding the string
     *
     * @param xmppUserName
     * @param xmppServerName
     * @return
     */
    public static String getFisikeUserName(String xmppUserName, String xmppServerName) {
        if (xmppUserName.contains("@" + xmppServerName)) {
            StringBuilder b = new StringBuilder(xmppUserName);
            b.replace(xmppUserName.lastIndexOf("@"), xmppUserName.length(), "");
            return b.toString();
        } else {
            return xmppUserName;
        }
    }

    public static boolean isSameDate(long time, long time1) {
        // 0 - today, 1 - tomorrow, -1 - yesterday
        return 0 == diff(time, time1) ? true : false;
    }

    public static long diff(long time, long time1) {
        long fieldTime = getFieldInMillis(Calendar.DAY_OF_YEAR);
        return (time / fieldTime - time1 / fieldTime);
    }

    public static long diff(long time, int field) {
        long fieldTime = getFieldInMillis(field);
        Calendar cal = Calendar.getInstance();
        long now = cal.getTimeInMillis();
        return (time / fieldTime - now / fieldTime);
    }

    private static final long getFieldInMillis(int field) {
        Calendar cal = Calendar.getInstance();
        long now = Calendar.getInstance().getTimeInMillis();
        cal.add(field, 1);
        long after = cal.getTimeInMillis();
        return after - now;
    }

    /**
     * JSON parsing method
     *
     * @param obj
     * @param key
     * @return
     * @throws JSONException
     */
    public static String getString(JSONObject obj, String key) throws JSONException {
        try {
            String str = obj.getString(key);

            if (str.startsWith("[")) {
                str = str.substring(1, str.length() - 1);
            }
            return str;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * To get the key for the object in json result
     *
     * @param obj
     * @param key
     * @return
     * @throws JSONException
     */
    public static String getStringOrNull(JSONObject obj, String key) throws JSONException {
        try {
            String str = obj.getString(key);
            if (str != null) {
                if (str.startsWith("[")) {
                    str = str.substring(1, str.length() - 1);
                }
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * JSON parsing method
     *
     * @param obj
     * @param key
     * @return
     * @throws JSONException
     */
    public static boolean getBoolean(JSONObject obj, String key) throws JSONException {
        try {
            boolean str = obj.getBoolean(key);
            return str;
        } catch (Exception e) {
            return false;
        }
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int size) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff4242DB;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = (100 <= size) ? 10 : 24;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        // canvas.drawCircle(0, 0, bitmap.getWidth(), paint);
        canvas.drawBitmap(bitmap, rect, rect, paint);

        // RectF roundRect; // the Rect you have to draw into
        // Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //
        // // draw the border at bottom
        // mPaint.setStyle(Paint.Style.FILL);
        // mPaint.setColor(mBorderColor);
        // canvas.drawRoundRect(roundRect, mRadius, mRadius, mPaint);
        //
        // // ------------------ draw scheme bitmap
        // roundRect.set(itemRect.left + mBorderSize, itemRect.top + mBorderSize, itemRect.right - mBorderSize, itemRect.bottom - mBorderSize);
        // Shader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        // mPaint.setShader(shader);
        // canvas.drawRoundRect(roundRect, mRadius, mRadius, mPaint);
        // mPaint.setShader(null);

        return output;
    }

    public static Bitmap roundCornerImage(Bitmap src, float round) {
        // Source image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create result bitmap output
        Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        // set canvas for painting
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);

        // configure paint
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);

        // configure rectangle for embedding
        final Rect rect = new Rect(3, 3, width - 3, height - 3);
        final RectF rectF = new RectF(0, 0, width, height);

        // draw Round rectangle to canvas
        canvas.drawRoundRect(rectF, round, round, paint);

        // create Xfer mode
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        // draw source image to canvas
        canvas.drawBitmap(src, rect, rect, paint);

        // return final image
        return result;
    }

    public static String getRealPathFromURI(Uri contentUri, Context context) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};

        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (null != cursor && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            cursor.close();
        }
        return res;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertSpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, dp, metrics);
        return px;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Create a file Uri for saving an image or video
     */
    public static Uri getOutputMediaFileUri(int type, Context context) {
        String extenstion = "";
        if (type == MEDIA_TYPE_IMAGE) {
            extenstion = ".jpg";
        } else if (type == MEDIA_TYPE_VIDEO) {
            extenstion = ".mp4";
        }
        File outputMediaFile = getOutputMediaFile(type, extenstion, context);
        if (outputMediaFile != null) {
            return Uri.fromFile(outputMediaFile);
        }
        return null;
    }

    /**
     * Create a File for saving an image or video
     *
     * @param extensionFromMimeType
     */
    public static File getOutputMediaFile(int type, String extensionFromMimeType, Context context) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                context.getString(R.string.app_name));
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                AppLog.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat(DateTimeUtil.yyyyMMdd_HHmmss).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "temp.jpg");
        if (type == MEDIA_TYPE_IMAGE) {
            // mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            // mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /**
     * Delete the captured file for the external directory/internal directory if not in use
     *
     * @param context
     * @param internalContentUri
     * @param attachment_type
     */
    public static void deleteFile(Context context, String filePath, Uri internalContentUri, String attachment_type) {
        try {
            File mediaFile = new File(filePath);
            if (mediaFile.exists()) {
                int id = getImageContentUri(context, mediaFile, internalContentUri, attachment_type);
                String mediaId = attachment_type.equals(Utils.MEDIA_TYPE_VIDEO) ? MediaStore.Video.Media._ID : MediaStore.Images.Media._ID;

                if (-1 != id && null != internalContentUri) {
                    removeImage(id, context, internalContentUri, mediaId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove the file form the directory
     *
     * @param id
     * @param context
     * @param uri
     * @param mediaId
     */
    public static void removeImage(int id, Context context, Uri uri, String mediaId) {
        try {
            ContentResolver cr = context.getContentResolver();
            int delete = cr.delete(uri, mediaId + "=?", new String[]{Long.toString(id)});
        } catch (Exception e) {
        }
    }

    public static int getImageContentUri(Context context, File imageFile, Uri internalContentUri, String attachmentType) {
        String mediaId = MediaStore.Images.Media._ID;
        String mediaData = MediaStore.Images.Media.DATA;
        String filePath = imageFile.getAbsolutePath();
        if (attachmentType.equalsIgnoreCase(VariableConstants.ATTACHMENT_VIDEO)) {
            mediaId = MediaStore.Video.Media._ID;
            mediaData = MediaStore.Video.Media.DATA;
        }
        Cursor cursor = context.getContentResolver().query(internalContentUri, new String[]{mediaId}, mediaData + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            // Uri baseUri = Uri.parse("content://media/external/images/media");
            // return Uri.withAppendedPath(baseUri, "" + id);
        }
        return -1;
    }

    public static File moveToInternalStorage(Context context, String filePath, int attachmentType) {
        File mediaFile = new File(filePath);
        if (mediaFile.exists()) {
            String filename = mediaFile.getName();
            String filenameArray[] = filename.split("\\.");
            String extension = filenameArray[filenameArray.length - 1];
            File saveToInternalSorage = saveToInternalSorage(context, attachmentType, extension);
            copyFile(context, mediaFile, saveToInternalSorage);
            return saveToInternalSorage;
        }
        return null;
    }

    public static File encriptAndMove(Context context, String filePath, int mediaType, boolean isToDeleteFile, String senderName, String appName) {
        File mediaFile = new File(filePath);
        if (mediaFile.exists()) {
            String encodeFilePath = Citadel.encodeFile(filePath, context, appName, senderName);
            Uri mediaUri = null;
            String attachmentType = "";
            if (Utils.MEDIA_TYPE_VIDEO == mediaType) {
                mediaUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                attachmentType = VariableConstants.ATTACHMENT_VIDEO;
            } else {
                mediaUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                attachmentType = VariableConstants.ATTACHMENT_IMAGE;
            }
            if (null == encodeFilePath || "".equals(encodeFilePath)) {
                if (isToDeleteFile) {
                    deleteFile(context, filePath, mediaUri, attachmentType);
                }
                return null;
            }
            if (isToDeleteFile) {
                deleteFile(context, filePath, mediaUri, attachmentType);
            }
            File encodeFile = new File(encodeFilePath);
            // copyFile(context, mediaFile, encodeFile);
            return encodeFile;
        }
        return null;
    }

    public static boolean copyFile(Context context, File from, File to) {
        try {
            int byteread = 0;
            // File oldfile = new File(from);
            if (from.exists()) {
                InputStream inStream = new FileInputStream(from);
                FileOutputStream fs = new FileOutputStream(to);
                byte[] buffer = new byte[1444];
                while ((byteread = inStream.read(buffer)) != -1) {
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
                fs.close();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean copyFile1(Context context, byte[] from, File to) {
        try {
            // int byteread = 0;
            // File oldfile = new File(from);
            FileOutputStream fs = new FileOutputStream(to);
            fs.write(from);
            fs.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static Bitmap scaleToActualAspectRatio(String imgFilePath, Bitmap bitmap, Activity context) {
        if (bitmap != null) {
            boolean flag = true;

            int deviceWidth = context.getWindowManager().getDefaultDisplay().getWidth();
            int deviceHeight = context.getWindowManager().getDefaultDisplay().getHeight();

            int bitmapHeight = bitmap.getHeight(); // 563
            int bitmapWidth = bitmap.getWidth(); // 900

            // aSCPECT rATIO IS Always WIDTH x HEIGHT rEMEMMBER 1024 x 768

            if (bitmapWidth > deviceWidth) {
                flag = false;

                // scale According to WIDTH
                int scaledWidth = deviceWidth;
                int scaledHeight = (scaledWidth * bitmapHeight) / bitmapWidth;

                try {
                    if (scaledHeight > deviceHeight)
                        scaledHeight = deviceHeight;

                    bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (flag) {
                if (bitmapHeight > deviceHeight) {
                    // scale According to HEIGHT
                    int scaledHeight = deviceHeight;
                    int scaledWidth = (scaledHeight * bitmapWidth) / bitmapHeight;

                    try {
                        if (scaledWidth > deviceWidth)
                            scaledWidth = deviceWidth;

                        bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            ExifInterface exif = new ExifInterface(imgFilePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            AppLog.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            exif = null;
            System.gc();

            // bitmap = BitmapFactory.decodeStream(new FileInputStream(imgFilePath), null, o);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {

        }
        return bitmap;
    }

    /**
     * Show Alert dialog for server down or error
     *
     * @param context
     * @param message
     * @param title
     */
    public static void showAlert(Context context, String title, String message) {
        DialogUtils.showErrorDialog(context, title != null ? title : context.getResources().getString(R.string.Alert), message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

   /* public static void encrypt(File file) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
        FileInputStream fis = new FileInputStream("data/cleartext");
        // This stream write the encrypted text. This stream will be wrapped by another stream.
        FileOutputStream fos = new FileOutputStream("data/encrypted");

        // Length is 16 byte
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[8];
        while ((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();
    }*/

    /*public static void decrypt() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        FileInputStream fis = new FileInputStream("data/encrypted");

        FileOutputStream fos = new FileOutputStream("data/decrypted");
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[8];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
    }*/

    public static File saveToInternalSorage(Context context, int type, String extention) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = null;
        // if (MEDIA_TYPE_IMAGE == type) {
        directory = cw.getDir("images", Context.MODE_PRIVATE);
        // } else if (MEDIA_TYPE_VIDEO == type) {
        // directory = cw.getDir("video", Context.MODE_PRIVATE);
        // }
        // File directory = cw.getDir(name, mode)
        // Create imageDir

        if (directory == null) {
            return null;
        }

        if (!directory.exists()) {
            try {
                directory.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // File file = new File(directory, System.currentTimeMillis() + "." + extention);
        File file = new File(directory, extention);

        return file;
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory() && dir.exists()) {
            String[] children = dir.list();
            for (int i = 0; children != null && i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    /*
     * input rootFolder - the storage location directoryName(/rajan)- the name of directory fileName - the name of file output myPath(File) - is
     * filepath of encrypted file location
     */
    public static File createFile(File rootFolder, String directoryName, String fileName) {
        File sdCard = rootFolder;
        File dir = new File(sdCard.getAbsolutePath() + directoryName);
        if (!dir.exists())
            dir.mkdirs();
        File myPath = new File(dir, fileName);
        return myPath;
    }

    public static byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /**
     * Extract groupId
     *
     * @param entityID
     * @return
     */
    public static String extractGroupId(String entityID) {
        return entityID.split("@" + VariableConstants.groupConferenceName)[0];
    }

    /*
     * checks network connectivity if available returns true, else return false and shows appropriate error dialog
     *
     * @param context app context to access resources
     *
     * @param isXMPPConnected true if the xmpp connection needs to be checked
     */
    public static boolean showDialogForNoNetwork(Context context, boolean isXMPPConnected) {
        if (isNetworkAvailable(context)) {
            // check for xmpp connection
            if (isXMPPConnected) {
                return showDialogForNoXMPPconnection(context);
            }
            return true;
        } else {
            // show dialog for no network
            showNoNetworkDialog(context);
            return false;
        }

    }

    public static void showNoNetworkDialog(final Context context) {
        DialogUtils.showAlertDialogCommon(context, context.getResources().getString(R.string.network_error_tittle),
                context.getResources().getString(R.string.network_error_message),
                context.getResources().getString(R.string.launch_settings),
                context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Dialog.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;

                            case Dialog.BUTTON_POSITIVE:
                                context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                                dialog.dismiss();
                                break;
                        }
                    }
                });
    }

    public static void showXMPPConnectionErrorDialog(Context context) {
        Resources resources = context.getResources();
        new AlertDialog.Builder(context).setTitle(resources.getString(R.string.xmpp_error_title))
                .setMessage(resources.getString(R.string.xmpp_error_message))
                .setPositiveButton(resources.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // retry the connection
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();

    }

    public static boolean showDialogForNoXMPPconnection(Context context) {
        if (isXMPPConnectionAvailable()) {
            return true;
        } else {
            Resources resources = context.getResources();
            String message = resources.getString(R.string.xmpp_error_message);
            // show dialog for broken Xmpp connection
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public static void showToast(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            System.out.println("Utils.isNetworkAvailable()---null pointer");
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isXMPPConnectionAvailable() {
        ConnectionInfo connectionInfo = ConnectionInfo.getInstance();
        XMPPConnection connection = connectionInfo.getXmppConnection();
        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
            return true;
        } else
            return false;
    }

    public static File encriptBitmap(Context context, Bitmap bitmap, String fileName, String appName) {
        if (bitmap == null) {
            return null;
        }
        String encodeFilePath = Citadel.encodeFile(bitmap, context, appName, fileName, null);
        File encodeFile = new File(encodeFilePath);
        return encodeFile;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        // final int height = options.outHeight;
        // final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static File saveToInternalSorage(Context context, String extention) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = null;
        directory = cw.getDir("pdf", Context.MODE_PRIVATE);
        if (directory == null) {
            return null;
        }

        if (!directory.exists()) {
            try {
                directory.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File file = new File(directory, extention);

        return file;
    }

    public static void deleteExternalStoragePrivatePicture(File file) {
        file.delete();
    }

    /**
     * This is the login method available in the service
     *
     * @param userName
     * @param password
     * @return
     */
    public static boolean logInUser(Context context, String userName, String password) {
        if (LoginConnection.isConnecting) {
            return true;
        }
        UserMO userMO = SettingManager.getInstance().getUserMO();
        if (userMO.getSalutation().equals(TextConstants.NO)) {
            return true;
        }
        new LoginConnection(context, userName, password).execute();
        return true;
    }

    public static void userLoggedinStatus(Context context, boolean isloggedin) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        preferences.edit().putBoolean(VariableConstants.IS_USER_LOGGED_IN, isloggedin).commit();
    }

    public static boolean isUserloggedin(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        return preferences.getBoolean(VariableConstants.IS_USER_LOGGED_IN, false);
    }

    public static void pendingMessageTimerStatus(Context context, boolean isTimerRunning) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        preferences.edit().putBoolean(VariableConstants.IS_PENDING_TIMER_RUNNING, isTimerRunning).commit();
    }

    public static boolean isPendingMessageTimerRunning(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        return preferences.getBoolean(VariableConstants.IS_PENDING_TIMER_RUNNING, false);
    }

    public static void timersRunningStatus(Context context, boolean isTimerShutDown) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        preferences.edit().putBoolean(VariableConstants.IS_TIMER_STOPPED, isTimerShutDown).commit();
    }

    public static boolean isTimerRunning(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        return preferences.getBoolean(VariableConstants.IS_TIMER_STOPPED, false);
    }

    public static boolean checkIfMessengerServiceIsRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals("MessengerService")) {
                return true;
            }
        }
        return false;
    }

    public static String generateChatContactPK(String contactJID) {
        contactJID = Utils.getFisikeUserName(contactJID, SettingManager.getInstance().getSettings().getFisikeServerIp());
        try {
            contactJID = URLDecoder.decode(contactJID, "UTF-8");
            return ("ChatContactMO" + contactJID).hashCode() + "";
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    public static void openSelectPopup(Context context, UserMO userMO, RoundedImageView imgPic, ImageOperationCallBack callback) {
        Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        Intent gallIntent = new Intent(Intent.ACTION_PICK);
        gallIntent.setType("image/*");
        final File photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                System.currentTimeMillis() + ".jpg");

        // look for available intents
        List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);

        if (listCam.size() > 0) {
            ResolveInfo res = listCam.get(0);

            final Intent finalIntent = new Intent(camIntent);
            photoUri.setWritable(true);
            Uri mImageCaptureUri = Uri.fromFile(photoUri);
            finalIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            finalIntent.putExtra("return-data", true);
            finalIntent.putExtra(VariableConstants.RESULT_CODE, TextConstants.CAMERA_IMAGE_INTENT);

            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            yourIntentsList.add(finalIntent);
            info.add(res);
        }
        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            finalIntent.putExtra("return-data", true);
            finalIntent.putExtra(VariableConstants.RESULT_CODE, TextConstants.GALLERY_IMAGE_INTENT);
            yourIntentsList.add(finalIntent);
            info.add(res);
        }

        ResolveInfo res = listGall.get(0);
        Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());

        if (null != bitmap) {
            Intent intent = new Intent();
            intent.putExtra(TextConstants.REMOVE_PIC, true);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, "Remove pic"));
            yourIntentsList.add(intent);
            info.add(res);
        }

        openDialog(yourIntentsList, info, null, TextConstants.SELECT_PHOTO, userMO, context, imgPic, callback, photoUri);
    }

    public static void openDialog(final List<Intent> yourIntentsList, List<ResolveInfo> info, String title, final int returnCode,
                                  final UserMO userMO, final Context context, final RoundedImageView imgPic, final ImageOperationCallBack callback, final File photoUri) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if (title != null) {
            dialog.setTitle(title);
        }
        Bitmap bitmap = UserProfilePic.loadImageFromStorage(MyApplication.getAppContext());

        boolean isToShowRemovePic = (null == bitmap) ? false : true;
        dialog.setAdapter(buildAdapter(context, info, isToShowRemovePic), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = yourIntentsList.get(id);
                if (intent.getExtras().containsKey(TextConstants.REMOVE_PIC)) {
                    if (null == userMO.getProfilePic()) {
                        if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                            imgPic.setImageResource(R.drawable.patient_generic);
                        } else {
                            imgPic.setImageResource(R.drawable.physician_generic);
                        }
                        dialog.dismiss();
                        return;
                    }
                    boolean removePic = intent.getExtras().getBoolean(TextConstants.REMOVE_PIC);
                    if (Utils.showDialogForNoNetwork(context, false)) {
                        if (removePic) {
                            if (userMO.getUserType().equalsIgnoreCase(VariableConstants.PATIENT)) {
                                imgPic.setImageResource(R.drawable.patient_generic);
                            } else {
                                imgPic.setImageResource(R.drawable.physician_generic);
                            }
                            callback.removeProfilePic();
                        }
                    }
                    dialog.dismiss();
                    return;
                }
                int resultCode = intent.getExtras().getInt(VariableConstants.RESULT_CODE);
                callback.updateProfilePic(intent, resultCode, photoUri);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     * Build the list of items to show using the intent_listview_row layout.
     *
     * @param context
     * @param activitiesInfo
     * @param isToShowRemovePic
     * @return
     */
    private static ArrayAdapter<ResolveInfo> buildAdapter(final Context context, final List<ResolveInfo> activitiesInfo,
                                                          final boolean isToShowRemovePic) {
        return new ArrayAdapter<ResolveInfo>(context, R.layout.intent_listview_row, R.id.title, activitiesInfo) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (isToShowRemovePic && position == activitiesInfo.size() - 1) {
                    ImageView image = (ImageView) view.findViewById(R.id.icon);
                    image.setImageResource(R.drawable.ic_pu_rem_pp_pic);
                    CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                    textview.setText(context.getResources().getString(R.string.Remove_profile_picture));
                    return view;
                }
                ResolveInfo res = activitiesInfo.get(position);
                ImageView image = (ImageView) view.findViewById(R.id.icon);
                image.setImageDrawable(res.loadIcon(context.getPackageManager()));
                CustomFontTextView textview = (CustomFontTextView) view.findViewById(R.id.title);
                textview.setText(res.loadLabel(context.getPackageManager()).toString());
                return view;
            }
        };
    }

    public static void sendPresenceAvailable() {
        SettingMO settingMO = SettingManager.getInstance().getSettings();
        UserMO userMO = SettingManager.getInstance().getUserMO();
        Presence presence = null;
        presence = new Presence(Presence.Type.available);
        presence.setStatus("");
        String fisikeServerIp = settingMO.getFisikeServerIp();
        presence.setFrom(userMO.getId() + "@" + fisikeServerIp);
        presence.setTo(fisikeServerIp);
        XMPPConnection connection = ConnectionInfo.getInstance().getXmppConnection();
        connection.sendPacket(presence);
        userMO.setStatus(TextConstants.STATUS_AVAILABLE_TEXT);
        userMO.setStatusMessage("");
        try {
            SettingManager.getInstance().updateUserMO(userMO);
            UserDBAdapter userDBAdapter = UserDBAdapter.getInstance(MyApplication.getAppContext());
            ContentValues values = new ContentValues();
            values.put(userDBAdapter.getSTATUS(), userMO.getStatus());
            values.put(userDBAdapter.getSTATUSMESSAGE(), userMO.getStatusMessage());
            userDBAdapter.updateUserInfo(userMO.getPersistenceKey(), values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showApiError(Context context) {
        Resources resources = context.getResources();
        String message = resources.getString(R.string.xmpp_error_message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String genrateRandomNumber() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int m = cal.get(Calendar.MINUTE);
        int s = cal.get(Calendar.SECOND);
        int ms = cal.get(Calendar.MILLISECOND);
        int v = (m % 10) * 1235 + s * 79 + ms * 982 + 1;
        String format = v + "";
        if (format.length() < 4) {
            return genrateRandomNumber();
        }
        String substring = format.substring(format.length() - 4, format.length());
        return String.format(Locale.getDefault(), "%04d", Integer.parseInt(substring));
    }

    /**
     * Hide keyboard
     */
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * This opens the XMPP Login Connection
     */
   /* public static void loginXMPP(Context context, String userName, String password) {
        Utils.logInUser(context, userName, password);
        new CustomSelectedListTask(userName, password).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
*/
    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
//        return true;
    }


    public static void truncateDB() {
        DBAdapter mHelper;
        mHelper = DBAdapter.getInstance(MyApplication.getAppContext());
        mHelper.truncateDB();
        try {
            SettingManager.getInstance().updateUserMO();
        } catch (Exception e) {
            e.printStackTrace();
        }
        clearSharedPref();

        File file = new File(Environment.getExternalStorageDirectory(), "/" + getCurrentActivityContext().getResources().getString(R.string.app_name));
        if (file != null && file.exists()) {
            Utils.deleteDir(file);
        }
    }

    public static void clearSharedPref() {
        SharedPreferences preferences = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        String countryCode = preferences.getString(VariableConstants.COUNTRY_CODE, "");
        boolean is_mobile_enabled = preferences.getBoolean(VariableConstants.IS_MOBILE_ENABLED, true);
        long otpSendTime = SharedPref.getOTPSentTime();
        boolean isAppTourPassed = preferences.getBoolean(VariableConstants.APP_TOUR_SHOWN, false);
        int numberOfTimeApplicationOpened = preferences.getInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, 0);
        boolean has_rated_app = preferences.getBoolean(VariableConstants.HAS_RATED_APP, false);
        boolean isAdminCreatedPasswordNeverChanged = preferences.getBoolean(VariableConstants.ADMIN_CREATED_PASSWORD_NEVER_CHANGED, false);
        boolean isSideDrawerShown = SharedPref.isSideDrawerShown();
        String languageSelected = SharedPref.getLanguageSelected();
        String languageKey = SharedPref.getLanguageKey();
        String defaultPasswordType = preferences.getString(VariableConstants.DEFAULT_PASSWORD_TYPE, MyApplication.getAppContext().getResources().getString(R.string.Default_Password));
        String passwordRegex = SharedPref.getPasswordRegex();
        long passwordLength = SharedPref.getPasswordLength();
        long passwordHistory = SharedPref.getPasswordHistoryCount();
        Editor edit = preferences.edit();
        edit.clear().commit();
        edit = preferences.edit();
        //edit.putString(VariableConstants.PHONE_NUMBER, phoneNumber);
        //edit.putString(VariableConstants.EMAIL_ADDRESS, emailAddress);
        edit.putBoolean(VariableConstants.IS_MOBILE_ENABLED, is_mobile_enabled);
        edit.putString(VariableConstants.COUNTRY_CODE, countryCode);
        edit.putString(SharedPref.LANGUAGE_SELECTED, languageSelected);
        edit.putString(SharedPref.LANGUAGE_SELECTED_KEY, languageKey);
        edit.putInt(VariableConstants.NUMBER_OF_TIME_APPLICATION_OPEN, numberOfTimeApplicationOpened);
        edit.putBoolean(VariableConstants.APP_TOUR_SHOWN, isAppTourPassed);
        edit.putBoolean(VariableConstants.HAS_RATED_APP, has_rated_app);
        edit.putLong(VariableConstants.OTP_SEND_TIME, otpSendTime);
        edit.putBoolean(VariableConstants.ADMIN_CREATED_PASSWORD_NEVER_CHANGED, isAdminCreatedPasswordNeverChanged);
        edit.putString(VariableConstants.DEFAULT_PASSWORD_TYPE, defaultPasswordType);
        edit.commit();
        SharedPref.setSideDrawerShown(isSideDrawerShown);
        SharedPref.setPasswordRegex(passwordRegex);
        SharedPref.setPasswordHistoryCount(passwordHistory);
        SharedPref.setPasswordLength(passwordLength);
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(false);
        config = ConfigManager.getInstance().updateConfig(config);
    }


    public static boolean compareStartAndEndDate(String startDate, String endDate) {
        try {
            Calendar cal = Calendar.getInstance();// Aug28, 2015

            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());

            Date start = sdf.parse(startDate);
            Date end = sdf.parse(endDate);
            if (endDate != null && start != null && start.compareTo(end) > 0) {
                return false;
            }


        } catch (ParseException e) {
        }
        return true;
    }

    public static boolean compareDateWithPresent(String startDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());

            Date start = sdf.parse(startDate);
            Date end = new Date(System.currentTimeMillis());
            if (start != null && start.compareTo(end) > 0) {
                return false;
            }
        } catch (ParseException e) {
        }
        return true;
    }


    public static String genralDateTime(String date, String inputFormate, String outputFormate) {
        Calendar cal = Calendar.getInstance();// Aug28, 2015
        SimpleDateFormat sdf;
        try {
            sdf = new SimpleDateFormat(inputFormate, Locale.getDefault());

            cal.setTime(sdf.parse(date));
        } catch (Exception e) {
        }

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        sdf = new SimpleDateFormat(outputFormate);
        return sdf.format(cal.getTime());
    }

    public static Calendar calculateDateTime(String mmmddyyyy) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, Locale.getDefault());
        try {
            cal.setTime(sdf.parse(mmmddyyyy));
        } catch (ParseException e) {
        }
        return cal;
    }

    public static Calendar calculateDateTimeNew(String hour, String min, String am_pm) {
        Calendar cal = Calendar.getInstance();
        if (am_pm.toUpperCase().contains("AM") || am_pm.toUpperCase().contains("PM")) {
            cal.set(Calendar.HOUR, Integer.parseInt(hour));
            cal.set(Calendar.MINUTE, Integer.parseInt(min));
            cal.set(Calendar.AM_PM, am_pm.toUpperCase().contains("AM") ? 0 : 1);
        } else {
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
            cal.set(Calendar.MINUTE, Integer.parseInt(min));
        }
        return cal;
    }

    public static String calculateMillis(long dateTime) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, Locale.getDefault());
        cal.setTimeInMillis(dateTime);
        return sdf.format(cal.getTime());
    }

    public static String calculateMillis(long dateTime, String destinationDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(destinationDate, Locale.getDefault());
        cal.setTimeInMillis(dateTime);
        return sdf.format(cal.getTime());
    }


    public static void checkAlternateContactDetails(String alternateContact) {
        if (!Utils.isValueAvailable(alternateContact)) {
            SharedPref.setIsToShowAlternateContactDialog(false);
        } else {
            long days = SharedPref.getNoOfDaysForAlternateContact();
            long session = SharedPref.getNoOfSessionOfDay();
            long diffCalculatedDate = SharedPref.getDiffrenceCalculatedDate() != 0 ? SharedPref.getDiffrenceCalculatedDate() : System.currentTimeMillis();
            long lastDisplayed;
            if (SharedPref.getLastDisplayedAlternateContactDate() != 0)
                lastDisplayed = SharedPref.getLastDisplayedAlternateContactDate();
            else
                lastDisplayed = System.currentTimeMillis();

            Date start_date = new Date(lastDisplayed);
            Date end_date = new Date(System.currentTimeMillis());
            Date diffCalCulatedDate = new Date(diffCalculatedDate);
            long diff = DateTimeUtil.daysBetween(start_date, end_date);
            long days_diff = DateTimeUtil.daysBetween(diffCalCulatedDate, end_date);
            if (diff > 0) {
                if (days_diff > 0) {
                    days = days + 1;
                    session = 1;
                } else {
                    session = session + 1;
                }
            } else {
                session = session + 1;
                if (SharedPref.getLastDisplayedAlternateContactDate() == 0)
                    days = 1;

            }

            ConfigMO config = ConfigManager.getInstance().getConfig();
            if ((((days) % (config.getNoOfDaysForAlternateContact() + 1)) == 0) && (session == TextConstants.SESSION_DIFF)) {
                SharedPref.setIsToShowAlternateContactDialog(true);

            } else
                SharedPref.setIsToShowAlternateContactDialog(false);

            SharedPref.setNoOfDaysForAlternateContact(days);
            SharedPref.setNoOfSessionOfDay(session);
            SharedPref.setDiffrenceCalculatedDate(System.currentTimeMillis());
            if (SharedPref.getLastDisplayedAlternateContactDate() == 0)
                SharedPref.setLastDisplayedAlternateContactDate(lastDisplayed);

        }
    }

    public static void startAlarmMedication(Context context, int alarmManagerUniqueKey, PrescriptionModel prescriptionModel, int interval, MedicineDoseFrequencyModel arrayDose) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(context, NotifyAlarm.class);
        myIntent.putExtra(VariableConstants.MEDICATION_PRESCRIPTION_ID, prescriptionModel.getID());
        myIntent.putExtra(VariableConstants.DOSE_INSTRUCTION_CODE, alarmManagerUniqueKey);
        myIntent.putExtra(VariableConstants.INTERVAL, interval);
        myIntent.putExtra(VariableConstants.REPEAT_TIME, prescriptionModel.getStartDate() + " " + arrayDose.getDoseHours() + ":" + arrayDose.getDoseMinutes() + " " + arrayDose.getDoseTimeAMorPM());
        myIntent.putExtra(VariableConstants.END_DATE, prescriptionModel.getEndDate());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmManagerUniqueKey, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Utils.calculateDateTimeNew(arrayDose.getDoseHours(), arrayDose.getDoseMinutes(), arrayDose.getDoseTimeAMorPM());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval * AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }


    public static void startAlarmVaccination(Context context, VaccinationModel vaccinationModel, long triggerAtMillis) {

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(context, VaccinationAlarm.class);
        myIntent.putExtra(VariableConstants.VACCINATION_MODEL, vaccinationModel);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, vaccinationModel.getReminderUniqueKey(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
    }

    public static void cancelAlarm(Context context, int requestCode) {
        Intent myIntent = new Intent(context, NotifyAlarm.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    public static void cancelVaccinationAlarm(Context context, int requestCode) {
        Intent myIntent = new Intent(context, VaccinationAlarm.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    /**
     * @param endDate
     * @param startDate
     * @return
     */
    public static String calculateDateInWeeks(String endDate, String startDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        if (BuildConfig.isPatientApp && !isValidFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, startDate)) {
            endDate = Utils.genralDateTime(endDate, DateTimeUtil.mm_dd_yyyy_HiphenSeperated, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        }
        Calendar startCal = Calendar.getInstance();
        try {
            startCal.setTime(sdf.parse(startDate));
        } catch (ParseException e) {
        }

        Calendar endCal = Calendar.getInstance();
        try {
            endCal.setTime(sdf.parse(endDate));
        } catch (ParseException e) {
        }
        if (startCal == null || endCal == null)
            return "";


        int noOfDays = endCal.get(Calendar.YEAR) - startCal.get(Calendar.YEAR);
        if (startCal.get(Calendar.MONTH) > endCal.get(Calendar.MONTH) ||
                (startCal.get(Calendar.MONTH) == endCal.get(Calendar.MONTH) && startCal.get(Calendar.DATE) > endCal.get(Calendar.DATE))) {
            noOfDays--;
        }

        if (noOfDays >= 20)
            return "Forever";

        else
            return MyApplication.getAppContext().getResources().getString(R.string.Specific_Dates);
    }

    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    public static String formatQuantity(double d, int i) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else {
            if (i > -1) {
                DecimalFormat df = new DecimalFormat("0.0");
                return df.format(d);
            }
            return String.format("%s", d);
        }
    }

    public static String calculateUnitToDisplay(String units, boolean isPlural) {


        Context context = MyApplication.getAppContext();
        if (units.equalsIgnoreCase(context.getString(R.string.conditional_spray_puffs)))
            units = context.getString(R.string.conditional_spray_puff);
        else if (units.equalsIgnoreCase(context.getString(R.string.conditional_inhaler_puffs)))
            units = context.getString(R.string.conditional_inhaler_puff);
        else if (units.equalsIgnoreCase(context.getString(R.string.conditional_drops_count)))
            units = context.getString(R.string.conditional_drop_count);
        else if (units.equalsIgnoreCase(context.getString(R.string.conditional_drops_ml)))
            units = context.getString(R.string.conditional_drop_ml);

        String plural_succesor = context.getString(R.string.plural_string);
        MedicationUnitsEnum unitEnum = MedicationUnitsEnum.getEnumInstanceBasedOnObject(units);
        units = MedicationUnitsEnum.getDisplayedValuefromCode(units);
        String tempUnits = units;
        String splitString[] = tempUnits.split("\\(");
        String typeName = splitString[0].trim();
        String unitName = new String();
        if (splitString.length > 1)
            unitName = (splitString[1].trim().split("\\)"))[0];
        String splitUnit = "";


        if (unitEnum != null && unitEnum.getIsCount() == 1) {
            splitUnit = typeName.trim();
            if (MedicationUnitsEnum.getUnitIsPlural(MedicationUnitsEnum.getCodeFromDisplayedValue(units)) == 1 && isPlural)
                splitUnit = splitUnit + plural_succesor;
        } else if (unitEnum != null && unitEnum.getIsCount() == 0) {
            splitUnit = unitName.trim();
            if (unitEnum.getIsPlural() == 1 && isPlural)
                splitUnit = splitUnit + plural_succesor;
        } else {
            if (typeName.equalsIgnoreCase(context.getString(R.string.unit_powder)))
                units = context.getString(R.string.unit_gram);
            else if (typeName.equalsIgnoreCase(context.getString(R.string.unit_granule)) ||
                    typeName.equalsIgnoreCase(context.getString(R.string.unit_granules)))
                units = context.getString(R.string.unit_granule);
            if (isPlural)
                splitUnit = units + plural_succesor;
            else
                splitUnit = units;
        }
/*
        String capitalizedString = Utils.capitalizeFirstLetter(splitUnit);
        return capitalizedString;
*/
        String smallSizedString = Utils.smallSizeFirstLetter(splitUnit);
        return smallSizedString;
    }


    public static void makeMaskImage(Context context, ImageView mImageView, int defaultImage, int maskImage, int frameImage, Bitmap resizedBitmap,
                                     int Widthdp, int Heightdp) {

        int widthpx = (int) convertDpToPixel(Widthdp, context);
        int heightpx = (int) convertDpToPixel(Heightdp, context);
        Bitmap mframe = resizeBitmap(BitmapFactory.decodeResource(context.getResources(), frameImage), widthpx, heightpx);
        Bitmap original;
        if (resizedBitmap != null)
            original = resizeBitmap(resizedBitmap, widthpx, heightpx);
        else
            original = resizeBitmap(BitmapFactory.decodeResource(context.getResources(), defaultImage), widthpx, heightpx);
        ;
        Bitmap mask = resizeBitmap(BitmapFactory.decodeResource(context.getResources(), maskImage), widthpx, heightpx);
        Bitmap result = Bitmap.createBitmap(widthpx, heightpx, Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        mCanvas.drawBitmap(mframe, 0, 0, null);
        mImageView.setImageBitmap(result);
        mImageView.setScaleType(ScaleType.CENTER);

    }

    private static Bitmap resizeBitmap(Bitmap Image, int Width, int Height) {
        Bitmap resized = Bitmap.createScaledBitmap(Image, Width, Height, false);
        return resized;
    }

    public static void makeMaskImage(Context context, ImageView mImageView, int actualImageId, int maskId, Bitmap resizedBitmap, int widthdp,
                                     int heightdp) {
        int width = (int) convertDpToPixel(widthdp, context);
        int height = (int) convertDpToPixel(heightdp, context);

        Bitmap actualImage;
        if (resizedBitmap != null)
            actualImage = resizeBitmap(resizedBitmap, width, height);
        else
            actualImage = resizeBitmap(BitmapFactory.decodeResource(context.getResources(), actualImageId), width, height);
        Bitmap maskImage = resizeBitmap(BitmapFactory.decodeResource(context.getResources(), maskId), width, height);
        Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(actualImage, 0, 0, null);
        mCanvas.drawBitmap(maskImage, 0, 0, paint);
        mImageView.setImageBitmap(result);
        mImageView.setScaleType(ScaleType.CENTER);

		/*
         * if (actualImage != null) { actualImage.recycle(); actualImage = null; } if (maskImage != null) { maskImage.recycle(); maskImage = null; }
		 */
    }

    public static String calculateEndDate(Context context, String startDate, String endDateFrequency) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        Calendar cal = Calendar.getInstance();
        try {
            if (startDate != null)
                cal.setTime(sdf.parse(startDate));
        } catch (ParseException e) {
        }
        if (endDateFrequency.equals("1 week")) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE) + 7);
        } else if (endDateFrequency.equals("1 month")) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
        } else if (endDateFrequency.equals("1 year")) {
            cal.set(cal.get(Calendar.YEAR) + 1, cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        } else if (endDateFrequency.equals(MedicationDurationEnum.FOREVER.getCode())) {
            cal.set(cal.get(Calendar.YEAR) + 20, cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        }
        return sdf.format(cal.getTime());
    }

    // calculates the date numberofDays after currentDate in "yyyy-MM-dd" format
    public static String calculateDateAfterNumberofDaysFromStartDate(String startDate, int numberOfDays) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
        Calendar cal = Calendar.getInstance();
        try {
            if (startDate != null)
                cal.setTime(sdf.parse(startDate));
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE) + numberOfDays);
        return sdf.format(cal.getTime());
    }

    public static String calculateMedicationEndDate(String startDate, String endDateFrequency) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(startDate));
        } catch (ParseException e) {
        }
        if (endDateFrequency.equals("1 week")) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE) + 7);
        } else if (endDateFrequency.equals("1 month")) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
        } else if (endDateFrequency.equals("1 year")) {
            cal.set(cal.get(Calendar.YEAR) + 1, cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        } else if (endDateFrequency.equals(MedicationDurationEnum.FOREVER.getCode())) {
            cal.set(cal.get(Calendar.YEAR) + 100, cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        }
        return sdf.format(cal.getTime());
    }


    public static String capitalizeFirstLetter(String input) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static String smallSizeFirstLetter(String input) {
        return input.substring(0, 1).toLowerCase() + input.substring(1);
    }

    /**
     * This method is to get the UUID of the current device
     *
     * @return
     */
    public static String getDeviceUUid(Context context) {

        String deviceUID;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        deviceUID = deviceUuid.toString();
        SharedPref.setDeviceUid(deviceUID);
        return deviceUID;

    }

    public static int getPatientId() {
        return (int) SettingManager.getInstance().getUserMO().getPatientId();
    }


    public static String toCamelCase(String s) {
        if (s == null) {
            return "";
        }

        s = s.replaceAll("\\s+", " ");
        if (s.trim().equals(""))
            return "";

        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts) {
            camelCaseString = camelCaseString + " " + toProperCase(part);
        }
        return camelCaseString;
    }

    static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static long getMuteChatTime(int hours, int day, int week, int year) {
        Date muteTilldate = null;
        long muteChatTimeInMillis = 0;

        try {
            String date;
            SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.MM_dd_yyyy_HH_mm_ss_Hiphen_Seperated);
            String currentDateandTime = sdf.format(new Date());

            muteTilldate = sdf.parse(currentDateandTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(muteTilldate);
            calendar.add(Calendar.HOUR_OF_DAY, hours);
            calendar.add(Calendar.DAY_OF_YEAR, day);
            calendar.add(Calendar.DAY_OF_YEAR, week);
            calendar.add(Calendar.YEAR, year);
            muteChatTimeInMillis = calendar.getTimeInMillis();
        } catch (Exception e) {

        }
        return muteChatTimeInMillis;

    }

    /**
     * Get group jid
     *
     * @param jid
     * @param isConversationCheck
     * @return
     */
    private static String fisikeGroupJid(String jid, boolean isConversationCheck) {
        String[] split = jid.split("_");
        if (split != null && split.length > 3) {
            return isConversationCheck ? split[0] + "_" + split[1] + "_" + split[2] : split[0] + "_" + split[2] + "_" + split[1];
        }
        return null;
    }

    /**
     * Group conversation key if exist
     *
     * @param jid
     * @param conversations
     * @return
     */
//    public static String checkGroupJid(String jid, Map<String, ChatConversationMO> conversations) {
//        String fisikeGroupJid = fisikeGroupJid(jid, false);
//        Collection<ChatConversationMO> values = conversations.values();
//        Iterator<ChatConversationMO> iterator = values.iterator();
//        while (iterator != null && iterator.hasNext()) {
//            ChatConversationMO next = iterator.next();
//            String conversationGroupJid = fisikeGroupJid(fisikeGroupJid, true);
//            if (fisikeGroupJid.equals(conversationGroupJid)) {
//                return next.getPersistenceKey() + "";
//            }
//        }
//        return null;
//    }
    public static String checkGroupJid(String jid, Map<String, ChatConversationMO> conversations) {
        String persistanceKey = ChatConversationMO.generatePersistenceKey(jid, (SettingManager.getInstance().getUserMO().getId() + "")) + "";
        Collection<ChatConversationMO> values = conversations.values();
        Iterator<ChatConversationMO> iterator = values.iterator();
        while (iterator != null && iterator.hasNext()) {
            ChatConversationMO next = iterator.next();

            if (persistanceKey.equals(next.getPersistenceKey())) {
                return next.getPersistenceKey() + "";
            }
        }
        return null;
    }


    public static boolean isKeyBoardOpen(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPasswordMatch(String s) {
        String pattern = ConfigManager.getInstance().getConfig().getPasswordRegex();
        try {
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    public static String validatePassword(String password, Context context) {

     /*    String alphabet_regex = ".*[a-zA-Z].*";
        Pattern alphabetPattern = Pattern.compile(alphabet_regex);
        String number_regex = ".*[0-9].*";
        Pattern numberPattern = Pattern.compile(number_regex);
        String special_char_regex = ".*[~`!@#$%^&*(-)_={}+\\|;:'\"<,.>/?].*";
        String characterSet = null;
        if(SharedPref.getPasswordRegex() != null)
        characterSet= ".*"+SharedPref.getPasswordRegex()+".*";
        Pattern specialCharPattern = Pattern.compile(characterSet);*/
        if (password.length() < SharedPref.getPasswordLength())
            return context.getResources().getString(R.string.password_length_error, SharedPref.getPasswordLength());
 /*       else if (!alphabetPattern.matcher(password).matches())
            return TextConstants.PASSWORD_ALPHANUMERIC_ERROR;
        else if (!numberPattern.matcher(password).matches())
            return TextConstants.PASSWORD_ALPHANUMERIC_ERROR;
        else if (characterSet!= null && specialCharPattern.matcher(password).matches())
            return TextConstants.PASSWORD_SPECIAL_CHARACTER_ERROR;
*/
        else
            return "";
    }

    public static void disableSpecialChar(final EditText editText) {
        final String blockCharacterSet = "[~` ]";
        String allowed_char_regex = ".*[a-zA-Z0-9Ã!@#$%^&*()][_+={}|Â¥:;â€™<,>.?/â€œâ€].*";
        final Pattern allowedCharPattern = Pattern.compile(allowed_char_regex);


        InputFilter filter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (source != null && allowedCharPattern.matcher(source).matches()) {
                    return "";
                }
//                else
//                    Toast.makeText(editText.getContext(), "This Character is not allowed as per password policy", Toast.LENGTH_LONG).show();
                return null;
            }
        };

        editText.setFilters(new InputFilter[]{filter});

    }


    /**
     * Genrate secoundryId
     *
     * @param groupId
     * @return
     */
    public static String genrateSecoundryId(String groupId) {
        if (groupId == null) {
            return groupId;
        }
        String[] splitGroupId = groupId.split(TextConstants.UNDERSCORE);
        if (splitGroupId.length < 2) {
            return groupId;
        }
        String id = groupId.split(TextConstants.UNDERSCORE)[1];
        String userId = SettingManager.getInstance().getUserMO().getId() + "";
        String serverip = "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
        return id.equals(userId) ? groupId.split(TextConstants.UNDERSCORE)[2] + serverip : id + serverip;
    }

    /**
     * Genrate secoundryId
     *
     * @param contactId
     * @return
     */
    public static String genrateSecoundryIdFromContact(String contactId) {
        String serverip = "@" + SettingManager.getInstance().getSettings().getFisikeServerIp();
        if (contactId.contains(serverip)) {
            return contactId;
        }
        return contactId + serverip;

    }

    public static void setUserLoggedOut(boolean value) {
        UserMO user = SettingManager.getInstance().getUserMO();
        if (user != null) {
            if (value)
                user.setSalutation(TextConstants.NO);
            else
                user.setSalutation(TextConstants.YES);

            try {
                SettingManager.getInstance().updateUserMO(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getAdminId(String jid) {
        if (jid.startsWith("single")) {
            String array[] = jid.split("_");
            return array[1];
        }

        return "";
    }

    public static String getUserSelectedLanguage(Context context) {
        return SharedPref.getLanguageKey();
    }

    private void cleanSharedPref() {
        SharedPreferences preferences = MyApplication.getAppContext().getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);


        Intent launchIntent = null;
        String phoneNumber = SharedPref.getMobileNumber();
        String countryCode = preferences.getString(VariableConstants.COUNTRY_CODE, "");
        String otpSavedCode = preferences.getString(VariableConstants.OTP_CODE, "");
        long otp_send_time = preferences.getLong(VariableConstants.OTP_SEND_TIME, 0);
        boolean isOTPVerified = preferences.getBoolean(VariableConstants.HAS_VERIFIED_OTP, false);
        boolean isUserExist = preferences.getBoolean(VariableConstants.USER_EXIST, false);
        boolean isSignUpScreenPassed = preferences.getBoolean(VariableConstants.IS_SIGNUP_SCREEN_PASSED, false);
        boolean isAppTourPassed = preferences.getBoolean(VariableConstants.APP_TOUR_SHOWN, false);
        boolean isDrawerDemoShown = SharedPref.isSideDrawerShown();
        Editor edit = preferences.edit();
        edit.clear().commit();
        edit = preferences.edit();
        edit.putString(VariableConstants.COUNTRY_CODE, countryCode);
        edit.putString(VariableConstants.OTP_CODE, otpSavedCode);
        edit.putLong(VariableConstants.OTP_SEND_TIME, otp_send_time);
        edit.putBoolean(VariableConstants.HAS_VERIFIED_OTP, isOTPVerified);
        edit.putBoolean(VariableConstants.USER_EXIST, isUserExist);
        edit.putBoolean(VariableConstants.IS_SIGNUP_SCREEN_PASSED, isSignUpScreenPassed);
        edit.putBoolean(VariableConstants.APP_TOUR_SHOWN, isAppTourPassed);
        edit.commit();
        SharedPref.setSideDrawerShown(isDrawerDemoShown);
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(false);
        config = ConfigManager.getInstance().updateConfig(config);

    }

    public static String getPateintIdFromJid(String jid) {
        if (jid == null) return "";
        if (jid.startsWith("single")) {

            String array[] = jid.split("_");

            return array[2];
        }
        return "";
    }

    public static void removeMpin(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        Editor edit = preferences.edit();
        edit.putInt(VariableConstants.INCORRECT_PIN, 0);
        edit.remove(VariableConstants.LOCK_PIN_CODE);
        edit.remove(VariableConstants.MPIN_SET);
        edit.commit();
        ConfigMO config = ConfigManager.getInstance().getConfig();
        config.setShowPin(false);
        config = ConfigManager.getInstance().updateConfig(config);
    }


    /*converts date  to long
    parameters date and the date format*/
    public static long convertDateToLong(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        GregorianCalendar cal = new GregorianCalendar();
        try {
            cal.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.getTimeInMillis();
    }

    public static Bitmap getBitmapFromUri(Context context, Uri uri, int reqWidth, int reqHeight) {
        try {
            InputStream bitmapInputStream = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(bitmapInputStream);
            return resizeBitmap(bitmap, reqWidth, reqHeight);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static int calculateAge(String dobDate) {
        String month[] = MyApplication.getAppContext().getResources().getStringArray(R.array.month_arr);
        ArrayList<String> mon = new ArrayList<String>(Arrays.asList(month));
        String[] date = dobDate.trim().split(" ");

        int d = Integer.parseInt(date[0]);
        int index = mon.indexOf(date[1].toLowerCase().toString());
        int m = index + 1;
        int y = Integer.parseInt(date[2]);
        ;
        int age = getAge(y, m, d);
        return age;
    }

    public static int getAge(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;
        try {

            y = cal.get(Calendar.YEAR);
            m = cal.get(Calendar.MONTH) + 1;
            d = cal.get(Calendar.DAY_OF_MONTH);
            cal.set(year, month, day);
            a = y - year;
            if ((m < month)
                    || ((m == month) && (d < day))) {
                --a;
            }
        } catch (Exception e) {
            e.printStackTrace();
            a = 0;
        }
        return a;
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static String[] getMedicineDetails(String text) {
        String[] timming = new String[4];
        if (text.contains("BB")) {
            timming[0] = "8";
            timming[1] = "00";
            timming[2] = "AM";
            timming[3] = "before food";
        } else if (text.contains("AB")) {
            timming[0] = "8";
            timming[1] = "30";
            timming[2] = "AM";
            timming[3] = "after food";
        } else if (text.contains("BL")) {
            timming[0] = "1";
            timming[1] = "00";
            timming[2] = "PM";
            timming[3] = "before food";
        } else if (text.contains("AL")) {
            timming[0] = "1";
            timming[1] = "30";
            timming[2] = "PM";
            timming[3] = "after food";
        } else if (text.contains("BD")) {
            timming[0] = "8";
            timming[1] = "00";
            timming[2] = "PM";
            timming[3] = "before food";
        } else if (text.contains("AD")) {
            timming[0] = "8";
            timming[1] = "30";
            timming[2] = "PM";
            timming[3] = "after food";
        }
        return timming;
    }

    public static String[] getMedicineTiming(String text) {
        String[] timming = new String[4];
        String time = text.split(",")[0].trim();
        timming[0] = time.split(":")[0]; //hrs
        timming[1] = time.split(":")[1].trim().split(" ")[0]; //mins
        timming[1] = timming[1].length() == 1 ? "0" + timming[1] : timming[1];
        timming[2] = time.split(":")[1].trim().split(" ")[1]; //AM or PM
        if (text.split(",").length > 1) {
            timming[3] = text.split(",")[1].trim();
        } else
            timming[3] = " ";


        return timming;
    }

    public static String[] getMedicineTimingIn24Hr(String text) {// 8: 00,After Food
        if (text.contains(",")) {
            String[] timming = new String[2];
            timming = text.split(",");
            return timming;
        }
        return null;
    }

    public static String getUserType() {
        if (BuildConfig.isPatientApp)
            return TextConstants.PATIENT_CAPS;
        else
            return TextConstants.PHYSICIAN;
    }

    public static boolean validateMail(String email) {
        String reg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,4})$";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(email);
        if (m.matches())
            return true;
        else
            return false;
    }

    public static Bitmap blur(Context context, Bitmap image, float blur_radius) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            ScriptIntrinsicBlur theIntrinsic = null;
            theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(blur_radius);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);
        }

        return outputBitmap;
//        return image;
    }


    public static String calculateAgeAsPerConstraints(String date, String decesedDate, String sourceFormat, boolean isOnlyToReturnYear) throws ParseException {
        String age = null;
        String newDate = DateTimeUtil.convertSourceDestinationDate(date, sourceFormat, DateTimeUtil.MM_dd_yyyy_HH_mm_ss_Hiphen_Seperated);
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.MM_dd_yyyy_HH_mm_ss_Hiphen_Seperated, Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(newDate));
        System.out.println(cal.toString());
        Calendar decesedCal = Calendar.getInstance();
        if (decesedDate != null && !decesedDate.equals("")) {
            decesedCal.setTime(sdf.parse(DateTimeUtil.convertSourceDestinationDate(decesedDate, sourceFormat, DateTimeUtil.MM_dd_yyyy_HH_mm_ss_Hiphen_Seperated)));
        }
        int ageInYears = calculateYears(cal, decesedCal);

        int ageInDays;
        if (isOnlyToReturnYear || ageInYears >= 18)
            age = ageInYears + "" + Year;
        else if (ageInYears < 18 && ageInYears >= 2)
            age = calculateAgeInYearAndMonths(cal, decesedCal);
        else if (ageInYears >= 1 && ageInYears < 2)
            age = calculateAgeInMonthAndDay(cal, decesedCal);
        else {
            ageInDays = calculateDays(cal, decesedCal);
            if (ageInYears < 1 && ((ageInDays / 7) >= 4))
                age = (ageInDays / 7) + week + ((ageInDays % 7) != 0 ? " " + (ageInDays % 7) + day : "");
            else if (ageInDays >= 2 && ((ageInDays / 7) < 4))
                age = ageInDays + day;
                // new age formatting rules - changed by Aastha
            else if (ageInDays < 2) {
                age = ageInDays + day;
            }
        }
        return age;
    }

    private static int calculateDays(Calendar cal, Calendar eDate) {
        // TODO Auto-generated method stub
        Calendar sDate = cal;
        int daysBetween = 0;
        if (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            while (sDate.before(eDate)) {
                sDate.add(Calendar.DAY_OF_MONTH, 1);
                daysBetween++;
            }
        }

        return (daysBetween < 0 ? daysBetween * -1 : daysBetween);
    }

    private static String calculateAgeInMonthAndDay(Calendar dob, Calendar currentDate) {
        int monthdiff = -1;
        Calendar tempMonth = dob;
        int months = 0;
        while (tempMonth.getTimeInMillis() < currentDate.getTimeInMillis()) {
            monthdiff = monthdiff + 1;
            tempMonth.add(Calendar.MONTH, 1);
        }

        int days_diff = calculateDaysInBetweenDates(dob);
        int days = days_diff;  /*((currentDate.get(Calendar.DAY_OF_MONTH) >= dob.get(Calendar.DAY_OF_MONTH)) ? (dob.get(Calendar.DAY_OF_MONTH) - currentDate.get(Calendar.DAY_OF_MONTH)) : currentDate.get(Calendar.DAY_OF_MONTH))*/
        ;
        return monthdiff + month + (days != 0 ? (" " + days + day) : "");
    }


    private static int calculateDaysInBetweenDates(Calendar dob) {
        Calendar currentDate = Calendar.getInstance();
        Calendar tempDate;
        tempDate = Calendar.getInstance();

        if (currentDate.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
            tempDate.set(Calendar.DAY_OF_MONTH, dob.get(Calendar.DAY_OF_MONTH));
            tempDate.add(Calendar.MONTH, -1);

        } else if (currentDate.get(Calendar.DAY_OF_MONTH) > dob.get(Calendar.DAY_OF_MONTH)) {
            tempDate = Calendar.getInstance();
            tempDate.set(Calendar.DAY_OF_MONTH, dob.get(Calendar.DAY_OF_MONTH));
        } else
            tempDate = currentDate;

        int day_diff = 0;
        while (tempDate.getTimeInMillis() < currentDate.getTimeInMillis()) {
            day_diff = day_diff + 1;
            tempDate.add(Calendar.DAY_OF_MONTH, 1);
        }
        return (day_diff < 0 ? day_diff * -1 : day_diff);
    }

    private static String calculateAgeInYearAndMonths(Calendar dob, Calendar currentDate) {
        int year = currentDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if ((currentDate.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) || ((currentDate.get(Calendar.MONTH) == dob.get(Calendar.MONTH)) && (currentDate.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH))))
            year = year - 1;

        int months = currentDate.get(Calendar.MONTH) - dob.get(Calendar.MONTH);
        months = months < 0 ? 12 + months : months;
        int days = currentDate.get(Calendar.DATE) - dob.get(Calendar.DATE);
        months = days > 15 || days < 0 ? months - 1 : months;
        return (year + Year + " " + (months != 0 ? (months + month) : ""));

    }

    public static int calculateYears(Calendar dob, Calendar currentDate) {
        int year = currentDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if ((currentDate.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) || ((currentDate.get(Calendar.MONTH) == dob.get(Calendar.MONTH)) && (currentDate.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH))))
            year = year - 1;


        return year;
    }

    public static Bitmap getCurvedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap scaleBitmapAndKeepRation(Bitmap TargetBmp, int reqHeightInPixels, int reqWidthInPixels) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, TargetBmp.getWidth(), TargetBmp.getHeight()), new RectF(0, 0, reqWidthInPixels, reqHeightInPixels), Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(TargetBmp, 0, 0, TargetBmp.getWidth(), TargetBmp.getHeight(), m, true);
        return scaledBitmap;
    }


    public static boolean checkToDisplayPINScreen(UserMO userMO) {
        boolean hasToDisplayLockScreen;
        long inActiveTime = ((System.currentTimeMillis() - SharedPref.getLastInteractionTime()) / 1000);
        //checking logged out
        if (userMO == null || (userMO != null && userMO.getSalutation() != null && userMO.getSalutation().equals(TextConstants.NO)) || SharedPref.getAccessToken() == null)
            return false;
        //checking mPIN is disabled and password pin lock is not enabled
        if (((userMO.getmPIN() == null && BuildConfig.isPasswordPinLockEnabled == false)))
            return false;
        else {
            ConfigMO configMO = ConfigManager.getInstance().getConfig();
            if (!Utils.checkIsToLogoutUser() && (SharedPref.getLastInteractionTime() != 0 && (inActiveTime >= (configMO.getPinLockTime() * 60))))
                hasToDisplayLockScreen = true;
            else
                hasToDisplayLockScreen = false;
            return hasToDisplayLockScreen;
        }
    }

    public static boolean checkTopActivity(Context context, String className) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            String packageName = context.getPackageName();
            if (packageName == null) {
                return false;
            } else if (topActivity.getPackageName().equals(packageName) && topActivity.getShortClassName().equals(className)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }


    public static String createMyInfo(UserMO userMo) {
        String myInfo = "";
        myInfo = Utils.getUserName();
        if (userMo != null && SharedPref.getAccessToken() != null) {
            myInfo = myInfo + (userMo.getFirstName() != null ? " | " + userMo.getFirstName() : "");
            myInfo = myInfo + (userMo.getLastName() != null ? " | " + userMo.getLastName() : "");
        }
        myInfo = myInfo + " | " + BuildConfig.VERSION_NAME;
        myInfo = myInfo + " | " + getDeviceName();
        myInfo = myInfo + " | " + Build.VERSION.SDK_INT;
        return myInfo;
    }

    public static String getEmailBodyFooterInfo(Context context, UserMO userMO) {
        return context.getString(R.string.mail_footer, Utils.createMyInfo(userMO),
                (userMO != null && SharedPref.getAccessToken() != null ? userMO.getUserFullName() : ""));
    }

    public static void resetLoginAttributes() {
        SharedPref.setOldMobileEnabled(SharedPref.getIsMobileEnabled());

        if (BuildConfig.isUserNameEnabled) {
            SharedPref.setUserNameEnabled(true);
            SharedPref.setEmailAndMobile(false);
            SharedPref.setMobileEnabled(false);
        } else if (BuildConfig.isPhoneEnabled && BuildConfig.isEmailEnabled) {
            SharedPref.setEmailAndMobile(true);
            SharedPref.setMobileEnabled(false);
        } else if (BuildConfig.isPhoneEnabled && !BuildConfig.isEmailEnabled) {
            SharedPref.setMobileEnabled(true);
            SharedPref.setEmailAndMobile(false);
        } else if (!BuildConfig.isPhoneEnabled && BuildConfig.isEmailEnabled) {
            SharedPref.setMobileEnabled(false);
            SharedPref.setEmailAndMobile(false);
        }

    }


    public static String generateUniqueKey(Context context) {
        String android_id = android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        AppLog.d("navya", "navya" + android_id.hashCode()+ "");
        return android_id.hashCode() + "";
    }

    /*  returns false if the value is null or empty after trimming
        return true otherwise*/
    public static boolean isValueAvailable(String value) {
        if (value == null || value.equals(MyApplication.getAppContext().getResources().getString(R.string.txt_null))) {
            return false;
        } else if (value.trim().equals("")) {
            return false;
        }
        return true;
    }

    // clears the local directory, used to store encrypted data in sd card
    public static void deleteFileORDirectory(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteFileORDirectory(child);

        fileOrDirectory.delete();
    }

    /*stores the  context of current active activity
    called from onCreate of activity*/
    public static void setCurrentActivityContext(Context currentActivityContext) {
        Utils.currentActivityContext = currentActivityContext;
    }


    /*returns the context of current active activity*/
    public static Context getCurrentActivityContext() {
        return currentActivityContext;
    }

    public static void restrictScreenShot(Activity activity) {
        if (BuildConfig.isRestrictScreenShot) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }


    public static void UpdateSelectItemFromNavigationDrawer(boolean isToSelect, int position) {
        SharedPref.setIsToSelectItem(isToSelect);
        SharedPref.setPositionToSelect(position);
    }

    public static int checkOtpSentDevices(String mobileNumber, String email) {
        boolean isEmail = false;
        boolean isMobile = false;
        if (Utils.isValueAvailable(mobileNumber)) {
            //number = mobileNumber;
            isMobile = true;
        }
        if (Utils.isValueAvailable(email)) {
            //number = email;
            isEmail = true;
        }
        return (isEmail && isMobile ? TextConstants.OTP_SENT_ON_EMAIL_AND_MOBILE : (isMobile ? TextConstants.OTP_SENT_ON_MOBILE_ONLY : (isEmail ? TextConstants.OTP_SENT_ON_EMAIL_ONLY : 2)));
    }

    public static String removeSpace(String unit) {
        String trimUnit = unit.trim();
        String removeSpace = trimUnit.replaceAll(" \\(", "(");
        return smallSizeFirstLetter(removeSpace);
    }

    public static ArrayList<String> convertStringArrayToList(String[] arr, String startChar, String endChar) {
        if (arr.length == 0)
            return null;
        ArrayList<String> stringArrayList = null;
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                stringArrayList = new ArrayList<>();
                arr[i] = arr[i].replace(startChar, "");
            }
            if (i == arr.length - 1)
                arr[i] = arr[i].replace(endChar, "");

            stringArrayList.add(arr[i]);
        }
        return
                stringArrayList;
    }


    public static char[] generatePasswordValidationString() {
        String passwordString = "";
        if (isValueAvailable(SharedPref.getPasswordRegex())) {
            String[] pass = SharedPref.getPasswordRegex().replaceAll("\"", "").split(",");
            ArrayList<String> listArray = convertStringArrayToList(pass, "[", "]");
            if (listArray == null || listArray.size() == 0)
                return TextConstants.passwordAllowedChars;
            else {
                for (String list : listArray) {
                    if (list.contains("A-Z")) {
                        passwordString = passwordString + "" + TextConstants.CHARACTER_SET_CAPITAL;
                    } else if (list.contains("a-z")) {
                        passwordString = passwordString + "" + TextConstants.CHARACTER_SET_SMALL;
                    } else if (list.contains("0-9")) {
                        passwordString = passwordString + "" + TextConstants.CHARACTER_SET_DIGITS;
                    } else
                        passwordString = passwordString + "" + list;
                }
                AppLog.showInfo("Password character ", passwordString);
                return passwordString.toCharArray();
            }
        }
        return TextConstants.passwordAllowedChars;

    }


    public static String getUserName() {

        String key = "";
        if (SharedPref.getIsUserNameEnabled())
            key = SharedPref.getLoginUserName();
        else if (SharedPref.getIsMobileEnabled())
            key = SharedPref.getMobileNumber();
        else if (!SharedPref.getIsMobileEnabled())
            key = SharedPref.getEmailAddress();
        return key;
    }

    public static void launchLoginScreen(Context context) {
        if (!Utils.isAppbackground(context) && !Utils.checkTopActivity(context, TextConstants.OTP_SCREEN) && !Utils.checkTopActivity(context, TextConstants.LOGIN_SCREEN)) {
            Intent startActivity = new Intent();
            startActivity.setClass(MyApplication.getAppContext(), LoginActivity.class);
            startActivity.setAction(LoginActivity.class.getName());
            startActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity.putExtra(TextConstants.PREFILL_NUM, true);
            context.startActivity(startActivity);
            ((Activity) context).finish();
        }
    }

    public static void launchRegionalBlockScreen(Context context) {
        if (!Utils.isAppbackground(context) && !Utils.checkTopActivity(context, TextConstants.REGIONALBLOCK_SCREEN)) {
            Intent startActivity = new Intent();
            startActivity.setClass(context, RegionalBlockActivity.class);
            startActivity.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(startActivity);
        }
    }

    public static boolean checkIsToLogoutUser() {
        ConfigMO configMO = ConfigManager.getInstance().getConfig();
        if (configMO.getAutoLockMinute() == 0)
            return false;

        try {

            long milliseconds1 = SharedPref.getLastInteractionTime();
            long milliseconds2 = System.currentTimeMillis();
            long diff = milliseconds2 - milliseconds1;

            long diffSeconds = diff / 1000;

            long autoLogoutTimeSeconds = (long) configMO.getAutoLockMinute() * 60;
            if (diffSeconds >= autoLogoutTimeSeconds && SharedPref.getAccessToken() != null)
                return true;
            else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getMyHealthTabNameBasedOnPosition(int position) {
        LinkedHashMap<String, Integer> myHealthTabsMap = MyHealthTabItemsHolder.getMyHealthTabItemsHolderInstance().getMyHealthTabItemsMap();
        for (Map.Entry<String, Integer> entry : myHealthTabsMap.entrySet()) {
            if (entry.getValue() == position) {
                return entry.getKey();
            }

        }
        return MyHealthItemsEnum.Vitals.getTabItemName();

    }


    public static void setPractitionerSpecDesigRole(PractitionerMO physicianMO) {
        try {
            SharedPref.setSpeciality(physicianMO.getPractitionerRole().get(0).getSpecialty().get(0).getText());
            SharedPref.setRole(physicianMO.getPractitionerRole().get(0).getRole().getText());
            SharedPref.setExperience(physicianMO.getYearsOfExperience());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void launchLocationChangeActivity(Context context) {
        Intent intent = new Intent(VariableConstants.LOGOUT_IF_AUTH_TOKEN_NULL);
        intent.putExtra(TextConstants.REGIONAL_BLOCK, true);
        SharedPref.setOutOfRegion(true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    public static String[] getShortWeekDays(String locale) {
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale(locale));
        String[] da = symbols.getShortWeekdays();
        List<String> days = new ArrayList<String>();
        for (int i = 0; i < da.length; i++) {
            if (!da[i].equals("")) {
                days.add(da[i]);
            }
        }
        String sunday = days.get(0);
        days.remove(0);
        days.add(days.size(), sunday);
        return days.toArray(new String[7]);
    }


    public static String[] getAMPM(String locale) {
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale(locale));
        String[] da = symbols.getAmPmStrings();
        return da;
    }

    public static String getAM(String locale) {
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale(locale));
        String[] da = symbols.getAmPmStrings();
        return da[0];
    }

    public static String getPM(String locale) {
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale(locale));
        String[] da = symbols.getAmPmStrings();
        return da[1];
    }



    /* ............... App language related Methods..........*/

    public static void changeAppLanguage(Context context) {
        String languageKey = getLanguageKey(context, null);
        Locale mLocale = new Locale(languageKey);
        Locale.setDefault(mLocale);
        Configuration config = context.getResources().getConfiguration();
        config.locale = mLocale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(mLocale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }


    public static void changeAppLanguage(Context context, UserMO userMO) {
        try {
            if (userMO != null && userMO.getUserSettings() != null && userMO.getUserSettings().getUserLanguage() != null
                    && !userMO.getUserSettings().getUserLanguage().equals(context.getResources().getString(R.string.txt_null))) {

                String languageKey = TextConstants.DEFALUT_LANGUAGE_KEY, languageTitle = SharedPref.getLanguageSelected();

                languageKey = getLanguageKey(context, userMO.getUserSettings().getUserLanguage());
                languageTitle = getLanguageTitle(context, userMO.getUserSettings().getUserLanguage());

                setLocalLanguage(context, languageKey, languageTitle);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void setLocalLanguage(Context context, String languageKey, String languageTitle) {
        Locale mLocale = new Locale(languageKey);
        Locale.setDefault(mLocale);
        Configuration config = context.getResources().getConfiguration();
        config.locale = mLocale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(mLocale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        SharedPref.setLanguageSelected(languageTitle, languageKey);
    }


    public static String getLanguageKey(Context context, String languageTitle) {
        String languageSelected = "", languageKey = TextConstants.DEFALUT_LANGUAGE_KEY;

        try {
            if (languageTitle != null && !languageTitle.equals(context.getResources().getString(R.string.txt_null)) && !languageTitle.equals("")) {
                languageSelected = languageTitle;
            } else {
                languageSelected = SharedPref.getLanguageSelected();
                languageKey = SharedPref.getLanguageKey();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        LinkedHashMap<String, String> languageList = ConfigManager.getInstance().getConfig().getLanguageList();
        if (languageList.containsKey(languageSelected)) {
            return languageList.get(languageSelected);
        }
        return languageKey;

    }

    public static String getLanguageTitle(Context context, String languageKey) {
        LinkedHashMap<String, String> languageList = ConfigManager.getInstance().getConfig().getLanguageList();
        Iterator<String> iterator = languageList.keySet().iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            String value = languageList.get(next);
            if (next.equalsIgnoreCase(languageKey)) {
                return value;
            }
        }
        return context.getString(R.string.english);
    }

    public static String getText(CustomFontEditTextView customFontEditTextView) {
        return customFontEditTextView.getText().toString().trim();
    }


    public static boolean isRTL(Context context) {
        String languageKey = getLanguageKey(context, null);
        Locale locale = new Locale(languageKey);
        int layouDirection = TextUtilsCompat.getLayoutDirectionFromLocale(locale);
        if (layouDirection == ViewCompat.LAYOUT_DIRECTION_RTL)
            return true;
        else
            return false;
    }


    public static Bitmap convertBase64ToBitmap(String base64) {
        try {
            byte[] img_64format = Base64.decode(base64, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(img_64format, 0, img_64format.length);
        } catch (Exception e) {
            return null;
        }
    }

    public static String convertBitmapToBase64(Bitmap bitmap) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

            return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            return null;
        }
    }
/* ............... App language related Methods..........*/

    public static String getAddressTextString(Address address) {
        if (address != null) {
            String addressLine1 = null, line2 = null;
            if (address.getLine() != null)
                addressLine1 = address.getLine().get(0);
            if (address.getLine() != null && address.getLine().size() > 1)
                line2 = address.getLine().get(1);

            return createAddressTextString(addressLine1, line2, address.getCountry(), address.getState(), address.getCity(), address.getPostalCode());
        }
        return "";
    }

    public static String createAddressTextString(String addressLine1, String addressLine2, String country, String state, String city, String postalCode) {

        String text = "";
        if (Utils.isValueAvailable(addressLine1))
            text = text + addressLine1;
        if (Utils.isValueAvailable(addressLine2))
            text = text + ", " + addressLine2;
        if (Utils.isValueAvailable(city))
            text = text + ", " + city;
        if (Utils.isValueAvailable(postalCode))
            text = text + ", " + postalCode;
        if (Utils.isValueAvailable(state))
            text = text + ", " + state;
        if (Utils.isValueAvailable(country))
            text = text + ", " + country;


        return text;
    }

    public static String formatTimeAccordingToDeviceTimeFormat(String time, Context context) {
        return CommonTasks.formateDateFromstring(DateTimeUtil.sourceTimeFormat, android.text.format.DateFormat.is24HourFormat(context) ?
                DateTimeUtil.sourceTimeFormat : DateTimeUtil.destinationTimeFormat, time.trim());

    }


}

