package com.mphrx.fisike.utils;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MyThreadPoolExecutor extends ThreadPoolExecutor {
    private ArrayList<Runnable> activeThread;

    public MyThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    public void execute(Runnable command) {
        if (null == activeThread) {
            activeThread = new ArrayList<Runnable>();
        }
        if (!activeThread.contains(command)) {
            activeThread.add(command);
        }
        super.execute(command);
    }

    public void addActiveThread(Runnable command) {
        if (null == activeThread) {
            activeThread = new ArrayList<Runnable>();
        }
        if (!activeThread.contains(command)) {
            activeThread.add(command);
        }
    }


    @Override
    protected void afterExecute(Runnable command, Throwable t) {
        if (activeThread.contains(command)) {
            activeThread.remove(command);
        }
        super.afterExecute(command, t);
    }

    public ArrayList<Runnable> getActiveThread() {
        return activeThread;
    }

    public void removeActiveThread(Runnable command) {
        activeThread.remove(command);
    }

}
