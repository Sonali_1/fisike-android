package com.mphrx.fisike;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mphrx.fisike.adapter.SearchMedicationAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Constraints;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.gson.request.MedicationSearchRequest;
import com.mphrx.fisike.gson.response.MedicationSearchResponse;
import com.mphrx.fisike.gson.response.MedicineList;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import appointment.utils.CommonControls;


/**
 * Created by laxmansingh on 10/17/2016.
 */

public class SearchMedicationActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private IconTextView mBtn_back, mBtnCross;
    private EditText mEt_search;
    private RelativeLayout mCross_progress_lyt;
    private ProgressBar mSearch_progress_bar;

    private RelativeLayout mNo_data_lyt;
    private ImageView mNo_data_image;
    private CustomFontTextView mTv_no_data_msg;
    private CustomFontButton mBtnAddMedicine;

    private CardView card_rv;
    private RecyclerView mRv_search;
    private List<MedicineList> medicineList = new ArrayList<MedicineList>();
    private SearchMedicationAdapter mSearchMedicationAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchmedication_activity);
        mContext = this;

        Intent intent = getIntent();
        findView();
        bindView();
        initView();

        mEt_search.setText(intent.getStringExtra("medicine_name"));
        mEt_search.setSelection(mEt_search.getText().toString().trim().length());
    }


    private void findView() {
        Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.opensans_ttf_semibold));

        card_rv = (CardView) findViewById(R.id.card_rv);
        mBtn_back = (IconTextView) findViewById(R.id.btn_back);
        if (Utils.isRTL(this)) {
            mBtn_back.setRotationY(180);
        } else {
            mBtn_back.setRotationY(0);
        }
        mBtnCross = (IconTextView) findViewById(R.id.btnCross);
        mEt_search = (EditText) findViewById(R.id.et_search);
        mEt_search.setTypeface(font);
        mCross_progress_lyt = (RelativeLayout) findViewById(R.id.cross_progress_lyt);
        mSearch_progress_bar = (ProgressBar) findViewById(R.id.search_progress_bar);
        mRv_search = (RecyclerView) findViewById(R.id.rv_search);

        mNo_data_lyt = (RelativeLayout) findViewById(R.id.no_data_lyt);
        mNo_data_image = (ImageView) findViewById(R.id.no_data_image);
        mTv_no_data_msg = (CustomFontTextView) findViewById(R.id.tv_no_data_msg);
        mBtnAddMedicine = (CustomFontButton) findViewById(R.id.btnAddMedicine);
    }

    private void bindView() {
        mBtn_back.setOnClickListener(this);
        mBtnCross.setOnClickListener(this);
        mRv_search.setLayoutManager(new LinearLayoutManager(mContext));
        mRv_search.setNestedScrollingEnabled(false);
        mSearchMedicationAdapter = new SearchMedicationAdapter(mContext, medicineList, mEt_search, mNo_data_lyt, mNo_data_image, mTv_no_data_msg, mBtnAddMedicine, card_rv, mBtnCross, mSearch_progress_bar);
        mRv_search.setAdapter(mSearchMedicationAdapter);

        mEt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSearchMedicationAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) {
                    mBtnCross.setVisibility(View.GONE);
                    mSearch_progress_bar.setVisibility(View.GONE);
                }
            }
        });

        mEt_search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Intent intent = new Intent();
                    intent.putExtra("medicine_name", mEt_search.getText().toString().trim());
                    intent.putExtra("medicine_id", -1);
                    CommonControls.closeKeyBoard(mContext);
                    setResult(TextConstants.MEDICINE_SEARCH_INTENT, intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }

    private void initView() {
        mSearch_progress_bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_to_change_theme),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                CommonControls.closeKeyBoard(mContext);
                finish();
                break;

            case R.id.btnCross:
                mEt_search.setText("");
                mBtnCross.setVisibility(View.GONE);
                mSearch_progress_bar.setVisibility(View.GONE);
                medicineList.clear();
                mSearchMedicationAdapter.notifyDataSetChanged();
                mSearchMedicationAdapter.setUIAndData();
                break;

            case R.id.btnAddMedicine:
                Intent intent = new Intent();
                intent.putExtra("medicine_name", mEt_search.getText().toString().trim());
                intent.putExtra("medicine_id", -1);
                CommonControls.closeKeyBoard(mContext);
                setResult(TextConstants.MEDICINE_SEARCH_INTENT, intent);
                finish();
                break;
        }
    }
}
