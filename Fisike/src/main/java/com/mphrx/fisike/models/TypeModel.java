package com.mphrx.fisike.models;

/**
 * Created by laxmansingh on 11/25/2016.
 */

public class TypeModel {

    private String type;
    private String viewType;
    private boolean selected;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }
}
