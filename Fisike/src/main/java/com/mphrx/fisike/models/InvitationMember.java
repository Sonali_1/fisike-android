package com.mphrx.fisike.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class InvitationMember implements Parcelable {

    private String memberName = null;
    private String occupation = null;
    private boolean selected = false;

    private Bitmap profilePic = null;

    public InvitationMember(String name, String occupation, boolean selected, Bitmap profilePic) {
        super();
        this.memberName = name;
        this.occupation = occupation;
        this.selected = selected;
        this.profilePic = profilePic;
    }

    private InvitationMember(Parcel in) {
        memberName = in.readString();
        occupation = in.readString();
        selected = in.readByte() != 0;
        profilePic = (Bitmap) in.readParcelable(getClass().getClassLoader());
    }

    public Bitmap getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Bitmap profilePic) {
        this.profilePic = profilePic;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(memberName);
        dest.writeString(occupation);
        dest.writeByte((byte) (selected ? 1 : 0));
        dest.writeValue(profilePic);

    }


}
