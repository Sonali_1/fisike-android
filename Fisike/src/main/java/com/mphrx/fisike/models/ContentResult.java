package com.mphrx.fisike.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by aastha on 5/3/2017.
 */

public class ContentResult implements Serializable{

    @Expose
    private String name;
    @Expose
    private String language;
    @Expose
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
