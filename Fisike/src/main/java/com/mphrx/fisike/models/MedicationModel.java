package com.mphrx.fisike.models;

public class MedicationModel {
	private int medicationID;
	private String medician;

	public int getMedicationID() {
		return medicationID;
	}

	public void setMedicationID(int medicationID) {
		this.medicationID = medicationID;
	}

	public String getMedician() {
		return medician;
	}

	public void setMedician(String medician) {
		this.medician = medician;
	}

}
