package com.mphrx.fisike.models;

/**
 * Created by laxmansingh on 8/1/2016.
 */
public class ObservationList {
    private int observationResource;
    private String observationText;
    private int colorId;

    public ObservationList(int resourceId, String observationText, int colorId) {
        this.observationResource = resourceId;
        this.observationText = observationText;
        this.colorId = colorId;
    }

    public int getObservationResource() {
        return observationResource;
    }

    public String getObservationText() {
        return observationText;
    }

    public int getColorId() {
        return colorId;
    }
}
