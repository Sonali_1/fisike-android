package com.mphrx.fisike.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class DoseModel implements Serializable {

	private static final long serialVersionUID = -1090586260546605051L;

	private String fromDate;
	private String tillDate;
	private String description;
	private Double quntity;
	private String unit;
	private String time;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTillDate() {
		return tillDate;
	}

	public void setTillDate(String tillDate) {
		this.tillDate = tillDate;
	}

	public Double getQuntity() {
		return quntity;
	}

	public void setQuntity(Double quntity) {
		this.quntity = quntity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public static ArrayList<DoseModel> parseDosageArray(JSONArray dosageInstruction) {
		if (dosageInstruction == null || dosageInstruction.length() == 0)
			return new ArrayList<>(0);

		ArrayList<DoseModel> arr = new ArrayList<>(dosageInstruction.length());
		for (int i=0; i<dosageInstruction.length(); i++) {
			try {
				arr.add(DoseModel.parseSingle(dosageInstruction.getJSONObject(i)));
			} catch (JSONException ignore) {
				continue;
			}
		}
		return arr;
	}

	private static DoseModel parseSingle(JSONObject jsonObject) throws JSONException, NullPointerException {
		DoseModel out = new DoseModel();
//		"dateEnded": null,
//	    "dateWritten": null,
//		JSONObject timing = jsonObject.getJSONObject("timingPeriod");
//		out.setFromDate(jsonObject.getString("dateWritten"));
//		out.setTillDate(jsonObject.getString("dateEnded"));

		JSONObject doseQuantity = jsonObject.getJSONObject("doseQuantity");
		out.setUnit(doseQuantity.getString("unit"));
		out.setQuntity(doseQuantity.getDouble("value"));

		out.setDescription(jsonObject.getString("text"));

		return out;
	}
}