package com.mphrx.fisike.models;

import java.io.Serializable;

public class DocumentReferenceModel implements Serializable {
	private static final long serialVersionUID = -4864661277317986887L;

	private int documentID;
	private String documentType;
	private String documentLocalPath;
	private String documentMasterIdentifier;
	private int patientID;
	private String timeStamp;
	private byte[] thumbImage;
	private int documentUploadedStatus;
	private int percentage;
	private int mimeType;
	private String reason;
	private boolean isToUpdate;
	private boolean isDirty;
	private long noOfAttachment;

	private String sourceName;

	public boolean isDirty() {
		return isDirty;
	}

	public void setDirty(boolean dirty) {
		isDirty = dirty;
	}

	public long getNoOfAttachment() {
		return noOfAttachment;
	}

	public void setNoOfAttachment(long noOfAttachment) {
		this.noOfAttachment = noOfAttachment;
	}



	public int getMimeType() {
		return mimeType;
	}

	public void setMimeType(int mimeType) {
		this.mimeType = mimeType;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getDocumentUploadedStatus() {
		return documentUploadedStatus;
	}

	public void setDocumentUploadedStatus(int documentUploadedStatus) {
		this.documentUploadedStatus = documentUploadedStatus;
	}

	public byte[] getThumbImage() {
		return thumbImage;
	}

	public void setThumbImage(byte[] thumbImage) {
		this.thumbImage = thumbImage;
	}

	public int getDocumentID() {
		return documentID;
	}

	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentLocalPath() {
		return documentLocalPath;
	}

	public void setDocumentLocalPath(String documentLocalPath) {
		this.documentLocalPath = documentLocalPath;
	}

	public String getDocumentMasterIdentifier() {
		return documentMasterIdentifier;
	}

	public void setDocumentMasterIdentifier(String documentMasterIdentifier) {
		this.documentMasterIdentifier = documentMasterIdentifier;
	}

	public int getPatientID() {
		return patientID;
	}

	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isToUpdate() {
		return isToUpdate;
	}

	public void setToUpdate(boolean toUpdate) {
		isToUpdate = toUpdate;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
}