package com.mphrx.fisike.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aastha on 5/3/2017.
 */

public class AgreementModel implements Serializable{

    @Expose
    private int agreementId;
    @Expose
    private String readDate;
    @Expose
    private String lastUpdated;
    @Expose
    private ArrayList<ContentResult> contentResult ;

    public int getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(int agreementId) {
        this.agreementId = agreementId;
    }

    public String getReadDate() {
        return readDate;
    }

    public void setReadDate(String readDate) {
        this.readDate = readDate;
    }

    public ArrayList<ContentResult> getContentResult() {
        return contentResult;
    }

    public void setContentResult(ArrayList<ContentResult> contentResult) {
        this.contentResult = contentResult;
    }


    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
