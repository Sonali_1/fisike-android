package com.mphrx.fisike.models;

/**
 * Created by aastha on 5/10/2017.
 */

public class SupportAgreementModel {

    private String readDate,content,title;

    public String getReadDate() {
        return readDate;
    }

    public void setReadDate(String readDate) {
        this.readDate = readDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
