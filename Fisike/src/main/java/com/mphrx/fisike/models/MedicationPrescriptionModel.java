package com.mphrx.fisike.models;

import com.mphrx.fisike.mo.DiseaseMO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MedicationPrescriptionModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7199482296341066030L;
    // medication prescription id
    private int ID;
    private int encounterID;
    private int practitionarID;
    private String practitionerName;
    private String unit;
    private String fromDate;
    private String toDate;
    private int patientID;
    private ArrayList<DoseModel> arrayDose;
    private ArrayList<ReminderModel> arrayReminder;
    private int medicianId;

    private ArrayList<DiseaseMO> diseaseMoList;

    private String medicineName;

    private int autoReminderSQLBool;
    private int alarmMangerRequestCode;

    private String diseaseName;
    private String diseaseCodingSystem;
    private String diseaseCode;


    public String getDiseaseCode() {
        return diseaseCode;
    }

    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseCodingSystem() {
        return diseaseCodingSystem;
    }

    public void setDiseaseCodingSystem(String diseaseCodingSystem) {
        this.diseaseCodingSystem = diseaseCodingSystem;
    }

    public String getPractitionerName() {
        return practitionerName;
    }

    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    public int getMedicianId() {
        return medicianId;
    }

    public void setMedicianId(int medicianId) {
        this.medicianId = medicianId;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public int getEncounterID() {
        return encounterID;
    }

    public void setEncounterID(int encounterID) {
        this.encounterID = encounterID;
    }

    public int getPractitionarID() {
        return practitionarID;
    }

    public void setPractitionarID(int practitionarID) {
        this.practitionarID = practitionarID;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public ArrayList<DoseModel> getArrayDose() {
        return arrayDose;
    }

    public void setArrayDose(ArrayList<DoseModel> arrayDose) {
        this.arrayDose = arrayDose;
    }

    public ArrayList<ReminderModel> getArrayReminder() {
        return arrayReminder;
    }

    public void setArrayReminder(ArrayList<ReminderModel> arrayReminder) {
        this.arrayReminder = arrayReminder;
    }

    public ArrayList<DiseaseMO> getDiseaseMoList() {
        return diseaseMoList;
    }

    public void setDiseaseMoList(ArrayList<DiseaseMO> diseaseMoList) {
        this.diseaseMoList = diseaseMoList;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public int getAutoReminderSQLBool() {
        return autoReminderSQLBool;
    }

    public void setAutoReminderSQLBool(int autoReminderSQLBool) {
        this.autoReminderSQLBool = autoReminderSQLBool;
    }

    public int getAlarmMangerRequestCode() {
        return alarmMangerRequestCode;
    }

    public void setAlarmMangerRequestCode(int alarmMangerRequestCode) {
        this.alarmMangerRequestCode = alarmMangerRequestCode;
    }

    public static Map<Integer, List<MedicationPrescriptionModel>> parseSearchMedicationArray(JSONArray list) {
        if (list == null || list.length() == 0)
            return new HashMap<>(0);

        HashMap<Integer, List<MedicationPrescriptionModel>> encounterMap = new HashMap<>();

        for (int i = 0; i < list.length(); i++) {
            try {
                JSONObject item = list.getJSONObject(i);
                MedicationPrescriptionModel model = parseSingle(item);

                if (!encounterMap.containsKey(model.getEncounterID())) {
                    encounterMap.put(model.getEncounterID(), new ArrayList<MedicationPrescriptionModel>(1));
                }
                encounterMap.get(model.getEncounterID()).add(model);
            } catch (Exception ignore) {
                continue;
            }
        }

        return encounterMap;
    }

    public static MedicationPrescriptionModel parseSingle(JSONObject item) throws JSONException, IndexOutOfBoundsException, NullPointerException {
        MedicationPrescriptionModel out = new MedicationPrescriptionModel();

        out.setID(item.getInt("id"));

        JSONObject encounter = item.getJSONObject("encounter");
        out.setEncounterID(encounter.getInt("id"));

        JSONObject practitioner = item.getJSONObject("prescriber");
        out.setPractitionarID(practitioner.getInt("id"));
        try {
            out.setPractitionerName(practitioner.getJSONObject("name").getString("text"));
        } catch (Exception e) {
            out.setPractitionerName("");
        }

        JSONObject patient = item.getJSONObject("patient");
        out.setPatientID(patient.getInt("id"));

        JSONObject medication = item.getJSONObject("medication");
        out.setMedicianId(medication.getInt("id"));
        JSONObject code = medication.getJSONObject("code");
        out.setMedicineName(code.getString("text"));

        JSONObject reason = item.getJSONObject("reason");
        JSONArray diseaseJSONArray = reason.getJSONArray("coding");
        ArrayList<DiseaseMO> diseaseMOs = new ArrayList<>();
        for (int i = 0; i < diseaseJSONArray.length(); i++) {
            JSONObject disease = diseaseJSONArray.getJSONObject(i);
            DiseaseMO temp = new DiseaseMO(disease.getString("display"), disease.getString("code"), disease.getString("system"));
            diseaseMOs.add(temp);
        }
        out.setDiseaseMoList(diseaseMOs);
    /*	out.setDiseaseCode(disease.getString("code"));
		out.setDiseaseCodingSystem(disease.getString("system"));
		out.setDiseaseName(disease.getString("display"));*/

        ArrayList<DoseModel> dosages = DoseModel.parseDosageArray(item.getJSONArray("dosageInstruction"));
        out.setArrayDose(dosages);
        if (dosages.size() > 0) {
            out.setUnit(dosages.get(0).getUnit());
//			out.setFromDate(dosages.get(0).getFromDate());
//			out.setToDate(dosages.get(0).getTillDate());
        }try {
            out.setFromDate(item.getJSONObject("dateWritten").getString("value"));
            out.setToDate(item.getJSONObject("dateEnded").getString("value"));
        }catch (Exception e){
            e.printStackTrace();
        }

        return out;
    }
}
