package com.mphrx.fisike.models;

public class PractitionarModel {
	private int practitionarID;
	private String physicianName;

	public int getPractitionarID() {
		return practitionarID;
	}

	public void setPractitionarID(int practitionarID) {
		this.practitionarID = practitionarID;
	}

	public String getPhysicianName() {
		return physicianName;
	}

	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}
}
