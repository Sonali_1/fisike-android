package com.mphrx.fisike.models;

/**
 * Created by laxmansingh on 8/1/2016.
 */
public class VitalsModel implements  Cloneable{


    private String date = "";
    private String id = "";
    private String labName = "";
    private String patientName = "";
    private String unit = "";
    private String value = "";
    private String paramId = "";
    private String range_status = "";
    private String code = "";
    private String convertedUnit = "";
    private String convertedValue = "";
    private String source = "";
    private String highReferenceRangeValue = "";
    private String lowReferenceRangeValue = "";
    private String highReferenceRangeUnit = "";
    private String lowReferenceRangeUnit = "";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getRange_status() {
        return range_status;
    }

    public void setRange_status(String range_status) {
        this.range_status = range_status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getConvertedUnit() {
        return convertedUnit;
    }

    public void setConvertedUnit(String convertedUnit) {
        this.convertedUnit = convertedUnit;
    }

    public String getConvertedValue() {
        return convertedValue;
    }

    public void setConvertedValue(String convertedValue) {
        this.convertedValue = convertedValue;
    }

    public String getHighReferenceRangeValue() {
        return highReferenceRangeValue;
    }

    public void setHighReferenceRangeValue(String highReferenceRangeValue) {
        this.highReferenceRangeValue = highReferenceRangeValue;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLowReferenceRangeValue() {
        return lowReferenceRangeValue;
    }

    public void setLowReferenceRangeValue(String lowReferenceRangeValue) {
        this.lowReferenceRangeValue = lowReferenceRangeValue;
    }

    public String getHighReferenceRangeUnit() {
        return highReferenceRangeUnit;
    }

    public void setHighReferenceRangeUnit(String highReferenceRangeUnit) {
        this.highReferenceRangeUnit = highReferenceRangeUnit;
    }

    public String getLowReferenceRangeUnit() {
        return lowReferenceRangeUnit;
    }

    public void setLowReferenceRangeUnit(String lowReferenceRangeUnit) {
        this.lowReferenceRangeUnit = lowReferenceRangeUnit;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
