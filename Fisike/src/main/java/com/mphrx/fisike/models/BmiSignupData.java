package com.mphrx.fisike.models;

import com.mphrx.fisike.vital_submit.request.ObservationValueUnit;

import java.util.ArrayList;

/**
 * Created by aastha on 12/8/2016.
 */
public class BmiSignupData {


    private float number;
    private ArrayList<ObservationValueUnit> observationArray;
    int patientId;
    String date;
    private static BmiSignupData bmiSignupData;

    public static BmiSignupData getInstance() {
        if (bmiSignupData == null) {
            bmiSignupData = new BmiSignupData();
        }
        return bmiSignupData;
    }
    public float getNumber() {
        return number;
    }

    public void setNumber(float number) {
        this.number = number;
    }

    public ArrayList<ObservationValueUnit> getObservationArray() {
        return observationArray;
    }

    public void setObservationArray(ArrayList<ObservationValueUnit> observationArray) {
        this.observationArray = observationArray;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
