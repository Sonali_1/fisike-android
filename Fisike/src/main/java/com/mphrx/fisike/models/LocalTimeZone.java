package com.mphrx.fisike.models;

/**
 * Created by Aastha on 6/23/2016.
 */
public class LocalTimeZone {

    String time;
    String id;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
