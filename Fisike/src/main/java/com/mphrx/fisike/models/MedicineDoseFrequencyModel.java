package com.mphrx.fisike.models;

import android.content.Context;

import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by xmb2nc on 11-02-2016.
 */
public class MedicineDoseFrequencyModel  implements Serializable,Comparable<MedicineDoseFrequencyModel> {
    private static final long serialVersionUID = 4888313811443401759L;

    private Double doseQuantity;
    private String doseHours;
    private String doseMinutes;

    private String doseTimeAMorPM;
    // tablet/syrup etc
    private String doseUnit;

    // after food/ before food
    private String doseIntakeTimeInstruction;

    // days on which dose needs to be taken
    private ArrayList<String> doseWeekdaysList;

    private String startDate;

    private String endDate;

    private boolean isRepeatAfterEverySelected;

    private int repeatDaysInterval;

    public int alarmManagerUniqueKey;

    public String getDoseHours() {
        return doseHours;
    }

    public void setDoseHours(String doseTime) {
        this.doseHours = doseTime;
    }

    public String getDoseMinutes() {
        return doseMinutes;
    }

    public void setDoseMinutes(String doseMinutes) {
        this.doseMinutes = doseMinutes;
    }

    public String getDoseTimeAMorPM() {
        if(Utils.isValueAvailable(doseTimeAMorPM)){
            return  doseTimeAMorPM;
        }
        return "";
    }

    public void setDoseTimeAMorPM(String doseTimeAMorPM) {
        this.doseTimeAMorPM = doseTimeAMorPM;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getDoseIntakeTimeInstruction() {
        return doseIntakeTimeInstruction;
    }

    public void setDoseIntakeTimeInstruction(String doseIntakeTimeInstruction) {
        this.doseIntakeTimeInstruction = doseIntakeTimeInstruction;
    }

    public ArrayList<String> getDoseWeekdaysList() {
        return doseWeekdaysList;
    }

    public void setDoseWeekdaysList(ArrayList<String> doseWeekdaysList) {
        this.doseWeekdaysList = doseWeekdaysList;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Double getDoseQuantity() {
        return doseQuantity;
    }

    public void setDoseQuantity(Double doseQuantity) {
        this.doseQuantity = doseQuantity;
    }

    public boolean isRepeatAfterEverySelected() {
        return isRepeatAfterEverySelected;
    }

    public void setIsRepeatAfterEverySelected(boolean isRepeatAfterEverySelected) {
        this.isRepeatAfterEverySelected = isRepeatAfterEverySelected;
    }

    public int getRepeatDaysInterval() {
        return repeatDaysInterval;
    }

    public void setRepeatDaysInterval(int repeatDaysInterval) {
        this.repeatDaysInterval = repeatDaysInterval;
    }

    public int getAlarmManagerUniqueKey() {
        return alarmManagerUniqueKey;
    }

    public void setAlarmManagerUniqueKey(int alarmManagerUniqueKey) {
        this.alarmManagerUniqueKey = alarmManagerUniqueKey;
    }

    public ArrayList<Integer> compareDoseUnitQuantity(ArrayList<MedicineDoseFrequencyModel> arrayMedicationArrayList, int startIndex) {
        ArrayList<Integer> index = new ArrayList<>();
        index.add(startIndex);
        for (int i = startIndex + 1; i < arrayMedicationArrayList.size(); i++) {
            MedicineDoseFrequencyModel tempMedicineDoseFrequencyModel = arrayMedicationArrayList.get(i);
            if (getDoseQuantity() == tempMedicineDoseFrequencyModel.getDoseQuantity() && getDoseUnit().equalsIgnoreCase(tempMedicineDoseFrequencyModel.getDoseUnit())) {
                index.add(i);
            }
        }
        return index;
    }


    @Override
    public int compareTo(MedicineDoseFrequencyModel p2) {
        MedicineDoseFrequencyModel p1=this;
        Calendar calendar = Calendar.getInstance();
        String amPm = calendar.get(Calendar.AM_PM) == 0 ? "AM" : "PM";
        String currentTime = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + " " + amPm;
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationTimeFormat,Locale.US);
        Date start = null;
        Date d1 = null,d2 = null;

        try
        {
            start = sdf.parse(currentTime.toLowerCase());
            d1=sdf.parse((p1.getDoseHours()+":"+p1.getDoseMinutes()+" "+p1.getDoseTimeAMorPM()).toLowerCase());
            d2=sdf.parse((p2.getDoseHours()+":"+p2.getDoseMinutes()+" "+p2.getDoseTimeAMorPM()).toLowerCase());
        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        if(d1==null||d2==null)
            return 0;
        long diffrence1=start.getTime()-d1.getTime();
        long diffrence2=start.getTime()-d2.getTime();

        if(diffrence1==diffrence2)
            return 0;
        else if(diffrence1<=0 && diffrence2>0)
            return 1;
        else if(diffrence2<=0 && diffrence1>0)
            return -1;
        else
        {
            return (diffrence1<diffrence2?1:-1);
        }
    }
}
