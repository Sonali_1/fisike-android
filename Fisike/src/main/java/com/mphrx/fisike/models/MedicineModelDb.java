package com.mphrx.fisike.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aastha on 10/14/2016.
 */

public class MedicineModelDb {

    private int id;
    private List<MedicineDoseFrequencyModel> arrayMedicine;

    public MedicineModelDb(ArrayList<MedicineDoseFrequencyModel> dosesList, int id) {
        this.id=id;
        this.arrayMedicine=dosesList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<MedicineDoseFrequencyModel> getArrayMedicine() {
        return arrayMedicine;
    }

    public void setArrayMedicine(ArrayList<MedicineDoseFrequencyModel> arrayMedicine) {
        this.arrayMedicine = arrayMedicine;
    }
}
