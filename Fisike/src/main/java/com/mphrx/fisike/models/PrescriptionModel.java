package com.mphrx.fisike.models;

import com.mphrx.fisike.gson.request.MedicationOrderRequest.Extension;
import com.mphrx.fisike.mo.DiseaseMO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/*
 *
 * Created by xmb2nc on 11-02-2016.
 *
 * the purpose of this class is to fetch the parameters entered by the user to create the json request object
 */
public class PrescriptionModel implements Serializable {
    private static final long serialVersionUID = 6673064794945382474L;

    private String medicineName;
    private ArrayList<MedicineDoseFrequencyModel> arrayDose;
    private ArrayList<DiseaseMO> diseaseMoList;
    private String practitionerName;
    private long patientID;
    private int medicianId;
    private int autoReminderSQLBool;
    private String startDate;
    private String endDate;
    private int practitionarID;
    private int id;
    private int encounterID;
    private String status;
    private int mimeType;
    private List<Extension> extensionList;
    private String sourceName;

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    private String updatedDate;
    private String doseInstruction;
    private String remark;
    private String patientName;


    public int getPractitionarID() {
        return practitionarID;
    }

    public void setPractitionarID(int practitionarID) {
        this.practitionarID = practitionarID;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getEncounterID() {
        return encounterID;
    }

    public void setEncounterID(int encounterID) {
        this.encounterID = encounterID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public ArrayList<MedicineDoseFrequencyModel> getArrayDose() {
        return arrayDose;
    }

    public void setArrayDose(ArrayList<MedicineDoseFrequencyModel> arrayDose) {
        Collections.sort(arrayDose);
        this.arrayDose = arrayDose;

    }

    public ArrayList<DiseaseMO> getDiseaseMoList() {
        return diseaseMoList;
    }

    public long getPatientID() {
        return patientID;
    }

    public void setPatientID(long patientID) {
        this.patientID = patientID;
    }

    public int getMedicianId() {
        return medicianId;
    }

    public void setMedicianId(int medicianId) {
        this.medicianId = medicianId;
    }

    public int getAutoReminderSQLBool() {
        return autoReminderSQLBool;
    }

    public void setAutoReminderSQLBool(int autoReminderSQLBool) {
        this.autoReminderSQLBool = autoReminderSQLBool;
    }

    public void setDiseaseMoList(ArrayList<DiseaseMO> diseaseMoList) {
        this.diseaseMoList = diseaseMoList;
    }

    public String getPractitionerName() {
        return practitionerName;
    }

    public void setPractitionerName(String practitionerName) {
        this.practitionerName = practitionerName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDoseInstruction() {
        return doseInstruction;
    }

    public void setDoseInstruction(String doseInstruction) {
        this.doseInstruction = doseInstruction;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public MedicineDoseFrequencyModel getElement(int alarmManagerUniqueKey) {
        Iterator<MedicineDoseFrequencyModel> iterator = getArrayDose().iterator();
        while (iterator.hasNext()) {
            MedicineDoseFrequencyModel arrayDose = iterator.next();
            if (arrayDose.getAlarmManagerUniqueKey() == alarmManagerUniqueKey) {
                return arrayDose;
            }
        }
        return null;
    }

    public int getMimeType() {
        return mimeType;
    }

    public void setMimeType(int mimeType) {
        this.mimeType = mimeType;
    }

    public List<Extension> getExtensionList() {
        return extensionList;
    }

    public void setExtensionList(List<Extension> extensionList) {
        this.extensionList = extensionList;
    }
}
