package com.mphrx.fisike.models;

import java.util.ArrayList;

/**
 * Created by aastha on 12/2/2016.
 */
public class VitalsHistoryModels  {

    private String serialNumber;
    private ArrayList<VitalsModel> vitalsModels = new ArrayList<VitalsModel>();

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public ArrayList<VitalsModel> getVitalsModels() {
        return vitalsModels;
    }

    public void setVitalsModels(ArrayList<VitalsModel> vitalsModels) {
        this.vitalsModels = vitalsModels;
    }
}
