package com.mphrx.fisike.models;

import java.io.Serializable;

public class ReminderModel implements Serializable {
	private static final long serialVersionUID = 7230485268698271548L;
	private String fromDate;
	private String tillDate;
	private String description;
	private Double quntity;
	private String unit;
	private String time;
	private int alarmManagerUniqueKey;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTillDate() {
		return tillDate;
	}

	public void setTillDate(String tillDate) {
		this.tillDate = tillDate;
	}

	public Double getQuntity() {
		return quntity;
	}

	public void setQuntity(Double quntity) {
		this.quntity = quntity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getAlarmManagerUniqueKey() {
		return alarmManagerUniqueKey;
	}

	public void setAlarmManagerUniqueKey(int alarmManagerUniqueKey) {
		this.alarmManagerUniqueKey = alarmManagerUniqueKey;
	}

}
