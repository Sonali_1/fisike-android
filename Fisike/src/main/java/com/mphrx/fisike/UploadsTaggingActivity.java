package com.mphrx.fisike;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.UploadTaggingFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import java.io.File;

/**
 * Created by Aastha on 05/05/2016.
 */
public class UploadsTaggingActivity extends BaseActivity {

    private Toolbar toolbar;
    private String toEncriptFilePath;
    private FrameLayout frameLayout;
    private CustomFontTextView toolbar_title;
    private ImageButton btnBack;
    private IconTextView bt_toolbar_right;
    private int mimeType;
    private String attachmentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.upload_tagging_activity, frameLayout);
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
     /*   toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //toolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        toolbar.setTitle("Document");*/
        initializeToolbar();
        Bundle bundle = getIntent().getExtras();
        toEncriptFilePath = bundle.getString(VariableConstants.PDF_FILE_PATH);
        attachmentPath = bundle.getString(VariableConstants.VIDEO_URI);
        mimeType = bundle.getInt(VariableConstants.MIME_TYPE);
        UploadTaggingFragment uploadTaggingFragment = new UploadTaggingFragment();
        uploadTaggingFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, uploadTaggingFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            toolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));

        btnBack = (ImageButton) toolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) toolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) toolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.txt_upload));
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        deleteFile();
        finish();
        super.onBackPressed();
    }

    /*@Override
    protected void onDestroy() {
        deleteFile();
        super.onDestroy();
    }*/

    public void setAttachmentPath(String attachmentPath, String toEncriptFilePath) {
        this.toEncriptFilePath = attachmentPath;
    }

    public void deleteFile() {
        String path = null;
        switch (mimeType) {
            case VariableConstants.MIME_TYPE_IMAGE:
                path = attachmentPath;
                break;
            case VariableConstants.MIME_TYPE_FILE:
                path = toEncriptFilePath;
                break;
            case VariableConstants.MIME_TYPE_DOC:
                path = toEncriptFilePath;
                break;
            case VariableConstants.MIME_TYPE_DOCX:
                path = toEncriptFilePath;
                break;
        }

        try {
            if (!path.equals("") && path != null) {
                File f = new File(path);
                if (f != null && f.exists()) {
                    f.delete();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
