package com.mphrx.fisike.response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.SharedPreferencesConstant;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.enums.MedicationFrequancyEnum;
import com.mphrx.fisike.fragments.MedicationOrderFragment;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.DiseaseMO;
import com.mphrx.fisike.models.MedicationModel;
import com.mphrx.fisike.models.MedicineDoseFrequencyModel;
import com.mphrx.fisike.models.PractitionarModel;
import com.mphrx.fisike.models.PrescriptionModel;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Kailash on 12/15/2015.
 */
public class MedicationEncounterFetchBG extends AsyncTask<Void, Void, Void> {
    private static final String MEDICATION_PRESCRIPTION = "/medicationOrder/searchMedications";
    private ProgressDialog dialog;
    private Context context;
    private JSONObject payLoad;
    private boolean isSyncApi;
    private MedicationOrderFragment medicationFragment;
    private String jsonString;
    private PractitionarModel practitionarModel;
    private ArrayList<MedicationModel> arrayMedicationModel;
    private boolean isUpdatedPrescription;

    public MedicationEncounterFetchBG(Context context, JSONObject payLoad, boolean isSyncApi, MedicationOrderFragment medicationFragment) {
        this.context = context;
        this.payLoad = payLoad;
        this.isSyncApi = isSyncApi;
        this.medicationFragment = medicationFragment;
    }


    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage(context.getResources().getString(R.string.sync_record));
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        APIManager apiManager = APIManager.getInstance();
        String baseUrl = APIManager.createMinervaBaseUrl() + MEDICATION_PRESCRIPTION;
        try {
            jsonString = apiManager.executeHttpPost(baseUrl, "", payLoad, context);
            Object array = GsonUtils.jsonToObjectMapper(jsonString, MedicationObjectArray.class);
            if (array != null && array instanceof MedicationObjectArray) {
                MedicationObjectArray medicationObjectArray = ((MedicationObjectArray) array);
                int totalCount = medicationObjectArray.getTotalCount();
                String medicationSyncDate = medicationObjectArray.getSyncDate();
                SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(SharedPreferencesConstant.RECORD_SYNC_TIME, medicationSyncDate);
                arrayMedicationModel = new ArrayList<MedicationModel>();
                for (int i = 0; i < medicationObjectArray.getTotalCount(); i++) {
                    try {
                        List list = medicationObjectArray.getList().get(i);
                        MedicationModel medicationModel = new MedicationModel();
                        medicationModel.setMedicationID(list.getMedication().getId());
                        medicationModel.setMedician(list.getMedication().getCode().getText());

                        arrayMedicationModel.add(medicationModel);

                        practitionarModel = new PractitionarModel();
                        if (list.getPrescriber() == null) {
                            practitionarModel.setPractitionarID(0);
                            practitionarModel.setPhysicianName("");
                        } else {
                            practitionarModel.setPractitionarID(list.getPrescriber().getId());
                            practitionarModel.setPhysicianName((list.getPrescriber().getName() == null || list.getPrescriber().getName().getText() == null) ? "" : list.getPrescriber().getName().getText());
                        }
                        long patienId = list.getPatient().getId();
                        String startDate = list.getDateWritten().getValue();
                        if (startDate != null && !startDate.equals("") && !startDate.equals("null")) {
                            startDate = DateTimeUtil.calculateDateEnglis(startDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                        }
                        String endDate = list.getDateEnded().getValue();
                        if (endDate != null && !endDate.equals("") && !endDate.equals("null")) {
                            endDate = DateTimeUtil.calculateDateEnglis(endDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate);
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.yyyy_mm_dd_HiphenSeperatedDate, Locale.US);
                        String updatedDate = sdf.format(new Date());

                        Encounter encounter = list.getEncounter();

                        Reason reason = list.getReason();
                        java.util.List<Coding> coding;
                        ArrayList<DiseaseMO> diseaseMOs = new ArrayList<>();
                        coding = reason.getCoding();
                        for (Coding code : coding) {
                            DiseaseMO temp = new DiseaseMO(code.getDisplay(), code.getCode(), code.getSystem());
                            diseaseMOs.add(temp);
                        }
                        Medication medication = list.getMedication();

                        PrescriptionModel prescriptionModel = new PrescriptionModel();
                        prescriptionModel.setID(list.getId());
                        prescriptionModel.setEncounterID(encounter.getId());
                        prescriptionModel.setPractitionarID(practitionarModel.getPractitionarID());
                        prescriptionModel.setMedicianId(medicationModel.getMedicationID());
                        prescriptionModel.setMedicineName(medicationModel.getMedician());
                        prescriptionModel.setPatientID(patienId);
                        prescriptionModel.setEndDate(endDate);
                        prescriptionModel.setStartDate(startDate);
                        prescriptionModel.setAutoReminderSQLBool(1);
                        prescriptionModel.setDiseaseMoList(diseaseMOs);
                        prescriptionModel.setPractitionerName(practitionarModel.getPhysicianName());
                        prescriptionModel.setUpdatedDate(updatedDate);
                        prescriptionModel.setStatus(VariableConstants.IN_PROGRESS);

                        boolean isRepeat = false;
                        if (list.getDosageInstruction().get(0).getTiming().getRepeat() != null && list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod() != null && list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod().getValue() > 1) {
                            isRepeat = true;
                        }

                        ArrayList<MedicineDoseFrequencyModel> medicineDoseFrequencyModelList = new ArrayList<>();
                        for (DosageInstruction dosageInstruction : list.getDosageInstruction()) {

                            if (dosageInstruction.getAdditionalInstructions() != null &&
                                    dosageInstruction.getAdditionalInstructions().getCoding() != null) {
                                for (com.mphrx.fisike.gson.request.MedicationOrderRequest.Coding codingMedication : dosageInstruction.getAdditionalInstructions().getCoding()) {

                                    MedicineDoseFrequencyModel medicineDoseFrequencyModel = new MedicineDoseFrequencyModel();
                                    medicineDoseFrequencyModel.setEndDate(endDate);
                                    medicineDoseFrequencyModel.setStartDate(startDate);
                                    if (list.getDosageInstruction().get(0).getText().contains("SOS")) {
                                        medicineDoseFrequencyModel.setDoseIntakeTimeInstruction(codingMedication.getDisplay().trim());
                                    } else {
                                        String[] time = Utils.getMedicineTimingIn24Hr(codingMedication.getDisplay());
                                        medicineDoseFrequencyModel.setAlarmManagerUniqueKey((int) System.currentTimeMillis());
                                        if (time != null && time.length > 1) {
                                            boolean isAmPm = time[0].toUpperCase().contains(TextConstants.AM) || time[0].toUpperCase().contains(TextConstants.PM) ? true : false;

                                            if (isAmPm && (time[0].toUpperCase().contains(TextConstants.PM) || time[0].toUpperCase().contains(TextConstants.AM))) {
                                                String hour = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.destinationTimeFormat, DateTimeUtil.HH);
                                                String min = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.destinationTimeFormat, DateTimeUtil.mm);
                                                medicineDoseFrequencyModel.setDoseHours(hour);
                                                medicineDoseFrequencyModel.setDoseMinutes(min);
                                            } else {
                                                String hour = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.medicationTimeFormat, DateTimeUtil.HH);
                                                String min = DateTimeUtil.calculateDateEnglis(time[0], DateTimeUtil.medicationTimeFormat, DateTimeUtil.mm);
                                                medicineDoseFrequencyModel.setDoseHours(hour);
                                                medicineDoseFrequencyModel.setDoseMinutes(min);
                                            }

                                            medicineDoseFrequencyModel.setDoseIntakeTimeInstruction(time[1]);
                                        }
                                    }
                                    medicineDoseFrequencyModel.setDoseQuantity(Double.parseDouble(dosageInstruction.getDoseQuantity().getValue()));
                                    medicineDoseFrequencyModel.setDoseUnit(dosageInstruction.getDoseQuantity().getUnit());

                                    if (dosageInstruction.getTiming() != null && dosageInstruction.getTiming().getExtension() != null && dosageInstruction.getTiming().getExtension().size() > 0 && dosageInstruction.getTiming().getExtension().get(0).getValue() != null && dosageInstruction.getTiming().getExtension().get(0).getValue().size() > 0) {
                                        ArrayList<String> days = dosageInstruction.getTiming().getExtension().get(0).getValue();
                                        medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                    } else {
                                        ArrayList<String> days = new ArrayList<String>();
                                        days.add(context.getResources().getString(R.string.txt_monday));
                                        days.add(context.getResources().getString(R.string.txt_tuesday));
                                        days.add(context.getResources().getString(R.string.txt_wednesday));
                                        days.add(context.getResources().getString(R.string.txt_thursday));
                                        days.add(context.getResources().getString(R.string.txt_friday));
                                        days.add(context.getResources().getString(R.string.txt_saturday));
                                        days.add(context.getResources().getString(R.string.txt_sunday));
                                        medicineDoseFrequencyModel.setDoseWeekdaysList(days);
                                    }

                                    medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(false);
                                    if (isRepeat) {
                                        medicineDoseFrequencyModel.setIsRepeatAfterEverySelected(true);
                                        medicineDoseFrequencyModel.setRepeatDaysInterval(list.getDosageInstruction().get(0).getTiming().getRepeat().getPeriod().getValue());
                                    }

                                    medicineDoseFrequencyModelList.add(medicineDoseFrequencyModel);
                                }
                            }
                        }
                        if (list.getDosageInstruction().get(0).getText().contains("SOS")) {
                            prescriptionModel.setDoseInstruction(context.getString(R.string.conditional_sos));
                            prescriptionModel.setAutoReminderSQLBool(0);
                        } else {
                            String[] dosage_frequency = context.getResources().getStringArray(R.array.dosage_frequency);
                            prescriptionModel.setDoseInstruction(MedicationFrequancyEnum.getCodeFromValue(dosage_frequency[medicineDoseFrequencyModelList.size() - 1]));
                        }
                        prescriptionModel.setArrayDose(medicineDoseFrequencyModelList);


                        if (prescriptionModel.getAutoReminderSQLBool() > 0) {
                            ArrayList<MedicineDoseFrequencyModel> arrayMedicineDoseFrequencyModels = prescriptionModel.getArrayDose();
                            for (int j = 0; arrayMedicineDoseFrequencyModels != null && j < arrayMedicineDoseFrequencyModels.size(); j++) {
                                MedicineDoseFrequencyModel medicineDoseFrequencyModel = arrayMedicineDoseFrequencyModels.get(j);
                                int interval = medicineDoseFrequencyModel.getRepeatDaysInterval() > 0 ? medicineDoseFrequencyModel.getRepeatDaysInterval() : 1;
                                Utils.startAlarmMedication(context, medicineDoseFrequencyModel.getAlarmManagerUniqueKey(), prescriptionModel, interval, medicineDoseFrequencyModel);
                            }
                        }

                        if (list.getExtension() != null && list.getExtension().size() > 0 && list.getExtension().get(0).getValue() != null && list.getExtension().get(0).getValue().size() > 0) {
                            prescriptionModel.setRemark(list.getExtension().get(0).getValue().get(0));
                        }

                        EncounterMedicationDBAdapter.getInstance(context).insertMedicationPrescription(prescriptionModel);

                        isUpdatedPrescription = true;
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (isSyncApi) {
            // update the sync time in shared pref
            SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(SharedPreferencesConstant.MEDICATION_SYNC_TIME_LOCAL, System.currentTimeMillis());
            editor.commit();
        }
        if (medicationFragment != null && isUpdatedPrescription) {
//            medicationFragment.dbListMedicationShow(encounterModel, arrayMedicationPrescriptionModel, arrayMedicationModel);
            medicationFragment.dbListMedicationShow();
        } else {
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
        dialog.dismiss();
        super.onPostExecute(aVoid);
    }
}
