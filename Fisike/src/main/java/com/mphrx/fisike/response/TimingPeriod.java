
package com.mphrx.fisike.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class TimingPeriod {

    private EndDate endDate;
    private List<Object> extension = new ArrayList<Object>();
    private Object id;
    private StartDate startDate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public EndDate getEndDate() {
        return endDate;
    }

    public void setEndDate(EndDate endDate) {
        this.endDate = endDate;
    }

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public StartDate getStartDate() {
        return startDate;
    }

    public void setStartDate(StartDate startDate) {
        this.startDate = startDate;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
