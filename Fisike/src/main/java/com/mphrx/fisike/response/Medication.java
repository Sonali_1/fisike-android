
package com.mphrx.fisike.response;

import com.mphrx.fisike.gson.request.Code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Medication {

    private Code code;
    private List<Object> extension = new ArrayList<Object>();
    private Integer id;
    private IsBrand isBrand;
    private String kind;
    private Object manufacturer;
    private Object medPackage;
    private Object medProduct;
    private String name;
    private String _class;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IsBrand getIsBrand() {
        return isBrand;
    }

    public void setIsBrand(IsBrand isBrand) {
        this.isBrand = isBrand;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Object getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Object manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Object getMedPackage() {
        return medPackage;
    }

    public void setMedPackage(Object medPackage) {
        this.medPackage = medPackage;
    }

    public Object getMedProduct() {
        return medProduct;
    }

    public void setMedProduct(Object medProduct) {
        this.medProduct = medProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
