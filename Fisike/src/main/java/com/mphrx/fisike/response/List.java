
package com.mphrx.fisike.response;

import com.google.gson.annotations.Expose;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Extension;
import com.mphrx.fisike.mo.PatientMO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class List {

    private StartDate dateWritten;
    private Object dispense;
    private EndDate dateEnded;
    private java.util.List<DosageInstruction> dosageInstruction = new ArrayList<DosageInstruction>();
    private Encounter encounter;
    private java.util.List<Extension> extension = new ArrayList<Extension>();
    private Integer id;
    private java.util.List<Object> identifier = new ArrayList<Object>();
    private Medication medication;
    private PatientMO patient;
    private Prescriber prescriber;
    private Reason reason;
    private String _class;
    private Object status;
    private Object substitution;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private String lastUpdated;

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public StartDate getDateWritten() {
        return dateWritten;
    }

    public void setDateWritten(StartDate dateWritten) {
        this.dateWritten = dateWritten;
    }

    public EndDate getDateEnded() {
        return dateEnded;
    }

    public void setDateEnded(EndDate dateEnded) {
        this.dateEnded = dateEnded;
    }

    public Object getDispense() {
        return dispense;
    }

    public void setDispense(Object dispense) {
        this.dispense = dispense;
    }

    public java.util.List<DosageInstruction> getDosageInstruction() {
        return dosageInstruction;
    }

    public void setDosageInstruction(java.util.List<DosageInstruction> dosageInstruction) {
        this.dosageInstruction = dosageInstruction;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    public java.util.List<Extension> getExtension() {
        return extension;
    }

    public void setExtension(java.util.List<Extension> extension) {
        this.extension = extension;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public java.util.List<Object> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(java.util.List<Object> identifier) {
        this.identifier = identifier;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public PatientMO getPatient() {
        return patient;
    }

    public void setPatient(PatientMO patient) {
        this.patient = patient;
    }

    public Prescriber getPrescriber() {
        return prescriber;
    }

    public void setPrescriber(Prescriber prescriber) {
        this.prescriber = prescriber;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getSubstitution() {
        return substitution;
    }

    public void setSubstitution(Object substitution) {
        this.substitution = substitution;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
