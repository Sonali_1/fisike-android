
package com.mphrx.fisike.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Prescriber {

    private Object address;
    private Object birthDate;
    private List<Object> communication = new ArrayList<Object>();
    private List<Object> extension = new ArrayList<Object>();
    private Object gender;
    private Integer id;
    private List<Object> identifier = new ArrayList<Object>();
    private List<Object> location = new ArrayList<Object>();
    private Object lowerCaseName;
    private Name name;
    private Object organization;
    private Object period;
    private List<Object> photo = new ArrayList<Object>();
    private List<Object> qualification = new ArrayList<Object>();
    private String _class;
    private List<Object> role = new ArrayList<Object>();
    private List<Object> speciality = new ArrayList<Object>();
    private List<Object> telecom = new ArrayList<Object>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Object birthDate) {
        this.birthDate = birthDate;
    }

    public List<Object> getCommunication() {
        return communication;
    }

    public void setCommunication(List<Object> communication) {
        this.communication = communication;
    }

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<Object> identifier) {
        this.identifier = identifier;
    }

    public List<Object> getLocation() {
        return location;
    }

    public void setLocation(List<Object> location) {
        this.location = location;
    }

    public Object getLowerCaseName() {
        return lowerCaseName;
    }

    public void setLowerCaseName(Object lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Object getOrganization() {
        return organization;
    }

    public void setOrganization(Object organization) {
        this.organization = organization;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public List<Object> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Object> photo) {
        this.photo = photo;
    }

    public List<Object> getQualification() {
        return qualification;
    }

    public void setQualification(List<Object> qualification) {
        this.qualification = qualification;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public List<Object> getRole() {
        return role;
    }

    public void setRole(List<Object> role) {
        this.role = role;
    }

    public List<Object> getSpeciality() {
        return speciality;
    }

    public void setSpeciality(List<Object> speciality) {
        this.speciality = speciality;
    }

    public List<Object> getTelecom() {
        return telecom;
    }

    public void setTelecom(List<Object> telecom) {
        this.telecom = telecom;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
