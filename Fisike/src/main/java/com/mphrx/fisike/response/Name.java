
package com.mphrx.fisike.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Name {

    private List<Object> family = new ArrayList<Object>();
    private List<Object> given = new ArrayList<Object>();
    private Object period;
    private List<Object> prefix = new ArrayList<Object>();
    private List<Object> suffix = new ArrayList<Object>();
    private String text;
    private String useCode;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Object> getFamily() {
        return family;
    }

    public void setFamily(List<Object> family) {
        this.family = family;
    }

    public List<Object> getGiven() {
        return given;
    }

    public void setGiven(List<Object> given) {
        this.given = given;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public List<Object> getPrefix() {
        return prefix;
    }

    public void setPrefix(List<Object> prefix) {
        this.prefix = prefix;
    }

    public List<Object> getSuffix() {
        return suffix;
    }

    public void setSuffix(List<Object> suffix) {
        this.suffix = suffix;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
