
package com.mphrx.fisike.response;

import com.mphrx.fisike.gson.request.MedicationOrderRequest.*;
import com.mphrx.fisike.gson.request.MedicationOrderRequest.Timing;

import java.util.HashMap;
import java.util.Map;

public class DosageInstruction {

    private AdditionalInstructions additionalInstructions;
    private Object asNeeded;
    private DoseQuantity doseQuantity;
    private Object maxDosePerPeriod;
    private Object method;
    private Object rate;
    private Object route;
    private Object site;
    private String text;
    private com.mphrx.fisike.gson.request.MedicationOrderRequest.Timing timing;
    private TimingPeriod timingPeriod;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AdditionalInstructions getAdditionalInstructions() {
        return additionalInstructions;
    }

    public void setAdditionalInstructions(AdditionalInstructions additionalInstructions) {
        this.additionalInstructions = additionalInstructions;
    }

    public Object getAsNeeded() {
        return asNeeded;
    }

    public void setAsNeeded(Object asNeeded) {
        this.asNeeded = asNeeded;
    }

    public DoseQuantity getDoseQuantity() {
        return doseQuantity;
    }

    public void setDoseQuantity(DoseQuantity doseQuantity) {
        this.doseQuantity = doseQuantity;
    }

    public Object getMaxDosePerPeriod() {
        return maxDosePerPeriod;
    }

    public void setMaxDosePerPeriod(Object maxDosePerPeriod) {
        this.maxDosePerPeriod = maxDosePerPeriod;
    }

    public Object getMethod() {
        return method;
    }

    public void setMethod(Object method) {
        this.method = method;
    }

    public Object getRate() {
        return rate;
    }

    public void setRate(Object rate) {
        this.rate = rate;
    }

    public Object getRoute() {
        return route;
    }

    public void setRoute(Object route) {
        this.route = route;
    }

    public Object getSite() {
        return site;
    }

    public void setSite(Object site) {
        this.site = site;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public com.mphrx.fisike.gson.request.MedicationOrderRequest.Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public TimingPeriod getTimingPeriod() {
        return timingPeriod;
    }

    public void setTimingPeriod(TimingPeriod timingPeriod) {
        this.timingPeriod = timingPeriod;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
