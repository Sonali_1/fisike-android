
package com.mphrx.fisike.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DoseQuantity {

    private Object code;
    private Object comparator;
    private List<Object> extension = new ArrayList<Object>();
    private Object id;
    private Object system;
    private String unit;
    private String value;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getComparator() {
        return comparator;
    }

    public void setComparator(Object comparator) {
        this.comparator = comparator;
    }

    public List<Object> getExtension() {
        return extension;
    }

    public void setExtension(List<Object> extension) {
        this.extension = extension;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getSystem() {
        return system;
    }

    public void setSystem(Object system) {
        this.system = system;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
