package com.mphrx.fisike;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import com.mphrx.fisike.customview.CustomFontTextView;

import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mphrx.fisike.adapter.NotificationAdapter;
import com.mphrx.fisike.adapter.SupportAdapter;
import com.mphrx.fisike.adapter.settingsAdapter;
import com.mphrx.fisike.adapter.settingsAdapter.SettingViewHolder;
import com.mphrx.fisike.adapter.settingsAdapter.clickListener;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.SettingItemData;
import com.mphrx.fisike.gson.request.SupportDataItem;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

public class NotificationActivity extends BaseActivity implements NotificationAdapter.clickListener, OnClickListener {
    private RecyclerView notificationRecyclerView;
    private NotificationAdapter notificationAdapter;
    private RecyclerView.LayoutManager notificationLayoutManager;
    String[] titleList, subTitleList;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    ArrayList<SettingItemData> itemList;
    private FrameLayout frameLayout;
    private boolean medication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_notification);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_notification, frameLayout);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        findViews();
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(R.string.Notifications);
        notificationRecyclerView = (android.support.v7.widget.RecyclerView) findViewById(R.id.rv_notifications_view);
        notificationLayoutManager = new LinearLayoutManager(this);
        notificationRecyclerView.setLayoutManager(notificationLayoutManager);
        titleList = getResources().getStringArray(R.array.notification_title);
        subTitleList = getResources().getStringArray(R.array.notification_subTitle);
        itemList = new ArrayList<SettingItemData>();
        initializeDataForSetting();
        notificationAdapter = new NotificationAdapter(itemList, this);
        notificationAdapter.setClickListener(this);
        notificationRecyclerView.setAdapter(notificationAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeDataForSetting() {
        for (int i = 0; i < titleList.length; i++) {
            if (BuildConfig.isChatEnabled && titleList[i].equals(getString(R.string.Messages))) {
                addTitlesToItemList(i);
            } else if (BuildConfig.isMedicationEnabled && titleList[i].equals(getString(R.string.Medications)) && !BuildConfig.isPhysicianApp) {
                addTitlesToItemList(i);
            } else if (!BuildConfig.hideStatus && titleList[i].equals(getString(R.string.DocumentUploaded)) && !BuildConfig.isPhysicianApp) {
                addTitlesToItemList(i);
            }
        }
    }

    private void addTitlesToItemList(int position) {
        SettingItemData data = new SettingItemData();
        data.setTitle(titleList[position]);
        data.setSubTitle(subTitleList[position]);
        itemList.add(data);
    }

    @Override
    public void itemClicked(final View view, int position) {
        // TODO Auto-generated method stub

        SharedPreferences sharedPref = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        Editor edit = sharedPref.edit();

        String title = notificationAdapter.getItemAtPosition(position);
        if (title.equals(getString(R.string.Messages)))
            edit.putBoolean(VariableConstants.MSG_NOTIFICATION_ENABLED, ((SwitchCompat) (view)).isChecked());
        else if (title.equals(getString(R.string.DocumentUploaded)))
            edit.putBoolean(VariableConstants.UPLOADS_NOTIFICATION_ENABLED, ((SwitchCompat) (view)).isChecked());
        else if (title.equals(getString(R.string.Medications))) {
            if (!((SwitchCompat) (view)).isChecked()) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(NotificationActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage(getResources().getString(R.string.medication_notification_turn_off));
                dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        medication = false;
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        medication = true;
                        ((SwitchCompat) (view)).setChecked(true);
                        dialog.cancel();
                    }
                });
                dialog.show();
            } else {
                medication = true;
                ((SwitchCompat) (view)).setChecked(true);
            }
            edit.putBoolean(VariableConstants.MEDICATION_NOTIFICATION_ENABLED, medication);
        }

        edit.commit();
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

}

