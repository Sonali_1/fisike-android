package com.mphrx.fisike.broadcast;

import android.content.Intent;

public interface ConnectivityChanged {

    public abstract void onNetConnectivityBroadcast(Intent intent);

}
