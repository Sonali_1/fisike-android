package com.mphrx.fisike.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.services.MessengerService.MessengerBinder;
import com.mphrx.fisike.utils.Utils;

public class NetworkChangeBroadcast extends BroadcastReceiver {
	private ConnectivityChanged connectivityChanged;

	public NetworkChangeBroadcast() {
	}

	public NetworkChangeBroadcast(ConnectivityChanged connectivityChanged) {
		this.connectivityChanged = connectivityChanged;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		boolean isUserLoggedin = Utils.isUserloggedin(context);
		if (Utils.isNetworkAvailable(context) && !Utils.isTimerRunning(context) && isUserLoggedin) {
			startTimers(getServiceInstance(context), context);
		} else if (!Utils.isNetworkAvailable(context) && Utils.isTimerRunning(context) && isUserLoggedin) {
			shutDownTimers(getServiceInstance(context));
			Utils.pendingMessageTimerStatus(context, false);
		}
		if (connectivityChanged != null) {
			connectivityChanged.onNetConnectivityBroadcast(intent);
		}
	}

	private void startTimers(MessengerService messengerService, Context context) {
		if (messengerService != null) {
			Utils.pendingMessageTimerStatus(context, true);
			messengerService.initiateTimers();
		}
	}

	private void shutDownTimers(MessengerService messengerService) {
		if (messengerService != null) {
			messengerService.shutDownNetworkTimers();
		}
	}

	private MessengerService getServiceInstance(Context context) {
		MessengerService service = retriveBindedService(context);
		if (service == null) {
			MyApplication.getAppContext().startService(new Intent(context, MessengerService.class));
			return retriveBindedService(context);
		}
		return service;
	}

	private MessengerService retriveBindedService(Context context) {
		try {
			Intent serviceIntent = new Intent(context, MessengerService.class);
			MessengerBinder binder = (MessengerBinder) peekService(context, serviceIntent);
			return binder.getService();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
