package com.mphrx.fisike.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import android.os.Vibrator;

import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.services.RegistrationIntentService;
import com.mphrx.fisike.services.ResetAlarmonBootupService;
import com.mphrx.fisike_physician.utils.AppLog;

public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent resetAlarmIntent = new Intent(context, ResetAlarmonBootupService.class);
        context.startService(resetAlarmIntent);
    }
}
