package com.mphrx.fisike.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

public class MySpinner extends Spinner {

    private boolean isSelected;

    public MySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

//	@Override
//	public void setSelection(int position) {
//		if (isSelected) {
//			this.isSelected = false;
//			super.setSelection(position);
//			getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
//		} else if (position == -1) {
//			this.isSelected = true;
//			super.setSelection(position);
//		} else {
//			this.isSelected = false;
//			super.setSelection(position);
//		}
//	}

    public void setPosition(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}