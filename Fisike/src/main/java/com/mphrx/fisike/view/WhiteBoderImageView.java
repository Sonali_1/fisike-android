package com.mphrx.fisike.view;

import android.content.Context;
import android.util.AttributeSet;

//import com.github.siyamed.shapeimageview.shader.RoundedShader;

public class WhiteBoderImageView extends ShaderImageView {

    public WhiteBoderImageView(Context context) {
        super(context);
    }

    public WhiteBoderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WhiteBoderImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected ShaderHelper createImageViewHelper() {
        RoundedShader roundedShader = new RoundedShader();
        roundedShader.setBorderColor(getResources().getColor(android.R.color.white));
        return roundedShader;
    }

}
