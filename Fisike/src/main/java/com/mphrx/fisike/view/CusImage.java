package com.mphrx.fisike.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class CusImage extends View {

    private Paint myPaint;
    private float startAngle;
    private float sweepAngle;
    private RectF rect;
    private int pix = 0;
    private int progress = 0;
    private float progressSize;
    private boolean isStrokeSame;
    private float strokeSize;

    public CusImage(Context context, AttributeSet attrs, MasterLayout m, int progresscolor) {
        super(context, attrs);
        init(progresscolor);
    }

    public CusImage(Context context, MasterLayout m, int progresscolor) {
        super(context);
        init(progresscolor);
    }

    public CusImage(Context context, MasterLayout masterLayout, float progressSize, boolean isStrokeSame, float strokeSize, int progresscolor) {
        super(context);
        this.progressSize = progressSize;
        this.isStrokeSame = isStrokeSame;
        this.strokeSize = strokeSize;
        init(progresscolor);
    }

    private void init(int progresscolor) {
        myPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        //
        // int width = metrics.widthPixels;
        // int height = metrics.heightPixels;

        // float size = Utils.convertDpToPixel(300, getContext());

        float scarea = progressSize * progressSize;

        pix = (int) Math.sqrt(scarea * 0.0217);

        myPaint.setAntiAlias(true);
        myPaint.setStyle(Paint.Style.STROKE);
        if (progresscolor == 0) {
            myPaint.setColor(Color.rgb(0, 161, 234));
        } else {
            myPaint.setColor(progresscolor); // Edit this to change progress arc color.
        }
        if (isStrokeSame) {
            myPaint.setStrokeWidth(strokeSize);
        } else {
            myPaint.setStrokeWidth(7);
        }
        myPaint.setAntiAlias(true);

        float startx = (float) (pix * 0.05);
        float endx = (float) (pix * 0.95);
        float starty = (float) (pix * 0.05);
        float endy = (float) (pix * 0.95);
        rect = new RectF(startx, starty, endx, endy);
    }

    public void setupprogress(int progress) {
        // Updating progress arc
        this.progress = progress;
        sweepAngle = (float) (progress * 3.6);
    }

    public int getProgress() {
        return progress;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = pix;
        int desiredHeight = pix;
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {

            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {

            height = Math.min(desiredHeight, heightSize);
        } else {

            height = desiredHeight;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        startAngle = -90;
        canvas.drawArc(rect, startAngle, sweepAngle, false, myPaint);
    }
}