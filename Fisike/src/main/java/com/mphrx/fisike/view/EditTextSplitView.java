package com.mphrx.fisike.view;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.lock_screen.SetUpdateMPIN;
import com.mphrx.fisike_physician.utils.AppLog;

/**
 * Created by manohar on 30/03/16.
 */
public class EditTextSplitView extends LinearLayout {
    EditText et1, et2, et3, et4;
    private boolean isPrefillValues;
    Context context;
    private int textColor;
    private int background;
    private Activity activity;

    public EditTextSplitView(Context context) {
        super(context);
        this.context = context;
        init();
        applyFieldAttributes(null);


    }

    public EditTextSplitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
        applyFieldAttributes(attrs);
    }

    public EditTextSplitView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
        applyFieldAttributes(attrs);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextSplitView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyFieldAttributes(attrs);
        init();
    }

    private void init() {

        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View v = layoutInflater.inflate(R.layout.split_edittext, null, false);
        et1 = (EditText) v.findViewById(R.id.et1);
        et2 = (EditText) v.findViewById(R.id.et2);
        et3 = (EditText) v.findViewById(R.id.et3);
        et4 = (EditText) v.findViewById(R.id.et4);


        et1.addTextChangedListener(new CustomWatcher(et1));
        et2.addTextChangedListener(new CustomWatcher(et2));
        et3.addTextChangedListener(new CustomWatcher(et3));
        et4.addTextChangedListener(new CustomWatcher(et4));


/*        et1.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_DEL)) {
                    Log.d("Hellpo", "Message");
                    return true;
                }
                return false;
            }
        });

*/
        et1.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return keyEvent((EditText) v, keyCode, event);
            }
        });
        et2.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                return keyEvent((EditText) v, keyCode, event);
            }
        });
        et3.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                return keyEvent((EditText) v, keyCode, event);
            }
        });
        et4.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                return keyEvent((EditText) v, keyCode, event);
            }
        });

/*
        et1.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.whit), PorterDuff.Mode.SRC_IN);
        et2.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.white_50), PorterDuff.Mode.SRC_IN);
        et3.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.white_50), PorterDuff.Mode.SRC_IN);
        et4.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.white_50), PorterDuff.Mode.SRC_IN);
*/
        et1.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchListener(v);
                return false;
            }
        });
        et2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchListener(v);
                return false;
            }
        });
        et3.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchListener(v);
                return false;
            }
        });
        et4.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchListener(v);
                return false;
            }
        });

        addView(v);

    }


    public void clearEditText() {
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et1.requestFocus();
    }

    public String getValue() {
        return getText(et1) + getText(et2) + getText(et3) + getText(et4);
    }

    private boolean validate() {

        if (et1.getText().length() == 1 && et2.getText().length() == 1 && et3.getText().length() == 1 && et4.getText().length() == 1) {
            return true;
        }
        return false;
    }


    private String getText(EditText e) {
        return (e.getText().toString() != null ? e.getText().toString() : "");
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    class CustomKeyListener implements OnKeyListener {

        private EditText view;

        public CustomKeyListener(EditText view) {
            this.view = view;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            return false;
        }
    }

    private void requestFocus(EditText v, final EditText focus) {

        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                focus.requestFocus();
                focus.setSelection(focus.length());
            }
        }, 10);
    }

    private boolean keyEvent(EditText view, int keyCode, KeyEvent event) {
        if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_DEL)) {

            switch (view.getId()) {
                case R.id.et2:
                    if (et2.getText().length() == 0) {
                        requestFocus(et2, et1);
                        return true;
                    }
                    break;
                case R.id.et3:
                    if (et3.getText().length() == 0) {
                        requestFocus(et3, et2);
                        return true;
                    }
                    break;
                case R.id.et4:
                    if (et4.getText().length() == 0) {
                        requestFocus(et4, et3);
                        return true;
                    }
                    break;
            }
            view.setText("");
            return true;
        } else if (!et1.getText().toString().trim().equals("")) {
            char pressedKey = (char) event.getUnicodeChar();

            if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_BACK)) {
                if (activity != null)
                    activity.onBackPressed();
                return true;
            } else {
                switch (view.getId()) {
                    case R.id.et1:
                        et1.setText(Character.toString(pressedKey));
                        requestFocus(et1, et2);
                        break;
                    case R.id.et2:
                        et2.setText(Character.toString(pressedKey));
                        requestFocus(et2, et3);
                        break;
                    case R.id.et3:
                        et3.setText(Character.toString(pressedKey));
                        requestFocus(et3, et4);
                        break;
                    case R.id.et4:
                        if (et4.getText().toString().trim().length() == 0) {
                            et4.setText(Character.toString(pressedKey));
                        }
                        requestFocus(et4, et4);
                        break;
                }
                return true;
            }
        }
        return false;
    }

    class CustomWatcher implements TextWatcher {

        private EditText view;

        public CustomWatcher(EditText view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            String text = charSequence.toString();

            if (text.equals("") || isPrefillValues)
                return;

            switch (view.getId()) {
                case R.id.et1:
                    if (i1 == 0) {
                        requestFocus(et1, et2);
                    }
                    break;
                case R.id.et2:
                    if (i1 == 0) {
                        requestFocus(et2, et3);
                    } else {
                        requestFocus(et2, et3);
                    }
                    break;
                case R.id.et3:
                    if (i1 == 0) {
                        requestFocus(et3, et4);
                    } else {
                        requestFocus(et3, et4);
                    }
                    break;
                case R.id.et4:
                    if (text.length() == 0) {
                        requestFocus(et4, et3);
                    }
                    break;
            }
        }

        public void afterTextChanged(Editable editable) {

        }


        private void requestFocus(EditText v, final EditText focus) {

            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    focus.requestFocus();
//                    focus.setText("");
                }
            }, 30);
        }
    }

    private void onTouchListener(final View v) {
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.requestFocus();
//                    ((EditText)v).setText("");
            }
        }, 30);
    }

    public void setText(String code) {

        int codeValue = Integer.parseInt(code);
        try {
            isPrefillValues = true;
            et1.setText("" + code.charAt(0));
            et2.setText("" + code.charAt(1));
            et3.setText("" + code.charAt(2));
            et4.setText("" + code.charAt(3));
        } catch (Exception e) {
            isPrefillValues = false;
            AppLog.d("EditText split view", "cannot set OTP text");
        }
        isPrefillValues = false;
    }


    private void applyFieldAttributes(AttributeSet attrs) {
        try {
            TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.splitView);
            textColor = attributeArray.getColor(R.styleable.splitView_splitViewTextColor, context.getResources().getColor(R.color.white));
        } catch (Exception e) {
            textColor = context.getResources().getColor(R.color.white);
        }
        //applying color
        et1.setTextColor(textColor);
        et2.setTextColor(textColor);
        et3.setTextColor(textColor);
        et4.setTextColor(textColor);

    }
}
