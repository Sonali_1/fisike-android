package com.mphrx.fisike.view;

import android.content.Context;
import android.util.AttributeSet;

//import com.github.siyamed.shapeimageview.shader.RoundedShader;

public class RoundedImageView extends ShaderImageView {

    public RoundedImageView(Context context) {
        super(context);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected ShaderHelper createImageViewHelper() {
        // TODO Auto-generated method stub
        return new RoundedShader();
    }

}
