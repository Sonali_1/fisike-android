package com.mphrx.fisike.view.ExpandableRecyclerView.viewholders;

import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;

import com.mphrx.fisike.R;
import com.mphrx.fisike.view.ExpandableRecyclerView.listeners.OnGroupClickListener;

/**
 * ViewHolder for the {link ExpandableGroup#title} in a {link ExpandableGroup}
 * <p>
 * The current implementation does now allow for sub {@link View} of the parent view to trigger
 * a collapse / expand. *Only* click events on the parent {@link View} will trigger a collapse or
 * expand
 */
public abstract class GroupViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

    private OnGroupClickListener listener;
    private boolean listRefreshing;

    public GroupViewHolder(View itemView) {
        super(itemView);
        itemView.findViewById(R.id.layout_show).setOnClickListener(this);
        itemView.findViewById(R.id.tv_title).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        System.out.println("Adapter Clicked : " + getAdapterPosition());
        if (!listRefreshing) {
            switch (v.getId()) {
                case R.id.layout_show:
                    if (listener != null) {
                        if (!listener.onGroupClick(getAdapterPosition())) {
                            expand();
                        } else {
                            collapse();
                        }
                    }
                    break;
                case R.id.tv_title:
                    setPatientInfoClick();
                    break;
            }
        }
    }

    public void setPatientInfoClick() {
    }

    public void setOnGroupClickListener(OnGroupClickListener listener) {
        this.listener = listener;
    }

    public void expand() {
    }

    public void collapse() {
    }

    public void setListRefreshing(boolean listRefreshing) {
        this.listRefreshing = listRefreshing;
    }

}
