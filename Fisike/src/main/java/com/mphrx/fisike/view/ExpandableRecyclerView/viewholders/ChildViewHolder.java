package com.mphrx.fisike.view.ExpandableRecyclerView.viewholders;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.RadiologyDetailScreen;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.record_screen.activity.MicrobiologyActivity;
import com.mphrx.fisike.record_screen.activity.ObservationActivity;
import com.mphrx.fisike.record_screen.model.DiagnosticOrder;
import com.mphrx.fisike.record_screen.model.DiagnosticReport;
import com.mphrx.fisike.view.ExpandableRecyclerView.listeners.OnGroupClickListener;


/**
 * ViewHolder for {link ExpandableGroup#items}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnGroupClickListener listener;

    public ChildViewHolder(View itemView) {
        super(itemView);
        itemView.findViewById(R.id.layout_hide).setOnClickListener(this);
        itemView.findViewById(R.id.layout_report).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_hide:
                if (listener != null) {
                    if (listener.onGroupClick(getAdapterPosition())) {
                        collapse();
                    }
                }
                break;
            case R.id.layout_report:
                setRecordClick();
                break;
        }
    }

    public void setRecordClick() {

    }

    public void expand() {
    }

    public void collapse() {
    }

    public void setOnGroupClickListener(OnGroupClickListener listener) {
        this.listener = listener;
    }

}
