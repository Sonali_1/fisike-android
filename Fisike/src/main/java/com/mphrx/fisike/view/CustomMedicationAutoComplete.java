package com.mphrx.fisike.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;

public class CustomMedicationAutoComplete extends AutoCompleteTextView {
 
    public CustomMedicationAutoComplete(Context context) {
        super(context);
    }

    public CustomMedicationAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
 
    public CustomMedicationAutoComplete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
 
    // disable AutoCompleteTextView filter
   @Override
   protected void performFiltering(final CharSequence text, final int keyCode) {
       super.performFiltering(text, keyCode);
   }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isPopupShowing()) {
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            if(inputManager.hideSoftInputFromWindow(findFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS)){
                return true;
            }
        }

        return super.onKeyPreIme(keyCode, event);
    }

}
