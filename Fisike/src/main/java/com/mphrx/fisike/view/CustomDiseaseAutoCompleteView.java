package com.mphrx.fisike.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.FontCache;

public class CustomDiseaseAutoCompleteView extends AutoCompleteTextView {

    public CustomDiseaseAutoCompleteView(Context context) {
        super(context);
    }

    public CustomDiseaseAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public CustomDiseaseAutoCompleteView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs);
    }

    // disable AutoCompleteTextView filter
    @Override
    protected void performFiltering(final CharSequence text, final int keyCode) {
        super.performFiltering(text, keyCode);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);
        Typeface customFont = selectTypeface(context, fontName, fontName);
        setTypeface(customFont);
        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName, String textStyle) {
        if (textStyle == null) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        }
        if (textStyle.equals(VariableConstants.BOLD)) {
            FontCache.getTypeface("sans/OpenSans-Bold.ttf", context);
        } else if (textStyle.equals(VariableConstants.MEDIUM)) {
            return FontCache.getTypeface("sans/OpenSans-Semibold.ttf", context);
        } else if (textStyle.equals(VariableConstants.LIGHT)) {
            return FontCache.getTypeface("sans/OpenSans-Light.ttf", context);
        } else if (textStyle.equals(VariableConstants.REGULAR)) {
            return FontCache.getTypeface("sans/OpenSans-Regular.ttf", context);
        } else if (textStyle.equals(VariableConstants.ITALIC)) {
            return FontCache.getTypeface("sans/OpenSans-Italic.ttf", context);
        }
        return FontCache.getTypeface("sans/OpenSans-Semiboldat.ttf", context);

    }


    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isPopupShowing()) {
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            if(inputManager.hideSoftInputFromWindow(findFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS)){
                return true;
            }
        }

        return super.onKeyPreIme(keyCode, event);
    }
}

