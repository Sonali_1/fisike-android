package com.mphrx.fisike.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.mphrx.fisike.R;

public class MasterLayout extends FrameLayout {

    public CusImage cusview;
    private int pix = 0;
    private RectF rect;

    private ImageView full_circle_image, arc_image;

    private Paint stroke_color;
    private RotateAnimation arcRotation;
    private boolean isProgressAnimation;
    float progressSize;
    float strokeSize;
    boolean isStrokeSame;
    int secoundryProgressColor;
    int progressColor;

    public MasterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ProgressPercentage, 0, 0);

        try {
            progressSize = a.getDimension(R.styleable.ProgressPercentage_progressSize, 300);
            strokeSize = a.getFloat(R.styleable.ProgressPercentage_strockSize, 3);
            isStrokeSame = a.getBoolean(R.styleable.ProgressPercentage_strockSame, false);
            progressColor = a.getColor(R.styleable.ProgressPercentage_progressColor, 0xFF333333);
            secoundryProgressColor = a.getColor(R.styleable.ProgressPercentage_secoundryProgressColor, 0xFF333333);
        } finally {
            a.recycle();
        }
        initialise();
        setpaint();
        displayMetrics();
        init();
    }

    public int getSecoundryProgressColor() {
        return secoundryProgressColor;
    }

    public void setSecoundryProgressColor(int secoundryProgressColor) {
        this.secoundryProgressColor = secoundryProgressColor;
        invalidate();
        requestLayout();
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        invalidate();
        requestLayout();
    }

    public float getProgressSize() {
        return progressSize;
    }

    public void setProgressSize(float progressSize) {
        this.progressSize = progressSize;
        invalidate();
        requestLayout();
    }

    public float getStrokeSize() {
        return strokeSize;
    }

    public void setStrokeSize(float strokeSize) {
        this.strokeSize = strokeSize;
        invalidate();
        requestLayout();
    }

    public boolean isStrokeSame() {
        return isStrokeSame;
    }

    public void setStrokeSame(boolean isStrokeSame) {
        this.isStrokeSame = isStrokeSame;
        invalidate();
        requestLayout();
    }

    public MasterLayout(Context context) {
        super(context, null);
    }

    private void initialise() {
        cusview = new CusImage(getContext(), this, progressSize, isStrokeSame, strokeSize, progressColor);
        full_circle_image = new ImageView(getContext());

        arc_image = new ImageView(getContext());

        arcRotation = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        arcRotation.setRepeatCount(Animation.INFINITE);
    }

    private void setpaint() {
        // Setting up color

        stroke_color = new Paint(Paint.ANTI_ALIAS_FLAG);
        stroke_color.setAntiAlias(true);
        // stroke_color.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
        if (isStrokeSame) {
            stroke_color.setColor(secoundryProgressColor);
        } else {
            stroke_color.setColor(Color.rgb(0, 161, 234)); // Edit this to change
        }
        // the circle color

        stroke_color.setStrokeWidth(strokeSize);
        stroke_color.setStyle(Paint.Style.STROKE);

        // stroke_color.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.TRANSPARENT, Color.WHITE, TileMode.CLAMP));
        // stroke_color.setColor(getContext().getResources().getDrawable(R.drawable.gradient_progress));
    }


    public void startAnimation() {
        if (!isProgressAnimation) {
            isProgressAnimation = true;

            full_circle_image.setVisibility(View.GONE);
            arc_image.setVisibility(View.VISIBLE);

            arc_image.startAnimation(arcRotation);

            invalidate();
        }

    }

    public void stopAnimation() {
        if (isProgressAnimation) {
            isProgressAnimation = false;
            full_circle_image.setVisibility(View.VISIBLE);
            arc_image.clearAnimation();
            arc_image.setVisibility(View.GONE);

            invalidate();
        }
    }

    public boolean isProgressAnimation() {
        return isProgressAnimation;
    }

    private void displayMetrics() {

        // Responsible for calculating the size of views and canvas based upon
        // screen resolution.

        // DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();

        // int width = metrics.widthPixels;
        // int height = metrics.heightPixels;
        // float scarea = width * height;

        // float size = Utils.convertDpToPixel(progressSize, getContext());

        float scarea = progressSize * progressSize;

        pix = (int) Math.sqrt(scarea * 0.0217);

    }

    public void init() {

        // Defining and drawing bitmaps and assigning views to the layout

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        lp.setMargins(10, 10, 10, 10);

        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap full_circle_bmp = Bitmap.createBitmap(pix, pix, conf);
        Bitmap arc_bmp = Bitmap.createBitmap(pix, pix, conf);

        Canvas full_circle_canvas = new Canvas(full_circle_bmp);
        Canvas arc_canvas = new Canvas(arc_bmp);

        float startx = (float) (pix * 0.05);
        float endx = (float) (pix * 0.95);
        float starty = (float) (pix * 0.05);
        float endy = (float) (pix * 0.95);
        rect = new RectF(startx, starty, endx, endy);

        full_circle_canvas.drawArc(rect, 0, 360, false, stroke_color);
        arc_canvas.drawArc(rect, -80, 340, false, stroke_color);

        full_circle_image.setImageBitmap(full_circle_bmp);

        arc_image.setImageBitmap(arc_bmp);

        addView(full_circle_image, lp);
        addView(arc_image, lp);
        addView(cusview, lp);

    }

}