package com.mphrx.fisike.view;

import android.content.Context;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconButton;
import com.mphrx.fisike.icomoon.IconTextView;

/**
 * Created by neharathore on 03/07/17.
 */

public class CrossedImageView extends RelativeLayout{

    clickInterface listener;
    IconButton iconCross;
    IconTextView iconCamera;
    CustomFontTextView textImageType;
    RelativeLayout rlImageBackGround;
    Context context;
    public CrossedImageView(Context context) {
        super(context);
        this.context=context;
        LayoutInflater inflateor= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflateor.inflate(R.layout.image_drawable_with_dotted_border,this);
        iconCross= (IconButton) v.findViewById(R.id.itv_cross);
        iconCamera= (IconTextView) v.findViewById(R.id.itv_icon_camera);
        initViews();

    }


    private void initViews()
    {
 /*       iconCross=new IconButton(context);
 */       iconCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null)
                    listener.clickCross();
            }
        });

   /*     iconCross.setGravity(Gravity.RIGHT);
        iconCross.setGravity(Gravity.TOP);
        iconCross.setText(context.getString(R.string.fa_cross));

        iconCamera=new IconTextView(context);
        iconCamera.setText(context.getString(R.string.fa_camera_icon));
        iconCamera.setGravity(Gravity.CENTER);



        rlImageBackGround=new RelativeLayout(context);
        rlImageBackGround.addView(iconCamera);
        rlImageBackGround.addView(iconCross);*/
    }




    public void setClickListener(clickInterface listener)
    {
                this.listener=listener;
    }
    public interface clickInterface
    {
        public void clickCross();
    }

}


