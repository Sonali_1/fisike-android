package com.mphrx.fisike.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;

public class ClearableEditText extends RelativeLayout {
    private CustomFontEditTextView editText;
    private IconTextView clearTextButton;
    private CustomFontTextView txtAdd;
    private TextChangedListener editTextListener = null;
    private OnEditorActionListener editTextActionListener = null;
    private Context context;
    private boolean isEditable = true;
    private boolean isAddTxtVisibile = true;
    private boolean isCrossVisibile = true;

    public ClearableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initViews(context, attrs);
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
        this.context = context;
        initViews(context, attrs);
    }

    public void addTextChangedListener(TextChangedListener listener) {
        this.editTextListener = listener;
    }

    public void setIsAddTxtVisible(boolean isAddTxtVisibile) {
        this.isAddTxtVisibile = isAddTxtVisibile;
        if (isAddTxtVisibile) {
            txtAdd.setVisibility(View.VISIBLE);
        } else {
            txtAdd.setVisibility(View.GONE);
        }
    }

    public void setIsCrossVisible(boolean isCrossVisibile) {
        this.isCrossVisibile = isCrossVisibile;
        if (isCrossVisibile) {
            clearTextButton.setVisibility(View.VISIBLE);
        } else {
            clearTextButton.setVisibility(View.GONE);
        }
    }


    public void setHint(String text) {
        if (text != null && !text.equals("null")) {
            editText.getEditText().setHint(text);
        }
    }

    public void setHintColor(int color) {
        editText.getEditText().setHintTextColor(context.getResources().getColor(color));
    }


    public void setSingleLine() {
        editText.getEditText().setSingleLine();
    }

    public void setTextSize(int size) {
        editText.getEditText().setTextSize(size);
    }

    public void setTextColor(int color) {
        editText.getEditText().setTextColor(context.getResources().getColor(color));
    }

    public void setInputType(int type) {
        editText.getEditText().setInputType(type);
    }

    public void addEditorActionListener(OnEditorActionListener editTextActionListener) {
        this.editTextActionListener = editTextActionListener;
    }

    public void setEditTextSize(int size) {
        editText.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    private void initViews(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ClearableEditText, 0, 0);
        String hintText;
        Boolean sendEnter;
        int deleteButtonRes;
        try {
            hintText = a.getString(R.styleable.ClearableEditText_hintText);
            deleteButtonRes = a.getResourceId(R.styleable.ClearableEditText_deleteButtonRes, R.drawable.ic_clear_text_24dp);
        } finally {
            a.recycle();
        }

        editText = createEditText(context, hintText);
        clearTextButton = createImageButton(context, deleteButtonRes);
        this.addView(editText);

        this.addView(clearTextButton);
        if (isAddTxtVisibile) {
            this.addView(txtAdd);
        }

        editText.getEditText().setGravity(Gravity.START);
        if (BuildConfig.isPatientApp) {
            editText.getEditText().setClickable(true);
            editText.getEditText().setFocusableInTouchMode(true);
            editText.getEditText().addTextChangedListener(txtEntered());
        } else {
            editText.getEditText().setEnabled(false);
        }

        registerSearchOptionClick();
        editText.getEditText().setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                }
                return false;
            }
        });

        editText.getEditText().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isEditable) {
                    if (editText.getEditText().getText().toString().length() > 0) {
                        if (isCrossVisibile) {
                            clearTextButton.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (isCrossVisibile) {
                            clearTextButton.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (isCrossVisibile) {
                        clearTextButton.setVisibility(View.GONE);
                    }
                }

            }

        });

        clearTextButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                editText.getEditText().setText("");

            }
        });

    }

    public void registerSearchOptionClick() {
        editText.getEditText().setOnEditorActionListener(new CustomFontTextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    editTextActionListener.onEditorAction(v, actionId, event);
                    return true;
                }
                return false;
            }

        });
    }

    public void setDisallowEnter() {
        editText.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }

    public void setScrolling() {
        editText.getEditText().setHorizontallyScrolling(false);
        editText.getEditText().setSingleLine(false);
        editText.getEditText().setMaxLines(10);
    }

    public void setScrollBar() {
        editText.getEditText().setVerticalScrollBarEnabled(true);
        editText.getEditText().setMovementMethod(ScrollingMovementMethod.getInstance());
        editText.getEditText().setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
    }

    public TextWatcher txtEntered() {
        return new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextListener != null)
                    editTextListener.onTextChanged(s, start, before, count);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editTextListener != null)
                    editTextListener.afterTextChanged(s);
                if (isEditable) {
                    if (editText.getEditText().getText().toString().length() > 0) {
                        if (isCrossVisibile) {
                            clearTextButton.setVisibility(View.VISIBLE);
                        }
                        visibleAddText(View.GONE);
                    } else {
                        if (isCrossVisibile) {
                            clearTextButton.setVisibility(View.GONE);
                        }
                        visibleAddText(View.VISIBLE);
                    }
                } else {
                    if (isCrossVisibile) {
                        clearTextButton.setVisibility(View.GONE);
                    }
                    visibleAddText(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editTextListener != null)
                    editTextListener.beforeTextChanged(s, start, count, after);
            }
        };
    }

    private void visibleAddText(int visible) {
        if (isAddTxtVisibile) {
            if (txtAdd == null) {
                txtAdd = new CustomFontTextView(context);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_END);
//                params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                if (Utils.isRTL(context)) {
                    params.setMargins(12, 18, 0, 0);
                } else {
                    params.setMargins(0, 18, 12, 0);
                }
                txtAdd.setLayoutParams(params);
                txtAdd.setText(context.getText(R.string.add));
                txtAdd.setTextSize(14);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    txtAdd.setTextColor(context.getResources().getColor(R.color.medium_green, null));
                } else {
                    txtAdd.setTextColor(context.getResources().getColor(R.color.medium_green));
                }
                txtAdd.setTypeface(txtAdd.selectTypeface(context, VariableConstants.MEDIUM, VariableConstants.MEDIUM));
            }
            txtAdd.setVisibility(visible);
        }
    }


    private CustomFontEditTextView createEditText(Context context, String hintText) {
        editText = new CustomFontEditTextView(context);
        editText.getEditText().setRawInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        editText.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        editText.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));

        editText.getEditText().setHorizontallyScrolling(false);
        editText.getEditText().setVerticalScrollBarEnabled(true);
        editText.getEditText().setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        editText.getEditText().setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
        editText.getEditText().setHint(hintText);

        if (Utils.isRTL(context)) {
            editText.setClearableEditTextPadding(80, 0, 14, 26);
        } else {
            editText.setClearableEditTextPadding(14, 0, 80, 26);
        }
        if (hintText != null) {
            editText.getEditText().setContentDescription(hintText.replaceAll(" ", "_"));
        }
        editText.getEditText().setSingleLine(false);

        return editText;
    }

    private IconTextView createImageButton(Context context, int deleteButtonRes) {
        clearTextButton = new IconTextView(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
//        params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

        if (Utils.isRTL(context)) {
            params.setMargins(12, 18, 0, 0);
        } else {
            params.setMargins(0, 18, 12, 0);
        }
        // params.setMargins(0, 0, 10, 0);

       /* int paddingPixel = 25;
        float density = context.getResources().getDisplayMetrics().density;
        int paddingDp = (int)(paddingPixel * density);*/

        //    clearTextButton.setPadding(0,0,0,-paddingDp);
        clearTextButton.setLayoutParams(params);
        clearTextButton.setText(context.getResources().getString(R.string.fa_cross_new));
        clearTextButton.setGravity(ALIGN_PARENT_TOP);
        clearTextButton.setTextSize(30);
        clearTextButton.setTextColor(context.getResources().getColor(R.color.grey_blue));
        clearTextButton.setVisibility(View.GONE);
        visibleAddText(View.VISIBLE);
        return clearTextButton;
    }

    public String getText() {
        return editText.getEditText().getText().toString();
    }

    public void clearEditText() {
        editText.getEditText().setText("");
    }

    public void setText(String message) {
        if (message != null) {
            editText.getEditText().setError(null);
            editText.getEditText().setText(message);
        }
    }

    public void setImeOptions(int imeAction) {
        editText.getEditText().setImeOptions(imeAction);
    }

    public interface TextChangedListener extends TextWatcher {
    }

    public void setMaxLength(int length) {
        int maxLength = length;
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
        editText.getEditText().setFilters(FilterArray);
    }


    public void setMargins(int left, int top, int right, int bottom) {
        if (Utils.isRTL(context)) {
            editText.setClearableEditTextPadding(right, top, left, bottom);
        } else {
            editText.setClearableEditTextPadding(left, top, right, bottom);
        }
    }

    public void setMinMaxLines() {
        editText.getEditText().setMinLines(1);
        editText.getEditText().setLines(3);
    }

    public void setCursorVisible(boolean isEditable) {
        editText.getEditText().setCursorVisible(isEditable);
    }

    public void setBasicEditableProperty(boolean isEditable) {
        this.isEditable = isEditable;
        editText.getEditText().setClickable(isEditable);
        editText.getEditText().setFocusable(isEditable);
        editText.getEditText().setFocusableInTouchMode(isEditable);
        editText.getEditText().setPadding(0, 0, 0, 10);
        if (isEditable) {
            clearTextButton.setVisibility(View.VISIBLE);
            visibleAddText(View.GONE);
        } else {
            clearTextButton.setVisibility(View.GONE);
            visibleAddText(View.VISIBLE);
        }
    }

    public EditText getEditText() {
        return editText.getEditText();
    }

    public IconTextView getclearTextButton() {
        return clearTextButton;
    }

    public void setInputType() {
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
    }

    public void setEditTextHint(String hint) {
        editText.getEditText().setHint(hint);
        editText.getEditText().setContentDescription(hint.replaceAll(" ", "_"));
    }

    public void setError(String errorMessage) {

        editText.setError(errorMessage);
    }


    public void setMargin(int margin) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, (int) (margin * context.getResources().getDisplayMetrics().density));
        this.setLayoutParams(layoutParams);
    }

    public IconTextView getClearTextButton() {
        return clearTextButton;
    }
}
//import com.mphrx.fisike.utils.Utils;
//
//public class ClearableEditText extends LinearLayout {
//	protected EditText editText;
//	protected ImageButton clearTextButton;
//	TextChangedListener editTextListener = null;
//	OnEditorActionListener editTextActionListener = null;
//	private Context context;
//	private boolean isEditable = true;
//
//	public ClearableEditText(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		this.context = context;
//		initViews(context, attrs);
//	}
//
//	public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
//		this(context, attrs);
//		initViews(context, attrs);
//	}
//
//	public void addTextChangedListener(TextChangedListener listener) {
//		this.editTextListener = listener;
//	}
//
//	public void setSingleLine() {
//		editText.setSingleLine();
//	}
//
//	public void setTextSize(int size) {
//		editText.setTextSize(size);
//	}
//
//	public void setTextColor(int color){
//		editText.setTextColor(color);
//	}
//
//	public void setInputType(int type)
//	{
//		editText.setInputType(type);
//	}
//
//	/*
//	 * public void setEditTextSelector() { editText.setBackgroundResource(R.drawable.clearable_edittext_selector);
//	 * clearTextButton.setBackgroundResource(R.drawable.clearable_edittext_selector); }
//	 */
//	public void addEditorActionListener(OnEditorActionListener editTextActionListener) {
//		this.editTextActionListener = editTextActionListener;
//	}
//
//	public void setEditTextSize(int size) {
//		editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
//	}
//
//	public void setBackgroundResource() {
//		this.setBackgroundResource(R.drawable.clearable_edittext_selector);
//	}
//
//	private void initViews(Context context, AttributeSet attrs) {
//		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ClearableEditText, 0, 0);
//		String hintText;
//		Boolean sendEnter;
//		int deleteButtonRes;
//		try {
//			// get the text and colors specified using the names in attr.xml
//			hintText = a.getString(R.styleable.ClearableEditText_hintText);
//			deleteButtonRes = a.getResourceId(R.styleable.ClearableEditText_deleteButtonRes, R.drawable.ic_clear_text_24dp);
//
//		} finally {
//			a.recycle();
//		}
//		editText = createEditText(context, hintText);
//		clearTextButton = createImageButton(context, deleteButtonRes);
//		this.addView(editText);
//		this.addView(clearTextButton);
//		editText.setClickable(true);
//		editText.setFocusableInTouchMode(false);
//		editText.addTextChangedListener(txtEntered());
//		editText.setGravity(Gravity.LEFT);
//		registerSearchOptionClick();
//		editText.setOnKeyListener(new OnKeyListener() {
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//				// You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
//				if (keyCode == KeyEvent.KEYCODE_DEL) {
//					// this is for backspace
//				}
//				return false;
//			}
//		});
//		/*
//		 * editText.setOnFocusChangeListener(new OnFocusChangeListener() {
//		 *
//		 * @Override public void onFocusChange(View v, boolean hasFocus) { if (isEditable) { if (hasFocus && editText.getText().toString().length() >
//		 * 0) clearTextButton.setVisibility(View.VISIBLE); else clearTextButton.setVisibility(View.GONE); } else {
//		 * clearTextButton.setVisibility(View.GONE); }
//		 *
//		 * } });
//		 */
//
//		editText.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//
//				if (isEditable) {
//					if (editText.getText().toString().length() > 0)
//						clearTextButton.setVisibility(View.VISIBLE);
//					else
//						clearTextButton.setVisibility(View.GONE);
//				} else {
//					clearTextButton.setVisibility(View.GONE);
//				}
//
//			}
//
//		});
//
//		clearTextButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				editText.setText("");
//
//			}
//		});
//
//	}
//
//	public void registerSearchOptionClick() {
//		editText.setOnEditorActionListener(new CustomFontTextView.OnEditorActionListener() {
//
//			@Override
//			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//					editTextActionListener.onEditorAction(v, actionId, event);
//					return true;
//				}
//				return false;
//			}
//
//		});
//	}
//
//	public void setDisallowEnter() {
//		editText.setOnLongClickListener(new OnLongClickListener() {
//			@Override
//			public boolean onLongClick(View v) {
//				return true;
//			}
//		});
//	}
//
//	public void setScrolling() {
//		editText.setHorizontallyScrolling(false);
//		editText.setSingleLine(false);
//		editText.setMaxLines(10);
//	}
//
//	public void setScrollBar() {
//		editText.setVerticalScrollBarEnabled(true);
//		editText.setMovementMethod(ScrollingMovementMethod.getInstance());
//		editText.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
//	}
//
//	public TextWatcher txtEntered() {
//		return new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				if (editTextListener != null)
//					editTextListener.onTextChanged(s, start, before, count);
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//				if (editTextListener != null)
//					editTextListener.afterTextChanged(s);
//				if (isEditable) {
//					if (editText.getText().toString().length() > 0)
//						clearTextButton.setVisibility(View.VISIBLE);
//					else
//						clearTextButton.setVisibility(View.GONE);
//				} else {
//					clearTextButton.setVisibility(View.GONE);
//				}
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//				if (editTextListener != null)
//					editTextListener.beforeTextChanged(s, start, count, after);
//
//			}
//
//		};
//	}
//
//
//	private EditText createEditText(Context context, String hintText) {
//		editText = new EditText(context);
//		editText.setRawInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
//		editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
//
//		editText.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
//		editText.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
//		editText.setHorizontallyScrolling(false);
//		editText.setVerticalScrollBarEnabled(true);
//		editText.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//		if (Build.VERSION.SDK_INT >= 16) {
//			editText.setBackground(null);
//		}
//		editText.setHint(hintText);
//		if (hintText != null) {
//			editText.setContentDescription(hintText.replaceAll(" ", "_"));
//		}
//		editText.setSingleLine(false);
//		return editText;
//	}
//
//	private ImageButton createImageButton(Context context, int deleteButtonRes) {
//		clearTextButton = new ImageButton(context);
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		params.gravity = Gravity.CENTER_VERTICAL;
//		clearTextButton.setLayoutParams(params);
//		clearTextButton.setBackgroundResource(deleteButtonRes);
//		clearTextButton.setVisibility(View.GONE);
//		return clearTextButton;
//	}
//
//	public String getText() {
//		return editText.getText().toString();
//	}
//
//	public void clearEditText() {
//		editText.setText("");
//	}
//
//	public void setText(String message) {
//		if (message != null) {
//			editText.setError(null);
//			editText.setText(message);
//		}
//	}
//
//	public void setImeOptions(int imeAction) {
//		editText.setImeOptions(imeAction);
//	}
//
//	public interface TextChangedListener extends TextWatcher {
//	}
//
//	public void setMaxLength(int length) {
//		int maxLength = length;
//		InputFilter[] FilterArray = new InputFilter[1];
//		FilterArray[0] = new InputFilter.LengthFilter(maxLength);
//		editText.setFilters(FilterArray);
//	}
//
//	public void setMinMaxLines() {
//		editText.setMinLines(1);
//		editText.setLines(3);
//	}
//
//	public void setCursorVisible(boolean isEditable) {
//		editText.setCursorVisible(isEditable);
//		// editText.setTextSize(context.getResources().getDimension(R.dimen.setting_value));
//	}
//
//	public void setBasicEditableProperty(boolean isEditable) {
//		this.isEditable = isEditable;
//		editText.setClickable(isEditable);
//		editText.setFocusable(isEditable);
//		editText.setFocusableInTouchMode(isEditable);
//		editText.setPadding(0, 0, 0, 10);
//		if (isEditable) {
//			clearTextButton.setVisibility(View.VISIBLE);
//			setBackgroundResource(R.drawable.edit_text_bg);
//		} else {
//			clearTextButton.setVisibility(View.GONE);
//			setBackgroundResource(android.R.color.transparent);
//		}
//	}
//
//	public EditText getEditText() {
//		return editText;
//	}
//
//	public ImageButton getclearTextButton() {
//		return clearTextButton;
//	}
//
//	public void setInputType() {
//		editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
//	}
//
//	public void setEditTextHint(String hint) {
//		editText.setHint(hint);
//		editText.setContentDescription(hint.replaceAll(" ", "_"));
//	}
//
//	public void setError(String errorMessage) {
//		editText.setText("");
//		editText.setError(errorMessage);
//		editText.requestFocus();
//	}
//
//	/*public void setMargins() {
//		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//				LinearLayout.LayoutParams.WRAP_CONTENT);
//		int margin =  (int)Utils.convertPixelsToDp(30, context);
//		layoutParams.setMargins(0, 0, 0, margin);
//		this.setLayoutParams(layoutParams);
//	}
//*/
//	public void setMargin(int margin) {
//		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//		layoutParams.setMargins(0, 0, 0, (int) (margin * context.getResources().getDisplayMetrics().density));
//		this.setLayoutParams(layoutParams);
//	}
//
//	public void setLayoutHeight(int width) {
//		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (int) (width * context.getResources()
//				.getDisplayMetrics().density));
//		this.setLayoutParams(layoutParams);
//	}
//
//}
