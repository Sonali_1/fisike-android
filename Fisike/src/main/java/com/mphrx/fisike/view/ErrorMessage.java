package com.mphrx.fisike.view;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike_physician.utils.AppLog;

public class ErrorMessage {
    int layoutId;
    private String TAG = "ERROR MESSAGE";
    private Activity activity;
    private Animation animationIn;
    private Animation animationOut;

    public ErrorMessage(Activity paramActivity, int paramInt) {
        this.activity = paramActivity;
        this.layoutId = paramInt;
    }

    private void handleAnimation(final LinearLayout paramLinearLayout) {
        this.animationIn = new TranslateAnimation(0.0F, 0.0F, -60.0F, 0.0F);
        this.animationIn.setDuration(300L);
        this.animationIn.setInterpolator(new AccelerateInterpolator(0.5F));
        this.animationOut = new TranslateAnimation(0.0F, 0.0F, 0.0F, -60.0F);
        this.animationOut.setDuration(300L);
        this.animationOut.setInterpolator(new AccelerateInterpolator(0.5F));
        paramLinearLayout.setAnimation(this.animationIn);
        paramLinearLayout.bringToFront();
        paramLinearLayout.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                paramLinearLayout.setAnimation(ErrorMessage.this.animationOut);
                paramLinearLayout.setVisibility(View.GONE);
            }
        }, 3000L);
    }

    private void handleInAnimation(LinearLayout paramLinearLayout) {
        this.animationIn = new TranslateAnimation(0.0F, 0.0F, -60.0F, 0.0F);
        this.animationIn.setDuration(300L);
        this.animationIn.setInterpolator(new AccelerateInterpolator(0.5F));
        paramLinearLayout.setAnimation(this.animationIn);
        paramLinearLayout.bringToFront();
        paramLinearLayout.setVisibility(View.VISIBLE);
    }

    private void handleOutAnimation(final LinearLayout paramLinearLayout) {
        this.animationOut = new TranslateAnimation(0.0F, 0.0F, 0.0F, -60.0F);
        this.animationOut.setDuration(300L);
        this.animationOut.setInterpolator(new AccelerateInterpolator(0.5F));
        new Handler().postDelayed(new Runnable() {
            public void run() {
                paramLinearLayout.setAnimation(ErrorMessage.this.animationOut);
                paramLinearLayout.setVisibility(View.GONE);
            }
        }, 300L);
    }

    public void showMessage(String paramString) {
        AppLog.d(this.TAG, this.activity.toString());
      /*  LinearLayout localLinearLayout = (LinearLayout) this.activity.findViewById(this.layoutId);
        if ((localLinearLayout == null) || (localLinearLayout.isShown())) {
            return;
        }
        localLinearLayout.removeAllViews();
        View localView = this.activity.getLayoutInflater().inflate(R.layout.error_message, null);
        ((CustomFontTextView) localView.findViewById(R.id.txtErrorMessage)).setText(paramString);
        localLinearLayout.addView(localView, 0, new RelativeLayout.LayoutParams(-1, -2));
        localView.setVisibility(View.VISIBLE);
        handleAnimation(localLinearLayout);*/
        Toast.makeText(activity,paramString,Toast.LENGTH_SHORT).show();
    }

    public void showMessage(String paramString, boolean isNetworkConnectionAvailable) {
        AppLog.d(this.TAG, this.activity.toString());
        LinearLayout localLinearLayout = (LinearLayout) this.activity.findViewById(this.layoutId);
        if (localLinearLayout != null && isNetworkConnectionAvailable) {
            handleOutAnimation(localLinearLayout);
            return;
        }
        if ((localLinearLayout == null) || (localLinearLayout.isShown())) {
            return;
        }
        localLinearLayout.removeAllViews();
        View localView = this.activity.getLayoutInflater().inflate(R.layout.error_message, null);
        ((CustomFontTextView) localView.findViewById(R.id.txtErrorMessage)).setText(paramString);
        localLinearLayout.addView(localView, 0, new RelativeLayout.LayoutParams(-1, -2));
        localView.setVisibility(View.VISIBLE);
        handleInAnimation(localLinearLayout);
    }
}