package com.mphrx.fisike.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.mphrx.fisike.R;
import com.mphrx.fisike.utils.Utils;

public class LineView extends View {
    private Paint paint;
    private int width;
    private int height;

    public LineView(Context context, int width, int height) {
        super(context);
        this.width = width;
        this.height = height;
        paint = new Paint();
        paint.setColor(context.getResources().getColor(R.color.gray));
        paint.setStrokeWidth(4);
    }

    public LineView(Context context, int width, int height, int color, int stroke) {
        super(context);
        this.width = width;
        this.height = height;
        paint = new Paint();
        paint.setColor(color);
        paint.setStrokeWidth(stroke);
    }

    @Override
    public void onDraw(Canvas canvas) {
        float heightDP = Utils.convertDpToPixel(height, getContext()) / 2;
        float widthDP = Utils.convertDpToPixel(width, getContext());
        canvas.drawLine(0, heightDP, widthDP, heightDP, paint);
    }

}
