package com.mphrx.fisike;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.background.CheckEmailExistGson;
import com.mphrx.fisike.background.CheckIfEmailExistsBG;
import com.mphrx.fisike.background.changePasswordBG;
import com.mphrx.fisike.background.getPatientShowDetails;
import com.mphrx.fisike.background.updateUserProfileBG;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.UpdatePatientRequest;
import com.mphrx.fisike.gson.request.User;
import com.mphrx.fisike.gson.response.ForgetPassOtpVerifyResponse;
import com.mphrx.fisike.gson.response.updatePatientResponse;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.PatientMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeDetails extends BaseActivity {

    boolean isChangePassword, isChangeEmail;
    private LinearLayout ll_Change_Email, ll_change_password;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private static final int EDIT_EMAIL = 1;
    private static final int EDIT_PASSWORD = 2;
    private CustomFontEditTextView etEmail, etOldPass, etNewPass, etConfirmPass;
    private CustomFontTextView tvPassError;
    String oldPass, NewPass, ConfirmPass, email;
    private UserMO userMo;
    private String error;
    private AlertDialog.Builder dialog;
    private String password;
    private ProgressDialog pd_ring;
    private PatientMO patientMo;
    private User userdata;
    private ChangeDetails context;
    private boolean isEditPassword = false;
    private boolean isDialogShowing;
    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.changedetails, frameLayout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            opendialog();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    private void opendialog() {
        finish();
    }

    public void open() {
        //startActivity(new Intent(this, TestActivity.class));
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        findViews();
        initView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        opendialog();
        return;
    }

    private void initializeUserMo() {
        userMo = SettingManager.getInstance().getUserMO();
    }

    private void findViews() {
        ll_Change_Email = (LinearLayout) findViewById(R.id.ll_changeEmail);
        ll_change_password = (LinearLayout) findViewById(R.id.ll_changePassword);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        etEmail = (CustomFontEditTextView) findViewById(R.id.et_email);
        etOldPass = (CustomFontEditTextView) findViewById(R.id.et_oldPassWord);
        etNewPass = (CustomFontEditTextView) findViewById(R.id.et_NewPassWord);
        etConfirmPass = (CustomFontEditTextView) findViewById(R.id.et_ConfirmPassWord);
        tvPassError = (CustomFontTextView) findViewById(R.id.pass_verification);
    }

    private void initView() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        isChangePassword = (getIntent().getBooleanExtra(VariableConstants.IS_CHANGE_PASSWORD, false));
        isChangeEmail = (getIntent().getBooleanExtra(VariableConstants.IS_CHANGE_EMAIL, false));
        etOldPass.setTransformationMethod(new PasswordTransformationMethod());
        etNewPass.setTransformationMethod(new PasswordTransformationMethod());
        etConfirmPass.setTransformationMethod(new PasswordTransformationMethod());
        etNewPass.setPasswordKeyListener();
        etConfirmPass.setPasswordKeyListener();

        if (Utils.isRTL(this)) {
            etOldPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_show_password, 0, 0, 0);
            etNewPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_show_password, 0, 0, 0);
            etConfirmPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_show_password, 0, 0, 0);
        } else {
            etOldPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_show_password, 0);
            etNewPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_show_password, 0);
            etConfirmPass.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_show_password, 0);
        }

        etNewPass.setPasswordKeyListener();
        etConfirmPass.setPasswordKeyListener();
        etOldPass.setPasswordKeyListener();

        if (isChangeEmail) {
            toolbar_title.setText(getResources().getString(R.string.changeEmail));
            showView(EDIT_EMAIL);
        } else {
            toolbar_title.setText(getResources().getString(R.string.changePassword));
            showView(EDIT_PASSWORD);
        }


        NumberKeyListener PwdkeyListener = new NumberKeyListener() {

            public int getInputType() {
                return InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
            }

            @Override
            protected char[] getAcceptedChars() {
                return TextConstants.passwordAllowedChars;
            }
        };


        etConfirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etOldPass.setError("");
                etConfirmPass.setError("");

            }

        });
    }

    private void showView(int i) {
        switch (i) {
            case EDIT_EMAIL:
                ll_Change_Email.setVisibility(View.VISIBLE);
                ll_change_password.setVisibility(View.GONE);
                tvPassError.setVisibility(View.GONE);
                break;
            case EDIT_PASSWORD:
                ll_Change_Email.setVisibility(View.GONE);
                ll_change_password.setVisibility(View.VISIBLE);
                tvPassError.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private boolean checkForBlankFields() {
        oldPass = etOldPass.getText().toString().trim();
        NewPass = etNewPass.getText().toString().trim();
        ConfirmPass = etConfirmPass.getText().toString().trim();
        if (!oldPass.equals("") && !NewPass.equals("") && !ConfirmPass.equals(""))
            return false;
        else {
            if (etOldPass.getText().toString().trim().equals("")) {
                etOldPass.setError(getResources().getString(R.string.please_enter_valid_value));
                return true;
            } else {
                etOldPass.setError("");
            }

            if (etNewPass.getText().toString().trim().equals("")) {
                etNewPass.setError(getResources().getString(R.string.please_enter_valid_value));
                return true;
            } else
                etNewPass.setError("");

            if (etConfirmPass.getText().toString().trim().equals("")) {
                etConfirmPass.setError(getResources().getString(R.string.please_enter_valid_value));
                return true;
            } else {
                etConfirmPass.setError("");
            }

            return true;
        }
    }

    private String validPassword() {
        oldPass = etOldPass.getText().toString().trim();
        NewPass = etNewPass.getText().toString().trim();
        ConfirmPass = etConfirmPass.getText().toString().trim();
        if (oldPass.equals("") || NewPass.equals("") || ConfirmPass.equals(""))
            return getResources().getString(R.string.please_enter_password);
        else if (!NewPass.equals(ConfirmPass))
            return getResources().getString(R.string.mismatch_new_and_confirm_Password);

        else if (!Utils.isPasswordMatch(NewPass)) {
            return getResources().getString(R.string.incorrect_password_pattern);
        } else
            return "";
    }

    public boolean isValidationsSuccessful() {
        hideKeyboard();
        oldPass = etOldPass.getText().toString().trim();
        NewPass = etNewPass.getText().toString().trim();
        ConfirmPass = etConfirmPass.getText().toString().trim();
        boolean valueToReturn = true;
        if (!Utils.isValueAvailable(oldPass)) {
            etOldPass.setError(getResources().getString(R.string.please_enter_password));
            valueToReturn = false;
        } else if (!Utils.isValueAvailable(NewPass)) {
            etNewPass.setError(getResources().getString(R.string.please_enter_password));
            valueToReturn = false;
        } else if (!Utils.isValueAvailable(ConfirmPass)) {
            etConfirmPass.setError(getResources().getString(R.string.please_enter_password));
            valueToReturn = false;
        } else if (!Utils.isPasswordMatch(NewPass)) {
            etNewPass.setError(getResources().getString(R.string.incorrect_password_pattern));
            valueToReturn = false;
        }
        if (valueToReturn == false)
            return false;
        if (!etNewPass.getText().toString().equals(ConfirmPass)) {
            etConfirmPass.setError(getResources().getString(R.string.password_mismatch));
            return false;
        }
        return true;
    }

    public void onClick(View view) {
        initializeUserMo();
        Utils.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.btn_email_done:
                error = validateEmail(userMo.getEmail());
                if (error.equals("")) {
                    if (Utils.showDialogForNoNetwork(this, false)) {
                        new CheckIfEmailExistsBG(email, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    etEmail.setError("");
                } else {
                    //showSnackBar(error);
                    etEmail.setError(error);
                }
                break;

            case R.id.btn_changePassword_submit:
//                error = validPassword();
//                if (!checkForBlankFields()) {
//                    if (error.equals("") && Utils.validatePassword(NewPass, this).equals("")) {
//                        if (Utils.showDialogForNoNetwork(ChangeDetails.this, false)) {
//                            new changePasswordBG(false, oldPass, NewPass, this).execute();
//                        }
//                    } else {
//                        if (etNewPass.getError().equals(""))
//                            showSnackBar(error);
//                    }
//
//
//                }

                if (isValidationsSuccessful() && Utils.showDialogForNoNetwork(ChangeDetails.this, false)) {
                    new changePasswordBG(false, oldPass, NewPass, this).execute();
                }
        }
    }

    private void showprogressdialog() {
        pd_ring = new ProgressDialog(this);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.hangon));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
        pd_ring.show();

    }

    private void getPatientInfo(String id) {
        showprogressdialog();
        patientMo = SettingManager.getInstance().getPatientMo(id);

        if (patientMo == null)
            new getPatientShowDetails(this, id).execute();
        else
            sendApiForUpdate();

    }

    private void sendApiForUpdate() {
        UpdatePatientRequest req = new UpdatePatientRequest();
        userdata = new User();
        userdata.setFirstName(userMo.getFirstName());
        userdata.setEmail(userMO.getEmail());
        userdata.setLastName(userMo.getLastName());
        userdata.setWeight(userMo.getWeight());
        userdata.setHeight(userMo.getHeight());
        String dob = userMO.getDateOfBirth() != null ? (userMO.getDateOfBirth().contains(":") ? Utils.getFormattedDate(userMO.getDateOfBirth(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated, DateTimeUtil.destinationDateFormatWithoutTime) : userMO
                .getDateOfBirth()) : "";
        userdata.setDob(dob);
        userdata.setGender(userMo.getGender());
        userdata.setPhoneNo(SharedPref.getMobileNumber());
        userdata.setId(userMo.getId());
        userdata.setUnverifiedEmail(email);
        req.setUser(userdata);
        req.setUserType(TextConstants.PATIENT);
        req.setPatient(patientMo);
        new updateUserProfileBG(this, req, isChangePassword).execute();
    }

    public void executePatientCallResult(PatientMO result) {
        if (result != null) {
            patientMo = result;
            sendApiForUpdate();
        } else {
            pd_ring.dismiss();
            showSnackBar(MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again));
        }
    }


    public void executeUpdateProfileResult(updatePatientResponse result, boolean isPasswordUpdated) {
        // TODO Auto-generated method stub
        if (result != null) {
            pd_ring.dismiss();

            SettingManager.getInstance().savePatientMo(result.getPatient());
            pd_ring.dismiss();
            showSnackBar(MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again));
        } else {
            pd_ring.dismiss();
            showSnackBar(MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again));
        }
    }

    private void showSnackBar(String error) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.WHITE);
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.RED);
        if (!error.equals(""))
            snackbar.show();
    }


    private void showDialogforEmail() {
        // TODO Auto-generated method stub
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setMessage(MyApplication.getAppContext().getResources().getString(R.string.email_sent_validate_button));
        context = ChangeDetails.this;
        dialog.setPositiveButton(MyApplication.getAppContext().getResources().getString(R.string.ok_got_it), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub
                if (Utils.showDialogForNoNetwork(ChangeDetails.this, false)) {
                    Intent i = getIntent();
                    i.putExtra(VariableConstants.NEW_EMAIL, email);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }

        });
        dialog.show();
    }

    private String validateEmail(String oldEmail) {
        email = etEmail.getText().toString().trim().toLowerCase();
        if (email.equals(""))
            return getResources().getString(R.string.enter_valid_email_address);
        else if (email.equalsIgnoreCase(oldEmail))
            return getResources().getString(R.string.new_email_cannot_same_old_one);
        else if (!validateMail(email))
            return getResources().getString(R.string.enter_valid_email_address);
        else
            return "";
    }

    private boolean validateMail(String email) {
        String reg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,4})$";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(email);
        if (m.matches())
            return true;
        else
            return false;
    }


    public void executeCheckEmailExistResult(CheckEmailExistGson response, String result) {
        // TODO Auto-generated method stub
        if (result.equals(TextConstants.SUCESSFULL_API_CALL)) {
         /*Intent i = getIntent();
         i.putExtra(VariableConstants.NEW_EMAIL, email);
         setResult(Activity.RESULT_OK, i);
         finish();*/

            getPatientInfo(userMo.getPatientId() + "");

        } else if (result.equals(TextConstants.EMAIL_NOT_UNIQUE))
            showDialogforPhoneEmailUnique(getResources().getString(R.string.already_registered_user_this_email));

        else
            showSnackBar(result);


    }

    private void showDialogforPhoneEmailUnique(String phoneNoNotUniqueMsg) {
        // TODO Auto-generated method stub
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setMessage(phoneNoNotUniqueMsg);
        dialog.setPositiveButton(MyApplication.getAppContext().getResources().getString(R.string.ok_got_it), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                etEmail.requestFocus();
            }

        });

        dialog.show();

    }

    public void executeChangePassword(ForgetPassOtpVerifyResponse response, String msg) {

        if (msg.equals(TextConstants.SUCESSFULL_API_CALL)) {

            try {
                String temp_Pass = URLEncoder.encode(NewPass, "UTF-8");
                userMo.setPassword(temp_Pass);
            } catch (Exception e) {
            }
            try {

                SettingManager.getInstance().updateUserMO(userMo);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            ChangeDetails.this.finish();
        } else {
            if (response != null && (response.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_422)) {
                if (response.getMsg() != null) {
                    if (response.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_LENGTH))
                        msg = getString(R.string.password_with_length_error, (SharedPref.getPasswordLength() + ""));
                    else if (response.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_PATTERN))
                        msg = getString(R.string.password_pattern_error);
                    else if (response.getMsg().equals(TextConstants.OLD_PASSWORD_INVALID))
                        msg = getString(R.string.old_password_incorrect);
                    else if (response.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() == 0)
                        msg = getString(R.string.last_password_mismatch);
                    else if (response.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() != 0)
                        msg = getString(R.string.last_n_password_mismatch, SharedPref.getPasswordHistoryCount() + "");
                    else
                        msg = TextConstants.UNEXPECTED_ERROR;
                } else
                    msg = TextConstants.UNEXPECTED_ERROR;
                showSnackBar(msg);
            } else if (response != null && (response.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_423)
                    && response.getMsg() != null && response.getMsg().equals(TextConstants.NEW_PASSWORD_INVALID)) {
                showSnackBar(getString(R.string.same_old_new_pass));
            } else
                showSnackBar(msg);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}