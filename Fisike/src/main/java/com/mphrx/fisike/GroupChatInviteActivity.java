package com.mphrx.fisike;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mphrx.fisike.customview.CustomFontTextView;

import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.mphrx.fisike.adapter.GroupInviteAdapter;
import com.mphrx.fisike.adapter.GroupInviteAdapter.ViewHolder;
import com.mphrx.fisike.adapter.SelectedInviteeAdapter;
import com.mphrx.fisike.connection.ConnectionInfo;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.gson.request.ContactListArray;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.SettingMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike.services.MessengerService.MessengerBinder;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.ClearableEditText;
import com.mphrx.fisike.view.ErrorMessage;
import com.mphrx.fisike_physician.utils.AppLog;

public class GroupChatInviteActivity extends BaseActivity implements OnItemClickListener, OnScrollListener, OnClickListener {
    private GroupInviteAdapter groupInviteAdapter;
    private SelectedInviteeAdapter selectedInviteeAdapter;
    private ListView inviteListView, selectedListView;
    private CustomFontTextView txtNoMoreHistory;
    private CustomFontTextView tvDoneLoading;
    private LinearLayout loadingViewContainer;
    private ProgressBar loadingProgressBar, progressBarAddMember;
    private static ProgressBar progressBar;
    private static LinearLayout llContainer;
    private ViewSwitcher switcher;

    private ArrayList<ChatContactMO> contacts;

    private ArrayList<ChatContactMO> alreadyAddedContacts;
    private int alreadyAddedContactSize = 0;

    private ClearableEditText etSearch;
    private View loadingView;
    private Bitmap profilePic;

    private String groupName;
    private String groupId;
    private String jsonString = "";
    private String searchString = "";
    private boolean isLoadMoreConnectionError;
    private boolean launchedFromGroupDetailActivity = false;

    private List<ChatContactMO> invitelist;

    private UserMO userMO;

    private boolean isScrollDownError = false;
    private boolean isLoadingContacts = false;
    private boolean loadMore;
    private boolean loadingMore;
    private int pageNum = 0;
    private com.mphrx.fisike.customview.CustomFontButton addMemberButton;

    private AsyncTask<Void, Void, Void> loadingContacts;

    private XMPPConnection connection;
    private ConnectionInfo connectionInfo;

    public static final int GROUP_MEMBER_SIZE_LIMIT = 50;

    private CustomFontTextView tvSelectedInviteCount;
    private Toolbar toolbarGroupChatInvite;
    private static MenuItem doneItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_group_chat_invite);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_group_chat_invite, frameLayout);
        // Bind the MessengerService
        contacts = new ArrayList<ChatContactMO>();
        toolbarGroupChatInvite = (Toolbar) frameLayout.findViewById(R.id.toolbar_group_chat_invite);

        setSupportActionBar(toolbarGroupChatInvite);
        toolbarGroupChatInvite.setNavigationIcon(R.drawable.ic_back_w_shadow);
        getIntentData();
        inviteListView = (ListView) findViewById(R.id.invite_list);
        selectedListView = (ListView) findViewById(R.id.selected_list);
        txtNoMoreHistory = (CustomFontTextView) findViewById(R.id.tv_no_more_history);
        switcher = (ViewSwitcher) findViewById(R.id.switcher);
        addMemberButton = (com.mphrx.fisike.customview.CustomFontButton) findViewById(R.id.add_members);
        addFooterView();
        etSearch = (ClearableEditText) findViewById(R.id.et_search);
        etSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        inviteListView.setOnItemClickListener(this);
        registerSearchOptionClick();
        // Assign adapter to ListView
        inviteListView.setAdapter(groupInviteAdapter);

        progressBarAddMember = (ProgressBar) findViewById(R.id.progress_bar_add_member);
        inviteListView.setOnScrollListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        llContainer = (LinearLayout) findViewById(R.id.ll_container);
        if (!isLoadingContacts) {
            runLoadContactsAsyncTask();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (switcher.getDisplayedChild() == 0) {
                    if (loadingContacts != null && !loadingContacts.isCancelled()) {
                        loadingContacts.cancel(true);
                    }
                    cancelButtonClicked(doneItem);
                } else {
                    // decide to move to groupDetail Activity/NewGroupActivity
                    if (launchedFromGroupDetailActivity) {
                        finish();
                    } else {
                        moveToNewGroupActivity();
                    }
                }
                break;

            case R.id.action_done:
                handleDoneButton();
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.group_chat_invite, menu);
        doneItem = menu.findItem(R.id.action_done);
        return super.onCreateOptionsMenu(menu);
    }

    private int getSelectedMembersCount() {
        if (groupInviteAdapter.getInvitees() != null) {
            return groupInviteAdapter.getInvitees().size() + alreadyAddedContactSize;
        }
        return alreadyAddedContactSize;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //code cleanup commenting iv_logo: iv_logo view was part of login.xml
            // which is nowhere in its xml
           /* case R.id.iv_logo:
                if (switcher.getDisplayedChild() == 0) {
                    if (loadingContacts != null && !loadingContacts.isCancelled()) {
                        loadingContacts.cancel(true);
                    }
                    cancelButtonClicked(doneItem);
                } else {
                    // decide to move to groupDetail Activity/NewGroupActivity
                    if (launchedFromGroupDetailActivity) {
                        finish();
                    } else {
                        moveToNewGroupActivity();
                    }
                }
                break;*/
//            case R.id.tv_selected_member_count:
//                Toast.makeText(this, "selected member count", Toast.LENGTH_LONG).show();
//                break;
//            case R.id.tv_done:
//                handleDoneButton();
//                break;
            default:
                break;
        }
    }

    public void registerSearchOptionClick() {
        etSearch.addEditorActionListener(new CustomFontTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performsearch();
                    return true;
                }
                return false;
            }

        });
    }

    private void addFooterView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        loadingView = inflater.inflate(R.layout.loading_view, null);
        loadingProgressBar = (ProgressBar) loadingView.findViewById(R.id.loading_progress_bar);
        tvDoneLoading = (CustomFontTextView) loadingView.findViewById(R.id.tv_done_loading);
        loadingViewContainer = (LinearLayout) loadingView.findViewById(R.id.ll_loading_view_container);
        inviteListView.addFooterView(loadingView);
    }

    private void runLoadContactsAsyncTask() {
        // api call
        if (userMO == null) {
            userMO = SettingManager.getInstance().getUserMO();
        }
        if (Utils.showDialogForNoNetwork(this, false)) {
            loadingContacts = new LoadContactsAsyncTask(userMO.getUsername(), userMO.getPassword(), true)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    @Override
    protected void onResume() {
        if (userMO == null) {
            userMO = SettingManager.getInstance().getUserMO();
        }
        registerBroadcastReceiver();
        super.onResume();
    }

    private void registerBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(groupCreatedBroadcast, new IntentFilter(VariableConstants.GROUP_INVITE_SENT));
    }

    private BroadcastReceiver groupCreatedBroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (progressBar.isShown()) {
                // need to have a check whether group is successfully created
                if (intent.getBooleanExtra(VariableConstants.LAUNCH_GROUP_ROSTER, false)) {
                    launchGroupRosterScreen();
                } else if (intent.getBooleanExtra(VariableConstants.GROUP_CREATION_ERROR, false)) {
                    // show error dialog.
                    showErrorDialog();
                } else {
                    // move to group details screen
                    Intent intent1 = new Intent();
                    intent1.putExtra(VariableConstants.INVITE_LIST_ADDED_MEMBERS, (ArrayList<ChatContactMO>) invitelist);
                    GroupChatInviteActivity.this.setResult(RESULT_OK, intent1);
                    GroupChatInviteActivity.this.finish();
                }
                dismissProgress();
            }
        }
    };

    private void launchGroupRosterScreen() {
        Intent intent = getIntent();
        intent.putExtra(VariableConstants.IS_GROUP_CHAT, true);
        this.setResult(RESULT_OK, intent);
        this.finish();
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle(getResources().getString(R.string.error));
        builder1.setMessage(getResources().getString(R.string.Something_went_wrong_Please_try_again));
        builder1.setCancelable(true);
        builder1.setPositiveButton(getResources().getString(R.string.txt_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(GroupChatInviteActivity.this, NewGroupActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                dialog.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void getIntentData() {
        try {
            Intent intent = getIntent();
            groupName = intent.getStringExtra(TextConstants.NEW_GROUP_NAME_INTENT);
            profilePic = intent.getParcelableExtra(TextConstants.NEW_GROUP_IMAGE_INTENT);
            groupId = intent.getStringExtra(VariableConstants.GROUP_ID_INTENT);
            launchedFromGroupDetailActivity = intent.getBooleanExtra(VariableConstants.LAUNCHED_FROM_GROUP_DETAIL_ACTIVITY, false);
            ArrayList<ChatContactMO> inviteList = intent.getParcelableArrayListExtra(TextConstants.SELECTED_INVITE_INTENT);

            alreadyAddedContacts = intent.getParcelableArrayListExtra(VariableConstants.ALREADY_ADDED_CONTACTS);
            if (alreadyAddedContacts != null && alreadyAddedContacts.size() > 0) {
                // this list includes the admin as well thats why (size()-1)
                alreadyAddedContactSize = alreadyAddedContacts.size() - 1;
            }
            groupInviteAdapter = new GroupInviteAdapter(this, contacts, alreadyAddedContactSize, getSupportActionBar());
            groupInviteAdapter.setSelectedInvites(inviteList);

            getSupportActionBar().setTitle(groupName);
            getSupportActionBar().setSubtitle(
                    getResources().getQuantityString(R.plurals.number_of_contacts_selected, getSelectedMembersCount(), getSelectedMembersCount()));

			/*
             * tvGroupName.setText(groupName); tvSelectedInviteCount.setText(getResources().getQuantityString(R.plurals.number_of_contacts_selected,
			 * getSelectedMembersCount(), getSelectedMembersCount()));
			 */
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<ChatContactMO> getSelectedInvites() {
        List<ChatContactMO> selectedinvitesList = new ArrayList<ChatContactMO>();
        for (ChatContactMO member : groupInviteAdapter.getInviteList()) {
            if (member.getIsSelected()) {
                selectedinvitesList.add(member);
            }
        }
        return selectedinvitesList;
    }

    private void resetCancelledContacts() {
        for (ChatContactMO member : groupInviteAdapter.getInviteList()) {
            member.setIsSelected(false);
        }
    }

    public void performsearch() {
        if (getSelectedInvites() != null && getSelectedInvites().size() > 0) {
            groupInviteAdapter.setSelectedInvites(getSelectedInvites());
        }
        searchString = etSearch.getText().toString().trim();
        pageNum = 0;
        try {
            if (null != contacts && contacts.size() > 0) {
                contacts.clear();
            }
        } catch (Exception e) {

        }
        txtNoMoreHistory.setVisibility(View.GONE);
        if (null != loadingContacts && !loadingContacts.isCancelled()) {
            loadingContacts.cancel(true);
        }
        isLoadingContacts = false;
        if (!loadMore) {
            loadingViewContainer.setVisibility(View.VISIBLE);
            tvDoneLoading.setVisibility(View.GONE);
        }
        nextPage(true);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    public void performsearchAddMembers(ViewSwitcher switcher) {
        if (getSelectedInvites() != null && getSelectedInvites().size() > 0) {
            groupInviteAdapter.setSelectedInvites(getSelectedInvites());
        }
        searchString = etSearch.getText().toString().trim();
        pageNum = 0;
        try {
            if (null != contacts && contacts.size() > 0) {
                contacts.clear();
            }
        } catch (Exception e) {

        }
        txtNoMoreHistory.setVisibility(View.GONE);
        if (null != loadingContacts && !loadingContacts.isCancelled()) {
            loadingContacts.cancel(true);
        }
        isLoadingContacts = false;
        if (!loadMore) {
            loadingViewContainer.setVisibility(View.VISIBLE);
            tvDoneLoading.setVisibility(View.GONE);
        }
        addMemberSearch(switcher);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    @Override
    protected void onPause() {
        unregisterLocalBroadCastReceiver();
        super.onPause();
    }

    private void unregisterLocalBroadCastReceiver() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(groupCreatedBroadcast);
        } catch (Exception e) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (view.getId() == R.id.loadingView) {
            return;
        }
        GroupInviteAdapter.ViewHolder holder = groupInviteAdapter.new ViewHolder();
        holder = (ViewHolder) view.getTag();
        Boolean isChecked = !holder.chequeBox.isChecked();
        if (isChecked) {
            // perform check on number of members
            if (groupInviteAdapter.getInvitees() != null
                    && groupInviteAdapter.getInvitees().size() + alreadyAddedContactSize >= GROUP_MEMBER_SIZE_LIMIT - 1) {
                Toast.makeText(this, getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG).show();
                return;
            }
        }
        ChatContactMO member = (ChatContactMO) parent.getItemAtPosition(position);
        ChatContactMO contactMo = (ChatContactMO) holder.chequeBox.getTag();
        ArrayList<ChatContactMO> list = new ArrayList<ChatContactMO>();
        list.add(contactMo);

        if (isChecked) {
            groupInviteAdapter.setSelectedInvites(list);
        } else {
            if (groupInviteAdapter.getInvitees().contains(contactMo)) {
                groupInviteAdapter.getInvitees().remove(contactMo);
            }
        }
        getSupportActionBar().setSubtitle(
                getResources().getQuantityString(R.plurals.number_of_contacts_selected, getSelectedMembersCount(), getSelectedMembersCount()));
        holder.chequeBox.setChecked(isChecked);
    }

    private void handleDoneButton() {
        invitelist = new ArrayList<ChatContactMO>();
        if (getSelectedInvites() != null && getSelectedInvites().size() > 0) {
            invitelist.addAll(getSelectedInvites());
        }
        if (groupInviteAdapter.getInvitees() != null && groupInviteAdapter.getInvitees().size() > 0) {
            invitelist.addAll(groupInviteAdapter.getInvitees());
        }
        invitelist = removeDuplicates(invitelist);
        if (invitelist.size() == 0) {
            Toast.makeText(this, getResources().getString(R.string.Please_select_at_least_one_member), Toast.LENGTH_LONG).show();
        }
        if (invitelist.size() + alreadyAddedContactSize > GROUP_MEMBER_SIZE_LIMIT) {
            Toast.makeText(this, getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG).show();
            return;
        } else if (mBound && invitelist.size() != 0) {
            if (switcher.getDisplayedChild() == 0) {
                switcher.setDisplayedChild(1);
                // add check here
                if (groupInviteAdapter.getInvitees().size() + alreadyAddedContactSize >= GROUP_MEMBER_SIZE_LIMIT - 1) {
                    addMemberButton.setVisibility(View.GONE);
                } else {
                    addMemberButton.setVisibility(View.VISIBLE);
                }
                if (launchedFromGroupDetailActivity) {
                    doneItem.setTitle(getResources().getString(R.string.invite_member));
                    getSupportActionBar().setTitle(getResources().getString(R.string.members));
                } else {
                    doneItem.setTitle(getResources().getString(R.string.create));
                    getSupportActionBar().setTitle(getResources().getString(R.string.members));
                }
                selectedInviteeAdapter = new SelectedInviteeAdapter(this, removeDuplicates(invitelist), groupInviteAdapter, addMemberButton,
                        getSupportActionBar());

                selectedListView.setAdapter(selectedInviteeAdapter);
            } else {
                connectionInfo = ConnectionInfo.getInstance();
                connection = connectionInfo.getXmppConnection();
                if (Utils.showDialogForNoNetwork(this, true)) {
                    showProgress();
                    switcher.setDisplayedChild(0);
                    // add checks here
                    if (launchedFromGroupDetailActivity) {
                        mService.sendInvite(invitelist, groupId);
                    } else {
                        // create the group
                        String groupID = System.currentTimeMillis() + "_" + userMO.getId();
                        mService.createNewGroup(groupName, invitelist, profilePic, true, groupID, false);
                    }
                }
            }
        }
    }

    private List<ChatContactMO> removeDuplicates(List<ChatContactMO> list) {
        list.size();
        List<ChatContactMO> finalList = new ArrayList<ChatContactMO>();
        for (ChatContactMO contactMO : list) {
            if (!finalList.contains(contactMO)) {
                finalList.add(contactMO);
            }
        }
        finalList.size();
        return finalList;
    }

    /**
     * This method is fired when the activity is destroyed
     */
    @Override
    protected void onDestroy() {
        unbindMessengerService();
        groupInviteAdapter.clearSDCard();
        super.onDestroy();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!loadMore) {
            return;
        }

        if (firstVisibleItem == visibleItemCount) {
            return;
        }

        if (((firstVisibleItem + visibleItemCount) == totalItemCount) && !(loadingMore)) {
            if (!isLoadingContacts) {
                // nextPage(true);
                nextPage(true, isLoadMoreConnectionError);
                if (!Utils.isNetworkAvailable(this)) {
                    isLoadMoreConnectionError = true;
                } else {
                    isLoadMoreConnectionError = false;
                }
            }
        }

    }

    private void nextPage(boolean isOnclick) {
        if (!isLoadingContacts) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                loadingContacts = new LoadContactsAsyncTask(userMO.getUsername(), userMO.getPassword(), isOnclick)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                isLoadMoreConnectionError = false;
            }
        }
    }

    private void nextPage(boolean isOnclick, boolean isLoadMoreConnectionError) {
        if (!isLoadingContacts && !isLoadMoreConnectionError) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                loadingContacts = new LoadContactsAsyncTask(userMO.getId() + "", userMO.getPassword(), isOnclick).execute();
            }
        }
    }

    private void addMemberSearch(ViewSwitcher switcher) {
        if (Utils.showDialogForNoNetwork(this, false)) {
            loadingContacts = new LoadContactsAsyncTask(userMO.getId() + "", userMO.getPassword(), true, true, switcher)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            isLoadMoreConnectionError = false;
        }

    }

    public void showError(final boolean isOnclick) {
        loadingMore = false;
        String txtMessage = TextConstants.UNEXPECTED_ERROR;
        if (!Utils.isNetworkAvailable(this)) {
            txtMessage = MyApplication.getAppContext().getResources().getString(R.string.connection_not_available);
            if (isOnclick) {
                ErrorMessage localErrorMessage = new ErrorMessage(GroupChatInviteActivity.this, R.id.layoutParent);
                localErrorMessage.showMessage(txtMessage);
                isScrollDownError = true;
            }
        }
    }

    private void showErrorInvalidUserName() {
        ErrorMessage localErrorMessage = new ErrorMessage(GroupChatInviteActivity.this, R.id.layoutParent);
        localErrorMessage.showMessage(MyApplication.getAppContext().getResources().getString(R.string.oops_invalid_user_credentials), false);
    }

    @Override
    public void changeOnConnectionAvalable() {
        if (isScrollDownError || groupInviteAdapter.getCount() == 0) {
            isScrollDownError = false;
            nextPage(false);
            loadingView.setVisibility(View.VISIBLE);
        }
        super.changeOnConnectionAvalable();
    }

    @Override
    public void onBackPressed() {
        if (switcher.getDisplayedChild() == 0) {
            cancelButtonClicked(doneItem);
            if (loadingContacts != null && !loadingContacts.isCancelled()) {
                loadingContacts.cancel(true);
            }
        } else {
            try {
                if (loadingContacts != null && !loadingContacts.isCancelled()) {
                    loadingContacts.cancel(true);
                }
                if (launchedFromGroupDetailActivity) {
                    finish();
                } else {
                    moveToNewGroupActivity();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                contacts.clear();
                groupInviteAdapter.clearSDCard();
            }
            finish();
            super.onBackPressed();
        }

    }

    public void moveToNewGroupActivity() {
        Intent newGroupActivityIntent = new Intent(this, NewGroupActivity.class);
        newGroupActivityIntent.putExtra(TextConstants.NEW_GROUP_NAME_INTENT, groupName);
        newGroupActivityIntent.putExtra(TextConstants.NEW_GROUP_IMAGE_INTENT, profilePic);
        List<ChatContactMO> list = getSelectedInvites();
        if (groupInviteAdapter.getInvitees() != null && groupInviteAdapter.getInvitees().size() > 0) {
            list.addAll(groupInviteAdapter.getInvitees());
        }
        list = removeDuplicates(list);
        newGroupActivityIntent.putParcelableArrayListExtra(TextConstants.SELECTED_INVITE_INTENT, (ArrayList<ChatContactMO>) list);
        startActivity(newGroupActivityIntent);
    }

    public void moveTomGroupDetailActivity() {
        Intent groupDetailIntent = new Intent(this, GroupDetailActivity.class);
        groupDetailIntent.putExtra(TextConstants.NEW_GROUP_NAME_INTENT, groupName);
        startActivity(groupDetailIntent);
    }

    public void cancelButtonClicked(MenuItem item) {
        List<ChatContactMO> list = new ArrayList<ChatContactMO>();
        resetCancelledContacts();
        switcher.setDisplayedChild(1);
        if (doneItem != null) {
            if (launchedFromGroupDetailActivity) {
                doneItem.setTitle(getResources().getString(R.string.invite_member));
                getSupportActionBar().setTitle(getResources().getString(R.string.members));
            } else {
                doneItem.setTitle(getResources().getString(R.string.create));
                getSupportActionBar().setTitle(getResources().getString(R.string.members));
            }
        }
        getSupportActionBar().setSubtitle(getResources().getQuantityString(R.plurals.number_of_contacts_selected, 0, 0));
        getSupportActionBar().setTitle(getResources().getString(R.string.members));
        groupInviteAdapter.clearSelectedInvites();
        selectedInviteeAdapter = new SelectedInviteeAdapter(this, list, groupInviteAdapter, addMemberButton, getSupportActionBar());
        selectedListView.setAdapter(selectedInviteeAdapter);
    }

    public void launchGroupInviteMemberScreen(MenuItem item) {
        List<ChatContactMO> invitelist = new ArrayList<ChatContactMO>();

        if (getSelectedInvites() != null && getSelectedInvites().size() > 0) {
            invitelist.addAll(getSelectedInvites());
        }
        if (groupInviteAdapter.getInvitees() != null && groupInviteAdapter.getInvitees().size() > 0) {
            invitelist.addAll(groupInviteAdapter.getInvitees());
        }
        invitelist = removeDuplicates(invitelist);
        if (invitelist.size() + alreadyAddedContactSize < GROUP_MEMBER_SIZE_LIMIT) {
            switcher.setDisplayedChild(1);
            if (doneItem != null) {
                if (launchedFromGroupDetailActivity) {
                    doneItem.setTitle(getResources().getString(R.string.invite_member));
                    getSupportActionBar().setTitle(getResources().getString(R.string.members));
                } else {
                    doneItem.setTitle(getResources().getString(R.string.create));
                    getSupportActionBar().setTitle(getResources().getString(R.string.members));
                }
            }
            getSupportActionBar().setTitle(getResources().getString(R.string.members));
            selectedInviteeAdapter = new SelectedInviteeAdapter(this, removeDuplicates(invitelist), groupInviteAdapter, addMemberButton,
                    getSupportActionBar());
            selectedListView.setAdapter(selectedInviteeAdapter);
        } else {
            Toast.makeText(this, getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG).show();
        }
    }

    public class LoadContactsAsyncTask extends AsyncTask<Void, Void, Void> {

        private String userName;
        private String password;
        private boolean isOnClick;
        private boolean addMembers = false;
        private ViewSwitcher viewSwitcher;

        public LoadContactsAsyncTask(String userName, String password, Boolean isOnClick) {
            this.userName = userName;
            this.password = password;
            this.isOnClick = isOnClick;
        }

        public LoadContactsAsyncTask(String userName, String password, Boolean isOnClick, Boolean addMembers, ViewSwitcher switcher) {
            this.userName = userName;
            this.password = password;
            this.isOnClick = isOnClick;
            this.addMembers = addMembers;
            this.viewSwitcher = switcher;
        }

        @Override
        protected void onPreExecute() {
            isLoadingContacts = true;
            loadMore = true;
            if (loadingView != null) {
                loadingView.setVisibility(View.VISIBLE);
            } else {
                addFooterView();
            }
            if (addMembers) {
                switcher.setDisplayedChild(0);
                tvSelectedInviteCount.setVisibility(View.GONE);
                progressBarAddMember.setVisibility(View.VISIBLE);
                inviteListView.setVisibility(View.GONE);
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                loadingMore = true;
                SettingManager settingManager = SettingManager.getInstance();
                SettingMO setMO = settingManager.getSettings();
                boolean useHTTPS = setMO.isUseHTTPS();

                String httpStr = "https://";
                if (!useHTTPS)
                    httpStr = "http://";

                String searchText = URLEncoder.encode(searchString.trim(), "UTF-8");
                searchText = searchText.replaceAll("\\+", "%20");
                final String url = APIManager.createMinervaBaseUrl() + APIManager.API_USER + "/getContactList";
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("_count", 10);
                jsonObject.put("_skip", pageNum);
                jsonObject.put("searchText", searchText);

                APIManager jsonManager = APIManager.getInstance();
                jsonString = jsonManager.executeHttpPost(url, "", jsonObject, GroupChatInviteActivity.this);
                /*
				 * final String postURL = httpStr + setMO.getServerIP() + ":" + setMO.getServerPort() + "/fisike/userApi/getContactList?q=" +
				 * searchText + "&offset=" + pageNum + "&username=" + URLDecoder.decode(userName, "UTF-8") + "&password=" + password + "&max=10";
				 * 
				 * APIManager jsonManager = APIManager.getInstance(); jsonString = jsonManager.executeUrlResponse(postURL);
				 */
                if (jsonString != null || !(jsonString.equals(""))) {
                    parseJson(jsonString);
                }

            } catch (Exception e) {
                jsonString = null;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            isLoadingContacts = false;
            if (contacts.size() == 0) {
                txtNoMoreHistory.setVisibility(View.VISIBLE);
                loadingView.setVisibility(View.GONE);
            } else {
                txtNoMoreHistory.setVisibility(View.GONE);
            }
            if (jsonString != null && !(jsonString.equals(""))) {
                loadContacts();
                if (loadMore) {
                    pageNum += 10;
                    loadingView.setVisibility(View.GONE);
                } else {
                    loadingViewContainer.setVisibility(View.GONE);
                    tvDoneLoading.setVisibility(View.VISIBLE);
                }

            } else {
                loadingView.setVisibility(View.GONE);
                handleError(isOnClick);
            }
            if (!Utils.isNetworkAvailable(GroupChatInviteActivity.this)) {
                loadingView.setVisibility(View.GONE);
            }
            if (addMembers) {
                tvSelectedInviteCount.setVisibility(View.VISIBLE);
                progressBarAddMember.setVisibility(View.GONE);
                inviteListView.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(result);
        }

    }

    public void handleError(boolean isOnclick) {
        loadingMore = false;
        ErrorMessage localErrorMessage = new ErrorMessage(GroupChatInviteActivity.this, R.id.layoutParent);
        localErrorMessage.showMessage(getResources().getString(R.string.unable_to_contact_server));
        isScrollDownError = true;

    }

    private void loadContacts() {
        try {
            if (jsonString.contains(TextConstants.ERROR_INVALID_USER_NAME)) {
                showErrorInvalidUserName();
                if (contacts.size() == 0) {
                    txtNoMoreHistory.setVisibility(View.VISIBLE);
                    loadingView.setVisibility(View.GONE);
                } else {
                    txtNoMoreHistory.setVisibility(View.GONE);
                }
                return;
            }
            if (contacts.size() == 0) {
                txtNoMoreHistory.setVisibility(View.VISIBLE);
                loadingView.setVisibility(View.GONE);
            } else {
                txtNoMoreHistory.setVisibility(View.GONE);
            }

            groupInviteAdapter.setInviteMemberListAdapter(contacts);
            loadingMore = false;

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public ArrayList<ChatContactMO> parseJson(String jsonResponse) throws JSONException, UnsupportedEncodingException {
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(jsonString, ContactListArray.class);
        if (jsonToObjectMapper instanceof ContactListArray) {
            contacts.addAll(((ContactListArray) jsonToObjectMapper).getResultList());

        }
        if (contacts.size() == 0) {
            loadingMore = false;
            return null;
        }
        return contacts;
    }

    public static void showProgress() {
        doneItem.setVisible(false);
        progressBar.setVisibility(View.VISIBLE);
        llContainer.setVisibility(View.GONE);
    }

    public static void dismissProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void add(View v) {
        try {
            etSearch.clearEditText();
            if (groupInviteAdapter.getInvitees().size() + alreadyAddedContactSize > GROUP_MEMBER_SIZE_LIMIT - 1) {
                Toast.makeText(this, getResources().getString(R.string.group_member_limit_exceeded), Toast.LENGTH_LONG).show();
                return;
            }

            if (!isLoadingContacts) {
                performsearchAddMembers(switcher);
            }
            doneItem.setTitle(getResources().getString(R.string.group_done));
            getSupportActionBar().setTitle(groupName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}