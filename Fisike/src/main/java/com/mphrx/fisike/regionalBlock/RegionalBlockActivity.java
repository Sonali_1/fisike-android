package com.mphrx.fisike.regionalBlock;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.services.MessengerService;
import com.mphrx.fisike_physician.activity.NetworkActivity;

import static android.util.Log.d;

/**
 * Created by laxmansingh on 1/23/2017.
 */

public class RegionalBlockActivity extends NetworkActivity implements View.OnClickListener {

/*

    Intent intent = new Intent(VariableConstants.LOGOUT_IF_AUTH_TOKEN_NULL);
    intent.putExtra(TextConstants.REGIONAL_BLOCK, true);
    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

*/

    private Context mContext;
    private CustomFontButton BtnEmailNow, BtnNoThanks;
    private CustomFontTextView tvContactAdmin;
    private IconTextView imgHeader;
    protected MessengerService mService;
    protected boolean mBound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_admin);
        mContext = this;
        findViews();
        bindView();
        initView();
    }

    private void initView() {
        BtnEmailNow.setText(mContext.getResources().getString(R.string.try_again));
        if (BuildConfig.isIcomoonForSplashEnabled) {
            imgHeader.setText(R.string.fa_fisike_icon);
            imgHeader.setTextSize(100f);
        } else {
            imgHeader.setBackgroundResource(R.drawable.fisike_icon);
            imgHeader.setText("");
            imgHeader.setTextSize(0);
        }
        if (!mBound) {
            bindMessengerService();
        }
    }


    private void findViews() {
        BtnEmailNow = (CustomFontButton) findViewById(R.id.btn_email_now);
        BtnNoThanks = (CustomFontButton) findViewById(R.id.no_thanks_btn);
        tvContactAdmin = (CustomFontTextView) findViewById(R.id.tv_contact_admin);
        imgHeader = (IconTextView) findViewById(R.id.img_header);
        tvContactAdmin.setText(getString(R.string.contact_admin, getString(R.string.app_name)));
    }

    private void bindView() {
        BtnEmailNow.setOnClickListener(this);
        BtnNoThanks.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_email_now:
                if (true) {

                } else {
                    mService.logoutUser(this);
                }
                break;

            case R.id.no_thanks_btn:
                break;
        }
    }


    @Override
    public void fetchInitialDataFromServer() {
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
    }










    /**
     * This is the service connection that is created to the MessengerService
     */
    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            d("Fisike", "com.mphrx.fisike.onServiceConnected : Bind Service");
            d("Fisike", "ListPersonActivity - Service Bound");
            MessengerService.MessengerBinder binder = (MessengerService.MessengerBinder) service;
            mService = binder.getService();
            mBound = true;

        }

        public void onServiceDisconnected(ComponentName className) {
            d("Fisike", "ListPersonActivity - Service Unbound");
            mBound = false;
        }
    };

    /**
     * This method disconnects the messenger service from this activity
     */
    private void unbindMessengerService() {
        try {

            if (mBound) {
                unbindService(mConnection);
                mBound = false;
            }
        } catch (Exception e) {

        }
    }


    /**
     * This method binds the messenger service to this activity
     */
    public void bindMessengerService() {
        if (!mBound) {
            d("Fisike", "com.mphrx.fisike.bindMessengerService");
            Intent intent = new Intent(this, MyApplication.getMessengerServiceClass());
            if (!checkIfMessengerServiceIsRunning()) {
                MyApplication.getAppContext().startService(intent);
            }
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        }

    }

    /**
     * This method checks if the Messenger Service is running or not
     *
     * @return
     */
    public boolean checkIfMessengerServiceIsRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals(MyApplication.getMessengerServiceClass().getName())) {
                return true;
            }
        }
        return false;
    }
}
