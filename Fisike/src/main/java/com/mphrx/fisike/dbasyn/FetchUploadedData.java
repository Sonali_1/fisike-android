package com.mphrx.fisike.dbasyn;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mphrx.fisike.R;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.persistence.DocumentReferenceDBAdapter;

import java.util.ArrayList;

public class FetchUploadedData extends AsyncTask<Void, Void, ArrayList<DocumentReferenceModel>> {

    private Context ctx;
    private UploadsFragment uploadsFragment;
    private boolean isToRefresh;
    private ProgressDialog progressDialog;


    public FetchUploadedData(Context ctx, UploadsFragment uploadsFragment, boolean isToRefresh) {
        this.ctx = ctx;
        this.uploadsFragment = uploadsFragment;
        this.isToRefresh = isToRefresh;
    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage(ctx.getResources().getString(R.string.progress_dialog));
        progressDialog.show();
        progressDialog.setCancelable(false);
        super.onPreExecute();
    }

    @Override
    protected ArrayList<DocumentReferenceModel> doInBackground(Void... params) {
        try {
            return DocumentReferenceDBAdapter.getInstance(ctx).fetchDocumentReference(ctx);
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<DocumentReferenceModel> result) {
        progressDialog.dismiss();
        uploadsFragment.setListView(result, isToRefresh);
        super.onPostExecute(result);
    }
}
