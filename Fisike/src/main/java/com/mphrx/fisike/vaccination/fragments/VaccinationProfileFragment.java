package com.mphrx.fisike.vaccination.fragments;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Line;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vaccination.background.SaveVaccinationProfile;
import com.mphrx.fisike.vaccination.mo.VaccinationInsertData;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Kailash Khurana on 6/16/2016.
 */
public class VaccinationProfileFragment extends Fragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private CustomFontEditTextView edFirstName;
    private CustomFontEditTextView edLastName;
    private CustomFontTextView spinnerGenderHeader;
    private LinearLayout gender_lyt;
    private CustomFontEditTextView edDob;
    private View myFragmentView;
    private boolean isUpdateProfile;
    private DatePickerDialog dobDialog;
    private CustomFontTextView txtGender;
    private CustomFontTextView txtHeader;
    private boolean isDobUpdated;
    private VaccinationProfile oldVaccinationProfile;
    private PopupMenu popupMenuGender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_vaccination_profile, container, false);
        findViews();

        setAttributes();
        setHasOptionsMenu(true);

        if (getArguments() != null && getArguments().containsKey(VariableConstants.VACCINATION_PROFILE)) {
            initValues();
            isUpdateProfile = true;
            ((BaseActivity) getActivity()).setToolbarBack();
        } else {
            txtGender.setText(popupMenuGender.getMenu().getItem(2).getTitle());
        }

        ((BaseActivity) getActivity()).setToolbarTitle(getActivity().getResources().getString(R.string.vaccination_title));
        if (isUpdateProfile) {
            txtHeader.setText(getActivity().getResources().getString(R.string.update_vaccination_profile));
        }

        return myFragmentView;
    }

    private void setAttributes() {
        edDob.setInputType(InputType.TYPE_NULL);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        dobDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dobDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

        edDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.hideKeyboard(getActivity());
                dobDialog.show();
                return false;
            }
        });
        edDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.hideKeyboard(getActivity());
                    dobDialog.show();
                }
            }
        });

        edLastName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    Utils.hideKeyboard(getActivity(), v);
                }
                return false;
            }
        });
        edFirstName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    edLastName.requestFocus();
                    edLastName.setFocusable(true);
                }
                return false;
            }
        });
        popupMenuGender = new PopupMenu(getActivity(), spinnerGenderHeader);
        popupMenuGender.getMenuInflater().inflate(R.menu.menu_gender, popupMenuGender.getMenu());
    }

    private void initValues() {
        oldVaccinationProfile = (VaccinationProfile) getArguments().get(VariableConstants.VACCINATION_PROFILE);
        edFirstName.setText(oldVaccinationProfile.getFirstName());
        edLastName.setText(oldVaccinationProfile.getLastName());
        edDob.setText(oldVaccinationProfile.getDateOfBirth());
        String gender = oldVaccinationProfile.getGender();
        txtGender.setText(gender == null || gender.equals("") ? popupMenuGender.getMenu().getItem(2).getTitle() : gender);
    }

    private void findViews() {
        gender_lyt = (LinearLayout) myFragmentView.findViewById(R.id.gender_lyt);
        gender_lyt.setOnClickListener(this);
        edFirstName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.edFirstName);
        edLastName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.edLastName);
        spinnerGenderHeader = (CustomFontTextView) myFragmentView.findViewById(R.id.spinnerGenderHeader);
        edDob = (CustomFontEditTextView) myFragmentView.findViewById(R.id.eddob);
        txtGender = (CustomFontTextView) myFragmentView.findViewById(R.id.txtGender);
        txtHeader = (CustomFontTextView) myFragmentView.findViewById(R.id.txtHeader);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validProfile()) {
                    new SaveVaccinationProfile(new ApiResponseCallback<VaccinationInsertData>() {
                        @Override
                        public void onSuccessRespone(final VaccinationInsertData dataInserted) {
                            if (dataInserted != null) {
                                getActivity().getSupportFragmentManager().beginTransaction().remove(VaccinationProfileFragment.this).commit();
                                VaccinationListFragment vaccinationListFragment = new VaccinationListFragment();
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(VariableConstants.VACCINATION_PROFILE, dataInserted.getVaccinationProfile());
                                vaccinationListFragment.setArguments(bundle);
                                ((BaseActivity)(getActivity())).setVaccinationProfileBundle(bundle);
                                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, vaccinationListFragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                fragmentTransaction.commitAllowingStateLoss();
                                //Toast.makeText(getActivity(), getActivity().getString(R.string.successfully_added_vaccination), Toast.LENGTH_SHORT).show();
                                MyApplication.getInstance().trackEvent(getActivity().getString(R.string.vaccination_profile), "Save vaccination button", "Successfull", "true", "Vaccination Profile");
                                MyApplication.getInstance().trackEvent(getActivity().getString(R.string.vaccination_profile), "Save vaccination button", "Successfull", "true", txtGender.getText().toString());
                            } else {
                                MyApplication.getInstance().trackEvent(getActivity().getString(R.string.vaccination_profile), "Save vaccination button", "Successfull", "false", "Vaccination Profile");
                                MyApplication.getInstance().trackEvent(getActivity().getString(R.string.vaccination_profile), "Save vaccination button", "Successfull", "false", txtGender.getText().toString());
                            }

                            if (isDobUpdated) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ArrayList<VaccinationModel> vaccinationModelList = dataInserted.getVaccinationModelList();
                                        for (int i = 0; vaccinationModelList != null && i < vaccinationModelList.size(); i++) {
                                           if(vaccinationModelList.get(i).getDoseTimeStamp()==0){
                                            Utils.cancelVaccinationAlarm(getActivity(), vaccinationModelList.get(i).getReminderUniqueKey());
                                            VaccinationAlarm.setAlarm(vaccinationModelList.get(i), getActivity(), false);
                                           }
                                        }
                                    }
                                }).start();
                            }
                            if (isUpdateProfile)
                                Toast.makeText(getActivity(), getString(R.string.vaccination_updated_success), Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(getActivity(), getString(R.string.successfully_added_vaccination), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailedResponse(Exception exception) {
                            MyApplication.getInstance().trackEvent(getActivity().getString(R.string.vaccination_profile), "Save vaccination button", "Successfull", "false", "Vaccination Profile");

                        }
                    }, getActivity(), true, new VaccinationProfile(edFirstName.getText().toString(), edLastName.getText().toString(), txtGender.getText().toString(), edDob.getText().toString()), isUpdateProfile, isDobUpdated).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                break;
            case R.id.gender_lyt:
                showPopupMenu(txtGender);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    private boolean validProfile() {
        if (edFirstName.getText() != null && edFirstName.getText().toString().trim().equals("")) {
            edFirstName.setError(getActivity().getResources().getString(R.string.please_enter_first_name));
            return false;
        }
        if (edLastName.getText() != null && edLastName.getText().toString().trim().equals("")) {
            edLastName.setError(getActivity().getResources().getString(R.string.please_enter_last_name));
            return false;
        }
        if (edDob.getText() != null && edDob.getText().toString().trim().equals("")) {
            edDob.setError(getActivity().getResources().getString(R.string.set_dob));
            return false;
        }
        if (isUpdateProfile && oldVaccinationProfile != null && !oldVaccinationProfile.getDateOfBirth().equalsIgnoreCase(edDob.getText().toString())) {
            isDobUpdated = true;
        }
        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        edDob.setText(DateTimeUtil.getFormattedDateWithoutUTC(((monthOfYear + 1) + "-" + dayOfMonth + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated));
    }

    private void showPopupMenu(final CustomFontTextView txtView) {
        //registering popup with OnMenuItemClickListener
        popupMenuGender.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.setText(item.getTitle());
                popupMenuGender.dismiss();
                return true;
            }
        });

        popupMenuGender.show();//showing popup menu
    }

    public boolean isUpdateFlow() {
        return isUpdateProfile;
    }

    public void setUpdateFlow(boolean value) {
        isUpdateProfile = value;
    }

}
