package com.mphrx.fisike.vaccination.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kailash Khurana on 6/23/2016.
 */
public class VaccinationAlarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle extras = intent.getExtras();
        VaccinationModel vaccinationModel = (VaccinationModel) extras.get(VariableConstants.VACCINATION_MODEL);
        try {
            vaccinationModel = VaccinationDBAdapter.getInstance(context).fetchVaccinationMO(vaccinationModel.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setAlarm(vaccinationModel, context, true);
    }

    public static void
    setAlarm(VaccinationModel vaccinationModel, Context context, boolean isFromNotification) {
        long now = System.currentTimeMillis();
        Calendar todayCal = Calendar.getInstance();
        todayCal.set(Calendar.HOUR_OF_DAY, 9);
        todayCal.set(Calendar.MINUTE, 0);
        todayCal.set(Calendar.SECOND, 0);
        todayCal.set(Calendar.MILLISECOND, 0);
        todayCal.setTimeInMillis(now);
        //tommorow date calculation
        long yesterday = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1);
        Calendar yesterdayCal = Calendar.getInstance();
        yesterdayCal.setTimeInMillis(yesterday);
        yesterdayCal.set(Calendar.HOUR_OF_DAY, 9);
        yesterdayCal.set(Calendar.MINUTE, 0);
        yesterdayCal.set(Calendar.SECOND, 0);
        yesterdayCal.set(Calendar.MILLISECOND, 0);

        long vaccinationTime = vaccinationModel.getDoseTimeStamp();
        Calendar vaccinationCal = Calendar.getInstance();
        vaccinationCal.setTimeInMillis(vaccinationTime);
        vaccinationCal.set(Calendar.HOUR_OF_DAY, 9);
        vaccinationCal.set(Calendar.MINUTE, 0);
        vaccinationCal.set(Calendar.SECOND, 0);
        vaccinationCal.set(Calendar.MILLISECOND, 0);
        Calendar weekCal = Calendar.getInstance();
        weekCal.setTimeInMillis(vaccinationTime + TimeUnit.DAYS.toMillis(7));
        weekCal.set(Calendar.HOUR_OF_DAY, 9);
        weekCal.set(Calendar.MINUTE, 0);
        weekCal.set(Calendar.SECOND, 0);
        weekCal.set(Calendar.MILLISECOND, 0);

        long diff = now - vaccinationTime;
        if (!vaccinationModel.isVaccinationTaken() && diff > 0 && diff / TimeUnit.DAYS.toMillis(1) > 7) {
            return;
        }
        /**
         * If due date is tomorrow and start alarm for next date
         */
        if (!vaccinationModel.isVaccinationTaken() && yesterdayCal.get(Calendar.DATE) == vaccinationCal.get(Calendar.DATE) && yesterdayCal.get(Calendar.MONTH) == vaccinationCal.get(Calendar.MONTH) && yesterdayCal.get(Calendar.YEAR) == vaccinationCal.get(Calendar.YEAR)) {
            if (todayCal.get(Calendar.HOUR_OF_DAY) >= 9 && todayCal.get(Calendar.HOUR_OF_DAY) < 10) {
                generateNotification(context, context.getResources().getString(R.string.vaccination_reminder_yesterday), vaccinationModel);
            }
            Utils.startAlarmVaccination(context, vaccinationModel, vaccinationModel.getDoseTimeStamp());
        }
        /**
         * If due date is today
         */
        else if (!vaccinationModel.isVaccinationTaken() && todayCal.get(Calendar.DATE) == vaccinationCal.get(Calendar.DATE) && todayCal.get(Calendar.MONTH) == vaccinationCal.get(Calendar.MONTH) && todayCal.get(Calendar.YEAR) == vaccinationCal.get(Calendar.YEAR)) {
            if (todayCal.get(Calendar.HOUR_OF_DAY) >= 9 && todayCal.get(Calendar.HOUR_OF_DAY) <= 10) {
                generateNotification(context, String.format(context.getResources().getString(R.string.vaccination_reminder_today), vaccinationModel.getVaccinationName()), vaccinationModel);
            }

            if(isFromNotification || todayCal.get(Calendar.HOUR_OF_DAY)>10)
                Utils.startAlarmVaccination(context, vaccinationModel, vaccinationModel.getDoseTimeStamp() + TimeUnit.DAYS.toMillis(7));

            else if(todayCal.get(Calendar.HOUR_OF_DAY)<9)
                Utils.startAlarmVaccination(context, vaccinationModel, vaccinationModel.getDoseTimeStamp());
         }

        /**
         * If due date had passed then show notification after seven days
         */
        else if (!vaccinationModel.isVaccinationTaken() && todayCal.after(vaccinationCal)) {//(todayCal.get(Calendar.DATE) > vaccinationCal.get(Calendar.DATE) || todayCal.get(Calendar.MONTH) > vaccinationCal.get(Calendar.MONTH) || todayCal.get(Calendar.YEAR) >= vaccinationCal.get(Calendar.YEAR))
            if (todayCal.get(Calendar.DATE) == weekCal.get(Calendar.DATE) && todayCal.get(Calendar.MONTH) == weekCal.get(Calendar.MONTH) && todayCal.get(Calendar.YEAR) == weekCal.get(Calendar.YEAR) && todayCal.get(Calendar.HOUR_OF_DAY) >= 9 && todayCal.get(Calendar.HOUR_OF_DAY) < 10) {
                generateNotification(context, String.format(context.getResources().getString(R.string.vaccination_reminder_week), vaccinationModel.getVaccinationName(), vaccinationModel.getDueDate()), vaccinationModel);
            }
            if (!isFromNotification) {
                Utils.startAlarmVaccination(context, vaccinationModel, vaccinationModel.getDoseTimeStamp() + TimeUnit.DAYS.toMillis(7));
            }
        }
        /**
         * If due date is future date
         */
        else if (!vaccinationModel.isVaccinationTaken() && todayCal.before(vaccinationCal)) {
            Utils.startAlarmVaccination(context, vaccinationModel, vaccinationModel.getDoseTimeStamp() - TimeUnit.DAYS.toMillis(1));
        }
    }


    /**
     * This method generates an in-app notification Depending
     *
     * @param context
     * @param notificationText
     * @param vaccinationModel
     */
    private static void generateNotification(Context context, String notificationText, VaccinationModel vaccinationModel) {
        try {
            NotificationCompat.Builder mBuilder;

            mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.notification_icon)
                    .setWhen(System.currentTimeMillis())
                    .setTicker(notificationText).setContentText(notificationText).setContentTitle(context.getResources().getString(R.string.vaccination_reminder_title));
            mBuilder.setAutoCancel(true);
            if (Utils.isAppbackground(context)) {
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            }
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
            style.bigText(notificationText);
            mBuilder.setStyle(style);
            Intent resultIntent;
            resultIntent = new Intent(context, HomeActivity.class);
            resultIntent.putExtra(VariableConstants.NOTIFICATION_REMINDER, true);
            resultIntent.putExtra(VariableConstants.VACCINATION_MODEL, vaccinationModel);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int id = (int) System.currentTimeMillis();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, id, resultIntent, 0);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(vaccinationModel.getReminderUniqueKey(), mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
