package com.mphrx.fisike.vaccination.fragments;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.vaccination.background.OpenVaccinationFragmenet;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationProfileDBAdapter;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;
import com.mphrx.fisike_physician.utils.DialogUtils;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 6/16/2016.
 */
public class VaccinationListFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private static final int BY_DATE_PAGE = 0;
    private static final int BY_VACCINATION_PAGE = 1;
    private View myFragmentView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String dob;
    private ArrayList<Fragment> fragmentMap = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_vaccination_list, container, false);

        if (getArguments() != null && getArguments().containsKey(VariableConstants.VACCINATION_PROFILE) && getArguments().get(VariableConstants.VACCINATION_PROFILE) instanceof VaccinationProfile) {
            dob = ((VaccinationProfile) getArguments().get(VariableConstants.VACCINATION_PROFILE)).getDateOfBirth();
        }

        ((BaseActivity) getActivity()).setToolbarTitle(getActivity().getResources().getString(R.string.vaccination_title));
        BaseActivity.bt_toolbar_icon.setVisibility(View.VISIBLE);
        BaseActivity.bt_toolbar_icon.setOnClickListener(this);
        BaseActivity.bt_toolbar_icon.setText(R.string.fa_info);
        BaseActivity.bt_toolbar_icon.setTextSize(32);
        BaseActivity.bt_toolbar_right.setVisibility(View.VISIBLE);
        BaseActivity.bt_toolbar_right.setText(R.string.fa_edit);
        BaseActivity.bt_toolbar_right.setTextSize(32);
        BaseActivity.bt_toolbar_right.setOnClickListener(this);
        ((BaseActivity) getActivity()).setHamburgerIcon();
        setHasOptionsMenu(true);
        findViews();
        initView();

        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.VACCINATION_FRAGMENT);

        return myFragmentView;
    }

    private void findViews() {
        viewPager = (ViewPager) myFragmentView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) myFragmentView.findViewById(R.id.tabs);
    }


    private void initView() {
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public Fragment getItem(int position) {
                Fragment fragment;
                if (position == 0) {
                    fragment = new ByDateVaccinationFragment();
                } else {
                    fragment = new ByVaccinationFragment();
                }
                Bundle bundle = new Bundle();
                bundle.putString(VariableConstants.VACCINATION_PROFILE_DOB, dob);
                fragment.setArguments(bundle);
                fragmentMap.add(position, fragment);
                return fragment;

            }


            @Override
            public CharSequence getPageTitle(int position) {
                CharSequence pageTitle = null;
                switch (position) {
                    case BY_DATE_PAGE:
                        pageTitle = getResources().getString(R.string.by_date);
                        break;
                    case BY_VACCINATION_PAGE:
                        pageTitle = getResources().getString(R.string.by_vaccine);
                        break;
                }
                return pageTitle;
            }
        });

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        //   menu.add(Menu.NONE, TextConstants.MENU_ITEM_DISCLAMER, Menu.NONE, "Disclaimer").setIcon(R.drawable.ic_filter_list_white_36dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        //   menu.add(Menu.NONE, TextConstants.MENU_ITEM_PROFILE_UPDATE, Menu.NONE, "Update Profile").setIcon(R.drawable.ic_filter_list_white_36dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case TextConstants.MENU_ITEM_PROFILE_UPDATE:
                VaccinationProfileFragment fragment = new VaccinationProfileFragment();
                fragment.setArguments(getArguments());
                getActivity().getSupportFragmentManager().beginTransaction().remove(VaccinationListFragment.this).commit();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.commitAllowingStateLoss();
                return true;
            case TextConstants.MENU_ITEM_DISCLAMER:
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getString(R.string.disclaimer), getActivity().getString(R.string.disclaimer_body), getActivity().getString(R.string.ok), getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case BY_DATE_PAGE:
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.BY_DATE);
                break;
            case BY_VACCINATION_PAGE:
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.BY_VACCINATION);
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void refreshFragment(boolean isByDate) {
        if (isByDate) {
            ((ByDateVaccinationFragment) fragmentMap.get(0)).refreshAdapter();
        } else {
            ((ByVaccinationFragment) fragmentMap.get(1)).refreshAdapter();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_toolbar_icon:
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getString(R.string.disclaimer), getActivity().getString(R.string.disclaimer_body), getActivity().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                break;


            case R.id.bt_toolbar_right:
                VaccinationProfileFragment fragment = new VaccinationProfileFragment();
                fragment.setArguments(((BaseActivity) getActivity()).getVaccinationProfileBundle());
                getActivity().getSupportFragmentManager().beginTransaction().remove(VaccinationListFragment.this).commit();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.commitAllowingStateLoss();
                break;
        }
    }
}