package com.mphrx.fisike.vaccination.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.vaccination.background.UpdateVaccinationTaken;
import com.mphrx.fisike.vaccination.fragments.ByDateVaccinationFragment;
import com.mphrx.fisike.vaccination.fragments.ByVaccinationFragment;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;

import java.util.ArrayList;

import appointment.utils.CommonTasks;

/**
 * Created by Kailash Khurana on 6/17/2016.
 */
public class VaccinationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<VaccinationModel> vaccinationModelList;
    private boolean isViewByDate;
    private final Context context;
    private Fragment fragment;

    public VaccinationListAdapter(ArrayList<VaccinationModel> vaccinationDose, boolean isViewByDate, Context context, Fragment fragment) {
        this.vaccinationModelList = vaccinationDose;
        this.isViewByDate = isViewByDate;
        this.context = context;
        this.fragment = fragment;
    }

    public void setVaccinationModelList(ArrayList<VaccinationModel> vaccinationModelList) {
        this.vaccinationModelList = vaccinationModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM && isViewByDate) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccination_dose_cell, parent, false);
            VaccinationCell itemCell = new VaccinationCell(row);
            return itemCell;
        } else if (viewType == TYPE_ITEM) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccination_dose_cell_italic, parent, false);
            VaccinationCell itemCell = new VaccinationCell(row);
            return itemCell;
        } else if (viewType == TYPE_HEADER) {
            View footerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccination_header_cell, parent, false);
            VaccinationHeader vaccinationHeader = new VaccinationHeader(footerView);
            return vaccinationHeader;
        }
        return null;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final VaccinationModel object = vaccinationModelList.get(position);
        if (isViewByDate) {
            if (object.isHeader()) {
                (((VaccinationHeader) holder).leftText).setText(CommonTasks.formateDateFromstring(DateTimeUtil.dd_MMM_yyyy, DateTimeUtil.EEE_dd_MMM_yyyy, object.getDueDate()));
                (((VaccinationHeader) holder).rightText).setText(object.getDosePeriod());
                if (position == 0) {
                    ((VaccinationHeader) holder).tempview.setVisibility(View.VISIBLE);
                } else {
                    ((VaccinationHeader) holder).tempview.setVisibility(View.GONE);
                }

            } else {
                (((VaccinationCell) holder).txtHeadH1).setText(object.getVaccinationType());
                (((VaccinationCell) holder).txtHeadH2).setText(object.getVaccinationName());
                (((VaccinationCell) holder).txtHeadH3).setText(object.getDose());
                (((VaccinationCell) holder).txtHeadH3).selectTypeface(context, null, null);
            }
        } else {
            if (object.isHeader()) {
                (((VaccinationHeader) holder).leftText).setText(object.getVaccinationType());
                (((VaccinationHeader) holder).rightText).setText(object.getVaccinationName());
                if (position == 0) {
                    ((VaccinationHeader) holder).tempview.setVisibility(View.VISIBLE);
                } else {
                    ((VaccinationHeader) holder).tempview.setVisibility(View.GONE);
                }
            } else {
                (((VaccinationCell) holder).txtHeadH1).setText(object.getDose());
                (((VaccinationCell) holder).txtHeadH2).setText(object.getDosePeriod());
                (((VaccinationCell) holder).txtHeadH3).setText(context.getResources().getString(R.string.Due_on) + object.getDueDate());
            }
        }
        if (!object.isHeader()) {
            (((VaccinationCell) holder).cbRead).setChecked(object.isVaccinationTaken());
            (((VaccinationCell) holder).cbRead).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UpdateVaccinationTaken(new ApiResponseCallback<Boolean>() {
                        @Override
                        public void onSuccessRespone(Boolean aBoolean) {
                            if (aBoolean) {
                                object.setVaccinationTaken(!object.isVaccinationTaken());
                                if (fragment instanceof ByDateVaccinationFragment) {
                                    ((ByDateVaccinationFragment) fragment).refreshOtherAdapter();
                                } else if (fragment instanceof ByVaccinationFragment) {
                                    ((ByVaccinationFragment) fragment).refreshOtherAdapter();
                                }
                                if (!object.isVaccinationTaken()) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            VaccinationAlarm.setAlarm(object, context, false);
                                        }
                                    }).start();
                                    Toast.makeText(context, context.getString(R.string.vaccintion_pending), Toast.LENGTH_SHORT).show();
                                } else {
                                    MyApplication.getInstance().trackEvent(context.getString(R.string.vaccination_title), "Taken vaccination", "Vaccination Name", object.getVaccinationName(), context.getString(R.string.vaccination_title));
                                    Toast.makeText(context, context.getString(R.string.vaccination_taken_text), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailedResponse(Exception exception) {

                        }
                    }, context, false, !object.isVaccinationTaken(), object.getId()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return vaccinationModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        VaccinationModel item = vaccinationModelList.get(position);
        if (item != null && item.isHeader()) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    public class VaccinationHeader extends RecyclerView.ViewHolder {
        private CustomFontTextView leftText;
        private CustomFontTextView rightText;
        private FrameLayout tempview;

        public VaccinationHeader(View v) {
            super(v);
            leftText = (CustomFontTextView) v.findViewById(R.id.txtHeadH1);
            rightText = (CustomFontTextView) v.findViewById(R.id.txtHeadH2);
            tempview = (FrameLayout) v.findViewById(R.id.tempview);
        }
    }

    public class VaccinationCell extends RecyclerView.ViewHolder {
        private CustomFontTextView txtHeadH1;
        private CustomFontTextView txtHeadH2;
        private CustomFontTextView txtHeadH3;
        private CheckBox cbRead;

        public VaccinationCell(View v) {
            super(v);
            txtHeadH1 = (CustomFontTextView) v.findViewById(R.id.txtHeadH1);
            txtHeadH2 = (CustomFontTextView) v.findViewById(R.id.txtHeadH2);
            txtHeadH3 = (CustomFontTextView) v.findViewById(R.id.txtHeadH3);
            cbRead = (CheckBox) v.findViewById(R.id.cbRead);
        }
    }
}