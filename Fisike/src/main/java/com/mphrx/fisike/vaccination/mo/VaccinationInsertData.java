package com.mphrx.fisike.vaccination.mo;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 6/30/2016.
 */
public class VaccinationInsertData {
    private ArrayList<VaccinationModel> vaccinationModelList;
    private VaccinationProfile vaccinationProfile;

    public ArrayList<VaccinationModel> getVaccinationModelList() {
        return vaccinationModelList;
    }

    public void setVaccinationModelList(ArrayList<VaccinationModel> vaccinationModelList) {
        this.vaccinationModelList = vaccinationModelList;
    }

    public VaccinationProfile getVaccinationProfile() {
        return vaccinationProfile;
    }

    public void setVaccinationProfile(VaccinationProfile vaccinationProfile) {
        this.vaccinationProfile = vaccinationProfile;
    }
}
