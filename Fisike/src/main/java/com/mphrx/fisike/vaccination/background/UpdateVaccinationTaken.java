package com.mphrx.fisike.vaccination.background;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.asynctaskmanger.AbstractAsyncTask;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;

/**
 * Created by Kailash Khurana on 6/16/2016.
 */
public class UpdateVaccinationTaken extends AbstractAsyncTask<Void, Void, Boolean> {
    private Context context;
    private boolean isVaccinationTaken;
    private int id;

    public UpdateVaccinationTaken(ApiResponseCallback<Boolean> callback, Context context, boolean isToShowProgress, boolean isVaccinationTaken, int id) {
        super(callback, context, isToShowProgress);
        this.context = context;
        this.isVaccinationTaken = isVaccinationTaken;
        this.id = id;
    }

    @Override
    protected Boolean getResult(Void... params) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(VaccinationDBAdapter.getIsVaccinationTaken(), isVaccinationTaken);
            return VaccinationDBAdapter.getInstance(context).updateVaccinationMo(id, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
