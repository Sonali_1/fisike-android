package com.mphrx.fisike.vaccination.background;

import android.content.Context;
import android.content.SharedPreferences;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.asynctaskmanger.AbstractAsyncTask;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.utils.AssetJsonFileReader;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Kailash Khurana on 6/17/2016.
 */
public class VaccinationJosnParsing extends AbstractAsyncTask<Void, Void, ArrayList<VaccinationModel>> {
    private boolean isViewByDate;
    private static String dob;
    private static String json;
    private static Context context;


    public VaccinationJosnParsing(ApiResponseCallback<ArrayList<VaccinationModel>> callback, Context context, boolean isToShowProgress, boolean isViewByDate, String dob) {
        super(callback, context, isToShowProgress);
        this.context = context;
        this.isViewByDate = isViewByDate;
        this.dob = dob;
    }

    @Override
    protected ArrayList<VaccinationModel> getResult(Void... params) {
        return loadVaccinationData(isViewByDate);
    }

    /*checks whether to load vaccination from json or from database*/
    private static synchronized ArrayList<VaccinationModel> loadVaccinationData(boolean isViewByDate) {
        // if there is nothing in database load from json

        SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        if (sharedPreferences.getBoolean(VariableConstants.IS_VACCINATION_PARSED, false)) {
            if(isViewByDate) {
                return fetchFromDBByDate();
            }else{
                return fetchFromDB();
            }
        }

        try {
            parseObjects(new ArrayList<VaccinationModel>());
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(isViewByDate) {
            return fetchFromDBByDate();
        }else{
            return fetchFromDB();
        }
    }

    private static ArrayList<VaccinationModel> fetchFromDB() {
        return VaccinationDBAdapter.getInstance(context).fetchAllVaccinationMO(false);
    }
    private static ArrayList<VaccinationModel> fetchFromDBByDate() {
        return VaccinationDBAdapter.getInstance(context).fetchAllVaccinationMO(true);
    }

    private static synchronized void parseObjects(ArrayList<VaccinationModel> arrayVaccinationModel)
            throws Exception {
        json = AssetJsonFileReader.readJSONFile("vaccination.json", context);
        parseJson(json);
    }

    private static synchronized void parseJson(String input) throws Exception {
        JSONArray vaccinationJsonArray = new JSONArray(json);
        VaccinationDBAdapter vaccinationDBAdapter = VaccinationDBAdapter.getInstance(context);
        for (int i = 0; i < vaccinationJsonArray.length(); i++) {
            JSONObject vaccinationJsonObject = vaccinationJsonArray.getJSONObject(i);
            String vaccinationType = vaccinationJsonObject.getString("vaccinationType");
            String vaccinationName = vaccinationJsonObject.getString("vaccinationName");
            JSONArray vaccinationDoseArray = vaccinationJsonObject.getJSONArray("vaccinationDue");
            for (int j = 0; j < vaccinationDoseArray.length(); j++) {
                VaccinationModel vaccinationModel = vaccinationCellModel(vaccinationDoseArray.getJSONObject(j), vaccinationType, vaccinationName);
                vaccinationDBAdapter.insert(vaccinationModel);
            }
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
        sharedPreferences.edit().putBoolean(VariableConstants.IS_VACCINATION_PARSED, true).commit();
    }

    private static VaccinationModel vaccinationCellModel(JSONObject vaccinationDoseObject, String vaccinationType, String vaccinationName) throws JSONException {
        VaccinationModel vaccinationModel = new VaccinationModel();
        vaccinationModel.setHeader(false);
        vaccinationModel.setVaccinationType(vaccinationType);
        vaccinationModel.setVaccinationName(vaccinationName);
        vaccinationModel.setDose(vaccinationDoseObject.getString("dose"));
        JSONObject dueOn = vaccinationDoseObject.getJSONObject("dueOn");
        String days = dueOn.optString("days");
        String weeksFull = dueOn.optString("weeks");
        String weeks = weeksFull != null && weeksFull.contains("-") ? weeksFull.split("-")[0].trim() : weeksFull;
        String monthsFull = dueOn.optString("months");
        String months = monthsFull != null && monthsFull.contains("-") ? monthsFull.split("-")[0].trim() : monthsFull;
        String yearsFull = dueOn.optString("years");
        String years = yearsFull != null && yearsFull.contains("-") ? yearsFull.split("-")[0].trim() : yearsFull;
        Calendar vaccinationCalendar = gettheNextdoseTimestamp(days, weeks, months, years, dob);
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
        String updatedDate = sdf.format(vaccinationCalendar.getTimeInMillis());
        vaccinationModel.setDueDate(updatedDate);
        vaccinationModel.setDoseTimeStamp(vaccinationCalendar.getTimeInMillis());
        vaccinationModel.setDosePeriod(getDosePeriod(days, weeksFull, monthsFull, yearsFull));
        vaccinationModel.setVaccinationTaken(false);
        vaccinationModel.setReminderUniqueKey((int) System.currentTimeMillis());
        vaccinationModel.setDays(days);
        vaccinationModel.setWeeks(weeks);
        vaccinationModel.setMonths(months);
        vaccinationModel.setYears(years);
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
        }
        return vaccinationModel;
    }

    public static Calendar gettheNextdoseTimestamp(String days, String weeks, String months, String years, String dob) throws JSONException {
        Calendar doseCalendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            doseCalendar.setTime(format.parse(dob));
        } catch (ParseException e) {
            e.printStackTrace();
            return Calendar.getInstance();
        }

        if (days != null && !days.equals("")) {
            doseCalendar.add(Calendar.DATE, Integer.parseInt(days));
        } else if (weeks != null && !weeks.equals("")) {
            doseCalendar.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(weeks));
        } else if (months != null && !months.equals("")) {
            doseCalendar.add(Calendar.MONTH, Integer.parseInt(months));
        } else if (years != null && !years.equals("")) {
            doseCalendar.add(Calendar.YEAR, Integer.parseInt(years));
        }
        doseCalendar.set(Calendar.HOUR_OF_DAY, 9);
        return doseCalendar;

    }

    public static String getDosePeriod(String days, String weeks, String months, String years) throws JSONException {
        if (days != null && !days.equals("")) {
            if (Integer.parseInt(days) == 0) {
                return MyApplication.getAppContext().getResources().getString(R.string.At_Birth);
            }
            return days + MyApplication.getAppContext().getResources().getString(R.string._days);
        } else if (weeks != null && !weeks.equals("")) {
            return weeks + MyApplication.getAppContext().getResources().getString(R.string._weeks);
        } else if (months != null && !months.equals("")) {
            return months + MyApplication.getAppContext().getResources().getString(R.string._months);
        } else if (years != null && !years.equals("")) {
            return years + MyApplication.getAppContext().getResources().getString(R.string._years);
        }
        return "";
    }

}
