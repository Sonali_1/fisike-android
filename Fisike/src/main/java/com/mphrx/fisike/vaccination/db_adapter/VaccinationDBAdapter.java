package com.mphrx.fisike.vaccination.db_adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.persistence.DBHelper;
import com.mphrx.fisike.persistence.SettingsDBAdapter;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.vaccination.background.VaccinationJosnParsing;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Kailash Khurana on 6/15/2016.
 */
public class VaccinationDBAdapter {

    private static VaccinationDBAdapter vaccinationDBAdapter;

    private Context context;
    private SQLiteDatabase database;
    private static final String VACCINATION_TABLE = "VaccinationMO";
    private static final String ID = "id";
    private static final String PERSISTENCEKEY = "persistenceKey";
    private static final String DUE_DATE = "dueDate";
    private static final String VACCINATION_TYPE = "vaccinationType";
    private static final String VACCINATION_NAME = "vaccinationName";
    private static final String DOSE = "dose";
    private static final String IS_VACCINATION_TAKEN = "isVaccinationTaken";
    private static final String IS_REMINDER_TO_SHOW = "isReminderToShow";
    private static final String DAYS = "days";
    private static final String WEEKS = "weeks";
    private static final String MONTHS = "months";
    private static final String YEARS = "years";
    private static final String DOSE_PERIOD = "dosePeriod";
    private static final String DOSE_TIME_STAMP = "doseTimeStamp";
    private static final String REMINDER_TIME_STAMP = "reminderTimeStamp";

    public VaccinationDBAdapter(Context context) {
        this.context = context;
    }

    public static String getVaccinationTable() {
        return VACCINATION_TABLE;
    }

    public static VaccinationDBAdapter getInstance(Context ctx) {
        if (null == vaccinationDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (vaccinationDBAdapter == null) {

                    vaccinationDBAdapter = new VaccinationDBAdapter(ctx);
                    vaccinationDBAdapter.open();
                }
            }
        }
        return vaccinationDBAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public boolean insert(VaccinationModel vaccinationModel) throws Exception {
        ContentValues values = new ContentValues();
        values.put(PERSISTENCEKEY, vaccinationModel.getPersistenceKey());
        values.put(DUE_DATE, vaccinationModel.getDueDate());
        values.put(VACCINATION_TYPE, vaccinationModel.getVaccinationType());
        values.put(VACCINATION_NAME, vaccinationModel.getVaccinationName());
        values.put(DOSE, vaccinationModel.getDose());
        values.put(IS_VACCINATION_TAKEN, vaccinationModel.isVaccinationTaken());
        values.put(IS_REMINDER_TO_SHOW, vaccinationModel.isReminderToShow());
        values.put(DAYS, vaccinationModel.getDays());
        values.put(WEEKS, vaccinationModel.getWeeks());
        values.put(MONTHS, vaccinationModel.getMonths());
        values.put(YEARS, vaccinationModel.getYears());
        values.put(DOSE_PERIOD, vaccinationModel.getDosePeriod());
        values.put(DOSE_TIME_STAMP, vaccinationModel.getDoseTimeStamp());
        values.put(REMINDER_TIME_STAMP, vaccinationModel.getReminderUniqueKey());
        long count = database.insert(VACCINATION_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    public VaccinationModel fetchVaccinationMO(int id) throws Exception {
        Cursor mCursor = database.query(true, VACCINATION_TABLE, null, ID + " = " + id, null, null,
                null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            VaccinationModel vaccinationModel = getVaccinationTableData(mCursor);
            mCursor.close();
            return vaccinationModel;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }

    private VaccinationModel getVaccinationTableData(Cursor mCursor) {
        VaccinationModel vaccinationModel = new VaccinationModel();
        vaccinationModel.setId(mCursor.getInt(mCursor.getColumnIndex(ID)));
        vaccinationModel.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
        vaccinationModel.setDueDate(mCursor.getString(mCursor.getColumnIndex(DUE_DATE)));
        vaccinationModel.setVaccinationType(mCursor.getString(mCursor.getColumnIndex(VACCINATION_TYPE)));
        vaccinationModel.setVaccinationName(mCursor.getString(mCursor.getColumnIndex(VACCINATION_NAME)));
        vaccinationModel.setVaccinationTaken(mCursor.getInt(mCursor.getColumnIndex(IS_VACCINATION_TAKEN)) == 1 ? true : false);
        vaccinationModel.setReminderToShow(mCursor.getInt(mCursor.getColumnIndex(IS_REMINDER_TO_SHOW)) == 1 ? true : false);
        vaccinationModel.setDose(mCursor.getString(mCursor.getColumnIndex(DOSE)));
        vaccinationModel.setDays(mCursor.getString(mCursor.getColumnIndex(DAYS)));
        vaccinationModel.setWeeks(mCursor.getString(mCursor.getColumnIndex(WEEKS)));
        vaccinationModel.setMonths(mCursor.getString(mCursor.getColumnIndex(MONTHS)));
        vaccinationModel.setYears(mCursor.getString(mCursor.getColumnIndex(YEARS)));
        vaccinationModel.setDosePeriod(mCursor.getString(mCursor.getColumnIndex(DOSE_PERIOD)));
        vaccinationModel.setDoseTimeStamp(mCursor.getLong(mCursor.getColumnIndex(DOSE_TIME_STAMP)));
        vaccinationModel.setReminderUniqueKey(mCursor.getInt(mCursor.getColumnIndex(REMINDER_TIME_STAMP)));
        return vaccinationModel;
    }


    public boolean updateVaccinationMo(int id, ContentValues contentValues) {
        int count = -1;
        try {
            count = database.update(VACCINATION_TABLE, contentValues, ID + "=" + id, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String createVaccinationTableQuery() {
        String query = "CREATE TABLE " + VACCINATION_TABLE + " ( " +
                ID + " INTEGER  PRIMARY KEY AUTOINCREMENT ," +
                PERSISTENCEKEY + " varchar(255) ," +
                DUE_DATE + " varchar(255), " +
                VACCINATION_TYPE + " varchar(255), " +
                VACCINATION_NAME + " varchar(255), " +
                DOSE + " varchar(255), " +
                IS_VACCINATION_TAKEN + " INTEGER, " +
                IS_REMINDER_TO_SHOW + " INTEGER, " +
                DAYS + " varchar(255), " +
                WEEKS + " varchar(255), " +
                MONTHS + " varchar(255), " +
                YEARS + " varchar(255), " +
                DOSE_PERIOD + " varchar(255)," +
                DOSE_TIME_STAMP + " LONG," +
                REMINDER_TIME_STAMP + " INTEGER )";

        return query;
    }

    public static String getID() {
        return ID;
    }

    public static String getPERSISTENCEKEY() {
        return PERSISTENCEKEY;
    }

    public static String getDueDate() {
        return DUE_DATE;
    }

    public static String getVaccinationType() {
        return VACCINATION_TYPE;
    }

    public static String getVaccinationName() {
        return VACCINATION_NAME;
    }

    public static String getDOSE() {
        return DOSE;
    }

    public static String getIsVaccinationTaken() {
        return IS_VACCINATION_TAKEN;
    }

    public static String getIsReminderToShow() {
        return IS_REMINDER_TO_SHOW;
    }

    public static String getDAYS() {
        return DAYS;
    }

    public static String getWEEKS() {
        return WEEKS;
    }

    public static String getMONTHS() {
        return MONTHS;
    }

    public static String getYEARS() {
        return YEARS;
    }

    public static String getDosePeriod() {
        return DOSE_PERIOD;
    }

    public static String getDoseTimeStamp() {
        return DOSE_TIME_STAMP;
    }

    public ArrayList<VaccinationModel> fetchAllVaccinationMO(boolean isByDate) {
        ArrayList<VaccinationModel> vaccinationModelArrayList = new ArrayList<>();

        Cursor mCursor;
        if (isByDate) {
            mCursor = database.query(true, VACCINATION_TABLE, null, null, null, null,
                    null, DOSE_TIME_STAMP + " ASC", null);
        } else {
            mCursor = database.query(true, VACCINATION_TABLE, null, null, null, null,
                    null, null, null);
        }

        for (int i = 0; mCursor != null && mCursor.moveToNext(); i++) {
            String vaccinationType = mCursor.getString(mCursor.getColumnIndex(VACCINATION_TYPE));
            String vaccinationName = mCursor.getString(mCursor.getColumnIndex(VACCINATION_NAME));
            String dosePeriod = mCursor.getString(mCursor.getColumnIndex(DOSE_PERIOD));
            String dueDate = mCursor.getString(mCursor.getColumnIndex(DUE_DATE));
            VaccinationModel vaccinationModel = new VaccinationModel();
            vaccinationModel.setId(mCursor.getInt(mCursor.getColumnIndex(ID)));
            vaccinationModel.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));
            vaccinationModel.setDueDate(dueDate);
            vaccinationModel.setVaccinationType(vaccinationType);
            vaccinationModel.setVaccinationName(vaccinationName);
            vaccinationModel.setVaccinationTaken(mCursor.getInt(mCursor.getColumnIndex(IS_VACCINATION_TAKEN)) == 1 ? true : false);
            vaccinationModel.setReminderToShow(mCursor.getInt(mCursor.getColumnIndex(IS_REMINDER_TO_SHOW)) == 1 ? true : false);
            vaccinationModel.setDose(mCursor.getString(mCursor.getColumnIndex(DOSE)));
            vaccinationModel.setDays(mCursor.getString(mCursor.getColumnIndex(DAYS)));
            vaccinationModel.setWeeks(mCursor.getString(mCursor.getColumnIndex(WEEKS)));
            vaccinationModel.setMonths(mCursor.getString(mCursor.getColumnIndex(MONTHS)));
            vaccinationModel.setYears(mCursor.getString(mCursor.getColumnIndex(YEARS)));
            vaccinationModel.setDosePeriod(dosePeriod);
            vaccinationModel.setDoseTimeStamp(mCursor.getLong(mCursor.getColumnIndex(DOSE_TIME_STAMP)));
            vaccinationModel.setReminderUniqueKey(mCursor.getInt(mCursor.getColumnIndex(REMINDER_TIME_STAMP)));
            if (i == 0) {
                VaccinationModel vaccinationModelHeader = new VaccinationModel();
                vaccinationModelHeader.setDueDate(dueDate);
                vaccinationModelHeader.setDosePeriod(dosePeriod);
                vaccinationModelHeader.setVaccinationType(vaccinationType);
                vaccinationModelHeader.setVaccinationName(vaccinationName);
                vaccinationModelHeader.setHeader(true);
                vaccinationModelArrayList.add(vaccinationModelHeader);
                i++;
            } else if (isByDate && vaccinationModelArrayList.get(i - 1).getDoseTimeStamp() != 0 && vaccinationModelArrayList.get(i - 1).getDoseTimeStamp() != vaccinationModel.getDoseTimeStamp()) {
                VaccinationModel vaccinationModelHeader = new VaccinationModel();
                vaccinationModelHeader.setDueDate(dueDate);
                vaccinationModelHeader.setDosePeriod(dosePeriod);
                vaccinationModelHeader.setVaccinationType(vaccinationType);
                vaccinationModelHeader.setVaccinationName(vaccinationName);
                vaccinationModelHeader.setHeader(true);
                vaccinationModelArrayList.add(vaccinationModelHeader);
                i++;
            } else if (!isByDate && vaccinationModelArrayList.get(i - 1).getVaccinationType() != null && !vaccinationModelArrayList.get(i - 1).getVaccinationType().equals(vaccinationModel.getVaccinationType())) {
                VaccinationModel vaccinationModelHeader = new VaccinationModel();
                vaccinationModelHeader.setVaccinationType(vaccinationType);
                vaccinationModelHeader.setVaccinationName(vaccinationName);
                vaccinationModelHeader.setDueDate(dueDate);
                vaccinationModelHeader.setDosePeriod(dosePeriod);
                vaccinationModelHeader.setHeader(true);
                vaccinationModelArrayList.add(vaccinationModelHeader);
                i++;
            }
            vaccinationModelArrayList.add(vaccinationModel);
        }
        mCursor.close();
        return vaccinationModelArrayList;
    }

    public ArrayList<VaccinationModel> updateVaccinationMo(Context context, String dateOfBirth, boolean isDobUpdated) {
        Cursor mCursor = database.query(true, VACCINATION_TABLE, null, null, null, null,
                null, null, null);
        ArrayList<VaccinationModel> vaccinationReminderList = new ArrayList<>();
        while (mCursor.moveToNext()) {
            try {
                VaccinationModel vaccinationModel = getVaccinationTableData(mCursor);
                ContentValues contentValues = new ContentValues();
                String days = vaccinationModel.getDays();
                String weeks = vaccinationModel.getWeeks();
                String months = vaccinationModel.getMonths();
                String years = vaccinationModel.getYears();
                Calendar vaccinationCalendar = VaccinationJosnParsing.gettheNextdoseTimestamp(days, weeks, months, years, dateOfBirth);
                SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime);
                String updatedDate = sdf.format(vaccinationCalendar.getTimeInMillis());
                contentValues.put(DUE_DATE, updatedDate);
                contentValues.put(DOSE_TIME_STAMP, vaccinationCalendar.getTimeInMillis());
                vaccinationModel.setDueDate(updatedDate);
                vaccinationModel.setDoseTimeStamp(vaccinationCalendar.getTimeInMillis());
                updateVaccinationMo(vaccinationModel.getId(), contentValues);
                if (isDobUpdated) {
                    vaccinationReminderList.add(vaccinationModel);
                }
            } catch (Exception ignore) {

            }
        }
        return vaccinationReminderList;
    }
}
