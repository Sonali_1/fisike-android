package com.mphrx.fisike.vaccination.background;

import android.content.Context;

import com.mphrx.fisike.asynctaskmanger.AbstractAsyncTask;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationProfileDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;

/**
 * Created by Kailash Khurana on 6/19/2016.
 */
public class OpenVaccinationFragmenet extends AbstractAsyncTask<Void, Void, VaccinationProfile> {
    private Context context;

    public OpenVaccinationFragmenet(ApiResponseCallback<VaccinationProfile> callback, Context context, boolean isToShowProgress) {
        super(callback, context, isToShowProgress);
        this.context = context;
    }

    @Override
    protected VaccinationProfile getResult(Void... params) {
        try {
            return VaccinationProfileDBAdapter.getInstance(context).fetchVaccinationProfileMO(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
