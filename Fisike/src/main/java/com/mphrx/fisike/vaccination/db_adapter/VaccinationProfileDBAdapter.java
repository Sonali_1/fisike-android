package com.mphrx.fisike.vaccination.db_adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.persistence.DBHelper;
import com.mphrx.fisike.persistence.SettingsDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by Kailash Khurana on 6/15/2016.
 */
public class VaccinationProfileDBAdapter {

    private static VaccinationProfileDBAdapter vaccinationDBAdapter;

    private Context context;
    private SQLiteDatabase database;

    public static String getVaccinationProfileTable() {
        return VACCINATION_PROFILE_TABLE;
    }

    private static final String VACCINATION_PROFILE_TABLE = "VaccinationProfileMO";
    private static final String ID = "id";
    private static final String FIRSTNAME = "firstName";
    private static final String LASTNAME = "lastName";
    private static final String GENDER = "gender";
    private static final String DATEOFBITH = "dateOfBirth";
    private static final String PERSISTENCEKEY = "persistenceKey";

    public VaccinationProfileDBAdapter(Context context) {
        this.context = context;
    }

    public static VaccinationProfileDBAdapter getInstance(Context ctx) {
        if (null == vaccinationDBAdapter) {
            synchronized (SettingsDBAdapter.class) {
                if (vaccinationDBAdapter == null) {

                    vaccinationDBAdapter = new VaccinationProfileDBAdapter(ctx);
                    vaccinationDBAdapter.open();
                }
            }
        }
        return vaccinationDBAdapter;
    }

    public static String getID() {
        return ID;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }


    public boolean insert(VaccinationProfile vaccinationProfile) throws Exception {
        ContentValues values = new ContentValues();
        values.put(PERSISTENCEKEY, vaccinationProfile.getPersistenceKey());
        values.put(ID, vaccinationProfile.getId());
        values.put(FIRSTNAME, vaccinationProfile.getFirstName());
        values.put(LASTNAME, vaccinationProfile.getLastName());
        values.put(GENDER, vaccinationProfile.getGender());
        values.put(DATEOFBITH, vaccinationProfile.getDateOfBirth());

        long count = database.insert(VACCINATION_PROFILE_TABLE, null, values);
        if (count == -1) {
            return false;
        }
        return true;
    }

    public VaccinationProfile fetchVaccinationProfileMO(int id) throws Exception {
        Cursor mCursor = database.query(true, VACCINATION_PROFILE_TABLE, null, ID + " = " + id, null, null,
                null, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            VaccinationProfile vaccinationProfile = new VaccinationProfile();
            vaccinationProfile.setId(mCursor.getInt(mCursor.getColumnIndex(ID)));
            vaccinationProfile.setFirstName(mCursor.getString(mCursor.getColumnIndex(FIRSTNAME)));
            vaccinationProfile.setLastName(mCursor.getString(mCursor.getColumnIndex(LASTNAME)));
            vaccinationProfile.setGender(mCursor.getString(mCursor.getColumnIndex(GENDER)));
            vaccinationProfile.setDateOfBirth(mCursor.getString(mCursor.getColumnIndex(DATEOFBITH)));
            vaccinationProfile.setPersistenceKey(mCursor.getLong(mCursor.getColumnIndex(PERSISTENCEKEY)));

            mCursor.close();
            return vaccinationProfile;
        } else {
            if (mCursor != null) {
                mCursor.close();
            }
            return null;
        }
    }


    public boolean updateVaccinationProfileMo(int id, ContentValues contentValues) {
        int count = -1;
        try {
            count = database.update(VACCINATION_PROFILE_TABLE, contentValues, ID + "=" + id, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String createVaccinationProfileTableQuery() {
        String query = "CREATE TABLE " + VACCINATION_PROFILE_TABLE + " ( " +
                ID + " INTEGER  PRIMARY KEY AUTOINCREMENT ," +
                PERSISTENCEKEY + " varchar(255) ," +
                FIRSTNAME + " varchar(255), " +
                LASTNAME + " varchar(255), " +
                GENDER + " varchar(255), " +
                DATEOFBITH + " varchar(255) )";

        return query;
    }


    public static String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public static String getLASTNAME() {
        return LASTNAME;
    }

    public static String getGENDER() {
        return GENDER;
    }

    public static String getDATEOFBITH() {
        return DATEOFBITH;
    }
}
