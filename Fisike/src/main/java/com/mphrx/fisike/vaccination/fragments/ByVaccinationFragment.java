package com.mphrx.fisike.vaccination.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.vaccination.adapter.VaccinationListAdapter;
import com.mphrx.fisike.vaccination.background.VaccinationJosnParsing;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.services.VaccinationAlarm;
import com.mphrx.fisike.view.DividerItemDecoration;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 6/17/2016.
 */
public class ByVaccinationFragment extends Fragment {

    private View myFragmentView;
    private RecyclerView listVaccination;
    private VaccinationListAdapter adapter;
    private ArrayList<VaccinationModel> itemList;
    private String dob;

    public ByVaccinationFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_vaccination_dose, container, false);

        if (getArguments() != null && getArguments().containsKey(VariableConstants.VACCINATION_PROFILE_DOB)) {
            dob = getArguments().getString(VariableConstants.VACCINATION_PROFILE_DOB);
        }

        findViews();
        initView();

        return myFragmentView;
    }

    private void findViews() {
        listVaccination = (RecyclerView) myFragmentView.findViewById(R.id.listVaccination);
    }

    private void initView() {
        listVaccination.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listVaccination.setLayoutManager(linearLayoutManager);
//        listVaccination.addItemDecoration(new DividerItemDecoration(getActivity()));

        refreshAdapter();
    }

    public void refreshAdapter() {
        itemList = new ArrayList<>();
        adapter = new VaccinationListAdapter(itemList, false, getActivity(), this);
        listVaccination.setAdapter(adapter);
        ((BaseActivity) getActivity()).setList(dob, adapter, false);

    }

    public void refreshOtherAdapter() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && parentFragment instanceof VaccinationListFragment) {
            ((VaccinationListFragment) parentFragment).refreshFragment(true);
        }
    }
}