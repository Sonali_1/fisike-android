package com.mphrx.fisike.vaccination.background;

import android.content.ContentValues;
import android.content.Context;

import com.mphrx.fisike.asynctaskmanger.AbstractAsyncTask;
import com.mphrx.fisike.interfaces.ApiResponseCallback;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationDBAdapter;
import com.mphrx.fisike.vaccination.db_adapter.VaccinationProfileDBAdapter;
import com.mphrx.fisike.vaccination.mo.VaccinationInsertData;
import com.mphrx.fisike.vaccination.mo.VaccinationModel;
import com.mphrx.fisike.vaccination.mo.VaccinationProfile;

import java.util.ArrayList;

/**
 * Created by Kailash Khurana on 6/16/2016.
 */
public class SaveVaccinationProfile extends AbstractAsyncTask<Void, Void, VaccinationInsertData> {
    private Context context;
    private VaccinationProfile vaccinationProfile;
    private boolean isUpdateVaccinationProfile;
    private boolean isDOBUpdated;

    public SaveVaccinationProfile(ApiResponseCallback<VaccinationInsertData> callback, Context context, boolean isToShowProgress, VaccinationProfile vaccinationProfile, boolean isUpdateVaccinationProfile, boolean isDOBUpdated) {
        super(callback, context, isToShowProgress);
        this.context = context;
        this.vaccinationProfile = vaccinationProfile;
        this.isUpdateVaccinationProfile = isUpdateVaccinationProfile;
        this.isDOBUpdated = isDOBUpdated;
    }

    @Override
    protected VaccinationInsertData getResult(Void... params) {
        boolean isInsertedOrUpdated;
        ArrayList<VaccinationModel> vaccinationModelList = null;
        try {
            if (isUpdateVaccinationProfile) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(VaccinationProfileDBAdapter.getFIRSTNAME(), vaccinationProfile.getFirstName());
                contentValues.put(VaccinationProfileDBAdapter.getLASTNAME(), vaccinationProfile.getLastName());
                contentValues.put(VaccinationProfileDBAdapter.getDATEOFBITH(), vaccinationProfile.getDateOfBirth());
                contentValues.put(VaccinationProfileDBAdapter.getGENDER(), vaccinationProfile.getGender());
                isInsertedOrUpdated = VaccinationProfileDBAdapter.getInstance(context).updateVaccinationProfileMo(1, contentValues);
                vaccinationModelList = VaccinationDBAdapter.getInstance(context).updateVaccinationMo(context, vaccinationProfile.getDateOfBirth(), isDOBUpdated);
            } else {
                isInsertedOrUpdated = VaccinationProfileDBAdapter.getInstance(context).insert(vaccinationProfile);
            }
            if (isInsertedOrUpdated) {
                VaccinationProfile vaccinationProfile = VaccinationProfileDBAdapter.getInstance(context).fetchVaccinationProfileMO(1);
                VaccinationInsertData vaccinationInsertData = new VaccinationInsertData();
                vaccinationInsertData.setVaccinationProfile(vaccinationProfile);
                vaccinationInsertData.setVaccinationModelList(vaccinationModelList);
                return vaccinationInsertData;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
