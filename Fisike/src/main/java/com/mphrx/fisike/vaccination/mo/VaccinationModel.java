package com.mphrx.fisike.vaccination.mo;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.mo.MobileObject;

/**
 * Created by Kailash Khurana on 6/17/2016.
 */
public class VaccinationModel extends MobileObject implements Comparable<VaccinationModel> {
    private int id;
    private String dueDate;// due on
    private String vaccinationType;
    private String vaccinationName;
    private String dose;
    private boolean isVaccinationTaken;
    private boolean isReminderToShow;
    private String days;
    private String weeks;
    private String months;
    private String years;
    private boolean isHeader;
    private String dosePeriod; // At birth 6-7 weeks
    private long doseTimeStamp;
    private int reminderUniqueKey;

    public long getDoseTimeStamp() {
        return doseTimeStamp;
    }

    public void setDoseTimeStamp(long doseTimeStamp) {
        this.doseTimeStamp = doseTimeStamp;
    }


    public VaccinationModel() {
        super();
    }

    protected VaccinationModel(Parcel in) {
        super(in);
        id = in.readInt();
        dueDate = in.readString();
        vaccinationType = in.readString();
        vaccinationName = in.readString();
        dose = in.readString();
        isVaccinationTaken = in.readByte() != 0 ? true : false;
        isReminderToShow = in.readByte() != 0 ? true : false;
        days = in.readString();
        weeks = in.readString();
        months = in.readString();
        years = in.readString();
        isHeader = in.readByte() != 0 ? true : false;
        dosePeriod = in.readString();
        doseTimeStamp = in.readLong();
        reminderUniqueKey = in.readInt();
    }

    public static final Parcelable.Creator<VaccinationModel> CREATOR = new Parcelable.Creator<VaccinationModel>() {
        @Override
        public VaccinationModel createFromParcel(Parcel in) {
            return new VaccinationModel(in);
        }

        @Override
        public VaccinationModel[] newArray(int size) {
            return new VaccinationModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getVaccinationType() {
        return vaccinationType;
    }

    public void setVaccinationType(String vaccinationType) {
        this.vaccinationType = vaccinationType;
    }

    public String getVaccinationName() {
        return vaccinationName;
    }

    public void setVaccinationName(String vaccinationName) {
        this.vaccinationName = vaccinationName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public boolean isVaccinationTaken() {
        return isVaccinationTaken;
    }

    public void setVaccinationTaken(boolean vaccinationTaken) {
        isVaccinationTaken = vaccinationTaken;
    }

    public boolean isReminderToShow() {
        return isReminderToShow;
    }

    public void setReminderToShow(boolean reminderToShow) {
        isReminderToShow = reminderToShow;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getWeeks() {
        return weeks;
    }

    public void setWeeks(String weeks) {
        this.weeks = weeks;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getDosePeriod() {
        return dosePeriod;
    }

    public void setDosePeriod(String dosePeriod) {
        this.dosePeriod = dosePeriod;
    }

    public int getReminderUniqueKey() {
        return reminderUniqueKey;
    }

    public void setReminderUniqueKey(int reminderUniqueKey) {
        this.reminderUniqueKey = reminderUniqueKey;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(id);
        dest.writeString(dueDate);
        dest.writeString(vaccinationType);
        dest.writeString(vaccinationName);
        dest.writeString(dose);
        dest.writeByte((byte) (isVaccinationTaken ? 1 : 0));
        dest.writeByte((byte) (isReminderToShow ? 1 : 0));
        dest.writeString(days);
        dest.writeString(weeks);
        dest.writeString(months);
        dest.writeString(years);
        dest.writeByte((byte) (isHeader ? 1 : 0));
        dest.writeString(dosePeriod);
        dest.writeLong(doseTimeStamp);
        dest.writeInt(reminderUniqueKey);
    }


    @Override
    public int compareTo(VaccinationModel vaccinationModel) {
        if (this.getDoseTimeStamp() > vaccinationModel.getDoseTimeStamp()) {
            return 1;
        } else if (this.getDoseTimeStamp() < vaccinationModel.getDoseTimeStamp()) {
            return -1;
        } else if (this.getDoseTimeStamp() == vaccinationModel.getDoseTimeStamp()) {
            return 0;
        }
        return 0;
    }
}
