package com.mphrx.fisike.vaccination.mo;

import android.os.Parcel;
import android.os.Parcelable;

import com.mphrx.fisike.mo.MobileObject;

/**
 * Created by Kailash Khurana on 6/15/2016.
 */
public class VaccinationProfile extends MobileObject {
    private Integer id;
    private String firstName;
    private String lastName;
    private String gender;
    private String dateOfBirth;

    public VaccinationProfile() {
        super();
    }

    protected VaccinationProfile(Parcel in) {
        super(in);
        id = in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        gender = in.readString();
        dateOfBirth = in.readString();
    }

    public static final Parcelable.Creator<VaccinationProfile> CREATOR = new Parcelable.Creator<VaccinationProfile>() {
        @Override
        public VaccinationProfile createFromParcel(Parcel in) {
            return new VaccinationProfile(in);
        }

        @Override
        public VaccinationProfile[] newArray(int size) {
            return new VaccinationProfile[size];
        }
    };

    public VaccinationProfile(String firstName, String lastName, String gender, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.id=1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(gender);
        dest.writeString(dateOfBirth);
    }

}
