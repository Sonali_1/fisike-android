package com.mphrx.fisike.AppTour;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;

public class AppTourScreenSliderFragmentScreen5 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.activity_app_tour_screen_slider_fragment_screen5, container, false);
        ImageView apptourImageView = (ImageView) rootView.findViewById(R.id.iv_appTourImage);
        apptourImageView.setImageDrawable(null);   //This will force the image to properly refresh
        apptourImageView.setImageResource(R.drawable.app_tour5);
        return rootView;
    }


}
