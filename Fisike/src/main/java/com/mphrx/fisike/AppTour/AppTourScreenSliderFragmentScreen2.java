package com.mphrx.fisike.AppTour;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;

public class AppTourScreenSliderFragmentScreen2 extends Fragment {
    private com.mphrx.fisike.customview.CustomFontButton skip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.sliding_apptour_fragmentscreen_two, container, false);
        ImageView apptourImageView = (ImageView) rootView.findViewById(R.id.iv_appTourImage);
        apptourImageView.setImageDrawable(null);   //This will force the image to properly refresh
        apptourImageView.setImageResource(R.drawable.app_tour2);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        super.onAttach(activity);

    }


}
