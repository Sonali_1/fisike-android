package com.mphrx.fisike.AppTour;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mphrx.fisike.R;

/**
 * Created by administrate on 1/7/2016.
 */
public class AppTourScreenSliderFragmentScreen4 extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.sliding_apptour_fragmentscreen_four, container, false);
        ImageView apptourImageView = (ImageView) rootView.findViewById(R.id.iv_appTourImage);
        apptourImageView.setImageDrawable(null);   //This will force the image to properly refresh
        apptourImageView.setImageResource(R.drawable.app_tour4);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }
}
