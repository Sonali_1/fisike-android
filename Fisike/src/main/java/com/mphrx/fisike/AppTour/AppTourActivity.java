package com.mphrx.fisike.AppTour;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.AppTourAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.LaunchingScreen;
import com.mphrx.fisike.view.CirclePageIndicator;
import com.mphrx.fisike_physician.activity.OtpActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

import login.activity.LoginActivity;


public class AppTourActivity extends FragmentActivity implements OnPageChangeListener {

    private ViewPager slidingViewPager;
    private PagerAdapter slidingPagerAdapter;
    private CirclePageIndicator mIndicator;
    private com.mphrx.fisike.customview.CustomFontButton skip;
    private List<Fragment> fragments = new ArrayList<Fragment>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_tour);
        slidingViewPager = (ViewPager) findViewById(R.id.pager);
        skip = (com.mphrx.fisike.customview.CustomFontButton) findViewById(R.id.btn_skip);
        fragments = getFragments();
        slidingPagerAdapter = new AppTourAdapter(getSupportFragmentManager(), skip, fragments);
        slidingViewPager.setAdapter(slidingPagerAdapter);
        slidingViewPager.addOnPageChangeListener(this);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(slidingViewPager);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_skip:
                SharedPreferences sharedpref = getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
                Editor edit = sharedpref.edit();
                edit.putBoolean(VariableConstants.APP_TOUR_SHOWN, true);
                edit.commit();
                UserMO userMo = SettingManager.getInstance().getUserMO();
                if(BuildConfig.isPatientApp) {
                    if (userMo != null && userMo.getSalutation().equalsIgnoreCase(TextConstants.YES))
                        finish();
                    else
                        LaunchingScreen.getInstance().launchDesion(this);
                }
                else {
                    if (SharedPref.getAccessToken() == null) {
                        startAndFinishActivity(LoginActivity.class);
                    }
                    else
                        finish();
                }
        }
    }

    private void startAndFinishActivity(Class<?> activity) {
        Intent i=new Intent(this,activity);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        this.finish();
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }


    @Override
    public void onBackPressed() {
        if (slidingViewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            slidingViewPager.setCurrentItem(slidingViewPager.getCurrentItem() - 1);
        }
    }


    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub

        if (arg0 == (fragments.size() - 1))
            skip.setText(MyApplication.getAppContext().getResources().getString(R.string.done));
        else
            skip.setText(MyApplication.getAppContext().getResources().getString(R.string.skip));

    }

    public List<Fragment> getFragments() {
        fragments.add(new AppTourScreenSliderFragmentScreen1());
        fragments.add(new AppTourScreenSliderFragmentScreen2());
        fragments.add(new AppTourScreenSliderFragmentScreen3());
        fragments.add(new AppTourScreenSliderFragmentScreen4());
        fragments.add(new AppTourScreenSliderFragmentScreen5());
        fragments.add(new AppTourScreenSliderFragmentScreen6());
        fragments.add(new AppTourScreenSliderFragmentScreen7());
        return fragments;
    }

}
