package com.mphrx.fisike.add_alternate_contact;

/**
 * Created by Neha on 12-04-2016.
 */
public class OtpGsonResponse {

    String msg;
    String status;
    boolean messageSent;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isMessageSent() {
        return messageSent;
    }

    public void setMessageSent(boolean messageSent) {
        this.messageSent = messageSent;
    }
}
