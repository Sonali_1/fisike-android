package com.mphrx.fisike.add_alternate_contact.Asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.mphrx.fisike.ForgetPasswordActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.add_alternate_contact.AddAlternateContact;
import com.mphrx.fisike.add_alternate_contact.OtpGsonResponse;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Neha on 12-04-2016.
 */
public class SendOtpToAlternateContactRequest extends AsyncTask<Void, Void, String> {
    Context context;
    private ProgressDialog pd_ring;
    OtpGsonResponse response;
    private String mAlternateContact;
    private String mAlternateContactType;


    public SendOtpToAlternateContactRequest(String mAlternateContact, String mAlternateContactType, Context context) {
        super();
        this.mAlternateContact = mAlternateContact;
        this.mAlternateContactType = mAlternateContactType;
        this.context = context;
        pd_ring = new ProgressDialog(context);
        pd_ring.setMessage(MyApplication.getAppContext().getResources().getString(R.string.sending_4_digit_code));
        pd_ring.setCancelable(false);
        pd_ring.setCanceledOnTouchOutside(false);
    }


    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        pd_ring.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {

        try {
            response = executeSendOtpApi();
            if (response != null)
                return TextConstants.SUCESSFULL_API_CALL;
            else
                return MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again);
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getMessage().equals(TextConstants.UNAUTHORIZED_ACCESS))
                return MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again);
            else if (e.getMessage().equals(TextConstants.SERVICE_UNAVAILABLE))
                return MyApplication.getAppContext().getResources().getString(R.string.unable_contact_server_error);
            else
                return MyApplication.getAppContext().getResources().getString(R.string.something_wrong_try_again);
        }


    }

    @Override
    protected void onPostExecute(String result) {
        if (pd_ring != null && pd_ring.isShowing()) {
            pd_ring.dismiss();
            pd_ring = null;
        }
        ((AddAlternateContact) context).executeSuccessfullSendOtpResult(result, response);
    }

    private OtpGsonResponse executeSendOtpApi() throws Exception {

        APIManager apiManager = APIManager.getInstance();
        String mailVerificationUrl = apiManager.sendOtpToAlternateContactUrl();
        String response = apiManager.executeHttpPost(mailVerificationUrl, SharedPref.getAccessToken(), getPayLoad(), context);
        if (response != null) {

            if (response.equals(TextConstants.UNAUTHORIZED_ACCESS))
                throw new Exception(TextConstants.UNAUTHORIZED_ACCESS);
            else if (response.equals(TextConstants.SERVICE_UNAVAILABLE))
                throw new Exception(TextConstants.SERVICE_UNAVAILABLE);
            else if (response.equals(TextConstants.UNEXPECTED_ERROR))
                throw new Exception(TextConstants.UNEXPECTED_ERROR);
            else {
                Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response, OtpGsonResponse.class);
                if (jsonToObjectMapper instanceof OtpGsonResponse)
                    return ((OtpGsonResponse) jsonToObjectMapper);
            }
        } else
            throw new Exception(TextConstants.UNEXPECTED_ERROR);

        return null;
    }


    private JSONObject getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.ALTERNATE_CONTACT, mAlternateContact);
            payLoad.put(MphRxUrl.K.CONTACT_TYPE, mAlternateContactType);
            AppLog.showInfo(getClass().getSimpleName(), payLoad.toString());
        } catch (Exception e) {
            AppLog.showError(getClass().getSimpleName(), e.getMessage());
            // Do Nothing
        }
        return payLoad;
    }
}