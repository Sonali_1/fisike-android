package com.mphrx.fisike.add_alternate_contact;

import android.content.Context;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.add_alternate_contact.Asynctask.SendOtpToAlternateContactRequest;
import com.mphrx.fisike.add_alternate_contact.Asynctask.VerifyOtpForAlternateContactRequest;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.response.OTPResponse.VerifyOTPResponseGson;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.update_user_profile.UpdateProfileBaseActivity;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.CountryAdapter;
import com.mphrx.fisike_physician.network.response.UpdateProfileResponse;
import com.mphrx.fisike_physician.services.SMSReceiver;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.views.TimerView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class AddAlternateContact extends UpdateProfileBaseActivity implements SMSReceiver.SMSReceiverListener {

    private boolean mPrefillNumber;

    public static final int MODE_SEND_OTP = 0;
    public static final int MODE_AUTOVERIFY_OTP = 1;
    public static final int MODE_MANUAL_VERIFY_OTP = 2;
    public static final int MODE_EMAIL_VERIFY_OTP = 3;
    private CustomFontEditTextView mCountryCodes, mMobileNumber;
    private CustomFontButton mSubmit;
    private TextView tvOtpVerifyText;
    private TimerView mTimerProgressBar;
    private CustomFontTextView tv_otp_verify_text_patient;
    private CustomFontEditTextView mOTPText, mEmailAddress;
    private CustomFontButton mSubmitNext;
    private CountDownTimer mCountTimer;
    private RelativeLayout rlVerifyOtp, rlSendOtp;
    private SMSReceiver mReceiver;
    private String alternatContactType;
    private String alternateContact;
    private ImageView iv_sad;
    private LinearLayout ll_manual_verification_line;
    boolean isResendOtp = false;
    /*[********** Edited by laxman *******]*/
    private Spinner spinner_change_country_code;
    private int position = 0;
    /*[********** End of Edited by laxman *******]*/
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private static CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    private CustomFontTextView autoVerifyCode;
    private CustomFontTextView get_started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_add_alternate_contact, frameLayout);
        //setContentView(R.layout.activity_add_alternate_contact);
        mPrefillNumber = getIntent().getBooleanExtra(TextConstants.PREFILL_NUM, false);
        findView();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onUpdateProfileResponse(UpdateProfileResponse response) {
        if (response.getTransactionId() != super.getTransactionId())
            return;
        super.updateProfileRequest(response);
    }

    private void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mMobileNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        mEmailAddress.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        spinner_change_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                String countrycode = stringTokenizer.nextToken().toString().trim();
                String countrycodeplus = "+" +countrycode;
                if (Integer.parseInt(countrycode) == TextConstants.INDIA_COUNTRYCODE) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    mMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    /*[  .... Moblile length set on   [ max_length- countrycode(including+) ]   ........  ]*/
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countrycodeplus.length()));
                    mMobileNumber.setFilters(FilterArray);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mCountTimer = new CountDownTimer(30 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimerProgressBar.setTimerProgress(30, millisUntilFinished / 1000);
                iv_sad.setVisibility(View.GONE);
                autoVerifyCode.setVisibility(View.GONE);
            }

            @Override
            public void onFinish() {

                mTimerProgressBar.setVisibility(View.GONE);
                iv_sad.setVisibility(View.VISIBLE);
                autoVerifyCode.setVisibility(View.VISIBLE);
                autoVerifyCode.setText(getString(R.string.automatic_sms_failure_patient));
            }
        };

        mEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mMobileNumber.getText().toString().equals(""))
                    mMobileNumber.setText("");
            }
        });


        mMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEmailAddress.getText().toString().equals(""))
                    mEmailAddress.setText("");
            }
        });

        showView(MODE_SEND_OTP);

        TelephonyManager telMgr = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_READY:
                setCountryCode(true);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                setCountryCode(false);
                break;
            default:
                setCountryCode(false);
                break;

        }

        if(!BuildConfig.isForgetPassword){
            get_started.setVisibility(View.GONE);
        }

    }


    public void findView() {
        mCountryCodes = (CustomFontEditTextView) findViewById(R.id.country_code);
        mMobileNumber = (CustomFontEditTextView) findViewById(R.id.et_mobile_number);
        spinner_change_country_code = (Spinner) findViewById(R.id.spinner_change_country_code);
        mSubmit = (CustomFontButton) findViewById(R.id.btn_ok);
        mCountryCodes.setFocusable(false);
        tvOtpVerifyText = (TextView) findViewById(R.id.tv_otp_verify_text_patient);
        autoVerifyCode = (CustomFontTextView) findViewById(R.id.autoVerifyCode);
        mTimerProgressBar = (TimerView) findViewById(R.id.pb_custom_timer_patient);
        iv_sad = (ImageView) findViewById(R.id.iv_sad);
        tv_otp_verify_text_patient = (CustomFontTextView) findViewById(R.id.tv_otp_verify_text_patient);
        mOTPText = (CustomFontEditTextView) findViewById(R.id.et_manual_otp_patient);
        mSubmitNext = (CustomFontButton) findViewById(R.id.btn_next);
        mEmailAddress = (CustomFontEditTextView) findViewById(R.id.et_email);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        rlSendOtp = (RelativeLayout) findViewById(R.id.rl_container_send_otp_alternate_contact);
        rlVerifyOtp = (RelativeLayout) findViewById(R.id.rl_container_verify_otp_alternate_contact);
        ll_manual_verification_line = (LinearLayout) findViewById(R.id.ll_manual_verification_line);
        get_started = (CustomFontTextView)findViewById(R.id.get_started);
    }


    private void stopTimer() {
        if (mCountTimer != null)
            mCountTimer.cancel();
    }

    private void startTimer() {
        stopTimer();
        if (mCountTimer != null) {
            mTimerProgressBar.setVisibility(View.VISIBLE);
            mCountTimer.start();
        }
    }


    public void setToolbarTitleText(String title) {
        toolbar_title.setText(title);
    }

    private void showView(int mode) {

        switch (mode) {
            case MODE_SEND_OTP:
                stopTimer();
                rlSendOtp.setVisibility(View.VISIBLE);
                rlVerifyOtp.setVisibility(View.GONE);
                setToolbarTitleText(getResources().getString(R.string.title_alternatecontact));
                unregisterSMSReceiver();
                break;

            case MODE_AUTOVERIFY_OTP:
                startTimer();
                registerSMSReceiver();
                tv_otp_verify_text_patient.setText(getResources().getString(R.string.enter_code_sent) + " : " + alternateContact);
                //autoVerifyCode.setText(getResources().getString(R.string.manual_verification) + " : " + alternateContact);
                autoVerifyCode.setVisibility(View.GONE);
                rlSendOtp.setVisibility(View.GONE);
                rlVerifyOtp.setVisibility(View.VISIBLE);
                ll_manual_verification_line.setVisibility(View.VISIBLE);
                setToolbarTitleText(getResources().getString(R.string.title_verify));
                break;

            case MODE_MANUAL_VERIFY_OTP:
                unregisterSMSReceiver();
                rlSendOtp.setVisibility(View.GONE);
                tv_otp_verify_text_patient.setText(getResources().getString(R.string.enter_code_sent) + " : " + alternateContact);
                rlVerifyOtp.setVisibility(View.VISIBLE);
                setToolbarTitleText(getResources().getString(R.string.title_verify));
                mTimerProgressBar.setVisibility(View.GONE);
                iv_sad.setVisibility(View.VISIBLE);
                ll_manual_verification_line.setVisibility(View.VISIBLE);
                break;

            case MODE_EMAIL_VERIFY_OTP:
                unregisterSMSReceiver();
                stopTimer();
                mTimerProgressBar.setVisibility(View.GONE);
                tv_otp_verify_text_patient.setText(getResources().getString(R.string.enter_code_sent) + " : " + alternateContact);
                rlSendOtp.setVisibility(View.GONE);
                rlVerifyOtp.setVisibility(View.VISIBLE);
                iv_sad.setVisibility(View.GONE);
                autoVerifyCode.setVisibility(View.GONE);
                setToolbarTitleText(getResources().getString(R.string.title_verify));
                ll_manual_verification_line.setVisibility(View.INVISIBLE);
                break;

        }
    }


    public void onClick(View view) {

        Utils.hideKeyboard(this);
        if (Utils.showDialogForNoNetwork(this, false)) {
            switch (view.getId()) {

                case R.id.btn_ok:
                    if (validateFieldAndCaptureAlternateContact()) {
                        isResendOtp = false;
                        callSendOtpApi(alternateContact, alternatContactType);
                    }


                    break;

                case R.id.btn_bottom:
                    mOTPText.setText("");
                    mOTPText.setErrorEnabled(false);
                    isResendOtp = true;
                    callSendOtpApi(alternateContact, alternatContactType);
                    break;

                case R.id.btn_next:
                    //Toast.makeText(this,"next button clicked",Toast.LENGTH_SHORT).show();
                    sendApiForValidateOtp();
                    break;

            }

        }
    }

    private void sendApiForValidateOtp() {
        String otpCode;
        otpCode = mOTPText.getText().toString();
        if (otpCode.equals(""))
            mOTPText.setError(getResources().getString(R.string.enter_valid_4_digit_code));
        else if (otpCode.length() != 4)
            mOTPText.setError(getResources().getString(R.string.enter_valid_4_digit_code));
        else
            new VerifyOtpForAlternateContactRequest(alternateContact, alternatContactType, otpCode, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void callSendOtpApi(String alternateContact, String alternatContactType) {
        new SendOtpToAlternateContactRequest(alternateContact, alternatContactType, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void onSendOtpResponse() {
        if (alternatContactType.equals(TextConstants.ALTERNATE_TYPE_PHONE))
            showView(MODE_AUTOVERIFY_OTP);
        else
            showView(MODE_EMAIL_VERIFY_OTP);
    }


    private boolean validateFieldAndCaptureAlternateContact() {

        String phone = mMobileNumber.getText().toString().trim();
        String email = mEmailAddress.getText().toString().trim();

        if (phone.equals("") && email.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.valid_number_or_email), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!phone.equals("")) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinner_change_country_code.getSelectedItem().toString().trim(), ",");
                String country_code = stringTokenizer.nextToken().trim().toString();
                String countrycodeplus = "+" + country_code;
                if (phone.trim().equals("")) {
                    setError(getResources().getString(R.string.enter_mobile_number));
                    return false;
                } else if (Integer.parseInt(country_code)== TextConstants.INDIA_COUNTRYCODE && (phone.length() != TextConstants.INDIA_PHONENO_LENGTH)) {
                    setError(getResources().getString(R.string.error_invalid_mobile_no));
                    return false;
                } else if ((countrycodeplus.length() + phone.length()) <= TextConstants.MIN_PHONENO_LENGTH) {
                    setError(getResources().getString(R.string.mobile_must_6digits_including));
                    return false;
                } else if ((countrycodeplus.length() + phone.length()) > TextConstants.MAX_PHONENO_LENGTH) {
                    setError(getResources().getString(R.string.error_invalid_mobile_no));
                    return false;
                } else if (Long.parseLong(phone) == 0) //to change server settings
                {
                    setError(getResources().getString(R.string.error_invalid_mobile_no));
                    return false;
                } else {

                    if (isValidateAlternatePhone(countrycodeplus + "" + phone)) {
                        alternatContactType = TextConstants.ALTERNATE_TYPE_PHONE;
                        alternateContact = country_code + "" + phone;
                        //    alternateContact = mCountryCodes.getHint().toString() + "" + phone;
                        return true;
                    } else
                        return false;
                }
            } else {
                if (isValidateAlternateEMail(email)) {
                    alternatContactType = TextConstants.ALTERNATE_TYPE_EMAIL;
                    alternateContact = email;
                    return true;
                } else
                    return false;
            }
        }


    }

    private void setError(CharSequence error) {
        mMobileNumber.setError(error);
    }

    private boolean isValidateAlternateEMail(String mail) {
        if (mail.equals(""))
            return true;
        else if (Utils.validateMail(mail))
            return true;
        else {
            mEmailAddress.setError(getResources().getString(R.string.enter_valid_email_address));
            return false;
        }
    }

    private boolean isValidateAlternatePhone(String phoneNumber) {
        if (phoneNumber.length() == 13) {
            if (phoneNumber.equals(SharedPref.getMobileNumber())) {
                Toast.makeText(this, getResources().getString(R.string.alternate_different_account_number), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }


    private void handleBack() {
        if (rlSendOtp.getVisibility() == View.VISIBLE)
            finish();
        else
            showView(MODE_SEND_OTP);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    @Override
    public void onSMSReceived(String otp) {
        unregisterSMSReceiver();
        verifyOTP(otp);
    }

    private void verifyOTP(String otp) {
        mOTPText.setText(otp);
    }

    public synchronized void registerSMSReceiver() {
        if (mReceiver == null) {
            mReceiver = new SMSReceiver(this);
            registerReceiver(mReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        }
    }

    private synchronized void unregisterSMSReceiver() {
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
        mReceiver = null;
    }


    private void sendApiForUpdate() {
        addAlternateContactFields(alternateContact);
    }

    public void executeSuccessfullSendOtpResult(String result, OtpGsonResponse response) {
        if (result.equals(TextConstants.SUCESSFULL_API_CALL)) {
            if (response.getStatus().equals("SC200")) {
                if (isResendOtp)
                    Toast.makeText(this, getResources().getString(R.string.sent_4_digit_code_again), Toast.LENGTH_SHORT).show();
                onSendOtpResponse();
            }
        } else
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }


    public void executeVerifyOtpResult(String result, VerifyOTPResponseGson response) {
        if (result.equals(TextConstants.SUCESSFULL_API_CALL)) {
            if (response.getStatus().equals("SC200")) {
                sendApiForUpdate();
            } else if (response.getMsg().equals(TextConstants.OTP_NOT_FOUND)) {
                mOTPText.setError(getResources().getString(R.string.enter_valid_4_digit_code));
            } else if (response.getMsg().equals(TextConstants.OTP_TIME_EXPIRED)) {
                Toast.makeText(this, getResources().getString(R.string.one_time_password_expired), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    /*[.................. Edited by laxman singh....................]*/
    public void setCountryCode(boolean on) {

        int country_code = 0;
        if (on) {
            try {
                country_code = Integer.parseInt(GetCountryZipCode(this).toString().trim());
            } catch (Exception e) {
                // TODO: handle exception
                country_code = 91;
            }
        } else if (!on) {
            country_code = 91;
        }
        String[] list = getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < list.length; i++) {
            StringTokenizer stringTokenizer = new StringTokenizer(list[i].toString().trim(), ",");
            //  String codeval =
            int valuecode = Integer.parseInt(stringTokenizer.nextToken());
            if (valuecode == country_code) {
                position = i;
                break;
            }
        }
        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        CountryAdapter adapter_country = new CountryAdapter(this, countrylist, "dusky_blue");
        spinner_change_country_code.setAdapter(adapter_country);
        spinner_change_country_code.setSelection(position);
    }

    public static String GetCountryZipCode(Context context) {
        String CountryZipCode = null;
        String CountryID = null;
        try {
            CountryID = getUserCountry(context).toString().toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
            CountryID = "IN";
        }

        try {
            String[] rl = context.getResources().getStringArray(
                    R.array.CountryCodes);
            for (int i = 0; i < rl.length; i++) {
                String[] g = rl[i].split(",");
                if (g[1].trim().equals(CountryID.trim())) {
                    CountryZipCode = g[0];
                    break;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return CountryZipCode;
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country
                // code is
                // available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }
    /*[.................. End of Edited by laxman singh....................]*/


}
