package com.mphrx.fisike;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.mphrx.fisike.customview.CustomFontTextView;

import android.widget.Toast;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

public class NewGroupActivity extends BaseActivity {
    private File photoUri;
    private Uri mImageCaptureUri = null;

    private ImageView ivProfilePic;
    private CustomFontEditTextView etGroupName;
    private CustomFontTextView tvGroupNameLength;
    private BitmapDrawable drawable;
    private Bitmap profilePicBitmap;

    private static final int SELECT_PHOTO = 1;
    private static final int CROP_RESULT = 2;
    private static final int INVITE_GROUP_MEMBERS = 0;

    private ArrayList<ChatContactMO> selectedMemberList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_new_group, frameLayout);
        //setContentView(R.layout.activity_new_group);
        ivProfilePic = (ImageView) findViewById(R.id.iv_grp_profile_pic);
        etGroupName = (CustomFontEditTextView) findViewById(R.id.et_group_name);
        etGroupName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        tvGroupNameLength = (CustomFontTextView) findViewById(R.id.tv_group_name_length);
        toolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        etGroupName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String space = " ";
                if (s.toString().trim().length() > 0) {
                    tvGroupNameLength.setVisibility(View.VISIBLE);
                } else {
                    tvGroupNameLength.setVisibility(View.GONE);
                }
                if (etGroupName.getText().toString().trim().length() > 24) {
                    Toast.makeText(NewGroupActivity.this, getResources().getString(R.string.group_name_character_limit_exceeded), Toast.LENGTH_LONG)
                            .show();
                }
                tvGroupNameLength.setText(VariableConstants.GROUP_NAME_CHARACTER_LIMIT - s.toString().trim().length() + "");
                if (s.toString().startsWith(space)) {
                    etGroupName.setText(s.toString().trim());
                } else
                    return;
            }
        });
        getIntentData();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
            getSupportActionBar().setTitle(getResources().getString(R.string.new_group));
        }

    }

    private void getIntentData() {
        Intent intent = getIntent();
        selectedMemberList = intent.getParcelableArrayListExtra(TextConstants.SELECTED_INVITE_INTENT);
        String intentgroupName = intent.getStringExtra(TextConstants.NEW_GROUP_NAME_INTENT);
        profilePicBitmap = intent.getParcelableExtra(TextConstants.NEW_GROUP_IMAGE_INTENT);
        if (intentgroupName != null && !intentgroupName.equals("")) {
            etGroupName.setText(intentgroupName);
        }
        if (profilePicBitmap != null) {
            ivProfilePic.setImageBitmap(profilePicBitmap);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_group, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                if (Utils.showDialogForNoNetwork(this, false)) {
                    startGroupChatInviteActivity();
                }
                break;
            case android.R.id.home:
                // startActivity(new Intent(this,HomeActivity.class));
                this.finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startGroupChatInviteActivity() {
        String groupName = etGroupName.getText().toString().trim();
        if (groupName == null || groupName.equals("")) {
            // show appropriate error message
            Toast.makeText(this, getResources().getString(R.string.Please_enter_a_group_Name), Toast.LENGTH_LONG).show();
            return;
        } else if (groupName != null) {
            Intent groupChatInviteActivityIntent = new Intent(this, GroupChatInviteActivity.class);
            groupChatInviteActivityIntent.putExtra(TextConstants.NEW_GROUP_NAME_INTENT, groupName);
            groupChatInviteActivityIntent.putExtra(VariableConstants.LAUNCHED_FROM_GROUP_DETAIL_ACTIVITY, false);
            if (selectedMemberList != null) {
                groupChatInviteActivityIntent.putParcelableArrayListExtra(TextConstants.SELECTED_INVITE_INTENT, selectedMemberList);
            }
            if (profilePicBitmap != null) {
                groupChatInviteActivityIntent.putExtra(TextConstants.NEW_GROUP_IMAGE_INTENT, profilePicBitmap);
            }
            startActivityForResult(groupChatInviteActivityIntent, INVITE_GROUP_MEMBERS);
        }
    }

    public void imageViewClicked(View view) {
        openSelectPopup();
    }

    /**
     * Open the android chooser for selecting Image
     */
    private void openSelectPopup() {
        // photoUri = null;

        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");
        galleryintent.putExtra("return-data", true);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        photoUri = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath(), System.currentTimeMillis()
                + ".jpg");

        photoUri.setWritable(true);
        mImageCaptureUri = Uri.fromFile(photoUri);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        cameraIntent.putExtra("return-data", true);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select Image");

        Intent[] intentArray = {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // System.out.println("Settings.onActivityResult()---" +
        // photoUri.toString());
        Bitmap bmp;
        if (resultCode == RESULT_OK) {
            // getUri(data);
            switch (requestCode) {
                case SELECT_PHOTO:
                    if (null != photoUri && !"".equals(photoUri)) {
                        mImageCaptureUri = Uri.fromFile(photoUri);
                    }
                    mImageCaptureUri = (null == data || null == data.getData()) ? mImageCaptureUri : data.getData();

                    if (null != mImageCaptureUri) {
                        String realPathFromURI = getPath(mImageCaptureUri);
                        if (null == realPathFromURI) {
                            realPathFromURI = mImageCaptureUri.getPath();
                            if (null == realPathFromURI) {
                                break;
                            }
                        }
                        doCrop(mImageCaptureUri);
                    }
                    onUserInteraction();
                    break;
                case CROP_RESULT:
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        bmp = extras.getParcelable("data");
                        bmp = Utils.getRoundedCornerBitmap(bmp, 100);
                        bmp = ThumbnailUtils.extractThumbnail(bmp, 120, 120);
                        drawable = new BitmapDrawable(getResources(), bmp);
                        profilePicBitmap = drawable.getBitmap();
                        ivProfilePic.setImageDrawable(drawable);
                    }
                    onUserInteraction();
                    break;
                case INVITE_GROUP_MEMBERS:
                    Intent intent = getIntent();
                    boolean isGroupChat = false;
                    Bundle extras2 = data.getExtras();
                    if (extras2 != null) {
                        isGroupChat = extras2.getBoolean(VariableConstants.IS_GROUP_CHAT);
                    }
                    intent.putExtra(VariableConstants.IS_GROUP_CHAT, isGroupChat);
                    this.finish();
                    break;
            }
        }
    }

    /**
     * Crop image for maintaining the aspect ratio
     *
     * @param mImageCaptureUri
     */
    private void doCrop(Uri mImageCaptureUri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(mImageCaptureUri, "image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 120);
            intent.putExtra("outputY", 120);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            Intent i = new Intent(intent);
            ResolveInfo res = list.get(0);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROP_RESULT);
        }
    }

    public String getPath(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};

        Cursor cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
        if (null != cursor && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            cursor.close();
        }
        return res;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}