package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by neharathore on 06/07/17.
 */

public enum FeedbackCommentsEnum {
    TEST_INCOMPLETE(MyApplication.getAppContext().getString(R.string.test_not_completed_key), R.string.test_not_completed_value),
    HOME_DRAW_IMPROPER(MyApplication.getAppContext().getString(R.string.home_draw_not_done_key), R.string.home_draw_not_done_value),
    PROFESSIONALISM(MyApplication.getAppContext().getString(R.string.professionalism_key), R.string.preventive_med_value),
    OTHER(MyApplication.getAppContext().getString(R.string.feedback_other_key), R.string.feedback_other_value);


    private String code;
    private int value;
    private static LinkedHashMap<String, FeedbackCommentsEnum> FeedbackCommentsEnumLinkedHashMap = null;


    private FeedbackCommentsEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, FeedbackCommentsEnum> getFeedbackCommentsEnumLinkedHashMap() {
        if (FeedbackCommentsEnumLinkedHashMap == null) {
            FeedbackCommentsEnumLinkedHashMap = new LinkedHashMap<String, FeedbackCommentsEnum>();
            for (FeedbackCommentsEnum vitals : FeedbackCommentsEnum.values()) {
                FeedbackCommentsEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return FeedbackCommentsEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getFeedbackCommentsEnumLinkedHashMap();
        for (Map.Entry<String, FeedbackCommentsEnum> enumMap : FeedbackCommentsEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getFeedbackCommentsEnumLinkedHashMap();
        if (FeedbackCommentsEnumLinkedHashMap != null) {
            for (Map.Entry<String, FeedbackCommentsEnum> enumMap : FeedbackCommentsEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
