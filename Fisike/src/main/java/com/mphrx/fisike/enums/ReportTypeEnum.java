package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by xmb2nc on 11-04-2017.
 */

public enum ReportTypeEnum {

    LAB(MyApplication.getAppContext().getString(R.string.lab_key), R.string.lab),
    MICROBIOLOGY(MyApplication.getAppContext().getString(R.string.microbiology_key), R.string.microbiology);


    private String code;
    private int value;
    private static LinkedHashMap<String, ReportTypeEnum> ReportTypeEnumLinkedHashMap = null;


    private ReportTypeEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ReportTypeEnum> getReportTypeEnumLinkedHashMap() {
        if (ReportTypeEnumLinkedHashMap == null) {
            ReportTypeEnumLinkedHashMap = new LinkedHashMap<String, ReportTypeEnum>();
            for (ReportTypeEnum vitals : ReportTypeEnum.values()) {
                ReportTypeEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return ReportTypeEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getReportTypeEnumLinkedHashMap();
        for (Map.Entry<String, ReportTypeEnum> enumMap : ReportTypeEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getValueFromCode(String code) {
        getReportTypeEnumLinkedHashMap();
        for (Map.Entry<String, ReportTypeEnum> enumMap : ReportTypeEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getCode().equals(code))
                return enumMap.getValue().getValue();
        return "";
    }
}
