package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha Rathore on 5/4/2017.
 */

public enum ExperienceEnum {

    LESS_ONE(MyApplication.getAppContext().getString(R.string.exp_less_than_1_key), R.string.exp_less_than_1_value),
    ONE(MyApplication.getAppContext().getString(R.string.exp_1_key), R.string.exp_1_value),
    TWO(MyApplication.getAppContext().getString(R.string.exp_2_key), R.string.exp_2_value),
    THREE(MyApplication.getAppContext().getString(R.string.exp_3_key), R.string.exp_3_value),
    FOUR(MyApplication.getAppContext().getString(R.string.exp_4_key), R.string.exp_4_value),
    FIVE(MyApplication.getAppContext().getString(R.string.exp_5_key), R.string.exp_5_value),
    SIX(MyApplication.getAppContext().getString(R.string.exp_6_key), R.string.exp_6_value),
    SEVEN(MyApplication.getAppContext().getString(R.string.exp_7_key), R.string.exp_7_value),
    EIGHT(MyApplication.getAppContext().getString(R.string.exp_8_key), R.string.exp_8_value),
    NINE(MyApplication.getAppContext().getString(R.string.exp_9_key), R.string.exp_9_value),
    TEN(MyApplication.getAppContext().getString(R.string.exp_10_key), R.string.exp_10_value),
    ELEVEN(MyApplication.getAppContext().getString(R.string.exp_11_key), R.string.exp_11_value),
    TWELVE(MyApplication.getAppContext().getString(R.string.exp_12_key), R.string.exp_12_value),
    THIRTEEN(MyApplication.getAppContext().getString(R.string.exp_13_key), R.string.exp_13_value),
    FOURTEEN(MyApplication.getAppContext().getString(R.string.exp_14_key), R.string.exp_14_value),
    FIFTEEN(MyApplication.getAppContext().getString(R.string.exp_15_key), R.string.exp_15_value),
    SIXEEN(MyApplication.getAppContext().getString(R.string.exp_16_key), R.string.exp_16_value),
    SEVENTEEN(MyApplication.getAppContext().getString(R.string.exp_17_key), R.string.exp_17_value),
    EIGHTEEN(MyApplication.getAppContext().getString(R.string.exp_18_key), R.string.exp_18_value),
    NINETEEN(MyApplication.getAppContext().getString(R.string.exp_19_key), R.string.exp_19_value),
    TWENTY(MyApplication.getAppContext().getString(R.string.exp_20_key), R.string.exp_20_value),
    TWENTYONE(MyApplication.getAppContext().getString(R.string.exp_21_key), R.string.exp_21_value),
    TWENTYTWO(MyApplication.getAppContext().getString(R.string.exp_22_key), R.string.exp_22_value),
    TWENTYTHREE(MyApplication.getAppContext().getString(R.string.exp_23_key), R.string.exp_23_value),
    TWENTYFOUR(MyApplication.getAppContext().getString(R.string.exp_24_key), R.string.exp_24_value),
    TWENTYFIVE(MyApplication.getAppContext().getString(R.string.exp_25_key), R.string.exp_25_value),
    TWENTYSIX(MyApplication.getAppContext().getString(R.string.exp_26_key), R.string.exp_26_value),
    TWENTYSEVEN(MyApplication.getAppContext().getString(R.string.exp_27_key), R.string.exp_27_value),
    TWENTYEIGHT(MyApplication.getAppContext().getString(R.string.exp_28_key), R.string.exp_28_value),
    TWENTYNINE(MyApplication.getAppContext().getString(R.string.exp_29_key), R.string.exp_29_value),
    THIRTY(MyApplication.getAppContext().getString(R.string.exp_30_key), R.string.exp_30_value),
    MORE_THAN_THIRTY(MyApplication.getAppContext().getString(R.string.exp_more_than_30_key), R.string.exp_more_than_30_value),;

    private String code;
    private int value;
    private static LinkedHashMap<String, ExperienceEnum> expEnumLinkedHashMap = null;


    private ExperienceEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ExperienceEnum> getExperienceEnumLinkedHashMap() {
        if (expEnumLinkedHashMap == null) {
            expEnumLinkedHashMap = new LinkedHashMap<String, ExperienceEnum>();
            for (ExperienceEnum vitals : ExperienceEnum.values()) {
                expEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return expEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getExperienceEnumLinkedHashMap();
        for (Map.Entry<String, ExperienceEnum> enumMap : expEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getExperienceEnumLinkedHashMap();
        if (expEnumLinkedHashMap != null) {
            for (Map.Entry<String, ExperienceEnum> enumMap : expEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
