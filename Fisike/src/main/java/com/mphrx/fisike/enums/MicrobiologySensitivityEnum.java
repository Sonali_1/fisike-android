package com.mphrx.fisike.enums;

/**
 * Created by xmb2nc on 12-04-2017.
 */

public enum  MicrobiologySensitivityEnum {
    S("Sensitive"),
    R("Resistant"),
    I("Intermediate");

    private String senstivityValue;

    private MicrobiologySensitivityEnum(String senstivityValue) {
        this.senstivityValue = senstivityValue;
    }

    public String getSenstivityValue() {
        return senstivityValue;
    }

    public void setSenstivityValue(String senstivityValue) {
        this.senstivityValue = senstivityValue;
    }
}
