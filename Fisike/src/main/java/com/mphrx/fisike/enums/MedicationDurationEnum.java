package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by laxmansingh on 5/2/2017.
 */

public enum MedicationDurationEnum {

    FOREVER(MyApplication.getAppContext().getString(R.string.forever_key), MyApplication.getAppContext().getString(R.string.forever_text));


    private String code, value;
    private static LinkedHashMap<String, MedicationDurationEnum> MedicationDurationEnumLinkedHashMap = null;


    private MedicationDurationEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, MedicationDurationEnum> getMedicationDurationEnumLinkedHashMap() {
        if (MedicationDurationEnumLinkedHashMap == null) {
            MedicationDurationEnumLinkedHashMap = new LinkedHashMap<String, MedicationDurationEnum>();
            for (MedicationDurationEnum vitals : MedicationDurationEnum.values()) {
                MedicationDurationEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return MedicationDurationEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getMedicationDurationEnumLinkedHashMap();
        for (Map.Entry<String, MedicationDurationEnum> enumMap : MedicationDurationEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }
}
