package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by neharathore on 25/08/17.
 */

public enum RelationShipEnum {

    RELATIONSHIP(MyApplication.getAppContext().getString(R.string.relationship_key), R.string.relationship),
    FATHER(MyApplication.getAppContext().getString(R.string.father_key), R.string.father),
    MOTHER(MyApplication.getAppContext().getString(R.string.Mother_key), R.string.Mother),
    SON(MyApplication.getAppContext().getString(R.string.Son_key), R.string.Son),
    DAUGHTER(MyApplication.getAppContext().getString(R.string.Daughter_key), R.string.Daughter),
    HUSBAND(MyApplication.getAppContext().getString(R.string.Husband_key), R.string.Husband),
    WIFE(MyApplication.getAppContext().getString(R.string.Wife_key), R.string.Wife),
    CAREGIVER(MyApplication.getAppContext().getString(R.string.Caregiver_key), R.string.Caregiver),
    OTHER(MyApplication.getAppContext().getString(R.string.relationship_other_key), R.string.relationship_other);
    private String code;
    private int value;
    private static LinkedHashMap<String, RelationShipEnum> RelationShipEnumLinkedHashMap = null;


    private RelationShipEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }


    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, RelationShipEnum> getRelationShipEnumLinkedHashMap() {
        if (RelationShipEnumLinkedHashMap == null) {
            RelationShipEnumLinkedHashMap = new LinkedHashMap<String, RelationShipEnum>();
            for (RelationShipEnum vitals : RelationShipEnum.values()) {
                RelationShipEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return RelationShipEnumLinkedHashMap;
    }


    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        try {
            getRelationShipEnumLinkedHashMap();
            for (Map.Entry<String, RelationShipEnum> enumMap : RelationShipEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getCode();
            return "";
        } catch (Exception e) {

        }
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        try {
            getRelationShipEnumLinkedHashMap();
            if (RelationShipEnumLinkedHashMap != null) {
                for (Map.Entry<String, RelationShipEnum> enumMap : RelationShipEnumLinkedHashMap.entrySet())
                    if (enumMap.getValue().getCode().equals(value))
                        return enumMap.getValue().getValue();
            }
            return value;
        } catch (Exception e) {
        }
        return value;
    }
}
