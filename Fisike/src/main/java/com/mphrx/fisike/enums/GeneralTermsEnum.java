package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by laxmansingh on 5/3/2017.
 */

public enum GeneralTermsEnum {

    UNAVAILABLE(MyApplication.getAppContext().getString(R.string.unavailable_key), MyApplication.getAppContext().getString(R.string.text_unavailable));

    private String code, value;
    private static LinkedHashMap<String, GeneralTermsEnum> GeneralTermsEnumLinkedHashMap = null;


    private GeneralTermsEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, GeneralTermsEnum> getGeneralTermsEnumLinkedHashMap() {
        if (GeneralTermsEnumLinkedHashMap == null) {
            GeneralTermsEnumLinkedHashMap = new LinkedHashMap<String, GeneralTermsEnum>();
            for (GeneralTermsEnum vitals : GeneralTermsEnum.values()) {
                GeneralTermsEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return GeneralTermsEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGeneralTermsEnumLinkedHashMap();
        for (Map.Entry<String, GeneralTermsEnum> enumMap : GeneralTermsEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getValueFromCode(String code) {
        getGeneralTermsEnumLinkedHashMap();
        for (Map.Entry<String, GeneralTermsEnum> enumMap : GeneralTermsEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getCode().equals(code))
                return enumMap.getValue().getValue();
        return "";
    }
}

