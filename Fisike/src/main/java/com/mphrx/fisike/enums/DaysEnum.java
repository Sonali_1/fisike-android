package com.mphrx.fisike.enums;

import com.mphrx.fisike.vital_submit.VitalsConfigGson.UnitsEnum;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by aastha on 4/11/2017.
 */

public enum DaysEnum {
    Monday("Monday", "Mo"),
    Tuesday("Tuesday", "Tu"),
    Wednesday("Wednesday", "We"),
    Thursday("Thursday", "Th"),
    Friday("Friday", "Fr"),
    Saturday("Saturday", "Sa"),
    Sunday("Sunday", "Su"),
    Default("Default", "De");

    private String fullDayName, shortName ;
    public static LinkedHashMap<String, DaysEnum> daysMap = null;

    private DaysEnum(String fullDayName, String shortName) {
        this.fullDayName = fullDayName;
        this.shortName = shortName;
    }

    public String getFullDayName() {
        return fullDayName;
    }

    public void setFullDayName(String fullDayName) {
        this.fullDayName = fullDayName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public static LinkedHashMap<String, DaysEnum> getMap() {
        if (daysMap == null) {
            daysMap = new LinkedHashMap<String, DaysEnum>();
            for (DaysEnum daysEnum : DaysEnum.values()) {
                daysMap.put(daysEnum.fullDayName, daysEnum);
            }
        }
        return daysMap;
    }

    public static String getCode(String fullDayName) {
        String code = "";
        getMap();
        for (Map.Entry<String, DaysEnum> entry : daysMap.entrySet())
        {
            if (entry.getValue().fullDayName.equalsIgnoreCase(fullDayName)) {
                code = entry.getValue().getShortName();
                break;
            }
        }
        return code;
    }
}
