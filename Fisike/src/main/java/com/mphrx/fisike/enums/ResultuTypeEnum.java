package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kailashkhurana on 09/08/17.
 */

public enum ResultuTypeEnum {
    ALL(MyApplication.getAppContext().getString(R.string.all_key), R.string.all),
    LAB(MyApplication.getAppContext().getString(R.string.lab_key), R.string.lab),
    RADIOLOGY(MyApplication.getAppContext().getString(R.string.radiologie_key), R.string.radiologie);


    private String code;
    private int value;
    private static LinkedHashMap<String, ResultuTypeEnum> genderEnumLinkedHashMap = null;


    private ResultuTypeEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ResultuTypeEnum> getGenderEnumLinkedHashMap() {
        if (genderEnumLinkedHashMap == null) {
            genderEnumLinkedHashMap = new LinkedHashMap<String, ResultuTypeEnum>();
            for (ResultuTypeEnum vitals : ResultuTypeEnum.values()) {
                genderEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return genderEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGenderEnumLinkedHashMap();
        for (Map.Entry<String, ResultuTypeEnum> enumMap : genderEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return value;
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getGenderEnumLinkedHashMap();
        if (genderEnumLinkedHashMap != null) {
            for (Map.Entry<String, ResultuTypeEnum> enumMap : genderEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
