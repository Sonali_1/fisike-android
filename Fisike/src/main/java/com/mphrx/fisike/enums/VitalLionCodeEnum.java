package com.mphrx.fisike.enums;

import java.util.LinkedHashMap;

/**
 * Created by laxmansingh on 02-08-2016.
 */


public enum VitalLionCodeEnum {
    BMI_CODE("39156-5"),
    BP_SYSTOLIC_CODE("8480-6"),
    BP_DIASTOLIC_CODE("8462-4"),
    GLUCOSE_RANDOM_CODE("2345-7"),
    GLUCOSE_AFTERMEAL_CODE("1521-4"),
    GLUCOSE_FASTING_CODE("1558-6");

    private String code;
    private static LinkedHashMap<String, VitalLionCodeEnum> vitalMap = null;


    private VitalLionCodeEnum(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, VitalLionCodeEnum> getVitalMap(){
        if(vitalMap == null){
            vitalMap = new LinkedHashMap<String, VitalLionCodeEnum>();
            for (VitalLionCodeEnum vitals : VitalLionCodeEnum.values()) {
                vitalMap.put(vitals.code, vitals);
            }
        }
        return vitalMap;
    }

}
