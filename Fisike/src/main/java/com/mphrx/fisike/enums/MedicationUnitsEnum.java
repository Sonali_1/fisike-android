package com.mphrx.fisike.enums;

/**
 * Created by Aastha on 10/5/2016.
 */

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

public enum MedicationUnitsEnum {

    tablet_count(R.string.tablet_count_value,1,1,MyApplication.getAppContext().getString(R.string.tablet_count_key)) ,
    tablet_mg(R.string.tablet_mg_value,0,0,MyApplication.getAppContext().getString(R.string.tablet_mg_key)),
    capsule_count(R.string.capsule_count_value,1,1,MyApplication.getAppContext().getString(R.string.capsule_count_key)),
    capsule_mg(R.string.capsule_mg_value,0,0,MyApplication.getAppContext().getString(R.string.capsule_mg_key)),
    syrup_ml(R.string.syrup_ml_value,0,0,MyApplication.getAppContext().getString(R.string.syrup_ml_key)),
    syrup_tbsp(R.string.syrup_tbsp_value,0,0,MyApplication.getAppContext().getString(R.string.syrup_tbsp_key)),
    syrup_tsp(R.string.syrup_tsp_value,0,0,MyApplication.getAppContext().getString(R.string.syrup_tsp_key)),
    injection_ml(R.string.injection_ml_value,0,0,MyApplication.getAppContext().getString(R.string.injection_ml_key)),
    injection_mg(R.string.injection_mg_value,0,0,MyApplication.getAppContext().getString(R.string.injection_mg_key)),
    injection_vv(R.string.injection_vv_value,0,0,MyApplication.getAppContext().getString(R.string.injection_vv_key)),
    injection_unit(R.string.injection_unit_value,1,0,MyApplication.getAppContext().getString(R.string.injection_unit_key)),
    spray_ml(R.string.spray_ml_value,0,0,MyApplication.getAppContext().getString(R.string.spray_ml_key)),
    spray_puffs(R.string.spray_puffs_value,1,0,MyApplication.getAppContext().getString(R.string.spray_puffs_key)),
    inhaler_puffs(R.string.inhaler_puffs_value,1,0,MyApplication.getAppContext().getString(R.string.inhaler_puffs_key)),
    drops_count(R.string.drops_count_value,1,1,MyApplication.getAppContext().getString(R.string.drops_count_key)),
    drops_ml(R.string.drops_ml_value,0,0,MyApplication.getAppContext().getString(R.string.drops_ml_key)),
    powder_gms(R.string.powder_gms_value,1,0,MyApplication.getAppContext().getString(R.string.powder_gms_key)),
    powder_mg(R.string.powder_mg_value,0,0,MyApplication.getAppContext().getString(R.string.powder_mg_key)),
    powder_tsp(R.string.powder_tsp_value,0,0,MyApplication.getAppContext().getString(R.string.powder_tsp_key)),
    powder_tbsp(R.string.powder_tbsp_value,0,0,MyApplication.getAppContext().getString(R.string.powder_tbsp_key)),
    granule_count(R.string.granule_count_value,1,1,MyApplication.getAppContext().getString(R.string.granule_count_key)),
    aasav_tsp(R.string.aasav_tsp_value,0,0,MyApplication.getAppContext().getString(R.string.aasav_tsp_key)),
    aasav_tbsp(R.string.aasav_tbsp_value,0,0,MyApplication.getAppContext().getString(R.string.aasav_tbsp_key)),
    ampule_count(R.string.ampule_count_value,1,1,MyApplication.getAppContext().getString(R.string.ampule_count_key)),
    astringent_ml(R.string.astringent_ml_value,0,0,MyApplication.getAppContext().getString(R.string.astringent_ml_key)),
    cream_mg(R.string.cream_mg_value,0,0,MyApplication.getAppContext().getString(R.string.cream_mg_key)),
    cream_ml(R.string.cream_ml_value,0,0,MyApplication.getAppContext().getString(R.string.cream_ml_key)),
    expectorant(R.string.expectorant_value,0,0,MyApplication.getAppContext().getString(R.string.expectorant_key)),
    gum_paint_ml(R.string.gum_paint_ml_value,0,0,MyApplication.getAppContext().getString(R.string.gum_paint_ml_key)),
    gum_paint_mg(R.string.gum_paint_mg_value,0,0,MyApplication.getAppContext().getString(R.string.gum_paint_mg_key)),
    mixture_gms(R.string.mixture_gms_value,1,0,MyApplication.getAppContext().getString(R.string.mixture_gms_key)),
    mixture_mg(R.string.mixture_mg_value,0,0,MyApplication.getAppContext().getString(R.string.mixture_mg_key)),
    mixture_tsp(R.string.mixture_tsp_value,0,0,MyApplication.getAppContext().getString(R.string.mixture_tsp_key)),
    mixture_tbsp(R.string.mixture_tbsp_value,0,0,MyApplication.getAppContext().getString(R.string.mixture_tbsp_key)),
    mouth_wash(R.string.mouth_wash_value,0,0,MyApplication.getAppContext().getString(R.string.mouth_wash_key)),
    gel_mg(R.string.gel_mg_value,0,0,MyApplication.getAppContext().getString(R.string.gel_mg_key)),
    gel_ml(R.string.gel_ml_value,0,0,MyApplication.getAppContext().getString(R.string.gel_ml_key)),
    kwath_tsp(R.string.kwath_tsp_value,0,0,MyApplication.getAppContext().getString(R.string.kwath_tsp_key)),
    kwath_tbsp(R.string.kwath_tbsp_value,0,0,MyApplication.getAppContext().getString(R.string.kwath_tbsp_key)),
    oil_ml(R.string.oil_ml_value,0,0,MyApplication.getAppContext().getString(R.string.oil_ml_key)),
    ointment_ml(R.string.ointment_ml_value,0,0,MyApplication.getAppContext().getString(R.string.ointment_ml_key)),
    ointment_mg(R.string.ointment_mg_value,0,0,MyApplication.getAppContext().getString(R.string.ointment_mg_key)),
    respule_mg(R.string.respule_mg_value,0,0,MyApplication.getAppContext().getString(R.string.respule_mg_key)),
    respule_mcg(R.string.respule_mcg_value,1,0,MyApplication.getAppContext().getString(R.string.respule_mcg_key)),
    sachets_count(R.string.sachets_count_value,1,1,MyApplication.getAppContext().getString(R.string.sachets_count_key)),
    suspension_ml(R.string.suspension_ml_value,0,0,MyApplication.getAppContext().getString(R.string.suspension_ml_key)),
    suspension_mg(R.string.suspension_mg_value,0,0,MyApplication.getAppContext().getString(R.string.suspension_mg_key)),
    toothpaste_mg(R.string.toothpaste_mg_value,0,0,MyApplication.getAppContext().getString(R.string.toothpaste_mg_key));

    private int medication_unit;
    private int isPlural,isCount;
    private String code;

    public static LinkedHashMap<String,MedicationUnitsEnum> medicationUnitsEnumLinkedHashMap=null;

    private MedicationUnitsEnum(int medication_unit, int isPlural,int count,String code){
        this.medication_unit=medication_unit;
        this.isPlural=isPlural;
        this.isCount=count;
        this.code=code;
    }

    public static LinkedHashMap<String, MedicationUnitsEnum> getMap() {
        if (medicationUnitsEnumLinkedHashMap == null) {
            medicationUnitsEnumLinkedHashMap = new LinkedHashMap<String, MedicationUnitsEnum>();
            for (MedicationUnitsEnum unit : MedicationUnitsEnum.values()) {
                medicationUnitsEnumLinkedHashMap.put(unit.getCode(), unit);
            }
        }
        return medicationUnitsEnumLinkedHashMap;
    }

    public static String[] units() {
        MedicationUnitsEnum[] medicationUnitsEnumList = values();
        String[] units = new String[medicationUnitsEnumList.length];
        int size=medicationUnitsEnumList.length;
        for (int i = 0; i < size; i++) {
            units[i] = getValueFromString(medicationUnitsEnumList[i].medication_unit);
        }

        return units;
    }

    private static String getValueFromString(int id) {
        try{
            return MyApplication.getAppContext().getString(id);}
        catch (Exception e)
        {

        }
        return "";
    }

    public String getMedication_unit() {
        try{
        return MyApplication.getAppContext().getString(medication_unit);}
        catch (Exception e)
        {

        }
        return "";
    }

    public void setMedication_unit(int medication_unit) {
        this.medication_unit = medication_unit;
    }

    public int getIsPlural() {
        return isPlural;
    }

    public void setIsPlural(int isPlural) {
        this.isPlural = isPlural;
    }

    public int getIsCount() {
        return isCount;
    }

    public void setIsCount(int isCount) {
        this.isCount = isCount;
    }

    public String getCode()
    {return code;}
    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value)
    {
        getMap();
        if(medicationUnitsEnumLinkedHashMap!=null){
        for(Map.Entry<String,MedicationUnitsEnum> enumMap: medicationUnitsEnumLinkedHashMap.entrySet())
            if(enumMap.getValue().getCode().equals(value))
                return enumMap.getValue().getMedication_unit();
        }
        return value;
    }

    public static MedicationUnitsEnum getEnumInstanceBasedOnObject(String value)
    {
        getMap();
        if(medicationUnitsEnumLinkedHashMap!=null){
            for(Map.Entry<String,MedicationUnitsEnum> enumMap: medicationUnitsEnumLinkedHashMap.entrySet())
                if(enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue();
        }
        return null;
    }

    public static String getCodeFromDisplayedValue(String value)
    {
        getMap();
        if(medicationUnitsEnumLinkedHashMap!=null){
            for(Map.Entry<String,MedicationUnitsEnum> enumMap: medicationUnitsEnumLinkedHashMap.entrySet())
                if(enumMap.getValue().getMedication_unit().equals(value))
                    return enumMap.getValue().getCode();
        }
        return value;
    }

    public static int getUnitIsPlural(String key)
    {
        try
        {
            return getMap().get(key).getIsPlural();
        }
        catch (Exception e)
        {
            return 0;
        }
    }
}
