package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha Rathore on 5/4/2017.
 */

public enum SpecialityEnum {
    ANESTHESIOLOGY(MyApplication.getAppContext().getString(R.string.anesthology_key), R.string.anesthology_value),
    CARDIOLOGY(MyApplication.getAppContext().getString(R.string.cardiology_key), R.string.cardiology_value),
    DERMATOLOGY(MyApplication.getAppContext().getString(R.string.dermatology_key), R.string.dermatology_value),
    ENDOCRINOLOGY(MyApplication.getAppContext().getString(R.string.endo_dia_meta_key), R.string.endo_dia_meta_value),
    EMERGENCY(MyApplication.getAppContext().getString(R.string.emergencymed_key), R.string.emergencymed_value),
    FAMILY(MyApplication.getAppContext().getString(R.string.family_key), R.string.family_value),
    GERIATRICS(MyApplication.getAppContext().getString(R.string.geriatrics_key), R.string.geriatrics_value),
    INTERNAL_MEDICINE(MyApplication.getAppContext().getString(R.string.internal_medicine_key), R.string.internal_medicine_value),
    MEDICAL_GENETICS(MyApplication.getAppContext().getString(R.string.medical_genetics_key), R.string.medical_genetics_value),
    NEUROLOGICAL_SURGERY(MyApplication.getAppContext().getString(R.string.neurolgy_surgery_key), R.string.neurolgy_surgery_value),
    NEUROLOGY(MyApplication.getAppContext().getString(R.string.neurology_key), R.string.neurology_value),
    OBSTERICS(MyApplication.getAppContext().getString(R.string.obstrics_key), R.string.obstrics_value),
    OPHTHALMOLOGY(MyApplication.getAppContext().getString(R.string.ophthalmology_key), R.string.ophthalmology_value),
    ONCOLOGY(MyApplication.getAppContext().getString(R.string.oncology_key), R.string.oncology_value),
    ORTHOPEDICS(MyApplication.getAppContext().getString(R.string.orthopedics_key), R.string.orthopedics_value),
    OTOLARYNGOLOGY(MyApplication.getAppContext().getString(R.string.otolary_key), R.string.otolary_value),
    PATHOLOGY(MyApplication.getAppContext().getString(R.string.pathology_key), R.string.pathology_value),
    PEDIATRIC(MyApplication.getAppContext().getString(R.string.pediatrics_key), R.string.pediatrics_value),
    PHYSICIAN_REHAB(MyApplication.getAppContext().getString(R.string.physician_rehab_key), R.string.physician_rehab_value),
    PLASTIC_SURGERY(MyApplication.getAppContext().getString(R.string.plastic_surgery_key), R.string.plastic_surgery_value),
    PREVENTIVE_MEDICINE(MyApplication.getAppContext().getString(R.string.preventive_med_key), R.string.preventive_med_value),
    PSYCHIATRY(MyApplication.getAppContext().getString(R.string.psychiatry_key), R.string.psychiatry_value),
    RADIOLOGY(MyApplication.getAppContext().getString(R.string.radiology_key), R.string.radiology_value),
    SURGERY(MyApplication.getAppContext().getString(R.string.surgery_key), R.string.surgery_value),
    UROLOGY(MyApplication.getAppContext().getString(R.string.urology_key), R.string.urology_value),
    OTHER(MyApplication.getAppContext().getString(R.string.spec_other_key), R.string.spec_other_value),;


    private String code;
    private int value;
    private static LinkedHashMap<String, SpecialityEnum> specialityEnumLinkedHashMap = null;


    private SpecialityEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }


    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, SpecialityEnum> getSpecialityEnumLinkedHashMap() {
        if (specialityEnumLinkedHashMap == null) {
            specialityEnumLinkedHashMap = new LinkedHashMap<String, SpecialityEnum>();
            for (SpecialityEnum vitals : SpecialityEnum.values()) {
                specialityEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return specialityEnumLinkedHashMap;
    }


    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getSpecialityEnumLinkedHashMap();
        for (Map.Entry<String, SpecialityEnum> enumMap : specialityEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getCode().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getSpecialityEnumLinkedHashMap();
        if (specialityEnumLinkedHashMap != null) {
            for (Map.Entry<String, SpecialityEnum> enumMap : specialityEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
