package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash Khurana on 5/1/2017.
 */

public enum AddressTypeEnum {
    TYPE(MyApplication.getAppContext().getString(R.string.type_key), R.string.Type),
    HOME(MyApplication.getAppContext().getString(R.string.home_address_key), R.string.home_address),
    WORK(MyApplication.getAppContext().getString(R.string.office_address_key), R.string.office_address),
    OLD(MyApplication.getAppContext().getString(R.string.old_address_key), R.string.old_address),;


    private String code;
    private int value;
    private static LinkedHashMap<String, AddressTypeEnum> addressEnumLinkedHashMap = null;


    private AddressTypeEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, AddressTypeEnum> getAddressEnumLinkedHashMap() {
        if (addressEnumLinkedHashMap == null) {
            addressEnumLinkedHashMap = new LinkedHashMap<String, AddressTypeEnum>();
            for (AddressTypeEnum vitals : AddressTypeEnum.values()) {
                addressEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return addressEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getAddressEnumLinkedHashMap();
        for (Map.Entry<String, AddressTypeEnum> enumMap : addressEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().contains(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        if (value == null) {
            return "";
        }
        getAddressEnumLinkedHashMap();
        if (addressEnumLinkedHashMap != null) {
            for (Map.Entry<String, AddressTypeEnum> enumMap : addressEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().contains(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
