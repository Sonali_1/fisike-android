package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kailashkhurana on 09/08/17.
 */

public enum SpecificDaysEnum {
    TODAY(MyApplication.getAppContext().getString(R.string.today_key), R.string.today),
    YESTERDAY(MyApplication.getAppContext().getString(R.string.yesterday_key), R.string.yesterday),
    THIS_WEEK(MyApplication.getAppContext().getString(R.string.this_week_key), R.string.this_week),
//    LAST_WEEK(MyApplication.getAppContext().getString(R.string.last_month_key), R.string.last_week),
    THIS_MONTH(MyApplication.getAppContext().getString(R.string.this_month_key), R.string.this_month),
    LAST_MONTH(MyApplication.getAppContext().getString(R.string.last_month_key), R.string.last_month);


    private String code;
    private int value;
    private static LinkedHashMap<String, SpecificDaysEnum> genderEnumLinkedHashMap = null;


    private SpecificDaysEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, SpecificDaysEnum> getGenderEnumLinkedHashMap() {
        if (genderEnumLinkedHashMap == null) {
            genderEnumLinkedHashMap = new LinkedHashMap<String, SpecificDaysEnum>();
            for (SpecificDaysEnum vitals : SpecificDaysEnum.values()) {
                genderEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return genderEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGenderEnumLinkedHashMap();
        for (Map.Entry<String, SpecificDaysEnum> enumMap : genderEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return value;
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getGenderEnumLinkedHashMap();
        if (genderEnumLinkedHashMap != null) {
            for (Map.Entry<String, SpecificDaysEnum> enumMap : genderEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
