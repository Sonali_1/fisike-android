package com.mphrx.fisike.enums;

import java.util.LinkedHashMap;

public enum DatabaseTables {
    UserMO_TABLE("userMoTable"),
    SettingMO_TABLE("settingMoTable"),
    ChatConversationMO_TABLE("chatConversationMoTable"),
    ChatContactMo_TABLE("chatContactMoTable"),
    ChatMessageMO_TABLE("chatMessageMoTable"),
    QuestionActivityMO_TABLE("questionActivityMoTable"),
    QuestionMO_TABLE("questionTable"),
    AttachmentMO_TABLE("attachmentMoTable"),
    GROUP_TEXT_CHAT_MO_TABLE("groupTextChatMoTable"),
    DRUG_DETATILS_TABLE("DrugDetailsTable"),
    DISEASES_TABLE("DiseaseTable"),
    MEDICATION_TABLE("Medication"),
	PRACTITIONAR_TABLE("Practitonar"),
	ENCOUNTER_TABLE("Encounter"),
	MEDICATION_PRESCRIPTION_TABLE("MedicationPrescription"),
	DOCUMENT_REFERENCE_TABLE("DocumentReference"),
//	OBSERVATION_TABLE("Observation"),
	NOTIFICATION_CENTER_TABLE("NotificationCenter"),
    PATIENT_RECORD_TABLE("PatientRecords");
    
    static LinkedHashMap<String, DatabaseTables> databaseTablesMap = null;
    public String tableName;

    private DatabaseTables(String tableName) {
        this.tableName = tableName;
    }

    public static LinkedHashMap<String, DatabaseTables> getMap() {
        if (databaseTablesMap == null) {
            databaseTablesMap = new LinkedHashMap<String, DatabaseTables>();
            for (DatabaseTables tables : DatabaseTables.values()) {
                databaseTablesMap.put(tables.tableName, tables);
            }
        }
        return databaseTablesMap;
    }

}
