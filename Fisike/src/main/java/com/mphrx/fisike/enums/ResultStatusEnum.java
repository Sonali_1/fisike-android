package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kailashkhurana on 09/08/17.
 */

public enum ResultStatusEnum {
    RESULT_FINAL(MyApplication.getAppContext().getString(R.string.result_final_key), R.string.result_final),
    RESULT_PARTIAL(MyApplication.getAppContext().getString(R.string.partial_key), R.string.partial),
    RESULT_PROVISIONAL(MyApplication.getAppContext().getString(R.string.provisional_key), R.string.provisional),
    RESULT_CANCELLED(MyApplication.getAppContext().getString(R.string.cancelled_key), R.string.cancelled),
    RESULT_REQUESTED(MyApplication.getAppContext().getString(R.string.requested_key), R.string.requested);


    private String code;
    private int value;
    private static LinkedHashMap<String, ResultStatusEnum> genderEnumLinkedHashMap = null;


    private ResultStatusEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ResultStatusEnum> getGenderEnumLinkedHashMap() {
        if (genderEnumLinkedHashMap == null) {
            genderEnumLinkedHashMap = new LinkedHashMap<String, ResultStatusEnum>();
            for (ResultStatusEnum vitals : ResultStatusEnum.values()) {
                genderEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return genderEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGenderEnumLinkedHashMap();
        for (Map.Entry<String, ResultStatusEnum> enumMap : genderEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return value;
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getGenderEnumLinkedHashMap();
        if (genderEnumLinkedHashMap != null) {
            for (Map.Entry<String, ResultStatusEnum> enumMap : genderEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
