package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.MedicationConstants;
import com.mphrx.fisike.constant.TextConstants;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Arun Kumar on 5/2/2017.
 */

public enum  MedicationFrequancyEnum
{
    ONCE_A_DAY(MyApplication.getAppContext().getString(R.string.Once_a_day_key),R.string.Once_a_day),
    TWICE_A_DAY(MyApplication.getAppContext().getString(R.string.Twice_a_day_key),R.string.Twice_a_day),
    THREE_TIMES_A_DAY(MyApplication.getAppContext().getString(R.string.Three_times_a_day_key),R.string.Three_times_a_day),
    FOUR_TIMES_A_DAY(MyApplication.getAppContext().getString(R.string.Four_times_a_day_key),R.string.Four_times_a_day),
    FIVE_TIMES_A_DAY( MyApplication.getAppContext().getString(R.string.Five_times_a_day_key),R.string.Five_times_a_day),
    SIX_TIMES_A_DAY(MyApplication.getAppContext().getString(R.string.Six_times_a_day_key), R.string.Six_times_a_day),
    SEVEN_TIMES_A_DAY(MyApplication.getAppContext().getString(R.string.Seven_times_a_day_key), R.string.Seven_times_a_day),
    EIGHT_TIMES_A_DAY(MyApplication.getAppContext().getString(R.string.Eight_times_a_day_key), R.string.Eight_times_a_day),
    SOS(MyApplication.getAppContext().getString(R.string.SOS_key), R.string.SOS),
    AM(MyApplication.getAppContext().getString(R.string.time_am_key), R.string.time_am),
    PM(MyApplication.getAppContext().getString(R.string.time_pm_key), R.string.time_pm),
    AFTER_FOOD(MyApplication.getAppContext().getString(R.string.after_food_key),R.string.after_food),
    BEFORE_FOOD(MyApplication.getAppContext().getString(R.string.before_food_key),R.string.before_food);

    private String code;
    int value;
    private static LinkedHashMap<String, MedicationFrequancyEnum> medicationFrequancyMap = null;

    private MedicationFrequancyEnum(String code,int value){
        this.code = code;
        this.value=value;
    }

    public String getValue() {
        try{
        return MyApplication.getAppContext().getString(value);}
        catch (Exception e)
        {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, MedicationFrequancyEnum> getMedicationFrequancyEnumLinkedHashMap(){
        if(medicationFrequancyMap == null){
            medicationFrequancyMap = new LinkedHashMap<String, MedicationFrequancyEnum>();
            for (MedicationFrequancyEnum vitals : MedicationFrequancyEnum.values()) {
                medicationFrequancyMap.put(vitals.code, vitals);
            }
        }
        return medicationFrequancyMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value)
    {
        if(value==null){
            return value;
        }
        getMedicationFrequancyEnumLinkedHashMap();
        for(Map.Entry<String,MedicationFrequancyEnum> enumMap: medicationFrequancyMap.entrySet())
            if(enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return value ;
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value)
    {
        if(value==null){
            return value;
        }
        getMedicationFrequancyEnumLinkedHashMap();
        if(medicationFrequancyMap!=null){
            for(Map.Entry<String,MedicationFrequancyEnum> enumMap: medicationFrequancyMap.entrySet())
                if(enumMap.getValue().getCode().equalsIgnoreCase(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }


}
