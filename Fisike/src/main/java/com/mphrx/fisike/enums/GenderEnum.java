package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash Khurana on 5/1/2017.
 */

public enum GenderEnum {
    MALE(MyApplication.getAppContext().getString(R.string.gender_male_key), R.string.gender_male),
    FEMALE(MyApplication.getAppContext().getString(R.string.gender_female_key), R.string.gender_female),
    OTHER(MyApplication.getAppContext().getString(R.string.gender_other_key), R.string.gender_other);


    private String code;
    private int value;
    private static LinkedHashMap<String, GenderEnum> genderEnumLinkedHashMap = null;


    private GenderEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, GenderEnum> getGenderEnumLinkedHashMap() {
        if (genderEnumLinkedHashMap == null) {
            genderEnumLinkedHashMap = new LinkedHashMap<String, GenderEnum>();
            for (GenderEnum vitals : GenderEnum.values()) {
                genderEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return genderEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGenderEnumLinkedHashMap();
        for (Map.Entry<String, GenderEnum> enumMap : genderEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        try {
        getGenderEnumLinkedHashMap();
        if (genderEnumLinkedHashMap != null) {
            for (Map.Entry<String, GenderEnum> enumMap : genderEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;}
        catch (Exception e)
        {

        }
        return value;
    }
}
