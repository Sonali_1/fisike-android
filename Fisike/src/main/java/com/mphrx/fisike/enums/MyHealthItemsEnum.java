package com.mphrx.fisike.enums;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.CustomizedTextConstants;

/**
 * Created by xmb2nc on 15-12-2016.
 */

public enum MyHealthItemsEnum {
    Vitals(CustomizedTextConstants.VITALS_TITLE),
    Records(CustomizedTextConstants.RECORDS_TITLE),
    Medications(CustomizedTextConstants.MEDICATION_TITLE),
    Uploads(CustomizedTextConstants.UPLOADS_TITLE);

    private String tabItemName;

    private MyHealthItemsEnum(String name) {
        tabItemName = name;
    }

    public String getTabItemName() {
        return tabItemName;
    }

    public void setTabItemName(String tabItemName) {
        this.tabItemName = tabItemName;
    }
}
