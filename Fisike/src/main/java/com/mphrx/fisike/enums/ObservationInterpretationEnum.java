package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;

/**
 * Created by xmb2nc on 02-08-2016.
 */
public enum ObservationInterpretationEnum {
    OFF_SCALE_LOW("<", R.string.off_scale_low),
    OFF_SCALE_HIGH(">", R.string.off_scale_high),
    ABNORMAL("A", R.string.abnormal),
    CRITICALLY_ABNORMAL("AA", R.string.critically_abnormal),
    DETECTED("DET", R.string.detected),
    HIGH("H", R.string.high),
    CRITICALLY_HIGH("HH", R.string.critically_high),
    VERY_HIGH("HU", R.string.very_high),
    INTERMEDIATE("I", R.string.intermediate),
    INSUFFICIENT_EVIDENCE("IE", R.string.insufficient_evidence),
    INDETERMINATE("IND", R.string.indeterminate),
    LOW("L", R.string.low),
    CRITICALLY_LOW("LL", R.string.critically_low),
    VER_LOW("LU", R.string.very_low),
    NORMAL("N", R.string.normal),
    NOT_DETECTED("ND", R.string.not_detected),
    NEGATIVE("NEG", R.string.negative),
    POSITIVE("POS", R.string.positive);

    private String code;
    private int description;
    public static LinkedHashMap<String, ObservationInterpretationEnum> observationInterpretationMap = null;

    private ObservationInterpretationEnum(String code, int description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        try {
            return MyApplication.getAppContext().getString(description);
        } catch (Exception e) {
            return "";
        }
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public static LinkedHashMap<String, ObservationInterpretationEnum> getMap() {
        if (observationInterpretationMap == null) {
            observationInterpretationMap = new LinkedHashMap<String, ObservationInterpretationEnum>();
            for (ObservationInterpretationEnum interpretation : ObservationInterpretationEnum.values()) {
                observationInterpretationMap.put(interpretation.code, interpretation);
            }
        }
        return observationInterpretationMap;
    }
}
