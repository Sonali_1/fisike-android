package com.mphrx.fisike.enums;

/**
 * Created by laxmansingh on 5/1/2017.
 */

public enum LanguageLocale {
    en,
    hi,
    fr

}
