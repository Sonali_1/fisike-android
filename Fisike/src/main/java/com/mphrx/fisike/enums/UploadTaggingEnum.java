package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha Rathore on 5/1/2017.
 */

public enum UploadTaggingEnum
{

    LAB_REPORT(MyApplication.getAppContext().getString(R.string.tag_lab_key),R.string.tag_lab),
    PRESCRIPTION(MyApplication.getAppContext().getString(R.string.tag_prescription_key), R.string.tag_prescription),
    DISCHARGE_NOTE(MyApplication.getAppContext().getString(R.string.tag_discharge_note_key), R.string.tag_discharge_note),
    RADIOLOGY_REPORT(MyApplication.getAppContext().getString(R.string.tag_radiology_report_key),R.string.tag_radiology_report),
    RADIOLOGY_IMAGE( MyApplication.getAppContext().getString(R.string.tag_radiology_image_key),R.string.tag_radiology_image),
    DOCTOR_LETTER(MyApplication.getAppContext().getString(R.string.tag_doctor_letter_key), R.string.tag_doctor_letter),
    OTHERS(MyApplication.getAppContext().getString(R.string.tag_other_key), R.string.tag_other),
    LAB(MyApplication.getAppContext().getString(R.string.lab_key), R.string.lab),
    MICROBIOLOGY(MyApplication.getAppContext().getString(R.string.microbiology_key), R.string.microbiology),
    RADIOLOGY(MyApplication.getAppContext().getString(R.string.radiologie_key), R.string.radiologie),
    ;


    private String code;
    private int value;
    private static LinkedHashMap<String, UploadTaggingEnum> uploadTaggingEnumLinkedHashMap = null;


    private UploadTaggingEnum(String code,int value){
        this.code = code;
        this.value=value;
    }


    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, UploadTaggingEnum> getUploadTaggingEnumLinkedHashMap(){
        if(uploadTaggingEnumLinkedHashMap == null){
            uploadTaggingEnumLinkedHashMap = new LinkedHashMap<String, UploadTaggingEnum>();
            for (UploadTaggingEnum vitals : UploadTaggingEnum.values()) {
                uploadTaggingEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return uploadTaggingEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getUploadTaggingEnumLinkedHashMap();
        for (Map.Entry<String, UploadTaggingEnum> enumMap : uploadTaggingEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getUploadTaggingEnumLinkedHashMap();
        if (uploadTaggingEnumLinkedHashMap != null) {
            for (Map.Entry<String, UploadTaggingEnum> enumMap : uploadTaggingEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
