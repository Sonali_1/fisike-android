package com.mphrx.fisike.enums;

import java.util.LinkedHashMap;

import com.mphrx.fisike.constant.TextConstants;

public enum PresenceMapEnum {
    Available(0),
    Busy(1),
	Unavailable(2);
	
	public int presenceValue;
	static LinkedHashMap<String, Integer> presenceHashMap = null;
		  
	
	private PresenceMapEnum(int presenceValue){
		this.presenceValue = presenceValue;
	}
	
	public int getValue(){
		return this.presenceValue;
	}
	
	public static LinkedHashMap<String, Integer> getPresenceMap(){
		if(presenceHashMap == null){
			presenceHashMap = new LinkedHashMap<String, Integer>();
			for(PresenceMapEnum presence : PresenceMapEnum.values()){
				presenceHashMap.put(presence.toString(), presence.getValue());
			}
		}
		return presenceHashMap;
	}
	
	@Override
	public String toString() {
		switch (this) {
		case Available:
			return TextConstants.STATUS_AVAILABLE_TEXT;
		case Unavailable:
			return TextConstants.STATUS_UNAVAILABLE_TEXT;
		case Busy:
			return TextConstants.STATUS_BUSY_TEXT;
		default:
			break;
		}
		return super.toString();
	}

}
