package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kailashkhurana on 09/08/17.
 */

public enum GenderApiEnum {
    MALE(MyApplication.getAppContext().getString(R.string.gender_male_code), R.string.gender_male_key),
    FEMALE(MyApplication.getAppContext().getString(R.string.gender_female_code), R.string.gender_female_key),
    OTHER(MyApplication.getAppContext().getString(R.string.gender_other_code), R.string.gender_other_key);


    private String code;
    private int value;
    private static LinkedHashMap<String, GenderApiEnum> genderEnumLinkedHashMap = null;


    private GenderApiEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, GenderApiEnum> getGenderEnumLinkedHashMap() {
        if (genderEnumLinkedHashMap == null) {
            genderEnumLinkedHashMap = new LinkedHashMap<String, GenderApiEnum>();
            for (GenderApiEnum vitals : GenderApiEnum.values()) {
                genderEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return genderEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getGenderEnumLinkedHashMap();
        for (Map.Entry<String, GenderApiEnum> enumMap : genderEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return value;
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getGenderEnumLinkedHashMap();
        if (genderEnumLinkedHashMap != null) {
            for (Map.Entry<String, GenderApiEnum> enumMap : genderEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
