package com.mphrx.fisike.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Neha Rathore on 5/4/2017.
 */

public enum DesignationEnum {

    DOCTOR(MyApplication.getAppContext().getString(R.string.des_doc_key), R.string.des_doc_value),
    PHYSICIAN(MyApplication.getAppContext().getString(R.string.des_phy_key), R.string.des_phy_value),
    SURGEON(MyApplication.getAppContext().getString(R.string.des_sur_key), R.string.des_sur_value),
    NURSE(MyApplication.getAppContext().getString(R.string.des_nur_key), R.string.des_nur_value),
    RADIOLOGY_TECHNICIAN(MyApplication.getAppContext().getString(R.string.des_rad_tech_key), R.string.des_rad_tech_value),
    MEDICAL_LAB_TECHNICIAN(MyApplication.getAppContext().getString(R.string.des_medical_tech_key), R.string.des_medical_tech_value),
    ADMIN_STAFF(MyApplication.getAppContext().getString(R.string.des_admin_key), R.string.des_admin_value),
    OTHER(MyApplication.getAppContext().getString(R.string.des_other_key), R.string.des_other_value),;


    private String code;
    private int value;
    private static LinkedHashMap<String, DesignationEnum> designationEnumLinkedHashMap = null;


    private DesignationEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, DesignationEnum> getDesignationEnumLinkedHashMap() {
        if (designationEnumLinkedHashMap == null) {
            designationEnumLinkedHashMap = new LinkedHashMap<String, DesignationEnum>();
            for (DesignationEnum vitals : DesignationEnum.values()) {
                designationEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return designationEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getDesignationEnumLinkedHashMap();
        for (Map.Entry<String, DesignationEnum> enumMap : designationEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getCode().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getDesignationEnumLinkedHashMap();
        if (designationEnumLinkedHashMap != null) {
            for (Map.Entry<String, DesignationEnum> enumMap : designationEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }

}
