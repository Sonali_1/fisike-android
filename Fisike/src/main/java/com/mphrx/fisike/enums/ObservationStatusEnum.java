package com.mphrx.fisike.enums;

import android.content.Context;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by xmb2nc on 12-04-2017.
 */

public enum ObservationStatusEnum {
    IP(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_ip), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_ip), R.string.observation_status_value_ip),
    P(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_p), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_p), R.string.observation_status_value_p),
    C(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_c), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_c), R.string.observation_status_value_c),
    F(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_f), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_f), R.string.observation_status_value_f),
    X(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_x), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_x), R.string.observation_status_value_x),
    CA(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_ca), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_ca), R.string.observation_status_value_ca),
    // CD:26 is the code
    CD(MyApplication.getAppContext().getResources().getString(R.string.observation_status_key_cd), MyApplication.getAppContext().getResources().getString(R.string.observation_status_keys_cd), R.string.observation_status_value_cd);

    private String code, completeCode;
    private int observationStatusValue;
    private static LinkedHashMap<String, ObservationStatusEnum> observationStatusMap = null;
    private static Context context = MyApplication.getAppContext();

    private ObservationStatusEnum(String code, String completeCode, int value) {
        this.code = code;
        this.completeCode = completeCode;
        this.observationStatusValue = value;
    }

    public String getCode() {
        return code;
    }


    public String getCompleteCode() {
        return completeCode;
    }

    public void setCompleteCode(String completeCode) {
        this.completeCode = completeCode;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ObservationStatusEnum> getObservationStatusMap() {
        if (observationStatusMap == null) {
            observationStatusMap = new LinkedHashMap<String, ObservationStatusEnum>();
            for (ObservationStatusEnum statusEnum : ObservationStatusEnum.values()) {
                observationStatusMap.put(statusEnum.code, statusEnum);
            }
        }
        return observationStatusMap;
    }

    public String getObservationStatusValue() {
        try {
            return MyApplication.getAppContext().getString(observationStatusValue);
        } catch (Exception e) {
            return "";
        }
    }

    public void setObservationStatusValue(int observationStatusValue) {
        this.observationStatusValue = observationStatusValue;
    }


    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getObservationStatusMap();
        for (Map.Entry<String, ObservationStatusEnum> enumMap : observationStatusMap.entrySet())
            if (enumMap.getValue().getObservationStatusValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getObservationStatusMap();
        if (observationStatusMap != null) {
            for (Map.Entry<String, ObservationStatusEnum> enumMap : observationStatusMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getObservationStatusValue();
        }
        return value;
    }




    //return code matching to particular value
    public static String getFullCodeFromValue(String value) {
        getObservationStatusMap();
        for (Map.Entry<String, ObservationStatusEnum> enumMap : observationStatusMap.entrySet())
            if (enumMap.getValue().getCompleteCode().equals(value))
                return enumMap.getValue().getCompleteCode();
        return "";
    }

    //return code matching to particular value
    public static String getFullDisplayedValuefromCode(String value) {
        getObservationStatusMap();
        if (observationStatusMap != null) {
            for (Map.Entry<String, ObservationStatusEnum> enumMap : observationStatusMap.entrySet())
                if (enumMap.getValue().getCompleteCode().equals(value)||enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getCompleteCode();
        }
        return value;
    }


}
