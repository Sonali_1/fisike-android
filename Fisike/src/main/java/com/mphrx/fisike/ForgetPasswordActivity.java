package com.mphrx.fisike;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.background.VolleyRequest.ForgetPasswordRequest.ForgetPasswordSendOTPApi;
import com.mphrx.fisike.background.VolleyRequest.ForgetPasswordRequest.SetPasswordApiRequest;
import com.mphrx.fisike.background.VolleyRequest.OTPRequest.VerifyOTPRequest;
import com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse.ForgetPasswordSendOTPResponse;
import com.mphrx.fisike.background.VolleyResponse.ForgetPasswordResponse.SetPasswordResponse;
import com.mphrx.fisike.background.VolleyResponse.OTPResponse.verifyOTPResponse;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.response.ForgetPassOtpVerifyResponse;
import com.mphrx.fisike.gson.response.ForgetPasswordResponseGson.ForgetPasswordSendOTPResponseGson;
import com.mphrx.fisike.gson.response.GenericStatusMsgResponse;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.provider.ChatIQ;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.EditTextSplitView;
import login.activity.LoginActivity;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

/**
 * Created by administrate on 12/21/2015.
 */
public class ForgetPasswordActivity extends NetworkActivity implements View.OnClickListener {


    CustomFontButton btnNext, btnContactUs;
    private CheckBox cbHidePasswordPat;
    private final int SEND_OTP_MODE = 0;
    private final int ENTER_OTP_MODE = 1;
    private final int ENTER_PASSWORD_MODE = 2;
    private final int SEND_OTP = 0;
    private final int RESEND_OTP = 1;
    private final int VERIFY_OTP = 2;
    private final int SET_PASSWORD = 3;
    private final int NO_ALTERNATE_CONTACT_MODE = 4;
    private final int ENTER_OTP_WITH_CONTACT = 5;
    private final int SET_PASSWORD_WITH_CONTACT = 6;

    private String email;
    private String otp;
    private String newPassword;
    private boolean isOTPSend;
    private CountDownTimer counttimer;
    private long OTPSendTime;
    private boolean isResetPass = false;
    private String alternateContact;
    private LinearLayout ll_no_contact_screen, ll_set_pass_enter_otp, ll_pass_otp;//ll_bottom_bar
    private CustomFontTextView tv_acc_secure, tv_contact, tv_forget_pass;
    private CustomFontButton btn_forget_password, btn_bottom, btn_bottom_signin_no_contact_screen;
    //    ImageView icFisike;
    private String OTPCode, NewPassword, title;
    private CustomEditTextWhite etPassword;
    private EditTextSplitView et_otp_code;
    private CustomFontTextView pass_verification;
    private CustomFontTextView tv_create_new;
    private CustomFontTextView tv_text_existing_account_continued, sign_in;
    private CustomFontTextView tv_choose_signup;
    private LinearLayout ll_existing_account;
    private CustomFontTextView tv_text_existing_account;
    boolean isResetPassword;
    private Toolbar toolbar_no_contact;
    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
//    private CustomFontButton btn_resend_otp_code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_forget_password);
        findView();
        initView();
        isResetPassword = SharedPref.getResetPassword();

        if (isResetPassword)
            title = getResources().getString(R.string.reset_password);
        else
            title = getResources().getString(R.string.forgot_password);

        toolbar_title.setText(title);


        SharedPref.setResetPassword(false);
        setForgetPasswordPage_pat();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setForgetPasswordPage_pat() {
        alternateContact = SharedPref.getForgetPasswordContactAltenate();

        // alternateContact="";
        if (alternateContact.equals(""))
            showView(NO_ALTERNATE_CONTACT_MODE);
        else
            showView(ENTER_OTP_WITH_CONTACT);
    }


    private void setForgetPasswordPage_Phy() {
        OTPSendTime = SharedPref.getForgetPassOTPSendTime();
        long timeDiff;
        if (OTPSendTime != 0) {
            long currentTimeStamp = System.currentTimeMillis();
            timeDiff = ((currentTimeStamp - SharedPref.getOTPSentTime()) / 1000);
            boolean isExpired = timeDiff > (60 * 60 * TextConstants.FORGET_PASS_SCREEN_TIME);
            if (!isExpired)
                showView(ENTER_OTP_MODE);
            else {
                SharedPref.setForgetPassOTP(null);
                SharedPref.setForgetPassOTPSendTime(0);
                showView(SEND_OTP_MODE);
            }
        } else {
            SharedPref.setForgetPassOTP(null);
            SharedPref.setForgetPassOTPSendTime(0);
            if (isResetPass)
                showView(ENTER_OTP_MODE);
            else
                showView(SEND_OTP_MODE);
        }


    }

    private void findView() {


        pass_verification = (CustomFontTextView) findViewById(R.id.pass_verification);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        cbHidePasswordPat = (CheckBox) findViewById(R.id.cb_showHidePass);

        ll_no_contact_screen = (LinearLayout) findViewById(R.id.ll_Alternate_Contact);
        btnContactUs = (CustomFontButton) ll_no_contact_screen.findViewById(R.id.btn);
        tv_create_new = (CustomFontTextView) ll_no_contact_screen.findViewById(R.id.tv_create_new);
        tv_text_existing_account_continued = (CustomFontTextView) ll_no_contact_screen.findViewById(R.id.tv_text_existing_account_continued);
        tv_choose_signup = (CustomFontTextView) ll_no_contact_screen.findViewById(R.id.tv_choose_signup);
        tv_text_existing_account = (CustomFontTextView) ll_no_contact_screen.findViewById(R.id.tv_text_existing_account);
        ll_existing_account = (LinearLayout) ll_no_contact_screen.findViewById(R.id.ll_existing_account);
        toolbar_no_contact = (Toolbar) ll_no_contact_screen.findViewById(R.id.toolbar);
        btn_bottom_signin_no_contact_screen = (CustomFontButton) ll_no_contact_screen.findViewById(R.id.btn_bottom_signin);
        ll_set_pass_enter_otp = (LinearLayout) findViewById(R.id.ll_verify_otp_set_pass);
//        tv_forget_pass= (CustomFontTextView) findViewById(R.id.tv_verify_pass);
        tv_acc_secure = (CustomFontTextView) ll_set_pass_enter_otp.findViewById(R.id.tv_acc_secure);
        tv_contact = (CustomFontTextView) ll_set_pass_enter_otp.findViewById(R.id.tv_contact);
        btnNext = (CustomFontButton) ll_set_pass_enter_otp.findViewById(R.id.btn_next);
        btn_forget_password = (CustomFontButton) ll_set_pass_enter_otp.findViewById(R.id.btn_forget_password);
//        btn_resend_otp_code = (CustomFontButton) ll_set_pass_enter_otp.findViewById(R.id.btn_forget_pass_resend_code);
//        ll_bottom_bar = (LinearLayout) ll_set_pass_enter_otp.findViewById(R.id.ll_bottom_bar);
        btn_bottom = (CustomFontButton) ll_set_pass_enter_otp.findViewById(R.id.btn_bottom);
//        icFisike= (ImageView) findViewById(R.id.img_fisike_icon);
        etPassword = (CustomEditTextWhite) ll_set_pass_enter_otp.findViewById(R.id.et_pass);
        et_otp_code = (EditTextSplitView) ll_set_pass_enter_otp.findViewById(R.id.et_otp_code);
        ll_pass_otp = (LinearLayout) findViewById(R.id.ll_pass_otp);
        etPassword.setPasswordKeyListener();
    }


    private void setNoAlternateContactScreen() {

        if (title.equals(getResources().getString(R.string.forgot_password))) {
            tv_create_new.setText(getResources().getString(R.string.Information));
//            ll_bottom_bar.setVisibility(View.GONE);
//            btn_bottom.setVisibility(View.GONE);

        }
//        tv_create_new.setText(title);
        btnContactUs.setText(R.string.contact_us);
        btn_bottom_signin_no_contact_screen.setVisibility(View.GONE);
        tv_text_existing_account_continued.setText(R.string.no_alternate_contact);
        //       tv_text_existing_account_continued.setVisibility(View.GONE);
        tv_choose_signup.setText(R.string.reset_pass_from_device);
        ll_existing_account.setVisibility(View.GONE);

    }

    private void resendOTPCode() {
        showProgressDialog();
        String resendId = new String();
        if (SharedPref.getIsMobileEnabled())
            resendId =/* SharedPref.getCountryCode()+""+*/SharedPref.getMobileNumber();
        else if (!SharedPref.getIsMobileEnabled())
            resendId = SharedPref.getEmailAddress();
        ThreadManager.getDefaultExecutorService().submit(new ForgetPasswordSendOTPApi(resendId, SharedPref.getDeviceUid(), getTransactionId(),SharedPref.getCountryCode()));
    }

    private void initView() {
        //hiding password in patient app:
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        email = SharedPref.getEmailAddress();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        btn_bottom.setOnClickListener(this);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_back_w_shadow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBack();
            }
        });
    }


    private void handleBack() {
        String title = getSupportActionBar().getTitle().toString();
        if (title.equals(getString(R.string.forgot_password_confirm_password))) {
            showView(ENTER_OTP_WITH_CONTACT);
        } else {
            Intent i = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            ForgetPasswordActivity.this.finish();
        }
    }


    private void showView(int mode) {

        switch (mode) {

            case ENTER_OTP_WITH_CONTACT:
                setPassScreen(ENTER_OTP_WITH_CONTACT);

                break;

            case SET_PASSWORD_WITH_CONTACT:
                setPassScreen(SET_PASSWORD_WITH_CONTACT);
                break;


            case NO_ALTERNATE_CONTACT_MODE:
                toolbar_no_contact.setVisibility(View.GONE);
                ll_no_contact_screen.setVisibility(View.VISIBLE);
                if (title.equals(getResources().getString(R.string.forgot_password))) {
                } else {
                }
                ll_set_pass_enter_otp.setVisibility(View.GONE);
                btnContactUs.setText(R.string.contact_us);
                setNoAlternateContactScreen();

        }
    }


    private void setPassScreen(int mode) {
        btn_forget_password.setVisibility(View.GONE);
        ll_no_contact_screen.setVisibility(View.GONE);
        ll_set_pass_enter_otp.setVisibility(View.VISIBLE);
        btn_bottom.setVisibility(View.GONE);
        switch (mode) {
            case ENTER_OTP_WITH_CONTACT:
                getSupportActionBar().setTitle(title);
                tv_acc_secure.setText(R.string.account_secure_label);
                tv_contact.setVisibility(View.VISIBLE);
                tv_contact.setText(getResources().getString(R.string.sent_email) + " " + SharedPref.getForgetPasswordContactAltenate());
                btnNext.setText(R.string.next);
                btn_forget_password.setText(R.string.resend_code);
                btn_forget_password.setVisibility(View.VISIBLE);
                ll_pass_otp.setVisibility(View.GONE);
                et_otp_code.setVisibility(View.VISIBLE);

                break;
            case SET_PASSWORD_WITH_CONTACT:
                tv_acc_secure.setText(getResources().getString(R.string.Your_account_is_secure_with_us));
                tv_contact.setVisibility(View.GONE);
                btnNext.setText(R.string.done);
                btn_forget_password.setVisibility(View.GONE);
                getSupportActionBar().setTitle(R.string.forgot_password_confirm_password);
                etPassword.setHint(R.string.newpassword);
                ll_pass_otp.setVisibility(View.VISIBLE);
                et_otp_code.setVisibility(View.GONE);
                break;
        }
    }


    public void onClick(View v) {
        Utils.hideKeyboard(this);
        if (Utils.showDialogForNoNetwork(this, false)) {
            switch (v.getId()) {
                case R.id.btn_forget_password:
                    resendOTPCode();
                    break;
                case R.id.btn_new_signupp:
                    cancel();
                    break;
                case R.id.btn:
                    contactUs();
                    break;
            }
        }
    }

    public void executeForgetPassOTPSendAPI(GenericStatusMsgResponse response, String result_status) {
        if (result_status.equals(TextConstants.SUCESSFULL_API_CALL) && (response.getHttpStatusCode() == 200)) {

            long currentTime = System.currentTimeMillis();
            SharedPref.setForgetPassOTPSendTime(currentTime);
            if (isOTPSend)
                showView(ENTER_OTP_MODE);
            else
                Toast.makeText(this, getResources().getString(R.string.otp_sent_mail_successfully), Toast.LENGTH_SHORT).show();

            counttimer.start();
        } else {
            Toast.makeText(this, result_status, Toast.LENGTH_SHORT).show();
        }

    }

    public void executeForgetPassOTPVerifyAPI(ForgetPassOtpVerifyResponse response, String result_status, String otpCode) {
        if (result_status.equals(TextConstants.SUCESSFULL_API_CALL)) {

            if (response.getHttpStatusCode() == 200) {
                showView(ENTER_PASSWORD_MODE);
                SharedPref.setForgetPassOTP(otpCode);
            } else
                Toast.makeText(this, getResources().getString(R.string.otp_not_matched_try_again), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, result_status, Toast.LENGTH_SHORT).show();
        }

    }

    public void executeForgetPassPasswdResetAPI(GenericStatusMsgResponse response, String result_status) {
        if (result_status.equals(TextConstants.SUCESSFULL_API_CALL)) {

            if (response.getHttpStatusCode() == 200) {
                SharedPref.setAccessToken(null);
                SharedPref.setUserLogout(false);
                SharedPref.setForgetPassOTP(null);
                SharedPref.setForgetPassOTPSendTime(0);
                SharedPref.setLoginAttempts(0);
                Toast.makeText(getApplicationContext(), MyApplication.getAppContext().getResources().getString(R.string.password_successfully_changed), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

        } else {
            Toast.makeText(this, result_status, Toast.LENGTH_SHORT).show();
        }
    }


    public boolean checkPermission(final String permissionType, String message, final int requestCode) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(ForgetPasswordActivity.this, new String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted();

                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
            }

            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {

                case SEND_OTP:
                    isOTPSend = true;
                    //sendOTP();
                case RESEND_OTP:
                    isOTPSend = false;
                    //sendOTP();
                case VERIFY_OTP:
                    //verifyOTP();
                case SET_PASSWORD:
                    //resetPassword();


            }
        } else
            onPermissionNotGranted();
    }

    public void onPermissionNotGranted() {
        Toast.makeText(this, getResources().getString(R.string.You_need_to_add_Permission_to_continue), Toast.LENGTH_SHORT)
                .show();

    }


    private void contactUs() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse(getResources().getString(R.string.mailto))); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
        intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(this, SettingManager.getInstance().getUserMO()));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void cancel() {
        this.finish();
    }


    private void next() {
        if (btnNext.getText().toString().equalsIgnoreCase(TextConstants.NEXT))
            verifyOtpOnMailOrEmail();

        else
            resetPasswordWithAlternateContact();
    }

    private void resetPasswordWithAlternateContact() {
        String alternateContact = SharedPref.getForgetPasswordContactAltenate();
        String contact = new String();
        if (SharedPref.getIsMobileEnabled())
            contact =/* SharedPref.getCountryCode()+""+*/SharedPref.getMobileNumber();
        else if (!SharedPref.getIsMobileEnabled())
            contact = SharedPref.getEmailAddress();
        NewPassword = etPassword.getText().toString().trim();
        if (NewPassword.equals("")) {
            etPassword.setError(MyApplication.getAppContext().getResources().getString(R.string.enter_yourpassword));
            return;
        }

        String error = Utils.validatePassword(NewPassword, this).trim();
        if (error.equals("")) {
            if (Utils.showDialogForNoNetwork(this, false)) {
                showProgressDialog();

                if (alternateContact.trim().equalsIgnoreCase(contact.trim()))
                    ThreadManager.getDefaultExecutorService().submit(new SetPasswordApiRequest(SharedPref.getForgetPasswordContactAltenate(), NewPassword, SharedPref.getDeviceUid(), OTPCode, null, SharedPref.getCountryCode(),getTransactionId()));
                else
                    ThreadManager.getDefaultExecutorService().submit(new SetPasswordApiRequest(contact, NewPassword, SharedPref.getDeviceUid(), OTPCode, SharedPref.getForgetPasswordContactAltenate(),SharedPref.getCountryCode(), getTransactionId()));
            }
        } else {
            etPassword.setError(error);
            return;
        }

    }

    private void verifyOtpOnMailOrEmail() {
        String alternateContact = SharedPref.getForgetPasswordContactAltenate();
        String contact = new String();
        if (SharedPref.getIsMobileEnabled())
            contact = /*SharedPref.getCountryCode()+""+*/SharedPref.getMobileNumber();
        else if (!SharedPref.getIsMobileEnabled())
            contact = SharedPref.getEmailAddress();
        OTPCode = et_otp_code.getValue().trim();
        if (OTPCode.length() != 4) {
            if (OTPCode.equals(""))
                Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.enter_a_valid_value), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.enter_valid_4_digit_code), Toast.LENGTH_SHORT).show();


            return;
        }

        showProgressDialog();

        if (alternateContact.trim().equalsIgnoreCase(contact.trim()))
            ThreadManager.getDefaultExecutorService().submit(new VerifyOTPRequest(SharedPref.getForgetPasswordContactAltenate(), OTPCode, getTransactionId(), SharedPref.getDeviceUid(),SharedPref.getCountryCode()));
        else
            ThreadManager.getDefaultExecutorService().submit(new VerifyOTPRequest(contact, OTPCode, getTransactionId(), SharedPref.getDeviceUid(), SharedPref.getForgetPasswordContactAltenate(),SharedPref.getCountryCode()));
    }


    @Subscribe
    public void onForgetPasswordSendOTPResponse(ForgetPasswordSendOTPResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {

            ForgetPasswordSendOTPResponseGson objectResponse = response.getForgetPasswordSendOTPResponse();
            if (objectResponse.getAlternateContact() != null)
                SharedPref.setForgetPasswordContactAltenate(objectResponse.getAlternateContact());
            else if (objectResponse.getStatus().equals("SC200") && objectResponse.getAlternateContact() == null)
                SharedPref.setForgetPasswordContactAltenate(/*SharedPref.getCountryCode()+""+*/SharedPref.getMobileNumber());
            setForgetPasswordPage_pat();
            Toast.makeText(this, getResources().getString(R.string.sent_4_digit_code_again), Toast.LENGTH_SHORT).show();
        } else if (response != null && response.getMsg() != null && (response.getMsg().equals(TextConstants.USER_DEVICE_TOKEN_NOT_FOUND) || response.getMsg().equals(TextConstants.ACTIVE_DEVICE_AND_ALTERNATE_CONTACT_NOT_FOUND))) {
            SharedPref.setForgetPasswordContactAltenate(null);
            setForgetPasswordPage_pat();
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void fetchInitialDataFromServer() {
        // Do Nothing
    }

    @Override
    public void fetchInitialDataFromCache(String cacheResponse) {
        // Do Nothing
    }

    @Subscribe
    public void onVerifyOTPFromServerResponse(verifyOTPResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();
        if (response.isSuccessful()) {
            if (response.getVerifyOTPResponseGson().getMsg().equals(TextConstants.SUCCESSFUL)) {
                //  unregisterSMSReceiver();
                SharedPref.setForgetPassOTP(OTPCode);
                showView(SET_PASSWORD_WITH_CONTACT);
            } else if (response.getVerifyOTPResponseGson().getMsg().equals(TextConstants.OTP_NOT_FOUND)) {
//                Toast.makeText(this, TextConstants.OTP_NOT_MATCHED_TEXT, Toast.LENGTH_SHORT).show();
                Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.incorrect_4_digit_code), Toast.LENGTH_SHORT).show();
                showView(ENTER_OTP_WITH_CONTACT);
            } else if (response.getVerifyOTPResponseGson().getMsg().equals(TextConstants.OTP_TIME_EXPIRED))
                Toast.makeText(this, getResources().getString(R.string.one_time_password_expired), Toast.LENGTH_SHORT).show();
                //otp expired

            else {
                Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    @Subscribe
    public void onSetPasswordResponse(SetPasswordResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        dismissProgressDialog();
        if (response.isSuccessful() && response.getSetPasswordResponseGson().getMsg().equals(TextConstants.SUCCESSFUL)) {
            //  unregisterSMSReceiver();
            Intent i = new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPref.setForgetPasswordContactAltenate(null);
            startActivity(i);

        } else if (response != null && (response.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_422)) {
            String msg = "";
            if (response.getMsg() != null) {
                if (response.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_LENGTH))
                    msg = getString(R.string.password_with_length_error, (SharedPref.getPasswordLength() + ""));
                else if (response.getMsg().equals(TextConstants.USER_PASSWORD_INCORRECT_PATTERN))
                    msg = getString(R.string.password_pattern_error);
                else if (response.getMsg().equals(TextConstants.OLD_PASSWORD_INVALID))
                    msg = getString(R.string.old_password_incorrect);
                else if (response.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() == 0)
                    msg = getString(R.string.last_password_mismatch);
                else if (response.getMsg().equals(TextConstants.USER_PASSWORD_SAME) && SharedPref.getPasswordHistoryCount() != 0)
                    msg = getString(R.string.last_n_password_mismatch, SharedPref.getPasswordHistoryCount() + "");
                else
                    msg = TextConstants.UNEXPECTED_ERROR;
            } else
                msg = TextConstants.UNEXPECTED_ERROR;

            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            showView(SET_PASSWORD_WITH_CONTACT);
        } else if (response != null && (response.getHttpStatusCode() + "").equals(TextConstants.HTTP_STATUS_423)
                && response.getMsg() != null && response.getMsg().equals(TextConstants.NEW_PASSWORD_INVALID)) {
            Toast.makeText(this, getString(R.string.same_old_new_pass), Toast.LENGTH_SHORT).show();
            showView(SET_PASSWORD_WITH_CONTACT);

        } else {
            Toast.makeText(this, TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            showView(SET_PASSWORD_WITH_CONTACT);
        }

    }
}


