package careplan.background;

import android.app.Activity;
import android.os.AsyncTask;

import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.util.List;

import careplan.database.databaseAdapter.CarePlanDatabaseAdapter;
import careplan.enums.ObserverOperationEnum;
import careplan.model.CarePlanModel;
import careplan.observable.CarePlanObservable;
import careplan.request.CarePlanRequest;

/**
 * Created by Kailash Khurana on 9/13/2016.
 */
public class CarePlanFetch extends AsyncTask<Void, Void, Void> {

    private Activity activity;
    private long patientId;
    private List<CarePlanModel> carePlanModelList;

    public CarePlanFetch(Activity activity,long patientId) {
        this.activity = activity;
        this.patientId = patientId;
    }

    @Override
    protected Void doInBackground(Void... params) {
        carePlanModelList = CarePlanDatabaseAdapter.getInstance(activity).fetchCarePlanList();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (carePlanModelList != null && carePlanModelList.size() > 0) {
            ObserverOperationEnum insert = ObserverOperationEnum.INSERT;
            CarePlanObservable.getInstance().setData(carePlanModelList, insert.getOpenation(), true, "");
        } else {
            if (Utils.showDialogForNoNetwork(activity, false)) {
                ThreadManager.getDefaultExecutorService().submit(new CarePlanRequest(System.currentTimeMillis(), activity, false, patientId));
            }else{
                CarePlanObservable.getInstance().setData(carePlanModelList, -1, true, "");
            }
        }
        super.onPostExecute(aVoid);
    }
}
