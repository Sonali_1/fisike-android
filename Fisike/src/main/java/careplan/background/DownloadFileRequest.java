package careplan.background;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.mphrx.fisike.PdfPreview;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.OpenImageFragment;
import com.mphrx.fisike.models.DocumentReferenceModel;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import careplan.constants.URLConstants;
import careplan.fragments.CareTaskDetailFragment;
import careplan.model.CareTaskAttachemtsModel;

/**
 * Created by Kailash Khurana on 3/28/2016.
 */
public class DownloadFileRequest extends AsyncTask<Void, Void, Boolean> {
    private CareTaskDetailFragment careTaskDetailFragment;
    private CareTaskAttachemtsModel careTaskAttachemtsModel;
    private int position;
    private long careTaskId;
    private Context context;
    private int docId;
    private int mimeType;
    private File file;
    private ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.progress_dialog));
        progressDialog.show();
        super.onPreExecute();
    }

    public DownloadFileRequest(Context context, int docId, int mimeType, CareTaskDetailFragment careTaskDetailFragment, CareTaskAttachemtsModel careTaskAttachemtsModel, int position, long careTaskId) {
        this.context = context;
        this.docId = docId;
        this.mimeType = mimeType;
        this.careTaskDetailFragment = careTaskDetailFragment;
        this.careTaskAttachemtsModel = careTaskAttachemtsModel;
        this.position = position;
        this.careTaskId = careTaskId;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String fileName = "tempFileDownloaded";
        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", "downloadedFile.pdf");
        } else if (mimeType == VariableConstants.MIME_TYPE_DOC) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", "downloadedFile.doc");
        } else if (mimeType == VariableConstants.MIME_TYPE_DOCX) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", "downloadedFile.docx");
        } else if (mimeType == VariableConstants.MIME_TYPE_IMAGE) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", "downloadedFile.jpg");
        } else if (mimeType == VariableConstants.MIME_TYPE_TEXT) {
            file = Utils.createFile(Environment.getExternalStorageDirectory(), "/" + context.getString(R.string.app_name) + "/.temp", "downloadedFile.txt");
        }
        return downloadFile(docId, file);
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (file == null || !aVoid) {
            Toast.makeText(context, context.getResources().getString(R.string.Something_went_wrong), Toast.LENGTH_SHORT).show();
            return;
        }
        if (mimeType == VariableConstants.MIME_TYPE_FILE) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/pdf");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(context, PdfPreview.class).putExtra(VariableConstants.PDF_FILE_PATH, file.getAbsolutePath()).putExtra(VariableConstants.MIME_TYPE, mimeType));
            }
        } else if (mimeType == VariableConstants.MIME_TYPE_DOC || mimeType == VariableConstants.MIME_TYPE_DOCX) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/msword");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context,context.getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
            }
        } else if (mimeType == VariableConstants.MIME_TYPE_IMAGE) {
            context.startActivity(new Intent(context, OpenImageFragment.class).putExtra(VariableConstants.DOCUMENT_TYPE, "").putExtra(VariableConstants.VIDEO_URI,
                    file.getAbsolutePath()).putExtra(VariableConstants.MIME_TYPE, careTaskAttachemtsModel.getMimeType()).putExtra(VariableConstants.DECRIPTED_FILE, true));//careTaskAttachemtsModel.getMimeType()
        } else if (mimeType == VariableConstants.MIME_TYPE_TEXT) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), careTaskAttachemtsModel.getType());
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.Can_not_open_document), Toast.LENGTH_SHORT).show();
            }
        }
        super.onPostExecute(aVoid);
    }

    private boolean downloadFile(int docId, File file) {
        InputStream input = null;
        FileOutputStream fos = null;
        HttpsURLConnection urlConnection = null;
        ByteArrayOutputStream output = null;
        try {
            URL httpPost = new URL(APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_DOWNLOAD_FILE_CONSTANT);
            urlConnection = (HttpsURLConnection) httpPost.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            // SharedPreferences sharedPreferences = context.getSharedPreferences(VariableConstants.USER_DETAILS_FILE, 0);
            // String authToken = sharedPreferences.getString(VariableConstants.AUTH_TOKEN, "");
            String authToken = SharedPref.getAccessToken();
            if (authToken != null) {
                urlConnection.addRequestProperty("x-auth-token", authToken);
            }

//            String str = "{\"attachmentId\":\"23446\",\"contentType\":\"image/jpeg\",\"attachmentName\":\"android_Care Plans_Task Details.jpg\"}";
            String str = "{\"attachmentId\":\"" + careTaskAttachemtsModel.getId() + "\",\"contentType\":\"" + careTaskAttachemtsModel.getType() + "\",\"attachmentName\":\"" + careTaskAttachemtsModel.getFileName() + "\",\"taskId\":\"" + careTaskId + "\"}";
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            urlConnection.connect();

            String headerType = urlConnection.getHeaderField("Content-Type");

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
//            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                return false;
//            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = urlConnection.getContentLength();

            if (file == null) {
                return false;
            }

            // download the file
            input = urlConnection.getInputStream();
            fos = new FileOutputStream(file.getAbsoluteFile());

            byte data[] = new byte[2048];
            int count;
            long total = 0;
            output = new ByteArrayOutputStream();

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.writeTo(fos);
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (fos != null)
                    fos.close();
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return true;
    }

}