package careplan.background;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.util.List;

import careplan.database.databaseAdapter.CareTaskDatabaseAdapter;
import careplan.enums.ObserverOperationEnum;
import careplan.model.CareTaskModel;
import careplan.observable.CarePlanObservable;
import careplan.observable.CareTaskObservable;
import careplan.request.CareTaskRequest;

/**
 * Created by Kailash Khurana on 9/14/2016.
 */
public class CareTaskFetch extends AsyncTask<Void, Void, Void> {

    private Activity activity;
    private long transactionId;
    private boolean isFromCarePlanScreen;
    private List<CareTaskModel> careTaskModelList;

    public CareTaskFetch(Activity activity, long transactionId, boolean isFromCarePlanScreen) {
        this.activity = activity;
        this.transactionId = transactionId;
        this.isFromCarePlanScreen = isFromCarePlanScreen;
    }

    @Override
    protected Void doInBackground(Void... params) {
        careTaskModelList = CareTaskDatabaseAdapter.getInstance(activity).fetchCareTaskList();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (careTaskModelList != null && careTaskModelList.size() > 0) {
            ObserverOperationEnum insert = ObserverOperationEnum.INSERT;
            CareTaskObservable.getInstance().setData(careTaskModelList, insert.getOpenation(), true, "");
        } else {
            if (Utils.isNetworkAvailable(activity)) {
                ThreadManager.getDefaultExecutorService().submit(new CareTaskRequest(transactionId, activity, 0, false, isFromCarePlanScreen, null));
                CareTaskObservable.getInstance().setData(careTaskModelList, -1, true, "");
            }
        }
        super.onPostExecute(aVoid);
    }
}