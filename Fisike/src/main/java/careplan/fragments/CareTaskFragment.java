package careplan.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.SettingWebView;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.adapter.VitalsAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import careplan.activity.CareTaskDetailActivity;
import careplan.activity.CareTaskFilterActivity;
import careplan.adapter.CareTaskRecyclerAdadpter;
import careplan.background.CareTaskFetch;
import careplan.constants.CarePlanConstants;
import careplan.model.CareHeaderModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;
import careplan.observable.CareTaskObservable;
import careplan.request.CareTaskRequest;
import careplan.response.CareTaskEditResponse;
import careplan.response.CareTaskResponse;
import careplan.response.CareTaskSubmitResponse;

/**
 * Created by Kailash Khurana on 9/1/2016.
 */
public class CareTaskFragment extends CareTaskActionFragment implements CareTaskRecyclerAdadpter.OnItemClickedListener, SwipeRefreshLayout.OnRefreshListener, Observer {

    private View careTaskFragmentView;
    private RecyclerView careTaskRecylerView;
    private CareTaskRecyclerAdadpter careTasksAdapter;
    //    private long mTransactionId;
    private List<Object> careTaskModelList = new ArrayList<Object>();
    private long caseLoadId;
    private SwipeRefreshLayout carePlanSwipeRefreshLayout;
    private boolean isFromCarePlanScreen;
    private boolean isFromSearchScreen = false;
    private CustomFontTextView txtEmptyText;
    private String searchStatus = null;
    private boolean isCompleted;

    public CareTaskFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        careTaskFragmentView = inflater.inflate(R.layout.care_plan_fragment, container, false);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(CarePlanConstants.CASE_LOAD_ID)) {
            caseLoadId = bundle.getLong(CarePlanConstants.CASE_LOAD_ID, 0);
        }
        if (bundle != null && bundle.containsKey(CarePlanConstants.VIEW_CARE_PLAN_SCREEN)) {
            isFromCarePlanScreen = bundle.getBoolean(CarePlanConstants.VIEW_CARE_PLAN_SCREEN);
        }

        if (caseLoadId == 0 || BuildConfig.isPhysicianApp) {
            ((BaseActivity) getActivity()).setToolbarTitle(getActivity().getResources().getString(R.string.what_next));
        }

        findView();
        initView();

        fetchCareTasksFromDatabase();
        if (BuildConfig.isPatientApp) {
            setHasOptionsMenu(true);
        }

        onRefresh();

        return careTaskFragmentView;
    }



    @Override
    public void onResume() {
        CareTaskObservable.getInstance().addObserver(this);
        try {
            BusProvider.getInstance().register(this);
        } catch (Exception e) {
        }
        super.onResume();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        careTaskRecylerView.setOnScrollListener(new EndLessScrollingListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView == null || recyclerView.getChildAt(0) == null) {
                    return;
                }
                if (recyclerView.getChildAt(0).getTop() == 0) {
                    carePlanSwipeRefreshLayout.setEnabled(true);
                } else {
                    carePlanSwipeRefreshLayout.setEnabled(false);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        //clear old list befor assigning to database
        if (careTaskModelList != null)
            careTaskModelList.clear();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        careTaskRecylerView.setLayoutManager(mLayoutManager);

        careTaskRecylerView.setItemAnimator(new DefaultItemAnimator());
        if (Utils.isValueAvailable(searchStatus) && (searchStatus.equalsIgnoreCase(CarePlanConstants.COMPLETE_STATUS) || searchStatus.equalsIgnoreCase(CarePlanConstants.CANCEL_STATUS)))
            isCompleted = true;
        careTasksAdapter = new CareTaskRecyclerAdadpter(getActivity(), careTaskModelList, this, this, caseLoadId, isFromCarePlanScreen, isCompleted);
        careTaskRecylerView.setAdapter(careTasksAdapter);
        careTasksAdapter.notifyItemsView();
    }

    private void findView() {
        careTaskRecylerView = (RecyclerView) careTaskFragmentView.findViewById(R.id.rv_care_plan);
        carePlanSwipeRefreshLayout = (SwipeRefreshLayout) careTaskFragmentView.findViewById(R.id.care_plan_refresh_layout);
        carePlanSwipeRefreshLayout.setColorSchemeResources(R.color.care_plan_status_button_color);
        carePlanSwipeRefreshLayout.setOnRefreshListener(this);
        txtEmptyText = ((CustomFontTextView) careTaskFragmentView.findViewById(R.id.txtEmptyText));
        txtEmptyText.setText(getActivity().getResources().getString(R.string.care_task_empty_name));

    }

    public void fetchCareTasksFromDatabase() {
        if (getArguments() != null && getArguments().containsKey("careTaskActiviti")) {
            onRefresh();
            if (caseLoadId != 0 || isFromCarePlanScreen && BuildConfig.isPhysicianApp) {
            } else {
                new CareTaskFetch(getActivity(), getTransactionId(), isFromCarePlanScreen).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            if (caseLoadId != 0 || isFromCarePlanScreen && BuildConfig.isPhysicianApp) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    ThreadManager.getDefaultExecutorService().submit(new CareTaskRequest(getTransactionId(), getActivity(), caseLoadId, false, isFromCarePlanScreen, searchStatus));
                }
            } else {
                new CareTaskFetch(getActivity(), getTransactionId(), isFromCarePlanScreen).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (BuildConfig.isPatientApp && !isFromCarePlanScreen) {
            getActivity().invalidateOptionsMenu();
            menu.clear();
            menu.add(Menu.NONE, TextConstants.MENU_ITEM_SEARCH, Menu.NONE, "Search").setIcon(R.drawable.ic_filter_list_white_36dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
    }


    public void showProgressDialog(String message) {
        pdialog = new ProgressDialog(getActivity());
        pdialog.setMessage(message);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.show();
    }


    @Override
    public void itemClicked(View view, int position) {
        switch (view.getId()) {
            case R.id.btnViewDoc:
                Intent i = new Intent(getActivity(), SettingWebView.class);
                i.putExtra(VariableConstants.TITLE_BAR, getActivity().getResources().getString(R.string.view_document));
                i.putExtra(VariableConstants.WEB_URL, ((CareTaskModel) careTaskModelList.get(position)).getCareTaskURL());
                if (i != null && Utils.showDialogForNoNetwork(getActivity(), false))
                    startActivity(i);
                break;
            case R.id.tv_view_detail:
                Intent intent = new Intent(getActivity(), CareTaskDetailActivity.class);
                CareTaskModel careTaskModel = ((CareTaskModel) careTaskModelList.get(position));
                intent.putExtra(CarePlanConstants.CARE_TASK_OBJECT, careTaskModel);
                intent.putExtra(CarePlanConstants.VIEW_CARE_PLAN_SCREEN, isFromCarePlanScreen);
                intent.putExtra(CarePlanConstants.CARE_TASK_FILTER_STATUS, searchStatus);
                intent.putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, ((CareTaskModel) careTaskModelList.get(position)).getQuestionnaireUrl());
                getActivity().startActivityForResult(intent, TextConstants.INTENT_CONSTANT);
                break;
            case R.id.tv_care_task_download_doc:
                Intent intent1 = new Intent(getActivity(), CareTaskDetailActivity.class);
                CareTaskModel careTaskModel1 = ((CareTaskModel) careTaskModelList.get(position));
                intent1.putExtra(CarePlanConstants.CARE_TASK_OBJECT, careTaskModel1);
                intent1.putExtra(CarePlanConstants.CARE_TASK_VIEW_ATTACHMENT, true);
                intent1.putExtra(CarePlanConstants.VIEW_CARE_PLAN_SCREEN, isFromCarePlanScreen);
                intent1.putExtra(CarePlanConstants.CARE_TASK_FILTER_STATUS, searchStatus);
                intent1.putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, careTaskModel1.getQuestionnaireUrl());

                getActivity().startActivityForResult(intent1, TextConstants.INTENT_CONSTANT);
                break;

        }
    }

    @Override
    public void handleAction(Context context, CareTaskModel careTaskModel, CareTaskButtons careTaskButtons, List<CareTaskButtons> listButtons, long careTaskId, String questionnaireUrl) {
        super.handleAction(context, careTaskModel, careTaskButtons, listButtons, careTaskId, questionnaireUrl);
    }

    @Subscribe
    public void onCareTaskSubmitResponse(CareTaskSubmitResponse careTaskSubmitResponse) {
        carePlanSwipeRefreshLayout.setRefreshing(false);
        if (mTransactionId != careTaskSubmitResponse.getTransactionId()) {
            return;
        }
        dismissProgressDialog();
        if (carePlanSwipeRefreshLayout.isRefreshing()) {
            carePlanSwipeRefreshLayout.setRefreshing(false);
        }
        if (careTaskSubmitResponse.isSuccess()) {
            carePlanSwipeRefreshLayout.setEnabled(true);
            carePlanSwipeRefreshLayout.setRefreshing(true);
            onRefresh();
            removeTask(careTaskSubmitResponse.getId());
            return;
        }
        Toast.makeText(getActivity(), careTaskSubmitResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onCareTaskEditResponse(CareTaskEditResponse careTaskEditResponse) {
        if (mTransactionId != careTaskEditResponse.getTransactionId()) {
            return;
        }
        dismissProgressDialog();
        if (carePlanSwipeRefreshLayout.isRefreshing()) {
            carePlanSwipeRefreshLayout.setRefreshing(false);
        }
        if (careTaskEditResponse.isSuccess()) {
            onRefresh();
            removeTask(careTaskEditResponse.getId());
            return;
        }
        Toast.makeText(getActivity(), careTaskEditResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void removeTask(long id) {
        for (int i = 0; careTaskModelList != null && i < careTaskModelList.size(); i++) {
            if (careTaskModelList.get(i) instanceof CareTaskModel) {
                CareTaskModel careTaskModel = (CareTaskModel) careTaskModelList.get(i);
                if (careTaskModel.getTaskId() == id) {
                    careTaskModelList.remove(i);
                    careTasksAdapter.notifyItemsView();
                    return;
                }
            }
        }
    }

    @Subscribe
    public void onCareTasksReceived(CareTaskResponse careTaskResponse) {
        if (mTransactionId != careTaskResponse.getTransactionId()) {
            return;
        }
        dismissProgressDialog();
        if (carePlanSwipeRefreshLayout.isRefreshing()) {
            carePlanSwipeRefreshLayout.setRefreshing(false);
        }

        if (careTaskResponse.isError()) {
            Toast.makeText(getActivity(), careTaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
            if (careTaskModelList == null || careTaskModelList.size() == 0) {
                careTaskFragmentView.findViewById(R.id.rl_care_plan_empty_view).setVisibility(View.VISIBLE);
                txtEmptyText.setText(getActivity().getResources().getString(R.string.care_task_empty_name));
            }
            return;
        } else {
            careTaskFragmentView.findViewById(R.id.rl_care_plan_empty_view).setVisibility(View.GONE);
        }

        careTaskModelList.clear();
        if (careTaskResponse.getCareTaskList() == null || careTaskResponse.getCareTaskList().size() == 0) {
            careTasksAdapter.setCareTaskLis(careTaskModelList);
            careTasksAdapter.notifyItemsView();
            careTaskFragmentView.findViewById(R.id.rl_care_plan_empty_view).setVisibility(View.VISIBLE);
            if (isFromSearchScreen)
                txtEmptyText.setText(getActivity().getResources().getString(R.string.care_task_no_search_result));
            else
                txtEmptyText.setText(getActivity().getResources().getString(R.string.care_task_empty_name));
            return;
        }

        List<Object> careTaskList = careTaskResponse.getCareTaskList();
        long totalActiveCareTaskCount = SharedPref.getTotalActiveCareTaskCount();
        CareHeaderModel careHeaderModel = new CareHeaderModel(totalActiveCareTaskCount);
        careTaskModelList.add(0, careHeaderModel);
        careTaskModelList.addAll(careTaskList);
        careTasksAdapter.setCareTaskLis(careTaskModelList);
        careTasksAdapter.setCompleted(isCompleted);
        careTasksAdapter.notifyItemsView();
        isFromSearchScreen = false;
        careTasksAdapter.notifyDataSetChanged();

    }

    @Override
    public void onPause() {
        CareTaskObservable.getInstance().deleteObserver(this);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BusProvider.getInstance().unregister(this);
            }
        });
        super.onPause();
    }

    @Override
    public void onRefresh() {
        carePlanSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                carePlanSwipeRefreshLayout.setRefreshing(true);
            }
        });
        setRefreshDataFromApi();
    }

    private void setRefreshDataFromApi() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            if (Utils.isValueAvailable(searchStatus) && (searchStatus.equalsIgnoreCase(CarePlanConstants.COMPLETE_STATUS) || searchStatus.equalsIgnoreCase(CarePlanConstants.CANCEL_STATUS) || searchStatus.equalsIgnoreCase(CarePlanConstants.FILTER_STATUS_FINISHED))) {
                isCompleted = true;
            } else {
                isCompleted = false;
            }
            ThreadManager.getDefaultExecutorService().submit(new CareTaskRequest(getTransactionId(), getActivity(), caseLoadId, true, isFromCarePlanScreen, searchStatus));
        } else {
            dismissProgressDialog();
            carePlanSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    carePlanSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void update(Observable observable, final Object data) {
        if (observable == null || !(observable instanceof CareTaskObservable)) {
            return;
        }

        final Object dataObject = ((CareTaskObservable) observable).getDataObject();
        dismissProgressDialog();
        careTaskModelList = (List<Object>) dataObject;
        Comparator<Object> comparator = new Comparator<Object>() {

            public int compare(Object object1, Object object2) {
                if (!(object1 instanceof CareTaskModel) || !(object2 instanceof CareTaskModel)) {
                    return 0;
                }
                SimpleDateFormat formatter;
                Date date1;
                Date date2;
                formatter = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                try {
                    date1 = formatter.parse(((CareTaskModel) object1).getCareTaskStartDate());
                    date2 = formatter.parse(((CareTaskModel) object2).getCareTaskStartDate());
                    if (date1.before(date2)) {
                        return 1;
                    } else if (date1.after(date2)) {
                        return -1;
                    } else {
                        return 0;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        };
        Collections.sort(careTaskModelList, comparator);

        if (careTaskModelList != null && careTaskModelList.size() > 0 && !(careTaskModelList.get(0) instanceof CareHeaderModel)) {
            CareHeaderModel careHeaderModel = new CareHeaderModel(careTaskModelList.size());
            careTaskModelList.add(0, careHeaderModel);
        }
        careTasksAdapter.setCareTaskLis((List<Object>) dataObject);
        careTasksAdapter.notifyItemsView();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    boolean isRefresh = bundle.getBoolean("isRefresh");
                    if (isRefresh) {
                        onRefresh();
                    }
                }
                break;
            case REQUEST_QUESTIONNAIRE_FLOW:
                if (resultCode == getActivity().RESULT_OK && data.getExtras().containsKey(CarePlanConstants.SUCESSFULL) && data.getBooleanExtra(CarePlanConstants.SUCESSFULL, false)) {
                    submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
                }
                break;

            case CareTaskActionFragment.OBSERVATION_SELECT:
                if (resultCode == getActivity().RESULT_OK) {
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                            boolean isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                            if (isToRefresh) {
                                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                                    submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
                                }
                            }
                        }
                    }
                }
                break;

            case CareTaskActionFragment.APPOINTMENT_SELECT:
                if (resultCode == getActivity().RESULT_OK) {
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                            boolean isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                            if (isToRefresh) {
                                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                                    submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
                                }
                            }
                        }
                    }
                }
                break;

            case TextConstants.MENU_ITEM_SEARCH:
                if (resultCode == getActivity().RESULT_OK) {
                    //Toast.makeText(getActivity(),data.getStringExtra(CareTaskFilterActivity.search_category_status),Toast.LENGTH_SHORT).show();
                    searchCareTasksFromServer(data.getStringExtra(TextConstants.search_category_status));
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*handling click of filter button results,
        * getting result again in this fragment
        *
        */

        if (item.getItemId() == TextConstants.MENU_ITEM_SEARCH) {
            Intent i = new Intent(getActivity(), CareTaskFilterActivity.class);
            i.putExtra(TextConstants.search_param, TextConstants.search_category_status);
            i.putExtra(TextConstants.param_value, searchStatus);
            startActivityForResult(i, TextConstants.MENU_ITEM_SEARCH);
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    public void searchCareTasksFromServer(String searchStatus) {
        isFromSearchScreen = true;
        this.searchStatus = searchStatus;
        showProgressDialog(getActivity().getResources().getString(R.string.please_wait));
        setRefreshDataFromApi();
    }


    public String getSearchStatus() {
        return searchStatus;
    }
}
