package careplan.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.models.ObservationList;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.activity.EnterObservationActivity;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import careplan.activity.CareTaskAppointment;
import careplan.activity.CareTaskQuestionnaryActivity;
import careplan.activity.ProcessTaskActivity;
import careplan.constants.CarePlanConstants;
import careplan.enums.CareTaskActionEnum;
import careplan.enums.CareTaskTypeEnum;
import careplan.enums.CareTaskVitalName;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;
import careplan.request.CareTaskEdit;
import careplan.request.CareTaskSubmit;
import careplan.request.CareTaskUpdate;

/**
 * Created by Kailash Khurana on 10/14/2016.
 */

public class CareTaskActionFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    /*    [Variables to handle DatePicker Dialog methods]     */
    private DatePickerDialog datePickerDialog;
    private CareTaskButtons task_careTaskButtons;
    private List<CareTaskButtons> task_listButtons;
    private long id_task;
    /*    [Variables to handle DatePicker Dialog methods]     */

    public static final int APPOINTMENT_SELECT = 1112;
    public static final int OBSERVATION_SELECT = 1111;
    public static final int REQUEST_QUESTIONNAIRE_FLOW = 1001;
    public ProgressDialog pdialog;
    public long mTransactionId;
    public CareTaskButtons careTaskButtons;
    public List<CareTaskButtons> listButtons;
    public long careTaskId;

    public void showProgressDialog(String message) {
        if (pdialog == null) {
            pdialog = new ProgressDialog(getActivity());
        }
        pdialog.setMessage(message);
        pdialog.setCancelable(false);
        if (!pdialog.isShowing()) {
            pdialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (pdialog != null && pdialog.isShowing()) {
            pdialog.dismiss();
        }
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    public void submitTask(String careTaskButtonsTitle, List<CareTaskButtons> listButtons, long id, String stringInput) {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            showProgressDialog(getActivity().getResources().getString(R.string.progress_updating));
            try {
                ThreadManager.getDefaultExecutorService().submit(new CareTaskSubmit(mTransactionId, getActivity(), getPayLoad(careTaskButtonsTitle, listButtons, id, stringInput), id));
            } catch (JSONException e) {
            }
        }
    }

    public void editTask(CareTaskButtons listButton, long id) {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            showProgressDialog(getActivity().getResources().getString(R.string.progress_updating));
            try {
                ThreadManager.getDefaultExecutorService().submit(new CareTaskEdit(mTransactionId, getActivity(), getPayLoadEdit(listButton, id), id));
            } catch (JSONException e) {
            }
        }
    }

    public void updateTask(CareTaskButtons listButton, long id) {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            showProgressDialog(getActivity().getResources().getString(R.string.progress_updating));
            try {
                ThreadManager.getDefaultExecutorService().submit(new CareTaskUpdate(mTransactionId, getActivity(), getPayLoadUpdate(listButton, id), id));
            } catch (JSONException e) {
            }
        }
    }

    public void handleAction(Context context, CareTaskModel careTaskModel, CareTaskButtons careTaskButtons, List<CareTaskButtons> listButtons, long careTaskId, String questionnaireUrl) {


        if(BuildConfig.isPatientApp) {

            if (questionnaireUrl != null && !questionnaireUrl.equals(getActivity().getResources().getString(R.string.txt_null)) && questionnaireUrl.contains("/")) {

                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;

                String newUrl = questionnaireUrl.replace("patResponse", "fillSelfResponse") + "/" + SharedPref.getAccessToken();
                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, newUrl)
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_fill_questionare)), TextConstants.INTENT_CONSTANT);

            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskModel.getCategory().equals("RecordVital")) {

                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;

                ObservationList observationList = null;
                if (careTaskModel.getVitalName().equals(CareTaskVitalName.BMI.getFlowValue())) {
                    observationList = new ObservationList(R.string.fa_observation_bmi, VariableConstants.BMI, R.color.fill3);
                    ((Activity) context).startActivityForResult(
                            new Intent(context, EnterObservationActivity.class).putExtra(VariableConstants.OBSERVATION_SELECTED,
                                    observationList.getObservationResource()).putExtra(VariableConstants.OBSERVATION_TEXT, observationList.getObservationText()),
                            OBSERVATION_SELECT);
                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.BLOODPRESSURE.getFlowValue())) {
                    observationList = new ObservationList(R.string.fa_observation_bp, VariableConstants.Blood_Pressure, R.color.fill1);
                    ((Activity) context).startActivityForResult(
                            new Intent(context, EnterObservationActivity.class).putExtra(VariableConstants.OBSERVATION_SELECTED,
                                    observationList.getObservationResource()).putExtra(VariableConstants.OBSERVATION_TEXT, observationList.getObservationText()),
                            OBSERVATION_SELECT);
                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.GLUCOSE.getFlowValue())) {
                    observationList = new ObservationList(R.string.fa_observation_glucose, VariableConstants.Glucose, R.color.fill2);
                    ((Activity) context).startActivityForResult(
                            new Intent(context, EnterObservationActivity.class).putExtra(VariableConstants.OBSERVATION_SELECTED,
                                    observationList.getObservationResource()).putExtra(VariableConstants.OBSERVATION_TEXT, observationList.getObservationText()),
                            OBSERVATION_SELECT);
                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.TEMPERATURE.getFlowValue())) {

                    getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                            .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                            .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.HEIGHT.getFlowValue())) {

                    getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                            .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                            .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.WEIGHT.getFlowValue())) {

                    getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                            .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                            .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.RESPIRATIONRATE.getFlowValue())) {

                    getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                            .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                            .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

                } else if (careTaskModel.getVitalName().equals(CareTaskVitalName.HEARTRATE.getFlowValue())) {

                    getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                            .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                            .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

                }


            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.MEDICATION).getFlowValue())) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(VariableConstants.DEEP_LINK_FOR_MEDICATION_CLICKED, true);
                ((BaseActivity) getActivity()).openFragmentRecord(bundle);

            } else if (careTaskButtons != null
                    && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.VITAL).getFlowValue())) {

                Bundle bundle = new Bundle();
                ((BaseActivity) getActivity()).openFragmentVital(bundle);

            } else if (careTaskButtons != null
                    && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskModel.getCategory().equals("BookAppointment")) {

                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;
                ((Activity) context).startActivityForResult(new Intent(getActivity(), CareTaskAppointment.class), APPOINTMENT_SELECT);

            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equalsIgnoreCase((CareTaskActionEnum.EDIT).getFlowValue())) {

                editTask(careTaskButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equalsIgnoreCase((CareTaskActionEnum.UPDATE).getFlowValue())) {
                updateTask(careTaskButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.STRING).getFlowValue())) {
                showInputDialog(careTaskButtons, listButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.DATE).getFlowValue())) {
                showDateDialog(careTaskButtons, listButtons, careTaskId);
            } else {
            /*[   For all non handle tasks just open tasks in  webview with taskid    ]*/
                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;
                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_task)), TextConstants.INTENT_CONSTANT);

                //   submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
            }
        }else if(BuildConfig.isPhysicianApp){

            if (questionnaireUrl != null && !questionnaireUrl.equals(getActivity().getResources().getString(R.string.txt_null)) && questionnaireUrl.contains("/")) {
                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;

                String newUrl = questionnaireUrl.replace("patResponse", "fillSelfResponse") + "/" + SharedPref.getAccessToken();
                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, newUrl)
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_fill_questionare)), TextConstants.INTENT_CONSTANT);

            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskModel.getCategory().equals("RecordVital")) {

                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;

                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_record_vital)), TextConstants.INTENT_CONSTANT);

            }else if (careTaskButtons != null
                    && careTaskButtons.getButtonAction().equals((CareTaskActionEnum.DEEPLINK).getFlowValue())
                    && careTaskModel.getCategory().equals("BookAppointment")) {

                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;
                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_book_appointment)), TextConstants.INTENT_CONSTANT);

            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equalsIgnoreCase((CareTaskActionEnum.EDIT).getFlowValue())) {

                editTask(careTaskButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonAction().equalsIgnoreCase((CareTaskActionEnum.UPDATE).getFlowValue())) {
                updateTask(careTaskButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.STRING).getFlowValue())) {
                showInputDialog(careTaskButtons, listButtons, careTaskId);
            } else if (careTaskButtons != null && careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.DATE).getFlowValue())) {
                showDateDialog(careTaskButtons, listButtons, careTaskId);
            } else {
            /*[   For all non handle tasks just open tasks in  webview with taskid    ]*/
                this.careTaskButtons = careTaskButtons;
                this.listButtons = listButtons;
                this.careTaskId = careTaskId;
                getActivity().startActivityForResult(new Intent(getActivity(), CareTaskQuestionnaryActivity.class)
                        .putExtra(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL, APIManager.getWebConnectBaseUrl() + "/" + careTaskModel.getTaskId() + "/" + SharedPref.getAccessToken())
                        .putExtra(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE, context.getResources().getString(R.string.title_task)), TextConstants.INTENT_CONSTANT);

                //   submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
            }
        }

    }

    private String getPayLoad(String itemClicked, List<CareTaskButtons> listButtons, long id, String stringInput) throws JSONException {
        JSONObject fetchCarePlanJsonObject = new JSONObject();
        JSONObject taskParamsObj = new JSONObject();
        for (int i = 0; i < listButtons.size(); i++) {
            CareTaskButtons careTaskButtons = listButtons.get(i);
            if (careTaskButtons.getButtonTitle().equalsIgnoreCase(itemClicked)) {
                if (careTaskButtons.getButtonValue().equalsIgnoreCase("false")) {
                    taskParamsObj.put(careTaskButtons.getButtonId(), "true");
                } else if (careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.STRING).getFlowValue())) {
                    taskParamsObj.put(careTaskButtons.getButtonId(), stringInput);
                } else if (careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.DATE).getFlowValue())) {
                    taskParamsObj.put(careTaskButtons.getButtonId(), stringInput);
                    JSONObject jsonSubmitData = new JSONObject();
                    jsonSubmitData.put(careTaskButtons.getButtonId(), stringInput);
                    fetchCarePlanJsonObject.put("userSubmittedData", jsonSubmitData);
                } else {
                    taskParamsObj.put(careTaskButtons.getButtonId(), careTaskButtons.getButtonValue());
                }
            } else if (careTaskButtons.getButtonValue().equalsIgnoreCase("true") || careTaskButtons.getButtonValue().equalsIgnoreCase("false")) {
                taskParamsObj.put(careTaskButtons.getButtonId(), "false");
            } else if (careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.STRING).getFlowValue())) {
                taskParamsObj.put(careTaskButtons.getButtonId(), "");
            } else if (careTaskButtons.getButtonValue().equalsIgnoreCase((CareTaskTypeEnum.DATE).getFlowValue())) {
                taskParamsObj.put(careTaskButtons.getButtonId(), null);
            }
        }
        JSONObject taskObj = new JSONObject();
        taskObj.put("id", Long.toString(id));
        fetchCarePlanJsonObject.put("taskParams", taskParamsObj);
        fetchCarePlanJsonObject.put("task", taskObj);
        return fetchCarePlanJsonObject.toString();
    }

    private String getPayLoadEdit(CareTaskButtons listButton, long id) throws JSONException {
        JSONObject fetchCarePlanJsonObject = new JSONObject();
        JSONObject taskParamsObj = new JSONObject();
        taskParamsObj.put("taskStatus", listButton.getButtonValue());
        fetchCarePlanJsonObject.put("taskId", Long.toString(id));
        fetchCarePlanJsonObject.put("taskVariables", taskParamsObj);
        return fetchCarePlanJsonObject.toString();
    }

    private String getPayLoadUpdate(CareTaskButtons listButton, long id) throws JSONException {
        JSONObject fetchCarePlanJsonObject = new JSONObject();
        JSONObject taskParamsObj = new JSONObject();
        taskParamsObj.put(listButton.getButtonId(), listButton.getButtonValue());
        fetchCarePlanJsonObject.put("taskId", Long.toString(id));
        fetchCarePlanJsonObject.put("processVariables", taskParamsObj);
        return fetchCarePlanJsonObject.toString();
    }

    public void showInputDialog(final CareTaskButtons careTaskButtons, final List<CareTaskButtons> listButtons, final long id) {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View v = layoutInflater.inflate(R.layout.dialog_care_task_input, null, false);
        alertDialog.setView(v);
        final CustomFontEditTextView etMessage = (CustomFontEditTextView) v.findViewById(R.id.etStatusMessage);

        v.findViewById(R.id.continue_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (etMessage.getText().toString().trim().equals("")) {
                    DialogUtils.showAlertDialogCommon(getContext(), null, getString(R.string.reason_empty_message), getString(R.string.btn_ok), null, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case Dialog.BUTTON_POSITIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
                    return;
                }
                submitTask(careTaskButtons.getButtonTitle(), listButtons, id, etMessage.getText().toString());
            }
        });
        v.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void processTaskFile(final CareTaskButtons careTaskButtons, final List<CareTaskButtons> listButtons, final long id) {
        getActivity().startActivity(new Intent(getActivity(), ProcessTaskActivity.class));
    }

    public void showDateDialog(final CareTaskButtons careTaskButtons, final List<CareTaskButtons> listButtons, final long id) {
        task_careTaskButtons = careTaskButtons;
        task_listButtons = listButtons;
        id_task = id;

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if (datePicker.isShown()) {
            String date = DateTimeUtil.calculateDate(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
            submitTask(task_careTaskButtons.getButtonTitle(), task_listButtons, id_task, date);
        }
    }
}
