package careplan.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.fragments.UploadsFragment;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import careplan.activity.CareTaskActivity;
import careplan.adapter.CarePlanRecyclerAdadpter;
import careplan.background.CarePlanFetch;
import careplan.constants.CarePlanConstants;
import careplan.model.CareHeaderModel;
import careplan.model.CarePlanModel;
import careplan.observable.CarePlanObservable;
import careplan.request.CarePlanRequest;

/**
 * Created by xmb2nc on 31-08-2016.
 */
public class CarePlanFragment extends Fragment implements CarePlanRecyclerAdadpter.OnItemClickedListener, Observer,
        SwipeRefreshLayout.OnRefreshListener {
    private static final int FILE_DOWNLOAD_INTENT = 100;
    private View carePlanFragmentView;
    private RecyclerView carePlanRecylerView;
    private CarePlanRecyclerAdadpter carePlansAdapter;
    private long mTransactionId;
    private List<Object> carePlanModelList;
    private ProgressDialog pdialog;
    private SwipeRefreshLayout carePlanSwipeRefreshLayout;

    private RelativeLayout rlCarePlanEmptyView;
    private LinearLayoutManager linearLayoutManager;
    private String documentId;
    private long patientId;

    public CarePlanFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        carePlanFragmentView = inflater.inflate(R.layout.care_plan_fragment, container, false);
        carePlanRecylerView = (RecyclerView) carePlanFragmentView.findViewById(R.id.rv_care_plan);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        carePlanRecylerView.setLayoutManager(mLayoutManager);
        ((BaseActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.care_plan));
        carePlanRecylerView.setItemAnimator(new DefaultItemAnimator());
        carePlansAdapter = new CarePlanRecyclerAdadpter(getActivity(), this);
        carePlanRecylerView.setAdapter(carePlansAdapter);

        if (getArguments() != null && getArguments().containsKey(VariableConstants.PATIENT)) {
            patientId = getArguments().getLong(VariableConstants.PATIENT);
        } else {
            patientId = SettingManager.getInstance().getUserMO().getPatientId();
        }

        carePlanSwipeRefreshLayout = (SwipeRefreshLayout) carePlanFragmentView.findViewById(R.id.care_plan_refresh_layout);
        carePlanSwipeRefreshLayout.setColorSchemeResources(R.color.care_plan_status_button_color);
        carePlanSwipeRefreshLayout.setOnRefreshListener(this);
        rlCarePlanEmptyView = (RelativeLayout) carePlanFragmentView.findViewById(R.id.rl_care_plan_empty_view);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        fetchCarePlans();

        carePlanRecylerView.setOnScrollListener(new EndLessScrollingListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getChildAt(0).getTop() == 0) {
                    carePlanSwipeRefreshLayout.setEnabled(true);
                } else {
                    carePlanSwipeRefreshLayout.setEnabled(false);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        setHasOptionsMenu(true);

        return carePlanFragmentView;
    }

    @Override
    public void onResume() {
        CarePlanObservable.getInstance().addObserver(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        CarePlanObservable.getInstance().deleteObserver(this);
        super.onPause();
    }

    public void fetchCarePlans() {
        pdialog = new ProgressDialog(getActivity());
        pdialog.setMessage(getActivity().getResources().getString(R.string.progress_dialog));
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.show();
        if (BuildConfig.isPatientApp) {
            new CarePlanFetch(getActivity(), patientId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                ThreadManager.getDefaultExecutorService().submit(new CarePlanRequest(System.currentTimeMillis(), getActivity(), false, patientId));
            }else{
                pdialog.dismiss();
            }
        }
    }

    @Override
    public void itemClicked(View view, int position) {
        switch (view.getId()) {
            case R.id.tv_care_plan_view_tasks:
                Bundle bundle = new Bundle();
                bundle.putLong(CarePlanConstants.CASE_LOAD_ID, ((CarePlanModel) carePlanModelList.get(position)).getCaseLoadId());
                bundle.putString(CarePlanConstants.CASE_LOAD_TITLE, ((CarePlanModel) carePlanModelList.get(position)).getCarePlanTitle());
                bundle.putBoolean(CarePlanConstants.VIEW_CARE_PLAN_SCREEN, true);
                Intent intent = new Intent(getActivity(), CareTaskActivity.class);
                intent.putExtra("bundle", bundle);
                startActivity(intent);
                break;
//            case R.id.tv_care_plan_plan_details:
//                Toast.makeText(getActivity(), "Not implemented " + position, Toast.LENGTH_LONG).show();
//                break;
            case R.id.btnViewDoc:
                if (!(checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_storage), FILE_DOWNLOAD_INTENT))) {
                    return;
                }
                documentId = ((CarePlanModel) carePlanModelList.get(position)).getDocumentId();
                fileDownload(documentId);
                break;
        }
    }

    public boolean checkPermission(final String permissionType, String message, final int PERMISSION_REQUEST_CODE) {
        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted(permissions[0], requestCode);
        } else {
            onPermissionNotGranted(permissions[0]);
        }

    }

    public void onPermissionGranted(String permission, int requestCode) {
        switch (requestCode) {
            case UploadsFragment.CAMERA_INTENT:
                fileDownload(documentId);
                break;
        }

    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.uninterruted_service), Toast.LENGTH_SHORT).show();
    }

    public long getTransactionId() {
        mTransactionId = System.currentTimeMillis();
        return mTransactionId;
    }

    private void fileDownload(String documentRefId) {
        if (Utils.isNetworkAvailable(getActivity())) {
//            new DownloadRecordFileRequest(this, documentRefId, fileType, MphRxUrl.getReportFile(), "diagnosticReportId").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void update(final Observable observable, Object data) {
        if (observable == null || !(observable instanceof CarePlanObservable)) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pdialog != null && pdialog.isShowing()) {
                    pdialog.dismiss();
                }

                if (carePlanSwipeRefreshLayout.isRefreshing()) {
                    carePlanSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        final Object dataObject = ((CarePlanObservable) observable).getDataObject();
        if (((CarePlanObservable) observable).isSucessfullyExecuted()) {
            if (dataObject == null || ((List) dataObject).size() == 0) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rlCarePlanEmptyView.setVisibility(View.VISIBLE);
                    }
                });
                return;

            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rlCarePlanEmptyView.setVisibility(View.GONE);
                }
            });
            carePlanModelList = new ArrayList<>();
            List<Object> carePlanModel = (List<Object>) dataObject;

            Comparator<Object> comparator = new Comparator<Object>() {

                public int compare(Object object1, Object object2) {
                    if (!(object1 instanceof CarePlanModel) || !(object2 instanceof CarePlanModel)) {
                        return 0;
                    }
                    SimpleDateFormat formatter;
                    Date date1;
                    Date date2;
                    formatter = new SimpleDateFormat(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
                    try {
                        date1 = formatter.parse(((CarePlanModel) object1).getStartDate());
                        date2 = formatter.parse(((CarePlanModel) object2).getStartDate());
                        if (date1.before(date2)) {
                            return 1;
                        } else if (date1.after(date2)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            };
            Collections.sort(carePlanModel, comparator);

            long totalActiveCarePlansCount = SharedPref.getTotalActiveCarePlansCount();
            if (carePlanModel != null && carePlanModel.size() > 0 && !(carePlanModel.get(0) instanceof CareHeaderModel)) {
                CareHeaderModel careHeaderModel = new CareHeaderModel(totalActiveCarePlansCount);
                carePlanModel.add(0, careHeaderModel);
            }
            carePlanModelList.addAll(carePlanModel);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    carePlansAdapter.setCarePlanModelList(carePlanModelList);
                    carePlansAdapter.notifyDataSetChanged();
                }
            });
        } else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), ((CarePlanObservable) observable).getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    public void onRefresh() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            if (carePlanModelList == null || carePlanModelList.size() < 1) {
                ThreadManager.getDefaultExecutorService().submit(new CarePlanRequest(System.currentTimeMillis(),
                        getActivity(), false, patientId));
            } else {
                ThreadManager.getDefaultExecutorService().submit(new CarePlanRequest(System.currentTimeMillis(),
                        getActivity(), true, patientId));
            }
        }
    }

}
