package careplan.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.CustomLinkMovementMethod;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.ItemOffsetDecoration;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import appointment.utils.CommonTasks;
import careplan.activity.CareTaskDetailActivity;
import careplan.adapter.AttachmentRecyclerAdpater;
import careplan.adapter.CareTaskRecyclerAdadpter;
import careplan.adapter.DynamicButtonsRecyclerAdapter;
import careplan.adapter.RelatedLinksAdapter;
import careplan.constants.CarePlanConstants;
import careplan.enums.CareTaskStatusEnum;
import careplan.model.CareTaskAttachemtsModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskDetailModel;
import careplan.model.CareTaskModel;
import careplan.request.CareTaskDetailRequest;
import careplan.response.CareTaskDetailResponse;
import careplan.response.CareTaskEditResponse;
import careplan.response.CareTaskSubmitResponse;

/**
 * Created by Kailash Khurana on 10/14/2016.
 */

public class CareTaskDetailFragment extends CareTaskActionFragment implements View.OnClickListener, DynamicButtonsRecyclerAdapter.OnItemClickedListener {

    private Context mContext;
    private View careTaskDetailFragmentView;
    private CustomFontTextView tvCareTaskTitle;
    private CustomFontTextView tvCarePathName;
    private CustomFontTextView tvAssignedTo;
    private CustomFontTextView tvPriority;
    private CustomFontTextView tvCompletedBy;
    private CustomFontTextView tvCareTaskStatus;
    private CustomFontTextView tvReportedBy;
    private CustomFontTextView tvDescription;

    private RelativeLayout lyt_care_path_name, lyt_care_task_status, lyt_complete_by, lyt_assigned_to, lyt_priority, lyt_reported_by, lyt_related_link;
    private RelativeLayout mLyt_progress;
    private ProgressBar mProgressBar;
    private CustomFontTextView mTv_loading_or_error;

    private NestedScrollView mScrollView;

    private RecyclerView mRv_RelatedLink;
    private RelatedLinksAdapter mRelatedLinkAdapter;
    private List<String> mRelatedLinkList = new ArrayList<String>();


    private RecyclerView mRv_Attachments;
    private AttachmentRecyclerAdpater mAttachmentRecyclerAdpater;
    private List<CareTaskAttachemtsModel> mAttachmentList = new ArrayList<CareTaskAttachemtsModel>();


    private RecyclerView mRv_Dynamic_Buttons;
    private DynamicButtonsRecyclerAdapter mDynamicButtonsRecyclerAdapter;
    private List<CareTaskButtons> mDynamicButtonsList = new ArrayList<CareTaskButtons>();

    private long careTaskId;

    private boolean isViewAttachment;
    private CardView mCard_view_attachment;
    private boolean isFromCarePlanScreen;

    private SpannableString text;
    private String content;
    private String appendContent;
    private boolean isExpanded;
    private int MAX_LENGTH = 115;

    private int position;
    private String questionnaireUrl;
    private CareTaskModel careTaskModel;
    private String status, more, less;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        careTaskDetailFragmentView = inflater.inflate(R.layout.care_task_detail_fragment, container, false);
        mContext = getActivity();

        careTaskModel = getArguments().getParcelable(CarePlanConstants.CARE_TASK_OBJECT);
        careTaskId = careTaskModel.getTaskId();
        questionnaireUrl = getArguments().getString(CarePlanConstants.CARE_TASK_QUESTIONNARIE_URL);
        status = getArguments().getString(CarePlanConstants.CARE_TASK_FILTER_STATUS);

        findView();

        isViewAttachment = getArguments().getBoolean(CarePlanConstants.CARE_TASK_VIEW_ATTACHMENT);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(CarePlanConstants.VIEW_CARE_PLAN_SCREEN)) {
            isFromCarePlanScreen = bundle.getBoolean(CarePlanConstants.VIEW_CARE_PLAN_SCREEN);
        }

        try {
            ThreadManager.getDefaultExecutorService().submit(new CareTaskDetailRequest(getTransactionId(), getActivity(), careTaskId, status));
        } catch (Exception e) {
        }

        return careTaskDetailFragmentView;
    }

    private void findView() {
        tvCareTaskTitle = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_care_task_title);
        tvCarePathName = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_care_path_name);
        tvAssignedTo = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_assigned_to);
        tvPriority = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_priority);
        tvCareTaskStatus = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_care_task_status);
        tvCompletedBy = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_complete_by);
        tvReportedBy = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_reported_by);
        tvDescription = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_description);

        mCard_view_attachment = (CardView) careTaskDetailFragmentView.findViewById(R.id.card_view_attachment);

        mLyt_progress = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_progress);
        mLyt_progress.setVisibility(View.VISIBLE);
        mTv_loading_or_error = (CustomFontTextView) careTaskDetailFragmentView.findViewById(R.id.tv_loading_or_error);
        mProgressBar = (ProgressBar) careTaskDetailFragmentView.findViewById(R.id.progressBar);
        mProgressBar.getIndeterminateDrawable().setColorFilter(
                mContext.getResources().getColor(R.color.action_bar_bg),
                android.graphics.PorterDuff.Mode.SRC_IN);

        mScrollView = (NestedScrollView) careTaskDetailFragmentView.findViewById(R.id.scrollView);
        mScrollView.setVisibility(View.GONE);

        lyt_care_path_name = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_care_path_name);
        lyt_care_task_status = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_care_task_status);
        lyt_complete_by = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_complete_by);
        lyt_assigned_to = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_assigned_to);
        lyt_priority = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_priority);
        lyt_reported_by = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_reported_by);
        lyt_related_link = (RelativeLayout) careTaskDetailFragmentView.findViewById(R.id.lyt_related_link);


        mRv_RelatedLink = (RecyclerView) careTaskDetailFragmentView.findViewById(R.id.rv_RelatedLink);
        mRv_Attachments = (RecyclerView) careTaskDetailFragmentView.findViewById(R.id.rv_Attachments);
        mRv_Dynamic_Buttons = (RecyclerView) careTaskDetailFragmentView.findViewById(R.id.rv_Dynamic_Buttons);

        mRv_RelatedLink.setNestedScrollingEnabled(false);
        mRv_Attachments.setNestedScrollingEnabled(false);
        mRv_Dynamic_Buttons.setNestedScrollingEnabled(false);

        mRv_RelatedLink.setLayoutManager(new LinearLayoutManager(mContext));
        mRelatedLinkAdapter = new RelatedLinksAdapter(mContext, mRelatedLinkList);
        mRv_RelatedLink.setAdapter(mRelatedLinkAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, TextConstants.NUM_OF_COLS);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.item_offset);
        mRv_Dynamic_Buttons.setLayoutManager(gridLayoutManager);
        //  mRv_Dynamic_Buttons.addItemDecoration(itemDecoration);
        mDynamicButtonsRecyclerAdapter = new DynamicButtonsRecyclerAdapter(mContext, mDynamicButtonsList, this, careTaskId, status);
        mRv_Dynamic_Buttons.setAdapter(mDynamicButtonsRecyclerAdapter);

        mRv_Attachments.setLayoutManager(new LinearLayoutManager(mContext));
        mAttachmentRecyclerAdpater = new AttachmentRecyclerAdpater(mContext, mAttachmentList, this, careTaskId);
        mRv_Attachments.setAdapter(mAttachmentRecyclerAdpater);


    }

    @Subscribe
    public void onCareTaskDetailResponse(CareTaskDetailResponse careTaskDetailResponse) {
        if (mTransactionId != careTaskDetailResponse.getTransactionId()) {
            return;
        }

        final CareTaskDetailModel careTaskDetailModel = careTaskDetailResponse.getCareTaskDetailModel();
        if (careTaskDetailResponse.isSuccess() && careTaskDetailModel != null) {
            mLyt_progress.setVisibility(View.GONE);
            mScrollView.setVisibility(View.VISIBLE);

            tvCareTaskTitle.setText(careTaskDetailModel.getCareTaskName());

            if (careTaskDetailModel.getCarePathName() == null || careTaskDetailModel.getCarePathName().equals("")) {
                lyt_care_path_name.setVisibility(View.GONE);
            } else {
                lyt_care_path_name.setVisibility(View.VISIBLE);
                tvCarePathName.setText(careTaskDetailModel.getCarePathName());
            }

            if (careTaskDetailModel.getCompleteBy() == null || careTaskDetailModel.getCompleteBy().equals("") || careTaskDetailModel.getCompleteBy().equals(mContext.getResources().getString(R.string.txt_null))) {
                lyt_complete_by.setVisibility(View.GONE);
            } else {
                lyt_complete_by.setVisibility(View.VISIBLE);

                String sourceDate = DateTimeUtil.convertSourceDestinationDate(careTaskDetailModel.getCompleteBy(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithoutTime);
                String sourceTime = DateTimeUtil.convertSourceDestinationDate(careTaskDetailModel.getCompleteBy(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, "ss");
                String date = sourceTime.equals("59") ? sourceDate : sourceDate + ", " + DateTimeUtil.convertSourceDestinationDate(careTaskDetailModel.getCompleteBy(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.sourceTimeFormat);
                tvCompletedBy.setText(String.format(mContext.getResources().getString(R.string.care_plan_or_task_completed_on), date));
            }

            if (careTaskModel.getCareTaskStatus() != null && careTaskModel.getCareTaskStatus().equals("")) {
                tvCareTaskStatus.setText(getResources().getString(R.string.care_task_btn_pending));
            } else {
                CareTaskStatusEnum careTaskStatusEnum = CareTaskStatusEnum.getCareTaskStatusEnumLinkedHashMap().get(careTaskModel.getCareTaskStatus());
                if (careTaskStatusEnum != null && careTaskStatusEnum.getValue() != 0) {
                    tvCareTaskStatus.setText(getActivity().getResources().getString(careTaskStatusEnum.getValue()));
                } else {
                    tvCareTaskStatus.setText(Utils.capitalizeFirstLetter(careTaskModel.getCareTaskStatus()));
                }
            }


            String content = careTaskDetailModel.getDescription() != null ? careTaskDetailModel.getDescription().toString().trim() : "";

            if (!content.equals("") && !content.equals("null")) {
                content = content.replaceAll("<br>", "\\\n");
                content = content.replaceAll("<BR>", "\\\n");
                tvDescription.setVisibility(View.VISIBLE);
                if (content.length() < MAX_LENGTH) {
                    tvDescription.setText(content);
                    isExpanded = false;
                    // styleAndAnimate(viewHolder.tv_instructions);
                } else {
                    more = MyApplication.getAppContext().getResources().getString(R.string.more);
                    less = MyApplication.getAppContext().getResources().getString(R.string.less);
                    appendContent = more;
                    isExpanded = false;
                    styleAndAnimate(tvDescription, content, mContext);
                }
            } else {
                tvDescription.setVisibility(View.INVISIBLE);
            }
            if (careTaskDetailModel.getAssignedTo() != null && !careTaskDetailModel.getAssignedTo().equals(null) && !careTaskDetailModel.getAssignedTo().equals(getActivity().getResources().getString(R.string.txt_null))) {
                lyt_assigned_to.setVisibility(View.VISIBLE);
                tvAssignedTo.setText(careTaskDetailModel.getAssignedTo() != null ? careTaskDetailModel.getAssignedTo() : "");
            } else {
                lyt_assigned_to.setVisibility(View.GONE);
            }

            if (careTaskDetailModel.getPriority() != null && !careTaskDetailModel.getPriority().equals(null) && !careTaskDetailModel.getPriority().equals(getActivity().getResources().getString(R.string.txt_null))) {
                lyt_priority.setVisibility(View.VISIBLE);
                tvPriority.setText(careTaskDetailModel.getPriority());
            } else {
                lyt_priority.setVisibility(View.GONE);
            }


            if (careTaskDetailModel.getReportedBy() != null && !careTaskDetailModel.getReportedBy().equals(null) && !careTaskDetailModel.getReportedBy().equals(getActivity().getResources().getString(R.string.txt_null))) {
                tvReportedBy.setText(careTaskDetailModel.getReportedBy() != null && !careTaskDetailModel.getReportedBy().equals("") ? careTaskDetailModel.getReportedBy() : mContext.getResources().getString(R.string.system));
            } else {
                tvReportedBy.setText(mContext.getResources().getString(R.string.system));
            }

            mAttachmentList.clear();
            if (careTaskDetailModel.getAttachemtsModelsList() != null && careTaskDetailModel.getAttachemtsModelsList().size() > 0) {
                mAttachmentList.addAll(careTaskDetailModel.getAttachemtsModelsList());
                mAttachmentRecyclerAdpater.callNotifyDataSetChanged();
                mCard_view_attachment.setVisibility(View.VISIBLE);
            } else {
                mCard_view_attachment.setVisibility(View.GONE);
            }

            mDynamicButtonsList.clear();
            if ((status == null || status.equalsIgnoreCase("unfinished")) && (BuildConfig.isPatientApp || (!isFromCarePlanScreen && BuildConfig.isPhysicianApp)) && careTaskDetailModel.getListButtons() != null && careTaskDetailModel.getListButtons().size() > 0) {
                mDynamicButtonsList.addAll(careTaskDetailModel.getListButtons());
                mDynamicButtonsRecyclerAdapter.notifyDataSetChanged();
            }

            mRelatedLinkList.clear();
            if (careTaskDetailModel.getRelatedLinkList() != null && careTaskDetailModel.getRelatedLinkList().size() > 0) {
                lyt_related_link.setVisibility(View.VISIBLE);
                mRelatedLinkList.addAll(careTaskDetailModel.getRelatedLinkList());
                mRelatedLinkAdapter.notifyDataSetChanged();
            } else {
                lyt_related_link.setVisibility(View.GONE);
            }

            if (isViewAttachment) {
                mScrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (careTaskDetailModel.getAttachemtsModelsList() != null && careTaskDetailModel.getAttachemtsModelsList().size() > 0) {
                            mScrollView.smoothScrollTo(0, mCard_view_attachment.getTop());
                        }
                    }
                }, 400);
            }

        } else {
            mLyt_progress.setVisibility(View.GONE);
//            Toast.makeText(getActivity(), careTaskDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();
//            getActivity().finish();

            mScrollView.setVisibility(View.VISIBLE);

            tvCareTaskTitle.setText(careTaskModel.getCareTaskTitle());

            if (careTaskModel.getCarePathwayName() == null || careTaskModel.getCarePathwayName().equals("")) {
                lyt_care_path_name.setVisibility(View.GONE);
            } else {
                lyt_care_path_name.setVisibility(View.VISIBLE);
                tvCarePathName.setText(careTaskModel.getCarePathwayName());
            }

            if (careTaskModel.getGetCareTaskEndDate() == null || careTaskModel.getGetCareTaskEndDate().equals("") || careTaskModel.getGetCareTaskEndDate().equals(getActivity().getResources().getString(R.string.txt_null))) {
                lyt_complete_by.setVisibility(View.GONE);
            } else {
                lyt_complete_by.setVisibility(View.VISIBLE);

                String sourceDate = DateTimeUtil.convertSourceDestinationDate(careTaskModel.getGetCareTaskEndDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithoutTime);
                String sourceTime = DateTimeUtil.convertSourceDestinationDate(careTaskModel.getGetCareTaskEndDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, "ss");
                String date = sourceTime.equals("59") ? sourceDate : sourceDate + ", " + DateTimeUtil.convertSourceDestinationDate(careTaskModel.getGetCareTaskEndDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.sourceTimeFormat);
                tvCompletedBy.setText(String.format(mContext.getResources().getString(R.string.care_plan_or_task_completed_on), date));
            }

            if (careTaskModel.getCareTaskStatus() != null && careTaskModel.getCareTaskStatus().equals("")) {
                tvCareTaskStatus.setText(Utils.capitalizeFirstLetter("Pending"));
            } else if (careTaskModel.getCareTaskStatus().equals("onHold")) {
                tvCareTaskStatus.setText(Utils.capitalizeFirstLetter("On Hold"));
            } else if (careTaskModel.getCareTaskStatus().equals("cancel")) {
                tvCareTaskStatus.setText(Utils.capitalizeFirstLetter("Cancelled"));
            } else {
                tvCareTaskStatus.setText(Utils.capitalizeFirstLetter(careTaskModel.getCareTaskStatus()));
            }

            String content = careTaskModel.getCareTaskDescription() != null ? careTaskModel.getCareTaskDescription().toString().trim() : "";
            if (!content.equals("") && !content.equals(getActivity().getResources().getString(R.string.txt_null))) {
                tvDescription.setVisibility(View.VISIBLE);
                if (content.length() < MAX_LENGTH) {
                    tvDescription.setText(content);
                    isExpanded = false;
                    // styleAndAnimate(viewHolder.tv_instructions);
                } else {
                    appendContent = less;
                    isExpanded = false;
                    styleAndAnimate(tvDescription, content, mContext);
                }
            } else {
                tvDescription.setVisibility(View.INVISIBLE);
            }
            lyt_assigned_to.setVisibility(View.GONE);
            lyt_priority.setVisibility(View.GONE);

            /*if (careTaskModel.getAssigneeName() != null && !careTaskModel.getAssigneeName().equals(null) && !careTaskModel.getAssigneeName().equals("null")) {
                tvReportedBy.setText(careTaskModel.getAssigneeName() != null && !careTaskModel.getAssigneeName().equals("") ? careTaskModel.getAssigneeName() : mContext.getResources().getString(R.string.system));
            } else {
                tvReportedBy.setText(mContext.getResources().getString(R.string.system));
            }*/
            lyt_reported_by.setVisibility(View.GONE);

            if (careTaskModel.getListAttachment() != null && careTaskModel.getListAttachment().size() > 0) {
                mAttachmentList.addAll(careTaskModel.getListAttachment());
                mAttachmentRecyclerAdpater.callNotifyDataSetChanged();
                mCard_view_attachment.setVisibility(View.VISIBLE);
            } else {
                mCard_view_attachment.setVisibility(View.GONE);
            }


            mAttachmentList.clear();
            if (careTaskModel.isToShowViewDownload()) {
                mAttachmentList.addAll(careTaskModel.getListAttachment());
                mAttachmentRecyclerAdpater.callNotifyDataSetChanged();
                mCard_view_attachment.setVisibility(View.VISIBLE);
            } else {
                mCard_view_attachment.setVisibility(View.GONE);
            }

            mDynamicButtonsList.clear();
            if ((status == null || status.equalsIgnoreCase("unfinished")) && (BuildConfig.isPatientApp || (!isFromCarePlanScreen && BuildConfig.isPhysicianApp)) && careTaskModel.getListButtons() != null && careTaskModel.getListButtons().size() > 0) {
                mDynamicButtonsList.addAll(careTaskModel.getListButtons());
                mDynamicButtonsRecyclerAdapter.notifyDataSetChanged();
            }

            mRelatedLinkList.clear();
//            if (careTaskDetailModel.getRelatedLinkList() != null && careTaskDetailModel.getRelatedLinkList().size() > 0) {
//                lyt_related_link.setVisibility(View.VISIBLE);
//                mRelatedLinkList.addAll(careTaskDetailModel.getRelatedLinkList());
//                mRelatedLinkAdapter.notifyDataSetChanged();
//            } else {
            lyt_related_link.setVisibility(View.GONE);
//            }

            if (isViewAttachment) {
                mScrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (careTaskModel.getListAttachment() != null && careTaskModel.getListAttachment().size() > 0) {
                            mScrollView.smoothScrollTo(0, mCard_view_attachment.getTop());
                        }
                    }
                }, 400);
            }
        }

    }

    private View getCommentView(String commentText) {
        return null;
    }

    private View getAttachmentView(CareTaskAttachemtsModel careTaskAttachemtsModel) {
        return null;
    }


    @Override
    public void onResume() {
        try {
            BusProvider.getInstance().register(this);
        } catch (Exception e) {
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BusProvider.getInstance().unregister(this);
            }
        });
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void itemClicked(View view, int position) {
        this.position = position;
        if (mDynamicButtonsList != null && mDynamicButtonsList.size() > 0) {
            handleAction(getActivity(), careTaskModel, mDynamicButtonsList.get(position), mDynamicButtonsList, careTaskId, questionnaireUrl);
        } else {
            handleAction(getActivity(), careTaskModel, null, null, careTaskId, questionnaireUrl);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == getActivity().RESULT_OK && data != null && requestCode == TextConstants.INTENT_CONSTANT) {
            Bundle bundle = data.getExtras();
            boolean isRefresh = bundle.getBoolean("isRefresh");
            if (isRefresh) {
                Intent intent = new Intent();
                intent.putExtra("isRefresh", true);
                ((CareTaskDetailActivity) mContext).setResult(Activity.RESULT_OK, intent);
                ((CareTaskDetailActivity) mContext).finish();
            }
        } else if (resultCode == getActivity().RESULT_OK && data != null && requestCode == CareTaskActionFragment.REQUEST_QUESTIONNAIRE_FLOW) {
            if (data.getExtras().containsKey(CarePlanConstants.SUCESSFULL) && data.getBooleanExtra(CarePlanConstants.SUCESSFULL, false)) {
                submitTask(mDynamicButtonsList.get(position).getButtonTitle(), mDynamicButtonsList, careTaskId, "");
            }
        } else if (resultCode == getActivity().RESULT_OK && data != null && requestCode == CareTaskActionFragment.OBSERVATION_SELECT) {
            Bundle extras = data.getExtras();
            if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                boolean isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                if (isToRefresh) {
                    if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                        submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
                    }
                }
            }
        } else if (resultCode == getActivity().RESULT_OK && data != null && requestCode == CareTaskActionFragment.APPOINTMENT_SELECT) {
            Bundle extras = data.getExtras();
            if (extras.containsKey(VariableConstants.REFRESH_RECORDS)) {
                boolean isToRefresh = extras.getBoolean(VariableConstants.REFRESH_RECORDS);
                if (isToRefresh) {
                    if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                        submitTask(careTaskButtons.getButtonTitle(), listButtons, careTaskId, "");
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe
    public void onCareTaskSubmitResponse(CareTaskSubmitResponse careTaskSubmitResponse) {
        if (mTransactionId != careTaskSubmitResponse.getTransactionId()) {
            return;
        }
        if (pdialog != null && pdialog.isShowing()) {
            pdialog.dismiss();
        }
        if (careTaskSubmitResponse.isSuccess()) {
            Intent intent = new Intent();
            intent.putExtra("isRefresh", true);
            ((CareTaskDetailActivity) mContext).setResult(Activity.RESULT_OK, intent);
            ((CareTaskDetailActivity) mContext).finish();
            return;
        }
        Toast.makeText(getActivity(), careTaskSubmitResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onCareTaskEditResponse(CareTaskEditResponse careTaskEditResponse) {
        if (mTransactionId != careTaskEditResponse.getTransactionId()) {
            return;
        }
        if (pdialog != null && pdialog.isShowing()) {
            pdialog.dismiss();
        }
        if (careTaskEditResponse.isSuccess()) {
            Intent intent = new Intent();
            intent.putExtra("isRefresh", true);
            ((CareTaskDetailActivity) mContext).setResult(Activity.RESULT_OK, intent);
            ((CareTaskDetailActivity) mContext).finish();
            return;
        }
        Toast.makeText(getActivity(), careTaskEditResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }


    private void styleAndAnimate(final CustomFontTextView textView, final String content, final Context mContext) {


        ClickableSpan clickableSpan = new ClickableSpan() {
            boolean isUnderline = false;

            @Override
            public void onClick(View view) {
                if (appendContent.equals(less)) {
                    isExpanded = false;
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                } else if (appendContent.equals(more)) {
                    isExpanded = true;
                    textView.setEllipsize(null);
                }
                styleAndAnimate(textView, content, mContext);
            }

            @Override

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(isUnderline);
                ds.setColor(mContext.getResources().getColor(R.color.link_blue_color));
                ds.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }
        };

        if (isExpanded && content.length() > MAX_LENGTH) {
            appendContent = less;
            text = new SpannableString(content + appendContent);
            text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.link_blue_color)), content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);

        } else if (!isExpanded && content.length() > MAX_LENGTH) {
            String contentTrim = content.substring(0, MAX_LENGTH);
            appendContent = more;
            text = new SpannableString(contentTrim + appendContent);
            text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.link_blue_color)), MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);
        }
    }

    public boolean checkPermission(final String permissionType, String message, final int PERMISSION_REQUEST_CODE) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CareTaskDetailActivity.DOWNLOAD_FILE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mAttachmentRecyclerAdpater.downloadFile();
        } else {
            onPermissionNotGranted(permissions[0]);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.permission_uniterrupted_service), Toast.LENGTH_SHORT).show();
    }
}