package careplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.SettingWebView;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.views.RTextView;

import java.util.ArrayList;

import appointment.FragmentBaseActivity;
import appointment.adapter.LocationAdapter;
import appointment.model.OrderSummary;
import appointment.model.locationmodel.List;

/**
 * Created by laxmansingh on 11/30/2016.
 */

public class RelatedLinksAdapter extends RecyclerView.Adapter<RelatedLinksAdapter.LinksViewHolder> {

    private Context mContext;
    private java.util.List<String> list;


    public RelatedLinksAdapter(Context mContext, java.util.List<String> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public RelatedLinksAdapter.LinksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_links_list_item, null);
        RelatedLinksAdapter.LinksViewHolder viewHolder = new RelatedLinksAdapter.LinksViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RelatedLinksAdapter.LinksViewHolder holder, int position) {
        holder.tv_title.setText(list.get(position).toString().trim());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class LinksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_title;

        public LinksViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            tv_title = (TextView) itemView.findViewById(R.id.tv_related_link_title);
            tv_title.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.tv_related_link_title:
                    Intent intent = new Intent(mContext, SettingWebView.class);
                    intent.putExtra(VariableConstants.TITLE_BAR, mContext.getResources().getString(R.string.related_link));
                    intent.putExtra(VariableConstants.WEB_URL, list.get(position).toString());
                    if (intent != null && Utils.showDialogForNoNetwork(mContext, false))
                        mContext.startActivity(intent);
                    break;

            }
        }
    }
}
