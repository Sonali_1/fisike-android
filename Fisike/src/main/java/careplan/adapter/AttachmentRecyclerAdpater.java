package careplan.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.request.DownloadRecordFileRequest;

import careplan.activity.CareTaskDetailActivity;
import careplan.background.DownloadFileRequest;

import java.util.List;

import careplan.fragments.CareTaskDetailFragment;
import careplan.fragments.CareTaskFragment;
import careplan.model.CareTaskAttachemtsModel;

/**
 * Created by laxmansingh on 12/1/2016.
 */

public class AttachmentRecyclerAdpater extends RecyclerView.Adapter<AttachmentRecyclerAdpater.AttachmentsViewHolder> {

    private Context mContext;
    private int collapsedItemCounts = 4;
    private List<CareTaskAttachemtsModel> listAttachements;
    private boolean isExpanded = false;
    private Typeface font_semibold;
    private Typeface font_regular;
    private CareTaskDetailFragment careTaskDetailFragment;
    private long careTaskId;
    private int tempPosition;


    public AttachmentRecyclerAdpater(Context mContext, List<CareTaskAttachemtsModel> listAttachements, CareTaskDetailFragment careTaskDetailFragment, long careTaskId) {
        this.listAttachements = listAttachements;
        this.mContext = mContext;
        font_semibold = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_semibold));
        font_regular = Typeface.createFromAsset(mContext.getAssets(), this.mContext.getResources().getString(R.string.opensans_ttf_regular));
        this.careTaskDetailFragment = careTaskDetailFragment;
        this.careTaskId = careTaskId;
    }

    @Override
    public AttachmentRecyclerAdpater.AttachmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_list_item, null);
        AttachmentRecyclerAdpater.AttachmentsViewHolder viewHolder = new AttachmentRecyclerAdpater.AttachmentsViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AttachmentRecyclerAdpater.AttachmentsViewHolder holder, int position) {

        GradientDrawable bgShape = null;/*= (GradientDrawable) holder.txt_icon_with_bg.getBackground();*/

        if (isExpanded) {
            if (position == listAttachements.size()) {
                holder.tv_attachment_name.setText(mContext.getResources().getString(R.string.View_Less));
                holder.tv_attachment_name.setTextColor(mContext.getResources().getColor(R.color.dark_blue_opacity_69));
                holder.tv_attachment_name.setTypeface(font_semibold);

                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.more_type_icon));
            } else {
                holder.tv_attachment_name.setText(listAttachements.get(position).getFileName().toString().trim());
                holder.tv_attachment_name.setTextColor(mContext.getResources().getColor(R.color.dusky_blue_70));
                holder.tv_attachment_name.setTypeface(font_regular);
                setMimeTypeImage(listAttachements.get(position).getMimeType(), bgShape, holder);
            }
        } else {
            if (position == collapsedItemCounts) {
                holder.tv_attachment_name.setText(listAttachements.size() - collapsedItemCounts + mContext.getResources().getString(R.string._More));
                holder.tv_attachment_name.setTextColor(mContext.getResources().getColor(R.color.dark_blue_opacity_69));
                holder.tv_attachment_name.setTypeface(font_semibold);

                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.more_type_icon));
            } else {
                holder.tv_attachment_name.setText(listAttachements.get(position).getFileName().toString().trim());
                holder.tv_attachment_name.setTextColor(mContext.getResources().getColor(R.color.dusky_blue_70));
                holder.tv_attachment_name.setTypeface(font_regular);
                setMimeTypeImage(listAttachements.get(position).getMimeType(), bgShape, holder);
            }
        }
    }


    @Override
    public int getItemCount() {
        if (listAttachements != null) {
            return listAttachements.size() > 4 ? isExpanded ? listAttachements.size() + 1 : collapsedItemCounts + 1 : listAttachements.size();
        }
        return 0;
    }

    public class AttachmentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CustomFontTextView tv_attachment_name;
        private ImageView txt_icon_with_bg;


        public AttachmentsViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            tv_attachment_name = (CustomFontTextView) itemView.findViewById(R.id.tv_attachment_name);
            tv_attachment_name.setOnClickListener(this);
            txt_icon_with_bg = (ImageView) itemView.findViewById(R.id.txt_icon_with_bg);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            switch (v.getId()) {
                case R.id.tv_attachment_name:
                    if (isExpanded) {
                        if (position == listAttachements.size()) {
                            isExpanded = false;
                            notifyDataSetChanged();
                        } else {
                            if (careTaskDetailFragment.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext.getString(R.string.permission_msg) + " " + mContext.getString(R.string.permission_storage), CareTaskDetailActivity.DOWNLOAD_FILE)) {
                                new DownloadFileRequest(mContext, 0, listAttachements.get(position).getMimeType(), careTaskDetailFragment, listAttachements.get(position), position, careTaskId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                tempPosition = position;
                            }
                        }
                    } else {
                        if (position == collapsedItemCounts) {
                            isExpanded = true;
                            notifyDataSetChanged();
                        } else {
                            if (careTaskDetailFragment.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext.getString(R.string.permission_msg) + " " + mContext.getString(R.string.permission_storage), CareTaskDetailActivity.DOWNLOAD_FILE)) {
                                new DownloadFileRequest(mContext, 0, listAttachements.get(position).getMimeType(), careTaskDetailFragment, listAttachements.get(position), position, careTaskId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                tempPosition = position;
                            }
                        }
                    }

                    break;
            }

        }
    }

    public void downloadFile(){
        new DownloadFileRequest(mContext, 0, listAttachements.get(tempPosition).getMimeType(), careTaskDetailFragment, listAttachements.get(tempPosition), tempPosition, careTaskId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private void setMimeTypeImage(int mimeType, GradientDrawable bgShape, AttachmentsViewHolder holder) {
        switch (mimeType) {
            case VariableConstants.MIME_TYPE_NOTHING:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unknown_type_icon));
                break;

            case VariableConstants.MIME_TYPE_IMAGE:
                // bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.img_type_icon));
                break;

            case VariableConstants.MIME_TYPE_FILE:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.injection));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.pdf_type_icon));
                break;

            case VariableConstants.MIME_TYPE_DOC:
                // bgShape.setColor(mContext.getResources().getColor(R.color.drop));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.doc_type_icon));
                break;

            case VariableConstants.MIME_TYPE_ZIP:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unknown_type_icon));
                break;

            case VariableConstants.MIME_TYPE_UNKNOWN:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unknown_type_icon));
                break;

            case VariableConstants.MIME_TYPE_DOCX:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.doc_type_icon));
                break;

            case VariableConstants.MIME_TYPE_TEXT:
                //  bgShape.setColor(mContext.getResources().getColor(R.color.capsule));
                holder.txt_icon_with_bg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.txt_type_icon));
                break;

        }
    }

    public void callNotifyDataSetChanged() {
        if (listAttachements.size() > 4) {
            isExpanded = false;
        } else if (listAttachements.size() <= 4 && listAttachements.size() > 0) {
            isExpanded = true;
        }
        notifyDataSetChanged();
    }

}
