package careplan.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONException;

import java.util.List;

import careplan.enums.CareTaskButtonEnum;
import careplan.model.CareTaskButtons;
import careplan.request.CareTaskEdit;

/**
 * Created by laxmansingh on 12/1/2016.
 */

public class DynamicButtonsRecyclerAdapter extends RecyclerView.Adapter<DynamicButtonsRecyclerAdapter.DynamicButtonViewHolder> {

    private Context context;
    private java.util.List<CareTaskButtons> list;
    private OnItemClickedListener itemClickedListener;
    private final long careTaskId;
    private String status;


    public DynamicButtonsRecyclerAdapter(Context context, List<CareTaskButtons> list, OnItemClickedListener itemClickedListener, long careTaskId, String status) {
        this.list = list;
        this.context = context;
        this.status = status;
        this.itemClickedListener = itemClickedListener;
        this.careTaskId = careTaskId;
    }

    public interface OnItemClickedListener {
        public void itemClicked(View view, int position);
    }

    @Override
    public DynamicButtonsRecyclerAdapter.DynamicButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dynamicbuttons_list_item, null);
        DynamicButtonsRecyclerAdapter.DynamicButtonViewHolder viewHolder = new DynamicButtonsRecyclerAdapter.DynamicButtonViewHolder(view, context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DynamicButtonsRecyclerAdapter.DynamicButtonViewHolder holder, int position) {
        CareTaskButtons careTaskButtonsModel = null;
        CareTaskButtonEnum careTaskButtonEnum;
        if (list != null && list.size() > 0) {
            careTaskButtonsModel = list.get(position);
            careTaskButtonEnum = CareTaskButtonEnum.getCareTaskBtnEnumLinkedHashMap().get(careTaskButtonsModel.getButtonTitle());
        } else {
            careTaskButtonEnum = CareTaskButtonEnum.getCareTaskBtnEnumLinkedHashMap().get("Complete");
        }
        if (careTaskButtonEnum != null) {
            holder.btn_dynamic.setText(careTaskButtonEnum.getValue() == 0 ? careTaskButtonsModel.getButtonTitle() : context.getString(careTaskButtonEnum.getValue()));
        } else {
            holder.btn_dynamic.setText(careTaskButtonsModel.getButtonTitle());
        }
    }

    @Override
    public int getItemCount() {

        return (list != null && list.size() > 0) ? list.size() : (status != null && status.equalsIgnoreCase("finished")) ? 0 : 1;
    }

    public class DynamicButtonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontButton btn_dynamic;

        public DynamicButtonViewHolder(View itemView, Context mContext) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            btn_dynamic = (CustomFontButton) itemView.findViewById(R.id.btn_dynamic);
            btn_dynamic.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClickedListener != null) {
                itemClickedListener.itemClicked(v, getAdapterPosition());
            }
        }
    }
}
