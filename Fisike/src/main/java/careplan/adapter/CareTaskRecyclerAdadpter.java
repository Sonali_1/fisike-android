package careplan.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.gson.request.Line;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.persistence.EncounterMedicationDBAdapter;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.CustomLinkMovementMethod;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import appointment.utils.CommonTasks;
import careplan.constants.CarePlanConstants;
import careplan.enums.CareTaskButtonEnum;
import careplan.enums.CareTaskStatusEnum;
import careplan.fragments.CareTaskFragment;
import careplan.model.CareHeaderModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;

/**
 * Created by xmb2nc on 30-08-2016.
 */
public class CareTaskRecyclerAdadpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> careTaskList;
    private Context context;
    private OnItemClickedListener itemClickedListener;
    private CareTaskFragment fragment;
    private long caseLoadId;
    private boolean isFromCarePlanScreen;
    private boolean isCompleted;
    private int noOfItemInEachRowCovered;
    private int rowThrashold = 0;
    private int noOfItemAdded = 0;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_HEADER = 1;


    private SpannableString text;
    private String appendContent;
    private boolean isExpanded;
    private int MAX_LENGTH = 115;
    private String more, less;


    public void setCareTaskLis(List<Object> careTaskList) {
        this.careTaskList = careTaskList;
    }

    public interface OnItemClickedListener {
        public void itemClicked(View view, int position);

        public void handleAction(Context context, CareTaskModel careTaskModel, CareTaskButtons careTaskButtons, List<CareTaskButtons> listButtons, long careTaskId, String questionnaireUrl);
    }

    public CareTaskRecyclerAdadpter(Context context, List<Object> plansList,
                                    OnItemClickedListener itemClickedListener, CareTaskFragment fragment, long caseLoadId, boolean isFromCarePlanScreen, boolean isCompleted) {
        this.careTaskList = plansList;
        this.context = context;
        this.itemClickedListener = itemClickedListener;
        this.fragment = fragment;
        this.caseLoadId = caseLoadId;
        this.isFromCarePlanScreen = isFromCarePlanScreen;
        this.isCompleted = isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View carePlanHeaderView = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.care_plan_task_header, viewGroup, false);
            return new CareTaskHeaderViewHolder(carePlanHeaderView);
        } else if (viewType == TYPE_ITEM) {
            View careTaskRowView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.care_task_row, viewGroup, false);
            return new CareTaskItemViewHolder(careTaskRowView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof CareTaskHeaderViewHolder) {
            CareHeaderModel careHeaderModel = (CareHeaderModel) careTaskList.get(i);
            if (BuildConfig.isPhysicianApp) {
                String textHeader = isCompleted ? context.getResources().getString(R.string.closed) : context.getResources().getString(R.string.pending);
                if (careHeaderModel.getTotalCarePlansCount() > 1) {
                    ((CareTaskHeaderViewHolder) viewHolder)
                            .tvActiveCarePlansCount.setText(String.format(context.getResources().
                            getString(R.string.multiple_plan_care_task_header_text), careHeaderModel.getTotalCarePlansCount(), textHeader));
                } else {
                    ((CareTaskHeaderViewHolder) viewHolder)
                            .tvActiveCarePlansCount.setText(String.format(context.getResources().
                            getString(R.string.care_pan_task_header_text), careHeaderModel.getTotalCarePlansCount(), textHeader));
                }
            } else {
                if (careHeaderModel.getTotalCarePlansCount() > 1) {
                    ((CareTaskHeaderViewHolder) viewHolder)
                            .tvActiveCarePlansCount.setText(String.format(context.getResources().
                            getString(R.string.multiple_care_task_header_text), careHeaderModel.getTotalCarePlansCount()));
                } else {
                    ((CareTaskHeaderViewHolder) viewHolder)
                            .tvActiveCarePlansCount.setText(String.format(context.getResources().
                            getString(R.string.care_task_header_text), careHeaderModel.getTotalCarePlansCount()));
                }
            }
            return;
        } else if (viewHolder instanceof CareTaskItemViewHolder) {
            final CareTaskModel careTaskModel = (CareTaskModel) careTaskList.get(i);


            if (Utils.isRTL(context)) {
                ((CareTaskItemViewHolder) viewHolder).tvArrowIcon.setRotationY(180);
            } else {
                ((CareTaskItemViewHolder) viewHolder).tvArrowIcon.setRotationY(0);
            }


            ((CareTaskItemViewHolder) viewHolder).tvCareTaskTitle.setText(careTaskModel.getCareTaskTitle());

            ((CareTaskItemViewHolder) viewHolder).tvCareTaskDescription.setText("");

            if (isFromCarePlanScreen && BuildConfig.isPhysicianApp && careTaskModel.getAssigneeName() != null && !careTaskModel.getAssigneeName().equals(context.getResources().getString(R.string.txt_null)) && !careTaskModel.getAssigneeName().equals("")) {
                ((CareTaskItemViewHolder) viewHolder).layoutAssignedTo.setVisibility(View.VISIBLE);
                ((CareTaskItemViewHolder) viewHolder).tvAssignedTo.setText(context.getResources().getString(R.string.assigned_to) + ": " + careTaskModel.getAssigneeName().trim());
            } else {
                ((CareTaskItemViewHolder) viewHolder).layoutAssignedTo.setVisibility(View.GONE);
            }


            if (careTaskModel.getCareTaskDescription() != null && !careTaskModel.getCareTaskDescription().equals(context.getResources().getString(R.string.txt_null))) {
                String content = careTaskModel.getCareTaskDescription().toString().trim();
                content = content.replaceAll("<br>", "\\\n");
                content = content.replaceAll("<BR>", "\\\n");
                if (content.length() < MAX_LENGTH) {
                    ((CareTaskItemViewHolder) viewHolder).tvCareTaskDescription.setText(content);
                    isExpanded = false;
                    // styleAndAnimate(viewHolder.tv_instructions);
                } else {
                    more = MyApplication.getAppContext().getResources().getString(R.string.more);
                    less = MyApplication.getAppContext().getResources().getString(R.string.less);
                    appendContent = less;
                    isExpanded = false;
                    styleAndAnimate(((CareTaskItemViewHolder) viewHolder).tvCareTaskDescription, content, context);
                }
            }

            if (careTaskModel.isToShowViewDownload()) {
                ((CareTaskItemViewHolder) viewHolder).layoutDownload.setVisibility(View.VISIBLE);
            } else {
                ((CareTaskItemViewHolder) viewHolder).layoutDownload.setVisibility(View.GONE);
            }

            if (careTaskModel.getCarePathwayName() != null && !careTaskModel.getCarePathwayName().equals("") && !careTaskModel.getCarePathwayName().equals(context.getResources().getString(R.string.txt_null))) {
                ((CareTaskItemViewHolder) viewHolder).layoutCarePathName.setVisibility(View.VISIBLE);
                ((CareTaskItemViewHolder) viewHolder).tvCarePathName.setText(careTaskModel.getCarePathwayName());
            } else {
                ((CareTaskItemViewHolder) viewHolder).layoutCarePathName.setVisibility(View.GONE);
            }

            /*if (careTaskModel.getQuestionnaireUrl() != null && !careTaskModel.getQuestionnaireUrl().equals("null") && careTaskModel.getQuestionnaireUrl().contains("/")) {
                ((CareTaskItemViewHolder) viewHolder).btnViewDoc.setVisibility(View.VISIBLE);
            } else {
                ((CareTaskItemViewHolder) viewHolder).btnViewDoc.setVisibility(View.GONE);
            }*/
            ((CareTaskItemViewHolder) viewHolder).tvCareTaskDownloadDoc.setVisibility(View.VISIBLE);


            if (careTaskModel.getCareTaskStatus() != null && careTaskModel.getCareTaskStatus().equals("")) {
                ((CareTaskItemViewHolder) viewHolder).btnCareTaskStatus.setText(context.getResources().getString(R.string.care_task_btn_pending));
            } else {
                CareTaskStatusEnum careTaskStatusEnum = CareTaskStatusEnum.getCareTaskStatusEnumLinkedHashMap().get(careTaskModel.getCareTaskStatus());
                if (careTaskStatusEnum != null && careTaskStatusEnum.getValue() != 0) {
                    ((CareTaskItemViewHolder) viewHolder).btnCareTaskStatus.setText(context.getResources().getString(careTaskStatusEnum.getValue()));
                } else {
                    ((CareTaskItemViewHolder) viewHolder).btnCareTaskStatus.setText(Utils.capitalizeFirstLetter(careTaskModel.getCareTaskStatus()));
                }
            }

          /*  if (Utils.isValueAvailable(careTaskModel.getRelatedTo())) {
                ((CareTaskItemViewHolder) viewHolder).layoutRelateto.setVisibility(View.VISIBLE);
                ((CareTaskItemViewHolder) viewHolder).tv_relatedto_name.setText("Related To: " + careTaskModel.getRelatedTo().toString().trim());
            } else {
                ((CareTaskItemViewHolder) viewHolder).layoutRelateto.setVisibility(View.GONE);
            }
*/
            String careTaskCompletedDate = careTaskModel.getGetCareTaskEndDate();
            if (careTaskCompletedDate != null && !careTaskCompletedDate.equals("") && !careTaskCompletedDate.equalsIgnoreCase("null")) {
                String sourceDate = DateTimeUtil.convertSourceDestinationDate(careTaskCompletedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithoutTime);
                String sourceTime = DateTimeUtil.convertSourceDestinationDate(careTaskCompletedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, "ss");
                String date = sourceTime.equals("59") ? sourceDate : sourceDate + ", " + DateTimeUtil.convertSourceDestinationDate(careTaskCompletedDate, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.sourceTimeFormat);
                ((CareTaskItemViewHolder) viewHolder).tvCareTaskStartDate.setText(String.format(context.getResources().getString(R.string.care_plan_or_task_completed_on), date));
                ((CareTaskItemViewHolder) viewHolder).layoutDate.setVisibility(View.VISIBLE);
            } else {
                ((CareTaskItemViewHolder) viewHolder).layoutDate.setVisibility(View.GONE);
            }
            List<CareTaskButtons> listButtons = careTaskModel.getListButtons();
            ((CareTaskItemViewHolder) viewHolder).layoutBtns.removeAllViews();
            if ((fragment.getSearchStatus() == null || !fragment.getSearchStatus().equalsIgnoreCase(CarePlanConstants.FILTER_STATUS_FINISHED)) && (BuildConfig.isPatientApp || (!isFromCarePlanScreen && BuildConfig.isPhysicianApp)) && listButtons != null && listButtons.size() > 0) {
                addButton(careTaskModel, viewHolder, listButtons, careTaskModel.getTaskId(), careTaskModel.getQuestionnaireUrl());
            } else if ((fragment.getSearchStatus() == null || !fragment.getSearchStatus().equalsIgnoreCase(CarePlanConstants.FILTER_STATUS_FINISHED)) && (BuildConfig.isPatientApp)) {
                addButton(careTaskModel, viewHolder, null, careTaskModel.getTaskId(), careTaskModel.getQuestionnaireUrl());
            }
        }
    }

    private void addButton(CareTaskModel careTaskModel, RecyclerView.ViewHolder viewHolder, final List<CareTaskButtons> listButtons, final long id, String questionnaireUrl) {
        int number = (listButtons != null && listButtons.size() > 0) ? listButtons.size() : 1;

        noOfItemInEachRowCovered = number;

        if (number == 0)
            return;
        for (int i = 0; i < number; i = i + rowThrashold) {
            if (noOfItemAdded == number)
                return;
            else {
                Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x - (int) Utils.convertDpToPixel(29, context);

                rowThrashold = 0;
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, 0, 0, 6);
                lp.gravity = Gravity.END;
                linearLayout.setLayoutParams(lp);

                //calculating upto which element need to loop within a row
//                int loop_until = (noOfItemInEachRowCovered >= rowThrashold) ? rowThrashold : noOfItemInEachRowCovered % rowThrashold;
//                for (int j = 0; j < loop_until; j++) {
                while (noOfItemAdded < number) {
                    CustomFontButton button = createButton(careTaskModel,
                            (listButtons != null && listButtons.size() > 0) ? listButtons.get(noOfItemAdded) : null,
                            id, (listButtons != null && listButtons.size() > 0) ? listButtons : null,
                            questionnaireUrl);
                    // button.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    button.setSingleLine(true);
                    int btnWidth = button.getMeasuredWidth();
                    if (screenWidth > btnWidth) {
                        linearLayout.addView(button);
                        noOfItemInEachRowCovered = noOfItemInEachRowCovered - 1;
                        noOfItemAdded = noOfItemAdded + 1;
                        rowThrashold++;
                        screenWidth -= btnWidth;
                    } else {
                        break;
                    }
                }
                ((CareTaskItemViewHolder) viewHolder).layoutBtns.addView(linearLayout);
            }

        }
        noOfItemAdded = 0;
    }

    private CustomFontButton createButton(final CareTaskModel careTaskModel, final CareTaskButtons finalCareTaskButton, final long id, final List<CareTaskButtons> listButtons, final String questionnaireUrl) {
        final CustomFontButton customFontButton = new CustomFontButton(context);

        CareTaskButtonEnum careTaskButtonEnum = null;
        if(listButtons!=null&& listButtons.size()>0){
             careTaskButtonEnum = CareTaskButtonEnum.getCareTaskBtnEnumLinkedHashMap().get(finalCareTaskButton.getButtonTitle());
        }else {
            careTaskButtonEnum = CareTaskButtonEnum.getCareTaskBtnEnumLinkedHashMap().get("Complete");
        }

        if (careTaskButtonEnum != null) {
            customFontButton.setText(careTaskButtonEnum.getValue() == 0 ? finalCareTaskButton.getButtonTitle() : context.getString(careTaskButtonEnum.getValue()));
        } else {
            customFontButton.setText(finalCareTaskButton.getButtonTitle());
        }
        customFontButton.setTextSize(14);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if (Utils.isRTL(context)) {
            params.setMargins(16, 0, 16, 0);
        } else {
            params.setMargins(16, 0, 16, 0);
        }
        customFontButton.setLayoutParams(params);
        customFontButton.setTypeface(customFontButton.selectTypeface(context, VariableConstants.MEDIUM, VariableConstants.MEDIUM));
        customFontButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ripple_blue_effect));
        customFontButton.setClickable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //  customFontButton.setBackgroundColor(context.getColor(R.color.transparent));
            customFontButton.setTextColor(context.getColor(R.color.action_bar_bg));
        } else {
            //  customFontButton.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            customFontButton.setTextColor(context.getResources().getColor(R.color.action_bar_bg));
        }
        customFontButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickedListener.handleAction(context, careTaskModel, finalCareTaskButton, listButtons, id, questionnaireUrl);
            }
        });

        return customFontButton;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = careTaskList.get(position);
        if (object instanceof CareHeaderModel) {
            return TYPE_HEADER;
        } else if (object instanceof CareTaskModel) {
            return TYPE_ITEM;
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return careTaskList == null ? 0 : careTaskList.size();
    }

    public class CareTaskHeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView tvActiveCarePlansCount;

        public CareTaskHeaderViewHolder(View view) {
            super(view);
            this.tvActiveCarePlansCount = (TextView) view.findViewById(R.id.tv_active_count);
        }
    }

    public class CareTaskItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //        private final CustomFontButton btnViewDoc;
        private CustomFontTextView tvViewDetail;
        private CustomFontTextView tvCarePathName;
        private CustomFontTextView tvAssignedTo;
        private CustomFontTextView tvCareTaskTitle, tvCareTaskStartDate, tvCareTaskDescription;
        private CustomFontTextView tvCareTaskDownloadDoc;
        private CustomFontTextView tv_relatedto_name;
        //        private CustomFontButton btnViewInfo;
        private LinearLayout layoutBtns;
        private LinearLayout layoutCarePathName;
        private LinearLayout layoutDate;
        private LinearLayout layoutDownload;
        private LinearLayout layoutAssignedTo;
        private LinearLayout layoutRelateto;
        private CustomFontButton btnCareTaskStatus;
        private IconTextView tvArrowIcon;

        public CareTaskItemViewHolder(View view) {
            super(view);
            tvCareTaskTitle = (CustomFontTextView) view.findViewById(R.id.tv_care_task_title);
            tvCareTaskStartDate = (CustomFontTextView) view.findViewById(R.id.tv_care_task_start_date);
            tvCareTaskDescription = (CustomFontTextView) view.findViewById(R.id.tv_care_task_description);
            tvCareTaskDownloadDoc = (CustomFontTextView) view.findViewById(R.id.tv_care_task_download_doc);
//            btnViewDoc = (CustomFontButton) view.findViewById(R.id.btnViewDoc);
            tvViewDetail = (CustomFontTextView) view.findViewById(R.id.tv_view_detail);
            tvAssignedTo = (CustomFontTextView) view.findViewById(R.id.tv_assigned_to);
            tv_relatedto_name = (CustomFontTextView) view.findViewById(R.id.tv_relatedto_name);
//            btnViewInfo = (CustomFontButton) view.findViewById(R.id.btnViewInfo);
            layoutBtns = (LinearLayout) view.findViewById(R.id.layoutBtns);
            tvCarePathName = (CustomFontTextView) view.findViewById(R.id.tv_care_path_name);
            layoutCarePathName = (LinearLayout) view.findViewById(R.id.layoutCarePathName);
            layoutDate = (LinearLayout) view.findViewById(R.id.layoutDate);
            layoutDownload = (LinearLayout) view.findViewById(R.id.layoutDownload);
            layoutAssignedTo = (LinearLayout) view.findViewById(R.id.layoutAssignedTo);
            layoutRelateto = (LinearLayout) view.findViewById(R.id.layoutRelateto);
            tvArrowIcon = (IconTextView) view.findViewById(R.id.tv_arrow_icon);

            view.findViewById(R.id.btn_care_task_status);
            btnCareTaskStatus = (CustomFontButton) view.findViewById(R.id.btn_care_task_status);
            this.tvCareTaskDownloadDoc.setOnClickListener(this);
            tvViewDetail.setOnClickListener(this);
//            this.btnViewInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickedListener != null) {
                itemClickedListener.itemClicked(v, getAdapterPosition());
            }
        }
    }


    public void notifyItemsView() {
        List<Object> tempList = new ArrayList<Object>();
        tempList.addAll(careTaskList);
        careTaskList.clear();
        careTaskList.addAll(tempList);
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }


    private void styleAndAnimate(final CustomFontTextView textView, final String content, final Context mContext) {

        ClickableSpan clickableSpan = new ClickableSpan() {
            boolean isUnderline = false;

            @Override
            public void onClick(View view) {
                if (appendContent.equals(less)) {
                    isExpanded = false;
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                } else if (appendContent.equals(more)) {
                    isExpanded = true;
                    textView.setEllipsize(null);
                }
                styleAndAnimate(textView, content, mContext);
            }

            @Override

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(isUnderline);
                ds.setColor(mContext.getResources().getColor(R.color.link_blue_color));
                ds.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }
        };

        if (isExpanded && content.length() > MAX_LENGTH) {
            appendContent = less;
            text = new SpannableString(content + appendContent);
            // text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, content.length(), 0);
            text.setSpan(clickableSpan, content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.link_blue_color)), content.length(), text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            // textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);

        } else if (!isExpanded && content.length() > MAX_LENGTH) {
            String contentTrim = content.substring(0, MAX_LENGTH);
            appendContent = more;
            text = new SpannableString(contentTrim + appendContent);
            text.setSpan(clickableSpan, MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.link_blue_color)), MAX_LENGTH, MAX_LENGTH + appendContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setMovementMethod(new CustomLinkMovementMethod());
            textView.setLinksClickable(true);
            textView.setText(text);
        }
    }
}