package careplan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.util.List;

import appointment.utils.CommonTasks;
import careplan.enums.CarePlanStatusEnum;
import careplan.enums.CareTaskStatusEnum;
import careplan.model.CareHeaderModel;
import careplan.model.CarePlanModel;

/**
 * Created by xmb2nc on 30-08-2016.
 */
public class CarePlanRecyclerAdadpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> carePlansList;
    private Context context;
    private OnItemClickedListener itemClickedListener;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_HEADER = 1;

    public void setCarePlanModelList(List<Object> carePlanModelList) {
        this.carePlansList = carePlanModelList;
    }

    public interface OnItemClickedListener {
        public void itemClicked(View view, int position);
    }

    public CarePlanRecyclerAdadpter(Context context, OnItemClickedListener itemClickedListener) {
        this.context = context;
        this.itemClickedListener = itemClickedListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View carePlanHeaderView = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.care_plan_task_header, viewGroup, false);
            return new CarePlanHeaderViewHolder(carePlanHeaderView);
        } else if (viewType == TYPE_ITEM) {
            View carePlanRowView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.care_plan_row, viewGroup, false);
            return new CarePlanItemViewHolder(carePlanRowView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder carePlanViewHolder, int i) {
        if (carePlanViewHolder instanceof CarePlanHeaderViewHolder) {
            CareHeaderModel careHeaderModel = (CareHeaderModel) carePlansList.get(i);
            // handling of header view
            ((CarePlanHeaderViewHolder) carePlanViewHolder)
                    .tvActiveCarePlansCount.setText(String.format(context.getResources().
                    getString(R.string.care_plan_header_text), careHeaderModel.getTotalCarePlansCount()));
            return;
        } else if (carePlanViewHolder instanceof CarePlanItemViewHolder) {
//            if (BuildConfig.isPhysicianApp) {
//                ((CarePlanItemViewHolder) carePlanViewHolder).tvCarePlanViewTasks.setVisibility(View.INVISIBLE);
//            }
            CarePlanModel carePlanModel = (CarePlanModel) carePlansList.get(i);
            // spannable + formating
            String date = CommonTasks.formateDateFromstring(DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil.destinationDateFormatWithoutTime, carePlanModel.getStartDate().toString().trim());
            String startedDate = String.format(context.getResources().getString(
                    R.string.care_plan_or_task_start_date), date);
//            Spannable startedDateSpan = new SpannableString(startedDate);
//            startedDateSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.action_bar_bg)), context.getResources().getString(
//                    R.string.care_plan_or_task_start_date).length() - 4, startedDate.length(),
//                    Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            ((CarePlanItemViewHolder) carePlanViewHolder).tvCarePlanTitle.setText(carePlanModel.getCarePlanTitle());
            ((CarePlanItemViewHolder) carePlanViewHolder).tvCarePlanStartDate.setText(startedDate);
            if (!carePlanModel.getDescription().equals("") && !carePlanModel.getDescription().equals("null")) {
                String description = carePlanModel.getDescription().replaceAll("<br>","\\\n");
                description = carePlanModel.getDescription().replaceAll("<BR>","\\\n");
                ((CarePlanItemViewHolder) carePlanViewHolder).tvCarePlanDescription.setText(description);
            }

            String carePlanStatus = CarePlanStatusEnum.getDisplayedValuefromCode(carePlanModel.getStatus());
            if (carePlanStatus!= null ) {
                ((CarePlanItemViewHolder) carePlanViewHolder).btnStatus.setText(carePlanStatus);
            }else {
                ((CarePlanItemViewHolder) carePlanViewHolder).btnStatus.setText(Utils.toCamelCase(carePlanModel.getStatus()));
            }
            if (carePlanModel.getDocumentId() != null && !carePlanModel.getDocumentId().equals("")) {
                ((CarePlanItemViewHolder) carePlanViewHolder).btnViewDoc.setVisibility(View.VISIBLE);
            } else if (((CarePlanItemViewHolder) carePlanViewHolder).btnViewDoc.getVisibility() != View.GONE) {
                ((CarePlanItemViewHolder) carePlanViewHolder).btnViewDoc.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object object = carePlansList.get(position);
        if (object instanceof CareHeaderModel) {
            return TYPE_HEADER;
        } else if (object instanceof CarePlanModel) {
            return TYPE_ITEM;
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return carePlansList == null ? 0 : carePlansList.size();
    }

    public class CarePlanHeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView tvActiveCarePlansCount;

        public CarePlanHeaderViewHolder(View view) {
            super(view);
            this.tvActiveCarePlansCount = (TextView) view.findViewById(R.id.tv_active_count);
        }

    }

    public class CarePlanItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final CustomFontButton btnViewDoc;
        private final CustomFontButton btnStatus;
        private CustomFontTextView tvCarePlanTitle, tvCarePlanStartDate, tvCarePlanDescription,
                tvCarePlanViewTasks;
        private CustomFontTextView tvCarePlanDetails;

        public CarePlanItemViewHolder(View view) {
            super(view);
            this.tvCarePlanTitle = (CustomFontTextView) view.findViewById(R.id.tv_care_plan_title);
            this.tvCarePlanStartDate = (CustomFontTextView) view.findViewById(R.id.tv_care_plan_start_date);
            this.tvCarePlanDescription = (CustomFontTextView) view.findViewById(R.id.tv_care_plan_description);
            this.tvCarePlanViewTasks = (CustomFontTextView) view.findViewById(R.id.tv_care_plan_view_tasks);
            btnViewDoc = (CustomFontButton) view.findViewById(R.id.btnViewDoc);
            btnStatus = (CustomFontButton) view.findViewById(R.id.btn_care_plan_status);
            this.tvCarePlanViewTasks.setOnClickListener(this);
//            this.tvCarePlanDetails = (CustomFontTextView) view.findViewById(R.id.tv_care_plan_plan_details);
//            this.tvCarePlanDetails.setOnClickListener(this);
            btnViewDoc.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClickedListener != null) {
                itemClickedListener.itemClicked(v, getAdapterPosition());
            }
        }
    }
}