package careplan.enums;

/**
 * Created by Kailash Khurana on 1/23/2017.
 */

public enum CareTaskActionEnum {
    DEEPLINK("deeplink"),
    EDIT("editStandAloneTask"),
    UPDATE("updatedProcessVariable");

    private String flowValue;

    private CareTaskActionEnum(String value) {
        this.flowValue = value;
    }

    public String getFlowValue() {
        return flowValue;
    }
}
