package careplan.enums;

/**
 * Created by Kailash Khurana on 1/23/2017.
 */

public enum CareTaskTypeEnum {
    BOOLEAN("boolean"),
    STRING("string"),
    DATE("date"),
    VITAL("vital"),
    MEDICATION("medication"),
    QUESTIONNAIRE("quetionnaire"),
    APPOINTMENT("patId");


    private String flowValue;

    private CareTaskTypeEnum(String value) {
        this.flowValue = value;
    }

    public String getFlowValue() {
        return flowValue;
    }
}
