package careplan.enums;

/**
 * Created by Kailash Khurana on 9/13/2016.
 */
public enum ObserverOperationEnum {
    Default(0),
    INSERT(1),
    UPDATE(2);

    private int openation;

    ObserverOperationEnum(int openation) {
        this.openation = openation;
    }

    public int getOpenation() {
        return openation;
    }
}
