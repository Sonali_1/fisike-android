package careplan.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash Khurana on 5/1/2017.
 */

public enum CareTaskStatusEnum {
    COMPLETE(MyApplication.getAppContext().getString(R.string.care_task_status_complete_key), R.string.care_task_btn_complete),
    COMPLETED(MyApplication.getAppContext().getString(R.string.care_task_status_completed_key), R.string.care_task_btn_completed),
    ONHOLD(MyApplication.getAppContext().getString(R.string.care_task_status_onhold_key), R.string.care_task_btn_onhold),
    PENDING(MyApplication.getAppContext().getString(R.string.care_task_status_pending_key), R.string.care_task_btn_pending),
    CANCEL(MyApplication.getAppContext().getString(R.string.care_task_status_cancel_key), R.string.care_task_btn_cancelled),;


    private String code;
    private int value;
    private static LinkedHashMap<String, CareTaskStatusEnum> careTaskStatusEnumLinkedHashMap = null;


    private CareTaskStatusEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, CareTaskStatusEnum> getCareTaskStatusEnumLinkedHashMap() {
        if (careTaskStatusEnumLinkedHashMap == null) {
            careTaskStatusEnumLinkedHashMap = new LinkedHashMap<String, CareTaskStatusEnum>();
            for (CareTaskStatusEnum vitals : CareTaskStatusEnum.values()) {
                careTaskStatusEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return careTaskStatusEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(int value) {
        getCareTaskStatusEnumLinkedHashMap();
        for (Map.Entry<String, CareTaskStatusEnum> enumMap : careTaskStatusEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue() == value)
                return enumMap.getValue().getCode();
        return "";
    }
}
