package careplan.enums;

/**
 * Created by laxmansingh on 7/19/2017.
 */

public enum CareTaskVitalName {

    BMI("BMI"),
    BLOODPRESSURE("BloodPressure"),
    GLUCOSE("Glucose"),
    WEIGHT("Weight"),
    HEIGHT("Height"),
    HEARTRATE("HeartRate"),
    RESPIRATIONRATE("RespirationRate"),
    TEMPERATURE("Temperature");

    private String flowValue;

    private CareTaskVitalName(String value) {
        this.flowValue = value;
    }

    public String getFlowValue() {
        return flowValue;
    }
}
