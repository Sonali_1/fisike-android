package careplan.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash Khurana on 5/1/2017.
 */

public enum CareTaskButtonEnum
{
    COMPLETE(MyApplication.getAppContext().getString(R.string.care_task_btn_complete_key),R.string.care_task_btn_complete),
    ONHOLD(MyApplication.getAppContext().getString(R.string.care_task_btn_onhold_key), R.string.care_task_btn_onhold),
    PENDING(MyApplication.getAppContext().getString(R.string.care_task_btn_pending_key), R.string.care_task_btn_pending),
    CANCEL(MyApplication.getAppContext().getString(R.string.care_task_btn_cancel_key), R.string.care_task_btn_cancel),
    RECORDVITAL(MyApplication.getAppContext().getString(R.string.care_task_btn_recordvital_key), R.string.care_task_btn_recordvital),
    RECORDVITALCOMPLETE(MyApplication.getAppContext().getString(R.string.care_task_btn_recordvitalcomplete_key), R.string.care_task_btn_recordvitalcomplete),
    FILLQUESTIONNAIRE(MyApplication.getAppContext().getString(R.string.care_task_btn_fillquestionnaire_key), R.string.care_task_btn_fillquestionnaire),
    BOOKAPPOINTMENT(MyApplication.getAppContext().getString(R.string.care_task_btn_bookappointment_key), R.string.care_task_btn_bookappointment),
    BOOKAPPOINTMENTCOMPLETE(MyApplication.getAppContext().getString(R.string.care_task_btn_bookappointmentcomplete_key), R.string.care_task_btn_bookappointmentcomplete),
    PROCESS(MyApplication.getAppContext().getString(R.string.care_task_btn_process_key), R.string.care_task_btn_process),
    ;


    private String code;
    private int value;
    private static LinkedHashMap<String, CareTaskButtonEnum> careTaskBtnEnumLinkedHashMap = null;


    private CareTaskButtonEnum(String code, int value){
        this.code = code;
        this.value=value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, CareTaskButtonEnum> getCareTaskBtnEnumLinkedHashMap(){
        if(careTaskBtnEnumLinkedHashMap == null){
            careTaskBtnEnumLinkedHashMap = new LinkedHashMap<String, CareTaskButtonEnum>();
            for (CareTaskButtonEnum vitals : CareTaskButtonEnum.values()) {
                careTaskBtnEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return careTaskBtnEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(int value)
    {
        getCareTaskBtnEnumLinkedHashMap();
        for(Map.Entry<String,CareTaskButtonEnum> enumMap: careTaskBtnEnumLinkedHashMap.entrySet())
            if(enumMap.getValue().getValue()==value)
                return enumMap.getValue().getCode();
        return "";
    }
}
