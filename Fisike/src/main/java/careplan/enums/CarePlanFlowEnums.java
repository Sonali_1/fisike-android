package careplan.enums;

/**
 * Created by xmb2nc on 07-09-2016.
 */
public enum CarePlanFlowEnums {
    Default(0),
    QuestionnaireFlow(1),
    DocumentFlow(2);

    private int flowValue;
    private CarePlanFlowEnums(int value){
    this.flowValue = value;
    }

}
