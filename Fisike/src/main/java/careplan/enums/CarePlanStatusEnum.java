package careplan.enums;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.enums.GenderEnum;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kailash Khurana on 5/1/2017.
 */

public enum CarePlanStatusEnum {
    COMPLETE(MyApplication.getAppContext().getString(R.string.care_task_status_complete_key), R.string.care_task_btn_complete),
    COMPLETED(MyApplication.getAppContext().getString(R.string.care_task_status_completed_key), R.string.care_task_btn_completed),
    ACTIVE(MyApplication.getAppContext().getString(R.string.care_plan_status_active_key), R.string.care_plan_status_active),
    CANCEL(MyApplication.getAppContext().getString(R.string.care_plan_status_cancel_key), R.string.care_task_btn_cancelled),
    PROPOSED(MyApplication.getAppContext().getString(R.string.care_plan_status_proposed_key), R.string.care_task_btn_proposed);

    private String code;
    private int value;
    private static LinkedHashMap<String, CarePlanStatusEnum> carePlanStatusEnumLinkedHashMap = null;


    private CarePlanStatusEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, CarePlanStatusEnum> getCarePlanStatusEnumLinkedHashMap() {
        if (carePlanStatusEnumLinkedHashMap == null) {
            carePlanStatusEnumLinkedHashMap = new LinkedHashMap<String, CarePlanStatusEnum>();
            for (CarePlanStatusEnum vitals : CarePlanStatusEnum.values()) {
                carePlanStatusEnumLinkedHashMap.put(vitals.code, vitals);
            }
        }
        return carePlanStatusEnumLinkedHashMap;
    }

    //return code matching to particular value
    public static String getCodeFromValue(int value) {
        getCarePlanStatusEnumLinkedHashMap();
        for (Map.Entry<String, CarePlanStatusEnum> enumMap : carePlanStatusEnumLinkedHashMap.entrySet())
            if (enumMap.getValue().getValue().equals(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        getCarePlanStatusEnumLinkedHashMap();
        if (carePlanStatusEnumLinkedHashMap != null) {
            for (Map.Entry<String, CarePlanStatusEnum> enumMap : carePlanStatusEnumLinkedHashMap.entrySet())
                if (enumMap.getValue().getCode().equals(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
