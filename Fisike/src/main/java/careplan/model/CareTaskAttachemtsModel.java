package careplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Kailash Khurana on 10/18/2016.
 */

public class CareTaskAttachemtsModel implements Parcelable, Serializable {
    private String contentId;
    private String description;
    private String id;
    private int mimeType;
    private String fileName;
    private String type;

    public CareTaskAttachemtsModel() {
    }

    public static final Parcelable.Creator<CareTaskAttachemtsModel> CREATOR = new Parcelable.Creator<CareTaskAttachemtsModel>() {
        @Override
        public CareTaskAttachemtsModel createFromParcel(Parcel in) {
            return new CareTaskAttachemtsModel(in);
        }

        @Override
        public CareTaskAttachemtsModel[] newArray(int size) {
            return new CareTaskAttachemtsModel[size];
        }
    };

    public CareTaskAttachemtsModel(Parcel in) {
        contentId = in.readString();
        description = in.readString();
        id = in.readString();
        mimeType = in.readInt();
        fileName = in.readString();
        type = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentId);
        dest.writeString(description);
        dest.writeString(id);
        dest.writeInt(mimeType);
        dest.writeString(fileName);
        dest.writeString(type);
    }


    public int getMimeType() {
        return mimeType;
    }

    public void setMimeType(int mimeType) {
        this.mimeType = mimeType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}