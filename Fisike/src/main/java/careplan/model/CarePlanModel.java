package careplan.model;

/**
 * Created by xmb2nc on 26-08-2016.
 */
public class CarePlanModel {
    private long caseLoadId;
    private String carePlanTitle;
    private String description;
    private long id;
    private String startDate;
    private String status;
    private ExternalURLModel externalURLModel;
    private String documentId;

    public CarePlanModel() {

    }

    public CarePlanModel(long id, long caseLoadId, String carePlanTitle, String description,
                         String startDate, String status, ExternalURLModel externalURLModel,String documentId) {
        this.id = id;
        this.caseLoadId = caseLoadId;
        this.carePlanTitle = carePlanTitle;
        this.description = description;
        this.startDate = startDate;
        this.status = status;
        this.externalURLModel = externalURLModel;
        this.documentId = documentId;
    }

    public long getCaseLoadId() {
        return caseLoadId;
    }

    public void setCaseLoadId(long caseLoadId) {
        this.caseLoadId = caseLoadId;
    }

    public String getCarePlanTitle() {
        return carePlanTitle;
    }

    public void setCarePlanTitle(String carePlanTitle) {
        this.carePlanTitle = carePlanTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        // format it in desired format before rendering.
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ExternalURLModel getExternalURlModel() {
        return externalURLModel;
    }

    public void setExternalURlModel(ExternalURLModel externalURlModel) {
        this.externalURLModel = externalURlModel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
}
