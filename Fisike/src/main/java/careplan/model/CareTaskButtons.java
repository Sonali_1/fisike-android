package careplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Kailash Khurana on 9/1/2016.
 */
public class CareTaskButtons implements Parcelable, Serializable {

    private String buttonTag;
    private String buttonAction;
    private String buttonTitle;
    private String buttonValue;
    private String buttonId;

    public CareTaskButtons() {
    }

    public static final Parcelable.Creator<CareTaskButtons> CREATOR = new Parcelable.Creator<CareTaskButtons>() {
        @Override
        public CareTaskButtons createFromParcel(Parcel in) {
            return new CareTaskButtons(in);
        }

        @Override
        public CareTaskButtons[] newArray(int size) {
            return new CareTaskButtons[size];
        }
    };

    public CareTaskButtons(Parcel in) {
        buttonTag = in.readString();
        buttonAction = in.readString();
        buttonTitle = in.readString();
        buttonValue = in.readString();
        buttonId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(buttonTag);
        dest.writeString(buttonAction);
        dest.writeString(buttonTitle);
        dest.writeString(buttonValue);
        dest.writeString(buttonId);
    }

    public String getButtonTag() {
        return buttonTag;
    }

    public void setButtonTag(String buttonTag) {
        this.buttonTag = buttonTag;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getButtonValue() {
        return buttonValue;
    }

    public void setButtonValue(String buttonValue) {
        this.buttonValue = buttonValue;
    }

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId;
    }

}