package careplan.model;

/**
 * Created by xmb2nc on 26-08-2016.
 */
public class ExternalURLModel {
    private String externalURLPath;
    // used to distinguish whether its questionnaire/pdf/image url
    private String type;

    public ExternalURLModel(String externalURLPath, String type){
        this.externalURLPath = externalURLPath;
        this.type = type;
    }

    public String getExternalURLPath() {
        return externalURLPath;
    }

    public String getType() {
        return type;
    }
}
