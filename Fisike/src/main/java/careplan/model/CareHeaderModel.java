package careplan.model;

/**
 * Created by xmb2nc on 01-09-2016.
 */
public class CareHeaderModel {
    private long totalCarePlansCount;

    public CareHeaderModel(long totalCarePlansCount){
        this.totalCarePlansCount = totalCarePlansCount;
    }

    public long getTotalCarePlansCount() {
        return totalCarePlansCount;
    }

}
