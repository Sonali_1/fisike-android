package careplan.model;

import java.util.List;

/**
 * Created by Kailash Khurana on 10/18/2016.
 */

public class CareTaskDetailModel {
    private String assignedTo;
    private String priority;
    private String completeBy;
    private String reportedBy;
    private String description;
    private List<String> relatedLinkList;
    private List<String> commentsList;
    private List<CareTaskAttachemtsModel> attachemtsModelsList;
    private List<CareTaskButtons> listButtons;
    private String taskStatus;
    private String carePathName;
    private String careTaskName;


    public String getCarePathName() {
        return carePathName;
    }

    public void setCarePathName(String carePathName) {
        this.carePathName = carePathName;
    }

    public String getCareTaskName() {
        return careTaskName;
    }

    public void setCareTaskName(String careTaskName) {
        this.careTaskName = careTaskName;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCompleteBy() {
        return completeBy;
    }

    public void setCompleteBy(String completeBy) {
        this.completeBy = completeBy;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getRelatedLinkList() {
        return relatedLinkList;
    }

    public void setRelatedLinkList(List<String> relatedLinkList) {
        this.relatedLinkList = relatedLinkList;
    }

    public List<String> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<String> commentsList) {
        this.commentsList = commentsList;
    }

    public List<CareTaskAttachemtsModel> getAttachemtsModelsList() {
        return attachemtsModelsList;
    }

    public void setAttachemtsModelsList(List<CareTaskAttachemtsModel> attachemtsModelsList) {
        this.attachemtsModelsList = attachemtsModelsList;
    }

    public List<CareTaskButtons> getListButtons() {
        return listButtons;
    }

    public void setListButtons(List<CareTaskButtons> listButtons) {
        this.listButtons = listButtons;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }
}
