package careplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import appointment.model.appointmentcommonmodel.AppointmentModel;

/**
 * Created by xmb2nc on 26-08-2016.
 */
public class CareTaskModel implements Parcelable {
    private long id;
    private long taskId;
    private long caseLoadId;
    private String careTaskTitle;
    private String careTaskDescription;
    private String carePathwayName;
    private String careTaskStartDate;
    private String getCareTaskEndDate;
    private String careTaskStatus;
    private List<CareTaskButtons> listButtons = new ArrayList<>();
    private List<CareTaskAttachemtsModel> listAttachment = new ArrayList<>();
    private String referralDate;
    private String patientId;
    private String deleteReason;
    private String assigneeName;
    private String questionnaireUrl;
    private Boolean isToShowViewDownload;
    private String category;
    private long priority;
    private String taskSubtype;
    private String createTime;
    private String assignee;
    private String vitalName;
    private String relatedTo;
    /**
     * Not used
     */
    private String infoUrl;
    private String careTaskURL;

    public CareTaskModel() {
    }

    public static final Creator<CareTaskModel> CREATOR = new Creator<CareTaskModel>() {
        @Override
        public CareTaskModel createFromParcel(Parcel in) {
            return new CareTaskModel(in);
        }

        @Override
        public CareTaskModel[] newArray(int size) {
            return new CareTaskModel[size];
        }
    };

    public CareTaskModel(Parcel in) {
        id = in.readLong();
        taskId = in.readLong();
        caseLoadId = in.readLong();
        careTaskTitle = in.readString();
        careTaskDescription = in.readString();
        carePathwayName = in.readString();
        careTaskStartDate = in.readString();
        getCareTaskEndDate = in.readString();
        careTaskStatus = in.readString();
        in.readList(listButtons, getClass().getClassLoader());
        in.readList(listAttachment, getClass().getClassLoader());
        referralDate = in.readString();
        patientId = in.readString();
        deleteReason = in.readString();
        assigneeName = in.readString();
        questionnaireUrl = in.readString();
        byte isEnableVal = in.readByte();
        isToShowViewDownload = isEnableVal == 0x02 ? false : isEnableVal != 0x00;
        infoUrl = in.readString();
        careTaskURL = in.readString();
        category = in.readString();
        priority = in.readLong();
        taskSubtype = in.readString();
        createTime = in.readString();
        assignee = in.readString();
        vitalName = in.readString();
        relatedTo=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(taskId);
        dest.writeLong(caseLoadId);
        dest.writeString(careTaskTitle);
        dest.writeString(careTaskDescription);
        dest.writeString(carePathwayName);
        dest.writeString(careTaskStartDate);
        dest.writeString(getCareTaskEndDate);
        dest.writeString(careTaskStatus);
        dest.writeList(listButtons);
        dest.writeList(listAttachment);
        dest.writeString(referralDate);
        dest.writeString(patientId);
        dest.writeString(deleteReason);
        dest.writeString(assigneeName);
        dest.writeString(questionnaireUrl);
        if (isToShowViewDownload == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isToShowViewDownload ? 0x01 : 0x00));
        }
        dest.writeString(infoUrl);
        dest.writeString(careTaskURL);

        dest.writeString(category);
        dest.writeLong(priority);
        dest.writeString(taskSubtype);
        dest.writeString(createTime);
        dest.writeString(assignee);
        dest.writeString(vitalName);
        dest.writeString(relatedTo);
    }

    public List<CareTaskAttachemtsModel> getListAttachment() {
        return listAttachment;
    }

    public void setListAttachment(List<CareTaskAttachemtsModel> listAttachment) {
        this.listAttachment = listAttachment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getCaseLoadId() {
        return caseLoadId;
    }

    public void setCaseLoadId(long caseLoadId) {
        this.caseLoadId = caseLoadId;
    }

    public String getCareTaskTitle() {
        return careTaskTitle;
    }

    public void setCareTaskTitle(String careTaskTitle) {
        this.careTaskTitle = careTaskTitle;
    }

    public String getCareTaskDescription() {
        return careTaskDescription;
    }

    public void setCareTaskDescription(String careTaskDescription) {
        this.careTaskDescription = careTaskDescription;
    }

    public String getCarePathwayName() {
        return carePathwayName;
    }

    public void setCarePathwayName(String carePathwayName) {
        this.carePathwayName = carePathwayName;
    }

    public String getCareTaskStartDate() {
        return careTaskStartDate;
    }

    public void setCareTaskStartDate(String careTaskStartDate) {
        this.careTaskStartDate = careTaskStartDate;
    }

    public String getGetCareTaskEndDate() {
        return getCareTaskEndDate;
    }

    public void setGetCareTaskEndDate(String getCareTaskEndDate) {
        this.getCareTaskEndDate = getCareTaskEndDate;
    }

    public String getCareTaskStatus() {
        return careTaskStatus;
    }

    public void setCareTaskStatus(String careTaskStatus) {
        this.careTaskStatus = careTaskStatus;
    }

    public List<CareTaskButtons> getListButtons() {
        return listButtons;
    }

    public void setListButtons(List<CareTaskButtons> listButtons) {
        this.listButtons = listButtons;
    }

    public String getReferralDate() {
        return referralDate;
    }

    public void setReferralDate(String referralDate) {
        this.referralDate = referralDate;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getCareTaskURL() {
        return careTaskURL;
    }

    public void setCareTaskURL(String careTaskURL) {
        this.careTaskURL = careTaskURL;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }

    public String getQuestionnaireUrl() {
        return questionnaireUrl;
    }



    public void setQuestionnaireUrl(String questionnaireUrl) {
        this.questionnaireUrl = questionnaireUrl;
    }

    public boolean isToShowViewDownload() {
        if (isToShowViewDownload == null) {
            return false;
        }
        return isToShowViewDownload;
    }

    public void setToShowViewDownload(boolean toShowViewDownload) {
        isToShowViewDownload = toShowViewDownload;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public String getTaskSubtype() {
        return taskSubtype;
    }

    public void setTaskSubtype(String taskSubtype) {
        this.taskSubtype = taskSubtype;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getVitalName() {
        return vitalName;
    }

    public void setVitalName(String vitalName) {
        this.vitalName = vitalName;
    }

    public String getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        this.relatedTo = relatedTo;
    }
}
