package careplan.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.VitalsAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.MyHealth;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import appointment.MyOrdersActivity;
import appointment.fragment.MyAppointmentFragment;
import appointment.fragment.SuccessFragment;
import careplan.constants.CarePlanConstants;
import careplan.fragments.CareTaskActionFragment;
import careplan.fragments.CareTaskFragment;

/**
 * Created by LENOVO on 9/19/2016.
 */
public class CareTaskActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton btnBack, btnRight;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    private CareTaskFragment careTaskFragment;
    private String statusValue;
    private Bundle bundle;
    private boolean isFromCarePlanScreen;
    public static String IS_FROM_DRAWER = "IS_FROM_DRAWER";

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.care_task_activity, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        bundle = getIntent().getBundleExtra("bundle");

        initializeToolbar();

        careTaskFragment = new CareTaskFragment();
        careTaskFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, careTaskFragment, "caretaskfragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        btnRight = (ImageButton) mToolbar.findViewById(R.id.toolbar_right_img);
        btnRight.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (bundle != null && bundle.containsKey(CarePlanConstants.VIEW_CARE_PLAN_SCREEN)) {
            isFromCarePlanScreen = bundle.getBoolean(CarePlanConstants.VIEW_CARE_PLAN_SCREEN);
        }
        if (isFromCarePlanScreen) {
            toolbar_title.setText(bundle.getString(CarePlanConstants.CASE_LOAD_TITLE));
        } else if (isFromCarePlanScreen && BuildConfig.isPhysicianApp) {
            toolbar_title.setText(getResources().getString(R.string.care_plan_task_title));
        } else {
            toolbar_title.setText(getResources().getString(R.string.care_task_title));
        }

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) toolbar_title.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, btnRight.getId());

        toolbar_title.setLayoutParams(params);

        boolean isFromSideDrawer = getIntent().getBooleanExtra(IS_FROM_DRAWER, false);
        //if from side drawer setting hamburger icon
        if (isFromSideDrawer) {
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.hamburger));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDrawer();
                }
            });

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSupportNavigateUp();
                }
            });
//        if (getIntent().getBundleExtra("bundle") == null) {
        }
//        if (getIntent().getBundleExtra("bundle") == null) {
        btnRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_filter_list_white_36dp));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CareTaskActivity.this, CareTaskFilterActivity.class);
                i.putExtra(TextConstants.search_param, TextConstants.search_category_status);
                i.putExtra(TextConstants.param_value, statusValue);
                startActivityForResult(i, TextConstants.MENU_ITEM_SEARCH);

            }
        });
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (BuildConfig.isPatientApp) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case TextConstants.MENU_ITEM_SEARCH:
                if (resultCode == RESULT_OK) {
                    if (careTaskFragment != null && careTaskFragment instanceof CareTaskFragment) {
                        statusValue = data.getStringExtra(TextConstants.search_category_status);
                        careTaskFragment.onActivityResult(requestCode, resultCode, data);
                        return;
                    }
                }
                break;


            case CareTaskActionFragment.OBSERVATION_SELECT:
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
                if (currentFragment instanceof CareTaskFragment) {
                    currentFragment.onActivityResult(requestCode, resultCode, data);
                }
                break;

            case CareTaskActionFragment.APPOINTMENT_SELECT:
                Fragment currentFragment1 = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
                if (currentFragment1 instanceof CareTaskFragment) {
                    currentFragment1.onActivityResult(requestCode, resultCode, data);
                }
                break;

            case TextConstants.INTENT_CONSTANT:
                Fragment currentFragment2 = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
                if (currentFragment2 instanceof CareTaskFragment) {
                    currentFragment2.onActivityResult(requestCode, resultCode, data);
                }
                break;
        }
    }

}
