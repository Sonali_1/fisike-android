package careplan.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import appointment.fragment.MyAppointmentFragment;
import careplan.fragments.QuestionaryWebViewFragment;

/**
 * Created by laxmansingh on 7/19/2017.
 */

public class CareTaskAppointment extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    private MyAppointmentFragment myAppointmentFragment;
    public static boolean isSuccessful = false;
    public static Activity careTaskAppointmentActivity;
    public static List<Activity> listAppointmentActivity = new ArrayList<Activity>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.care_task_activity, frameLayout);
        careTaskAppointmentActivity = this;

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        initializeToolbar();

        myAppointmentFragment = new MyAppointmentFragment();
        myAppointmentFragment.setArguments(getIntent().getExtras());

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, myAppointmentFragment, "caretaskappointmentFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        toolbar_title.setText(getResources().getString(R.string.appointments));
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (CareTaskAppointment.isSuccessful) {
            Intent putExtra = getIntent().putExtra(VariableConstants.REFRESH_RECORDS, true);
            setResult(Activity.RESULT_OK, putExtra);
        }
        isSuccessful=false;
        listAppointmentActivity.clear();
        careTaskAppointmentActivity = null;
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}