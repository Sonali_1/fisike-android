package careplan.activity;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import careplan.constants.CarePlanConstants;

/**
 * Created by Aastha on 10/17/2016.
 */

public class RelatedLinkActivity extends BaseActivity {

    private FrameLayout frameLayout;
    private Toolbar mToolbar;
    private String link;
    private WebView webView;
    private CustomFontTextView toolbar_title;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_relatedlink, frameLayout);
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        webView= (WebView) findViewById(R.id.webView1);

        initializeToolbar();
        setProgressDialog();

        if (getIntent().hasExtra(CarePlanConstants.LINK)){
            this.link=getIntent().getStringExtra(CarePlanConstants.LINK);
            startWebView(this.link);
        }
    }

    private void setProgressDialog(){
        pd = new ProgressDialog(RelatedLinkActivity.this);
        pd.setMessage(getResources().getString(R.string.loading));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void startWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                if (!pd.isShowing()) {
                    pd.show();
                }
                return true;
            }


            public void onPageFinished(WebView view, String url) {
                try {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });
        webView.loadUrl(url);

    }


    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(getResources().getString(R.string.task_detail));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
