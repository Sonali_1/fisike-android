package careplan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.Utils;

import careplan.constants.CarePlanConstants;

public class CareTaskFilterActivity extends BaseActivity implements View.OnClickListener{

    private FrameLayout frameLayout;
    ImageButton btnFilterCross;
//            btnFilterdone;
    CustomFontButton btnReset,btnApply;
    private String searchStatus,searchCategory;
    RadioGroup radioGroup;
    RadioButton radioButtonPending,radioButtonComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_care_task_filter, frameLayout);
        searchCategory= getIntent().getStringExtra(TextConstants.search_param);
        searchCategory=(Utils.isValueAvailable(searchCategory)?searchCategory:getString(R.string.filter_status_lbl));
        searchStatus=getIntent().getStringExtra(TextConstants.param_value);
        searchStatus=(Utils.isValueAvailable(searchStatus)?searchStatus:CarePlanConstants.CARE_TASK_UNFINISHED);

        showHideToolbar(false);
        findView();
        initView();
    }

    private void findView()
    {
        btnFilterCross= (ImageButton) findViewById(R.id.btn_filter_cross);
//        btnFilterdone= (ImageButton) findViewById(R.id.btn_done_filter);
        radioGroup= (RadioGroup) findViewById(R.id.rg_status);
        btnReset = (CustomFontButton) findViewById(R.id.btnReset);
        btnApply= (CustomFontButton) findViewById(R.id.btnApply);
        radioButtonPending= (RadioButton) findViewById(R.id.rb_status_pending);
        radioButtonComplete= (RadioButton) findViewById(R.id.rb_status_complete);
    }

    private void initView()
    {
//        btnFilterdone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clickAction(v);
//            }
//        });
        btnFilterCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(v);
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(v);
            }
        });
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(v);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                searchCategory=TextConstants.search_category_status;
                switch (checkedId)
                {
                    case R.id.rb_status_pending:
                        searchStatus= CarePlanConstants.CARE_TASK_UNFINISHED;
                        break;
                    case R.id.rb_status_complete:
                        searchStatus= CarePlanConstants.CARE_TASK_FINISHED;
                        break;
                }
            }
        });

        if(searchStatus.equalsIgnoreCase(CarePlanConstants.CARE_TASK_UNFINISHED))
            radioButtonPending.setChecked(true);
        else
            radioButtonComplete.setChecked(true);
    }

    public void clickAction(View view) {
        switch (view.getId())
        {
            case R.id.btn_filter_cross:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btnReset:
                radioButtonPending.setChecked(true);
                break;
//            case R.id.btn_done_filter:
            case R.id.btnApply:
                Intent i=getIntent();
                i.putExtra(searchCategory,searchStatus);
                setResult(RESULT_OK,i);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        setResult(RESULT_CANCELED);
        finish();
    }
}
