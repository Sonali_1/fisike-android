package careplan.activity;

import android.os.Build;
import android.os.Bundle;
import android.print.PrintDocumentAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;

import careplan.constants.CarePlanConstants;
import careplan.fragments.CareTaskDetailFragment;
import careplan.fragments.QuestionaryWebViewFragment;

/**
 * Created by Kailash Khurana on 10/13/2016.
 */

public class CareTaskQuestionnaryActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private FrameLayout frameLayout;
    String title = "Task";
    public static WebView wvLoadURL;
    private QuestionaryWebViewFragment questionaryWebViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.care_task_activity, frameLayout);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE)) {
                title = getIntent().getExtras().getString(CarePlanConstants.CARE_TASK_WEBVIEW_TITLE);
            }
        }

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        initializeToolbar();

        questionaryWebViewFragment = new QuestionaryWebViewFragment();
        questionaryWebViewFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, questionaryWebViewFragment, "questionaryWebViewFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        }
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setOnClickListener(this);
        bt_toolbar_right.setText(getResources().getString(R.string.fa_cross_ico));
        bt_toolbar_right.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(title);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }

    @Override
    public void onClick(View view) {
        questionaryWebViewFragment.onClick(view);
    }

    @Override
    public void onBackPressed() {
        if (wvLoadURL.canGoBack()) {
            wvLoadURL.goBack();
            return;
        }
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}