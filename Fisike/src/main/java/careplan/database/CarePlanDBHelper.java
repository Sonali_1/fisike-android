package careplan.database;

import android.content.Context;

import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.utils.Utils;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by xmb2nc on 07-09-2016.
 */
public class CarePlanDBHelper {

    private static SQLiteDatabase database;
    private CarePlanDBManager carePlanDBManager;

    public static SQLiteDatabase getInstance(Context context) {
        if (database == null) {
            synchronized (CarePlanDBHelper.class) {
                if (database == null) {
                    database = open(context);
                }
            }
        }
        return database;
    }

    private static SQLiteDatabase open(Context context) {
        if (database == null || !database.isOpen()) {
            CarePlanDBManager carePlanDbManager = new CarePlanDBManager(context);
            try {
                database = carePlanDbManager.getWritableDatabase(Utils.generateUniqueKey(context));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return database;
    }
}
