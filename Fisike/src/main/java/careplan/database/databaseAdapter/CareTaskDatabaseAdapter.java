package careplan.database.databaseAdapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import careplan.database.CarePlanDBHelper;
import careplan.model.CareTaskAttachemtsModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;

/**
 * Created by xmb2nc on 07-09-2016.
 */
public class CareTaskDatabaseAdapter {
    public static final String CARE_TASK_TABLE_NAME = "CareTaskTable";

    private static final String TASK_ID = "TASK_ID";
    private static final String CASE_LOAD_ID = "caseLoadID";
    private static final String CARE_TASK_TITLE = "careTaskTitle";
    private static final String CARE_TASK_DESCRIPTION = "careTaskDescription";
    private static final String CARE_PATHWAY_NAME = "carePathwayName";
    private static final String CARE_TASK_START_DATE = "careTASKStartDate";
    private static final String CARE_TASK_END_DATE = "careTASKEndDate";
    private static final String CARE_TASK_STATUS = "careTASKStatus";
    private static final String ID = "ID";
    private static final String BUTTONS = "buttons";
    private static String DELETE_REASON = "deleteReason";
    private static final String QUESIONARY_URL = "quesionaryURL";
    private static final String PATIENT_ID = "patientId";
    private static final String IS_VIEW_DOWNLOAD = "isViewDownload";
    private static final String CARE_TASK_ATTACHMENT_LIST = "careTaskAttachmentlist";
    public static final String CATEGORY = "category";
    public static final String PRIORITY = "priority";
    public static final String TASKSUBTYPE = "taskSubtype";
    public static final String CREATETIME = "createTime";
    public static final String ASSIGNEE = "assignee";
    public static final String VITALNAME = "vitalName";


    private static CareTaskDatabaseAdapter careTaskDatabaseAdapter;
    private SQLiteDatabase database;

    private Context context;


    public static String getCareTaskTableName() {
        return CARE_TASK_TABLE_NAME;
    }

    private CareTaskDatabaseAdapter(Context context) {
        this.context = context;

    }

    public static CareTaskDatabaseAdapter getInstance(Context context) {
        if (careTaskDatabaseAdapter == null) {
            synchronized (CareTaskDatabaseAdapter.class) {
                if (careTaskDatabaseAdapter == null) {
                    careTaskDatabaseAdapter = new CareTaskDatabaseAdapter(context);
                    careTaskDatabaseAdapter.open();
                }
            }
        }
        return careTaskDatabaseAdapter;
    }

    public void open() throws SQLException {
        database = CarePlanDBHelper.getInstance(context);
    }

    public static String createCareTaskTableQuery() {
        String query = "CREATE TABLE " + CARE_TASK_TABLE_NAME + "(" +
                TASK_ID + " LONG PRIMARY KEY," +
                CASE_LOAD_ID + " LONG , " +
                ID + " LONG, " +
                PATIENT_ID + " varchar(255), " +
                CARE_TASK_TITLE + " varchar(255), " +
                CARE_TASK_DESCRIPTION + " varchar(255), " +
                CARE_PATHWAY_NAME + " varchar(255), " +
                CARE_TASK_START_DATE + " varchar(255), " +
                CARE_TASK_END_DATE + " varchar(255), " +
                CARE_TASK_STATUS + " varchar(255), " +
                BUTTONS + " BLOB, " +
                CARE_TASK_ATTACHMENT_LIST + " BLOB, " +
                DELETE_REASON + " varchar(255), " +
                QUESIONARY_URL + " varchar(255), " +
                CATEGORY + " varchar(255), " +
                PRIORITY + " LONG , " +
                TASKSUBTYPE + " varchar(255), " +
                CREATETIME + " varchar(255), " +
                ASSIGNEE + " varchar(255), " +
                VITALNAME + " varchar(255), " +
                IS_VIEW_DOWNLOAD + " INT )";
        return query;

    }

    public List<CareTaskModel> fetchCareTaskList() {
        Cursor mCursor = database.query(true, CARE_TASK_TABLE_NAME, null, null, null, null, null, null, null);
        List<CareTaskModel> carePlanModelList = new ArrayList<>();
        if (mCursor != null && mCursor.moveToLast()) {
            do {
                CareTaskModel careTaskModel = new CareTaskModel();
                careTaskModel.setId(mCursor.getLong(mCursor.getColumnIndex(ID)));
                careTaskModel.setPatientId(mCursor.getString(mCursor.getColumnIndex(PATIENT_ID)));
                careTaskModel.setTaskId(mCursor.getLong(mCursor.getColumnIndex(TASK_ID)));
                careTaskModel.setCaseLoadId(mCursor.getLong(mCursor.getColumnIndex(CASE_LOAD_ID)));
                careTaskModel.setCareTaskTitle(mCursor.getString(mCursor.getColumnIndex(CARE_TASK_TITLE)));
                careTaskModel.setCareTaskDescription(mCursor.getString(mCursor.getColumnIndex(CARE_TASK_DESCRIPTION)));
                careTaskModel.setDeleteReason(mCursor.getString(mCursor.getColumnIndex(DELETE_REASON)));
                careTaskModel.setCarePathwayName(mCursor.getString(mCursor.getColumnIndex(CARE_PATHWAY_NAME)));
                careTaskModel.setCareTaskStartDate(mCursor.getString(mCursor.getColumnIndex(CARE_TASK_START_DATE)));
                careTaskModel.setGetCareTaskEndDate(mCursor.getString(mCursor.getColumnIndex(CARE_TASK_END_DATE)));
                careTaskModel.setCareTaskStatus(mCursor.getString(mCursor.getColumnIndex(CARE_TASK_STATUS)));
                careTaskModel.setCategory(mCursor.getString(mCursor.getColumnIndex(CATEGORY)));
                careTaskModel.setPriority(mCursor.getLong(mCursor.getColumnIndex(PRIORITY)));
                careTaskModel.setTaskSubtype(mCursor.getString(mCursor.getColumnIndex(TASKSUBTYPE)));
                careTaskModel.setCreateTime(mCursor.getString(mCursor.getColumnIndex(CREATETIME)));
                careTaskModel.setAssignee(mCursor.getString(mCursor.getColumnIndex(ASSIGNEE)));
                careTaskModel.setVitalName(mCursor.getString(mCursor.getColumnIndex(VITALNAME)));


                byte[] listBytes = mCursor.getBlob(mCursor.getColumnIndex(BUTTONS));
                try {
                    if (listBytes != null) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(listBytes);
                        ObjectInputStream ois = null;
                        ois = new ObjectInputStream(bais);
                        List<CareTaskButtons> listVector = (List<CareTaskButtons>) ois.readObject();
                        careTaskModel.setListButtons(listVector);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                try {
                    byte[] listAttachmentBytes = mCursor.getBlob(mCursor.getColumnIndex(CARE_TASK_ATTACHMENT_LIST));
                    if (listAttachmentBytes != null) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(listAttachmentBytes);
                        ObjectInputStream ois = null;
                        ois = new ObjectInputStream(bais);
                        List<CareTaskAttachemtsModel> listVector = (List<CareTaskAttachemtsModel>) ois.readObject();
                        careTaskModel.setListAttachment(listVector);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                careTaskModel.setDeleteReason(mCursor.getString(mCursor.getColumnIndex(DELETE_REASON)));
                careTaskModel.setQuestionnaireUrl(mCursor.getString(mCursor.getColumnIndex(QUESIONARY_URL)));
                careTaskModel.setToShowViewDownload(mCursor.getInt(mCursor.getColumnIndex(IS_VIEW_DOWNLOAD)) == 0 ? false : true);
                carePlanModelList.add(careTaskModel);
            } while (mCursor.moveToPrevious());
        }
        mCursor.close();
        return carePlanModelList;
    }

    public void insertCareTaskList(List<Object> careTaskModels, boolean isSycApi) {
        if (isSycApi) {
//            for (int i = 0; i < careTaskModels.size(); i++) {
//                if (careTaskModels.get(i) instanceof CareTaskModel) {
//                    CareTaskModel careTaskModel = (CareTaskModel) careTaskModels.get(i);
//
//                    deleteCareTask(careTaskModel);
//                    insertCareTask(careTaskModel);
//                }
//            }
            try {
                database.delete(CareTaskDatabaseAdapter.getCareTaskTableName(), null, null);
            } catch (Exception e) {
                AppLog.d("Truncate Table", CareTaskDatabaseAdapter.getCareTaskTableName() + " No SUCH TABLE FOUND");
            }

        }

        for (int i = 0; i < careTaskModels.size(); i++) {
            if (careTaskModels.get(i) instanceof CareTaskModel) {
                CareTaskModel careTaskModel = (CareTaskModel) careTaskModels.get(i);
                insertCareTask(careTaskModel);
            }
        }
    }

    private void deleteCareTask(CareTaskModel careTaskModel) {
        try {
            database.delete(CARE_TASK_TABLE_NAME, TASK_ID + "=" + careTaskModel.getTaskId(), null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertCareTask(CareTaskModel careTaskModel) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, careTaskModel.getId());
            contentValues.put(CASE_LOAD_ID, careTaskModel.getCaseLoadId());
            contentValues.put(TASK_ID, careTaskModel.getTaskId());
            contentValues.put(CARE_TASK_TITLE, careTaskModel.getCareTaskTitle());
            contentValues.put(CARE_TASK_DESCRIPTION, careTaskModel.getCareTaskDescription());
            contentValues.put(PATIENT_ID, careTaskModel.getPatientId());
            contentValues.put(CARE_TASK_STATUS, careTaskModel.getCareTaskStatus());
            contentValues.put(CARE_PATHWAY_NAME, careTaskModel.getCarePathwayName());
            contentValues.put(CARE_TASK_START_DATE, careTaskModel.getCareTaskStartDate());
            contentValues.put(CARE_TASK_END_DATE, careTaskModel.getGetCareTaskEndDate());
            contentValues.put(QUESIONARY_URL, careTaskModel.getQuestionnaireUrl());
            contentValues.put(DELETE_REASON, careTaskModel.getDeleteReason());
            if (careTaskModel.getListButtons() != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(careTaskModel.getListButtons());
                contentValues.put(BUTTONS, bytes.toByteArray());
            }

            contentValues.put(CASE_LOAD_ID, careTaskModel.getCaseLoadId());
            contentValues.put(CARE_PATHWAY_NAME, careTaskModel.getCarePathwayName());
            contentValues.put(IS_VIEW_DOWNLOAD, careTaskModel.isToShowViewDownload() ? 1 : 0);

            contentValues.put(CATEGORY, careTaskModel.getCategory());
            contentValues.put(PRIORITY, careTaskModel.getPriority());
            contentValues.put(TASKSUBTYPE, careTaskModel.getTaskSubtype());
            contentValues.put(CREATETIME, careTaskModel.getCreateTime());
            contentValues.put(ASSIGNEE, careTaskModel.getAssignee());
            contentValues.put(VITALNAME, careTaskModel.getVitalName());

            //          the row ID of the newly inserted row, or -1 if an error occurred

            long te = database.insert(CARE_TASK_TABLE_NAME, null, contentValues);
            AppLog.d("logn....insert", "" + te);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCareTaskAttachmentList() {
        return CARE_TASK_ATTACHMENT_LIST;
    }
}
