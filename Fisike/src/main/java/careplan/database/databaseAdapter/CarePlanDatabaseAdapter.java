package careplan.database.databaseAdapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


import com.mphrx.fisike_physician.utils.AppLog;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import careplan.model.ExternalURLModel;
import careplan.database.CarePlanDBHelper;
import careplan.model.CarePlanModel;

/**
 * Created by xmb2nc on 07-09-2016.
 */
public class CarePlanDatabaseAdapter {
    private static final String CARE_PLAN_TABLE_NAME = "CarePlanTable";

    private static final String CASE_LOAD_ID = "caseLoadID";
    private static final String CARE_PLAN_TITLE = "carePlanTitle";
    private static final String CARE_PLAN_DESCRIPTION = "carePlanDescription";
    private static final String CARE_PLAN_START_DATE = "carePlanStartDate";
    private static final String CARE_PLAN_STATUS = "carePlanStatus";
    private static final String CARE_PLAN_DOCUMENT_TYPE = "carePlanDocumentType";
    private static final String ID = "ID";
    private static final String DOCUMENT_ID = "documentId";

    private static final String CARE_PLAN_URL = "carePlanURL";
    private static CarePlanDatabaseAdapter carePlanDatabaseAdapter;
    private SQLiteDatabase database;

    private Context context;

    private CarePlanDatabaseAdapter(Context context) {
        this.context = context;

    }

    public static CarePlanDatabaseAdapter getInstance(Context context) {
        if (carePlanDatabaseAdapter == null) {
            synchronized (CarePlanDatabaseAdapter.class) {
                if (carePlanDatabaseAdapter == null) {
                    carePlanDatabaseAdapter = new CarePlanDatabaseAdapter(context);
                    carePlanDatabaseAdapter.open();
                }
            }
        }
        return carePlanDatabaseAdapter;
    }

    public void open() throws SQLException {
        database = CarePlanDBHelper.getInstance(context);
    }

    public static String getCarePlanTableName() {
        return CARE_PLAN_TABLE_NAME;
    }

    public static String createCarePlanTableQuery() {
        String query = "CREATE TABLE " + CARE_PLAN_TABLE_NAME + "(" +
                ID + " INTEGER PRIMARY KEY," +
                CASE_LOAD_ID + " INTEGER , " +
                CARE_PLAN_TITLE + " varchar(255), " +
                CARE_PLAN_DESCRIPTION + " varchar(255), " +
                CARE_PLAN_START_DATE + " varchar(255), " +
                CARE_PLAN_STATUS + " varchar(255), " +
                CARE_PLAN_DOCUMENT_TYPE + " varchar(255), " +
                CARE_PLAN_URL + " varchar(255), " +
                DOCUMENT_ID + " varchar(255))";
        return query;

    }

    public void truncateDB() {
        try {
            deleteTable(CarePlanDatabaseAdapter.getCarePlanTableName(), null, null);
            deleteTable(CareTaskDatabaseAdapter.getCareTaskTableName(), null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteTable(String tableName, String whereClause, String[] whereArgs) {
        try {
            database.delete(tableName, whereClause, whereArgs);
        } catch (Exception e) {
            AppLog.d("Truncate Table", tableName + " No SUCH TABLE FOUND");
        }
    }

    public List<CarePlanModel> fetchCarePlanList() {
        Cursor mCursor = database.query(true, CARE_PLAN_TABLE_NAME, null, null, null, null, null, null, null);
        List<CarePlanModel> carePlanModelList = new ArrayList<>();
        if (mCursor != null && mCursor.moveToLast()) {
            do {
                CarePlanModel carePlanModel = new CarePlanModel();
                carePlanModel.setId(mCursor.getLong(mCursor.getColumnIndex(ID)));
                carePlanModel.setCaseLoadId(mCursor.getLong(mCursor.getColumnIndex(CASE_LOAD_ID)));
                carePlanModel.setCarePlanTitle(mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_TITLE)));
                carePlanModel.setDescription(mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_DESCRIPTION)));
                carePlanModel.setStartDate(mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_START_DATE)));
                carePlanModel.setStatus(mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_STATUS)));
                carePlanModel.setExternalURlModel(new ExternalURLModel(mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_URL)), mCursor.getString(mCursor.getColumnIndex(CARE_PLAN_DOCUMENT_TYPE))));
                carePlanModel.setDocumentId(mCursor.getString(mCursor.getColumnIndex(DOCUMENT_ID)));
                carePlanModelList.add(carePlanModel);
            } while (mCursor.moveToPrevious());
        }
        mCursor.close();
        return carePlanModelList;
    }

    public void insertCarePlan(List<CarePlanModel> listCarePlanModel, boolean isSycApi) {
        if (isSycApi) {
            for (int i = 0; i < listCarePlanModel.size(); i++) {
                deleteCarePlan(listCarePlanModel.get(i));
                insertCarePlan(listCarePlanModel.get(i));
            }

        } else {

            for (int i = 0; i < listCarePlanModel.size(); i++) {
                insertCarePlan(listCarePlanModel.get(i));
            }
        }
    }

    private void deleteCarePlan(CarePlanModel carePlanModel) {
        database.delete(CARE_PLAN_TABLE_NAME, ID + "=" + carePlanModel.getId(), null);
    }

    private void insertCarePlan(CarePlanModel carePlanModel) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, carePlanModel.getId());
            contentValues.put(CASE_LOAD_ID, carePlanModel.getCaseLoadId());
            contentValues.put(CARE_PLAN_TITLE, carePlanModel.getCarePlanTitle());
            contentValues.put(CARE_PLAN_DESCRIPTION, carePlanModel.getDescription());
            contentValues.put(CARE_PLAN_START_DATE, carePlanModel.getStartDate());
            contentValues.put(CARE_PLAN_STATUS, carePlanModel.getStatus());
            contentValues.put(CARE_PLAN_DOCUMENT_TYPE, carePlanModel.getExternalURlModel().getType());
            contentValues.put(CARE_PLAN_URL, carePlanModel.getExternalURlModel().getExternalURLPath());
            contentValues.put(DOCUMENT_ID, carePlanModel.getDocumentId());
            database.insert(CARE_PLAN_TABLE_NAME, null, contentValues);
        } catch (Exception e) {
        }
    }
}
