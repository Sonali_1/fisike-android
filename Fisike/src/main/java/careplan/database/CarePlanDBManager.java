package careplan.database;

import android.content.Context;

import com.mphrx.fisike.persistence.PatientDBAdapter;
import com.mphrx.fisike.persistence.UserDBAdapter;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import careplan.database.databaseAdapter.CarePlanDatabaseAdapter;
import careplan.database.databaseAdapter.CareTaskDatabaseAdapter;

/**
 * Created by xmb2nc on 07-09-2016.
 */
public class CarePlanDBManager extends SQLiteOpenHelper {
    private static final String DB_FILE_NAME = "carePlan.db";
    private static final int DB_VERSION = 1;


    public CarePlanDBManager(Context context) {
        super(context, DB_FILE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CarePlanDatabaseAdapter.createCarePlanTableQuery());
        sqLiteDatabase.execSQL(CareTaskDatabaseAdapter.createCareTaskTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;

        switch (oldVersion) {
            case 1:
                upgradeVersionTwo(db);
                break;
        }
    }

    private void upgradeVersionTwo(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + CareTaskDatabaseAdapter.getCareTaskTableName() +
                " ADD " + CareTaskDatabaseAdapter.getCareTaskAttachmentList() + " BLOB");
    }

}
