package careplan.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import careplan.constants.CarePlanConstants;
import careplan.constants.URLConstants;
import careplan.response.CareTaskResponse;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskRequest extends BaseObjectRequest {
    private long transactionId;
    private Context context;
    private long caseLoadId;
    private boolean isSynApi;
    private boolean isFromCarePlanScreen;
    private String status;

    public CareTaskRequest(long transactionId, Context context, long caseLoadId, boolean isSynApi, boolean isFromCarePlanScreen, String status) {
        this.transactionId = transactionId;
        this.context = context;
        this.caseLoadId = caseLoadId;
        this.isSynApi = isSynApi;
        this.isFromCarePlanScreen = isFromCarePlanScreen;
        this.status = status;
    }

    @Override
    public void doInBackground() {
        try {
            if (caseLoadId != 0) {
                String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_WITH_PLAN_API_CONSTANT;

                AppLog.showInfo(getClass().getSimpleName(), baseUrl);
//                APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoadTask(), this, this);
                APIManager apiManager = APIManager.getInstance();
                String response = apiManager.executeHttpPost(baseUrl, SharedPref.getAccessToken(), getPayLoadTask(), context);
                try {
                    JSONArray jsonElements;
                    try {
                        JSONObject responseObject = new JSONObject(response);
                        jsonElements = responseObject.getJSONArray("returnTasks");
                    } catch (Exception e) {
                        jsonElements = new JSONArray(response);
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("tasks", jsonElements);
                    AppLog.showInfo(getClass().getSimpleName(), response.toString());
                    BusProvider.getInstance().post(new CareTaskResponse(jsonObject, transactionId, context, isSynApi, isFromCarePlanScreen, caseLoadId));
                } catch (Exception e) {
                    AppLog.showError(getClass().getSimpleName(), "");
                    BusProvider.getInstance().post(new CareTaskResponse(e, transactionId, isSynApi));
                }
//                Network.getGeneralRequestQueue().add(request);
            } else {
                String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_API_CONSTANT;
                AppLog.showInfo(getClass().getSimpleName(), baseUrl);
                APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad().toString(), this, this);
                Network.getGeneralRequestQueue().add(request);
            }

        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CareTaskResponse(error, transactionId, isSynApi));
    }

    @Override
    public void onResponse(JSONObject
                                   response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        if (Utils.isValueAvailable(status))
            BusProvider.getInstance().post(new CareTaskResponse(response, transactionId, context, isSynApi, isFromCarePlanScreen, caseLoadId, true));
        else
            BusProvider.getInstance().post(new CareTaskResponse(response, transactionId, context, isSynApi, isFromCarePlanScreen, caseLoadId));

    }

    private JSONObject getPayLoad() throws JSONException {
        JSONObject fetchCareTaskJsonObject = new JSONObject();
        JSONObject processVariableJsonObject = new JSONObject();
        if (caseLoadId != 0) {
            processVariableJsonObject.put("carePlanId", String.valueOf(caseLoadId));
            fetchCareTaskJsonObject.put("processVariables", processVariableJsonObject);
        }

        fetchCareTaskJsonObject.put("taskName", "");
        if (Utils.isValueAvailable(status))
            fetchCareTaskJsonObject.put("status", status);
        else
            fetchCareTaskJsonObject.put("status", "unfinished"); //finished

        fetchCareTaskJsonObject.put("offset", 0);
        fetchCareTaskJsonObject.put("max", 100);
        fetchCareTaskJsonObject.put("sortDir", "desc");
        fetchCareTaskJsonObject.put("sortMethod", "orderByHistoricTaskInstanceStartTime");
        fetchCareTaskJsonObject.put("sortField", "startTime");
        return fetchCareTaskJsonObject;
    }

    private JSONObject getPayLoadTask() throws JSONException {
        JSONObject fetchCareTaskJsonObject = new JSONObject();
        JSONObject processVariableJsonObject = new JSONObject();
        if (caseLoadId != 0) {
            fetchCareTaskJsonObject.put("workflow`carePlanId", String.valueOf(caseLoadId));
            if (BuildConfig.isPatientApp) {
                processVariableJsonObject.put("assignedTo", "taskCandidateOrAssigned");
            }
        }
        processVariableJsonObject.put("taskVariable", fetchCareTaskJsonObject);
        if (Utils.isValueAvailable(status) && (status.equalsIgnoreCase(CarePlanConstants.COMPLETE_STATUS) || status.equalsIgnoreCase(CarePlanConstants.CANCEL_STATUS) || status.equalsIgnoreCase(CarePlanConstants.FILTER_STATUS_FINISHED)))
            processVariableJsonObject.put("showCompleted", true);
        else
            processVariableJsonObject.put("showCompleted", false); //finished
        return processVariableJsonObject;
    }
}
