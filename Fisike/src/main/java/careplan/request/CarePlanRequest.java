package careplan.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import careplan.constants.URLConstants;
import careplan.database.databaseAdapter.CarePlanDatabaseAdapter;
import careplan.enums.ObserverOperationEnum;
import careplan.model.CarePlanModel;
import careplan.observable.CarePlanObservable;
import careplan.response.CarePlanSearchResponse;

/**
 * Created by xmb2nc on 02-09-2016.
 */
public class CarePlanRequest extends BaseObjectRequest {
    private long transactionId;
    private Context context;
    private boolean isSyncApi;
    private long patientId;

    public CarePlanRequest(long transactionId, Context context, boolean isSyncApi, long patientId) {
        this.transactionId = transactionId;
        this.context = context;
        this.isSyncApi = isSyncApi;
        this.patientId = patientId;
    }

    @Override
    public void doInBackground() {
        try {
            String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_PLAN_SEARCH_API_CONSTANT;
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(), this, this);
            Network.getGeneralRequestQueue().add(request);
        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        CarePlanSearchResponse carePlanSearchResponse = new CarePlanSearchResponse(error, transactionId);

        ObserverOperationEnum insert = ObserverOperationEnum.INSERT;
        CarePlanObservable.getInstance().setData(null, insert.getOpenation(), false, carePlanSearchResponse.getMessage());
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        final CarePlanSearchResponse carePlanSearchResponse = new CarePlanSearchResponse(response, transactionId, context);

        List<CarePlanModel> carePlanList = carePlanSearchResponse.getCarePlanList();
        if (carePlanList == null || carePlanList.size() == 0) {
            ObserverOperationEnum insert = ObserverOperationEnum.INSERT;
            CarePlanObservable.getInstance().setData(carePlanList, insert.getOpenation(), true, carePlanSearchResponse.getMessage());
            return;
        }
        CarePlanDatabaseAdapter.getInstance(context).insertCarePlan(carePlanSearchResponse.getCarePlanList(), isSyncApi);
        long totalActiveCarePlansCount = carePlanSearchResponse.getTotalActiveCarePlansCount();
        if (totalActiveCarePlansCount > -1) {
            SharedPref.setTotalActiveCarePlansCount(totalActiveCarePlansCount);
        }

        ObserverOperationEnum insert = ObserverOperationEnum.INSERT;
        CarePlanObservable.getInstance().setData(carePlanList, insert.getOpenation(), true, "");
    }


    private String getPayLoad() throws JSONException {
        JSONObject fetchCarePlanJsonObject = new JSONObject();
        /*fetchCarePlanJsonObject.put("_count", 10);*/
        fetchCarePlanJsonObject.put("_skip", 0);
        fetchCarePlanJsonObject.put("subject", patientId);
        fetchCarePlanJsonObject.put("_sort:desc", "period.startDate");
        JSONObject constraintsJsonObject = new JSONObject();
        constraintsJsonObject.put("constraints", fetchCarePlanJsonObject);
        return constraintsJsonObject.toString();
    }
}
