package careplan.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import careplan.constants.URLConstants;
import careplan.response.CareTaskQuetionnaireResponse;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskQuetionnaireRequest extends BaseObjectRequest {
    private long transactionId;
    private Context context;
    private String questionnaireId;

    public CareTaskQuetionnaireRequest(long transactionId, Context context, String questionnaireId) {
        this.transactionId = transactionId;
        this.context = context;
        this.questionnaireId = questionnaireId;
    }

    @Override
    public void doInBackground() {
        try {
            String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_QUESTIONNAIRE;
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad().toString(), this, this);
            Network.getGeneralRequestQueue().add(request);

        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CareTaskQuetionnaireResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new CareTaskQuetionnaireResponse(response, transactionId, context));

    }

    private JSONObject getPayLoad() throws JSONException {
        JSONObject fetchCareTaskJsonObject = new JSONObject();
        fetchCareTaskJsonObject.put("patientId", SettingManager.getInstance().getUserMO().getPatientId());
        fetchCareTaskJsonObject.put("questionnaireId", questionnaireId);
        return fetchCareTaskJsonObject;
    }
}
