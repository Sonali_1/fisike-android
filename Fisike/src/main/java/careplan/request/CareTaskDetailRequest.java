package careplan.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import careplan.constants.CarePlanConstants;
import careplan.constants.URLConstants;
import careplan.database.databaseAdapter.CarePlanDatabaseAdapter;
import careplan.enums.ObserverOperationEnum;
import careplan.model.CarePlanModel;
import careplan.observable.CarePlanObservable;
import careplan.response.CarePlanSearchResponse;
import careplan.response.CareTaskDetailResponse;
import careplan.response.CareTaskResponse;

/**
 * Created by Kailash Khurana on 10/17/2016.
 */

public class CareTaskDetailRequest extends BaseObjectRequest {
    private long transactionId;
    private Context context;
    private static long careTaskId;
    private String taskStatus;

    public CareTaskDetailRequest(long transactionId, Context context, long careTaskId, String taskStatus) {
        this.transactionId = transactionId;
        this.context = context;
        this.careTaskId = careTaskId;
        this.taskStatus = taskStatus;
    }

    @Override
    public void doInBackground() {
        try {
            String taskUrl = taskStatus == null || !taskStatus.equalsIgnoreCase(CarePlanConstants.FILTER_STATUS_FINISHED) ? URLConstants.CARE_TASK_DETAIL_API_CONSTANT : URLConstants.CARE_TASK_DETAIL_COMPLETE_API_CONSTANT;
            String baseUrl = APIManager.createMinervaBaseUrl() + taskUrl;
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, getPayLoad(careTaskId), this, this);
            Network.getGeneralRequestQueue().add(request);
        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CareTaskDetailResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new CareTaskDetailResponse(response, transactionId, context));
    }

    public static String getPayLoad(long careTaskId) throws JSONException {
        JSONObject fetchCarePlanJsonObject = new JSONObject();
        fetchCarePlanJsonObject.put("taskId", String.valueOf(careTaskId));
        return fetchCarePlanJsonObject.toString();
    }

}
