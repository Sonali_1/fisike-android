package careplan.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import careplan.constants.URLConstants;
import careplan.response.CareTaskEditResponse;
import careplan.response.CareTaskSubmitResponse;

/**
 * Created by Kailash Khurana on 9/15/2016.
 */
public class CareTaskEdit extends BaseObjectRequest {
    private long transactionId;
    private Context context;
    private String payloadJSON;
    private long id;

    public CareTaskEdit(long transactionId, Context context, String payloadJSON, long id) {
        this.transactionId = transactionId;
        this.context = context;
        this.payloadJSON = payloadJSON;
        this.id = id;
    }

    @Override
    public void doInBackground() {
        try {
            String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_EDIT_API_CONSTANT;
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, payloadJSON, this, this);
            Network.getGeneralRequestQueue().add(request);
        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new CareTaskEditResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new CareTaskEditResponse(response, id, transactionId, context));

        try {
            String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.CARE_TASK_DETAIL_API_CONSTANT;
            String payLoad = null;
            payLoad = CareTaskDetailRequest.getPayLoad(id);
            APIObjectRequest apiObjectRequest = new APIObjectRequest(Request.Method.POST, baseUrl, payLoad, this, this);
            Network.getGeneralRequestQueue().getCache().remove(apiObjectRequest.getCacheKey());
        } catch (JSONException e) {
        }
    }

}

