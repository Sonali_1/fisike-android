package careplan.response;

import android.content.Context;

import com.mphrx.fisike_physician.network.response.BaseResponse;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskSubmitResponse extends BaseResponse {

    private boolean isSuccess;
    private long id;

    public CareTaskSubmitResponse(JSONObject response, long id, long transactionId,
                                  Context context) {
        super(response, transactionId);
        this.id = id;
        isSuccess = true;
    }

    public CareTaskSubmitResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        isSuccess = false;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public long getId() {
        return id;
    }
}
