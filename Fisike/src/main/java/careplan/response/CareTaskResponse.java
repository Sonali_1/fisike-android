package careplan.response;

import android.content.Context;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike_physician.network.response.BaseResponse;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import careplan.constants.CarePlanConstants;
import careplan.database.databaseAdapter.CareTaskDatabaseAdapter;
import careplan.model.CareTaskAttachemtsModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskModel;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskResponse extends BaseResponse {
    private Context context;
    private boolean isSyncApi;
    private boolean isFromCarePlanScreen;
    private List<Object> careTaskModelList;
    private boolean isError;
    private long caseLoadId;
    private boolean isToShowViewDownload;
    private boolean isFromSearch = false;

    public CareTaskResponse(JSONObject response, long transactionId,
                            Context context, boolean isSyncApi, boolean isFromCarePlanScreen, long caseLoadId) {
        super(response, transactionId);
        this.context = context;
        this.isSyncApi = isSyncApi;
        this.isFromCarePlanScreen = isFromCarePlanScreen;
        this.isError = false;
        this.caseLoadId = caseLoadId;
        parseResponse(response);
    }

    public CareTaskResponse(JSONObject response, long transactionId,
                            Context context, boolean isSyncApi, boolean isFromCarePlanScreen, long caseLoadId, boolean isFromSearch) {
        super(response, transactionId);
        this.context = context;
        this.isSyncApi = isSyncApi;
        this.isFromCarePlanScreen = isFromCarePlanScreen;
        this.isError = false;
        this.caseLoadId = caseLoadId;
        this.isFromSearch = isFromSearch;
        parseResponse(response);
    }

    public CareTaskResponse(Exception exception, long transactionId, boolean isSyncApi) {
        super(exception, transactionId);
        this.isSyncApi = isSyncApi;
        this.isError = true;
    }

    public boolean isError() {
        return isError;
    }

    private void parseResponse(JSONObject response) {
        try {
            careTaskModelList = new ArrayList<>();
            JSONObject careTaskJsonObject = new JSONObject(response.toString());
            JSONArray careTaskJsonArray = careTaskJsonObject.getJSONArray("tasks");
            long totalActiveCareTaskCount = careTaskJsonObject.optLong("totalTaskCount");
            if (totalActiveCareTaskCount > -1) {
                SharedPref.setTotalActiveCareTaskCount(totalActiveCareTaskCount);
            }
            parseCareTaskJsonArray(careTaskJsonArray);
        } catch (JSONException jsonException) {
        }
    }

    private void parseCareTaskJsonArray(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONObject processVariable = jsonObject.optJSONObject("processVariable");
            JSONObject taskVariable = jsonObject.optJSONObject("taskVariable");
            if (taskVariable == null) {
                taskVariable = jsonObject.optJSONObject("taskVariables");
            }

            JSONArray attachments = jsonObject.optJSONArray("taskAttachments");
            isToShowViewDownload = attachments == null || attachments.length() < 1 ? false : true;

            ArrayList<CareTaskAttachemtsModel> careTaskAttachemtsModelArrayList = new ArrayList<>();
            for (int j = 0; attachments != null && j < attachments.length(); j++) {
                CareTaskAttachemtsModel careTaskAttachemtsModel = new CareTaskAttachemtsModel();
                JSONObject attachmentObject = attachments.getJSONObject(j);
                careTaskAttachemtsModel.setContentId(attachmentObject.getString("contentId"));
                careTaskAttachemtsModel.setId(attachmentObject.getString("id"));
                JSONObject persistentState = attachmentObject.optJSONObject("persistentState");
                careTaskAttachemtsModel.setFileName(persistentState.optString("name"));
                careTaskAttachemtsModel.setDescription(persistentState.optString("description"));
                careTaskAttachemtsModel.setType(attachmentObject.optString("type"));
                if (careTaskAttachemtsModel.getType().contains("image")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_IMAGE);
                } else if (careTaskAttachemtsModel.getType().contains("pdf")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_FILE);
                } else if (careTaskAttachemtsModel.getType().contains("application/msword")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_DOC);
                } else if (careTaskAttachemtsModel.getType().contains("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_DOCX);
                } else if (careTaskAttachemtsModel.getType().contains("text/plain")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_TEXT);
                } else {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_UNKNOWN);
                }
                careTaskAttachemtsModelArrayList.add(careTaskAttachemtsModel);
            }

            CareTaskModel careTaskModel = new CareTaskModel();
            careTaskModel.setListAttachment(careTaskAttachemtsModelArrayList);
            careTaskModel.setToShowViewDownload(isToShowViewDownload);
            careTaskModel.setTaskId(jsonObject.optLong("id"));
            careTaskModel.setDeleteReason(jsonObject.optString("deleteReason"));
            careTaskModel.setCareTaskStartDate(jsonObject.optString("startTime"));
            careTaskModel.setCareTaskDescription(jsonObject.optString("description"));
            careTaskModel.setCategory(jsonObject.optString("category"));
            careTaskModel.setPriority(jsonObject.optLong("priority"));
            careTaskModel.setTaskSubtype(jsonObject.optString("taskSubtype"));
            careTaskModel.setCreateTime(jsonObject.optString("createTime"));
            careTaskModel.setAssignee(jsonObject.optString("assignee"));


            JSONObject assigneeNameJsonObject = jsonObject.optJSONObject("assigneeName");
            if (assigneeNameJsonObject != null) {
                String firsName = assigneeNameJsonObject.optString("firstName");
                String lastName = assigneeNameJsonObject.optString("lastName");
                String Name = firsName + " " + ((lastName != null && !lastName.equals(context.getResources().getString(R.string.txt_null))) ? lastName.trim() : "");
                careTaskModel.setAssigneeName(Name.trim());
            }
            careTaskModel.setGetCareTaskEndDate(jsonObject.optString("dueDate"));

            if (processVariable != null) {
                if (careTaskModel.getGetCareTaskEndDate() == null || careTaskModel.getGetCareTaskEndDate().equals("") || careTaskModel.getGetCareTaskEndDate().equals(context.getResources().getString(R.string.txt_null))) {
                    careTaskModel.setGetCareTaskEndDate(processVariable.optString("dueDate"));
                }
                careTaskModel.setCaseLoadId(processVariable.optLong("caseLoadId1"));
                careTaskModel.setCarePathwayName(processVariable.optString("carePathwayName"));
            }

            if (careTaskModel.getGetCareTaskEndDate() == null || careTaskModel.getGetCareTaskEndDate().equals("") || careTaskModel.getGetCareTaskEndDate().equals(context.getResources().getString(R.string.txt_null))) {
                careTaskModel.setGetCareTaskEndDate(taskVariable.optString("dueDate"));
            }

            careTaskModel.setInfoUrl(taskVariable.optString("infoUrl"));
            careTaskModel.setCareTaskTitle(taskVariable.optString("taskTitle"));
            careTaskModel.setCareTaskStatus(taskVariable.optString("taskStatus"));


            careTaskModel.setVitalName(taskVariable.optString("vitalName"));

            if (processVariable != null) {
                String vitalName = careTaskModel.getVitalName();
                String category = careTaskModel.getCategory();

                if (vitalName == null || vitalName.equals("") || vitalName.equals(context.getResources().getString(R.string.txt_null))) {
                    careTaskModel.setVitalName(processVariable.optString("vitalName"));
                }

                if (category == null || category.equals("") || category.equals(context.getResources().getString(R.string.txt_null))) {
                    careTaskModel.setCategory(processVariable.optString("category"));
                }
            }


            String careTaskTitle = careTaskModel.getCareTaskTitle();
            if (careTaskTitle == null || careTaskTitle.equals("") || careTaskTitle.equals(context.getResources().getString(R.string.txt_null))) {
                careTaskModel.setCareTaskTitle(jsonObject.optString("name"));
            }
            JSONArray btnsList = taskVariable.optJSONArray("buttons");
            List<CareTaskButtons> careTaskBtnList = new ArrayList<CareTaskButtons>();
            for (int j = 0; btnsList != null && j < btnsList.length(); j++) {
                JSONObject btnObject = btnsList.getJSONObject(j);
                CareTaskButtons careTaskButton = new CareTaskButtons();
                careTaskButton.setButtonAction(btnObject.optString("buttonAction"));
                careTaskButton.setButtonId(btnObject.optString("buttonId"));
                careTaskButton.setButtonTitle(btnObject.optString("buttonTitle"));
                careTaskButton.setButtonTag(btnObject.optString("buttonTag"));
                careTaskButton.setButtonValue(btnObject.optString("buttonValue"));
                careTaskBtnList.add(careTaskButton);
            }
            String taskStatus = null;
            if (taskVariable != null) {
                taskStatus = taskVariable.optString("taskStatus");
            }
            careTaskModel.setCareTaskStatus(taskStatus == null || taskStatus.equalsIgnoreCase(context.getResources().getString(R.string.txt_null)) ? CarePlanConstants.PENDING_STATUS : taskStatus);
            careTaskModel.setListButtons(careTaskBtnList);
            Iterator<String> iter = taskVariable.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                /*if (key.contains("infoUrl")) {
                    careTaskModel.setInfoUrl(taskVariable.optString(key));
                } else*/
                if (key.contains("questionnaireUrl")) {
                    careTaskModel.setQuestionnaireUrl(taskVariable.optString(key));
                } else if (key.contains("Url") || key.contains("url")) {
                    if (careTaskBtnList.size() > 0) {
                        if (careTaskBtnList.get(0).getButtonAction().equals("Questionnaire/show") || careTaskBtnList.get(0).getButtonTitle().equals("FillQuestionnaire")) {
                            careTaskModel.setQuestionnaireUrl(taskVariable.optString(key));
                        } else {
                            careTaskModel.setCareTaskURL(taskVariable.optString(key));
                        }
                    }
                }
            }


            if (jsonObject.has("relatedTo") && jsonObject.optJSONObject("relatedTo") != null) {
                JSONObject relatedJsonObject = jsonObject.optJSONObject("relatedTo");

                JSONArray lowerCaseNameJsonArray = relatedJsonObject.optJSONArray("lowerCaseName");
                if (lowerCaseNameJsonArray.length() > 0) {
                    String relatedtoName = lowerCaseNameJsonArray.getJSONObject(0).optString("text");
                    careTaskModel.setRelatedTo(relatedtoName);
                }
            }

            careTaskModelList.add(careTaskModel);
        }

        if (SharedPref.getTotalActiveCareTaskCount() == 0) {
            SharedPref.setTotalActiveCareTaskCount(careTaskModelList.size());
        }


        if (caseLoadId == 0 && !isFromCarePlanScreen && !isFromSearch) {
            CareTaskDatabaseAdapter.getInstance(context).insertCareTaskList(careTaskModelList, isSyncApi);
        }
    }

    public List<Object> getCareTaskList() {
        return careTaskModelList;
    }

    public boolean isSyncApi() {
        return isSyncApi;
    }
}
