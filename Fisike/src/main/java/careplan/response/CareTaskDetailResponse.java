package careplan.response;


import android.content.Context;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import careplan.constants.CarePlanConstants;
import careplan.model.CareTaskAttachemtsModel;
import careplan.model.CareTaskButtons;
import careplan.model.CareTaskDetailModel;

/**
 * Created by Kailash Khurana on 10/17/2016.
 */

public class CareTaskDetailResponse extends BaseResponse {

    private boolean isError;
    private Context context;
    private CareTaskDetailModel careTaskDetailModel;

    public CareTaskDetailResponse(JSONObject response, long transactionId, Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public CareTaskDetailResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        this.isError = true;
    }

    private void parseResponse(JSONObject response) {
        careTaskDetailModel = new CareTaskDetailModel();
        try {
            int priority = response.optInt("priority");
            if (priority == 0) {
                careTaskDetailModel.setPriority(context.getResources().getString(R.string.Low));
            } else if (priority == 50) {
                careTaskDetailModel.setPriority(context.getResources().getString(R.string.Medium));
            } else if (priority == 100) {
                careTaskDetailModel.setPriority(context.getResources().getString(R.string.High));
            }
            JSONObject taskVariablesObject = response.optJSONObject("taskVariables");
            JSONObject processVariables = response.getJSONObject("processVariables");
            JSONObject assigneeNameObject = response.optJSONObject("assigneeName");
            JSONObject reportedByObject = response.optJSONObject("ownerName");

            String firstNameAssigneeName = assigneeNameObject.optString("firstName");
            String lastNameAssigneeName = assigneeNameObject.optString("lastName");
            String name = firstNameAssigneeName != null && !firstNameAssigneeName.equals(context.getResources().getString(R.string.txt_null)) ? firstNameAssigneeName + " " : "";
            name += lastNameAssigneeName != null && !lastNameAssigneeName.equals(context.getResources().getString(R.string.txt_null)) ? lastNameAssigneeName : "";
            careTaskDetailModel.setAssignedTo(name.trim());

            String firstNameReportedBy = reportedByObject.optString("firstName");
            String lastNameReportedBy = reportedByObject.optString("lastName");
            String reportedByName = firstNameReportedBy != null && !firstNameReportedBy.equals(context.getResources().getString(R.string.txt_null)) ? firstNameReportedBy + " " : "";
            reportedByName += lastNameReportedBy != null && !lastNameReportedBy.equals(context.getResources().getString(R.string.txt_null)) ? lastNameReportedBy : "";
            careTaskDetailModel.setReportedBy(reportedByName.trim());

            careTaskDetailModel.setCarePathName(processVariables.optString("carePathwayName"));
            careTaskDetailModel.setCareTaskName(taskVariablesObject.optString("title"));

            if (careTaskDetailModel.getCareTaskName() == null || careTaskDetailModel.getCareTaskName().equals("") || careTaskDetailModel.getCareTaskName().equals(context.getResources().getString(R.string.txt_null))) {
                careTaskDetailModel.setCareTaskName(response.optString("name"));
            }
            if (careTaskDetailModel.getDescription() == null || careTaskDetailModel.getDescription().equals("") || careTaskDetailModel.getDescription().equals(context.getResources().getString(R.string.txt_null))) {
                careTaskDetailModel.setDescription(response.optString("description"));
            }
            careTaskDetailModel.setCompleteBy(response.optString("dueDate"));

            String taskStatus = null;
            if (taskVariablesObject != null) {
                taskStatus = taskVariablesObject.optString("taskStatus");
            }
            careTaskDetailModel.setTaskStatus(taskStatus == null || taskStatus.equals("") || taskStatus.equalsIgnoreCase(context.getResources().getString(R.string.txt_null)) ? CarePlanConstants.PENDING_STATUS : taskStatus);
            JSONArray attachments = response.getJSONArray("attachments");
            ArrayList<CareTaskAttachemtsModel> careTaskAttachemtsModelArrayList = new ArrayList<>();
            for (int i = 0; attachments != null && i < attachments.length(); i++) {
                CareTaskAttachemtsModel careTaskAttachemtsModel = new CareTaskAttachemtsModel();
                JSONObject attachmentObject = attachments.getJSONObject(i);
                careTaskAttachemtsModel.setContentId(attachmentObject.getString("contentId"));
                careTaskAttachemtsModel.setId(attachmentObject.getString("id"));
                JSONObject persistentState = attachmentObject.optJSONObject("persistentState");
                careTaskAttachemtsModel.setFileName(persistentState.optString("name"));
                careTaskAttachemtsModel.setDescription(persistentState.optString("description"));
                careTaskAttachemtsModel.setType(attachmentObject.optString("type"));
                if (careTaskAttachemtsModel.getType().contains("image")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_IMAGE);
                } else if (careTaskAttachemtsModel.getType().contains("pdf")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_FILE);
                } else if (careTaskAttachemtsModel.getType().contains("application/msword")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_DOC);
                } else if (careTaskAttachemtsModel.getType().contains("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_DOCX);
                } else if (careTaskAttachemtsModel.getType().contains("text/plain")) {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_TEXT);
                } else {
                    careTaskAttachemtsModel.setMimeType(VariableConstants.MIME_TYPE_UNKNOWN);
                }
                careTaskAttachemtsModelArrayList.add(careTaskAttachemtsModel);
            }

            careTaskDetailModel.setAttachemtsModelsList(careTaskAttachemtsModelArrayList);


            JSONArray btnsList = taskVariablesObject.optJSONArray("buttons");
            List<CareTaskButtons> careTaskBtnList = new ArrayList<CareTaskButtons>();
            for (int j = 0; btnsList != null && j < btnsList.length(); j++) {
                JSONObject btnObject = btnsList.getJSONObject(j);
                CareTaskButtons careTaskButton = new CareTaskButtons();
                careTaskButton.setButtonAction(btnObject.optString("buttonAction"));
                careTaskButton.setButtonId(btnObject.optString("buttonId"));
                careTaskButton.setButtonTitle(btnObject.optString("buttonTitle"));
                careTaskButton.setButtonTag(btnObject.optString("buttonTag"));
                careTaskButton.setButtonValue(btnObject.optString("buttonValue"));
                careTaskBtnList.add(careTaskButton);
            }
            careTaskDetailModel.setListButtons(careTaskBtnList);

            JSONArray infoUrlsJsonArray = taskVariablesObject.optJSONArray("infoUrls");
            List<String> infoUrlsList = new ArrayList<String>();
            for (int j = 0; infoUrlsJsonArray != null && j < infoUrlsJsonArray.length(); j++) {
                infoUrlsList.add(infoUrlsJsonArray.getString(j));
            }
            careTaskDetailModel.setRelatedLinkList(infoUrlsList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public CareTaskDetailModel getCareTaskDetailModel() {
        return careTaskDetailModel;
    }

    public boolean isSuccess() {
        return true;
    }

    public boolean isError() {
        return isError;
    }
}
