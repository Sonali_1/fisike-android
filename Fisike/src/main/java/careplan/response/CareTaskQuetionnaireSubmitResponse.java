package careplan.response;

import android.content.Context;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskQuetionnaireSubmitResponse extends BaseResponse {
    private Context context;
    private boolean isError;
    private String url;
    private String status;

    public CareTaskQuetionnaireSubmitResponse(JSONObject response, long transactionId, Context context) {
        super(response, transactionId);
        this.context = context;
        this.isError = false;
        parseResponse(response);
    }

    public CareTaskQuetionnaireSubmitResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        this.isError = true;
    }

    public boolean isError() {
        return isError;
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONObject careTaskJsonObject = new JSONObject(response.toString());
            status = careTaskJsonObject.getString("status");
        } catch (JSONException jsonException) {

        }
    }

    public String getStatus() {
        return status;
    }
}