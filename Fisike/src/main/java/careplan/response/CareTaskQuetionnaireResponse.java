package careplan.response;

import android.content.Context;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskQuetionnaireResponse extends BaseResponse {
    private Context context;
    private boolean isError;
    private String url;
    private String appId;

    public CareTaskQuetionnaireResponse(JSONObject response, long transactionId, Context context) {
        super(response, transactionId);
        this.context = context;
        this.isError = false;
        parseResponse(response);
    }

    public CareTaskQuetionnaireResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        this.isError = true;
    }

    public boolean isError() {
        return isError;
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONObject careTaskJsonObject = new JSONObject(response.toString());
            url = careTaskJsonObject.getString("url");
            appId = careTaskJsonObject.getString("appId");
//            messageStatusQuetionnarie = careTaskJsonObject.optString("msg");
        } catch (JSONException jsonException) {

        }
    }

    public String getAppId() {
        return appId;
    }

    public String getUrl() {
        return url;
    }
}
