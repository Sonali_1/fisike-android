package careplan.response;

import android.content.Context;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by mphrx dec13 on 08-Sep-16.
 */
public class CareTaskEditResponse extends BaseResponse {

    private boolean isSuccess;
    private long id;

    public CareTaskEditResponse(JSONObject response, long id, long transactionId,
                                Context context) {
        super(response, transactionId);
        this.id = id;
        isSuccess = true;
    }

    public CareTaskEditResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        isSuccess = false;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public long getId() {
        return id;
    }
}
