package careplan.response;

import android.content.Context;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import careplan.constants.CarePlanConstants;
import careplan.enums.CarePlanFlowEnums;
import careplan.model.CarePlanModel;
import careplan.model.ExternalURLModel;

/**
 * Created by xmb2nc on 02-09-2016.
 */
public class CarePlanSearchResponse extends BaseResponse {
    private Context context;
    private List<CarePlanModel> carePlanModelList;
    private long totalActiveCarePlansCount = -1;

    public CarePlanSearchResponse(JSONObject response, long transactionId,
                                  Context context) {
        super(response, transactionId);
        this.context = context;
        parseResponse(response);
    }

    public CarePlanSearchResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    private void parseResponse(JSONObject response) {
        try {
            carePlanModelList = new ArrayList<>();
            JSONObject carePlanJsonObject = new JSONObject(response.toString());
            JSONArray carePlanJsonArray = carePlanJsonObject.getJSONArray("list");
            totalActiveCarePlansCount = carePlanJsonObject.optLong("totalCount");
            parseCarePlanJsonArray(carePlanJsonArray);
        } catch (JSONException jsonException) {
        }
    }

    private void parseCarePlanJsonArray(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONObject extensionObject = (JSONObject) jsonObject.getJSONArray("category").get(0);
//            JSONObject valueObject = (JSONObject) extensionObject.optJSONArray("value").get(0);
            JSONObject periodObject = jsonObject.getJSONObject("period");

            String title = extensionObject.optString("text");
//            Long caseLoadId = extensionObject.optLong("caseLoadId");
            String description = jsonObject.optString("description");
            String startDate = periodObject.optJSONObject("startDate").optString("value");
            // mongo id
            Long id = jsonObject.optLong("id");
            ExternalURLModel externalModel = new ExternalURLModel("", CarePlanFlowEnums.Default.toString());
            String status = jsonObject.optString("status");
//            status = (status == null || !status.equalsIgnoreCase("completed")) ? CarePlanConstants.ACTIVE : CarePlanConstants.COMPLETE;
            String documentID = null;
            JSONArray supportArray = jsonObject.optJSONArray("support");
            if (supportArray != null && supportArray.length() > 0) {
                JSONObject supportObject = (JSONObject) supportArray.get(0);
                if (supportObject != null && supportObject.has("id")) {
                    documentID = supportObject.optString("id");
                }
            }

            // right now setting the status of care plan to active and flow type to default
            CarePlanModel carePlanModel = new CarePlanModel(id, id, title, description,
                    startDate, status,
                    new ExternalURLModel("",
                            CarePlanFlowEnums.Default.toString()),documentID);
            carePlanModelList.add(carePlanModel);
        }
    }

    public List<CarePlanModel> getCarePlanList() {
        return carePlanModelList;
    }

    public long getTotalActiveCarePlansCount() {
        return totalActiveCarePlansCount;
    }
}
