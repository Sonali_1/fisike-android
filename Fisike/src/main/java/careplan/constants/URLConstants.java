package careplan.constants;

/**
 * Created by xmb2nc on 02-09-2016.
 */
public class URLConstants {
    // https://mobdev2.mphrx.com/minerva/CarePlan/search
    public static final String CARE_PLAN_SEARCH_API_CONSTANT  = "/CarePlan/search";
    public static final String CARE_TASK_API_CONSTANT  = "/workflow/getHistoryCategorisedTasks";
    public static final String CARE_TASK_WITH_PLAN_API_CONSTANT  = "/workflow/getTasksForCenter";
    public static final String CARE_TASK_SUBMIT_API_CONSTANT  = "/workflow/completeTask";
    public static final String CARE_TASK_DETAIL_API_CONSTANT = "/workflow/getTask";
    public static final String CARE_TASK_DETAIL_COMPLETE_API_CONSTANT = "/workflow/getHistoryTask";
    public static final String CARE_TASK_EDIT_API_CONSTANT  = "/workflow/editStandAloneTask";
    public static final String CARE_TASK_UPDATE_API_CONSTANT  = "/workflow/updatedProcessVariable";

    public static final String CARE_TASK_DOWNLOAD_FILE_CONSTANT  = "/Workflow/getTaskAttachmentContent";
    public static final String CARE_TASK_QUESTIONNAIRE  = "/questionnaire/generateResponseUrlForUser";
    public static final String CARE_TASK_QUESTIONNAIRE_SUBMIT  = "/questionnaireResponse/showApplication/";
}
