package careplan.constants;

/**
 * Created by Kailash Khurana on 9/20/2016.
 */
public class CarePlanConstants {

    public static final String ACTIVE = "Active";
    public static final String COMPLETE = "Completed";
    public static final String LINK = "Link";

    public static final String CARE_TASK_ID = "CARE_TASK_ID";
    public static final String PENDING_STATUS = "Pending";
    public static final String COMPLETE_STATUS = "Complete";
    public static final String FILTER_STATUS_FINISHED = "finished";
    public static final String CARE_TASK_VIEW_ATTACHMENT = "CARE_TASK_VIEW_ATTACHMENT";

    public static final String SUCESSFULL="SUCESSFULL";
    public static final String QUESTIONNARIE_ID = "QUESTIONNARIE_ID";
    public static final String CARE_TASK_QUESTIONNARIE_URL = "CARE_TASK_QUESTIONNARIE_URL";

    public static final String CARE_TASK_WEBVIEW_TITLE="WEBVIEW_TITLE";

    public static final String CARE_TASK_OBJECT = "CARE_TASK_OBJECT";

    public static final String CARE_TASK_FILTER_STATUS = "CARE_TASK_FILTER_STATUS";

    // care task api request
    public static final String CARE_TASK_FINISHED="finished";
    public static final String CARE_TASK_UNFINISHED="unfinished";

    public static final String VIEW_CARE_PLAN_SCREEN = "VIEW_CARE_PLAN_SCREEN";
    public static final String CANCEL_STATUS ="Cancel";
    public static final String CASE_LOAD_TITLE = "CASE_LOAD_TITLE";

    public static final String CASE_LOAD_ID = "caseLoadId";
}
