package careplan.observable;

import java.util.Observable;

/**
 * Created by Kailash Khurana on 9/13/2016.
 */
public class CareTaskObservable extends Observable {
    private static CareTaskObservable parsingClass;
    private Object dataObject;
    private int operation;
    private boolean isSucessfullyExecuted;
    private String message;

    public static CareTaskObservable getInstance() {
        if (parsingClass == null) {
            parsingClass = new CareTaskObservable();
        }
        return parsingClass;
    }

    public CareTaskObservable() {

    }

    public void setData(Object dataObject, int operation, boolean isSucessfullyExecuted, String message) {
        this.dataObject = dataObject;
        this.operation = operation;
        this.isSucessfullyExecuted = isSucessfullyExecuted;
        this.message = message;
        setChanged();
        notifyObservers();
    }

    public Object getDataObject() {
        return dataObject;
    }

    public int getOperation() {
        return operation;
    }

    public boolean isSucessfullyExecuted() {
        return isSucessfullyExecuted;
    }

    public String getMessage() {
        return message;
    }

}
