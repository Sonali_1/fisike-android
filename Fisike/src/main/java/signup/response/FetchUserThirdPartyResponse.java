package signup.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by aastha on 7/27/2017.
 */

public class FetchUserThirdPartyResponse extends BaseResponse {
    private String code,responseStatus;
    private String firstname, lastname, gender , dob , email ;
    private boolean isSignupComplete ;

    public FetchUserThirdPartyResponse(JSONObject response, long transactionID) {
        super(response, transactionID);
        parseResponse(response);
    }

    public FetchUserThirdPartyResponse(Exception exception, long mTransactionid) {
        super(exception,mTransactionid);

    }

    private void parseResponse(JSONObject response) {
        if(Utils.isValueAvailable(getInfoCode())){
            responseStatus = getInfoResponseStatus();
            code = getInfoCode();
        }else {
            try {
                firstname = response.getString("firstName");
                lastname = response.getString("lastName");
                email = response.getString("email");
                dob = response.getString("dob");
                gender = response.getString("gender");
                isSignupComplete = response.getBoolean("signUpDetailsComplete");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGender() {
        return gender;
    }

    public String getDob() {
        return dob;
    }

    public String getEmail() {
        return email;
    }

    public boolean isSignupComplete() {
        return isSignupComplete;
    }

    public void setSignupComplete(boolean signupComplete) {
        isSignupComplete = signupComplete;
    }
}
