package signup.response;


import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by rajantalwar on 7/11/17.

 */

public class GetCityMapResponse extends BaseResponse {
    private ArrayList<String> cityList = new ArrayList<>();
    public GetCityMapResponse(JSONObject response, long transactionID) {
        super(response, transactionID);
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray arrayCity= response.optJSONArray("cityList");
            if(arrayCity == null || arrayCity.length() == 0){
                return;
            }
            for(int i =0;i<arrayCity.length();i++){
                cityList.add(arrayCity.getString(i));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public GetCityMapResponse(Exception exception, long transactionID) {
        super(exception, transactionID);
    }

    public ArrayList<String> getCityList() {
        return cityList;
    }
}
