package signup.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 7/5/2017.
 */

public class VerifyOtpSignupResponse extends BaseResponse{
    private String code;
    private boolean isSuccessScenario;

    public VerifyOtpSignupResponse(JSONObject response, long mTransactionId) {
        super(response,mTransactionId);
        isSuccessScenario = true;
        try {
            JSONArray info = response.optJSONArray("info");
            code = info.getJSONObject(0).optString("code");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public VerifyOtpSignupResponse(Exception exception,long mTransactionId) {
        super(exception, mTransactionId);
    }

    @Override
    public String getCode() {
        return code;
    }

    public boolean isSuccessScenario() {
        return isSuccessScenario;
    }
}
