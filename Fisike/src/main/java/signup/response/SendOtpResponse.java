package signup.response;

import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aastha on 6/29/2017.
 */

public class SendOtpResponse extends BaseResponse {

    private String code;
    private boolean isSuccessScenario;
    private  boolean isResend;
    public SendOtpResponse(JSONObject response, long transactionId) {
        super(response,transactionId);
        isSuccessScenario = response.optBoolean("success");
    }

    public SendOtpResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
        isSuccessScenario = false;
    }

    @Override
    public String getCode() {
        return code;
    }

    public boolean isSuccessScenario() {
        return isSuccessScenario;
    }
}
