package signup.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by aastha on 7/12/2017.
 */

public class SignupMOResponse extends BaseResponse {

    private UserMO user;

    public SignupMOResponse(Exception exception, long mTransactionId) {
        super(exception,mTransactionId);
    }

    public SignupMOResponse(JSONObject response, long mTransactionId) {
        super(response,mTransactionId);
        parseResponse(response);

    }

    private void parseResponse(JSONObject response) {
        Object jsonToObjectMapper = GsonUtils.jsonToObjectMapper(response.toString(), UserMO.class);
        user=((UserMO) jsonToObjectMapper);
    }

    public UserMO getUser() {
        return user;
    }
}
