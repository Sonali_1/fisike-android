package signup.response;

import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;

/**
 * Created by aastha on 8/31/2017.
 */

public class ResetPasswordResponse extends BaseResponse {

    public ResetPasswordResponse(JSONObject response, long mTransactionid) {
        super(response,mTransactionid);
    }

    public ResetPasswordResponse(Exception exception, long mTransactionid) {
        super(exception,mTransactionid);
    }
}
