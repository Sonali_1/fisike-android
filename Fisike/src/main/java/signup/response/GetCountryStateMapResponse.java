package signup.response;


import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by aastha on 7/7/2017.
   sprint-48-login
 */

public class GetCountryStateMapResponse extends BaseResponse {
    HashMap<String,ArrayList<String>> cityMap = new HashMap<>();
    public ArrayList<String> keysList = new ArrayList<>();
    public boolean fromAddress;

    public GetCountryStateMapResponse(JSONObject response, boolean fromAddress, long transactionID) {
        super(response, transactionID);
        this.fromAddress = fromAddress;
        parseResponse(response);
    }

    private void parseResponse(JSONObject response) {
        Iterator keysToCopyIterator = response.keys();
        keysList = new ArrayList<String>();
        while(keysToCopyIterator.hasNext()) {
            String key = (String) keysToCopyIterator.next();
            if(!key.equals("headers"))
                keysList.add(key);
        }
        for(int i=0;i<keysList.size();i++){
            String countryKey = keysList.get(i);
            ArrayList<String> cityNames = new ArrayList<>();
            try {
                JSONArray cities = response.getJSONArray(countryKey);
                for(int j=0; j<cities.length();j++){
                    cityNames.add(j,cities.getString(j));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cityMap.put(countryKey,cityNames);
        }
    }

    public GetCountryStateMapResponse(Exception exception, long transactionID) {
        super(exception, transactionID);
    }
    public HashMap<String, ArrayList<String>> getStatesAndCitiesList() {
        return cityMap;
    }

    public ArrayList<String> getKeysList() {
        return keysList;
    }

    public boolean isFromAddress() {
        return fromAddress;
    }
}


