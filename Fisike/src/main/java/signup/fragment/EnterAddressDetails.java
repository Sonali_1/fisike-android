package signup.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.ThreadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import signup.OnGetTransactionId;
import signup.activity.SignUpActivity;
import signup.request.GetCityMapRequest;
import signup.request.GetCountryStateMapRequest;

/**
 * Created by aastha on 6/23/2017.
 */

public class EnterAddressDetails extends BaseFragment implements View.OnClickListener{

    private CustomEditTextWhite etPostalCode,etStreet, etAppartment;
    private String postalCode, city, state , street , appartment , country;
    private CustomFontTextView etCity,etState,etCountry;
    private JSONArray address =null;
    private boolean isCountryListFetched;
    private HashMap<String,ArrayList<String>> countryList;
    private ArrayList<String> statesList ;
    private ArrayList<String> countryKeys;
    private ArrayList<String> cityList;
    private PopupWindow mPopup;
    private OnGetTransactionId mListener;
    private long transactionid;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionId) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }


    public static EnterAddressDetails getInstance(Bundle bundle) {
        EnterAddressDetails fragment = new EnterAddressDetails();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    protected int getLayoutId() {
        return R.layout.signup_address_details;
    }

    @Override
    public void findView() {
        etPostalCode = (CustomEditTextWhite) getView().findViewById(R.id.zipCode);
        etCity = (CustomFontTextView) getView().findViewById(R.id.cityName);
        etState = (CustomFontTextView) getView().findViewById(R.id.stateName);
        etStreet = (CustomEditTextWhite) getView().findViewById(R.id.streetNumber);
        etAppartment = (CustomEditTextWhite) getView().findViewById(R.id.appartmentNo);
        etCountry = (CustomFontTextView) getView().findViewById(R.id.countryName);
    }

    @Override
    public void initView() {
        ((SignUpActivity) getActivity()).setToolbar(SignUpActivity.ADDRESS_DETAILS);
        isCountryListFetched =  ((SignUpActivity) getActivity()).isStateAndCityFetched();
        etCity.setEnabled(false);
        if(!isCountryListFetched)
        {
            etCountry.setEnabled(true);
            etState.setEnabled(false);
        }
        else
        {
            countryList = ((SignUpActivity) getActivity()).getCountryStateMap();
            countryKeys =  ((SignUpActivity) getActivity()).getCountryKeysList();
            etCountry.setEnabled(true);
            etState.setEnabled(false);
        }
        etCountry.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if(isCountryListFetched){
                        showPopupNames(etCountry);
                        if(cityList!=null)
                        {
                            cityList.clear();
                            etState.setEnabled(false);
                        }
                            ((SignUpActivity) getActivity()).setCityListFetched(false);
                            etState.setText("");
                            etCity.setText("");
                            etState.setHint(getResources().getString(R.string.state));
                            etCity.setHint(getResources().getString(R.string.city));
                            etCity.setEnabled(false);

                    }
                    else{
                        transactionid = System.currentTimeMillis();
                        mListener.setIdInActivity(transactionid);
                        showProgressDialog(getResources().getString(R.string.loading_txt));
                        ThreadManager.getDefaultExecutorService().submit(new GetCountryStateMapRequest(transactionid,true));
                    }
                }
                return true;
            }

        });
        etState.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                hideKeyboard();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showPopupNames(etState);
                    if(cityList!=null)
                    {
                        cityList.clear();
                        ((SignUpActivity) getActivity()).setCityListFetched(false);
                        etCity.setEnabled(false);
                        etCity.setText("");
                        etCity.setHint(getResources().getString(R.string.city));
                    }
                }
                return true;
            }
        });
        etCity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                if (event.getAction() == MotionEvent.ACTION_UP && (((SignUpActivity) getActivity()).isCityListFetched()) ) {
                    showPopupNames(etCity);
                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    transactionid = System.currentTimeMillis();
                    mListener.setIdInActivity(transactionid);
                    showProgressDialog(getResources().getString(R.string.loading_txt));
                    ThreadManager.getDefaultExecutorService().submit(new GetCityMapRequest(transactionid,country,state));
                }
                return true;
            }

        });
    }

    @Override
    public void bindView() {
    etStreet.requestFocus();
    setImeOptionListener();
    }

    public void showPopupNames(View view) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        String[] arrayPopupString = null;
        final long idView = view.getId();
        if (idView == R.id.countryName) {

            if(countryKeys ==null)
                countryKeys = ((SignUpActivity)getActivity()).getCountryKeysList();
            int size = countryKeys.size();
            arrayPopupString = new String[size];
            for (int i = 0; i < size; i++) {
                arrayPopupString[i] = countryKeys.get(i);
            }
        }
        else if(idView == R.id.stateName){
            if(statesList ==null)
                statesList = countryList.get(country);
            if(statesList!=null){
            int size = statesList.size();
            arrayPopupString = new String[size];
            for (int i = 0; i < size; i++) {
                arrayPopupString[i] = statesList.get(i);
            }}
        }
        else if(idView == R.id.cityName){
            if(cityList ==null || cityList.size()==0)
              cityList = ((SignUpActivity)getActivity()).getCityList();
            int size = cityList.size();
            arrayPopupString = new String[size];
            for (int i = 0; i < size; i++) {
                arrayPopupString[i] = cityList.get(i);
            }
        }
        int width = etCountry.getWidth();
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.setFocusable(false);
        mPopup.setOutsideTouchable(true);
        mPopup.showAsDropDown(view, (int) Utils.convertDpToPixel(0, getActivity()), (int) Utils.convertDpToPixel(0, getActivity()));
        final StatusAdapter adapter = new StatusAdapter(getActivity(), R.layout.text_status, arrayPopupString);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (idView == R.id.countryName) {
                    country = adapter.getItem(position);
                    etCountry.setText(country);
                    if(countryList==null)
                        countryList = ((SignUpActivity) getActivity()).getCountryStateMap();
                    statesList = countryList.get(country);
                    etState.setEnabled(true);
                    if (mPopup != null && mPopup.isShowing()) {
                        mPopup.dismiss();
                    }
                } else if (idView == R.id.stateName) {
                    state = adapter.getItem(position);
                    etState.setText(state);
                    etCity.setEnabled(true);
                    if (mPopup != null && mPopup.isShowing()) {
                        mPopup.dismiss();
                    }
                } else if (idView == R.id.cityName) {
                    city = adapter.getItem(position);
                    etCity.setText(city);
                    if (mPopup != null && mPopup.isShowing()) {
                        mPopup.dismiss();
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                {
                    if(isValidationsSuccessful()) {
                        ((SignUpActivity) getActivity()).setAddressDetailsValidated(true);
                        ((SignUpActivity) getActivity()).setAddressPayload(getPayload());
                        ((SignUpActivity) getActivity()).onClick(view);
                    }
                break;
                }
        }
    }

    public boolean isValidationsSuccessful() {
        hideKeyboard();
        boolean returnValue=true;
        if(!Utils.isValueAvailable(etAppartment.getText().toString())){
            etStreet.setError(getActivity().getResources().getString(R.string.please_enter_appartment_name));
            returnValue =false;
        }
        if(!Utils.isValueAvailable(etPostalCode.getText().toString())){
            etPostalCode.setError(getActivity().getResources().getString(R.string.please_enter_zip_code));
            returnValue = false;
        }
        if(returnValue == false)
            return false;
        if(!Utils.isValueAvailable(etCountry.getText().toString())||etCountry.getText().toString().equals(getActivity().getResources().getString(R.string.country_hint))){
            Toast.makeText(MyApplication.getAppContext(),getActivity().getResources().getString(R.string.please_select_your_country),Toast.LENGTH_SHORT).show();
            returnValue = false;
        }
        else if(!Utils.isValueAvailable(etState.getText().toString())||etState.getText().toString().equals(getActivity().getResources().getString(R.string.state))){
            Toast.makeText(MyApplication.getAppContext(),getActivity().getResources().getString(R.string.please_select_your_state),Toast.LENGTH_SHORT).show();
            returnValue = false;
        }
        else if(!Utils.isValueAvailable(etCity.getText().toString())||etCity.getText().toString().equals(getActivity().getResources().getString(R.string.city))){
            Toast.makeText(MyApplication.getAppContext(),getActivity().getResources().getString(R.string.please_select_your_city),Toast.LENGTH_SHORT).show();
            returnValue = false;
        }
        return returnValue;
    }

    public JSONObject getPayload() {
        postalCode = etPostalCode.getText().toString().trim();
        street = etStreet.getText().toString().trim();
        appartment = etAppartment.getText().toString().trim();
        JSONObject jsonObject = new JSONObject();
        try {
            String useCode = "home";

            jsonObject.put("country",country);
            jsonObject.put("state",state);
            jsonObject.put("city",city);
            jsonObject.put("postalCode",postalCode);
            jsonObject.put("useCode",useCode);
            String text = "";
            jsonObject.put("text", Utils.createAddressTextString(appartment, street, country, state, city, postalCode));
            JSONArray line = new JSONArray();
            line.put(0,appartment);
            line.put(1,street);
            jsonObject.put("line",line);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void setImeOptionListener()
    {
        etStreet.getEditText().requestFocus();
        etStreet.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                etAppartment.requestFocus();
                return true;
            }
        });

        etAppartment.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                etCountry.requestFocus();
                hideKeyboard();
                return true;
            }
        });

        etCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        etPostalCode.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etPostalCode.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
               hideKeyboard();
                if(actionId==EditorInfo.IME_ACTION_DONE )
                {
                    onClick(getView().findViewById(R.id.btn_proceed));
                    return true;
                }
            return true;
            }
        });
    }


}
