package signup.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.background.RemoveProfilePicture;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.icomoon.IconButton;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.views.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import signup.activity.SignUpActivity;

import static android.app.Activity.RESULT_OK;
import static signup.activity.SignUpActivity.PERSONAL_DETAILS;

/**
 * Created by Kailash Khurana on 6/22/2017.
 */

public class EnterYourPersonalDetails extends signup.fragment.ImageUploadUIFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, signup.fragment.CameraFragment.CameraIntegration {

    private CustomEditTextWhite firstName, lastName, middleName, email, dateOfBirth, aadharNumber;
    private CustomEditTextWhite uniqueId, edAadhar, edSsn;
    private CustomFontTextView gender;
    private CircleImageView civUserProfilePic;
    private BottomSheetDialog mBottomSheetDialog;
    private File photoUri;
    private Bitmap profilePic;
    private String realPathFromURI, dob;
    private Uri mImageCaptureUri = null;
    private byte[] profilePictureByte;
    private boolean isCameraImage;
    private Bitmap tempBitmap;
    private ProgressDialog pd_ring;
    private Context mContext;
    private CustomFontButton proceed, skip;
    private DatePickerDialog datePickerDialog;
    private int year, month, day;
    private Bitmap profileBitmap;
    private PopupWindow mPopup;
    IconButton itvIconCamera;
    RelativeLayout btnChangeProfilePic;

    public static EnterYourPersonalDetails getInstance(Bundle bundle) {
        EnterYourPersonalDetails fragment = new EnterYourPersonalDetails();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.signup_personal_details;
    }

    @Override
    public void findView() {
        findViewPersonal();
        setImeOptionListeners();
    }


    private void findViewPersonal() {
        firstName = (CustomEditTextWhite) getView().findViewById(R.id.firstNameSignup);
        middleName = (CustomEditTextWhite) getView().findViewById(R.id.middleNameSignup);
        lastName = (CustomEditTextWhite) getView().findViewById(R.id.lastNameSignup);
        email = (CustomEditTextWhite) getView().findViewById(R.id.emailSignup);
        dateOfBirth = (CustomEditTextWhite) getView().findViewById(R.id.dateOfBirthSignup);
        gender = (CustomFontTextView) getView().findViewById(R.id.genderSignup);
        aadharNumber = (CustomEditTextWhite) getView().findViewById(R.id.mrnSignup);
        civUserProfilePic = (CircleImageView) getView().findViewById(R.id.civ_user_profile_pic);
        btnChangeProfilePic = (RelativeLayout) getView().findViewById(R.id.btn_change_profile_pic);
        proceed = (CustomFontButton) getView().findViewById(R.id.btn_proceed);
        itvIconCamera= (IconButton) getView().findViewById(R.id.itv_icon_camera);
    }

    @Override
    public void initView() {
        ((SignUpActivity) getActivity()).setToolbar(PERSONAL_DETAILS);
        mContext = getActivity();
        initPersonalDetailFlow();
    }

    private void initPersonalDetailFlow() {

        if (Utils.isRTL(mContext)) {
            dateOfBirth.getEditText().setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.calendar_signup), null, null, null);
            gender.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.dropdown_signup), null, null, null);
        } else {
            dateOfBirth.getEditText().setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.calendar_signup), null);
            gender.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.dropdown_signup), null);
        }
        cameraIntegrationListener = this;
    }

    @Override
    public void bindView() {
        dateOfBirth.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == motionEvent.ACTION_UP)
                    showDatePickerDialog();
                return true;
            }
        });

        dateOfBirth.setInputType(InputType.TYPE_NULL);
       /* dateOfBirth.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {*/
       /*     @Override*/
       /*     public void onFocusChange(View v, boolean hasFocus) {*/
       /*         showDatePickerDialog();*/
       /*     }*/
       /* });*/

        gender.setInputType(InputType.TYPE_NULL);
        gender.setOnClickListener(this);
        gender.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                showPopupNames(v);
            }
        });
    }

    public void showOrHideRemoveImageMenu(View view) {
        Utils.hideKeyboard(getActivity());
        if (profilePic != null)
            view.findViewById(R.id.remove_image).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.remove_image).setVisibility(View.GONE);
    }


    private void removeImage() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            new RemoveProfilePicture(getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                callProceed();
                break;
            case R.id.btn_change_profile_pic:
                showBottomSheet(profileBitmap);
                break;
            case R.id.civ_user_profile_pic:
                if (btnChangeProfilePic.getVisibility() == View.GONE) {
                    showBottomSheet();
                }
                break;
            case R.id.genderSignup:
                showPopupNames(view);
                break;
        }
    }

    private void callProceed() {
        if (isValidationSuccessful()) {
            //if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    ((SignUpActivity)getActivity()).setTheme(R.style.AppStyle_themeWhite);
            ((SignUpActivity) getActivity()).setFirstName(firstName.getText().toString().trim());
            ((SignUpActivity) getActivity()).setLastName(lastName.getText().toString().trim());
            ((SignUpActivity) getActivity()).setEmail(email.getText().toString());
            if (aadharNumber.getVisibility() == View.VISIBLE)
                ((SignUpActivity) getActivity()).setAadharNo(aadharNumber.getText().toString());

            if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                new VerifyDOB().execute(MphRxUrl.getVerifyDobUrl());
            }
            String genderInEnglish = GenderEnum.getCodeFromValue(gender.getText().toString());
            ((SignUpActivity) getActivity()).setGender(genderInEnglish);
            if (profileBitmap != null) {
                ((SignUpActivity) getActivity()).setUserProfilePicture(Utils.convertBitmapToBase64(profileBitmap));
                ((SignUpActivity) getActivity()).setProfileBitmap(profileBitmap);
            } else
                ((SignUpActivity) getActivity()).setUserProfilePicture("");
            if (Utils.isValueAvailable(middleName.getText().toString())) {
                ((SignUpActivity) getActivity()).setMiddleName(middleName.getText().toString().trim());
            } else {
                ((SignUpActivity) getActivity()).setMiddleName("");
            }
        }
    }

    private void showPopupNames(View view) {
        hideKeyboard();
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        String[] arrayPopupString = getResources().getStringArray(R.array.gender_type);
        int width = view.getWidth();
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.setFocusable(false);
        mPopup.setOutsideTouchable(true);
        mPopup.showAsDropDown(view, (int) Utils.convertDpToPixel(0, getActivity()), (int) Utils.convertDpToPixel(-70, getActivity()));
        final StatusAdapter adapter = new StatusAdapter(getActivity(), R.layout.text_status, arrayPopupString);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                gender.setText(adapter.getItem(i));
                if (mPopup != null && mPopup.isShowing()) {
                    mPopup.dismiss();
                }
            }
        });
    }

    private void showDatePickerDialog() {
        hideKeyboard();
        //if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            ((SignUpActivity) getActivity()).setTheme(R.style.AppStyle);
        // Use the current date as the default date in the picker

        Calendar minDate = Calendar.getInstance();
        minDate.add(Calendar.YEAR, -100);

        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.YEAR, -18);

        Calendar calendar = Calendar.getInstance();// Aug28, 2015
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            if(Utils.isValueAvailable(dateOfBirth.getText().toString())){
                calendar.setTime(sdf.parse(dateOfBirth.getText().toString().trim()));
            } else {
                calendar.setTime(new Date());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        maxDate.set(Calendar.HOUR_OF_DAY, 23);
        maxDate.set(Calendar.MINUTE, 59);
        maxDate.set(Calendar.SECOND, 59);


        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        datePickerDialog.setCustomTitle(layout);
        }
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if (datePicker.isShown()) {
            //if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
               getActivity().setTheme(R.style.AppStyle_themeWhite);
            String dateSelected = getDateInDD_MMM_YYYY(datePicker);
            String dateSelectedEnglish = getDateInDD_MMM_YYYYEnglish(datePicker);
            String calculatedDate = Utils.getFormattedDateEnglish(dateSelectedEnglish, DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
            int age = Utils.calculateAge(dateSelectedEnglish);
            if (age >= 18) {
                dob = calculatedDate;
                dateOfBirth.setText(dateSelected);
            } else {
                Toast.makeText(getActivity(), MyApplication.getAppContext().getResources().getString(R.string.must_18_old_above), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String getDateInDD_MMM_YYYY(DatePicker datePicker) {
        String[] array = getResources().getStringArray(R.array.months_arr);
        // datePicker.getDayOfMonth() + " " + array[datePicker.getMonth()] + " " + datePicker.getYear();

        return DateTimeUtil.convertSourceDestinationDateLocale(datePicker.getDayOfMonth() + " " + (datePicker.getMonth() + 1) + " " + datePicker.getYear()
                , DateTimeUtil.d_M_yyyy, DateTimeUtil.dd_MMM_yyyy);
    }

    public String getDateInDD_MMM_YYYYEnglish(DatePicker datePicker) {
        String[] array = getResources().getStringArray(R.array.months_arr);
        String dateset = datePicker.getDayOfMonth() + " " + array[datePicker.getMonth()] + " " + datePicker.getYear();
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        java.util.Date dateObj = null;
        try {
            dateObj = sdf.parse(dateset);
        } catch (ParseException e) {
        }
        return sdf.format(dateObj).toString();
    }

    @Override
    public void setExtractedBitmap(Bitmap bitmap) {
        profileBitmap = bitmap;
        if(bitmap!=null){
            civUserProfilePic.setImageBitmap(profileBitmap);
            btnChangeProfilePic.setVisibility(View.VISIBLE);
            itvIconCamera.setVisibility(View.GONE);
        }
        else
        {
            civUserProfilePic.setImageBitmap(null);
            btnChangeProfilePic.setVisibility(View.GONE);
            itvIconCamera.setVisibility(View.VISIBLE);
        }
    }

    public boolean isValidationSuccessful() {
        hideKeyboard();
        boolean returnValue = true;
        if (!Utils.isValueAvailable(firstName.getText().toString())) {
            firstName.setError(getActivity().getResources().getString(R.string.please_enter_first_name));
            returnValue = false;
        }
        if (!Utils.isValueAvailable(lastName.getText().toString())) {
            lastName.setError(getActivity().getResources().getString(R.string.please_enter_last_name));
            returnValue = false;
        }
        if (!Utils.isValueAvailable(email.getText().toString())) {
            email.setError(getActivity().getResources().getString(R.string.please_enter_email));
            returnValue = false;
        } else if (!Utils.validateMail(email.getText().toString())) {
            email.setError(getActivity().getResources().getString(R.string.please_enter_valid_value));
            returnValue = false;
        }
        if (!Utils.isValueAvailable(dateOfBirth.getText().toString())) {
            dateOfBirth.setError(getActivity().getResources().getString(R.string.enter_valid_dob));
            returnValue = false;
        }
        if (aadharNumber.getVisibility() == View.VISIBLE) {
            if ((Utils.isValueAvailable(aadharNumber.getText().toString()) && aadharNumber.getText().toString().length() != 12)) {
                aadharNumber.setError(getActivity().getResources().getString(R.string.aadhar_length_message));
                returnValue = false;
            }
        }
        if (returnValue == false)
            return false;
        else if (gender.getText().toString().equals(getActivity().getResources().getString(R.string.gender))) {
            Toast.makeText(MyApplication.getAppContext(), getActivity().getResources().getString(R.string.select_your_gender), Toast.LENGTH_SHORT).show();
            returnValue = false;
        }
        return returnValue;
    }


    private class VerifyDOB extends AsyncTask<String, Void, String> {

        private boolean validationSuccessful;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getResources().getString(R.string.verifying));
        }

        @Override
        protected String doInBackground(String... url) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("dob", dob);
            } catch (Exception e) {
                jsonObject = null;
            }
            String jsonString = APIManager.getInstance().executeHttpPost(url[0], jsonObject);
            return jsonString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dismissProgressDialog();
            if (result != null) {
                String code;
                JSONObject data = null;
                try {
                    data = new JSONObject(result.toString());
                    JSONObject info = data.optJSONArray("info").optJSONObject(0);
                    code = info.optString("code");
                } catch (JSONException e) {
                    JSONObject info = data.optJSONArray("error").optJSONObject(0);
                    code = info.optString("code");
                }
                if (code.equals("VDOB63")) {
                    ((SignUpActivity) getActivity()).setDob(dob);
                    ((SignUpActivity) getActivity()).setPersonalDetailsValidated(true);
                } else if (code.equals("VDOB62")) {
                    Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.enter_valid_dob), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setImeOptionListeners() {
        firstName.getEditText().requestFocus();

        firstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                middleName.requestFocus();
                return true;
            }
        });

        middleName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                lastName.requestFocus();
                return true;
            }
        });

        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                email.requestFocus();
                return true;
            }
        });

        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideKeyboard();
                return true;
            }
        });

    }


}

