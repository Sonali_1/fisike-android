package signup.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike_physician.utils.DialogUtils;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     ImageUploadUIFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link ImageUploadUIFragment.Listener}.</p>
 */
public class ImageUploadUIFragment extends CameraFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_ITEM_COUNT = "item_count";
    private BottomSheetDialog mBottomSheetDialog;

    // TODO: Customize parameters
    public static ImageUploadUIFragment newInstance(int itemCount) {
        final ImageUploadUIFragment fragment = new ImageUploadUIFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_ITEM_COUNT, itemCount);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface Listener {
        void onItemClicked(int position);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_item_list_dialog_item, parent, false));
            text = (TextView) itemView.findViewById(R.id.text);
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // we have to call the dismiss of the bottom sheet.
                    //   dismiss();
                }
            });
        }

    }

    private void dismissBottomSheet() {
        if (mBottomSheetDialog.isShowing()) {
            mBottomSheetDialog.dismiss();
        }
    }

    /**
     * Show bottom sheet for sending the attachment
     */
    public void showBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.change_image_layout, null);
        float peekHeight = view.getMeasuredHeight();
        view.findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();

                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " +
                        getString(R.string.permission_storage), CAMERA_RESULT)) {
                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_camera), CAMERA_RESULT))
                        takePhoto();
                }

            }
        });

        view.findViewById(R.id.choose_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " +
                        getString(R.string.permission_storage), GALLERY_RESULT))
                    openGallery();

            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    //overloaded showbottom sheet to show remove image option
    public void showBottomSheet(Bitmap bitmap) {
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.change_image_layout, null);
        float peekHeight = view.getMeasuredHeight();
        view.findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();

                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().
                        getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), READ_WRITE_INTENT)) {
                    // camera permission
                    if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_camera), CAMERA_RESULT)){
                        takePhoto();
                    }
                }

            }
        });

        view.findViewById(R.id.choose_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheet();
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().
                        getString(R.string.permission_msg) + " " + getActivity().getString(R.string.permission_storage), READ_WRITE_INTENT)) {
                    if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_msg) + " " +
                            getString(R.string.permission_storage), GALLERY_RESULT))
                        openGallery();
                }

            }
        });

        if(bitmap!=null)
        {
            view.findViewById(R.id.remove_image).setVisibility(View.VISIBLE);
            view.findViewById(R.id.remove_image).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissBottomSheet();
                    if (cameraIntegrationListener != null)
                        cameraIntegrationListener.setExtractedBitmap(null);

                }
            });
        }

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }


    public boolean checkPermission(final String permissionType, String message, final int PERMISSION_REQUEST_CODE) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), permissionType) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissionType)) {
                DialogUtils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.permission), message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
                        else
                            onPermissionNotGranted(permissionType);
                    }
                });
            } else {
                requestPermissions(new String[]{permissionType}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }

        return true;
    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Permission_needs_granted_access), Toast.LENGTH_SHORT).show();
    }

    public void onPermissionGranted(String permission) {
        Toast.makeText(getActivity(), "Permission have been granted ", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted(permissions[0], requestCode);
        } else {
            onPermissionNotGranted(permissions[0]);
        }

    }

    private void onPermissionGranted(String permission, int requestCode) {
        if (requestCode == CAMERA_RESULT) {
            if (checkPermission(Manifest.permission.CAMERA, getString(R.string.permission_msg) + " " +
                    getString(R.string.permission_camera), CAMERA_RESULT))
            takePhoto();
        } else if (requestCode == GALLERY_RESULT) {
            openGallery();

        }
    }


}
