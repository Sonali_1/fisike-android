package signup.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.utils.CountryCodeUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.CountryAdapter;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import login.activity.LoginActivity;
import signup.OnGetTransactionId;
import signup.activity.LoginSignupActivity;
import signup.activity.SignUpActivity;
import signup.request.SendOtpRequest;

/**
 * Created by Kailash Khurana on 6/22/2017.
 */

public class EnterLoginIdScreen extends BaseFragment implements OnGetTransactionId{

    private CustomEditTextWhite edMobileNumber;
    private Spinner spinnerCountryCode;
    private int position;
    private CountryAdapter countryAdapter;
    private long mTransactionId;
    private OnGetTransactionId mListener;
    private String countrycode;
    private String numbertext;
    private String SOURCE = TextConstants.SELF_SIGN_UP_MOBILE;
    private String phoneNumber;
    private String countryId=null;
    private String countrycodeplus;

    public static EnterLoginIdScreen getInstance(Bundle bundle) {
        EnterLoginIdScreen fragment = new EnterLoginIdScreen();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionId) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    @Override
    public void setIdInActivity(long id) {
        mTransactionId = id;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_signup_login_id;
    }

    @Override
    public void findView() {
        edMobileNumber = (CustomEditTextWhite) getView().findViewById(R.id.et_mobile_number);
        spinnerCountryCode = (Spinner) getView().findViewById(R.id.spinner_change_country_code);
    }

    @Override
    public void initView() {
        ((SignUpActivity) getActivity()).setToolbar(SignUpActivity.ENTER_ID);
        TelephonyManager telMgr = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                setCountryCode(false);
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                setCountryCode(true);
                break;
            case TelephonyManager.SIM_STATE_READY:
                setCountryCode(true);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                setCountryCode(false);
                break;
        }
        if(getArguments()!=null)
            readArguments(getArguments());
    }

    private void readArguments(Bundle bundle) {
        phoneNumber = bundle.getString(VariableConstants.PHONE_NUMBER,"");
        if(phoneNumber!=null && !phoneNumber.equals(""))
                edMobileNumber.setText(phoneNumber);
    }

    @Override
    public void bindView() {
        spinnerCountryCode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Utils.hideKeyboard(getActivity());
                return false;
            }
        });
    }

    public void setCountryCode(boolean on) {

        if (on) {
            try {
                countryId = CountryCodeUtils.getCountryId(getActivity());
                countrycode = CountryCodeUtils.getCountryZipCode(getActivity(),countryId).toString().trim();
            } catch (Exception e) {
               setDefaultCountryCodes();
            }
        } else if (!on) {
            setDefaultCountryCodes();
        }
        if(BuildConfig.isShowMultipleCountryCodes)
            position = CountryCodeUtils.getCountryPosition(getActivity(), Integer.parseInt(countrycode),countryId);
        else
            position = 0;
        spinnerCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinnerCountryCode.getSelectedItem().toString().trim(), ",");
                countrycode =  stringTokenizer.nextToken().toString().trim();

                countrycodeplus = "+"+ countrycode ;
                if (Integer.parseInt(countrycode) == TextConstants.INDIA_COUNTRYCODE) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    edMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    /*[  .... Moblile length set on   [ max_length- countrycode(including+) ]   ........  ]*/
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countrycodeplus.length()));
                    edMobileNumber.setFilters(FilterArray);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        if(BuildConfig.isShowMultipleCountryCodes)
            countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        else
        {
            if(TextConstants.DEFAULT_COUNTRY_INDIA)
                countrylist.add(String.valueOf(getResources().getString(R.string.india_country)));
            else
                countrylist.add(String.valueOf(getResources().getString(R.string.us_country)));
        }
        countryAdapter = new CountryAdapter(getActivity(), countrylist, "white");
        spinnerCountryCode.setAdapter(countryAdapter);
        spinnerCountryCode.setSelection(position);
    }

    private void setDefaultCountryCodes() {
        if(TextConstants.DEFAULT_COUNTRY_INDIA)
        {
            countryId = TextConstants.IN_COUNTRYNAME;
            countrycode = String.valueOf(TextConstants.INDIA_COUNTRYCODE);
        }
        else
        {
            countryId = TextConstants.US_COUNTRYNAME;
            countrycode = String.valueOf(TextConstants.US_COUNTRYCODE);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_old_user:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
                break;
            case R.id.btn_proceed:
                if(!isValidationsSuccessful())
                    return;
                Bundle bundle = new Bundle();
                bundle.putString(TextConstants.LOGIN_ID, spinnerCountryCode.getSelectedItem().toString() + edMobileNumber.getText().toString());
                CheckMultiplePermission();
                break;
        }
    }

        private void CheckMultiplePermission() {
            List<String> permissionsNeeded = new ArrayList<String>();
            final List<String> permissionsList = new ArrayList<String>();
            if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
                permissionsNeeded.add(getActivity().getString(R.string.permission_phone_state));

            if (BuildConfig.isPhoneEnabled && !addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
                permissionsNeeded.add(getActivity().getString(R.string.permission_sms_recieve));

            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = getActivity().getString(R.string.permission_msg)+ " "+permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        FragmentCompat.requestPermissions(EnterLoginIdScreen.this, permissionsList.toArray(new String[permissionsList.size()]),
                                                TextConstants.PERMISSION_REQUEST_CODE);
                                    }
                                }
                            });
                    return;
                }
                //System dialogs generated for each permission.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    FragmentCompat.requestPermissions(EnterLoginIdScreen.this, permissionsList.toArray(new String[permissionsList.size()]),
                            TextConstants.PERMISSION_REQUEST_CODE);
                }
                return;
            }
            // otp flow
            sendOtpApi();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                permissionsList.add(permission);
                return false;
            } else if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED && !FragmentCompat.shouldShowRequestPermissionRationale(EnterLoginIdScreen.this, permission)) {
                permissionsList.add(permission);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case TextConstants.PERMISSION_REQUEST_CODE: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    onPermissionGranted(permissions[0]);
                } else if (perms.get(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // Permission Denied
                    onPermissionNotGranted(permissions[0]);
                }

            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onPermissionGranted(String permission) {
        sendOtpApi();
    }

    private void sendOtpApi() {
        ((LoginSignupActivity)getActivity()).setResendOTPCode(false);
        String deviceUid = SharedPref.getDeviceUid();
        if (deviceUid == null) {
            deviceUid = Utils.getDeviceUUid(getActivity());
            SharedPref.setDeviceUid(deviceUid);
        }
        mTransactionId = System.currentTimeMillis();
        mListener.setIdInActivity(mTransactionId);
        if (SharedPref.getIsMobileEnabled()) {
            StringTokenizer stringTokenizer = new StringTokenizer(spinnerCountryCode.getSelectedItem().toString().trim(), ",");
            countrycodeplus = "+" + stringTokenizer.nextToken().toString().trim();
            numbertext = edMobileNumber.getText().toString().trim();
        }
        if (SharedPref.getIsMobileEnabled()) {
            SharedPref.setMobileNumber(numbertext);
            SharedPref.setCountryCode(countrycodeplus);
        }
        if(Utils.showDialogForNoNetwork(getActivity(),false)) {
            showProgressDialog(getResources().getString(R.string.loading_txt));
            ThreadManager.getDefaultExecutorService().submit(new SendOtpRequest(numbertext, countrycodeplus, SOURCE, true, mTransactionId, deviceUid));
            ((SignUpActivity)getActivity()).setLoginId(countrycodeplus+numbertext);
            ((SignUpActivity) getActivity()).setCountrycode(countrycodeplus);
            ((SignUpActivity) getActivity()).setPhoneNo(numbertext);
            ((SignUpActivity) getActivity()).setDeviceUID(SharedPref.getDeviceUid());
        }
    }

    @Override
    public void onPermissionNotGranted(String permission) {
        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                //denied
                DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.deny_permission),getActivity().getResources().getString(R.string.app_name), getActivity().getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                return;
            }
            DialogUtils.showAlertDialogCommon(getActivity(), null, getActivity().getResources().getString(R.string.permission_require_message), getActivity().getResources().getString(R.string.grant_permission), getActivity().getResources().getString(R.string.exit_app), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (which == AlertDialog.BUTTON_POSITIVE) {
                        FragmentCompat.requestPermissions(EnterLoginIdScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                                TextConstants.PERMISSION_REQUEST_CODE);
                    } else if (which == AlertDialog.BUTTON_NEGATIVE) {
                        EnterLoginIdScreen.this.getActivity().onBackPressed();
                    }
                }
            });
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.allow), okListener)
                .setNegativeButton(getResources().getString(R.string.deny), null)
                .create()
                .show();
    }

    public boolean isValidationsSuccessful() {
        hideKeyboard();
        String mobileNumber = edMobileNumber.getText().toString().trim();
        if(!Utils.isValueAvailable(mobileNumber)) {
            edMobileNumber.setError(getActivity().getResources().getString(R.string.enter_mobile_number));
            return false;
        }
        else if (BuildConfig.isPhoneEnabled && Integer.parseInt(countrycode) == TextConstants.INDIA_COUNTRYCODE && (mobileNumber.length() != TextConstants.INDIA_PHONENO_LENGTH)) {
            edMobileNumber.setError(getActivity().getResources().getString(R.string.error_invalid_mobile_no));
            return false;
        } else if (BuildConfig.isPhoneEnabled && (countrycodeplus.length() + mobileNumber.length()) <= TextConstants.MIN_PHONENO_LENGTH) {
            edMobileNumber.setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            return false;
        } else if (BuildConfig.isPhoneEnabled && (countrycodeplus.length() + mobileNumber.length()) > TextConstants.MAX_PHONENO_LENGTH) {
            edMobileNumber.setError(getActivity().getResources().getString(R.string.mobile_must_6digits_including));
            return false;
        }
        return true;
    }
}
