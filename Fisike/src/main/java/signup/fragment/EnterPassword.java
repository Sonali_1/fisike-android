package signup.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.utils.ThreadManager;

import login.activity.LoginActivity;
import signup.OnGetTransactionId;
import signup.activity.SignUpActivity;
import signup.request.GetCountryStateMapRequest;
import signup.request.ResetPasswordRequest;

/**
 * Created by aastha on 6/28/2017.
 */

public class EnterPassword extends BaseFragment implements View.OnClickListener {

    private OnGetTransactionId mListener;
    private CustomEditTextWhite password, confirmPassword;
    private CustomFontTextView password_hint;
    private long mTransactionId;
    private String otp;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionId) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }

    public static EnterPassword getInstance(Bundle bundle) {
        EnterPassword fragment = new EnterPassword();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void findView() {
        password = (CustomEditTextWhite) getView().findViewById(R.id.password);
        confirmPassword = (CustomEditTextWhite) getView().findViewById(R.id.confirm_password);
        password_hint = (CustomFontTextView) getView().findViewById(R.id.enter_a_password);
        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        confirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        confirmPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
        confirmPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    Utils.hideKeyboard(getActivity());
                    return true;
                }
                return true;
            }
        });
        setOnActionListener();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_enter_password;
    }

    @Override
    public void initView() {

        if (getActivity() instanceof SignUpActivity) {
            ((SignUpActivity) getActivity()).setToolbar(SignUpActivity.ENTER_PASSWORD);
        } else {
            ((LoginActivity) getActivity()).setToolbar(LoginActivity.ENTER_PASSWORD);
        }
        password_hint.setText(getResources().getText(R.string.enter_password_policy));
        if (BuildConfig.isToCallCountryAPI) {
            mTransactionId = System.currentTimeMillis();
            mListener.setIdInActivity(mTransactionId);
            ThreadManager.getDefaultExecutorService().submit(new GetCountryStateMapRequest(mTransactionId, false));
        }
    }

    @Override
    public void bindView() {
        password.setPasswordKeyListener();
        confirmPassword.setPasswordKeyListener();

        if (getArguments() != null)
            otp = getArguments().getString(MphRxUrl.K.OTP, "");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed: {
                if (isValidationsSuccessful()) {
                    if (getActivity() instanceof SignUpActivity) {
                        ((SignUpActivity) getActivity()).setPasswordValidated(true);
                        ((SignUpActivity) getActivity()).setPassword(password.getText().toString());
                        ((SignUpActivity) getActivity()).onClick(view);
                    } else {
                        sendResetPasswordAPI();
                    }
                }
            }
        }
    }

    private void sendResetPasswordAPI() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            mTransactionId = System.currentTimeMillis();
            mListener.setIdInActivity(mTransactionId);
            showProgressDialog(getResources().getString(R.string.loading_txt));
            ThreadManager.getDefaultExecutorService().submit(new ResetPasswordRequest(otp, confirmPassword.getText().toString(), mTransactionId));
        }
    }

    public boolean isValidationsSuccessful() {
        hideKeyboard();
        boolean valueToReturn = true;
        if (!Utils.isValueAvailable(password.getText().toString())) {
            password.setError(getActivity().getResources().getString(R.string.please_enter_password));
            valueToReturn = false;
        }
      /*  else if(password.getText().toString().length()<6)
        {
            password.setError(getResources().getString(R.string.password_length_error,6));
            valueToReturn = false;
        }*/
        else if (!Utils.isPasswordMatch(password.getText().toString().trim())) {
            password.setError(getResources().getString(R.string.incorrect_password_pattern));
            valueToReturn = false;
        }
        if (!Utils.isValueAvailable(confirmPassword.getText().toString())) {
            confirmPassword.setError(getActivity().getResources().getString(R.string.please_enter_password));
            valueToReturn = false;
        } else if (!Utils.isPasswordMatch(confirmPassword.getText().toString().trim())) {
            confirmPassword.setError(getResources().getString(R.string.incorrect_password_pattern));
            valueToReturn = false;
        }
        if (valueToReturn == false)
            return false;
        if (!password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
            confirmPassword.setError(getActivity().getResources().getString(R.string.password_mismatch));
            return false;
        }
        return true;
    }

    private void setOnActionListener() {
        password.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                confirmPassword.requestFocus();
                return true;
            }
        });


    }
}
