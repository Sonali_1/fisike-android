package signup.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.StatusAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.fragments.CameraFragment;
import com.mphrx.fisike.fragments.ImageUploadUIFragment;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import appointment.utils.CommonTasks;
import signup.activity.SignUpActivity;

import static com.mphrx.fisike.R.id.ed_policyHoldersNumber;
import static com.mphrx.fisike.R.id.frame_other_or_medicare;

/**
 * Created by aastha on 6/27/2017.
 */

public class EnterInsuranceDetails extends signup.fragment.ImageUploadUIFragment implements signup.fragment.CameraFragment.CameraIntegration, View.OnClickListener {

    private RadioGroup rgSelection;
    private RadioButton rbSelectPicture, rbEnterManually;
    private LinearLayout ll_uploadPictureLayout, ll_medicalLayout, ll_policyLayout, ll_medicare_others;
    private CustomFontTextView tvMedicare;
    private CustomEditTextWhite policyNumber, groupId, policyHolderName, insuranceCompany;
    private ImageView frontImage, backImage;
    private IconTextView crossButton1, crossButton2;
    private FrameLayout imageLayoutfront, imageLayoutBack;
    private RelativeLayout rl_emptyLayoutFront, rl_emptyLayoutBack;
    private Bitmap frontBitmap, backBitmap;
    boolean isFirst;
    private CharSequence[] medicareArray;
    private PopupWindow mPopup;
    private CustomEditTextWhite ed_medicare_number;
    private boolean isPictureUploaded = true, isMedicare = true;
    private boolean medicareValid;
    private boolean privateValid;
    private String insurancetype = "insuranceType";
    private String medicaretype;
    private FrameLayout otherOrMedicareFrame;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_insurance_details;
    }

    public static EnterInsuranceDetails getInstance(Bundle bundle) {
        EnterInsuranceDetails fragment = new EnterInsuranceDetails();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void findView() {
        rgSelection = (RadioGroup) getView().findViewById(R.id.rb_select_pic);
        rbSelectPicture = (RadioButton) getView().findViewById(R.id.rb_take_picture);
        rbEnterManually = (RadioButton) getView().findViewById(R.id.rb_enter_manually);
        ll_uploadPictureLayout = (LinearLayout) getView().findViewById(R.id.ll_photo_layout);
        ll_medicalLayout = (LinearLayout) getView().findViewById(R.id.ll_medicare);
        tvMedicare = (CustomFontTextView) getView().findViewById(R.id.ed_medicare_type);
        imageLayoutfront = (FrameLayout) getView().findViewById(R.id.frameLayout1);
        imageLayoutBack = (FrameLayout) getView().findViewById(R.id.frameLayout2);
        frontImage = (ImageView) getView().findViewById(R.id.img_insurance1);
        backImage = (ImageView) getView().findViewById(R.id.img_insurance2);
        crossButton1 = (IconTextView) getView().findViewById(R.id.crossButton1);
        crossButton2 = (IconTextView) getView().findViewById(R.id.crossButton2);
        rl_emptyLayoutFront = (RelativeLayout) getView().findViewById(R.id.rl_cameraCentre1);
        rl_emptyLayoutBack = (RelativeLayout) getView().findViewById(R.id.rl_cameraCentre2);
        ed_medicare_number = (CustomEditTextWhite) getView().findViewById(R.id.ed_medicare_number);
        ll_medicare_others = (LinearLayout) getView().findViewById(R.id.ll_medicare_others);
        policyNumber = (CustomEditTextWhite) getView().findViewById(R.id.ed_policy_name);
        groupId = (CustomEditTextWhite) getView().findViewById(R.id.ed_group_name);
        policyHolderName = (CustomEditTextWhite) getView().findViewById(R.id.ed_policyHoldersNumber);
        insuranceCompany = (CustomEditTextWhite) getView().findViewById(R.id.ed_insurance_company);
        otherOrMedicareFrame = (FrameLayout) getView().findViewById(frame_other_or_medicare);
    }

    @Override
    public void initView() {
        ((SignUpActivity) getActivity()).setToolbar(SignUpActivity.INSURANCE_DETAILS);
        cameraIntegrationListener = this;
        medicareArray = getResources().getTextArray(R.array.medicare_type);
        medicaretype = getResources().getString(R.string.insurance_type_medicare);
        setImeOptions();
    }

    @Override
    public void bindView() {
        rgSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (i == R.id.rb_take_picture) {
                    ll_medicalLayout.setVisibility(View.GONE);
                    ll_uploadPictureLayout.setVisibility(View.VISIBLE);
                    isPictureUploaded = true;
                } else if (i == R.id.rb_enter_manually) {
                    ll_uploadPictureLayout.setVisibility(View.GONE);
                    ll_medicalLayout.setVisibility(View.VISIBLE);
                    ed_medicare_number.setVisibility(View.VISIBLE);
                    ll_medicare_others.setVisibility(View.GONE);
                    tvMedicare.setText(getResources().getString(R.string.medicare_text));
                    ed_medicare_number.setHint(getResources().getString(R.string.medicare_number));
                    isPictureUploaded = false;
                    tvMedicare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPopUpMenu(tvMedicare);
                        }
                    });
                }
            }


        });
        frontImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst = true;
                showBottomSheet();
            }
        });
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst = false;
                showBottomSheet();
            }
        });
        crossButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeImage(true);
            }
        });
        crossButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeImage(false);
            }
        });
    }

    private void showPopUpMenu(CustomFontTextView spinnerMedicare) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.select_status_popup, null);
        ListView list = (ListView) layout.findViewById(R.id.listStatus);
        list.setDivider(null);

        String[] arrayPopupString = new String[medicareArray.length];
        for (int i = 0; i < medicareArray.length; i++) {
            arrayPopupString[i] = medicareArray[i].toString();
        }
        final StatusAdapter adapter = new StatusAdapter(getActivity(), R.layout.text_status, arrayPopupString);

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                medicaretype = adapter.getItem(position);
                tvMedicare.setText(medicaretype);
                if (mPopup != null && mPopup.isShowing()) {
                    mPopup.dismiss();
                }
                onItemSelected(medicaretype);
            }
        });

        int width = tvMedicare.getWidth();
        mPopup = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopup.setBackgroundDrawable(new BitmapDrawable());
        mPopup.setFocusable(false);
        mPopup.setOutsideTouchable(true);
        mPopup.showAsDropDown(spinnerMedicare, (int) Utils.convertDpToPixel(0, getActivity()), -(int) Utils.convertDpToPixel(0, getActivity()));
    }

    private void onItemSelected(String selectedType) {
        if (selectedType.equals(getResources().getString(R.string.others))) {
            medicaretype = getActivity().getResources().getString(R.string.private_txt);
            ed_medicare_number.setVisibility(View.GONE);
            ll_medicare_others.setVisibility(View.VISIBLE);
            isMedicare = false;
        } else {
            ed_medicare_number.setVisibility(View.VISIBLE);
            ed_medicare_number.setText("");
            ed_medicare_number.setHint(getHint(medicaretype));
            ll_medicare_others.setVisibility(View.GONE);
            isMedicare = true;
        }
    }

    private String getHint(String medicaretype) {
        if (medicaretype.equalsIgnoreCase(getResources().getString(R.string.insurance_type_medicare))) {
            return getResources().getString(R.string.medicare_number);
        } else
            return getResources().getString(R.string.medicaid_number);
    }

    private void removeImage(boolean isFirst) {
        if (isFirst) {
            frontBitmap = null;
            rl_emptyLayoutFront.setVisibility(View.VISIBLE);
            crossButton1.setVisibility(View.GONE);
            frontImage.setImageDrawable(null);
        } else {
            backBitmap = null;
            rl_emptyLayoutBack.setVisibility(View.VISIBLE);
            crossButton2.setVisibility(View.GONE);
            backImage.setImageDrawable(null);
        }
    }

    @Override
    public void setExtractedBitmap(Bitmap bitmap) {
        if (isFirst) {
            frontBitmap = Utils.getCurvedCornerBitmap(bitmap, 30);
            frontImage.setImageBitmap(frontBitmap);
            rl_emptyLayoutFront.setVisibility(View.GONE);
            imageLayoutfront.setVisibility(View.VISIBLE);
            crossButton1.setVisibility(View.VISIBLE);
            crossButton1.bringToFront();
        } else {
            backBitmap = Utils.getCurvedCornerBitmap(bitmap, 30);
            backImage.setImageBitmap(backBitmap);
            rl_emptyLayoutBack.setVisibility(View.GONE);
            imageLayoutBack.setVisibility(View.VISIBLE);
            crossButton2.setVisibility(View.VISIBLE);
            crossButton2.bringToFront();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TextConstants.INTENT_CONSTANT:
                if (null != data && resultCode == getActivity().RESULT_OK) {
                }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed: {
                JSONObject coverage = null;
                coverage = new JSONObject();
                if (isPictureUploaded) {
                    if (isPicturesValidated()) {
                        try {
                            String typePicture = getString(R.string.insurance_type_picture);
                            coverage.put(insurancetype, typePicture);
                            if (frontBitmap != null)
                                coverage.put("frontImage", Utils.convertBitmapToBase64(frontBitmap));
                            else
                                coverage.put("frontImage", "");
                            if (backBitmap != null)
                                coverage.put("backImage", Utils.convertBitmapToBase64(backBitmap));
                            else
                                coverage.put("backImage", "");
                            coverage.put("frontImageMimeType", "image/jpg");
                            coverage.put("backImageMimeType", "image/jpg");
                            ((SignUpActivity) getActivity()).setCoveragePayload(coverage);
                            ((SignUpActivity) getActivity()).setInsuranceDetailsValidated(true);
                            ((SignUpActivity) getActivity()).setInsuranceType(typePicture);
                            ((SignUpActivity) getActivity()).setFrontImage(Utils.convertBitmapToBase64(frontBitmap));
                            ((SignUpActivity) getActivity()).setBackImage(Utils.convertBitmapToBase64(backBitmap));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                } else {
                    if (isMedicare) {
                        if (isMedicareValid()) {
                            try {
                                coverage.put(insurancetype, medicaretype);
                                coverage.put("policyNumber", ed_medicare_number.getText().toString());
                                ((SignUpActivity) getActivity()).setCoveragePayload(coverage);
                                ((SignUpActivity) getActivity()).setInsuranceDetailsValidated(true);
                                ((SignUpActivity) getActivity()).setInsuranceType(medicaretype);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        if (isPrivateValid()) {
                            try {
                                coverage.put(insurancetype, medicaretype);
                                coverage.put("policyNumber", policyNumber.getText().toString());
                                coverage.put("primaryInsuranceCompany", insuranceCompany.getText().toString());
                                coverage.put("policyHolderName", policyHolderName.getText().toString());
                                coverage.put("groupId", groupId.getText().toString());
                                ((SignUpActivity) getActivity()).setCoveragePayload(coverage);
                                ((SignUpActivity) getActivity()).setInsuranceDetailsValidated(true);
                                ((SignUpActivity) getActivity()).setInsuranceType(medicaretype);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    public boolean isPicturesValidated() {
        boolean returnValue = true;
        if (frontBitmap == null) {
            Toast.makeText(MyApplication.getAppContext(), getActivity().getResources().getString(R.string.upload_frontimage), Toast.LENGTH_SHORT).show();
            returnValue = false;
        } else if (backBitmap == null) {
            Toast.makeText(MyApplication.getAppContext(), getActivity().getResources().getString(R.string.upload_backimage), Toast.LENGTH_SHORT).show();
            returnValue = false;
        }
        return returnValue;
    }

    public boolean isMedicareValid() {
        hideKeyboard();
        if (!Utils.isValueAvailable(ed_medicare_number.getText().toString())) {
            ed_medicare_number.setError(getActivity().getResources().getString(R.string.enter_medicare_number));
            return false;
        }
        return true;
    }

    public boolean isPrivateValid() {
        hideKeyboard();
        boolean valueToreturn = true;
        if (!Utils.isValueAvailable(policyHolderName.getText().toString())) {
            policyHolderName.setError(getActivity().getResources().getString(R.string.please_enter_valid_value));
            valueToreturn = false;
        }
        if (!Utils.isValueAvailable(insuranceCompany.getText().toString())) {
            insuranceCompany.setError(getActivity().getResources().getString(R.string.please_enter_valid_value));
            valueToreturn = false;
        }
        if (!Utils.isValueAvailable(groupId.getText().toString())) {
            groupId.setError(getActivity().getResources().getString(R.string.please_enter_valid_value));
            valueToreturn = false;
        }
        if (!Utils.isValueAvailable(policyNumber.getText().toString())) {
            policyNumber.setError(getActivity().getResources().getString(R.string.please_enter_valid_value));
            valueToreturn = false;
        }

        return valueToreturn;
    }

    private void setImeOptions() {


        ed_medicare_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideKeyboard();
                return true;
            }
        });

        insuranceCompany.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                groupId.requestFocus();
                return true;
            }
        });

        groupId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                policyNumber.requestFocus();
                return true;
            }
        });

        policyNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                policyHolderName.requestFocus();
                return true;
            }
        });

        policyHolderName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideKeyboard();
                return true;
            }
        });


    }
}
