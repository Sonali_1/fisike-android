package signup.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.createUserProfile.ContactAdminActivity;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.platform.ConfigManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.view.EditTextSplitView;
import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.TimerView;

import login.activity.LoginActivity;
import signup.OnGetTransactionId;
import signup.activity.LoginSignupActivity;
import signup.activity.SignUpActivity;
import signup.request.SendOtpRequest;
import signup.request.VerifyOtpSignupRequest;

/**
 * Created by Aastha Jain on 6/22/2017.
 */

public class OTPFragment extends BaseFragment  {

    private EditTextSplitView mOTPText;
    private TextView tvOtpVerifyText;
    private TimerView mTimerProgressBar;
    private String mobileHeader,phoneNumber;
    private boolean hasSmsPermission;
    private CountDownTimer mCountTimer;
    private RelativeLayout autoVerificationOngoing, autoVerificationFailed;
    private CustomFontTextView tvNumber;
    private OnGetTransactionId mListener;
    private String SOURCE ="SELF_SIGN_UP_MOBILE";
    private long mTransactionId;
    private ImageView imgEdit;
    private TextView tvOtpFailedText;
    private CustomFontButton btnProceed;
    private LinearLayout editLayout;

    public static OTPFragment getInstance(Bundle bundle) {
        OTPFragment fragment = new OTPFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_otp_verify;
    }

    @Override
    public void findView() {
        if (getView() == null)
            throw new RuntimeException("Get view cannot be null.");

        autoVerificationOngoing = (RelativeLayout) getView().findViewById(R.id.rl_timer_running);
        autoVerificationFailed = (RelativeLayout) getView().findViewById(R.id.rl_auto_verification_failed);
        tvOtpVerifyText = (TextView) getView().findViewById(R.id.tv_otp_verify_text_patient);
        tvOtpFailedText = (TextView) getView().findViewById(R.id.tv_auto_verification_failed);
        mTimerProgressBar = (TimerView) getView().findViewById(R.id.pb_custom_timer_patient);
        mOTPText = (EditTextSplitView) getView().findViewById(R.id.et_manual_otp_patient);
        editLayout = (LinearLayout) getView().findViewById(R.id.edit_layout);
        tvNumber  = (CustomFontTextView) editLayout.findViewById(R.id.tv_number_otp_sent);
        imgEdit = (ImageView) editLayout.findViewById(R.id.edit_number);
        readArguments(getArguments());
    }

    public void readArguments(Bundle bundle) {
        mobileHeader = bundle.getString(TextConstants.LOGIN_ID, "");
        phoneNumber = bundle.getString(VariableConstants.PHONE_NUMBER,"");
        if(Utils.isValueAvailable(mobileHeader)) {
            tvNumber.setText(mobileHeader);
        }
    }

    private String getVerificationText(String smsOrEmail) {
        if (hasSmsPermission) {
            return smsOrEmail;
        } else
            return getActivity().getResources().getString(R.string.sent_email);
    }


    @Override
    public void initView() {
        if (getActivity() instanceof SignUpActivity) {
            ((SignUpActivity) getActivity()).setToolbar(SignUpActivity.OTP_VERIFICATION);
        }
        else {
            ((LoginActivity) getActivity()).setToolbar(LoginActivity.OTP_VERIFICATION);
        }
    }

    @Override
    public void bindView() {
        if (SharedPref.getIsMobileEnabled() || !Utils.isValueAvailable(mobileHeader)) {
            mCountTimer = new CountDownTimer(30 * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimerProgressBar.setTimerProgress(30, millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    ((LoginSignupActivity) getActivity()).onSMSAutoDetectionTimeComplete();
                    autoVerificationOngoing.setVisibility(View.GONE);
                    autoVerificationFailed.setVisibility(View.VISIBLE);
                }
            };
        }
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    getActivity().onBackPressed();
            }
        });

        if(SharedPref.getIsMobileEnabled())
            startTimerPatient();
        else
            setTextAndTimerVisibility(getResources().getString(R.string.sent_sms_non_mobile));

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                if (mOTPText.getValue().length() == 4) {
                    sendVerifyOtpAPI();
                } else {
                    if (getActivity() instanceof SignUpActivity) {
                        ((SignUpActivity) getActivity()).setOtpVerified(false);
                    }
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_valid_4_digit_code), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_resend_otp:
                resendOtp();
                break;
        }
    }

    private void sendVerifyOtpAPI() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            mTransactionId = System.currentTimeMillis();
            mListener.setIdInActivity(mTransactionId);
            ((LoginSignupActivity) getActivity()).setPhoneOTP(mOTPText.getValue());
            showProgressDialog(getResources().getString(R.string.loading_txt));
            if(getActivity() instanceof LoginActivity)
                SOURCE = TextConstants.FORGOT_PASSWORD;
            ThreadManager.getDefaultExecutorService().submit(new VerifyOtpSignupRequest(SOURCE,mOTPText.getValue(),mTransactionId));
        }
    }

    private void resendOtp() {
        if (Utils.showDialogForNoNetwork(getActivity(), false)) {
            mOTPText.clearEditText();
            mTransactionId = System.currentTimeMillis();
            mListener.setIdInActivity(mTransactionId);
                ((LoginSignupActivity) getActivity()).setResendOTPCode(true);
            showProgressDialog(getResources().getString(R.string.loading_txt));
            if(getActivity() instanceof LoginActivity)
                SOURCE = TextConstants.FORGOT_PASSWORD;
            ThreadManager.getDefaultExecutorService().submit(new SendOtpRequest(mobileHeader,
                    SharedPref.getCountryCode(), SOURCE, true, mTransactionId, Utils.getDeviceUUid(getActivity())));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionId) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }


    public void displayOtpCode(String code) {
        mOTPText.setText(code);
        sendVerifyOtpAPI();
    }

    @Override
    public long getTransactionId() {
        if (getActivity() instanceof NetworkActivity) {
            mTransactionId = System.currentTimeMillis();
            return mTransactionId;
        }
        return 0;
    }

    public void startTimerPatient() {
        /*checking the case when permission is granted*/
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            setTextAndTimerVisibility(getResources().getString(R.string.sent_sms_non_mobile));
        } else if (mCountTimer != null) {
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (65 * scale + 0.5f);
            if (Utils.isRTL(getActivity())) {
                tvNumber.setPadding(0, 0, dpAsPixels, 0);
            } else {
                tvNumber.setPadding(dpAsPixels, 0, 0, 0);
            }
            autoVerificationOngoing.setVisibility(View.VISIBLE);
            autoVerificationFailed.setVisibility(View.GONE);
            mCountTimer.start();
        }
    }

    public void setVerificationText(String text){
        tvOtpVerifyText.setText(text);
    }

    public void setTextAndTimerVisibility(String string){
        setVerificationText(string);
        mTimerProgressBar.setVisibility(View.GONE);
        tvOtpVerifyText.setVisibility(View.VISIBLE);
        autoVerificationFailed.setVisibility(View.GONE);
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (30 * scale + 0.5f);
        if (Utils.isRTL(getActivity())) {
            tvNumber.setPadding(0, 0, dpAsPixels, 0);
        } else {
            tvNumber.setPadding(dpAsPixels, 0, 0, 0);
        }
    }

    @Override
    public void onDestroyView() {
        if (mCountTimer != null)
            mCountTimer.cancel();
        super.onDestroyView();
    }

}
