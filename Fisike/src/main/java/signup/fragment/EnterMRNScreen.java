package signup.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomEditTextWhite;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.utils.ThreadManager;

import signup.OnGetTransactionId;
import signup.activity.SignUpActivity;
import signup.request.FetchUserThirdParty;

/**
 * Created by Kailash Khurana on 6/22/2017.
 */

public class EnterMRNScreen extends BaseFragment implements View.OnClickListener, OnGetTransactionId{

    private CustomEditTextWhite uniqueId,edAadhar,edMrn,edSsn;
    private CustomFontButton proceed,skip;
    private OnGetTransactionId mListener;
    private long mTransactionid;

    @Override
    protected int getLayoutId() {
        return R.layout.enter_unique_identifier;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGetTransactionId) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnGetTransactionIdListener");
        }
    }
    @Override
    public void findView() {
        uniqueId = (CustomEditTextWhite) getView().findViewById(R.id.ed_unique_identifier);
        edAadhar = (CustomEditTextWhite) getView().findViewById(R.id.ed_aadhar);
        edMrn = (CustomEditTextWhite) getView().findViewById(R.id.ed_mrn);
        edSsn = (CustomEditTextWhite) getView().findViewById(R.id.ed_ssn);
        proceed = (CustomFontButton) getView().findViewById(R.id.btn_proceed);
        skip = (CustomFontButton) getView().findViewById(R.id.btn_skip);
    }

    @Override
    public void initView() {
     //   edMrn.setTransformationMethod(PasswordTransformationMethod.getInstance());
        edMrn.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideKeyboard();
                return true;
            }
        });
    }

    @Override
    public void bindView() {
        skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                    if(isMRNValidationSuccessful()){
                      if (Utils.showDialogForNoNetwork(getActivity(), false) ){
                          mTransactionid = System.currentTimeMillis();
                          mListener.setIdInActivity(mTransactionid);
                          showProgressDialog(getResources().getString(R.string.loading_txt));
                          ThreadManager.getDefaultExecutorService().submit(new FetchUserThirdParty(mTransactionid,edMrn.getText().toString()));
                          ((SignUpActivity)getActivity()).setMrnNumber(edMrn.getText().toString());                      }
                    }
                    break;
            case R.id.btn_skip:
                ((SignUpActivity)getActivity()).setToProceedFurther(true);
                break;
        }
    }


    public static EnterMRNScreen getInstance(Bundle bundle) {
        EnterMRNScreen fragment = new EnterMRNScreen();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    private boolean isMRNValidationSuccessful() {
        if(!Utils.isValueAvailable(edMrn.getText().toString())){
            edMrn.setError(getResources().getString(R.string.please_enter_valid_value));
            return false;
        }else if(edMrn.getText().toString().length()!=14) {
            edMrn.setError(getResources().getString(R.string.mrn_length_message));
            return false;
        }
        return true;
    }

    @Override
    public void setIdInActivity(long id) {

    }
}
