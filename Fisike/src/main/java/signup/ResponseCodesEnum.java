package signup;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by aastha on 7/6/2017.
 */

public enum ResponseCodesEnum {

    CODE_MOSR64("MOSR64", R.string.maximum_otps_reached_email),
    CODE_MOSR65("MOSR65", R.string.maximum_otps_reached_phone),
    CODE_MOSR66("MOSR66", R.string.maximum_otps_reached_username),
    CODE_VDOB62("VDOB62", R.string.dob_invalid),
    CODE_VDOB63("VDOB63", R.string.dob_valid),
    CODE_ONM68("ONM68",R.string.otp_unmatch),
    CODE_ONF69("ONF69",R.string.otp_invalid),
    CODE_UV17("UV17",R.string.incorrect_password_pattern);


    private String code;
    private int value;
    private static LinkedHashMap<String, ResponseCodesEnum> responseCodesEnum = null;


    private ResponseCodesEnum(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getValue() {
        try {
            return MyApplication.getAppContext().getString(value);
        } catch (Exception e) {
            return "";
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static LinkedHashMap<String, ResponseCodesEnum> getResponseCodesEnum() {
        if (responseCodesEnum == null) {
            responseCodesEnum = new LinkedHashMap<String, ResponseCodesEnum>();
            for (ResponseCodesEnum vitals : ResponseCodesEnum.values()) {
                responseCodesEnum.put(vitals.code, vitals);
            }
        }
        return responseCodesEnum;
    }

    //return code matching to particular value
    public static String getCodeFromValue(String value) {
        getResponseCodesEnum();
        for (Map.Entry<String, ResponseCodesEnum> enumMap : responseCodesEnum.entrySet())
            if (enumMap.getValue().getValue().contains(value))
                return enumMap.getValue().getCode();
        return "";
    }

    //return code matching to particular value
    public static String getDisplayedValuefromCode(String value) {
        if (value == null) {
            return "";
        }
        getResponseCodesEnum();
        if (responseCodesEnum != null) {
            for (Map.Entry<String, ResponseCodesEnum> enumMap : responseCodesEnum.entrySet())
                if (enumMap.getValue().getCode().contains(value))
                    return enumMap.getValue().getValue();
        }
        return value;
    }
}
