package signup.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mphrx.fisike.AgreementsActivity;
import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.HomeActivity;
import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.background.UserProfilePic;
import com.mphrx.fisike.constant.CustomizedTextConstants;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.Coverage;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.persistence.UserDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike.vital_submit.background.FetchVitalConfigRequest;
import com.mphrx.fisike.vital_submit.response.VitalsConfigResponse;
import com.mphrx.fisike_physician.fragment.BaseFragment;
import com.mphrx.fisike_physician.network.request.RegisterUserDeviceRequest;
import com.mphrx.fisike_physician.network.response.RegisterUserDeviceResponse;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import appointment.model.LinkedPatientInfoMO;
import appointment.profile.FetchUpdateIdentifier;
import appointment.profile.FetchUpdateTelecom;
import appointment.request.FetchSelfAndAssociatedPatients;
import appointment.request.FetchSelfAndAssociatedPatientsResponse;
import login.activity.LoginActivity;
import signup.ResponseCodesEnum;
import signup.fragment.EnterAddressDetails;
import signup.fragment.EnterInsuranceDetails;
import signup.fragment.EnterLoginIdScreen;
import signup.fragment.EnterMRNScreen;
import signup.fragment.EnterPassword;
import signup.fragment.EnterYourPersonalDetails;
import signup.fragment.OTPFragment;
import signup.request.SignupMORequest;
import signup.response.FetchUserThirdPartyResponse;
import signup.response.GetCityMapResponse;
import signup.response.GetCountryStateMapResponse;
import signup.response.SendOtpResponse;
import signup.response.SignupMOResponse;
import signup.response.VerifyOtpSignupResponse;

/**
 * Created by Kailash Khurana on 6/22/2017.
 */

public class SignUpActivity extends LoginSignupActivity implements FragmentManager.OnBackStackChangedListener {

    /**
     * TODO : email id and username signup flow
     */

    public Toolbar mToolbar;
    private ImageButton btnBack;
    private TextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private BroadcastReceiver mReceiver;
    private String otpCode;
    private String mPhoneNumber;
    private String countrycode;

    private LinkedHashMap<Integer, Boolean> fragmentsToLoad;
    private JSONArray address;
    private JSONObject coverage;
    private String dob;
    private String email;
    private String firstName;
    private String gender;
    private String lastName;
    private String password;
    private String phoneNo;
    private String userType;
    private boolean disableExistingAccount;
    private String countryCode;
    private String middleName;
    private String signupSource;
    private String loginId, aadharNo, mrnNumber;
    private String userProfilePicture, status;
    private String phoneOTP, emailOTP;
    private String backImage, frontImage, insuranceType, primaryInsuranceCompany, policyHolderName, policyNumber, groupId, deviceUID;
    private int attemptsLeft;
    private HashMap<String, ArrayList<String>> countryStateMap = new HashMap<>();
    private ArrayList<String> cityList = new ArrayList<>();
    private ArrayList<String> countryKeysList;
    private boolean isCityListFetched;
    private boolean isAddressDetailsValidated;
    private boolean isPasswordValidated;
    private boolean isInsuranceDetailsValidated;
    private boolean isOtpVerified = false;
    private boolean isPersonalDetailsValidated;
    private boolean isStateAndCityFetched;
    private boolean fetchUserAgreements;
    private String userLanguage, userProfilePictureMimeType;
    private Object payload;
    private UserMO signUpUserMo;
    private boolean isToLaunchVitalsBlockScreen;
    private Bitmap profileBitmap;
    private boolean correctAgreementStatus;
    private int fragmentLoaded;
    private boolean isBackPressed;
    private boolean isToProceedFurther;
    private boolean userAlreadyValidated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyle_themeWhite);
        setContentView(R.layout.activity_login);
        fragmentsToLoad = new LinkedHashMap<>();
        fragmentsToLoad.put(OTP_VERIFICATION, false);
        fragmentsToLoad.put(ENTER_PASSWORD,false);
        //fragmentsToLoad.put(ENTER_MRN,false);
        fragmentsToLoad.put(PERSONAL_DETAILS, false);
        fragmentsToLoad.put(ADDRESS_DETAILS, false);
        /*fragmentsToLoad.put(INSURANCE_DETAILS, false);*/
        if(getIntent().getExtras()!=null)
            addFragment(EnterLoginIdScreen.getInstance(getIntent().getExtras()), R.id.fragment_container, false);
        else
            addFragment(EnterLoginIdScreen.getInstance(null), R.id.fragment_container, false);
        mListener = SignUpActivity.this;
        SharedPref.setMobileEnabled(true);
        findView();
    }

    public void onClick(View view) {
        Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragmentById == null) {
            return;
        }

        if (fragmentById instanceof EnterLoginIdScreen) {
            ((EnterLoginIdScreen) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof OTPFragment && !isOtpVerified) {
            ((OTPFragment) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterMRNScreen && !isUserAlreadyValidated()) {
            ((EnterMRNScreen) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterYourPersonalDetails && !isPersonalDetailsValidated) {
            ((EnterYourPersonalDetails) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterAddressDetails && !isAddressDetailsValidated) {
            ((EnterAddressDetails) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterPassword && !isPasswordValidated) {
            ((EnterPassword) fragmentById).onClick(view);
            return;
        } else if (fragmentById instanceof EnterInsuranceDetails && !isInsuranceDetailsValidated) {
            ((EnterInsuranceDetails) fragmentById).onClick(view);
            return;
        }
        if (view.getId() == R.id.btn_proceed) {
            loadNextFragment();
            return;
        }
    }

    private void loadNextFragment() {
        fragmentLoaded = getFragmentToLoad();
        switch (fragmentLoaded) {
            case SIGNUP_COMPLETE:
                callSignupAPI();
                break;
            case OTP_VERIFICATION:
                Bundle bundle = new Bundle();
                bundle.putString(TextConstants.LOGIN_ID, loginId);
                bundle.putString(VariableConstants.PHONE_NUMBER, getPhoneNo());
                addFragment(OTPFragment.getInstance(bundle), OTP_VERIFICATION);
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                    registerSMSReceiver();
                }
                break;
            case PERSONAL_DETAILS:
                addFragment(EnterYourPersonalDetails.getInstance(null), PERSONAL_DETAILS);
                break;
            case ADDRESS_DETAILS:
                addFragment(EnterAddressDetails.getInstance(null), ADDRESS_DETAILS);
                break;
            case INSURANCE_DETAILS:
                addFragment(EnterInsuranceDetails.getInstance(null), INSURANCE_DETAILS);
                break;
            case ENTER_MRN:
                addFragment(EnterMRNScreen.getInstance(null), ENTER_MRN);
                break;
            case ENTER_PASSWORD:
                addFragment(EnterPassword.getInstance(null), ENTER_PASSWORD);
                break;
        }
    }

    private void callSignupAPI() {
        mTransactionId = System.currentTimeMillis();
        mListener.setIdInActivity(mTransactionId);
        showProgressDialog(getResources().getString(R.string.loading_txt));
        ThreadManager.getDefaultExecutorService().submit(new SignupMORequest(SignUpActivity.this, mTransactionId, getPayload()));
    }


    private int getFragmentToLoad() {
        Iterator<Integer> iterator = fragmentsToLoad.keySet().iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (!fragmentsToLoad.get(next)) {
                return next;
            }
        }
        return SIGNUP_COMPLETE;
    }

    private int getFragmentToPop() {
        Iterator<Integer> iterator = fragmentsToLoad.keySet().iterator();
        int index = -1;
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (fragmentsToLoad.get(next)) {
                index = next;
            } else
                return index;
        }
        return index;
    }

    @Override
    public void onBackPressed() {
        int fragmentToPop = getFragmentToPop();
        if(fragmentToPop == -1){
            Intent i = new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
        else{
            Utils.hideKeyboard(this);
            //if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                setTheme(R.style.AppStyle_themeWhite);
            fragmentsToLoad.put(fragmentToPop, false);
            getFragmentManager().popBackStack();
        }
    }

    public void addFragment(BaseFragment fragment, int fragmentKey) {
        addFragment(fragment, R.id.fragment_container, true);
        getFragmentManager().addOnBackStackChangedListener(this);
        fragmentsToLoad.put(fragmentKey, true);
    }

    @Override
    public void onBackStackChanged() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (f != null)
            updateTitle(f);
    }

    private void updateTitle(Fragment f) {
        String className = f.getClass().getName();
        initToolbar();

        if (className.equals(EnterAddressDetails.class.getName())) {
            setToolbar(ADDRESS_DETAILS);
            setAddressDetailsValidated(false);
        } else if (className.equals(EnterInsuranceDetails.class.getName())) {
            setToolbar(INSURANCE_DETAILS);
            setInsuranceDetailsValidated(false);
        } else if (className.equals(EnterMRNScreen.class.getName())) {
            setToolbar(ENTER_MRN);
            setUserAlreadyValidated(false);
        } else if (className.equals(EnterYourPersonalDetails.class.getName())) {
            setToolbar(PERSONAL_DETAILS);
            setPersonalDetailsValidated(false);
        } else if (className.equals(EnterPassword.class.getName())) {
            setToolbar(ENTER_PASSWORD);
            setPasswordValidated(false);
        } else if (className.equals(OTPFragment.class.getName())) {
            setToolbar(OTP_VERIFICATION);
            setOtpVerified(false);
        } else if(className.equals(EnterLoginIdScreen.class.getName())){
            setToolbar(ENTER_ID);
        }
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        toolbar_title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        mToolbar.setBackgroundColor(getResources().getColor(R.color.theme_btn_normal));
    }

    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void findView() {
        initToolbar();
    }

    public void setToolbar(int mode) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        mToolbar.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switch (mode) {
            case ENTER_ID:
                toolbar_title.setText(getResources().getString(R.string.sign_up));
                break;
            case PERSONAL_DETAILS:
                toolbar_title.setText(getResources().getString(R.string.your_personal_details));
                break;
            case ADDRESS_DETAILS:
                toolbar_title.setText(getResources().getString(R.string.your_address_details));
                break;
            case INSURANCE_DETAILS:
                toolbar_title.setText(getResources().getString(R.string.your_insurance_details));
                break;
            case OTP_VERIFICATION:
                toolbar_title.setText(getResources().getString(R.string.lets_verify));
                break;
            case ENTER_PASSWORD:
                toolbar_title.setText(getResources().getString(R.string.password_header));
                break;
            case ENTER_MRN:
                toolbar_title.setText(getResources().getString(R.string.your_personal_details));
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public boolean checkPermission(final String permissionType, String message, final int
            requestCode) {

        if (Build.VERSION.SDK_INT < 23)
            return true;

        if (ContextCompat.checkSelfPermission(this, permissionType) != PackageManager
                .PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                DialogUtils.showAlertDialog(this, getResources().getString(R.string.permission), message, new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_POSITIVE)
                            ActivityCompat.requestPermissions(SignUpActivity.this, new
                                    String[]{permissionType}, requestCode);
                        else
                            onPermissionNotGranted();

                    }
                });
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
            }

            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof EnterYourPersonalDetails)
                fragment.onActivityResult(requestCode, resultCode, data);
            else if (fragment instanceof EnterInsuranceDetails)
                fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterSMSReceiver();
        super.onDestroy();
    }

    public void onPermissionNotGranted(String permission) {
        Toast.makeText(this, getResources().getString(R.string.granted_access), Toast.LENGTH_SHORT).show();
    }

    public void onPermissionGranted(String permission) {

    }

    @Subscribe
    public void onFetchUserThirdPartyResponse(FetchUserThirdPartyResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            if (response.isInfoStatus()) {
                setUserAlreadyValidated(false);
                setToProceedFurther(true);
                setMrnNumber("");
                Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.mrn_invalid), Toast.LENGTH_SHORT).show();
            } else {
                setFirstName(response.getFirstname());
                setLastName(response.getLastname());
                setEmail(response.getEmail());
                setGender(response.getGender());
                setDob(response.getDob());
                setUserAlreadyValidated(true);
                setToProceedFurther(false);
            }
        } else {
            String responseStatus = response.getErrorResponseStatus();
            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onSendOtpResponse(SendOtpResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if(response.isSuccessful()){
            if(response.isInfoStatus()){
                if(!isResendOtpCode())
                    loadNextFragment();
                else
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.otp_sent_successfully),Toast.LENGTH_SHORT).show();
                    Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof OTPFragment) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                            registerSMSReceiver();
                        }
                        ((OTPFragment) fragment).startTimerPatient();
                    }
                }
            } else {
                String code = response.getInfoCode();
                if (Utils.isValueAvailable(code)) {
                    String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
                    Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            String responseStatus = response.getErrorResponseStatus();
            String code = response.getErrorCode();

            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            else if (Utils.isValueAvailable(code)) {
                String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
                Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }

    }

    @Subscribe
    public void onVerifyOtpSignup(VerifyOtpSignupResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessScenario()) {
            setOtpVerified(true);
        } else {
                /*Temp set otpverified true*/
            setOtpVerified(false);
            String responseStatus = response.getErrorResponseStatus();
            String code = response.getErrorCode();

            if (Utils.isValueAvailable(responseStatus) && ((Integer.parseInt(responseStatus) == 500) || (Integer.parseInt(responseStatus) == 400) || (Integer.parseInt(responseStatus) == 404)))
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();

            else if (Utils.isValueAvailable(code)) {
                String message = ResponseCodesEnum.getDisplayedValuefromCode(code);
                Toast.makeText(MyApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onGetCountryStateMap(GetCountryStateMapResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        if (response.isFromAddress() && response.isSuccessful()) {
            dismissProgressDialog();
            Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);
            setStateAndCityFetched(true);
            setCountryStateMap(response.getStatesAndCitiesList());
            setCountryKeysList(response.getKeysList());
            if (fragmentById instanceof EnterAddressDetails) {
                ((EnterAddressDetails) fragmentById).showPopupNames(findViewById(R.id.countryName));
            }
        } else if (response.isSuccessful()) {
            setStateAndCityFetched(true);
            setCountryStateMap(response.getStatesAndCitiesList());
            setCountryKeysList(response.getKeysList());
        } else {
            if (response.isFromAddress())
                dismissProgressDialog();
            setStateAndCityFetched(false);
        }
    }

    @Subscribe
    public void onGetCityMapResponse(GetCityMapResponse response) {
        if (mTransactionId != response.getTransactionId())
            return;
        dismissProgressDialog();
        if (response.isSuccessful()) {
            cityList = response.getCityList();
            isCityListFetched = true;
            Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
            ((EnterAddressDetails) fragment).showPopupNames(findViewById(R.id.cityName));
        } else {
            isCityListFetched = false;
            Toast.makeText(MyApplication.getAppContext(), getResources().getString(R.string.something_wrong_try_again), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onSignupResponse(SignupMOResponse signupMOResponse) {
        if (mTransactionId != signupMOResponse.getTransactionId())
            return;
        if (signupMOResponse.isSuccessful()) {
            signUpUserMo = signupMOResponse.getUser();
            signUpUserMo.setSalutation(TextConstants.NO);
            SharedPref.setUserLogout(false);
            SharedPref.setAccessToken(signUpUserMo.getToken());
            correctAgreementStatus = signUpUserMo.isCorrectAgreementStatus();
            ThreadManager.getDefaultExecutorService().submit(new RegisterUserDeviceRequest(getTransactionId()));
        } else {
            dismissProgressDialog();
            setInsuranceDetailsValidated(false);
            Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, SignUpActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

    }

    @Subscribe
    public void onVitalsConfigResponse(VitalsConfigResponse response) {

        if (response.getTransactionId() != getTransactionId())
            return;

        if (response.isSuccessful()) {
            if (response.isValidJson()) // success API JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION_SUCCESS,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS, GoogleAnalyticsConstants.VITALS_API_NAME);
            else //success API but blank JSON
                MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                                GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                        GoogleAnalyticsConstants.VITALS_VALUE_SUCCESS_INCORRECT_JSON, GoogleAnalyticsConstants.VITALS_API_NAME);

        } else
            MyApplication.getInstance().trackEvent(GoogleAnalyticsConstants.VITALS_EVENT_CATEGORY + "_" +
                            GoogleAnalyticsConstants.VITALS_API_NAME, GoogleAnalyticsConstants.VITALS_EVENT_ACTION,
                    GoogleAnalyticsConstants.VITALS_VALUE_FAILURE, GoogleAnalyticsConstants.VITALS_API_NAME);

        if (BuildConfig.isPatientApp)
            ThreadManager.getDefaultExecutorService().submit(new FetchSelfAndAssociatedPatients(getTransactionId(), TextConstants.FETCH_SELF));


    }

    @Subscribe
    public void onRegisterUserDeviceResponse(RegisterUserDeviceResponse response) {

        if (getTransactionId() != response.getTransactionId())
            return;
        if (response.isSuccessful()) {
            saveUserInfoAndLaunchHomeScreen(signUpUserMo);
            UserMO userMO = SettingManager.getInstance().getUserMO();
            if(userMO != null)
                fetchAllAgreementsRequest(userMO);
            else
            {
                dismissProgressDialog();
                Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            dismissProgressDialog();
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    public void saveUserInfoAndLaunchHomeScreen(UserMO user) {
        user.setSalutation(TextConstants.YES);
        user.setPassword(getPassword());
        if (user.getDateOfBirth() != null)
            user.setDateOfBirth(Utils.getFormattedDateEnglish(user.getDateOfBirth(),
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z,
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated));
        if (Utils.isValueAvailable(getInsuranceType()) && getInsuranceType().equals(getResources().getString(R.string.insurance_type_picture))) {
            user.getCoverage().get(0).setFrontImage(getFrontImage());
            user.getCoverage().get(0).setBackImage(getBackImage());
        }
        try {
            long userKey = SharedPref.setUserName(user.getId());
            user.setPersistenceKey(userKey);
            UserDBAdapter.getInstance(this).insert(user);
            /*After successful signup , this image is saved in internal storage and used for showing after login*/
            if (getProfileBitmap() != null)
                UserProfilePic.saveToInternalStorage(getProfileBitmap(), MyApplication.getAppContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchAllAgreementsRequest(UserMO userMO) {
        ArrayList<AgreementModel> agreementModels = userMO.getAgreements();
        if((agreementModels == null || agreementModels.size() == 0) && !correctAgreementStatus) {
            dismissProgressDialog();
            DialogUtils.showAlertDialog(this, getResources().getString(R.string.language_no_info), getResources().getString(R.string.language_not_available), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == Dialog.BUTTON_POSITIVE) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        //TODO extra text
                        intent.putExtra(Intent.EXTRA_TEXT, Utils.getEmailBodyFooterInfo(SignUpActivity.this, null) + "\n" + getString(R.string.regards));
                        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CustomizedTextConstants.EMAIL_TO});
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    } else if (which == Dialog.BUTTON_NEGATIVE) {
                        dialog.dismiss();
                        Toast.makeText(MyApplication.getAppContext(), TextConstants.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SignUpActivity.this, SignUpActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
            });
        }else if (agreementModels != null && agreementModels.size() > 0 && correctAgreementStatus) {
            dismissProgressDialog();
            if (userMO == null) {
                return;
            }
            Intent agreementsIntent = new Intent(this, AgreementsActivity.class);
            agreementsIntent.putExtra(TextConstants.FROM_SIGNUP, true);
            startActivity(agreementsIntent);
        }else {
            UserDBAdapter.getInstance(this).updateSalutation(userMO.getPersistenceKey(), TextConstants.NO);
            ThreadManager.getDefaultExecutorService().submit(new FetchVitalConfigRequest(this, getTransactionId(), false));
        }
    }

    public void setAddressPayload(JSONObject jsonAddress) {
        address = new JSONArray();
        try {
            address.put(0, jsonAddress);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setCoveragePayload(JSONObject coverage) {
        this.coverage = coverage;
    }

    public void setOtpVerified(boolean otpVerified) {
        isOtpVerified = otpVerified;
        if (isOtpVerified)
            loadNextFragment();
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public HashMap<String, ArrayList<String>> getCountryStateMap() {
        return countryStateMap;
    }

    public void setCountryStateMap(HashMap<String, ArrayList<String>> countryStateMap) {
        this.countryStateMap = countryStateMap;
    }

    public boolean isStateAndCityFetched() {
        return isStateAndCityFetched;
    }

    public ArrayList<String> getCountryKeysList() {
        return countryKeysList;
    }

    public void setCountryKeysList(ArrayList<String> countryKeysList) {
        this.countryKeysList = countryKeysList;
    }

    public void setStateAndCityFetched(boolean stateAndCityFetched) {
        isStateAndCityFetched = stateAndCityFetched;
    }

    public ArrayList<String> getCityList() {
        return cityList;
    }

    public boolean isPersonalDetailsValidated() {
        return isPersonalDetailsValidated;
    }

    public boolean isAddressDetailsValidated() {
        return isAddressDetailsValidated;
    }

    public void setAddressDetailsValidated(boolean addressDetailsValidated) {
        isAddressDetailsValidated = addressDetailsValidated;
    }

    public void setPersonalDetailsValidated(boolean personalDetailsValidated) {
        isPersonalDetailsValidated = personalDetailsValidated;
        if (isPersonalDetailsValidated)
            loadNextFragment();
    }

    public boolean isCityListFetched() {
        return isCityListFetched;
    }

    public void setCityListFetched(boolean cityListFetched) {
        isCityListFetched = cityListFetched;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUserType() {
        return "PATIENT";
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isDisableExistingAccount() {
        return true;
    }

    public void setDisableExistingAccount(boolean disableExistingAccount) {
        this.disableExistingAccount = disableExistingAccount;
    }


    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSignupSource() {
        return signupSource;
    }

    public void setSignupSource(String signupSource) {
        this.signupSource = signupSource;
    }

    public String getUserProfilePicture() {
        return userProfilePicture;
    }

    public void setUserProfilePicture(String userProfilePicture) {
        this.userProfilePicture = userProfilePicture;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        this.frontImage = frontImage;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getPrimaryInsuranceCompany() {
        return primaryInsuranceCompany;
    }

    public void setPrimaryInsuranceCompany(String primaryInsuranceCompany) {
        this.primaryInsuranceCompany = primaryInsuranceCompany;
    }

    public String getPolicyHolderName() {
        return policyHolderName;
    }

    public void setPolicyHolderName(String policyHolderName) {
        this.policyHolderName = policyHolderName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getDeviceUID() {
        return deviceUID;
    }

    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    public boolean isPasswordValidated() {
        return isPasswordValidated;
    }

    public void setPasswordValidated(boolean passwordValidated) {
        isPasswordValidated = passwordValidated;
    }

    public boolean isInsuranceDetailsValidated() {
        return isInsuranceDetailsValidated;
    }

    public void setInsuranceDetailsValidated(boolean insuranceDetailsValidated) {
        isInsuranceDetailsValidated = insuranceDetailsValidated;
        if (isInsuranceDetailsValidated)
            loadNextFragment();
    }

    public String getPayload() {
        JSONObject signupRequest = new JSONObject();
        try {
            JSONObject userFields = new JSONObject();
            userFields.put("dob", getDob());
            userFields.put("firstName", getFirstName());
            userFields.put("email", getEmail());
            userFields.put("gender", getGender());
            userFields.put("lastName", getLastName());
            userFields.put("password", getPassword());
            userFields.put("phoneNo", getPhoneNo());
            userFields.put("userType", getUserType());
            userFields.put("disableExistingAccount", isDisableExistingAccount());
            userFields.put("countryCode", getCountrycode());
            userFields.put("signUpSource", TextConstants.SELF_SIGN_UP_MOBILE);
            userFields.put("isPhoneNoVerified", true);
            userFields.put("isEmailVerified", false);
            userFields.put("phoneOTP", getPhoneOTP());
            userFields.put("emailOTP", getEmailOTP());
            JSONObject activeResouce = new JSONObject();
            activeResouce.put("status", getStatus());
            userFields.put("activeResource", activeResouce);
            userFields.put("middleName", getMiddleName());
            /*signupRequest.put("height","");
            signupRequest.put("weight","");*/
            JSONObject customFields = new JSONObject();
            customFields.put("fetchUserAgreements", isFetchUserAgreements());
            customFields.put("userLanguage", getUserLanguage());
            customFields.put("coverage", coverage);
            customFields.put("address", address);
            customFields.put("deviceUID", getDeviceUID());
            customFields.put("userProfilePicture", getUserProfilePicture());
            customFields.put("userProfilePictureMimeType", getUserProfilePictureMimeType());
            customFields.put("userAlreadyValidated", isUserAlreadyValidated());
            customFields.put("aadhar", getAadharNo());
            customFields.put(TextConstants.MRN, getMrnNumber());
            signupRequest.put("customFields", customFields);
            signupRequest.put("userFields", userFields);
            signupRequest.put("embededObject", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return signupRequest.toString();
    }

    public String getStatus() {
        return "ACTIVE";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isFetchUserAgreements() {
        return true;
    }

    public void setFetchUserAgreements(boolean fetchUserAgreements) {
        this.fetchUserAgreements = fetchUserAgreements;
    }

    public String getUserLanguage() {
        return Utils.getUserSelectedLanguage(this);
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    public String getUserProfilePictureMimeType() {
        return "image/jpg";
    }

    public void setUserProfilePictureMimeType(String userProfilePictureMimeType) {
        this.userProfilePictureMimeType = userProfilePictureMimeType;
    }

    public Bitmap getProfileBitmap() {
        return profileBitmap;
    }

    public void setProfileBitmap(Bitmap profileBitmap) {
        this.profileBitmap = profileBitmap;
    }



    public String getEmailOTP() {
        return "";
    }

    public void setEmailOTP(String emailOTP) {
        this.emailOTP = emailOTP;
    }

    public boolean isUserAlreadyValidated() {
        return userAlreadyValidated;
    }

    public void setUserAlreadyValidated(boolean userAlreadyValidated) {
        this.userAlreadyValidated = userAlreadyValidated;
    }

    public boolean isToProceedFurther() {
        return isToProceedFurther;
    }

    public void setToProceedFurther(boolean toProceedFurther) {
        isToProceedFurther = toProceedFurther;
        if (isToProceedFurther)
            loadNextFragment();
        else
            callSignupAPI();
    }

    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public String getMrnNumber() {
        return mrnNumber;
    }

    public void setMrnNumber(String mrnNumber) {
        this.mrnNumber = mrnNumber;
    }

    @Subscribe
    public void onGetLinkedPatientsResponse(FetchSelfAndAssociatedPatientsResponse fetchSelfAndAssociatedPatientsResponse) {
        if (getTransactionId() != fetchSelfAndAssociatedPatientsResponse.getTransactionId())
            return;

        long persistenceKey = SettingManager.getInstance().getUserMO().getPersistenceKey();
        if (fetchSelfAndAssociatedPatientsResponse.isSuccessful()) {
            ArrayList<LinkedPatientInfoMO> linkedPatients = fetchSelfAndAssociatedPatientsResponse.getPatientInfoMOArrayList();
            ArrayList<Coverage> coverages = linkedPatients.get(0).getCoverage();
            ArrayList<FetchUpdateTelecom> phoneList = linkedPatients.get(0).getPhoneList();
            ArrayList<FetchUpdateTelecom> emailList = linkedPatients.get(0).getEmailList();
            ArrayList<FetchUpdateIdentifier> identifiers = linkedPatients.get(0).getIdentifier();


            ContentValues contentValues = new ContentValues();
            if (coverages != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(coverages);
                    contentValues.put(UserDBAdapter.getCOVERAGE(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing phone list in UserMo
            if (phoneList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(phoneList);
                    contentValues.put(UserDBAdapter.getPHONELIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }

            //writing emailList in usermo
            if (emailList != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(emailList);
                    contentValues.put(UserDBAdapter.getEMAILLIST(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }
            //writing identifierlist in usermo
            if (identifiers != null) {
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bytes);
                    out.writeObject(identifiers);
                    contentValues.put(UserDBAdapter.getIDENTIFIER(), bytes.toByteArray());
                } catch (Exception e) {
                }
            }
            String maritalStaus = linkedPatients.get(0).getMaritalStatus();
            if (Utils.isValueAvailable(maritalStaus))
                contentValues.put(UserDBAdapter.getMARITALSTATUS(), maritalStaus);

            UserDBAdapter.getInstance(this).updateUserInfo(persistenceKey, contentValues);

            /*TODO add valid check here*/
            if (SharedPref.getIsCoverageUpdated()) {
                dismissProgressDialog();
                UserDBAdapter.getInstance(this).updateSalutation(persistenceKey, TextConstants.YES);
                Intent i = new Intent(this, HomeActivity.class);
                i.putExtra(VariableConstants.LAUNCH_UPLOAD_SCREEN, false);
                i.putExtra(VariableConstants.LAUNCH_BLOCK_VITAL_SCREEN, false);
                startActivity(i);
                this.finish();

            } else {
                UserDBAdapter.getInstance(this).updateSalutation(persistenceKey, TextConstants.NO);
                dismissProgressDialog();
                Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
                SharedPref.setAccessToken(null);
            }
        } else {

            UserDBAdapter.getInstance(this).updateSalutation(persistenceKey, TextConstants.NO);
            dismissProgressDialog();
            Toast.makeText(this, getString(R.string.patient_show_fail_error), Toast.LENGTH_SHORT).show();
            SharedPref.setAccessToken(null);
        }
    }

}


