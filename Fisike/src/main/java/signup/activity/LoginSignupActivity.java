package signup.activity;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

import com.mphrx.fisike_physician.activity.NetworkActivity;
import com.mphrx.fisike_physician.services.SMSReceiver;

import signup.OnGetTransactionId;
import signup.fragment.OTPFragment;

/**
 * Created by aastha on 8/29/2017.
 */

public class LoginSignupActivity extends NetworkActivity implements OnGetTransactionId,SMSReceiver.SMSReceiverListener{


    private BroadcastReceiver mReceiver;
    public long mTransactionId;
    public OnGetTransactionId mListener;
    public static final int PERSONAL_DETAILS = 2, ADDRESS_DETAILS = 3, INSURANCE_DETAILS = 4,ENTER_MRN=5, OTP_VERIFICATION = 0, SIGNUP_COMPLETE =-1 ,ENTER_PASSWORD =1,ENTER_ID=7;
    private String phoneOTP;
    public boolean isResendOtpCode;

    public synchronized void registerSMSReceiver() {
        if (mReceiver == null) {
            mReceiver = new SMSReceiver(this);
            registerReceiver(mReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        }
    }
    public void onSMSAutoDetectionTimeComplete() {
        unregisterSMSReceiver();
    }

    public synchronized void unregisterSMSReceiver() {
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
        mReceiver = null;
    }
    @Override
    public void onSMSReceived(String otp) {
        unregisterSMSReceiver();
        setOTPText(otp);
    }

    public void setOTPText(String code) {
        OTPFragment fragment = (OTPFragment) getFragmentByKey(OTPFragment.class);
        if (code.length() != 4)
            return;
        if (fragment == null || !fragment.isVisible()) {
            return;
        } else {
            fragment.displayOtpCode(code);
        }
    }

    @Override
    public void setIdInActivity(long id) {
        mTransactionId = id;
    }

    public String getPhoneOTP() {
        return phoneOTP;
    }
    public void setResendOTPCode(boolean value) {
        this.isResendOtpCode = value;
    }

    public void setPhoneOTP(String phoneOTP) {
        this.phoneOTP = phoneOTP;
    }

    public boolean isResendOtpCode() {
        return isResendOtpCode;
    }
}
