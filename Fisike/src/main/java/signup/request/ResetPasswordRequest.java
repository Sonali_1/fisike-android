package signup.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import signup.response.ResetPasswordResponse;
import signup.response.VerifyOtpSignupResponse;

/**
 * Created by aastha on 8/31/2017.
 */

public class ResetPasswordRequest extends BaseObjectRequest {
    private String otpCode, password ;
    private long mTransactionid;

    public ResetPasswordRequest(String otp, String password, long mTransactionId) {
        this.mTransactionid = mTransactionId;
        this.otpCode = otp;
        this.password = password;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getResetPasswordurl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new ResetPasswordResponse(error, mTransactionid));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new ResetPasswordResponse(response, mTransactionid));
    }

    public String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(MphRxUrl.K.OTP,otpCode);
            if (SharedPref.getIsMobileEnabled())
            {
                payLoad.put(MphRxUrl.K.PHONE, SharedPref.getMobileNumber());
                payLoad.put(MphRxUrl.K.COUNTRY_CODE,SharedPref.getCountryCode());
            }
            else if(SharedPref.getIsEmailEnabled())
            {
                payLoad.put(MphRxUrl.K.EMAIL,SharedPref.getEmailAddress());
            }
            else if(SharedPref.getIsUserNameEnabled())
            {
                payLoad.put(MphRxUrl.K.USERNAME,SharedPref.getLoginUserName());
            }
            payLoad.put(MphRxUrl.K.DEVICE_ID, SharedPref.getDeviceUid());
            payLoad.put(MphRxUrl.K.SOURCE, TextConstants.FORGOT_PASSWORD);
            payLoad.put("newPassword",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
}
