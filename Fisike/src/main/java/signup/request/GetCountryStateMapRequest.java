package signup.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import com.mphrx.fisike_physician.utils.SharedPref;


import org.json.JSONObject;

import signup.response.GetCountryStateMapResponse;

/**

 * Created by aastha on 7/7/2017.
 */

public class GetCountryStateMapRequest extends BaseObjectRequest {
    private long transactionID;
    private boolean fromAddress;

    public GetCountryStateMapRequest(long mTransactionId,boolean fromAddress) {
        this.transactionID = mTransactionId;
        this.fromAddress = fromAddress;
    }

    @Override
    public void doInBackground() {
        String url= MphRxUrl.getStateCityUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.GET, url,null, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new GetCountryStateMapResponse(error, transactionID));
    }


    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new GetCountryStateMapResponse(response,fromAddress, transactionID));
    }

    public boolean isFromAddress() {
        return fromAddress;
    }
}



