package signup.request;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.network.response.OTPGetStatusResponse;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import signup.response.SendOtpResponse;

/**
 * Created by aastha on 6/29/2017.
 */

public class SendOtpRequest extends BaseObjectRequest {

    private String source;
    private String loginId;
    private String countrycode;
    private boolean checkUser;
    private String deviceUid;
    private long mTransactionId;


    public SendOtpRequest(String numbertext, String countrycode, String source, boolean checkUser, long mTransactionId, String deviceUid) {
        this.source = source;
        this.loginId = numbertext;
        this.countrycode = countrycode;
        this.checkUser = checkUser;
        this.mTransactionId = mTransactionId;
        this.deviceUid = deviceUid;
    }

    @Override
    public void doInBackground() {
        String url = MphRxUrl.getSendOtpUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, url, getPayLoad(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SendOtpResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SendOtpResponse(response, mTransactionId));
    }

    public String getPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            if (SharedPref.getIsMobileEnabled())
            {
                payLoad.put(MphRxUrl.K.PHONE, SharedPref.getMobileNumber());
                if(Utils.isValueAvailable(countrycode))
                    payLoad.put(MphRxUrl.K.COUNTRY_CODE,countrycode);
                else
                    payLoad.put(MphRxUrl.K.COUNTRY_CODE,SharedPref.getCountryCode());
            }
            else if(SharedPref.getIsEmailEnabled())
            {
                payLoad.put(MphRxUrl.K.EMAIL,loginId);
            }
            else if(SharedPref.getIsUserNameEnabled())
            {
                payLoad.put(MphRxUrl.K.USERNAME,loginId);
            }
            payLoad.put(MphRxUrl.K.DEVICE_ID, deviceUid);
            payLoad.put(MphRxUrl.K.USERTYPE, Utils.getUserType());
            payLoad.put(MphRxUrl.K.SOURCE,source);
            if(checkUser)
            payLoad.put(MphRxUrl.K.CHECK_USER,checkUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

}
