package signup.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.mphrx.fisike.createUserProfile.VolleyResponse.SignUpResponse;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import signup.activity.SignUpActivity;
import signup.response.SignupMOResponse;

/**
 * Created by aastha on 7/12/2017.
 */

public class SignupMORequest extends BaseObjectRequest {
    private long mTransactionId;
    private Context context;
    private String payload;

    public SignupMORequest(Context context,long mTransactionId,String payload) {
        this.mTransactionId = mTransactionId;
        this.context = context;
        this.payload = payload;
    }

    @Override
    public void doInBackground() {
        String signUpUrl = MphRxUrl.getMoSignupUrl();
        AppLog.showInfo(getClass().getSimpleName(), signUpUrl);
        APIObjectRequest request = new APIObjectRequest(Request.Method.POST, signUpUrl,payload, this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SignupMOResponse(error, mTransactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SignupMOResponse(response, mTransactionId));
    }
}
