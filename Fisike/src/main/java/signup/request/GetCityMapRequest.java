package signup.request;

import com.android.volley.VolleyError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONException;
import org.json.JSONObject;

import signup.response.GetCityMapResponse;

/**
 * Created by rajantalwar on 7/11/17.
import signup.response.GetCountryStateMapResponse;

 */

public class GetCityMapRequest extends BaseObjectRequest {
    private final long transactionID;
    private String country, state;

    public GetCityMapRequest(long mTransactionId, String country, String state) {
        this.transactionID = mTransactionId;
        this.country = country;
        this.state = state;
    }

    @Override
    public void doInBackground() {
        String url= MphRxUrl.getCityUrl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(com.android.volley.Request.Method.POST, url,getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new GetCityMapResponse(error, transactionID));
    }


    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new GetCityMapResponse(response, transactionID));
    }

    public String getPayload() {
        JSONObject payload = new JSONObject();

        try {
            payload.put("country",country);
            payload.put("state",state);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload.toString();
    }
}
