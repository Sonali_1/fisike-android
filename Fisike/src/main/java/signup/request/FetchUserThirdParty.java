package signup.request;

import com.android.volley.VolleyError;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.MphRxUrl;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import signup.response.FetchUserThirdPartyResponse;

/**
 * Created by aastha on 7/27/2017.
 */

public class FetchUserThirdParty extends BaseObjectRequest {
    private long mTransactionid;
    private String mrnNumber;
    private String payload;

    public FetchUserThirdParty(long transactionid, String mrnNumber){
        mTransactionid = transactionid;
        this.mrnNumber = mrnNumber;
    }


    @Override
    public void doInBackground() {
        String url = MphRxUrl.getFetchThirdPartyurl();
        AppLog.showInfo(getClass().getSimpleName(), url);
        APIObjectRequest request = new APIObjectRequest(com.android.volley.Request.Method.POST, url, getPayload(), this, this);
        Network.getGeneralRequestQueue().add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new FetchUserThirdPartyResponse(error, mTransactionid));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new FetchUserThirdPartyResponse(response, mTransactionid));
    }

    public String getPayload() {
        JSONObject payload = new JSONObject();

        try {
            JSONObject customFields = new JSONObject();
              customFields.put(TextConstants.MRN,mrnNumber);
            payload.put("customFields",customFields);

            // temp fix for NH demo, later on remove after discussion with Anmol D
            JSONObject userFields = new JSONObject();
            userFields.put(MphRxUrl.K.PHONE_NUMBER, SharedPref.getMobileNumber());
            userFields.put(MphRxUrl.K.COUNTRY_CODE, SharedPref.getCountryCode());
            payload.put("userFields",userFields);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload.toString();
    }
}
