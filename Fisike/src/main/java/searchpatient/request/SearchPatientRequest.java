package searchpatient.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike_physician.network.APIObjectRequest;
import com.mphrx.fisike_physician.network.Network;
import com.mphrx.fisike_physician.network.request.BaseObjectRequest;
import com.mphrx.fisike_physician.utils.AppLog;
import com.mphrx.fisike_physician.utils.BusProvider;

import org.json.JSONObject;

import searchpatient.constants.URLConstants;
import searchpatient.response.SearchPatientResponse;

/**
 * Created by Kailash Khurana on 11/8/2016.
 */

public class SearchPatientRequest extends BaseObjectRequest {

    private final long transactionId;
    private final Context context;
    private String payloadJson;

    public SearchPatientRequest(long transactionId, Context context, String payloadJson) {
        this.transactionId = transactionId;
        this.context = context;
        this.payloadJson = payloadJson;
    }

    @Override
    public void doInBackground() {
        try {
            String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.SEARCH_PATIENT_API_CONSTANT;
            AppLog.showInfo(getClass().getSimpleName(), baseUrl);
            APIObjectRequest request = new APIObjectRequest(Request.Method.POST, baseUrl, payloadJson, this, this);
            Network.getGeneralRequestQueue().add(request);
        } catch (Exception JSONException) {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.showError(getClass().getSimpleName(), error.getMessage());
        BusProvider.getInstance().post(new SearchPatientResponse(error, transactionId));
    }

    @Override
    public void onResponse(JSONObject response) {
        AppLog.showInfo(getClass().getSimpleName(), response.toString());
        BusProvider.getInstance().post(new SearchPatientResponse(response, transactionId, context));
    }
}