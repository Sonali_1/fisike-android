package searchpatient.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.PastConditionsAdapter;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.models.PastConditionsModel;
import com.mphrx.fisike_physician.network.request.PastConditionRequest;
import com.mphrx.fisike_physician.network.response.PastConditionResponse;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.mphrx.fisike_physician.views.CircleImageView;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import searchpatient.model.SearchPatientResponseModel;
import searchpatient.request.PatientProfileRequest;
import searchpatient.response.PatientProfileResponse;

public class PatientProfileActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton btnBack;
    private CustomFontTextView toolbar_title;
    private IconTextView bt_toolbar_right;
    private CustomFontTextView txtUserStatus;
    private CustomFontTextView txtUserName;
    private CustomFontTextView txtUserGender;
    private CustomFontTextView txtUserDOB, txtHeader;
    private CustomFontButton btnAddAddress;
    private CustomFontTextView txtAadharHeader,txtAadhar;

    private LinearLayout layoutAddAddressFields;
    private PastConditionsAdapter pastConditionsAdapter;
    private Intent intent;
    private View pastconditions_view;
    private RecyclerView rv_past_conditions;
    private java.util.List<PastConditionsModel> pastConditionsModelList = new
            ArrayList<PastConditionsModel>();
    private RelativeLayout temp_msg_lyt;
    private ProgressBar progress;
    private CustomFontTextView tv_msg;
    private SearchPatientResponseModel searchPatientResponseModel;
    private FrameLayout frameLayout;
    private CircleImageView ivProfilePic;
    private ImageView imgProfile;

    private CardView cardViewUserAddress;
    private RelativeLayout btnChangeProfilePic;
    private CustomFontTextView tvUserNumber;
    private CustomFontTextView txtStatusHeader, txtDOBHeader;
    private byte[] profilePictureByte;


    private CustomFontTextView tv_view_more;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentItem(-1);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_patient_profile, frameLayout);
        showHideToolbar(false);
        searchPatientResponseModel = (SearchPatientResponseModel) getIntent().getSerializableExtra
                (TextConstants.PATIENT_MODEL);
        findViews();
        initViews();
        setUserProfilePic(null);
        setData();
        try {
            ThreadManager.getDefaultExecutorService().submit(new PatientProfileRequest(getTransactionId(), this, getPayLoad(searchPatientResponseModel.getPatientId())));
        } catch (JSONException e) {
        }

        if (BuildConfig.isPhysicianApp && BuildConfig.isPastConditionsEnabled) {
            pastconditions_view.setVisibility(View.VISIBLE);
            ThreadManager.getDefaultExecutorService().submit(new PastConditionRequest(getTransactionId(), this, searchPatientResponseModel.getPatientId()));
        } else if (BuildConfig.isPhysicianApp && !BuildConfig.isPastConditionsEnabled) {
            pastconditions_view.setVisibility(View.GONE);
        }

    }

    private String getPayLoad(long patientId) throws JSONException {
        JSONObject constraintsJsonObject = new JSONObject();
        JSONObject searchPatientObject = new JSONObject();
        searchPatientObject.put("_id", patientId);
        constraintsJsonObject.put("constraints", searchPatientObject);
        return constraintsJsonObject.toString();
    }

    private void findViews() {
        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        tvUserNumber = (CustomFontTextView) findViewById(R.id.tv_user_number);
        cardViewUserAddress = (CardView) findViewById(R.id.cardViewUserAddress);
        txtUserName = (CustomFontTextView) findViewById(R.id.txtUserName);
        layoutAddAddressFields = (LinearLayout) findViewById(R.id.layoutAddAdressFields);
        btnAddAddress = (CustomFontButton) findViewById(R.id.btnAddAddress);
        txtUserGender = (CustomFontTextView) findViewById(R.id.txtUserGender);
        txtUserDOB = (CustomFontTextView) findViewById(R.id.txtUserDOB);
        txtUserStatus = (CustomFontTextView) findViewById(R.id.txtUserStatus);
        txtStatusHeader = (CustomFontTextView) findViewById(R.id.txtStatusHeader);
        txtAadhar = (CustomFontTextView) findViewById(R.id.txtAadhar);
        txtAadharHeader = (CustomFontTextView) findViewById(R.id.txtAadharHeader);
        txtDOBHeader = (CustomFontTextView) findViewById(R.id.txtDOBHeader);
        ivProfilePic = (CircleImageView) findViewById(R.id.civ_user_profile_pic);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        txtHeader = (CustomFontTextView) findViewById(R.id.txtHeader);
        pastconditions_view = findViewById(R.id.pastconditions);
        rv_past_conditions = (RecyclerView) pastconditions_view.findViewById(R.id
                .rv_past_conditions);
        tv_view_more = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_view_more);
        temp_msg_lyt = (RelativeLayout) pastconditions_view.findViewById(R.id.temp_msg_lyt);
        progress = (ProgressBar) pastconditions_view.findViewById(R.id.progress);
        tv_msg = (CustomFontTextView) pastconditions_view.findViewById(R.id.tv_msg);

        rv_past_conditions.setLayoutManager(new LinearLayoutManager(this));
        rv_past_conditions.setNestedScrollingEnabled(false);
        pastConditionsAdapter = new PastConditionsAdapter(this, pastConditionsModelList, tv_view_more);
        rv_past_conditions.setAdapter(pastConditionsAdapter);
        btnChangeProfilePic = (RelativeLayout) findViewById(R.id.btn_change_profile_pic);

    }

    /**
     * Initilizing the elements value
     */
    private void setData() {
        initToolbar();
        String lastname = "";
        if (searchPatientResponseModel != null)
            lastname = searchPatientResponseModel.getLastName();
        if (lastname == null)
            lastname = "";
        txtUserName.setText((searchPatientResponseModel.getFirstName() == null ? "" : searchPatientResponseModel.getFirstName()) + " " + lastname);
        if (!(searchPatientResponseModel.getBirthDate() == null || searchPatientResponseModel.getBirthDate().trim().equals("")))
            txtUserDOB.setText(Utils.getFormattedDate(searchPatientResponseModel.getBirthDate(),
                    DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, DateTimeUtil
                            .destinationDateFormatWithoutTime));
        else {
            //hide dob value and header
            txtDOBHeader.setVisibility(View.GONE);
            txtUserDOB.setVisibility(View.GONE);
        }
        String maritalStatus = searchPatientResponseModel.getMaritalStatus();

        if(searchPatientResponseModel.getGender()  != null && !searchPatientResponseModel.getGender() .trim().equals("") && GenderEnum.getGenderEnumLinkedHashMap().containsKey(searchPatientResponseModel.getGender() )){
            txtUserGender.setText(GenderEnum.getGenderEnumLinkedHashMap().get(searchPatientResponseModel.getGender() ).getValue());
        }else{
            txtUserGender.setText(GenderEnum.getGenderEnumLinkedHashMap().get(getString(R.string.gender_other_key)).getValue());
        }

        if (maritalStatus == null || maritalStatus.equals("") || maritalStatus.equals(getResources().getString(R.string.txt_null))) {
            txtUserStatus.setVisibility(View.GONE);
            txtStatusHeader.setVisibility(View.GONE);
        } else {
            txtUserStatus.setText((maritalStatus.toUpperCase().startsWith("S")) ? getResources().getString(R.string.Single) : getResources().getString(R.string.Married));
            txtUserStatus.setVisibility(View.VISIBLE);
        }
        setAddress();
        if(!BuildConfig.isPatientApp){
            txtAadharHeader.setVisibility(View.GONE);
            txtAadhar.setVisibility(View.VISIBLE);
        }

    }

    private void setAddress() {
        ArrayList<Address> addresses = searchPatientResponseModel.getAddressList();
        if (addresses == null || addresses.size() == 0) {
            cardViewUserAddress.setVisibility(View.GONE);
            findViewById(R.id.txtAddressSubHeader).setVisibility(View.VISIBLE);
            layoutAddAddressFields.removeAllViews();
            return;
        }
        layoutAddAddressFields.removeAllViews();
        findViewById(R.id.txtAddressSubHeader).setVisibility(View.GONE);
        for (int i = 0; searchPatientResponseModel.getAddressList() != null && i <
                searchPatientResponseModel.getAddressList().size(); i++) {
            Address address = searchPatientResponseModel.getAddressList().get(i);
            addAddress(address, i);
        }
    }

    private void initViews() {
        btnAddAddress.setVisibility(View.GONE);
        btnChangeProfilePic.setVisibility(View.GONE);
        if (searchPatientResponseModel.getMobileNumber() != null && !searchPatientResponseModel.getMobileNumber().equals("") && !searchPatientResponseModel.getMobileNumber().equals(getResources().getString(R.string.txt_null))) {
            tvUserNumber.setText(searchPatientResponseModel.getMobileNumber());
        }
//        txtHeader.setVisibility(View.GONE);
    }

    /**
     * Initilizing and handling toolbar
     */

    private void initToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        toolbar_title = (CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar_title.setText(getResources().getString(R.string.patient_profile_heading));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // toolbar click handling
                PatientProfileActivity.this.finish();
            }
        });
    }

    /**
     * Add address to be shown in the UI
     *
     * @param address
     * @param index
     */
    private void addAddress(final Address address, int index) {
        int dp29 = (int) Utils.convertDpToPixel(29, this);
        int dp12 = (int) Utils.convertDpToPixel(12, this);
        int dp7 = (int) Utils.convertDpToPixel(7, this);
        int dp20 = (int) Utils.convertDpToPixel(20, this);
        CustomFontTextView txtAddressHeader = new CustomFontTextView(this);
        txtAddressHeader.setText(((!address.getUseCode().equals(getResources().getString(R.string.txt_null))) ? address.getUseCode() : ""));
        txtAddressHeader.setTextColor(getResources().getColor(R.color.dusky_blue_50));
        txtAddressHeader.setTextSize(12);
        CustomFontTextView txtAddress = new CustomFontTextView(this);

        String postal = address.getPostalCode();
        String city = address.getCity();
        String state = address.getState();
        String country = address.getCountry();
        ArrayList<String> line = address.getLine();
        String addLine = new String();
        for (int i = 0; i < line.size(); i++)
            if (!line.get(i).trim().equals("") || !line.get(i).trim().equals(getResources().getString(R.string.txt_null)))
                addLine += line.get(i) + ", ";

        String tempAddress = new String();
        if (addLine != null)
            tempAddress += addLine;
        if (city != null && !city.equals(getResources().getString(R.string.txt_null)) && !city.equals(""))
            tempAddress += city + ", ";
        if (state != null && !state.equals(getResources().getString(R.string.txt_null)) && !state.equals(""))
            tempAddress += state + ", ";
        if (postal != null && !postal.equals(getResources().getString(R.string.txt_null)) && !postal.equals(""))
            tempAddress += postal;
        if (country != null && !country.equals(getResources().getString(R.string.txt_null)) && !country.equals(""))
            tempAddress += ", " + country;
        txtAddress.setText(tempAddress);
        txtAddress.setTextColor(getResources().getColor(R.color.dusky_blue_70));
        txtAddress.setTextSize(14);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                .MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(dp29, 0, dp29, 0);
        txtAddress.setLayoutParams(lp);
        txtAddress.setGravity(Gravity.CENTER_VERTICAL);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        txtAddressHeader.setLayoutParams(lp);
        txtAddressHeader.setGravity(Gravity.CENTER_VERTICAL);
        relativeLayout.addView(txtAddressHeader);
        relativeLayout.setGravity(Gravity.CENTER_VERTICAL);
        LinearLayout.LayoutParams relativeLayoutParams = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        relativeLayoutParams.setMargins(dp29, index > 0 ? 30 : 0, dp7, 0);
        relativeLayout.setLayoutParams(relativeLayoutParams);
        if (index > 0) {
            View view = new View(this);
            LinearLayout.LayoutParams lineView = new LinearLayout.LayoutParams(LinearLayout
                    .LayoutParams.MATCH_PARENT, 3);
            lineView.setMargins(0, dp12, 0, 0);
            view.setLayoutParams(lineView);
            view.setBackgroundColor(getResources().getColor(R.color.blue_grey_20));
            layoutAddAddressFields.addView(view);
        }
        layoutAddAddressFields.addView(relativeLayout);
        layoutAddAddressFields.addView(txtAddress);
    }

    @Subscribe
    public void onPatientProfilePicResponse(PatientProfileResponse patientProfileResponse) {
        if (getTransactionId() != patientProfileResponse.getTransactionId()) {
            return;
        }
        if (patientProfileResponse.getBitmap() != null) {
            setUserProfilePic(patientProfileResponse.getBitmap());
        }
    }

    @Subscribe
    public void onPastConditionResponse(PastConditionResponse response) {
        if (getTransactionId() != response.getTransactionId())
            return;

        if (response.isSuccessful() && response.getmPastConditionsResponseList() != null && response.getmPastConditionsResponseList().size() > 0) {
            pastconditions_view.setVisibility(View.VISIBLE);
            pastConditionsModelList.clear();
            pastConditionsModelList.addAll(response.getmPastConditionsResponseList());
            rv_past_conditions.setVisibility(View.VISIBLE);
            temp_msg_lyt.setVisibility(View.GONE);
            pastConditionsAdapter.notifyDataSetChanged();
        } else {
            rv_past_conditions.setVisibility(View.GONE);
            temp_msg_lyt.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            tv_msg.setText(getResources().getString(R.string.No_Past_Conditions));
            pastconditions_view.setVisibility(View.GONE);
        }
    }

    private void setUserProfilePic(final Bitmap bitmap) {
        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap blurBitmap;

                if (bitmap != null) {
                    ivProfilePic.setImageBitmap(bitmap);
                    profilePictureByte = Utils.convertBitmapToByteArray(bitmap);
                    blurBitmap = Utils.blur(PatientProfileActivity.this, BitmapFactory.decodeByteArray(profilePictureByte, 0, profilePictureByte.length), 20f);
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    blurBitmap = Utils.scaleBitmapAndKeepRation(blurBitmap, (int) Utils.convertDpToPixel(187, PatientProfileActivity.this), metrics.widthPixels);
                } else {

                    ivProfilePic.setImageBitmap(BitmapFactory.decodeResource(PatientProfileActivity.this.getResources(), R.drawable.default_profile_pic));
                    blurBitmap = BitmapFactory.decodeResource(PatientProfileActivity.this.getResources(), R.drawable.bg_default_profile_bg);
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    imgProfile.setImageBitmap(blurBitmap);
                } else {
                    imgProfile.setImageBitmap(blurBitmap);
                }

            }
        });
    }

}

