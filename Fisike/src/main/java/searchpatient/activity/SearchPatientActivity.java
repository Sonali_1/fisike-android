package searchpatient.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.R;
import com.mphrx.fisike.adapter.EndLessScrollingListener;
import com.mphrx.fisike.constant.TextConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontButton;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderApiEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.mo.ChatContactMO;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.persistence.chatContactDBAdapter;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.BaseActivity;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.HealthActivity;
import com.mphrx.fisike_physician.results.model.LookupSearchModel;
import com.mphrx.fisike_physician.utils.SharedPref;
import com.mphrx.fisike_physician.utils.ThreadManager;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import careplan.activity.CarePlanActivity;
import searchpatient.adapter.SearchPatientAdapter;
import searchpatient.constants.KeyConstants;
import searchpatient.model.SearchPatientRequestModel;
import searchpatient.model.SearchPatientResponseModel;
import searchpatient.request.SearchPatientRequest;
import searchpatient.response.SearchPatientResponse;


/**
 * Created by Kailash Khurana on 11/8/2016.
 */

public class SearchPatientActivity extends BaseActivity implements SearchPatientAdapter.clickListener, SwipeRefreshLayout.OnRefreshListener, SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String SEARCH_KEY = "SEARCH_KEY";
    private RelativeLayout layoutSortOption;

    private LinearLayoutManager searchPatientlinearLayoutManager;
    private RecyclerView rvSearchedPatientsList;
    private SearchPatientAdapter searchPatientAdapter;

    // this boolean indicates whether an api call is in progess
    private boolean isLoadingSearchedPatients = false;
    private long mTransactionId;

    private ImageView ivPatientSearchBlankView;
    private List<SearchPatientResponseModel> searchedPatientList = new ArrayList<SearchPatientResponseModel>();
    private ImageView ivBlankScreenPatientSearch;
    private RadioButton rbDescending;
    private RadioButton rbAscending;
    private SwipeRefreshLayout swipeContainer;
    private CustomFontTextView tvAscending;
    private CustomFontTextView tvDescending;
    private long searchPatientTotalCount;
    private boolean isAllPatientLoaded;
    private SearchPatientRequestModel searchPatientRequestModel;
    private boolean isLoading;
    private CustomFontButton btnClearFilter;
    private CustomFontTextView tvNoResult;
    private ImageView ivFilter;
    private CustomFontTextView tvFilter;
    private CustomFontTextView tvSort;
    private boolean isStartService;
    private FrameLayout frameLayout;
    private Toolbar mToolbar;
    private LookupSearchModel searchObject;


    protected void onRestart() {
        super.onRestart();
        isStartService = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);

        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_search_patient, frameLayout);

        mToolbar = (Toolbar) frameLayout.findViewById(R.id.toolbar);
        initializeToolbar();

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(SEARCH_KEY)) {
            searchObject = (LookupSearchModel) getIntent().getExtras().get(SEARCH_KEY);
        }

        findViews();
        String filterName = SharedPref.getFilterName();
        String filterId = SharedPref.getFilterId();
        String filterMobileNumber = SharedPref.getFilterMobileNumber();
        String filterEmailId = SharedPref.getFilterEmailId();
        String countryCode = SharedPref.getPatientSearchCountryCode();
        if (filterName != null || filterId != null || filterMobileNumber != null || filterEmailId != null) {
            searchPatientRequestModel = new SearchPatientRequestModel();
            searchPatientRequestModel.setName(filterName);
            searchPatientRequestModel.setId(filterId);
            searchPatientRequestModel.setMobileNumber(filterMobileNumber);
            searchPatientRequestModel.setEmailId(filterEmailId);
            searchPatientRequestModel.setCountryCode(countryCode);
        }

        getFilterObject();

        setFilter();
        paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
        // set the adapter before calling this method.
        setInfiniteScrolling();
    }


    private void initializeToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackground(getResources().getDrawable(R.drawable.gradient_toolbar));
        } else
            mToolbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_toolbar));
        btnBack = (ImageButton) mToolbar.findViewById(R.id.bt_toolbar_cross);
        btnBack.setVisibility(View.GONE);
        bt_toolbar_right = (IconTextView) mToolbar.findViewById(R.id.bt_toolbar_right);
        bt_toolbar_right.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((CustomFontTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.patient_view_list));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_w_shadow));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }

    private void getFilterObject() {
        Bundle patient_search_filter = getIntent().getExtras();

        if (patient_search_filter.containsKey("PATIENT_SEARCH_FILTER")) {
            this.searchPatientRequestModel = (SearchPatientRequestModel) patient_search_filter.getParcelable(KeyConstants.PATIENT_SEARCH_FILTER);
            SharedPref.clearFilter();
            if (searchPatientRequestModel.getName().equals("") && searchPatientRequestModel.getId().equals("") && searchPatientRequestModel.getEmailId().equals("") && searchPatientRequestModel.getMobileNumber().equals("")) {
                this.searchPatientRequestModel = null;
            } else {
                ArrayList<String> filter = new ArrayList<>();
                filter.add(searchPatientRequestModel.getName());
                filter.add(searchPatientRequestModel.getId());
                filter.add(searchPatientRequestModel.getMobileNumber());
                filter.add(searchPatientRequestModel.getEmailId());
                filter.add(searchPatientRequestModel.getCountryCode());
                SharedPref.setFilter(filter);
            }
            setFilter();
            resetValue();
            paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
        }
    }

    private void setFilter() {
        if (searchPatientRequestModel == null) {
            tvFilter.setTextColor(getResources().getColor(R.color.dusky_blue));
            tvFilter.setTypeface(tvFilter.selectTypeface(this, null, VariableConstants.REGULAR));
            ivFilter.setBackgroundResource(R.drawable.filter_search_patient_normal);
        } else if (searchPatientRequestModel != null || (searchObject != null && Utils.isValueAvailable(searchObject.getGender()))) {
            tvFilter.setTextColor(getResources().getColor(R.color.lime_green));
            ivFilter.setBackgroundResource(R.drawable.filter_search_patient_focus);
            tvFilter.selectTypeface(this, null, VariableConstants.MEDIUM);
            /*tvFilter.setTypeface(tvFilter.getTypeface(), Typeface.BOLD);*/
            tvFilter.setTypeface(tvFilter.selectTypeface(this, null, VariableConstants.SEMI_BOLD));
        }
    }

    private void findViews() {
        layoutSortOption = (RelativeLayout) findViewById(R.id.layout_sort_option);
        ivPatientSearchBlankView = (ImageView) findViewById(R.id.iv_blank_screen_search_patient);
        rvSearchedPatientsList = (RecyclerView) findViewById(R.id.rv_search_patient);
        tvNoResult = (CustomFontTextView) findViewById(R.id.tv_no_result);
        ivFilter = (ImageView) findViewById(R.id.iv_filter);
        tvFilter = (CustomFontTextView) findViewById(R.id.tv_filter);
        searchPatientlinearLayoutManager = new LinearLayoutManager(this);
        rvSearchedPatientsList.setLayoutManager(searchPatientlinearLayoutManager);
        ivBlankScreenPatientSearch = (ImageView) findViewById(R.id.iv_blank_screen_search_patient);
        rbDescending = (RadioButton) findViewById(R.id.rb_descending);
        rbAscending = (RadioButton) findViewById(R.id.rb_ascending);
        if (SharedPref.getFilterIsAsc()) {
            rbAscending.setChecked(true);
            rbDescending.setChecked(false);
        } else {
            rbAscending.setChecked(false);
            rbDescending.setChecked(true);
        }
        tvAscending = (CustomFontTextView) findViewById(R.id.tv_ascending);
        tvDescending = (CustomFontTextView) findViewById(R.id.tv_descending);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        btnClearFilter = (CustomFontButton) findViewById(R.id.btn_clear_filter);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setEnabled(false);
        swipeContainer.setRefreshing(false);

        searchPatientAdapter = new SearchPatientAdapter(this, searchedPatientList);
        searchPatientAdapter.setClickListener(this);
        rvSearchedPatientsList.setAdapter(searchPatientAdapter);
        searchPatientAdapter.setFooterVisible(false);
        tvSort = (CustomFontTextView) findViewById(R.id.tv_sort);
        tvSort.setTypeface(tvSort.selectTypeface(this, null, VariableConstants.SEMI_BOLD));

        rbDescending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbAscending.setChecked(false);
                }
                resetValue();
                layoutSortOption.setVisibility(View.GONE);
                paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
                SharedPref.setSearchPatientSort(KeyConstants.PATIENT_SEARCH_FILTER_IS_ASC, false);
            }
        });

        rbAscending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbDescending.setChecked(false);
                }
                resetValue();
                layoutSortOption.setVisibility(View.GONE);
                paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
                SharedPref.setSearchPatientSort(KeyConstants.PATIENT_SEARCH_FILTER_IS_ASC, true);
            }
        });

        layoutSortOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSortOption.setVisibility(View.GONE);
            }
        });

    }

    private void setInfiniteScrolling() {
        rvSearchedPatientsList.setOnScrollListener(new EndLessScrollingListener(searchPatientlinearLayoutManager) {

            @Override
            public void onLoadMore(int current_page) {
                // all record loaded check
                // pagination call
                if (!isAllPatientLoaded) {
                    paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), true);
                }
            }
        });

    }

    /*@Override
    protected void onResume() {
        super.onResume();
        setCurrentItem(NavigationDrawerActivity.SEARCHPATIENTS);
        if (BuildConfig.isSearchPatient) {
            isSearchOnTop = true;
            if (SharedPref.getIsToSelectItem())
                selectItemPhysician(SharedPref.getPositionToSelect());

            if (!Utils.checkToDisplayPINScreen(SettingManager.getInstance().getUserMO()) && SharedPref.getIsToShowAlternateContactDialog())
                showDialogToAddAlternateContact();

            if (isGcmRegistrationRequired()) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }
            setCurrentItem(NavigationDrawerActivity.SEARCHPATIENTS);
        }
    }*/


    @Override
    protected void onDestroy() {
        getSharedPreferences(VariableConstants.USER_DETAILS_FILE, Context.MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @Override
    public void onMessengerServiceBind() {
        UserMO userMO = SettingManager.getInstance().getUserMO();

        if (userMO != null) {
            String userId = userMO.getId() + "";
            if (!userId.equals("")) {
                if (!chatContactDBAdapter.getInstance(this).isChatContactMoAvailable(userId)) {
                    if (getMessengerService() != null) {
                        getMessengerService().getGroupChatUsersDetails(userId);
                    }
                }
            }
            getMessengerService().changeStatus(TextConstants.STATUS_AVAILABLE, SettingManager.getInstance().getUserMO().getStatusMessage());

        }

        if (mService != null && !isStartService && !SharedPref.getIsAgreementsUpdated()) {
            mService.initiateTimers();
        } else if (mService != null && SharedPref.getIsAgreementsUpdated()) {
            mService.startGetProfileUpdatesTimer();
        }
    }

    @Override
    public void onMessengerServiceUnbind() {

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(SEARCH_KEY, searchPatientRequestModel == null ? true : false);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_filter:
                onBackPressed();
                break;
            case R.id.layout_sort:
                if (layoutSortOption.getVisibility() == View.GONE) {
                    layoutSortOption.setVisibility(View.VISIBLE);
                } else {
                    layoutSortOption.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_clear_filter:
                SharedPref.clearFilter();
                searchPatientRequestModel = null;
                resetValue();
                paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
                setFilter();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (layoutSortOption.getVisibility() == View.VISIBLE) {
            layoutSortOption.setVisibility(View.GONE);
        }
    }

    private String getPayLoad(SearchPatientRequestModel searchPatientRequestModel, boolean isAsc) throws JSONException {
        JSONObject searchPatientObject = new JSONObject();
        searchPatientObject.put("_count", 10);
        searchPatientObject.put("_skip", (searchedPatientList == null || searchedPatientList.size() == 0) ? 0 : searchedPatientList.size() - 1);
        searchPatientObject.put("sendPhotoData", false);
        if (isAsc) {
            searchPatientObject.put("_sort:asc", "lowerCaseName.family.value");
        } else {
            searchPatientObject.put("_sort:desc", "lowerCaseName.family.value");
        }
        if (searchObject != null) {
            searchPatientObject.put("reason", searchObject.getReason());
            if (Utils.isValueAvailable(searchObject.getComment())) {
                searchPatientObject.put("comment", searchObject.getComment());
            }
            searchPatientObject.put("authParams", "showAll");
            String dob = DateTimeUtil.convertSourceDestinationDate(searchObject.getDob(), DateTimeUtil.destinationDateFormatWithoutTime, DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z);
            searchPatientObject.put("birthDateFrom", dob);
            searchPatientObject.put("birthDateTo", dob);
            searchPatientObject.put("patientBTGFlow", "true");
            String gender = GenderEnum.getCodeFromValue(searchObject.getGender());
            gender = GenderApiEnum.getCodeFromValue(gender);
            String patientInfo = "{\"$and\":[{\"searchElementsList\":{\"$all\":[" + getStringSplit(searchObject.getFamilyName().toLowerCase(), "fn") + getStringSplit(searchObject.getGivenName().toLowerCase(), "gn") +" {\"$regex\":\"^ge_" + gender.toLowerCase() + ".*\"}]}}]}\"";
            searchPatientObject.put("_query", patientInfo);
        } else if (searchPatientRequestModel != null) {

                String name = searchPatientRequestModel.getName().toLowerCase();
                String id = searchPatientRequestModel.getId().toLowerCase();
                String phoneNumber = searchPatientRequestModel.getMobileNumber();
                String emailId = searchPatientRequestModel.getEmailId();
                if (Utils.isValueAvailable(name) || Utils.isValueAvailable(id) || Utils.isValueAvailable(phoneNumber) || Utils.isValueAvailable(emailId)) {
                    String query = "{\"$and\":[{\"searchElementsList\":{\"$all\":[";
                    if (Utils.isValueAvailable(name)) {
                        query += getSplitRegex(name, "pn");
                    }
                    if (Utils.isValueAvailable(id)) {
                        query += getSplitRegex(id, "id");
                    }
                    if (Utils.isValueAvailable(phoneNumber)) {
                        query += getSplitRegex(phoneNumber, "ph");
                    }
                    if (Utils.isValueAvailable(emailId)) {
                        query += getSplitRegex(emailId, "em");
                    }
                    if (query.endsWith(", ")) {
                        query = query.substring(0, query.length() - 2);
                    }

                    query += "]}}]}";
                    searchPatientObject.put("_query", query);
                }
            }
            JSONObject constraintsJsonObject = new JSONObject();
            constraintsJsonObject.put("constraints", searchPatientObject);
            return constraintsJsonObject.toString();
        }

    private String getSplitRegex(String orderId, String keyword) {
        String returnString = "";
        String str[] = orderId.split(" ");
        for (int i = 0; i < str.length; i++) {
            returnString += "{\"$regex\":\"^" + keyword + "_" + str[i] + ".*\"}, ";
        }
        return returnString;
    }

    private String getStringSplit(String string, String keyword) {
        String returnString = "";
        String str[] = string.split(" ");
        for (int i = 0; i < str.length; i++) {
            returnString += "\"" + keyword + "_" + str[i] + "\" ,";
        }
        return returnString;
    }


    private void resetValue() {
        searchPatientTotalCount = 0;
        isAllPatientLoaded = false;
        searchPatientAdapter.setFooterVisible(true);
        setInfiniteScrolling();
    }

    /**
     * common api call for filter and searchPatientpagination call
     *
     * @param searchPatientRequestModel if  null then its a searchPatientpagination\
     *                                  else filter call
     */
    private void paginationApiCall(SearchPatientRequestModel searchPatientRequestModel, boolean isAsc, boolean isScrolled) {
        this.searchPatientRequestModel = searchPatientRequestModel;
        try {
            if (searchPatientTotalCount > 0 && searchPatientAdapter != null && searchPatientAdapter.getItemCount() == searchPatientTotalCount) {
                isAllPatientLoaded = true;
                searchPatientAdapter.setFooterVisible(false);
                return;
            } else if (!isLoading) {
                mTransactionId = getTransactionId();
                if (!isScrolled) {
                    swipeContainer.setEnabled(true);
                    swipeContainer.setRefreshing(true);
                    searchedPatientList.clear();
                    searchPatientAdapter.setFooterVisible(false);
                } else {
                    searchPatientAdapter.setFooterVisible(true);
                    swipeContainer.setEnabled(false);
                }
                searchPatientAdapter.setFooterVisible(true);
                swipeContainer.setEnabled(false);
                if (Utils.showDialogForNoNetwork(this, false)) {
                    ThreadManager.getDefaultExecutorService().submit(new SearchPatientRequest(mTransactionId, this, getPayLoad(searchPatientRequestModel, isAsc)));
                    isLoading = true;
                }
            }
        } catch (JSONException e) {
        }
    }

    @Subscribe
    public void onSearchPatientResponse(SearchPatientResponse searchPatientResponse) {
        if (mTransactionId != searchPatientResponse.getTransactionId()) {
            return;
        }
        isLoading = false;

        searchPatientAdapter.setFooterVisible(false);

        swipeContainer.setEnabled(false);
        swipeContainer.setRefreshing(false);

        if (searchPatientResponse.isSuccessful()) {
            ArrayList<SearchPatientResponseModel> listSearchPatientResponseModel = searchPatientResponse.getListSearchPatientResponseModel();
            // check the size
            if (listSearchPatientResponseModel != null && listSearchPatientResponseModel.size() > 0) {
                searchedPatientList.addAll(listSearchPatientResponseModel);
                ivBlankScreenPatientSearch.setVisibility(View.GONE);
                rvSearchedPatientsList.setVisibility(View.VISIBLE);
                searchPatientAdapter.notifyDataSetChanged();
                btnClearFilter.setVisibility(View.INVISIBLE);
                tvNoResult.setVisibility(View.INVISIBLE);
                if (checkIfTopActivity())
                    Toast.makeText(this, searchPatientResponse.getTotalPaitentCount() + getResources().getString(R.string._Patients), Toast.LENGTH_SHORT).show();
            } else if (searchedPatientList.size() == 0) {
                ivBlankScreenPatientSearch.setVisibility(View.VISIBLE);
                rvSearchedPatientsList.setVisibility(View.GONE);
                tvNoResult.setVisibility(View.VISIBLE);
                if (searchPatientRequestModel != null) {
                    btnClearFilter.setVisibility(View.VISIBLE);
                } else {
                    btnClearFilter.setVisibility(View.INVISIBLE);
                }
//                swipeContainer.setEnabled(true);
            }
            searchPatientTotalCount = searchPatientResponse.getTotalPaitentCount();
            if (searchObject != null && searchObject instanceof LookupSearchModel) {
                SharedPref.updatePreferences(SharedPref.SEARCH_PATIENT_BTG_TIME, System.currentTimeMillis());
            }
        } else {
            swipeContainer.setEnabled(true);
            if (searchPatientAdapter.getItemCount() <= 0)
                ivBlankScreenPatientSearch.setVisibility(View.VISIBLE);
            Toast.makeText(this, searchPatientResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private boolean checkIfTopActivity() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = activityManager.getRunningTasks(1);
        if (taskInfo.get(0).topActivity.getClassName().equals(SearchPatientActivity.this.getClass().getName()))
            return true;
        return false;
    }

    @Override
    public void onRefresh() {
        paginationApiCall(searchPatientRequestModel, rbAscending.isChecked(), false);
    }

    @Override
    public void itemClicked(View view, int position) {
        switch (view.getId()) {
            case R.id.imgOverflow:
                showPopUpMenu((IconTextView) view.findViewById(R.id.imgOverflow), searchedPatientList.get(position), position);
                break;
            //over all row click will redirect to Patient's Health
            case R.id.ll_parent_layout:
                openPatientPatientHealth(searchedPatientList.get(position));
                break;
        }
    }

    @Override
    public void itemLongClicked(View view, int position) {

    }

    private void showPopUpMenu(final IconTextView imgOverFlow, final SearchPatientResponseModel searchPatientResponseModel, final int tempPosition) {
        final Resources resources = getResources();
        final PopupMenu popup = new PopupMenu(this, imgOverFlow);
        popup.getMenu().add(resources.getString(R.string.view_records));
        popup.getMenu().add(resources.getString(R.string.view_profile));
        //
        if (SharedPref.getDefinedRoles() != null && BuildConfig.isWhatsNextEnabled &&
                (SharedPref.getDefinedRoles().contains(TextConstants.ROLE_CARE_MANAGER) ||
                        SharedPref.getDefinedRoles().contains(TextConstants.ROLE_CARE_COORDINATOR)))
            popup.getMenu().add(resources.getString(R.string.view_care_plan));

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                String itemClicked = item.getTitle().toString();
                if (itemClicked.equalsIgnoreCase(resources.getString(R.string.view_profile))) {
                    //open profile of patient
                    openPatientProfile(searchPatientResponseModel);
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.view_records))) {
                    //open patient's MyHealth
                    openPatientPatientHealth(searchPatientResponseModel);
                } else if (itemClicked.equalsIgnoreCase(resources.getString(R.string.view_care_plan))) {
                    startActivity(new Intent(SearchPatientActivity.this, CarePlanActivity.class).putExtra(VariableConstants.PATIENT, searchPatientResponseModel.getPatientId()));
                }

                popup.dismiss();
                return true;
            }
        });
        popup.show();//showing popup menu
    }

    private void openPatientProfile(SearchPatientResponseModel searchPatientResponseModel) {
        ChatContactMO chatContactMO = new ChatContactMO();
        chatContactMO.setId(String.valueOf(searchPatientResponseModel.getPatientId()));
        chatContactMO.setGender(searchPatientResponseModel.getGender());
        chatContactMO.setFirstName(searchPatientResponseModel.getFirstName());
        chatContactMO.setLastName(searchPatientResponseModel.getLastName());
        startActivity(new Intent(SearchPatientActivity.this, PatientProfileActivity.class).putExtra(TextConstants.PATIENT_MODEL, searchPatientResponseModel));
    }


    private void openPatientPatientHealth(SearchPatientResponseModel searchPatientResponseModel) {
        SearchPatientActivity.this.startActivity(new Intent(SearchPatientActivity.this, HealthActivity.class)
                .putExtra("title", searchPatientResponseModel.getFullName())
                .putExtra("patientId", String.valueOf(searchPatientResponseModel.getPatientId())));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SharedPref.LANGUAGE_SELECTED)) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
            }
            recreate();
        }
    }

}
