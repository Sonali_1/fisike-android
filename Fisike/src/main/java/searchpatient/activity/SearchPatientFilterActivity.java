package searchpatient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mphrx.fisike.BuildConfig;
import com.mphrx.fisike.NavigationDrawerActivity;
import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.services.RegistrationIntentService;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.activity.PhysicianHeaderActivity;
import com.mphrx.fisike_physician.utils.SharedPref;

import searchpatient.fragment.SearchFragment;

/**
 * Created by kailashkhurana on 06/07/17.
 */

public class SearchPatientFilterActivity extends PhysicianHeaderActivity {

    public static final int REQUEST_CLEAR_FILTER = 1112;
    private Bundle bundle;
    private SearchFragment resultSearchFragment;
    private CustomFontTextView toolbar_title;
    private ImageButton btnRight;

    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
//        setContentView(R.layout.activity_search_filter_base);
        getLayoutInflater().inflate(R.layout.activity_search_filter_base, frameLayout);

        bundle = getIntent().getBundleExtra("bundle");

        resultSearchFragment = new SearchFragment();
        resultSearchFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentParentView, resultSearchFragment, "resultSearchFragment")
                .addToBackStack(null)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void onClick(View view) {
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
        if (fragmentById instanceof SearchFragment) {
            ((SearchFragment) fragmentById).onClick(view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentItem(NavigationDrawerActivity.SEARCHPATIENTS);
        if (BuildConfig.isSearchPatient) {
            if (!Utils.checkToDisplayPINScreen(SettingManager.getInstance().getUserMO()) && SharedPref.getIsToShowAlternateContactDialog())
                showDialogToAddAlternateContact();

            if (isGcmRegistrationRequired()) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CLEAR_FILTER && data != null && data.getExtras() != null && data.getExtras().containsKey(SearchPatientActivity.SEARCH_KEY) && data.getExtras().getBoolean(SearchPatientActivity.SEARCH_KEY)) {
            Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragmentParentView);
            if (fragmentById instanceof SearchFragment) {
                ((SearchFragment) fragmentById).onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
}
