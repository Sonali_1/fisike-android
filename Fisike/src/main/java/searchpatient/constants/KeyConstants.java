package searchpatient.constants;

/**
 * Created by Kailash Khurana on 11/9/2016.
 */

public class KeyConstants {
    public final static String PATIENT_SEARCH_FILTER = "PATIENT_SEARCH_FILTER";
    public static final String PATIENT_SEARCH_FILTER_NAME = "PATIENT_SEARCH_FILTER_NAME";
    public static final String PATIENT_SEARCH_FILTER_ID = "PATIENT_SEARCH_FILTER_ID";
    public static final String PATIENT_SEARCH_FILTER_MOBILE_NUMBER = "PATIENT_SEARCH_FILTER_MOBILE_NUMBER";
    public static final String PATIENT_SEARCH_FILTER_EMAIL_ID = "PATIENT_SEARCH_FILTER_EMAIL_ID";
    public static final String PATIENT_SEARCH_FILTER_IS_ASC = "PATIENT_SEARCH_FILTER_IS_ASC";
    public static final String PATIENT_SEARCH_FILTER_COUNTRY_CODE = "PATIENT_SEARCH_FILTER_COUNTRY_CODE";
    public static final String RESULT_FILTER = "RESULT_FILTER";
}
