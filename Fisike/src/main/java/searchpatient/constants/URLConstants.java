package searchpatient.constants;

/**
 * Created by Kailash Khurana on 11/8/2016.
 */

public class URLConstants {

    public static final String SEARCH_PATIENT_API_CONSTANT = "/moPatient/search";
    public static final String PATIENT_PHOTO_API_CONSTANT = "/MoPatient/getMobilePatientPhoto";
    public static final String BTG_API = "/selectList/getReasonListForBreakTheGlass";
}
