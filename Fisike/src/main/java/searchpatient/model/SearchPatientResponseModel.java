package searchpatient.model;

import java.io.Serializable;

import com.mphrx.fisike.gson.request.Address;

import java.util.ArrayList;

/**
 * Created by xmb2nc on 09-11-2016.
 */

public class SearchPatientResponseModel implements Serializable {

    private static final long serialVersionUID = -2494726010845421334L;
    private long patientId;
    private String externalPatientId;
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private String prefferedName;
    private String birthDate;
    private String gender;
    private byte[] photo;
    private String mobileNumber;
    private String maritalStatus;
    private ArrayList<Address> addressList;
    private String deceasedDate;

    private int backgroundColor;


    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getExternalPatientId() {
        if (externalPatientId == null || externalPatientId.equals("null")) {
            return "";
        }
        return externalPatientId;
    }

    public void setExternalPatientId(String externalPatientId) {
        this.externalPatientId = externalPatientId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null && firstName.equals("null")) {
            this.firstName = null;
            return;
        }
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        if (middleName != null && middleName.equals("null")) {
            this.middleName = null;
            return;
        }
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName != null && lastName.equals("null")) {
            this.lastName = null;
            return;
        }
        this.lastName = lastName;
    }

    public String getPrefferedName() {
        if (prefferedName == null || prefferedName.equals("null")) {
            return "";
        }
        return prefferedName;
    }

    public void setPrefferedName(String prefferedName) {
        this.prefferedName = prefferedName;
    }

    public String getBirthDate() {
        if (birthDate == null || birthDate.equals("null")) {
            return "";
        }
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        if (gender == null || gender.equals("null")) {
            return "";
        }
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }


    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public ArrayList<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<Address> addressList) {
        this.addressList = addressList;
    }

    public String getDeceasedDate() {
        return deceasedDate;
    }

    public void setDeceasedDate(String deceasedDate) {
        this.deceasedDate = deceasedDate;
    }
}
