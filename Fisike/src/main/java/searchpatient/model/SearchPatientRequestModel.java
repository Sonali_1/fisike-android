package searchpatient.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xmb2nc on 09-11-2016.
 */

public class SearchPatientRequestModel implements Parcelable {
    private String name;
    private String id;
    private String mobileNumber;
    private String emailId;
    private String countryCode;

    public SearchPatientRequestModel() {

    }

    public SearchPatientRequestModel(Parcel in) {
        name = in.readString();
        id = in.readString();
        mobileNumber = in.readString();
        emailId = in.readString();
        countryCode = in.readString();
    }

    public static final Creator<SearchPatientRequestModel> CREATOR = new Creator<SearchPatientRequestModel>() {
        @Override
        public SearchPatientRequestModel createFromParcel(Parcel in) {
            return new SearchPatientRequestModel(in);
        }

        @Override
        public SearchPatientRequestModel[] newArray(int size) {
            return new SearchPatientRequestModel[size];
        }
    };

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        if (id == null) {
            return "";
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileNumber() {
        if (mobileNumber == null) {
            return "";
        }
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        if (emailId == null) {
            return "";
        }
        return emailId;
    }

    public String getCountryCode() {
        if(countryCode==null){
            return "";
        }
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(mobileNumber);
        dest.writeString(emailId);
        dest.writeString(countryCode);
    }
}
