package searchpatient.bacground;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.mphrx.fisike.enums.BTGEnum;
import com.mphrx.fisike.platform.APIManager;
import com.mphrx.fisike.platform.ApiResult;
import com.mphrx.fisike_physician.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import searchpatient.constants.URLConstants;
import searchpatient.database.BTGReasonAdapter;
import searchpatient.fragment.SearchBTGFragment;

/**
 * Created by kailashkhurana on 08/08/17.
 */

public class FetchReasons extends AsyncTask<Void, Void, Void> {

    private ProgressDialog progressDialog;
    private Context context;
    private SearchBTGFragment searchBTGFragment;
    private ProgressBar progressBar;
    private ArrayList<String> reasonList;

    public FetchReasons(Context context, SearchBTGFragment searchBTGFragment, ProgressBar progressBar) {
        this.context = context;
        this.searchBTGFragment = searchBTGFragment;
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (SharedPref.isTrue(SharedPref.IS_BTG_ACCESS)) {
            try {
                reasonList = BTGReasonAdapter.getInstance(context).fetchReasons();
            } catch (Exception e) {
            }
        }
        String baseUrl = APIManager.createMinervaBaseUrl() + URLConstants.BTG_API;

        try {
            ApiResult response = APIManager.getInstance().executeHttpPost(baseUrl, new JsonObject(), true, context);
            if (response == null) {
                return null;

            }
            JSONObject carePlanJsonObject = new JSONObject(response.getJsonString().toString());
            JSONArray patientJsonArray = carePlanJsonObject.optJSONArray("reasonList");
            reasonList = new ArrayList<>();
            for (int i = 0; patientJsonArray != null && i < patientJsonArray.length(); i++) {
                reasonList.add(BTGEnum.getDisplayedValuefromCode(patientJsonArray.getString(i)));
            }
            BTGReasonAdapter.getInstance(context).insertBTGReasons(reasonList);
            SharedPref.updatePreferences(SharedPref.IS_BTG_ACCESS, true);
        } catch (JSONException jsonException) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (searchBTGFragment != null) {
            searchBTGFragment.setReasonsList(reasonList);
        }
        progressBar.setVisibility(View.GONE);
        super.onPostExecute(aVoid);
    }
}
