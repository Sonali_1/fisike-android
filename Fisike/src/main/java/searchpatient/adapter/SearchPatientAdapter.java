
package searchpatient.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.enums.GenderApiEnum;
import com.mphrx.fisike.enums.GenderEnum;
import com.mphrx.fisike.icomoon.IconTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.views.CircleImageView;

import java.text.ParseException;
import java.util.List;

import searchpatient.model.SearchPatientResponseModel;

/**
 * Created by Kailash Khurana on 11/7/2016.
 */

public class SearchPatientAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public SearchPatientAdapter.clickListener listener;
    private List<SearchPatientResponseModel> searchedPatientList;
    private Context context;
    private boolean isVisible;
    private final int TYPE_ITEM = 0;
    private final int TYPE_FOOTER = 1;

    public SearchPatientAdapter(Context context, List<SearchPatientResponseModel> list) {
        this.context = context;
        this.searchedPatientList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        if (arg1 == TYPE_ITEM) {
            View row = LayoutInflater.from(arg0.getContext()).inflate(R.layout.patient_search_row, arg0, false);
            SearchViewHolder searchViewHolder = new SearchViewHolder(row);
            return searchViewHolder;
        } else if (arg1 == TYPE_FOOTER) {
            View footerView = LayoutInflater.from(arg0.getContext()).inflate(R.layout.loading_view, arg0, false);
            FooterViewHolder footerViewHolder = new FooterViewHolder(footerView);
            return footerViewHolder;
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (searchedPatientList == null) {
            return 0;
        }
        if (isVisible) {
            return searchedPatientList.size() + 1;
        } else {
            return searchedPatientList.size();
        }
    }

    public void setFooterVisible(boolean isVisible) {
        this.isVisible = isVisible;
        notifyDataSetChanged();
    }

    private boolean isPositionFooter(int position) {
        return position == searchedPatientList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder recyclerViewHolder, int position) {
        if (recyclerViewHolder instanceof SearchViewHolder) {
            SearchViewHolder holder = (SearchViewHolder) recyclerViewHolder;
            SearchPatientResponseModel searchedPatientModel = searchedPatientList.get(position);
            holder.txt_patient_name.setText(searchedPatientModel.getFullName());
            if (searchedPatientModel.getPrefferedName().equals("")) {
                holder.txt_prefferd_name.setVisibility(View.GONE);
                holder.lblPrefferedName.setVisibility(View.GONE);
            } else {
                holder.txt_prefferd_name.setText(searchedPatientModel.getPrefferedName());
                holder.txt_prefferd_name.setVisibility(View.VISIBLE);
                holder.lblPrefferedName.setVisibility(View.VISIBLE);
            }
            if (searchedPatientModel.getBirthDate().equals("")) {
                holder.txt_age.setVisibility(View.GONE);
                holder.lblAge.setVisibility(View.GONE);
                holder.txt_born.setText(context.getResources().getString(R.string.unknown));
            } else {
                holder.txt_born.setText(DateTimeUtil.getFormattedDateWithoutTime(searchedPatientModel.getBirthDate(),
                        DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
                try {
                    String deceasedDate = searchedPatientModel.getDeceasedDate();
                    String age = Utils.calculateAgeAsPerConstraints(searchedPatientModel.getBirthDate(), deceasedDate,
                            DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, false);
                    holder.txt_age.setText(age);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                holder.txt_born.setVisibility(View.VISIBLE);
                holder.txt_age.setVisibility(View.VISIBLE);
                holder.lblBorn.setVisibility(View.VISIBLE);
                holder.lblAge.setVisibility(View.VISIBLE);
            }
            if (searchedPatientModel.getExternalPatientId().equals("")) {
                holder.txt_id.setText(context.getResources().getString(R.string.unknown));
            } else {
                holder.txt_id.setText(searchedPatientModel.getExternalPatientId());
                holder.txt_id.setVisibility(View.VISIBLE);
                holder.lblId.setVisibility(View.VISIBLE);
            }
            if (searchedPatientModel.getGender().equals("")) {
                holder.txt_gender.setVisibility(View.GONE);
                holder.lblGender.setVisibility(View.GONE);
            } else {
                String gender = GenderApiEnum.getDisplayedValuefromCode(searchedPatientModel.getGender());
                gender = GenderEnum.getDisplayedValuefromCode(gender);
                holder.txt_gender.setText(gender);
                holder.txt_gender.setVisibility(View.VISIBLE);
                holder.lblGender.setVisibility(View.VISIBLE);
            }
            if (searchedPatientModel.getDeceasedDate() != null && !searchedPatientModel.getDeceasedDate().equals("")) {
                holder.txtDied.setText(DateTimeUtil.getFormattedDateWithoutTime(searchedPatientModel.getDeceasedDate(),
                        DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z));
                holder.txtDied.setVisibility(View.VISIBLE);
                holder.lblDied.setVisibility(View.VISIBLE);
                try {
                    String age = Utils.calculateAgeAsPerConstraints(searchedPatientModel.getBirthDate(), searchedPatientModel.getDeceasedDate(), DateTimeUtil.yyyy_MM_dd_HH_mm_ss_Hiphen_Seperated_Z, false);
                    holder.txt_age.setText(age);
                    holder.txt_age.setVisibility(View.VISIBLE);
                    holder.lblAge.setVisibility(View.VISIBLE);
                    holder.lblAge.setText(context.getResources().getString(R.string.txt_patient_age_at_death));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                holder.txtDied.setVisibility(View.GONE);
                holder.lblDied.setVisibility(View.GONE);
            }
            if (searchedPatientModel.getPhoto() != null && searchedPatientModel.getPhoto().length > 0) {
                holder.imgProfile.setVisibility(View.VISIBLE);
                holder.txtNameShorts.setVisibility(View.GONE);
                Bitmap bitmap = BitmapFactory.decodeByteArray(searchedPatientModel.getPhoto(), 0, searchedPatientModel.getPhoto().length);
                holder.imgProfile.setImageBitmap(bitmap);
            } else {
                holder.imgProfile.setVisibility(View.GONE);
                holder.txtNameShorts.setVisibility(View.VISIBLE);
                String firstName = searchedPatientModel.getFirstName();
                String lastName = searchedPatientModel.getLastName();
                if (firstName != null && !firstName.trim().equals("") && lastName != null && !lastName.trim().equals("")) {
                    holder.txtNameShorts.setText(firstName.trim().substring(0, 1).toUpperCase() + "" + lastName.trim().substring(0, 1).toUpperCase());
                } else if (firstName != null && !firstName.equals("")) {
                    holder.txtNameShorts.setText(firstName.trim().substring(0, 1).toUpperCase());
                } else if (lastName != null && !lastName.equals("")) {
                    holder.txtNameShorts.setText(lastName.trim().substring(0, 1).toUpperCase());
                }
                if (firstName == null && lastName == null) {
                    holder.txtNameShorts.setText(context.getString(R.string.u));
                    setColor(holder.txtNameShorts, R.color.unkown_gray);
                } else {
                    setColor(holder.txtNameShorts, searchedPatientModel.getBackgroundColor());
                }
            }
        }
    }

    //view holder for supportAdapter
    public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final CustomFontTextView lblPrefferedName;
        private final CustomFontTextView lblBorn;
        private final CustomFontTextView lblAge;
        private final CustomFontTextView lblId;
        private final CustomFontTextView lblStatus;
        private final CustomFontTextView lblGender;
        private final CircleImageView imgProfile;
        private final CustomFontTextView lblDied;
        private final CustomFontTextView txtDied;
        private CustomFontTextView txt_patient_name, txtNameShorts, txt_prefferd_name, txt_born, txt_id,
                txt_gender, txt_age;
        private IconTextView imgOverflow;
        private LinearLayout llParentLayout; //parent element

        public SearchViewHolder(View v) {
            super(v);
            this.txt_patient_name = (CustomFontTextView) v.findViewById(R.id.txt_patient_name);
            this.txtNameShorts = (CustomFontTextView) v.findViewById(R.id.txtNameShorts);
            this.txt_prefferd_name = (CustomFontTextView) v.findViewById(R.id.txt_prefferd_name);
            this.txt_born = (CustomFontTextView) v.findViewById(R.id.txt_born);
            this.txt_id = (CustomFontTextView) v.findViewById(R.id.txt_id);
            this.txt_gender = (CustomFontTextView) v.findViewById(R.id.txt_gender);
            this.txt_age = (CustomFontTextView) v.findViewById(R.id.txt_age);
            imgOverflow = (IconTextView) v.findViewById(R.id.imgOverflow);
            lblPrefferedName = (CustomFontTextView) v.findViewById(R.id.lbl_preffered_name);
            lblBorn = (CustomFontTextView) v.findViewById(R.id.lbl_born);
            lblAge = (CustomFontTextView) v.findViewById(R.id.lbl_age);
            lblId = (CustomFontTextView) v.findViewById(R.id.lbl_id);
            lblStatus = (CustomFontTextView) v.findViewById(R.id.lbl_status);
            lblGender = (CustomFontTextView) v.findViewById(R.id.lbl_gender);
            imgProfile = (CircleImageView) v.findViewById(R.id.img_profile);
            txtDied = (CustomFontTextView) v.findViewById(R.id.txt_died);
            lblDied = (CustomFontTextView) v.findViewById(R.id.lbl_died);
            llParentLayout = (LinearLayout) v.findViewById(R.id.ll_parent_layout);
            imgOverflow.setOnClickListener(this);
            llParentLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                //handling Item Click
                listener.itemClicked(v, getAdapterPosition());
            }
        }
    }

    public interface clickListener {
        public void itemClicked(View view, int position);

        public void itemLongClicked(View view, int position);
    }

    public void setClickListener(SearchPatientAdapter.clickListener listener) {
        this.listener = (SearchPatientAdapter.clickListener) listener;
    }

    private void setColor(View view, int color) {
        GradientDrawable bgShape = (GradientDrawable) view.getBackground();
        bgShape.setColor(color);
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgMedician;
        private CustomFontTextView txtMedicationName;
        private CustomFontTextView txtDoses;
        private CustomFontTextView txtNumberOfTimes;
        private CustomFontTextView txtNextMedicine;
        private ImageButton imgOverflow;
        private CustomFontTextView txtPatientName;


        public FooterViewHolder(View v) {
            super(v);

        }
    }

}
