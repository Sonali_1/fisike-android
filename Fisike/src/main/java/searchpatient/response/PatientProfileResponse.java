package searchpatient.response;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.mphrx.fisike.gson.request.GsonUtils;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONObject;


/**
 * Created by Kailash Khurana on 11/16/2016.
 */

public class PatientProfileResponse extends BaseResponse {
    private Bitmap bitmap;

    public PatientProfileResponse(JSONObject response, long transactionId, Context context) {
        super(response, transactionId);
        parseObject(response);
    }

    private void parseObject(JSONObject response) {

        try {
//             commenting as moving to gson
//           JSONObject carePlanJsonObject = new JSONObject(response.toString());
//            JSONArray patientJsonArray = carePlanJsonObject.getJSONArray("photoArray");
//                        if (patientJsonArray != null) {
//                byte[] listSearchPatientResponseModel = new byte[patientJsonArray.length()];
//                for (int i = 0; i < patientJsonArray.length(); i++) {
//                    listSearchPatientResponseModel[i] = (byte) ((int) patientJsonArray.get(i) & 0xFF);
//                }
//                bitmap = BitmapFactory.decodeByteArray(listSearchPatientResponseModel, 0, listSearchPatientResponseModel.length);

            photoArrayGson gson = (photoArrayGson) GsonUtils.jsonToObjectMapper(response.toString(), photoArrayGson.class);
            if (gson.getPhotoArray() != null) {
                byte[] img_64format = Base64.decode(gson.getPhotoArray(), Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(img_64format, 0, img_64format.length);
            }

        } catch (Exception e) {
            bitmap = null;
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    class photoArrayGson {
        byte[] photoArray;

        public byte[] getPhotoArray() {
            return photoArray;
        }

        public void setPhotoArray(byte[] photoArray) {
            this.photoArray = photoArray;
        }
    }

}
