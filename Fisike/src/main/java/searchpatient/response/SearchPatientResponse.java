package searchpatient.response;

import android.content.Context;

import com.mphrx.fisike.R;
import com.mphrx.fisike.gson.request.Address;
import com.mphrx.fisike_physician.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import searchpatient.model.SearchPatientResponseModel;

/**
 * Created by Kailash Khurana on 11/8/2016.
 */

public class SearchPatientResponse extends BaseResponse {
    private ArrayList<SearchPatientResponseModel> listSearchPatientResponseModel;
    private long totalPaitentCount;
    private Context context;

    public SearchPatientResponse(JSONObject response, long transactionId, Context context) {
        super(response, transactionId);
        this.context = context;
        parseObject(response);
    }

    private void parseObject(JSONObject response) {
        try {
            listSearchPatientResponseModel = new ArrayList<>();
            JSONObject carePlanJsonObject = new JSONObject(response.toString());
            JSONArray patientJsonArray = carePlanJsonObject.getJSONArray("list");
            totalPaitentCount = carePlanJsonObject.optLong("totalCount");
            parsesPatientArray(patientJsonArray);
        } catch (JSONException jsonException) {
        }
    }

    public long getTotalPaitentCount() {
        return totalPaitentCount;
    }

    private void parsesPatientArray(JSONArray patientJsonArray) {
        for (int i = 0; patientJsonArray != null && i < patientJsonArray.length(); i++) {
            try {
                SearchPatientResponseModel searchPatientResponseModel = new SearchPatientResponseModel();
                JSONObject jsonObject = patientJsonArray.getJSONObject(i);
                searchPatientResponseModel.setPatientId(jsonObject.optLong("id"));
                searchPatientResponseModel.setExternalPatientId(jsonObject.optString("patientId"));
                searchPatientResponseModel.setFullName(jsonObject.optString("fullName"));
                searchPatientResponseModel.setFirstName(jsonObject.optString("firstName"));
                searchPatientResponseModel.setMiddleName(jsonObject.optString("middleName"));
                searchPatientResponseModel.setLastName(jsonObject.optString("lastName"));
                searchPatientResponseModel.setFullName(jsonObject.optString("fullName"));
                searchPatientResponseModel.setPrefferedName(jsonObject.optString("prefferedName"));
                searchPatientResponseModel.setBirthDate(jsonObject.optString("dob"));
                searchPatientResponseModel.setGender(jsonObject.optString("gender"));
                searchPatientResponseModel.setPhoto(parseprofilePic(jsonObject.optJSONArray("photo")));
                searchPatientResponseModel.setMaritalStatus(jsonObject.optString("maritalStatus"));
                searchPatientResponseModel.setMobileNumber(jsonObject.optString("mobileNumber"));
                String deceasedDate = jsonObject.optString("deceasedDate");
                searchPatientResponseModel.setDeceasedDate(deceasedDate == null || deceasedDate.equalsIgnoreCase("null") ? "" : deceasedDate);
                JSONArray addressJsonArray = jsonObject.optJSONArray("address");
                ArrayList<Address> addressArrayList = new ArrayList<>();
                for (int j = 0; addressJsonArray != null && j < addressJsonArray.length(); j++) {
                    Address address = new Address();
                    JSONObject addressObject = addressJsonArray.optJSONObject(j);
                    address.setCountry(addressObject.optString("country"));
                    address.setCity(addressObject.optString("city"));
                    address.setUseCode(addressObject.optString("useCode"));
                    address.setState(addressObject.optString("state"));
                    address.setPostalCode(addressObject.optString("postalCode"));
                    address.setText(addressObject.optString("text"));
                    JSONArray lineJsonArray = addressObject.optJSONArray("line");
                    ArrayList<String> lineArrayList = new ArrayList<>();
                    for (int k = 0; lineJsonArray != null && k < lineJsonArray.length(); k++) {
                        lineArrayList.add(lineJsonArray.optString(k));
                    }
                    address.setLine(lineArrayList);
                    addressArrayList.add(address);
                }
                searchPatientResponseModel.setAddressList(addressArrayList);

                int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
                int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
                searchPatientResponseModel.setBackgroundColor(randomAndroidColor);

//                Random rnd = new Random();
//                int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//                searchPatientResponseModel.setBackgroundColor(color);
                listSearchPatientResponseModel.add(searchPatientResponseModel);
            } catch (Exception ignore) {

            }
        }

    }

    private byte[] parseprofilePic(JSONArray jsonArray) {
        if (jsonArray == null) {
            return null;
        }
        try {
            byte[] profilePic = new byte[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                profilePic[i] = (byte) jsonArray.getInt(i);
            }
            return profilePic;
        } catch (Exception e) {

        }
        return null;
    }

    public SearchPatientResponse(Exception exception, long transactionId) {
        super(exception, transactionId);
    }

    public ArrayList<SearchPatientResponseModel> getListSearchPatientResponseModel() {
        return listSearchPatientResponseModel;
    }
}
