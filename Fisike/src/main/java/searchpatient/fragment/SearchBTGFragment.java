package searchpatient.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.DefaultConnection;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.results.fragment.ResultSearchFragment;
import com.mphrx.fisike_physician.results.model.LookupSearchModel;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import appointment.utils.CustomTypefaceSpan;
import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;
import searchpatient.bacground.FetchReasons;

/**
 * Created by kailashkhurana on 07/07/17.
 */

public class SearchBTGFragment extends SearchPatientBaseFragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private String reason;
    private String comment;
    private View myFragmentView;
    private CustomFontEditTextView edGivenName;
    private CustomFontEditTextView edFamilyName;
    private CustomFontEditTextView edDOB;
    private CustomFontEditTextView spinnerGender;
    private ArrayList<String> listBTGReason;
    private CustomFontEditTextView edReason;
    private RelativeLayout layoutBtg;
    private RelativeLayout layoutLookup;
    private CustomFontEditTextView edDescription;
    private ScrollView scrollView;
    private ProgressBar progressBar;
    private CustomFontTextView txtMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myFragmentView = inflater.inflate(R.layout.layout_lookup_view, container, false);

        findView();
        initView();

        new FetchReasons(getActivity(), this, progressBar).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return myFragmentView;
    }

    @Override
    public void onResume() {
        long hour = TimeUnit.MILLISECONDS.toHours(SharedPref.getLong(SharedPref.SEARCH_PATIENT_BTG_TIME) - System.currentTimeMillis());
        if (hour < 0 || hour > VariableConstants.BTG_SESSION_TIMOUT) {
            layoutBtg.setVisibility(View.VISIBLE);
            layoutLookup.setVisibility(View.GONE);
            reset();
        } else {
            layoutBtg.setVisibility(View.GONE);
            layoutLookup.setVisibility(View.VISIBLE);
            reason = SharedPref.getString(SharedPref.PATEINT_BTG_REASON);
            comment = SharedPref.getString(SharedPref.PATEINT_BTG_COMMENT);
        }
        super.onResume();
    }

    public void findView() {
        edReason = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_reason);
        edDescription = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_description);
        layoutLookup = (RelativeLayout) myFragmentView.findViewById(R.id.layout_lookup);
        edGivenName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_given_name);
        edFamilyName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_family_name);
        spinnerGender = (CustomFontEditTextView) myFragmentView.findViewById(R.id.spinner_gender);
        edDOB = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_dob);
        layoutBtg = (RelativeLayout) myFragmentView.findViewById(R.id.layout_btg);
        scrollView = (ScrollView) myFragmentView.findViewById(R.id.scroll_view);
        progressBar = (ProgressBar) myFragmentView.findViewById(R.id.progress_bar);
        txtMessage = (CustomFontTextView) myFragmentView.findViewById(R.id.txt_message);
    }

    public void initView() {
        Typeface font_bold = Typeface.createFromAsset(getActivity().getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_bold));
        Typeface font_regular = Typeface.createFromAsset(getActivity().getAssets(), MyApplication.getAppContext().getResources().getString(R.string.opensans_ttf_regular));

        edDescription.getEditText().setSingleLine(false);
        edDescription.getEditText().setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        edDescription.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        edDescription.getEditText().setMinLines(1);
        edDescription.getEditText().setVerticalScrollBarEnabled(true);
        edDescription.getEditText().setMovementMethod(ScrollingMovementMethod.getInstance());
        edDescription.getEditText().setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);

        String boldString = getResources().getString(R.string.bth_txt_message_value);
        String messageString = getResources().getString(R.string.txt_message);

        if (messageString.contains(boldString)) {
            int startIndex = messageString.indexOf(boldString);
            int endIndex = startIndex + boldString.length();

            SpannableString messageSpannable = new SpannableString(messageString);
            messageSpannable.setSpan(new CustomTypefaceSpan("", font_regular), 0, startIndex - 1, 0);
            messageSpannable.setSpan(new CustomTypefaceSpan("", font_bold), startIndex, endIndex, 0);
            messageSpannable.setSpan(new CustomTypefaceSpan("", font_regular), endIndex + 1, messageSpannable.length(), 0);

            txtMessage.setText(messageSpannable);
        }
        setSpinnerProperty(edReason, 0, R.drawable.dropdown);
        edReason.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(edReason);
                }
                return true;
            }
        });

        setSpinnerProperty(spinnerGender, 0, R.drawable.dropdown);
        spinnerGender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(spinnerGender);
                }
                return true;
            }
        });

        setSpinnerProperty(edDOB, 0, R.drawable.calendar);
        edDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onClick(edDOB);
                }
                return true;
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (Utils.isKeyBoardOpen(getActivity())) {
                    Utils.hideKeyboard(getActivity());
                }
                return false;
            }
        });

        edFamilyName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    v.clearFocus();
                    Utils.hideKeyboard(SearchBTGFragment.this.getActivity(), v);
                }
                return false;
            }
        });
    }

    public void onClick(View v) {
        if (layoutBtg.getVisibility() == View.VISIBLE && progressBar.getVisibility() == View.VISIBLE) {
            return;
        }
        switch (v.getId()) {
            case R.id.spinner_gender:
                showPopupMenu(R.menu.menu_gender, spinnerGender, spinnerGender);
                break;
            case R.id.btn_search:
                LookupSearchModel lookUpModel = getLookUpModel();
                if (isValidateData(lookUpModel))
                    getActivity().startActivity(new Intent(getActivity(), SearchPatientActivity.class).putExtra(SearchPatientActivity.SEARCH_KEY, lookUpModel));
                break;
            case R.id.btn_reset:
                reset();
                break;
            case R.id.ed_dob:
                showDateDialog(edDOB, SearchBTGFragment.this);
                break;
            case R.id.btn_cancel:
                ((SearchFragment) getParentFragment()).getViewPager().setCurrentItem(SearchFragment.FILTER_SEARCH);
                break;
            case R.id.btn_allow_access:
                long hour = TimeUnit.MILLISECONDS.toHours(SharedPref.getLong(SharedPref.SEARCH_PATIENT_BTG_TIME) - System.currentTimeMillis());
                if (hour > VariableConstants.BTG_SESSION_TIMOUT) {
                    layoutBtg.setVisibility(View.VISIBLE);
                    layoutLookup.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.btg_session_expire), Toast.LENGTH_SHORT).show();
                    return;
                }
                Utils.hideKeyboard(getActivity());
                reason = Utils.getText(edReason);
                comment = Utils.getText(edDescription);
                if (reason.equals("")) {
                    edReason.setError(getString(R.string.error_enter_reason));
                    return;
                }
                layoutBtg.setVisibility(View.GONE);
                layoutLookup.setVisibility(View.VISIBLE);
                SharedPref.updatePreferences(SharedPref.PATEINT_BTG_REASON, reason);
                SharedPref.updatePreferences(SharedPref.PATEINT_BTG_COMMENT, comment);
                break;
            case R.id.ed_reason:
                if (listBTGReason != null) {
                    showPopupMenu(listBTGReason, edReason);
                } else {
                    new FetchReasons(getActivity(), this, progressBar).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                break;
        }

    }

    private boolean isValidateData(LookupSearchModel lookUpModel) {
        boolean isValidateData = true;
        if (!Utils.isValueAvailable(lookUpModel.getGivenName())) {
            isValidateData = false;
            edGivenName.setError(getString(R.string.enter_given_name));
        }
        if (!Utils.isValueAvailable(lookUpModel.getFamilyName())) {
            isValidateData = false;
            edFamilyName.setError(getString(R.string.enter_family_name));
        }
        if (!Utils.isValueAvailable(lookUpModel.getGender())) {
            isValidateData = false;
            spinnerGender.setError(getString(R.string.enter_gender));
        }
        if (!Utils.isValueAvailable(lookUpModel.getDob())) {
            isValidateData = false;
            edDOB.setError(getString(R.string.enter_dob));
        }

        return isValidateData;
    }

    private void reset() {
        edGivenName.setText("");
        edFamilyName.setText("");
        edDOB.setText("");
        spinnerGender.setText("");
    }

    private LookupSearchModel getLookUpModel() {
        LookupSearchModel lookupSearchModel = new LookupSearchModel();
        lookupSearchModel.setGivenName(Utils.getText(edGivenName));
        lookupSearchModel.setFamilyName(Utils.getText(edFamilyName));
        lookupSearchModel.setGender(Utils.getText(spinnerGender));
        lookupSearchModel.setDob(Utils.getText(edDOB));
        lookupSearchModel.setReason(reason);
        lookupSearchModel.setComment(comment);
        return lookupSearchModel;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (view.isShown()) {
            String date = DateTimeUtil.getFormattedDateWithoutUTC(((month + 1) + "-" + day + "-" + year), DateTimeUtil.mm_dd_yyyy_HiphenSeperated);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edDOB.getWindowToken(), 0);
            edDOB.setText(date);
        }
    }

    public void setSearchClick() {
        if (layoutBtg.getVisibility() == View.GONE) {
            layoutBtg.setVisibility(View.VISIBLE);
            layoutLookup.setVisibility(View.GONE);

            edReason.setText("");
            edDescription.setText("");

            reset();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK && requestCode == SearchPatientFilterActivity.REQUEST_CLEAR_FILTER && data != null && data.getExtras() != null && data.getExtras().containsKey(SearchPatientActivity.SEARCH_KEY) && data.getExtras().getBoolean(SearchPatientActivity.SEARCH_KEY)) {
            reset();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setReasonsList(ArrayList<String> reason) {
        this.listBTGReason = reason;
    }
}
