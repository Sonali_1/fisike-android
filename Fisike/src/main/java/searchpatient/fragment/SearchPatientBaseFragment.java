package searchpatient.fragment;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.utils.DateTimeUtil;
import com.mphrx.fisike.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kailashkhurana on 14/07/17.
 */

public class SearchPatientBaseFragment extends Fragment implements View.OnFocusChangeListener, View.OnClickListener {

    public void showDateDialog(CustomFontEditTextView tv, DatePickerDialog.OnDateSetListener dateSetListener) {
        // Use the current date as the default date in the picker
        Calendar c = Calendar.getInstance();// Aug28, 2015
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtil.destinationDateFormatWithoutTime, Locale.getDefault());
        try {
            c.setTime(sdf.parse(tv.getText().toString().trim()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), dateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void showPopupMenu(final int menuId, View anchorView, final CustomFontEditTextView txtView) {
        final PopupMenu popup = new PopupMenu(getActivity(), anchorView);
        popup.getMenuInflater().inflate(menuId, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.setText(item.getTitle().toString());
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public void showPopupMenu(ArrayList<String> menuId, final CustomFontEditTextView txtView) {

        final PopupMenu popup = new PopupMenu(getActivity(), txtView);
        for (int i = 0; i < menuId.size(); i++) {
            popup.getMenu().add(Menu.NONE, i, Menu.NONE, menuId.get(i));
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtView.setVisibility(View.VISIBLE);
                txtView.getEditText().setText(item.getTitle());
                popup.dismiss();
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public void setSpinnerProperty(final CustomFontEditTextView customFontEditTextView, int resID, int drawableRight) {
        customFontEditTextView.getEditText().setCompoundDrawablePadding(10);
        customFontEditTextView.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, drawableRight, 0);
        customFontEditTextView.setOnClickListener(this);
        customFontEditTextView.setOnFocusChangeListener(this);
        if (resID != 0) {
            customFontEditTextView.setText(getString(resID));
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            Utils.hideKeyboard(SearchPatientBaseFragment.this.getActivity());
        }
    }

    @Override
    public void onClick(View view) {

    }
}
