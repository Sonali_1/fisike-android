package searchpatient.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.R;
import com.mphrx.fisike.customview.CustomFontEditTextView;
import com.mphrx.fisike.customview.CustomFontTextView;
import com.mphrx.fisike.utils.CountryCodeUtils;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.adapters.CountryAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import searchpatient.activity.SearchPatientActivity;
import searchpatient.activity.SearchPatientFilterActivity;
import searchpatient.constants.KeyConstants;
import searchpatient.model.SearchPatientRequestModel;

/**
 * Created by Kailash Khurana on 11/7/2016.
 */

public class SearchFilterFragment extends SearchPatientBaseFragment implements View.OnClickListener {

    private CustomFontEditTextView edName;
    private CustomFontEditTextView edPatientId;
    private CustomFontEditTextView edMobileNumber;
    private CustomFontEditTextView edEmailId;
    //    private Spinner spinnerCountryCode;
    private int position;
    private int countryCode;
    private CountryAdapter countryAdapter;
    private CustomFontTextView btToolbarRight;
    private SearchPatientRequestModel searchPatientRequestModel;
    private View myFragmentView;
    private String countryId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_search_patient_filter, container, false);
        findView();
        searchPatientRequestModel = getActivity().getIntent().getParcelableExtra(KeyConstants.PATIENT_SEARCH_FILTER);
        if (searchPatientRequestModel != null) {
            setData();
        } else {
            setCountryPosition();
        }
        setCountryCode();
//        setCurrentItem(-1);
        return myFragmentView;
    }

    private void setData() {
        if (searchPatientRequestModel.getName() != null) {
            edName.setText(searchPatientRequestModel.getName());
        }
        if (searchPatientRequestModel.getId() != null) {
            edPatientId.setText(searchPatientRequestModel.getId());
        }
        if (searchPatientRequestModel.getMobileNumber() != null) {
            edMobileNumber.setText(searchPatientRequestModel.getMobileNumber());
        }
        if (searchPatientRequestModel.getEmailId() != null) {
            edEmailId.setText(searchPatientRequestModel.getEmailId());
        }
        if (searchPatientRequestModel.getCountryCode() != null && !searchPatientRequestModel.getCountryCode().equals("")) {
            countryCode = Integer.parseInt(searchPatientRequestModel.getCountryCode());
        } else {
            setCountryPosition();
        }
    }

    private void findView() {
//        spinnerCountryCode = (Spinner) frameLayout.findViewById(spinner_change_country_code);
        edName = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_name);
        edPatientId = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_id);
        edMobileNumber = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_mobile_number);
        edEmailId = (CustomFontEditTextView) myFragmentView.findViewById(R.id.ed_email_id);
        edMobileNumber.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
        edName.setCapsWords();/*getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK && requestCode == SearchPatientFilterActivity.REQUEST_CLEAR_FILTER && data != null && data.getExtras() != null && data.getExtras().containsKey(SearchPatientActivity.SEARCH_KEY) && data.getExtras().getBoolean(SearchPatientActivity.SEARCH_KEY)) {
            resetPage();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_apply_filter:

                String name = edName.getText().toString().trim();
                String externalPatientId = edPatientId.getText().toString().trim();
                String emailId = edEmailId.getText().toString().trim();
                String mobileNumber = edMobileNumber.getText().toString().trim();
//                if (name.equals("") && externalPatientId.equals("") && emailId.equals("") && mobileNumber.equals("")) {
//                    Intent intent = new Intent();
//                    intent.putExtra(KeyConstants.PATIENT_SEARCH_FILTER, null);
//                    setResult(RESULT_OK, intent);
//                    finish();
//                }else
                if (Utils.showDialogForNoNetwork(getActivity(), false)) {
                    SearchPatientRequestModel searchPatientRequestModel = new SearchPatientRequestModel();
                    searchPatientRequestModel.setName(name);
                    searchPatientRequestModel.setId(externalPatientId);
                    searchPatientRequestModel.setEmailId(emailId);
                    if (!mobileNumber.equals("")) {
                        searchPatientRequestModel.setMobileNumber(mobileNumber);
                        searchPatientRequestModel.setCountryCode(String.valueOf(countryCode));
                    }
                    Intent intent = new Intent(getActivity(), SearchPatientActivity.class);
                    intent.putExtra(KeyConstants.PATIENT_SEARCH_FILTER, searchPatientRequestModel);
                    getActivity().startActivityForResult(intent, SearchPatientFilterActivity.REQUEST_CLEAR_FILTER);
                }
                break;
            case R.id.btn_reset:
                resetPage();
                break;
        }
    }

    private void resetPage() {
        edName.setText("");
        edPatientId.setText("");
        edMobileNumber.setText("");
        edEmailId.setText("");
        setCountryPosition();
        setCountryCode();
    }

    private void setCountryPosition() {
        boolean on = false;
        TelephonyManager telMgr = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_READY:
                on = true;
                break;
        }
        countryCode = 0;
        countryId = "";
        if (on) {
            try {
                countryId = CountryCodeUtils.getCountryId(getActivity());
                countryCode = Integer.parseInt(CountryCodeUtils.getCountryZipCode(getActivity(), countryId).toString().trim());
            } catch (Exception e) {
                countryId = "US";
                countryCode = 1;
            }
        } else if (!on) {
            countryId = "US";
            countryCode = 1;
        }

    }

    private void setCountryCode() {
        position = CountryCodeUtils.getCountryPosition(getActivity(), countryCode, countryId);

        /*spinnerCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringTokenizer stringTokenizer = new StringTokenizer(spinnerCountryCode.getSelectedItem().toString().trim(), ",");
                String countrycode = "+" + stringTokenizer.nextToken().toString().trim();

                if (countrycode.equals(TextConstants.INDIA_COUNTRYCODE)) {
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(TextConstants.INDIA_PHONENO_LENGTH);
                    edMobileNumber.setFilters(FilterArray);
                } else {
                    InputFilter[] FilterArray = new InputFilter[1];
                    *//*[  .... Moblile length set on   [ max_length- countrycode(including+) ]   ........  ]*//*
                    FilterArray[0] = new InputFilter.LengthFilter((TextConstants.MAX_PHONENOENTER_LENGTH - countrycode.length()));
                    edMobileNumber.setFilters(FilterArray);
                }
                countryCode = Integer.parseInt(countrycode.replaceAll("\\+",""));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        List<String> countrylist = new ArrayList<String>();
        countrylist.clear();
        countrylist = Arrays.asList(getResources().getStringArray(R.array.CountryCodes));
        countryAdapter = new CountryAdapter(getActivity(), countrylist, "dusky_blue");
//        spinnerCountryCode.setAdapter(countryAdapter);
//        spinnerCountryCode.setSelection(position);
    }


}
