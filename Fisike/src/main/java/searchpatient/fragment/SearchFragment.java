package searchpatient.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mphrx.fisike.MyApplication;
import com.mphrx.fisike.R;
import com.mphrx.fisike.constant.GoogleAnalyticsConstants;
import com.mphrx.fisike.constant.VariableConstants;
import com.mphrx.fisike.mo.UserMO;
import com.mphrx.fisike.platform.SettingManager;
import com.mphrx.fisike.utils.Utils;
import com.mphrx.fisike_physician.utils.DialogUtils;
import com.mphrx.fisike_physician.utils.SharedPref;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by kailashkhurana on 06/07/17.
 */

public class SearchFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener {


    public static final int FILTER_SEARCH = 0;
    private static final int FILTER_BTG = 1;
    private View myFragmentView;
    private ArrayList<Fragment> fragmentMap = new ArrayList<>();
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private boolean isBTGAccess;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_search_patient_list, container, false);

        isBTGAccess = SettingManager.getInstance().getUserMO().containAuthoritie("ROLE_ACCESS_PATIENT_BTG");

        setHasOptionsMenu(true);
        findViews();
        initView();

        MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_SEARCH_FRAGMENT);

        return myFragmentView;
    }

    private void findViews() {
        viewPager = (ViewPager) myFragmentView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) myFragmentView.findViewById(R.id.tabs);
        if (isBTGAccess) {
            tabLayout.setVisibility(View.VISIBLE);
        } else {
            tabLayout.setVisibility(View.GONE);
        }
    }


    private void initView() {
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public int getCount() {
                return isBTGAccess ? 2 : 1;
            }

            @Override
            public Fragment getItem(int position) {
                Fragment fragment;
                if (position == FILTER_SEARCH) {
                    fragment = new SearchFilterFragment();
                } else {
                    fragment = new SearchBTGFragment();
                }
                fragmentMap.add(position, fragment);
                return fragment;

            }


            @Override
            public CharSequence getPageTitle(int position) {
                CharSequence pageTitle = null;
                switch (position) {
                    case FILTER_SEARCH:
                        pageTitle = getResources().getString(R.string.my_patients);
                        break;
                    case FILTER_BTG:
                        pageTitle = getResources().getString(R.string.all_patients);
                        break;
                }
                return pageTitle;
            }


        });

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case FILTER_SEARCH:
                long hour = TimeUnit.MILLISECONDS.toHours(SharedPref.getLong(SharedPref.SEARCH_PATIENT_BTG_TIME) - System.currentTimeMillis());
                if (fragmentMap.get(FILTER_BTG) != null && hour < 0 || hour > VariableConstants.BTG_SESSION_TIMOUT) {
                    ((SearchBTGFragment) fragmentMap.get(FILTER_BTG)).setSearchClick();
                }
                Utils.hideKeyboard(SearchFragment.this.getActivity(), tabLayout);
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_SEARCH_FRAGMENT);
                break;
            case FILTER_BTG:
                MyApplication.getInstance().trackScreenView(GoogleAnalyticsConstants.RESULT_LOOKUP_FRAGMENT);
                Utils.hideKeyboard(SearchFragment.this.getActivity(), tabLayout);
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment;
        if (viewPager.getCurrentItem() == FILTER_SEARCH) {
            ((SearchFilterFragment) fragmentMap.get(FILTER_SEARCH)).onActivityResult(requestCode, resultCode, data);
        } else {
            ((SearchBTGFragment) fragmentMap.get(FILTER_BTG)).onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        Fragment fragment;
        if (viewPager.getCurrentItem() == FILTER_SEARCH) {
            fragment = ((SearchFilterFragment) fragmentMap.get(FILTER_SEARCH));
        } else {
            fragment = ((SearchBTGFragment) fragmentMap.get(FILTER_BTG));
        }
        switch (v.getId()) {
            case R.id.bt_toolbar_icon:
                DialogUtils.showAlertDialogCommon(getActivity(), getActivity().getString(R.string.disclaimer), getActivity().getString(R.string.disclaimer_body), getActivity().getString(R.string.ok), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
        }
        if (viewPager.getCurrentItem() == FILTER_SEARCH && fragment instanceof SearchFilterFragment) {
            ((SearchFilterFragment) fragment).onClick(v);
        } else if (viewPager.getCurrentItem() == FILTER_BTG && fragment instanceof SearchBTGFragment) {
            ((SearchBTGFragment) fragment).onClick(v);
        }
    }

    public ViewPager getViewPager() {
        return viewPager;
    }
}
