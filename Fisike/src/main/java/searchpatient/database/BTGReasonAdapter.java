package searchpatient.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.mphrx.fisike.models.AgreementModel;
import com.mphrx.fisike.persistence.DBHelper;
import com.mphrx.fisike.persistence.DBManager;
import com.mphrx.fisike.persistence.VitalsDBAdapter;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by kailashkhurana on 08/08/17.
 */

public class BTGReasonAdapter {
    private static BTGReasonAdapter btgReasonAdapter;

    private Context context;
    private SQLiteDatabase database;
    private DBManager dbManager;

    private static final String BTG_REASON_TABLE = "BTG_REASON_TABLE";
    private static final String REASONS = "Reasons";


    public BTGReasonAdapter(Context context) {
        this.context = context;
    }

    public static BTGReasonAdapter getInstance(Context ctx) {
        if (null == btgReasonAdapter) {
            synchronized (VitalsDBAdapter.class) {
                if (btgReasonAdapter == null) {
                    btgReasonAdapter = new BTGReasonAdapter(ctx);
                    btgReasonAdapter.open();
                }
            }
        }
        return btgReasonAdapter;
    }

    public void open() throws SQLException {
        database = DBHelper.getInstance(context);
    }

    public static String createVitalsQuery() {
        String query;
        query = "CREATE TABLE " + BTG_REASON_TABLE + "(" +
                REASONS + " BLOB)";
        return query;
    }

    /**
     * @return
     * @throws Exception
     */
    public ArrayList<String> fetchReasons() throws Exception {
        Cursor mCursor = database.query(true, BTG_REASON_TABLE, null, null, null, null, null, null, null);
        ArrayList<AgreementModel> agreementModelArrayList = new ArrayList<AgreementModel>();
        if (mCursor == null) {
            return null;
        }
        try {
            mCursor.moveToFirst();
            byte[] contentResults = mCursor.getBlob(mCursor.getColumnIndex(REASONS));
            if (contentResults != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(contentResults);
                ObjectInputStream ois = new ObjectInputStream(bais);
                ArrayList<String> reasonList = (ArrayList<String>) ois.readObject();
                return reasonList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCursor.close();
        }
        return null;
    }

    /**
     * This method creates or updates the BTGreason details
     *
     * @return
     * @throws Exception
     */
    public boolean insertBTGReasons(ArrayList<String> btgReasonList) {

        try {
            ContentValues contentValues = new ContentValues();
            if (btgReasonList != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bytes);
                out.writeObject(btgReasonList);
                contentValues.put(REASONS, bytes.toByteArray());
            }
            database.insert(BTG_REASON_TABLE, null, contentValues);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
