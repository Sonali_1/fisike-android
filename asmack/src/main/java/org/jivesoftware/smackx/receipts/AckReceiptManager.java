package org.jivesoftware.smackx.receipts;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.DiscoverInfo;

public class AckReceiptManager implements PacketListener{
	 private static Map<Connection, AckReceiptManager> instances =
	            Collections.synchronizedMap(new WeakHashMap<Connection, AckReceiptManager>());

	    static {
	        Connection.addConnectionCreationListener(new ConnectionCreationListener() {
	            public void connectionCreated(Connection connection) {
	                getInstanceFor(connection);
	            }
	        });
	    }

	    private Connection connection;
	    private boolean auto_receipts_enabled = false;
	    private Set<ReceiptReceivedListener> receiptReceivedListeners = Collections
	            .synchronizedSet(new HashSet<ReceiptReceivedListener>());

	    private AckReceiptManager(Connection connection) {
	        ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection);
	        sdm.addFeature(AckReceipt.NAMESPACE);
	        this.connection = connection;
	        instances.put(connection, this);

	        // register listener for ack receipts and requests
	        connection.addPacketListener(this, new PacketExtensionFilter(AckReceipt.NAMESPACE));
	    }

	    /**
	     * Obtain the AckReceiptManager responsible for a connection.
	     *
	     * @param connection the connection object.
	     *
	     * @return the AckReceiptManager instance for the given connection
	     */
	     public static synchronized AckReceiptManager getInstanceFor(Connection connection) {
	        AckReceiptManager receiptManager = instances.get(connection);

	        if (receiptManager == null) {
	            receiptManager = new AckReceiptManager(connection);
	        }

	        return receiptManager;
	    }

	    /**
	     * Returns true if Ack Receipts are supported by a given JID
	     * 
	     * @param jid
	     * @return true if supported
	     */
	    public boolean isSupported(String jid) {
	        try {
	            DiscoverInfo result =
	                ServiceDiscoveryManager.getInstanceFor(connection).discoverInfo(jid);
	            return result.containsFeature(AckReceipt.NAMESPACE);
	        }
	        catch (XMPPException e) {
	            return false;
	        }
	    }

	    // handle incoming receipts and receipt requests
	    @Override
	    public void processPacket(Packet packet) {
	        AckReceipt dr = (AckReceipt)packet.getExtension(
	                AckReceipt.ELEMENT, AckReceipt.NAMESPACE);
	        if (dr != null) {
	            // notify listeners of incoming receipt
	            for (ReceiptReceivedListener l : receiptReceivedListeners) {
	                l.onReceiptReceived(packet.getFrom(), packet.getTo(), dr.getId());
	            }

	        }

	        // if enabled, automatically send a receipt
	        if (auto_receipts_enabled) {
	            AckReceiptRequest drr = (AckReceiptRequest)packet.getExtension(
	                    AckReceiptRequest.ELEMENT, AckReceipt.NAMESPACE);
	            if (drr != null) {
	                Message ack = new Message(packet.getFrom(), Message.Type.normal);
	                ack.addExtension(new AckReceipt(packet.getPacketID()));
	                connection.sendPacket(ack);
	            }
	        }
	    }

	    /**
	     * Configure whether the {@link AckReceiptManager} should automatically
	     * reply to incoming {@link AckReceipt}s. By default, this feature is off.
	     *
	     * @param new_state whether automatic transmission of
	     *                  AckReceipts should be enabled or disabled
	     */
	    public void setAutoReceiptsEnabled(boolean new_state) {
	        auto_receipts_enabled = new_state;
	    }

	    /**
	     * Helper method to enable automatic AckReceipt transmission.
	     */
	    public void enableAutoReceipts() {
	        setAutoReceiptsEnabled(true);
	    }

	    /**
	     * Helper method to disable automatic AckReceipt transmission.
	     */
	    public void disableAutoReceipts() {
	        setAutoReceiptsEnabled(false);
	    }

	    /**
	     * Check if AutoReceipts are enabled on this connection.
	     */
	    public boolean getAutoReceiptsEnabled() {
	        return this.auto_receipts_enabled;
	    }

	    /**
	     * Get informed about incoming ack receipts with a {@link ReceiptReceivedListener}.
	     * 
	     * @param listener the listener to be informed about new receipts
	     */
	    public void addReceiptReceivedListener(ReceiptReceivedListener listener) {
	        receiptReceivedListeners.add(listener);
	    }

	    /**
	     * Stop getting informed about incoming ack receipts.
	     * 
	     * @param listener the listener to be removed
	     */
	    public void removeReceiptReceivedListener(ReceiptReceivedListener listener) {
	        receiptReceivedListeners.remove(listener);
	    }

	    /**
	     * Test if a packet requires a ack receipt.
	     *
	     * @param p Packet object to check for a AckReceiptRequest
	     *
	     * @return true if a ack receipt was requested
	     */
	    public static boolean hasAckReceiptRequest(Packet p) {
	        return (p.getExtension(AckReceiptRequest.ELEMENT,
	                    AckReceipt.NAMESPACE) != null);
	    }

	    /**
	     * Add a ack receipt request to an outgoing packet.
	     *
	     * Only message packets may contain receipt requests as of XEP-0184,
	     * therefore only allow Message as the parameter type.
	     *
	     * @param m Message object to add a request to
	     */
	    public static void addAckReceiptRequest(Message m) {
	        m.addExtension(new AckReceiptRequest());
	    }

}
