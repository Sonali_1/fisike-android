package org.jivesoftware.smackx.receipts;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class AckReceiptRequest implements PacketExtension {
	public static final String ELEMENT = "request";

	public String getElementName() {
		return ELEMENT;
	}

	public String getNamespace() {
		return AckReceipt.NAMESPACE;
	}

	public String toXML() {
		return "<request xmlns='" + AckReceipt.NAMESPACE + "'/>";
	}

	/**
	 * This Provider parses and returns DeliveryReceiptRequest packets.
	 */
	public static class Provider implements PacketExtensionProvider {
		@Override
		public PacketExtension parseExtension(XmlPullParser parser) {
			return new AckReceiptRequest();
		}
	}
}
