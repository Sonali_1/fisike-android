package org.jivesoftware.smackx.receipts;

import java.util.ArrayList;

public class DeliveryReceiptAck {
	public static final String ELEMENT = "item";

	private ArrayList<String> id; // / original ID of the delivered message
	private String from;
	private String to;

	public DeliveryReceiptAck(ArrayList<String> id) {
		this.id = id;
	}

	@Override
	public String toString() {
		String str = new String();
		for (int i = 0; id != null && i < id.size(); i++) {
			str += "<item from='spool'>" + id + "</item>";
		}
		return "<iq type='set' id='" + id + "' from='" + from + "' to='" + to + "' > <query xmlns='jabber:iq:private' action='delete'> " + str + "</query>";
	}
	/*
	 * <iq type='set' id='32075' from='abc@dev1' to='dev1' > <query xmlns='jabber:iq:private' action='delete'> <item from='spool'>messageID1</item> <item from='spool'>messageID2</item> </query> </iq>
	 */
}
