package org.jivesoftware.smackx.receipts;

import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;


/**
 * 
 * @author kkhurana
 */

public class AckReceipt implements PacketExtension {

	public static final String NAMESPACE = "urn:xmpp:acknowledgement";
	public static final String ELEMENT = "acknowledgement";

	private String id; // / original ID of the delivered message

	public AckReceipt(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Override
	public String getElementName() {
		return ELEMENT;
	}

	@Override
	public String getNamespace() {
		return NAMESPACE;
	}

	@Override
	public String toXML() {
		return "<acknowledgement xmlns='" + NAMESPACE + "' id='" + id + "'/>";
	}

	/**
	 * This Provider parses and returns AckReceipt packets.
	 */
	public static class Provider extends EmbeddedExtensionProvider {

		@Override
		protected PacketExtension createReturnExtension(String currentElement, String currentNamespace, Map<String, String> attributeMap, List<? extends PacketExtension> content) {
			return new AckReceipt(attributeMap.get("id"));
		}

	}

}
